﻿using System;
using System.Collections;
using System.Data;


namespace DataAccess
{
    public class CFHATotalAuditData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        static CFHATotalAuditData()
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("sfTransformDataToPml");
            list.Add("sApprVal");
            list.Add("sTransNetCash");
            list.Add("sLAmtCalc");
            list.Add("sLPurposeT");
            list.Add("sApprVal");
            list.Add("aAsstLiqTot");
            list.Add("sPurchPrice");
            list.Add("sCreditScoreType2Soft");
            list.Add("sTotCcPboPbs");
            list.Add("sTotTransC");
            list.Add("sFinalLAmt");
            list.Add("sLTotI");
            list.Add("sLiaMonLTot");
            list.Add("sFfUfmip1003");
            list.Add("sFfUfmipFinanced");
            list.Add("aBDecCitizen");
            list.Add("aBDecResidency");
            list.Add("aBDecForeignNational");
            list.Add("aCDecCitizen");
            list.Add("aBEmpCollection");
            list.Add("aCEmpCollection");
            list.Add("aBSsn");
            list.Add("aCSsn");
            list.Add("aBFirstNm");
            list.Add("aBMidNm");
            list.Add("aBLastNm");
            list.Add("aCFirstNm");
            list.Add("aCMidNm");
            list.Add("aCLastNm");
            list.Add("aCDecOcc");
            list.Add("aAsstLiqTot");
            list.Add("aLiaMonTot");
            list.Add("aBTotI");
            list.Add("aLiaXmlContent");
            list.Add("aReXmlContent");

            list.Add("aBDecOcc");


            list.Add("aBEquifaxScore");
            list.Add("aBTransUnionScore");
            list.Add("aBEquifaxScore");
            list.Add("aCDecResidency");
            list.Add("aCEquifaxScore");
            list.Add("aCTransUnionScore");
            list.Add("aCEquifaxScore");

            list.Add("aBBaseI");
            list.Add("aLiaMonTot");
            list.Add("sApprVal");
            list.Add("aCEquifaxScore");
            list.Add("aCEquifaxScore");
            list.Add("sTransNetCashLckd");
            list.Add("sLPurposeT");

            list.Add("aFHABBaseI");
            list.Add("aFHACBaseI");
            list.Add("aFHABOI");
            list.Add("aFHACOI");
            list.Add("aFHANetRentalI");
            list.Add("sFHAGift1gAmt");
            list.Add("sFHAGift2gAmt");
            list.Add("sFHAPro1stMPmt");
            list.Add("sTransmFntcLckd");
            list.Add("sVerifAssetAmt");
            list.Add("sFHASecondaryFinancingAmt");
            list.Add("sTotalScoreRefiT");
            list.Add("sTotalScoreCurrentMortgageStatusT");
            list.Add("sTotalScoreFhaProductT");
            list.Add("sFHASellerContribution");
            list.Add("sFinMethT");
            list.Add("sTransmFntc");

            list.Add("sTotalScoreAppraisedFairMarketRent");
            list.Add("sTotalScoreVacancyFactor");
            list.Add("sTotalScoreNetRentalIncome");
            list.Add("sTotalScoreIsIdentityOfInterest");

            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sRAdj1stCapMon");

            list.Add("sConcurSubFin");
            list.Add("sSubFin");


            list.Add("sFHASecondaryFinancingIsGov");
            list.Add("sFHASecondaryFinancingIsFamily");
            list.Add("sFHASecondaryFinancingIsNP");
            list.Add("sFHASecondaryFinancingIsOther");
            list.Add("sFHASecondaryFinancingOtherDesc");
            list.Add("aFHABCaivrsNum");
            list.Add("aFHACCaivrsNum");


            list.Add("aFHABLdpGsaTri");
            list.Add("aFHACLdpGsaTri");

            list.Add("aBDecPastOwnership");
            list.Add("aCDecPastOwnership");
            list.Add("sFHALenderIdCode");
            list.Add("sFHASponsorAgentIdCode");
            list.Add("sAgencyCaseNum");

            list.Add("aBTotalScoreIsCAIVRSAuthClear");
            list.Add("aCTotalScoreIsCAIVRSAuthClear");
            list.Add("aBTotalScoreFhtbCounselingT");
            list.Add("aCTotalScoreFhtbCounselingT");

            list.Add("sTotalScoreUploadDataFHATransmittal");
            list.Add("sTotalScoreUploadData1003");

            list.Add("aBSuffix");
            list.Add("aCSuffix");

            list.Add("sUnitsNum");
            list.Add("aBAddrYrs");
            list.Add("aBDependNum");
            list.Add("aPres1stM");
            list.Add("aPresRent");
            list.Add("aPresOFin");

            list.Add("aPresHazIns");
            list.Add("aPresRealETx");
            list.Add("aPresMIns");
            list.Add("aPresHoAssocDues");
            list.Add("aPresOHExp");

            list.Add("aBAge");
            list.Add("aCAge");
            list.Add("aBIsAmericanIndian");
            list.Add("aBIsAsian");
            list.Add("aBIsBlack");
            list.Add("aBIsPacificIslander");
            list.Add("aBIsWhite");
            list.Add("aBNoFurnish");
            list.Add("aCIsAmericanIndian");
            list.Add("aCIsAsian");
            list.Add("aCIsBlack");
            list.Add("aCIsPacificIslander");
            list.Add("aCIsWhite");
            list.Add("aCNoFurnish");
            list.Add("aBHispanicT");
            list.Add("aCHispanicT");
            list.Add("aBDob");
            list.Add("aCDob");
            list.Add("aBGender");
            list.Add("aCGender");
            list.Add("sTotalScoreUniqueLoanId");
            list.Add("aOpNegCfLckd");
            list.Add("aBNetNegCf");
            list.Add("aCNetNegCf");
            list.Add("sTotalScoreVersion");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpLien");
            list.Add("sGseSpT");
            list.Add("sUnitsNum");
            list.Add("sSpCountyFips");
            list.Add("sFHASecondaryFinancingIsGov");
            list.Add("sFHASecondaryFinancingIsNP");
            list.Add("sFHASecondaryFinancingIsFamily");
            list.Add("sFHAConstructionT");
            list.Add("sFHAPurposeIs203k");
            list.Add("sFHAHousingActSection");
            list.Add("aBAddrYrs");
            list.Add("aCAddrYrs");
            list.Add("aCDependNum");
            list.Add("sFHAPurposeIsPurchaseExistHome");
            list.Add("sFHAPurposeIsConstructHome");
            list.Add("sTotalScoreCertificateXmlContent");
            list.Add("sProdSpT");
            list.Add("sProdSpStructureT");
            list.Add("sOccR");
            list.Add("sGrossProfit");
            list.Add("sProRealETxPe");
            list.Add("sProOHExpPe");
            list.Add("sProdCondoStories");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("sOccT");
            list.Add("sSpZip");
            list.Add("sSpCounty");
            list.Add("sFHAProMInsLckd");
            list.Add("sFHAProMIns");
            list.Add("sFHAProHoAssocDues");
            list.Add("sFHAProGroundRent");
            list.Add("sFHAPro2ndFinPmt");
            list.Add("sFHAProHazIns");
            list.Add("sFHAProRealETx");
            list.Add("sFHADebtLckd");
            list.Add("aFHADebtInstallPmt");
            list.Add("aFHAChildSupportPmt");
            list.Add("sFHAPmtFixedTot");
            list.Add("sBranchId");
            list.Add("sTempLpeTaskMessageXml");
            list.Add("sProdLpePriceGroupId");

            list.Add("aProdBCitizenT");
            list.Add("aProdCCitizenT");
            list.Add("aOpNegCfPeval");
            list.Add("sIsSelfEmployed");
            list.Add("sOpNegCfPeval");
            list.Add("sIsCommunityLending");
            list.Add("aBorrowerCreditModeT");
            list.Add("sTotalScoreCreditRiskResultT");
            list.Add("sfLockProMInsUsingUWShorthand");

            list.Add("sFHAIncomeLckd");
            list.Add("sFHAPurposeIsFinanceImprovement");
            list.Add("sFHAPurposeIsFinanceCoopPurchase");
            list.Add("sFHAPurposeIsPurchaseNewCondo");
            list.Add("sFHAPurposeIsPurchaseExistCondo");
            list.Add("sFHAPurposeIsPurchaseNewHome");
            list.Add("sFHAPurposeIsRefinance");
            list.Add("sFHAPurposeIsConstructHome");
            list.Add("sFHAPurposeIsPurchaseManufacturedHome");
            list.Add("sFHAPurposeIsManufacturedHomeAndLot");
            list.Add("sFHAPurposeIsRefiManufacturedHomeToBuyLot");
            list.Add("sFHAPurposeIsRefiManufacturedHomeOrLotLoan");
            list.Add("sFHAPurposeIsEnergyEfficientMortgage");
            list.Add("sFHACcTot");
            list.Add("sTotalScoreDataConflictSourceT");
            list.Add("sTotalScorePreReviewResultT");
            list.Add("sTotalScoreReviewRules");
            list.Add("aBMaritalStatT");
            list.Add("sTotalScoreHasConflictBetweenFhaTransmittalAnd1003");
            list.Add("aLiaBalTot");
            list.Add("aReTotVal");
            list.Add("aReTotMAmt");
            list.Add("sMortgageLiaList");
            list.Add("sFannieCommunityLendingT");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sTotalScoreIsIdentityOfInterestException");
            list.Add("sH4HIsAllReoInheritedTri");
            list.Add("sH4HAdditionalHousingExpenseTotal");
            list.Add("sH4HPresentMortgagePaymentTotal");
            list.Add("sH4HPresentMortgageTopRatio");
            list.Add("sH4HHasOtherREO");
            list.Add("aPresOHExpDesc");
            list.Add("sfSetH4HStatus");
            list.Add("sH4HCertificateXmlContent");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("BranchNm");
            list.Add("sTotalScoreFrontEndRatio");
            list.Add("sTotalScoreBackEndRatio");
            list.Add("sTotalScoreTempCertificateXmlContent");
            list.Add("sMclCraCode");
            list.Add("sNonOccCoBorrOnOrigNoteT");
            list.Add("sH4HOriginationReqMetT");
            list.Add("aBHasFraudConvictionT");
            list.Add("aCHasFraudConvictionT");
            list.Add("sTotalScorecardAnnualMipFactor");
            list.Add("sTotalScorecardUpfrontMipFactor");
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("sLPurposeTTotalScorecardWarning");
            list.Add("aBH4HNonOccInterestT");
            list.Add("aCH4HNonOccInterestT");
            list.Add("sFHASponsoredOriginatorEIN");
            list.Add("sFhaLenderIdT");
            list.Add("sTotalScorecardFirstRunD"); // opm 67722 av
            list.Add("sTotalScorecardLastRunD"); // opm 102976 sk
            list.Add("sLNm");
            list.Add("sPrimaryNm");
            list.Add("sEmployeeLoanRep");
            list.Add("sEmployeeUnderwriter");
            list.Add("sTotCcPbs");
            list.Add("sAusFindingsPull");
            list.Add("sTotEstCcPbb");
            list.Add("sTotEstCcNoDiscnt1003");
            list.Add("sTotCcPbs");
            list.Add(nameof(sProdSpT)); // OPM 457191 replaced sProdSpT with sGseSpT[FriendlyDisplay], but going through all references to remove this field seems too risky.
            list.Add(nameof(sGseSpTFriendlyDisplay));
            list.Add("aAssetCollection");
            list.Add("aLiaCollection");
            list.Add("aCrRd");
            list.Add("sPrimaryGiftFundSource");
            list.Add("sDwnPmtSrc");
            list.Add("sTotalScoreEvalStatusT");
            list.Add("sLTotBaseI");
            list.Add("sLTotOvertimeI");
            list.Add("sLTotBonusesI");
            list.Add("sLTotCommisionI");
            list.Add("sLTotDividendI");
            list.Add("sLTotNetRentI1003");
            list.Add("sLTotSpPosCfPlussLTotOI");
            list.Add("sTotalScoreLtvR");
            list.Add("sTotalScoreCLtvR");
            list.Add("sProMIns");
            list.Add("sIsRefinancing");
            list.Add("sBrokerId");
            list.Add("aBTypeT");
            list.Add("aCTypeT");
            list.Add("sUseLegacyTotalCounselTypeDefinition");
            list.Add("sTotalScoreSecondaryFinancingSrc");
            list.Add("sTotalScoreSecondaryFinancingSrcLckd");

            list.Add("sIsTargeting2019Ulad");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }
        public CFHATotalAuditData(Guid fileId)
            : base(fileId, "CFHATotalAuditData", s_selectProvider)
        {
        }
    }
}
