﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using LendersOffice.UI;

namespace DataAccess.HomeownerCounselingOrganizations
{
    [XmlRoot]
    public class HomeownerCounselingOrganizationCollection : List<HomeownerCounselingOrganization>, IEquatable<HomeownerCounselingOrganizationCollection>
    {
        public string Serialize()
        {
            this.ForEach((co) => co.Id = co.Id == Guid.Empty ? Guid.NewGuid() : co.Id);

            XmlSerializer serializer = new XmlSerializer(this.GetType());
            string ret;

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, this);
                ret = writer.ToString();
            }

            return ret;
        }

        public static HomeownerCounselingOrganizationCollection Deserialize(string input)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(HomeownerCounselingOrganizationCollection));
            using (var sr = new StringReader(input))
            {
                return (HomeownerCounselingOrganizationCollection)deserializer.Deserialize(sr);
            }
        }

        /// <summary>
        /// Remove a homeowner counseling organization by id.
        /// </summary>
        /// <param name="hocId">The id of the homeowner counseling organization.</param>
        /// <returns>True if removed. False otherwise.</returns>
        public bool RemoveById(Guid hocId)
        {
            return this.RemoveAll((hoc) => hoc.Id == hocId) != 0;
        }

        /// <summary>
        /// Retrieves a homeowner counseling organization by id.
        /// </summary>
        /// <param name="hocId">The id of the homeowner counseling organization.</param>
        /// <returns>The homeowner counseling organization if found. Null otherwise.</returns>
        public HomeownerCounselingOrganization RetrieveById(Guid hocId)
        {
            return this.FirstOrDefault((hoc) => hoc.Id == hocId);
        }

        public bool Equals(HomeownerCounselingOrganizationCollection other)
        {
            return this.Equals(other, ignoreId: false);
        }

        public bool Equals(HomeownerCounselingOrganizationCollection other, bool ignoreId)
        {
            if (other == null || other.Count != this.Count)
            {
                return false;
            }

            for (var i = 0; i < this.Count; ++i)
            {
                var thisHco = this[i];
                var otherHco = other[i];

                if (!thisHco.Equals(otherHco, ignoreId))
                {
                    return false;
                }
            }

            return true;
        }
    }

    public class HomeownerCounselingOrganization : IEquatable<HomeownerCounselingOrganization>
    {
        [XmlAttribute]
        public string Name { get; set; }

        public Address Address { get; set; }
        
        [XmlAttribute]
        public string Phone { get; set; }

        [XmlAttribute]
        public string Email { get; set; }

        [XmlAttribute]
        public string Website { get; set; }

        [LqbInputModel(type: InputFieldType.TextArea)]
        public string Services { get; set; }

        [LqbInputModel(type: InputFieldType.TextArea)]
        public string Languages { get; set; }

        [XmlAttribute]
        public string Distance { get; set; }

        [XmlAttribute]
        public Guid Id
        {
            get;
            set;
        }

        public HomeownerCounselingOrganization()
        {
            Address = new Address();
        }

        public HomeownerCounselingOrganization(bool shouldSetNewId)
            :this()
        {
            if (shouldSetNewId)
            {
                this.Id = Guid.NewGuid();
            }
        }

        public bool IsDataComplete()
        {
            return !string.IsNullOrWhiteSpace(this.Name) &&
                   !string.IsNullOrWhiteSpace(this.Phone) &&
                   !string.IsNullOrWhiteSpace(this.Email) &&
                   !string.IsNullOrWhiteSpace(this.Website) &&
                   !string.IsNullOrWhiteSpace(this.Services) &&
                   !string.IsNullOrWhiteSpace(this.Languages) &&
                   !string.IsNullOrWhiteSpace(this.Distance) &&
				   this.Id != Guid.Empty &&
                   this.Address.IsDataComplete();
        }

        #region IEquatable<HomeownerCounselingOrganization> Members
        public override bool Equals(object obj)
        {
            return this.Equals(obj as HomeownerCounselingOrganization);
        }

        public bool Equals(HomeownerCounselingOrganization other)
        {
            return this.Equals(other, ignoreId: false);
        }

        public bool Equals(HomeownerCounselingOrganization other, bool ignoreId)
        {
            if (other == null) return false;

            return this.Name == other.Name &&
                   this.Address.Equals(other.Address) &&
                   this.Phone == other.Phone &&
                   this.Email == other.Email &&
                   this.Website == other.Website &&
                   this.Services == other.Services &&
                   this.Languages == other.Languages &&
                   this.Distance == other.Distance &&
                   (ignoreId || this.Id == other.Id);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                int filler = 2; //Just a prime number that will fill in when there's a null - 29 + 2 == 31, another prime
                hash = hash * 29 + (Name == null ? filler : Name.GetHashCode());
                hash = hash * 29 + (Address == null ? filler : Address.GetHashCode());
                hash = hash * 29 + (Phone == null ? filler : Phone.GetHashCode());
                hash = hash * 29 + (Email == null ? filler : Email.GetHashCode());
                hash = hash * 29 + (Services == null ? filler : Services.GetHashCode());
                hash = hash * 29 + (Languages == null ? filler : Languages.GetHashCode());
                hash = hash * 29 + (Distance == null ? filler : Distance.GetHashCode());
                hash = hash * 29 + (this.Id == null ? filler : Id.GetHashCode());

                return hash;
            }
        }

        #endregion
    }
    public class Address : IEquatable<Address>
    {
        [XmlAttribute]
        public string StreetAddress { get; set; }

        [XmlAttribute]
        public string StreetAddress2 { get; set; }

        [XmlAttribute]
        public string City { get; set; }

        [XmlAttribute]
        public string State { get; set; }

        [XmlAttribute]
        [LqbInputModel(name: "Zip")]
        public string Zipcode { get; set; }

        public bool IsDataComplete()
        {
            return !string.IsNullOrWhiteSpace(StreetAddress) &&
                   !string.IsNullOrWhiteSpace(City) &&
                   !string.IsNullOrWhiteSpace(Zipcode) &&
                   !string.IsNullOrWhiteSpace(State);
        }

        #region IEquatable<Address> Members

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Address);
        }

        public bool Equals(Address other)
        {
            if (other == null) return false;

            return this.StreetAddress == other.StreetAddress &&
                   this.StreetAddress2 == other.StreetAddress2 &&
                   this.City == other.City &&
                   this.Zipcode == other.Zipcode &&
                   this.State == other.State;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                int filler = 2; //Just a prime number that will fill in when there's a null; 29 + 2 == 31, another prime
                hash = hash * 29 + (StreetAddress == null ? filler : StreetAddress.GetHashCode());
                hash = hash * 29 + (StreetAddress2 == null ? filler : StreetAddress2.GetHashCode());
                hash = hash * 29 + (City == null ? filler : City.GetHashCode());
                hash = hash * 29 + (Zipcode == null ? filler : Zipcode.GetHashCode());
                hash = hash * 29 + (State == null ? filler : State.GetHashCode());
                return hash;
            }
        }
        #endregion
    }
}
