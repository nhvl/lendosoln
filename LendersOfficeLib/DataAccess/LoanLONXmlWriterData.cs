namespace DataAccess
{
    using System;

    public class CLoanLONXmlWriterData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanLONXmlWriterData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sLNm");
            list.Add("BranchNm");
            list.Add("BranchCode");
            list.Add("Division");
            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sStatusT");
            list.Add("sFinalLAmt");
            list.Add("sLpTemplateNm");
            list.Add("sOpenedD");
            list.Add("sOpenedN");
            list.Add("sSubmitD");
            list.Add("sSubmitN");
            list.Add("sApprovD");
            list.Add("sApprovN");
            list.Add("sDocsD");
            list.Add("sDocsN");
            list.Add("sFundD");
            list.Add("sFundN");
            list.Add("sRecordedD");
            list.Add("sRecordedN");
            list.Add("sRejectD");
            list.Add("sRejectN");
            list.Add("sSuspendedD");
            list.Add("sSuspendedN");
            list.Add("sCanceledD");
            list.Add("sCanceledN");
            list.Add("sClosedN");
            list.Add("sClosedD");
            list.Add("sNoteIR");
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sSchedDueD1");
            list.Add("sEstCloseD");
            list.Add("sEstCloseN");
            list.Add("aBFirstNm");
            list.Add("aBLastNm");
            list.Add("aCFirstNm");
            list.Add("aCLastNm");
            list.Add("sApprRprtOd");
            list.Add("sApprRprtRd");
            list.Add("aCrOd");
            list.Add("aCrRd");
            list.Add("sPrelimRprtOd");
            list.Add("sPrelimRprtRd");
            list.Add("sU1DocStatDesc");
            list.Add("sU1DocStatOd");
            list.Add("sU1DocStatRd");
            list.Add("sU2DocStatDesc");
            list.Add("sU2DocStatOd");
            list.Add("sU2DocStatRd");
            list.Add("sU3DocStatDesc");
            list.Add("sU3DocStatOd");
            list.Add("sU3DocStatRd");
            list.Add("sU4DocStatDesc");
            list.Add("sU4DocStatOd");
            list.Add("sU4DocStatRd");
            list.Add("sU5DocStatDesc");
            list.Add("sU5DocStatOd");
            list.Add("sU5DocStatRd");
            list.Add("sCondDataSet");
            list.Add("aBEmail");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("aBAddr");
            list.Add("aBCity");
            list.Add("aBState");
            list.Add("aBZip");
            list.Add("sAgentDataSet");
            list.Add("sPreparerDataSet");
            list.Add("aBSsn");
            list.Add("sMonthlyPmt");
            list.Add("sProThisMPmt");
            list.Add("sBrokerLockOriginatorPriceBrokComp1PcPrice");
            list.Add("sApprVal");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add(nameof(CPageData.IsValid));
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );


        }        
	        
		public CLoanLONXmlWriterData(Guid fileId) : base(fileId, "CLoanLONXmlWriterData", s_selectProvider)
		{
		}
	}
}
