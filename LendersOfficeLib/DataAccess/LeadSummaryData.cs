﻿using System;
using System.Collections;

namespace DataAccess
{
    public class CLeadSummaryData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        static CLeadSummaryData()
        {
            StringList list = new StringList();

            list.Add("sLNm");
            list.Add("aBLastNm");
            list.Add("aBFirstNm");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("aBExperianScore");
            list.Add("aBTransUnionScore");
            list.Add("aBEquifaxScore");
            list.Add("aCExperianScore");
            list.Add("aCTransUnionScore");
            list.Add("aCEquifaxScore");
            list.Add("sClosingCostFeeVersionT");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CLeadSummaryData(Guid fileId)
            : base(fileId, "LeadSummaryData", s_selectProvider)
        {
        }
    }
}