﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LendersOffice.Common;

    public enum E_RateLockAuditActionT
    {
        BreakRateLock,
        LockRate,
        ExtendRateLock,
        ClearRateLockRequest,
        LockRequested,
        FloatDownRate,
        ReLockRate,
        UNDEFINED,
        SuspendLock,
        ResumeLock
    }

    public abstract class AbstractRateLockHistoryItem
    {
        public string Action = "";
        public string EventDate = "";
        public string DoneBy = "";
        public string sRLckBreakReasonDesc = "";
        public string eventId = "";

        public E_RateLockAuditActionT sRateLockAuditActionT { get; set; }

        public List<PricingAdjustment> pricingAdjustments = new List<PricingAdjustment>();

        public int CompareTo(object o)
        {
            AbstractRateLockHistoryItem _o = (AbstractRateLockHistoryItem)o;

            DateTime dt1 = DateTime.Parse(EventDate.Replace("PDT", "").Replace("PST", ""));
            DateTime dt2 = DateTime.Parse(_o.EventDate.Replace("PDT", "").Replace("PST", ""));

            return dt2.CompareTo(dt1); // 10/9/2004 dd - Compare in DESCENDING order
        }
    }

    public sealed class RateLockHistoryItem : AbstractRateLockHistoryItem, IComparable
    {
        public string sNoteIR = "";
        public string sBrokComp1Pc = "";
        public string sRAdjMarginR = "";
        public string sTerm_Due = "";
        public string sAmortMethodT = "";
        public string sRLckdExpiredD = "";
        public string sPpmtPenaltyMon_rep = "";
        public string sRLckdD = "";
        public string sIsOptionArm = "";
        public string sOptionArmTeaserR = "";
        public string sQualIR = "";
        public string sLpTemplateNm = "";
        public string sRLckdDays = "";

        public string sBrokerLockBaseNoteIR = "";
        public string sBrokerLockBaseBrokComp1PcPrice = "";
        public string sBrokerLockBaseBrokComp1PcFee = "";
        public string sBrokerLockBaseRAdjMarginR = "";
        public string sBrokerLockBaseQualIR = "";
        public string sBrokerLockBaseOptionArmTeaserR = "";
        public string sBrokerLockBrokerBaseNoteIR = "";
        public string sBrokerLockBrokerBaseBrokComp1PcPrice = "";
        public string sBrokerLockBrokerBaseBrokComp1PcFee = "";
        public string sBrokerLockBrokerBaseRAdjMarginR = "";
        public string sBrokerLockBrokerBaseQualIR = "";
        public string sBrokerLockBrokerBaseOptionArmTeaserR = "";
        public string sBrokerLockTotHiddenAdjNoteIR = "";
        public string sBrokerLockTotHiddenAdjBrokComp1PcPrice = "";
        public string sBrokerLockTotHiddenAdjBrokComp1PcFee = "";
        public string sBrokerLockTotHiddenAdjRAdjMarginR = "";
        public string sBrokerLockTotHiddenAdjQualIR = "";
        public string sBrokerLockTotHiddenAdjOptionArmTeaserR = "";
        public string sBrokerLockTotVisibleAdjNoteIR = "";
        public string sBrokerLockTotVisibleAdjBrokComp1PcPrice = "";
        public string sBrokerLockTotVisibleAdjBrokComp1PcFee = "";
        public string sBrokerLockTotVisibleAdjRAdjMarginR = "";
        public string sBrokerLockTotVisibleAdjQualIR = "";
        public string sBrokerLockTotVisibleAdjOptionArmTeaserR = "";
        public string sBrokerLockFinalBrokComp1PcPrice = "";
        public string sBrokerLockRateSheetEffectiveD = "";

        private static IList<RateLockHistoryItem> SortRateHistory(XmlNodeList list)
        {
            List<RateLockHistoryItem> ret = new List<RateLockHistoryItem>();
            foreach (XmlElement el in list)
            {
                var o = GetRateLockHistoryItemFrom(el);
                ret.Add(o);
            }

            ret.Sort();
            return ret;
        }

        private static RateLockHistoryItem GetRateLockHistoryItemFrom(XmlElement el)
        {
            RateLockHistoryItem o = new RateLockHistoryItem();
            //                o.Action = el.GetAttribute("EvenType") == "BreakLockEvent" ? "Break Rate Lock" : "Rate Lock";

            switch (el.GetAttribute("EvenType"))
            {
                case "BreakLockEvent":
                    o.Action = "Break Rate Lock";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.BreakRateLock;
                    break;
                case "LockEvent":
                    o.Action = "Rate Locked";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.LockRate;
                    break;
                case "ExtendLockEvent":
                    o.Action = "Extend Lock Expiration Date";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ExtendRateLock;
                    break;
                case "RemoveRequestedRateEvent":
                    o.Action = "Clear Rate Lock Request";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ClearRateLockRequest;
                    break;
                case "LockRequestedEvent":
                    o.Action = "Lock Requested";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.LockRequested;
                    break;
                case "FloatDownEvent":
                    o.Action = "Rate Float Down";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.FloatDownRate;
                    break;
                case "ReLockRateEvent":
                    o.Action = "Re-Lock Rate";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ReLockRate;
                    break;
                case "SuspendLockEvent":
                    o.Action = "Lock Suspended";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.SuspendLock;
                    break;
                case "ResumeLockEvent":
                    o.Action = "Lock Modified";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ResumeLock;
                    break;
                default:
                    o.Action = "????";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.UNDEFINED;
                    Tools.LogBug("Rate Lock History. Unrecognized event type=" + el.GetAttribute("EvenType"));
                    break;
            }

            string term_due = el.GetAttribute("sTerm");

            if (el.GetAttribute("sDue") != "")
                term_due += " / " + el.GetAttribute("sDue");

            o.sTerm_Due = term_due;
            o.EventDate = el.GetAttribute("EventDate");
            o.DoneBy = el.GetAttribute("DoneBy");
            o.sNoteIR = el.GetAttribute("sNoteIR");
            // 10/28/2004 dd - Due to migration in Xml from sLOrigFpc to new sBrokComp1Pc. I have to read in the old value (sLOrigFPc)
            // if sBrokComp1Pc attribute does not exist.
            if (el.HasAttribute("sBrokComp1Pc"))
                o.sBrokComp1Pc = el.GetAttribute("sBrokComp1Pc");
            else
                o.sBrokComp1Pc = el.GetAttribute("sLOrigFPc"); // 10/28/2004 dd - New attribute does not existed. Read old value.

            o.sRLckdD = el.GetAttribute("sRLckdD");

            o.sRAdjMarginR = el.GetAttribute("sRAdjMarginR");
            o.sAmortMethodT = el.GetAttribute("sAmortMethodT");

            // 11/10/2004 dd - Added
            o.sRLckdExpiredD = el.GetAttribute("sRLckdExpiredD");
            if (o.Action == "Rate Lock" && o.sRLckdExpiredD == "")
                o.sRLckdExpiredD = "Unrecorded";

            o.sRLckBreakReasonDesc = el.GetAttribute("sRLckBreakReasonDesc");

            switch (el.GetAttribute("sPpmtPenaltyMon"))
            {
                case "0":
                    o.sPpmtPenaltyMon_rep = "No PP";
                    break;
                case "12":
                    o.sPpmtPenaltyMon_rep = "1 yr";
                    break;
                case "24":
                    o.sPpmtPenaltyMon_rep = "2 yrs";
                    break;
                case "36":
                    o.sPpmtPenaltyMon_rep = "3 yrs";
                    break;
                case "48":
                    o.sPpmtPenaltyMon_rep = "4 yrs";
                    break;
                case "60":
                    o.sPpmtPenaltyMon_rep = "5 yrs";
                    break;
                default:
                    o.sPpmtPenaltyMon_rep = "";
                    break;
            }

            o.sIsOptionArm = el.GetAttribute("sIsOptionArm");
            o.sOptionArmTeaserR = el.GetAttribute("sOptionArmTeaserR");
            if (o.sOptionArmTeaserR == "") o.sOptionArmTeaserR = "0.000%";
            o.sQualIR = el.GetAttribute("sQualIR");
            o.sLpTemplateNm = el.GetAttribute("sLpTemplateNm");
            o.sRLckdDays = el.GetAttribute("sRLckdDays");

            // MSDN: "An empty string is returned if a matching attribute is not found"

            o.eventId = el.GetAttribute("EventId");
            o.sBrokerLockBaseNoteIR = el.GetAttribute("sBrokerLockBaseNoteIR");
            o.sBrokerLockBaseBrokComp1PcPrice = el.GetAttribute("sBrokerLockBaseBrokComp1PcPrice");
            o.sBrokerLockBaseBrokComp1PcFee = el.GetAttribute("sBrokerLockBaseBrokComp1PcFee");
            o.sBrokerLockBaseRAdjMarginR = el.GetAttribute("sBrokerLockBaseRAdjMarginR");
            o.sBrokerLockBaseQualIR = el.GetAttribute("sBrokerLockBaseQualIR");
            o.sBrokerLockBaseOptionArmTeaserR = el.GetAttribute("sBrokerLockBaseOptionArmTeaserR");
            o.sBrokerLockBrokerBaseNoteIR = el.GetAttribute("sBrokerLockBrokerBaseNoteIR");
            o.sBrokerLockBrokerBaseBrokComp1PcPrice = el.GetAttribute("sBrokerLockBrokerBaseBrokComp1PcPrice");
            o.sBrokerLockBrokerBaseBrokComp1PcFee = el.GetAttribute("sBrokerLockBrokerBaseBrokComp1PcFee");
            o.sBrokerLockBrokerBaseRAdjMarginR = el.GetAttribute("sBrokerLockBrokerBaseRAdjMarginR");
            o.sBrokerLockBrokerBaseQualIR = el.GetAttribute("sBrokerLockBrokerBaseQualIR");
            o.sBrokerLockBrokerBaseOptionArmTeaserR = el.GetAttribute("sBrokerLockBrokerBaseOptionArmTeaserR");
            o.sBrokerLockTotHiddenAdjNoteIR = el.GetAttribute("sBrokerLockTotHiddenAdjNoteIR");
            o.sBrokerLockTotHiddenAdjBrokComp1PcPrice = el.GetAttribute("sBrokerLockTotHiddenAdjBrokComp1PcPrice");
            o.sBrokerLockTotHiddenAdjBrokComp1PcFee = el.GetAttribute("sBrokerLockTotHiddenAdjBrokComp1PcFee");
            o.sBrokerLockTotHiddenAdjRAdjMarginR = el.GetAttribute("sBrokerLockTotHiddenAdjRAdjMarginR");
            o.sBrokerLockTotHiddenAdjQualIR = el.GetAttribute("sBrokerLockTotHiddenAdjQualIR");
            o.sBrokerLockTotHiddenAdjOptionArmTeaserR = el.GetAttribute("sBrokerLockTotHiddenAdjOptionArmTeaserR");
            o.sBrokerLockTotVisibleAdjNoteIR = el.GetAttribute("sBrokerLockTotVisibleAdjNoteIR");
            o.sBrokerLockTotVisibleAdjBrokComp1PcPrice = el.GetAttribute("sBrokerLockTotVisibleAdjBrokComp1PcPrice");
            o.sBrokerLockTotVisibleAdjBrokComp1PcFee = el.GetAttribute("sBrokerLockTotVisibleAdjBrokComp1PcFee");
            o.sBrokerLockTotVisibleAdjRAdjMarginR = el.GetAttribute("sBrokerLockTotVisibleAdjRAdjMarginR");
            o.sBrokerLockTotVisibleAdjQualIR = el.GetAttribute("sBrokerLockTotVisibleAdjQualIR");
            o.sBrokerLockTotVisibleAdjOptionArmTeaserR = el.GetAttribute("sBrokerLockTotVisibleAdjOptionArmTeaserR");
            o.sBrokerLockFinalBrokComp1PcPrice = el.GetAttribute("sBrokerLockFinalBrokComp1PcPrice");
            o.sBrokerLockRateSheetEffectiveD = el.GetAttribute("sBrokerLockRateSheetEffectiveD");

            if (el.FirstChild != null && el.FirstChild.Name == "Adjustments")
            {
                XmlElement adjustmentList = (XmlElement)el.FirstChild;
                foreach (XmlElement adjustment in adjustmentList.ChildNodes)
                {
                    PricingAdjustment newAdj = new PricingAdjustment()
                    {
                        Description = adjustment.GetAttribute("Description"),
                        Rate = adjustment.GetAttribute("Rate"),
                        Price = adjustment.GetAttribute("Price"),
                        Fee = adjustment.GetAttribute("Fee"),
                        Margin = adjustment.GetAttribute("Margin"),
                        QualifyingRate = adjustment.GetAttribute("Qrate"),
                        TeaserRate = adjustment.GetAttribute("TRate"),
                        IsHidden = adjustment.GetAttribute("IsHidden") == "Yes",
                    };

                    o.pricingAdjustments.Add(newAdj);
                }
            }

            return o;
        }

        /// <summary>
        /// Parses the rate lock history XML. If the xml is null or empty it returns an empty list.
        /// The list is sorted.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static IList<RateLockHistoryItem> GetSortedRateHistory(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new List<RateLockHistoryItem>();
            }

            XmlDocument doc = Tools.CreateXmlDoc(xml);

            XmlNodeList nodeList = doc.SelectNodes("//RateLockHistory/Event");

            IList<RateLockHistoryItem> list = RateLockHistoryItem.SortRateHistory(nodeList);

            return list ;
        }

        /// <summary>
        /// Looks in the xml for the given rate lock event. If the event is not found a not found exception is thrown. 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static RateLockHistoryItem GetRateLockEventFromXml(string xml, Guid eventId)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new LendersOffice.Common.NotFoundException(ErrorMessages.Generic, "Rate Lock Event Requested, but xml is empty");
            }

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            XmlElement element = (XmlElement)doc.SelectSingleNode("//RateLockHistory/Event[@EventId='" + eventId.ToString() + "']");

            if (element == null)
            {
                throw new LendersOffice.Common.NotFoundException(ErrorMessages.Generic, "Rate lock event not found in xml.");
            }

            RateLockHistoryItem item = GetRateLockHistoryItemFrom(element);

            return item;
        }
    }

    public sealed class InvestorRateLockHistoryItem : AbstractRateLockHistoryItem, IComparable
    {
        //Lock Data
        public string sInvestorLockLpInvestorNm = "";
        public string sInvestorLockLpTemplateNm = "";
        public string sInvestorLockRLckdDays = ""; 
        public string sInvestorLockRLckdD = ""; 
        public string sInvestorLockRLckExpiredD = ""; 
        public string sInvestorLockDeliveryExpiredD = "";
        public string sInvestorLockLoanNum = "";
        public string sInvestorLockProgramId = string.Empty;
        public string sInvestorLockConfNum = ""; 
        public string sInvestorLockRateSheetEffectiveTime = "";
        public string sInvestorLockLockFee = ""; 
        public string sInvestorLockCommitmentT = ""; 

        // Pricing Data
        public string sInvestorLockNoteIR = ""; 
        public string sInvestorLockBrokComp1Pc = ""; 
        public string sInvestorLockBrokComp1PcPrice = "";
        public string sInvestorLockRAdjMarginR = ""; 
        public string sInvestorLockQualIR = ""; 
        public string sInvestorLockOptionArmTeaserR = ""; 
        public string sInvestorLockBaseNoteIR = ""; 
        public string sInvestorLockBaseBrokComp1PcFee = "";
        public string sInvestorLockBaseBrokComp1PcPrice = "";
        public string sInvestorLockBaseRAdjMarginR = ""; 
        public string sInvestorLockBaseQualIR = ""; 
        public string sInvestorLockBaseOptionArmTeaserR = "";
        public string sInvestorLockTotAdjNoteIR = ""; 
        public string sInvestorLockTotAdjBrokComp1PcFee = "";
        public string sInvestorLockTotAdjBrokComp1PcPrice = "";
        public string sInvestorLockTotAdjRAdjMarginR = "";
        public string sInvestorLockTotAdjQualIR = ""; 
        public string sInvestorLockTotAdjOptionArmTeaserR = "";
        
        private static IList<InvestorRateLockHistoryItem> SortRateHistory(XmlNodeList list)
        {
            List<InvestorRateLockHistoryItem> ret = new List<InvestorRateLockHistoryItem>();
            foreach (XmlElement el in list)
            {
                var o = GetRateLockHistoryItemFrom(el);
                ret.Add(o);
            }

            ret.Sort();
            return ret;
        }

        private static InvestorRateLockHistoryItem GetRateLockHistoryItemFrom(XmlElement el)
        {
            InvestorRateLockHistoryItem o = new InvestorRateLockHistoryItem();
            //                o.Action = el.GetAttribute("EvenType") == "BreakLockEvent" ? "Break Rate Lock" : "Rate Lock";

            switch (el.GetAttribute("EvenType"))
            {
                case "BreakInvestorLockEvent":
                    o.Action = "Break Rate Lock";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.BreakRateLock;
                    break;
                case "InvestorLockEvent" :
                    o.Action = "Rate Locked";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.LockRate;
                    break;
                case "ExtendInvestorLockEvent":
                    o.Action = "Extend Lock Expiration Date";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ExtendRateLock;
                    break;
                case "InvestorResumeLockEvent":
                    o.Action = "Lock Modified";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.ResumeLock;
                    break;
                case "SuspendInvestorLockEvent":
                    o.Action = "Lock Suspended";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.SuspendLock;
                    break;
                default:
                    o.Action = "????";
                    o.sRateLockAuditActionT = E_RateLockAuditActionT.UNDEFINED;
                    Tools.LogBug("Rate Lock History. Unrecognized event type=" + el.GetAttribute("EvenType"));
                    break;
            }

            o.EventDate = el.GetAttribute("EventDate");
            o.DoneBy = el.GetAttribute("DoneBy");
            o.sRLckBreakReasonDesc = el.GetAttribute("sRLckBreakReasonDesc");
            o.eventId = el.GetAttribute("EventId");

            // Lock Data
            o.sInvestorLockLpInvestorNm = el.GetAttribute("sInvestorLockLpInvestorNm");
            o.sInvestorLockLpTemplateNm = el.GetAttribute("sInvestorLockLpTemplateNm");
            o.sInvestorLockRLckdDays = el.GetAttribute("sInvestorLockRLckdDays");
            o.sInvestorLockRLckdD = el.GetAttribute("sInvestorLockRLckdD");
            o.sInvestorLockRLckExpiredD = el.GetAttribute("sInvestorLockRLckExpiredD");
            o.sInvestorLockDeliveryExpiredD = el.GetAttribute("sInvestorLockDeliveryExpiredD");
            o.sInvestorLockLoanNum = el.GetAttribute("sInvestorLockLoanNum");
            o.sInvestorLockProgramId = el.GetAttribute("sInvestorLockProgramId");
            o.sInvestorLockConfNum = el.GetAttribute("sInvestorLockConfNum");
            o.sInvestorLockRateSheetEffectiveTime = el.GetAttribute("sInvestorLockRateSheetEffectiveTime");
            o.sInvestorLockLockFee = el.GetAttribute("sInvestorLockLockFee");
            o.sInvestorLockCommitmentT = el.GetAttribute("sInvestorLockCommitmentT");

            // Pricing Data
            o.sInvestorLockNoteIR = el.GetAttribute("sInvestorLockNoteIR");
            o.sInvestorLockBrokComp1Pc = el.GetAttribute("sInvestorLockBrokComp1Pc");
            o.sInvestorLockBrokComp1PcPrice = el.GetAttribute("sInvestorLockBrokComp1PcPrice");
            o.sInvestorLockRAdjMarginR = el.GetAttribute("sInvestorLockRAdjMarginR");
            o.sInvestorLockQualIR = el.GetAttribute("sInvestorLockQualIR");
            o.sInvestorLockOptionArmTeaserR = el.GetAttribute("sInvestorLockOptionArmTeaserR");
            o.sInvestorLockBaseNoteIR = el.GetAttribute("sInvestorLockBaseNoteIR");
            o.sInvestorLockBaseBrokComp1PcFee = el.GetAttribute("sInvestorLockBaseBrokComp1PcFee");
            o.sInvestorLockBaseBrokComp1PcPrice = el.GetAttribute("sInvestorLockBaseBrokComp1PcPrice");
            o.sInvestorLockBaseRAdjMarginR = el.GetAttribute("sInvestorLockBaseRAdjMarginR");
            o.sInvestorLockBaseQualIR = el.GetAttribute("sInvestorLockBaseQualIR");
            o.sInvestorLockBaseOptionArmTeaserR = el.GetAttribute("sInvestorLockBaseOptionArmTeaserR");
            o.sInvestorLockTotAdjNoteIR = el.GetAttribute("sInvestorLockTotAdjNoteIR");
            o.sInvestorLockTotAdjBrokComp1PcFee = el.GetAttribute("sInvestorLockTotAdjBrokComp1PcFee");
            o.sInvestorLockTotAdjBrokComp1PcPrice = el.GetAttribute("sInvestorLockTotAdjBrokComp1PcPrice");
            o.sInvestorLockTotAdjRAdjMarginR = el.GetAttribute("sInvestorLockTotAdjRAdjMarginR");
            o.sInvestorLockTotAdjQualIR = el.GetAttribute("sInvestorLockTotAdjQualIR");
            o.sInvestorLockTotAdjOptionArmTeaserR = el.GetAttribute("sInvestorLockTotAdjOptionArmTeaserR");

            if (el.FirstChild != null && el.FirstChild.Name == "Adjustments")
            {
                XmlElement adjustmentList = (XmlElement)el.FirstChild;
                foreach (XmlElement adjustment in adjustmentList.ChildNodes)
                {
                    PricingAdjustment newAdj = new PricingAdjustment()
                    {
                        Description = adjustment.GetAttribute("Description"),
                        Rate = adjustment.GetAttribute("Rate"),
                        Price = adjustment.GetAttribute("Price"),
                        Fee = adjustment.GetAttribute("Fee"),
                        Margin = adjustment.GetAttribute("Margin"),
                        QualifyingRate = adjustment.GetAttribute("Qrate"),
                        TeaserRate = adjustment.GetAttribute("TRate"),
                        IsHidden = adjustment.GetAttribute("IsHidden") == "Yes",
                    };

                    o.pricingAdjustments.Add(newAdj);
                }
            }

            return o;
        }

        /// <summary>
        /// Parses the rate lock history XML. If the xml is null or empty it returns an empty list.
        /// The list is sorted.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static IList<InvestorRateLockHistoryItem> GetSortedRateHistory(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new List<InvestorRateLockHistoryItem>();
            }
            XmlDocument doc = Tools.CreateXmlDoc(xml);

            XmlNodeList nodeList = doc.SelectNodes("//RateLockHistory/Event");

            IList<InvestorRateLockHistoryItem> list = InvestorRateLockHistoryItem.SortRateHistory(nodeList);

            return list;
        }

        /// <summary>
        /// Looks in the xml for the given rate lock event. If the event is not found a not found exception is thrown. 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static InvestorRateLockHistoryItem GetRateLockEventFromXml(string xml, Guid eventId)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new LendersOffice.Common.NotFoundException(ErrorMessages.Generic, "Rate Lock Event Requested, but xml is empty");
            }

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            XmlElement element = (XmlElement)doc.SelectSingleNode("//RateLockHistory/Event[@EventId='" + eventId.ToString() + "']");

            if (element == null)
            {
                throw new LendersOffice.Common.NotFoundException(ErrorMessages.Generic, "Rate lock event not found in xml.");
            }

            InvestorRateLockHistoryItem item = GetRateLockHistoryItemFrom(element);

            return item;
        }
    }
}