using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;

namespace DataAccess
{
	public class CVolData : CPageData
	{
        private static CSelectStatementProvider s_selectProvider;
        static CVolData() 
        {
            StringList list = new StringList();

            list.Add("aLiaCollection");
            list.Add("aBFirstNm");
            list.Add("aBLastNm");
            list.Add("sfGetAgentOfRole");
            list.Add("sLenderNumVerif");
            list.Add("sOpenedD");
            list.Add("sPreparerXmlContent");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CVolData(Guid fileId) : base(fileId, "VOL", s_selectProvider) 
        {
        }
	}
}