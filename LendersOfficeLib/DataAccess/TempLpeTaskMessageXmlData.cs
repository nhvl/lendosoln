using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CTempLpeTaskMessageXmlData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CTempLpeTaskMessageXmlData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sTempLpeTaskMessageXml");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CTempLpeTaskMessageXmlData(Guid fileId) : base(fileId, "CTempLpeTaskMessageXmlData", s_selectProvider)
		{
		}
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

	}
}
