﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace DataAccess
{
    public enum E_PricingAdjustmentT 
    {
        Undefined = 0,
        PricingEngineLoanLevelAdjustment = 1,
        LockExtension = 2,
        RelockFee = 3,
        RolldownFee = 4
    }
    [XmlType]
    public sealed class PricingAdjustment 
    {
        private const string DEFAULT_VALUE = "0.000%";
        private string m_sPrice = DEFAULT_VALUE;
        private string m_sFee = DEFAULT_VALUE;
        private string m_sDescription = "";
        private string m_sRate = DEFAULT_VALUE;
        private string m_sTeaserRate = DEFAULT_VALUE;
        private string m_sQualRate = DEFAULT_VALUE;
        private string m_sMargin = DEFAULT_VALUE;

        private string CheckDecimal(string svalue)
        {
            if (svalue == "null" || svalue == "")
            {
                return DEFAULT_VALUE;
            }

            return svalue;
        }
         

        [XmlElement]
        public string Description
        {
            get
            {
                return m_sDescription;
            }
            set
            {
                m_sDescription = value;
            }
        }

        [XmlElement]        
        public string Rate {
            get
            {
                return m_sRate;
            }
            set
            {
                m_sRate = CheckDecimal(value);
            }
        }

        [XmlElement]
        public string Fee
        {
            get
            {
                return m_sFee;
            }
            set
            {
                m_sFee = CheckDecimal(value);
                UpdatePrice();
            }
        }

        /// <summary>
        /// Set is no op, Please set fee. Set fee before reading as well or string empty will be returned.
        /// </summary>
        public string Price
        {
            get
            {
                return m_sPrice ?? ""; 
            }
            //no op 
            set
            { }
        }

        [XmlElement]
        public string Margin
        {
            get
            {
                return m_sMargin;
            }
            set
            {
                m_sMargin = CheckDecimal(value);
            }
        }

        [XmlElement]        
        public string QualifyingRate {
            get
            {
                return m_sQualRate;
            }
            set
            {
                m_sQualRate = CheckDecimal(value);
            }
        }

        [XmlElement]        
        public string TeaserRate {
            get
            {
                return m_sTeaserRate;
            }
            set
            {
                m_sTeaserRate = CheckDecimal(value);
            }
        }


        [XmlElement]
        public bool IsHidden { get; set; }
        [XmlElement]
        public bool IsLenderAdjustment { get; set; }
        [XmlElement]
        public bool IsSRPAdjustment { get; set; }

        [XmlElement]
        public bool IsPersist { get; set; }

        // 1/13/2014 dd - I am adding more metadata (adjustment type, extension days) to adjustment object.
        // I use version to distinguish old adjustment without the metadata.
        // Version
        //     Null or Empty - Old adjustment (pre 1/13/2014)
        //     2 - Adjustment with PricingAdjustmentT & ExtensionDays

        [XmlElement]
        public string Version { get; set; }

        // 1/13/2014 dd - Add so I can use in custom lock policy. Old / Legacy adjustment may not have this
        // set.
        [XmlElement]
        public E_PricingAdjustmentT PricingAdjustmentT { get; set; }

        [XmlElement]
        public int ExtensionDays { get; set; } // 1/13/2014 dd - Store number of days of extension

        private void UpdatePrice()
        {
            if (string.IsNullOrEmpty(Fee))
            {
                m_sPrice = "";
                return;
            }

            m_sPrice = string.Empty;

            string pFee = Fee.Replace("%", "");
            decimal fee;
            if (decimal.TryParse(pFee, out fee))
            {
                fee = fee * -1;
                m_sPrice = fee.ToString("0.000\\%");
            }
        }
    }

    public sealed class VisiblePricingAdjustmentData
    {
        public string TotalPoint
        {
            get;
            private set;
        }

        public string TotalFee
        {
            get;
            private set;
        }

        public string TotalRate
        {
            get;
            private set; 
        }

        public ICollection<PricingAdjustment> VisibleAdjustments
        {
            get;
            private set;
        }

        public VisiblePricingAdjustmentData(ICollection<PricingAdjustment> adjustments)
        {

            ProcessAdjustments(adjustments);
        }

        private void ProcessAdjustments(ICollection<PricingAdjustment> adjustments)
        {
            LosConvert ls = new LosConvert();
            List<PricingAdjustment> mvisibleAdjustments = new List<PricingAdjustment>();
            decimal mtotalRate = 0;
            decimal mtotalPoint = 0;

            foreach (var adjustment in adjustments)
            {
                if (adjustment.IsHidden)
                {
                    continue;
                }

                mtotalRate += ls.ToRate(adjustment.Rate);
                mtotalPoint += ls.ToRate(adjustment.Price);
                mvisibleAdjustments.Add(adjustment);
            }

            TotalPoint = ls.ToRateString(mtotalPoint);
            TotalFee = ls.ToRateString(-mtotalPoint);
            TotalRate = ls.ToRateString(mtotalRate);
            VisibleAdjustments = mvisibleAdjustments;
        }
    }
}
