﻿// <copyright file="GetLpePriceGroupIdByEmployeeIdResult.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   1/21/2015
// </summary>

namespace DataAccess.SqlResultTypes
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;

    /// <summary>
    /// Storage class for the results of the query.
    /// </summary>
    public class GetLpePriceGroupIdByEmployeeIdResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetLpePriceGroupIdByEmployeeIdResult" /> class.
        /// </summary>
        /// <param name="reader">The reader should have be in position with a read and should be from the appropriately named stored procedure.</param>
        public GetLpePriceGroupIdByEmployeeIdResult(DbDataReader reader)
        {
            if (!(reader["LpePriceGroupId"] is DBNull))
            {
                this.LpePriceGroupId = (Guid)reader["LpePriceGroupId"];
            }

            if (!(reader["MiniCorrespondentPriceGroupID"] is DBNull))
            {
                this.MiniCorrespondentPriceGroupID = (Guid)reader["MiniCorrespondentPriceGroupID"];
            }

            if (!(reader["CorrespondentPriceGroupID"] is DBNull))
            {
                this.CorrespondentPriceGroupID = (Guid)reader["CorrespondentPriceGroupID"];
            }

            if (!(reader["BranchLpePriceGroupIdDefault"] is DBNull))
            {
                this.BranchLpePriceGroupIdDefault = (Guid)reader["BranchLpePriceGroupIdDefault"];
            }

            if (!(reader["BrokerLpePriceGroupIdDefault"] is DBNull))
            {
                this.BrokerLpePriceGroupIdDefault = (Guid)reader["BrokerLpePriceGroupIdDefault"];
            }
        }

        /// <summary>
        /// Gets the normal price group id.
        /// </summary>
        /// <value>Null or a Guid.</value>
        public Guid? LpePriceGroupId { get; private set; }

        /// <summary>
        /// Gets the price group id for the mini correspondent channel.
        /// </summary>
        /// <value>Null or a Guid.</value>
        public Guid? MiniCorrespondentPriceGroupID { get; private set; }

        /// <summary>
        /// Gets the price group id for the correspondent channel.
        /// </summary>
        /// <value>Null or a Guid.</value>
        public Guid? CorrespondentPriceGroupID { get; private set; }

        /// <summary>
        /// Gets the price group id for the branch.
        /// </summary>
        /// <value>Null or a Guid.</value>
        public Guid? BranchLpePriceGroupIdDefault { get; private set; }

        /// <summary>
        /// Gets the price group id for the broker.
        /// </summary>
        /// <value>Null or a Guid.</value>
        public Guid? BrokerLpePriceGroupIdDefault { get; private set; }
    }
}
