using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.UI;
using LendersOffice.Security;
using LqbGrammar.DataTypes;

namespace DataAccess
{

	public class CLiaJobExpense : CLiaSpecial, ILiabilityJobExpense
    {
		internal static ILiabilityJobExpense CreateJobExpense1( CAppBase parent, DataSet ds, CLiaCollection liaColl )
		{
			return new CLiaJobExpense( parent, ds, liaColl, E_DebtJobExpenseT.JobExpense1 );
		}
		internal static ILiabilityJobExpense CreateJobExpense2( CAppBase parent, DataSet ds, CLiaCollection liaColl )
		{
			return new CLiaJobExpense( parent, ds, liaColl, E_DebtJobExpenseT.JobExpense2 );
		}

		internal static ILiabilityJobExpense Reconstruct( CAppBase parent, DataSet ds, CLiaCollection liaColl, int iRow )
		{
			return new CLiaJobExpense( parent, ds, liaColl, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        internal CLiaJobExpense(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

		/// <summary>
		/// creating new record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="jobExpenseT"></param>
		private CLiaJobExpense( CAppBase parent, DataSet ds, CLiaCollection recordList,  E_DebtJobExpenseT jobExpenseT )
			: base( parent, ds, recordList )
		{
			SetDebtT( E_DebtT.JobRelatedExpense );
			DebtJobExpenseT = jobExpenseT;
		}

		/// <summary>
		/// Reconstructing a record out of a row
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="jobExpenseT"></param>
		private CLiaJobExpense( CAppBase parent, DataSet ds, CLiaCollection liaColl, int iRow )
			: base( parent, ds, liaColl, iRow )
		{
			SetDebtT( E_DebtT.JobRelatedExpense );

			DataRowCollection rows = ds.Tables[0].Rows;
			string thisRawVal = rows[iRow]["DebtT"].ToString();
			if( thisRawVal != "" )
				return; // nothing special needs to be done, AssetCashDepositT should work fine.

			// Figure out what to Deposit type to give to record that didn't have deposit type stored.
			// This is the situation where it's read for the first time since we restructure the xml list code.

			int nRow = rows.Count;
			bool bJobExpense1 = false;
			bool bJobExpense2 = false;
			for( int i = 0; i < nRow; ++i )
			{
				if( i == iRow )
					continue;

				DataRow row = rows[i];
				E_DebtT debtT = (E_DebtT) int.Parse( row["DebtT"].ToString() );
				if( E_DebtT.JobRelatedExpense == debtT )
				{
					string rawJobExpenseT = row["AssetCashDepositT"].ToString();
					if( "" != rawJobExpenseT  )
					{
						E_DebtJobExpenseT expenseT = (E_DebtJobExpenseT) int.Parse( rawJobExpenseT );
						switch( expenseT )
						{
							case E_DebtJobExpenseT.JobExpense1:
								bJobExpense1 = true; break;
							case E_DebtJobExpenseT.JobExpense2: 
								bJobExpense2 = true; break;
						}
					}
				}
			}		
			if( !bJobExpense1 )
				DebtJobExpenseT = E_DebtJobExpenseT.JobExpense1;
			else if( !bJobExpense2 )
				DebtJobExpenseT = E_DebtJobExpenseT.JobExpense2;
			else
				IsOnDeathRow = true;
		}
		/// <summary>
		/// Valid only if DebtT == JobRelatedExpense
		/// </summary>
		public E_DebtJobExpenseT DebtJobExpenseT
		{
			get { return (E_DebtJobExpenseT) GetEnum( "DebtJobExpenseT", E_DebtJobExpenseT.JobExpense1 ); }
			set { SetEnum( "DebtJobExpenseT", value ); }
		}

		public string ExpenseDesc
		{
			get { return GetDescString( "ComNm" ); }
			set { SetDescString( "ComNm", value ); }
		}

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            var objInputFieldModel = base.ToInputFieldModel();

            objInputFieldModel.Add("ExpenseDesc", new InputFieldModel(this.ExpenseDesc));
            objInputFieldModel.Add("DebtJobExpenseT", new InputFieldModel(this.DebtJobExpenseT));

            return objInputFieldModel;
        }

        /// <summary>
        /// Update liability from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update liability.</param> 
        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel); 
            
            if (objInputFieldModel == null)
            {
                return;
            }

            objInputFieldModel.BindObjectFromModel("ExpenseDesc", this, x => x.ExpenseDesc);
            objInputFieldModel.BindObjectFromModel("DebtJobExpenseT", this, x => x.DebtJobExpenseT);
        }
	}

	public class CLiaAlimony : CLiaSpecial, ILiabilityAlimony
    {

		public static ILiabilityAlimony Create( CAppBase parent, DataSet ds, CLiaCollection recordList )
		{
			return new CLiaAlimony( parent, ds, recordList );
		}

		public static ILiabilityAlimony Reconstruct( CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id )
		{
			return new CLiaAlimony( parent, ds, recordList, id );
		}

		public static ILiabilityAlimony Reconstruct( CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow )
		{
			return new CLiaAlimony( parent, ds, recordList, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        internal CLiaAlimony(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

		/// <summary>
		/// Create new
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		private CLiaAlimony( CAppBase parent, DataSet ds, CLiaCollection recordList )
			: base( parent, ds, recordList )
		{
			SetDebtT( E_DebtT.Alimony );
		}

		private CLiaAlimony( CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id )
			: base( parent, ds, recordList, id )
		{
			SetDebtT( E_DebtT.Alimony );
		} 

		private CLiaAlimony( CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow )
			: base( parent, ds, recordList, iRow )
		{
			SetDebtT( E_DebtT.Alimony );
		}

		/// <summary>
		/// Name of person that the borrower owes alimony to
		/// </summary>
		public string OwedTo
		{
			get { return GetDescString( "ComNm" ); }
			set { SetDescString( "ComNm", value ); }
		}

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            var objInputFieldModel = base.ToInputFieldModel();
            objInputFieldModel.Add("OwedTo", new InputFieldModel(this.OwedTo));
            return objInputFieldModel;
        }

        /// <summary>
        /// Update liability from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update liability.</param> 
        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel); 
            
            if (objInputFieldModel == null)
            {
                return;
            }

            objInputFieldModel.BindObjectFromModel("OwedTo", this, x => x.OwedTo);
        }
	}

    /// <summary>
    /// Represents a child support liability.
    /// </summary>
    public class CLiaChildSupport : CLiaSpecial, ILiabilityChildSupport
    {
        /// <summary>
        /// Creates a child support liability.
        /// </summary>
        /// <param name="parentApp">
        /// The parent app.
        /// </param>
        /// <param name="parentCollectionSet">
        /// The dataset for the parent liabilities collection.
        /// </param>
        /// <param name="parentCollection">
        /// The reference to the parent liabilities collection.
        /// </param>
        /// <returns>
        /// The child support liability.
        /// </returns>
        public static ILiabilityChildSupport Create(CAppBase parentApp, DataSet parentCollectionSet, CLiaCollection parentCollection)
        {
            return new CLiaChildSupport(parentApp, parentCollectionSet, parentCollection);
        }

        /// <summary>
        /// Recreates a child support liability.
        /// </summary>
        /// <param name="parentApp">
        /// The parent app.
        /// </param>
        /// <param name="parentCollectionSet">
        /// The dataset for the parent liabilities collection.
        /// </param>
        /// <param name="parentCollection">
        /// The reference to the parent liabilities collection.
        /// </param>
        /// <param name="row">
        /// The row of the liability in the parent set.
        /// </param>
        /// <returns>
        /// The child support liability.
        /// </returns>
        public static ILiabilityChildSupport Reconstruct(CAppBase parentApp, DataSet parentCollectionSet, CLiaCollection parentCollection, int row)
        {
            return new CLiaChildSupport(parentApp, parentCollectionSet, parentCollection, row);
        }

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        internal CLiaChildSupport(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

        /// <summary>
        /// Creates a new child support liability.
        /// </summary>
        /// <param name="parentApp">
        /// The parent app.
        /// </param>
        /// <param name="parentCollectionSet">
        /// The dataset for the parent liabilities collection.
        /// </param>
        /// <param name="parentCollection">
        /// The reference to the parent liabilities collection.
        /// </param>
        private CLiaChildSupport(CAppBase parentApp, DataSet parentCollectionSet, CLiaCollection parentCollection)
            : base(parentApp, parentCollectionSet, parentCollection)
        {
            this.SetDebtT(E_DebtT.ChildSupport);
        }

        /// <summary>
        /// Creates a new child support liability.
        /// </summary>
        /// <param name="parentApp">
        /// The parent app.
        /// </param>
        /// <param name="parentCollectionSet">
        /// The dataset for the parent liabilities collection.
        /// </param>
        /// <param name="parentCollection">
        /// The reference to the parent liabilities collection.
        /// </param>
        /// <param name="row">
        /// The row of the liability in the parent set.
        /// </param>
        private CLiaChildSupport(CAppBase parentApp, DataSet parentCollectionSet, CLiaCollection parentCollection, int row)
            : base(parentApp, parentCollectionSet, parentCollection, row)
        {
            this.SetDebtT(E_DebtT.ChildSupport);
        }

        /// <summary>
        /// Gets or sets the name of person that the borrower owes child support to.
        /// </summary>
        public string OwedTo
        {
            get { return GetDescString("ComNm"); }
            set { SetDescString("ComNm", value); }
        }

        /// <summary>
        /// Converts the child support liability to a <see cref="SimpleObjectInputFieldModel"/>.
        /// </summary>
        /// <returns>
        /// The generated model.
        /// </returns>
        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            var objInputFieldModel = base.ToInputFieldModel();
            objInputFieldModel.Add("OwedTo", new InputFieldModel(this.OwedTo));
            return objInputFieldModel;
        }

        /// <summary>
        /// Update liability from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">
        /// Input field model to update liability.
        /// </param> 
        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel);

            if (objInputFieldModel == null)
            {
                return;
            }

            objInputFieldModel.BindObjectFromModel("OwedTo", this, x => x.OwedTo);
        }
    }

    public abstract class CLiaSpecial : CLiaFields, ILiabilitySpecial
    {
        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        protected CLiaSpecial(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

		protected CLiaSpecial( CAppBase parent, DataSet ds, CLiaCollection recordList )
			: base( parent, ds, recordList )
		{
		}

		protected CLiaSpecial( CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id )
			: base( parent, ds, recordList, id )
		{
		} 

		protected CLiaSpecial( CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow )
			: base( parent, ds, recordList, iRow )
		{
		}

		new public E_DebtSpecialT DebtT
		{
			get{ return (E_DebtSpecialT) base.DebtT; }
            set { SetDebtT((E_DebtT)value); }
        }

       public override SimpleObjectInputFieldModel ToInputFieldModel()
       {
           var objInputFieldModel = base.ToInputFieldModel();
           objInputFieldModel["DebtT"] = new InputFieldModel(this.DebtT);
           return objInputFieldModel;
       }

       /// <summary>
       /// Update liability from an input field model.
       /// </summary>
       /// <param name="objInputFieldModel">Input field model to update liability.</param> 
       public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
       {
           base.BindFromObjectModel(objInputFieldModel);
           if (objInputFieldModel == null)
           {
               return;
           }
       }
	}

	public class CLiaRegular : CLiaFields, ILiabilityRegular
    {
        internal static ILiabilityRegular Create(CAppBase parent, DataSet ds, CLiaCollection recordList)
		{
			return new CLiaRegular( parent, ds, recordList );
		}

        internal static ILiabilityRegular Reconstruct(CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id)
		{
			return new CLiaRegular( parent, ds, recordList, id );
		}

        internal static ILiabilityRegular Reconstruct(CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow)
		{
			return new CLiaRegular( parent, ds, recordList, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        internal CLiaRegular(DataSet data, LosConvert converter, Guid recordId, CAppBase app = null)
            : base(data, converter, recordId)
        {
            this.m_parentAppData = app;
        }

        /// <summary>
        /// Use in LoansPQ integration to convert the liability id from credit report to a unique GUID.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Guid ConvertStringToGuid(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return Guid.Empty;
            }
            byte[] buffer = new byte[16];
            byte[] byteString = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
            System.Array.Copy(byteString, buffer, Math.Min(buffer.Length, byteString.Length));

            return new Guid(buffer);
        }

        /// <summary>
        /// Gets a calculated count of the remaining months to pay off a liability, automatically calculating a value for revolving "(R)" and liabilities excluded for underwriting.
        /// This value is designed to be appropriate for the Loan Product Advisor export, but may be appropriate for other uses.
        /// </summary>
        /// <returns>The liability remaining months </returns>
        public string GetRemainingMonthsForLpa()
        {
            if (this.RemainMons_raw.Equals("(R)", StringComparison.OrdinalIgnoreCase))
            {
                // Should return empty as of LPA v4.3.
                return string.Empty;
            }
            else if (string.IsNullOrEmpty(this.RemainMons_raw) && this.ExcFromUnderwriting)
            {
                if (this.Pmt == 0)
                {
                    return "0";
                }
                else
                {
                    return this.m_convertLos.ToCountString((int)Math.Ceiling(this.Bal / this.Pmt));
                }
            }
            else if (string.IsNullOrEmpty(this.RemainMons_raw))
            {
                // If blank, don't presume 0.
                return string.Empty;
            }
            else
            {
                return this.m_convertLos.ToCount(this.RemainMons_raw).ToString();
            }
        }

        /// <summary>
        /// Should only use in Loans PQ integration to override record id.
        /// </summary>
        /// <param name="s"></param>
        public void SetRecordId(string s)
        {
            this.DataRow["RecordId"] = ConvertStringToGuid(s).ToString();
        }



        private CAppBase m_parentAppData = null;
		/// <summary>
		/// Create new
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
        private CLiaRegular(CAppBase parent, DataSet ds, CLiaCollection recordList)
			: base( parent, ds, recordList )
		{
			DebtT = E_DebtRegularT.Revolving;
            AccNm = parent.aBNm; // Default Account name with borrower name.
            m_parentAppData = parent;
		}

        private CLiaRegular(CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id)
			: base( parent, ds, recordList, id )
		{
            m_parentAppData = parent;
		}

        private CLiaRegular(CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow)
			: base( parent, ds, recordList, iRow )
		{
            m_parentAppData = parent;
		}

		public bool IsSameTradeline( string accNumber, string creditorName )
		{
			string accNum1		= AccNum.Value.TrimWhitespaceAndBOM().ToUpper();
			string accNum2		= accNumber.TrimWhitespaceAndBOM().ToUpper();

			// Definition: 2 tradeline is the same when at least one of these is true
			// 1) Both account numbers are the same and not empty string
			// 2) If account numbers are both empty string and creditor names are the same.
			if( accNum1 == accNum2 )
			{
				if( "" == accNum1 )
				{
					string creditorNm1	= ComNm.TrimWhitespaceAndBOM().ToUpper();
					string creditorNm2	= creditorName.TrimWhitespaceAndBOM().ToUpper();
					return creditorNm1 == creditorNm2;
				}

				return true;
			}
			return false;
		}

        public E_LiabilityReconcileStatusT ReconcileStatusT
        {
            get { return (E_LiabilityReconcileStatusT) GetEnum("ReconcileStatusT", E_LiabilityReconcileStatusT.LoanFileOnly); }
            set { SetEnum("ReconcileStatusT", value); }
        }
		/// <summary>
		/// FHA Loan only
		/// </summary>
		public decimal OrigDebtAmt
		{
			get { return GetMoney( "OrigDebtAmt" ); }
			set { SetMoney( "OrigDebtAmt", value ); }
		}

		/// <summary>
		/// FHA Loan only
		/// </summary>
		public string OrigDebtAmt_rep
		{
			get { return m_convertLos.ToMoneyString(OrigDebtAmt, FormatDirection.ToRep); }
			set { OrigDebtAmt = m_convertLos.ToMoney(value); }
		}

		/// <summary>
		/// FHA Loan only
		/// </summary>
		public string AutoYearMake
		{
			get { return GetDescString( "AutoYearMake" ); }
			set { SetDescString( "AutoYearMake", value ); }
		}

		/// <summary>
		/// FHA Loan only
		/// </summary>
		public bool IsMortFHAInsured
		{
			get { return GetBool( "IsMortFHAInsured" ); }
			set { SetBool( "IsMortFHAInsured", value ); }
		}

		/// <summary>
		/// FHA loan only
		/// </summary>
		public bool IsForAuto
		{
			get { return GetBool( "IsForAuto" ); }
			set { SetBool( "IsForAuto", value ); }
		}

		new public E_DebtRegularT DebtT
		{
			get{ return (E_DebtRegularT) base.DebtT; }
			set{ SetDebtT( (E_DebtT) value ); }
		}

        public string DebtT_rep
        {
            get { return DisplayStringOfDebtT(DebtT); }
        }

		static public E_DebtT DebtTFallback
		{
			get { return E_DebtT.Other; }
		}

		static public string DisplayStringOfDebtT( E_DebtRegularT debtT )
		{
			switch (debtT) 
			{
				case E_DebtRegularT.Installment:			return "Installment";
				case E_DebtRegularT.Mortgage:				return "Mortgage";
				case E_DebtRegularT.Open:					return "Open";
				case E_DebtRegularT.Revolving:				return "Revolving";
				case E_DebtRegularT.Other:					return "Other";
			}
			return "???";
		}
		public override string SortValueFor( string fieldName )
		{
			switch( fieldName )
			{
				case "DebtT":
					return DisplayStringOfDebtT( DebtT );
				case "OwnerT":
					return DisplayStringOfOwnerT( OwnerT );
				case "WillBePdOff":
					return DisplayWillBePdOff( WillBePdOff );
				case "NotUsedInRatio":
					return DisplayUsedInRatio( !NotUsedInRatio );
				default: return base.SortValueFor( fieldName );
			}
		}
		static public string DisplayWillBePdOff( bool willBePaidOff )
		{
			if( willBePaidOff )
				return "Yes";
			return "No";
		}

		static public string DisplayUsedInRatio( bool usedInRatio )
		{
			if( usedInRatio )
				return "Yes";
			return "No";
		}
		override public void PrepareToFlush()
		{
			WillBePdOff = WillBePdOff; // To save the calculated value.
			NotUsedInRatio = NotUsedInRatio; // To save the calculated value.
			base.PrepareToFlush();
		}
		public Guid MatchedReRecordId
		{
			get 
            {
                if (DebtT != E_DebtRegularT.Mortgage)
                {
                    return Guid.Empty;
                }

                Guid reoId = GetGuid("MatchedReRecordId");
                if (reoId != Guid.Empty)
                {
                    try
                    {
                        var reo = m_parentAppData.aReCollection.GetRegRecordOf(reoId);
                        if (null == reo)
                        {
                            reoId = Guid.Empty;
                        }
                    }
                    catch
                    {
                        reoId = Guid.Empty; // Could not find Reo record.
                    }
                }
                return reoId;
                //return GetGuid("MatchedReRecordId"); 
            }
			set{ SetGuid( "MatchedReRecordId", value ); }
        }

        public string ComAddr
		{
			get { return GetDescString( "ComAddr" ); }
			set { SetDescString( "ComAddr", value ); }
		}

		public string ComCity
		{
			get { return GetDescString( "ComCity" ); }
			set { SetDescString( "ComCity", value ); }
		}
		public string ComState
		{
			get { return GetState( "ComState" ); }
			set { SetState( "ComState", value ); }
		}

		public string ComZip
		{
			get { return GetZipCode( "ComZip" ); }
			set { SetZipCode( "ComZip", value ); }
		}

		public string ComPhone
		{
			get { return GetPhoneNum( "ComPhone" ); }
			set { SetPhoneNum( "ComPhone", value ); }
		}

		public string ComFax
		{
			get { return GetPhoneNum( "ComFax" ); }
			set { SetPhoneNum( "ComFax", value ); }
		}

		public Sensitive<string> AccNum
		{
            get { return GetDescString("AccNum"); }
            set { SetDescString("AccNum", value); }
        }
        /// <summary>
        /// Name of the debtor
        /// </summary>
        public string AccNm
		{
			get { return GetDescString( "AccNm" ); }
			set { SetDescString( "AccNm", value ); }
		}

		public decimal Bal
		{
			get{ return GetMoney( "Bal" ); }
			set { SetMoney( "Bal", value ); }
		}
	
		public string Bal_rep
		{
			get { return m_convertLos.ToMoneyString(Bal, FormatDirection.ToRep ); }
			set { Bal = m_convertLos.ToMoney(value); }
		}

        public decimal R 
        {
            get { return GetRate("R"); }
            set { SetRate("R", value); }
        }
		public string R_rep
		{
			get { return m_convertLos.ToRateString(R); }
			set { R = m_convertLos.ToRate(value); }
		}
		
		public string OrigTerm_rep
		{
			get { return GetCountVarChar_rep( "OrigTerm" ); }
			set { SetCountVarChar_rep( "OrigTerm", value ); }
		}
		public string Due_rep
		{
			get { return GetCountVarChar_rep( "Due" ); }
			set { SetCountVarChar_rep( "Due", value ); }
		}	

		public bool IsPiggyBack
		{
			get{ return GetBool( "IsPiggyBack" ); }
			set { SetBool( "IsPiggyBack", value ); }
		}
		public string Late30_rep
		{
			get { return GetCountVarChar_rep( "Late30" ); }
			set { SetCountVarChar_rep( "Late30", value ); }
		}
		public string Late60_rep
		{
			get { return GetCountVarChar_rep( "Late60" ); }
			set { SetCountVarChar_rep( "Late60", value ); }
		}
		public string Late90Plus_rep
		{
			get { return GetCountVarChar_rep( "Late90Plus" ); }
			set { SetCountVarChar_rep( "Late90Plus", value ); }
		}
		public bool IncInReposession
		{
			get{ return GetBool( "IncInReposession" );	}
			set { SetBool( "IncInReposession", value ); }
		}

		public bool IncInBankruptcy
		{
			get	{ return GetBool( "IncInBankruptcy" ); }
			set { SetBool( "IncInBankruptcy", value ); }
		}

		public bool IncInForeclosure
		{
			get{ return GetBool( "IncInForeclosure" ); }
			set{ SetBool( "IncInForeclosure", value ); }
		}

		public bool ExcFromUnderwriting
		{
			get{ return GetBool( "ExcFromUnderwriting" ); }
			set{ SetBool( "ExcFromUnderwriting", value ); }
		}
		  
		public CDateTime VerifSentD
		{
			get { return GetDateTime( "VerifSentD" ); }
			set { SetDateTime( "VerifSentD", value ); }
		}

		public string VerifSentD_rep
		{
			get { return VerifSentD.ToString(m_convertLos); }
			set { VerifSentD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifRecvD
		{
			get { return GetDateTime( "VerifRecvD" ); }
			set { SetDateTime( "VerifRecvD", value ); }
		}
		public string VerifRecvD_rep
		{
			get { return VerifRecvD.ToString(m_convertLos); }
			set { VerifRecvD = CDateTime.Create(value, m_convertLos ); }
		}

		public CDateTime VerifExpD
		{
			get { return GetDateTime( "VerifExpD" ); }
			set { SetDateTime( "VerifExpD", value ); }
		}
		public string VerifExpD_rep
		{
			get { return VerifExpD.ToString(m_convertLos); }
			set { VerifExpD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifReorderedD
		{
			get { return GetDateTime( "VerifReorderedD" ); }
			set { SetDateTime( "VerifReorderedD", value ); }
		}
		public string VerifReorderedD_rep
		{
			get { return VerifReorderedD.ToString(m_convertLos); }
			set { VerifReorderedD = CDateTime.Create(value, m_convertLos); }
		}
		public string Attention
		{
			get { return GetDescString( "Attention" ); }
			set { SetDescString( "Attention", value ); }
		}

		public CDateTime PrepD
		{
			get { return GetDateTime( "PrepD" ); }
			set { SetDateTime( "PrepD", value ) ; }
		}
	
		public bool IsSeeAttachment
		{
			get { return GetBool( "IsSeeAttachment", true ); }
			set { SetBool( "IsSeeAttachment", value ); }
		}
        /// <summary>
        /// Gets the options of Desc property.
        /// </summary>
        /// <value>The options of Desc property.</value>
        [System.Xml.Serialization.XmlIgnore]
        [IgnoreDataMember]
        public List<KeyValuePair<string, string>> DescOptions
        {
            get
            {
                return new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("1st Existing Lien",null),
                    new KeyValuePair<string, string>("2nd Existing Lien",null),
                    new KeyValuePair<string, string>("Home Equity Line of Credit",null),
                    new KeyValuePair<string, string>("Installment Loan",null),
                    new KeyValuePair<string, string>("Lease Payments",null),
                    new KeyValuePair<string, string>("Liens",null),
                    new KeyValuePair<string, string>("Mortgage",null),
                    new KeyValuePair<string, string>("Open, 30day Charge Acc",null),
                    new KeyValuePair<string, string>("Other Liability",null),
                    new KeyValuePair<string, string>("Other Existing Lien",null),
                    new KeyValuePair<string, string>("Revolving Charge",null),
                    new KeyValuePair<string, string>("Taxes",null)
                };
            }
        }

		public string Desc
		{
			get { return GetDescString( "Desc" ); }
			set { SetDescString( "Desc", value ); }
		}


		/// <summary>
		/// Name of institution
		/// </summary>
		public string ComNm
		{
			get { return GetDescString( "ComNm" ); }
			set { SetDescString( "ComNm", value ); }
		}

		public void FieldUpdateAudit(string userName, string loginName, string field, string value)
		{
			this.Audit(userName, loginName, "Field Update", field, value);
		}

		public void LiabilityCreationAudit(string userName, string loginName)
		{
			this.Audit(userName, loginName, "Liability Creation", "", "");
		}

        /// <summary>
        /// Only relevant to mortgage tradeline. If it is a mortgage tradeline then it will determine whether
        /// mortgage is belong to subject property base on REO.
        /// </summary>
        public bool IsSubjectPropertyMortgage
        {
            get
            {
                if (DebtT == E_DebtRegularT.Mortgage)
                {
                    Guid reoId = MatchedReRecordId;
                    if (reoId == Guid.Empty)
                    {
                        // 11/16/2009 dd - There is no REO associate with this mortgage tradeline. Default to true.
                        return true;
                    }
                    else
                    {
                        var aReFields = m_parentAppData.aReCollection.GetRegRecordOf(reoId);
                        if (null != aReFields)
                        {
                            return aReFields.IsSubjectProp;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false; // This is not a mortgage tradeline.
                }
            }
            set 
            {
                Guid reoId = MatchedReRecordId;

                if (reoId == Guid.Empty)
                {
                    CreateDefaultReoRecord(value);
                }
                else
                {
                    var aReFields = m_parentAppData.aReCollection.GetRegRecordOf(reoId);
                    if (null != aReFields)
                    {
                        if (aReFields.IsSubjectProp != value && value == false)
                        {
                            // 12/1/2009 dd - If user switch from Subject property to other property then create
                            // a new REO record instead of changing existing REO record from Subject Property to other.
                            CreateDefaultReoRecord(value);
                        }
                        else
                        {
                            aReFields.IsSubjectProp = value; // Change the IsSubjectProp of Reo base on user input.
                        }
                    }
                    else
                    {
                        CreateDefaultReoRecord(value);
                    }
                }
            }
        }

        public bool IsSubjectProperty1stMortgage
        {
            get
            {
                if (IsSubjectPropertyMortgage)
                {
                    return GetBool("IsSubjectProperty1stMortgage");
                }
                else
                {
                    return false;
                }
            }
            set 
            {
                if (value)
                {
                    // 11/17/2009 dd - If value is true then we loop through all liability and mark other field false.
                    for (int i = 0; i < m_parentAppData.aLiaCollection.CountRegular; i++)
                    {
                        ILiabilityRegular o = m_parentAppData.aLiaCollection.GetRegularRecordAt(i);
                        if (o is CLiaRegular)
                        {
                            var regular = (CLiaRegular)o;
                            regular.SetBool("IsSubjectProperty1stMortgage", false);
                        }
                    }
                }
                SetBool("IsSubjectProperty1stMortgage", value);
            }
        }
        private void CreateDefaultReoRecord(bool isSubjProperty)
        {
            var aReCollection = m_parentAppData.aReCollection;
            IRealEstateOwned reFields = null;
            if (aReCollection.CountRegular > 0 && isSubjProperty)
            {
                // 11/16/2009 dd - Find first reo record with subject property
                for (int i = 0; i < aReCollection.CountRegular; i++)
                {
                    if (aReCollection.GetRegularRecordAt(i).IsSubjectProp)
                    {
                        reFields = aReCollection.GetRegularRecordAt(i);
                        break;
                    }
                }

            }

            if (reFields == null) 
            {
                reFields = aReCollection.AddRegularRecord();
            }
            reFields.IsSubjectProp = isSubjProperty;
            reFields.Update();

            MatchedReRecordId = reFields.RecordId;

        }
        public decimal FullyIndexedPITIPayment
        {
            get
            {
                if (IsSubjectPropertyMortgage)
                {
                    return GetMoney("FullyIndexedPITIPayment");
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                SetMoney("FullyIndexedPITIPayment", value);
            }
        }
        public string FullyIndexedPITIPayment_rep
        {
            get { return m_convertLos.ToMoneyString(FullyIndexedPITIPayment, FormatDirection.ToRep); }
            set { FullyIndexedPITIPayment = m_convertLos.ToMoney(value); }
        }
        private void Audit(string userName, string loginName, string action, string field, string value) 
        {
            XmlDocument doc = new XmlDocument();
            string xml = this.PmlAuditTrailXmlContent;

            XmlElement rootElement = null;
            if (null == xml || "" == xml) 
            {
                rootElement = doc.CreateElement("History");
                doc.AppendChild(rootElement);
            } 
            else 
            {
                try 
                {
                    doc.LoadXml(xml);
                    rootElement = (XmlElement) doc.ChildNodes[0];
                } 
                catch (Exception exc)
                {
                    Tools.LogError("PmlAuditTrailXmlContent is corrupted. xml=" + xml, exc);
                    rootElement = doc.CreateElement("History");
                    doc.AppendChild(rootElement);

                }
            }

            XmlElement eventElement = doc.CreateElement("Event");
            rootElement.AppendChild(eventElement);

            eventElement.SetAttribute("UserName", userName);
			eventElement.SetAttribute("LoginId", loginName);
			eventElement.SetAttribute("Action", action);
			if (field.Length != 0)
				eventElement.SetAttribute("Field", field);
            if (value.Length != 0)
				eventElement.SetAttribute("Value", value);
			eventElement.SetAttribute("EventDate", Tools.GetDateTimeNowString());
			
            this.PmlAuditTrailXmlContent = doc.OuterXml;
        }
        public Guid VerifSigningEmployeeId
        {
            get
            {
                return GetGuid("VerifSigningEmployeeId");
            }
            private set
            {
                SetGuid("VerifSigningEmployeeId", value);
            }
        }

        public string VerifSignatureImgId
        {
            get
            {
                return GetString("VerifSignatureImgId");
            }
            private set
            {
                SetString("VerifSignatureImgId", value);
            }
        }

        public bool VerifHasSignature
        {
            get
            {
                return false == string.IsNullOrEmpty(VerifSignatureImgId);
            }
        }

        public void ClearSignature()
        {
            VerifSignatureImgId = "";
            VerifSigningEmployeeId = Guid.Empty;
        }

        public void ApplySignature(Guid employeeId, string imgId)
        {
            VerifSignatureImgId = imgId;
            VerifSigningEmployeeId = employeeId;
        }

        public bool PayoffAmtLckd
        {
            get { return this.GetBool("PayoffAmtLckd", bDefaultVal: false); }
            set { this.SetBool("PayoffAmtLckd", value); }
        }

        public decimal PayoffAmt
        {
            get
            {
                if (this.PayoffAmtLckd)
                {
                    return this.GetMoney("PayoffAmt");
                }

                return this.Bal;
            }

            set
            {
                this.SetMoney("PayoffAmt", value);
            }
        }

        public string PayoffAmt_rep
        {
            get 
            { 
                return this.m_convertLos.ToMoneyString(this.PayoffAmt, FormatDirection.ToRep); 
            }

            set
            {
                this.PayoffAmt = this.m_convertLos.ToMoney(value);
            }
        }

        public bool PayoffTimingLckd
        {
            get 
            {
                if (!this.WillBePdOff)
                {
                    return false;
                }

                return this.GetBool("PayoffTimingLckd", bDefaultVal: false); 
            }

            set 
            { 
                this.SetBool("PayoffTimingLckd", value); 
            }
        }

        public E_Timing PayoffTiming
        {
            get
            {
                if (!this.WillBePdOff)
                {
                    return E_Timing.Blank;
                }
                else if (this.PayoffTimingLckd)
                {
                    return (E_Timing)this.GetEnum("PayoffTiming", fallbackReturnVal: E_Timing.Blank);
                }

                if (this.m_parentAppData.LoanData.sIsRefinancing)
                {
                    return E_Timing.At_Closing;
                }
                else
                {
                    return E_Timing.Before_Closing;
                }
            }

            set
            {
                if (value == E_Timing.After_Closing)
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        "After Closing is not a valid value for the payoff " +
                        "timing of a liability. Accepted values: Blank, " +
                        "At_Closing, Before_Closing");
                }

                this.SetEnum("PayoffTiming", value);
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public bool UsedInRatio
        {
            get
            {
                return !this.NotUsedInRatio;
            }

            set
            {
                this.NotUsedInRatio = !value;
            }
        }

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            var objInputFieldModel = base.ToInputFieldModel();

            objInputFieldModel["DebtT"] = new InputFieldModel(this.DebtT);
            objInputFieldModel.Add("ReconcileStatusT", new InputFieldModel(this.ReconcileStatusT));
            objInputFieldModel.Add("ComAddr", new InputFieldModel(this.ComAddr));
            objInputFieldModel.Add("ComCity", new InputFieldModel(this.ComCity));
            objInputFieldModel.Add("ComState", new InputFieldModel(this.ComState, customInputType: InputFieldType.DropDownList, customOptions : EnumUtilities.GetValuesFromField("state", PrincipalFactory.CurrentPrincipal)));
            objInputFieldModel.Add("ComZip", new InputFieldModel(this.ComZip, customInputType : InputFieldType.Zipcode));
            objInputFieldModel.Add("ComPhone", new InputFieldModel(this.ComPhone, customInputType: InputFieldType.Phone));
            objInputFieldModel.Add("ComFax", new InputFieldModel(this.ComFax, customInputType: InputFieldType.Phone));
            objInputFieldModel.Add("AccNum", new InputFieldModel(this.AccNum));
            objInputFieldModel.Add("AccNm", new InputFieldModel(this.AccNm));
            objInputFieldModel.Add("Bal", new InputFieldModel(this.Bal));
            objInputFieldModel.Add("R", new InputFieldModel(this.R, customInputType: InputFieldType.Percent));
            objInputFieldModel.Add("OrigTerm_rep", new InputFieldModel(this.OrigTerm_rep, customInputType: InputFieldType.Number));
            objInputFieldModel.Add("Due_rep", new InputFieldModel(this.Due_rep));
            objInputFieldModel.Add("IsPiggyBack", new InputFieldModel(this.IsPiggyBack));
            objInputFieldModel.Add("Late30_rep", new InputFieldModel(this.Late30_rep, customInputType: InputFieldType.Number));
            objInputFieldModel.Add("Late60_rep", new InputFieldModel(this.Late60_rep, customInputType: InputFieldType.Number));
            objInputFieldModel.Add("Late90Plus_rep", new InputFieldModel(this.Late90Plus_rep, customInputType: InputFieldType.Number));
            objInputFieldModel.Add("IncInReposession", new InputFieldModel(this.IncInReposession));
            objInputFieldModel.Add("IncInBankruptcy", new InputFieldModel(this.IncInBankruptcy));
            objInputFieldModel.Add("IncInForeclosure", new InputFieldModel(this.IncInForeclosure));
            objInputFieldModel.Add("ExcFromUnderwriting", new InputFieldModel(this.ExcFromUnderwriting));
            objInputFieldModel.Add("VerifSentD", new InputFieldModel(this.VerifSentD));
            objInputFieldModel.Add("VerifRecvD", new InputFieldModel(this.VerifRecvD));
            objInputFieldModel.Add("VerifExpD", new InputFieldModel(this.VerifExpD));
            objInputFieldModel.Add("VerifReorderedD", new InputFieldModel(this.VerifReorderedD));
            objInputFieldModel.Add("Attention", new InputFieldModel(this.Attention));
            objInputFieldModel.Add("PrepD", new InputFieldModel(this.PrepD));
            objInputFieldModel.Add("IsSeeAttachment", new InputFieldModel(this.IsSeeAttachment));
            objInputFieldModel.Add("Desc", new InputFieldModel(this.Desc, customOptions: this.DescOptions));
            objInputFieldModel.Add("ComNm", new InputFieldModel(this.ComNm));
            // objInputFieldModel.Add("IsSubjectPropertyMortgage", new InputFieldModel(this.IsSubjectPropertyMortgage));
            // objInputFieldModel.Add("IsSubjectProperty1stMortgage", new InputFieldModel(this.IsSubjectProperty1stMortgage));
            objInputFieldModel.Add("FullyIndexedPITIPayment", new InputFieldModel(this.FullyIndexedPITIPayment));
            objInputFieldModel.Add("VerifSigningEmployeeId", new InputFieldModel(this.VerifSigningEmployeeId));
            objInputFieldModel.Add("VerifSignatureImgId", new InputFieldModel(this.VerifSignatureImgId));
            objInputFieldModel.Add("VerifHasSignature", new InputFieldModel(this.VerifHasSignature));
            objInputFieldModel.Add("PayoffAmtLckd", new InputFieldModel(this.PayoffAmtLckd, customInputType : InputFieldType.Lock));
            objInputFieldModel.Add("PayoffAmt", new InputFieldModel(this.PayoffAmt, readOnly : !this.PayoffAmtLckd ));
            objInputFieldModel.Add("PayoffTimingLckd", new InputFieldModel(this.PayoffTimingLckd, customInputType: InputFieldType.Lock, readOnly: !this.WillBePdOff));
            objInputFieldModel.Add("PayoffTiming", new InputFieldModel(this.PayoffTiming, readOnly: !this.PayoffTimingLckd, customOptions: new List<KeyValuePair<string, string>>() {
                // LN - 248058
                // After Closing is not a valid value for the payoff timing of a liability. Accepted values: Blank, At_Closing, Before_Closing
                EnumUtilities.KeyValuePairFromEnum(E_Timing.Blank),
                EnumUtilities.KeyValuePairFromEnum(E_Timing.Before_Closing),
                EnumUtilities.KeyValuePairFromEnum(E_Timing.At_Closing)
            }));

            var subcoll = this.m_parentAppData.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
            var options = subcoll.Cast<IRealEstateOwned>().Select(x => new KeyValuePair<string, string>( x.RecordId.ToString(), string.Format(@"{0}, {1}, {2} {3}", x.Addr, x.City, x.State, x.Zip))).ToList();
            options.Insert(0, new KeyValuePair<string, string>(Guid.Empty.ToString(), "<-- Select a matched REO -->"));
            objInputFieldModel.Add("MatchedReRecordId", new InputFieldModel(this.MatchedReRecordId, customInputType: InputFieldType.DropDownList, customOptions: options, readOnly: this.DebtT != E_DebtRegularT.Mortgage));

            objInputFieldModel.Remove("NotUsedInRatio");
            objInputFieldModel.Add("UsedInRatio", new InputFieldModel(this.UsedInRatio));

            if (this.m_parentAppData.LoanData.sLT == E_sLT.FHA)
            {
                objInputFieldModel.Add("OrigDebtAmt", new InputFieldModel(this.OrigDebtAmt_rep, customInputType : InputFieldType.Money));
                objInputFieldModel.Add("AutoYearMake", new InputFieldModel(this.AutoYearMake));
                objInputFieldModel.Add("IsForAuto", new InputFieldModel(this.IsForAuto));
                objInputFieldModel.Add("IsMortFHAInsured", new InputFieldModel(this.IsMortFHAInsured));
            }

            return objInputFieldModel;
        }

        /// <summary>
        /// Update liability from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update liability.</param> 
        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel);

            if (objInputFieldModel == null)
            {
                return;
            }

            objInputFieldModel.BindObjectFromModel("ReconcileStatusT", this, x => x.ReconcileStatusT);
            objInputFieldModel.BindObjectFromModel("OrigDebtAmt", this, x => x.OrigDebtAmt);
            objInputFieldModel.BindObjectFromModel("AutoYearMake", this, x => x.AutoYearMake);
            objInputFieldModel.BindObjectFromModel("IsMortFHAInsured", this, x => x.IsMortFHAInsured);
            objInputFieldModel.BindObjectFromModel("IsForAuto", this, x => x.IsForAuto);
            objInputFieldModel.BindObjectFromModel("MatchedReRecordId", this, x => x.MatchedReRecordId);
            objInputFieldModel.BindObjectFromModel("ComAddr", this, x => x.ComAddr);
            objInputFieldModel.BindObjectFromModel("ComCity", this, x => x.ComCity);
            objInputFieldModel.BindObjectFromModel("ComState", this, x => x.ComState);
            objInputFieldModel.BindObjectFromModel("ComZip", this, x => x.ComZip);
            objInputFieldModel.BindObjectFromModel("ComPhone", this, x => x.ComPhone);
            objInputFieldModel.BindObjectFromModel("ComFax", this, x => x.ComFax);
            objInputFieldModel.BindObjectFromModel("AccNum", this, x => x.AccNum);
            objInputFieldModel.BindObjectFromModel("AccNm", this, x => x.AccNm);
            objInputFieldModel.BindObjectFromModel("Bal", this, x => x.Bal);
            objInputFieldModel.BindObjectFromModel("R", this, x => x.R);
            objInputFieldModel.BindObjectFromModel("OrigTerm_rep", this, x => x.OrigTerm_rep);
            objInputFieldModel.BindObjectFromModel("Due_rep", this, x => x.Due_rep);
            objInputFieldModel.BindObjectFromModel("IsPiggyBack", this, x => x.IsPiggyBack);
            objInputFieldModel.BindObjectFromModel("Late30_rep", this, x => x.Late30_rep);
            objInputFieldModel.BindObjectFromModel("Late60_rep", this, x => x.Late60_rep);
            objInputFieldModel.BindObjectFromModel("Late90Plus_rep", this, x => x.Late90Plus_rep);
            objInputFieldModel.BindObjectFromModel("IncInReposession", this, x => x.IncInReposession);
            objInputFieldModel.BindObjectFromModel("IncInBankruptcy", this, x => x.IncInBankruptcy);
            objInputFieldModel.BindObjectFromModel("IncInForeclosure", this, x => x.IncInForeclosure);
            objInputFieldModel.BindObjectFromModel("ExcFromUnderwriting", this, x => x.ExcFromUnderwriting);
            objInputFieldModel.BindObjectFromModel("VerifSentD", this, x => x.VerifSentD);
            objInputFieldModel.BindObjectFromModel("VerifRecvD", this, x => x.VerifRecvD);
            objInputFieldModel.BindObjectFromModel("VerifExpD", this, x => x.VerifExpD);
            objInputFieldModel.BindObjectFromModel("VerifReorderedD", this, x => x.VerifReorderedD);
            objInputFieldModel.BindObjectFromModel("Attention", this, x => x.Attention);
            objInputFieldModel.BindObjectFromModel("PrepD", this, x => x.PrepD);
            objInputFieldModel.BindObjectFromModel("IsSeeAttachment", this, x => x.IsSeeAttachment);
            objInputFieldModel.BindObjectFromModel("Desc", this, x => x.Desc);
            objInputFieldModel.BindObjectFromModel("ComNm", this, x => x.ComNm);
            // objInputFieldModel.BindObjectFromModel("IsSubjectPropertyMortgage", this, x => x.IsSubjectPropertyMortgage);
            // objInputFieldModel.BindObjectFromModel("IsSubjectProperty1stMortgage", this, x => x.IsSubjectProperty1stMortgage);
            objInputFieldModel.BindObjectFromModel("FullyIndexedPITIPayment", this, x => x.FullyIndexedPITIPayment);
            objInputFieldModel.BindObjectFromModel("VerifSigningEmployeeId", this, x => x.VerifSigningEmployeeId);
            objInputFieldModel.BindObjectFromModel("VerifSignatureImgId", this, x => x.VerifSignatureImgId);
            objInputFieldModel.BindObjectFromModel("VerifHasSignature", this, x => x.VerifHasSignature);
            objInputFieldModel.BindObjectFromModel("PayoffAmtLckd", this, x => x.PayoffAmtLckd);
            objInputFieldModel.BindObjectFromModel("PayoffAmt", this, x => x.PayoffAmt);
            objInputFieldModel.BindObjectFromModel("PayoffTimingLckd", this, x => x.PayoffTimingLckd);
            objInputFieldModel.BindObjectFromModel("PayoffTiming", this, x => x.PayoffTiming);
            objInputFieldModel.BindObjectFromModel("UsedInRatio", this, x => x.UsedInRatio);

            objInputFieldModel.BindObjectFromModel("DebtT", this, x => x.DebtT);

            if (objInputFieldModel.ContainsKey("WillBePdOff") && objInputFieldModel["WillBePdOff"].IsReadOnly)
            {
                this.WillBePdOff = false;
            } 
            else if (!this.WillBePdOff && objInputFieldModel.ContainsKey("UsedInRatio") && objInputFieldModel["UsedInRatio"].IsReadOnly)
            {
                this.NotUsedInRatio = true;
            }
        }
	}

	abstract public class CLiaFields : CXmlRecordBase2, ILiability
    {
        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        protected CLiaFields(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

		/// <summary>
		/// Create new row/record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
        protected CLiaFields(CAppBase parent, DataSet ds, CLiaCollection recordList)
			: base( parent, ds, recordList, "LiaXmlContent" )
		{
		}

		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="iRow"></param>
        protected CLiaFields(CAppBase parent, DataSet ds, CLiaCollection recordList, int iRow)
			: base( parent, ds, recordList, iRow, "LiaXmlContent" )
		{
		}

		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="id"></param>
        protected CLiaFields(CAppBase parent, DataSet ds, CLiaCollection recordList, Guid id) 
			: base (parent, ds, recordList, id, "LiaXmlContent") 
		{
		}

		static public string DisplayStringOfOwnerT( E_LiaOwnerT ownerT )
		{
			switch( ownerT )
			{
				case E_LiaOwnerT.Borrower:		return "B";
				case E_LiaOwnerT.CoBorrower:	return "C";
				case E_LiaOwnerT.Joint:			return "J";
				default: return "???";
			}
		}

		static public bool IsTypeOfSpecialRecord( E_DebtT debtT )
		{
			switch (debtT)
			{
				case E_DebtT.Alimony:
				case E_DebtT.JobRelatedExpense:
                case E_DebtT.ChildSupport:
					return true;
				default:
					return false;
			}
		}

        public string PmlAuditTrailXmlContent
        {
            get{ return GetDescString( "PmlAuditTrailXmlContent" ); }
            set{ SetDescString( "PmlAuditTrailXmlContent", value ); }
        }

		public E_DebtT DebtT
		{
			get { return (E_DebtT) GetEnum("DebtT", E_DebtT.Other); }
		}

		protected void SetDebtT( E_DebtT debtT )
		{
            SetEnum("DebtT", debtT);
		}

		public E_LiaOwnerT OwnerT
		{
			get { return (E_LiaOwnerT) GetEnum("OwnerT", E_LiaOwnerT.Borrower); }
			set { SetEnum( "OwnerT", value ); }
		}

        public string OwnerT_rep
        {
            get { return DisplayStringOfOwnerT(OwnerT); }
        }

		/// <summary>
		/// This would be what subcollection of records will be based on.
		/// </summary>
		override public Enum KeyType
		{
			get { return DebtT; }
			set { SetDebtT( (E_DebtT) value ); }
		}
		
		public decimal Pmt
		{
			get{ return GetMoney( "Pmt" ); }
			set{ SetMoney( "Pmt", value ); }
		}

		/// <summary>
		/// Used to strip parentheses from Point's mortgage payments. Point wraps
		/// mortgage payments in parens to indicate "not included in ratio calculation"
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		internal static string StripParens(string s)
		{
			if (s.Length < 2)
				return s ;
			else if ('(' == s[0] && ')' == s[s.Length-1])
				return s.Substring(1, s.Length-2) ;
			else
				return s ;
		}

		public string Pmt_repPoint
		{
			get
			{
				try
				{
					if( NotUsedInRatio && ( Pmt != 0 ) )
						return "(" + Pmt_rep + ")";
					return Pmt_rep;
				}
				catch
				{
					return "";
				}
			}
			set
			{
				string newPmtRep = StripParens( value ) ;
				Pmt_rep = newPmtRep;
				NotUsedInRatio = newPmtRep.Length < value.Length; // non included in ratio if there were "(" and ")", 
			}
		}

		public string Pmt_rep
		{
			get { return m_convertLos.ToMoneyString( Pmt, FormatDirection.ToRep ); }
			set { Pmt = m_convertLos.ToMoney(value);  }
		}
		
		/// <summary>
		/// This return string value use to display in 1003 Liability section box "Monthly Payt & Mos. Left To Pay".
		/// The generic format will be payment / months left. If this liability will be paid off then
		/// add * in front.
		/// </summary>
		public string PmtRemainMons_rep1003 
		{
			get 
			{
				string str = Pmt_repPoint + " / " + RemainMons_rep;

				if (WillBePdOff) 
				{
					str = " * " + str;
				}

				return str;
			}
		}

		public string RemainMons_rep
		{
			get { return GetCountVarChar_rep( "RemainMons" ); }
			set { SetCountVarChar_rep( "RemainMons", value ); }
		}

        public string RemainMons_raw
        {
            get { return GetString("RemainMons"); }
        }
		
		override public void PrepareToFlush()
		{
		}

		public bool WillBePdOff
		{
			get{ return GetBool( "WillBePdOff" ); }
			set { SetBool( "WillBePdOff", value ); }
		}

		public bool NotUsedInRatio
		{
			get
			{
				if( E_DebtT.Mortgage == DebtT )
					return true; 
				return GetBool( "NotUsedInRatio" );
			}
			set{ SetBool( "NotUsedInRatio", value ); }
		}

        public int H4HNumOfJointOwners
        {
            get
            {
                int value = GetCount("H4HNumOfJointOwners");
                if (value <= 1)
                {
                    if (OwnerT == E_LiaOwnerT.Joint)
                    {
                        return 2; // 12/8/2009 dd - Default to 2 when it is a joint acocunt.
                    }
                    else
                    {
                        return 1;
                    }
                }
                return value;
            }
            set { SetCount("H4HNumOfJointOwners", value); }
        }

        public string H4HNumOfJointOwners_rep
        {
            get { return m_convertLos.ToCountString(H4HNumOfJointOwners); }
            set { H4HNumOfJointOwners = m_convertLos.ToCount(value); }
        }

        public virtual bool H4HIsRetirement
        {
            get { return GetBool("H4HIsRetirement", false); }
            set { SetBool("H4HIsRetirement", value); }
        }

        /// <summary>
        /// Return then json input model of an asset.
        /// </summary>
        /// <returns>An Input field type of the asset.</returns>
        public virtual SimpleObjectInputFieldModel ToInputFieldModel()
        {
            SimpleObjectInputFieldModel objInputFieldModel = new SimpleObjectInputFieldModel();

            objInputFieldModel.Add("id", new InputFieldModel(this.RecordId, readOnly: true));
            objInputFieldModel.Add("DebtT", new InputFieldModel(this.DebtT));
            objInputFieldModel.Add("OwnerT", new InputFieldModel(this.OwnerT));
            objInputFieldModel.Add("OwnerT_rep", new InputFieldModel(this.OwnerT_rep));
            objInputFieldModel.Add("KeyType", new InputFieldModel(this.KeyType));
            objInputFieldModel.Add("Pmt", new InputFieldModel(this.Pmt));
            objInputFieldModel.Add("RemainMons_rep", new InputFieldModel(this.RemainMons_rep));
            objInputFieldModel.Add("WillBePdOff", new InputFieldModel(this.WillBePdOff));
            objInputFieldModel.Add("NotUsedInRatio", new InputFieldModel(this.NotUsedInRatio));
            objInputFieldModel.Add("H4HNumOfJointOwners", new InputFieldModel(this.H4HNumOfJointOwners));
            objInputFieldModel.Add("H4HIsRetirement", new InputFieldModel(this.H4HIsRetirement));

            var auditString = this.PmlAuditTrailXmlContent;
            var auditJson = string.Empty;
            if (!string.IsNullOrEmpty(auditString))
            {
                var auditXDoc = new XmlDocument();
                auditXDoc.LoadXml(auditString);

                auditJson = SerializationHelper.JsonNetSerialize(auditXDoc.GetElementsByTagName("Event").Cast<XmlNode>().Select(x => new AuditItem(x)).OrderByDescending(x => x.EventDate).OrderByDescending(x => x.Field).ToList());
            }

            objInputFieldModel.Add("PmlAuditTrail", new InputFieldModel(auditJson));

            return objInputFieldModel;
        }

        /// <summary>
        /// Update liability from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update liability.</param> 
        public virtual void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            if (objInputFieldModel == null)
            {
                return;
            }

            objInputFieldModel.BindObjectFromModel("OwnerT", this, x => x.OwnerT);
            objInputFieldModel.BindObjectFromModel("Pmt", this, x => x.Pmt);
            objInputFieldModel.BindObjectFromModel("RemainMons_rep", this, x => x.RemainMons_rep);
            objInputFieldModel.BindObjectFromModel("WillBePdOff", this, x => x.WillBePdOff);
            objInputFieldModel.BindObjectFromModel("NotUsedInRatio", this, x => x.NotUsedInRatio);
            objInputFieldModel.BindObjectFromModel("H4HNumOfJointOwners", this, x => x.H4HNumOfJointOwners);
            objInputFieldModel.BindObjectFromModel("H4HIsRetirement", this, x => x.H4HIsRetirement);
        }

        /// <summary>
        /// The PML audit info.
        /// </summary>
        private class AuditItem
        {
            public string EventDate = "";
            public string UserName = "";
            public string Field = "";
            public string Action = "";
            public string Value = "";
            public string LoginId = "";

            /// <summary>
            /// Initializes a new instance of the AuditItem class from an xml node.
            /// </summary>
            /// <param name="xmlNode">The xml nod</param>
            public AuditItem(XmlNode xmlNode)
            {
                if (xmlNode.Attributes["EventDate"] != null)
                    this.EventDate = xmlNode.Attributes["EventDate"].Value;
                if (xmlNode.Attributes["UserName"] != null)
                    this.UserName = xmlNode.Attributes["UserName"].Value;
                if (xmlNode.Attributes["LoginId"] != null)
                    this.LoginId = xmlNode.Attributes["LoginId"].Value;
                if (xmlNode.Attributes["Action"] != null)
                    this.Action = xmlNode.Attributes["Action"].Value;
                if (xmlNode.Attributes["Field"] != null)
                    this.Field = xmlNode.Attributes["Field"].Value;
                if (xmlNode.Attributes["Value"] != null)
                    this.Value = xmlNode.Attributes["Value"].Value;
            }
        }
	}
}

