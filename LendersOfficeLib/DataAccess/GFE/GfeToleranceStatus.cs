﻿// <copyright file="GfeToleranceStatus.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   4/15/2015
// </summary>
namespace DataAccess.GFE
{
    /// <summary>
    /// A summary of which tolerance violations have been triggered.
    /// </summary>
    public class GfeToleranceStatus
    {
        /// <summary>
        /// Indicates whether a zero tolerance violation exists.
        /// </summary>
        public readonly bool NeedsGfeZeroAlert;

        /// <summary>
        /// Indicates whether a violation of ten percent or more exists.
        /// </summary>
        public readonly bool NeedsGfeTenAlert;

        /// <summary>
        /// Indicates whether re-disclosing then pending archive will clear the zero percent violation.
        /// </summary>
        public readonly bool RedisclosingPendingArchiveWillClearZeroPercentCure;

        /// <summary>
        /// Indicates whether re-disclosing then pending archive will clear the zero percent violation.
        /// </summary>
        public readonly bool RedisclosingPendingArchiveWillClearTenPercentCure;

        /// <summary>
        /// Initializes a new instance of the <see cref="GfeToleranceStatus" /> class.
        /// </summary>
        /// <param name="needsZeroAlert">True iff there was a zero percent violation.</param>
        /// <param name="needsTenAlert">True iff there was a ten percent violation.</param>
        /// <param name="redisclosingWillClearZero">True if disclosing the "Pending..." status Loan Estimate Archive will clear the zero percent cure violation.</param>
        /// <param name="redisclosingWillClearTen">True if disclosing the "Pending..." status Loan Estimate Archive will clear the ten percent cure violation.</param>
        public GfeToleranceStatus(bool needsZeroAlert, bool needsTenAlert, bool redisclosingWillClearZero, bool redisclosingWillClearTen)
        {
            this.NeedsGfeZeroAlert = needsZeroAlert;
            this.NeedsGfeTenAlert = needsTenAlert;
            this.RedisclosingPendingArchiveWillClearZeroPercentCure = redisclosingWillClearZero;
            this.RedisclosingPendingArchiveWillClearTenPercentCure = redisclosingWillClearTen;
        }
    }
}
