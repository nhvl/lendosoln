﻿// <copyright file="GfeToleranceViolationCalculator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   4/15/2015
// </summary>
namespace DataAccess.GFE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CommonProjectLib.Caching;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Gets tolerance violations on a loan.
    /// </summary>
    public class GfeToleranceViolationCalculator
    {
        /// <summary>
        /// Fee types that should be considered for zero tolerance violations.
        /// </summary>
        private static HashSet<E_ClosingCostFeeMismoFeeT> zeroToleranceFeeTypes = new HashSet<E_ClosingCostFeeMismoFeeT>(
            new E_ClosingCostFeeMismoFeeT[]
                {
                    E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee, // "County Deed Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee, // "County Mortgage Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee, // "State Deed Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee, // "State Mortgage Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee, // "City Deed Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee, // "City Mortgage Tax Stamp Fee"
                    E_ClosingCostFeeMismoFeeT.DocumentaryStampFee // "Documentary Stamp Fee"
                });

        /// <summary>
        /// A list of the MISMO recording fee types.
        /// </summary>
        private static HashSet<E_ClosingCostFeeMismoFeeT> recordingFeeTypes =
            new HashSet<E_ClosingCostFeeMismoFeeT>
            {
                    E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee, // "Assignment Recording Fee"
                    E_ClosingCostFeeMismoFeeT.DeedRecordingFee, // "Deed Recording Fee"
                    E_ClosingCostFeeMismoFeeT.MortgageRecordingFee, // "Mortgage Recording Fee"
                    E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee, // "Release Recording Fee"
                    E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee, // "Municipal Lien Certificate Recording Fee"
                    E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination, // "Recording Fee For Subordination"
                    E_ClosingCostFeeMismoFeeT.RecordingFeeTotal, // "Recording Fee Total"
                    E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee, // "Power of Attorney Recording Fee"
            };

        /// <summary>
        /// Fee types that should be considered for ten percent tolerance violations.
        /// </summary>
        private static HashSet<E_ClosingCostFeeMismoFeeT> tenPercentToleranceFeeTypes =
            new HashSet<E_ClosingCostFeeMismoFeeT>(
                recordingFeeTypes
                .Concat(new E_ClosingCostFeeMismoFeeT[0])); // Add more non-recording fee types here

        /// <summary>
        /// Fee types that should always be excluded from either the zero or ten percent tolerance calculations.
        /// </summary>
        private static HashSet<Guid> tridToleranceCalcExceptionsFeeTypeIds = new HashSet<Guid>(
            new Guid[]
            {
                DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId,
                DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId,
                DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId,
                DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId,
                DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId,
                DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId, // Property Taxes Reserves???
                DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId,
                DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId,
                DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId,
            });

        /// <summary>
        /// Simply meaning no violations will be shown to the be user.
        /// </summary>
        private static GfeToleranceStatus noViolation = new GfeToleranceStatus(needsZeroAlert: false, needsTenAlert: false, redisclosingWillClearZero: false, redisclosingWillClearTen: false);

        /// <summary>
        /// The initialized loan used by the private methods.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// Gets a HashSet of the MISMO recording fees types for quick lookups.
        /// </summary>
        /// <value>A HashSet of the MISMO recording fees types for quick lookups.</value>
        public static HashSet<E_ClosingCostFeeMismoFeeT> RecordingFeeTypes
        {
            get { return recordingFeeTypes; }
        }

        /// <summary>
        /// Given a fee, tells if we should consider it for zero tolerance violation when in new gfe mode.
        /// </summary>
        /// <param name="fee">The closing cost fee to consider or not.</param>
        /// <returns>True iff we want to consider that fee in the new gfe mode.</returns>
        public static bool ConsiderFeeForZeroToleranceInNewGFEMode(BorrowerClosingCostFee fee)
        {
            return
                  fee.GfeSectionT == E_GfeSectionT.B1
               || fee.GfeSectionT == E_GfeSectionT.B2
               || fee.GfeSectionT == E_GfeSectionT.B8;
        }

        /// <summary>
        /// Given a fee, tells if we should consider it for zero tolerance violation when in TRID mode.
        /// </summary>
        /// <param name="fee">The closing cost fee to consider or not.</param>
        /// <returns>True iff we want to consider that fee in the TRID mode.</returns>
        public static bool ConsiderFeeForZeroToleranceInTRIDMode(BorrowerClosingCostFee fee)
        {
            return !(tridToleranceCalcExceptionsFeeTypeIds.Contains(fee.SourceFeeTypeId ?? fee.ClosingCostFeeTypeId) || tenPercentToleranceFeeTypes.Contains(fee.MismoFeeT)) &&
                    ConsiderFeeForUnlimitedToleranceInTRIDMode(fee) == false &&
                    ConsiderFeeForTenToleranceInTRIDMode(fee) == false;
        }

        /// <summary>
        /// Given a fee, tells if we should consider it for ten percent tolerance violation when in new gfe mode.
        /// </summary>
        /// <param name="fee">The closing cost fee to consider or not.</param>
        /// <returns>True iff we want to consider that fee in the new gfe mode.</returns>
        public static bool ConsiderFeeForTenToleranceInNewGFEMode(BorrowerClosingCostFee fee)
        {
            return
                 ((fee.GfeSectionT == E_GfeSectionT.B3
                || fee.GfeSectionT == E_GfeSectionT.B4
                || fee.GfeSectionT == E_GfeSectionT.B5
                || fee.GfeSectionT == E_GfeSectionT.B6)
                && !fee.DidShop)
                || fee.GfeSectionT == E_GfeSectionT.B7;
        }

        /// <summary>
        /// Given a fee, tells if we should consider it for ten percent tolerance violation when in TRID mode.
        /// </summary>
        /// <param name="fee">The closing cost fee to consider or not.</param>
        /// <returns>True iff we want to consider that fee in the TRID mode.</returns>
        public static bool ConsiderFeeForTenToleranceInTRIDMode(BorrowerClosingCostFee fee)
        {
            return !(tridToleranceCalcExceptionsFeeTypeIds.Contains(fee.SourceFeeTypeId ?? fee.ClosingCostFeeTypeId) || zeroToleranceFeeTypes.Contains(fee.MismoFeeT)) &&
                    ConsiderFeeForUnlimitedToleranceInTRIDMode(fee) == false &&
                    (tenPercentToleranceFeeTypes.Contains(fee.MismoFeeT) ||
                      (fee.IsThirdParty == true && fee.Beneficiary != E_AgentRoleT.Lender && fee.Beneficiary != E_AgentRoleT.Broker &&
                       (fee.BeneficiaryAgent == null || fee.BeneficiaryAgent.IsLender == false) && fee.IsAffiliate == false && fee.CanShop == true));
        }

        /// <summary>
        /// Given a fee, tells if we consider it for unlimited tolerance in TRID mode.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True iff the fee has unlimited tolerance in TRID mode.</returns>
        public static bool ConsiderFeeForUnlimitedToleranceInTRIDMode(BorrowerClosingCostFee fee)
        {
            // EM - 9/29/15 - If the fee is Archived, ignore they DidShop bool by anding it to false.
            return !(tenPercentToleranceFeeTypes.Contains(fee.MismoFeeT) || zeroToleranceFeeTypes.Contains(fee.MismoFeeT)) &&
                    (tridToleranceCalcExceptionsFeeTypeIds.Contains(fee.SourceFeeTypeId ?? fee.ClosingCostFeeTypeId) || fee.DidShop == true || fee.IsOptional == true || fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionH);
        }

        /// <summary>
        /// Gets violations for the loan, depending on which mode it is in.
        /// </summary>
        /// <param name="loanID">The id of the loan in question.</param>
        /// <param name="currentUser">The current user looking at the loan.</param>
        /// <returns>An object with which violations are in effect.</returns>
        public static GfeToleranceStatus GetGfeToleranceInfo(Guid loanID, AbstractUserPrincipal currentUser)
        {
            if (ConstStage.EnableGetGfeToleranceInfoCache)
            {
                string key = loanID + "_" + Tools.GetLoanFileVersion(loanID, PrincipalFactory.CurrentPrincipal.BrokerId);

                return MemoryCacheUtilities.GetOrAddExisting(key, () => GetGfeToleranceInfoValueFactory(loanID, currentUser), DateTimeOffset.Now.AddHours(6));
            }
            else
            {
                return GetGfeToleranceInfoValueFactory(loanID, currentUser);
            }
        }

        /// <summary>
        /// Calculates the zero percent tolerance cure depending on mode. Only for Integrated Disclosure or New GFE Mode.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archivedFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="discReg">The disclosure regulation setting.</param>
        /// <param name="feeVer">The closing cost fee version.</param>
        /// <param name="archivedCreditAsCharge">The archived discount points.</param>
        /// <param name="liveCreditAsCharge">The live discount points.</param>
        /// <returns>The tolerance cure if in a supported mode, 0 otherwise.</returns>
        public decimal GetZeroPercentToleranceCure(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archivedFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, E_sDisclosureRegulationT discReg, E_sClosingCostFeeVersionT feeVer, decimal archivedCreditAsCharge, decimal liveCreditAsCharge)
        {
            bool unusedAlert;
            if (discReg == E_sDisclosureRegulationT.TRID)
            {
                return this.CalculateZeroToleranceTRID(archivedClosingCostSet, archivedFilter, liveCCSet, liveFilter, ConsiderFeeForZeroToleranceInTRIDMode, archivedCreditAsCharge, liveCreditAsCharge, out unusedAlert);
            }
            else if (discReg == E_sDisclosureRegulationT.GFE && (feeVer == E_sClosingCostFeeVersionT.ClosingCostFee2015 || feeVer == E_sClosingCostFeeVersionT.LegacyButMigrated))
            {
                return this.CalculateZeroToleranceGFE(archivedClosingCostSet, archivedFilter, liveCCSet, liveFilter, ConsiderFeeForZeroToleranceInNewGFEMode, archivedCreditAsCharge, liveCreditAsCharge, out unusedAlert);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Calculate the zero percent tolerance cure depending on mode. Only for Integrated Disclosure or New GFE Mode.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archiveFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="discReg">The disclosure regulation setting.</param>
        /// <param name="feeVer">The closing cost fee version.</param>
        /// <returns>The tolerance cure if in a supported mode, 0 otherwise.</returns>
        public decimal GetTenPercentToleranceCure(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archiveFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, E_sDisclosureRegulationT discReg, E_sClosingCostFeeVersionT feeVer)
        {
            bool unusedAlert;
            if (discReg == E_sDisclosureRegulationT.TRID)
            {
                return this.CalculateTenTolerance(archivedClosingCostSet, archiveFilter, liveCCSet, liveFilter, ConsiderFeeForTenToleranceInTRIDMode, out unusedAlert);
            }
            else if (discReg == E_sDisclosureRegulationT.GFE && (feeVer == E_sClosingCostFeeVersionT.ClosingCostFee2015 || feeVer == E_sClosingCostFeeVersionT.LegacyButMigrated))
            {
                return this.CalculateTenTolerance(archivedClosingCostSet, archiveFilter, liveCCSet, liveFilter, ConsiderFeeForTenToleranceInNewGFEMode, out unusedAlert);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Checks if the live closing cost set fees are greater than the closing cost set of the archived.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archiveFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="discReg">The regulation mode.</param>
        /// <param name="feeVer">The fee version.</param>
        /// <returns>True if live fees are 1.1 times greater than the archived. False otherwise.</returns>
        public bool CheckLiveFeesGreaterThanArchiveForTenTolerance(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archiveFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, E_sDisclosureRegulationT discReg, E_sClosingCostFeeVersionT feeVer)
        {
            bool greater = false;
            if (discReg == E_sDisclosureRegulationT.TRID)
            {
                this.CalculateTenTolerance(archivedClosingCostSet, archiveFilter, liveCCSet, liveFilter, ConsiderFeeForTenToleranceInTRIDMode, out greater);
            }
            else if (discReg == E_sDisclosureRegulationT.GFE && (feeVer == E_sClosingCostFeeVersionT.ClosingCostFee2015 || feeVer == E_sClosingCostFeeVersionT.LegacyButMigrated))
            {
                this.CalculateTenTolerance(archivedClosingCostSet, archiveFilter, liveCCSet, liveFilter, ConsiderFeeForTenToleranceInNewGFEMode, out greater);
            }
            else
            {
                return false;
            }

            return greater;
        }

        /// <summary>
        /// Gets violations for the loan, depending on which mode it is in.
        /// </summary>
        /// <param name="loanID">The id of the loan in question.</param>
        /// <param name="currentUser">The current user looking at the loan.</param>
        /// <returns>An object with which violations are in effect.</returns>
        private static GfeToleranceStatus GetGfeToleranceInfoValueFactory(Guid loanID, AbstractUserPrincipal currentUser)
        {
            var violationGetter = new GfeToleranceViolationCalculator();
            return violationGetter.GetGfeToleranceInfoImpl(loanID, currentUser);
        }

        /// <summary>
        /// Gets violations for the loan, depending on which mode it is in.
        /// </summary>
        /// <param name="loanID">The id of the loan in question.</param>
        /// <param name="currentUser">The current user looking at the loan.</param>
        /// <returns>An object with which violations are in effect.</returns>
        private GfeToleranceStatus GetGfeToleranceInfoImpl(Guid loanID, AbstractUserPrincipal currentUser)
        {
            if (currentUser.BrokerId != Guid.Empty)
            {
                var broker = currentUser.BrokerDB;

                if (!broker.IsGFEandCoCVersioningEnabled)
                {
                    return noViolation;
                }
            }

            this.dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanID, typeof(GfeToleranceViolationCalculator));
            this.dataLoan.ByPassFieldSecurityCheck = true;
            this.dataLoan.InitLoad();

            if (!this.dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return noViolation;
            }

            if (this.dataLoan.sIsLineOfCredit)
            {
                return noViolation;
            }

            if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.Blank)
            {
                return noViolation;
            }
            else if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                return this.GetGfeToleranceInfoForTRID();
            }
            else if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE &&
                (this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015
                || this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated))
            {
                return this.GetGfeToleranceInfoForNewGfe();
            }
            else if (this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                return this.GetGfeToleranceInfoForLegacy();
            }
            else
            {
                Tools.LogError(
                    string.Format(
                        "Unhandled enums sDisclosureRegulationT = {0}, sClosingCostFeeVersionT = {1}.",
                        this.dataLoan.sDisclosureRegulationT.ToString("g"),
                        this.dataLoan.sClosingCostFeeVersionT.ToString("g")));

                return noViolation;
            }
        }

        /// <summary>
        /// Gets violations when the loan is in TRID mode.
        /// </summary>
        /// <returns>An object with which violations are in effect.</returns>
        private GfeToleranceStatus GetGfeToleranceInfoForTRID()
        {
            return this.GetGfeToleranceInfoForFees(ConsiderFeeForZeroToleranceInTRIDMode, ConsiderFeeForTenToleranceInTRIDMode);
        }

        /// <summary>
        /// Gets violations when the loan is in new gfe mode.
        /// </summary>
        /// <returns>An object with which violations are in effect.</returns>
        private GfeToleranceStatus GetGfeToleranceInfoForNewGfe()
        {
            return this.GetGfeToleranceInfoForFees(ConsiderFeeForZeroToleranceInNewGFEMode, ConsiderFeeForTenToleranceInNewGFEMode);
        }

        /// <summary>
        /// Gets violations based on the new fee structure given the provided fee selectors.
        /// </summary>
        /// <param name="zeroToleranceFeeSelector">A function that returns true iff the fee should be considered for zero tolerance violations.</param>
        /// <param name="tenToleranceFeeSelector">A function that returns true iff the fee should be considered for ten percent tolerance violations.</param>
        /// <returns>An object containing any violations that may have occurred.</returns>
        private GfeToleranceStatus GetGfeToleranceInfoForFees(Predicate<BorrowerClosingCostFee> zeroToleranceFeeSelector, Predicate<BorrowerClosingCostFee> tenToleranceFeeSelector)
        {
            if (this.dataLoan.sClosingCostArchive.Count == 0)
            {
                return noViolation;
            }

            var lastDisclosedArchive = this.GetLastDisclosedClosingCostArchive();

            if (lastDisclosedArchive == null)
            {
                return noViolation;
            }

            var lastDisclosedClosingCostSet = lastDisclosedArchive.GetClosingCostSet();
            bool needsZeroPercentAlert = this.HasZeroPercentToleranceViolation(lastDisclosedArchive, lastDisclosedClosingCostSet, zeroToleranceFeeSelector);

            var tenPercentClosingCostSet = this.GetTenPercentToleranceClosingCostSet(lastDisclosedClosingCostSet);
            bool needsTenPercentAlert = this.HasTenPercentToleranceViolation(tenPercentClosingCostSet, tenToleranceFeeSelector);

            bool redisclosingWillClearZeroPercent = false;
            bool redisclosingWillClearTenPercent = false;

            if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.dataLoan.sHasLoanEstimateArchiveInPendingStatus)
            {
                var pendingArchive = this.dataLoan.sLoanEstimateArchiveInPendingStatus;
                if (this.dataLoan.sTridTargetRegulationVersionT == LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT.TRID2017 &&
                    pendingArchive.LinkedArchiveId.HasValue)
                {
                    // Use the tolerance data for the pending archive.
                    pendingArchive = this.dataLoan.sClosingCostArchive.FirstOrDefault(a => a.Id == pendingArchive.LinkedArchiveId);
                }

                var pendingClosingCosts = pendingArchive.GetClosingCostSet();

                bool pendingArchiveHasZeroPercentViolation = this.HasZeroPercentToleranceViolation(pendingArchive, pendingClosingCosts, zeroToleranceFeeSelector);
                bool pendingArchiveHasTenPercentViolation = this.HasTenPercentToleranceViolation(pendingClosingCosts, tenToleranceFeeSelector);

                redisclosingWillClearZeroPercent = needsZeroPercentAlert && !pendingArchiveHasZeroPercentViolation;
                redisclosingWillClearTenPercent = needsTenPercentAlert && !pendingArchiveHasTenPercentViolation;
            }

            return new GfeToleranceStatus(needsZeroPercentAlert, needsTenPercentAlert, redisclosingWillClearZeroPercent, redisclosingWillClearTenPercent);
        }

        /// <summary>
        /// Gets the last disclosed closing cost archive.
        /// </summary>
        /// <returns>The last disclosed closing cost archive. Null if not found.</returns>
        private ClosingCostArchive GetLastDisclosedClosingCostArchive()
        {
            bool isIntegratedDisclosureMode = this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            bool isNewGfeMode = this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE
                && (this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 || this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated);

            var lastDisclosedArchive = this.dataLoan.sClosingCostArchive.FirstOrDefault(p => p.Id == this.dataLoan.sLastDisclosedLoanEstimateArchiveLinkedToleranceId);

            if (isNewGfeMode && 
                (lastDisclosedArchive == null || lastDisclosedArchive.ClosingCostArchiveType != ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015))
            {
                lastDisclosedArchive = this.dataLoan.sClosingCostArchive.FirstOrDefault(p => p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015);
            }
            else if (isIntegratedDisclosureMode &&
                lastDisclosedArchive != null &&
                lastDisclosedArchive.ClosingCostArchiveType != ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)
            {
                // If we are in TRID mode, then the last disclosed GFE archive ID will
                // be the id of the most recent LE archive in the disclosed status.
                // If we haven't found that, then we need to act as if there were no archives.
                // This should be fine, because we only ever expect the tolerance archive to
                // be set to a LE archive in the disclosed status.
                lastDisclosedArchive = null;
            }
            else if (!isNewGfeMode && !isIntegratedDisclosureMode)
            {
                // Unsupported mode.
                lastDisclosedArchive = null;
                Tools.LogWarning("Tried to get last disclosed closing cost archive for file using old data layer.");
            }

            return lastDisclosedArchive;
        }

        /// <summary>
        /// Gets the closing cost set to use for the ten percent tolerance test.
        /// </summary>
        /// <param name="defaultClosingCostSet">The default closing cost set if no basis archive is found.</param>
        /// <returns>The closing cost set to use for the ten percent tolerance test.</returns>
        private BorrowerClosingCostSet GetTenPercentToleranceClosingCostSet(BorrowerClosingCostSet defaultClosingCostSet)
        {
            var tenPercentClosingCostSet = defaultClosingCostSet;

            // If we are in integrated disclosure mode, 10% calculations need the basis archive.
            if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                var basisArchive = this.dataLoan.sClosingCostArchive.FirstOrDefault(p => p.Id == this.dataLoan.sTolerance10BasisLEArchiveId);
                if (basisArchive != null && basisArchive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)
                {
                    tenPercentClosingCostSet = basisArchive.GetClosingCostSet();
                }
            }

            return tenPercentClosingCostSet;
        }

        /// <summary>
        /// Gets a value indicating whether the given archive and closing cost set have a zero percent tolerance violation.
        /// </summary>
        /// <param name="archive">The closing cost archive.</param>
        /// <param name="archivedClosingCostSet">The closing cost set from the archive.</param>
        /// <param name="zeroToleranceFeeSelector">A function that returns true iff the fee should be considered for zero tolerance violations.</param>
        /// <returns>True if there is a zero percent tolerance violation. Otherwise, false.</returns>
        private bool HasZeroPercentToleranceViolation(ClosingCostArchive archive, BorrowerClosingCostSet archivedClosingCostSet, Predicate<BorrowerClosingCostFee> zeroToleranceFeeSelector)
        {
            decimal archivedCreditAsCharge;
            decimal liveCreditAsCharge;

            if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                archivedCreditAsCharge = -1 * archive.GetMoneyValue("sTRIDLoanEstimateLenderCredits");
                liveCreditAsCharge = -1 * decimal.Round(this.dataLoan.sTRIDLoanEstimateLenderCredits, 2, MidpointRounding.ToEven);
            }
            else
            {
                decimal archived_sLDiscnt = archive.GetMoneyValue("sLDiscnt");
                decimal live_sLDiscnt = decimal.Round(this.dataLoan.sLDiscnt, 2, MidpointRounding.ToEven);

                archivedCreditAsCharge = archived_sLDiscnt > 0 ? 0 : archived_sLDiscnt;
                liveCreditAsCharge = live_sLDiscnt > 0 ? 0 : live_sLDiscnt;
            }

            bool needsZeroPercentAlert;
            Func<BaseClosingCostFee, bool> liveFilter = this.GetLiveFeeFilter();

            // Made from an archive but we still want to include the orig comp fee.
            Func<BaseClosingCostFee, bool> lastDisclosedArchiveFilter = f => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)f, archivedClosingCostSet.ClosingCostArchive); 
            
            if (this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                this.CalculateZeroToleranceTRID(archivedClosingCostSet, lastDisclosedArchiveFilter, this.dataLoan.sClosingCostSet, liveFilter, zeroToleranceFeeSelector, archivedCreditAsCharge, liveCreditAsCharge, out needsZeroPercentAlert);
            }
            else
            {
                this.CalculateZeroToleranceGFE(archivedClosingCostSet, lastDisclosedArchiveFilter, this.dataLoan.sClosingCostSet, liveFilter, zeroToleranceFeeSelector, archivedCreditAsCharge, liveCreditAsCharge, out needsZeroPercentAlert);
            }

            return needsZeroPercentAlert;
        }

        /// <summary>
        /// Gets a value indicating whether the given closing cost set has a ten percent tolerance violation.
        /// </summary>
        /// <param name="tenPercentClosingCostSet">The closing cost set to evaluate.</param>
        /// <param name="tenToleranceFeeSelector">A function that returns true iff the fee should be considered for ten percent tolerance violations.</param>
        /// <returns>True if there is a ten percent tolerance violation. Otherwise, false.</returns>
        private bool HasTenPercentToleranceViolation(BorrowerClosingCostSet tenPercentClosingCostSet, Predicate<BorrowerClosingCostFee> tenToleranceFeeSelector)
        {
            bool needsTenPercentAlert;
            Func<BaseClosingCostFee, bool> liveFilter = this.GetLiveFeeFilter();

            // Made from an archive but we still want to include the orig comp fee.
            Func<BaseClosingCostFee, bool> tenPercArchiveFilter = f => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)f, tenPercentClosingCostSet.ClosingCostArchive);

            this.CalculateTenTolerance(tenPercentClosingCostSet, tenPercArchiveFilter, this.dataLoan.sClosingCostSet, liveFilter, tenToleranceFeeSelector, out needsTenPercentAlert);

            return needsTenPercentAlert;
        }

        /// <summary>
        /// Gets the filter to use for live fees in the zero and ten percent tolerance calculations.
        /// </summary>
        /// <returns>The filter to use for live fees in the zero and ten percent tolerance calculations.</returns>
        private Func<BaseClosingCostFee, bool> GetLiveFeeFilter()
        {
            return f => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter(
                (BorrowerClosingCostFee)f, 
                this.dataLoan.sOriginatorCompensationPaymentSourceT, 
                this.dataLoan.sDisclosureRegulationT, 
                this.dataLoan.sGfeIsTPOTransaction);
        }

        /// <summary>
        /// Calculates the zero percent tolerance cure for TRID loans and if an alert is needed.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archivedFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="zeroToleranceFeeSelector">The zero percent fee selector.</param>
        /// <param name="archivedCreditAsCharge">The archived discount points.</param>
        /// <param name="liveCreditAsCharge">The current discount points.</param>
        /// <param name="needsZeroPercentAlert">Value indicating if an alert is needed.</param>
        /// <returns>The tolerance cure value.</returns>
        private decimal CalculateZeroToleranceTRID(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archivedFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, Predicate<BorrowerClosingCostFee> zeroToleranceFeeSelector, decimal archivedCreditAsCharge, decimal liveCreditAsCharge, out bool needsZeroPercentAlert)
        {
            var liveZeroToleranceFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                         where zeroToleranceFeeSelector(fee)
                                         select fee).ToList();

            // EM - Case 227466 - Account for the live fee regardless of whether it passes the 0% selector;
            Dictionary<Guid, BorrowerClosingCostFee> archivedZeroToleranceFeesById = (from fee in archivedClosingCostSet.GetFees(archivedFilter).Cast<BorrowerClosingCostFee>()
                                                                                      select fee).ToDictionary(fee => fee.ClosingCostFeeTypeId);

            decimal sum = 0;
            needsZeroPercentAlert = false;

            foreach (BorrowerClosingCostFee fee in liveZeroToleranceFees)
            {
                if (archivedZeroToleranceFeesById.ContainsKey(fee.ClosingCostFeeTypeId))
                {
                    if (fee.TotalAmount > archivedZeroToleranceFeesById[fee.ClosingCostFeeTypeId].TotalAmount)
                    {
                        needsZeroPercentAlert = true;
                        sum += fee.TotalAmount - archivedZeroToleranceFeesById[fee.ClosingCostFeeTypeId].TotalAmount;
                    }
                }
                else
                {
                    if (fee.TotalAmount > 0)
                    {
                        needsZeroPercentAlert = true;
                        sum += fee.TotalAmount;
                    }
                }
            }

            if (liveCreditAsCharge > archivedCreditAsCharge)
            {
                needsZeroPercentAlert = true;
                sum += Tools.SumMoney(new decimal[] { liveCreditAsCharge, -1 * archivedCreditAsCharge });
            }

            return sum;
        }

        /// <summary>
        /// Calculates the zero percent tolerance cure for GFE loans and if an alert is needed.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archiveFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="zeroToleranceFeeSelector">The zero percent fee selector.</param>
        /// <param name="archivedCreditAsCharge">The archived discount points.</param>
        /// <param name="liveCreditAsCharge">The current discount points.</param>
        /// <param name="needsZeroPercentAlert">Value indicating if an alert is needed.</param>
        /// <returns>The tolerance cure value.</returns>
        private decimal CalculateZeroToleranceGFE(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archiveFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, Predicate<BorrowerClosingCostFee> zeroToleranceFeeSelector, decimal archivedCreditAsCharge, decimal liveCreditAsCharge, out bool needsZeroPercentAlert)
        {
            var liveZeroToleranceFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                         where zeroToleranceFeeSelector(fee)
                                         select fee).ToList();

            var archivedZeroToleranceFees = (from fee in archivedClosingCostSet.GetFees(archiveFilter).Cast<BorrowerClosingCostFee>()
                                             where zeroToleranceFeeSelector(fee)
                                             select fee).ToList();

            decimal boxA1FeeTotalLive = 0;
            decimal boxA1FeeTotalArchived = 0;
            decimal boxA1FeeCure;

            decimal boxA2FeeTotalLive = 0;
            decimal boxA2FeeTotalArchived = 0;
            decimal boxA2FeeCure;

            decimal boxB8FeeTotalLive = 0;
            decimal boxB8FeeTotalArchived = 0;
            decimal boxB8FeeCure;

            needsZeroPercentAlert = false;

            foreach (BorrowerClosingCostFee fee in liveZeroToleranceFees)
            {
                if (fee.GfeSectionT == E_GfeSectionT.B1)
                {
                    boxA1FeeTotalLive += fee.TotalAmount;
                }
                else if (fee.GfeSectionT == E_GfeSectionT.B2)
                {
                    boxA2FeeTotalLive += fee.TotalAmount;
                }
                else if (fee.GfeSectionT == E_GfeSectionT.B8)
                {
                    boxB8FeeTotalLive += fee.TotalAmount;
                }
            }

            boxA2FeeTotalLive += liveCreditAsCharge;

            foreach (BorrowerClosingCostFee archivedFee in archivedZeroToleranceFees)
            {
                if (archivedFee.GfeSectionT == E_GfeSectionT.B1)
                {
                    boxA1FeeTotalArchived += archivedFee.TotalAmount;
                }
                else if (archivedFee.GfeSectionT == E_GfeSectionT.B2)
                {
                    boxA2FeeTotalArchived += archivedFee.TotalAmount;
                }
                else if (archivedFee.GfeSectionT == E_GfeSectionT.B8)
                {
                    boxB8FeeTotalArchived += archivedFee.TotalAmount;
                }
            }

            boxA2FeeTotalArchived += archivedCreditAsCharge;

            boxA1FeeCure = Math.Max(0, boxA1FeeTotalLive - boxA1FeeTotalArchived);
            boxA2FeeCure = Math.Max(0, boxA2FeeTotalLive - boxA2FeeTotalArchived);
            boxB8FeeCure = Math.Max(0, boxB8FeeTotalLive - boxB8FeeTotalArchived);

            decimal sum = boxA1FeeCure + boxA2FeeCure + boxB8FeeCure;

            if (sum > 0)
            {
                needsZeroPercentAlert = true;
            }

            return sum;
        }

        /// <summary>
        /// Calculates the ten percent tolerance cure and if an alert is needed.
        /// </summary>
        /// <param name="archivedClosingCostSet">Closing cost set from the archive.</param>
        /// <param name="archiveFilter">The filter for the archived set.</param>
        /// <param name="liveCCSet">The live closing cost set.</param>
        /// <param name="liveFilter">The filter for the live set.</param>
        /// <param name="tenToleranceFeeSelector">The ten percent tolerance fee selector.</param>
        /// <param name="needsTenPercentAlert">Value indicating if an alert is needed.</param>
        /// <returns>The tolerance cure value.</returns>
        private decimal CalculateTenTolerance(BorrowerClosingCostSet archivedClosingCostSet, Func<BaseClosingCostFee, bool> archiveFilter, BorrowerClosingCostSet liveCCSet, Func<BaseClosingCostFee, bool> liveFilter, Predicate<BorrowerClosingCostFee> tenToleranceFeeSelector, out bool needsTenPercentAlert)
        {
            decimal liveTenToleranceFeesSum = 0;
            decimal archivedTenToleranceFeesSum = 0;

            // Recording fees must be aggregated and treated as a single fee. Zero recording fees should be included
            // here--we will only exclude if the sum is zero (see below).
            var liveTenToleranceRecordingFeeAmounts = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                       where tenToleranceFeeSelector(fee) &&
                                                             RecordingFeeTypes.Contains(fee.MismoFeeT)
                                                       select fee.TotalAmount).ToList();

            var liveTenToleranceRecordingFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                 where tenToleranceFeeSelector(fee) &&
                                                       RecordingFeeTypes.Contains(fee.MismoFeeT)
                                                 select fee).ToList();

            // Grab all the recording fees from the archive, regardless of whether or not they are in the live set.
            var archivedTenToleranceRecordingFeeAmounts = from fee in archivedClosingCostSet.GetFees(archiveFilter).Cast<BorrowerClosingCostFee>()
                                                          where tenToleranceFeeSelector(fee) &&
                                                                RecordingFeeTypes.Contains(fee.MismoFeeT)
                                                          select fee.TotalAmount;

            decimal liveTenToleranceRecordingFeesSum = Tools.SumMoney(liveTenToleranceRecordingFeeAmounts);
            decimal archivedTenToleranceRecordingFeeSum = Tools.SumMoney(archivedTenToleranceRecordingFeeAmounts);

            // Only add the recording fees to the sums if the live value is not 0 (i.e. treat
            // the recording fee sum the same as we treat the individual fees below).
            if (liveTenToleranceRecordingFeesSum != 0)
            {
                liveTenToleranceFeesSum += liveTenToleranceRecordingFeesSum;
                archivedTenToleranceFeesSum += archivedTenToleranceRecordingFeeSum;
            }

            // Non-recording fees receive the normal treatment, which screens out zeros.
            var liveTenToleranceNonRecordingFeeAmounts = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                          where tenToleranceFeeSelector(fee) &&
                                                                !RecordingFeeTypes.Contains(fee.MismoFeeT) &&
                                                                fee.TotalAmount != 0
                                                          select fee.TotalAmount).ToList();

            var liveTenToleranceNonRecordingFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                    where tenToleranceFeeSelector(fee) &&
                                                          !RecordingFeeTypes.Contains(fee.MismoFeeT) &&
                                                          fee.TotalAmount != 0
                                                    select fee).ToList();

            // Only grab the fees from the archive that have corresponding fees in the live set.
            var archivedTenToleranceNonRecordingFeeAmounts = from fee in archivedClosingCostSet.GetFees(archiveFilter).Cast<BorrowerClosingCostFee>()
                                                             where liveTenToleranceNonRecordingFees.Find(p => p.ClosingCostFeeTypeId.Equals(fee.ClosingCostFeeTypeId)) != null
                                                             select fee.TotalAmount;

            decimal liveTenToleranceNonRecordingFeesSum = Tools.SumMoney(liveTenToleranceNonRecordingFeeAmounts);
            decimal archivedTenToleranceNonRecordingFeesSum = Tools.SumMoney(archivedTenToleranceNonRecordingFeeAmounts);

            // Unconditionally add the non-recording fees to the sums.
            liveTenToleranceFeesSum += liveTenToleranceNonRecordingFeesSum;
            archivedTenToleranceFeesSum += archivedTenToleranceNonRecordingFeesSum;

            decimal adjustedarchivedTenToleranceFeesSum = Math.Round(1.1M * archivedTenToleranceFeesSum, 2);
            needsTenPercentAlert = liveTenToleranceFeesSum > adjustedarchivedTenToleranceFeesSum;
            decimal difference = Tools.SumMoney(new decimal[] { liveTenToleranceFeesSum, -1 * adjustedarchivedTenToleranceFeesSum });

            if (difference > 0)
            {
                return difference;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets violations when the loan is in legacy gfe mode.
        /// </summary>
        /// <returns>An object with which violations are in effect.</returns>
        private GfeToleranceStatus GetGfeToleranceInfoForLegacy()
        {
            if (this.dataLoan.LastDisclosedGFEArchive == null)
            {
                return noViolation;
            }

            bool needsTenPercentAlert = false;
            bool needsZeroPercentAlert = false;

            var currentA1Sum = this.dataLoan.sGfeA1Sum;
            var currentA2Sum = this.dataLoan.sGfeA2Sum;
            var currentB8Sum = this.dataLoan.sGfeB8Sum;
            var currentB2ThroughB7SumExcludingBorrowerSelected = this.dataLoan.sGfeB3Though7SumNotBorrowerSelected;

            this.dataLoan.ApplyGFEArchive(this.dataLoan.LastDisclosedGFEArchive);

            var lastArchivedA1Sum = this.dataLoan.sGfeA1Sum;
            var lastArchivedA2Sum = this.dataLoan.sGfeA2Sum;
            var lastArchivedB8Sum = this.dataLoan.sGfeB8Sum;
            var lastArchivedB2ThroughB7SumExcludingBorrowerSelected = this.dataLoan.sGfeB3Though7SumNotBorrowerSelected;

            needsZeroPercentAlert = currentA1Sum > lastArchivedA1Sum
                                        || currentA2Sum > lastArchivedA2Sum
                                        || currentB8Sum > lastArchivedB8Sum;

            needsTenPercentAlert = currentB2ThroughB7SumExcludingBorrowerSelected
                > 1.1M * lastArchivedB2ThroughB7SumExcludingBorrowerSelected;

            return new GfeToleranceStatus(needsZeroPercentAlert, needsTenPercentAlert, redisclosingWillClearZero: false, redisclosingWillClearTen: false);
        }
    }
}
