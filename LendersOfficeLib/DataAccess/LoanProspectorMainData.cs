using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanProspectorMainData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanProspectorMainData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sFreddieLoanId");
            list.Add("sFreddieTransactionId");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLoanProspectorMainData(Guid fileId) : base(fileId, "CLoanProspectorMainData", s_selectProvider)
		{
		}
	}
}
