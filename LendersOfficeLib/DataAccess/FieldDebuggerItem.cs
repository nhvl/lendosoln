﻿/// <copyright file="DependencyUtils.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   5/16/2016
/// </summary>
namespace DataAccess
{
    /// <summary>
    /// Provides a wrapper for field debugger items.
    /// </summary>
    public class FieldDebuggerItem
    {
        /// <summary>
        /// Gets or sets the value for the name of the field.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the field.
        /// </value>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the
        /// value of the field.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> representation for the 
        /// value of the field.
        /// </value>
        public string FieldValue { get; set; }

        /// <summary>
        /// Gets or sets the value for the json of the field.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> json of the field.
        /// </value>
        public string FieldJson { get; set; }

        /// <summary>
        /// Gets or sets the value for the message logged
        /// while obtaining the field's value in the event
        /// that an error was encountered or the field's 
        /// value is null, the empty string, or not defined.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> message.
        /// </value>
        public string Message { get; set; }
    }
}
