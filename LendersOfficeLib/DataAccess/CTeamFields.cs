﻿// <copyright file="CTeamFields.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Matthew Flynn
//    Date:   06/02/2014
//  
//    Mosty followed mechanism of CEmployeeFields      
// </summary>

namespace DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;

    /// <summary>
    /// Represents a Team.
    /// </summary>
    public class CTeamFields
    {
        /// <summary>
        /// Storage row of team.
        /// </summary>
        private IDataContainer row;

        /// <summary>
        /// New team id before it is committed.
        /// </summary>
        private Guid tempNewTeamId = Guid.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CTeamFields" /> class.
        /// </summary>
        /// <param name="dataRow">Data row containing team.</param>
        public CTeamFields(IDataContainer dataRow)
        {
            this.row = dataRow;
            this.IsDirty = false;
        }

        /// <summary>
        /// Gets new empty team.
        /// </summary>
        /// <value>New empty team.</value>
        public static CTeamFields Empty
        {
            get { return new CTeamFields(null); }
        }
       
        /// <summary>
        /// Gets a value indicating whether it is valid.
        /// </summary>
        /// <value>If it is valid.</value>
        public bool IsValid
        {
            get { return this.row != null; }
        }

        /// <summary>
        /// Gets Role of the team.
        /// </summary>
        /// <value>Role of the team.</value>
        public CEmployeeFields.E_Role Role
        {
            get
            {
                if (!this.IsValid)
                {
                    return CEmployeeFields.E_Role.Undefined;
                }

                if (this.RoleId == CEmployeeFields.s_LoanRepRoleId)
                {
                    return CEmployeeFields.E_Role.LoanRep;
                }
                else if (this.RoleId == CEmployeeFields.s_ManagerRoleId)
                {
                    return CEmployeeFields.E_Role.Manager;
                }
                else if (this.RoleId == CEmployeeFields.s_CallCenterAgentRoleId)
                {
                    return CEmployeeFields.E_Role.CallCenterAgent;
                }
                else if (this.RoleId == CEmployeeFields.s_ProcessorRoleId)
                {
                    return CEmployeeFields.E_Role.Processor;
                }
                else if (this.RoleId == CEmployeeFields.s_RealEstateAgentRoleId)
                {
                    return CEmployeeFields.E_Role.RealEstateAgent;
                }
                else if (this.RoleId == CEmployeeFields.s_AdministratorRoleId)
                {
                    return CEmployeeFields.E_Role.Administrator;
                }
                else if (this.RoleId == CEmployeeFields.s_FunderRoleId)
                {
                    return CEmployeeFields.E_Role.Funder;
                }
                else if (this.RoleId == CEmployeeFields.s_UnderwriterRoleId)
                {
                    return CEmployeeFields.E_Role.Underwriter;
                }
                else if (this.RoleId == CEmployeeFields.s_LenderAccountExecRoleId)
                {
                    return CEmployeeFields.E_Role.LenderAccountExec;
                }
                else if (this.RoleId == CEmployeeFields.s_LockDeskRoleId)
                {
                    return CEmployeeFields.E_Role.LockDesk;
                }
                else if (this.RoleId == CEmployeeFields.s_LoanOpenerId)
                {
                    return CEmployeeFields.E_Role.LoanOpener;
                }
                else if (this.RoleId == CEmployeeFields.s_ShipperId)
                {
                    return CEmployeeFields.E_Role.Shipper;
                }
                else if (this.RoleId == CEmployeeFields.s_CloserId)
                {
                    return CEmployeeFields.E_Role.Closer;
                }
                else if (this.RoleId == CEmployeeFields.s_AccountantId)
                {
                    return CEmployeeFields.E_Role.Accountant;
                }
                else if (this.RoleId == CEmployeeFields.s_BrokerProcessorId)
                {
                    return CEmployeeFields.E_Role.BrokerProcessor;
                }
                else if (this.RoleId == CEmployeeFields.s_PostCloserId)
                {
                    return CEmployeeFields.E_Role.PostCloser;
                }
                else if (this.RoleId == CEmployeeFields.s_InsuringId)
                {
                    return CEmployeeFields.E_Role.Insuring;
                }
                else if (this.RoleId == CEmployeeFields.s_CollateralAgentId)
                {
                    return CEmployeeFields.E_Role.CollateralAgent;
                }
                else if (this.RoleId == CEmployeeFields.s_DocDrawerId)
                {
                    return CEmployeeFields.E_Role.DocDrawer;
                }
                else if (this.RoleId == CEmployeeFields.s_CreditAuditorId)
                {
                    return CEmployeeFields.E_Role.CreditAuditor;
                }
                else if (this.RoleId == CEmployeeFields.s_DisclosureDeskId)
                {
                    return CEmployeeFields.E_Role.DisclosureDesk;
                }
                else if (this.RoleId == CEmployeeFields.s_JuniorProcessorId)
                {
                    return CEmployeeFields.E_Role.JuniorProcessor;
                }
                else if (this.RoleId == CEmployeeFields.s_JuniorUnderwriterId)
                {
                    return CEmployeeFields.E_Role.JuniorUnderwriter;
                }
                else if (this.RoleId == CEmployeeFields.s_LegalAuditorId)
                {
                    return CEmployeeFields.E_Role.LegalAuditor;
                }
                else if (this.RoleId == CEmployeeFields.s_LoanOfficerAssistantId)
                {
                    return CEmployeeFields.E_Role.LoanOfficerAssistant;
                }
                else if (this.RoleId == CEmployeeFields.s_PurchaserId)
                {
                    return CEmployeeFields.E_Role.Purchaser;
                }
                else if (this.RoleId == CEmployeeFields.s_QCComplianceId)
                {
                    return CEmployeeFields.E_Role.QCCompliance;
                }
                else if (this.RoleId == CEmployeeFields.s_SecondaryId)
                {
                    return CEmployeeFields.E_Role.Secondary;
                }
                else if (this.RoleId == CEmployeeFields.s_ServicingId)
                {
                    return CEmployeeFields.E_Role.Servicing;
                }

                throw new System.Exception("Unhandled role id assigned to a loan file");
            }
        }

        /// <summary>
        /// Gets the role id of this team.
        /// </summary>
        /// <value>The role id of this team.</value>
        public Guid RoleId
        {
            get { return this.GetGuid("RoleId"); }
        }

        /// <summary>
        /// Gets the name of this team.
        /// </summary>
        /// <value>The name of this team.</value>
        public string Name
        {
            get
            {
                return this.GetString("Name");
            }
        }

        /// <summary>
        /// Gets the team id of this team.
        /// </summary>
        /// <value>The team id of this team.</value>
        public Guid TeamId
        {
            get
            {
                if (this.IsDirty)
                {
                    return this.tempNewTeamId;
                }
                else
                {
                    return this.GetGuid("Id");
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this team id has been updated.
        /// </summary>
        /// <value>If it is dirty.</value>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// Sets the new team Id.
        /// </summary>
        /// <param name="teamId">New Id to be set.</param>
        public void SetNewTeamId(Guid teamId)
        {
            this.IsDirty = true;
            this.tempNewTeamId = teamId;
        }

        /// <summary>
        /// Get Id value from row.
        /// </summary>
        /// <param name="fieldName">Name of column.</param>
        /// <returns>Id value of fieldName.</returns>
        private Guid GetGuid(string fieldName)
        {
            if (!this.IsValid)
            {
                return Guid.Empty;
            }

            if (this.IsDirty == true)
            {
                return Guid.Empty;
            }

            return new Guid(this.row[fieldName].ToString());
        }

        /// <summary>
        /// Get string value from row.
        /// </summary>
        /// <param name="fieldName">Name of column.</param>
        /// <returns>String value of fieldName.</returns>
        private string GetString(string fieldName)
        {
            if (!this.IsValid)
            {
                return string.Empty;
            }

            if (this.IsDirty == true)
            {
                return string.Empty;
            }

            if (this.row[fieldName] == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                return this.row[fieldName].ToString();
            }
        }
    }
}