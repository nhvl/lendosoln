using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanTemplateSettingsData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanTemplateSettingsData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sTemplatePublishLevelT");
            list.Add("sTemplateAssignLoanOfficerT");
            list.Add("sTemplateAssignProcessorT");
            list.Add("sTemplateAssignManagerT");
            list.Add("sTemplateAssignCallCenterAgentT");
            list.Add("sTemplateAssignSellingAgentT");
            list.Add("sTemplateAssignLoanOpenerT");
            list.Add("sTemplateAssignUnderwriterT");
            list.Add("sTemplateAssignLockDeskT");
            list.Add("sTemplateAssignAeT");
			list.Add("sBranchIdForNewFileFromTemplate");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLoanTemplateSettingsData(Guid fileId) : base(fileId, "CLoanTemplateSettingsData", s_selectProvider)
		{
		}
	}
}
