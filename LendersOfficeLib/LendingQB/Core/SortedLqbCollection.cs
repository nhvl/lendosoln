﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A collection implementation that has a custom order.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the collection.</typeparam>
    internal class SortedLqbCollection<TValueKind, TIdValue, TValue> : BaseLqbCollectionDecorator<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// The comparer to use when ordering the collection.
        /// </summary>
        private IComparer<TValue> comparer;

        /// <summary>
        /// Initializes a new instance of the <see cref="SortedLqbCollection{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="comparer">The comparer to use when ordering the collection.</param>
        private SortedLqbCollection(
            IMutableLqbCollection<TValueKind, TIdValue, TValue> contained,
            IComparer<TValue> comparer)
            : base(contained)
        {
            this.comparer = comparer;
        }

        /// <summary>
        /// Gets the identifiers of all the values in the collection.
        /// </summary>
        /// <value>The identifiers of all the values in the collection.</value>
        public override IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> Keys => this.Select(kvp => kvp.Key);

        /// <summary>
        /// Gets all the values in the collection.
        /// </summary>
        /// <value>All the values in the collection.</value>
        public override IEnumerable<TValue> Values => this.Select(kvp => kvp.Value);

        /// <summary>
        /// Create an implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="comparer">The comparer to use when sorting the collection.</param>
        /// <returns>An implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbCollection<TValueKind, TIdValue, TValue> Create(
            IMutableLqbCollection<TValueKind, TIdValue, TValue> contained,
            IComparer<TValue> comparer)
        {
            return new SortedLqbCollection<TValueKind, TIdValue, TValue>(contained, comparer);
        }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public override IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Keys.Select(k => new DataPathSelectionElement(new SelectIdExpression(k.Value.ToString())));
        }

        /// <summary>
        /// Gets an enumerator for the collection. The enumerator will not be affected by collection
        /// additions, removals, or record updates. The enumerator works off of the state of the 
        /// collection when it was initially retrieved.
        /// </summary>
        /// <returns>An enumerator of key-value pairs for the collection.</returns>
        public override IEnumerator<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> GetEnumerator()
        {
            return this.Collection.OrderBy(kvp => kvp.Value, this.comparer).ThenBy(kvp => kvp.Key.Value).GetEnumerator();
        }
    }
}
