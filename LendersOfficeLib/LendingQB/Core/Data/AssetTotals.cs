﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains summary information for assets.
    /// </summary>
    public class AssetTotals
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetTotals"/> class.
        /// </summary>
        /// <param name="subtotalReo">The REO subtotal.</param>
        /// <param name="subtotalLiquid">The liquid assets subtotal.</param>
        /// <param name="subtotalIlliquid">The illiquid assets subtotal.</param>
        internal AssetTotals(Money subtotalReo, Money subtotalLiquid, Money subtotalIlliquid)
        {
            this.SubtotalReo = subtotalReo;
            this.SubtotalLiquid = subtotalLiquid;
            this.SubtotalIlliquid = subtotalIlliquid;
        }

        /// <summary>
        /// Gets the REO market value.
        /// </summary>
        public Money SubtotalReo { get; }

        /// <summary>
        /// Gets the liquid assets subtotal.
        /// </summary>
        public Money SubtotalLiquid { get; }

        /// <summary>
        /// Gets the illiquid assets subtotal.
        /// </summary>
        public Money SubtotalIlliquid { get; }

        /// <summary>
        /// Gets the total value.
        /// </summary>
        public Money Total => this.SubtotalReo + this.SubtotalLiquid + this.SubtotalIlliquid;

        /// <summary>
        /// Creates assets totals from the given assets and real properties.
        /// </summary>
        /// <param name="assets">The assets.</param>
        /// <param name="realProperties">The real properties.</param>
        /// <param name="includeCashDeposit">A value indicating whether cash deposits should be included in the totals.</param>
        /// <returns>The asset totals.</returns>
        public static AssetTotals Create(IEnumerable<Asset> assets, IEnumerable<RealProperty> realProperties, bool includeCashDeposit)
        {
            if (assets == null)
            {
                throw new ArgumentNullException();
            }

            Money liquid = 0;
            Money illiquid = 0;

            foreach (var asset in assets)
            {
                if (asset.IsCreditAtClosing.HasValue && asset.IsCreditAtClosing.Value)
                {
                    continue;
                }

                if (asset.IsLiquidAsset.GetValueOrDefault(false))
                {
                    if (asset.AssetType == DataAccess.E_AssetT.CashDeposit)
                    {
                        if (includeCashDeposit)
                        {
                            liquid += asset.Value ?? Money.Zero;
                        }
                    }
                    else
                    {
                        liquid += asset.Value ?? Money.Zero;
                    }
                }
                else
                {
                    illiquid += asset.Value ?? Money.Zero;
                }
            }

            // Eventually, we may want to take in the real estate totals and pull the relevant total from that.
            var reo = realProperties.Where(r => r.Status != DataAccess.E_ReoStatusT.Sale)
                .Select(r => r.MarketValue)
                .Aggregate(Money.Zero, (total, marketValue) => total + (marketValue ?? Money.Zero));

            return new AssetTotals(reo, liquid, illiquid);
        }
    }
}
