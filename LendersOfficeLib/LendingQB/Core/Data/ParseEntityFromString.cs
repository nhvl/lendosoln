﻿namespace LendingQB.Core.Data
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to parse semantic data types from strings using
    /// different formatting logic based on the target.
    /// </summary>
    /// <remarks>
    /// This class should be exposed as internal, but it is public for the
    /// purpose of the migration code in ScheduleExecutable.
    /// </remarks>
    public class ParseEntityFromString : ParseFromStringBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseEntityFromString"/> class.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        public ParseEntityFromString(FormatTarget target, FormatDirection direction)
        {
            this.Converter = LosConvert.Create(target);
            this.Direction = direction;

            this.NumericConverter = new FormatAsNumericBase();
        }

        /// <summary>
        /// Gets or sets the converter.
        /// </summary>
        private ILosConvert Converter { get; set; }

        /// <summary>
        /// Gets or sets a numeric formatter.
        /// </summary>
        private IFormatAsNumeric NumericConverter { get; set; }

        /// <summary>
        /// Gets or sets the direction in which the formatting is going to be used.
        /// </summary>
        private FormatDirection Direction { get; set; }

        /// <summary>
        /// Parse the string representation into a Count&lt;T&gt; instance.
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <param name="value">The string representation of a Count&lt;T&gt; value.</param>
        /// <returns>The Count&lt;T&gt; instance, or null.</returns>
        public override Count<T>? TryParseCount<T>(string value)
        {
            int count = this.Converter.ToCount(value);
            return Count<T>.Create(count);
        }

        /// <summary>
        /// Parse the string representation into a Money instance.
        /// </summary>
        /// <param name="value">The string representation of a monetary value.</param>
        /// <returns>The Money instance, or null.</returns>
        public override Money? TryParseMoney(string value)
        {
            decimal numeric = this.Converter.ToMoney(value);
            return Money.Create(numeric);
        }

        /// <summary>
        /// Parse the string representation into a Percentage instance.
        /// </summary>
        /// <param name="value">The string representation of a percentage value.</param>
        /// <returns>The Percentage instance, or null.</returns>
        public override Percentage? TryParsePercentage(string value)
        {
            decimal numeric = this.Converter.ToRate(value);
            return Percentage.Create(numeric);
        }

        /// <summary>
        /// Parse the string representation into a PhoneNumber instance.
        /// </summary>
        /// <param name="value">The string representation of a phone number value.</param>
        /// <returns>The PhoneNumber instance, or null.</returns>
        public override PhoneNumber? TryParsePhoneNumber(string value)
        {
            string parsed = this.Converter.ToPhoneNumFormat(value, this.Direction);
            return PhoneNumber.Create(parsed);
        }

        /// <summary>
        /// Parse the string representation into an UnzonedDate intstance.
        /// </summary>
        /// <param name="value">The string representation of a date value.</param>
        /// <returns>The UnzonedDate instance, or null.</returns>
        public override UnzonedDate? TryParseUnzonedDate(string value)
        {
            // ILosConvert does not have a method to parse a string to a DateTime.
            // Instead all such parsing is routed through the CDateTime class, which
            // takes into account the FormatTarget value.  Once we can switch to a 
            // better implementation of ILosConvert, we can can add a method for parsing
            // UnzonedDate directly (and other date time classes) to have better consistency.
            var cdate = CDateTime.Create(value, this.Converter.FormatTargetCurrent);
            if (!cdate.IsValid)
            {
                return null;
            }

            var dt = cdate.DateTimeForComputation;
            return UnzonedDate.Create(dt);
        }
    }
}
