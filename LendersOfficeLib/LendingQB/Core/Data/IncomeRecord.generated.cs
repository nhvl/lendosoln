﻿// <auto-generated />
namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    public partial class IncomeRecord : ITrackChanges, IPathResolvable, IPathableStringFormatterProvider
    {
        private IChangeTrackingDataContainer data = null;

        public IncomeRecord()
        {
            var fieldDictionary = new Dictionary<string, object>();
            fieldDictionary["IncomeType"] = null;
            fieldDictionary["MonthlyAmount"] = null;
            var baseContainer = new DictionaryContainer(fieldDictionary);
            this.data = new ChangeTrackingDataContainer(baseContainer);
        }

        public IncomeRecord(IncomeRecord other) : this()
        {
            this.IncomeType = other.IncomeType;
            this.MonthlyAmount = other.MonthlyAmount;
        }

        public IncomeRecord(IChangeTrackingDataContainer data)
        {
            this.data = data;
        }

		internal static IncomeRecord CopyAsIfUnmodified(IncomeRecord other)
		{
			var cleanTracking = new ChangeTrackingDataContainer(other.data);
			return new IncomeRecord(cleanTracking);
		}

        public IncomeType? IncomeType
        {
            get
            {
                return (IncomeType?)this.data[nameof(IncomeType)];
            }

            set
            {
                this.data[nameof(IncomeType)] = value;
            }
        }

        public Money? MonthlyAmount
        {
            get
            {
                return (Money?)this.data[nameof(MonthlyAmount)];
            }

            set
            {
                this.data[nameof(MonthlyAmount)] = value;
            }
        }

        public bool HasChanges { get { return this.data.HasChanges; } }

        public IEnumerable<KeyValuePair<string, object>> GetChanges()
        {
            return this.data.GetChanges();
        }

        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new ArgumentException($"IncomeRecord can only handle {nameof(DataPathBasicElement)}, but passed element of type {element.GetType()}");
            }

            switch (element.Name.ToLower())
            {
                case "incometype": return this.IncomeType;
                case "monthlyamount": return this.MonthlyAmount;
                default: throw new ArgumentException($"Invalid field id: {element.Name}.");
            }
        }

        IPathableStringFormatter IPathableStringFormatterProvider.GetStringFormatter(IFormatAsString formatter, IParseFromString parser)
        {
            return this.GetStringFormatter(formatter, parser);
        }

        public IIncomeRecordStringFormatter GetStringFormatter(IFormatAsString formatter, IParseFromString parser)
        {
            var factory = new StringFormatterFactory();
            return factory.Create(this, formatter, parser);
        }

        public struct StringFormatterFactory
        {
            public IIncomeRecordStringFormatter Create(IncomeRecord data, IFormatAsString formatter, IParseFromString parser)
            {
                return new StringFormatter(data, formatter, parser);
            }
        }

        private sealed class StringFormatter : IIncomeRecordStringFormatter
        {
            private IncomeRecord data;
            private IFormatAsString formatter;
            private IParseFromString parser;

            public StringFormatter(IncomeRecord data, IFormatAsString formatter, IParseFromString parser)
            {
                this.data = data;
                this.formatter = formatter == null ? new FormatAsStringBase() : formatter;
                this.parser = parser == null ? new ParseFromStringBase() : parser;
            }

            public string IncomeType
            {
                get { return this.formatter.FormatEnum(this.data.IncomeType); }
                set { this.data.IncomeType = this.parser.TryParseEnum<IncomeType>(value); }
            }

            public string MonthlyAmount
            {
                get { return this.formatter.Format(this.data.MonthlyAmount); }
                set { this.data.MonthlyAmount = this.parser.TryParseMoney(value); }
            }

            public object GetElement(IDataPathElement element)
            {
                if (!(element is DataPathBasicElement))
                {
                    throw new ArgumentException($"IncomeRecord.StringFormatter can only handle {nameof(DataPathBasicElement)}, but passed element of type {element.GetType()}");
                }

                switch (element.Name.ToLower())
                {
                    case "incometype": return this.IncomeType;
                    case "monthlyamount": return this.MonthlyAmount;
                    default: throw new ArgumentException($"Invalid field id: {element.Name}.");
                }
            }

            public void SetElement(IDataPathElement element, object value)
            {
                if (!(element is DataPathBasicElement))
                {
                    throw new ArgumentException($"IncomeRecord.StringFormatter can only handle {nameof(DataPathBasicElement)}, but passed element of type {element.GetType()}");
                }

                switch (element.Name.ToLower())
                {
                    case "incometype": this.IncomeType = ForceString(value); break;
                    case "monthlyamount": this.MonthlyAmount = ForceString(value); break;
                    default: throw new ArgumentException($"Invalid field id: {element.Name}.");
                }
            }

            private static string ForceString(object value)
            {
                string stringValue = value as string;
                if (value != null && stringValue == null)
                {
                    throw new ArgumentException("IncomeRecord.StringFormatter can only handle string values.");
                }

                return stringValue;
            }
        }
    }

    public interface IIncomeRecordStringFormatter : IPathableStringFormatter
    {
        string IncomeType { get; set; }
        string MonthlyAmount { get; set; }
    }
}

