﻿namespace LendingQB.Core.Data
{
    /// <summary>
    /// Provides functionality needed to convert from the type used by
    /// <see cref="IAppBase"/> to the semantic type used by an entity.
    /// </summary>
    /// <typeparam name="TApp">The app type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    internal interface IAppToEntityTypeConverter<TApp, TEntity>
    {
        /// <summary>
        /// Converts the app value to the entity value.
        /// </summary>
        /// <param name="appValue">The app value.</param>
        /// <returns>The entity value.</returns>
        TEntity ToEntityType(TApp appValue);
    }
}
