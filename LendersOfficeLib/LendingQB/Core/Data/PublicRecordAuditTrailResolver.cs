﻿namespace LendingQB.Core.Data
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Class that pulls changes from a public record entity and updates
    /// the audit trail.
    /// </summary>
    internal sealed class PublicRecordAuditTrailResolver : IPreEntitySaveHandler
    {
        /// <summary>
        /// Text to use in the audit trail to indicate a blank value.
        /// </summary>
        private const string BlankMarker = "[Blank]";

        /// <summary>
        /// The value to use when the display name for the interactive user is not populated.
        /// </summary>
        private const string NoUserDisplayName = "Unknown";

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicRecordAuditTrailResolver"/> class.
        /// </summary>
        /// <param name="publicRecord">The public record data.</param>
        /// <param name="userDisplayName">The display name of the interactive user, if any.</param>
        public PublicRecordAuditTrailResolver(PublicRecord publicRecord, string userDisplayName)
        {
            this.Data = publicRecord;
            this.UserDisplayName = string.IsNullOrEmpty(userDisplayName) ? NoUserDisplayName : userDisplayName;
        }

        /// <summary>
        /// Gets or sets the underlying public record data.
        /// </summary>
        private PublicRecord Data { get; set; }

        /// <summary>
        /// Gets or sets the display name for the interactive user.
        /// </summary>
        private string UserDisplayName { get; set; }

        /// <summary>
        /// Update the public record's audit trail with the changes.
        /// </summary>
        public void ResolveChanges()
        {
            var audit = this.Data.AuditTrail;
            var recentValues = this.ReconstructHistory(audit);

            var changes = this.Data.GetChanges();
            var changedValues = this.ResolveHistory(recentValues, changes);

            if (changedValues.Count > 0)
            {
                string timestamp = Tools.GetDateTimeNowString();

                var sb = new StringBuilder();
                foreach (var key in changedValues.Keys)
                {
                    sb.AppendLine(changedValues[key]);
                }

                if (audit == null)
                {
                    audit = new List<CPublicRecordAuditItem>();
                }

                var auditItem = new CPublicRecordAuditItem(this.UserDisplayName, timestamp, sb.ToString());
                audit.Add(auditItem);

                this.Data.AuditTrail = audit;
            }
        }

        /// <summary>
        /// Use the most recent values pulled from the audit history and the changes to the existing public record fields to 
        /// calculate the new items that need to be added to the audit history.
        /// </summary>
        /// <param name="recentValues">The most recent values pulled from the audit history.</param>
        /// <param name="changes">The changes that have been made to the existing public record fields.</param>
        /// <returns>The new items that need to be added to the audit history.</returns>
        private Dictionary<string, string> ResolveHistory(Dictionary<string, string> recentValues, IEnumerable<KeyValuePair<string, object>> changes)
        {
            var recents = new HashSet<string>();
            foreach (string key in recentValues.Keys)
            {
                recents.Add(key);
            }

            var data = new Dictionary<string, string>();

            foreach (var pair in changes)
            {
                if (pair.Key == "AuditTrail")
                {
                    continue;
                }

                string key = this.MapKey(pair.Key);
                string changedValue = pair.Value.ToString();

                if (recentValues.ContainsKey(key))
                {
                    recents.Remove(key);

                    string recentValue = recentValues[key];
                    if (changedValue != recentValue)
                    {
                        string description = this.FormulateDescription(key, recentValue, changedValue);
                        data[key] = description;
                    }
                }
                else
                {
                    string description = this.FormulateDescription(key, null, changedValue);
                    data[key] = description;
                }
            }

            foreach (string key in recents)
            {
                string recentValue = recentValues[key];
                string description = this.FormulateDescription(key, recentValue, null);
                data[key] = description;
            }

            return data;
        }

        /// <summary>
        /// Formulate the description string for the audit history.
        /// </summary>
        /// <param name="key">The name of the changed field as it appears in the audit history.</param>
        /// <param name="oldValue">The previous value for the public record field.</param>
        /// <param name="newValue">The changed value for the public record field.</param>
        /// <returns>The description string for the audit history.</returns>
        private string FormulateDescription(string key, string oldValue, string newValue)
        {
            if (string.IsNullOrEmpty(oldValue))
            {
                oldValue = BlankMarker;
            }

            if (string.IsNullOrEmpty(newValue))
            {
                newValue = BlankMarker;
            }

            return string.Format("{0}: {1} -> {2}", key, oldValue, newValue);
        }

        /// <summary>
        /// Map property names to the corresponding names that appear in the audit trail.
        /// </summary>
        /// <param name="propertyName">The name of the public record property.</param>
        /// <returns>The name of the property that appears in the audit trail.</returns>
        private string MapKey(string propertyName)
        {
            switch (propertyName)
            {
                case "BankruptcyLiabilitiesAmount":
                    return "Liabilities Amount";
                case "BkFileDate":
                    return "File Date";
                case "CourtName":
                    return "Court Name";
                case "DispositionDate":
                    return "Disposition Date";
                case "DispositionType":
                    return "Disposition";
                case "IdFromCreditReport":
                    return "IdFromCreditReport";
                case "IncludeInPricing":
                    return "IncludeInPricing";
                case "LastEffectiveDate":
                    return "Last Effective Date";
                case "ReportedDate":
                    return "Reported Date";
                case "Owner":
                    return "Owner";
                case "Type":
                    return "Type";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unrecognized value - Public Record audit trail contains the unrecognized propert name " + propertyName == null ? "NULL" : propertyName);
            }
        }

        /// <summary>
        /// Use the current audit history to reconstruct the most recent value of each of the fields.
        /// </summary>
        /// <remarks>
        /// There are two reasons that the actual initialized values aren't used:
        /// i) They are not exposed publicly so we cannot use them without changing their exposure.
        /// ii) If there is a gap in the audit trail it will be less confusing to fill in the gap based on what 
        ///     is in the audit trail than the changes from the out-of-sync initialized data.
        /// </remarks>
        /// <param name="audit">The most recently loaded audit trail.</param>
        /// <returns>Condensation of the audit history that only retains the most recent values.</returns>
        private Dictionary<string, string> ReconstructHistory(List<CPublicRecordAuditItem> audit)
        {
            var data = new Dictionary<string, string>();

            if (audit != null)
            {
                foreach (var item in audit)
                {
                    using (var reader = new StringReader(item.Description))
                    {
                        string line = reader.ReadLine();
                        while (line != null)
                        {
                            int colonIndex = line.IndexOf(':');
                            if (colonIndex > 0)
                            {
                                string key = line.Substring(0, colonIndex);

                                int arrowIndex = line.IndexOf(" -> ", colonIndex);
                                if (arrowIndex > colonIndex)
                                {
                                    string value = line.Substring(arrowIndex + 4).Trim();
                                    if (value == BlankMarker)
                                    {
                                        value = string.Empty;
                                    }

                                    data[key] = value;
                                }
                            }

                            line = reader.ReadLine();
                        }
                    }
                }
            }

            return data;
        }
    }
}
