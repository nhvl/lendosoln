﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory class for creating implementations interfaces involved in formatting/parsing entity data.
    /// </summary>
    public struct FormatEntityFactory
    {
        /// <summary>
        /// Create an implementation of the IFormatAsString interface.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        /// <returns>An implementation of the IFormatAsString interface.</returns>
        public IFormatAsString CreateStringFormatter(FormatTarget target, FormatDirection direction)
        {
            return new FormatEntityAsString(target, direction);
        }

        /// <summary>
        /// Create an implementation of the IParseFromString interface.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        /// <returns>An implementation of the IParseFromString interface.</returns>
        public IParseFromString CreateStringParser(FormatTarget target, FormatDirection direction)
        {
            return new ParseEntityFromString(target, direction);
        }

        /// <summary>
        /// Create an implementation of the IFormatAsString interface used for direct conversion from
        /// semantic types to string types for legacy application and consumer properties.
        /// </summary>
        /// <returns>Interface to use for formatting semantic types as strings.</returns>
        public IFormatAsString CreateLegacyStringFormatter()
        {
            return new FormatLegacyAsString();
        }

        /// <summary>
        /// Create an implementation of the IFormatAsNumeric interface used for direct conversion from
        /// semantic types to numeric types for legacy application and consumer properties.
        /// </summary>
        /// <returns>Interface used for formatting semantic types as numeric values.</returns>
        public IFormatAsNumeric CreateLegacyNumericFormatter()
        {
            return new FormatAsNumericBase();
        }

        /// <summary>
        /// Create an implementation of the IParseFromString interface used for direct conversion to
        /// semantic types from string types for legacy application and consumer properties.
        /// </summary>
        /// <returns>Interface used for parsing strings into semantic types.</returns>
        public IParseFromString CreateLegacyStringParser()
        {
            return new ParseLegacyFromString();
        }
    }
}
