﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using DataAccess.PathDispatch;
    using LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// String formatter for an income record collection.
    /// </summary>
    public class IncomeRecordCollectionStringFormatter : IPathResolvableCollection, IPathableNestedStringFormatter<IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncomeRecordCollectionStringFormatter"/> class.
        /// </summary>
        /// <param name="instance">The underlying instance this string formatter wraps.</param>
        /// <param name="formatter">The formatter to apply to output values.</param>
        /// <param name="parser">The parser to convert input values.</param>
        public IncomeRecordCollectionStringFormatter(IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> instance, IFormatAsString formatter, IParseFromString parser)
        {
            this.Instance = instance;
            this.Formatter = formatter;
            this.Parser = parser;
        }

        /// <summary>
        /// Gets the LQB collection that is being wrapped.
        /// </summary>
        public IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> Instance { get; }

        /// <summary>
        /// Gets the string formatter.
        /// </summary>
        public IFormatAsString Formatter { get; }

        /// <summary>
        /// Gets the string parser.
        /// </summary>
        public IParseFromString Parser { get; }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Instance.GetAllKeys();
        }

        /// <summary>
        /// Gets the string formatter for the income record at the specified path.
        /// </summary>
        /// <param name="element">The path element to retrieve.</param>
        /// <returns>The string formatter for the income record at the specified path.</returns>
        public object GetElement(IDataPathElement element)
        {
            var record = (IncomeRecord)this.Instance.GetElement(element);
            return record.GetStringFormatter(this.Formatter, this.Parser);
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="path">The path to not support.</param>
        /// <param name="value">The value to not set.</param>
        /// <exception cref="NotSupportedException">Always thrown.</exception>
        public void SetElement(IDataPathElement path, object value)
        {
            throw new NotSupportedException();
        }
    }
}
