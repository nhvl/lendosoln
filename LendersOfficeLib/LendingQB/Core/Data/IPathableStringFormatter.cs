﻿namespace LendingQB.Core.Data
{
    using DataAccess.PathDispatch;

    /// <summary>
    /// An interface for entity string formatters.
    /// </summary>
    public interface IPathableStringFormatter : IPathResolvable, IPathSettable
    {
    }

    /// <summary>
    /// A marker interface to indicate <see cref="IPathableNestedStringFormatter{T}"/>.
    /// Types should not implement this directly, and should instead use the generic version.
    /// </summary>
    public interface IPathableNestedStringFormatter : IPathableStringFormatter
    {
    }

    /// <summary>
    /// An interface for nested entities on top of instances of <see cref="IPathableStringFormatter"/>.
    /// </summary>
    /// <typeparam name="T">The type of the underlying instance.</typeparam>
    /// <remarks>
    /// This is necessary particularly so that we have a way to get at the underlying value that's being modified via
    /// the string formatter, i.e., so we have a value to set on the actual object in the setter using this.
    /// </remarks>
    public interface IPathableNestedStringFormatter<T> : IPathableNestedStringFormatter
    {
        /// <summary>
        /// Gets the underlying instance this string formatter wraps.
        /// </summary>
        T Instance { get; }
    }
}
