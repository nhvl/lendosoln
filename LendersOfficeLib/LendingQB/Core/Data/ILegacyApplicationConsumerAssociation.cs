﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// An association between a legacy application and a consumer.
    /// </summary>
    /// <remarks>This should be modified when the association is no longer based on CAppData.</remarks>
    public interface ILegacyApplicationConsumerAssociation
    {
        /// <summary>
        /// Gets the legacy application id.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> LegacyApplicationId { get; }

        /// <summary>
        /// Gets the consumer id.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId { get; }

        /// <summary>
        /// Gets a value indicating whether the associated consumer is the primary borrower for the legacy application.
        /// </summary>
        bool IsPrimary { get; }
    }
}
