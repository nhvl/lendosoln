﻿namespace LendingQB.Core.Data
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface for entities to implement that provides access to a string formatter.
    /// </summary>
    public interface IPathableStringFormatterProvider
    {
        /// <summary>
        /// Gets a string formatter for the entity.
        /// </summary>
        /// <param name="formatter">The formatter used when converting entity values to strings.</param>
        /// <param name="parser">The parser to use when parsing incoming string data to entity values.</param>
        /// <returns>A string formatter for the entity.</returns>
        IPathableStringFormatter GetStringFormatter(IFormatAsString formatter, IParseFromString parser);
    }
}
