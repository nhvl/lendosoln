﻿namespace LendingQB.Core.Data
{
    using DataAccess;

    /// <summary>
    /// A defaults provider for the liability entity.
    /// </summary>
    internal class LiabilityDefaultsProvider : ILiabilityDefaultsProvider
    {
        /// <summary>
        /// The loan data.
        /// </summary>
        private ILoanLqbCollectionContainerLoanDataProvider loanData;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityDefaultsProvider"/> class.
        /// </summary>
        /// <param name="loanData">The loan data to use when calculating the defaults.</param>
        public LiabilityDefaultsProvider(ILoanLqbCollectionContainerLoanDataProvider loanData)
        {
            this.loanData = loanData;
        }

        /// <summary>
        /// The default value for the payoff timing.
        /// </summary>
        public PayoffTiming? PayoffTiming => this.loanData.sIsRefinancing ? LendingQB.Core.Data.PayoffTiming.AtClosing : LendingQB.Core.Data.PayoffTiming.BeforeClosing;

        /// <summary>
        /// Gets the list of names needed to be loaded with the loan in order for this provider to function correctly.
        /// </summary>
        /// <remarks>
        /// As long as this class is simple, it is easier to maintain this list manually.
        /// </remarks>
        public string[] LoanDependencies
        {
            get
            {
                return new string[] { "sIsRefinancing" };
            }
        }
    }
}
