﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defaults provider for employment records.
    /// </summary>
    public interface IEmploymentRecordDefaultsProvider
    {
        /// <summary>
        /// Gets the list of names needed to be loaded with the loan in order for this provider to function correctly.
        /// </summary>
        string[] LoanDependencies { get; }

        /// <summary>
        /// Gets the business phone of the given consumer.
        /// </summary>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The business phone of the consumer.</returns>
        PhoneNumber? GetBusPhone(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId);
    }
}
