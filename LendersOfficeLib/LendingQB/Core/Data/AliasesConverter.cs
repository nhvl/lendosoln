﻿namespace LendingQB.Core.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides the functionality to convert borrower aliases between the types
    /// used by <see cref="IAppBase"/> and <see cref="IConsumer"/>.
    /// </summary>
    internal class AliasesConverter : 
        IAppToEntityTypeConverter<IEnumerable<string>, IEnumerable<PersonName>>,
        IEntityToAppTypeConverter<IEnumerable<PersonName>, IEnumerable<string>>
    {
        /// <summary>
        /// Converts aliases in string form to <see cref="PersonName"/>s.
        /// </summary>
        /// <param name="legacyValue">The aliases strings.</param>
        /// <returns>An enumerable of <see cref="PersonName"/>.</returns>
        public IEnumerable<PersonName> ToEntityType(IEnumerable<string> legacyValue)
        {
            if (legacyValue == null)
            {
                return null;
            }

            return legacyValue.Select(name => PersonName.Create(name).Value);
        }

        /// <summary>
        /// Converts aliases in <see cref="PersonName"/> form to strings.
        /// </summary>
        /// <param name="newValue">The semantic typed aliases.</param>
        /// <returns>An enumerable of strings.</returns>
        public IEnumerable<string> ToAppBaseType(IEnumerable<PersonName> newValue)
        {
            return newValue.Select(name => name.ToString());
        }
    }
}
