﻿namespace LendingQB.Core.Data
{
    using System;
    using DataAccess;
    using LendersOffice;
    using LendersOffice.Common;
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Implement the methods of IEmploymentRecord that have to be done manually.
    /// </summary>
    public partial class EmploymentRecord : IAddEntityHandler, IRemoveEntityHandler
    {
        /// <summary>
        /// Gets the duration of employment in units of years.
        /// </summary>
        public Quantity<UnitType.Year>? EmploymentLen
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcYearDuration(this.EmploymentStartDate, this.EmploymentEndDate);
            }
        }

        /// <summary>
        /// Gets the number of remaining whole months after subtracting the whole years from the duration of employment.
        /// </summary>
        public Count<UnitType.Month>? EmploymentMonths
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcRemainderWholeMonths(this.EmploymentStartDate, this.EmploymentEndDate);
            }
        }

        /// <summary>
        /// Gets the number of full months from the duration of employment.
        /// </summary>
        public Count<UnitType.Month>? EmploymentTotalMonths
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcWholeMonths(this.EmploymentStartDate, this.EmploymentEndDate);
            }
        }

        /// <summary>
        /// Gets the number of whole years from the duration of employment.
        /// </summary>
        /// <value>The number of years for this employment.</value>
        public Count<UnitType.Year>? EmploymentYears
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcWholeYears(this.EmploymentStartDate, this.EmploymentEndDate);
            }
        }

        /// <summary>
        /// Gets the duration of time spent in the current profession in units of years.
        /// </summary>
        public Quantity<UnitType.Year>? ProfessionLen
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcYearDuration(this.ProfessionStartDate, null);
            }
        }

        /// <summary>
        /// Gets the number of remaining whole months after subtracting the whole years from the duration of the current profession.
        /// </summary>
        public Count<UnitType.Month>? ProfessionMonths
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcRemainderWholeMonths(this.ProfessionStartDate, null);
            }
        }

        /// <summary>
        /// Gets the number of full months from the duration of the current profession.
        /// </summary>
        public Count<UnitType.Month>? ProfessionTotalMonths
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcWholeMonths(this.ProfessionStartDate, null);
            }
        }

        /// <summary>
        /// Gets the number of whole years from the duration of the current profession.
        /// </summary>
        public Count<UnitType.Year>? ProfessionYears
        {
            get
            {
                return CalculatedFields.EmploymentRecord.CalcWholeYears(this.ProfessionStartDate, null);
            }
        }

        /// <summary>
        /// Gets a value determining whether the Employer has a special relationship with a party involved in the loan transaction.
        /// </summary>
        public bool? IsSpecialBorrowerEmployerRelationship
        {
            get
            {
                if (this.IsSelfEmployed == null)
                {
                    return null;
                }
                else if (this.IsSelfEmployed.Value)
                {
                    return this.IsSpecialBorrowerEmployerRelationshipData;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the percentage type that the borrower owned of the self-employed job.
        /// </summary>
        public SelfOwnershipShare? SelfOwnershipShareT
        {
            get
            {
                if (this.IsSelfEmployed == null)
                {
                    return null;
                }
                else if (this.IsSelfEmployed.Value)
                {
                    return this.SelfOwnershipShareTData;
                }

                return SelfOwnershipShare.Blank;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this VOE has a signature.
        /// </summary>
        public bool? VerifHasSignature => this.VerifSigningEmployeeId.HasValue && this.VerifSignatureImgId.HasValue;

        /// <summary>
        /// Sets the consumer who is associated with the employment record.
        /// </summary>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="legacyAppId">The legacy application id the consumer is associated with.</param>
        public void SetConsumer(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId)
        {
            this.ConsumerId = consumerId;
            this.LegacyAppId = legacyAppId;
        }

        /// <summary>
        /// Erase the data for the verification signature.
        /// </summary>
        public void ClearVerifSignature()
        {
            this.VerifSignatureImgId = null;
            this.VerifSigningEmployeeId = null;
        }

        /// <summary>
        /// Set the two data points related to signing a VOE.
        /// </summary>
        /// <param name="signer">The employeeId of the person that signed the VOE.</param>
        /// <param name="signature">The file identifier for the image file containing the signature.</param>
        public void SetVerifSignature(
            DataObjectIdentifier<DataObjectKind.Employee, Guid> signer,
            DataObjectIdentifier<DataObjectKind.Signature, Guid> signature)
        {
            this.VerifSigningEmployeeId = signer;
            this.VerifSignatureImgId = signature;
        }

        /// <summary>
        /// Handles an AddEntity command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parser">The parser to use if field values have been specified for the new entity.</param>
        /// <returns>The id of the newly added entity.</returns>
        Guid IAddEntityHandler.HandleAdd(AddEntity command, IParseFromString parser)
        {
            switch (command.CollectionName)
            {
                case nameof(IncomeRecords):
                    var incomeRecord = new IncomeRecord();

                    if (command.Fields != null)
                    {
                        LendersOffice.ObjLib.LqbDataService.LqbSetRequestResolver.ResolveSetRequest(command.Fields, incomeRecord, parser);
                    }

                    return this.IncomeRecords.Add(incomeRecord).Value;
                default:
                    throw CBaseException.GenericException("Unsupported collection name: " + command.CollectionName);
            }
        }

        /// <summary>
        /// Removes the entity specified by the command.
        /// </summary>
        /// <param name="command">The remove command.</param>
        void IRemoveEntityHandler.HandleRemove(RemoveEntity command)
        {
            switch (command.CollectionName)
            {
                case nameof(IncomeRecords):
                    this.IncomeRecords.Remove(command.Id.ToIdentifier<DataObjectKind.IncomeRecord>());
                    break;
                default:
                    throw CBaseException.GenericException("Unsupported collection name: " + command.CollectionName);
            }
        }

        /// <summary>
        /// Gets a blank collection for the income records.
        /// </summary>
        /// <returns>A blank collection for the income records.</returns>
        private IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> GetDefaultIncomeRecordsCollection()
        {
            return OrderedLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(
                Name.Create(nameof(IncomeRecords)).Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeRecord>());
        }

        /// <summary>
        /// Copies the income records from the given collection.
        /// </summary>
        /// <param name="sourceIncomeRecords">The income records to duplicate.</param>
        private void CopyIncomeRecords(IReadOnlyLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> sourceIncomeRecords)
        {
            foreach (var sourceIncomeRecord in sourceIncomeRecords.Values)
            {
                var copy = new IncomeRecord(sourceIncomeRecord);
                this.IncomeRecords.Add(copy);
            }
        }
    }
}
