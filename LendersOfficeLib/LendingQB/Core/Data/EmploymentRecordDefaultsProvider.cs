﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defaults provider for employment records.
    /// </summary>
    public class EmploymentRecordDefaultsProvider : IEmploymentRecordDefaultsProvider
    {
        /// <summary>
        /// The loan data.
        /// </summary>
        private DataAccess.ILoanLqbCollectionContainerLoanDataProvider loanData;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmploymentRecordDefaultsProvider"/> class.
        /// </summary>
        /// <param name="loanData">The loan data to use when calculating the defaults.</param>
        public EmploymentRecordDefaultsProvider(DataAccess.ILoanLqbCollectionContainerLoanDataProvider loanData)
        {
            this.loanData = loanData;
        }

        /// <summary>
        /// Gets the list of names needed to be loaded with the loan in order for this provider to function correctly.
        /// </summary>
        public string[] LoanDependencies
        {
            get
            {
                return new string[] { nameof(DataAccess.CPageBase.Consumers), nameof(IConsumer.BusPhone) };
            }
        }

        /// <summary>
        /// Gets the business phone of the given consumer.
        /// </summary>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The business phone of the consumer.</returns>
        public PhoneNumber? GetBusPhone(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.loanData.Consumers[consumerId].BusPhone;
        }
    }
}
