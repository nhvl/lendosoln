﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains summary totals for a set of real properties.
    /// </summary>
    public class RealPropertyTotals
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RealPropertyTotals"/> class.
        /// </summary>
        /// <param name="marketValue">The total market value where the real property status is not sold.</param>
        /// <param name="mortgageAmount">The total mortgage amount where the real property status is not sold.</param>
        /// <param name="rentalNetRentalIncome">The total net rental income for real properties in the Rental status.</param>
        /// <param name="retainedNetRentalIncome">The total net rental income for the real properties where the status is Retained.</param>
        internal RealPropertyTotals(Money marketValue, Money mortgageAmount, Money rentalNetRentalIncome, Money retainedNetRentalIncome)
        {
            this.MarketValue = marketValue;
            this.MortgageAmount = mortgageAmount;
            this.RentalNetRentalIncome = rentalNetRentalIncome;
            this.RetainedNetRentalIncome = retainedNetRentalIncome;
        }

        /// <summary>
        /// Gets the total market value where the real property status is not sold.
        /// </summary>
        public Money MarketValue { get; }

        /// <summary>
        /// Gets the total mortgage amount where the real property status is not sold.
        /// </summary>
        public Money MortgageAmount { get; }

        /// <summary>
        /// Gets the total net rental income for real properties in the Rental status.
        /// </summary>
        public Money RentalNetRentalIncome { get; }

        /// <summary>
        /// Gets the total net rental income for the real properties where the status is Retained.
        /// </summary>
        public Money RetainedNetRentalIncome { get; }

        /// <summary>
        /// Creates real property totals based on the given real properties.
        /// </summary>
        /// <param name="realProperties">The real properties to total up.</param>
        /// <returns>The resulting totals.</returns>
        public static RealPropertyTotals Create(IEnumerable<RealProperty> realProperties)
        {
            if (realProperties == null)
            {
                throw new ArgumentNullException();
            }

            var marketValue = Money.Zero;
            var mortgageAmount = Money.Zero;
            var rentalNetRentalIncome = Money.Zero;
            var retainedNetRentalIncome = Money.Zero;

            foreach (var realProperty in realProperties)
            {
                if (realProperty.Status != DataAccess.E_ReoStatusT.Sale)
                {
                    marketValue += realProperty.MarketValue ?? Money.Zero;
                    mortgageAmount += realProperty.MtgAmt ?? Money.Zero;
                }

                if (realProperty.Status == DataAccess.E_ReoStatusT.Rental)
                {
                    rentalNetRentalIncome += realProperty.NetRentInc ?? Money.Zero;
                }
                else if (realProperty.Status == DataAccess.E_ReoStatusT.Residence)
                {
                    retainedNetRentalIncome += realProperty.NetRentInc ?? Money.Zero;
                }
            }

            return new RealPropertyTotals(marketValue, mortgageAmount, rentalNetRentalIncome, retainedNetRentalIncome);
        }
    }
}
