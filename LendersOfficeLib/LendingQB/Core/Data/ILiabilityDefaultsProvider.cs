﻿namespace LendingQB.Core.Data
{
    /// <summary>
    /// A defaults provider for the liability entity.
    /// </summary>
    public interface ILiabilityDefaultsProvider
    {
        /// <summary>
        /// Gets the default value for the payoff timing.
        /// </summary>
        PayoffTiming? PayoffTiming { get; }

        /// <summary>
        /// Gets the list of names needed to be loaded with the loan in order for this provider to function correctly.
        /// </summary>
        string[] LoanDependencies { get; }
    }
}
