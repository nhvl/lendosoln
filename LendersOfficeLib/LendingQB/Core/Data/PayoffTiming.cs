﻿namespace LendingQB.Core.Data
{
    /// <summary>
    /// Indicates the payoff timing for a liability.
    /// </summary>
    public enum PayoffTiming
    {
        /// <summary>
        /// The blank option.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// The liability will be paid off before closing.
        /// </summary>
        BeforeClosing = 1,

        /// <summary>
        /// The liability will be paid off at closing.
        /// </summary>
        AtClosing = 2
    }
}
