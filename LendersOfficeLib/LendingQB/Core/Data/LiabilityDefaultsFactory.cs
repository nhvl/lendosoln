﻿namespace LendingQB.Core.Data
{
    using DataAccess;

    /// <summary>
    /// Factory for creation implementations of the ILiabilityDefaultsProvider interface.
    /// </summary>
    public struct LiabilityDefaultsFactory
    {
        /// <summary>
        /// Create an implementation of the ILiabilityDefaultsProvider interface.
        /// </summary>
        /// <param name="loanData">The loan data to use when calculating the defaults.</param>
        /// <returns>An implementation of the ILiabilityDefaultsProvider interface.</returns>
        public ILiabilityDefaultsProvider Create(ILoanLqbCollectionContainerLoanDataProvider loanData)
        {
            return new LiabilityDefaultsProvider(loanData);
        }
    }
}
