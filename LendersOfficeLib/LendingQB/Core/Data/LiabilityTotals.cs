﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains summary totals for a set of liabilities.
    /// </summary>
    public class LiabilityTotals
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityTotals"/> class.
        /// </summary>
        /// <param name="balance">The REO subtotal.</param>
        /// <param name="payment">The liquid assets subtotal.</param>
        /// <param name="paidOff">The illiquid assets subtotal.</param>
        internal LiabilityTotals(Money balance, Money payment, Money paidOff)
        {
            this.Balance = balance;
            this.Payment = payment;
            this.PaidOff = paidOff;
        }

        /// <summary>
        /// Gets total balance.
        /// </summary>
        public Money Balance { get; }

        /// <summary>
        /// Gets the total payment.
        /// </summary>
        public Money Payment { get; }

        /// <summary>
        /// Gets the total amount that will be paid off.
        /// </summary>
        public Money PaidOff { get; }

        /// <summary>
        /// Creates liability totals from the given liabilities.
        /// </summary>
        /// <param name="liabilities">The liabilities.</param>
        /// <returns>The liability totals.</returns>
        public static LiabilityTotals Create(IEnumerable<Liability> liabilities)
        {
            if (liabilities == null)
            {
                throw new ArgumentNullException();
            }

            var balance = Money.Zero;
            var payment = Money.Zero;
            var paidOff = Money.Zero;

            foreach (var liability in liabilities)
            {
                balance += liability.Bal ?? Money.Zero;

                if (liability.UsedInRatio)
                {
                    payment += liability.Pmt ?? Money.Zero;
                }

                if (liability.WillBePdOff.GetValueOrDefault(false) && liability.PayoffTiming == PayoffTiming.AtClosing)
                {
                    paidOff += liability.PayoffAmt ?? Money.Zero;
                }
            }

            return new LiabilityTotals(balance, payment, paidOff);
        }
    }
}
