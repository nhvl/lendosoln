﻿namespace LendingQB.Core.Data
{
    using LendersOffice;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using EmpVerifId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Employee, System.Guid>;
    using SigVerifId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Signature, System.Guid>;

    /// <summary>
    /// Represents a liability.
    /// </summary>
    public partial class Liability
    {
        /// <summary>
        /// Gets a value indicating whether the liability is used in the DTI.
        /// </summary>
        public bool UsedInRatio
        {
            get
            {
                if (this.DebtType.EqualsOneOf(DataAccess.E_DebtT.Mortgage, DataAccess.E_DebtT.Heloc))
                {
                    return false;
                }

                return this.UsedInRatioData ?? true;
            }
        }

        /// <summary>
        /// Gets the payoff amount.
        /// </summary>
        public Money? PayoffAmt
        {
            get
            {
                if (this.PayoffAmtLocked == null)
                {
                    return null;
                }
                else if (this.PayoffAmtLocked.Value)
                {
                    return this.PayoffAmtData;
                }

                return this.Bal;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the payoff timing is locked.
        /// </summary>
        public bool? PayoffTimingLocked
        {
            get
            {
                if (this.WillBePdOff == null)
                {
                    return null;
                }
                else if (this.WillBePdOff.Value)
                {
                    return this.PayoffTimingLockedData;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Liability requires a verification of land.
        /// </summary>
        public bool? RequiresVol
        {
            get
            {
                if (!this.DebtType.HasValue)
                {
                    return null;
                }

                return this.DebtType == DataAccess.E_DebtT.Installment || this.DebtType == DataAccess.E_DebtT.Revolving;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the liability requires a verification of mortgage.
        /// </summary>
        public bool? RequiresVom
        {
            get
            {
                if (!this.DebtType.HasValue)
                {
                    return null;
                }

                return this.DebtType == DataAccess.E_DebtT.Mortgage || this.DebtType == DataAccess.E_DebtT.Heloc;
            }
        }

        /// <summary>
        /// Gets the calculated payoff timing.
        /// </summary>
        public PayoffTiming? PayoffTiming
        {
            get
            {
                return CalculatedFields.Liability.PayoffTiming(
                    this.WillBePdOff,
                    this.PayoffTimingLocked,
                    this.PayoffTimingData,
                    this.defaultsProvider);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this liability has a signature.
        /// </summary>
        public bool? VerifHasSignature => this.VerifSigningEmployeeId.HasValue && this.VerifSignatureImgId.HasValue;

        /// <summary>
        /// Clears the signature-related data points.
        /// </summary>
        public void ClearVerifSignature()
        {
            this.VerifSigningEmployeeId = null;
            this.VerifSignatureImgId = null;
        }

        /// <summary>
        /// Set the two data points related to signing a liability.
        /// </summary>
        /// <param name="signer">The employeeId of the person that signed the liability.</param>
        /// <param name="signature">The file identifier for the image file containing the signature.</param>
        public void SetVerifSignature(EmpVerifId signer, SigVerifId signature)
        {
            this.VerifSigningEmployeeId = signer;
            this.VerifSignatureImgId = signature;
        }
    }
}
