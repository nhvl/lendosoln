﻿namespace LendingQB.Core.Data
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a real property, the new data layer version of the REO record.
    /// </summary>
    public partial class RealProperty
    {
        /// <summary>
        /// Gets the market value minus the mortgage amount.
        /// </summary>
        public Money? NetValue
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.NetValue(this.MarketValue, this.MtgAmt);
            }
        }

        /// <summary>
        /// Gets the net rent income.
        /// </summary>
        public Money? NetRentInc
        {
            get
            {
                if (this.NetRentIncLocked == null)
                {
                    return null;
                }
                else if (this.NetRentIncLocked.Value)
                {
                    return this.NetRentIncData;
                }

                return this.NetRentIncCalc;
            }
        }

        /// <summary>
        /// Gets the calculated value for the net rent income regardless of the lock bit.
        /// </summary>
        public Money? NetRentIncCalc
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.NetRentInc(
                    this.Status,
                    this.IsForceCalcNetRentalInc,
                    this.OccR,
                    this.GrossRentInc,
                    this.HExp,
                    this.MtgPmt);
            }
        }
    }
}
