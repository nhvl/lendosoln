﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Parse legacy application and consumer properties to semantic types.
    /// </summary>
    internal class ParseLegacyFromString : ParseFromStringBase
    {
        /// <summary>
        /// Attempt to parse the input string into a BooleanKleene instance.
        /// </summary>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>A BooleanKleene, or null.</returns>
        public override BooleanKleene? TryParseBooleanKleene(string value)
        {
            switch (value)
            {
                case "Y":
                    return BooleanKleene.True;
                case "N":
                    return BooleanKleene.False;
                case "":
                    return BooleanKleene.Indeterminant;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unrecognized value - attempting to create a BooleanKleene instance from the value " + value == null ? "NULL" : value);
            }
        }

        /// <summary>
        /// Parse the input string into a BooleanKleene.
        /// </summary>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>The BooleanKleene, or the default value.</returns>
        public override BooleanKleene ParseBooleanKleene(string value, BooleanKleene defaultValue)
        {
            if (value == null)
            {
                return defaultValue;
            }

            return this.TryParseBooleanKleene(value).Value;
        }
    }
}
