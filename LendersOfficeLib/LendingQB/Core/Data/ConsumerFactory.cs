﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating implemenations of the IConsumer interface.
    /// </summary>
    public struct ConsumerFactory
    {
        /// <summary>
        /// Create an implementation of the IConsumer interface.
        /// </summary>
        /// <param name="consumer">A POD consumer implemenation.</param>
        /// <param name="workflowValidator">The workflow validator.</param>
        /// <returns>An implemenation of the IConsumer interface.</returns>
        public IConsumer Create(IConsumer consumer, ILoanFileFieldValidator workflowValidator)
        {
            return new AppDataConsumer(consumer as AppDataConsumer, workflowValidator);
        }

        /// <summary>
        /// Create an implementation of the IConsumer interface.
        /// </summary>
        /// <param name="app">Provider of legacy application and consumer data.</param>
        /// <param name="borrower">Value indication whether this consumer is the borrower or coborrower for it's containing legacy application.</param>
        /// <param name="workflowValidator">The workflow validator.</param>
        /// <param name="legacyToSemanticTypeParser">Parser for field data from the legacy provider, formats the data as semantic types.</param>
        /// <param name="semanticToLegacyNumericFormatter">Formatter for converting semantic types to legacy numeric types.</param>
        /// <param name="semanticToLegacyStringFormatter">Formatter for converting semantic types to legacy string types.</param>
        /// <param name="aliasesConverter">Converter that handles alias information.</param>
        /// <returns>An implementation of the IConsumer interface.</returns>
        internal IConsumer Create(
            IAppDataConsumerAppDataProvider app,
            E_BorrowerModeT borrower,
            ILoanFileFieldValidator workflowValidator,
            IParseFromString legacyToSemanticTypeParser,
            IFormatAsNumeric semanticToLegacyNumericFormatter,
            IFormatAsString semanticToLegacyStringFormatter,
            AliasesConverter aliasesConverter)
        {
            return new AppDataConsumer(app, borrower, workflowValidator, legacyToSemanticTypeParser, semanticToLegacyNumericFormatter, semanticToLegacyStringFormatter, aliasesConverter);
        }
    }
}
