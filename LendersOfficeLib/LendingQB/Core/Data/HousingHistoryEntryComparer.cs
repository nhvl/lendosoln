﻿namespace LendingQB.Core.Data
{
    using System.Collections.Generic;

    /// <summary>
    /// A comparer for sorting housing history items.
    /// </summary>
    public class HousingHistoryEntryComparer : IComparer<HousingHistoryEntry>
    {
        /// <summary>
        /// Constant for -1.
        /// </summary>
        private const int NegativeOne = -1;

        /// <summary>
        /// Constant for 0.
        /// </summary>
        private const int Zero = 0;

        /// <summary>
        /// Constant for 1.
        /// </summary>
        private const int One = 1;

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than,
        /// equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first value to compare.</param>
        /// <param name="y">The second value to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of x and y.
        /// A negative value if x &lt; y, a positive value if x &gt; y, and zero if x == y.
        /// </returns>
        public int Compare(HousingHistoryEntry x, HousingHistoryEntry y)
        {
            if (x.IsPresentAddress.GetValueOrDefault() && !y.IsPresentAddress.GetValueOrDefault())
            {
                return NegativeOne;
            }
            else if (!x.IsPresentAddress.GetValueOrDefault() && y.IsPresentAddress.GetValueOrDefault())
            {
                return One;
            }
            else if (x.IsPresentAddress.GetValueOrDefault() && y.IsPresentAddress.GetValueOrDefault())
            {
                return Zero;
            }

            if (x.EndDate.HasValue && !y.EndDate.HasValue)
            {
                return NegativeOne;
            }
            else if (!x.EndDate.HasValue && y.EndDate.HasValue)
            {
                return One;
            }
            else if (x.EndDate.HasValue && y.EndDate.HasValue)
            {
                var endDateX = x.EndDate.Value.MinimumDate();
                var endDateY = y.EndDate.Value.MinimumDate();

                // We want more recent dates to come first, so we multiply
                // the date comparison by -1.
                return -1 * endDateX.CompareTo(endDateY);
            }
            else
            {
                return Zero;
            }
        }
    }
}
