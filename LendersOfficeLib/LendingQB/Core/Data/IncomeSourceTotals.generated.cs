﻿// Generated via GenerateRecordClass.ttinclude
namespace LendingQB.Core.Data
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains summary totals for a set of income sources.
    /// </summary>
    public partial class IncomeSourceTotals
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncomeSourceTotals"/> class.
        /// </summary>
        /// <param name="monthlyAmount">The total monthly income amount for the set of income sources.</param>
        public IncomeSourceTotals(Money monthlyAmount)
        {
            this.MonthlyAmount = monthlyAmount;
        }

        /// <summary>
        /// Gets the total monthly income amount for the set of income sources.
        /// </summary>
        public Money MonthlyAmount { get; }
    }
}