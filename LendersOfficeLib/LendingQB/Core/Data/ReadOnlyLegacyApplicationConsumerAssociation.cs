﻿namespace LendingQB.Core.Data
{
    using System;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A string-based legacy application consumer association.
    /// </summary>
    public interface ILegacyApplicationConsumerAssociationStringFormatter : IPathableStringFormatter
    {
        /// <summary>
        /// Gets the legacy application id.
        /// </summary>
        string LegacyApplicationId { get; }

        /// <summary>
        /// Gets the consumer id.
        /// </summary>
        string ConsumerId { get; }

        /// <summary>
        /// Gets a value indicating whether the consumer is the primary borrower for the application.
        /// </summary>
        string IsPrimary { get; }
    }

    /// <summary>
    /// A read only legacy application -> consumer association.
    /// </summary>
    /// <remarks>
    /// This class is intended for use while the legacy application -> consumers are based on
    /// CAppData because in that case, they are not mutable (changes to them would not be saved).
    /// </remarks>
    internal sealed class ReadOnlyLegacyApplicationConsumerAssociation : ILegacyApplicationConsumerAssociation, IPathResolvable, IPathableStringFormatterProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyLegacyApplicationConsumerAssociation"/> class.
        /// </summary>
        /// <param name="appId">The legacy application id.</param>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="isPrimary">A value indicating whether the consumer is the primary borrower for the app.</param>
        public ReadOnlyLegacyApplicationConsumerAssociation(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            bool isPrimary)
        {
            this.LegacyApplicationId = appId;
            this.ConsumerId = consumerId;
            this.IsPrimary = isPrimary;
        }

        /// <summary>
        /// Gets the legacy application id.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> LegacyApplicationId { get; }

        /// <summary>
        /// Gets the consumer id.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId { get; }

        /// <summary>
        /// Gets a value indicating whether the consumer is the primary borrower.
        /// </summary>
        public bool IsPrimary { get; }

        /// <summary>
        /// Gets the data point specified by the path.
        /// </summary>
        /// <param name="element">The path element.</param>
        /// <returns>The element at the given path.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    $"Can only handle {nameof(DataPathBasicElement)}, but passed element of type {element.GetType()}");
            }

            switch (element.Name.ToLower())
            {
                case "legacyapplicationid": return this.LegacyApplicationId;
                case "consumerid": return this.ConsumerId;
                case "isprimary": return this.IsPrimary;
                default: throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unsupported field: " + element.Name);
            }
        }

        /// <summary>
        /// Gets a string formatter for the association.
        /// </summary>
        /// <param name="formatter">The string formatter.</param>
        /// <param name="parser">The string value parser.</param>
        /// <returns>A string formatter for the association.</returns>
        public IPathableStringFormatter GetStringFormatter(IFormatAsString formatter, IParseFromString parser)
        {
            var factory = new StringFormatterFactory();
            return factory.Create(this, formatter);
        }

        /// <summary>
        /// A factory for the string formatter.
        /// </summary>
        public struct StringFormatterFactory
        {
            /// <summary>
            /// Creates a new string formatter with the given data.
            /// </summary>
            /// <param name="data">The association.</param>
            /// <param name="formatter">The formatter.</param>
            /// <returns>A new string formatter with the given data.</returns>
            public ILegacyApplicationConsumerAssociationStringFormatter Create(ILegacyApplicationConsumerAssociation data, IFormatAsString formatter)
            {
                return new StringFormatter(data, formatter);
            }
        }

        /// <summary>
        /// The string formatter for the legacy app consumer association.
        /// </summary>
        private sealed class StringFormatter : ILegacyApplicationConsumerAssociationStringFormatter
        {
            /// <summary>
            /// The association.
            /// </summary>
            private ILegacyApplicationConsumerAssociation data;

            /// <summary>
            /// The string formatter.
            /// </summary>
            private IFormatAsString formatter;

            /// <summary>
            /// Initializes a new instance of the <see cref="StringFormatter"/> class.
            /// </summary>
            /// <param name="data">The association.</param>
            /// <param name="formatter">The string formatter.</param>
            public StringFormatter(ILegacyApplicationConsumerAssociation data, IFormatAsString formatter)
            {
                this.data = data;
                this.formatter = formatter == null ? new FormatAsStringBase() : formatter;
            }

            /// <summary>
            /// Gets the string representation of the consumer id.
            /// </summary>
            public string ConsumerId
            {
                get
                {
                    return this.formatter.Format<DataObjectKind.Consumer, Guid>(this.data.ConsumerId);
                }
            }

            /// <summary>
            /// Gets the string representation of whether the consumer is the primary borrower for the application.
            /// </summary>
            public string IsPrimary
            {
                get
                {
                    return this.formatter.Format(this.data.IsPrimary);
                }
            }

            /// <summary>
            /// Gets the string representation of the legacy application id.
            /// </summary>
            public string LegacyApplicationId
            {
                get
                {
                    return this.formatter.Format<DataObjectKind.LegacyApplication, Guid>(this.data.LegacyApplicationId);
                }
            }

            /// <summary>
            /// Gets the data point specified by the path.
            /// </summary>
            /// <param name="element">The path element.</param>
            /// <returns>The element at the given path.</returns>
            public object GetElement(IDataPathElement element)
            {
                if (!(element is DataPathBasicElement))
                {
                    throw new CBaseException(
                        LendersOffice.Common.ErrorMessages.Generic,
                        $"Can only handle {nameof(DataPathBasicElement)}, but passed element of type {element.GetType()}");
                }

                switch (element.Name.ToLower())
                {
                    case "legacyapplicationid": return this.LegacyApplicationId;
                    case "consumerid": return this.ConsumerId;
                    case "isprimary": return this.IsPrimary;
                    default: throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unsupported field: " + element.Name);
                }
            }

            /// <summary>
            /// Throws an exception because this association is read-only.
            /// </summary>
            /// <param name="path">The path element.</param>
            /// <param name="value">The value to set.</param>
            public void SetElement(IDataPathElement path, object value)
            {
                    throw new CBaseException(
                        LendersOffice.Common.ErrorMessages.Generic,
                        $"IPathSettable not supported on ReadOnlyLegacyApplicationConsumer association.");
            }
        }
    }
}
