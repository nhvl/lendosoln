﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a housing history entry.
    /// </summary>
    public partial class HousingHistoryEntry
    {
        /// <summary>
        /// Sets the consumer who is associated with the housing history.
        /// </summary>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="legacyAppId">The legacy application id the consumer is associated with.</param>
        public void SetConsumer(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId)
        {
            this.ConsumerId = consumerId;
            this.LegacyAppId = legacyAppId;
        }
    }
}
