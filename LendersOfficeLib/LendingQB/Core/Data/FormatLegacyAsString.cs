﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to format semantic data types as strings using
    /// formatting logic specific to legacy application and consumer properties.
    /// </summary>
    internal class FormatLegacyAsString : FormatAsStringBase
    {
        /// <summary>
        /// Format a BooleanKleene value as a string.
        /// </summary>
        /// <param name="value">The value for formatting.</param>
        /// <returns>The string representation of the value.</returns>
        public override string Format(BooleanKleene? value)
        {
            return this.Format(value, null);
        }

        /// <summary>
        /// Format a BooleanKleene value as a string.
        /// </summary>
        /// <param name="value">The value for formatting.</param>
        /// <param name="defaultVal">The default value to use instead of returning null.</param>
        /// <returns>The string representation of the value.</returns>
        public override string Format(BooleanKleene? value, string defaultVal)
        {
            if (value == null)
            {
                return defaultVal;
            }
            else if (value == BooleanKleene.True)
            {
                return "Y";
            }
            else if (value == BooleanKleene.False)
            {
                return "N";
            }
            else
            {
                // Indeterminant
                return string.Empty;
            }
        }

        /// <summary>
        /// Format a SocialSecurityNumber as an unmasked string.
        /// </summary>
        /// <param name="value">The value for formatting.</param>
        /// <param name="defaultValue">The default value to use instead of returning null.</param>
        /// <returns>The unmasked SSN.</returns>
        public override string Format(SocialSecurityNumber? value, string defaultValue)
        {
            return value.HasValue ? value.Value.UnmaskedSsn : defaultValue;
        }
    }
}
