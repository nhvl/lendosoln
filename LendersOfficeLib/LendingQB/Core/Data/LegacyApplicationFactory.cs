﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating implementations of the ILegacyApplication interface.
    /// </summary>
    public struct LegacyApplicationFactory
    {
        /// <summary>
        /// Create an implemenation of the ILegacyApplication interface.
        /// </summary>
        /// <param name="legacyApplication">A POD legacy application implementation.</param>
        /// <param name="workflowValidator">The workflow validator.</param>
        /// <returns>An implemenation of the ILegacyApplication interface.</returns>
        public ILegacyApplication Create(
            ILegacyApplication legacyApplication,
            ILoanFileFieldValidator workflowValidator)
        {
            return new AppDataLegacyApplication(legacyApplication as AppDataLegacyApplication, workflowValidator);
        }

        /// <summary>
        /// Create an implemenation of the ILegacyApplication interface.
        /// </summary>
        /// <param name="app">Provider of legacy application and consumer data.</param>
        /// <param name="workflowValidator">The workflow validator.</param>
        /// <param name="legacyToSemanticTypeParser">Parser for field data from the legacy provider, formats the data as semantic types.</param>
        /// <param name="semanticToLegacyNumericFormatter">Formatter for direct conversion of semantic types into numeric types used by legacy application and consumer properties.</param>
        /// <param name="semanticToLegacyStringFormatter">Formatter for direct conversion of semantic types into string types used by legacy application and consumer properties.</param>
        /// <returns>An implemenation of the ILegacyApplication interface.</returns>
        internal ILegacyApplication Create(
            IAppDataLegacyApplicationAppDataProvider app,
            ILoanFileFieldValidator workflowValidator,
            IParseFromString legacyToSemanticTypeParser,
            IFormatAsNumeric semanticToLegacyNumericFormatter,
            IFormatAsString semanticToLegacyStringFormatter)
        {
            return new AppDataLegacyApplication(app, workflowValidator, legacyToSemanticTypeParser, semanticToLegacyNumericFormatter, semanticToLegacyStringFormatter);
        }
    }
}
