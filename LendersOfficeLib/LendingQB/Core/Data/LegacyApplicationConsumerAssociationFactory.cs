﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for legacy application to consumer associations.
    /// </summary>
    public struct LegacyApplicationConsumerAssociationFactory
    {
        /// <summary>
        /// Creates a new legacy application consumer association.
        /// </summary>
        /// <param name="appId">The legacy application id.</param>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="isPrimary">A value indicating whether the consumer is the primary borrower for the application.</param>
        /// <returns>The new legacy application consumer association.</returns>
        public ILegacyApplicationConsumerAssociation Create(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            bool isPrimary)
        {
            return new ReadOnlyLegacyApplicationConsumerAssociation(appId, consumerId, isPrimary);
        }
    }
}
