﻿namespace LendingQB.Core.Data
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Record representing an asset.
    /// </summary>
    public partial class Asset
    {
        /// <summary>
        /// Gets the gift source.
        /// </summary>
        public E_GiftFundSourceT? GiftSource
        {
            get
            {
                if (this.AssetType == null)
                {
                    return null;
                }
                
                switch (this.AssetType)
                {
                    case E_AssetT.GiftEquity:
                    case E_AssetT.LeasePurchaseCredit:
                    case E_AssetT.TradeEquityFromPropertySwap:
                        return E_GiftFundSourceT.Seller;
                    case E_AssetT.EmployerAssistance:
                        return E_GiftFundSourceT.Employer;
                    case E_AssetT.GiftFunds:
                    case E_AssetT.Grant:
                    case E_AssetT.SweatEquity:
                        return GiftSourceData;
                    case E_AssetT.Auto:
                    case E_AssetT.Bonds:
                    case E_AssetT.Business:
                    case E_AssetT.Checking:
                    case E_AssetT.LifeInsurance:
                    case E_AssetT.Retirement:
                    case E_AssetT.Savings:
                    case E_AssetT.Stocks:
                    case E_AssetT.OtherIlliquidAsset:
                    case E_AssetT.CashDeposit:
                    case E_AssetT.OtherLiquidAsset:
                    case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
                    case E_AssetT.CertificateOfDeposit:
                    case E_AssetT.MoneyMarketFund:
                    case E_AssetT.MutualFunds:
                    case E_AssetT.SecuredBorrowedFundsNotDeposit:
                    case E_AssetT.BridgeLoanNotDeposited:
                    case E_AssetT.TrustFunds:
                    case E_AssetT.StockOptions:
                    case E_AssetT.IndividualDevelopmentAccount:
                    case E_AssetT.ProceedsFromSaleOfNonRealEstateAsset:
                    case E_AssetT.ProceedsFromUnsecuredLoan:
                    case E_AssetT.OtherPurchaseCredit:
                        return E_GiftFundSourceT.Blank;
                    default:
                        throw new UnhandledEnumException(this.AssetType);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is a liquid asset.
        /// </summary>
        public bool? IsLiquidAsset
        {
            get
            {
                switch (this.AssetType)
                {
                    case E_AssetT.Bonds:
                    case E_AssetT.Checking:
                    case E_AssetT.GiftFunds:
                    case E_AssetT.GiftEquity:
                    case E_AssetT.LifeInsurance:
                    case E_AssetT.Savings:
                    case E_AssetT.Stocks:
                    case E_AssetT.CashDeposit:
                    case E_AssetT.OtherLiquidAsset:
                    case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
                    case E_AssetT.BridgeLoanNotDeposited:
                    case E_AssetT.CertificateOfDeposit:
                    case E_AssetT.MoneyMarketFund:
                    case E_AssetT.MutualFunds:
                    case E_AssetT.SecuredBorrowedFundsNotDeposit:
                    case E_AssetT.TrustFunds:
                    case E_AssetT.ProceedsFromSaleOfNonRealEstateAsset:
                    case E_AssetT.ProceedsFromUnsecuredLoan:
                    case E_AssetT.EmployerAssistance:
                    case E_AssetT.StockOptions:
                    case E_AssetT.IndividualDevelopmentAccount:
                    case E_AssetT.Grant:
                        return true;
                    case E_AssetT.Auto:
                    case E_AssetT.Business:
                    case E_AssetT.Retirement:
                    case E_AssetT.OtherIlliquidAsset:
                    case E_AssetT.LeasePurchaseCredit:
                    case E_AssetT.SweatEquity:
                    case E_AssetT.TradeEquityFromPropertySwap:
                    case E_AssetT.OtherPurchaseCredit:
                        return false;
                    case null:
                        return null;
                    default:
                        throw new UnhandledEnumException(this.AssetType);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asset must ask the user whether it should be credited at closing.
        /// </summary>
        public bool IsCreditAtClosingUserDependent
        {
            get
            {
                if (!this.AssetType.HasValue)
                {
                    return false;
                }

                return this.AssetType.Value.EqualsOneOf(E_AssetT.GiftFunds, E_AssetT.EmployerAssistance, E_AssetT.Grant);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asset is credited at closing.
        /// </summary>
        public bool? IsCreditAtClosing
        {
            get
            {
                if (this.IsCreditAtClosingUserDependent)
                {
                    return this.IsCreditAtClosingData;
                }

                if (!this.AssetType.HasValue)
                {
                    return null;
                }

                return this.AssetType.Value.EqualsOneOf(E_AssetT.GiftEquity, E_AssetT.LeasePurchaseCredit, E_AssetT.SweatEquity, E_AssetT.TradeEquityFromPropertySwap, E_AssetT.OtherPurchaseCredit);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asset requires a Verification of Deposit.
        /// </summary>
        public bool? RequiresVod
        {
            get
            {
                if (!this.AssetType.HasValue)
                {
                    return null;
                }

                var convertedAssetGroupT = (E_AssetGroupT)Math.Pow(2, (int)this.AssetType);
                return !E_AssetGroupT.DontNeedVOD.HasFlag(convertedAssetGroupT);
            }
        }

        /// <summary>
        /// Gets the friendly description for the Asset type.
        /// </summary>
        public DescriptionField? AssetTypeDesc
        {
            get
            {
                return DescriptionField.Create(EnumUtilities.GetDescription(this.AssetType.GetValueOrDefault()));
            }
        }

        /// <summary>        
        /// Gets a value indicating whether the asset verification has a signature.
        /// </summary>
        public bool? VerifHasSignature
        {
            get
            {
                return this.VerifSigningEmployeeId.HasValue && this.VerifSignatureImgId.HasValue;
            }
        }

        /// <summary>
        /// Clear the signature on the asset.
        /// </summary>
        public void ClearSignature()
        {
            this.VerifSignatureImgId = null;
            this.VerifSigningEmployeeId = null;
        }

        /// <summary>
        /// Set the two data points related to signing a asset.
        /// </summary>
        /// <param name="employeeId">The employeeId of the person that signed the asset.</param>
        /// <param name="imgId">The file identifier for the image file containing the signature.</param>
        public void SetVerifSignature(DataObjectIdentifier<DataObjectKind.Employee, Guid> employeeId, DataObjectIdentifier<DataObjectKind.Signature, Guid> imgId)
        {
            this.VerifSigningEmployeeId = employeeId;
            this.VerifSignatureImgId = imgId;
        }
    }
}