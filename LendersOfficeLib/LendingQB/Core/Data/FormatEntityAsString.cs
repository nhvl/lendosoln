﻿namespace LendingQB.Core.Data
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to format semantic data types as strings using
    /// different formatting logic based on the target.
    /// </summary>
    internal class FormatEntityAsString : FormatAsStringBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatEntityAsString"/> class.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        public FormatEntityAsString(FormatTarget target, FormatDirection direction)
        {
            this.Converter = LosConvert.Create(target);
            this.Direction = direction;

            this.NumericConverter = new FormatAsNumericBase();
        }

        /// <summary>
        /// Gets or sets the converter.
        /// </summary>
        private ILosConvert Converter { get; set; }

        /// <summary>
        /// Gets or sets a numeric formatter.
        /// </summary>
        private IFormatAsNumeric NumericConverter { get; set; }

        /// <summary>
        /// Gets or sets the direction in which the formatting is going to be used.
        /// </summary>
        private FormatDirection Direction { get; set; }

        /// <summary>
        /// Format a Count&lt;T&gt; as a string.
        /// </summary>
        /// <typeparam name="T">The generic type specifier.</typeparam>
        /// <param name="value">The Count&lt;T&gt; value.</param>
        /// <returns>The string representation of the Count&lt;T&gt; value.</returns>
        public override string Format<T>(Count<T>? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                int count = this.NumericConverter.Format(value);
                return this.Converter.ToCountString(count);
            }
        }

        /// <summary>
        /// Formats a string-based count as a string.
        /// </summary>
        /// <param name="value">The count to format.</param>
        /// <returns>The string representation of the string-based count.</returns>
        public override string Format(CountString? value)
        {
            return this.Converter.ToCountVarCharStringRep(value?.ToString() ?? string.Empty);
        }

        /// <summary>
        /// Formats the remaining months for a liability as a string.
        /// </summary>
        /// <param name="value">The remaining months to format.</param>
        /// <returns>The string representation of the remaining months.</returns>
        public override string Format(LiabilityRemainingMonths? value)
        {
            return this.Converter.ToCountVarCharStringRep(value?.ToString() ?? string.Empty);
        }

        /// <summary>
        /// Format a Money value as a string.
        /// </summary>
        /// <param name="value">The Money value.</param>
        /// <returns>The string representation of the Money value.</returns>
        public override string Format(Money? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                decimal numeric = this.NumericConverter.Format(value);
                return this.Converter.ToMoneyString(numeric, this.Direction);
            }
        }

        /// <summary>
        /// Format a Percentage value as a string.
        /// </summary>
        /// <param name="value">The Percentage value.</param>
        /// <returns>The string representation of the Percentage value.</returns>
        public override string Format(Percentage? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                decimal numeric = this.NumericConverter.Format(value);
                return this.Converter.ToRateString(numeric);
            }
        }

        /// <summary>
        /// Format a PhoneNumber value as a string.
        /// </summary>
        /// <param name="value">The PhoneNumber value.</param>
        /// <returns>The string representation of the PhoneNumber value.</returns>
        public override string Format(PhoneNumber? value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            return this.Converter.ToPhoneNumFormat(value.ToString(), this.Direction);
        }

        /// <summary>
        /// Format an UnzonedDate value as a string.
        /// </summary>
        /// <param name="value">The UnzonedDate value.</param>
        /// <returns>The string representation of the UnzonedDate value.</returns>
        public override string Format(UnzonedDate? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                return this.Converter.ToDateTimeString(value.Value.Date, false);
            }
        }
    }
}
