﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// An <see cref="IPathableStringFormatter"/> instance for the <see cref="DataAccess.PostalAddress"/> inheritance hierarchy.
    /// </summary>
    public class PostalAddressStringFormatter : IPathableNestedStringFormatter<DataAccess.PostalAddress>, IPathResolvableExpandable
    {
        /// <summary>
        /// The set of stored set operations sent to this object, which are only applied when the actual address instance is constructed.
        /// </summary>
        private readonly Dictionary<string, string> stateBag = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The helper instance for the particular type of address being get/set.  Note that this may be null.
        /// </summary>
        private PostalAddressStringFormatterHelper helper;

        /// <summary>
        /// A cached value for <see cref="Instance"/>.
        /// </summary>
        private DataAccess.PostalAddress cachedInstance;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddressStringFormatter"/> class.
        /// </summary>
        /// <param name="instance">The underlying instance this string formatter wraps.</param>
        /// <param name="formatter">The formatter to apply to output values.</param>
        /// <param name="parser">The parser to convert input values.</param>
        public PostalAddressStringFormatter(DataAccess.PostalAddress instance, IFormatAsString formatter, IParseFromString parser)
        {
            if (instance is DataAccess.GeneralPostalAddress)
            {
                this.helper = new GeneralPostalAddressStringFormatterHelper((DataAccess.GeneralPostalAddress)instance, formatter, parser);
            }
            else if (instance is DataAccess.UnitedStatesPostalAddress)
            {
                this.helper = new UnitedStatesPostalAddressStringFormatterHelper((DataAccess.UnitedStatesPostalAddress)instance, formatter, parser);
            }
            else if (instance != null)
            {
                throw DataAccess.CBaseException.GenericException("Unhandled instance of DataAccess.PostalAddress.  Type was '" + instance.GetType().FullName + "'");
            }

            this.Formatter = formatter;
            this.Parser = parser;
            this.cachedInstance = instance;
        }

        /// <summary>
        /// Gets the underlying instance this string formatter wraps.
        /// </summary>
        public DataAccess.PostalAddress Instance
        {
            get
            {
                if (this.cachedInstance == null)
                {
                    if (this.helper == null)
                    {
                        return null;
                    }

                    foreach (KeyValuePair<string, string> kvp in this.stateBag)
                    {
                        this.SetValue(kvp.Key, kvp.Value);
                    }

                    this.cachedInstance = this.helper.GetAddress();
                }

                return this.cachedInstance;
            }
        }

        /// <summary>
        /// Gets the formatter to apply to output values.
        /// </summary>
        public IFormatAsString Formatter { get; }

        /// <summary>
        /// Gets the parser to convert input values.
        /// </summary>
        public IParseFromString Parser { get; }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.PostalAddress.StreetAddress"/> of the address.
        /// This is read-only unless <see cref="Instance"/> is a <see cref="DataAccess.GeneralPostalAddress"/>.
        /// </summary>
        public string StreetAddress
        {
            get { return this.GetValue(nameof(DataAccess.PostalAddress.StreetAddress)); }
            set { this.SetValue(nameof(DataAccess.GeneralPostalAddress.StreetAddress), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.PostalAddress.City"/> of the address.
        /// </summary>
        public string City
        {
            get { return this.GetValue(nameof(DataAccess.PostalAddress.City)); }
            set { this.SetValue(nameof(DataAccess.PostalAddress.City), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.PostalAddress.State"/> of the address.
        /// This is read-only unless <see cref="Instance"/> is a <see cref="DataAccess.GeneralPostalAddress"/>.
        /// </summary>
        public string State
        {
            get { return this.GetValue(nameof(DataAccess.PostalAddress.State)); }
            set { this.SetValue(nameof(DataAccess.GeneralPostalAddress.State), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.PostalAddress.PostalCode"/> of the address.
        /// This is read-only unless <see cref="Instance"/> is a <see cref="DataAccess.GeneralPostalAddress"/>.
        /// </summary>
        public string PostalCode
        {
            get { return this.GetValue(nameof(DataAccess.PostalAddress.PostalCode)); }
            set { this.SetValue(nameof(DataAccess.GeneralPostalAddress.PostalCode), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.PostalAddress.CountryCode"/> of the address.
        /// This is read-only unless <see cref="Instance"/> is a <see cref="DataAccess.GeneralPostalAddress"/>.
        /// </summary>
        public string CountryCode
        {
            get { return this.GetValue(nameof(DataAccess.PostalAddress.CountryCode)); }
            set { this.SetValue(nameof(DataAccess.GeneralPostalAddress.CountryCode), value); }
        }

        /// <summary>
        /// Gets the <see cref="DataAccess.PostalAddress.Country"/> of the address.
        /// </summary>
        public string Country => this.GetValue(nameof(DataAccess.PostalAddress.Country));

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.UnparsedStreetAddress"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string UnparsedStreetAddress
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnparsedStreetAddress)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnparsedStreetAddress), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.AddressNumber"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string AddressNumber
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.AddressNumber)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.AddressNumber), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.StreetPreDirection"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string StreetPreDirection
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetPreDirection)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetPreDirection), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.StreetName"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string StreetName
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetName)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetName), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.StreetPostDirection"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string StreetPostDirection
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetPostDirection)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetPostDirection), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.StreetSuffix"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string StreetSuffix
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetSuffix)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.StreetSuffix), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.UnitType"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string UnitType
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnitType)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnitType), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.UnitIdentifier"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string UnitIdentifier
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnitIdentifier)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.UnitIdentifier), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.UsState"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string UsState
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.UsState)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.UsState), value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataAccess.UnitedStatesPostalAddress.Zipcode"/> of the address.
        /// This will throw unless <see cref="Instance"/> is a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        public string Zipcode
        {
            get { return this.GetValue(nameof(DataAccess.UnitedStatesPostalAddress.Zipcode)); }
            set { this.SetValue(nameof(DataAccess.UnitedStatesPostalAddress.Zipcode), value); }
        }

        /// <summary>
        /// Gets the children of this instance supporting read/get operations.
        /// </summary>
        /// <returns>The set of readable child elements.</returns>
        public IEnumerable<DataPathBasicElement> GetReadableChildren()
        {
            return this.helper?.Properties.Select(p => new DataPathBasicElement(p.Name)) ?? Enumerable.Empty<DataPathBasicElement>();
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new ArgumentException($"PostalAddressStringFormatter can only handle {nameof(DataPathBasicElement)}, but passed element of type {element?.GetType().FullName ?? "null"}");
            }

            return this.GetValue(element.Name);
        }

        /// <summary>
        /// Sets the value indicated by the path to the given value.
        /// </summary>
        /// <param name="element">The path to the field.</param>
        /// <param name="value">The field value to set.</param>
        public void SetElement(IDataPathElement element, object value)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new ArgumentException($"PostalAddressStringFormatter can only handle {nameof(DataPathBasicElement)}, but passed element of type {element?.GetType().FullName ?? "null"}");
            }

            string stringValue = value as string;
            if (value != null && stringValue == null)
            {
                throw new ArgumentException("PostalAddressStringFormatter can only handle string values.");
            }

            string elementName = element.Name.ToLower();
            if (elementName == "$type")
            {
                switch (stringValue)
                {
                    case "UnitedStatesAddress":
                        this.SetType(typeof(DataAccess.UnitedStatesPostalAddress));
                        break;
                    case "GeneralAddress":
                        this.SetType(typeof(DataAccess.GeneralPostalAddress));
                        break;
                    default:
                        throw new ArgumentException("Invalid '$type' specified.");
                }
            }
            else
            {
                this.stateBag[element.Name] = stringValue;
            }

            this.cachedInstance = null;
        }

        /// <summary>
        /// Sets the return type of <see cref="Instance"/>.
        /// </summary>
        /// <param name="t">The type of the subclasss of <see cref="DataAccess.PostalAddress"/> to set.</param>
        public void SetType(Type t)
        {
            PostalAddressStringFormatterHelper newHelper;
            if (t == typeof(DataAccess.UnitedStatesPostalAddress))
            {
                newHelper = new UnitedStatesPostalAddressStringFormatterHelper(null, this.Formatter, this.Parser);
            }
            else if (t == typeof(DataAccess.GeneralPostalAddress))
            {
                newHelper = new GeneralPostalAddressStringFormatterHelper(null, this.Formatter, this.Parser);
            }
            else
            {
                throw DataAccess.CBaseException.GenericException("Unhandled type; expected subclass of DataAccess.PostalAddress.  Type was '" + t?.FullName + "'");
            }

            if (this.helper?.GetType() != newHelper.GetType())
            {
                this.helper = newHelper;
                this.cachedInstance = null;
            }
        }

        /// <summary>
        /// Gets the value of the property with the specified name.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>The value of the property.</returns>
        private string GetValue(string propertyName)
        {
            AddressProperty property = this.helper?[propertyName];
            if (property?.GetValue == null)
            {
                throw new ArgumentException($"Invalid field id: {propertyName}.");
            }

            return property.GetValue(this.Instance);
        }

        /// <summary>
        /// Sets the value of the property with the specified name.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="value">The value of the property.</param>
        private void SetValue(string propertyName, string value)
        {
            AddressProperty property = this.helper[propertyName];
            if (property?.SetValue == null)
            {
                throw new ArgumentException($"Invalid field id: {propertyName}.");
            }

            property.SetValue(value);
        }

        /// <summary>
        /// A base class to unify the interface and core functionality for constructing instances of <see cref="DataAccess.PostalAddress"/>.
        /// </summary>
        private abstract class PostalAddressStringFormatterHelper
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PostalAddressStringFormatterHelper"/> class.
            /// </summary>
            /// <param name="typeName">The name of the type.</param>
            /// <param name="formatter">The formatter to apply to output values.</param>
            /// <param name="parser">The parser to convert input values.</param>
            public PostalAddressStringFormatterHelper(string typeName, IFormatAsString formatter, IParseFromString parser)
            {
                this.TypeName = typeName;
                this.Formatter = formatter;
                this.Parser = parser;
                this.Properties = this.GetProperties();
            }

            /// <summary>
            /// Gets the formatter to apply to output values.
            /// </summary>
            public IFormatAsString Formatter { get; }

            /// <summary>
            /// Gets the parser to convert input values.
            /// </summary>
            public IParseFromString Parser { get; }

            /// <summary>
            /// Gets the name of the type of address.
            /// </summary>
            public string TypeName { get; }

            /// <summary>
            /// Gets a list of properties for the address.
            /// </summary>
            public AddressProperty[] Properties { get; }

            /// <summary>
            /// Gets the address property specified by a particular name.
            /// </summary>
            /// <param name="name">The name of the property.</param>
            /// <returns>The address property, or <see langword="null"/> if it is not found.</returns>
            public AddressProperty this[string name]
            {
                get { return this.Properties.SingleOrDefault(p => StringComparer.OrdinalIgnoreCase.Equals(p.Name, name)); }
            }

            /// <summary>
            /// Computes the address instance represented by this helper.
            /// </summary>
            /// <returns>The address instance.</returns>
            public abstract DataAccess.PostalAddress GetAddress();

            /// <summary>
            /// Replaces the specified instance of <see cref="AddressProperty"/> with an updated value for <see cref="AddressProperty.SetValue"/>.
            /// </summary>
            /// <param name="properties">The list of address properties.</param>
            /// <param name="name">The name of the address property to update.</param>
            /// <param name="setValue">The <see cref="AddressProperty.SetValue"/> function.</param>
            protected static void AddSetValue(AddressProperty[] properties, string name, Action<string> setValue)
            {
                for (int i = 0; i < properties.Length; ++i)
                {
                    if (properties[i].Name == name)
                    {
                        properties[i] = new AddressProperty(properties[i].Name, properties[i].GetValue, setValue);
                        return;
                    }
                }
            }

            /// <summary>
            /// Gets the properties for a base instance of <see cref="DataAccess.PostalAddress"/>.
            /// </summary>
            /// <returns>The list of properties.</returns>
            protected virtual AddressProperty[] GetProperties()
            {
                return new[]
                {
                    new AddressProperty("$type", a => this.TypeName, null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.StreetAddress), a => this.Formatter.Format(a.StreetAddress), null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.City), a => this.Formatter.Format(a.City), null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.State), a => this.Formatter.Format(a.State), null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.PostalCode), a => this.Formatter.Format(a.PostalCode), null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.CountryCode), a => this.Formatter.Format(a.CountryCode), null),
                    new AddressProperty(nameof(DataAccess.PostalAddress.Country), a => this.Formatter.Format(a.Country), null),
                };
            }
        }

        /// <summary>
        /// Wraps the functionality for constructing instances of <see cref="DataAccess.UnitedStatesPostalAddress"/>.
        /// </summary>
        private class UnitedStatesPostalAddressStringFormatterHelper : PostalAddressStringFormatterHelper
        {
            /// <summary>
            /// The builder used to construct the new instance.
            /// </summary>
            private readonly DataAccess.UnitedStatesPostalAddress.Builder builder;

            /// <summary>
            /// Initializes a new instance of the <see cref="UnitedStatesPostalAddressStringFormatterHelper"/> class.
            /// </summary>
            /// <param name="instance">The address instance to use as a base set of values.</param>
            /// <param name="formatter">The formatter to apply to output values.</param>
            /// <param name="parser">The parser to convert input values.</param>
            public UnitedStatesPostalAddressStringFormatterHelper(DataAccess.UnitedStatesPostalAddress instance, IFormatAsString formatter, IParseFromString parser)
                : base("UnitedStatesAddress", formatter, parser)
            {
                this.builder = instance?.GetBuilder() ?? new DataAccess.UnitedStatesPostalAddress.Builder();
            }

            /// <summary>
            /// Computes the address instance represented by this helper.
            /// </summary>
            /// <returns>The address instance.</returns>
            public override DataAccess.PostalAddress GetAddress() => this.builder.GetAddress();

            /// <summary>
            /// Gets the properties for an instance of <see cref="DataAccess.UnitedStatesPostalAddress"/>.
            /// </summary>
            /// <returns>The list of properties.</returns>
            protected override AddressProperty[] GetProperties()
            {
                var baseProperties = base.GetProperties();
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.UnitedStatesPostalAddress.City), v => this.builder.City = this.Parser.TryParseCity(v));
                return baseProperties.Concat(new[]
                {
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.UnparsedStreetAddress), a => this.Formatter.Format(ForceUsAddress(a).UnparsedStreetAddress), v => this.builder.UnparsedStreetAddress = this.Parser.TryParseStreetAddress(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.AddressNumber), a => this.Formatter.Format(ForceUsAddress(a).AddressNumber), v => this.builder.AddressNumber = this.Parser.TryParseUspsAddressNumber(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.StreetPreDirection), a => this.Formatter.Format(ForceUsAddress(a).StreetPreDirection), v => this.builder.StreetPreDirection = this.Parser.TryParseUspsDirectional(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.StreetName), a => this.Formatter.Format(ForceUsAddress(a).StreetName), v => this.builder.StreetName = this.Parser.TryParseStreetName(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.StreetPostDirection), a => this.Formatter.Format(ForceUsAddress(a).StreetPostDirection), v => this.builder.StreetPostDirection = this.Parser.TryParseUspsDirectional(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.StreetSuffix), a => this.Formatter.Format(ForceUsAddress(a).StreetSuffix), v => this.builder.StreetSuffix = this.Parser.TryParseStreetSuffix(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.UnitType), a => this.Formatter.Format(ForceUsAddress(a).UnitType), v => this.builder.UnitType = this.Parser.TryParseAddressUnitType(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.UnitIdentifier), a => this.Formatter.Format(ForceUsAddress(a).UnitIdentifier), v => this.builder.UnitIdentifier = this.Parser.TryParseAddressUnitIdentifier(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.UsState), a => this.Formatter.Format(ForceUsAddress(a).UsState), v => this.builder.UsState = this.Parser.TryParseUnitedStatesPostalState(v)),
                    new AddressProperty(nameof(DataAccess.UnitedStatesPostalAddress.Zipcode), a => this.Formatter.Format(ForceUsAddress(a).Zipcode), v => this.builder.Zipcode = this.Parser.TryParseZipcode(v)),
                }).ToArray();
            }

            /// <summary>
            /// Casts a <see cref="DataAccess.PostalAddress"/> to a <see cref="DataAccess.UnitedStatesPostalAddress"/>.
            /// </summary>
            /// <param name="address">The address to cast.</param>
            /// <returns>The address.</returns>
            private static DataAccess.UnitedStatesPostalAddress ForceUsAddress(DataAccess.PostalAddress address) => (DataAccess.UnitedStatesPostalAddress)address;
        }

        /// <summary>
        /// Wraps the functionality for constructing instances of <see cref="DataAccess.GeneralPostalAddress"/>.
        /// </summary>
        private class GeneralPostalAddressStringFormatterHelper : PostalAddressStringFormatterHelper
        {
            /// <summary>
            /// The builder used to construct the new instance.
            /// </summary>
            private readonly DataAccess.GeneralPostalAddress.Builder builder;

            /// <summary>
            /// Initializes a new instance of the <see cref="GeneralPostalAddressStringFormatterHelper"/> class.
            /// </summary>
            /// <param name="instance">The address instance to use as a base set of values.</param>
            /// <param name="formatter">The formatter to apply to output values.</param>
            /// <param name="parser">The parser to convert input values.</param>
            public GeneralPostalAddressStringFormatterHelper(DataAccess.GeneralPostalAddress instance, IFormatAsString formatter, IParseFromString parser)
                : base("GeneralAddress", formatter, parser)
            {
                this.builder = instance?.GetBuilder() ?? new DataAccess.GeneralPostalAddress.Builder();
            }

            /// <summary>
            /// Computes the address instance represented by this helper.
            /// </summary>
            /// <returns>The address instance.</returns>
            public override DataAccess.PostalAddress GetAddress() => this.builder.GetAddress();

            /// <summary>
            /// Gets the properties for an instance of <see cref="DataAccess.GeneralPostalAddress"/>.
            /// </summary>
            /// <returns>The list of properties.</returns>
            protected override AddressProperty[] GetProperties()
            {
                var baseProperties = base.GetProperties();
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.GeneralPostalAddress.StreetAddress), v => this.builder.StreetAddress = this.Parser.TryParseStreetAddress(v));
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.GeneralPostalAddress.City), v => this.builder.City = this.Parser.TryParseCity(v));
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.GeneralPostalAddress.State), v => this.builder.State = this.Parser.TryParseAdministrativeArea(v));
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.GeneralPostalAddress.PostalCode), v => this.builder.PostalCode = this.Parser.TryParsePostalCode(v));
                PostalAddressStringFormatterHelper.AddSetValue(baseProperties, nameof(DataAccess.GeneralPostalAddress.CountryCode), v => this.builder.CountryCode = this.Parser.TryParseCountryCodeIso3(v));
                return baseProperties;
            }
        }

        /// <summary>
        /// Represents a class property of an address.
        /// </summary>
        private class AddressProperty
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AddressProperty"/> class.
            /// </summary>
            /// <param name="name">The name of the property.</param>
            /// <param name="getValue">The getter for the property from an instance of the address.</param>
            /// <param name="setValue">A function to set the property for some type of address.</param>
            public AddressProperty(string name, Func<DataAccess.PostalAddress, string> getValue, Action<string> setValue)
            {
                this.Name = name;
                this.GetValue = getValue;
                this.SetValue = setValue;
            }

            /// <summary>
            /// Gets the name of the property.
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Gets the getter for the property from an instance of the address.
            /// </summary>
            public Func<DataAccess.PostalAddress, string> GetValue { get; }

            /// <summary>
            /// Gets a function to set the property for some type of address.
            /// </summary>
            public Action<string> SetValue { get; }
        }
    }
}
