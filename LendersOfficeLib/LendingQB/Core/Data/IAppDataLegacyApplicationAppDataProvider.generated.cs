﻿// <auto-generated />
namespace LendingQB.Core.Data
{
    using System;
    using DataAccess;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;

    public interface IAppDataLegacyApplicationAppDataProvider
    {
        Guid aBConsumerId { get; }
        Guid aCConsumerId { get; }
        int aAddrRemainderMonths { get; }
        int aAddrWholeYears { get; }
        string aAppCPortalFullNm { get; }
        string aAppFullNm { get; }
        string aAppNm { get; }
        bool aAsstLiaCompletedNotJointly { get; set; }
        int aBAvgScore { get; }
        string aBNm_aCNm { get; }
        bool aBorrowerAddressSameAsSubjectProperty { get; }
        E_aBorrowerCreditModeT aBorrowerCreditModeT { get; set; }
        E_BorrowerModeT BorrowerModeT { get; set; }
        E_aTypeT aBorrowerTypeT { get; }
        bool aCIsDefined { get; }
        decimal aFHAAssetAvail { get; set; }
        decimal aFHABorrCertInformedPropVal { get; set; }
        bool aFHABorrCertInformedPropValAwareAtContractSigning { get; set; }
        bool aFHABorrCertInformedPropValDeterminedByHUD { get; set; }
        bool aFHABorrCertInformedPropValDeterminedByVA { get; set; }
        bool aFHABorrCertInformedPropValNotAwareAtContractSigning { get; set; }
        bool aFHABorrCertOccIsAsHome { get; set; }
        bool aFHABorrCertOccIsAsHomeForActiveSpouse { get; set; }
        bool aFHABorrCertOccIsAsHomePrev { get; set; }
        bool aFHABorrCertOccIsAsHomePrevForActiveSpouse { get; set; }
        bool aFHABorrCertOccIsChildAsHome { get; set; }
        bool aFHABorrCertOccIsChildAsHomePrev { get; set; }
        string aFHABorrCertOtherPropCity { get; set; }
        E_TriState aFHABorrCertOtherPropCoveredByThisLoanTri { get; set; }
        decimal aFHABorrCertOtherPropOrigMAmt { get; set; }
        decimal aFHABorrCertOtherPropSalesPrice { get; set; }
        string aFHABorrCertOtherPropStAddr { get; set; }
        string aFHABorrCertOtherPropState { get; set; }
        bool aFhaBorrCertOtherPropToBeSoldNA { get; }
        E_TriState aFHABorrCertOtherPropToBeSoldTri { get; set; }
        string aFHABorrCertOtherPropZip { get; set; }
        E_TriState aFHABorrCertOwnMoreThan4DwellingsTri { get; set; }
        E_TriState aFHABorrCertOwnOrSoldOtherFHAPropTri { get; set; }
        E_TriState aFHABorrCertReceivedLeadPaintPoisonInfoTri { get; set; }
        E_TriState aFhaBorrCertWillOccupyFor1YearTri { get; }
        string aFHACreditRating { get; set; }
        decimal aFHADebtInstallBal { get; set; }
        decimal aFHADebtInstallPmt { get; set; }
        decimal aFHAGiftFundAmt { get; set; }
        string aFHAGiftFundSrc { get; set; }
        decimal aFHAGrossMonI { get; }
        decimal aFHAMPmtToIRatio { get; }
        decimal aFHANegCfRentalI { get; set; }
        decimal aFHANetRentalI { get; set; }
        decimal aFHAOtherDebtBal { get; set; }
        decimal aFHAOtherDebtPmt { get; set; }
        decimal aFHAOtherDebtPmtMCAWPrinting { get; }
        string aFHARatingAssetAdequacy { get; set; }
        string aFHARatingIAdequacy { get; set; }
        string aFHARatingIStability { get; set; }
        decimal aFHATotBaseI { get; }
        decimal aFHATotOI { get; }
        string a1003ContEditSheet { get; set; }
        CDateTime a1003InterviewD { get; }
        CDateTime a1003InterviewDData { get; set; }
        bool a1003InterviewDLckd { get; set; }
        CDateTime a1003SignD { get; }
        string a4506TAddr { get; }
        string a4506TAddr_Multiline { get; }
        string a4506TCity { get; }
        string a4506TPrevAddr { get; }
        string a4506TPrevAddr_Multiline { get; }
        string a4506TState { get; }
        string a4506TStreetAddr { get; }
        string a4506TThirdPartyInfo { get; }
        string a4506TZip { get; }
        bool aHasClearCaivrsAuthCode { get; }
        bool aHasExtraFormerAddress { get; }
        bool aBHasSpouse { get; }
        bool aHasValidApplicant { get; }
        E_aIntrvwrMethodT aIntrvwrMethodT { get; }
        E_aIntrvwrMethodT aIntrvwrMethodTData { get; set; }
        bool aIntrvwrMethodTLckd { get; set; }
        bool aIs4506TFiledTaxesSeparately { get; set; }
        bool aIsBorrSpousePrimaryWageEarner { get; set; }
        bool aIsBorrSpousePrimaryWageEarnerPe { get; set; }
        bool aIsBorrSpousePrimaryWageEarnerPelckd { get; set; }
        bool aIsCreditReportManual { get; }
        bool aIsFannieTestBorrower { get; }
        bool aIsHudApprovedFTHBCounselingComplete { get; }
        bool aIsPrimary { get; set; }
        decimal aLiquidAssets { get; set; }
        bool aLiquidAssetsExists { get; set; }
        decimal aLiquidAssetsPe { get; set; }
        string aManner { get; set; }
        E_aOccT aOccT { get; set; }
        bool aOccTPelckd { get; }
        decimal aOpNegCfData { get; set; }
        bool aOpNegCfLckd { get; set; }
        decimal aOpNegCfPeval { get; set; }
        decimal aPresHazIns { get; }
        decimal aPresMIns { get; }
        decimal aPresPersistentRent { get; }
        decimal aPresRealETx { get; }
        decimal aPresTotHExp { get; }
        int aProdCrManual120MortLateCount { get; set; }
        int aProdCrManual150MortLateCount { get; set; }
        int aProdCrManual30MortLateCount { get; set; }
        int aProdCrManual60MortLateCount { get; set; }
        int aProdCrManual90MortLateCount { get; set; }
        bool aProdCrManualBk13Has { get; set; }
        int aProdCrManualBk13RecentFileMon { get; set; }
        int aProdCrManualBk13RecentFileYr { get; set; }
        int aProdCrManualBk13RecentSatisfiedMon { get; set; }
        int aProdCrManualBk13RecentSatisfiedYr { get; set; }
        E_sProdCrManualDerogRecentStatusT aProdCrManualBk13RecentStatusT { get; set; }
        bool aProdCrManualBk7Has { get; set; }
        int aProdCrManualBk7RecentFileMon { get; set; }
        int aProdCrManualBk7RecentFileYr { get; set; }
        int aProdCrManualBk7RecentSatisfiedMon { get; set; }
        int aProdCrManualBk7RecentSatisfiedYr { get; set; }
        E_sProdCrManualDerogRecentStatusT aProdCrManualBk7RecentStatusT { get; set; }
        bool aProdCrManualForeclosureHas { get; set; }
        int aProdCrManualForeclosureRecentFileMon { get; set; }
        int aProdCrManualForeclosureRecentFileYr { get; set; }
        int aProdCrManualForeclosureRecentSatisfiedMon { get; set; }
        int aProdCrManualForeclosureRecentSatisfiedYr { get; set; }
        E_sProdCrManualDerogRecentStatusT aProdCrManualForeclosureRecentStatusT { get; set; }
        E_BkType aProdCrManualMostRecentBkT { get; }
        int aProdCrManualMostRecentFileMon { get; }
        int aProdCrManualMostRecentFileYr { get; }
        int aProdCrManualMostRecentSatisfiedMon { get; }
        int aProdCrManualMostRecentSatisfiedYr { get; }
        E_sProdCrManualDerogRecentStatusT aProdCrManualMostRecentStatusT { get; }
        int aProdCrManualNonRolling30MortLateCount { get; set; }
        int aProdCrManualRolling60MortLateCount { get; set; }
        int aProdCrManualRolling90MortLateCount { get; set; }
        int aProdRepresentativeScore { get; }
        bool aRanPricingAfterCreatingApp { get; }
        decimal aReTotMPmtPrimaryResidence { get; set; }
        decimal aSpNegCf { get; }
        decimal aSpNegCfData { get; set; }
        bool aSpNegCfLckd { get; set; }
        bool aSpouseIExcl { get; set; }
        string aSpouseSsn { get; }
        E_sTotalScoreLdpOrGsaExclusionT aTotalScoreLdpOrGsaExclusionT { get; }
        decimal aTotBaseI { get; }
        decimal aTotNetRentI1003 { get; }
        decimal aTotNonbaseI { get; }
        decimal aTotOIFrom1008 { get; }
        decimal aTotSpPosCf { get; }
        decimal aTransmBTotI { get; }
        decimal aTransmCTotI { get; }
        decimal aTransmOMonPmtPeval { get; set; }
        decimal aTransmProMIns { get; }
        decimal aTransmProRent { get; }
        CDateTime aU1DocStatDueD { get; }
        CDateTime aU2DocStatRd { get; }
        bool aVaApplyOtherLoanIsCalculated { get; }
        E_TriState aVaBorrCertHadVaLoanTri { get; set; }
        bool aVaIsCoborIConsidered { get; }
        bool aVaOwnOtherVaLoanTReadOnly { get; }
        decimal aVaTakehomeEmplmtI { get; }
        decimal aVaTotEmplmtI { get; }
        decimal aVaTotITax { get; }
        E_TriState aWillOccupyRefiResidence { get; }
        E_TriState aActiveMilitaryStatTri { get; set; }
        string aAltNm1 { get; set; }
        Sensitive<string> aAltNm1AccNum { get; set; }
        string aAltNm1CreditorNm { get; set; }
        string aAltNm2 { get; set; }
        Sensitive<string> aAltNm2AccNum { get; set; }
        string aAltNm2CreditorNm { get; set; }
        string aBDecForeignNational { get; set; }
        E_TriState aHas1stTimeBuyerTri { get; set; }
        bool aHasMortgageLia { get; }
        string aIdResolution { get; set; }
        decimal aPres1stM { get; set; }
        decimal aPresHoAssocDues { get; set; }
        decimal aPresOFin { get; set; }
        string aPresOHExpDesc { get; set; }
        decimal aPresOHExpPeval { get; set; }
        decimal aPresRent { get; set; }
        decimal aPresTotHExpCalc { get; }
        decimal aPresTotHExpData { get; set; }
        string aPresTotHExpDesc { get; set; }
        bool aPresTotHExpLckd { get; set; }
        string aU1DocStatDesc { get; set; }
        string aU1DocStatN { get; set; }
        CDateTime aU1DocStatOd { get; set; }
        CDateTime aU1DocStatRd { get; set; }
        string aU2DocStatDesc { get; set; }
        CDateTime aU2DocStatDueD { get; set; }
        string aU2DocStatN { get; set; }
        CDateTime aU2DocStatOd { get; set; }
        string aVaApplyOneTimeRestorationCityState { get; set; }
        string aVaApplyOneTimeRestorationDateOfLoan { get; set; }
        string aVaApplyOneTimeRestorationStAddr { get; set; }
        E_TriState aVaApplyOneTimeRestorationTri { get; set; }
        string aVaApplyRefiNoCashoutCityState { get; set; }
        string aVaApplyRefiNoCashoutDateOfLoan { get; set; }
        string aVaApplyRefiNoCashoutStAddr { get; set; }
        E_TriState aVaApplyRefiNoCashoutTri { get; set; }
        string aVaApplyRegularRefiCashoutCityState { get; set; }
        string aVaApplyRegularRefiCashoutDateOfLoan { get; set; }
        string aVaApplyRegularRefiCashoutStAddr { get; set; }
        E_TriState aVaApplyRegularRefiCashoutTri { get; set; }
        string aVaClaimFolderNum { get; set; }
        string aVaClaimNum { get; set; }
        E_TriState aVaCrRecordSatisfyTri { get; set; }
        bool aVaDischargedDisabilityIs { get; set; }
        decimal aVaEntitleAmt { get; set; }
        string aVaEntitleCode { get; set; }
        decimal aVaFamilySuportGuidelineAmt { get; set; }
        E_TriState aVAFileClaimDisabilityCertifyTri { get; set; }
        E_TriState aVaIndebtCertifyTri { get; set; }
        E_TriState aVaIsVeteranFirstTimeBuyerTri { get; set; }
        E_aVaMilitaryStatT aVaMilitaryStatT { get; set; }
        bool aVaNotDischargedOrReleasedSinceCertOfEligibility { get; set; }
        E_aVaOwnOtherVaLoanT aVaOwnOtherVaLoanT { get; set; }
        string aVaServedAltName { get; set; }
        bool aVaServedAltNameIs { get; set; }
        E_aVaServiceBranchT aVaServiceBranchT { get; set; }
        string aVaServiceNum { get; set; }
        E_TriState aVaUtilityIncludedTri { get; set; }
        E_TriState aWereActiveMilitaryDutyDayAfterTri { get; set; }
    }
}
