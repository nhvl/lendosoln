﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides an interface of <see cref="IPathableStringFormatter"/> for a list of <see cref="LiabilityPmlAuditTrailEvent"/> instances.
    /// </summary>
    public class PmlAuditTrailStringFormatter : DataAccess.PathDispatch.IPathResolvableCollection, IPathableNestedStringFormatter<List<LiabilityPmlAuditTrailEvent>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PmlAuditTrailStringFormatter"/> class.
        /// </summary>
        /// <param name="instance">The underlying instance this string formatter wraps.</param>
        /// <param name="formatter">The formatter to apply to output values.</param>
        /// <param name="parser">The parser to convert input values.</param>
        public PmlAuditTrailStringFormatter(List<LiabilityPmlAuditTrailEvent> instance, IFormatAsString formatter, IParseFromString parser)
        {
            this.Instance = instance;
            this.Formatter = formatter;
            this.Parser = parser;
        }

        /// <summary>
        /// Gets the underlying instance this string formatter wraps.
        /// </summary>
        public List<LiabilityPmlAuditTrailEvent> Instance { get; }

        /// <summary>
        /// Gets the formatter to apply to output values.
        /// </summary>
        public IFormatAsString Formatter { get; }

        /// <summary>
        /// Gets the parser to convert input values.
        /// </summary>
        public IParseFromString Parser { get; }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Instance.CoalesceWithEmpty().Select((e, i) => new DataPathSelectionElement(new SelectIdExpression(i.ToString())));
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException($"{nameof(PmlAuditTrailStringFormatter)} can only handle {nameof(DataPathSelectionElement)}, but passed element of type {element.GetType()}");
            }

            int? maybeIndex = element.Name.ToNullable<int>(int.TryParse);
            if (!maybeIndex.HasValue)
            {
                throw new ArgumentException($"Could not parse '{element.Name}' into a valid identifier.");
            }

            return new PmlAuditTrailEventStringFormatter(this.Instance[maybeIndex.Value], this.Formatter, this.Parser);
        }

        /// <summary>
        /// Sets the value indicated by the path to the given value.
        /// </summary>
        /// <param name="path">The path to the field.</param>
        /// <param name="value">The field value to set.</param>
        public void SetElement(IDataPathElement path, object value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Provides an interface of <see cref="IPathableStringFormatter"/> for an individual instance of <see cref="LiabilityPmlAuditTrailEvent"/>.
        /// </summary>
        private class PmlAuditTrailEventStringFormatter : IPathableNestedStringFormatter<LiabilityPmlAuditTrailEvent>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PmlAuditTrailEventStringFormatter"/> class.
            /// </summary>
            /// <param name="instance">The underlying instance this string formatter wraps.</param>
            /// <param name="formatter">The formatter to apply to output values.</param>
            /// <param name="parser">The parser to convert input values.</param>
            public PmlAuditTrailEventStringFormatter(LiabilityPmlAuditTrailEvent instance, IFormatAsString formatter, IParseFromString parser)
            {
                this.Instance = instance;
                this.Formatter = formatter;
                this.Parser = parser;
            }

            /// <summary>
            /// Gets the underlying instance this string formatter wraps.
            /// </summary>
            public LiabilityPmlAuditTrailEvent Instance { get; }

            /// <summary>
            /// Gets the formatter to apply to output values.
            /// </summary>
            public IFormatAsString Formatter { get; }

            /// <summary>
            /// Gets the parser to convert input values.
            /// </summary>
            public IParseFromString Parser { get; }

            /// <summary>
            /// Returns the value specified by the element.
            /// </summary>
            /// <param name="element">The element.</param>
            /// <returns>The value specified by the element.</returns>
            public object GetElement(IDataPathElement element)
            {
                switch (element.Name.ToLower())
                {
                    case "username": return this.Instance.UserName;
                    case "loginid": return this.Instance.LoginId;
                    case "action": return this.Instance.Action;
                    case "field": return this.Instance.Field;
                    case "value": return this.Instance.Value;
                    case "eventdate": return this.Instance.EventDate;
                    default:
                        throw new ArgumentException($"Invalid field id: {element.Name}.");
                }
            }

            /// <summary>
            /// Sets the value indicated by the path to the given value.
            /// </summary>
            /// <param name="path">The path to the field.</param>
            /// <param name="value">The field value to set.</param>
            public void SetElement(IDataPathElement path, object value)
            {
                throw new NotSupportedException();
            }
        }
    }
}
