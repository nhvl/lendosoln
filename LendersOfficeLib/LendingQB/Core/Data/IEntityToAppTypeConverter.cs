﻿namespace LendingQB.Core.Data
{
    /// <summary>
    /// Provides functionality needed to convert entity semantic types
    /// to the types used by <see cref="IAppBase"/>.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <typeparam name="TApp">The app type.</typeparam>
    internal interface IEntityToAppTypeConverter<TEntity, TApp>
    {
        /// <summary>
        /// Converts the given entity value to the corresponding app value.
        /// </summary>
        /// <param name="entityValue">The entity value.</param>
        /// <returns>The app value.</returns>
        TApp ToAppBaseType(TEntity entityValue);
    }
}
