﻿namespace LendingQB.Core.Data
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Common interface for associations that support the concept of primary ownership.
    /// </summary>
    /// <typeparam name="TValueKind">The data object kind of the owned entity.</typeparam>
    /// <typeparam name="TIdValue">The type of the underlying identifier of the owned entity.</typeparam>
    public interface IPrimaryOwnershipAssociation<TValueKind, TIdValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets the consumer id of the owner.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId { get; }

        /// <summary>
        /// Gets the id of the owned entity.
        /// </summary>
        DataObjectIdentifier<TValueKind, TIdValue> OwnedEntityId { get; }

        /// <summary>
        /// Gets the legacy application id.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this is the primary ownership association for this entity kind.
        /// </summary>
        bool IsPrimary { get; set; }
    }
}
