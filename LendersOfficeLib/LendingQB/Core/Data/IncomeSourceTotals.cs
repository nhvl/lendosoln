﻿namespace LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains summary totals for a set of income sources.
    /// </summary>
    public partial class IncomeSourceTotals
    {
        /// <summary>
        /// Creates totals based on the given  income sources.
        /// </summary>
        /// <param name="items">The items to total up.</param>
        /// <returns>The resulting totals.</returns>
        public static IncomeSourceTotals Create(IEnumerable<IncomeSource> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException();
            }

            var monthlyAmount = Money.Zero;
            foreach (var item in items)
            {
                monthlyAmount += item.MonthlyAmountData ?? Money.Zero;
            }

            return new IncomeSourceTotals(monthlyAmount);
        }
    }
}
