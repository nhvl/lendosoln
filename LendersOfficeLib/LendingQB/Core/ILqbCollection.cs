﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A general interface for read only collections holding business data.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the collection.</typeparam>
    public interface IReadOnlyLqbCollection<TValueKind, TIdValue, TValue> : IReadOnlyDictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>, IPathResolvableCollection
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
    }

    /// <summary>
    /// An association set is a collection of associations.  The (Association, AssociationSet) pairing is very 
    /// similar to the (Entity, Collection) pairing.  This interface is a marker so that association sets can 
    /// be distinguished for collections.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the assocation set.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the association set.</typeparam>
    public interface IReadOnlyLqbAssociationSet<TValueKind, TIdValue, TValue> : IReadOnlyLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
    }

    /// <summary>
    /// Defines a general interface for collections holding business data.
    /// </summary>
    /// <remarks>
    /// This interface is intended to be used internally by the collections framework and not by client code.
    /// </remarks>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the collection.</typeparam>
    public interface IMutableLqbCollection<TValueKind, TIdValue, TValue> : IReadOnlyLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets a value indicating whether the collection has any changes.
        /// </summary>
        bool HasChanges { get; }

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier used to identify the new value.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> Add(TValue value);

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        void Add(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value);

        /// <summary>
        /// Adds a value to the collection, but prevents it from being tracked as an added entity.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        void AddWithoutTracking(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value);

        /// <summary>
        /// Removes the value identified by the id parameter.
        /// </summary>
        /// <param name="id">The identifier of the value to remove.</param>
        void Remove(DataObjectIdentifier<TValueKind, TIdValue> id);

        /// <summary>
        /// Causes the collection to start tracking addition and removal of records.
        /// </summary>
        void BeginTrackingChanges();

        /// <summary>
        /// Gets the updates that have occurred since <see cref="BeginTrackingChanges"/> was called.
        /// </summary>
        /// <returns>A <see cref="LqbCollectionUpdates{TId, TValue}"/> containing the changes that have been made.</returns>
        LqbCollectionChangeRecords<TValueKind, TIdValue, TValue> GetChanges();
    }

    /// <summary>
    /// An association set is a collection of associations.  The (Association, AssociationSet) pairing is very 
    /// similar to the (Entity, Collection) pairing.  This interface is a marker so that association sets can 
    /// be distinguished for collections.
    /// </summary>
    /// <remarks>
    /// This interface is intended to be used internally by the collections framework and not by client code.
    /// </remarks>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the assocation set.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the association set.</typeparam>
    internal interface IMutableLqbAssociationSet<TValueKind, TIdValue, TValue> : IMutableLqbCollection<TValueKind, TIdValue, TValue>, IReadOnlyLqbAssociationSet<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
    }
}
