﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class that keeps track of the order of items in a collection.  The identifiers for the items are held here, not the items themselves.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <remarks>
    /// There are two lines of interface inheritance.  The first is for managing the ordering without modifying the elements, which is safe 
    /// to expose directly to client code.  The second line of inheritance contains all the methods that modify the set of elements, either by 
    /// adding or removing an element.  This cannot be exposed to client code as it must be coordinated with modifications of the actual collection.
    /// Thus, IConstructCollectionOrder should only be held and called by classes that implement collections.
    /// </remarks>
    internal class OrderedIdentifierCollection<TValueKind, TIdValue> : IMutableOrderedIdentifierCollection<TValueKind, TIdValue>, IInitializeCollectionOrder<TIdValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// True when initialization has been done.
        /// </summary>
        private bool initDone;

        /// <summary>
        /// Prevents a default instance of the <see cref="OrderedIdentifierCollection{TValueKind,TIdValue}"/> class from being created.
        /// </summary>
        private OrderedIdentifierCollection()
        {
            this.Items = new LinkedList<DataObjectIdentifier<TValueKind, TIdValue>>();
        }

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return this.Items.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether a change has been made on the order.
        /// </summary>
        public bool HasBeenModified { get; private set; }

        /// <summary>
        /// Gets or sets the list of identifiers.
        /// </summary>
        private LinkedList<DataObjectIdentifier<TValueKind, TIdValue>> Items { get; set; }

        /// <summary>
        /// Create an implementation of the ILqbCollectionOrder interface.
        /// </summary>
        /// <returns>An implementation of the ILqbCollectionOrder interface.</returns>
        public static IOrderedIdentifierCollection<TValueKind, TIdValue> Create()
        {
            return new OrderedIdentifierCollection<TValueKind, TIdValue>();
        }

        /// <summary>
        /// Add the identifier to the end of the collection.
        /// </summary>
        /// <param name="identifier">The identifier getting added.</param>
        public void Add(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            if (this.Items.Contains(identifier))
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Cannot add a duplicate item to the ordering. Id: " + identifier.ToString());
            }

            this.Items.AddLast(identifier);
            this.MarkChange();
        }

        /// <summary>
        /// Add the identifier to the end of the collection.
        /// </summary>
        /// <param name="id">The raw identifier value getting added.</param>
        public void Add(TIdValue id)
        {
            var identifier = DataObjectIdentifier<TValueKind, TIdValue>.Create(id);
            this.Add(identifier);
        }

        /// <summary>
        /// Once the order has been constructed, mark the end of the construction.
        /// </summary>
        public void SetInitializationDone()
        {
            this.initDone = true;
        }

        /// <summary>
        /// Retrieve the identifier at a given position.
        /// </summary>
        /// <param name="position">The position of interest.</param>
        /// <remarks>
        /// Will throw an exception if the position is out of bounds (i.e., not within the range [0, Count]).
        /// </remarks>
        /// <returns>The desired identifier, or throw an exception if the position is out of bounds.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue> GetIdentifierAt(int position)
        {
            if (position < 0 || position >= this.Count)
            {
                throw new IndexOutOfRangeException("OrderedIdentifierCollection.GetIdentifierAt");
            }

            return this.Items.ElementAt(position);
        }

        /// <summary>
        /// Get the enumerator for the collection.
        /// </summary>
        /// <returns>The enumerator for the collection.</returns>
        public IEnumerator<DataObjectIdentifier<TValueKind, TIdValue>> GetEnumerator()
        {
            return this.Items.GetEnumerator();
        }

        /// <summary>
        /// Get the position for the identifier.
        /// </summary>
        /// <param name="identifier">The identifier of interest.</param>
        /// <remarks>
        /// Throws an exception if the identifier is not in the collection.
        /// </remarks>
        /// <returns>The position where the identifier is located in the collection.</returns>
        public int GetPositionOf(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            int index = 0;
            foreach (var item in this.Items)
            {
                if (item.Equals(identifier))
                {
                    return index;
                }

                index++;
            }

            throw new CBaseException("Cannot find item.", "OrderedIdentifierCollection.GetPositionOf has an input identifier that isn't in the collection.");
        }

        /// <summary>
        /// Insert an identifier at a given position.
        /// </summary>
        /// <param name="position">The position where the identifier should be located.</param>
        /// <param name="identifier">The identifier getting inserted.</param>
        /// <remarks>
        /// Will throw an exception if the position is out of bounds (i.e., not within the range [0, Count]).
        /// </remarks>
        public void InsertAt(int position, DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            if (position < 0 || position > this.Count)
            {
                throw new IndexOutOfRangeException("OrderedIdentifierCollection.InsertAt");
            }
            else if (this.Items.Contains(identifier))
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Cannot add a duplicate item to the ordering. Id: " + identifier.ToString());
            }

            if (position == 0)
            {
                this.Items.AddFirst(identifier);
            }
            else if (position == this.Count)
            {
                this.Items.AddLast(identifier);
            }
            else
            {
                int index = 0;
                var node = this.Items.First;
                while (index++ != position)
                {
                    node = node.Next;
                }

                this.Items.AddBefore(node, identifier);
            }

            this.MarkChange();
        }

        /// <summary>
        /// Move the identifier back one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        /// <remarks>
        /// Will throw an exception if the identifier is not in the collection.
        /// </remarks>
        public void MoveBack(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            var node = this.Items.Find(identifier);
            if (node != null && node != this.Items.Last)
            {
                var next = node.Next;
                this.Items.Remove(node);
                this.Items.AddAfter(next, node);
                this.MarkChange();
            }
        }

        /// <summary>
        /// Move the identifier forward one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        /// <remarks>
        /// Will throw an exception if the identifier is not in the collection.
        /// </remarks>
        public void MoveForward(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            var node = this.Items.Find(identifier);
            if (node != null && node != this.Items.First)
            {
                var prev = node.Previous;
                this.Items.Remove(node);
                this.Items.AddBefore(prev, node);
                this.MarkChange();
            }
        }

        /// <summary>
        /// Remove all the entities from the order.
        /// </summary>
        public void Clear()
        {
            if (this.Items.Any())
            {
                this.Items.Clear();
                this.MarkChange();
            }
        }

        /// <summary>
        /// Remove the identifier to the collection.
        /// </summary>
        /// <param name="identifier">The identifier getting removed.</param>
        public void Remove(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            var node = this.Items.Find(identifier);
            if (node != null)
            {
                this.Items.Remove(node);
                this.MarkChange();
            }
        }

        /// <summary>
        /// Modify the order by substituting the previous order with the current order.
        /// </summary>
        /// <param name="previousOrder">The set of identifiers in the old order.</param>
        /// <param name="currentOrder">The set of identifiers in the new order.</param>
        public void ReOrder(DataObjectIdentifier<TValueKind, TIdValue>[] previousOrder, DataObjectIdentifier<TValueKind, TIdValue>[] currentOrder)
        {
            // Perform various sanity checks on the input arrays
            int countPrevious = previousOrder.Count();
            int countCurrent = currentOrder.Count();
            if (countPrevious != countCurrent)
            {
                throw new CBaseException("Invalid state detected.", "Sets of identifiers used in a reordering operation have different counts.");
            }

            var hashSet = new HashSet<DataObjectIdentifier<TValueKind, TIdValue>>();
            for (int i = 0;  i < countPrevious; ++i)
            {
                hashSet.Add(previousOrder[i]);
            }

            if (hashSet.Count != countPrevious)
            {
                throw new CBaseException("Invalid state detected.", "An ordered set of identifiers contained duplicates.");
            }

            for (int i = 0; i < countCurrent; ++i)
            {
                hashSet.Remove(currentOrder[i]);
            }

            if (hashSet.Count > 0)
            {
                throw new CBaseException("Invalid state detected.", "Sets of mismatched identifiers are being used in a reordering operation.");
            }

            // Reorder by removing them and adding in the new order
            foreach (var id in previousOrder)
            {
                this.Remove(id);
            }

            foreach (var id in currentOrder)
            {
                this.Add(id);
            }
        }

        /// <summary>
        /// Get the enumerator of raw values for the collection.
        /// </summary>
        /// <returns>The enumerator of raw values for the collection.</returns>
        IEnumerator<TIdValue> IEnumerable<TIdValue>.GetEnumerator()
        {
            foreach (var identifier in this.Items)
            {
                yield return identifier.Value;
            }
        }

        /// <summary>
        /// Get the enumerator for the collection.
        /// </summary>
        /// <returns>The enumerator for the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.Items.GetEnumerator();
        }
        
        /// <summary>
        /// Set the flag indicating the order has been changed.
        /// </summary>
        private void MarkChange()
        {
            if (!this.initDone)
            {
                this.HasBeenModified = true;
            }
        }
    }
}
