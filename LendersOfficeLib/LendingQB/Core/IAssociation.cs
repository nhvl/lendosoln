﻿namespace LendingQB.Core
{
    /// <summary>
    /// Interface that all associations must implement.
    /// </summary>
    internal interface IAssociation
    {
        /// <summary>
        /// Test an association for whether it's first entity matches this association's first entity.
        /// </summary>
        /// <param name="comparisonAssociation">The association being compared with this association.</param>
        /// <returns>True if the two associations reference the same first entity.</returns>
        bool MatchesFirstEntity(IAssociation comparisonAssociation);

        /// <summary>
        /// Test an association for whether it's second entity matches this association's first entity.
        /// </summary>
        /// <param name="comparisonAssociation">The association being compared with this association.</param>
        /// <returns>True if the two associations reference the same first entity.</returns>
        bool MatchesSecondEntity(IAssociation comparisonAssociation);
    }
}