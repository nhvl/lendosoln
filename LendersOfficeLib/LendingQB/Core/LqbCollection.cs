﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A simple, in-memory collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier to use.</typeparam>
    /// <typeparam name="TValue">The type of the values to store.</typeparam>
    internal class LqbCollection<TValueKind, TIdValue, TValue> : RecordSetBase<TValueKind, TIdValue, TValue>, IMutableLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbCollection{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dict">The dictionary used to initialize the collection.</param>
        private LqbCollection(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict)
            : base(name, idFactory, observer, dict)
        {
        }

        /// <summary>
        /// Creates an implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbCollection<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory)
        {
            return Create(name, idFactory, null);
        }

        /// <summary>
        /// Creates an implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbCollection<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer)
        {
            return Create(name, idFactory, observer, new Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>());
        }

        /// <summary>
        /// Creates an implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dict">The dictionary used to initialize the collection.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbCollection<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict)
        {
            return new LqbCollection<TValueKind, TIdValue, TValue>(name, idFactory, observer, dict);
        }
    }
}
