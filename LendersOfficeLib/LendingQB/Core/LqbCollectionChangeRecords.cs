﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains the changes that have been made to a collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the ID for the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/>.</typeparam>
    /// <typeparam name="TValue">The type of the value for the <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/>.</typeparam>
    public class LqbCollectionChangeRecords<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbCollectionChangeRecords{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="added">The records that have been added.</param>
        /// <param name="updated">The records that have been updated.</param>
        /// <param name="removed">The records that have been removed.</param>
        public LqbCollectionChangeRecords(
            IEnumerable<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> added,
            IEnumerable<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> updated,
            IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> removed)
        {
            this.AddedRecords = added;
            this.UpdatedRecords = updated;
            this.RemovedRecords = removed;
        }

        /// <summary>
        /// Gets the records that were added since the initial snapshot.
        /// </summary>
        public IEnumerable<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> AddedRecords { get; }

        /// <summary>
        /// Gets the records that were updated since the initial snapshot.
        /// </summary>
        public IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> RemovedRecords { get; }

        /// <summary>
        /// Gets the records that were removed since the initial snapshot.
        /// </summary>
        public IEnumerable<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> UpdatedRecords { get; }
    }
}
