﻿namespace LendingQB.Core
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines an interface for observers of <see cref="IMutableLqbCollection{TValueKind, TIdValue, TValue}"/> objects.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the collection.</typeparam>
    internal interface ILqbCollectionObserver<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// A method that is called when an item is added to the collection.
        /// </summary>
        /// <param name="id">The identifier of the newly added item.</param>
        /// <param name="value">The value of the newly added item.</param>
        void NotifyAdd(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value);

        /// <summary>
        /// A method that is called when an item is removed from the collection.
        /// </summary>
        /// <param name="id">The identifier of the recently deleted item.</param>
        /// <param name="value">The value of the recently deleted item.</param>
        void NotifyDelete(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value);
    }
}
