﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// When an ordering is derived from other orderings, this class manages changes to the underlying orderings.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    internal class DerivedOrderedIdentifierCollection<TValueKind, TIdValue> : IOrderedIdentifierCollection<TValueKind, TIdValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DerivedOrderedIdentifierCollection{TValueKind, TIdValue}"/> class.
        /// </summary>
        /// <param name="orders">The base orders upon which this order stands.</param>
        public DerivedOrderedIdentifierCollection(IEnumerable<IOrderedIdentifierCollection<TValueKind, TIdValue>> orders)
        {
            var baseOrders = new List<IOrderedIdentifierCollection<TValueKind, TIdValue>>();
            baseOrders.AddRange(orders);
            this.BaseOrders = baseOrders;
        }

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                int count = 0;
                foreach (var order in this.BaseOrders)
                {
                    count += order.Count;
                }

                return count;
            }
        }

        /// <summary>
        /// Gets or sets the underlying base orderings.
        /// </summary>
        private IEnumerable<IOrderedIdentifierCollection<TValueKind, TIdValue>> BaseOrders { get; set; }

        /// <summary>
        /// Retrieve the type-safe enumerator for the identifiers.
        /// </summary>
        /// <returns>The type-save enumerator for the identifiers.</returns>
        public IEnumerator<DataObjectIdentifier<TValueKind, TIdValue>> GetEnumerator()
        {
            foreach (var order in this.BaseOrders)
            {
                foreach (var item in order)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Retrieve the identifier at a given position.
        /// </summary>
        /// <param name="position">The position of interest.</param>
        /// <remarks>
        /// Will throw an exception if the position is out of bounds (i.e., not within the range [0, Count)).
        /// </remarks>
        /// <returns>The desired identifier, or throw an exception if the position is out of bounds.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue> GetIdentifierAt(int position)
        {
            int count = 0;
            foreach (var order in this.BaseOrders)
            {
                int prevCount = count;
                count += order.Count;
                if (position < count)
                {
                    return order.GetIdentifierAt(position - prevCount);
                }
            }

            throw new IndexOutOfRangeException("DerivedOrderedIdentifierCollection.GetIdentifierAt");
        }

        /// <summary>
        /// Get the position for the identifier.
        /// </summary>
        /// <param name="identifier">The identifier of interest.</param>
        /// <remarks>
        /// Throws an exception if the identifier is not in the collection.
        /// </remarks>
        /// <returns>The position where the identifier is located in the collection.</returns>
        public int GetPositionOf(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            int count = 0;
            foreach (var order in this.BaseOrders)
            {
                foreach (var item in order)
                {
                    if (item == identifier)
                    {
                        return count;
                    }

                    ++count;
                }
            }

            throw new CBaseException("Cannot find item.", "DerivedOrderedIdentifierCollection.GetPositionOf has an input identifier that isn't in the collection.");
        }

        /// <summary>
        /// Move the identifier back one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        /// <remarks>
        /// This will only work within the relevant base order.
        /// </remarks>
        public void MoveBack(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            foreach (var order in this.BaseOrders)
            {
                foreach (var item in order)
                {
                    if (item == identifier)
                    {
                        order.MoveBack(identifier);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Move the identifier forward one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        /// <remarks>
        /// This will only work within the relevant base order.
        /// </remarks>
        public void MoveForward(DataObjectIdentifier<TValueKind, TIdValue> identifier)
        {
            foreach (var order in this.BaseOrders)
            {
                foreach (var item in order)
                {
                    if (item == identifier)
                    {
                        order.MoveForward(identifier);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Since the order is derived, this method will throw an exception.
        /// </summary>
        /// <param name="previousOrder">The set of identifiers in the old order.</param>
        /// <param name="currentOrder">The set of identifiers in the new order.</param>
        public void ReOrder(DataObjectIdentifier<TValueKind, TIdValue>[] previousOrder, DataObjectIdentifier<TValueKind, TIdValue>[] currentOrder)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Retrieve the enumerator for the identifiers that is not type-safe.
        /// </summary>
        /// <returns>The type-save enumerator for the identifiers.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var order in this.BaseOrders)
            {
                foreach (var item in order)
                {
                    yield return item;
                }
            }
        }
    }
}
