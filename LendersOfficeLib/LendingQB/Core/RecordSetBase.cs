﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A simple, in-memory collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier to use.</typeparam>
    /// <typeparam name="TValue">The type of the values to store.</typeparam>
    internal abstract class RecordSetBase<TValueKind, TIdValue, TValue> : IEnumerable
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// The factory used to generate new identifiers.
        /// </summary>
        protected readonly IDataObjectIdentifierFactory<TValueKind, TIdValue> IdFactory;

        /// <summary>
        /// The in-memory data store.
        /// </summary>
        private readonly IDictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict;

        /// <summary>
        /// The observer to notify for certain collection operations.
        /// </summary>
        private readonly ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer;

        /// <summary>
        /// Contains the object ids that were in the collection when <see cref="BeginTrackingChanges"/>
        /// was called.
        /// </summary>
        private HashSet<DataObjectIdentifier<TValueKind, TIdValue>> trackedIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordSetBase{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dict">The dictionary used to initialize the collection.</param>
        protected RecordSetBase(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict)
        {
            this.Name = name;
            this.IdFactory = idFactory;
            this.observer = observer;
            this.dict = dict;
        }

        /// <summary>
        /// Gets the name for display in messages about the collection.
        /// </summary>
        /// <value>The name for display in messages about the collection.</value>
        public Name Name { get; }

        /// <summary>
        /// Gets the number of values in the collection.
        /// </summary>
        /// <value>The number of values in the collection.</value>
        public int Count
        {
            get { return this.dict.Count; }
        }

        /// <summary>
        /// Gets the identifiers of all the values in the collection.
        /// </summary>
        /// <value>The identifiers of all the values in the collection.</value>
        public IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> Keys
        {
            get { return this.dict.Keys; }
        }

        /// <summary>
        /// Gets all the values in the collection.
        /// </summary>
        /// <value>All the values in the collection.</value>
        public IEnumerable<TValue> Values
        {
            get { return this.dict.Values; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection has any changes.
        /// </summary>
        public bool HasChanges
        {
            get
            {
                if (this.trackedIds == null)
                {
                    throw new InvalidOperationException("Cannot call HasChanges before BeginTrackingChanges() is called.");
                }

                foreach (var idValuePair in this)
                {
                    if (!this.trackedIds.Contains(idValuePair.Key))
                    {
                        // items were added
                        return true;
                    }
                    else if (idValuePair.Value.HasChanges)
                    {
                        // items were modified
                        return true;
                    }
                }

                if (this.trackedIds.Except(this.Keys).Any())
                {
                    // items were removed
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the identified value.
        /// </summary>
        /// <param name="id">The identifier for the value to get.</param>
        /// <returns>The value corresponding to the identifier.</returns>
        public TValue this[DataObjectIdentifier<TValueKind, TIdValue> id]
        {
            get
            {
                return this.dict[id];
            }
        }

        /// <summary>
        /// Determines if the collection contains the selected identifier.
        /// </summary>
        /// <param name="id">The identifier to check.</param>
        /// <returns>True if the collection contains the identifier, false otherwise.</returns>
        public bool ContainsKey(DataObjectIdentifier<TValueKind, TIdValue> id)
        {
            return this.dict.ContainsKey(id);
        }

        /// <summary>
        /// Gets the value that is associated with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">
        /// When this method returns, the value associated with the specified key, if they key is found.
        /// Otherwise, the default value for the type of the value parameter.
        /// </param>
        /// <returns>True if the collection contains a value for the specified identifier. Otherwise, false.</returns>
        public bool TryGetValue(DataObjectIdentifier<TValueKind, TIdValue> id, out TValue value)
        {
            return this.dict.TryGetValue(id, out value);
        }

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier used to identify the new value.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue> Add(TValue value)
        {
            bool canAdd = this.CanAdd(value);
            if (!canAdd)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid operation - attempting to add an item to a collection in violation of the business constraints.");
            }

            DataObjectIdentifier<TValueKind, TIdValue> newId = this.IdFactory.NewId();

            this.dict.Add(newId, value);
            this.observer?.NotifyAdd(newId, value);

            return newId;
        }

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public void Add(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value)
        {
            bool canAdd = this.CanAdd(value);
            if (!canAdd)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid operation - attempting to add an item to a collection in violation of the business constraints.");
            }

            this.dict.Add(id, value);
            this.observer?.NotifyAdd(id, value);
        }

        /// <summary>
        /// Adds a value to the collection, but prevents it from being tracked as an added entity.
        /// </summary>
        /// <remarks>
        /// This method is used when an entity creation is handled outside the scope of the automated change tracking, for example
        /// UladApplication and UladApplicationConsumerAssociation.
        /// </remarks>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public void AddWithoutTracking(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value)
        {
            this.Add(id, value);
            this.trackedIds.Add(id);
        }

        /// <summary>
        /// Causes the collection to start tracking addition and removal of records.
        /// </summary>
        public void BeginTrackingChanges()
        {
            if (this.trackedIds != null)
            {
                throw new InvalidOperationException("Cannot call BeginTrackingChanges() multiple times.");
            }

            this.trackedIds = new HashSet<DataObjectIdentifier<TValueKind, TIdValue>>(this.dict.Keys);
        }

        /// <summary>
        /// Gets the updates that have occurred since <see cref="BeginTrackingChanges"/> was called.
        /// </summary>
        /// <returns>A <see cref="LqbCollectionUpdates{TValueKind, TIdValue, TValue}"/> containing the changes that have been made.</returns>
        public LqbCollectionChangeRecords<TValueKind, TIdValue, TValue> GetChanges()
        {
            if (this.trackedIds == null)
            {
                throw new InvalidOperationException("Cannot call GetChanges() before BeginTrackingChanges() is called.");
            }

            var added = new List<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>>();
            var updated = new List<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>>();

            foreach (var idValuePair in this)
            {
                if (!this.trackedIds.Contains(idValuePair.Key))
                {
                    added.Add(idValuePair);
                }
                else if (idValuePair.Value.GetChanges().Any())
                {
                    updated.Add(idValuePair);
                }
            }

            var removed = this.trackedIds.Except(this.Keys);

            return new LqbCollectionChangeRecords<TValueKind, TIdValue, TValue>(added, updated, removed);
        }

        /// <summary>
        /// Removes the value identified by the id parameter.
        /// </summary>
        /// <param name="id">The identifier of the value to remove.</param>
        public void Remove(DataObjectIdentifier<TValueKind, TIdValue> id)
        {
            TValue value;
            bool notify = this.dict.TryGetValue(id, out value);

            this.dict.Remove(id);

            if (notify)
            {
                this.observer?.NotifyDelete(id, value);
            }
        }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Keys.Select(k => new DataPathSelectionElement(new SelectIdExpression(k.Value.ToString())));
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException($"{this.Name} can only handle {nameof(DataPathSelectionElement)}, but passed element of type {element.GetType()}");
            }

            DataObjectIdentifier<TValueKind, TIdValue>? id = this.IdFactory.Create(element.Name);

            if (!id.HasValue)
            {
                throw new ArgumentException($"Could not parse '{element.Name}' into a valid identifier.");
            }

            return this[id.Value];
        }

        /// <summary>
        /// Implement IEnumerable{TId, TValue}.
        /// </summary>
        /// <returns>An enumerator of key-value pairs for the collection.</returns>
        public IEnumerator<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> GetEnumerator()
        {
            return this.dict.GetEnumerator();
        }

        /// <summary>
        /// Implement IEnumerable.
        /// </summary>
        /// <returns>An enumerator of key-value pairs for the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Determine whether the value can be added to the collection.
        /// </summary>
        /// <param name="value">The value that is being added.</param>
        /// <returns>True if the value can be added, false otherwise.</returns>
        protected virtual bool CanAdd(TValue value)
        {
            return true;
        }
    }
}
