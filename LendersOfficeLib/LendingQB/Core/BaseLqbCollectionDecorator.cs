﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A decorator that passes all calls through to the underlying collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The type of the values stored in the collection.</typeparam>
    internal class BaseLqbCollectionDecorator<TValueKind, TIdValue, TValue> : IMutableLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseLqbCollectionDecorator{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        protected BaseLqbCollectionDecorator(IMutableLqbCollection<TValueKind, TIdValue, TValue> contained)
        {
            this.Collection = contained;
        }

        /// <summary>
        /// Gets a value indicating whether the collection has any changes.
        /// </summary>
        public virtual bool HasChanges => this.Collection.HasChanges;

        /// <summary>
        /// Gets the identifiers of all the values in the collection.
        /// </summary>
        /// <value>The identifiers of all the values in the collection.</value>
        public virtual IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> Keys => this.Collection.Keys;

        /// <summary>
        /// Gets all the values in the collection.
        /// </summary>
        /// <value>All the values in the collection.</value>
        public virtual IEnumerable<TValue> Values => this.Collection.Values;

        /// <summary>
        /// Gets the number of values in the collection.
        /// </summary>
        /// <value>The number of values in the collection.</value>
        public virtual int Count => this.Collection.Count;

        /// <summary>
        /// Gets the collection to decorate.
        /// </summary>
        protected IMutableLqbCollection<TValueKind, TIdValue, TValue> Collection { get; }

        /// <summary>
        /// Gets or sets the identified value.
        /// </summary>
        /// <param name="key">The identifier for the value to get.</param>
        /// <returns>The value corresponding to the identifier.</returns>
        public virtual TValue this[DataObjectIdentifier<TValueKind, TIdValue> key] => this.Collection[key];

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier used to identify the new value.</returns>
        public virtual DataObjectIdentifier<TValueKind, TIdValue> Add(TValue value) => this.Collection.Add(value);

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public virtual void Add(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value) => this.Collection.Add(id, value);

        /// <summary>
        /// Adds a value to the collection, but prevents it from being tracked as an added entity.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public virtual void AddWithoutTracking(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value) => this.Collection.AddWithoutTracking(id, value);

        /// <summary>
        /// Causes the collection to start tracking addition and removal of records.
        /// </summary>
        public virtual void BeginTrackingChanges() => this.Collection.BeginTrackingChanges();

        /// <summary>
        /// Determines if the collection contains the selected identifier.
        /// </summary>
        /// <param name="key">The identifier to check.</param>
        /// <returns>True if the collection contains the identifier, false otherwise.</returns>
        public virtual bool ContainsKey(DataObjectIdentifier<TValueKind, TIdValue> key) => this.Collection.ContainsKey(key);

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public virtual IEnumerable<DataPathSelectionElement> GetAllKeys() => this.Collection.GetAllKeys();

        /// <summary>
        /// Gets the updates that have occurred since <see cref="BeginTrackingChanges"/> was called.
        /// </summary>
        /// <returns>A <see cref="LqbCollectionUpdates{TId, TValue}"/> containing the changes that have been made.</returns>
        public virtual LqbCollectionChangeRecords<TValueKind, TIdValue, TValue> GetChanges() => this.Collection.GetChanges();

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public virtual object GetElement(IDataPathElement element) => this.Collection.GetElement(element);

        /// <summary>
        /// Implement IEnumerable{TId, TValue}.
        /// </summary>
        /// <returns>An enumerator of key-value pairs for the collection.</returns>
        public virtual IEnumerator<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> GetEnumerator() => this.Collection.GetEnumerator();

        /// <summary>
        /// Removes the value identified by the id parameter.
        /// </summary>
        /// <param name="id">The identifier of the value to remove.</param>
        public virtual void Remove(DataObjectIdentifier<TValueKind, TIdValue> id) => this.Collection.Remove(id);

        /// <summary>
        /// Gets the value that is associated with the specified identifier.
        /// </summary>
        /// <param name="key">The identifier.</param>
        /// <param name="value">
        /// When this method returns, the value associated with the specified key, if they key is found.
        /// Otherwise, the default value for the type of the value parameter.
        /// </param>
        /// <returns>True if the collection contains a value for the specified identifier. Otherwise, false.</returns>
        public virtual bool TryGetValue(DataObjectIdentifier<TValueKind, TIdValue> key, out TValue value) => this.Collection.TryGetValue(key, out value);

        /// <summary>
        /// Implement IEnumerable{TId, TValue}.
        /// </summary>
        /// <returns>An enumerator of key-value pairs for the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
