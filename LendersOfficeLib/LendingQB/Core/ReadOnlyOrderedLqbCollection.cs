﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Decorator for a read-only ILqbCollection that also manages the order of the items in the collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier to use.</typeparam>
    /// <typeparam name="TValue">The type of the values to store.</typeparam>
    internal class ReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue> : IReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="order">The ordering for the collection.</param>
        private ReadOnlyOrderedLqbCollection(IReadOnlyLqbCollection<TValueKind, TIdValue, TValue> contained, IOrderedIdentifierCollection<TValueKind, TIdValue> order)
        {
            this.Contained = contained;
            this.OrderingInterface = order;

            if (contained.Count != order.Count && order is IMutableOrderedIdentifierCollection<TValueKind, TIdValue>)
            {
                // TODO: Re-enable this logging when we expect the ordering to be in sync.
                ////DataAccess.Tools.LogError("Encountered mismatch between collection records and collection order. Removing extra ids from order and adding missing ids to order.");

                var mutable = (IMutableOrderedIdentifierCollection<TValueKind, TIdValue>)order;
                var irrelevantIdsInOrder = order.Where(id => !contained.ContainsKey(id)).ToList();
                foreach (var irrelevantId in irrelevantIdsInOrder)
                {
                    mutable.Remove(irrelevantId);
                }

                var idsMissingFromOrder = this.Contained.Keys.Where(id => !order.Contains(id));
                foreach (var missingId in idsMissingFromOrder)
                {
                    mutable.Add(missingId);
                }
            }
        }

        /// <inheritdoc />
        public IOrderedIdentifierCollection<TValueKind, TIdValue> OrderingInterface { get; private set; }

        /// <inheritdoc />
        public int Count => this.Contained.Count;

        /// <inheritdoc />
        public IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> Keys => this.OrderingInterface; // inherits from IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> and returns the keys in the current order.

        /// <inheritdoc />
        public IEnumerable<TValue> Values
        {
            get
            {
                foreach (var key in this.Keys)
                {
                    yield return this.Contained[key];
                }
            }
        }

        /// <summary>
        /// Gets or sets the decorated interface.
        /// </summary>
        private IReadOnlyLqbCollection<TValueKind, TIdValue, TValue> Contained { get; set; }

        /// <inheritdoc />
        public TValue this[DataObjectIdentifier<TValueKind, TIdValue> key] => this.Contained[key];

        /// <summary>
        /// Create an implementation of the <see cref="IReadOnlyOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="order">The ordering for the collection.</param>
        /// <returns>An implementation of the <see cref="IReadOnlyOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue> Create(
            IReadOnlyLqbCollection<TValueKind, TIdValue, TValue> contained,
            IOrderedIdentifierCollection<TValueKind, TIdValue> order)
        {
            return new ReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue>(contained, order);
        }

        /// <inheritdoc />
        public bool ContainsKey(DataObjectIdentifier<TValueKind, TIdValue> key)
        {
            return this.Contained.ContainsKey(key);
        }

        /// <inheritdoc />
        public IEnumerator<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> GetEnumerator()
        {
            foreach (var key in this.Keys)
            {
                yield return new KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>(key, this.Contained[key]);
            }
        }

        /// <inheritdoc />
        public bool TryGetValue(DataObjectIdentifier<TValueKind, TIdValue> key, out TValue value)
        {
            return this.Contained.TryGetValue(key, out value);
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <inheritdoc />
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Keys.Select(key => new DataPathSelectionElement(new SelectIdExpression(key.Value.ToString())));
        }

        /// <inheritdoc />
        public object GetElement(IDataPathElement element)
        {
            return this.Contained.GetElement(element);
        }
    }
}
