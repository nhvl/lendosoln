﻿namespace LendingQB.Core.Commands
{
    using System;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to get the asset totals.
    /// </summary>
    public sealed class GetAssetTotals
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAssetTotals"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="uladApplicationId">The ULAD application id.</param>
        /// <param name="legacyApplicationId">The legacy app id.</param>
        /// <param name="consumerId">The consumer id.</param>
        private GetAssetTotals(DataPath path, Guid? uladApplicationId, Guid? legacyApplicationId, Guid? consumerId)
        {
            this.Path = path;
            this.UladApplicationId = uladApplicationId;
            this.LegacyApplicationId = legacyApplicationId;
            this.ConsumerId = consumerId;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the ULAD application identifier.
        /// </summary>
        public Guid? UladApplicationId { get; }

        /// <summary>
        /// Gets the legacy application identifier.
        /// </summary>
        public Guid? LegacyApplicationId { get; }

        /// <summary>
        /// Gets the consumer identifier.
        /// </summary>
        public Guid? ConsumerId { get; }

        /// <summary>
        /// Creates a get asset totals command for the given ULAD application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <returns>A get asset totals command for the given ULAD app.</returns>
        public static GetAssetTotals CreateForLoan(DataPath path)
        {
            return new GetAssetTotals(path, null, null, null);
        }

        /// <summary>
        /// Creates a get asset totals command for the given ULAD application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="uladApplicationId">The ULAD application id.</param>
        /// <returns>A get asset totals command for the given ULAD app.</returns>
        public static GetAssetTotals CreateForUladApplication(DataPath path, Guid uladApplicationId)
        {
            return new GetAssetTotals(path, uladApplicationId, null, null);
        }

        /// <summary>
        /// Creates a get asset totals command for the given legacy app.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The legacy application id.</param>
        /// <returns>A get asset totals command for the given legacy app.</returns>
        public static GetAssetTotals CreateForLegacyApplication(DataPath path, Guid legacyApplicationId)
        {
            return new GetAssetTotals(path, null, legacyApplicationId, null);
        }

        /// <summary>
        /// Creates a get asset totals command for the given consumer.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>A get asset totals command for the given consumer.</returns>
        public static GetAssetTotals CreateForConsumer(DataPath path, Guid consumerId)
        {
            return new GetAssetTotals(path, null, null, consumerId);
        }
    }
}
