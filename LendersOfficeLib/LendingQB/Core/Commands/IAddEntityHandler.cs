﻿namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// An interface for classes that can handle an AddEntity command.
    /// </summary>
    public interface IAddEntityHandler
    {
        /// <summary>
        /// Adds an entity specified by the command.
        /// </summary>
        /// <param name="command">The add command.</param>
        /// <param name="parser">The parser to convert string inputs to typed data.</param>
        /// <returns>The identifier for the newly created entity.</returns>
        Guid HandleAdd(AddEntity command, IParseFromString parser);
    }
}
