﻿namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to remove an association from a collection.
    /// </summary>
    public sealed class RemoveAssociation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoveAssociation"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="associationSet">The name of the association set to remove from.</param>
        /// <param name="id">The identifier for the association.</param>
        public RemoveAssociation(DataPath path, string associationSet, Guid id)
        {
            this.Path = path;
            this.AssociationSet = associationSet;
            this.Id = id;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the collection we want to remove the entity from.
        /// </summary>
        public string AssociationSet { get; }

        /// <summary>
        /// Gets the identifier of the record to remove.
        /// </summary>
        public Guid Id { get; }
    }
}
