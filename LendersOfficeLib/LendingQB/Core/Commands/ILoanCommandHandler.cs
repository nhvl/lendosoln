﻿namespace LendingQB.Core.Commands
{
    using System;
    using Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defined methods for handling various commands that operation on a loan file.
    /// </summary>
    public interface ILoanCommandHandler : IAddEntityHandler, IRemoveEntityHandler
    {
        /// <summary>
        /// Adds an association specified by the command.
        /// </summary>
        /// <param name="command">The add command.</param>
        /// <returns>The identifier for the newly created association.</returns>
        Guid HandleAdd(AddAssociation command);

        /// <summary>
        /// Retrieves the asset totals.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The asset totals requested by the command.</returns>
        AssetTotals HandleGetAssetTotals(GetTotals<DataObjectKind.Asset> command);

        /// <summary>
        /// Retrieves the liability totals.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The liability totals requested by the command.</returns>
        LiabilityTotals HandleGetLiabilityTotals(GetTotals<DataObjectKind.Liability> command);

        /// <summary>
        /// Retrieves the real property totals.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The real property totals requested by the command.</returns>
        RealPropertyTotals HandleGetRealPropertyTotals(GetTotals<DataObjectKind.RealProperty> command);

        /// <summary>
        /// Retrieves the income source totals.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The income source totals requested by the command.</returns>
        IncomeSourceTotals HandleGetIncomeSourceTotals(GetTotals<DataObjectKind.IncomeSource> command);

        /// <summary>
        /// Removes the association specified by the command.
        /// </summary>
        /// <param name="command">The remove command.</param>
        void HandleRemove(RemoveAssociation command);

        /// <summary>
        /// Updates the order for the collection as specified by the command.
        /// </summary>
        /// <param name="command">The set order command.</param>
        void HandleSetOrder(SetOrder command);

        /// <summary>
        /// Adds the liability balance and payment to a real property as specified by the command.
        /// </summary>
        /// <param name="command">The command.</param>
        void HandleAddLiabilityBalanceAndPaymentToRealProperty(AddLiabilityBalanceAndPaymentToRealProperty command);

        /// <summary>
        /// Updates the ownership for the entity as specified by the command.
        /// </summary>
        /// <param name="command">The set ownership command.</param>
        void HandleSetOwnership(SetOwnership command);
    }
}
