﻿namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to add an association, without any populated properties, to an association set.
    /// </summary>
    public sealed class AddAssociation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddAssociation"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="associationSet">The collection name to remove from.</param>
        /// <param name="firstIdentifier">The identifier corresponding to EntityOne of the association.</param>
        /// <param name="secondIdentifier">The identifier corresponding to EntityTwo of the association.</param>
        public AddAssociation(DataPath path, string associationSet, Guid firstIdentifier, Guid secondIdentifier)
        {
            this.Path = path;
            this.AssociationSet = associationSet;
            this.FirstIdentifier = firstIdentifier;
            this.SecondIdentifier = secondIdentifier;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the collection to which a new entity will get added.
        /// </summary>
        public string AssociationSet { get; }

        /// <summary>
        /// Gets the identifier for the first entity.
        /// </summary>
        public Guid FirstIdentifier { get; }

        /// <summary>
        /// Gets the identifier for the second entity.
        /// </summary>
        public Guid SecondIdentifier { get; }
    }
}
