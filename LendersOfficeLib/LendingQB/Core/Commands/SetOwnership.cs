﻿namespace LendingQB.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to set the ownership for an entity.
    /// </summary>
    public class SetOwnership
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetOwnership"/> class.
        /// </summary>
        /// <param name="path">The path to the target.</param>
        /// <param name="recordId">The id of the entity.</param>
        /// <param name="associationName">The association name that needs to be updated.</param>
        /// <param name="primaryOwnerId">The consumer id of the primary owner.</param>
        /// <param name="additionalOwnerIds">The consumer ids of any additional owners.</param>
        public SetOwnership(
            DataPath path,
            Guid recordId,
            string associationName,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.Path = path;
            this.RecordId = recordId;
            this.AssociationName = associationName;
            this.PrimaryOwnerId = primaryOwnerId;
            this.AdditionalOwnerIds = additionalOwnerIds;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the identifier of the record.
        /// </summary>
        public Guid RecordId { get; }

        /// <summary>
        /// Gets the association name we want to remove the entity from.
        /// </summary>
        public string AssociationName { get; }

        /// <summary>
        /// Gets the consumer id of the primary owner.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> PrimaryOwnerId { get; }

        /// <summary>
        /// Gets the consumer ids of any additional owners.
        /// </summary>
        public IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> AdditionalOwnerIds { get; }
    }
}
