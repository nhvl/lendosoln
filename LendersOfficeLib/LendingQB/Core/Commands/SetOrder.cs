﻿namespace LendingQB.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Command to set the loan-level order for a collection.
    /// </summary>
    public class SetOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetOrder"/> class.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="collectionName">The collection name to order.</param>
        /// <param name="uladApplicationId">If not null, the order will be at the level of the ULAD application rather than the loan.</param>
        /// <param name="order">The desired order of the identifiers.</param>
        public SetOrder(DataPath path, string collectionName, Guid? uladApplicationId, IEnumerable<Guid> order)
        {
            this.Path = path;
            this.CollectionName = collectionName;
            this.UladApplicationId = uladApplicationId;
            this.Order = order;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the collection name.
        /// </summary>
        public string CollectionName { get; }

        /// <summary>
        /// Gets the ULAD application identifier, or null.
        /// </summary>
        public Guid? UladApplicationId { get; }

        /// <summary>
        /// Gets the desired order for the collection records.
        /// </summary>
        public IEnumerable<Guid> Order { get; }
    }
}
