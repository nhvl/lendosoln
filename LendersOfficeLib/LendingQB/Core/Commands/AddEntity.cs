﻿namespace LendingQB.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to add an entity, without any populated properties, to a collection.
    /// </summary>
    public sealed class AddEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddEntity"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="collectionName">The collection name to remove from.</param>
        /// <param name="fields">The field name/values passed in with the command.</param>
        public AddEntity(DataPath path, string collectionName, RequestNode fields)
        {
            this.Path = path;
            this.CollectionName = collectionName;
            this.Fields = fields;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddEntity"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="collectionName">The collection name to remove from.</param>
        /// <param name="ownerId">The identifier for the owner of the new entity.</param>
        /// <param name="additionalOwnerIds">The identifiers for the additional owners of the new entity.</param>
        /// <param name="fields">The field name/values passed in with the command.</param>
        /// <param name="index">The index where this record should be added.</param>
        public AddEntity(DataPath path, string collectionName, Guid ownerId, IEnumerable<Guid> additionalOwnerIds, RequestNode fields, int? index)
        {
            this.Path = path;
            this.CollectionName = collectionName;
            this.OwnerId = ownerId;
            this.AdditionalOwnerIds = additionalOwnerIds;
            this.Fields = fields;
            this.Index = index;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the collection to which a new entity will get added.
        /// </summary>
        public string CollectionName { get; }

        /// <summary>
        /// Gets the identifier for the owner of the new entity.
        /// </summary>
        public Guid? OwnerId { get; }

        /// <summary>
        /// Gets the identifiers for the additional owners of the new entity.
        /// </summary>
        public IEnumerable<Guid> AdditionalOwnerIds { get; }

        /// <summary>
        /// Gets the field name/values passed in with the command.
        /// </summary>
        public RequestNode Fields { get; }

        /// <summary>
        /// Gets the index where the new entity should be inserted.
        /// </summary>
        public int? Index { get; }
    }
}
