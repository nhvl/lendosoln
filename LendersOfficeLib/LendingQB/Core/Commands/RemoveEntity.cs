﻿namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to remove an entity from a collection.
    /// </summary>
    public class RemoveEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoveEntity"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="collectionName">The collection name to remove from.</param>
        /// <param name="id">The record identifier.</param>
        public RemoveEntity(DataPath path, string collectionName, Guid id)
        {
            this.Path = path;
            this.CollectionName = collectionName;
            this.Id = id;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the collection we want to remove the entity from.
        /// </summary>
        public string CollectionName { get; }

        /// <summary>
        /// Gets the identifier of the record to remove.
        /// </summary>
        public Guid Id { get; }
    }
}
