﻿namespace LendingQB.Core.Commands
{
    /// <summary>
    /// An interface for classes that can handle a RemoveEntity command.
    /// </summary>
    public interface IRemoveEntityHandler
    {
        /// <summary>
        /// Removes the entity specified by the command.
        /// </summary>
        /// <param name="command">The remove command.</param>
        void HandleRemove(RemoveEntity command);
    }
}
