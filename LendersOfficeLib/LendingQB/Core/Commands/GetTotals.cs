﻿namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to get the totals.
    /// </summary>
    /// <typeparam name="T">The data object kind to retrieve totals.</typeparam>
    public sealed class GetTotals<T> where T : DataObjectKind
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetTotals{T}"/> class.
        /// </summary>
        /// <param name="path">The path to the target object.</param>
        /// <param name="totalsType">The type of the totals to retrieve.</param>
        /// <param name="id">
        /// The identifier for the aggregate we will be using for the total. Use null when retrieving loan-level totals.
        /// </param>
        internal GetTotals(DataPath path, TotalsType totalsType, Guid? id)
        {
            this.Path = path;
            this.TotalsType = totalsType;
            this.Id = id;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the type of totals to retrieve.
        /// </summary>
        public TotalsType TotalsType { get; }

        /// <summary>
        /// Gets the identifier for the aggregate we will be using for the total. Will be null if we're retrieving loan-level totals.
        /// </summary>
        public Guid? Id { get; }
    }
}
