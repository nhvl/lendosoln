﻿namespace LendingQB.Core.Commands
{
    using System;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Defines a command (one of a set of possible operations) for managing borrowers and applications on a loan.
    /// These need to handle their own updates since the legacy code requires us to have custom loan save state logic.
    /// </summary>
    public abstract class BorrowerManagementCommand
    {
        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; private set; }

        /// <summary>
        /// Gets the identifier for the legacy application specified in the command, or null if none was specified.
        /// </summary>
        public Guid? LegacyApplicationId { get; private set; }

        /// <summary>
        /// Gets the full identifier for the legacy application specified in the command, or null if none was specified.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? LegacyAppIdentifier => DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.LegacyApplicationId);

        /// <summary>
        /// Gets the identifier for the ULAD application specified in the command, or null if none was specified.
        /// </summary>
        public Guid? UladApplicationId { get; private set; }

        /// <summary>
        /// Gets the full identifier for the ULAD application specified in the command, or null if none was specified.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? UladAppIdentifier => DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create(this.UladApplicationId);

        /// <summary>
        /// Gets the identifier for the consumer specified in the command, or null if none was specified.
        /// </summary>
        public Guid? ConsumerId { get; private set; }

        /// <summary>
        /// Gets the full identifier for the consumer specified in the command, or null if none was specified.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid>? ConsumerIdentifier => DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.ConsumerId);

        /// <summary>
        /// Creates a borrower management command to set the primary application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The identifier for the legacy application specified in the command, or null if none was specified.</param>
        /// <param name="uladApplicationId">The identifier for the ULAD application specified in the command, or null if none was specified.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateSetPrimaryApplication(DataPath path, Guid? legacyApplicationId, Guid? uladApplicationId)
        {
            return new SetPrimaryApplication
            {
                Path = path,
                LegacyApplicationId = legacyApplicationId,
                UladApplicationId = uladApplicationId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to add a borrower to an application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The identifier for the legacy application specified in the command, or null if none was specified.</param>
        /// <param name="uladApplicationId">The identifier for the ULAD application specified in the command, or null if none was specified.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateAddBorrowerToApplication(DataPath path, Guid? legacyApplicationId, Guid? uladApplicationId)
        {
            return new AddBorrowerToApplication
            {
                Path = path,
                LegacyApplicationId = legacyApplicationId,
                UladApplicationId = uladApplicationId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to add an application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="isLegacyApplication">A value indicating whether the application to add is a legacy application.  If false, it is assumed to be a ULAD application.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateAddApplication(DataPath path, bool isLegacyApplication)
        {
            return new AddApplication
            {
                Path = path,
                LegacyApplicationId = isLegacyApplication ? Guid.Empty : default(Guid?),
                UladApplicationId = isLegacyApplication ? default(Guid?) : Guid.Empty,
            };
        }

        /// <summary>
        /// Creates a borrower management command to delete an application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The identifier for the legacy application specified in the command, or null if none was specified.</param>
        /// <param name="uladApplicationId">The identifier for the ULAD application specified in the command, or null if none was specified.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateDeleteApplication(DataPath path, Guid? legacyApplicationId, Guid? uladApplicationId)
        {
            return new DeleteApplication
            {
                Path = path,
                LegacyApplicationId = legacyApplicationId,
                UladApplicationId = uladApplicationId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to delete a borrower from an application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="consumerId">The identifier for the consumer specified in the command.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateDeleteBorrower(DataPath path, Guid consumerId)
        {
            return new DeleteBorrower
            {
                Path = path,
                ConsumerId = consumerId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to delete a co-borrower from an application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The identifier for the legacy application specified in the command.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateDeleteCoBorrower(DataPath path, Guid legacyApplicationId)
        {
            return new DeleteBorrower
            {
                Path = path,
                LegacyApplicationId = legacyApplicationId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to set a borrower to be the primary borrower on their ULAD application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="consumerId">The identifier for the consumer specified in the command.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateSetPrimaryBorrower(DataPath path, Guid consumerId)
        {
            return new SetPrimaryBorrower
            {
                Path = path,
                ConsumerId = consumerId,
            };
        }

        /// <summary>
        /// Creates a borrower management command to swap the primary-ness of the borrower and co-borrower on a legacy application.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="legacyApplicationId">The identifier for the legacy application specified in the command.</param>
        /// <returns>The command instance to validate and run.</returns>
        public static BorrowerManagementCommand CreateSwapBorrowers(DataPath path, Guid legacyApplicationId)
        {
            return new SwapBorrowers
            {
                Path = path,
                LegacyApplicationId = legacyApplicationId,
            };
        }

        /// <summary>
        /// Validates the specified command, throwing an exception on an invalid command state.
        /// </summary>
        public virtual void ValidateCommand()
        {
            if ((this.LegacyApplicationId.HasValue && this.UladApplicationId.HasValue)
                || (!this.LegacyApplicationId.HasValue && !this.UladApplicationId.HasValue))
            {
                throw this.CreateException("Expected a legacy application id or a ULAD application id.");
            }
        }

        /// <summary>
        /// Handles the application of the command to a particular loan file.  This is the actual execution of the command.
        /// </summary>
        /// <param name="loanId">The identifier of the loan to apply the command to.</param>
        /// <returns>The identifier of a created data point (e.g. a consumer or application identifier, or null if no applicable data point was created.</returns>
        public abstract Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId);

        /// <summary>
        /// Creates an instance of a common exeption.
        /// </summary>
        /// <param name="message">The developer message for the exception.</param>
        /// <returns>The exception instance.</returns>
        protected Exception CreateException(string message) => new DataAccess.CBaseException(ErrorMessages.Generic, message);

        /// <summary>
        /// Represents a command to set the primary application.
        /// </summary>
        private class SetPrimaryApplication : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(loanId.Value, typeof(SetPrimaryApplication));
                loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
                if (this.LegacyAppIdentifier.HasValue)
                {
                    loan.SetPrimaryLegacyApplication(this.LegacyAppIdentifier.Value);
                }
                else
                {
                    loan.SetPrimaryUladApplication(this.UladAppIdentifier.Value);
                }

                loan.Save();
                return null;
            }
        }

        /// <summary>
        /// Represents a command to add a borrower to an application.
        /// </summary>
        private class AddBorrowerToApplication : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId;
                if (this.LegacyAppIdentifier.HasValue)
                {
                    var loan = DataAccess.CPageData.CreateUsingSmartDependency(loanId.Value, typeof(AddBorrowerToApplication));
                    loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
                    consumerId = loan.AddCoborrowerToLegacyApplication(this.LegacyAppIdentifier.Value);
                    loan.Save();
                }
                else
                {
                    consumerId = DataAccess.CPageData.AddConsumerToUladApplication(loanId, this.UladAppIdentifier.Value);
                }

                return consumerId.Value;
            }
        }

        /// <summary>
        /// Represents a command to add an application.
        /// </summary>
        private class AddApplication : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                Guid returnValue;
                if (this.LegacyAppIdentifier.HasValue)
                {
                    returnValue = DataAccess.CPageData.AddBlankLegacyApplication(loanId).Value;
                }
                else
                {
                    returnValue = DataAccess.CPageData.AddBlankUladApplication(loanId).Key.Value;
                }

                return returnValue;
            }
        }

        /// <summary>
        /// Represents a command to delete an application.
        /// </summary>
        private class DeleteApplication : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                if (this.LegacyAppIdentifier.HasValue)
                {
                    DataAccess.CPageData.RemoveLegacyApplication(loanId, this.LegacyAppIdentifier.Value);
                }
                else
                {
                    DataAccess.CPageData.RemoveUladApplication(loanId, this.UladAppIdentifier.Value);
                }

                return null;
            }
        }

        /// <summary>
        /// Represents a command to delete a borrower from an application.
        /// </summary>
        private class DeleteBorrower : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override void ValidateCommand()
            {
                if (!this.ConsumerId.HasValue
                    && !this.LegacyApplicationId.HasValue)
                {
                    throw this.CreateException("Expected a delete borrower command to either specify a consumer id or a legacy application to delete the co-borrower from.");
                }
            }

            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                if (this.ConsumerIdentifier.HasValue)
                {
                    DataAccess.CPageData.RemoveBorrower(loanId, this.ConsumerIdentifier.Value);
                }
                else
                {
                    var loan = DataAccess.CPageData.CreateUsingSmartDependency(loanId.Value, typeof(DeleteBorrower));
                    loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
                    loan.RemoveCoborrowerFromLegacyApplication(this.LegacyAppIdentifier.Value);
                    loan.Save();
                }

                return null;
            }
        }

        /// <summary>
        /// Represents a command to set the borrower on a ULAD application.
        /// </summary>
        private class SetPrimaryBorrower : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override void ValidateCommand()
            {
                if (!this.ConsumerId.HasValue)
                {
                    throw this.CreateException("Expected a set primary borrower command to specify the consumer id to set as primary.");
                }
            }

            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(loanId.Value, typeof(SetPrimaryBorrower));
                loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
                loan.SetPrimaryBorrowerOnUladApplication(this.ConsumerIdentifier.Value);
                loan.Save();
                return null;
            }
        }

        /// <summary>
        /// Represents a command to swap the primary-ness of the borrower and co-borrower on a legacy application.
        /// </summary>
        private class SwapBorrowers : BorrowerManagementCommand
        {
            /// <inheritdoc />
            public override Guid? Handle(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId)
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(loanId.Value, typeof(SwapBorrowers));
                loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
                loan.SwapBorrowers(this.LegacyAppIdentifier.Value);
                loan.Save();
                return null;
            }
        }
    }
}
