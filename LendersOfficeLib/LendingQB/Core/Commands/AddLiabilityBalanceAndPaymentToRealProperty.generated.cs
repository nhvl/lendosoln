﻿// Generated via GenerateRecordClass.ttinclude
namespace LendingQB.Core.Commands
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A command to add the balance and payment from a liability to a real property.
    /// </summary>
    public partial class AddLiabilityBalanceAndPaymentToRealProperty
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddLiabilityBalanceAndPaymentToRealProperty"/> class.
        /// </summary>
        /// <param name="path">The path to the command target.</param>
        /// <param name="liabilityIdentifier">The identifier for the liability.</param>
        /// <param name="realPropertyIdentifier">The identifier for the real property.</param>
        public AddLiabilityBalanceAndPaymentToRealProperty(DataPath path, DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityIdentifier, DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyIdentifier)
        {
            this.Path = path;
            this.LiabilityIdentifier = liabilityIdentifier;
            this.RealPropertyIdentifier = realPropertyIdentifier;
        }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public DataPath Path { get; }

        /// <summary>
        /// Gets the identifier for the liability.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Liability, Guid> LiabilityIdentifier { get; }

        /// <summary>
        /// Gets the identifier for the real property.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.RealProperty, Guid> RealPropertyIdentifier { get; }
    }
}
