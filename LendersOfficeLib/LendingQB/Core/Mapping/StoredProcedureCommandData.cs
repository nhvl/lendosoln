﻿namespace LendingQB.Core.Mapping
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Holds the data needed to execute a stored procedure.
    /// </summary>
    internal class StoredProcedureCommandData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureCommandData"/> class.
        /// </summary>
        /// <param name="name">The stored procedure name.</param>
        /// <param name="parameters">The stored procedure parameters.</param>
        public StoredProcedureCommandData(
            StoredProcedureName name,
            IEnumerable<SqlParameter> parameters)
        {
            this.Name = name;
            this.Parameters = parameters;
        }

        /// <summary>
        /// Gets the stored procedure name to execute.
        /// </summary>
        public StoredProcedureName Name { get; private set; }

        /// <summary>
        /// Gets the parameters to use when executing the stored procedure.
        /// </summary>
        public IEnumerable<SqlParameter> Parameters { get; private set; }
    }
}
