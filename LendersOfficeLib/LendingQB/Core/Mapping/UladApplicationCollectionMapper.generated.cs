﻿// <auto-generated />
namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    internal partial class UladApplicationCollectionMapper : StoredProcedureCollectionMapper<DataObjectKind.UladApplication, Guid, UladApplication>
    {
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;

        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;

        public UladApplicationCollectionMapper(
            DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId,
            DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId,
            IStoredProcedureDriver storedProcedureDriver)
            : base(storedProcedureDriver)
        {
            this.loanId = loanId;
            this.brokerId = brokerId;
        }

        protected override StoredProcedureName CreateStoredProcedureName => StoredProcedureName.Create("ULAD_APPLICATION_Create").Value;

        protected override StoredProcedureName DeleteStoredProcedureName => StoredProcedureName.Create("ULAD_APPLICATION_Delete").Value;

        protected override StoredProcedureName LoadAllRecordsStoredProcedureName => StoredProcedureName.Create("ULAD_APPLICATION_RetrieveByLoanIdBrokerId").Value;

        protected override StoredProcedureName UpdateStoredProcedureName => StoredProcedureName.Create("ULAD_APPLICATION_Update").Value;

        protected override string IdColumnName => "UladApplicationId";

        protected override IEnumerable<SqlParameter> GetFilterSqlParameters()
        {
            return new[]
            {
                new SqlParameter("@LoanId", this.loanId.Value),
                new SqlParameter("@BrokerId", this.brokerId.Value),
            };
        }

        protected override UladApplication CreateRecord(IChangeTrackingDataContainer data)
        {
            return new UladApplication(data);
        }

        protected override IEnumerable<SqlParameter> GetSaveParameters(UladApplication value, ISqlParameterConverter sqlParameterConverter)
        {
            return new[]
            {
                sqlParameterConverter.ToSqlParameter("IsPrimary", value.IsPrimary),
            };
        }

        protected override IDataContainer LoadRecord(IDataReader reader, IDataReaderConverter dataReaderConverter)
        {
            var fieldValues = new Dictionary<string, object>();
            fieldValues["IsPrimary"] = dataReaderConverter.CreateBool(reader["IsPrimary"]);
            return new DictionaryContainer(fieldValues);
        }
        protected override bool ValidateForSave(IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection)
        {
            if (collection.Values.Any() && collection.Values.Count(a => a.IsPrimary) != 1)
            {
                return false;
            }

            return true;
        }
    }
}
