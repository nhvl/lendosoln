﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides functionality for loading and saving a <see cref="IMutableLqbCollection{TIdValue, TValue, TValueKind}"/> from the database.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the ID in the <see cref="IMutableLqbCollection{TIdValue, TValue, TValueKind}"/>.</typeparam>
    /// <typeparam name="TValue">The type of the value in the <see cref="IMutableLqbCollection{TIdValue, TValue, TValueKind}"/>.</typeparam>
    internal interface ILqbCollectionSqlMapper<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Loads a collection from the database.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="collectionName">The name to use for the collection.</param>
        /// <param name="idFactory">The id factory for the collection to use.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dataReaderConverter">Used to convert reader results into specific types.</param>
        /// <returns>The collection that was loaded.</returns>
        IMutableLqbCollection<TValueKind, TIdValue, TValue> Load(
            IDbConnection connection,
            IDbTransaction transaction,
            Name collectionName,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            IDataReaderConverter dataReaderConverter);

        /// <summary>
        /// Saves a collection to the database.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="sqlParameterConverter">The converter to use when generating SQL parameters from the collection records.</param>
        /// <param name="collection">The collection to save.</param>
        void Save(
            IDbConnection connection,
            IDbTransaction transaction,
            ISqlParameterConverter sqlParameterConverter,
            IMutableLqbCollection<TValueKind, TIdValue, TValue> collection);
    }
}
