﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Mapper between json and list of classes.
    /// </summary>
    public class EmploymentRecordEmploymentRecordVoeDataMapper : ICustomFieldMapper<List<EmploymentRecordVOEData>>
    {
        /// <summary>
        /// Creates an object of type List&lt;EmploymentRecordVOEData&gt; from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type List&lt;EmploymentRecordVOEData&gt;.</returns>
        public List<EmploymentRecordVOEData> Create(object value)
        {
            string json = (string)value;
            if (string.IsNullOrEmpty(json))
            {
                return new List<EmploymentRecordVOEData>();
            }
            else
            {
                return SerializationHelper.JsonNetDeserialize<List<EmploymentRecordVOEData>>(json);
            }
        }

        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        public SqlParameter ToSqlParameter(string fieldId, List<EmploymentRecordVOEData> value)
        {
            string json = SerializationHelper.JsonNetSerialize(value);

            var pram = new SqlParameter(fieldId, SqlDbType.VarChar);
            pram.Value = json;
            return pram;
        }
    }
}
