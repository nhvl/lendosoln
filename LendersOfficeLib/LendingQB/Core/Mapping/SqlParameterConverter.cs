﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides conversions from types to <see cref="SqlParameter"/>s.
    /// </summary>
    internal partial class SqlParameterConverter : ISqlParameterConverter
    {
        /// <summary>
        /// Converts an bool to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The enum value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        public SqlParameter ToSqlParameter(string fieldId, bool? value)
        {
            return new SqlParameter(fieldId, value == null ? (object)DBNull.Value : value.Value);
        }

        /// <summary>
        /// Converts an enumerated type to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The enum value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        public SqlParameter ToSqlParameter(string fieldId, Enum value)
        {
            return new SqlParameter(fieldId, value != null ? Convert.ChangeType(value, value.GetTypeCode()) : DBNull.Value);
        }

        /// <summary>
        /// Converts a nullable data object identifier based on the <see cref="Guid"/> type, to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of data object.</typeparam>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        public SqlParameter ToSqlParameter<TValueKind>(string fieldId, DataObjectIdentifier<TValueKind, Guid>? value) where TValueKind : DataObjectKind
        {
            object dbvalue = DBNull.Value;
            if (value != null)
            {
                dbvalue = value.Value.Value;
            }

            var pram = new SqlParameter(fieldId, SqlDbType.UniqueIdentifier);
            pram.Value = dbvalue;
            return pram;
        }

        /// <summary>
        /// Converts a non-nullable data object identifier based on the <see cref="Guid"/> type, to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of data object.</typeparam>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        public SqlParameter ToSqlParameter<TValueKind>(string fieldId, DataObjectIdentifier<TValueKind, Guid> value) where TValueKind : DataObjectKind
        {
            var pram = new SqlParameter(fieldId, SqlDbType.UniqueIdentifier);
            pram.Value = value.Value;
            return pram;
        }
    }
}
