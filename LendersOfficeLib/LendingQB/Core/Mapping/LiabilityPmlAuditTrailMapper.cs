﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Xml;
    using DataAccess;

    /// <summary>
    /// Field mapper to get from liability audit trail list to xml.
    /// </summary>
    internal class LiabilityPmlAuditTrailMapper : ICustomFieldMapper<List<LiabilityPmlAuditTrailEvent>>
    {
        /// <summary>
        /// Creates an object of type List&lt;LiabilityPmlAuditTrailEvent&gt; from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type List&lt;LiabilityPmlAuditTrailEvent&gt;.</returns>
        public List<LiabilityPmlAuditTrailEvent> Create(object value)
        {
            List<LiabilityPmlAuditTrailEvent> list = new List<LiabilityPmlAuditTrailEvent>();

            string xml = (string)value;
            if (!string.IsNullOrEmpty(xml))
            {
                XmlDocument doc = Tools.CreateXmlDoc(xml);
                XmlNodeList nodeList = doc.SelectNodes("//History/Event");
                foreach (XmlElement auditEvent in nodeList)
                {
                    string userName = auditEvent.GetAttribute("UserName");
                    string loginId = auditEvent.GetAttribute("LoginId");
                    string action = auditEvent.GetAttribute("Action");
                    string field = auditEvent.GetAttribute("Field");
                    string eventValue = auditEvent.GetAttribute("Value");
                    string eventDate = auditEvent.GetAttribute("EventDate");

                    list.Add(new LiabilityPmlAuditTrailEvent(userName, loginId, action, field, eventValue, eventDate));
                }
            }

            return list;
        }

        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        public SqlParameter ToSqlParameter(string fieldId, List<LiabilityPmlAuditTrailEvent> value)
        {
            var pram = new SqlParameter(fieldId, SqlDbType.VarChar);

            string databaseValue = this.DatabaseRepresentation(value);

            if (databaseValue == null)
            {
                pram.Value = DBNull.Value;
            }
            else
            {
                pram.Value = databaseValue;
            }

            return pram;
        }

        /// <summary>
        /// Gets the string representation of the audit history that will be used for storage.
        /// </summary>
        /// <param name="value">The audit items.</param>
        /// <returns>The string representation of the items for storage.</returns>
        public string DatabaseRepresentation(List<LiabilityPmlAuditTrailEvent> value)
        {
            if (value == null || value.Count == 0)
            {
                return null;
            }
            else
            {
                var doc = new XmlDocument();
                var rootElement = doc.CreateElement("History");
                doc.AppendChild(rootElement);

                foreach (var item in value)
                {
                    var elem = doc.CreateElement("Event");
                    elem.SetAttribute("UserName", item.UserName);
                    elem.SetAttribute("LoginId", item.LoginId);
                    elem.SetAttribute("Action", item.Action);

                    if (item.Field.Length != 0)
                    {
                        elem.SetAttribute("Field", item.Field);
                    }

                    if (item.Value.Length != 0)
                    {
                        elem.SetAttribute("Value", item.Value);
                    }

                    elem.SetAttribute("EventDate", item.EventDate);
                    rootElement.AppendChild(elem);
                }

                return doc.OuterXml;
            }
        }
    }
}
