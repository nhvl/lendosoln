﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// A database mapper that supplies CRUD operations based on stored procedures.
    /// </summary>
    /// <typeparam name="TValueKind">The kind of kind of the value.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier.</typeparam>
    /// <typeparam name="TValue">The type of the collection item.</typeparam>
    internal abstract class StoredProcedureMapperBase<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// The stored procedure driver to use.
        /// </summary>
        private IStoredProcedureDriver storedProcedureDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureMapperBase{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="storedProcedureDriver">The driver to use when executing stored procedures.</param>
        protected StoredProcedureMapperBase(IStoredProcedureDriver storedProcedureDriver)
        {
            this.storedProcedureDriver = storedProcedureDriver;
        }

        /// <summary>
        /// Gets the stored procedure to use when we are loading all records associated with a loan file.
        /// </summary>
        protected abstract StoredProcedureName LoadAllRecordsStoredProcedureName { get; }

        /// <summary>
        /// Gets the stored procedure to use when we are creating a new record.
        /// </summary>
        protected abstract StoredProcedureName CreateStoredProcedureName { get; }

        /// <summary>
        /// Gets the stored procedure to use when we are updating an existing record.
        /// </summary>
        protected abstract StoredProcedureName UpdateStoredProcedureName { get; }

        /// <summary>
        /// Gets the stored procedure to use when we are deleting an existing record.
        /// </summary>
        protected abstract StoredProcedureName DeleteStoredProcedureName { get; }

        /// <summary>
        /// Gets the column name for the record id.
        /// </summary>
        protected abstract string IdColumnName { get; }

        /// <summary>
        /// Loads a collection from the database.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="collectionName">The name to use for the collection.</param>
        /// <param name="idFactory">The id factory for the collection to use.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dataReaderConverter">The converter to use when parsing data from the data reader.</param>
        /// <returns>The collection that was loaded.</returns>
        public IMutableLqbCollection<TValueKind, TIdValue, TValue> Load(
            IDbConnection connection,
            IDbTransaction transaction,
            Name collectionName,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            IDataReaderConverter dataReaderConverter)
        {
            StoredProcedureName procedureName;
            IEnumerable<SqlParameter> filterParams;
            this.RetrieveLoadCallData(out procedureName, out filterParams);

            using (var reader = this.storedProcedureDriver.ExecuteReader(connection, transaction, procedureName, filterParams))
            {
                return this.Fill(reader, collectionName, idFactory, observer, dataReaderConverter);
            }
        }

        /// <summary>
        /// Extract the data necessary to make the stored procedure call for loading a collection.
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="filterSqlParameters">The parameters passed into the stored procedure.</param>
        public void RetrieveLoadCallData(out StoredProcedureName procedureName, out IEnumerable<SqlParameter> filterSqlParameters)
        {
            procedureName = this.LoadAllRecordsStoredProcedureName;
            filterSqlParameters = this.GetFilterSqlParameters();
        }

        /// <summary>
        /// Populate and return the collection data from the data reader passed in.
        /// </summary>
        /// <param name="reader">The data reader, assumed to be positioned correctly for this collection type.</param>
        /// <param name="collectionName">The name to use for the collection.</param>
        /// <param name="idFactory">The id factory for the collection to use.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dataReaderConverter">The converter to use when parsing data from the data reader.</param>
        /// <returns>The collection that was loaded.</returns>
        public IMutableLqbCollection<TValueKind, TIdValue, TValue> Fill(
            IDataReader reader,
            Name collectionName,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            IDataReaderConverter dataReaderConverter)
        {
            var data = new Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>();

            while (reader.Read())
            {
                DataObjectIdentifier<TValueKind, TIdValue> id = idFactory.Create((TIdValue)reader[this.IdColumnName]);
                IDataContainer originalData = this.LoadRecord(reader, dataReaderConverter);
                TValue value = this.CreateRecord(new ChangeTrackingDataContainer(originalData));
                data.Add(id, value);
            }

            return this.CreateRecord(collectionName, idFactory, observer, data);
        }

        /// <summary>
        /// Saves a collection to the database.
        /// </summary>
        /// <remarks>
        /// Initially, all Create, Update, and Delete stored procedures will get the record id and
        /// the loan id. We may want to add the broker id.
        /// </remarks>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="sqlParameterConverter">The converter to use when generating SQL parameters from the collection records.</param>
        /// <param name="collection">The collection to save.</param>
        public void Save(
            IDbConnection connection,
            IDbTransaction transaction,
            ISqlParameterConverter sqlParameterConverter,
            IMutableLqbCollection<TValueKind, TIdValue, TValue> collection)
        {
            var commands = this.GenerateSaveCommandData(sqlParameterConverter, collection);

            foreach (var add in commands.Creates)
            {
                this.storedProcedureDriver.ExecuteNonQuery(connection, transaction, this.CreateStoredProcedureName, add.Parameters);
            }

            foreach (var update in commands.Updates)
            {
                this.storedProcedureDriver.ExecuteNonQuery(connection, transaction, this.UpdateStoredProcedureName, update.Parameters);
            }

            foreach (var delete in commands.Deletes)
            {
                this.storedProcedureDriver.ExecuteNonQuery(connection, transaction, this.DeleteStoredProcedureName, delete.Parameters);
            }
        }

        /// <summary>
        /// Generates the stored procedure calls that will be needed to save the changes to this collection.
        /// </summary>
        /// <param name="sqlParameterConverter">The converter to use when generating SQL parameters from the collection records.</param>
        /// <param name="collection">The collection to save.</param>
        /// <returns>The commands needed to save the collection.</returns>
        public SaveCommandData GenerateSaveCommandData(
            ISqlParameterConverter sqlParameterConverter,
            IMutableLqbCollection<TValueKind, TIdValue, TValue> collection)
        {
            if (!this.ValidateForSave(collection))
            {
                // GFTODO: Need to discuss the error handling approach when validation fails.
                throw new CBaseException(ErrorMessages.Generic, "Invalid data - a collection contains data that has failed validation.");
            }

            LqbCollectionChangeRecords<TValueKind, TIdValue, TValue> changes = collection.GetChanges();
            var adds = new List<StoredProcedureCommandData>();
            var updates = new List<StoredProcedureCommandData>();
            var deletes = new List<StoredProcedureCommandData>();

            foreach (KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue> idValuePair in changes.AddedRecords)
            {
                IEnumerable<SqlParameter> parameters = this.GetFilterSqlParameters()
                    .Concat(this.GetSaveParameters(idValuePair.Value, sqlParameterConverter))
                    .Concat(new[] { new SqlParameter(this.IdColumnName, idValuePair.Key.Value) });

                var command = new StoredProcedureCommandData(this.CreateStoredProcedureName, parameters);
                adds.Add(command);
            }

            foreach (KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue> idValuePair in changes.UpdatedRecords)
            {
                IEnumerable<SqlParameter> parameters = this.GetFilterSqlParameters()
                    .Concat(this.GetSaveParameters(idValuePair.Value, sqlParameterConverter))
                    .Concat(new[] { new SqlParameter(this.IdColumnName, idValuePair.Key.Value) });

                var command = new StoredProcedureCommandData(this.UpdateStoredProcedureName, parameters);
                updates.Add(command);
            }

            foreach (DataObjectIdentifier<TValueKind, TIdValue> id in changes.RemovedRecords)
            {
                IEnumerable<SqlParameter> parameters = this.GetFilterSqlParameters()
                    .Concat(new[] { new SqlParameter(this.IdColumnName, id.Value) });

                var command = new StoredProcedureCommandData(this.DeleteStoredProcedureName, parameters);
                deletes.Add(command);
            }

            return new SaveCommandData(adds, updates, deletes);
        }

        /// <summary>
        /// Gets a value indicating whether the data is in a valid state for saving.
        /// </summary>
        /// <param name="collection">The collection to save.</param>
        /// <returns>True if the collection is ready to be saved. False otherwise. Defaults to true unless overridden by subclasses.</returns>
        protected virtual bool ValidateForSave(IMutableLqbCollection<TValueKind, TIdValue, TValue> collection)
        {
            return true;
        }

        /// <summary>
        /// Create an instance of the class that implements the correct collection type.
        /// </summary>
        /// <param name="collectionName">The name to use for the collection.</param>
        /// <param name="idFactory">The id factory for the collection to use.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="data">The data extracted from the database.</param>
        /// <returns>An instance of the class that implements the correct collection type.</returns>
        protected abstract IMutableLqbCollection<TValueKind, TIdValue, TValue> CreateRecord(
            Name collectionName,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> data);

        /// <summary>
        /// Loads an <see cref="IDataContainer"/> with the contents required to create a new record.
        /// The data container should be populated with the types that are going to be used by the
        /// entity. For example, if the record is going to use semantic types, the data container
        /// is expected to contain semantic types.
        /// </summary>
        /// <param name="reader">The data reader.</param>
        /// <param name="dataReaderConverter">Used to convert reader results into specific types.</param>
        /// <returns>The data container.</returns>
        protected abstract IDataContainer LoadRecord(IDataReader reader, IDataReaderConverter dataReaderConverter);

        /// <summary>
        /// Creates a record from the data container.
        /// </summary>
        /// <param name="data">The data container.</param>
        /// <returns>The new record.</returns>
        protected abstract TValue CreateRecord(IChangeTrackingDataContainer data);

        /// <summary>
        /// Gets the SQL parameters required to save the given record.
        /// </summary>
        /// <param name="value">The item that will be saved.</param>
        /// <param name="sqlParameterConverter">The converter to use when generating SQL parameters from the collection records.</param>
        /// <returns>The SQL parameters required to save the given record.</returns>
        protected abstract IEnumerable<SqlParameter> GetSaveParameters(TValue value, ISqlParameterConverter sqlParameterConverter);

        /// <summary>
        /// Gets the SqlParameter to for the parent object's id.
        /// </summary>
        /// <returns>The SqlParameter.</returns>
        protected abstract IEnumerable<SqlParameter> GetFilterSqlParameters();
    }
}
