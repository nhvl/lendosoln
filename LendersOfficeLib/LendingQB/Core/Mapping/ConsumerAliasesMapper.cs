﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a way to convert an <see cref="IEnumerable&lt;PersonName&gt;"/> to and from the database.
    /// </summary>
    internal class ConsumerAliasesMapper : ICustomFieldMapper<IEnumerable<PersonName>>
    {
        /// <summary>
        /// Creates an object of type T from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type T.</returns>
        public IEnumerable<PersonName> Create(object value)
        {
            var stringNames = SerializationHelper.JsonNetDeserialize<IEnumerable<string>>((string)value);
            return stringNames.Select(s => PersonName.Create(s).Value);
        }

        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        public SqlParameter ToSqlParameter(string fieldId, IEnumerable<PersonName> value)
        {
            if (value == null)
            {
                return new SqlParameter(fieldId, DBNull.Value);
            }

            foreach (var item in value)
            {
                SqlStringLengthChecker.CheckLength(item);
            }

            string stringValue = SerializationHelper.JsonNetSerialize(value.Select(name => name.ToString()).ToList());
            return new SqlParameter(fieldId, stringValue);
        }
    }
}
