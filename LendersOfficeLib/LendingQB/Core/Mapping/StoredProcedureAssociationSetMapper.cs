﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// A database mapper that supplies CRUD operations based on stored procedures.
    /// </summary>
    /// <typeparam name="TValueKind">The kind of kind of the value.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier.</typeparam>
    /// <typeparam name="TValue">The type of the collection item.</typeparam>
    internal abstract class StoredProcedureAssociationSetMapper<TValueKind, TIdValue, TValue> : StoredProcedureMapperBase<TValueKind, TIdValue, TValue>, ILqbCollectionSqlMapper<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges, IAssociation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureAssociationSetMapper{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="storedProcedureDriver">The driver to use when executing stored procedures.</param>
        public StoredProcedureAssociationSetMapper(IStoredProcedureDriver storedProcedureDriver)
            : base(storedProcedureDriver)
        {
        }

        /// <summary>
        /// Gets the cardinality of the first entity in the association set.
        /// </summary>
        protected abstract int MaxFirstCardinality { get; }

        /// <summary>
        /// Gets the cardinality of the second entity in the association set.
        /// </summary>
        protected abstract int MaxSecondCardinality { get; }

        /// <summary>
        /// Create an instance of the LqbCollection class.
        /// </summary>
        /// <param name="collectionName">The name to use for the collection.</param>
        /// <param name="idFactory">The id factory for the collection to use.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="data">The data extracted from the database.</param>
        /// <returns>An instance of the LqbCollection class.</returns>
        protected override IMutableLqbCollection<TValueKind, TIdValue, TValue> CreateRecord(
            Name collectionName,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> data)
        {
            return LqbAssociationSet<TValueKind, TIdValue, TValue>.Create(collectionName, idFactory, observer, data, this.MaxFirstCardinality, this.MaxSecondCardinality);
        }
    }
}
