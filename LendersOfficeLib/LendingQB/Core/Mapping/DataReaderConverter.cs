﻿namespace LendingQB.Core.Mapping
{
    using System;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides functionality to convert objects from a DataReader into semantic types.
    /// </summary>
    internal class DataReaderConverter : IDataReaderConverter
    {
        /// <summary>
        /// Attempts to create an ApproximateDate from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created ApproximateDate or null if there was an error.</returns>
        public ApproximateDate? CreateApproximateDate(object value)
        {
            return ApproximateDate.CreateFromDatabaseValue((string)value);
        }

        /// <summary>
        /// Attempts to create a BankAccountNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created BankAccountNumber or null if there was an error.</returns>
        public BankAccountNumber? CreateBankAccountNumber(object value)
        {
            return BankAccountNumber.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a bool from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created bool or null if there was an error.</returns>
        public bool? CreateBool(object value)
        {
            return (bool)value;
        }

        /// <summary>
        /// Attempts to create a BooleanKleene from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created BooleanKleene or null if there was an error.</returns>
        public BooleanKleene? CreateBooleanKleene(object value)
        {
            return BooleanKleene.Create((byte)value);
        }

        /// <summary>
        /// Attempts to create a City from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created City or null if there was an error.</returns>
        public City? CreateCity(object value)
        {
            return City.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a CityState from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created CityState or null if there was an error.</returns>
        public CityState? CreateCityState(object value)
        {
            return CityState.Create((string)value);
        }

        /// <summary>
        /// Converts an object to a count of the specified unit.
        /// </summary>
        /// <typeparam name="T">The unit type.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The count value.</returns>
        public Count<T>? CreateCount<T>(object value) where T : UnitType
        {
            return Count<T>.Create((int)value);
        }

        /// <summary>
        /// Attempts to create a Country from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Country or null if there was an error.</returns>
        public Country? CreateCountry(object value)
        {
            return Country.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a CountString from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created CountString or null if there was an error.</returns>
        public CountString? CreateCountString(object value)
        {
            return CountString.Create((string)value);
        }

        /// <summary>
        /// Dummy method that allows everything to compile nicely.  Implement more specific methods.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of object.</typeparam>
        /// <typeparam name="TIdValue">The data type of the id value, specific methods should specifiy this.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The data object identifier.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue>? CreateDataObjectIdentifier<TValueKind, TIdValue>(object value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            if (Convert.IsDBNull(value))
            {
                return null;
            }
            else
            {
                return DataObjectIdentifier<TValueKind, TIdValue>.Create((TIdValue)value);
            }
        }

        /// <summary>
        /// Attempts to create a DependentAges from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created DependentAges or null if there was an error.</returns>
        public DependentAges? CreateDependentAges(object value)
        {
            return DependentAges.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a DescriptionField from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created DescriptionField or null if there was an error.</returns>
        public DescriptionField? CreateDescriptionField(object value)
        {
            return DescriptionField.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a EmailAddress from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created EmailAddress or null if there was an error.</returns>
        public EmailAddress? CreateEmailAddress(object value)
        {
            return EmailAddress.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a EntityName from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created EntityName or null if there was an error.</returns>
        public EntityName? CreateEntityName(object value)
        {
            return EntityName.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a TEnum from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created TEnum or null if there was an error.</returns>
        /// <typeparam name="TEnum">The type of the enumeration.</typeparam>
        public TEnum? CreateEnum<TEnum>(object value) where TEnum : struct
        {
            TEnum enumValue;
            bool ok = Enum.TryParse<TEnum>(value.ToString(), out enumValue);
            if (ok)
            {
                return enumValue;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Attempts to create a FhaCode from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created FhaCode or null if there was an error.</returns>
        public FhaCode? CreateFhaCode(object value)
        {
            return FhaCode.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a FicoCreditScore from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created FicoCreditScore or null if there was an error.</returns>
        public FicoCreditScore? CreateFicoCreditScore(object value)
        {
            return FicoCreditScore.Create((int)value);
        }

        /// <summary>
        /// Attempts to create a IRSIdentifier from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created IRSIdentifier or null if there was an error.</returns>
        public IRSIdentifier? CreateIRSIdentifier(object value)
        {
            return IRSIdentifier.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a LiabilityRemainingMonths from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created LiabilityRemainingMonths or null if there was an error.</returns>
        public LiabilityRemainingMonths? CreateLiabilityRemainingMonths(object value)
        {
            return LiabilityRemainingMonths.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a LongDescriptionField from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created LongDescriptionField or null if there was an error.</returns>
        public LongDescriptionField? CreateLongDescriptionField(object value)
        {
            return LongDescriptionField.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a MilitaryServiceNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created MilitaryServiceNumber or null if there was an error.</returns>
        public MilitaryServiceNumber? CreateMilitaryServiceNumber(object value)
        {
            return MilitaryServiceNumber.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a Money from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Money or null if there was an error.</returns>
        public Money? CreateMoney(object value)
        {
            return Money.Create((decimal)value);
        }

        /// <summary>
        /// Attempts to create a Month from a given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Month or null if there was an error.</returns>
        public Month? CreateMonth(object value)
        {
            int month;
            bool ok = int.TryParse(value.ToString(), out month);
            if (ok)
            {
                return Month.Create(month);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Attempts to create a Name from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Name or null if there was an error.</returns>
        public Name? CreateName(object value)
        {
            return Name.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a NameSuffix from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created NameSuffix or null if there was an error.</returns>
        public NameSuffix? CreateNameSuffix(object value)
        {
            return NameSuffix.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a Percentage from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Percentage or null if there was an error.</returns>
        public Percentage? CreatePercentage(object value)
        {
            return Percentage.Create((decimal)value);
        }

        /// <summary>
        /// Attempts to create a Percentile from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Percentile or null if there was an error.</returns>
        public Percentile? CreatePercentile(object value)
        {
            return Percentile.Create((int)value);
        }

        /// <summary>
        /// Attempts to create a PersonName from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created PersonName or null if there was an error.</returns>
        public PersonName? CreatePersonName(object value)
        {
            return PersonName.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a PhoneNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created PhoneNumber or null if there was an error.</returns>
        public PhoneNumber? CreatePhoneNumber(object value)
        {
            return PhoneNumber.Create((string)value);
        }

        /// <summary>
        /// Converts an object to a quantity of the specified unit.
        /// </summary>
        /// <typeparam name="T">The unit type.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The quantity value.</returns>
        public Quantity<T>? CreateQuantity<T>(object value) where T : UnitType
        {
            return Quantity<T>.Create((decimal)value);
        }

        /// <summary>
        /// Attempts to create a SocialSecurityNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created SocialSecurityNumber or null if there was an error.</returns>
        public SocialSecurityNumber? CreateSocialSecurityNumber(object value)
        {
            return SocialSecurityNumber.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a StreetAddress from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created StreetAddress or null if there was an error.</returns>
        public StreetAddress? CreateStreetAddress(object value)
        {
            return StreetAddress.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a TaxFormNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created TaxFormNumber or null if there was an error.</returns>
        public TaxFormNumber? CreateTaxFormNumber(object value)
        {
            return TaxFormNumber.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a ThirdPartyIdentifier from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created ThirdPartyIdentifier or null if there was an error.</returns>
        public ThirdPartyIdentifier? CreateThirdPartyIdentifier(object value)
        {
            return ThirdPartyIdentifier.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a UnitedStatesPostalState from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created UnitedStatesPostalState or null if there was an error.</returns>
        public UnitedStatesPostalState? CreateUnitedStatesPostalState(object value)
        {
            return UnitedStatesPostalState.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a UnitedStatesState from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created UnitedStatesState or null if there was an error.</returns>
        public UnitedStatesState? CreateUnitedStatesState(object value)
        {
            return UnitedStatesState.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a UnzonedDate from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created UnzonedDate or null if there was an error.</returns>
        public UnzonedDate? CreateUnzonedDate(object value)
        {
            return UnzonedDate.Create((DateTime)value);
        }

        /// <summary>
        /// Attempts to create a VaDateOfLoan from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created VaDateOfLoan or null if there was an error.</returns>
        public VaDateOfLoan? CreateVaDateOfLoan(object value)
        {
            return VaDateOfLoan.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a VaEntitlementCode from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created VaEntitlementCode or null if there was an error.</returns>
        public VaEntitlementCode? CreateVaEntitlementCode(object value)
        {
            return VaEntitlementCode.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a VaLoanNumber from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created VaLoanNumber or null if there was an error.</returns>
        public VaLoanNumber? CreateVaLoanNumber(object value)
        {
            return VaLoanNumber.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a Year from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Year or null if there was an error.</returns>
        public Year? CreateYear(object value)
        {
            return Year.Create(value.ToString());
        }

        /// <summary>
        /// Attempts to create a YearAsString from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created YearAsString or null if there was an error.</returns>
        public YearAsString? CreateYearAsString(object value)
        {
            return YearAsString.Create((string)value);
        }

        /// <summary>
        /// Attempts to create a Zipcode from the given object.
        /// </summary>
        /// <param name="value">The value from the data reader.</param>
        /// <returns>The created Zipcode or null if there was an error.</returns>
        public Zipcode? CreateZipcode(object value)
        {
            return Zipcode.Create((string)value);
        }
    }
}
