﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a way to convert a <see cref="PostalAddress"/> to and from the database.
    /// </summary>
    internal sealed class PostalAddressMapper : ICustomFieldMapper<PostalAddress>
    {
        /// <summary>
        /// Serializes a postal address into a string.
        /// </summary>
        /// <param name="value">The address.</param>
        /// <returns>A serialized string of the address.</returns>
        public static string Serialize(PostalAddress value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            PostalAddressData addressData;
            if (value is GeneralPostalAddress)
            {
                GeneralPostalAddress address = (GeneralPostalAddress)value;
                CheckLengths(address);
                addressData = new PostalAddressData(address);
            }
            else if (value is UnitedStatesPostalAddress)
            {
                UnitedStatesPostalAddress address = (UnitedStatesPostalAddress)value;
                CheckLengths(address);
                addressData = new PostalAddressData(address);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Could not determine the concrete type of the input PostalAddress");
            }

            string stringValue = SerializationHelper.JsonNetSerialize(addressData);
            if (stringValue != null && stringValue.Length > 1024)
            {
                throw new DataTruncationException($"PostalAddress json string is too long. Value length: {stringValue.Length}, Max length: 1024");
            }

            return stringValue;
        }

        /// <summary>
        /// Deserializes a string into a postal address.
        /// </summary>
        /// <param name="value">The serialized address.</param>
        /// <returns>The address, or null if the value deserialized to null.</returns>
        public static PostalAddress Deserialize(string value)
        {
            return SerializationHelper.JsonNetDeserialize<PostalAddressData>(value)?.BuildPostalAddress();
        }

        /// <summary>
        /// Creates an object of type PostalAddress from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type PostalAddress.</returns>
        public PostalAddress Create(object value)
        {
            return Deserialize((string)value);
        }

        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        public SqlParameter ToSqlParameter(string fieldId, PostalAddress value)
        {
            if (value == null)
            {
                return new SqlParameter(fieldId, DBNull.Value);
            }

            string stringValue = Serialize(value);
            var pram = new SqlParameter(fieldId, SqlDbType.VarChar);
            pram.Value = stringValue;
            return pram;
        }

        /// <summary>
        /// Ensure the lengths of every field are good for database storage.
        /// </summary>
        /// <param name="address">The address to check.</param>
        private static void CheckLengths(GeneralPostalAddress address)
        {
            SqlStringLengthChecker.CheckLength(address.City);
            SqlStringLengthChecker.CheckLength(address.CountryCode);
            SqlStringLengthChecker.CheckLength(address.PostalCode);
            SqlStringLengthChecker.CheckLength(address.State);
            SqlStringLengthChecker.CheckLength(address.StreetAddress);
        }

        /// <summary>
        /// Ensure the lengths of every field are good for database storage.
        /// </summary>
        /// <param name="address">The address to check.</param>
        private static void CheckLengths(UnitedStatesPostalAddress address)
        {
            SqlStringLengthChecker.CheckLength(address.AddressNumber);
            SqlStringLengthChecker.CheckLength(address.City);
            SqlStringLengthChecker.CheckLength(address.StreetName);
            SqlStringLengthChecker.CheckLength(address.StreetPostDirection);
            SqlStringLengthChecker.CheckLength(address.StreetPreDirection);
            SqlStringLengthChecker.CheckLength(address.StreetSuffix);
            SqlStringLengthChecker.CheckLength(address.UnitIdentifier);
            SqlStringLengthChecker.CheckLength(address.UnitType);
            SqlStringLengthChecker.CheckLength(address.UnparsedStreetAddress);
            SqlStringLengthChecker.CheckLength(address.UsState);
            SqlStringLengthChecker.CheckLength(address.Zipcode);
        }
    }
}
