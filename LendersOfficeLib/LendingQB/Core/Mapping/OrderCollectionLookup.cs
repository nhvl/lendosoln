﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using CommonProjectLib.Common;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility class that holds order collections based on a naming key system.
    /// </summary>
    internal sealed class OrderCollectionLookup
    {
        /// <summary>
        /// Dictionary of named collection orders.
        /// </summary>
        private Dictionary<OrderKey, IInitializeCollectionOrder<Guid>> collectionOrders;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderCollectionLookup"/> class.
        /// </summary>
        public OrderCollectionLookup()
        {
            this.collectionOrders = new Dictionary<OrderKey, IInitializeCollectionOrder<Guid>>();
        }

        /// <summary>
        /// Gets and enumerator for the collections.
        /// </summary>
        public IEnumerable<KeyValuePair<OrderKey, IInitializeCollectionOrder<Guid>>> OrderCollections
        {
            get
            {
                foreach (var key in this.collectionOrders.Keys)
                {
                    yield return new KeyValuePair<OrderKey, IInitializeCollectionOrder<Guid>>(key, this.collectionOrders[key]);
                }
            }
        }

        /// <summary>
        /// Add a collection order with an explicit name for the collection.
        /// </summary>
        /// <param name="collectionOrder">The collection.</param>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        /// <param name="name">The name for the collection.</param>
        public void AddCollectionOrder(
            IInitializeCollectionOrder<Guid> collectionOrder,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId,
            string name)
        {
            var key = this.CreateKey(legacyApplicationId, consumerId, uladApplicationId, name);
            this.AddCollectionOrder(key, collectionOrder);
        }

        /// <summary>
        /// Add a collection order, allowing the name for the collection to be calculated.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of objects held in the collection.</typeparam>
        /// <param name="collectionOrder">The collection.</param>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        public void AddCollectionOrder<TValueKind>(
            IInitializeCollectionOrder<Guid> collectionOrder,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId)
            where TValueKind : DataObjectKind
        {
            var key = this.CreateKey<TValueKind>(legacyApplicationId, consumerId, uladApplicationId);
            this.AddCollectionOrder(key, collectionOrder);
        }

        /// <summary>
        /// Attempt to retrieve the order.
        /// </summary>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        /// <param name="name">The name for the collection.</param>
        /// <param name="order">The order collection, or null.</param>
        /// <returns>True if the collection is found and returned, false otherwise.</returns>
        public bool TryGetValue(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId,
            string name,
            out IInitializeCollectionOrder<Guid> order)
        {
            order = null;

            var key = this.CreateKey(legacyApplicationId, consumerId, uladApplicationId, name);
            if (this.collectionOrders.ContainsKey(key))
            {
                order = this.collectionOrders[key];
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Attempt to retrieve the order.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of objects held in the collection.</typeparam>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        /// <param name="order">The order collection, or null.</param>
        /// <returns>True if the collection is found and returned, false otherwise.</returns>
        public bool TryGetValue<TValueKind>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId,
            out IInitializeCollectionOrder<Guid> order)
            where TValueKind : DataObjectKind
        {
            order = null;

            var key = this.CreateKey<TValueKind>(legacyApplicationId, consumerId, uladApplicationId);
            if (this.collectionOrders.ContainsKey(key))
            {
                order = this.collectionOrders[key];
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Remove all entries from the lookup dictionary.
        /// </summary>
        public void Clear()
        {
            this.collectionOrders.Clear();
        }

        /// <summary>
        /// Add a collection order with the given lookup key.
        /// </summary>
        /// <param name="key">The lookup key.</param>
        /// <param name="collectionOrder">The collection.</param>
        private void AddCollectionOrder(
            OrderKey key,
            IInitializeCollectionOrder<Guid> collectionOrder)
        {
            if (collectionOrder is IInitializeCollectionOrder<Guid>)
            {
                this.collectionOrders[key] = (IInitializeCollectionOrder<Guid>)collectionOrder;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Attempting to initialize a collection order with a collection that doesn't support initialization.");
            }
        }

        /// <summary>
        /// Create a lookup key with a calculated name for the collection.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of objects held in the collection.</typeparam>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        /// <returns>The lookup key.</returns>
        private OrderKey CreateKey<TValueKind>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId)
            where TValueKind : DataObjectKind
        {
            string name = typeof(TValueKind).Name;
            return this.CreateKey(legacyApplicationId, consumerId, uladApplicationId, name);
        }

        /// <summary>
        /// Create a lookup key with an explicit name for the collection.
        /// </summary>
        /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
        /// <param name="consumerId">The consumer identifier, or null.</param>
        /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
        /// <param name="name">The name of the collection.</param>
        /// <returns>The lookup key.</returns>
        private OrderKey CreateKey(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId,
            string name)
        {
            return new OrderKey(legacyApplicationId, consumerId, uladApplicationId, name);
        }

        /// <summary>
        /// Encapsulate the key for mapping the ordered collections.
        /// </summary>
        public struct OrderKey
        {
            /// <summary>
            /// The key value is a tuple.
            /// </summary>
            private System.Tuple<LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.LegacyApplication, System.Guid>?,
                LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Consumer, System.Guid>?,
                LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.UladApplication, System.Guid>?,
                string> value;

            /// <summary>
            /// Initializes a new instance of the <see cref="OrderKey"/> struct.
            /// </summary>
            /// <param name="legacyApplicationId">The legacy application identifier, or null.</param>
            /// <param name="consumerId">The consumer identifier, or null.</param>
            /// <param name="uladApplicationId">The ULAD application identifier, or null.</param>
            /// <param name="name">The name of the collection.</param>
            internal OrderKey(
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId,
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId,
                DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId,
                string name)
            {
                this.value = Tuple.Create(legacyApplicationId, consumerId, uladApplicationId, name);
            }

            /// <summary>
            /// Gets the legacy application identifier component of the order key.
            /// </summary>
            public Guid? LegacyApplicationId
            {
                get
                {
                    if (this.value.Item1 == null)
                    {
                        return null;
                    }
                    else
                    {
                        return this.value.Item1.Value.Value;
                    }
                }
            }

            /// <summary>
            /// Gets the consumer identifier component of the order key.
            /// </summary>
            public Guid? ConsumerId
            {
                get
                {
                    if (this.value.Item2 == null)
                    {
                        return null;
                    }
                    else
                    {
                        return this.value.Item2.Value.Value;
                    }
                }
            }

            /// <summary>
            /// Gets the ULAD application identifier component of the order key.
            /// </summary>
            public Guid? UladApplicationId
            {
                get
                {
                    if (this.value.Item3 == null)
                    {
                        return null;
                    }
                    else
                    {
                        return this.value.Item3.Value.Value;
                    }
                }
            }

            /// <summary>
            /// Gets the collection name component of the order key.
            /// </summary>
            public string CollectionName
            {
                get { return this.value.Item4; }
            }
        }
    }
}
