﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Xml;
    using DataAccess;

    /// <summary>
    /// Mapper between XML audit trail and list of classes.
    /// </summary>
    internal class PublicRecordAuditTrailMapper : ICustomFieldMapper<List<CPublicRecordAuditItem>>
    {
        /// <summary>
        /// Creates an object of type List&lt;CPublicRecordAuditItem&gt; from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type List&lt;CPublicRecordAuditItem&gt;.</returns>
        public List<CPublicRecordAuditItem> Create(object value)
        {
            List<CPublicRecordAuditItem> list = new List<CPublicRecordAuditItem>();

            string xml = (string)value;
            if (!string.IsNullOrEmpty(xml))
            {
                XmlDocument doc = Tools.CreateXmlDoc(xml);
                XmlNodeList nodeList = doc.SelectNodes("//History/Event");
                foreach (XmlElement auditEvent in nodeList)
                {
                    string userName = auditEvent.GetAttribute("UserName");
                    string eventDate = auditEvent.GetAttribute("EventDate");
                    string description = auditEvent.GetAttribute("Description");
                    list.Add(new CPublicRecordAuditItem(userName, eventDate, description));
                }
            }

            return list;
        }

        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        public SqlParameter ToSqlParameter(string fieldId, List<CPublicRecordAuditItem> value)
        {
            var pram = new SqlParameter(fieldId, SqlDbType.VarChar);

            string dbvalue = this.DatabaseRepresentation(value);
            if (dbvalue == null)
            {
                pram.Value = DBNull.Value;
            }
            else
            {
                pram.Value = dbvalue;
            }

            return pram;
        }

        /// <summary>
        /// Converts the list of audit items into the string representation stored in the database.
        /// </summary>
        /// <param name="value">The list of audit items.</param>
        /// <returns>The string representation of the list of audit items.</returns>
        public string DatabaseRepresentation(List<CPublicRecordAuditItem> value)
        {
            if (value == null || value.Count == 0)
            {
                return null;
            }

            var doc = new XmlDocument();
            var rootElement = doc.CreateElement("History");
            doc.AppendChild(rootElement);

            foreach (var item in value)
            {
                var elem = doc.CreateElement("Event");
                elem.SetAttribute("UserName", item.UserName);
                elem.SetAttribute("EventDate", item.EventDate);
                elem.SetAttribute("Description", item.Description);

                rootElement.AppendChild(elem);
            }

            return doc.OuterXml;
        }
    }
}
