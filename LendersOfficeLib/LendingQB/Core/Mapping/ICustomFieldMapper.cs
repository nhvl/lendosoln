﻿namespace LendingQB.Core.Mapping
{
    using System.Data.SqlClient;

    /// <summary>
    /// Provides custom conversions for database mappers.
    /// </summary>
    /// <typeparam name="T">The type of the object to convert.</typeparam>
    public interface ICustomFieldMapper<T>
    {
        /// <summary>
        /// Converts an object to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>The SqlParameter for the value.</returns>
        SqlParameter ToSqlParameter(string fieldId, T value);

        /// <summary>
        /// Creates an object of type T from an object.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>An object of type T.</returns>
        T Create(object value);
    }
}
