﻿namespace LendingQB.Core.Mapping
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains the commands to save a data set.
    /// </summary>
    internal class SaveCommandData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveCommandData"/> class.
        /// </summary>
        /// <param name="creates">The commands to create new records.</param>
        /// <param name="updates">The commands to update existing records.</param>
        /// <param name="deletes">The commands to delete records.</param>
        public SaveCommandData(
            IReadOnlyList<StoredProcedureCommandData> creates,
            IReadOnlyList<StoredProcedureCommandData> updates,
            IReadOnlyList<StoredProcedureCommandData> deletes)
        {
            this.Creates = creates;
            this.Updates = updates;
            this.Deletes = deletes;
        }

        /// <summary>
        /// Gets the commands to create new records.
        /// </summary>
        public IReadOnlyList<StoredProcedureCommandData> Creates { get; private set; }

        /// <summary>
        /// Gets the commands to update existing records.
        /// </summary>
        public IReadOnlyList<StoredProcedureCommandData> Updates { get; private set; }

        /// <summary>
        /// Gets the commands to delete records.
        /// </summary>
        public IReadOnlyList<StoredProcedureCommandData> Deletes { get; private set; }
    }
}
