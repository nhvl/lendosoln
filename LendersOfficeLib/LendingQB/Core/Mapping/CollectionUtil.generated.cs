﻿// <auto-generated />
namespace LendingQB.Core.Mapping
{
	using System;
	using DataAccess;
    using LendersOffice.Common;
	using LqbGrammar.DataTypes;

	internal static class CollectionUtil
	{
		private static readonly string[] collectionNames = new string[]
		{
				"Asset",
				"Consumer",
				"CounselingEvent",
				"EmploymentRecord",
				"IncomeSource",
				"HousingHistoryEntry",
				"LegacyApplication",
				"Liability",
				"PublicRecord",
				"RealProperty",
				"UladApplication",
				"VaPreviousLoan",
				"VorRecord",
		};

		public static string[] GetCollectionNames()
		{
			return collectionNames;
		}

		public static IInitializeCollectionOrder<TIdValue> CreateCollectionOrder<TIdValue>(string collectionName)
			where TIdValue : struct, IEquatable<TIdValue>
		{
			switch (collectionName)
			{
				case "Asset":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.Asset, TIdValue>.Create();
				case "Consumer":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.Consumer, TIdValue>.Create();
				case "CounselingEvent":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.CounselingEvent, TIdValue>.Create();
				case "EmploymentRecord":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.EmploymentRecord, TIdValue>.Create();
				case "IncomeSource":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.IncomeSource, TIdValue>.Create();
				case "HousingHistoryEntry":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.HousingHistoryEntry, TIdValue>.Create();
				case "LegacyApplication":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.LegacyApplication, TIdValue>.Create();
				case "Liability":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.Liability, TIdValue>.Create();
				case "PublicRecord":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.PublicRecord, TIdValue>.Create();
				case "RealProperty":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.RealProperty, TIdValue>.Create();
				case "UladApplication":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.UladApplication, TIdValue>.Create();
				case "VaPreviousLoan":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.VaPreviousLoan, TIdValue>.Create();
				case "VorRecord":
					return (IInitializeCollectionOrder<TIdValue>)OrderedIdentifierCollection<DataObjectKind.VorRecord, TIdValue>.Create();
				default:
					throw new CBaseException(ErrorMessages.Generic, "Unrecognized collection name " + collectionName == null ? "NULL" : collectionName);
			}
		}
	}
}
