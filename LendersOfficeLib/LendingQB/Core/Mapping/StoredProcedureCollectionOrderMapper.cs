﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    using OrderKey = System.Tuple<LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.LegacyApplication, System.Guid>?,
        LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Consumer, System.Guid>?,
        string>;

    /// <summary>
    /// Mapper that loads and saves the order for collections.
    /// </summary>
    internal sealed class StoredProcedureCollectionOrderMapper
    {
        /// <summary>
        /// Format string used to serialize/deserialize guids.
        /// </summary>
        private const string DatabaseGuidFormat = "D";

        /// <summary>
        /// Delimiter for the database SortOrder column, expressed as an array with one element since that simplifies the code below.
        /// </summary>
        private static readonly char[] DatabaseDelimiter = new char[] { ',' };

        /// <summary>
        /// Name of the stored procedure used to update a record for a loan.
        /// </summary>
        private static readonly StoredProcedureName InsertOrUpdateStoredProcedureName = StoredProcedureName.Create("LOAN_COLLECTION_SORT_ORDER_InsertOrUpdate").Value;

        /// <summary>
        /// Name of the stored procedure used to delete a record for a loan.
        /// </summary>
        private static readonly StoredProcedureName DeleteStoredProcedureName = StoredProcedureName.Create("LOAN_COLLECTION_SORT_ORDER_Delete").Value;

        /// <summary>
        /// Name of the stored procedure used to retrieve all the records for a loan.
        /// </summary>
        private static readonly StoredProcedureName LoadAllRecordsStoredProcedureName = StoredProcedureName.Create("LOAN_COLLECTION_SORT_ORDER_RetrieveByLoanId").Value;

        /// <summary>
        /// The loan identifier to which the collections are attached.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;

        /// <summary>
        /// The stored procedure driver to use.
        /// </summary>
        private IStoredProcedureDriver storedProcedureDriver;

        /// <summary>
        /// Dictionary of named collection orders.
        /// </summary>
        private OrderCollectionLookup collectionOrders;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureCollectionOrderMapper"/> class.
        /// </summary>
        /// <param name="loanId">The loan identifier to which the collections are attached.</param>
        /// <param name="storedProcedureDriver">The driver to use when executing stored procedures.</param>
        public StoredProcedureCollectionOrderMapper(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId, IStoredProcedureDriver storedProcedureDriver)
        {
            this.loanId = loanId;
            this.storedProcedureDriver = storedProcedureDriver;
            this.collectionOrders = new OrderCollectionLookup();
        }

        /// <summary>
        /// Add a collection order prior to saving the full set.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection to return.</typeparam>
        /// <param name="collectionOrder">The collection order which will be saved. If null, then the collection order row will be created.</param>
        /// <param name="legacyApplicationId">The legacy application identifier for the collection. Use null if the order is not specific to the application.</param>
        /// <param name="consumerId">The consumerId for the collection. Use null if the order is not specific to the consumer.</param>
        /// <param name="uladApplicationId">The ULAD application identifier for the collection. Use null if the order is not specific to the application.</param>
        public void AddCollectionOrder<TValueKind>(
            IOrderedIdentifierCollection<TValueKind, Guid> collectionOrder,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId = null,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId = null,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId = null)
            where TValueKind : DataObjectKind
        {
            if (collectionOrder is IInitializeCollectionOrder<Guid>)
            {
                this.collectionOrders.AddCollectionOrder<TValueKind>((IInitializeCollectionOrder<Guid>)collectionOrder, legacyApplicationId, consumerId, uladApplicationId);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid operation - input ordered identifier collection doesn't support the initialization interface.");
            }
        }

        /// <summary>
        /// Load all the collection orders for the loan.
        /// </summary>
        /// <param name="connection">The connection to the database.</param>
        /// <param name="transaction">The current database transaction, or null.</param>
        public void Load(IDbConnection connection, IDbTransaction transaction)
        {
            StoredProcedureName procedureName;
            IEnumerable<SqlParameter> prams;
            this.RetrieveLoadCallData(out procedureName, out prams);

            using (var reader = this.storedProcedureDriver.ExecuteReader(connection, transaction, procedureName, prams))
            {
                this.Fill(reader);
            }
        }

        /// <summary>
        /// Extract the data necessary to make the stored procedure call for loading all the orders.
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="filterSqlParameters">The parameters passed into the stored procedure.</param>
        public void RetrieveLoadCallData(out StoredProcedureName procedureName, out IEnumerable<SqlParameter> filterSqlParameters)
        {
            procedureName = LoadAllRecordsStoredProcedureName;
            filterSqlParameters = new SqlParameter[] { new SqlParameter("@LoanId", this.loanId.Value) };
        }

        /// <summary>
        /// Retrieve the sql commmands needed to save orderings that are empty.
        /// </summary>
        /// <returns>The sql commmands needed to save orderings that are empty.</returns>
        public List<StoredProcedureCommandData> RetrieveSaveEmptyOrdersCallData()
        {
            var list = new List<StoredProcedureCommandData>();
            foreach (var keyValue in this.collectionOrders.OrderCollections)
            {
                if (!IsEmpty(keyValue))
                {
                    continue;
                }

                var parameters = GetRowModificationParameters(this.loanId, keyValue.Key, null);
                list.Add(new StoredProcedureCommandData(DeleteStoredProcedureName, parameters));
            }

            return list;
        }

        /// <summary>
        /// Retrieve the sql commmands needed to save orderings that are not empty.
        /// </summary>
        /// <returns>The sql commmands needed to save orderings that are not empty.</returns>
        public List<StoredProcedureCommandData> RetrieveSaveOrdersCallData()
        {
            var list = new List<StoredProcedureCommandData>();
            foreach (var keyValue in this.collectionOrders.OrderCollections)
            {
                var key = keyValue.Key;
                var collectionOrder = keyValue.Value;
                if (IsEmpty(keyValue) || !collectionOrder.HasBeenModified)
                {
                    continue;
                }

                var sb = new StringBuilder();
                foreach (var identifier in collectionOrder)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(DatabaseDelimiter);
                    }

                    sb.Append(identifier.ToString(DatabaseGuidFormat));
                }

                var parameters = GetRowModificationParameters(this.loanId, keyValue.Key, sb.ToString());
                list.Add(new StoredProcedureCommandData(InsertOrUpdateStoredProcedureName, parameters));
            }

            return list;
        }

        /// <summary>
        /// Populate the order data from the data reader passed in.
        /// </summary>
        /// <param name="reader">The data reader, assumed to be positioned correctly for this collection type.</param>
        public void Fill(IDataReader reader)
        {
            this.collectionOrders.Clear();

            while (reader.Read())
            {
                string collectionName = (string)reader["CollectionName"];
                string sortOrder = (string)reader["SortOrder"];

                var databaseAppId = reader["AppId"];
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? applicationId =
                    databaseAppId == DBNull.Value ?
                        (DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>?)null
                        : DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create((Guid)databaseAppId);

                var databaseConsumerId = reader["ConsumerId"];
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId =
                    databaseConsumerId == DBNull.Value ?
                        (DataObjectIdentifier<DataObjectKind.Consumer, Guid>?)null
                        : DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create((Guid)databaseConsumerId);

                var databaseUladId = reader["UladApplicationId"];
                DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladId =
                    databaseUladId == DBNull.Value ?
                        (DataObjectIdentifier<DataObjectKind.UladApplication, Guid>?)null
                        : DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create((Guid)databaseUladId);

                IInitializeCollectionOrder<Guid> orderCollection;
                if (!this.collectionOrders.TryGetValue(applicationId, consumerId, uladId, collectionName, out orderCollection))
                {
                    orderCollection = CollectionUtil.CreateCollectionOrder<Guid>(collectionName);
                    this.collectionOrders.AddCollectionOrder(orderCollection, applicationId, consumerId, uladId, collectionName);
                }

                var identifiers = sortOrder.Split(DatabaseDelimiter, StringSplitOptions.RemoveEmptyEntries);
                foreach (string id in identifiers)
                {
                    var guid = Guid.ParseExact(id, DatabaseGuidFormat);
                    orderCollection.Add(guid);
                }

                orderCollection.SetInitializationDone();
            }
        }

        /// <summary>
        /// Remove the entity from all collection orders.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection to return.</typeparam>
        /// <param name="entityId">The identifier for the entity to remove.</param>
        public void Remove<TValueKind>(DataObjectIdentifier<TValueKind, Guid> entityId)
            where TValueKind : DataObjectKind
        {
            foreach (var keyValue in this.collectionOrders.OrderCollections)
            {
                var collectionOrder = keyValue.Value;
                var order = collectionOrder as IMutableOrderedIdentifierCollection<TValueKind, Guid>;
                if (order != null && order.Contains(entityId))
                {
                    order.Remove(entityId);
                }
            }
        }

        /// <summary>
        /// Remove any orderings defined for the legacy application.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection.</typeparam>
        /// <param name="legacyApplicationId">The identifier for the legacy application.</param>
        public void ClearLegacyApplicationOrders<TValueKind>(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyApplicationId)
            where TValueKind : DataObjectKind
        {
            foreach (var keyValue in this.collectionOrders.OrderCollections.Where(c => c.Key.LegacyApplicationId == legacyApplicationId.Value))
            {
                var collectionOrder = keyValue.Value;
                var order = collectionOrder as IMutableOrderedIdentifierCollection<TValueKind, Guid>;
                if (order != null)
                {
                    order.Clear();
                }
            }
        }

        /// <summary>
        /// Remove any orderings defined for the consumer.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection.</typeparam>
        /// <param name="consumerId">The identifier for the consumer.</param>
        public void ClearConsumerOrders<TValueKind>(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
            where TValueKind : DataObjectKind
        {
            foreach (var keyValue in this.collectionOrders.OrderCollections.Where(c => c.Key.ConsumerId == consumerId.Value))
            {
                var collectionOrder = keyValue.Value;
                var order = collectionOrder as IMutableOrderedIdentifierCollection<TValueKind, Guid>;
                if (order != null)
                {
                    order.Clear();
                }
            }
        }

        /// <summary>
        /// Remove any orderings defined for the legacy application.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection.</typeparam>
        /// <param name="uladApplicationId">The identifier for the ULAD application.</param>
        public void ClearUladApplicationOrders<TValueKind>(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladApplicationId)
            where TValueKind : DataObjectKind
        {
            foreach (var keyValue in this.collectionOrders.OrderCollections.Where(c => c.Key.UladApplicationId == uladApplicationId.Value))
            {
                var collectionOrder = keyValue.Value;
                var order = collectionOrder as IMutableOrderedIdentifierCollection<TValueKind, Guid>;
                if (order != null)
                {
                    order.Clear();
                }
            }
        }

        /// <summary>
        /// Update all the collection orders.
        /// </summary>
        /// <param name="connection">The connection to the database.</param>
        /// <param name="transaction">The current database transaction, or null.</param>
        public void Save(IDbConnection connection, IDbTransaction transaction)
        {
            var list = this.RetrieveSaveOrdersCallData();
            foreach (var item in list)
            {
                this.storedProcedureDriver.ExecuteNonQuery(connection, transaction, item.Name, item.Parameters);
            }
        }

        /// <summary>
        /// Retrieve the collection order using it's name.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of collection to return.</typeparam>
        /// <param name="legacyApplicationId">The legacy application identifier for the collection. Use null if the order is not specific to the application.</param>
        /// <param name="consumerId">The consumerId for the collection. Use null if the order is not specific to the consumer.</param>
        /// <param name="uladApplicationId">The ULAD application identifier for the collection. Use null if the order is not specific to the application.</param>
        /// <returns>The desired collection, or throws an exception.</returns>
        internal IMutableOrderedIdentifierCollection<TValueKind, Guid> RetrieveCollectionOrder<TValueKind>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>? legacyApplicationId = null,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId = null,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid>? uladApplicationId = null)
            where TValueKind : DataObjectKind
        {
            string name = typeof(TValueKind).Name;
            IInitializeCollectionOrder<Guid> collectionOrder;
            if (!this.collectionOrders.TryGetValue<TValueKind>(legacyApplicationId, consumerId, uladApplicationId, out collectionOrder))
            {
                collectionOrder = CollectionUtil.CreateCollectionOrder<Guid>(name);
                this.collectionOrders.AddCollectionOrder<TValueKind>(collectionOrder, legacyApplicationId, consumerId, uladApplicationId);
            }

            if (collectionOrder is IMutableOrderedIdentifierCollection<TValueKind, Guid>)
            {
                return (IMutableOrderedIdentifierCollection<TValueKind, Guid>)collectionOrder;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid operation - the stored ordered identifier collection doesn't support the mutable interface.");
            }
        }

        /// <summary>
        /// Determine whether an order id considered empty and so should be handled by the SaveEmptyOrders method.
        /// </summary>
        /// <param name="keyValuePair">The information about the order.</param>
        /// <returns>True if the order should be handled by the SaveEmptyOrders method, false otherwise.</returns>
        private static bool IsEmpty(KeyValuePair<OrderCollectionLookup.OrderKey, IInitializeCollectionOrder<Guid>> keyValuePair)
        {
            if (keyValuePair.Value.Count() == 0)
            {
                var key = keyValuePair.Key;
                return key.LegacyApplicationId != null || key.UladApplicationId != null || key.ConsumerId != null;
            }

            return false;
        }

        /// <summary>
        /// Generate the array of paramaters to pass into the stored procedure to delete or insert/update a row.
        /// </summary>
        /// <param name="loanId">The loan identifier.</param>
        /// <param name="key">The key used to lookup the order, it contains all the identifiers necessary to determine the database row of interest.</param>
        /// <param name="sortOrder">The sort order for insert/update, or null for a row deletion.</param>
        /// <returns>The array of parameters that will be passed into the stored procedure.</returns>
        private static SqlParameter[] GetRowModificationParameters(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId, OrderCollectionLookup.OrderKey key, string sortOrder)
        {
            var loanIdParam = new SqlParameter("@LoanId", loanId.Value);

            SqlParameter appIdParam;
            if (key.LegacyApplicationId.HasValue)
            {
                appIdParam = new SqlParameter("@AppId", key.LegacyApplicationId.Value);
            }
            else
            {
                appIdParam = new SqlParameter("@AppId", DBNull.Value);
            }

            SqlParameter consumerIdParam;
            if (key.ConsumerId.HasValue)
            {
                consumerIdParam = new SqlParameter("@ConsumerId", key.ConsumerId.Value);
            }
            else
            {
                consumerIdParam = new SqlParameter("@ConsumerId", DBNull.Value);
            }

            SqlParameter uladIdParam;
            if (key.UladApplicationId.HasValue)
            {
                uladIdParam = new SqlParameter("@UladApplicationId", key.UladApplicationId.Value);
            }
            else
            {
                uladIdParam = new SqlParameter("@UladApplicationId", DBNull.Value);
            }

            var collectionNameParam = CreateVarCharSqlParameter("@CollectionName", key.CollectionName);

            if (sortOrder != null)
            {
                var sortOrderParam = CreateVarCharSqlParameter("@SortOrder", sortOrder);
                return new SqlParameter[] { loanIdParam, appIdParam, consumerIdParam, uladIdParam, collectionNameParam, sortOrderParam };
            }
            else
            {
                return new SqlParameter[] { loanIdParam, appIdParam, consumerIdParam, uladIdParam, collectionNameParam };
            }
        }

        /// <summary>
        /// Create an SqlParameter for a varchar.  It is important to set the SqlDbType as otherwise an nvarchar is assumed and there is a conversion penalty.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <returns>The created SqlParameter.</returns>
        private static SqlParameter CreateVarCharSqlParameter(string name, string value)
        {
            var pram = new SqlParameter(name, SqlDbType.VarChar);
            pram.Value = value;
            return pram;
        }
    }
}
