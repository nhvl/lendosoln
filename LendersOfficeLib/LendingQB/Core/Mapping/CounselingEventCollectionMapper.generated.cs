﻿// <auto-generated />
namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    internal partial class CounselingEventCollectionMapper : StoredProcedureCollectionMapper<DataObjectKind.CounselingEvent, Guid, CounselingEvent>
    {
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;

        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;

        public CounselingEventCollectionMapper(
            DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId,
            DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId,
            IStoredProcedureDriver storedProcedureDriver)
            : base(storedProcedureDriver)
        {
            this.loanId = loanId;
            this.brokerId = brokerId;
        }

        protected override StoredProcedureName CreateStoredProcedureName => StoredProcedureName.Create("COUNSELING_EVENT_Create").Value;

        protected override StoredProcedureName DeleteStoredProcedureName => StoredProcedureName.Create("COUNSELING_EVENT_Delete").Value;

        protected override StoredProcedureName LoadAllRecordsStoredProcedureName => StoredProcedureName.Create("COUNSELING_EVENT_RetrieveByLoanIdBrokerId").Value;

        protected override StoredProcedureName UpdateStoredProcedureName => StoredProcedureName.Create("COUNSELING_EVENT_Update").Value;

        protected override string IdColumnName => "CounselingEventId";

        protected override IEnumerable<SqlParameter> GetFilterSqlParameters()
        {
            return new[]
            {
                new SqlParameter("@LoanId", this.loanId.Value),
                new SqlParameter("@BrokerId", this.brokerId.Value),
            };
        }

        protected override CounselingEvent CreateRecord(IChangeTrackingDataContainer data)
        {
            return new CounselingEvent(data);
        }

        protected override IEnumerable<SqlParameter> GetSaveParameters(CounselingEvent value, ISqlParameterConverter sqlParameterConverter)
        {
            return new[]
            {
                sqlParameterConverter.ToSqlParameter("CompletedDate", value.CompletedDate),
                sqlParameterConverter.ToSqlParameter("CounselingType", value.CounselingType),
                sqlParameterConverter.ToSqlParameter("CounselingFormat", value.CounselingFormat),
                sqlParameterConverter.ToSqlParameter("HousingCounselingAgency", value.HousingCounselingAgency),
                sqlParameterConverter.ToSqlParameter("HousingCounselingHudAgencyID", value.HousingCounselingHudAgencyID),
            };
        }

        protected override IDataContainer LoadRecord(IDataReader reader, IDataReaderConverter dataReaderConverter)
        {
            var fieldValues = new Dictionary<string, object>();
            fieldValues["CompletedDate"] = Convert.IsDBNull(reader["CompletedDate"]) ? null : dataReaderConverter.CreateUnzonedDate(reader["CompletedDate"]);
            fieldValues["CounselingType"] = Convert.IsDBNull(reader["CounselingType"]) ? null : dataReaderConverter.CreateEnum<CounselingType>(reader["CounselingType"]);
            fieldValues["CounselingFormat"] = Convert.IsDBNull(reader["CounselingFormat"]) ? null : dataReaderConverter.CreateEnum<CounselingFormatType>(reader["CounselingFormat"]);
            fieldValues["HousingCounselingAgency"] = Convert.IsDBNull(reader["HousingCounselingAgency"]) ? null : dataReaderConverter.CreateEntityName(reader["HousingCounselingAgency"]);
            fieldValues["HousingCounselingHudAgencyID"] = Convert.IsDBNull(reader["HousingCounselingHudAgencyID"]) ? null : dataReaderConverter.CreateThirdPartyIdentifier(reader["HousingCounselingHudAgencyID"]);
            return new DictionaryContainer(fieldValues);
        }
    }
}

