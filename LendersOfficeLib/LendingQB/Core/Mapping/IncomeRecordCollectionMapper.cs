﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Handles serializing and deserializing a collection of income records..
    /// </summary>
    internal class IncomeRecordCollectionMapper : ICustomFieldMapper<IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>>
    {
        /// <summary>
        /// Serializes a collection to a json string.
        /// </summary>
        /// <param name="value">The collection.</param>
        /// <returns>The serialized collection.</returns>
        public static string ToJson(IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }

            var dtoKeyValuePairs = value.Select(kvp => new KeyValuePair<Guid, IncomeRecordDto>(kvp.Key.Value, ToDto(kvp.Value))).ToList();
            return SerializationHelper.JsonNetSerialize(dtoKeyValuePairs);
        }

        /// <summary>
        /// Creates a collection from the given value.
        /// </summary>
        /// <param name="value">The serialized collection string.</param>
        /// <returns>The deserialized collection.</returns>
        public IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> Create(object value)
        {
            var keyValuePairs = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<Guid, IncomeRecordDto>>>((string)value);

            var order = OrderedIdentifierCollection<DataObjectKind.IncomeRecord, Guid>.Create() as IInitializeCollectionOrder<Guid>;
            foreach (var key in keyValuePairs.Select(kvp => kvp.Key))
            {
                order.Add(key);
            }

            order.SetInitializationDone();

            var entityDictionary = keyValuePairs.ToDictionary(kvp => kvp.Key.ToIdentifier<DataObjectKind.IncomeRecord>(), kvp => ToEntity(kvp.Value));
            var collection = LqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(
                Name.Create("IncomeRecords").Value,
                idFactory: new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeRecord>(),
                observer: null,
                dict: entityDictionary);
            return OrderedLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(collection, order as IMutableOrderedIdentifierCollection<DataObjectKind.IncomeRecord, Guid>);
        }

        /// <summary>
        /// Converts a collection to a SqlParameter.
        /// </summary>
        /// <param name="fieldId">The field id for the parameter.</param>
        /// <param name="value">The collection.</param>
        /// <returns>The SqlParameter for the collection.</returns>
        public SqlParameter ToSqlParameter(string fieldId, IMutableLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord> value)
        {
            return new SqlParameter(fieldId, ToJson(value));
        }

        /// <summary>
        /// Converts an income record to a DTO.
        /// </summary>
        /// <param name="source">The income record.</param>
        /// <returns>A DTO for the given income record.</returns>
        private static IncomeRecordDto ToDto(IncomeRecord source)
        {
            return new IncomeRecordDto()
            {
                IncomeType = source.IncomeType,
                MonthlyAmount = (decimal?)source.MonthlyAmount
            };
        }

        /// <summary>
        /// Converts a DTO to an income record.
        /// </summary>
        /// <param name="dto">The DTO to convert.</param>
        /// <returns>The income record from the DTO.</returns>
        private static IncomeRecord ToEntity(IncomeRecordDto dto)
        {
            return new IncomeRecord
            {
                IncomeType = dto.IncomeType,
                MonthlyAmount = dto.MonthlyAmount
            };
        }

        /// <summary>
        /// A DTO for the income record serialization.
        /// </summary>
        private class IncomeRecordDto
        {
            /// <summary>
            /// Gets or sets the income type.
            /// </summary>
            public IncomeType? IncomeType { get; set; }

            /// <summary>
            /// Gets or sets the monthly amount.
            /// </summary>
            public decimal? MonthlyAmount { get; set; }
        }
    }
}
