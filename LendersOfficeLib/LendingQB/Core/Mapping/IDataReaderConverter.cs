﻿namespace LendingQB.Core.Mapping
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a way to convert objects from a data reader into semantic types.
    /// </summary>
    internal partial interface IDataReaderConverter
    {
        /// <summary>
        /// Converts an object to a value of the specified enumeration type.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enumeration.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The enum value.</returns>
        TEnum? CreateEnum<TEnum>(object value) where TEnum : struct;

        /// <summary>
        /// Converts an object to a count of the specified unit.
        /// </summary>
        /// <typeparam name="T">The unit type.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The count value.</returns>
        Count<T>? CreateCount<T>(object value) where T : UnitType;

        /// <summary>
        /// Converts an object to a quantity of the specified unit.
        /// </summary>
        /// <typeparam name="T">The unit type.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The quantity value.</returns>
        Quantity<T>? CreateQuantity<T>(object value) where T : UnitType;

        /// <summary>
        /// Converts an object to a data object identifier that is stored as a Guid.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of object.</typeparam>
        /// <typeparam name="TIdValue">The data type of the identifier's value.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The data object identifier.</returns>
        DataObjectIdentifier<TValueKind, TIdValue>? CreateDataObjectIdentifier<TValueKind, TIdValue>(object value) where TValueKind : DataObjectKind where TIdValue : struct, IEquatable<TIdValue>;
    }
}
