﻿// <auto-generated />
namespace LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    internal partial class ConsumerRealPropertyMapper : StoredProcedureAssociationSetMapper<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation>
    {
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;

        public ConsumerRealPropertyMapper(
            DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId,
            IStoredProcedureDriver storedProcedureDriver)
            : base(storedProcedureDriver)
        {
            this.loanId = loanId;
        }

        protected override StoredProcedureName CreateStoredProcedureName => StoredProcedureName.Create("CONSUMER_X_REAL_PROPERTY_Create").Value;

        protected override StoredProcedureName DeleteStoredProcedureName => StoredProcedureName.Create("CONSUMER_X_REAL_PROPERTY_Delete").Value;

        protected override StoredProcedureName LoadAllRecordsStoredProcedureName => StoredProcedureName.Create("CONSUMER_X_REAL_PROPERTY_RetrieveByLoanId").Value;

        protected override StoredProcedureName UpdateStoredProcedureName => StoredProcedureName.Create("CONSUMER_X_REAL_PROPERTY_Update").Value;

        protected override string IdColumnName => "ConsumerRealPropertyAssociationId";

		protected override int MaxFirstCardinality { get { return -1; } }
		protected override int MaxSecondCardinality { get { return -1; } }

        protected override IEnumerable<SqlParameter> GetFilterSqlParameters()
        {
            return new[]
            {
                new SqlParameter("@LoanId", this.loanId.Value),
            };
        }

        protected override ConsumerRealPropertyAssociation CreateRecord(IChangeTrackingDataContainer data)
        {
            // We will assume that all entities have a constructor that accepts a change tracking
            // data container. If this changes in the future we update the base mapper to accept
            // an entity factory.
            var association = new ConsumerRealPropertyAssociation(data);
			return association;
        }

        protected override IEnumerable<SqlParameter> GetSaveParameters(ConsumerRealPropertyAssociation value, ISqlParameterConverter sqlParameterConverter)
        {
            return new[]
            {
				sqlParameterConverter.ToSqlParameter("ConsumerId", value.ConsumerId),
				sqlParameterConverter.ToSqlParameter("RealPropertyId", value.RealPropertyId),
                sqlParameterConverter.ToSqlParameter("AppId", value.AppId),
                sqlParameterConverter.ToSqlParameter("IsPrimary", value.IsPrimary),
            };
        }

        protected override IDataContainer LoadRecord(IDataReader reader, IDataReaderConverter dataReaderConverter)
        {
            var fieldValues = new Dictionary<string, object>();
            fieldValues["ConsumerId"] = dataReaderConverter.CreateDataObjectIdentifier<DataObjectKind.Consumer, Guid>(reader["ConsumerId"]);
            fieldValues["RealPropertyId"] = dataReaderConverter.CreateDataObjectIdentifier<DataObjectKind.RealProperty, Guid>(reader["RealPropertyId"]);
            fieldValues["AppId"] = dataReaderConverter.CreateDataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>(reader["AppId"]);
            fieldValues["IsPrimary"] = dataReaderConverter.CreateBool(reader["IsPrimary"]);
            return new DictionaryContainer(fieldValues);
        }

        protected override bool ValidateForSave(IMutableLqbCollection<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> collection)
        {
            foreach (var singularGroup in collection.Values.GroupBy(a => a.RealPropertyId))
            {
                if (singularGroup.Count(a => a.IsPrimary) != 1)
                {
                    return false;
                }
            }

            return true;
        }
    }
}

