﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Data.SqlClient;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides conversions from types to <see cref="SqlParameter"/>s.
    /// </summary>
    internal partial interface ISqlParameterConverter
    {
        /// <summary>
        /// Converts an enumerated type to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The enum value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        SqlParameter ToSqlParameter(string fieldId, Enum value);

        /// <summary>
        /// Converts an bool to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The enum value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        SqlParameter ToSqlParameter(string fieldId, bool? value);

        /// <summary>
        /// Converts a nullable data object identifier based on the <see cref="Guid"/> type, to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of data object.</typeparam>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        SqlParameter ToSqlParameter<TValueKind>(string fieldId, DataObjectIdentifier<TValueKind, Guid>? value) where TValueKind : DataObjectKind;

        /// <summary>
        /// Converts a non-nullable data object identifier based on the <see cref="Guid"/> type, to a <see cref="SqlParameter"/>.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of data object.</typeparam>
        /// <param name="fieldId">The name to use for the parameter.</param>
        /// <param name="value">The value to use for the parameter.</param>
        /// <returns>The created <see cref="SqlParameter"/>.</returns>
        SqlParameter ToSqlParameter<TValueKind>(string fieldId, DataObjectIdentifier<TValueKind, Guid> value) where TValueKind : DataObjectKind;
    }
}
