﻿namespace LendingQB.Core.Mapping
{
    using System;
    using System.Linq;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Database mapper for the housing history collection.
    /// </summary>
    internal partial class HousingHistoryEntryCollectionMapper
    {
        /// <summary>
        /// Gets a value indicating whether all present addresses have a blank end date.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>A value indicating whether all present addresses have a blank end date.</returns>
        public bool EnsurePresentAddressesDoNotHaveEndDate(IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection)
        {
            return collection.Values
                .Where(e => e.IsPresentAddress.GetValueOrDefault())
                .All(e => e.EndDate == null);
        }
    }
}
