﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A simple, in-memory association set.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier to use.</typeparam>
    /// <typeparam name="TValue">The type of the values to store.</typeparam>
    internal class LqbAssociationSet<TValueKind, TIdValue, TValue> : RecordSetBase<TValueKind, TIdValue, TValue>, IMutableLqbAssociationSet<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges, IAssociation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbAssociationSet{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dict">The dictionary used to initialize the collection.</param>
        /// <param name="firstCardinality">The cardinality for the first entity in the association set.</param>
        /// <param name="secondCardinality">The cardinality for the second entity in the association set.</param>
        private LqbAssociationSet(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict,
            int firstCardinality,
            int secondCardinality)
            : base(name, idFactory, observer, dict)
        {
            this.MaxFirstCardinality = firstCardinality;
            this.MaxSecondCardinality = secondCardinality;
        }

        /// <summary>
        /// Gets or sets the cardinality for the first entity in the association set.
        /// </summary>
        private int MaxFirstCardinality { get; set; }

        /// <summary>
        /// Gets or sets the cardinality for the second entity in the association set.
        /// </summary>
        private int MaxSecondCardinality { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbAssociationSet{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="firstCardinality">The cardinality for the first entity in the association set.</param>
        /// <param name="secondCardinality">The cardinality for the second entity in the association set.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbAssociationSet{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbAssociationSet<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            int firstCardinality,
            int secondCardinality)
        {
            return Create(name, idFactory, null, new Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>(), firstCardinality, secondCardinality);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbAssociationSet{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="firstCardinality">The cardinality for the first entity in the association set.</param>
        /// <param name="secondCardinality">The cardinality for the second entity in the association set.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbAssociationSet{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbAssociationSet<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            int firstCardinality,
            int secondCardinality)
        {
            return Create(name, idFactory, observer, new Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>(), firstCardinality, secondCardinality);
        }

        /// <summary>
        /// Create an implementation of the <see cref="IMutableLqbAssociationSet{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="name">The name to display in messages about the collection.</param>
        /// <param name="idFactory">The factory used to generate new identifiers.</param>
        /// <param name="observer">The observer for the collection.</param>
        /// <param name="dict">The dictionary used to initialize the collection.</param>
        /// <param name="firstCardinality">The cardinality for the first entity in the association set.</param>
        /// <param name="secondCardinality">The cardinality for the second entity in the association set.</param>
        /// <returns>An implementation of the <see cref="IMutableLqbAssociationSet{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableLqbAssociationSet<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            ILqbCollectionObserver<TValueKind, TIdValue, TValue> observer,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> dict,
            int firstCardinality,
            int secondCardinality)
        {
            return new LqbAssociationSet<TValueKind, TIdValue, TValue>(name, idFactory, observer, dict, firstCardinality, secondCardinality);
        }

        /// <summary>
        /// Determine whether the value can be added to the collection.
        /// </summary>
        /// <remarks>
        /// It is expected that that the size of each collection is relatively small so looping over every element should be ok.
        /// However, if a particular collection is expected to be large, we should consider another class that keeps track of 
        /// the counts for each entity so this method can be efficient.
        /// </remarks>
        /// <param name="value">The value that is being added.</param>
        /// <returns>True if the value can be added, false otherwise.</returns>
        protected override bool CanAdd(TValue value)
        {
            var itest = value as IAssociation;
            bool manyToMany = this.MaxFirstCardinality == -1 && this.MaxSecondCardinality == -1;
            bool firstHasMaxCardinality = this.MaxFirstCardinality > 0;
            bool secondHasMaxCardinality = this.MaxSecondCardinality > 0;

            int countFirstMatch = 0;
            int countSecondMatch = 0;
            foreach (var existing in this.Values)
            {
                bool matchFirst = existing.MatchesFirstEntity(itest);
                bool matchSecond = existing.MatchesSecondEntity(itest);

                // Can never add the same association because the pair is the natural key
                if (matchFirst && matchSecond)
                {
                    return false;
                }

                if (!manyToMany)
                {
                    if (matchFirst)
                    {
                        ++countFirstMatch;
                    }

                    if (matchSecond)
                    {
                        ++countSecondMatch;
                    }

                    if (matchFirst && firstHasMaxCardinality)
                    {
                        if (countFirstMatch >= this.MaxFirstCardinality)
                        {
                            return false;
                        }
                    }

                    if (matchSecond && secondHasMaxCardinality)
                    {
                        if (countSecondMatch >= this.MaxSecondCardinality)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }
    }
}
