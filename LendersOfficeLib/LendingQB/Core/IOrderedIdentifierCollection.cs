﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// If a collection supports ordering operations, then this interface should be supported.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The class that contains the data.</typeparam>
    /// <remarks>
    /// While the interfaces defined below are for a tool that maintains the order, this interface
    /// is to be applied to the collection itself.
    /// </remarks>
    public interface IReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue> : IReadOnlyLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets the interface used to manipulate the collection's order.
        /// </summary>
        IOrderedIdentifierCollection<TValueKind, TIdValue> OrderingInterface { get; }
    }

    /// <summary>
    /// Interface used for interacting with the order of items in a collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    public interface IOrderedIdentifierCollection<TValueKind, TIdValue> : IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Move the identifier back one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        void MoveBack(DataObjectIdentifier<TValueKind, TIdValue> identifier);

        /// <summary>
        /// Move the identifier forward one position in the collection.
        /// </summary>
        /// <param name="identifier">The identifier that will be moved.</param>
        void MoveForward(DataObjectIdentifier<TValueKind, TIdValue> identifier);

        /// <summary>
        /// Retrieve the identifier at a given position.
        /// </summary>
        /// <param name="position">The position of interest.</param>
        /// <remarks>
        /// Will throw an exception if the position is out of bounds (i.e., not within the range [0, Count)).
        /// </remarks>
        /// <returns>The desired identifier, or throw an exception if the position is out of bounds.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> GetIdentifierAt(int position);

        /// <summary>
        /// Get the position for the identifier.
        /// </summary>
        /// <param name="identifier">The identifier of interest.</param>
        /// <remarks>
        /// Throws an exception if the identifier is not in the collection.
        /// </remarks>
        /// <returns>The position where the identifier is located in the collection.</returns>
        int GetPositionOf(DataObjectIdentifier<TValueKind, TIdValue> identifier);

        /// <summary>
        /// Modify the order by substituting the previous order with the current order.
        /// </summary>
        /// <param name="previousOrder">The set of identifiers in the old order.</param>
        /// <param name="currentOrder">The set of identifiers in the new order.</param>
        void ReOrder(DataObjectIdentifier<TValueKind, TIdValue>[] previousOrder, DataObjectIdentifier<TValueKind, TIdValue>[] currentOrder);
    }

    /// <summary>
    /// If a collection supports ordering operations, then this interface should be supported.
    /// </summary>
    /// <remarks>
    /// This interface is intended to be used internally by the collections framework and not by client code.
    /// </remarks>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    /// <typeparam name="TValue">The class that contains the data.</typeparam>
    internal interface IMutableOrderedLqbCollection<TValueKind, TIdValue, TValue> : IMutableLqbCollection<TValueKind, TIdValue, TValue>, IReadOnlyOrderedLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets the interface used to add/remove items from the collection's order.
        /// </summary>
        IMutableOrderedIdentifierCollection<TValueKind, TIdValue> OrderManagerInterface { get; }

        /// <summary>
        /// Inserts the value at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index where the value should be added.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier of the entity.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> Insert(int index, TValue value);
    }

    /// <summary>
    /// Interface used for interacting with the order of items in a collection.
    /// </summary>
    /// <remarks>
    /// This interface is intended to be used internally by the collections framework and not by client code.
    /// </remarks>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> with which the contained identifiers are associated.</typeparam>
    /// <typeparam name="TIdValue">The type of the key used to find values in the collection.</typeparam>
    internal interface IMutableOrderedIdentifierCollection<TValueKind, TIdValue> : IOrderedIdentifierCollection<TValueKind, TIdValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Add the identifier to the end of the collection.
        /// </summary>
        /// <remarks>
        /// This method should only be called in synchronization with the adding an item to the ILqbCollection interface.
        /// If care is not taken to limit the callers then the order will contain identifiers not held within the associated collection.
        /// </remarks>
        /// <param name="identifier">The identifier getting added.</param>
        void Add(DataObjectIdentifier<TValueKind, TIdValue> identifier);

        /// <summary>
        /// Remove all the entities from the order.
        /// </summary>
        void Clear();

        /// <summary>
        /// Remove the identifier to the collection.
        /// </summary>
        /// <param name="identifier">The identifier getting removed.</param>
        void Remove(DataObjectIdentifier<TValueKind, TIdValue> identifier);

        /// <summary>
        /// Insert an identifier at a given position.
        /// </summary>
        /// <param name="position">The zero-based position where the identifier should be located.</param>
        /// <param name="identifier">The identifier getting inserted.</param>
        /// <remarks>
        /// Will throw an exception if the position is out of bounds (i.e., not within the range [0, Count]).
        /// </remarks>
        void InsertAt(int position, DataObjectIdentifier<TValueKind, TIdValue> identifier);
    }

    /// <summary>
    /// Interface for constructing the ordering with raw identifier values.
    /// </summary>
    /// <remarks>
    /// This interface is intended to be used internally by the collections framework and not by client code.
    /// </remarks>
    /// <typeparam name="TIdValue">The primitive type of the raw identifiers.</typeparam>
    internal interface IInitializeCollectionOrder<TIdValue> : IEnumerable<TIdValue>
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets a value indicating whether a change has been made on the order.
        /// </summary>
        bool HasBeenModified { get; }

        /// <summary>
        /// Add the identifier to the end of the collection.
        /// </summary>
        /// <param name="id">The raw identifier value getting added.</param>
        void Add(TIdValue id);

        /// <summary>
        /// Once the order has been constructed, mark the end of the construction.
        /// </summary>
        void SetInitializationDone();
    }
}
