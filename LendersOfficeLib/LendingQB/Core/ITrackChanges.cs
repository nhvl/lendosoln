﻿namespace LendingQB.Core
{
    using System.Collections.Generic;

    /// <summary>
    /// An interface for entities that keep track of changes.
    /// </summary>
    internal interface ITrackChanges
    {
        /// <summary>
        /// Gets a value indicating whether or not there are changes.
        /// </summary>
        bool HasChanges { get; }

        /// <summary>
        /// Gets the updates that have been made to the entity.
        /// </summary>
        /// <returns>The updates that have been made to the entity.</returns>
        IEnumerable<KeyValuePair<string, object>> GetChanges();
    }
}
