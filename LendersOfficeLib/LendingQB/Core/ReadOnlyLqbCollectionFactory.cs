﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Read only collection factory.
    /// </summary>
    public struct ReadOnlyLqbCollectionFactory
    {
        /// <summary>
        /// Creates a new read only collection based on the given values.
        /// </summary>
        /// <typeparam name="TValueKind">The object kind of the contained value.</typeparam>
        /// <typeparam name="TIdValue">The type of the underlying id.</typeparam>
        /// <typeparam name="TValue">The type of the contained value.</typeparam>
        /// <param name="name">The name of the collection.</param>
        /// <param name="idFactory">The identifier factory to use.</param>
        /// <param name="values">The values for the collection.</param>
        /// <returns>The new read only association set.</returns>
        public IReadOnlyLqbCollection<TValueKind, TIdValue, TValue> Create<TValueKind, TIdValue, TValue>(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> values)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            return new ReadOnlyLqbCollection<TValueKind, TIdValue, TValue>(name, idFactory, values);
        }
    }
}
