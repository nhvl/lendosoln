﻿namespace LendingQB.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Decorator for an ILqbCollection that also manages the order of the items in the collection.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> associated with <typeparamref name="TValue"/>.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier to use.</typeparam>
    /// <typeparam name="TValue">The type of the values to store.</typeparam>
    internal class OrderedLqbCollection<TValueKind, TIdValue, TValue> : IMutableOrderedLqbCollection<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
        where TValue : ITrackChanges
    {
        /// <summary>
        /// Used to manage the order of the identifiers.
        /// </summary>
        private IMutableOrderedIdentifierCollection<TValueKind, TIdValue> manageOrder;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedLqbCollection{TValueKind, TIdValue, TValue}"/> class.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="order">The ordering for the collection.</param>
        private OrderedLqbCollection(IMutableLqbCollection<TValueKind, TIdValue, TValue> contained, IMutableOrderedIdentifierCollection<TValueKind, TIdValue> order)
        {
            this.Contained = contained;
            this.manageOrder = order;

            if (this.Contained.Count != this.manageOrder.Count)
            {
                // TODO: Re-enable this logging when we expect the ordering to be in sync.
                ////DataAccess.Tools.LogError("Encountered mismatch between collection records and collection order. Removing extra ids from order and adding missing ids to order.");

                var irrelevantIdsInOrder = this.manageOrder.Where(id => !contained.ContainsKey(id)).ToList();
                foreach (var irrelevantId in irrelevantIdsInOrder)
                {
                    this.manageOrder.Remove(irrelevantId);
                }

                var idsMissingFromOrder = this.Contained.Keys.Where(id => !this.manageOrder.Contains(id));
                foreach (var missingId in idsMissingFromOrder)
                {
                    this.manageOrder.Add(missingId);
                }
            }
        }

        /// <summary>
        /// Gets the interface used to manipulate the collection's order.
        /// </summary>
        public IOrderedIdentifierCollection<TValueKind, TIdValue> OrderingInterface
        {
            get { return this.manageOrder; }
        }

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return this.Contained.Count;
            }
        }

        /// <summary>
        /// Gets the keys for the collection.
        /// </summary>
        public IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> Keys
        {
            get
            {
                return this.OrderingInterface; // inherits from IEnumerable<DataObjectIdentifier<TValueKind, TIdValue>> and returns the keys in the current order.
            }
        }

        /// <summary>
        /// Gets the values contained in the collection.
        /// </summary>
        public IEnumerable<TValue> Values
        {
            get
            {
                foreach (var key in this.Keys)
                {
                    yield return this.Contained[key];
                }
            }
        }

        /// <summary>
        /// Gets the interface used to add/remove items from the collection's order.
        /// </summary>
        IMutableOrderedIdentifierCollection<TValueKind, TIdValue> IMutableOrderedLqbCollection<TValueKind, TIdValue, TValue>.OrderManagerInterface
        {
            get { return this.manageOrder; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection has any changes.
        /// </summary>
        /// <remarks>
        /// This is only returning true for changes to the collection and 
        /// ignores changes to the ordering of the entities within the collections.
        /// </remarks>
        public bool HasChanges
        {
            get
            {
                return this.Contained.HasChanges;
            }
        }

        /// <summary>
        /// Gets or sets the decorated interface.
        /// </summary>
        private IMutableLqbCollection<TValueKind, TIdValue, TValue> Contained { get; set; }

        /// <summary>
        /// Gets the value from the key.
        /// </summary>
        /// <param name="key">The key for the desired value.</param>
        /// <returns>The value associated with the key.</returns>
        public TValue this[DataObjectIdentifier<TValueKind, TIdValue> key]
        {
            get
            {
                return this.Contained[key];
            }
        }

        /// <summary>
        /// Create an implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="contained">The contained collection that this instance is decorating.</param>
        /// <param name="order">The ordering for the collection.</param>
        /// <returns>An implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableOrderedLqbCollection<TValueKind, TIdValue, TValue> Create(
            IMutableLqbCollection<TValueKind, TIdValue, TValue> contained,
            IMutableOrderedIdentifierCollection<TValueKind, TIdValue> order)
        {
            return new OrderedLqbCollection<TValueKind, TIdValue, TValue>(contained, order);
        }

        /// <summary>
        /// Create an implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.
        /// </summary>
        /// <param name="name">The name for the collection.</param>
        /// <param name="idFactory">The id factory to use.</param>
        /// <returns>An implementation of the <see cref="IMutableOrderedLqbCollection{TValueKind, TIdValue, TValue}"/> interface.</returns>
        public static IMutableOrderedLqbCollection<TValueKind, TIdValue, TValue> Create(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory)
        {
            var collection = LqbCollection<TValueKind, TIdValue, TValue>.Create(name, idFactory);
            var order = OrderedIdentifierCollection<TValueKind, TIdValue>.Create() as IMutableOrderedIdentifierCollection<TValueKind, TIdValue>;
            return Create(collection, order);
        }

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier used to identify the new value.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue> Add(TValue value)
        {
            var identifier = this.Contained.Add(value);

            this.manageOrder.Add(identifier);

            return identifier;
        }

        /// <summary>
        /// Adds a value to the collection.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public void Add(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value)
        {
            this.Contained.Add(id, value);

            this.manageOrder.Add(id);
        }

        /// <summary>
        /// Adds a value to the collection, but prevents it from being tracked as an added entity.
        /// </summary>
        /// <param name="id">The identifier for the new value.</param>
        /// <param name="value">The value to add.</param>
        public void AddWithoutTracking(DataObjectIdentifier<TValueKind, TIdValue> id, TValue value)
        {
            this.Contained.AddWithoutTracking(id, value);

            this.manageOrder.Add(id);
        }

        /// <summary>
        /// Inserts the value at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index where the value should be added.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>The identifier of the entity.</returns>
        public DataObjectIdentifier<TValueKind, TIdValue> Insert(int index, TValue value)
        {
            var id = this.Contained.Add(value);

            this.manageOrder.InsertAt(index, id);

            return id;
        }

        /// <summary>
        /// Causes the collection to start tracking addition and removal of records.
        /// </summary>
        public void BeginTrackingChanges()
        {
            this.Contained.BeginTrackingChanges();
        }

        /// <summary>
        /// Determine whether or not the collection contains the key.
        /// </summary>
        /// <param name="key">The key of interest.</param>
        /// <returns>True if the key id found in the collection, false otherwise.</returns>
        public bool ContainsKey(DataObjectIdentifier<TValueKind, TIdValue> key)
        {
            return this.Contained.ContainsKey(key);
        }

        /// <summary>
        /// Gets the updates that have occurred since <see cref="BeginTrackingChanges"/> was called.
        /// </summary>
        /// <returns>A <see cref="LqbCollectionUpdates{TId, TValue}"/> containing the changes that have been made.</returns>
        public LqbCollectionChangeRecords<TValueKind, TIdValue, TValue> GetChanges()
        {
            return this.Contained.GetChanges();
        }

        /// <summary>
        /// Retrieve the enumerator for the collection.
        /// </summary>
        /// <returns>The enumerator for the collection.</returns>
        public IEnumerator<KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>> GetEnumerator()
        {
            foreach (var key in this.Keys)
            {
                yield return new KeyValuePair<DataObjectIdentifier<TValueKind, TIdValue>, TValue>(key, this.Contained[key]);
            }
        }

        /// <summary>
        /// Removes the value identified by the id parameter.
        /// </summary>
        /// <param name="id">The identifier of the value to remove.</param>
        public void Remove(DataObjectIdentifier<TValueKind, TIdValue> id)
        {
            this.Contained.Remove(id);

            this.manageOrder.Remove(id);
        }

        /// <summary>
        /// Attempt to retrieve a value from the collection based on the key.
        /// </summary>
        /// <param name="key">The key used to retrieve an associated value.</param>
        /// <param name="value">The value to be populated if the key is found.</param>
        /// <returns>True if the key is found, false otherwise.</returns>
        public bool TryGetValue(DataObjectIdentifier<TValueKind, TIdValue> key, out TValue value)
        {
            return this.Contained.TryGetValue(key, out value);
        }

        /// <summary>
        /// Get the plain enumerator that returns objects.
        /// </summary>
        /// <returns>The plain enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Keys.Select(key => new DataPathSelectionElement(new SelectIdExpression(key.Value.ToString())));
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            return this.Contained.GetElement(element);
        }
    }
}
