﻿namespace LendingQB.Core
{
    /// <summary>
    /// If an entity contains data that needs to be calculated prior to saving,
    /// e.g., and audit trail, then it must implement this interface.
    /// </summary>
    internal interface IPreEntitySaveHandler
    {
        /// <summary>
        /// Called just prior to saving the entity's data.
        /// </summary>
        void ResolveChanges();
    }
}
