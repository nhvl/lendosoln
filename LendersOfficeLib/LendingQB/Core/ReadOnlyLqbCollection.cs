﻿namespace LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A simple read only collection implementation.
    /// </summary>
    /// <typeparam name="TValueKind">The kind of the object being stored.</typeparam>
    /// <typeparam name="TIdValue">The type of the underlying id value.</typeparam>
    /// <typeparam name="TValue">The type of the objects that will be contained..</typeparam>
    internal class ReadOnlyLqbCollection<TValueKind, TIdValue, TValue> : ReadOnlyDictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue>, IReadOnlyLqbCollection<TValueKind, TIdValue, TValue>, IReadOnlyLqbAssociationSet<TValueKind, TIdValue, TValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// The factory used to generate new identifiers.
        /// </summary>
        private readonly IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory;

        /// <summary>
        /// Gets the name for display in messages about the collection.
        /// </summary>
        /// <value>The name for display in messages about the collection.</value>
        private readonly Name name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyLqbCollection{TValueKind,TIdValue,TValue}"/> class.
        /// </summary>
        /// <param name="name">The name of the collection.</param>
        /// <param name="idFactory">The identifier factory to use.</param>
        /// <param name="values">The values for the collection.</param>
        internal ReadOnlyLqbCollection(
            Name name,
            IDataObjectIdentifierFactory<TValueKind, TIdValue> idFactory,
            Dictionary<DataObjectIdentifier<TValueKind, TIdValue>, TValue> values) : base(values)
        {
            this.name = name;
            this.idFactory = idFactory;
        }

        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        public IEnumerable<DataPathSelectionElement> GetAllKeys()
        {
            return this.Keys.Select(k => new DataPathSelectionElement(new SelectIdExpression(k.Value.ToString())));
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException($"{this.name} can only handle {nameof(DataPathSelectionElement)}, but passed element of type {element.GetType()}");
            }

            DataObjectIdentifier<TValueKind, TIdValue>? id = this.idFactory.Create(element.Name);

            if (!id.HasValue)
            {
                throw new ArgumentException($"Could not parse '{element.Name}' into a valid identifier.");
            }

            return this[id.Value];
        }
    }
}
