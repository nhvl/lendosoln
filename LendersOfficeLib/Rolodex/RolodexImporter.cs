namespace LendersOffice.Rolodex
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OleDb;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Constants;

    // 03/14/06 mf - This class was created for OPM 2706. It imports entries
    // from a outlook-exported contacts CSV file to the LO contacts list.
    //
    // 07/25/06 mf - Now that we have our own expiring cache mechanism, we
    // will deploy the importer.
    //
    // 10/03/11 rwn - There's a csv parser in LoLib now, so
    // I've refactored this to rely on that instead of implementing its own (and I had to
    // change the one in LoLib to support a rare case that this one supported, namely newlines
    // inside quotes in a CSV file - what won't Outlook think of next?)
    // Also added support for a custom import format, which maps directly to RolodexDB.


    //Some aliases in order to make the giant generic type definitions make sense
    //Item1 is always the destination column name, item2 is some sort of function 
    //that operates either on the contents of the column, or on an entire data row
    using Transformer = System.Tuple<string, System.Func<string, string>>; //modifies the column's value somehow and returns the new value
    using Translator = System.Tuple<string, System.Func<System.Data.DataRow, string>>; //Takes data row from an Outlook contacts dump and 
    using Validator = System.Tuple<string, System.Func<string, bool>>; //returns true if the column's value is valid
                                                              //returns the translated column value
    /// <summary>
    /// Class designed to import a csv file into the LO contacts list.
    /// </summary>
    public class RolodexImporter
	{
        /// <summary>
        /// Indicates the format of the imported file.
        /// </summary>
        private enum ImportFormat
        {
            Unknown,
            Outlook,
            LendingQB,
        }

		private Guid m_brokerID;

		// Constants	
		private const int AGENT_NM_LENGTH      = 56;
		private const int PHONE_LENGTH         = 21;
		private const int TITLE_LENGTH         = 21;
		private const int COM_NM_LENGTH        = 36;
		private const int ADDR_LENGTH          = 36;
		private const int CITY_LENGTH          = 36;
		private const int DEPARTMENT_NM_LENGTH = 100;
		private const int EMAIL_LENGTH         = 80;
		private const int URL_LENGTH           = 200;
        private const int LICENSE_LENGTH       = 30;
        private const int NOTES_LENGTH         = 500;
        private const int COUNTY_LENGTH = 36;
        private const int COMPANY_ID_LENGTH = 25;

		// Regular Expressions
		// We will only allow values that the contact editor would accept
        private static Regex EMAIL_PATTERN = new Regex(ConstApp.EmailValidationExpression, RegexOptions.Compiled);
        //Most people omit the http:// in URLs nowadays, so that's optional (but it's required in the contact editor)
        private static Regex URL_PATTERN = new Regex(@"^(https?://)?([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$", RegexOptions.Compiled);
        //So we add it in a transformer, if it's not there.
        private static Regex HTTP_PATTERN = new Regex(@"^https?://", RegexOptions.Compiled);
        private static Regex STATE_PATTERN = new Regex(@"(?i)^(a[klrz]|c[aot]|d[ce]|fl|ga|hi|i[adln]|k[sy]|la|m[adeinost]|n[ebcdhjmvy]|o[hkr]|pa|ri|s[cd]|t[nx]|ut|v[at]|w[aivy]){1}$", RegexOptions.Compiled);
        private static Regex ZIP_PATTERN   = new Regex(@"^\d{5}(-\d{4})?$", RegexOptions.Compiled);
        private static Regex COMPANY_ID_PATTERN = new Regex(@"^[A-Za-z\d-]+$", RegexOptions.Compiled);

        //The list of the names of the columns
        private static readonly string[] columnNames = new string[]
        {
            "Agent Name",
            "Agent Title",
            "Agent Phone",
            "Agent Cell Phone",
            "Agent Department",
            "Agent Email",
            "Company Website",
            "Agent Fax",
            "Agent Pager",
            "Agent Type",
            "Agent Type Other Description",
            "Agent License Number",
            "Company Name",
            "Company Phone",
            "Company Fax",
            "Company License Number",
            "Street Address",
            "City",
            "State",
            "Zip",
            "Notes",
            "Pay To Bank Name",
            "Pay To City/State",
            "Pay To ABA Number",
            "Pay To Account Name",
            "Pay To Account Number",
            "Further Credit To Account Name",
            "Further Credit To Account Number",
            "Branch Name",
            "County",
            "System ID",
            "Company ID",
        };

        //For checking agent types (roles) - this is better than try{enum.parse}catch, because it
        //avoids the try/catch overhead on failure.
        private static readonly List<string> m_agentRoles = new List<string>(Enum.GetNames(typeof(E_AgentRoleT)).Select((a) => a.ToLowerInvariant()));
        
        //These guys make sure columns are valid
        private static readonly Validator[] Validations = new Validator[]
        { 
            //Note the use of short-circuit evaluation: 
            //if the string is empty, the regex is skipped and it's valid. If the string isn't empty, the regex is checked.
            new Validator("Agent Email", (a) => string.IsNullOrEmpty(a) || EMAIL_PATTERN.IsMatch(a)),
            new Validator("Company Website", (a) => string.IsNullOrEmpty(a) || URL_PATTERN.IsMatch(a)),
            new Validator("State", (a) => string.IsNullOrEmpty(a) || STATE_PATTERN.IsMatch(a)),
            new Validator("Zip", (a) => string.IsNullOrEmpty(a) || ZIP_PATTERN.IsMatch(a)),
            //Ignore case when doing this validation
            new Validator("Agent Type", (a) => string.IsNullOrEmpty(a) || m_agentRoles.Contains(a.Replace(" ", "").ToLowerInvariant())),
            new Validator("Company ID", (a) => string.IsNullOrEmpty(a) || COMPANY_ID_PATTERN.IsMatch(a)),
        };

        //These guys turn the content of a column into something else
        private static readonly Transformer[] Transformations = new Transformer[] 
        { 
            new Transformer("Agent Fax", ParsePhone),
            new Transformer("Agent Phone", ParsePhone),
            new Transformer("Agent Cell Phone", ParsePhone),
            new Transformer("Agent Pager", ParsePhone),
            new Transformer("Company Phone", ParsePhone),
            new Transformer("Company Fax", ParsePhone),
            new Transformer("Agent Fax", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Agent Phone", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Agent Cell Phone", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Agent Pager", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Company Phone", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Company Fax", (a) => TrimToLength(a, PHONE_LENGTH)),
            new Transformer("Agent Name", (a) => TrimToLength(a, AGENT_NM_LENGTH)),
            new Transformer("Agent Title", (a) => TrimToLength(a, TITLE_LENGTH)),
            new Transformer("Company Name", (a) => TrimToLength(a, COM_NM_LENGTH)),
            new Transformer("Street Address", (a) => TrimToLength(a, ADDR_LENGTH)),
            new Transformer("City", (a) => TrimToLength(a, CITY_LENGTH)),
            new Transformer("Agent Department", (a) => TrimToLength(a, DEPARTMENT_NM_LENGTH)),
            new Transformer("Agent Email", (a) => TrimToLength(a, EMAIL_LENGTH)),
            new Transformer("Company Website", (a) => TrimToLength(a, URL_LENGTH)),
            new Transformer("Agent License Number", (a) => TrimToLength(a, LICENSE_LENGTH)),
            new Transformer("Company License Number", (a) => TrimToLength(a, LICENSE_LENGTH)),
            new Transformer("Notes", (a) => TrimToLength(a, NOTES_LENGTH)),
            //Remove internal spaces, so users can enter things like "Selling Agent" instead of "SellingAgent"
            new Transformer("Agent Type", (a) => a.Replace(" ", "")), 
            //State abbreviations are always upper case
            new Transformer("State", (a) => a.ToUpper()),
            //Force URLs to start with http:// if it doesn't start with http:// or https://
            new Transformer("Company Website", (a) => { if(!HTTP_PATTERN.IsMatch(a)) return "http://" + a; else return a;}), 
            //Zipcodes are occasionally entered in with the 5-4 format, so just take the first five digits
            new Transformer("Zip", (a) => 
            {
                if(string.IsNullOrEmpty(a)) return a;
                return a.PadLeft(5, '0').Substring(0, 5);
            }),
            new Transformer("County", (a) => TrimToLength(a, COUNTY_LENGTH)),
            new Transformer("Company ID", (a) => TrimToLength(a, COMPANY_ID_LENGTH))
        };

        //These guys translate from Outlook's weird contact dump format to our format.
        private static readonly Translator[] OutlookTranslation = new Translator[]
        {
            new Translator("Agent Name", (row) => ConcatFields(new string[] 
            {
                "First Name", 
                "Middle Name", 
                "Last Name", 
                "Suffix"
            }, row, " ")),

            new Translator("Street Address", (row) => ConcatFields(new string[]
            {
                "Business Street",
                "Business Street 2",
                "Business Street 3",
            }, row, " ")),

            new Translator("Agent Email", (row) => FindPattern(new string[] 
            {
                 "E-mail Address", 
                 "E-mail Display Name",
            }, row, EMAIL_PATTERN)),
            
            new Translator("Agent Cell Phone", (row) => string.IsNullOrEmpty((string)row["Mobile Phone"])?
                                                       (string)row["Car Phone"]:(string)row["Mobile Phone"]),

            new Translator("Agent Title", (row)=>(string)row["Job Title"]),
            new Translator("Agent Phone", (row) => (string)row["Business Phone"]),
            new Translator("Agent Fax", (row) => (string)row["Business Fax"]),
            new Translator("Agent Pager", (row) => (string)row["Pager"]),
            new Translator("Company Name", (row) => (string)row["Company"]),
            new Translator("Agent Department", (row) => (string)row["Department"]),
            new Translator("City", (row) => (string)row["Business City"]),
            new Translator("State", (row) => (string)row["Business State"]),
            new Translator("Zip", (row) => (string)row["Business Postal Code"]),
            new Translator("Company Phone", (row) => (string)row["Company Main Phone"]),
            new Translator("Company Fax", (row) => (string)row["Other Fax"]),
            new Translator("Company Website", (row) => (string)row["Web Page"]),
        };
                                                       

		// Storage Members
		private DataTable m_loImportedValues = new DataTable();
        private DataTable m_duplicateValues = new DataTable();

		// For detecting collisions
		private HashSet<KeyValuePair<string, string>> m_existingNames = new HashSet<KeyValuePair<string, string>>();
		private HashSet<KeyValuePair<string, string>> m_existingCompanies = new HashSet<KeyValuePair<string, string>>();

        private Dictionary<string, RolodexDB> existingContactsBySystemId = new Dictionary<string, RolodexDB>();

        // Import Members		
        private bool m_isValid = false;
        private ImportFormat importFormat = ImportFormat.Unknown;

        // Searches for the pattern in the list of known fields.  Returns empty string if not found.
        private static string FindPattern(string[] fields, DataRow dR, Regex pattern)
        {
            Regex searchExpression = pattern;

            for (int i = 1; i < fields.Length; i++)
            {
                string possibleField = (dR.Table.Columns.Contains(fields[i])) ? (string)dR[fields[i]] : string.Empty;
                Match fieldMatch = searchExpression.Match(possibleField);
                if (fieldMatch.Success)
                {
                    return fieldMatch.Value;
                }
            }

            return string.Empty;
        }

        // Concat all the strings from input file with the delimiter
		private static string ConcatFields( string[] fields, DataRow dR, string delimiter )
		{
			string completedString = string.Empty;
			for ( int i = 0; i < fields.Length; i++ )
			{
				if ( dR.Table.Columns.Contains( fields[ i ] ) && (string) dR[ fields[ i ] ] != string.Empty )
				{
					completedString += ( completedString == string.Empty ) ? (string) dR[ fields[ i ] ] : ( delimiter + (string) dR[ fields[ i ] ] );
				}
			}
			return completedString;
		}

        // To allow valid comparisons between DB and potential duplicates
        // have to trim the values to be compared
        private static string TrimToLength(string target, int length)
        {
            return (target.Length <= length) ? target : target.Substring(0, length);
        }

        private static string ParsePhone(string rawPhone)
        {
            // We would rather not import than risk importing
            // an inaccurate number.

            string clearPhone = rawPhone.Replace("(", string.Empty).Replace(")", string.Empty).Replace("-", string.Empty).Replace(" ", string.Empty);
            int length = clearPhone.Length;

            if (length == 0)
            {
                return string.Empty;
            }

            for (int i = 0; i < length; i++)
            {
                if ((i < 10 && !char.IsDigit(clearPhone, i))
                    || (i >= 10 && !char.IsLetterOrDigit(clearPhone, i)))
                {
                    return string.Empty;
                }
            }

            // Mimic how the value would have to be entered in the contacts editor
            if (clearPhone.Length < 3)
            {
                return "(" + clearPhone;
            }
            else if (clearPhone.Length < 6)
            {
                return "(" + clearPhone.Substring(0, 3) + ") " + clearPhone.Substring(3);
            }
            else
            {
                return "(" + clearPhone.Substring(0, 3) + ") " + clearPhone.Substring(3, 3) + "-" + clearPhone.Substring(6);
            }
        }


        #region Properties
        public DataTable Duplicates
		{
			get 
			{
				return m_isValid ? m_duplicateValues : null;
			}
		}

		public DataTable LoTable
		{
			get 
			{
				return m_isValid ? m_loImportedValues : null;
			}
		}
		#endregion

		// Constructor
		public RolodexImporter( Guid BrokerID )
		{
			m_brokerID = BrokerID;
		}

		// Create list of entries that are currently in Contacts. 
		// for collision detection. We only check for name collisions,
		// but for company-only entries, on blank names we use company
		private void CreateExistingList()
		{
            this.existingContactsBySystemId = RolodexDB.ListRolodexEntriesBySystemIdWithPermissionBypass(this.m_brokerID);

            foreach (RolodexDB record in this.existingContactsBySystemId.Values)
			{
                if (string.IsNullOrEmpty(record.Name))
				{
                    if (!this.m_existingCompanies.Any(p => string.Equals(p.Key, record.CompanyName) && string.Equals(p.Value, record.BranchName)))
					{
                        this.m_existingCompanies.Add(new KeyValuePair<string, string>(record.CompanyName, record.BranchName));
					}
				}
				else
				{
                    if (!this.m_existingNames.Any(p => string.Equals(p.Key, record.Name) && string.Equals(p.Value, record.BranchName)))
					{
                        this.m_existingNames.Add(new KeyValuePair<string, string>(record.Name, record.BranchName));
					}
				}
			}
		}

		/// <summary>
		/// Perform the import using the specified filename.
		/// </summary>

		public bool Import( string FileName )
		{
            // Import file
            DataTable toImport;
            ILookup<int, string> parseErrors;
            try
            {
                toImport = DataParsing.FileToDataTable(FileName, true, null, out parseErrors);
            }
            catch (OleDbException)
            {
                LogFailure("OLEDB Exception thrown while trying to read file " + FileName, null, null, null);
                return m_isValid = false;
            }
            //Note that no columns are required to be filled out in this validation pass 
            //(it's more complicated than that, so we do it later)

            if (parseErrors.Any())
            {
                foreach (var error in parseErrors)
                {
                    foreach (string errorMsg in error)
                    {
                        LogFailure(errorMsg, null, error.Key + "", null);
                    }
                }

                return m_isValid = false;
            }

            //Now check and see what kind of DataTable we have - Outlook won't have Agent License Number, we won't have First Name.
            foreach (DataColumn c in toImport.Columns)
            {
                if (c.ColumnName == "First Name")
                {
                    this.importFormat = ImportFormat.Outlook;
                    break;
                }

                if (c.ColumnName == "Agent License Number")
                {
                    this.importFormat = ImportFormat.LendingQB;
                    break;
                }
            }

            if (this.importFormat == ImportFormat.Unknown)
            {
                //We couldn't figure out how to handle this data table
                LogFailure("Could not figure out if file " + FileName + " contains an outlook contacts export or a custom export", null, null, null);
                return m_isValid = false;
            }
            else if (this.importFormat == ImportFormat.Outlook)
            {
                //It's an outlook table, so we need to translate it our format
                DataTable translated = new DataTable();
                foreach (string col in columnNames)
                {
                    DataColumn temp = translated.Columns.Add(col, typeof(string));
                    temp.DefaultValue = "";
                }

                foreach (DataRow fromOutlook in toImport.Rows)
                {
                    DataRow imported = translated.NewRow();
                    foreach (Translator t in OutlookTranslation)
                    {
                        string destColumn = t.Item1;
                        var translationFxn = t.Item2;
                        imported[destColumn] = translationFxn(fromOutlook);
                    }
                    translated.Rows.Add(imported);
                }

                toImport = translated;
            }

            //Now that we know for sure what toImport looks like, clone its schema into our 
            //member variables
            m_duplicateValues = toImport.Clone();
            m_loImportedValues = toImport.Clone();

            StringBuilder errors = new StringBuilder();
            errors.AppendLine();
            bool validateFailure = false;
            for (int i = 0; i < toImport.Rows.Count; i++)
            {
                DataRow row = toImport.Rows[i];
                List<string> failedColumns;
                failedColumns = ValidateRow(row);
                if (failedColumns.Count > 0)
                {
                    errors.AppendLine("Row " + i + " failed validation on columns: ");
                    foreach (string col in failedColumns)
                    {
                        if (col == NO_AGENT_OR_COMPANY_NAME)
                            errors.AppendLine("\t" + col);
                        else
                            errors.AppendLine("\t" + col + ": value=[" + row[col] +"]");
                    }
                    errors.Length -= 2;
                    errors.AppendLine();
                    validateFailure = true;
                }
                //Transformations must be applied before we try to look for dupes in ConvertToLoTable,
                //because otherwise we might have erroneous non-duplicates.

                Action<Exception> rowTransformationExceptionHandler = (e) =>
                {
                    errors.AppendLine("Row " + i + " failed transformation, exception is: " + e.Message);
                    Tools.LogError("Row couldn't transform", e);
                    validateFailure = true;
                };

                try
                {
                    TransformRow(row);
                }
                catch (ArgumentException e)
                {
                    rowTransformationExceptionHandler(e);
                }
                catch (DeletedRowInaccessibleException e)
                {
                    rowTransformationExceptionHandler(e);
                }
                catch (InvalidCastException e)
                {
                    rowTransformationExceptionHandler(e);
                }
                catch (NoNullAllowedException e)
                {
                    rowTransformationExceptionHandler(e);
                }
            }

            if (validateFailure)
            {
                LogFailure(errors.ToString(), null, "Multiple", null);
                return m_isValid = false;
            }
            
			// Create table of entries that will go into LO and duplicates table
			ConvertToLoTable(toImport);
			
			return m_isValid = true;
		}

        private const string NO_AGENT_OR_COMPANY_NAME = "Agent Name and Company Name both empty";

        private List<string> ValidateRow(DataRow row)
        {
            List<string> failedColumns = new List<string>();
            foreach(var validator in Validations)
            {
                string columnName = validator.Item1;
                var validationFxn = validator.Item2;
                if (!validationFxn((string)row[columnName]))
                {
                    failedColumns.Add(columnName);
                }
            }
            //And here's a couple of things that don't fit that format:
            //Both agent name and company name may not be empty
            if (string.IsNullOrEmpty((string)row["Agent Name"]) &&
                             string.IsNullOrEmpty((string)row["Company Name"]))
            {
                failedColumns.Add(NO_AGENT_OR_COMPANY_NAME);
            }
            

            return failedColumns;
        }

        private void TransformRow(DataRow row)
        {
            foreach (var trans in Transformations)
            {
                string columnName = trans.Item1;
                var transFxn = trans.Item2;
                row[columnName] = transFxn((string)row[columnName]);
            }
        }
			
		// Determine which entries in the imported table can be used in contacts.
		// Put them in the duplicates table if they can't.
		private void ConvertToLoTable(DataTable toImport)
		{
            // Get list of existing entries for collision detection
            //Since this hits the DB, we only wanna do it if we get this far
            CreateExistingList();

            for (int i = 0, j = toImport.Rows.Count; i < j; i++)
			{
                bool conversionSuccessful = ConvertToLoRow(toImport.Rows[i]);
                //Conversions only fail when there's a duplicate entry
                if (!conversionSuccessful)
				{
                    m_duplicateValues.ImportRow(toImport.Rows[i]);
				}
			}
		}
	
		private bool ConvertToLoRow( DataRow importedRow )
		{
            // Foreach LO Field, add column to new datarow consisting of fields from CSV
            // Fail if an entry already exists in the Contacts list or in the CSV itself

            string agentCompanyName = (string)importedRow["Company Name"];
            string agentBranchName = (string)importedRow["Branch Name"];
            string agentName = (string)importedRow["Agent Name"];
            string systemId = (string)importedRow["System ID"];

            // If this row is modifying an existing record, we already know it's a "duplicate" of that record.
            // Exclude from the duplicate checking logic.
            bool modifyingExisting = this.existingContactsBySystemId.ContainsKey(systemId);

            if (!modifyingExisting)
            {
                // If the system ID does not match an existing record, blank it out so it cannot inadvertently become valid.
                // (eg. if a lender has 3 contacts but puts "4" as a system ID intending that row to be new, that could
                // end up modifying an incorrect row if a different new record were added first)
                importedRow["System ID"] = string.Empty;

                // Return false on duplication
                if (string.IsNullOrEmpty(agentName))
                {
                    if (this.m_existingCompanies.Any(p => string.Equals(p.Key, agentCompanyName) && string.Equals(p.Value, agentBranchName)))
                    {
                        return false;
                    }
                }
                else
                {
                    if (this.m_existingNames.Any(p => string.Equals(p.Key, agentName) && string.Equals(p.Value, agentBranchName)))
                    {
                        return false;
                    }
                }

                // This row can be added to our contacts
                // Also add it to known names
                if (string.IsNullOrEmpty(agentName))
                {
                    this.m_existingCompanies.Add(new KeyValuePair<string, string>(agentCompanyName, agentBranchName));
                }
                else
                {
                    this.m_existingNames.Add(new KeyValuePair<string, string>(agentName, agentBranchName));
                }
            }

            this.m_loImportedValues.ImportRow(importedRow);
            return true;
		}

        // Return a csv string of the duplicates
        public string CreateDuplicatesCSV()
        {
            if (!m_isValid)
            {
                return null;
            }

            System.Text.StringBuilder contents = new System.Text.StringBuilder();
            return m_duplicateValues.ToCSV(true);
        }

		public bool WriteEntriesToRolodex()
		{
			if (!m_isValid)
			{
				return false;
			}

			bool success = true;
			foreach (DataRow entryRow in m_loImportedValues.Rows)
			{
                var systemId = (string)entryRow["System ID"];
                bool importIntoExisting = !string.IsNullOrEmpty(systemId) && this.existingContactsBySystemId.ContainsKey(systemId);
                RolodexDB rolodexEntry = importIntoExisting ? this.existingContactsBySystemId[systemId] : new RolodexDB();

				// Fields we know
				rolodexEntry.BrokerID = m_brokerID;
				rolodexEntry.Name = (string)entryRow["Agent Name"];
				rolodexEntry.Title = (string)entryRow["Agent Title"];
				rolodexEntry.Phone = (string)entryRow["Agent Phone"];
                //Everything everywhere seems to refer to the Alternate Phone as the cell phone
                //So why buck the trend?
				rolodexEntry.AlternatePhone = (string)entryRow["Agent Cell Phone"];
				rolodexEntry.CompanyName = (string)entryRow["Company Name"];
				rolodexEntry.DepartmentName = (string)entryRow["Agent Department"];
				rolodexEntry.Email = (string)entryRow["Agent Email"];
				rolodexEntry.Fax = (string)entryRow["Agent Fax"];
				rolodexEntry.Address.StreetAddress = (string)entryRow["Street Address"];
				rolodexEntry.Address.City = (string)entryRow["City"];
				rolodexEntry.Address.State = (string)entryRow["State"];
				rolodexEntry.Address.Zipcode = (string)entryRow["Zip"];
                rolodexEntry.Address.County = (string)entryRow["County"];
				rolodexEntry.Pager = (string)entryRow["Agent Pager"];
				rolodexEntry.PhoneOfCompany = (string)entryRow["Company Phone"];
				rolodexEntry.FaxOfCompany = (string)entryRow["Company Fax"];
				rolodexEntry.WebsiteUrl = (string)entryRow["Company Website"];
                rolodexEntry.AgentLicenseNumber = (string)entryRow["Agent License Number"];
                rolodexEntry.CompanyLicenseNumber = (string)entryRow["Company License Number"];
                rolodexEntry.Note = (string)entryRow["Notes"];
                rolodexEntry.PayToBankName = (string)entryRow["Pay To Bank Name"];
                rolodexEntry.PayToBankCityState = (string)entryRow["Pay To City/State"];
                rolodexEntry.PayToABANumber = (string)entryRow["Pay To ABA Number"];
                rolodexEntry.PayToAccountName = (string)entryRow["Pay To Account Name"];
                rolodexEntry.PayToAccountNumber = (string)entryRow["Pay To Account Number"];
                rolodexEntry.FurtherCreditToAccountName = (string)entryRow["Further Credit To Account Name"];
                rolodexEntry.FurtherCreditToAccountNumber = (string)entryRow["Further Credit To Account Number"];
                rolodexEntry.BranchName = (string)entryRow["Branch Name"];
                rolodexEntry.CompanyID = (string)entryRow["Company ID"];

                E_AgentRoleT agentType = E_AgentRoleT.Other;
                if (!string.IsNullOrEmpty((string)entryRow["Agent Type"]))
                {
                    try
                    {
                        agentType = (E_AgentRoleT)Enum.Parse(
                            typeof(E_AgentRoleT),
                            (string)entryRow["Agent Type"]);
                    }
                    catch (ArgumentException)
                    {
                        //We really shouldn't see this, but it doesn't hurt to watch out for it
                        LogFailure("Agent type \"" + (string)entryRow["Agent Type"] + "\" was not recognized on import", null, null, null);
                        return false;
                    }
                }

                rolodexEntry.Type = agentType;

                if (agentType == E_AgentRoleT.Other)
                {
                    rolodexEntry.TypeOtherDescription = (string)entryRow["Agent Type Other Description"];
                }
				
				if (!rolodexEntry.Save())
				{
					success = false;
				}
			}

			return success;
		}

		// We log to PB if the user failed to import.  Usually this
		// is a parse error.  The user gets a basic "Unable to Import"
		// message, and they can try again.
		private void LogFailure( string message, string token, string line, string[] tokenList )
		{
			const int MAX_TOKEN_LENGTH = 1200;
			const int MAX_LINE_LENGTH = 1200;
			string logMessage = string.Empty;

			if ( message != null && message != string.Empty )
			{
				logMessage += "RolodexImporter: Contacts Import Failure: " + message;
			}
			if ( token != null && token != string.Empty )
			{
				if ( token.Length < MAX_TOKEN_LENGTH )
				{
					logMessage += Environment.NewLine + Environment.NewLine + "***Token: " + token;
				}
				else
				{
					logMessage += Environment.NewLine + Environment.NewLine + "***Token[First " + MAX_TOKEN_LENGTH + " characters]: " + token.Substring( 0, MAX_TOKEN_LENGTH );
				}
			}
			if ( line != null && line != string.Empty )
			{
				if ( line.Length < MAX_LINE_LENGTH )
				{
					logMessage += Environment.NewLine + Environment.NewLine + "***Line: " + line;
				}
				else
				{
                    logMessage += Environment.NewLine + Environment.NewLine + "***Line[First " + MAX_LINE_LENGTH + " characters]: " + line.Substring( 0, MAX_LINE_LENGTH );
				}
			}
			if ( tokenList != null )
			{
				string list = arrayToString( tokenList );
				if ( list.Length < MAX_LINE_LENGTH )
				{
					logMessage += Environment.NewLine + Environment.NewLine + "***Token List: " + list;
				}
				else
				{
					logMessage += Environment.NewLine + Environment.NewLine + "***Token List [First " + MAX_LINE_LENGTH + " characters]: " + list.Substring( 0, MAX_LINE_LENGTH );
				}
			}

			if (logMessage != string.Empty)
			{
				Tools.LogInfo( logMessage );
			}
		}

		private string arrayToString( string[] stringList )
		{
			if ( stringList == null )
				return null;

			StringBuilder sb = new StringBuilder();
			
			foreach( string str in stringList )
			{
				if ( sb.Length != 0 )
					sb.Append( "," + str );
				else
					sb.Append( str );
			}
			return sb.ToString();
		}

	}
}