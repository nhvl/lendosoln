namespace LendersOffice.Rolodex
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading;
    using System.Web;
    using System.Web.UI.WebControls;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using System.Data.Common;

    public class RolodexDB
    {
        #region Private Members
        private Guid m_id;
        private Guid m_brokerID;
        private E_AgentRoleT m_type;
        private string m_typeOtherDescription = string.Empty;
        private string m_name = "";
        private string m_title = "";
        private string m_phone = "";
        private string m_alternatePhone = ""; // Cellphone.
        private string m_pager = "";
        private string m_fax = "";
        private string m_email = "";
        private string m_companyName = "";
        private string m_agentLicenseNum = "";
		private string m_companyLicenseNum = "";
        private CommonLib.Address m_address = new CommonLib.Address();
        private string m_note = "";
        private string m_departmentName = "";
        private string m_phoneOfCompany = "";
        private string m_faxOfCompany = "";
        private string titleProviderCode = null;
        private string m_websiteUrl = ""; // OPM 3286
		private bool m_isNew;
        private bool m_isNotifyWhenLoanStatusChange = false;
		private decimal m_CommissionPointOfLoanAmount = 0;
		private decimal m_CommissionPointOfGrossProfit = 0;
		private decimal m_CommissionMinBase = 0;
		private LicenseInfoList m_licenseInfoList; //OPM 2703
        private string m_companyID = "";
        private string m_branchName = ""; // OPM 109299 gf v
        private string m_payToBankName = "";
        private string m_payToBankCityState = "";
        private string m_payToABANumber = "";
        private string m_payToAccountName = "";
        private string m_payToAccountNumber = "";
        private string m_furtherCreditToAccountName = "";
        private string m_furtherCreditToAccountNumber = ""; // OPM 109299 gf ^
        private bool m_bypassReadPermissionCheck = false; // opm 206839 sk
        private bool m_agentIsApproved = true;      // OPM 109122
        private bool m_agentIsApprovedWasSet = false;
        private bool m_agentIsAffiliated = false;
        private bool m_overrideLicenses = false;
        #endregion

        #region Public properties
        /// <summary>
        /// Gets Rolodex unique id.
        /// </summary>
        public Guid ID 
        {
            get { return m_id; }
        }

        public Guid BrokerID 
        {
            get { return m_brokerID; }
            set { m_brokerID = value; }
        }

		public LicenseInfoList LicenseInformationList
		{
			get { return  m_licenseInfoList; }
			set { m_licenseInfoList = value; }
		}

        public E_AgentRoleT Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public string TypeDescription 
        {
            get { return GetTypeDescription(m_type); }
        }

        public string TypeOtherDescription
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }

        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string Title 
        {
            get { return m_title; }
            set { m_title = value; }
        }
        public string Phone 
        {
            get { return m_phone; }
            set { m_phone = value; }
        }
        public string AlternatePhone 
        {
            get { return m_alternatePhone; }
            set { m_alternatePhone = value; }
        }
        public string Pager 
        {
            get { return m_pager; }
            set { m_pager = value; }
        }
        public string Fax 
        {
            get { return m_fax; }
            set { m_fax = value; }
        }
        public string Email 
        {
            get { return m_email; }
            set { m_email = value; }
        }
        public string CompanyName 
        {
            get { return m_companyName; }
            set { m_companyName = value; }
        }

        public CommonLib.Address Address 
        {
            get { return m_address; }
            set { m_address = value; }
        }
        public string Note 
        {
            get { return m_note; }
            set { m_note = value; }
        }
        public string DepartmentName 
        {
            get { return m_departmentName; }
            set { m_departmentName = value; }
        }
        public string AgentLicenseNumber
        {
            get { return m_agentLicenseNum; }
            set { m_agentLicenseNum = value; }
        }

		public string CompanyLicenseNumber
		{
			get { return m_companyLicenseNum; }
			set { m_companyLicenseNum = value; }
		}

        public string PhoneOfCompany
        {
            get { return m_phoneOfCompany; }
            set { m_phoneOfCompany = value; }
        }

        public string FaxOfCompany
        {
            get { return m_faxOfCompany; }
            set { m_faxOfCompany = value; }
        }

        public string TitleProviderCode
        {
            get
            {
                // null => We haven't checked the DB for a code yet.
                // string.Empty => We have checked and there is none stored.
                // Only hit the DB when the value is null.
                if (this.titleProviderCode == null)
                {
                    this.titleProviderCode = TitleProviderCodeDB.Retrieve(this.BrokerID, this.ID);
                }

                return this.titleProviderCode;
            }

            set
            {
                this.titleProviderCode = value;
            }
        }

		public string WebsiteUrl
		{
			get { return m_websiteUrl; }
			set { m_websiteUrl = value; }
		}

        public bool IsNotifyWhenLoanStatusChange 
        {
            get { return m_isNotifyWhenLoanStatusChange; }
            set { m_isNotifyWhenLoanStatusChange = value; }
        }

		public decimal CommissionPointOfLoanAmount
		{
			get { return m_CommissionPointOfLoanAmount; }
			set { m_CommissionPointOfLoanAmount = value; }
		}

		public decimal CommissionPointOfGrossProfit
		{
			get { return m_CommissionPointOfGrossProfit; }
			set { m_CommissionPointOfGrossProfit = value; }
		}

		public decimal CommissionMinBase
		{
			get { return m_CommissionMinBase; }
			set { m_CommissionMinBase = value; }
		}

        public string CompanyID
        {
            get { return m_companyID; }
            set { m_companyID = value; }
        }

        public string BranchName
        {
            get { return m_branchName; }
            set { m_branchName = value; }
        }

        public string PayToBankName
        {
            get { return m_payToBankName; }
            set { m_payToBankName = value; }
        }

        public string PayToBankCityState
        {
            get { return m_payToBankCityState; }
            set { m_payToBankCityState = value; }
        }

        public Sensitive<string> PayToABANumber
        {
            get { return this.m_payToABANumber; }
            set { this.m_payToABANumber = value.Value; }
        }

        public string PayToAccountName
        {
            get { return m_payToAccountName; }
            set { m_payToAccountName = value; }
        }

        public Sensitive<string> PayToAccountNumber
        {
            get { return this.m_payToAccountNumber; }
            set { this.m_payToAccountNumber = value.Value; }
        }

        public string FurtherCreditToAccountName
        {
            get { return m_furtherCreditToAccountName; }
            set { m_furtherCreditToAccountName = value; }
        }

        public Sensitive<string> FurtherCreditToAccountNumber
        {
            get { return this.m_furtherCreditToAccountNumber; }
            set { this.m_furtherCreditToAccountNumber = value.Value; }
        }

        public bool AgentIsApproved // opm 109122
        {
            get { return m_agentIsApproved; }
            set 
            {
                if(!HasPermission(Permission.AllowManagingContactList))
                {
                    var msg = "User does not have permission to approve or disable contacts.";
                    throw new CBaseException(msg, msg);
                }
                m_agentIsApproved = value;
                m_agentIsApprovedWasSet = true;
            }
        }
        public bool AgentIsAffiliated
        {
            get { return m_agentIsAffiliated; }
            set { m_agentIsAffiliated = value; }
        }

        public bool OverrideLicenses
        {
            get { return m_overrideLicenses; }
            set { m_overrideLicenses = value; }
        }

        /// <summary>
        /// Gets the value for the entry's unique system ID.
        /// </summary>
        /// <value>
        /// The per-broker unique system ID.
        /// </value>
        public string SystemId { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this entry is a 
        /// settlement service provider.
        /// </summary>
        /// <value>
        /// True if the contact is a settlement service provider,
        /// false otherwise.
        /// </value>
        public bool IsSettlementServiceProvider { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// This constructor is intended primarily for static retrieval methods.
        /// </summary>
        private RolodexDB(Guid brokerID, IDataReader reader, E_BypassReadPermissionCheckT bypassReadPermissionCheckT)
        {
            m_isNew = false;
            m_id = (Guid)reader["AgentID"];
            m_brokerID = brokerID;

            switch(bypassReadPermissionCheckT)
            {
                case E_BypassReadPermissionCheckT.True:
                    m_bypassReadPermissionCheck = true;
                    break;
                case E_BypassReadPermissionCheckT.False:
                    m_bypassReadPermissionCheck= false;
                    break;
                default:
                    throw new UnhandledEnumException(bypassReadPermissionCheckT);
            }

            PullInfoFromReader(reader);
        }

        public RolodexDB(Guid id, Guid brokerID)
        {
            m_isNew = false;
            m_id = id;
            m_brokerID = brokerID;
        }

        public RolodexDB(Guid id, Guid brokerID, E_BypassReadPermissionCheckT bypassPermissionCheck)
            : this(id, brokerID)
        {
            this.m_bypassReadPermissionCheck = bypassPermissionCheck == E_BypassReadPermissionCheckT.True;
        }

        public RolodexDB()
        {
            m_isNew = true;
            m_licenseInfoList = new LicenseInfoList();
        }
        #endregion

        private static bool HasPermission(Permission permission)
        {
            var p = Thread.CurrentPrincipal;
            var u = p as AbstractUserPrincipal;
            return u != null && u.HasPermission(permission);
        }

        /// <summary>
        /// does not update AgentID or BrokerID, though at least one of those may be in the reader.
        /// </summary>
        private void PullInfoFromReader(IDataReader reader)
        {
            if (false == m_bypassReadPermissionCheck && !HasPermission(Permission.AllowReadingFromRolodex))
            {
                var msg = "User does not have permission to read from contacts.";
                throw new CBaseException(msg, msg);
            }

            this.SystemId = reader["SystemId"].ToString();
            m_type = (E_AgentRoleT)(int)reader["AgentType"];
            
            if (reader["AgentTypeOtherDescription"] != DBNull.Value)
            {
                m_typeOtherDescription = (string)reader["AgentTypeOtherDescription"];
            }

            m_name = (string)reader["AgentNm"];
            m_title = (string)reader["AgentTitle"];
            m_phone = (string)reader["AgentPhone"];
            m_alternatePhone = (string)reader["AgentAltPhone"];
            m_pager = (string)reader["AgentPager"];
            m_fax = (string)reader["AgentFax"];
            m_email = (string)reader["AgentEmail"];
            m_companyName = (string)reader["AgentComNm"];
            m_address.StreetAddress = (string)reader["AgentAddr"];
            m_address.City = (string)reader["AgentCity"];
            m_address.State = (string)reader["AgentState"];
            m_address.Zipcode = (string)reader["AgentZip"];
            m_address.County = (string)reader["AgentCounty"];
            m_agentLicenseNum = (string)reader["AgentLicenseNumber"];
            m_companyLicenseNum = (string)reader["CompanyLicenseNumber"];
            m_note = (string)reader["AgentN"];
            m_departmentName = (string)reader["AgentDepartmentName"];
            m_phoneOfCompany = (string)reader["PhoneOfCompany"];
            m_faxOfCompany = (string)reader["FaxOfCompany"];
            m_websiteUrl = (string)reader["AgentWebsiteUrl"]; // OPM 3286

            //OPM 630
            m_CommissionPointOfLoanAmount = (decimal)reader["CommissionPointOfLoanAmount"];
            m_CommissionPointOfGrossProfit = (decimal)reader["CommissionPointOfGrossProfit"];
            m_CommissionMinBase = (decimal)reader["CommissionMinBase"];

            m_companyID = (string)reader["CompanyId"];

            // OPM 109299
            m_branchName = (string)reader["AgentBranchName"];
            m_payToBankName = (string)reader["AgentPayToBankName"];
            m_payToBankCityState = (string)reader["AgentPayToBankCityState"];
            m_payToABANumber = (string)reader["AgentPayToABANumber"];
            m_payToAccountName = (string)reader["AgentPayToAccountName"];
            m_payToAccountNumber = (string)reader["AgentPayToAccountNumber"];
            m_furtherCreditToAccountName = (string)reader["AgentFurtherCreditToAccountName"];
            m_furtherCreditToAccountNumber = (string)reader["AgentFurtherCreditToAccountNumber"];
            this.IsSettlementServiceProvider = (bool)reader["IsSettlementServiceProvider"];
            m_agentIsApproved = (bool)reader["AgentIsApproved"]; // OPM 109122
            m_agentIsAffiliated = (bool)reader["AgentIsAffiliated"];
            m_overrideLicenses = (bool)reader["OverrideLicenses"];

            // 3/15/07 db - OPM 2703 
            try
            {
                if (reader["LicenseXmlContent"].ToString().TrimWhitespaceAndBOM() != "")
                {
                    m_licenseInfoList = LicenseInfoList.ToObject((string)reader["LicenseXmlContent"]);
                }
            }
            catch (Exception e)
            {

                Tools.LogError("Failed to convert agent's (" + m_name + ") license info.", e);
            }

            if (m_licenseInfoList == null)
            {
                m_licenseInfoList = new LicenseInfoList();
            }

            if (!(reader["IsNotifyWhenLoanStatusChange"] is DBNull))
            {
                m_isNotifyWhenLoanStatusChange = (bool)reader["IsNotifyWhenLoanStatusChange"];
            }
        }

        private static DbDataReader GetReaderByRolodexFilter(RolodexFilter filter)
        {
            //if (!HasPermission(Permission.AllowReadingFromRolodex))
            //{
            //    var msg = "User does not have permission to read from contacts.";
            //    throw new CBaseException(msg, msg);
            //}
            var parameters = new System.Collections.Generic.List<SqlParameter>() 
                { 
                      new SqlParameter("@BrokerID", filter.BrokerID)
                    , new SqlParameter("@NameFilter", filter.NameFilter)
                    , new SqlParameter("@TypeFilter", filter.TypeFilter)
                    , new SqlParameter("@IsApprovedFilter", filter.IsApprovedFilter)
                };
            
            return StoredProcedureHelper.ExecuteReader(filter.BrokerID, "ListRolodexByBrokerID", parameters);
        }

        public static IEnumerable<RolodexDB> ListRolodexByRolodexFilter(RolodexFilter filter)
        {
            var contacts = new List<RolodexDB>();
            using (var reader = GetReaderByRolodexFilter(filter))
            {
                while (reader.Read())
                {
                    contacts.Add(new RolodexDB(filter.BrokerID, reader, filter.BypassReadPermissionCheckT));
                }
            }
            return contacts;
        }

        /// <summary>
        /// Retrieves the contact entries for the broker with the specified <paramref name="brokerId"/>.
        /// NOTE: Does not enforce AllowReadingFromRolodex permission check on the retrieved contacts.
        /// </summary>
        /// <param name="brokerId">
        /// The broker where the contacts are defined.
        /// </param>
        /// <param name="restrictToSettlementServiceProviders">
        /// True if the result should be restricted to settlement service provider entries,
        /// false otherwise.
        /// </param>
        /// <returns>
        /// A <see cref="Dictionary{TKey, TValue}"/> with the contact entries by system ID.
        /// </returns>
        public static Dictionary<string, RolodexDB> ListRolodexEntriesBySystemIdWithPermissionBypass(Guid brokerId, bool restrictToSettlementServiceProviders = false)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@RestrictToSettlementServiceProviders", restrictToSettlementServiceProviders)
            };

            var procedureName = StoredProcedureName.Create("ListRolodexEntries");
            if (!procedureName.HasValue)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError);
            }

            var result = new Dictionary<string, RolodexDB>();

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName.Value, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    var contact = new RolodexDB(brokerId, reader, E_BypassReadPermissionCheckT.True);
                    result.Add(contact.SystemId, contact);
                }
            }

            return result;
        }

        public bool Retrieve() 
        {
            if (false == m_bypassReadPermissionCheck && !HasPermission(Permission.AllowReadingFromRolodex))
            {
                var msg = "User does not have permission to read from contacts.";
                throw new CBaseException(msg, msg);
            }
            
            if (m_isNew) 
            {
                return false;
            }
            bool isExisted = false;
            try 
            {
                SqlParameter[] parameters = new SqlParameter[] {
                                                                   new SqlParameter("@AgentID", m_id),
                                                                   new SqlParameter("@BrokerID", m_brokerID)
                                                               };

				using (var reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "RetrieveRolodexByID", parameters))
				{
					if (reader.Read()) 
					{
                        PullInfoFromReader(reader);

                        isExisted = true;
					}
				}
            } 
            catch 
            {
            }

            return isExisted;
        }
        public bool Save() 
        {
            if (!HasPermission(Permission.AllowWritingToRolodex))
            {
                var msg = "User does not have permission to write to contacts.";
                throw new CBaseException(msg, msg);
            }
            bool isSuccessful = false;
            var rowsAffected = 0;
            StoredProcedureName procedureName;
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter agentIDParam = new SqlParameter("@AgentID", SqlDbType.UniqueIdentifier);
            agentIDParam.Direction = ParameterDirection.Output;
            if (m_isNew) 
            {
                procedureName = StoredProcedureName.Create("CreateRolodex").Value;
                parameters.Add(agentIDParam);
            } 
            else 
            {
                procedureName = StoredProcedureName.Create("UpdateRolodex").Value;
                parameters.Add(new SqlParameter("@AgentID", m_id));
            }

            parameters.Add(new SqlParameter("@BrokerID", m_brokerID));
            parameters.Add(new SqlParameter("@AgentType", (int) m_type));
            parameters.Add(new SqlParameter("@AgentTypeOtherDescription", m_typeOtherDescription));
            parameters.Add(new SqlParameter("@AgentNm", m_name));
            parameters.Add(new SqlParameter("@AgentTitle", m_title));
            parameters.Add(new SqlParameter("@AgentPhone", m_phone));
            parameters.Add(new SqlParameter("@AgentAltPhone", m_alternatePhone));
            parameters.Add(new SqlParameter("@AgentPager", m_pager));
            parameters.Add(new SqlParameter("@AgentFax", m_fax));
            parameters.Add(new SqlParameter("@AgentEmail", m_email));
            parameters.Add(new SqlParameter("@AgentComNm", m_companyName));
            parameters.Add(new SqlParameter("@AgentAddr", m_address.StreetAddress));
            parameters.Add(new SqlParameter("@AgentCity", m_address.City));
            parameters.Add(new SqlParameter("@AgentState", m_address.State));
            parameters.Add(new SqlParameter("@AgentZip", m_address.Zipcode));
            parameters.Add(new SqlParameter("@AgentCounty", m_address.County));
            parameters.Add(new SqlParameter("@AgentLicenseNumber", m_agentLicenseNum));
			parameters.Add(new SqlParameter("@CompanyLicenseNumber", m_companyLicenseNum));
            parameters.Add(new SqlParameter("@AgentN", m_note));
            parameters.Add(new SqlParameter("@AgentDepartmentName", m_departmentName));
            parameters.Add(new SqlParameter("@PhoneOfCompany", m_phoneOfCompany));
            parameters.Add(new SqlParameter("@FaxOfCompany", m_faxOfCompany));
			parameters.Add(new SqlParameter("@AgentWebsiteUrl", m_websiteUrl));  // OPM 3286
            parameters.Add(new SqlParameter("@IsNotifyWhenLoanStatusChange", m_isNotifyWhenLoanStatusChange));

			//OPM 630
			parameters.Add(new SqlParameter("@CommissionPointOfLoanAmount", m_CommissionPointOfLoanAmount));
			parameters.Add(new SqlParameter("@CommissionPointOfGrossProfit", m_CommissionPointOfGrossProfit));
			parameters.Add(new SqlParameter("@CommissionMinBase", m_CommissionMinBase));
            
			//OPM 2703
			if(m_licenseInfoList == null)
				m_licenseInfoList = new LicenseInfoList();
			parameters.Add(new SqlParameter("@LicenseXmlContent", m_licenseInfoList.ToString()));

            parameters.Add(new SqlParameter("@CompanyId", m_companyID));

            // OPM 109299
            parameters.Add(new SqlParameter("@AgentBranchName", m_branchName));
            parameters.Add(new SqlParameter("@AgentPayToBankName", m_payToBankName));
            parameters.Add(new SqlParameter("@AgentPayToBankCityState", m_payToBankCityState));
            parameters.Add(new SqlParameter("@AgentPayToABANumber", m_payToABANumber));
            parameters.Add(new SqlParameter("@AgentPayToAccountName", m_payToAccountName));
            parameters.Add(new SqlParameter("@AgentPayToAccountNumber", m_payToAccountNumber));
            parameters.Add(new SqlParameter("@AgentFurtherCreditToAccountName", m_furtherCreditToAccountName));
            parameters.Add(new SqlParameter("@AgentFurtherCreditToAccountNumber", m_furtherCreditToAccountNumber));

            // OPM 109122
            if (m_agentIsApprovedWasSet)
            {
                parameters.Add(new SqlParameter("@AgentIsApproved", m_agentIsApproved));
            }

            parameters.Add(new SqlParameter("AgentIsAffiliated", m_agentIsAffiliated));
            parameters.Add(new SqlParameter("OverrideLicenses", m_overrideLicenses));
            parameters.Add(new SqlParameter("@IsSettlementServiceProvider", this.IsSettlementServiceProvider));

            using (var exec = new CStoredProcedureExec(this.m_brokerID))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    if (this.m_isNew)
                    {
                        int rolodexSystemIdCounter = BrokerDB.GetAndUpdateRolodexSystemIdCounter(this.BrokerID, exec);
                        parameters.Add(new SqlParameter("@SystemId", rolodexSystemIdCounter));
                    }

                    rowsAffected = exec.ExecuteNonQuery(procedureName.ToString(), parameters.ToArray());

                    if (rowsAffected > 0)
                    {
                        isSuccessful = true;

                        if (m_isNew)
                        {
                            m_id = (Guid)agentIDParam.Value;
                            m_isNew = false;
                        }

                        this.SaveProviderCode(exec);
                    }

                    exec.CommitTransaction();
                }
                catch (Exception exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError(exc);
                }
            }

            return isSuccessful;
        }

        private static ReadOnlyDictionary<int, string> m_RoleDescriptionByRoleTypeValue;
        private static readonly object m_RoleDescriptionByRoleTypeLock = new Object();

        public static ReadOnlyDictionary<int, string> GetRoleDecriptionByRoleTypeValue()
        {
            lock (m_RoleDescriptionByRoleTypeLock)
            {
                if (m_RoleDescriptionByRoleTypeValue == null)
                {
                    var agentRoleTypes = Enum.GetValues(typeof(E_AgentRoleT));
                    var roleDescByRoleTypeValue = new Dictionary<int, string>(agentRoleTypes.Length);
                    foreach (E_AgentRoleT roleT in agentRoleTypes)
                    {
                        roleDescByRoleTypeValue.Add((int)roleT, GetTypeDescription(roleT));
                    }

                    m_RoleDescriptionByRoleTypeValue = new ReadOnlyDictionary<int, string>(roleDescByRoleTypeValue);
                }

                return m_RoleDescriptionByRoleTypeValue;
            }
        }
        
        public static string GetTypeDescription(E_AgentRoleT t, bool useShortened = false) 
        {
            string ret = "";
            switch (t) 
            {
                case E_AgentRoleT.Appraiser:
                    ret = "Appraiser"; break;
                case E_AgentRoleT.Bank:
                    ret = "Bank"; break;
				case E_AgentRoleT.Broker:
					ret = "Broker"; break;
				case E_AgentRoleT.BrokerRep:
					ret = "Broker Rep"; break;
                case E_AgentRoleT.Builder:
                    ret = "Builder"; break;
                case E_AgentRoleT.BuyerAgent:
                    ret = "Buyer Agent"; break;
                case E_AgentRoleT.BuyerAttorney:
                    ret = "Buyer Attorney"; break;
                case E_AgentRoleT.CallCenterAgent:
                    ret = "Call Center Agent"; break;
                case E_AgentRoleT.ClosingAgent:
                    ret = "Closing Agent"; break;
                case E_AgentRoleT.CreditReport:
                    ret = "Credit Report"; break;
                case E_AgentRoleT.CreditReportAgency2:
                    ret = "Credit Report Agency #2"; break;
                case E_AgentRoleT.CreditReportAgency3:
                    ret = "Credit Report Agency #3"; break;
				case E_AgentRoleT.ECOA:
					ret = "ECOA"; break;
                case E_AgentRoleT.Escrow:
                    ret = "Escrow"; break;
                case E_AgentRoleT.FairHousingLending:
                    ret = "Fair Housing Lending"; break;
                case E_AgentRoleT.FloodProvider:
                    ret = "Flood Provider"; break;
                case E_AgentRoleT.HazardInsurance:
                    ret = "Homeowner Insurance"; break;
                case E_AgentRoleT.HomeOwnerInsurance:
                    ret = "Homeowner Insurance (OBSOLETE)"; break;
                case E_AgentRoleT.HomeOwnerAssociation:
                    ret = "Homeowner Association"; break;
                case E_AgentRoleT.Investor:
                    ret = "Investor"; break;
                case E_AgentRoleT.Lender:
                    ret = "Lender"; break;
                case E_AgentRoleT.ListingAgent:
                    ret = "Listing Agent"; break;
				case E_AgentRoleT.LoanOfficer:
					ret = "Loan Officer"; break;
                case E_AgentRoleT.LoanOpener:
                    ret = "Loan Opener"; break;
                case E_AgentRoleT.Manager:
                    ret = "Manager"; break;
                case E_AgentRoleT.MarketingLead:
                    ret = "Marketing Lead"; break;
                case E_AgentRoleT.MortgageInsurance:
                    ret = "Mortgage Insurance"; break;
                case E_AgentRoleT.Mortgagee:
                    ret = "Mortgagee"; break;
                case E_AgentRoleT.Other:
                    ret = "Other"; break;
                case E_AgentRoleT.Processor:
                    ret = "Processor"; break;
                case E_AgentRoleT.Realtor:
                    ret = "Realtor"; break;
				case E_AgentRoleT.Seller:
					ret = "Seller"; break;
                case E_AgentRoleT.SellerAttorney:
                    ret = "Seller Attorney"; break;
                case E_AgentRoleT.SellingAgent:
                    ret = "Selling Agent"; break;
                case E_AgentRoleT.Servicing:
                    ret = "Servicing"; break;
				case E_AgentRoleT.Surveyor:
					ret = "Surveyor"; break;
                case E_AgentRoleT.Title:
                    ret = "Title"; break;
                case E_AgentRoleT.Underwriter:
                    ret = "Underwriter"; break;
				case E_AgentRoleT.HomeInspection:
					ret = "Home Inspection"; break;
                case E_AgentRoleT.Shipper:
                    ret = "Shipper"; break;
                case E_AgentRoleT.BrokerProcessor:
                    ret = "Processor (External)"; break;
                case E_AgentRoleT.Trustee:
                    ret = "Trustee"; break;
                case E_AgentRoleT.Funder:
                    ret = "Funder"; break;
                case E_AgentRoleT.PostCloser:
                    ret = "Post-Closer"; break;
                case E_AgentRoleT.Insuring:
                    ret = "Insuring"; break;
                case E_AgentRoleT.CollateralAgent:
                    ret = "Collateral Agent"; break;
                case E_AgentRoleT.DocDrawer:
                    ret = "Doc Drawer"; break;
                case E_AgentRoleT.PropertyManagement:
                    ret = "Property Management"; break;
                case E_AgentRoleT.TitleUnderwriter:
                    ret = "Title Underwriter"; break;
                case E_AgentRoleT.CreditAuditor:
                    ret = "Credit Auditor"; break;
                case E_AgentRoleT.DisclosureDesk:
                    ret = "Disclosure Desk"; break;
                case E_AgentRoleT.JuniorProcessor:
                    ret = "Junior Processor"; break;
                case E_AgentRoleT.JuniorUnderwriter:
                    ret = "Junior Underwriter"; break;
                case E_AgentRoleT.LegalAuditor:
                    ret = "Legal Auditor"; break;
                case E_AgentRoleT.LoanOfficerAssistant:
                    ret = "Loan Officer Assistant"; break;
                case E_AgentRoleT.Purchaser:
                    ret = "Purchaser"; break;
                case E_AgentRoleT.QCCompliance:
                    ret = "QC Compliance"; break;
                case E_AgentRoleT.Secondary:
                    ret = "Secondary"; break;
                case E_AgentRoleT.PestInspection:
                    ret = "Pest Inspection"; break;
                case E_AgentRoleT.Subservicer:
                    ret = "Subservicer"; break;
                case E_AgentRoleT.ExternalSecondary:
                    ret = "Secondary (External)"; break;
                case E_AgentRoleT.ExternalPostCloser:
                    ret = "Post-Closer (External)"; break;
                case E_AgentRoleT.LoanPurchasePayee:
                    ret = "Loan Purchase Payee"; break;
                case E_AgentRoleT.AppraisalManagementCompany:
                    ret = useShortened ? "AMC" : "Appraisal Management Company"; break;
                case E_AgentRoleT.Referral:
                    ret = "Referral"; break;
            }
            return ret;
        }

        public static IEnumerable<E_AgentRoleT> GetAgentTypeOptions()
        {
            return new[] {
                E_AgentRoleT.Appraiser,
                E_AgentRoleT.AppraisalManagementCompany,
                E_AgentRoleT.Bank,
                E_AgentRoleT.Broker,
                E_AgentRoleT.BrokerRep,
                E_AgentRoleT.Builder,
                E_AgentRoleT.BuyerAgent,
                E_AgentRoleT.BuyerAttorney,
                E_AgentRoleT.CallCenterAgent,
                E_AgentRoleT.ClosingAgent,
                E_AgentRoleT.CollateralAgent,
                E_AgentRoleT.CreditAuditor,
                E_AgentRoleT.CreditReport,
                E_AgentRoleT.CreditReportAgency2,
                E_AgentRoleT.CreditReportAgency3,
                E_AgentRoleT.DisclosureDesk,
                E_AgentRoleT.DocDrawer,
                E_AgentRoleT.ECOA,
                E_AgentRoleT.Escrow,
                E_AgentRoleT.FairHousingLending,
                E_AgentRoleT.FloodProvider,
                E_AgentRoleT.Funder,
                E_AgentRoleT.HomeInspection,
                E_AgentRoleT.HomeOwnerAssociation,
                E_AgentRoleT.HazardInsurance,
                E_AgentRoleT.Insuring,
                E_AgentRoleT.Investor,
                E_AgentRoleT.JuniorProcessor,
                E_AgentRoleT.JuniorUnderwriter,
                E_AgentRoleT.LegalAuditor,
                E_AgentRoleT.Lender,
                E_AgentRoleT.ListingAgent,
                E_AgentRoleT.LoanOfficer,
                E_AgentRoleT.LoanOfficerAssistant,
                E_AgentRoleT.LoanOpener,
                E_AgentRoleT.LoanPurchasePayee,
                E_AgentRoleT.Manager,
                E_AgentRoleT.MarketingLead,
                E_AgentRoleT.MortgageInsurance,
                E_AgentRoleT.Mortgagee,
                E_AgentRoleT.PestInspection,
                E_AgentRoleT.PostCloser,
                E_AgentRoleT.ExternalPostCloser,
                E_AgentRoleT.Processor,
                E_AgentRoleT.BrokerProcessor,
                E_AgentRoleT.PropertyManagement,
                E_AgentRoleT.Purchaser,
                E_AgentRoleT.QCCompliance,
                E_AgentRoleT.Realtor,
                E_AgentRoleT.Referral,
                E_AgentRoleT.Secondary,
                E_AgentRoleT.ExternalSecondary,
                E_AgentRoleT.Seller,
                E_AgentRoleT.SellerAttorney,
                E_AgentRoleT.SellingAgent,
                E_AgentRoleT.Servicing,
                E_AgentRoleT.Shipper,
                E_AgentRoleT.Subservicer,
                E_AgentRoleT.Surveyor,
                E_AgentRoleT.Title,
                E_AgentRoleT.TitleUnderwriter,
                E_AgentRoleT.Trustee,
                E_AgentRoleT.Underwriter,
                E_AgentRoleT.Other,
            }.Where(t =>
            {
                AbstractUserPrincipal p = HttpContext.Current.User as AbstractUserPrincipal;

                bool isPmlRole =
                    t == E_AgentRoleT.BrokerProcessor
                    || t == E_AgentRoleT.ExternalSecondary
                    || t == E_AgentRoleT.ExternalPostCloser;

                return p.HasFeatures(E_BrokerFeatureT.PriceMyLoan) || !isPmlRole;
            });
        }

        public static IEnumerable<KeyValuePair<string, E_AgentRoleT>> GetAgentRolesByTypeDescription()
        {
            return GetAgentTypeOptions().Select(type => new KeyValuePair<string, E_AgentRoleT>(GetTypeDescription(type), type));
        }

        public static void PopulateAgentTypeDropDownList(DropDownList ddl, bool useShortened = false) 
        {
            ddl.Items.AddRange(GetAgentTypeOptions().Select(t => new ListItem(GetTypeDescription(t, useShortened), t.ToString("D"))).ToArray());
        }

        public static string GetAgentType(string code, string otherDesc)
        {
            int v;

            if (string.IsNullOrEmpty(code) == false)
            {
                if (int.TryParse(code, out v))
                {
                    return GetAgentType((E_AgentRoleT)v, otherDesc);
                }
            }
            return "";
        }
        public static string GetAgentType(E_AgentRoleT role, string otherDesc)
        {
            if (role != E_AgentRoleT.Other || otherDesc.TrimWhitespaceAndBOM() == "")
            {
                return RolodexDB.GetTypeDescription(role);
            }
            else
            {
                return otherDesc;
            }
        }

        public static List<RolodexDB> GetPmlBrokerAffiliates(Guid BrokerId, Guid PmlBrokerId)
        {
            List<RolodexDB> affiliates = new List<RolodexDB>();

            SqlParameter[] parameters = new SqlParameter[] {
                                                                new SqlParameter("@BrokerID", BrokerId),
                                                                new SqlParameter("@PmlBrokerId", PmlBrokerId)
                                                           };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "GetPmlBrokerAffiliates", parameters))
            {
                while (reader.Read())
                {
                    affiliates.Add(new RolodexDB(BrokerId, reader, E_BypassReadPermissionCheckT.False));
                }
            }

            return affiliates;
        }

        /// <summary>
        /// Saves the Title provider code to the database.
        /// </summary>
        /// <param name="exec">The context of the DB call.</param>
        private void SaveProviderCode(CStoredProcedureExec exec)
        {
            if (string.IsNullOrEmpty(this.titleProviderCode))
            {
                // No point storing a link with an empty provider code.
                TitleProviderCodeDB.Delete(exec, this.m_brokerID, this.m_id);
            }
            else
            {
                TitleProviderCodeDB.Save(exec, this.m_brokerID, this.m_id, this.titleProviderCode);
            }
        }
    }

    public class RolodexFilter
    {
        public Guid BrokerID;

        /// <summary>
        /// can be null
        /// </summary>
        public string NameFilter;
        public int? TypeFilter;
        public bool? IsApprovedFilter;
        public E_BypassReadPermissionCheckT BypassReadPermissionCheckT = E_BypassReadPermissionCheckT.False;

        /// <summary>
        /// all filters require brokerid.
        /// </summary>
        public RolodexFilter(Guid brokerID)
        {
            BrokerID = brokerID;
        }
    }
}
