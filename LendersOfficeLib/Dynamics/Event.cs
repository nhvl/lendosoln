using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using LendersOffice.Security;
using LendersOffice.Common;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Dynamics
{
	/// <summary>
	/// Label each event member variable with this attribute to
	/// help serialization.
	/// </summary>

    [AttributeUsage(AttributeTargets.Property)]
    public class EventMemberAttribute : XmlElementAttribute
    {
        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventMemberAttribute(String sLabel, Type tMember)
            : base(sLabel, tMember)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventMemberAttribute(Type tMember)
            : base(tMember)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventMemberAttribute(String sLabel)
            : base(sLabel)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventMemberAttribute()
        {
        }

        #endregion

    }

	/// <summary>
	/// Label each event member array with this attribute to
	/// help serialization.
	/// </summary>

    [AttributeUsage(AttributeTargets.Property)]
    public class EventArrayAttribute : XmlArrayItemAttribute
    {
        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventArrayAttribute(String sLabel, Type tMember)
            : base(sLabel, tMember)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventArrayAttribute(Type tMember)
            : base(tMember)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventArrayAttribute(String sLabel)
            : base(sLabel)
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EventArrayAttribute()
        {
        }

        #endregion

    }

	/// <summary>
	/// Each event processed by the framework must adhere to this
	/// common base class.  Though simplistic, it makes for a nice
	/// placeholder for low-level changes.  Note for event designers:
	/// To have your event participate in serialization, use public
	/// properties with both a 'get' and 'set' accessor and decorate
	/// with the appropriate attribute (either event-member or
	/// event-array):
	/// 
	///	public class MyEvent : DispatchEvent
	///	{
	///		...
	///	
	///		[ EventMember( "MyProperty" )
	///		]
	///		public String MyProperty
	///		{
	///			// Access member.
	///	
	///			set
	///			{
	///				{assign something}
	///			}
	///			get
	///			{
	///				{return something}
	///			}
	///		}
	///	
	///		...
	///	
	///		[ EventArray( "Item" , typeof( String ) )
	///		]
	///		public ArrayList MyList
	///		{
	///			// Access member.
	///	
	///			set
	///			{
	///				{assign something}
	///			}
	///			get
	///			{
	///				{return something}
	///			}
	///		}
	///	
	///		...
	///	
	///	}
	///	
	///	In this manner you decorate all members of a custom
	///	event.  Each member will be serialized to comprise a
	///	single xml document.
	/// </summary>

	public abstract class DispatchEvent
	{
		/// <summary>
		/// Keep track of basic event interface elements.  For
		/// now, we track the option identifier and the employee's
		/// display name (presumably, his/her full name).
		/// </summary>

		private E_ApplicationT m_AppId = E_ApplicationT.LendersOffice;
		private DateTime  m_OccurredOn = DateTime.Now;
		private String    m_BrokerName = String.Empty;
		private String      m_WhoDidIt = String.Empty;
		private Guid        m_WhoDidId = Guid.Empty;
		private Guid        m_BrokerId = Guid.Empty;

		public abstract String EventDescription
		{
			// Access member.

			get;
		}

		public abstract String EventDetails
		{
			// Access member.

			get;
		}

		public abstract Object ArgumentData
		{
			// Access member.

			get;
		}

		public abstract String Label
		{
			// Access member.

			get;
		}

		#region ( Event properties )

		public E_ApplicationT AppId
		{
			// Access member.

			set
			{
				m_AppId = value;
			}
			get
			{
				return m_AppId;
			}
		}

		public DateTime OccurredOn
		{
			// Access member.

			set
			{
				m_OccurredOn = value;
			}
			get
			{
				return m_OccurredOn;
			}
		}

		public String WhoDidIt
		{
			// Access member.

			set
			{
				m_WhoDidIt = value;
			}
			get
			{
				return m_WhoDidIt;
			}
		}

		public Guid WhoDidId
		{
			// Access member.

			set
			{
				m_WhoDidId = value;
			}
			get
			{
				return m_WhoDidId;
			}
		}

		public Guid BrokerId
		{
			// Access member.

			set
			{
				m_BrokerId = value;
			}
			get
			{
				return m_BrokerId;
			}
		}

		public String BrokerName
		{
			// Access member.

			set
			{
				m_BrokerName = value;
			}
			get
			{
				return m_BrokerName;
			}
		}

		#endregion

	}


}
