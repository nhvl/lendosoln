﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using global::ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;

    /// <summary>
    /// Converts the ExecutingEngineDebug's failed parameters to user readable messages
    /// </summary>
    public class ExecutionEngineDebugMessage
    {        
        private const int MAX_CONDITIONS_TO_DISPLAY = 10;
        private const int MAX_PARAMS_TO_DISPLAY = 10;
        Guid _brokerId;
        private LendingQBExecutingEngineDebug m_lendingQBExecutingEngineDebug;

        private ConditionType? failedConditionType;

        private IReadOnlyCollection<string> userFriendlyMessages;

        public ExecutionEngineDebugMessage(Guid brokerId, LendingQBExecutingEngineDebug lendingQBExecutingEngineDebug)
        {
            _brokerId = brokerId;            
            m_lendingQBExecutingEngineDebug = lendingQBExecutingEngineDebug;
        }

        public ConditionType FailedConditionType
        {
            get
            {
                if (this.failedConditionType == null)
                {
                    this.GetUserFriendlyMessagesList();
                }

                return this.failedConditionType.Value;
            }
        }

        public IReadOnlyCollection<string> UserFriendlyMessages
        {
            get
            {
                if (this.userFriendlyMessages == null)
                {
                    this.GetUserFriendlyMessagesList();
                }

                return this.userFriendlyMessages;
            }
        }

        public override string ToString()
        {
            if (this.UserFriendlyMessages.Count == 1)
            {
                return this.UserFriendlyMessages.First();
            }

            StringBuilder sbMessage = new StringBuilder();
      
            int nCharIdx = 'A';
            foreach(string sMessage in this.UserFriendlyMessages)
            {                
                sbMessage.AppendLine((char)(nCharIdx++) + ". " + sMessage);
            }

            return sbMessage.ToString();
        }

        private SystemConfig GetRepo()
        {
            IConfigRepository repository = ConfigHandler.GetRepository(_brokerId);
            var activeRelease = repository.LoadActiveRelease(_brokerId);
            if (activeRelease == null)
            {
                var systemRepository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
                activeRelease = repository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid);
            }
            return activeRelease.Configuration;
        }

        private HashSet<WorkflowOperation> getMessageFromRuleOps = new HashSet<WorkflowOperation>()
        {
            WorkflowOperations.ExportToCoreSystem,
            WorkflowOperations.GenerateDocs,
            WorkflowOperations.OrderAppraisal,
            WorkflowOperations.OrderCredit,
            WorkflowOperations.OrderVoa,
            WorkflowOperations.OrderInitialDisclosureInTpoPortal,
            WorkflowOperations.OrderVoe,
            WorkflowOperations.GenerateUCD,
            WorkflowOperations.RunDo,
            WorkflowOperations.RunDu,
            WorkflowOperations.RunLp,
            WorkflowOperations.OrderTitleQuote,
            WorkflowOperations.ApplyTitleQuote,
            WorkflowOperations.LeadToLoanConversion,
            WorkflowOperations.UseDocumentCapture,
            WorkflowOperations.OrderMIPolicyDelegated,
            WorkflowOperations.OrderMIPolicyNonDelegated,
            WorkflowOperations.OrderMIQuote,
            WorkflowOperations.UploadEDocs
        };

        private HashSet<WorkflowOperation> fallBackToSystemConfigWhenNoPrivilegeDenialMessage = new HashSet<WorkflowOperation>()
        {
            WorkflowOperations.ExportToCoreSystem,
            WorkflowOperations.LeadToLoanConversion,
        };

        private void GetUserFriendlyMessagesList()
        {
            List<string> messageList = GetUserFriendlyMessagesForRestraint();
            if (messageList.Count > 0 )
            {
                this.failedConditionType = ConditionType.Restraint;
                this.userFriendlyMessages = messageList;
                return;
            }

            this.failedConditionType = ConditionType.Privilege;
            this.userFriendlyMessages = GetUserFriendlyMessagesForPrivileges();
        }

        private List<string> GetUserFriendlyMessagesForRestraint()
        {
            SystemConfig config = GetRepo();
            List<string> messagesList = new List<string>();

            foreach (var opDebugList in m_lendingQBExecutingEngineDebug.ExecutingEngineDebugList)
            {
                if (opDebugList.CanPerform)
                {
                    continue;
                }

                foreach (var conditionDebug in opDebugList.RestraintList)
                {
                    if (conditionDebug.FailedParameterCount > 0 || // Skip failed restraints.
                        DoesConditionIncludeParameter(conditionDebug, "AlwaysTrue") || //OPM 60250
                        DoesConditionIncludeFailedParameter(conditionDebug, "OCGroup")) //OPM 65071. if the user doesn't belong to ocgroup for which condition applies, do not display the condition to the user.
                    {
                        continue;
                    }

                    var restraint = config.Restraints.Get(conditionDebug.ConditionId);
                    messagesList.Add(restraint.FailureMessage);

                    if (messagesList.Count >= MAX_CONDITIONS_TO_DISPLAY)
                    {
                        return messagesList;
                    }
                }
            }

            return messagesList;
        }

        private List<string> GetUserFriendlyMessagesForPrivileges()
        {
            SystemConfig config = null;
            if (getMessageFromRuleOps.Contains(m_lendingQBExecutingEngineDebug.Operation))
            {
                config = GetRepo();
            }

            List<string> messagesList = new List<string>();

            // 10/5/2010 dd - Gather all the failed conditions.
            List<ExecutingEngineConditionDebug> conditionList = new List<ExecutingEngineConditionDebug>();
            foreach (var opDebugList in m_lendingQBExecutingEngineDebug.ExecutingEngineDebugList)
            {
                if (opDebugList.CanPerform) // 7/27/2012 sk - skip any ops that are allowed.
                {
                    continue;
                }

                foreach (var conditionDebug in opDebugList.ConditionList)
                {
                    if (DoesConditionIncludeParameter(conditionDebug, "AlwaysTrue") || //OPM 60250
                        DoesConditionIncludeFailedParameter(conditionDebug, "OCGroup")) //OPM 65071. if the user doesn't belong to ocgroup for which condition applies, do not display the condition to the user.
                    { continue; }

                    if (config != null)
                    {
                        var condition = config.Conditions.Get(conditionDebug.ConditionId);
                        if (!string.IsNullOrEmpty(condition?.FailureMessage))
                        {
                            return new List<string>() { condition.FailureMessage };
                        }
                    }

                    conditionList.Add(conditionDebug);
                }

                if (fallBackToSystemConfigWhenNoPrivilegeDenialMessage.Contains(m_lendingQBExecutingEngineDebug.Operation))
                {
                    var repository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
                    var activeRelease = repository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid);
                    var systemCondition = activeRelease.Configuration.Conditions.FirstOrDefault(cond =>
                        cond.SystemOperationNames.Contains(m_lendingQBExecutingEngineDebug.Operation.Id));

                    if (!string.IsNullOrEmpty(systemCondition?.FailureMessage))
                    {
                        return new List<string>() { systemCondition.FailureMessage };
                    }
                }
            }

            //First MAX_CONDITIONS_TO_DISPLAY conditions with least failed params.
            conditionList = DeDupConditionsOnFailedParams(conditionList);
            IEnumerable<ExecutingEngineConditionDebug> list = conditionList.OrderBy(condition => condition.FailedParameterList.Count()).Take(MAX_CONDITIONS_TO_DISPLAY);

            foreach (ExecutingEngineConditionDebug condition in list)
            {
                string sMessage = GetMessageForCondition(condition);
                if (!string.IsNullOrEmpty(sMessage)) messagesList.Add(sMessage);
            }

            return messagesList;
        }

        private string GetMessageForCondition(ExecutingEngineConditionDebug condition)
        {            
            StringBuilder sbMessage = new StringBuilder();
            foreach (ExecutingEngineParameterDebug failedParam in condition.FailedParameterList.Take(MAX_PARAMS_TO_DISPLAY))
            {
                //OPM 60529 : Remove IsTemplate param
                if (failedParam.VarName.Equals("IsTemplate", StringComparison.OrdinalIgnoreCase))
                    continue;

                string sName = failedParam.VarName;
                sbMessage.Append(GetParamMessage(failedParam) + " AND ");                
            }
            if (condition.FailedParameterList.Count() > 0 && sbMessage.Length>0)
                sbMessage = sbMessage.Remove(sbMessage.Length - 5, 5);//Trim the trailing " AND "
            sbMessage.Append(Environment.NewLine);
            return sbMessage.ToString();
        }

        private string GetParamMessage(ExecutingEngineParameterDebug paramDebug)
        {
            StringBuilder sbMessage = new StringBuilder();
            ParameterReporting paramReporting = ParameterReporting.RetrieveByBrokerId(_brokerId);
            SecurityParameter.Parameter parameter = paramReporting[paramDebug.VarName];

            if (parameter != null)
            {
                sbMessage.Append(parameter.FriendlyName + " = ");

                if (parameter is SecurityParameter.EnumeratedParmeter)
                {
                        //Get the friendly names for the expected enum values
                        var q = from e in ((SecurityParameter.EnumeratedParmeter)parameter).EnumMapping
                                where paramDebug.ExpectedValueList.Contains(e.RepValue)
                                select e.FriendlyValue;

                        sbMessage.Append(string.Join("/", q.ToArray()));                    
                }
                else
                {
                    IEnumerable<string> values = paramDebug.ExpectedValueList;

                    switch(parameter.Type)
                    { 
                        case SecurityParameter.SecurityParameterType.Boolean:
                            values = values.Select(a => a.TrimWhitespaceAndBOM() == "1" ? "Yes" : "No").ToArray();
                            break;
                        case SecurityParameter.SecurityParameterType.Rate:
                            values = values.Select(a => a + "%").ToArray();
                            break;
                        case SecurityParameter.SecurityParameterType.Money:
                            values = values.Select(a => "$" + a).ToArray();
                            break;
                        default:
                            break;
                    }
                    sbMessage.Append(string.Join(",", values.ToArray()));
                }
            }
            else
            {
                sbMessage.Append(paramDebug.VarName + " = " + string.Join(",", paramDebug.ExpectedValueList.ToArray()));
            }
            return sbMessage.ToString();
        }

        /// <summary>
        /// Remove duplicate conditions based on failed parameters list
        /// </summary>
        /// <param name="conditionList"></param>
        /// <returns></returns>
        private List<ExecutingEngineConditionDebug> DeDupConditionsOnFailedParams(List<ExecutingEngineConditionDebug> conditionList)
        {
            List<ExecutingEngineConditionDebug> exclusionList  =  new List<ExecutingEngineConditionDebug>();

            foreach (ExecutingEngineConditionDebug condition in conditionList)
            {
                if (exclusionList.Contains(condition))
                    continue;

              List<ExecutingEngineConditionDebug> l = (from c in conditionList
                                                        where AreFailedParamsSameForConditions(c, condition) && c != condition
                                                        select c).ToList<ExecutingEngineConditionDebug>();             
                exclusionList.AddRange(l);
            }

            return (from c in conditionList
                    where !exclusionList.Contains(c)
                    select c).ToList<ExecutingEngineConditionDebug>();
        }

        private bool AreFailedParamsSameForConditions(ExecutingEngineConditionDebug c1, ExecutingEngineConditionDebug c2)
        {            
            foreach (ExecutingEngineParameterDebug p in c1.FailedParameterList)
            {
                IEnumerable<ExecutingEngineParameterDebug> matchList = from a in c2.FailedParameterList
                                                                       where a.VarName == p.VarName && AreExpectedValuesSame(a.ExpectedValueList, p.ExpectedValueList)
                                                                       select a;
                
                if (matchList.Count() == 0)
                    return false;
            }
            return true;
        }
        private bool AreExpectedValuesSame(IEnumerable<string> values1, IEnumerable<string> values2)
        {
            foreach (string sVal in values1)
            {
                if (!values2.Contains(sVal))
                    return false;
            }
            foreach (string sVal in values2)
            {
                if (!values1.Contains(sVal))
                    return false;
            }
            return true;
        }

        private bool DoesConditionIncludeParameter(ExecutingEngineConditionDebug condition, string sParamName)
        {
            return DoesConditionIncludeFailedParameter(condition, sParamName) || DoesConditionIncludeSuccessfulParameter(condition, sParamName);           
        }

        private bool DoesConditionIncludeFailedParameter(ExecutingEngineConditionDebug condition, string sParamName)
        {
            foreach (ExecutingEngineParameterDebug param in condition.FailedParameterList)
                if (param.VarName.Equals(sParamName, StringComparison.OrdinalIgnoreCase))
                    return true;

            return false;
        }

        private bool DoesConditionIncludeSuccessfulParameter(ExecutingEngineConditionDebug condition, string sParamName)
        {
            foreach (ExecutingEngineParameterDebug param in condition.SuccessfulParameterList)
                if (param.VarName.Equals(sParamName, StringComparison.OrdinalIgnoreCase))
                    return true;

            return false;
        }
    }
}
