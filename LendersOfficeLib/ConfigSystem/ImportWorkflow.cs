﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigSystem.DataAccess;
using System.Data;
using DataAccess;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;

namespace LendersOffice.ConfigSystem
{
    public class ImportWorkflow
    {
        public class _Item
        {
            public string CustomerCode { get; set; }
            public string BrokerNm { get; set; }
            public Guid BrokerId { get; set; }
        }

        private ImportWorkflow()
        {
            
        }

        public static IEnumerable<_Item> Load(string brokerNmFilter, string customerCodeFilter, bool isDraft)
        {
            List<_Item> list = new List<_Item>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {

                List<SqlParameter> parameters = new List<SqlParameter>();

                if (brokerNmFilter.Length > 0)
                {
                    parameters.Add(new SqlParameter("@BrokerName", brokerNmFilter));
                }
                if (customerCodeFilter.Length > 0)
                {
                    parameters.Add(new SqlParameter("@CustomerCode", customerCodeFilter));
                }

                parameters.Add(new SqlParameter("@IsDraft", isDraft));

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "CONFIG_LO_GenerateImportListing", parameters))
                {
                    while (reader.Read())
                    {
                        _Item item = new _Item();
                        item.CustomerCode = (string)reader["CustomerCode"];
                        item.BrokerNm = (string) reader["BrokerNm"];
                        item.BrokerId = (Guid)reader["BrokerId"];

                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public static void Import(Guid fromBrokerId, Guid toBrokerId)
        {
            try
            {
                IConfigRepository fromRepo = ConfigHandler.GetRepository(fromBrokerId);
                BasicConfigData data = fromRepo.LoadActiveRelease(fromBrokerId);

                IConfigRepository toRepo = ConfigHandler.GetRepository(toBrokerId);
                toRepo.SaveDraftConfig(toBrokerId, data.Configuration);
            }
            catch (ConfigValidationException e)
            {
                throw new CBaseException(e.ToString(), e);
            }
            catch (Exception e)
            {
                throw new CBaseException("Unable to import workflow configuration from brokerId: " + fromBrokerId + " into brokerId: " + toBrokerId, e);
            }
        }
    }
}
