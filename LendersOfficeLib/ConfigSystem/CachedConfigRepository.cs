﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;

    using CommonProjectLib.Caching;
    using global::ConfigSystem.DataAccess;

    public class CachedConfigRepository : IConfigRepository
    {
        private const string CacheKeyPrefix = "WORKFLOW_CONFIG_CACHE";

        private IConfigRepository m_repository;

        public CachedConfigRepository(IConfigRepository repository)
        {
            m_repository = repository;
        }

        #region IConfigRepository Members

        public BasicConfigData LoadDraft(Guid organizationId)
        {
            return m_repository.LoadDraft(organizationId);
        }

        public ReleaseConfigData LoadActiveRelease(Guid organizationId)
        {
            string key = $"{CacheKeyPrefix}_{organizationId}";

            return MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                key,
                () => m_repository.GetConfigVersion(organizationId),
                () => m_repository.LoadActiveRelease(organizationId),
                TimeSpan.FromMinutes(5));
        }

        public global::ConfigSystem.FilterSet LoadFilters(Guid organizationId)
        {
            return m_repository.LoadFilters(organizationId);
        }

        public DateTime? GetLastModifiedDateForActiveRelease(Guid organizationId)
        {
            return m_repository.GetLastModifiedDateForActiveRelease(organizationId);
        }

        public long SaveReleaseConfig(Guid organizationId, global::ConfigSystem.SystemConfig config, Guid userId)
        {
            return m_repository.SaveReleaseConfig(organizationId, config, userId);
        }

        public void SaveDraftConfig(Guid organizationId, global::ConfigSystem.SystemConfig config)
        {
            m_repository.SaveDraftConfig(organizationId, config);
        }

        public void SaveFilters(Guid organizationId, global::ConfigSystem.FilterSet filters)
        {
            m_repository.SaveFilters(organizationId, filters);
        }

        public IEnumerable<ReleaseConfigData> GetActiveReleases()
        {
            return m_repository.GetActiveReleases();
        }

        public IEnumerable<BasicConfigData> GetDrafts()
        {
            return m_repository.GetDrafts();
        }

        public IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid organizationId)
        {
            return m_repository.GetAllReleasesFor(organizationId);
        }

        public long GetConfigVersion(Guid organizationId)
        {
            return m_repository.GetConfigVersion(organizationId);
        }

        public long? GetActiveReleaseId(Guid brokerId)
        {
            return m_repository.GetActiveReleaseId(brokerId);
        }
        #endregion
    }
    

}
