﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    using global::ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes.PathDispatch;
    using Security;

    public sealed class WorkflowSystemConfigModel
    {
        private Guid m_brokerId;
        private SystemConfig m_configSystem;
        private bool m_isGlobal; // Is this a global system config?
        private bool m_isDraft;
        private ParameterReporting m_parameter;
        private bool m_usingCache = false;

        /// <summary>
        /// The unique key for the cache.
        /// </summary>
        private string CacheKey
        {
            get
            {
                return "CONFIG_" + CacheId;
            }
        }

        /* opm 236979

            "Field Protection Rules" export structure
                    Id
                    (Don't have Operations column)
                    Protected Field
                    Save Denial Message
                    Notes
                    Rule Parameters

            "Export Triggers" export structure
                    Id
                    Trigger Name
                    Notes
                    Parameters
        */

        public enum TableType
        {
            Constraints=0,
            Conditions = 1,
            FieldProtectionRules = 2,
            ConditionsExport = 3,
            FieldProtectionRulesExport = 4,
            Restraints = 5,
        };

        /// <summary>
        /// The unique key needed to load the cache.
        /// </summary>
        public Guid CacheId { get; private set; }
        
        public WorkflowSystemConfigModel(Guid brokerId, bool loadDraft)
        {
            m_isGlobal = brokerId == ConstAppDavid.SystemBrokerGuid;
            Init(brokerId, loadDraft);
        }
        public WorkflowSystemConfigModel(Guid brokerId, bool loadDraft, Guid cacheId)
        {
            m_isGlobal = brokerId == ConstAppDavid.SystemBrokerGuid;
            CacheId = cacheId;
            m_usingCache = true;
            Init(brokerId, loadDraft);
        }
        public WorkflowSystemConfigModel(Guid brokerId, string sXml, bool isDraft)
        {
            m_isGlobal = brokerId == ConstAppDavid.SystemBrokerGuid;
            m_brokerId = brokerId;
            m_isDraft = isDraft;
            m_configSystem = new SystemConfig(sXml);
            m_parameter = ParameterReporting.RetrieveByBrokerId(brokerId);
            m_parameter.UpdateCustomList(GenerateCustomVariableList().ToArray());
        }

        public bool CanSave
        {
            get
            {
                if (m_isGlobal)
                {
                    return Constants.ConstAppDavid.CurrentServerLocation == LendersOffice.Constants.ServerLocation.LocalHost;
                }

                return true;
            }
        }
        private void Init(Guid brokerId, bool loadDraft)
        {

            m_brokerId = brokerId;
            m_isDraft = loadDraft;

            if (m_usingCache)
            {
                string data = AutoExpiredTextCache.GetFromCache(CacheKey);
                if (data == null)
                {
                    throw new CBaseException(ErrorMessages.FailedToLoad, "Cache has probably expired");
                }

                XDocument doc = XDocument.Parse(data);
                m_configSystem = new SystemConfig(doc.Element("Config").Value);
            }
            else
            {
                IConfigRepository configRepository = ConfigHandler.GetUncachedRepository(brokerId);

                BasicConfigData data = loadDraft ? configRepository.LoadDraft(brokerId) : configRepository.LoadActiveRelease(brokerId);
                m_configSystem = data.Configuration;
            }

            m_parameter = ParameterReporting.RetrieveByBrokerId(brokerId);
            m_parameter.UpdateCustomList(GenerateCustomVariableList().ToArray());
        }
        public ParameterReporting ParameterREporting
        {
            get { return m_parameter; }
        }
        private IEnumerable<CustomVariable> GenerateCustomVariableList()
        {
            var customVariables = new HashSet<CustomVariable>();

            if (!m_isGlobal) // Add global custom variables
            {
                var globalConfigRepository = ConfigHandler.GetUncachedRepository(ConstAppDavid.SystemBrokerGuid);
                var data = globalConfigRepository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid);
                var globalConfig = data.Configuration;
                var globalCustomVariables = globalConfig.CustomVariables;
                foreach (var variable in globalCustomVariables)
                {
                    customVariables.Add(variable);
                }
            }

            foreach (CustomVariable variable in m_configSystem.CustomVariables)
            {
                customVariables.Add(variable);
            }

            return customVariables;
        }

        public ConditionSet GetFullConditionSet()
        {
            return m_configSystem.Conditions;
        }

        public ConstraintSet GetFullConstraintSet()
        {
            return m_configSystem.Constraints;
        }

        public RestraintSet GetFullRestraintSet()
        {
            return m_configSystem.Restraints;
        }

        public void AddNew(AbstractConditionGroup cg)
        {
            var condition = cg as Condition;
            if (condition != null)
            {
                m_configSystem.Conditions.AddCondition(condition);
                return;
            }

            var constraint = cg as Constraint;
            if (constraint != null)
            {
                m_configSystem.Constraints.AddConstraint(constraint);
                return;
            }

            var restraint = cg as Restraint;
            if (restraint != null)
            {
                m_configSystem.Restraints.AddRestraint(restraint);
                return;
            }
        }

        public void AddNew(CustomVariable customVariable)
        {
            m_configSystem.CustomVariables.Add(customVariable);
        }

        public void CopyCondition(Guid id)
        {
            m_configSystem.Conditions.Copy(id);
        }

        public void CopyConstraint(Guid id)
        {
            m_configSystem.Constraints.Copy(id);
        }

        public void CopyRestraint(Guid id)
        {
            m_configSystem.Restraints.Copy(id);
        }

        public void CopyCustomVariable(Guid id)
        {
            m_configSystem.CustomVariables.Copy(id);
        }

        public void RemoveCondition(Guid id)
        {
            m_configSystem.Conditions.Remove(id);
        }

        public void RemoveConstraint(Guid id)
        {
            m_configSystem.Constraints.Remove(id);
        }

        public void RemoveRestraint(Guid id)
        {
            m_configSystem.Restraints.Remove(id);
        }

        public void RemoveCustomVariable(Guid id)
        {
            m_configSystem.CustomVariables.Remove(id);
        }

        public void Edit(Guid id, AbstractConditionGroup cg)
        {
            var condition = cg as Condition;
            if (condition != null)
            {
                m_configSystem.Conditions.Replace(id, condition);
                return;
            }

            var constraint = cg as Constraint;
            if (constraint != null)
            {
                m_configSystem.Constraints.Replace(id, constraint);
                return;
            }

            var restraint = cg as Restraint;
            if (restraint != null)
            {
                m_configSystem.Restraints.Replace(id, restraint);
                return;
            }
        }

        public void Edit(Guid id, CustomVariable customVariable)
        {
            m_configSystem.CustomVariables.Replace(id, customVariable);
        }

        public AbstractConditionGroup GetCondition(Guid id)
        {
            return m_configSystem.Conditions.Get(id);
        }

        public AbstractConditionGroup GetConstraint(Guid id)
        {
            return m_configSystem.Constraints.Get(id);
        }

        public AbstractConditionGroup GetRestraint(Guid id)
        {
            return m_configSystem.Restraints.Get(id);
        }

        public CustomVariable GetCustomVariable(Guid id)
        {
            return m_configSystem.CustomVariables.Get(id);
        }

        public IEnumerable<SecurityParameter.Parameter> GetFields()
        {
            return m_parameter.ParametersSorted;
        }

        private static HashSet<string> excludeFromUi = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "sProdRLckdDays",
            "sSelectedProductCodeFilter",
            "sAvailableProductCodeFilter",
            "sProductCodesByFileType",
            "sApprWaiverReceivedD",
            "sApprECopyConsentReceivedD"
        };

        public IEnumerable<SecurityParameter.Parameter> GetFieldsForUi()
        {
            return m_parameter.ParametersSorted.Where(p => !excludeFromUi.Contains(p.Id));
        }

        public List<String> GetCategories()
        {
            return m_parameter.Categories;
        }

        public SecurityParameter.Parameter GetParameterFromFields(string parameterName)
        {
            foreach (SecurityParameter.Parameter p in GetFields())
            {
                if (string.Equals(p.Id, parameterName, StringComparison.OrdinalIgnoreCase))
                    return p;
            }

            return null;
        }

        public SecurityParameter.Parameter GetParameterWithValues(string parameterName)
        {
            return m_parameter.GetSecurityParameterWithValues(parameterName);
        }

        public void Filter(string searchText)
        {
            m_configSystem.Filter(searchText);
        }

        /// <summary>
        /// Dispose the output stream because this method does not.
        /// </summary>
        /// <param name="conditionGroupType"></param>
        /// <param name="outputStream"></param>
        /// <param name="roleFilter"></param>
        /// <param name="opFilter"></param>
        public void ExportDataTable(TableType tableType, TextWriter outputStream, IEnumerable<string> roleFilter, IEnumerable<WorkflowOperation> opFilter)
        {
            System.Data.DataTable permissionGroupRep = GenerateDataTable(tableType, roleFilter, opFilter);
            outputStream.Write(permissionGroupRep.ToCSV());
        }

        /// <summary>
        /// Exports the Read/Write access information into a temporary Excel file.
        /// </summary>
        /// <param name="filePath">The file name where the Excel file will be saved.</param>
        /// <param name="brokerDB">The information of the lender being exported; or null if it should be loaded from this instance.</param>
        public void ExportReadWriteAccessToExcel(string filePath, LendersOffice.Admin.BrokerDB brokerDB = null)
        {
            if (brokerDB == null)
            {
                brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(this.m_brokerId);
            }
            else if (brokerDB.BrokerID != this.m_brokerId)
            {
                throw new GenericUserErrorMessageException("brokerID of BrokerDB did not match this instance's internal brokerId");
            }

            ExportReadWriteAccess.ExportToExcel(filePath, brokerDB, this.m_configSystem);
        }

        public void ExportDraftXml(TextWriter outputStream)
        {
            XDocument doc = XDocument.Parse(m_configSystem.ToString());
            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(outputStream, settings))
            {
                doc.WriteTo(writer);
            }
        }

        public void ImportIntoDraft(Stream fs)
        {
            using (XmlReader reader = XmlReader.Create(fs))
            {
                XDocument doc = XDocument.Load(reader);
                m_configSystem = new SystemConfig(doc.ToString());
                Save();

                //Thien 4/10/2018: fixed bug for GetFriendlyName(...)
                m_parameter = ParameterReporting.RetrieveByBrokerId(m_brokerId);
                m_parameter.UpdateCustomList(GenerateCustomVariableList().ToArray());
            }
        }

        /// <summary>
        /// Generates a data table containing the conditions/constraints filtered by role/operation
        /// </summary>
        /// <param name="tableType">The requested output table type</param>
        /// <param name="interestingRoles">An IEnumerable containing the list of roles to be considered</param>
        /// <param name="interestingOps">An IEnumerable containing the list of operations to be considered</param>
        /// <returns>Data table</returns>
        public System.Data.DataTable GenerateDataTable(TableType tableType, IEnumerable<string> interestingRoles, IEnumerable<WorkflowOperation> interestingOps)
        {
            var interestingOpIds = interestingOps.Select(o => o.Id);

            Func<AbstractConditionGroup, bool> filter = delegate (AbstractConditionGroup c)
            {
                bool satisfiedConditions = true;
                Parameter conditionRoles = c.ParameterSet.GetParameter("Role");
                if (conditionRoles != null)
                {
                    satisfiedConditions = conditionRoles.Values.Any(p => interestingRoles.Contains(p, StringComparer.CurrentCultureIgnoreCase))
                        || conditionRoles.Exceptions.Any(p => interestingRoles.Contains(p, StringComparer.CurrentCultureIgnoreCase));
                }
                else
                {
                    satisfiedConditions = interestingRoles.Count() > 0;
                }
                satisfiedConditions &= c.SysOpSet.SystemOperationNames.Any(p => interestingOpIds.Contains(p, StringComparer.CurrentCultureIgnoreCase));
                return satisfiedConditions;
            };

            AbstractSecurityPermissionSet set = null;
            switch (tableType)
            {
                case TableType.Conditions:
                case TableType.ConditionsExport:
                case TableType.FieldProtectionRules:
                case TableType.FieldProtectionRulesExport:
                    set = m_configSystem.Conditions;
                    break;
                case TableType.Constraints:
                    set = m_configSystem.Constraints;
                    break;
                case TableType.Restraints:
                    set = m_configSystem.Restraints;
                    break;
                default:
                    throw new UnhandledEnumException(tableType);
            }

            IList<AbstractConditionGroup> conditionGroup = set.Where(p => filter(p)).ToList();
            return GenerateDataTable(conditionGroup, tableType);
        }

        public System.Data.DataTable GenerateDataTable(IEnumerable<AbstractConditionGroup> conditionGroup, TableType tableType)
        {
            bool showOperation = (tableType != TableType.FieldProtectionRulesExport);
            bool showProtectedField = (tableType == TableType.FieldProtectionRulesExport);
            bool showWriteAndChangeLoanStatusFieldNames = (tableType == TableType.ConditionsExport);

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Id", typeof(Guid));

            //Operations is a not store as a parameter
            if (showOperation)
            {
                dt.Columns.Add("Operations", typeof(string));
            }

            if (tableType == TableType.Conditions)
            {
                dt.Columns.Add("Exclude", typeof(string));
            }

            if (showWriteAndChangeLoanStatusFieldNames)
            {
                dt.Columns.Add("Affected Fields", typeof(string));
            }

            if (showProtectedField)
            {
                dt.Columns.Add("Protected Fields", typeof(string));
                dt.Columns.Add("Save Denial Message", typeof(string));
            }

            bool forConditions = tableType != TableType.Constraints && tableType != TableType.Restraints;
            var displayFieldsWithOperationNames = tableType.EqualsOneOf(TableType.Conditions, TableType.Restraints, TableType.FieldProtectionRules);

            if (forConditions)
            {
                dt.Columns.Add("Evaluate All?", typeof(string));
            }

            dt.Columns.Add("Notes", typeof(string));

            IEnumerable<string> parameters = GetUsedParameters(conditionGroup);

            foreach (string parameterName in parameters)
            {
                dt.Columns.Add(GetFriendlyName(parameterName), typeof(string));
            }

            foreach (AbstractConditionGroup group in conditionGroup)
            {
                System.Data.DataRow row = dt.NewRow();

                row["Id"] = group.Id;

                if (showOperation)
                {
                    if (displayFieldsWithOperationNames)
                    {
                        row["Operations"] = string.Join(", ", GetFriendlyOperationNamesWithFields(group));
                    }
                    else
                    {
                        row["Operations"] = string.Join(", ", GetFriendlyOperationNames(group.SystemOperationNames));
                    }
                }

                if (tableType == TableType.Conditions)
                {
                    row["Exclude"] = group.ExcludeFromRWExport ? "X" : string.Empty;
                }

                if (showWriteAndChangeLoanStatusFieldNames)
                {
                    var writeFieldOp = group.SysOpSet.Get(WorkflowOperations.WriteField);
                    var changeLoanStatusOp = group.SysOpSet.Get(WorkflowOperations.LoanStatusChange);

                    var writeFieldNames = writeFieldOp?.Fields?.FieldNames ?? Enumerable.Empty<string>();
                    var changeLoanStatusNames = changeLoanStatusOp?.Fields?.FieldNames ?? Enumerable.Empty<string>();

                    row["Affected Fields"] = string.Join(",", writeFieldNames.Union(changeLoanStatusNames));
                }

                if (showProtectedField)
                {
                    var protectFieldOp = group.SysOpSet.Get(WorkflowOperations.ProtectField);
                    row["Protected Fields"] = (protectFieldOp != null && protectFieldOp.Fields != null)  ? String.Join(",", protectFieldOp.Fields.FieldNames) : string.Empty;
                    row["Save Denial Message"] =group.FailureMessage??string.Empty;
                }

                if (forConditions)
                {
                    row["Evaluate All?"] = group.SystemOperationNames.Any(opname => WorkflowOperations.AreAllConditionsEvaluatedForOperation(opname)) ? "X" : string.Empty;
                }

                row["Notes"] = group.Notes;
                foreach (Parameter parameter in group.ParameterSet)
                {
                    SecurityParameter.Parameter p = m_parameter.GetSecurityParameterWithValues(parameter.Name);

                    row[GetFriendlyName(parameter.Name)] = GetOperandRep(parameter);
                }
                dt.Rows.Add(row);
            }
            return dt;

        }

        private System.Data.DataTable GenerateDataTable(CustomVariable variable)
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            dt.Columns.Add("Id", typeof(Guid));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Notes", typeof(string));

            foreach (Parameter parameter in variable.Parameters)
            {
                dt.Columns.Add(parameter.Name, typeof(string));
            }

            System.Data.DataRow row = dt.NewRow();
            row["Notes"] = variable.Notes;
            row["Id"] = variable.Id;
            row["Name"] = variable.Name;

            foreach (Parameter parameter in variable.Parameters)
            {
                SecurityParameter.Parameter p = m_parameter.GetSecurityParameterWithValues(parameter.Name);
                string data = GetOperandRep(parameter);

                if (p == null)
                {
                    data = String.Concat("ERROR - NOT FOUND ", data);
                }

                row[parameter.Name] = data;
            }

            dt.Rows.Add(row);

            return dt;
        }

        public IEnumerable<System.Data.DataTable> GetCustomVariableDataTables()
        {
            foreach (CustomVariable variable in m_configSystem.CustomVariables.OrderBy(p => p.Name))
            {
                yield return GenerateDataTable(variable);
            }
        }

        public IEnumerable<CustomVariable> GetCustomVariables()
        {
            foreach (CustomVariable variable in m_configSystem.CustomVariables.OrderBy(p => p.Name))
            {
                yield return variable;
            }
        }

        public string GetFriendlyName(string fieldName)
        {
            if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                return GetFriendlyNameFromPath(fieldName);
            }
            else
            {
                return GetFriendlyNameFromSecurityParameters(fieldName);
            }
        }

        private string GetFriendlyNameFromSecurityParameters(string fieldName)
        {
            SecurityParameter.Parameter p = m_parameter[fieldName];
            if (p == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Invalid {nameof(fieldName)}: '{fieldName}'");
            }

            return p.FriendlyName;
        }

        private string GetFriendlyNameFromPath(string fieldName)
        {
            DataPath path = DataPath.Create(fieldName);
            StringBuilder friendlyNameBuilder = new StringBuilder();

            // Set initial parameter.
            SecurityParameter.Parameter currentParameter = m_parameter[path.Head.Name];
            if (currentParameter == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
            }

            // Set root of friendly name path.
            // Note: root parameter friendly name should already include the path header.
            friendlyNameBuilder.Append(currentParameter.FriendlyName);

            foreach (IDataPathElement element in path.Tail.PathList)
            {
                if (element is DataPathBasicElement)
                {
                    friendlyNameBuilder.Append(".");

                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                    }

                    friendlyNameBuilder.Append(currentParameter.FriendlyName);
                }
                else if (element is DataPathCollectionElement)
                {
                    friendlyNameBuilder.Append(".");

                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                    }

                    // Only Collection parameters correspond to Collection data path nodes. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                    }

                    friendlyNameBuilder.Append(collectionParameter.FriendlyName);
                }
                else if (element is DataPathSelectionElement)
                {
                    // Only Collection parameters can be indexed by a selection parameter. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                    }
                        
                    if (string.Equals(element.Name, DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase))
                    {
                        friendlyNameBuilder.Append(element.ToString());
                    }
                    else if (element.Name.StartsWith(DataPath.SubsetMarker, StringComparison.OrdinalIgnoreCase))
                    {
                        // Verify subset name.
                        if (!collectionParameter.Subsets.ContainsKey(element.Name.Substring(DataPath.SubsetMarker.Length)))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                        }

                        friendlyNameBuilder.Append(element.ToString());
                    }
                    else
                    {
                        // Verify record ID.
                        PathableRecordTypeInfo recordInfo;
                        if (!collectionParameter.Records.TryGetValue(element.Name, out recordInfo))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{fieldName}]");
                        }

                        friendlyNameBuilder.Append("[");
                        friendlyNameBuilder.Append(recordInfo.FriendlyDescription);
                        friendlyNameBuilder.Append("]");
                    }
                }
            }

            return friendlyNameBuilder.ToString();
        }

        public string ExportXml()
        {
            return m_configSystem.ToString();
        }

        public bool ValidateBeforeSave(out string errMsg)
        {
            SystemConfigValidator v = new SystemConfigValidator(m_brokerId, m_configSystem);
            if (!v.IsValid())
            {
                var exc = new ConfigValidationException(v);
                errMsg = exc.ToString();
                return false;
            }

            try
            {
                m_configSystem.ToString();
            }
            catch(XmlException exc)
            {
                errMsg = exc.Message;
                return false;
            }

            errMsg = string.Empty;
            return true;
        }

        public Guid SaveToCache()
        {
            SystemConfigValidator v = new SystemConfigValidator(m_brokerId, m_configSystem);
            if (!v.IsValid())
            {
                throw new ConfigValidationException(v);
            }

            XDocument doc = new XDocument(new XElement("Config", new XCData(m_configSystem.ToString())));
            if (!m_usingCache)
            {
                CacheId = Guid.NewGuid();
                m_usingCache = true;
                AutoExpiredTextCache.AddToCache(doc.ToString(), TimeSpan.FromHours(1), CacheKey);
                return CacheId;
            }
            else
            {
                AutoExpiredTextCache.UpdateCache(doc.ToString(), CacheKey);
                return CacheId;
            }
        }

        public void Save(bool forceSaveOnValidationError = false)
        {
            if (m_isDraft == false )
            {
                throw new CBaseException(ErrorMessages.Generic, "cannot save");
            }

            try
            {
                IConfigRepository configRepository = ConfigHandler.GetRepository(m_brokerId);
                configRepository.SaveDraftConfig(m_brokerId, m_configSystem);
            }
            catch(ConfigValidationException)
            {
                if (forceSaveOnValidationError)
                {
                    IConfigRepository configRepository = ConfigHandler.GetUncachedUnvalidatedRepository(m_brokerId);
                    configRepository.SaveDraftConfig(m_brokerId, m_configSystem);
                }

                throw;
            }
            
            //if (m_isGlobal)
            //{
            //    configRepository.SaveReleaseConfig(m_brokerId, m_configSystem);
            //}
        }

        public void SwitchToDraft()
        {
            m_isDraft = true;
        }

        public static void Publish(Guid brokerId, AbstractUserPrincipal principal)
        {
            Guid userId = principal.UserId;

            IConfigRepository configRepository = ConfigHandler.GetAuditRepository(brokerId, principal, WorkflowAuditType.ReleaseConfig, "Workflow rules released");

            BasicConfigData data = configRepository.LoadDraft(brokerId);

            if (data.Configuration.CheckForConflictsByConditions().Count > 0)
            {
                throw new CBaseException("There are constraint failures.", $"Constraint Check failed. BrokerId=[{brokerId}].");
            }

            if (data.Configuration.Conditions.Count() == 0 && data.Configuration.Restraints.Count() == 0)
            {
                throw new CBaseException("Trying to save empty configuration.", $"Empty Config. BrokerId=[{brokerId}].");
            }

            configRepository.SaveReleaseConfig(brokerId, data.Configuration, userId);
            configRepository.SaveDraftConfig(brokerId, new SystemConfig());
        }

        /// <summary>
        /// These custom variables/triggers were added by section 5 on the task automation spec
        /// </summary>
        /// <returns></returns>
        public static List<CustomVariable> GetHardcodedCustomVariables()
        {
            var hardcodedCustomVariables = new List<CustomVariable>();
            hardcodedCustomVariables.Add(new CustomVariable("Liability_To_Be_paid", CustomVariable.E_Scope.System));
            foreach (string assetTypeName in Enum.GetNames(typeof(E_AssetT)))
            {
                hardcodedCustomVariables.Add(new CustomVariable(string.Format("Asset_{0}_Added", assetTypeName), CustomVariable.E_Scope.System));
            }
            hardcodedCustomVariables.Add(new CustomVariable("REO_Incomplete", CustomVariable.E_Scope.System));

            return hardcodedCustomVariables;
        }

        public static List<CustomVariable> GetCustomVariablesForTaskTriggerTemplate(Guid brokerId)
        {
            var customVariables = new List<CustomVariable>();

            // Add global config triggers
            WorkflowSystemConfigModel globalConfig = new WorkflowSystemConfigModel(ConstAppDavid.SystemBrokerGuid, false);
            customVariables.AddRange(globalConfig.GetCustomVariables());

            // Add lender config triggers
            WorkflowSystemConfigModel lenderConfig = new WorkflowSystemConfigModel(brokerId, false);
            customVariables.AddRange(lenderConfig.GetCustomVariables());

            // Add hardcoded triggers
            customVariables.AddRange(WorkflowSystemConfigModel.GetHardcodedCustomVariables());

            // TODO: Sort

            return customVariables;
        }

        /// <summary>
        /// Goes through all the conditions recording all the possible parameters.
        /// </summary>
        /// <returns>A collection of parameters</returns>
        private ICollection<String> GetNewConditionParameters(AbstractSecurityPermissionSet permissionSet )
        {
            HashSet<string> parameters = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            parameters.Add("Role");    //add role first since it has to go first
            foreach (AbstractConditionGroup condition in permissionSet)
            {
                foreach (Parameter p in condition.ParameterSet)
                {
                    parameters.Add(p.Name);
                }
            }

            return parameters;
        }

        /// <summary>
        /// Goes through all the conditions recording all the possible parameters.
        /// </summary>
        /// <returns>A collection of parameters</returns>
        public IEnumerable<String> GetUsedParameters(IEnumerable<AbstractConditionGroup> conditions)
        {
            HashSet<string> parameters = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            parameters.Add("Role");    //add role first since it has to go first
            foreach (AbstractConditionGroup condition in conditions)
            {
                foreach (Parameter p in condition.ParameterSet)
                {
                    parameters.Add(p.Name);
                }
            }

            return parameters;
        }
        /// <summary>
        /// Create a set of all parameters that appear in all condition/constraints with the given interesting operations.
        /// </summary>
        /// <param name="tableType">The requested output table type</param>
        /// <param name="interestingOperations">Operations that we need to include in the search for parameters</param>
        /// <returns></returns>
        public IEnumerable<String> GetNewConditionParameters(TableType tableType, Guid id, params WorkflowOperation[] interestingOperations)
        {
            HashSet<string> parameters = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            parameters.Add("Role");    //add role first since it has to go first
            foreach (AbstractConditionGroup cg in m_configSystem.Conditions.Concat(m_configSystem.Constraints))
            {
                bool includeCondition = false;
                foreach (string op in cg.SystemOperationNames)
                {
                    if (string.IsNullOrEmpty(op) || !WorkflowOperations.Exists(op))
                    {
                        continue;
                    }

                    if (interestingOperations.Contains(WorkflowOperations.Get(op)))
                    {
                        includeCondition = true;
                        break;
                    }
                }
                if (false == includeCondition)
                {
                    continue;
                }
                foreach (Parameter p in cg.ParameterSet)
                {
                    parameters.Add(p.Name);
                }
            }

            if (id == Guid.Empty) // the AbstractConditionGroup id
            {
                return parameters;
            }

            ParameterSet paramSet;
            switch (tableType)
            {
                case TableType.Conditions:
                case TableType.ConditionsExport:
                case TableType.FieldProtectionRules:
                case TableType.FieldProtectionRulesExport:
                    paramSet = m_configSystem.Conditions.Get(id).ParameterSet; ;
                    break;
                case TableType.Constraints:
                    paramSet = m_configSystem.Constraints.Get(id).ParameterSet; ;
                    break;
                case TableType.Restraints:
                    paramSet = m_configSystem.Restraints.Get(id).ParameterSet; ;
                    break;
                default:
                    throw new UnhandledEnumException(tableType);
            }

            foreach (Parameter p in paramSet)
            {
                parameters.Add(p.Name);
            }

            return parameters;
        }

        public IEnumerable<String> GetNewCustomVariableParameters(Guid id)
        {
            HashSet<string> parameters = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (Parameter p in m_configSystem.CustomVariables.Get(id).ParameterSet)
            {
                parameters.Add(p.Name);
            }
            return parameters;
        }

        private string[] GetFriendlyValuesFromParameter(Parameter parameter)
        {
            SecurityParameter.EnumeratedParmeter enumSystemParameter =  m_parameter[parameter.Name] as SecurityParameter.EnumeratedParmeter;
            if( enumSystemParameter == null ) 
            {
                return parameter.Values.ToArray();
            }

            List<string> friendlyValues = new List<string>(parameter.Values.Count());

            foreach (String configValue in parameter.Values  )
            {
                string friendlyValue = configValue; // Default friendly value to parameter value.
                string desc = parameter.ValueDesc[configValue];
                SecurityParameter.EnumMapping mapping = enumSystemParameter.EnumMapping.Where(s => s.RepValue == configValue).FirstOrDefault();
                if (mapping != null)
                {
                    friendlyValue = mapping.FriendlyValue;
                }
                else if (!string.IsNullOrWhiteSpace(desc))
                {
                    friendlyValue = desc;
                }

                friendlyValues.Add(friendlyValue);
            }

            return friendlyValues.ToArray();
        }

        private string[] GetFriendlyExceptionsFromParameter(Parameter parameter)
        {
            SecurityParameter.EnumeratedParmeter enumSystemParameter = m_parameter[parameter.Name] as SecurityParameter.EnumeratedParmeter;
            if (enumSystemParameter == null)
            {
                return parameter.Exceptions.ToArray();
            }

            List<string> friendlyValues = new List<string>(parameter.Exceptions.Count());

            foreach (string exception in parameter.Exceptions)
            {
                string friendlyValue = exception; // Default friendly value to parameter value.
                string desc = parameter.ExceptionDesc[exception];
                SecurityParameter.EnumMapping mapping = enumSystemParameter.EnumMapping.Where(s => s.RepValue == exception).FirstOrDefault();
                if (mapping != null)
                {
                    friendlyValue = mapping.FriendlyValue;
                }
                else if (!string.IsNullOrWhiteSpace(desc))
                {
                    friendlyValue = desc;
                }

                friendlyValues.Add(friendlyValue);
            }

            return friendlyValues.ToArray();
        }

        /// <summary>
        /// Takes a parameter and looks at its function to generate a friendly representation of its values and the operand.
        /// </summary>
        /// <param name="p">The parameter to represent</param>
        /// <returns>A representation of the operends and oparands</returns>
        public string GetOperandRep(Parameter p)
        {
            string op;
            switch (p.FunctionT)
            {
                case E_FunctionT.In:
                    string[] values = GetFriendlyValuesFromParameter(p);
                    string[] exceptions = GetFriendlyExceptionsFromParameter(p);

                    StringBuilder ret = new StringBuilder();
                    ret.Append(string.Join(", ", values));

                    if (exceptions.Length > 0)
                    {
                        if (ret.Length > 0)
                        {
                            ret.Append(", ");
                        }

                        ret.Append("<b>!(");
                        ret.Append(string.Join(", ", exceptions));
                        ret.Append(")</b>");
                    }

                    return ret.ToString();
                case E_FunctionT.Between:
                    return "BETWEEN " + String.Join(", ", p.Values.ToArray());
                case E_FunctionT.EqualVar:
                case E_FunctionT.Equal:
                    op = "==";
                    break;

                case E_FunctionT.IsGreaterThan:
                case E_FunctionT.IsGreaterThanVar:
                    op = ">";
                    break;
                case E_FunctionT.IsLessThan:
                case E_FunctionT.IsLessThanVar:
                    op = "<";
                    break;


                case E_FunctionT.IsGreaterThanOrEqualToVar:
                case E_FunctionT.IsGreaterThanOrEqualTo:
                    op = ">=";
                    break;
                case E_FunctionT.IsLessThanOrEqualToVar:
                case E_FunctionT.IsLessThanOrEqualTo:
                    op = "<=";
                    break;

                case E_FunctionT.IsAnyValue:
                    op = "Is any value:";
                    break;
                case E_FunctionT.IsNonblankStr:
                    op = "Is nonblank string:";
                    break;
                case E_FunctionT.IsNonzeroValue:
                    op = "Is nonzero value:";
                    break;
                default:
                    throw new UnhandledEnumException(p.FunctionT);
            }

            return string.Format("{0} {1}", op, p.Values.ElementAt(0)) ;
        }

        public static IEnumerable<string> GetFriendlyOperationNames(IEnumerable<string> operationIds)
        {
            List<string> friendlyNames = new List<string>();
            foreach (var id in operationIds)
            {
                if (WorkflowOperations.Exists(id))
                {
                    friendlyNames.Add(WorkflowOperations.Get(id).FriendlyDescription);
                }
            }

            return friendlyNames;
        }

        public static IEnumerable<string> GetFriendlyOperationNamesWithFields(AbstractConditionGroup group)
        {
            var friendlyNames = new List<string>();
            foreach (var id in group.SystemOperationNames)
            {
                if (!WorkflowOperations.Exists(id))
                {
                    continue;
                }

                var workflowOperation = WorkflowOperations.Get(id);
                var operationFriendlyName = workflowOperation.FriendlyDescription;

                if (workflowOperation.EqualsOneOf(WorkflowOperations.WriteField, WorkflowOperations.ProtectField, WorkflowOperations.LoanStatusChange))
                {
                    var systemOperation = group.SysOpSet.Get(workflowOperation.Id);
                    if (systemOperation != null & systemOperation.Fields != null)
                    {
                        if (workflowOperation == WorkflowOperations.LoanStatusChange)
                        {
                            // Display only the status names for this operation, 
                            // e.g. Approved instead of Loan Status (sStatusT) : Approved
                            var statusSeparator = new[] { ':' };
                            var statusNames = systemOperation.Fields.FieldNames.Select(name =>
                            {
                                var parts = name.Split(statusSeparator, StringSplitOptions.RemoveEmptyEntries);
                                return parts.Length == 2 ? parts[1] : string.Empty; // Skip invalid entries
                            });
                            
                            operationFriendlyName += $" ({string.Join(",", statusNames)})";
                        }
                        else
                        {
                            var friendlyFieldNamesWithId = systemOperation.Fields.Values.Select(field =>
                            {
                                if (field.FriendlyName.Contains(field.Id, StringComparison.OrdinalIgnoreCase))
                                {
                                    return field.FriendlyName;
                                }

                                return $"{field.FriendlyName} ({field.Id})";
                            });

                            operationFriendlyName += $" ({string.Join(",", friendlyFieldNamesWithId)})";
                        }
                    }
                }

                friendlyNames.Add(operationFriendlyName);
            }

            return friendlyNames;
        }

        public Dictionary<Guid, List<Guid>> CheckForConflictsByConditions()
        {
            return m_configSystem.CheckForConflictsByConditions();
        }

        public Dictionary<Guid, List<Guid>> CheckForConflictsByConstraints()
        {
            return m_configSystem.CheckForConflictsByConstraints();
        }

        public void RenameParameter(string oldVal, string newVal)
        {
            m_configSystem.RenameParameter(oldVal, newVal);
        }

        public void RenameNonParameter(E_TypeToRename typeToRename, string parameterName, string oldVal, string newVal)
        {
            m_configSystem.RenameNonParameter(typeToRename, parameterName, oldVal, newVal);
        }

        public bool DoesExistNonParameter(E_TypeToRename typeToFind, string parameterName, string valueToFind)
        {
            return m_configSystem.DoesExistNonParameter(typeToFind, parameterName, valueToFind);
        }

        public bool HasDraft()
        {
            SystemConfig draftConfig = ConfigHandler.GetRepository(m_brokerId).LoadDraft(m_brokerId).Configuration;
            return (draftConfig.Conditions.Count() > 0) || (draftConfig.Constraints.Count() > 0) || (draftConfig.CustomVariables.Count() > 0);
        }
    }

    public static class Extensions
    {
        public static string ToCSV(this System.Data.DataTable table)
        {
            var result = new StringBuilder();
            for (int i = 0; i < table.Columns.Count; i++)
            {
                result.Append(table.Columns[i].ColumnName);
                result.Append(i == table.Columns.Count - 1 ? "\n" : ",");
            }

            foreach (System.Data.DataRow row in table.Rows)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    string data = row[i].ToString();
                    data = data.Replace("\"", "\"\"");  //double up quotes inside

                    result.Append("\"" + data + "\""); //always wrap stuff in quotes so we dont have to figure it out
                    result.Append(i == table.Columns.Count - 1 ? "\n" : ",");
                }
            }

            return result.ToString();
        }
    }

}
