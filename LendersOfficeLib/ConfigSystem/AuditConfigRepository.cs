﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using Security;

    /// <summary>
    /// Audit workflow configuration repository class.
    /// </summary>
    public class AuditConfigRepository : IConfigRepository
    {
        /// <summary>
        /// IConfigRepository object.
        /// </summary>
        private IConfigRepository repository;

        /// <summary>
        /// Abstract user principal.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// Workflow audit type.
        /// </summary>
        private WorkflowAuditType auditType;

        /// <summary>
        /// Workflow audit description.
        /// </summary>
        private string auditDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditConfigRepository"/> class.
        /// </summary>
        /// <param name="repository">IConfigRepository object.</param>
        /// <param name="principal">Abstract user principal object.</param>
        /// <param name="auditType">Audit type for the workflow change history audit being created.</param>
        /// <param name="auditDescription">Audit description for the workflow change history audit being created.</param>
        public AuditConfigRepository(IConfigRepository repository, AbstractUserPrincipal principal, WorkflowAuditType auditType, string auditDescription)
        {
            this.repository = repository;
            this.principal = principal;
            this.auditType = auditType;
            this.auditDescription = auditDescription;
        }

        /// <summary>
        /// Get active releases.
        /// </summary>
        /// <returns>All active release configuration data.</returns>
        public IEnumerable<ReleaseConfigData> GetActiveReleases()
        {
            return this.repository.GetActiveReleases();
        }

        /// <summary>
        /// Get all release configuration data belonging to a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Release configuration data belonging to a broker.</returns>
        public IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid organizationId)
        {
            return this.repository.GetAllReleasesFor(organizationId);
        }

        /// <summary>
        /// Get configuration version belonging to a broker..
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Configuration version belonging to a broker.</returns>
        public long GetConfigVersion(Guid organizationId)
        {
            return this.repository.GetConfigVersion(organizationId);
        }

        /// <summary>
        /// Get draft configuration data.
        /// </summary>
        /// <returns>Draft configuration data.</returns>
        public IEnumerable<BasicConfigData> GetDrafts()
        {
            return this.repository.GetDrafts();
        }

        /// <summary>
        /// Gets the last modified date for active release belonging to a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Last modified date for active release belonging to a broker.</returns>
        public DateTime? GetLastModifiedDateForActiveRelease(Guid organizationId)
        {
            return this.repository.GetLastModifiedDateForActiveRelease(organizationId);
        }

        /// <summary>
        /// Loads active release belonging to a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Release configuration data that is active for a broker.</returns>
        public ReleaseConfigData LoadActiveRelease(Guid organizationId)
        {
            return this.repository.LoadActiveRelease(organizationId);
        }

        /// <summary>
        /// Loads draft belonging to a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Draft configuration data belonging to a broker.</returns>
        public BasicConfigData LoadDraft(Guid organizationId)
        {
            return this.repository.LoadDraft(organizationId);
        }

        /// <summary>
        /// Loads filters belonging to a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <returns>Filters belonging to a broker.</returns>
        public global::ConfigSystem.FilterSet LoadFilters(Guid organizationId)
        {
            return this.repository.LoadFilters(organizationId);
        }

        /// <summary>
        /// Saves draft configuration for a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <param name="config">Configuration to save.</param>
        public void SaveDraftConfig(Guid organizationId, global::ConfigSystem.SystemConfig config)
        {
            this.repository.SaveDraftConfig(organizationId, config);
        }

        /// <summary>
        /// Saves filters for a broker.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <param name="filters">Filters to save.</param>
        public void SaveFilters(Guid organizationId, global::ConfigSystem.FilterSet filters)
        {
            this.repository.SaveFilters(organizationId, filters);
        }

        /// <summary>
        /// Save release configuration id for a broker. Also creates an workflow change history audit.
        /// </summary>
        /// <param name="organizationId">Broker id.</param>
        /// <param name="config">Configuration to save.</param>
        /// <param name="userId">User id that is initiating save.</param>
        /// <returns>Configuration id that got saved.</returns>
        public long SaveReleaseConfig(Guid organizationId, global::ConfigSystem.SystemConfig config, Guid userId)
        {
            long configId = this.repository.SaveReleaseConfig(organizationId, config, userId);

            WorkflowChangeAudit audit = new WorkflowChangeAudit(organizationId, this.principal, this.auditType, this.auditDescription, configId);
            audit.Save();

            return configId;
        }

        /// <summary>
        /// Get active release configuration id for given broker.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <returns>Active release configuration id.</returns>
        public long? GetActiveReleaseId(Guid brokerId)
        {
            return this.repository.GetActiveReleaseId(brokerId);
        }
    }
}
