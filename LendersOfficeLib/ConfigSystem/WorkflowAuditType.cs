﻿namespace LendersOffice.ConfigSystem
{
    /// <summary>
    /// Workflow Change Event enum.
    /// For now, only track release configuration change, but in the future, we can potentially track addition/deletion/edit of existing rules and custom variables.
    /// </summary>
    public enum WorkflowAuditType
    {
        /// <summary>
        /// Workflow configuration release event.
        /// </summary>
        ReleaseConfig = 0,

        /// <summary>
        /// Renaming branch/employee/OC group event.
        /// </summary>
        RenameGroup = 1,

        /// <summary>
        /// Revert to a previous release configuration.
        /// </summary>
        Revert = 2,        

        /// <summary>
        /// Creating a new branch/employee/OC group.
        /// </summary>
        CreateGroup = 3,

        /// <summary>
        /// Delete an existing branch/employee OC group.
        /// </summary>
        DeleteGroup = 4,
    }
}
