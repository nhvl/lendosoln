﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;
using LendersOffice.Common;

namespace LendersOffice.ConfigSystem
{
    public class ConfigValidationException : CBaseException
    {
        private SystemConfigValidator m_validator;
       
        public IEnumerable<String> ConditionErrors
        {
            get
            {
                return m_validator.ConditionErrors;
            }
        }

        public IEnumerable<String> ConstraintErrors
        {
            get
            {
                return m_validator.ConstraintErrors;
            }
        }

        public IEnumerable<String> CustomVariableErrors
        {
            get
            {
                return m_validator.CustomVariableErrors;
            }
        }

       
        public ConfigValidationException(SystemConfigValidator validator)
            : base( ErrorMessages.ConfigSystem.ValidationErrors, "SystemConfig Validation Errors")
        {
            m_validator = validator;
        }

        public override string ToString()
        {
            StringBuilder fail = new StringBuilder();
            foreach (string a in m_validator.ConditionErrors)
            {
                fail.AppendLine(a);
            }
            fail.AppendLine();
            foreach (string a in m_validator.ConstraintErrors)
            {
                fail.AppendLine(a);
            }
            fail.AppendLine();
            foreach (string a in m_validator.CustomVariableErrors)
            {
                fail.AppendLine(a);
            }
            fail.AppendLine();
            return fail.ToString() + base.ToString();
        }
    }
}
