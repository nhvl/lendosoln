﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a default workflow operation on a loan, requiring loan read access
    /// in addition to the operation itself.
    /// </summary>
    public class LoanWorkflowOperation : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        public LoanWorkflowOperation(string id, string description)
            : base(id, description)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        /// <param name="evaluateAllConditionsForOperation">True if all conditions with this type should evaluate, false if the first successful evaluation ends evaluation execution.</param>
        public LoanWorkflowOperation(string id, string description, bool evaluateAllConditionsForOperation)
            : base(id, description, evaluateAllConditionsForOperation)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        /// <param name="evaluateAllConditionsForOperation">True if all conditions with this type should evaluate, false if the first successful evaluation ends evaluation execution.</param>
        /// <param name="isRestrainable">Indicates whether this operation can be used to create restraints.</param>
        public LoanWorkflowOperation(string id, string description, bool evaluateAllConditionsForOperation, bool isRestrainable)
            : base(id, description, evaluateAllConditionsForOperation, isRestrainable)
        {
        }

        /// <summary>
        /// Gets the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.ReadLoan,
            };
        }
    }
}
