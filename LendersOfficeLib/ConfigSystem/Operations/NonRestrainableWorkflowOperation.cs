﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a non-restrainable workflow operation.
    /// Operations of this type cannot be used to create restraints.
    /// </summary>
    public class NonRestrainableWorkflowOperation : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NonRestrainableWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        public NonRestrainableWorkflowOperation(string id)
            : base(id, id, evaluateAllConditionsForOperation: false, isRestrainable: false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonRestrainableWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        public NonRestrainableWorkflowOperation(string id, string description)
            : base(id, description, evaluateAllConditionsForOperation: false, isRestrainable: false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonRestrainableWorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        /// <param name="evaluateAllConditionsForOperation">True if all conditions with this type should evaluate, false if the first successful evaluation ends evaluation execution.</param>
        public NonRestrainableWorkflowOperation(string id, string description, bool evaluateAllConditionsForOperation)
            : base(id, description, evaluateAllConditionsForOperation, isRestrainable: false)
        {
        }
    }
}
