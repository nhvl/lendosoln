﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;

    /// <summary>
    /// Workflow operation for manually overriding QM settings.
    /// </summary>
    public class ManuallyOverrideQMSettings : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManuallyOverrideQMSettings"/> class.
        /// </summary>
        public ManuallyOverrideQMSettings()
            : base("ManuallyOverrideQMSettings", "Manually override QM third party and affiliate settings")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.WriteLoan,
                WorkflowOperations.ReadLoan
            };
        }
    }
}
