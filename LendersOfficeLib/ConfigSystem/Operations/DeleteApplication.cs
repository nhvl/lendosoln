﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// Workflow operation for deleting applications on a loan file.
    /// </summary>
    public class DeleteApplication : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteApplication"/> class.
        /// </summary>
        public DeleteApplication()
            : base("DeleteApplication", "Delete Application")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.WriteLoanOrTemplate
            };
        }

        /// <summary>
        /// Evaluates the operation against the given result dictionary.
        /// </summary>
        /// <param name="operationResults">A map from operation id to result.</param>
        /// <returns>True if all conditions are satisfied. Otherwise, false.</returns>
        public override bool GetResult(Dictionary<WorkflowOperation, ExecutingEngineResultType> operationResults)
        {
            return operationResults.IsAllow(this) 
                && ((operationResults.IsAllow(WorkflowOperations.WriteLoan) && operationResults.IsAllow(WorkflowOperations.ReadLoan)) 
                    || (operationResults.IsAllow(WorkflowOperations.WriteTemplate) && operationResults.IsAllow(WorkflowOperations.ReadTemplate)));
        }
    }
}
