﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This model is used as a model for a single operation.  To be used for WorkflowOperations directive in loadmin.
    /// </summary>
    public class WorkflowOperationModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperationModel"/> class.
        /// </summary>
        public WorkflowOperationModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperationModel"/> class.
        /// </summary>
        /// <param name="id">The operation's id.</param>
        /// <param name="description">The operation's description.</param>
        /// <param name="isParent">Is this model a parent.</param>
        public WorkflowOperationModel(string id, string description, bool isParent = false)
        {
            this.Id = id;
            this.Description = description;
            this.IsParent = isParent;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperationModel"/> class.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="isParent">Is this model a parent.</param>
        public WorkflowOperationModel(WorkflowOperation operation, bool isParent = false)
        {
            this.Id = operation.Id;
            this.Description = operation.FriendlyDescription;
            this.IsParent = isParent;
        }

        /// <summary>
        /// Gets or sets the Id of the operation.
        /// </summary>
        /// <value>The operation's id.</value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the operation's description.
        /// </summary>
        /// <value>The operation's description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the operations children.  Children are usually exclusive in that only one can be selected.  Operations don't actually have children, but this is used
        /// for grouping purposes.
        /// </summary>
        /// <value>The operation's children.</value>
        public List<WorkflowOperationModel> Children { get; set; } = new List<WorkflowOperationModel>();

        /// <summary>
        /// Gets or sets the id of this child's parent.
        /// </summary>
        /// <value>The operation's parent id.</value>
        public string ParentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this model is a Parent containing operations that are exclusive to each other.
        /// </summary>
        /// <value>Whether the model is a parent.</value>
        public bool IsParent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the operation is disabled in the ui.  This only happens when the operation is exclusive, and a sibling has already been chosen.
        /// </summary>
        /// <value>Whether the operation should be disabled.</value>
        public bool Disabled { get; set; }
    }
}
