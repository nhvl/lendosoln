﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;

    /// <summary>
    /// Workflow operation for importing data into an existing file.
    /// </summary>
    public class ImportIntoExistingFile : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportIntoExistingFile"/> class.
        /// </summary>
        public ImportIntoExistingFile()
            : base("ImportIntoExistingFile", "Import FNM 3.2 Into Existing File")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.WriteLoan,
                WorkflowOperations.ReadLoan
            };
        }
    }
}
