﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// Workflow operation for reading a loan or template.
    /// </summary>
    public class ReadLoanOrTemplate : NonRestrainableWorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadLoanOrTemplate"/> class.
        /// </summary>
        public ReadLoanOrTemplate()
            : base("ReadLoanOrTemplate")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                WorkflowOperations.ReadLoan,
                WorkflowOperations.ReadTemplate
            };
        }

        /// <summary>
        /// Evaluates the operation against the given result dictionary.
        /// </summary>
        /// <param name="operationResults">A map from operation id to result.</param>
        /// <returns>True if all conditions are satisfied. Otherwise, false.</returns>
        public override bool GetResult(Dictionary<WorkflowOperation, ExecutingEngineResultType> operationResults)
        {
            return operationResults.IsAllow(WorkflowOperations.ReadLoan)
                || operationResults.IsAllow(WorkflowOperations.ReadTemplate);
        }
    }
}
