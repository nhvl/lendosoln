﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CommonProjectLib.ConfigSystem;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// Represents a basic workflow operation.
    /// </summary>
    /// <remarks>
    /// This class handles the most basic type of workflow operation. The
    /// operation does not have any dependencies other than itself, and
    /// evaluation only checks the single operation.
    /// Operations with more complex dependencies or evaluation logic should
    /// be added by creating a subclass of this class.
    /// </remarks>
    public class WorkflowOperation : IEquatable<WorkflowOperation>, ISystemOperationIdentifier
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        internal WorkflowOperation(string id)
            : this(id, id)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        internal WorkflowOperation(string id, string description)
        {
            this.Id = id;
            this.FriendlyDescription = description;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        /// <param name="evaluateAllConditionsForOperation">True if all conditions with this type should evaluate, false if the first successful evaluation ends evaluation execution.</param>
        internal WorkflowOperation(string id, string description, bool evaluateAllConditionsForOperation)
            : this(id, description)
        {
            this.EvaluateAllConditionsForOperation = evaluateAllConditionsForOperation;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowOperation"/> class.
        /// </summary>
        /// <param name="id">The workflow operation id that will be used by the workflow engine.</param>
        /// <param name="description">The friendly description of the operation.</param>
        /// <param name="evaluateAllConditionsForOperation">True if all conditions with this type should evaluate, false if the first successful evaluation ends evaluation execution.</param>
        /// <param name="isRestrainable">Indicates whether this operation can be used to create restraint.</param>
        internal WorkflowOperation(string id, string description, bool evaluateAllConditionsForOperation, bool isRestrainable)
            : this(id, description, evaluateAllConditionsForOperation)
        {
            this.IsRestrainable = isRestrainable;
        }

        /// <summary>
        /// Gets the id of the operation.
        /// </summary>
        /// <value>The id of the operation.</value>
        public string Id { get; }

        /// <summary>
        /// Gets the description of the operation.
        /// </summary>
        /// <value>The description of the operation.</value>
        /// <remarks>Can be the same as the Id in some cases.</remarks>
        public string FriendlyDescription { get; }

        /// <summary>
        /// Gets a value indicating whether all conditions with this
        /// operation type are evaluated during workflow evaluation,
        /// rather than evaluation completing on the first successful
        /// condition.
        /// </summary>
        /// <value>
        /// True if all conditions of this type should be evaluated,
        /// false if evaluation should complete on the first successful
        /// evaluation.
        /// </value>
        public bool EvaluateAllConditionsForOperation { get; }

        /// <summary>
        /// Gets a value indicating whether this operation may be
        /// used when creating restraints.
        /// </summary>
        /// <value>True if this operation can be used to create
        /// restraint conditions. False, otherwise.</value>
        /// <remarks>Defaults to true.</remarks>
        public bool IsRestrainable { get; } = true;

        /// <summary>
        /// Determines if two workflow operations are equal.
        /// </summary>
        /// <param name="lhs">The left hand side of the equality comparison.</param>
        /// <param name="rhs">The right hand side of the equality comparison.</param>
        /// <returns>True if the operations equal. Otherwise, false.</returns>
        public static bool operator ==(WorkflowOperation lhs, WorkflowOperation rhs)
        {
            if (ReferenceEquals(lhs, rhs))
            {
                return true;
            }
            else if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
            {
                return false;
            }

            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Determines if two workflow operations are not equal.
        /// </summary>
        /// <param name="lhs">The left hand side of the inequality comparison.</param>
        /// <param name="rhs">The right hand side of the inequality comparison.</param>
        /// <returns>True if the operations not equal. Otherwise, false.</returns>
        public static bool operator !=(WorkflowOperation lhs, WorkflowOperation rhs)
        {
            return !(lhs == rhs);
        }

        /// <summary>
        /// Gets the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The workflow operations this operation depends on.</returns>
        public virtual IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[] { this };
        }

        /// <summary>
        /// Evaluates the operation against the given result dictionary.
        /// </summary>
        /// <param name="operationResults">A map from operation id to result.</param>
        /// <returns>True if all conditions are satisfied. Otherwise, false.</returns>
        public virtual bool GetResult(Dictionary<WorkflowOperation, ExecutingEngineResultType> operationResults)
        {
            return this.GetOperationDependencies().All(operationResults.IsAllow);
        }

        /// <summary>
        /// Determines if the instance is equal to the given object.
        /// </summary>
        /// <param name="obj">The object to test for equality.</param>
        /// <returns>True if the objects are equal. Otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as WorkflowOperation);
        }

        /// <summary>
        /// Determines if the instance is equal to the given operation.
        /// </summary>
        /// <param name="other">The operation to test for equality.</param>
        /// <returns>True if the operations are equal. Otherwise, false.</returns>
        public bool Equals(WorkflowOperation other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }
            else if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.Id, other.Id, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the hash code of the operation.
        /// </summary>
        /// <returns>The hash code of the operation.</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Gets the string representation of the operation.
        /// </summary>
        /// <returns>The string representation of the operation.</returns>
        public override string ToString()
        {
            return this.Id;
        }
    }
}
