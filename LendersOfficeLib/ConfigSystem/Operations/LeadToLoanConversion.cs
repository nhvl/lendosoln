﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;

    /// <summary>
    /// Operation for converting a lead file to a loan file.
    /// </summary>
    public class LeadToLoanConversion : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadToLoanConversion"/> class.
        /// </summary>
        public LeadToLoanConversion()
            : base("LeadToLoanConversion", "Lead to Loan Conversion")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>
        /// The ids of the workflow operations this operation depends on.
        /// </returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.WriteLoan,
                WorkflowOperations.ReadLoan
            };
        }
    }
}
