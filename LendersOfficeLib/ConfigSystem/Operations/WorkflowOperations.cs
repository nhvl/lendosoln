﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Static class to hold all LendingQB workflow operations.
    /// </summary>
    public static class WorkflowOperations
    {
        /*
         * Operations that only have a single dependency and perform simple evaluation.
         */

        /// <summary>
        /// Operation for extending rate lock.
        /// </summary>
        public static readonly WorkflowOperation ExtendLock = new WorkflowOperation("ExtendLock", "Extend Lock");

        /// <summary>
        /// Operation for performing a float down rate lock.
        /// </summary>
        public static readonly WorkflowOperation FloatDownLock = new WorkflowOperation("FloatDownLoack", "Float Down Lock");

        /// <summary>
        /// Operation for generating documents.
        /// </summary>
        public static readonly WorkflowOperation GenerateDocs = new WorkflowOperation("DocMagicGenerateDocs", "Generate Docs");

        /// <summary>
        /// Operation for performing rate re-lock.
        /// </summary>
        public static readonly WorkflowOperation RateReLock = new WorkflowOperation("RateReLock", "Rate Re-Lock");

        /// <summary>
        /// Operation for registering a loan.
        /// </summary>
        public static readonly WorkflowOperation RegisterLoan = new WorkflowOperation("RegisterLoan", "Register Loan");

        /// <summary>
        /// Operation for using the old security model in PML.
        /// </summary>
        public static readonly WorkflowOperation UseOldSecurityModelForPml = new WorkflowOperation("UseOldSecurityModelForPML", "Use Old Security Model For PML");

        /// <summary>
        /// Operation for reading a loan.
        /// </summary>
        public static readonly WorkflowOperation ReadLoan = new NonRestrainableWorkflowOperation("ReadLoan", "Read Loan");

        /// <summary>
        /// Operation for reading a template.
        /// </summary>
        public static readonly WorkflowOperation ReadTemplate = new NonRestrainableWorkflowOperation("ReadTemplate");

        /// <summary>
        /// Operation for performing pre-save validation.
        /// </summary>
        /// <remarks>
        /// 3/18/2014 David Dao - OPM 144112.
        /// </remarks>
        public static readonly WorkflowOperation PreSaveValidation = new NonRestrainableWorkflowOperation("PreSaveValidation", "Pre-Saved Validation");

        /// <summary>
        /// Operation for field-level write.
        /// </summary>
        /// <remarks>
        /// Field-level write permission, Task Backend Business Rules, OPM 73366.
        /// </remarks>
        public static readonly WorkflowOperation WriteField = new NonRestrainableWorkflowOperation("WriteField", "Write: Field Level");

        /// <summary>
        /// Operation for field protection.
        /// </summary>
        public static readonly WorkflowOperation ProtectField = new NonRestrainableWorkflowOperation("ProtectField", "Protection: Field Level");

        /*
         * Operations that only have a dependency on itself and ReadLoan.
         */

        /// <summary>
        /// Operation for applying a title quote to a loan file.
        /// </summary>
        public static readonly WorkflowOperation ApplyTitleQuote = new LoanWorkflowOperation("ApplyTitleQuote", "Apply Title Quote");

        /// <summary>
        /// Operation for clearing the first entered dates for the RESPA 6.
        /// </summary>
        public static readonly WorkflowOperation ClearRespaFirstEnteredDates = new LoanWorkflowOperation("ClearRespaFirstEnteredDates", "Clear RESPA First Entered Dates");

        /// <summary>
        /// Operation for exporting the loan to a core system.
        /// </summary>
        /// <example>Episys/Symitar, Jack Henry, or other banking cores.</example>
        public static readonly WorkflowOperation ExportToCoreSystem = new LoanWorkflowOperation("ExportToCoreSystem", "Export to Core System");

        /// <summary>
        /// Operation for ordering an appraisal.
        /// </summary>
        public static readonly WorkflowOperation OrderAppraisal = new LoanWorkflowOperation("OrderAppraisal", "Order Appraisal");

        /// <summary>
        /// Operation for ordering VOE/VOI.
        /// </summary>
        public static readonly WorkflowOperation OrderVoe = new LoanWorkflowOperation("OrderVOE", "Order VOE/VOI");

        /// <summary>
        /// Operation for ordering credit.
        /// </summary>
        public static readonly WorkflowOperation OrderCredit = new LoanWorkflowOperation("OrderCredit", "Order Credit");

        /// <summary>
        /// Operation for ordering verification of assets or deposits.
        /// </summary>
        public static readonly WorkflowOperation OrderVoa = new LoanWorkflowOperation("OrderVOA", "Order VOA/VOD");

        /// <summary>
        /// Operation for ordering a Mortgage Insurance policy with delegated underwriting authority for a loan.
        /// </summary>
        public static readonly WorkflowOperation OrderMIPolicyDelegated = new LoanWorkflowOperation(nameof(OrderMIPolicyDelegated), "Order MI Policy (Delegated)");

        /// <summary>
        /// Operation for ordering a Mortgage Insurance policy for a loan.
        /// </summary>
        public static readonly WorkflowOperation OrderMIPolicyNonDelegated = new LoanWorkflowOperation(nameof(OrderMIPolicyNonDelegated), "Order MI Policy (Non-Delegated)");

        /// <summary>
        /// Operation for ordering a Mortgage Insurance policy for a loan.
        /// </summary>
        public static readonly WorkflowOperation OrderMIQuote = new LoanWorkflowOperation(nameof(OrderMIQuote), "Order MI Quote");

        /// <summary>
        /// Operation for generating UCD.
        /// </summary>
        public static readonly WorkflowOperation GenerateUCD = new LoanWorkflowOperation("GenerateUCD", "Generate UCD");

        /// <summary>
        /// Operation for ordering initial disclosures in the TPO portal.
        /// </summary>
        public static readonly WorkflowOperation OrderInitialDisclosureInTpoPortal = new LoanWorkflowOperation(nameof(OrderInitialDisclosureInTpoPortal), "Order Initial Disclosure in Originator Portal", evaluateAllConditionsForOperation: true);

        /// <summary>
        /// Operation for ordering SSA-89, also known as SSN verification.
        /// </summary>
        public static readonly WorkflowOperation OrderSSA89 = new LoanWorkflowOperation("OrderSSA89", "Order SSA-89");

        /// <summary>
        /// Operation for ordering a title quote. This excludes quick quotes placed through pricing.
        /// </summary>
        public static readonly WorkflowOperation OrderTitleQuote = new LoanWorkflowOperation("OrderTitleQuote", "Order Title Quote");

        /// <summary>
        /// Operation to request an initial closing disclosure in the TPO portal for a loan file.
        /// </summary>
        public static readonly WorkflowOperation RequestInitialClosingDisclosureInTpoPortal = new LoanWorkflowOperation("RequestInitialClosingDisclosureInTpoPortal", "Request Initial Closing Disclosure in Originator Portal");

        /// <summary>
        /// Operation to determine if a rate lock request auto-locks the file.
        /// </summary>
        public static readonly WorkflowOperation RequestRateLockCausesLock = new RequestRateLockCausesLock();

        /// <summary>
        /// Operation to determine if rate lock requests require a manual lock.
        /// </summary>
        public static readonly WorkflowOperation RequestRateLockRequiresManualLock = new LoanWorkflowOperation("RequestRateLockRequiresManualLock", "Request Rate Lock (manual lock)");

        /// <summary>
        /// Operation to request a re-disclosure in the TPO portal for a loan file.
        /// </summary>
        public static readonly WorkflowOperation RequestRedisclosureInTpoPortal = new LoanWorkflowOperation("RequestRedisclosureInTpoPortal", "Request Redisclosure in Originator Portal");

        /// <summary>
        /// Operation for running DO on a loan file.
        /// </summary>
        public static readonly WorkflowOperation RunDo = new LoanWorkflowOperation("RunDo", "Run DO");

        /// <summary>
        /// Operation for running DU on a loan file.
        /// </summary>
        public static readonly WorkflowOperation RunDu = new LoanWorkflowOperation("RunDu", "Run DU");

        /// <summary>
        /// Operation for running LP on a loan file.
        /// </summary>
        public static readonly WorkflowOperation RunLp = new LoanWorkflowOperation("RunLp", "Run LPA");

        /// <summary>
        /// Operation for running PML for all loans.
        /// </summary>
        public static readonly WorkflowOperation RunPmlForAllLoans = new LoanWorkflowOperation("RunPmlForAllLoans", "Run PML (all programs)");

        /// <summary>
        /// Operation for running PML for registered loans.
        /// </summary>
        public static readonly WorkflowOperation RunPmlForRegisteredLoans = new LoanWorkflowOperation("RunPmlForRegisteredLoans", "Run PML (registered program only)");

        /// <summary>
        /// Operation for running PML to step 3.
        /// </summary>
        public static readonly WorkflowOperation RunPmlToStep3 = new LoanWorkflowOperation("RunPmlToStep3", "Run PML (to step 3)");

        /// <summary>
        /// Operation for writing/updating a loan file.
        /// </summary>
        public static readonly WorkflowOperation WriteLoan = new LoanWorkflowOperation("WriteLoan", "Write Loan", evaluateAllConditionsForOperation: false, isRestrainable: false);

        /// <summary>
        /// Operation to allow recording external disclosure events.
        /// </summary>
        public static readonly WorkflowOperation AllowRecordingExternalDisclosureEvents = new LoanWorkflowOperation("AllowRecordingExternalDisclosureEvents", "Allow recording external disclosure events");

        /// <summary>
        /// Operation for using the document capture engine.
        /// </summary>
        public static readonly WorkflowOperation UseDocumentCapture = new LoanWorkflowOperation("UseDocumentCapture", "Use Document Capture");

        /// <summary>
        /// Operation for uploading EDocs.
        /// </summary>
        public static readonly WorkflowOperation UploadEDocs = new LoanWorkflowOperation("UploadEDocs", "Upload EDocs");

        /*
         * Complex operations that have multiple dependencies or complex logic
         */

        /// <summary>
        /// Operation for converting a lead file to a loan file.
        /// </summary>
        public static readonly WorkflowOperation LeadToLoanConversion = new LeadToLoanConversion();

        /// <summary>
        /// Operation for deleting an application.
        /// </summary>
        public static readonly WorkflowOperation DeleteApp = new DeleteApplication();

        /// <summary>
        /// Operation for deleting a loan file.
        /// </summary>
        public static readonly WorkflowOperation DeleteLoan = new DeleteLoan();

        /// <summary>
        /// Operation for importing data into an existing loan file.
        /// </summary>
        public static readonly WorkflowOperation ImportIntoExistingFile = new ImportIntoExistingFile();

        /// <summary>
        /// Operation for performing loan status change.
        /// </summary>
        public static readonly WorkflowOperation LoanStatusChange = new LoanStatusChange();

        /// <summary>
        /// Operation for performing status change to Condition Review within PML.
        /// </summary>
        public static readonly WorkflowOperation LoanStatusChangeToConditionReview = new LoanStatusChangeToConditionReview();

        /// <summary>
        /// Operation for performing status change to Document Check within PML.
        /// </summary>
        public static readonly WorkflowOperation LoanStatusChangeToDocumentCheck = new LoanStatusChangeToDocumentCheck();

        /// <summary>
        /// Operation for manually overriding QM settings.
        /// </summary>
        public static readonly WorkflowOperation ManuallyOverrideQMSettings = new ManuallyOverrideQMSettings();

        /// <summary>
        /// Operation for reading a loan or template.
        /// </summary>
        public static readonly WorkflowOperation ReadLoanOrTemplate = new ReadLoanOrTemplate();

        /// <summary>
        /// Operation for writing/updating a loan or template.
        /// </summary>
        public static readonly WorkflowOperation WriteLoanOrTemplate = new WriteLoanOrTemplate();

        /// <summary>
        /// Operation for writing/updating a template.
        /// </summary>
        public static readonly WorkflowOperation WriteTemplate = new WriteTemplate();

        /// <summary>
        /// Operations excluded for non-internal users.
        /// </summary>
        private static HashSet<WorkflowOperation> operationsExcludedForNonInternalUsers = new HashSet<WorkflowOperation>()
        {
            LoanStatusChangeToConditionReview,
            LoanStatusChangeToDocumentCheck
        };

        /// <summary>
        /// A map from operation id to operation.
        /// </summary>
        private static Dictionary<string, WorkflowOperation> operationById;

        /// <summary>
        /// Initializes static members of the <see cref="WorkflowOperations"/> class.
        /// </summary>
        static WorkflowOperations()
        {
            operationById = typeof(WorkflowOperations).GetFields(BindingFlags.Static | BindingFlags.Public)
                .Where(fieldInfo => typeof(WorkflowOperation).IsAssignableFrom(fieldInfo.FieldType))
                .Select(fieldInfo => fieldInfo.GetValue(null))
                .Cast<WorkflowOperation>()
                .ToDictionary(p => p.Id, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets an enumerable of operations excluded for non-internal users.
        /// </summary>
        public static IEnumerable<WorkflowOperation> OperationsExcludedForNonInternalUsers
        {
            get
            {
                return operationsExcludedForNonInternalUsers;
            }
        }

        /// <summary>
        /// Gets an enumerable of all workflow operations.
        /// </summary>
        /// <returns>An enumerable of all workflow operations.</returns>
        public static IEnumerable<WorkflowOperation> GetAll()
        {
            return operationById.Values;
        }

        /// <summary>
        /// Gets an enumerable of the workflow operations for display in the editing UI.
        /// </summary>
        /// <param name="isInternalUser">Is the user this display list is for an internal user?.</param>
        /// <returns>An enumerable of the workflow operations for display in the editing UI.</returns>
        public static IEnumerable<WorkflowOperation> GetForDisplay(bool isInternalUser)
        {
            WorkflowOperation[] excludedOperations = new WorkflowOperation[]
            {
                ReadLoanOrTemplate,
                WriteLoanOrTemplate,
                ProtectField,
                ReadTemplate,
                WriteTemplate
            };

            IEnumerable<WorkflowOperation> operations = operationById.Values.Except(excludedOperations);

            if (!isInternalUser)
            {
                return operations.Except(OperationsExcludedForNonInternalUsers);
            }

            return operations;
        }

        /// <summary>
        /// Gets an operation by its id.
        /// </summary>
        /// <param name="id">The id of the operation.</param>
        /// <returns>The workflow operation.</returns>
        public static WorkflowOperation Get(string id)
        {
            return operationById[id];
        }

        /// <summary>
        /// Checks if an operation exists.
        /// </summary>
        /// <param name="id">The id of the operation.</param>
        /// <returns>True if the id corresponds to an existing operation. False otherwise.</returns>
        public static bool Exists(string id)
        {
            return operationById.ContainsKey(id);
        }

        /// <summary>
        /// Determines whether all conditions for the operation with the
        /// specified id are evaluated during workflow evaluation,
        /// rather than evaluation completing on the first successful
        /// condition.
        /// </summary>
        /// <param name="id">
        /// The id of the operation.
        /// </param>
        /// <returns>
        /// True if all conditions of the operation are evaluated,
        /// false if evaluation completes on the first successful
        /// evaluation.
        /// </returns>
        public static bool AreAllConditionsEvaluatedForOperation(string id)
        {
            return Exists(id) && Get(id).EvaluateAllConditionsForOperation;
        }
    }
}
