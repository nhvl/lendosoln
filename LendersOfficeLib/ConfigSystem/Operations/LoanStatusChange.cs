﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// Workflow operation for changing loan status.
    /// </summary>
    public class LoanStatusChange : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanStatusChange"/> class.
        /// </summary>
        public LoanStatusChange()
            : base("LoanStatusChange", "Change Loan Status")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.WriteLoan,
                WorkflowOperations.ReadLoan
            };
        }

        /// <summary>
        /// Evaluates the operation against the given result dictionary.
        /// </summary>
        /// <param name="operationResults">A map from operation id to result.</param>
        /// <returns>True if all conditions are satisfied. Otherwise, false.</returns>
        public override bool GetResult(Dictionary<WorkflowOperation, ExecutingEngineResultType> operationResults)
        {
            return operationResults.IsAllow(this)
                && operationResults.IsAllow(WorkflowOperations.ReadLoan);
        }
    }
}
