﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// The WorkflowViewModel acts as the main datasource  for the WorkflowOperation's directive in the LoAdmin.
    /// It contains a list of available and selected WorkflowOperationModels.
    /// </summary>
    public class WorkflowViewModel
    {
        /// <summary>
        /// An id for the header of the (potentially) exclusive Run PML operations.
        /// </summary>
        private const string RunPml = "RunPml";

        /// <summary>
        /// An id for the header of the (potentially) exclusive Rate Lock operations.
        /// </summary>
        private const string RequestRateLock = "RequestRateLock";

        /// <summary>
        /// An id of the header for the (potentially) exclusive rate lock operations.
        /// </summary>
        private const string LockStatusChange = "LockStatusChange";

        /// <summary>
        /// The dictionary that maps a parent's id to a list of WorkflowOperation ids.
        /// </summary>
        private static Dictionary<string, WorkflowOperation[]> exclusiveValues = new Dictionary<string, WorkflowOperation[]>()
        {
            { RunPml, new[] { WorkflowOperations.RunPmlForAllLoans, WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlToStep3 } },
            { RequestRateLock, new[] { WorkflowOperations.RequestRateLockCausesLock, WorkflowOperations.RequestRateLockRequiresManualLock } },
            { LockStatusChange, new[] { WorkflowOperations.LoanStatusChangeToDocumentCheck, WorkflowOperations.LoanStatusChangeToConditionReview } }
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowViewModel"/> class.
        /// </summary>
        public WorkflowViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowViewModel"/> class.
        /// </summary>
        /// <param name="isEnforceExclusive">A boolean that determines whether the exclusive logic should be enforced.</param>
        /// <param name="allOperations">A list of all WorkflowOperations available in the ui.</param>
        /// <param name="selectedOperations">A list of WorkflowOperations that have been selected.</param>
        public WorkflowViewModel(
            bool isEnforceExclusive,
            IEnumerable<WorkflowOperation> allOperations,
            IEnumerable<WorkflowOperation> selectedOperations)
        {
            this.IsEnforceExclusive = isEnforceExclusive;
            this.SetSelectedOperations(allOperations, selectedOperations);
            this.EnforceExclusiveLogic();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowViewModel"/> class.
        /// </summary>
        /// <param name="isEnforceExclusive">A boolean that determines whether the exclusive logic should be enforced.</param>
        /// <param name="availableOperationModels">The list of operation models that are still available for selection.</param>
        /// <param name="selectedOperationModels">The list of already selected operation models.</param>
        public WorkflowViewModel(
            bool isEnforceExclusive,
            List<WorkflowOperationModel> availableOperationModels,
            List<WorkflowOperationModel> selectedOperationModels)
        {
            this.IsEnforceExclusive = isEnforceExclusive;
            this.AvailableOperations = availableOperationModels;
            this.SelectedOperations = selectedOperationModels;

            this.EnforceExclusiveLogic();
        }

        /// <summary>
        /// Gets or sets the list of WorkflowOperationModels that are selected.  Does not include selected operations.
        /// </summary>
        /// <value>A list of selected WorkflowOperationModels.</value>
        public List<WorkflowOperationModel> SelectedOperations { get; set; } = new List<WorkflowOperationModel>();

        /// <summary>
        /// Gets or sets the list of WorkflowOperationModels that are available for selection.  Does not include available operations.
        /// </summary>
        /// <value>A list of available WorkflowOperationModels.</value>
        public List<WorkflowOperationModel> AvailableOperations { get; set; } = new List<WorkflowOperationModel>();

        /// <summary>
        /// Gets or sets a value indicating whether the exclusive logic is enforced in the Ui.
        /// </summary>
        /// <value>A boolean that determines whether the exclusive logic is enforced.</value>
        public bool IsEnforceExclusive { get; set; }

        /// <summary>
        /// Takes in a list of operations and converts them to WorkflowOperationModels.  It then populates pushes those models to the provided list of models.
        /// </summary>
        /// <param name="operations">The operations to convert.</param>
        /// <param name="models">The list of models to add the converted oeprations to.</param>
        public static void ConvertOperationsToModels(IEnumerable<WorkflowOperation> operations, List<WorkflowOperationModel> models)
        {
            var runPml = new WorkflowOperationModel(RunPml, "Run PML", true);
            var requestRateLock = new WorkflowOperationModel(RequestRateLock, "Request Rate Lock", true);
            var lockStatusChange = new WorkflowOperationModel(LockStatusChange, "Loan Status Change", true);

            foreach (WorkflowOperation operation in operations)
            {
                var pairs = WorkflowViewModel.exclusiveValues.Where(pair => pair.Value.Contains(operation));

                var item = new WorkflowOperationModel(operation);

                if (pairs.Count() == 0)
                {
                    models.Add(item);
                }
                else
                {
                    switch (pairs.First().Key)
                    {
                        case RunPml:
                            runPml.Children.Add(item);
                            item.ParentId = RunPml;
                            break;
                        case RequestRateLock:
                            requestRateLock.Children.Add(item);
                            item.ParentId = RequestRateLock;
                            break;
                        case LockStatusChange:
                            lockStatusChange.Children.Add(item);
                            item.ParentId = LockStatusChange;
                            break;
                        default:
                            throw new CBaseException(
                                "Unexpected Workflow Operation.",
                                $"There was an unexpected exclusive type of operation. Could not add the operation {operation.FriendlyDescription} to a parent.");
                    }
                }
            }

            if (runPml.Children.Count > 0)
            {
                models.Add(runPml);
            }

            if (requestRateLock.Children.Count > 0)
            {
                models.Add(requestRateLock);
            }

            if (lockStatusChange.Children.Count > 0)
            {
                models.Add(lockStatusChange);
            }
        }

        /// <summary>
        /// Checks to see whether to enforce the exclusive logic.  If it's enforced, it will set the disabled property to true for Operations if one of
        /// their corresponding siblings have been selected.
        /// </summary>
        public void EnforceExclusiveLogic()
        {
            if (this.IsEnforceExclusive)
            {
                var selectedParents = this.SelectedOperations.Where(item => item.Children.Count() > 0);
                var availableParents = this.AvailableOperations.Where(item => item.Children.Count() > 0);
                foreach (WorkflowOperationModel item in selectedParents)
                {
                    var availableParent = availableParents.FirstOrDefault(p => p.Id == item.Id);
                    if (availableParent != null)
                    {
                        foreach (WorkflowOperationModel child in availableParent.Children)
                        {
                            child.Disabled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Takes in a list of all available Operations, and a list of selected Operations.  It then proceeds to determine
        /// which operations are still available, and populates the AvailableOperationModels and SelectedOperationModels lists.
        /// </summary>
        /// <param name="allOperations">All of the operations available in the ui.</param>
        /// <param name="selectedOperations">All of the already selected operations.</param>
        public void SetSelectedOperations(IEnumerable<WorkflowOperation> allOperations, IEnumerable<WorkflowOperation> selectedOperations)
        {
            var availableOperations = allOperations.Where(item1 => !selectedOperations.Any(item2 => string.Equals(item1.Id, item2.Id)));

            List<WorkflowOperationModel> availableOperationItems = new List<WorkflowOperationModel>();

            WorkflowViewModel.ConvertOperationsToModels(availableOperations, this.AvailableOperations);
            WorkflowViewModel.ConvertOperationsToModels(selectedOperations, this.SelectedOperations);
        }

        /// <summary>
        /// Retrieves a list of WorkflowOperations by enumerating through the selected WorkflowOperationModels.
        /// </summary>
        /// <returns>A list of selected Workflow Operations.</returns>
        public List<WorkflowOperation> RetrieveSelectedOperations()
        {
            List<WorkflowOperation> selectedOperations = new List<WorkflowOperation>();

            foreach (WorkflowOperationModel item in this.SelectedOperations)
            {
                if (item.Children.Count() == 0)
                {
                    selectedOperations.Add(WorkflowOperations.Get(item.Id));
                }
                else
                {
                    foreach (WorkflowOperationModel child in item.Children)
                    {
                        selectedOperations.Add(WorkflowOperations.Get(child.Id));
                    }
                }
            }

            return selectedOperations;
        }
    }
}
