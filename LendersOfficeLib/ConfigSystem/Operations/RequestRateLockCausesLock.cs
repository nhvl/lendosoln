﻿namespace LendersOffice.ConfigSystem.Operations
{
    using System.Collections.Generic;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// Workflow operation to control whether a rate lock request auto-locks the rate.
    /// </summary>
    public class RequestRateLockCausesLock : WorkflowOperation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestRateLockCausesLock"/> class.
        /// </summary>
        public RequestRateLockCausesLock()
            : base("RequestRateLockCausesLock", "Request Rate Lock (auto lock)")
        {
        }

        /// <summary>
        /// Gets the ids of the workflow operations this operation depends on.
        /// </summary>
        /// <returns>The ids of the workflow operations this operation depends on.</returns>
        public override IEnumerable<WorkflowOperation> GetOperationDependencies()
        {
            return new[]
            {
                this,
                WorkflowOperations.ReadLoan
            };
        }

        /// <summary>
        /// Evaluates the operation against the given result dictionary.
        /// </summary>
        /// <param name="operationResults">A map from operation id to result.</param>
        /// <returns>True if all conditions are satisfied. Otherwise, false.</returns>
        public override bool GetResult(Dictionary<WorkflowOperation, ExecutingEngineResultType> operationResults)
        {
            return operationResults.ContainsKey(this)
                && operationResults.IsAllow(this)
                && operationResults.IsAllow(WorkflowOperations.ReadLoan);
        }
    }
}
