﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Admin;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using Security;

    /// <summary>
    /// Workflow change audit data class.
    /// </summary>
    public class WorkflowChangeAudit : IComparable<WorkflowChangeAudit>
    {
        /// <summary>
        /// User description for caching.
        /// </summary>
        private string userDescription = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowChangeAudit"/> class.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="userId">User Id belonging to the principal.</param>
        /// <param name="auditType">Audit Type.</param>
        /// <param name="auditDesc">Audit Description.</param>
        /// <param name="auditDate">Audit created date.</param>
        /// <param name="isInternalUser">Is internal user.</param>
        /// <param name="releaseConfigId">Release configuration id.</param>
        public WorkflowChangeAudit(Guid brokerId, Guid userId, WorkflowAuditType auditType, string auditDesc, DateTime auditDate, bool isInternalUser, long releaseConfigId = -1)
        {
            this.BrokerId = brokerId;
            this.UserId = userId;
            this.AuditType = auditType;
            this.AuditDescription = auditDesc;
            this.AuditDate = CDateTime.Create(auditDate);
            this.IsInternalUser = isInternalUser;
            this.ReleaseConfigId = releaseConfigId;            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowChangeAudit"/> class.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="auditType">Audit Type.</param>
        /// <param name="auditDesc">Audit Description.</param>
        /// <param name="releaseConfigId">Release configuration id.</param>
        public WorkflowChangeAudit(Guid brokerId, AbstractUserPrincipal principal, WorkflowAuditType auditType, string auditDesc, long releaseConfigId = -1)
        {
            this.BrokerId = brokerId;
            this.UserId = principal.UserId;
            this.AuditType = auditType;
            this.AuditDescription = auditDesc;
            this.AuditDate = CDateTime.Create(DateTime.Now);
            this.IsInternalUser = principal is InternalUserPrincipal;
            this.ReleaseConfigId = releaseConfigId;
        }

        /// <summary>
        /// Gets or sets broker Id belonging to the workflow change.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets user Id of the user that updated workflow.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets workflow change event enum.
        /// </summary>
        public WorkflowAuditType AuditType { get; set; }

        /// <summary>
        /// Gets or sets workflow change description.
        /// </summary>
        public string AuditDescription { get; set; }

        /// <summary>
        /// Gets or sets when the workflow change was made.
        /// </summary>
        public CDateTime AuditDate { get; set; }

        /// <summary>
        /// Gets workflow change date formatted to MM/DD/YYYY HH/MM.
        /// </summary>
        public string AuditDateFormatted
        {
            get
            {
                return this.AuditDate.DateTimeForComputationWithTime.ToString("MM/dd/yyyy h:mm tt");
            }
        }

        /// <summary>
        /// Gets string that describes who changed workflow event.
        /// </summary>
        public string UserDescription
        {
            get
            {
                if (this.IsInternalUser)
                {
                    return "Internal LendingQB";
                }

                if (this.userDescription == null)
                {
                    this.userDescription = EmployeeDB.RetrieveNameByUserId(this.BrokerId, this.UserId);
                }

                return this.userDescription;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether you are an internal LQB user.
        /// </summary>
        public bool IsInternalUser { get; set; }

        /// <summary>
        /// Gets a value indicating whether the change was a release configuration or an another event.
        /// </summary>
        public bool IsReleaseConfigAudit
        {
            get
            {
                if (this.AuditType == WorkflowAuditType.ReleaseConfig)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets release configuration id matching column in CONFIG_RELEASED table.
        /// </summary>
        public long ReleaseConfigId { get; set; }

        /// <summary>
        /// List all audits belonging to a broker.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <returns>List of audits belonging to a broker.</returns>
        public static List<WorkflowChangeAudit> ListAllWorkflowChangeAuditsForBroker(Guid brokerId)
        {
            List<WorkflowChangeAudit> audits = new List<WorkflowChangeAudit>();

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
            };

            StoredProcedureName sp = StoredProcedureName.Create("WORKFLOW_CHANGE_AUDIT_LIST_ALL_FOR_BROKER").Value;

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, sp, parameters, TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    Guid userId = (Guid)reader["UserId"];
                    WorkflowAuditType auditType = (WorkflowAuditType)int.Parse(reader["WorkflowAuditType"].ToString());
                    string auditDesc = reader["AuditDescription"].ToString();
                    DateTime auditDate = (DateTime)reader["AuditDate"];
                    bool isInternal = (bool)reader["IsInternalUser"];
                    long configId = (long)reader["ReleaseConfigId"];

                    WorkflowChangeAudit audit = new WorkflowChangeAudit(brokerId, userId, auditType, auditDesc, auditDate, isInternal, configId);

                    audits.Add(audit);
                }
            }            

            return audits.OrderByDescending(audit => audit).ToList();
        }

        /// <summary>
        /// Retrieve specific workflow configuration XML with a configuration id.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="configId">Configuration id.</param>
        /// <returns>XML string for that configuration id.</returns>
        public static string RetrieveWorkflowConfigXMLByConfigId(Guid brokerId, long configId)
        {
            string configXML = string.Empty;

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ConfigId", configId),
            };

            StoredProcedureName sp = StoredProcedureName.Create("CONFIG_RELEASED_RETRIEVE_CONFIGXML_BY_ID").Value;

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, sp, parameters, TimeoutInSeconds.Default))
            {
                if (reader.Read())
                {
                    configXML = reader["ConfigurationXmlContent"].ToString();
                }
                else
                {
                    throw new CBaseException("Invalid attempt at trying to retrieve workflow configuration XML.", $"Invalid attempt at trying to retrieve workflow configuration XML. BrokerId: {brokerId}, ConfigId: {configId}.");
                }
            }

            return configXML;
        }

        /// <summary>
        /// Save the audit.
        /// </summary>
        public void Save()
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@UserId", this.UserId),
                new SqlParameter("@WorkflowAuditType", this.AuditType),
                new SqlParameter("@AuditDescription", this.AuditDescription),
                new SqlParameter("@AuditDate", this.AuditDate.DateTimeForDBWriting),
                new SqlParameter("@IsInternalUser", this.IsInternalUser),
                new SqlParameter("@ReleaseConfigId", this.ReleaseConfigId)
            };

            StoredProcedureName sp = StoredProcedureName.Create("WORKFLOW_CHANGE_AUDIT_CREATE").Value;

            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, sp, parameters, TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Compare function for audits.
        /// </summary>
        /// <param name="a">Audit a to compare.</param>
        /// <param name="b">Audit b to compare.</param>
        /// <returns>Standard compare return values.</returns>
        public int Compare(WorkflowChangeAudit a, WorkflowChangeAudit b)
        {
            if (a.AuditDate.DateTimeForComputationWithTime < b.AuditDate.DateTimeForComputationWithTime)
            {
                return -1;
            }
            else if (a.AuditDate.DateTimeForComputationWithTime == b.AuditDate.DateTimeForComputationWithTime)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Compare function for audits.
        /// </summary>
        /// <param name="other">Audit other.</param>
        /// <returns>Standard compare return values.</returns>
        public int CompareTo(WorkflowChangeAudit other)
        {
            if (this.AuditDate.DateTimeForComputationWithTime < other.AuditDate.DateTimeForComputationWithTime)
            {
                return -1;
            }
            else if (this.AuditDate.DateTimeForComputationWithTime == other.AuditDate.DateTimeForComputationWithTime)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}