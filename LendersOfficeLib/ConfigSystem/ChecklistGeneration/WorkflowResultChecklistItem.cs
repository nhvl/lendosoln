﻿/// <copyright file="WorkflowResultChecklistItem.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/14/2017
/// </summary>
namespace LendersOffice.ConfigSystem.ChecklistGeneration
{
    /// <summary>
    /// Provides a wrapper for a checklist item.
    /// </summary>
    public class WorkflowResultChecklistItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowResultChecklistItem"/> class.
        /// </summary>
        /// <param name="pass">
        /// True if the checklist item passed, false otherwise.
        /// </param>
        /// <param name="description">
        /// The description for the checklist item.
        /// </param>
        public WorkflowResultChecklistItem(bool pass, string description)
        {
            this.Pass = pass;
            this.Description = description;
        }

        /// <summary>
        /// Gets a value indicating whether the checklist item passed.
        /// </summary>
        /// <value>
        /// True if the checklist item passed, false otherwise.
        /// </value>
        public bool Pass { get; private set; }

        /// <summary>
        /// Gets the description for the checklist item.
        /// </summary>
        /// <value>
        /// The description for the checklist item.
        /// </value>
        public string Description { get; private set; }
    }
}
