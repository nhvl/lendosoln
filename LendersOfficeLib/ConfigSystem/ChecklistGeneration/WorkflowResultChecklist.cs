﻿/// <copyright file="WorkflowResultChecklist.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/14/2017
/// </summary>
namespace LendersOffice.ConfigSystem.ChecklistGeneration
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a wrapper for workflow result checklist data.
    /// </summary>
    public class WorkflowResultChecklist
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowResultChecklist"/> class.
        /// </summary>
        /// <param name="canPerformOrder">
        /// True if the user can perform the operation, false otherwise.
        /// </param>
        /// <param name="failureMessages">
        /// The failure messages for conditions that evaluated to false.
        /// </param>
        /// <param name="lineItems">
        /// The items in the checklist.
        /// </param>
        public WorkflowResultChecklist(bool canPerformOrder, List<string> failureMessages, List<WorkflowResultChecklistItem> lineItems)
        {
            this.CanPerformOrder = canPerformOrder;
            this.OrderDenialReason = string.Join(Environment.NewLine, failureMessages);
            this.LineItems = lineItems;
        }

        /// <summary>
        /// Gets a value indicating whether the user 
        /// can perform the workflow operation.
        /// </summary>
        /// <value>
        /// True if the user can perform the operation,
        /// false otherwise.
        /// </value>
        public bool CanPerformOrder { get; private set; }

        /// <summary>
        /// Gets the denial reason if the user cannot 
        /// perform the operation.
        /// </summary>
        /// <value>
        /// The denial reason.
        /// </value>
        public string OrderDenialReason { get; private set; }

        /// <summary>
        /// Gets the items in the checklist.
        /// </summary>
        /// <value>
        /// The items in the checklist.
        /// </value>
        public IReadOnlyList<WorkflowResultChecklistItem> LineItems { get; private set; }
    }
}