﻿/// <copyright file="WorkflowResultChecklistGenerator.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/14/2017
/// </summary>
namespace LendersOffice.ConfigSystem.ChecklistGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means for generating user-friendly checklists based
    /// on the evaluation of a workflow operation.
    /// </summary>
    public class WorkflowResultChecklistGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowResultChecklistGenerator"/> class.
        /// </summary>
        /// <param name="principal">
        /// The user principal used to evaluate workflow.
        /// </param>
        /// <param name="operation">
        /// The operation to evaluate.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan to evaluate.
        /// </param>
        public WorkflowResultChecklistGenerator(AbstractUserPrincipal principal, WorkflowOperation operation, Guid loanId)
        {
            this.Principal = principal;
            this.Operation = operation;
            this.LoanId = loanId;
        }

        /// <summary>
        /// Gets or sets the user principal used to evaluate workflow.
        /// </summary>
        /// <value>
        /// The user principal.
        /// </value>
        private AbstractUserPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets the operation to evaluate.
        /// </summary>
        /// <value>
        /// The operation to evaluate.
        /// </value>
        private WorkflowOperation Operation { get; set; }

        /// <summary>
        /// Gets or sets the ID of the loan to evaluate.
        /// </summary>
        /// <value>
        /// The ID of the loan to evaluate.
        /// </value>
        private Guid LoanId { get; set; }

        /// <summary>
        /// Generates a <seealso cref="WorkflowResultChecklist"/> by
        /// evaluating all workflow rules for the  specified <see cref="Operation"/>.
        /// </summary>
        /// <returns>
        /// The result checklist.
        /// </returns>
        public WorkflowResultChecklist Generate()
        {
            var loanValueEvaluator = new LoanValueEvaluator(this.Principal.BrokerId, this.LoanId, this.Operation);
            loanValueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.Principal));

            var evaluationResults = LendingQBExecutingEngine.MasterDebugOperation(this.Operation, loanValueEvaluator);

            var restraintsList = new HashSet<ExecutingEngineConditionDebug>(new ExecutingEngineConditionDebugComparer());
            var privilegesList = new HashSet<ExecutingEngineConditionDebug>(new ExecutingEngineConditionDebugComparer());
            foreach (var orderDisclosureDebug in evaluationResults.ExecutingEngineDebugList.Where(debug => debug.Operation == this.Operation.Id))
            {
                restraintsList.UnionWith(orderDisclosureDebug.RestraintList);
                privilegesList.UnionWith(orderDisclosureDebug.ConditionList);
            }

            var canPerformOrder = true;
            var lineItems = new List<WorkflowResultChecklistItem>();
            var failureMessages = new List<string>();
            var workflowModel = new WorkflowSystemConfigModel(this.Principal.BrokerId, loadDraft: false);

            canPerformOrder &= ParseChecklistItems(restraintsList, workflowModel, true, lineItems, failureMessages);
            canPerformOrder &= ParseChecklistItems(privilegesList, workflowModel, false, lineItems, failureMessages);

            return new WorkflowResultChecklist(canPerformOrder, failureMessages, lineItems);
        }

        /// <summary>
        /// Parses checklist items from a list of conditions.
        /// </summary>
        /// <param name="conditionSet">The set of conditions to parse.</param>
        /// <param name="workflowModel">The workflow config model.</param>
        /// <param name="isForRestraints">True if we are parsing restraint conditions.</param>
        /// <param name="lineItems">Running list of line items. Used as output.</param>
        /// <param name="failureMessages">Running list of failure messages. Used as Output.</param>
        /// <returns>True if all conditions passed. False otherwise.</returns>
        private static bool ParseChecklistItems(
            HashSet<ExecutingEngineConditionDebug> conditionSet,
            WorkflowSystemConfigModel workflowModel,
            bool isForRestraints,
            List<WorkflowResultChecklistItem> lineItems,
            List<string> failureMessages)
        {
            bool allPassed = true;

            foreach (var conditionDebug in conditionSet)
            {
                var conditionFromConfig = isForRestraints ? workflowModel.GetRestraint(conditionDebug.ConditionId) : workflowModel.GetCondition(conditionDebug.ConditionId);
                if (conditionFromConfig == null)
                {
                    // If this condition is null, it is coming from the system config
                    // and should be skipped.
                    continue;
                }

                var hasRequiredInclusionParameterFailure = conditionDebug.FailedParameterList.Any(failedParameter =>
                    conditionFromConfig.ParameterSet.IsRequiredParameterForChecklist(failedParameter.VarName));

                if (hasRequiredInclusionParameterFailure)
                {
                    continue;
                }

                var passed = isForRestraints ? conditionDebug.FailedParameterCount > 0 : conditionDebug.FailedParameterCount == 0;
                allPassed &= passed;

                if (!passed)
                {
                    failureMessages.Add(conditionFromConfig.FailureMessage);
                }

                lineItems.Add(new WorkflowResultChecklistItem(passed, conditionFromConfig.FailureMessage));
            }

            return allPassed;
        }
    }
}
