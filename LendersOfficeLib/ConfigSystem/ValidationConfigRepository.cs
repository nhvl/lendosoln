﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigSystem.DataAccess;
using ConfigSystem;
using DataAccess;

namespace LendersOffice.ConfigSystem
{
    /// <summary>
    /// Implements a sql config repository that validates before saving and loading.
    /// </summary>
    public class ValidationConfigRepository : IConfigRepository
    {
        private IConfigRepository m_repository;       

        #region IConfigRepository Members

        public ValidationConfigRepository(IConfigRepository repository)
        {
            m_repository = repository;
        }

        public Nullable<DateTime> GetLastModifiedDateForActiveRelease(Guid organizationId)
        {
            return m_repository.GetLastModifiedDateForActiveRelease(organizationId);
        }

        public BasicConfigData LoadDraft(Guid organizationId)
        {
            return m_repository.LoadDraft(organizationId);
        }

        public FilterSet LoadFilters(Guid organizationId)
        {
            return m_repository.LoadFilters(organizationId);
        }

        public ReleaseConfigData LoadActiveRelease(Guid organizationId)
        {
            return m_repository.LoadActiveRelease(organizationId);
        }

        public long SaveReleaseConfig(Guid organizationId, SystemConfig config, Guid userId)
        {

            SystemConfigValidator validator = new SystemConfigValidator(organizationId, config);
            if (false == validator.IsValid())
            {
                throw new ConfigValidationException(validator);
            }
            long configId = m_repository.SaveReleaseConfig(organizationId, config, userId);

            return configId;
        }

        public void SaveFilters(Guid organizationId, FilterSet filters)
        {
            m_repository.SaveFilters(organizationId, filters);
        }

        public void SaveDraftConfig(Guid organizationId, SystemConfig config)
        {
            if (organizationId != new Guid("11111111-1111-1111-1111-111111111111"))
            {
                SystemConfigValidator validator = new SystemConfigValidator(organizationId, config);
                if (false == validator.IsValid())
                {
                    throw new ConfigValidationException(validator);
                }
            }
   
            m_repository.SaveDraftConfig(organizationId, config);
        }

        public IEnumerable<ReleaseConfigData> GetActiveReleases()
        {
            return m_repository.GetActiveReleases();
        }

        public IEnumerable<BasicConfigData> GetDrafts()
        {
            return m_repository.GetDrafts();
        }

        public IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid organizationId)
        {
            return m_repository.GetAllReleasesFor(organizationId);
        }

        public long GetConfigVersion(Guid organizationId)
        {
            return m_repository.GetConfigVersion(organizationId);
        }

        public long? GetActiveReleaseId(Guid brokerId)
        {
            return m_repository.GetActiveReleaseId(brokerId);
        }
        #endregion
    }

}
