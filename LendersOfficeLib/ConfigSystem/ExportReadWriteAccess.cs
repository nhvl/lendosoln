﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ClosedXML.Excel;
    using global::ConfigSystem;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using Operations;

    /// <summary>
    /// Provides a way to export simple read/write data for sending to clients.
    /// </summary>
    public class ExportReadWriteAccess
    {
        /// <summary>
        /// Requested Order for statuses, as specified by Walter in Case 147799.
        /// </summary>
        private static readonly ReadOnlyCollection<E_sStatusT> RequestedStatusOrder = new ReadOnlyCollection<E_sStatusT>(
            new E_sStatusT[]
        {
            E_sStatusT.Lead_New,
            E_sStatusT.Lead_Canceled,
            E_sStatusT.Lead_Declined,
            E_sStatusT.Lead_Other,

            E_sStatusT.Loan_Open,
            E_sStatusT.Loan_Prequal,
            E_sStatusT.Loan_Registered,
            E_sStatusT.Loan_PreProcessing,
            E_sStatusT.Loan_Processing,
            E_sStatusT.Loan_DocumentCheck,
            E_sStatusT.Loan_DocumentCheckFailed,
            E_sStatusT.Loan_LoanSubmitted,
            E_sStatusT.Loan_PreUnderwriting,
            E_sStatusT.Loan_Underwriting,
            E_sStatusT.Loan_Preapproval,
            E_sStatusT.Loan_Approved,
            E_sStatusT.Loan_ConditionReview,
            E_sStatusT.Loan_FinalUnderwriting,
            E_sStatusT.Loan_PreDocQC,
            E_sStatusT.Loan_ClearToClose,
            E_sStatusT.Loan_DocsOrdered,
            E_sStatusT.Loan_DocsDrawn,
            E_sStatusT.Loan_Docs,
            E_sStatusT.Loan_DocsBack,
            E_sStatusT.Loan_FundingConditions,
            E_sStatusT.Loan_Funded,
            E_sStatusT.Loan_Recorded,
            E_sStatusT.Loan_FinalDocs,
            E_sStatusT.Loan_Closed,

            E_sStatusT.Loan_SubmittedForPurchaseReview,
            E_sStatusT.Loan_InPurchaseReview,
            E_sStatusT.Loan_PrePurchaseConditions,
            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
            E_sStatusT.Loan_InFinalPurchaseReview,
            E_sStatusT.Loan_ClearToPurchase,
            E_sStatusT.Loan_Purchased,

            E_sStatusT.Loan_ReadyForSale,
            E_sStatusT.Loan_Shipped,
            E_sStatusT.Loan_InvestorConditions,
            E_sStatusT.Loan_InvestorConditionsSent,
            E_sStatusT.Loan_LoanPurchased,

            E_sStatusT.Loan_CounterOffer,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_Canceled,
            E_sStatusT.Loan_Suspended,
            E_sStatusT.Loan_Rejected,
            E_sStatusT.Loan_Withdrawn,
            E_sStatusT.Loan_Archived,

            E_sStatusT.Loan_Other,
            E_sStatusT.Loan_WebConsumer,
        });

        /// <summary>
        /// Requested Order for roles, as specified by Walter in Case 147799.
        /// </summary>
        private static readonly ReadOnlyCollection<E_RoleT> RequestedRoleOrder = new ReadOnlyCollection<E_RoleT>(
            new E_RoleT[]
        {
            E_RoleT.Administrator,
            E_RoleT.Manager,
            E_RoleT.DisclosureDesk,
            E_RoleT.CallCenterAgent,
            E_RoleT.LoanOfficer,
            E_RoleT.LoanOfficerAssistant,
            E_RoleT.LenderAccountExecutive,
            E_RoleT.LoanOpener,
            E_RoleT.JuniorProcessor,
            E_RoleT.Processor,
            E_RoleT.JuniorUnderwriter,
            E_RoleT.Underwriter,
            E_RoleT.QCCompliance,
            E_RoleT.DocDrawer,
            E_RoleT.Closer,
            E_RoleT.Funder,
            E_RoleT.Shipper,
            E_RoleT.CollateralAgent,
            E_RoleT.Insuring,
            E_RoleT.Servicing,
            E_RoleT.PostCloser,
            E_RoleT.LockDesk,
            E_RoleT.Secondary,
            E_RoleT.CreditAuditor,
            E_RoleT.LegalAuditor,
            E_RoleT.Purchaser,
            E_RoleT.Accountant,
            E_RoleT.RealEstateAgent,
            E_RoleT.Consumer,

            E_RoleT.Pml_LoanOfficer,
            E_RoleT.Pml_BrokerProcessor,
            E_RoleT.Pml_Secondary,
            E_RoleT.Pml_PostCloser,
            E_RoleT.Pml_Administrator,
        });

        /// <summary>
        /// The level of access to a loan file.
        /// </summary>
        private enum AccessLevel
        {
            /// <summary>
            /// The user should have no access to the loan file.
            /// </summary>
            None = 0,

            /// <summary>
            /// The user should have read only access to the loan file.
            /// </summary>
            Read = 1,

            /// <summary>
            /// The user should have read and write access to the loan file.
            /// </summary>
            Write = 2
        }

        /// <summary>
        /// Exports Read/Write access data to an Excel file.
        /// </summary>
        /// <param name="filePath">The file name where the Excel file will be saved.</param>
        /// <param name="brokerDB">The lender settings of the lender needing export.</param>
        /// <param name="workflowConfig">The workflow config to export.</param>
        public static void ExportToExcel(string filePath, BrokerDB brokerDB, SystemConfig workflowConfig)
        {
            Func<E_sStatusT, E_RoleT, AccessLevel> accessLevelLookup = ReadWriteAccessEvaluator.RetrieveGeneralAccessLevelByStatusAndRole(workflowConfig.Conditions);

            XLWorkbook workbook = new XLWorkbook();
            WriteStatusRoleAccessToWorksheet(workbook.Worksheets.Add("Read-Write by Role and Status"), accessLevelLookup, brokerDB);
            workbook.SaveAs(filePath);
        }

        /// <summary>
        /// Writes the specified access levels per role to a worksheet in an Excel file.
        /// </summary>
        /// <param name="worksheet">The worksheet to which the data is written.</param>
        /// <param name="accessLevelLookup">A mapping of statuses and roles to access levels.</param>
        /// <param name="brokerDB">The lender settings of the lender needing export.</param>
        private static void WriteStatusRoleAccessToWorksheet(IXLWorksheet worksheet, Func<E_sStatusT, E_RoleT, AccessLevel> accessLevelLookup, BrokerDB brokerDB)
        {
            var channels = new ReadOnlyCollection<ChannelInformation>(
                new ChannelInformation[]
            {
                new ChannelInformation(E_BranchChannelT.Retail, comment: "Retail Channel:" + Environment.NewLine + "* You are the originator" + Environment.NewLine + "* You are the lender"),
                new ChannelInformation(E_BranchChannelT.Broker, comment: "Broker channel: You are brokering out a loan to another investor." + Environment.NewLine + "* You are the originator" + Environment.NewLine + "* You are not the lender"),
                new ChannelInformation(E_BranchChannelT.Wholesale, comment: "TPO Wholesale channel:" + Environment.NewLine + "* You are not the originator" + Environment.NewLine + "* You are the lender"),
                new ChannelInformation(E_BranchChannelT.Correspondent, correspondentProcess: E_sCorrespondentProcessT.MiniCorrespondent),
                new ChannelInformation(E_BranchChannelT.Correspondent, correspondentProcess: E_sCorrespondentProcessT.PriorApproved),
                new ChannelInformation(E_BranchChannelT.Correspondent, correspondentProcess: E_sCorrespondentProcessT.Delegated),
            });

            var noAccessKeyCell = WriteAccessKeyCellAndDescription(worksheet, 1, channels.Count, string.Empty, "= No read or write access", XLColor.FromArgb(242, 220, 219));
            var readOnlyAccessKeyCell = WriteAccessKeyCellAndDescription(worksheet, 2, channels.Count, "r", "= Has read-only access", XLColor.FromArgb(255, 255, 153), isItalic: true);
            var writeAccessKeyCell = WriteAccessKeyCellAndDescription(worksheet, 3, channels.Count, "w", "= Has write (editing) access", XLColor.FromArgb(216, 228, 188), isBold: true);
            Func<AccessLevel, IXLCell> accessLevelToKeyCell = access =>
                {
                    switch (access)
                    {
                        case AccessLevel.None:
                            return noAccessKeyCell;
                        case AccessLevel.Read:
                            return readOnlyAccessKeyCell;
                        case AccessLevel.Write:
                            return writeAccessKeyCell;
                        default:
                            throw new UnhandledEnumException(access);
                    }
                };

            var orderedRoles = RequestedRoleOrder.Union((E_RoleT[])Enum.GetValues(typeof(E_RoleT))).ToList();
            var orderedStatusesForLender = RequestedStatusOrder
                .Union((E_sStatusT[])Enum.GetValues(typeof(E_sStatusT)))
                .Where(status => Tools.IsStatusLead(status) || brokerDB.HasEnabledInAnyChannel(status))
                .ToList();

            const int HeaderStartRowIndex = 4;
            int headerEndRowIndex = WriteHeaderRows(worksheet, HeaderStartRowIndex, channels, orderedRoles);

            const string ChannelCheckValue = "x";
            const string ChannelUnCheckValue = "";
            int rowIndex = headerEndRowIndex;
            foreach (E_sStatusT status in orderedStatusesForLender)
            {
                rowIndex += 1;
                int columnIndex = 1;
                worksheet.Cell(rowIndex, columnIndex).SetValue(CPageBase.sStatusT_map_rep(status));
                foreach (var channelAndProcess in channels)
                {
                    columnIndex += 1;
                    worksheet.Cell(rowIndex, columnIndex).SetValue(
                        Tools.IsStatusLead(status) || brokerDB.GetEnabledStatusesByChannelAndProcessType(channelAndProcess.Channel, channelAndProcess.CorrespondentProcess).Contains(status)
                        ? ChannelCheckValue
                        : ChannelUnCheckValue);
                }

                foreach (E_RoleT role in orderedRoles)
                {
                    columnIndex += 1;
                    worksheet.Cell(rowIndex, columnIndex).Value = accessLevelToKeyCell(accessLevelLookup(status, role)).Value;
                }
            }

            worksheet.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

            const int ChannelColumnOffset = 1;
            var channelStatusRange = worksheet.Range(headerEndRowIndex + 1, ChannelColumnOffset + 1, headerEndRowIndex + orderedStatusesForLender.Count, ChannelColumnOffset + channels.Count);
            channelStatusRange.AddConditionalFormat().WhenEquals(ChannelCheckValue).Fill.BackgroundColor = XLColor.FromArgb(220, 230, 241);
            channelStatusRange.AddConditionalFormat().WhenEquals("\"" + ChannelUnCheckValue + "\"").Fill.BackgroundColor = XLColor.FromArgb(128, 128, 128);
            channelStatusRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            int roleColumnOffset = ChannelColumnOffset + channels.Count;
            var roleStatusRange = worksheet.Range(headerEndRowIndex + 1, roleColumnOffset + 1, headerEndRowIndex + orderedStatusesForLender.Count, roleColumnOffset + orderedRoles.Count);
            roleStatusRange.AddConditionalFormat().WhenEquals("=" + ToAbsoluteA1Reference(noAccessKeyCell.Address)).Fill.SetBackgroundColor(noAccessKeyCell.Style.Fill.BackgroundColor);
            roleStatusRange.AddConditionalFormat().WhenEquals("=" + ToAbsoluteA1Reference(readOnlyAccessKeyCell.Address)).Fill.SetBackgroundColor(readOnlyAccessKeyCell.Style.Fill.BackgroundColor).Font.SetItalic();
            roleStatusRange.AddConditionalFormat().WhenEquals("=" + ToAbsoluteA1Reference(writeAccessKeyCell.Address)).Fill.SetBackgroundColor(writeAccessKeyCell.Style.Fill.BackgroundColor).Font.SetBold();
            roleStatusRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            worksheet.Columns(1, 1).AdjustToContents();
            worksheet.Columns(channelStatusRange.RangeAddress.FirstAddress.ColumnNumber, channelStatusRange.RangeAddress.LastAddress.ColumnNumber).AdjustToContents(10.0, 15.0);
            worksheet.Columns(roleStatusRange.RangeAddress.FirstAddress.ColumnNumber, roleStatusRange.RangeAddress.LastAddress.ColumnNumber).AdjustToContents(12.75, 15.0).Style.Alignment.WrapText = true;
            worksheet.SheetView.FreezeRows(headerEndRowIndex);
            worksheet.SheetView.FreezeColumns(roleStatusRange.RangeAddress.FirstAddress.ColumnNumber - 1);
        }

        /// <summary>
        /// Writes the cells for the key of access level values.
        /// </summary>
        /// <param name="worksheet">The worksheet to write the cell.</param>
        /// <param name="rowIndex">The index of the row to write the key and description.</param>
        /// <param name="lastColumnOfDescription">The final column used by the description.</param>
        /// <param name="keyValue">The value of the key cell.</param>
        /// <param name="descriptionValue">The description of the meaning of <paramref name="keyValue"/>.</param>
        /// <param name="color">The color used to represent this value.</param>
        /// <param name="isItalic">Indicates whether or not the value should be italicized.</param>
        /// <param name="isBold">Indicates whether or not the value should be bolded.</param>
        /// <returns>The key cell for the specified access level.</returns>
        private static IXLCell WriteAccessKeyCellAndDescription(IXLWorksheet worksheet, int rowIndex, int lastColumnOfDescription, string keyValue, string descriptionValue, XLColor color, bool isItalic = false, bool isBold = false)
        {
            const int StartingColumnIndex = 1;
            var accessKeyCell = worksheet.Cell(rowIndex, StartingColumnIndex);
            accessKeyCell.SetValue(keyValue).Style
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Fill.SetBackgroundColor(color);
            if (isItalic)
            {
                accessKeyCell.Style.Font.SetItalic();
            }

            if (isBold)
            {
                accessKeyCell.Style.Font.SetBold();
            }

            worksheet.Range(rowIndex, StartingColumnIndex + 1, rowIndex, StartingColumnIndex + lastColumnOfDescription)
                .Merge()
                .SetValue(descriptionValue)
                .Style.Fill.SetBackgroundColor(color);

            return accessKeyCell;
        }

        /// <summary>
        /// Writes the header rows for the data to the <paramref name="worksheet"/>, returning
        /// the index of the last row used.
        /// </summary>
        /// <param name="worksheet">The worksheet to write the data to.</param>
        /// <param name="rowIndex">The index of the of the row where the header will begin.</param>
        /// <param name="channels">The channels to include in the header.</param>
        /// <param name="roles">The roles to include in the header.</param>
        /// <returns>The index of the last row in the header.</returns>
        private static int WriteHeaderRows(IXLWorksheet worksheet, int rowIndex, IList<ChannelInformation> channels, IList<E_RoleT> roles)
        {
            int columnIndex = 1;
            worksheet.Range(rowIndex, columnIndex, rowIndex + 1, columnIndex).Merge().SetValue("Loan Status");

            for (int i = 0; i < channels.Count; ++i)
            {
                columnIndex += 1;
                WriteChannelHeaderCell(
                    worksheet.Cell(rowIndex, columnIndex),
                    worksheet.Cell(rowIndex + 1, columnIndex),
                    channels[i]);
            }

            for (int i = 0; i < roles.Count; ++i)
            {
                columnIndex += 1;
                worksheet.Range(rowIndex, columnIndex, rowIndex + 1, columnIndex).Merge().SetValue(Role.GetRoleDescription(roles[i]));
            }

            worksheet.Rows(rowIndex, rowIndex + 1).Style.Font.SetBold().Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            return rowIndex + 1;
        }

        /// <summary>
        /// Writes the header cells for a particular channel and process.
        /// </summary>
        /// <param name="channelCell">The cell to fill with the channel.</param>
        /// <param name="processCell">The cell to fill with the correspondent process.</param>
        /// <param name="channelDetails">The channel information to insert into the cells.</param>
        private static void WriteChannelHeaderCell(IXLCell channelCell, IXLCell processCell, ChannelInformation channelDetails)
        {
            channelCell.SetValue(Tools.GetDescription(channelDetails.Channel));
            processCell.SetValue(Tools.GetDescription(channelDetails.CorrespondentProcess));
            if (!string.IsNullOrEmpty(channelDetails.Comment))
            {
                channelCell.Comment.AddText(channelDetails.Comment);
            }

            IXLRange range = channelCell.Worksheet.Range(channelCell, processCell);
            range.Style.Fill.SetBackgroundColor(XLColor.FromArgb(184, 204, 228));
            if (channelDetails.CorrespondentProcess == E_sCorrespondentProcessT.Blank)
            {
                range.Merge();
            }
            else
            {
                MergePrecedingIdenticalCellsInRow(channelCell);
            }
        }

        /// <summary>
        /// Iterates through the immediately preceding cells in the row and merges
        /// all cells with values identical to <paramref name="cell"/>.
        /// </summary>
        /// <param name="cell">The cell to start with.</param>
        private static void MergePrecedingIdenticalCellsInRow(IXLCell cell)
        {
            IXLCell currentPrecedingCell = cell;
            bool cellsAreEqual = true;
            while (cellsAreEqual && currentPrecedingCell.Address.ColumnNumber > 1)
            {
                IXLCell candidateCell = cell.Worksheet.Cell(cell.Address.RowNumber, currentPrecedingCell.Address.ColumnNumber - 1);
                cellsAreEqual = candidateCell.Value == cell.Value;
                if (cellsAreEqual)
                {
                    currentPrecedingCell = candidateCell;
                }
            }

            cell.Worksheet.Range(currentPrecedingCell, cell).Merge();
        }

        /// <summary>
        /// Converts an address into an Excel absolute reference.
        /// It is not clear why, but Excel seems to like this better.
        /// </summary>
        /// <example>For the address at "A1", this would be "$A$1".</example>
        /// <param name="address">The address to look up.</param>
        /// <returns>The address in A1 format.</returns>
        private static string ToAbsoluteA1Reference(IXLAddress address)
        {
            return "$" + address.ColumnLetter + "$" + address.RowNumber;
        }

        /// <summary>
        /// Evaluates the existence of read/write access under the simplest of conditions.
        /// </summary>
        private class ReadWriteAccessEvaluator
        {
            /// <summary>
            /// Determines the access level available for statuses and roles, ignoring any other parameters.
            /// </summary>
            /// <param name="conditions">The set of conditions to determine the access levels for.</param>
            /// <returns>A function mapping a given status and role to a particular access level.</returns>
            public static Func<E_sStatusT, E_RoleT, AccessLevel> RetrieveGeneralAccessLevelByStatusAndRole(ConditionSet conditions)
            {
                Dictionary<E_sStatusT, int> statusToIndex = GetDictionaryToIndices<E_sStatusT>();
                Dictionary<E_RoleT, int> roleToIndex = GetDictionaryToIndices<E_RoleT>();
                AccessLevel[,] resultsByStatusRole = new AccessLevel[statusToIndex.Count, roleToIndex.Count];

                foreach (var condition in conditions)
                {
                    bool specifiesReadPermission = condition.SysOpSet.Any(operation => operation.Name == WorkflowOperations.ReadLoan.Id);
                    bool specifiesWritePermission = condition.SysOpSet.Any(operation => operation.Name == WorkflowOperations.WriteLoan.Id);

                    if ((!specifiesWritePermission && !specifiesReadPermission) || ParametersDoNotApplyToNormalLoans(condition) || condition.ExcludeFromRWExport)
                    {
                        continue;
                    }

                    var permissionSpecified = specifiesWritePermission ? AccessLevel.Write : AccessLevel.Read;

                    var statusParaExist = condition.ParameterSet.SingleOrDefault(p => p.Name == "sStatusT") != null;
                    var statuses = statusParaExist ? GetEnumParameterValues<E_sStatusT>(condition, "sStatusT") 
                                                   : GetEnumParameterValues<E_sStatusT>(condition, "sStatusProgressT");
                    var roles = GetEnumParameterValues<E_RoleT>(condition, "Role");

                    foreach (var status in statuses)
                    {
                        foreach (var role in roles)
                        {
                            AccessLevel current = resultsByStatusRole[statusToIndex[status], roleToIndex[role]];
                            if (current != AccessLevel.Write)
                            {
                                resultsByStatusRole[statusToIndex[status], roleToIndex[role]] = permissionSpecified;
                            }
                        }
                    }
                }

                return (status, role) => resultsByStatusRole[statusToIndex[status], roleToIndex[role]];
            }

            /// <summary>
            /// For an enum specified by <typeparamref name="TEnum"/>, computes a one-to-one mapping of
            /// each value to 0 through N-1, where N is the number of enum values.
            /// </summary>
            /// <typeparam name="TEnum">A enum type.</typeparam>
            /// <returns>A mapping between the enum and indices in an array of the values.</returns>
            private static Dictionary<TEnum, int> GetDictionaryToIndices<TEnum>() where TEnum : struct
            {
                TEnum[] values = (TEnum[])Enum.GetValues(typeof(TEnum));
                return values
                    .Select((value, index) => new { value, index })
                    .ToDictionary(indexedValue => indexedValue.value, indexedValue => indexedValue.index);
            }

            /// <summary>
            /// Gets the possible values of an enum specified in the parameters for a particular condition.
            /// </summary>
            /// <typeparam name="TEnum">The type of enum to look for.</typeparam>
            /// <param name="condition">The condition to look through.</param>
            /// <param name="parameterName">The name of the parameter to the condition.</param>
            /// <returns>An enumeration of the values of the enum.</returns>
            private static IEnumerable<TEnum> GetEnumParameterValues<TEnum>(AbstractConditionGroup condition, string parameterName) where TEnum : struct
            {
                var parameter = condition.ParameterSet.SingleOrDefault(p => p.Name == parameterName);
                if (parameter == null)
                {
                    return (TEnum[])Enum.GetValues(typeof(TEnum)); // if parameter is omitted, any value is permissable
                }
                else
                {
                    return parameter.Values.Select(value => (TEnum)Enum.Parse(typeof(TEnum), value));
                }
            }

            /// <summary>
            /// Determines in the simplest way possible if the condition applies to a normal loan file.
            /// (Checking if the rule only applies to templates or deleted loans).
            /// </summary>
            /// <param name="condition">The condition to check for abnormal loans.</param>
            /// <returns><see langword="true"/> if the condition would not apply to a normal loan file, false otherwise.</returns>
            private static bool ParametersDoNotApplyToNormalLoans(AbstractConditionGroup condition)
            {
                // Ignore Templates
                var parameterIsTemplate = condition.ParameterSet.FirstOrDefault(p => p.Name == "IsTemplate" && p.ValueType == E_ParameterValueType.Bool && p.FunctionT == E_FunctionT.Equal);
                if (parameterIsTemplate != null && !parameterIsTemplate.Values.Contains(bool.FalseString, StringComparer.OrdinalIgnoreCase))
                {
                    return true;
                }

                // Ignore deleted loans
                var parameterIsValidLoan = condition.ParameterSet.FirstOrDefault(p => p.Name == "IsValid" && p.ValueType == E_ParameterValueType.Bool && p.FunctionT == E_FunctionT.Equal);
                if (parameterIsValidLoan != null && !parameterIsValidLoan.Values.Contains(bool.TrueString, StringComparer.OrdinalIgnoreCase))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Represents a joined channel and process, together with an optional comment of explanation.
        /// </summary>
        private class ChannelInformation
        {
            /// <summary>
            /// The channel.
            /// </summary>
            public readonly E_BranchChannelT Channel;

            /// <summary>
            /// The correspondent process.
            /// </summary>
            public readonly E_sCorrespondentProcessT CorrespondentProcess;

            /// <summary>
            /// The comment about the channel and process.
            /// </summary>
            public readonly string Comment;

            /// <summary>
            /// Initializes a new instance of the <see cref="ChannelInformation"/> class.
            /// </summary>
            /// <param name="channel">The channel of this instance.</param>
            /// <param name="correspondentProcess">The process of the channel of this instance.</param>
            /// <param name="comment">A comment about the channel and process of this instance.</param>
            public ChannelInformation(E_BranchChannelT channel, E_sCorrespondentProcessT correspondentProcess = E_sCorrespondentProcessT.Blank, string comment = "")
            {
                this.Channel = channel;
                this.CorrespondentProcess = correspondentProcess;
                this.Comment = comment;
            }
        }
    }
}
