﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;

    using CommonProjectLib.ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes.PathDispatch;
    using OperationResultDictionary = System.Collections.Generic.Dictionary<LendersOffice.ConfigSystem.Operations.WorkflowOperation, global::ConfigSystem.Engine.ExecutingEngineResultType>;
    using CommonProjectLib.Caching;

    public class LendingQBExecutingEngine
    {

        static LendingQBExecutingEngine()
        {
            InitializeLoanFileCacheDependencySet();

        }

        private static void InitializeLoanFileCacheDependencySet()
        {

            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);
            #region Dependency Set for fields do not map one-to-one to datalayer field name.
            set.Add("IsPmlExternalAssigned", new string[] { "IsPmlUserManagerOfLoan", "IsUserIsInSamePmlBroker" });
            set.Add("IsExtAssigned", new string[] { "IsPmlExternalAssigned" });
            set.Add("issamebranchasloanfile", new string[] { "sBranchId" });
            set.Add("ispmlassigned", new string[] { "IsUserIsInSamePmlBroker", "GetAssignmentFor" });
            set.Add("isassigned", new string[] { "GetAssignmentFor" });
            set.Add("haveunderwriter", new string[] { "HaveAssignmentFor" });
            set.Add("haveprocessor", new string[] { "haveprocessor" });
            set.Add("IsPmlUserManagerOfLoan", new string[] { "PmlExternalBrokerProcessorManagerEmployeeId", "PmlExternalManagerEmployeeId", "PmlExternalSecondaryManagerEmployeeId", "PmlExternalPostCloserManagerEmployeeId" });
            set.Add("IsUserIsInSamePmlBroker", new string[] { "sPmlBrokerId" });
            set.Add("GetAssignmentFor", new string[] { "GetRoleAssignmentCacheFieldId" });
            set.Add("GetRoleAssignmentCacheFieldId", new string[] { "sEmployeeCallCenterAgentId", "sEmployeeCloserId", 
                "sEmployeeLenderAccExecId", "sEmployeeLoanRepId", "sEmployeeLoanOpenerId", "sEmployeeLockDeskId", 
                "sEmployeeManagerId", "sEmployeeProcessorId", "sEmployeeRealEstateAgentId", "sEmployeeUnderwriterId",
                "sEmployeeLoanRepId", "sEmployeeBrokerProcessorId", "sEmployeeShipperId", "sEmployeeFunderId",
                "sEmployeePostCloserId", "sEmployeeInsuringId", "sEmployeeCollateralAgentId", "sEmployeeDocDrawerId",
                "sEmployeeCreditAuditorId", "sEmployeeDisclosureDeskId", "sEmployeeJuniorProcessorId", 
                "sEmployeeJuniorUnderwriterId", "sEmployeeLegalAuditorId", "sEmployeeLoanOfficerAssistantId",
                "sEmployeePurchaserId", "sEmployeeQCComplianceId", "sEmployeeSecondaryId", "sEmployeeServicingId",
                "sEmployeeExternalSecondaryId", "sEmployeeExternalPostCloserId" });
            set.Add("actasratelocked", new string[] { "sRateLockStatusT", "sNoteIRSubmitted", "sStatusT" });
            set.Add("Lien", new string[] { "sLienPosT", "sLId" });
            set.Add("Scope", new string[] { "sBrokerId", "sBranchId", "GetAssignmentFor", "IsTeamAssigned", "IsPmlUserManagerOfLoan", "IsUserIsInSamePmlBroker" });
            set.Add("IsTeamAssigned", new string[] { "sTeamCallCenterAgentId", "sTeamCloserId", 
                "sTeamLenderAccExecId", "sTeamLoanRepId", "sTeamLoanOpenerId", "sTeamLockDeskId", 
                "sTeamManagerId", "sTeamProcessorId", "sTeamRealEstateAgentId", "sTeamUnderwriterId",
                "sTeamLoanRepId", "sTeamShipperId", "sTeamFunderId",
                "sTeamPostCloserId", "sTeamInsuringId", "sTeamCollateralAgentId", "sTeamDocDrawerId",
                "sTeamCreditAuditorId", "sTeamDisclosureDeskId", "sTeamJuniorProcessorId", 
                "sTeamJuniorUnderwriterId", "sTeamLegalAuditorId", "sTeamLoanOfficerAssistantId",
                "sTeamPurchaserId", "sTeamQCComplianceId", "sTeamSecondaryId", "sTeamServicingId" });
            set.Add("GetAssignAsRole", new string[] { "GetAssignmentFor" });
            set.Add("sLpTemplateIdNonEmpty", new string[] { "sLpTemplateId" });
            set.Add("AreAllConditionsClosed", new string[] { "sfGetDistinctConditionCategoryWhereAllConditionsAreClosed" });
            set.Add("DoesConditionExist", new string[] { "sfGetDistinctConditionCategory" });
            set.Add("OCGroup", new string[] { "sPmlBrokerId" });
            set.Add("BranchGroup", new string[] { "sBranchId" });
            set.Add("DoesActiveConditionExist", new string[] { "sfGetDistinctConditionCategoryWithActiveCondition" });
            s_loanFileCacheDependencySet = set;
            #endregion

            #region Dependency set for operation
            s_operationDependencySet = GetOperationDependencySet();
            #endregion
        }

        public static DependencySet<WorkflowOperation> GetOperationDependencySet()
        {
            var set = new DependencySet<WorkflowOperation>();

            foreach (var operation in WorkflowOperations.GetAll())
            {
                set.Add(operation, operation.GetOperationDependencies());
            }

            return set;
        }

        private static DependencySet<string> s_loanFileCacheDependencySet;
        private static DependencySet<WorkflowOperation> s_operationDependencySet;

        public static bool GetResult(WorkflowOperation operation, OperationResultDictionary dictionary)
        {
            bool result = operation.GetResult(dictionary);
            return result;
        }

        public static ExecutingEngine SystemExecutingEngine
        {
            get
            {
                var cacheKey = ExecutingEngine.CacheKeyPrefix + ConstAppDavid.SystemBrokerGuid;
                return MemoryCacheUtilities.GetOrAddExisting(
                    cacheKey,
                    () => GetEngineByBrokerId(ConstAppDavid.SystemBrokerGuid),
                    ExecutingEngine.EngineCacheDuration);
            }
        }

        public static string GetUserFriendlyMessage(WorkflowOperation operation, LoanValueEvaluator valueEvaluator)
        {
            var o = MasterDebugOperation(operation, valueEvaluator);
            if (o.CanPerform)
            {
                return string.Empty; // 10/5/2010 dd - Do not return message if user can perform operation.
            }
            else
            {
                ExecutionEngineDebugMessage msg = new ExecutionEngineDebugMessage(valueEvaluator.CurrentPrincipal.BrokerId, o);
                return msg.ToString();
            }
        }

        public static IReadOnlyCollection<string> GetUserFriendlyMessageList(WorkflowOperation operation, LoanValueEvaluator valueEvaluator)
        {
            var o = MasterDebugOperation(operation, valueEvaluator);
            if (o.CanPerform)
            {
                return new List<string>(); // 10/5/2010 dd - Do not return message if user can perform operation.
            }
            else
            {
                ExecutionEngineDebugMessage msg = new ExecutionEngineDebugMessage(valueEvaluator.CurrentPrincipal.BrokerId, o);
                return msg.UserFriendlyMessages;
            }
        }

        public static ExecutionEngineDebugMessage GetExecutionEngineDebugMessage(WorkflowOperation operation, LoanValueEvaluator valueEvaluator)
        {
            var o = MasterDebugOperation(operation, valueEvaluator);
            if (o.CanPerform)
            {
                return null; // 10/5/2010 dd - Do not return message if user can perform operation.
            }
            else
            {
                return new ExecutionEngineDebugMessage(valueEvaluator.CurrentPrincipal.BrokerId, o);
            }
        }

        public static LendingQBExecutingEngineDebug MasterDebugOperation(WorkflowOperation operation, LoanValueEvaluator valueEvaluator, bool debugTriggerEnable = false)
        {
            string description; // HardCode description for now.

            WorkflowOperation[] operationRequiresRead = 
            { 
                WorkflowOperations.WriteLoan, 
                WorkflowOperations.RunPmlForAllLoans, 
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.OrderInitialDisclosureInTpoPortal,
                WorkflowOperations.RequestRedisclosureInTpoPortal,
                WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal,
                WorkflowOperations.LeadToLoanConversion
            };

            if (operationRequiresRead.Contains(operation))
            {
                description = operation + " REQUIRES " + WorkflowOperations.ReadLoan.ToString();
            }
            else
            {
                description = operation.ToString();
            }

            LendingQBExecutingEngineDebug lendingQbDebug = new LendingQBExecutingEngineDebug(operation, description);

            ExecutingEngine brokerEngine = GetEngineByBrokerId(valueEvaluator.CurrentPrincipal.BrokerId);
            IEnumerable<WorkflowOperation> operationList = s_operationDependencySet.GetDependsOn(new[] { operation });

            Dictionary<WorkflowOperation, ExecutingEngine> restraintEngineMap = new Dictionary<WorkflowOperation, ExecutingEngine>();
            Dictionary<WorkflowOperation, ExecutingEngine> privilegeEngineMap = new Dictionary<WorkflowOperation, ExecutingEngine>();

            foreach (var op in operationList)
            {
                if (brokerEngine != null && brokerEngine.HasRestraint(op))
                {
                    restraintEngineMap.Add(op, brokerEngine);
                }
                else if (SystemExecutingEngine.HasRestraint(op))
                {
                    restraintEngineMap.Add(op, SystemExecutingEngine);
                }

                if (brokerEngine != null && brokerEngine.HasPrivilege(op))
                {
                    privilegeEngineMap.Add(op, brokerEngine);
                }
                else if (SystemExecutingEngine.HasPrivilege(op))
                {
                    privilegeEngineMap.Add(op, SystemExecutingEngine);
                }
            }

            // NOTE: This only thrown if NONE of the dependent ops are have privileges defined.
            // It will continue evaluation if only some of the ops have no privileges defined
            if (privilegeEngineMap.Count == 0)
            {
                throw new GenericUserErrorMessageException(operation + " is not a valid operation.");
            }

            // 12/2/2010 dd - Based on Brian's spec, deploy specialist (DS) can only define either OPERATION_RUN_PML_ALL_LOANS or OPERATION_RUN_PML_REGISTERED_LOANS
            // Therefore return FALSE for a missing operation from lender workflow configuration.  If both operations are missing from lender then use default configuration
            // from system.
            if (brokerEngine != null)
            {
                if (operation == WorkflowOperations.RunPmlForAllLoans
                    || operation == WorkflowOperations.RunPmlForRegisteredLoans)
                {
                    if (brokerEngine.HasPrivilege(WorkflowOperations.RunPmlForAllLoans) ||
                        brokerEngine.HasPrivilege(WorkflowOperations.RunPmlForRegisteredLoans))
                    {
                        if (brokerEngine.HasPrivilege(operation) == false)
                        {
                            // Lender does not define this operation but define other operation. Therefore return false.
                            lendingQbDebug.CanPerform = false;
                            return lendingQbDebug;
                        }
                    }
                }
            }

            var dictionary = new Dictionary<WorkflowOperation, ExecutingEngineResultType>();

            foreach (var op in operationList)
            {
                ExecutingEngineDebug engineDebug = DebugOperation(restraintEngineMap.GetValueOrNull(op), privilegeEngineMap.GetValueOrNull(op), valueEvaluator, op, debugTriggerEnable);
                dictionary.Add(op, engineDebug.CanPerform ? ExecutingEngineResultType.Allow : ExecutingEngineResultType.NotAllow);
                lendingQbDebug.Add(engineDebug);
            }

            lendingQbDebug.CanPerform = GetResult(operation, dictionary);

            return lendingQbDebug;
        }

        private static ExecutingEngineDebug DebugOperation(ExecutingEngine restraintEngine, ExecutingEngine privilegeEngine, IEngineValueEvaluator ticket, 
                                                           ISystemOperationIdentifier operation, bool triggerDebugEnable=false)
        {
            if (ticket == null || operation == null)
            {
                throw new ArgumentNullException();
            }

            ExecutingEngineDebug debug = new ExecutingEngineDebug(operation.Id);
            debug.CanPerform = true;
            Dictionary<string, TriggerDebug> restraintTriggerDict = null, privilegeTriggerDict = null;
            if(triggerDebugEnable)
            {
                restraintTriggerDict = new Dictionary<string, TriggerDebug>();
                privilegeTriggerDict = new Dictionary<string, TriggerDebug>();
            }
                
            if (restraintEngine != null && restraintEngine.HasPassingRestraint(operation, ticket, debug, restraintTriggerDict))
            {
                debug.CanPerform = false;
            }

            if (privilegeEngine == null)
            {
                debug.CanPerform = false;
            }
            else
            {
                debug.CanPerform &= privilegeEngine.HasPassingPrivilege(operation, ticket, debug, privilegeTriggerDict);
            }

            debug.SetTriggers(restraintTriggerDict, privilegeTriggerDict);

            return debug;
        }

        public static bool CanPerformAny(LoanValueEvaluator valueEvaluator, params WorkflowOperation[] operations)
        {
            return operations.Any(p => CanPerform(p, valueEvaluator));
        }

        /// <summary>
        /// Return a list of priviledge/condition id that evaluate to true. This method DOES NOT look up the operation dependency and evaluate
        /// all the depends operation. Nor does is evaluate Restraints. In other word, it will only evaluate Privilege rules for the given operation.
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="valueEvaluator"></param>
        /// <returns></returns>
        public static IEnumerable<Guid> GetAllPassingPrivilegeConditionIds(WorkflowOperation operation, LoanValueEvaluator valueEvaluator)
        {
            ExecutingEngine brokerEngine = GetEngineByBrokerId(valueEvaluator.CurrentPrincipal.BrokerId);
            bool hasLenderRules = brokerEngine != null && brokerEngine.HasPrivilege(operation);

            // Privileges should be defined at either the broker of system level. Throw if none found.
            if (!hasLenderRules && !SystemExecutingEngine.HasPrivilege(operation))
            {
                throw new GenericUserErrorMessageException(operation + " is not a valid operation.");
            }

            List<Guid> conditionList = new List<Guid>();

            if (hasLenderRules)
            {
                conditionList.AddRange(brokerEngine.GetAllPassingPrivilegeConditionIds(operation, valueEvaluator));
            }
            else
            {
                conditionList.AddRange(SystemExecutingEngine.GetAllPassingPrivilegeConditionIds(operation, valueEvaluator));
            }

            return conditionList;
        }

        /// <summary>
        /// Return a list of restraint condition IDs that evaluate to true. This method DOES NOT look up the operation dependency and evaluate
        /// all the depends operation. In other word, it will only evaluate Restraint rules for the given operation.
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="valueEvaluator"></param>
        /// <returns></returns>
        public static IEnumerable<Guid> GetAllPassingRestraintConditionIds(WorkflowOperation operation, LoanValueEvaluator valueEvaluator)
        {
            ExecutingEngine brokerEngine = GetEngineByBrokerId(valueEvaluator.CurrentPrincipal.BrokerId);
            bool hasLenderRules = brokerEngine != null && brokerEngine.HasRestraint(operation);

            // If no restraints are defined then return early.
            // NOTE: Default restraints are not required, so it is possible to have an op with no restraints defined.
            if (!hasLenderRules && !SystemExecutingEngine.HasRestraint(operation))
            {
                return Enumerable.Empty<Guid>();
            }

            List<Guid> conditionList = new List<Guid>();

            if (hasLenderRules)
            {
                conditionList.AddRange(brokerEngine.GetAllPassingRestraintConditionIds(operation, valueEvaluator));
            }
            else
            {
                conditionList.AddRange(SystemExecutingEngine.GetAllPassingRestraintConditionIds(operation, valueEvaluator));
            }

            return conditionList;
        }

        public static ExecutingEngine GetEngineByBrokerId(Guid brokerId)
        {
            // Retrieve from context to prevent multiple DB lookups in a single request.
            HttpContext context = HttpContext.Current;
            string cacheKey = ExecutingEngine.CacheKeyPrefix + brokerId.ToString("N");
            if (context != null && context.Items.Contains(cacheKey))
            {
                return context.Items[cacheKey] as ExecutingEngine;
            }

            IConfigRepository configRepository = ConfigHandler.GetRepository(brokerId);
            if (configRepository == null)
            {
                throw new GenericUserErrorMessageException("ConfigRepository is null for BrokerId=" + brokerId);
            }

            Stopwatch sw = Stopwatch.StartNew();
            var engine = ExecutingEngine.GetEngineByBrokerId(configRepository, brokerId);
            sw.Stop();

            PerformanceMonitorItem.AddTimingDetailsToCurrent("ExecutingEngine.GetEngineByBrokerId::" + brokerId, sw.ElapsedMilliseconds);

            // Cache to context.
            if (context != null)
            {
                context.Items[cacheKey] = engine;
            }

            return engine;
        }

        public static bool CanPerform(WorkflowOperation operation, LoanValueEvaluator valueEvaluator, ExecutingEngine engine = null)
        {
            if (valueEvaluator.GetBoolValues("IsTemplate")[0] && valueEvaluator.CurrentPrincipal != null)
            {
                bool canRead = valueEvaluator.CurrentPrincipal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch) ||
                    valueEvaluator.GetSafeScalarString("sBranchId", "N/A") == valueEvaluator.CurrentPrincipal.BranchId.ToString();
                bool canWrite = canRead && (valueEvaluator.CurrentPrincipal.HasPermission(Permission.CanEditLoanTemplates));

                if (operation == WorkflowOperations.ReadLoanOrTemplate
                    || operation == WorkflowOperations.ReadTemplate)
                {
                    return canRead;
                }
                else if (operation == WorkflowOperations.WriteLoanOrTemplate
                    || operation == WorkflowOperations.WriteTemplate
                    || operation == WorkflowOperations.DeleteLoan
                    || operation == WorkflowOperations.ClearRespaFirstEnteredDates)
                {
                    return canWrite;
                }
                else
                {
                    return false;
                }
            }

            ExecutingEngine brokerEngine = engine ?? GetEngineByBrokerId(valueEvaluator.CurrentPrincipal.BrokerId);
            IEnumerable<WorkflowOperation> operationList = s_operationDependencySet.GetDependsOn(new[] { operation });

            Dictionary<WorkflowOperation, ExecutingEngine> restraintEngineMap = new Dictionary<WorkflowOperation, ExecutingEngine>();
            Dictionary<WorkflowOperation, ExecutingEngine> privilegeEngineMap = new Dictionary<WorkflowOperation, ExecutingEngine>();

            foreach (var op in operationList)
            {
                if (brokerEngine != null && brokerEngine.HasRestraint(op))
                {
                    restraintEngineMap.Add(op, brokerEngine);
                }
                else if (SystemExecutingEngine.HasRestraint(op))
                {
                    restraintEngineMap.Add(op, SystemExecutingEngine);
                }

                if (brokerEngine != null && brokerEngine.HasPrivilege(op))
                {
                    privilegeEngineMap.Add(op, brokerEngine);
                }
                else if (SystemExecutingEngine.HasPrivilege(op))
                {
                    privilegeEngineMap.Add(op, SystemExecutingEngine);
                }
            }

            // NOTE: This only thrown if NONE of the dependent ops are have privileges defined.
            // It will continue evaluation if only some of the ops have no privileges defined
            if (privilegeEngineMap.Count == 0)
            {
                throw new GenericUserErrorMessageException(operation + " is not a valid operation.");
            }

            // 12/2/2010 dd - Based on Brian's spec, deploy specialist (DS) can only define either OPERATION_RUN_PML_ALL_LOANS or OPERATION_RUN_PML_REGISTERED_LOANS
            // Therefore return FALSE for a missing operation from lender workflow configuration.  If both operations are missing from lender then use default configuration
            // from system.
            if (brokerEngine != null)
            {
                if (operation == WorkflowOperations.RunPmlForAllLoans
                    || operation == WorkflowOperations.RunPmlForRegisteredLoans)
                {
                    if (brokerEngine.HasPrivilege(WorkflowOperations.RunPmlForAllLoans) ||
                        brokerEngine.HasPrivilege(WorkflowOperations.RunPmlForRegisteredLoans))
                    {
                        if (brokerEngine.HasPrivilege(operation) == false)
                        {
                            // Lender does not define this operation but define other operation. Therefore return false.
                            return false;
                        }
                    }
                }

                if (operation == WorkflowOperations.RequestRateLockCausesLock
                    && !brokerEngine.HasPrivilege(operation))
                {
                    //Lender doesn't define this operation and neither do we, so just return false 
                    //(e.g, no you can't automatically lock the rate on request)
                    return false;
                }
            }

            bool bResult = false;

            OperationResultDictionary dictionary = new OperationResultDictionary();
            foreach (var op in operationList)
            {
                ExecutingEngineResult result = CanPerform(restraintEngineMap.GetValueOrNull(op), privilegeEngineMap.GetValueOrNull(op), valueEvaluator, op);
                dictionary.Add(op, result.Result);
            }

            bResult = GetResult(operation, dictionary);

            return bResult;
        }

        private static ExecutingEngineResult CanPerform(ExecutingEngine restraintEngine, ExecutingEngine privilegeEngine, IEngineValueEvaluator ticket, ISystemOperationIdentifier operation)
        {
            if (ticket == null || operation == null)
            {
                throw new ArgumentNullException();
            }
            
            // Since there is no requirement to have any restraints defined, we only really care if a privilege is defined.
            if (privilegeEngine == null || !privilegeEngine.HasPrivilege(operation))
            {
                return new ExecutingEngineResult(operation.Id, ExecutingEngineResultType.OperationNotFound);
            }

            bool CanPerform = (restraintEngine == null || !restraintEngine.HasPassingRestraint(operation, ticket)) && privilegeEngine.HasPassingPrivilege(operation, ticket);
            ExecutingEngineResultType result = CanPerform ? ExecutingEngineResultType.Allow : ExecutingEngineResultType.NotAllow;

            return new ExecutingEngineResult(operation.Id, result);
        }

        private static IEnumerable<string> GetDependencyFieldsByOperationImpl(ExecutingEngine brokerEngine, params WorkflowOperation[] operationList)
        {
            var systemOperationList = new HashSet<WorkflowOperation>();
            var lenderOperationList = new HashSet<WorkflowOperation>();

            foreach (var op in operationList)
            {
                foreach (var o in s_operationDependencySet.GetDependsOn(new[] { op }))
                {
                    if (brokerEngine != null && brokerEngine.HasOperation(o))
                    {
                        lenderOperationList.Add(o);
                    }

                    if (SystemExecutingEngine.HasOperation(o))
                    {
                        systemOperationList.Add(o);
                    }
                }
            }

            if (systemOperationList.Count == 0 && lenderOperationList.Count == 0)
            {
                throw new GenericUserErrorMessageException("Invalid operation list.");
            }

            HashSet<string> resultSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            resultSet.Add("sLNm"); // 11/2/2010 dd - Always include sLNm for debugging purpose.
            resultSet.Add("sNoteIRSubmitted"); // 12/6/2010 dd - OPM 60336 - Temporary there are obsolete code that require this fields to be load.
            resultSet.Add("sStatusT"); // 1/16/2012 dd - We are include sStatusT in logging for loan editor page.
            resultSet.Add("sBranchId");
            resultSet.Add("IsTemplate");

            if (lenderOperationList.Count > 0)
            {
                foreach (var field in brokerEngine.GetDependencyVariableList(lenderOperationList))
                {
                    resultSet.Add(field);
                }
            }

            if (systemOperationList.Count > 0)
            {
                foreach (var field in SystemExecutingEngine.GetDependencyVariableList(systemOperationList))
                {
                    resultSet.Add(field);
                }

            }

            return resultSet;
        }

        public static IEnumerable<string> GetDependencyFieldsByOperation(Guid brokerId, params WorkflowOperation[] operationList)
        {
            if (brokerId == Guid.Empty)
            {
                throw new GenericUserErrorMessageException("BrokerId is Guid.Empty");
            }

            HashSet<string> actualFieldList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            CFieldDbInfoList fieldDbList = CFieldDbInfoList.TheOnlyInstance;

            ExecutingEngine engine = GetEngineByBrokerId(brokerId);
            IEnumerable<string> fieldList = GetDependencyFieldsByOperationImpl(engine, operationList);

            foreach (string field in fieldList)
            {
                if (field == null)
                {
                    // 09-21-2015 - This exception only happen when app pool just get recycle. 
                    // I added this throw to confirm this is where the NullReferenceException occur.
                    throw new GenericUserErrorMessageException("field cannot be null.");
                }

                if (field.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || field.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (field.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    actualFieldList.UnionWith(PathResolver.GetPathDependencyFields(field));
                    continue;
                }

                CFieldDbInfo fieldInfo = fieldDbList.Get(field);
                if (fieldInfo != null && fieldInfo.IsCachedField)
                {
                    actualFieldList.Add(field);
                }
                else if (PageDataUtilities.ContainsField(field))
                {
                    actualFieldList.Add(field);
                }
                else
                {
                    foreach (var o in s_loanFileCacheDependencySet.GetDependsOn(new string[] { field }))
                    {
                        fieldInfo = fieldDbList.Get(o);
                        if (fieldInfo != null && fieldInfo.IsCachedField)
                        {
                            actualFieldList.Add(o);
                        }
                        else if (PageDataUtilities.ContainsField(o))
                        {
                            actualFieldList.Add(o);
                        }
                        else if (o.StartsWith("sf") || o.StartsWith("af"))
                        {
                            actualFieldList.Add(o);
                            // 12/2/2011 dd - This will allow field name such as sfGetDistinctConditionCategory to
                            // include in dependency list.
                        }

                    }
                }
            }
            return actualFieldList;
        }

        public static IEnumerable<string> GetDependencyFieldsByTrigger(Guid brokerId, params string[] triggerList)
        {
            return GetDependencyFieldsByTrigger(brokerId, false, triggerList);
        }

        public static IEnumerable<string> GetDependencyFieldsByTrigger(Guid brokerId, bool includeDefaults, params string[] triggerList)
        {
            if (brokerId == Guid.Empty)
            {
                throw new GenericUserErrorMessageException("BrokerId is Guid.Empty");
            }

            ExecutingEngine engine = GetEngineByBrokerId(brokerId);
            IEnumerable<string> fieldList = GetDependencyFieldsByTriggerImpl(engine, includeDefaults, triggerList);

            HashSet<string> actualFieldList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            CFieldDbInfoList fieldDbList = CFieldDbInfoList.TheOnlyInstance;

            foreach (string field in fieldList)
            {
                if (field.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || field.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (field.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    actualFieldList.UnionWith(PathResolver.GetPathDependencyFields(field));
                    continue;
                }

                CFieldDbInfo fieldInfo = fieldDbList.Get(field);
                if (fieldInfo != null && fieldInfo.IsCachedField)
                {
                    actualFieldList.Add(field);
                }
                else if (PageDataUtilities.ContainsField(field))
                {
                    actualFieldList.Add(field);
                }
                else
                {
                    foreach (var o in s_loanFileCacheDependencySet.GetDependsOn(new string[] { field }))
                    {
                        fieldInfo = fieldDbList.Get(o);
                        if (fieldInfo != null && fieldInfo.IsCachedField)
                        {
                            actualFieldList.Add(o);
                        }
                        else if (PageDataUtilities.ContainsField(o))
                        {
                            actualFieldList.Add(o);
                        }
                        else if (o.StartsWith("sf") || o.StartsWith("af"))
                        {
                            actualFieldList.Add(o);
                            // 12/2/2011 dd - This will allow field name such as sfGetDistinctConditionCategory to
                            // include in dependency list.
                        }

                    }
                }
            }

            return actualFieldList;
        }

        private static IEnumerable<string> GetDependencyFieldsByTriggerImpl(ExecutingEngine engine, bool includeDefaults, params string[] triggerList)
        {
            HashSet<string> resultList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            if (includeDefaults)
            {
                resultList.Add("sLNm"); // 11/2/2010 dd - Always include sLNm for debugging purpose.
                resultList.Add("sNoteIRSubmitted"); // 12/6/2010 dd - OPM 60336 - Temporary there are obsolete code that require this fields to be load.
                resultList.Add("sStatusT"); // 1/16/2012 dd - We start to record sStatusT to our log request. Therefore always load status.
            }

            ExecutingEngine systemEngine = engine.SystemExecutingEngine;
            foreach (var trigger in triggerList)
            {
                if (engine.HasCustomVariable(trigger))
                {
                    foreach (var field in engine.GetDependencyVariableListForTrigger(trigger))
                    {
                        resultList.Add(field);
                    }
                }
                else if (systemEngine.HasCustomVariable(trigger))
                {
                    foreach (var field in systemEngine.GetDependencyVariableListForTrigger(trigger))
                    {
                        resultList.Add(field);
                    }
                }
            }

            return resultList;
        }

        public static bool EvaluateTrigger(string trigger, LoanValueEvaluator valueEvaluator)
        {
            ExecutingEngine engine = GetEngineByBrokerId(valueEvaluator.CurrentPrincipal.BrokerId);
            if (engine.HasCustomVariable(trigger))
            {
                return engine.EvaluateCustomVar(trigger, valueEvaluator);
            }
            else
            {
                return engine.SystemExecutingEngine.EvaluateCustomVar(trigger, valueEvaluator);
            }
        }
    }

    public static class LendingQBExecutingEngineHelper
    {
        public static bool IsAllow(this OperationResultDictionary dictionary, WorkflowOperation operation)
        {
            ExecutingEngineResultType operationResult;
            if (dictionary.TryGetValue(operation, out operationResult))
            {
                return operationResult == ExecutingEngineResultType.Allow;
            }
            throw new GenericUserErrorMessageException(operation + " is not define in the result dictionary.");
        }

        // For testing
        public static bool IsAllow(this Dictionary<string, ExecutingEngineResultType> dictionary, string operation)
        {
            ExecutingEngineResultType operationResult;
            if (dictionary.TryGetValue(operation, out operationResult))
            {
                return operationResult == ExecutingEngineResultType.Allow;
            }
            throw new GenericUserErrorMessageException(operation + " is not define in the result dictionary.");
        }
    }
}
