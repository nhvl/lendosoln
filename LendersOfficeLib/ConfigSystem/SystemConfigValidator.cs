﻿namespace LendersOffice.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    using global::ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Admin;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes.PathDispatch;

    public class SystemConfigValidator
    {
        public class TopologicalSort
        {
            private readonly int[] m_vertices;
            private readonly int[,] m_matrix;
            private int m_numberOfVertices;
            private readonly int[] _sortedArray;

            public TopologicalSort(int size)
            {
                m_vertices = new int[size];
                m_matrix = new int[size, size];
                m_numberOfVertices = 0;

                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                    {
                        m_matrix[i, j] = 0;
                    }

                _sortedArray = new int[size];
            }

            public int AddVertex(int vertex)
            {
                m_vertices[m_numberOfVertices++] = vertex;
                return m_numberOfVertices - 1;
            }

            public void AddEdge(int start, int end)
            {
                m_matrix[start, end] = 1;
            }

            public int[] Sort()
            {
                //StringBuilder sb = new StringBuilder();
                //for (int i = 0; i < m_numberOfVertices; i++)
                //{
                //    for (int j = 0; j < m_numberOfVertices; j++)
                //    {
                //        sb.Append(m_matrix[i, j] + " ");
                //    }
                //    sb.AppendLine();
                //}

                //Tools.LogError(sb.ToString());

                while (m_numberOfVertices > 0) // while vertices remain,
                {
                    // get a vertex with no successors, or -1
                    int currentVertex = noSuccessors();
                    if (currentVertex == -1) // must be a cycle                
                        throw new ArgumentException("Graph has cycles");

                    // insert vertex label in sorted array (start at end)
                    _sortedArray[m_numberOfVertices - 1] = m_vertices[currentVertex];

                    deleteVertex(currentVertex); // delete vertex
                }

                // vertices all gone; return sortedArray
                return _sortedArray;
            }

            private int noSuccessors()
            {
                for (int row = 0; row < m_numberOfVertices; row++)
                {
                    bool isEdge = false; // edge from row to column in adjMat
                    for (int col = 0; col < m_numberOfVertices; col++)
                    {
                        if (m_matrix[row, col] > 0) // if edge to another,
                        {
                            isEdge = true;
                            break; // this vertex has a successor try another
                        }
                    }
                    if (!isEdge) // if no edges, has no successors
                    {
                        return row;
                    }
                }
                return -1; // no
            }

            private void deleteVertex(int delVert)
            {
                // if not last vertex, delete from vertexList
                if (delVert != m_numberOfVertices - 1)
                {
                    for (int j = delVert; j < m_numberOfVertices - 1; j++)
                        m_vertices[j] = m_vertices[j + 1];

                    for (int row = delVert; row < m_numberOfVertices - 1; row++)
                        moveRowUp(row, m_numberOfVertices);

                    for (int col = delVert; col < m_numberOfVertices - 1; col++)
                        moveColLeft(col, m_numberOfVertices - 1);
                }
                m_numberOfVertices--; // one less vertex
            }

            private void moveRowUp(int row, int length)
            {
                for (int col = 0; col < length; col++)
                    m_matrix[row, col] = m_matrix[row + 1, col];
            }

            private void moveColLeft(int col, int length)
            {
                for (int row = 0; row < length; row++)
                    m_matrix[row, col] = m_matrix[row, col + 1];
            }
        }

        private enum E_CVSource
        {
            OK,
            Bad,
            Depends
        }

        #region variables
        private SecurityParameter.SecurityParameterType[] x_SystemParameterTypes = {
                SecurityParameter.SecurityParameterType.Boolean,
                SecurityParameter.SecurityParameterType.Count,
                SecurityParameter.SecurityParameterType.Custom,
                SecurityParameter.SecurityParameterType.Date,
                SecurityParameter.SecurityParameterType.Enum,
                SecurityParameter.SecurityParameterType.Money,
                SecurityParameter.SecurityParameterType.Rate,
                SecurityParameter.SecurityParameterType.String 
        };

        private string[] x_RequiresNonLoanSourceOps = new string[] {
            WorkflowOperations.ReadLoan.Id, 
            WorkflowOperations.WriteLoan.Id, 
            WorkflowOperations.RunPmlForAllLoans.Id,
            WorkflowOperations.RunPmlForRegisteredLoans.Id,
            WorkflowOperations.RunPmlToStep3.Id
        };

        private List<string> m_conditionErrors;
        private List<string> m_constraintErrors;
        private List<string> m_customVarErrors;



        private Dictionary<string, List<CustomVariable>> m_customVariableSet;
        private SystemConfig m_globalConfig;
        private bool m_ValidatingSystemConfig;

        private Guid m_brokerId;
        private SystemConfig m_config;
        private ParameterReporting m_reporting;

        private bool m_isValidationDone = false;

        public IEnumerable<string> ConditionErrors { get { return m_conditionErrors; } }
        public IEnumerable<string> ConstraintErrors { get { return m_constraintErrors; } }
        public IEnumerable<string> CustomVariableErrors { get { return m_customVarErrors; } }

        public int ConditionErrorCount { get { return m_conditionErrors.Count; } }
        public int ConstraintErrorCount { get { return m_constraintErrors.Count; } }
        public int CustomVariableErrorCount { get { return m_customVarErrors.Count; } }



        #endregion 



        public SystemConfigValidator(Guid brokerId, SystemConfig systemConfig) 
        {
            IConfigRepository repo = ConfigHandler.GetUncachedRepository(ConstAppDavid.SystemBrokerGuid);
            ReleaseConfigData data = repo.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid);
            Init(brokerId, systemConfig, data.Configuration);
        }

        public SystemConfigValidator(Guid brokerId, SystemConfig systemConfig, SystemConfig globalConfig)
        {
            Init(brokerId, systemConfig, globalConfig);
        }


        public bool IsValid()
        {
            if (m_isValidationDone)
            {
                return ConditionErrorCount == 0 &&
                    ConstraintErrorCount == 0 &&
                    CustomVariableErrorCount == 0;
            }

            m_customVariableSet = GetAllCustomVariables();
            var variables = new List<CustomVariable>(m_customVariableSet.Count);
            foreach (var entry in m_customVariableSet)
            {
                variables.AddRange(entry.Value);
            }

            
            var items = ValidateCustomVariablesTwo(variables.ToArray(), p => m_customVarErrors.Add("Custom " + p));
            m_reporting.UpdateCustomList(variables, items);

            foreach (var variable in variables)
            {
                if (string.IsNullOrEmpty(variable.Name))
                {
                    m_customVarErrors.Add("Custom variable missing name.");
                }

                foreach (Parameter p in variable.Parameters)
                {
                    if (p.Name.Equals(variable.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        m_customVarErrors.Add("Custom var " + variable.Name + " references itself.");
                    }
                }

                ValidateParameters(variable.Parameters,  p => m_customVarErrors.Add("Custom " + p), false, false);
            }

            ValidateParameterSet(m_config.Conditions, p => m_conditionErrors.Add("Condition " + p), true);
            ValidateParameterSet(m_config.Constraints, p => m_constraintErrors.Add("Constraint " + p), false);
            
            m_isValidationDone = true;

         

           if ( m_ValidatingSystemConfig )
           {
               //validate children 
               foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
               {
                   var repo = ConfigHandler.GetUncachedRepository(connInfo);
                   foreach (ReleaseConfigData data in repo.GetActiveReleases())
                   {
                       if (data.OrganizationId == ConstAppDavid.SystemBrokerGuid)
                       {
                           continue;
                       }
                        if (!BrokerDB.IsEnabled(data.OrganizationId))
                        {
                            continue;
                        }
                       SystemConfigValidator v = new SystemConfigValidator(data.OrganizationId, data.Configuration, this.m_config);
                       if (v.IsValid() == false)
                       {
                           m_conditionErrors.Add("Saving the system config will break the following broker : " + data.OrganizationId.ToString());
                           m_conditionErrors.AddRange(v.ConditionErrors);
                           m_conditionErrors.AddRange(v.ConstraintErrors);
                           m_customVarErrors.AddRange(v.CustomVariableErrors);
                       }
                   }
               }
           }

           return ConditionErrorCount == 0 &&
                 ConstraintErrorCount == 0 &&
                 CustomVariableErrorCount == 0;
        }

        private void Init(Guid brokerId, SystemConfig systemConfig, SystemConfig globalConfig)
        {
            m_brokerId = brokerId;
            m_config = systemConfig;
            m_reporting = new ParameterReporting(brokerId);
            m_globalConfig = globalConfig;
            m_conditionErrors = new List<String>();
            m_constraintErrors = new List<string>();
            m_customVarErrors = new List<string>();

            m_ValidatingSystemConfig = brokerId == ConstAppDavid.SystemBrokerGuid;
        }
        /// <summary>
        /// Used by both conditions and constraints
        /// </summary>
        /// <param name="p"></param>
        /// <param name="errorAction"></param>
        private void ValidateParameterSet(AbstractSecurityPermissionSet p, Action<string> errorAction, bool allowVariableOps)
        {
            foreach (AbstractConditionGroup group in p)
            {
                ValidateParameters(group.ParameterSet, error => errorAction.Invoke("ID " + group.Id + ": " + error), true, allowVariableOps);
                bool parameterSourceHasToBeCustomReports = group.SysOpSet.Any(x => x_RequiresNonLoanSourceOps.Contains(x.Name));
                if (parameterSourceHasToBeCustomReports)
                {
                    foreach (Parameter param in group.ParameterSet)
                    {
                        var field = m_reporting[param.Name];  //custom variables should be loan or app or custom report not both.
                        if( field == null || ( field.Source != SecurityParameter.E_FieldSource.CustomReport &&
                            field.Source != SecurityParameter.E_FieldSource.SpecialField &&
                            field.Source != SecurityParameter.E_FieldSource.UserGroup) 
                            )
                        {
                            errorAction.Invoke(param.Name + " Id " + group.Id + " has a read/write op with fields that are not in custom reports <null>");
                            continue;     
                        }
                    }
                }
            }
        }

        private Dictionary<string, SecurityParameter.E_FieldSource> ValidateCustomVariablesTwo(IEnumerable<CustomVariable> s, Action<string> errorAction)
        {
            Dictionary<string, HashSet<String>> customVariables = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, SecurityParameter.E_FieldSource> m_fieldStatus = new Dictionary<string, SecurityParameter.E_FieldSource>();
            foreach (var newField in s)
            {
                HashSet<string> parametersToResolve;
                if (m_fieldStatus.ContainsKey(newField.Name))
                {
                    var status = m_fieldStatus[newField.Name];
                    if (status == SecurityParameter.E_FieldSource.LoanOrApp)
                    {
                        continue; //dont bother already failed;
                    }

                    parametersToResolve = customVariables[newField.Name];
                }
                else
                {
                    m_fieldStatus.Add(newField.Name, SecurityParameter.E_FieldSource.CompositeField);
                    parametersToResolve = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    customVariables.Add(newField.Name, parametersToResolve);
                }

                foreach (var cvParam in newField.Parameters)
                {
                    SecurityParameter.Parameter possibleField;
                    if (cvParam.Name.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                    {
                        DataPath path = DataPath.Create(cvParam.Name);
                        possibleField = m_reporting[path.Head.Name];
                    }
                    else
                    {
                        possibleField = m_reporting[cvParam.Name];
                    }

                    if (possibleField != null)
                    {
                        switch (possibleField.Source)
                        {
                            case SecurityParameter.E_FieldSource.UserGroup:
                            case SecurityParameter.E_FieldSource.SpecialField:
                            case SecurityParameter.E_FieldSource.CustomReport:
                                //this is fine
                                break;

                            //these are okay -- some ar enot
                            case SecurityParameter.E_FieldSource.Condition:
                            case SecurityParameter.E_FieldSource.LoanOrApp:
                                //automatic fail
                                m_fieldStatus[newField.Name] = SecurityParameter.E_FieldSource.LoanOrApp;
                                break;
                            case SecurityParameter.E_FieldSource.CompositeField:    //composite fields should not have been added to the custom reprot object yet.
                            default:
                                throw new UnhandledEnumException(possibleField.Source);
                        }
                    }
                    else
                    {
                        parametersToResolve.Add(cvParam.Name);
                    }
                }
            }

            int id = 0;
            Dictionary<string, int> m_indeces = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            string[] names = new string[customVariables.Count];
            var sort = new TopologicalSort(customVariables.Count);

            foreach (var entry in customVariables)
            {
                m_indeces[entry.Key.ToLower()] = sort.AddVertex(id);
                names[id] = entry.Key;
                id++;
            }

            foreach (var entry in customVariables)
            {
                if (entry.Value.Count == 0 && m_fieldStatus[entry.Key] == SecurityParameter.E_FieldSource.CompositeField)
                {
                    m_fieldStatus[entry.Key] = SecurityParameter.E_FieldSource.CustomReport;
                }
                foreach (string name in entry.Value)
                {
                    if (m_indeces.ContainsKey(name.ToLower()) == false)
                    {
                        errorAction("Did not find custom variable " + name);
                        //failure validate everything else 
                        return m_fieldStatus;
                    }
                    else
                    {
                        sort.AddEdge(m_indeces[entry.Key.ToLower()], m_indeces[name.ToLower()]);
                    }
                }
            }

            int[] result;

            try
            {
                result = sort.Sort().Reverse().ToArray();
            }
            catch (ArgumentException)
            {
                errorAction("Cycle found in custom variables.");
                return m_fieldStatus;
            }

            foreach (int x in result)
            {
                if (m_fieldStatus[names[x]] != SecurityParameter.E_FieldSource.CompositeField)
                {
                    continue; //already determined
                }
                bool fail = false;
                foreach (var entry in customVariables[names[x]])
                {
                    if (m_fieldStatus[entry] == SecurityParameter.E_FieldSource.LoanOrApp)
                    {
                        fail = true;
                        break;
                    }
                    else if (m_fieldStatus[entry] == SecurityParameter.E_FieldSource.CustomReport)
                    {
                        //ok
                    }
                    else
                    {
                        throw new UnhandledEnumException(m_fieldStatus[entry]);//should not happen
                    }
                }

                m_fieldStatus[names[x]] = fail ? SecurityParameter.E_FieldSource.LoanOrApp : SecurityParameter.E_FieldSource.CustomReport;
            }


            return m_fieldStatus;
        }




        private void ValidateParameters(IEnumerable<Parameter> parameters, Action<string> errorAction, bool enforceParameterUniqueness, bool allowVarOps)
        {
            HashSet<string> parameterNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (Parameter parameter in parameters)
            {
                if (enforceParameterUniqueness)
                {
                    if (parameterNames.Contains(parameter.Name))
                    {
                        errorAction.Invoke("Parameter " + parameter.Name + " cannot appear more than once in a condition or constraint.");
                    }
                    else
                    {
                        parameterNames.Add(parameter.Name);
                    }
                }
                ValidateParameter(parameter, errorAction, allowVarOps);
            }
        }

        private void ValidateParameter(Parameter p, Action<string> errorAction, bool allowVarOps)
        {
            switch (p.FunctionT)
            {
                case E_FunctionT.In:
                    ValidateInParameter(p, errorAction);

                    break;
                case E_FunctionT.Between:
                    ValidateBetweenParameter(p, errorAction);
                    break;
                case E_FunctionT.Equal:
                    ValidateEqualityParameter(p, errorAction, false);
                    break;
                case E_FunctionT.EqualVar:
                    if (allowVarOps)
                    {
                        ValidateEqualityParameter(p, errorAction, true);
                    }
                    else
                    {
                        errorAction.Invoke("Cannot use EqualVar with constraint");
                    }
                    break;
                case E_FunctionT.IsGreaterThan:
                case E_FunctionT.IsGreaterThanOrEqualTo:
                case E_FunctionT.IsLessThanOrEqualTo:
                case E_FunctionT.IsLessThan:
                    ValidateComparableParameter(p, errorAction, false);
                    break;
                case E_FunctionT.IsLessThanVar:
                case E_FunctionT.IsGreaterThanVar:
                case E_FunctionT.IsGreaterThanOrEqualToVar:
                case E_FunctionT.IsLessThanOrEqualToVar:
                    if (allowVarOps)
                    {
                        ValidateComparableParameter(p, errorAction, true);
                    }
                    else
                    {
                        errorAction.Invoke("Cannot use " + p.FunctionT.ToString() + " in constraints/custom variables");
                    }
                        break;
                case E_FunctionT.IsNonzeroValue:
                    ValidateIsAnyValue(p, errorAction);
                    break;
                case E_FunctionT.IsAnyValue:
                    ValidateIsAnyValue(p, errorAction);
                    break;
                case E_FunctionT.IsNonblankStr:
                    ValidateIsNonblankStr(p, errorAction);
                    break;
                default:
                    throw new UnhandledEnumException(p.FunctionT);
            }

        }


        #region methods for validating SystemConfig parameters 
        private void ValidateInParameter(Parameter p, Action<string> errorAction)
        {
            SecurityParameter.Parameter systemParameter = m_reporting.GetSecurityParameterWithValues(p.Name);
            if (systemParameter == null)
            {
                errorAction.Invoke("Parameter " + p.Name + " is not a system field, path, or custom variable");
                return;
            }

            if (systemParameter.Type != SecurityParameter.SecurityParameterType.Enum)
            {
                errorAction.Invoke("Parameter " + p.Name + " In can only be used with enum type fields");
                return;
            }

            if (p.Values.Count <= 0 && p.Exceptions.Count <= 0)
            {
                errorAction.Invoke("Parameter " + p.Name + " In needs at least 1 or more values");
            }

            // Verify no value is in both values and exceptions list.
            var intersectingValues = p.Values.Intersect(p.Exceptions);
            if (intersectingValues.Any())
            {
                errorAction.Invoke("Parameter " + p.Name
                    + " In cannot have the same value in both the values and the exclusion list. Intersecting values: "
                    + string.Join(",", intersectingValues));
            }

            SecurityParameter.EnumeratedParmeter enumParameter = (SecurityParameter.EnumeratedParmeter)systemParameter;

            switch (enumParameter.BaseType)
            {
                case SecurityParameter.EnumParamType.Int:
                    ValidateType(p, errorAction, E_ParameterValueType.Int);
                    break;
                case SecurityParameter.EnumParamType.String:
                    ValidateType(p, errorAction, E_ParameterValueType.String);
                    break;
                default:
                    throw new UnhandledEnumException(enumParameter.BaseType);
            }

            foreach (string value in p.Values)
            {
                if (enumParameter.EnumMapping.Where(ep => ep.RepValue == value).Count() == 0)
                {
                    errorAction.Invoke("Parameter " + p.Name + " Did not find value " + value + " in enum mapping.");
                    return;
                }
            }

            foreach (string exception in p.Exceptions)
            {
                if (enumParameter.EnumMapping.Where(ep => ep.RepValue == exception).Count() == 0)
                {
                    errorAction.Invoke("Parameter " + p.Name + " Did not find exception " + exception + " in enum mapping.");
                    return;
                }
            }
        }

        private void ValidateBetweenParameter(Parameter p, Action<string> errorAction)
        {
            if (false == ValidateValueCount(p, 2, errorAction) || false == ValidateSystemField(p.Name, p.ValueType, errorAction))
            {
                return;
            }

                string[] values = p.Values.ToArray();


            if (values[0].Equals(values[1], StringComparison.CurrentCultureIgnoreCase))
            {
                errorAction.Invoke("Parameter " + p.Name + " Between Parameters cannot be the same");
                return;
            }


            if (ValidateType(p, errorAction, E_ParameterValueType.DateTime, E_ParameterValueType.Float, E_ParameterValueType.Int) && false == Parse(values, p.ValueType, errorAction))
            {
                IComparable v1, v2;
                switch (p.ValueType)
                {
                    case E_ParameterValueType.Int:
                        v1 = int.Parse(values[0]);
                        v2 = int.Parse(values[1]);
                        break;
                    case E_ParameterValueType.Float:
                        v1 = float.Parse(values[0]);
                        v2 = float.Parse(values[1]);
                        break;
                    case E_ParameterValueType.DateTime:
                        v1 = DateTime.Parse(values[0]);
                        v2 = DateTime.Parse(values[1]);
                        break;

                    default:
                        throw new UnhandledEnumException(p.ValueType);
                }

                if (v1.CompareTo(v2) > -1)
                {
                    errorAction.Invoke("Parameter " + p.Name + " Between first value has to be less than the second value.");
                }
            }

        }

        private void ValidateEqualityParameter(Parameter p, Action<string> errorAction, bool isVar)
        {

            if (false == ValidateValueCount(p, 1, errorAction))
            {
                return;
            }

            //strings no longer supported either
            List<SecurityParameter.SecurityParameterType> allowedParamTypes = new List<SecurityParameter.SecurityParameterType>()
            {
                SecurityParameter.SecurityParameterType.Boolean,
                SecurityParameter.SecurityParameterType.Date,
                SecurityParameter.SecurityParameterType.Count
            };



            if (isVar)    
            {
                //allow var string comparisons
                allowedParamTypes.Add(SecurityParameter.SecurityParameterType.String);
            }
            else // if not var allow custom variables
            {
                allowedParamTypes.Add(SecurityParameter.SecurityParameterType.Custom);
            }

            if (false == ValidateSystemField(p.Name, p.ValueType, errorAction, allowedParamTypes.ToArray()))
            {
                return;
            }

            if (isVar)
            {
                ValidateSystemField(p.Values.First(), p.ValueType, errorAction);
            }
            else
            {
                Parse(p.Values.First(), p.ValueType, errorAction);
            }
        }
                  
        private void ValidateComparableParameter(Parameter p, Action<string> errorAction, bool isVar)
        {
            //make sure there are only 1 values
            if (false == ValidateValueCount(p, 1, errorAction))
            {
                return;
            }

            //make sure that the system field exist and it matches what the user put
            if (false == ValidateSystemField(p.Name, p.ValueType, errorAction))
            {
                return;
            }

            //make sure that they are comparable types
            if (false == ValidateType(p, errorAction, E_ParameterValueType.DateTime, E_ParameterValueType.Float, E_ParameterValueType.Int))
            {
                return;
            }

            if (isVar)
            {
                //make sure that the value is a variable and it should have the same type as the parameter value type
                ValidateSystemField(p.Values.First(), p.ValueType, errorAction);
            }
            else
            {
                //make sure that the values are of the type we can compare by parsing them
                Parse(p.Values.First(), p.ValueType, errorAction);
            }

            return;
                   
        }

        private void ValidateIsNonblankStr(Parameter p, Action<string> errorAction)
        {
            if (false == ValidateValueCount(p, 1, errorAction))
            {
                return;
            }
            if (false == ValidateType(p, errorAction, E_ParameterValueType.String))
            {
                return;
            }
            if (false == ValidateSystemField(p.Name, E_ParameterValueType.String, errorAction, SecurityParameter.SecurityParameterType.String))
            {
                return;
            }
            Parse(p.Values.First(), E_ParameterValueType.Bool, errorAction);

        }

        private void ValidateIsNonzeroValue(Parameter p, Action<string> errorAction)
        {
            if (false == ValidateValueCount(p, 1, errorAction))
            {
                return;
            }
            if (false == ValidateType(p, errorAction, E_ParameterValueType.Float, E_ParameterValueType.Int))
            {
                return;
            }

            if (false == ValidateSystemField(p.Name, p.ValueType, errorAction,
                SecurityParameter.SecurityParameterType.Count,
                SecurityParameter.SecurityParameterType.Money,
                SecurityParameter.SecurityParameterType.Rate))
            {
                return;
            }

            Parse(p.Values.First(), E_ParameterValueType.Bool, errorAction);


        }

        private void ValidateIsAnyValue(Parameter p, Action<string> errorAction)
        {
            if (false == ValidateValueCount(p, 1, errorAction))
            {
                return;
            }
            if (false == ValidateType(p, errorAction, E_ParameterValueType.Float, E_ParameterValueType.Int, E_ParameterValueType.DateTime))
            {
                return;
            }
            if (false == ValidateSystemField(p.Name, p.ValueType, errorAction,
                  SecurityParameter.SecurityParameterType.Count,
                  SecurityParameter.SecurityParameterType.Money,
                  SecurityParameter.SecurityParameterType.Rate,
                  SecurityParameter.SecurityParameterType.Date))
            {
                return;
            }

            Parse(p.Values.First(), E_ParameterValueType.Bool, errorAction);

        }

        #endregion 

        #region helper methods for the validation

        public static E_ParameterValueType ParamTypeToValueType(SecurityParameter.Parameter systemParameter)
        {
            E_ParameterValueType expectedType;

            switch (systemParameter.Type)
            {
                case SecurityParameter.SecurityParameterType.Rate:
                    expectedType = E_ParameterValueType.Float;
                    break;
                case SecurityParameter.SecurityParameterType.Money:
                    expectedType = E_ParameterValueType.Float;
                    break;
                case SecurityParameter.SecurityParameterType.Count:
                    expectedType = E_ParameterValueType.Int;
                    break;
                case SecurityParameter.SecurityParameterType.String:
                    expectedType = E_ParameterValueType.String;
                    break;
                case SecurityParameter.SecurityParameterType.Date:
                    expectedType = E_ParameterValueType.DateTime;
                    break;
                case SecurityParameter.SecurityParameterType.Boolean:
                    expectedType = E_ParameterValueType.Bool;
                    break;
                case SecurityParameter.SecurityParameterType.Enum:
                    expectedType = E_ParameterValueType.Int;
                    break;
                case SecurityParameter.SecurityParameterType.Custom:
                    expectedType = E_ParameterValueType.CustomVariable;
                    break;
                default:
                    throw new UnhandledEnumException(systemParameter.Type);
            }

            return expectedType;
        }

        private Dictionary<string,List<CustomVariable>> GetAllCustomVariables()
        {
            var customVariablesById = new Dictionary<string, List<CustomVariable>>(StringComparer.OrdinalIgnoreCase);

            foreach (var variable in m_config.CustomVariables)
            {
                List<CustomVariable> variables;
                if( false == customVariablesById.TryGetValue(variable.Name, out variables))
                {
                    variables =new List<CustomVariable>();
                    customVariablesById.Add(variable.Name, variables);
                }
                variables.Add(variable);
            }

            if (m_ValidatingSystemConfig == false)
            {
                Dictionary<string, List<CustomVariable>> globalVars = new Dictionary<string, List<CustomVariable>>();

                foreach (var variable in m_globalConfig.CustomVariables)
                {
                    List<CustomVariable> variables;
                    if (false == globalVars.TryGetValue(variable.Name, out variables))
                    {
                        variables = new List<CustomVariable>();
                        globalVars.Add(variable.Name, variables);
                    }
                    variables.Add(variable);
                }

                foreach (var entry in globalVars)
                {
                    if (customVariablesById.ContainsKey(entry.Key))
                    {
                        m_customVarErrors.Add(entry.Key + " already exist in global config cannot add to lender config.");
                    }
                    else
                    {
                        customVariablesById.Add(entry.Key, entry.Value);
                    }
                }
            }

            return customVariablesById;
        }

        /// <summary>
        /// Finds the given field if not there logs error and returns. 
        /// If the type doesnt match the config type error and return.
        /// expected system types is optional and if its not in the sett error and returns 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="expectedConfigType"></param>
        /// <param name="errorAction"></param>
        /// <param name="expectedTypes"></param>
        /// <returns></returns>
        private bool ValidateSystemField( string fieldName, E_ParameterValueType expectedConfigType, Action<string> errorAction, params SecurityParameter.SecurityParameterType[] expectedTypes  )
        {
            SecurityParameter.Parameter systemParameter = m_reporting.GetSecurityParameterWithValues(fieldName);
            if (systemParameter == null)
            {
                errorAction.Invoke("Parameter " + fieldName + " is not a system field, path, or custom variable");
                return false;
            }

            E_ParameterValueType expectedType = ParamTypeToValueType(systemParameter);

            if (expectedType != expectedConfigType)
            {
                errorAction.Invoke("Parameter " + fieldName + " Value type " + expectedConfigType + " doees not match " + expectedType);
                return false;
            }

            if (expectedTypes!= null && expectedTypes.Length  > 0 &&  false == expectedTypes.Contains(systemParameter.Type))
            {
                errorAction.Invoke("Parameter " + fieldName + " invalid field type.");
                return false;
            }

            return true;

        }
        private bool ValidateValueCount(Parameter p, int countExpectedValues, Action<string> errorAction)
        {
            if (p.Values.Count != countExpectedValues)
            {
                errorAction.Invoke("Parameter " + p.Name + " has invalid number of values for function expected " + countExpectedValues +" found " + p.Values.Count);
                return false;
            }
            return true;
        }

        private bool ValidateAtLeastValueCount(Parameter p, int countExpectedValues, Action<string> errorAction)
        {
            if (p.Values.Count < countExpectedValues)
            {
                errorAction.Invoke("Parameter " + p.Name + " has invalid number of values for function expected atleast" + countExpectedValues + " found " + p.Values.Count);
                return false;
            }
            return true;
        }

        private bool ValidateType(Parameter p, Action<string> errorAction, params E_ParameterValueType[] expectedTypes) 
        {
            if (expectedTypes.Contains( p.ValueType) == false)
            {
                errorAction.Invoke("Parameter " + p.Name + " has invalid type " + p.ValueType);
                return false;
            }
            return true;
        }




        /// <summary>
        /// Parses the given string as the argument p logs error and returns wehther it parsed
        /// </summary>
        /// <param name="value"></param>
        /// <param name="p"></param>
        /// <param name="errorAction"></param>
        /// <returns></returns>
        public bool Parse(string value, E_ParameterValueType p, Action<String> errorAction)
        {
            bool success = true;
            switch (p)
            {
                case E_ParameterValueType.Int:
                    int discaredInt;
                    success = int.TryParse(value, out discaredInt);
                    break;
                case E_ParameterValueType.Float:
                    float discardedFloat;
                    success = float.TryParse(value, out discardedFloat);
                    break;
                case E_ParameterValueType.String:
                    success = true; //always string 
                    break;
                case E_ParameterValueType.DateTime:
                    DateTime discardedTime;
                    success = DateTime.TryParse(value, out discardedTime) || Regex.IsMatch(value, @"^today[+-][0-9]+$"); // support the new date offset comparison (Task Business Rules 2.2.1)
                    break;
                case E_ParameterValueType.CustomVariable:
                case E_ParameterValueType.Bool:
                    bool discardedBool;
                    success = Boolean.TryParse(value, out discardedBool);
                    break;
                default:
                    throw new UnhandledEnumException(p);
            }

            if (false == success)
            {
                errorAction.Invoke("Invalid value " + value + " for type " + p);
            }
            return success;
        }

        /// <summary>
        /// Tries to parse the values according to the type of the parameter.  
        /// </summary>
        /// <param name="values"></param>
        /// <param name="p"></param>
        /// <param name="errorAction"></param>
        /// <returns></returns>
        private bool Parse(string[] values, E_ParameterValueType p, Action<String> errorAction)
        {
            int count = 0;
            foreach (string value in values)
            {
                count += Parse(value, p, errorAction) ? 0 : 1;
            }

            return count > 0;
        }

        #endregion 
    }
}
