﻿namespace LendersOffice.ConfigSystem
{
    using System;

    using global::ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using DataAccess;
    using Security;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using LqbGrammar.DataTypes;
    using Drivers.SqlServerDB;

    public static class ConfigHandler
    {
        public static IConfigRepository GetUncachedRepository(DbConnectionInfo connInfo)
        {
            return new ValidationConfigRepository(new DBConfigRepository(SqlConnectionManager.Get(connInfo), WriteConfigToFileDb, ReadConfigFromFileDb));
        }

        public static IConfigRepository GetUncachedRepository(Guid brokerId)
        {
            return new ValidationConfigRepository(new DBConfigRepository(SqlConnectionManager.Get(brokerId), WriteConfigToFileDb, ReadConfigFromFileDb));
        }

        public static IConfigRepository GetRepository(global::CommonProjectLib.Database.CStoredProcedureExec exec)
        {
            return new TransactionDBConfigRepository(exec, WriteConfigToFileDb, ReadConfigFromFileDb);
        }

        public static IConfigRepository GetRepository(Guid brokerId)
        {
            return new CachedConfigRepository(GetUncachedRepository(brokerId));
        }

        public static IConfigRepository GetUncachedUnvalidatedRepository(Guid brokerId)
        {
            return new DBConfigRepository(SqlConnectionManager.Get(brokerId), WriteConfigToFileDb, ReadConfigFromFileDb);
        }

        /// <summary>
        /// Repository that will create a workflow change history audit when you save release configuration.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="principal">Principal object.</param>
        /// <param name="auditType">Workflow audit type.</param>
        /// <param name="auditDescription">Workflow audit description.</param>
        /// <returns>IConfigRepository object.</returns>
        public static IConfigRepository GetAuditRepository(Guid brokerId, AbstractUserPrincipal principal, WorkflowAuditType auditType, string auditDescription)
        {
            return new AuditConfigRepository(GetUncachedRepository(brokerId), principal, auditType, auditDescription);
        }

        private static Guid WriteConfigToFileDb(Guid organizationId, SystemConfig config)
        {
            Guid keyGuid = Guid.NewGuid();
            string fullKey = $"{keyGuid}_{organizationId}_{SystemConfig.fileDbKeySuffix}";
            FileDBTools.WriteData(E_FileDB.Normal, fullKey, config.ToString());

            return keyGuid;
        }

        private static SystemConfig ReadConfigFromFileDb(Guid organizationId, Guid fileDbKey)
        {
            string fullKey = $"{fileDbKey}_{organizationId}_{SystemConfig.fileDbKeySuffix}";
            string configXml = FileDBTools.ReadDataText(E_FileDB.Normal, fullKey);

            return new SystemConfig(configXml);
        }        
    }
}
