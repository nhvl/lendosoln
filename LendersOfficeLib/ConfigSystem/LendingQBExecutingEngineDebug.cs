﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using ConfigSystem.Engine;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ConfigSystem
{
    public class LendingQBExecutingEngineDebug
    {
        public bool CanPerform { get; set; }
        public WorkflowOperation Operation { get; private set; }
        public string Description { get; private set; }


        private List<ExecutingEngineDebug> m_executingEngineDebugList;

        public IEnumerable<ExecutingEngineDebug> ExecutingEngineDebugList
        {
            get { return m_executingEngineDebugList; }
        }

        public LendingQBExecutingEngineDebug(WorkflowOperation operation, string description)
        {
            Operation = operation;
            Description = description;
            m_executingEngineDebugList = new List<ExecutingEngineDebug>();
        }

        public void Add(ExecutingEngineDebug debug)
        {
            m_executingEngineDebugList.Add(debug);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Operation.ToString() + " - Can Perform:" + CanPerform);

            foreach (var o in ExecutingEngineDebugList)
            {
                sb.AppendLine("    Op:" + o.Operation + ", Can Perform:" + o.CanPerform);
                int idx = 0;
                foreach (var c in o.ConditionList)
                {
                    if (c.FailedParameterCount == 0)
                    {
                        sb.AppendLine("    Successful Condition Idx=" + idx);
                        foreach (var p in c.SuccessfulParameterList)
                        {
                            sb.AppendLine("        " + p.ToString());
                        }
                    }
                    else
                    {
                        sb.AppendLine("    Fail Condition Idx=" + idx);
                        foreach (var p in c.FailedParameterList)
                        {
                            sb.AppendLine("        " + p.ToString());
                        }
                    }
                    idx++;
                }
            }
            return sb.ToString();
        }
    }
}
