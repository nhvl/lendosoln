using System;
using System.Collections;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Dynamics;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Collections.Generic;
using System.Linq;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
	/// <summary>
	/// This base initialize boiler plate settings for loan
	/// events.  For now, the from address is the same on
	/// all email events.  We can split out a new base if
	/// we need a new from address.
	/// </summary>

	public abstract class BatchNotification : NotifyEvent
	{
		/// <summary>
		/// Provide defaults for custom event writers.  These
		/// values are available after construction.
		/// </summary>

		protected string   receiverFullName = String.Empty;
		protected bool sendNotifications = false;
        		
		protected void SetDetails( E_ApplicationT appCode, ref EmployeeDetails eInfo /*, out BrokerAccessInfo bInfo */)
		{
			// Access member.

			try
			{
				// Load the specified employee's name and notification
				// settings.

                //bInfo = new BrokerAccessInfo();
                //NotificationRules nRule = new NotificationRules();

                BrokerDB brokerDB = BrokerDB.RetrieveById(eInfo.BrokerId);
                NotificationRules nRule = brokerDB.NotifRules;

				this.Signature = "";

                //try
                //{
                //    bInfo.Retrieve( eInfo.BrokerId );
                //}
                //catch( Exception e )
                //{
                //    Tools.LogError( "Invalid broker access info for " + eInfo.BrokerId + "." , e );
                //}

                //try
                //{
                //    nRule.Retrieve( bInfo.ReplyXml );
                //}
                //catch( Exception e )
                //{
                //    Tools.LogError( "Invalid notif rule xml for " + eInfo.BrokerId + "." , e );
                //}

				this.Signature = string.Format( "If you have any questions please contact us." );

				foreach( NotificationRule cRule in nRule )
				{
					// Get the current event's reply-to configuration.  We
					// currently use the same rule for all events.  Soon,
					// each event will be known by its own label.  Reply
					// handling involves setting the reply-to address and
					// the bcc-address, if any.

					if( cRule.EventLabel == m_NotificationEventLabel )
					{
						if( cRule.DoNotReply == false )
						{
							// Look up the role preferences.  If not found,
							// then use the default.

							if( cRule.DefaultReply != "" )
							{
								if( cRule.DefaultLabel != "" )
								{
									this.From = String.Format( "\"{0}\" <{1}>", cRule.DefaultLabel, cRule.DefaultReply);
								}
								else
								{
									this.From = string.Format( "{0}", cRule.DefaultReply);
								}
							}
						}

						if( cRule.CarbonCopyTo != "" )
						{
							this.Bcc = cRule.CarbonCopyTo;
						}

						break;
					}
				}

				if( appCode == E_ApplicationT.LendersOffice )
				{
                    this.Closing = "This notification was automatically generated for you by LendingQB (www.lendingqb.com).  Please do not directly reply to this email.";
				}
				else
				{
					this.Closing = "This notification was automatically generated for you.  Please do not directly reply to this email.";
				}

				if( eInfo.DoNotify == true && eInfo.IsActive == true )
				{
					sendNotifications = true;
				}
				else
				{
					sendNotifications = false;
				}

				receiverFullName = eInfo.FullName;

				if( this.From == "" )
				{
					this.From = String.Format( "\"Do not reply\" <{0}>", eInfo.Email);
				}

				this.To = eInfo.Email;
			}
			catch( Exception e )
			{
				throw new EventNotifyException( "Failed to set batch event details." , e );
			}
		}

		/// <summary>
		/// Send out whatever is assigned.  If from or to list is not
		/// initialized, then we skip sending.  To field can be a
		/// comma-separated list.
		/// </summary>

		public override void Send()
		{
			// Check if we ought to send.  If so, then away we go.

			if( sendNotifications == true && this.To.TrimWhitespaceAndBOM() != "" )
			{
				base.Send();
			}
		}

	}



	/// <summary>
	/// Send out email detailing that the specified lock was broken
	/// by the given user.
	/// </summary>

	public class RateLockBroken : LoanNotification
	{
		/// <summary>
		/// Designate a label for this notification for config
		/// matching and event processing.
		/// </summary>

		protected override string m_NotificationEventLabel
		{
			get { return "Event"; }
		}

		/// <summary>
		/// Initialize message with borrower's name and loan details
		/// when the lock was broken.  We send to whoever is specified
		/// in the to-list (comma separated list is accepted).
		/// </summary>
        public void Initialize(
            string sReason,
            Guid toEmployeeId,
            LoanEventData dataLoan,
            LoanEventData linkedLoan,
            LoanAssignmentContactTable loanAssignments,
            E_ApplicationT appCode)
        {
            try
            {
                this.SetDetails(toEmployeeId, dataLoan, linkedLoan, loanAssignments, appCode);

                this.Subject = "Rate lock has been broken";

                const string doubleNewLine = "\r\n\r\n";
                StringBuilder bodyBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(this.receiverFullName))
                {
                    bodyBuilder.Append(this.receiverFullName + "," + doubleNewLine);
                }

                bodyBuilder.Append(GetLoanDescription(this.loanName, this.openedDate, this.primaryBorrowerFullName) + " is no longer locked -- broken on " + Tools.GetDateTimeNowString() + "." + doubleNewLine);
                if (!string.IsNullOrEmpty(sReason))
                {
                    bodyBuilder.Append("Reason for breaking the lock: " + sReason + "." + doubleNewLine);
                }

                bodyBuilder.Append("You can re-login to the system to review loan status and conditions.");
                this.Body = bodyBuilder.ToString();
            }
            catch(Exception e)
            {
                throw new EventNotifyException("Bad init for rate lock broken.", e);
            }
        }

        /// <summary>
        /// Builds a string describing the specific loan referred to by the parameters intended to
        /// refer to the recipient's specific loan.
        /// </summary>
        /// <param name="loanName">The name of the loan.</param>
        /// <param name="openedDate">The date the loan was opened.</param>
        /// <param name="primaryBorrowerFullName">The full name of the primary borrower.</param>
        /// <returns>A string describing the loan for the email.</returns>
        private static string GetLoanDescription(string loanName, string openedDate, string primaryBorrowerFullName)
        {
            if (!string.IsNullOrEmpty(loanName) && !string.IsNullOrEmpty(openedDate))
            {
                if (!string.IsNullOrEmpty(primaryBorrowerFullName))
                {
                    return "Your loan " + loanName + ", which was opened on " + openedDate + " for " + primaryBorrowerFullName + ",";
                }
                else
                {
                    return "Your loan " + loanName + ", which was opened on " + openedDate + ",";
                }
            }
            else if (!string.IsNullOrEmpty(primaryBorrowerFullName))
            {
                return "Your loan for " + primaryBorrowerFullName;
            }

            return "One of your loans";
        }

        public static bool Send(Guid brokerId, Guid loanId, string reason, E_ApplicationT appCode)
        {
            var loanAssignmentTable = new LoanAssignmentContactTable(brokerId, loanId);
            var employees = Utilities.GetLoanAssignments(loanAssignmentTable, fileOutRole: E_RoleT.Administrator);
            LoanEventData primaryLoan = null, linkedLoan = null;
            bool allNotificationsSucceeded = true;

            if (employees.Any())
            {
                primaryLoan = new LoanEventData(loanId);
                primaryLoan.InitLoad();

                if (primaryLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    linkedLoan = new LoanEventData(primaryLoan.sLinkedLoanInfo.LinkedLId);
                    linkedLoan.InitLoad();
                }
            }

            foreach (Guid sendEmployeeId in employees)
            {
                try
                {
                    RateLockBroken loanEvt = new RateLockBroken();
                    loanEvt.Initialize(reason, sendEmployeeId, primaryLoan, linkedLoan, loanAssignmentTable, appCode);
                    loanEvt.Send();
                }
                catch (Exception e)
                {
                    allNotificationsSucceeded = false;
                    Tools.LogError(e);
                }
            }

            return allNotificationsSucceeded;
        }
    }

	/// <summary>
	/// Track changes and list them in a single loan event.
	/// </summary>

	public class ConditionChangeRecord
	{
		/// <summary>
		/// Track changes and list them in a single loan
		/// event.
		/// </summary>

		private string m_Condition = "";
        private string m_Category = "";
        private string m_DateDone = "";
		private bool   m_IsDone = false;
        private string m_AlertDate = "";
        private string m_Who = "";

		#region ( Record Properties )

		public string Condition
		{
			set { m_Condition = value; }
			get { return m_Condition; }
		}

        public string Category
		{
			set { m_Category = value; }
			get { return m_Category; }
		}

        public string DateDone
		{
			set { m_DateDone = value; }
			get { return m_DateDone; }
		}

		public bool IsDone
		{
			set { m_IsDone = value; }
			get { return m_IsDone; }
		}

        public string AlertDate
		{
			set { m_AlertDate = value; }
			get { return m_AlertDate; }
		}

        public string Who
		{
			set { m_Who = value; }
			get { return m_Who; }
		}

		#endregion

	}

	/// <summary>
	/// Send out email detailing that a user's loan's condition
	/// was just saved with changes.
	/// </summary>

	public class LoanConditionsChanged : LoanNotification
	{
		/// <summary>
		/// Designate a label for this notification for config
		/// matching and event processing.
		/// </summary>

		protected override String m_NotificationEventLabel
		{
			get { return "Event"; }
		}

		/// <summary>
		/// Build a single email to the intended user describing all
		/// the changes to a loan's conditions that we have tracked
		/// over a recent period.
		/// </summary>

		public void Initialize( IList changeRecords , Guid toEmployeeId , Guid loanId , E_ApplicationT appCode )
		{
			// Construct detailed message for assigned agents to consider.

			try
			{
				// Build up simple message with condition list.

				SetDetails( toEmployeeId , loanId , appCode );

				this.Body = "";
				StringBuilder bodySB = new StringBuilder();

				if( receiverFullName != "" )
				{
					bodySB.AppendFormat( "{0},\r\n\r\n", receiverFullName);
				}

				if( changeRecords.Count > 1 )
				{
					bodySB.Append("The following conditions ");
				}
				else if( changeRecords.Count > 0 )
				{
					bodySB.Append("The following condition ");
				}
				else
				{
					bodySB.Append("Conditions ");
				}

				if( primaryBorrowerFullName != "" && loanName != "" && openedDate != "" )
				{
					bodySB.AppendFormat
						( "of loan {0}, which was opened on {1} for {2}, "
						, loanName
						, openedDate
						, primaryBorrowerFullName
						);
				}
				else if( loanName != "" && openedDate != "" )
				{
					bodySB.AppendFormat
						( "of loan {0}, which was opened on {1}, "
						, loanName
						, openedDate
						);
				}
				else if( openedDate != "" )
				{
					bodySB.AppendFormat( "of one of your loans, which was opened on {0}, ", openedDate);
				}
				else
				{
					bodySB.Append("of one of your loans");
				}

				if( changeRecords.Count > 1 )
				{
					bodySB.Append("have been ");
				}
				else
				{
					bodySB.Append("has been ");
				}

				bodySB.Append("updated. You can re-login to the system to review these changes.\r\n");

				if( changeRecords.Count > 1 )
				{
					bodySB.AppendFormat( "\r\nCondition Report ({0} conditions listed):", changeRecords.Count);
				}
				else
				{
					bodySB.AppendFormat( "\r\nCondition Report ({0} condition listed):", changeRecords.Count);
				}

				int i = 1;

				foreach( ConditionChangeRecord chRec in changeRecords )
				{
					bodySB.AppendFormat( "\r\n\r\n#{0} ", i);

					if( chRec.Condition != "" && chRec.Category != "" )
					{
						bodySB.AppendFormat( "\"{0}\" ({1}) ", chRec.Condition, chRec.Category);
					}
					else if( chRec.Condition != "" )
					{
						bodySB.AppendFormat
							( "\"{0}\" ( -- category not specified -- ) "
							, chRec.Condition
							);
					}
					else if( chRec.Category != "" )
					{
						bodySB.AppendFormat
							( "\"-- empty condition --\" ({0}) "
							, chRec.Category
							);
					}
					else
					{
						bodySB.AppendFormat
							( "One of this loan's conditions ");
					}

					if( chRec.Who != "" && chRec.AlertDate != "" )
					{
						bodySB.AppendFormat
							( "was modified by {0} on {1}"
							, chRec.Who
							, chRec.AlertDate
							);
					}
					else if( chRec.Who != "" )
					{
						bodySB.AppendFormat
							( "was modified by {0}"
							, chRec.Who
							);
					}
					else if( chRec.AlertDate != "" )
					{
						bodySB.AppendFormat
							( "was modified on {0}"
							, chRec.AlertDate
							);
					}
					else 
                    {
						bodySB.Append("was modified");
					}

					if( chRec.IsDone == true )
					{
						bodySB.AppendFormat
							( "\r\n"
							+ "Marked done on {0}"
							, chRec.DateDone
							);
					}

					++i;
				}

				this.Subject = "Conditions have been updated";
				this.Body = bodySB.ToString();
			}
			catch( Exception e )
			{
				// Oops!

				throw new EventNotifyException( "Bad init for loan condition changed." , e );
			}
		}

	}

	/// <summary>
	/// Keep track of all the roles for an individual loan.  We list
	/// out multiple loan changes for each person.  The recorded changes
	/// are from the recipient's perspective, so expect specs with
	/// nothing as a mode for all roles that the recipient isn't
	/// modifying.
	/// </summary>

	public class LoanAssignmentRecord
	{
		/// <summary>
		/// Keep track of all the roles for an individual loan.  We
		/// list out multiple loan changes for each person.  The
		/// recorded changes are from the recipient's perspective,
		/// so expect specs with nothing as a mode for all roles
		/// that the recipient isn't modifying.
		/// </summary>

		private ArrayList   m_Set = new ArrayList();
		private string m_LoanName = String.Empty;
		private string m_Borrower = String.Empty;
		private Guid m_LoanId = Guid.Empty;
		private Guid m_BranchId = Guid.Empty;

		public string LoanName
		{
			set { m_LoanName = value; }
			get { return m_LoanName; }
		}

		public string Borrower
		{
			set { m_Borrower = value; }
			get { return m_Borrower; }
		}

		public Guid LoanId
		{
			set { m_LoanId = value; }
			get { return m_LoanId; }
		}

		public Guid BranchId
		{
			set { m_BranchId = value; }
			get { return m_BranchId; }
		}


		#region ( List manipulation )

		/// <summary>
		/// Describe the change mode for an individual role assignment.
		/// </summary>

		public enum E_ChangeMode
		{
			/// <summary>
			/// Describe the change mode for an individual role
			/// assignment.
			/// </summary>

			Invalid = 0 ,
			Replace = 1 ,
			Assign  = 2 ,
			Remove  = 3 ,
			Nothing = 4 ,

		}

		/// <summary>
		/// Keep track of individual role assignments.  If nothing
		/// changed, then expect a mode of 'nothing' with the current
		/// assignment in 'assign'.
		/// </summary>

		public class RoleSpec
		{
			/// <summary>
			/// Keep track of individual role assignments.  If
			/// nothing changed, then expect a mode of 'nothing'
			/// with the current assignment in 'assign'.
			/// </summary>

			private E_ChangeMode m_Mode = E_ChangeMode.Invalid;
			private String     m_Before = String.Empty;
			private String     m_Assign = String.Empty;
			private String     m_Reason = String.Empty;
			private String     m_Role   = String.Empty;

			#region ( Role specification properties )

			public E_ChangeMode Mode
			{
				set { m_Mode = value; }
				get { return m_Mode; }
			}

			public string Before
			{
				set { m_Before = value; }
				get { return m_Before; }
			}

			public string Assign
			{
				set { m_Assign = value; }
				get { return m_Assign; }
			}

			public string Reason
			{
				set { m_Reason = value; }
				get { return m_Reason; }
			}

			public string Role
			{
				set { m_Role = value; }
				get { return m_Role; }
			}

			#endregion

		}

		/// <summary>
		/// Append a new role spec to our set.  We don't care about
		/// collisions -- just take it as is.
		/// </summary>

		public void Add( String sRole , String sBefore , String sAssign , String sReason , E_ChangeMode eMode )
		{
			// Add a new role specification (only if unique) that
			// describes the current state of the role (no change).

			RoleSpec nS = new RoleSpec();

			if( sBefore == null )
			{
				throw new ArgumentException( "Invalid before name." , "Before" );
			}

			if( sAssign == null )
			{
				throw new ArgumentException( "Invalid assign name." , "Assign" );
			}

			if( sRole == null )
			{
				throw new ArgumentException( "Invalid role name." , "Role" );
			}

			nS.Assign = sAssign;
			nS.Before = sBefore;

			if( sReason != null )
			{
				nS.Reason = sReason;
			}
			else
			{
				nS.Reason = "";
			}

			nS.Role = sRole;
			nS.Mode = eMode;

			m_Set.Add( nS );
		}

		/// <summary>
		/// Set the current role's spec in our set.  We don't care
		/// about collisions -- just take it as is.
		/// </summary>

		public void Set( String sRole , String sAssign )
		{
			// Add a new role specification (only if unique) that
			// describes the current state of the role (no change).

			RoleSpec nS = new RoleSpec();

			nS.Mode = E_ChangeMode.Nothing;

			if( sAssign == null )
			{
				throw new ArgumentException( "Invalid assign name." , "Assign" );
			}

			if( sRole == null )
			{
				throw new ArgumentException( "Invalid role name." , "Role" );
			}

			nS.Assign = sAssign;
			nS.Role   = sRole;

			m_Set.Add( nS );
		}

		/// <summary>
		/// Return the looping interface for walking role specs.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Return the looping interface for walking.

			return m_Set.GetEnumerator();
		}

		#endregion

	}

	/// <summary>
	/// Send out email detailing that a user's loan's role assignments
	/// were just modified.  Each record should reflect the recipient's
	/// perspective.
	/// </summary>

	public class LoanAssignmentsUpdated : BatchNotification
	{
		/// <summary>
		/// Designate a label for this notification for config
		/// matching and event processing.
		/// </summary>

		protected override String m_NotificationEventLabel
		{
			get { return "Event"; }
		}

		/// <summary>
		/// Build a single email to the intended user describing all
		/// the changes to a loan's conditions that we have tracked
		/// over a recent period.
		/// </summary>

		// 09/27/06 mf - OPM 7404 We now allow admins to use the branch name
		// on notifications instead of the company name.  
		// Updated this class so we group loans by how the name is to be displayed
		// Also cleaned up the loading a bit since this is the only batch notification
		// we have.  We can generalize the base as needed in the future.

		public void Initialize( Guid brokerId, IList assignRecords , Guid toEmployeeId , E_ApplicationT appCode )
		{
			// Construct detailed message for updated recipients to
			// consider.

			try
			{
                this.BrokerId = brokerId;
                // Build up simple message with role change list.

                EmployeeDetails employeeInfo;
				
				if ( toEmployeeId != Guid.Empty )
				{
					employeeInfo = new EmployeeDetails( brokerId, toEmployeeId );
				}
				else
				{
					throw new ArgumentException( "Empty employee id not accepted." );
				}

				SetDetails( appCode, ref employeeInfo );

                BrokerDB brokerDB = BrokerDB.RetrieveById(employeeInfo.BrokerId);
                AssignmentChangeList changes = new AssignmentChangeList(employeeInfo.BrokerId, brokerDB.Name, brokerDB.Phone);
				
				foreach( LoanAssignmentRecord asRec in assignRecords )
				{
					changes.Add(asRec);
				}

				this.Body = changes.BuildBodyString( receiverFullName );

				this.Subject = "Your loan assignments have been updated";
			}
			catch( Exception e )
			{
				// Oops!

				throw new EventNotifyException( "Bad init for loan assignments updated." , e );
			}
		}

		// 10/04/06 mf - OPM 7404
		// Class to maintain list of loan assignment changes
		// and generate the assignment update body text.
		private class AssignmentChangeList
		{
			private Hashtable m_branchCache = new Hashtable();
			private Hashtable m_loanDisplayGroups = new Hashtable();
			private Guid m_brokerId;
			private ContactInfo m_brokerInfo;
			private int m_loanCount = 0;

			public AssignmentChangeList( Guid brokerId, string brokerName, string brokerPhone )
			{
				m_brokerInfo.Name = brokerName;
				m_brokerInfo.Phone = brokerPhone;
				m_brokerId = brokerId;
			}

			// Build the body text of the batch changes.
			public string BuildBodyString( string receiverFullName )
			{
				System.Text.StringBuilder body = new System.Text.StringBuilder();

				if( receiverFullName != "" )
				{
					body.AppendFormat
						( "{0},"
						+ "\r\n"
						+ "\r\n"
						, receiverFullName
						);
				}

				if( m_loanCount > 0 )
				{
					body.AppendFormat
						( "The following loan assignments have been updated. You"
						+ " can re-login to the system to review these changes."
						+ "\r\n"
						);
				}
				else
				{
					body.AppendFormat
						( "Loan assignments have been updated. You can re-login"
						+ " to the system to review the changes."
						+ "\r\n"
						);
				}

				if( m_loanCount > 1 )
				{
					body.AppendFormat
						( "\r\n"
						+ "Loan Assignment Report ({0} loans listed):"
						, m_loanCount
						);
				}
				else
				{
					body.AppendFormat
						( "\r\n"
						+ "Loan Assignment Report ({0} loan listed):"
						, m_loanCount
						);
				}

				int i = 1;
				foreach(DictionaryEntry change in m_loanDisplayGroups)
				{
					ArrayList branchChanges = change.Value as ArrayList;
					
					ContactInfo contactInfo;
					if ( m_branchCache[ change.Key ] != null )
					{
						contactInfo = (ContactInfo) m_branchCache[ change.Key ];
					}
					else
					{
						contactInfo = m_brokerInfo;
					}
				
					if ( m_loanDisplayGroups.Count > 1 )
					{
						string introText = String.Format
							( "{0} ({1}):"
							, contactInfo.Name
							, branchChanges.Count == 1 ? "1 loan" : branchChanges.Count.ToString() + " loans"
							);

						body.AppendFormat( "{0}{0}{1}{0}{2}"
							, Environment.NewLine
							, string.Empty.PadRight( introText.Length, '-' ) 
							, introText
							);
					}
					foreach( object o in branchChanges )
					{
						LoanAssignmentRecord asRec = ( LoanAssignmentRecord ) o;
						
						body.AppendFormat
							( "\r\n"
							+ "\r\n"
							+ "#{0} "
							, i
							);

						if( asRec.LoanName != "" && asRec.Borrower != "" )
						{
							body.AppendFormat
								( "{0} for {1}"
								, asRec.LoanName
								, asRec.Borrower
								);
						}
						else if( asRec.LoanName != "" )
						{
							body.AppendFormat
								( "{0}"
								, asRec.LoanName
								);
						}
						else if( asRec.Borrower != "" )
						{
							body.AppendFormat
								( "Loan for {0}"
								, asRec.Borrower
								);
						}
						else
						{
							body.AppendFormat( "One of your loans");
						}

						foreach( LoanAssignmentRecord.RoleSpec rS in asRec )
						{
							body.AppendFormat
								( "\r\n"
								+ "{0}:"
								, rS.Role
								);

							switch( rS.Mode )
							{
								case LoanAssignmentRecord.E_ChangeMode.Replace:
								{
									body.AppendFormat
										( " {0} => {1}"
										, rS.Before
										, rS.Assign
										);
								}
									break;

								case LoanAssignmentRecord.E_ChangeMode.Assign:
								{
									body.AppendFormat
										( " Unassigned => {0}"
										, rS.Assign
										);
								}
									break;

								case LoanAssignmentRecord.E_ChangeMode.Remove:
								{
									body.AppendFormat
										( " {0} => Unassigned"
										, rS.Before
										);
								}
									break;

								default:
								{
									body.AppendFormat
										( " {0}"
										, rS.Assign
										);
								}
									break;
							}

							if( rS.Reason != "" )
							{
								body.AppendFormat
									( " ({0})"
									, rS.Reason
									);
							}

						}
						i++;

					}

					body.Append ( Environment.NewLine + Environment.NewLine );
					body.Append
						( contactInfo.Name
						+ Environment.NewLine
						+ contactInfo.Phone
						);
					i = 1;

				}

				return body.ToString();
			}
			
			public void Add( LoanAssignmentRecord assignments )
			{
				// Add the record to our list.  We cache branches.
				// Basically, we maintain one group for all loans from branches
				// where we do not display branch info, and the rest are
				// grouped by branch.

				Guid displayGroupId = Guid.Empty;
				
				if ( m_branchCache.Contains( assignments.BranchId ) == false )
				{
					BranchDB branch = new BranchDB( assignments.BranchId, m_brokerId );
					branch.Retrieve();

					if ( branch.IsDisplayNmModified )
					{
						ContactInfo info = new ContactInfo();
						info.Name = branch.DisplayNm;
						info.Phone = branch.Phone;

						// Add the branch info if we need to display it
						m_branchCache.Add( branch.BranchID, info ); 
						displayGroupId = branch.BranchID;
					}
					else
					{
						m_branchCache.Add( branch.BranchID, null );
					}
				}
				else
				{
					if ( m_branchCache[ assignments.BranchId ] != null )
					{
						displayGroupId = assignments.BranchId;
					}
				}

				if ( m_loanDisplayGroups.Contains( displayGroupId ) )
				{
					( (ArrayList) m_loanDisplayGroups[ displayGroupId ] ).Add( assignments );
				}
				else
				{
					ArrayList branchAssignments = new ArrayList();
					branchAssignments.Add( assignments );
					m_loanDisplayGroups.Add( displayGroupId, branchAssignments );
				}
				
				m_loanCount++;
			}
			
			private struct ContactInfo
			{
				public string Name;
				public string Phone;
				public ContactInfo( string name, string phone )
				{
					this.Name = name;
					this.Phone = phone;
				}
			}

		}

	}

}

namespace LendersOffice.Events
{
	/// <summary>
	/// Every loan action event should inherit from this class.  We
	/// provide a standard interface for querying loan action details,
	/// as well as maintain important, common aspects of the event.
	/// </summary>

	public abstract class LoanActionEvent : DispatchEvent
	{
		/// <summary>
		/// Every loan action event should inherit from this class.
		/// We provide a standard interface for querying loan action
		/// details, as well as maintain important, common aspects of
		/// the event.
		/// </summary>

		protected string  m_LoanName = String.Empty;
        protected string m_Borrower = String.Empty;
		protected DateTime m_OpenedD = DateTime.MinValue;
		protected Guid      m_LoanId = Guid.Empty;

		#region ( Event properties )

		[ EventMember( "LoanName" , typeof( String ) )]
        public string LoanName
		{
			set { m_LoanName = value; }
			get { return m_LoanName; }
		}

		[ EventMember( "Borrower" , typeof( String ) )]
        public string Borrower
		{
			set { m_Borrower = value; }
			get { return m_Borrower; }
		}

		[ EventMember( "OpenedD" , typeof( DateTime ) )]
		public DateTime OpenedD
		{
			set { m_OpenedD = value; }
			get { return m_OpenedD; }
		}

		[ EventMember( "LoanId" , typeof( Guid ) )]
		public Guid LoanId
		{
			set { m_LoanId = value; }
			get { return m_LoanId; }
		}

		#endregion

	}

	/// <summary>
	/// Track the rate-locked loan action event.  We keep track
	/// of basic loan details and the locked rate with fees.
	/// </summary>

	public class RateLockedEvent : LoanActionEvent
	{
		/// <summary>
		/// Track the rate-locked loan action event.  We keep
		/// track of basic loan details and the locked rate with
		/// fees.
		/// </summary>

		protected Decimal          m_Rate = Decimal.MinusOne;
		protected Decimal    m_BrokerFees = Decimal.MinusOne;
		protected Decimal        m_Margin = Decimal.MinusOne;
		protected Int32            m_Term = Int32.MinValue;
		protected Int32             m_Due = Int32.MinValue;
		protected Int32            m_Days = Int32.MinValue;
		protected DateTime  m_ExpirationD = DateTime.MinValue;
		protected E_sFinMethT m_FinMethod = E_sFinMethT.Fixed;

		#region ( Event properties )

		[ EventMember( "BrokerFees" , typeof( Decimal ) )]
		public Decimal BrokerFees
		{
			set { m_BrokerFees = value; }
			get { return m_BrokerFees; }
		}

		[ EventMember( "Rate" , typeof( Decimal ) ) ]
		public Decimal Rate
		{
			set { m_Rate = value; }
			get { return m_Rate; }
		}

		[ EventMember( "Margin" , typeof( Decimal ) ) ]
		public Decimal Margin
		{
			set { m_Margin = value; }
			get { return m_Margin; }
		}

		[ EventMember( "Term" , typeof( Int32 ) )]
		public int Term
		{
			set { m_Term = value; }
			get { return m_Term; }
		}

		[ EventMember( "Due" , typeof( Int32 ) )]
		public int Due
		{
			set { m_Due = value; }
			get { return m_Due; }
		}

		[ EventMember( "Days" , typeof( Int32 ) ) ]
		public int Days
		{
			set { m_Days = value; }
			get { return m_Days; }
		}

		[ EventMember( "ExpirationD" , typeof( DateTime ) )]
		public DateTime ExpirationD
		{
			set { m_ExpirationD = value; }
			get { return m_ExpirationD; }
		}

		[ EventMember( "FinMethod" , typeof( E_sFinMethT ) )]
		public E_sFinMethT FinMethod
		{
			set { m_FinMethod = value; }
			get { return m_FinMethod; }
		}

		#endregion

		public override string EventDescription
		{
			get { return "Loan rate has been locked"; }
		}

		public override string EventDetails
		{
			// Access member.

			get
			{
				if( Borrower != "" && LoanName != "" )
				{
					return String.Format
						( "Your loan {0}, which was opened on {1} for {2}, has"
						+ " been locked at {3} rate and {4} points on {5}. You"
						+ " can re-login to the system to review loan status"
						+ " and conditions."
						, LoanName
						, OpenedD
						, Borrower
						, Rate
						, BrokerFees
						);
				}
				else if( LoanName != "" )
				{
					return String.Format
						( "Your loan {0}, which was opened on {1}, has been"
						+ " locked at {3} rate and {4} points on {5}. You can"
						+ " re-login to the system to review loan status and"
						+ " conditions."
						, LoanName
						, OpenedD
						, Borrower
						, Rate
						, BrokerFees
						, OccurredOn
						);
				}
				else if( Borrower != "" )
				{
					return String.Format
						( "Your loan for {0} has been locked at {1} rate and {2}"
						+ " points on {3}. You can re-login to the system to"
						+ " review loan status and conditions."
						, Borrower
						, Rate
						, BrokerFees
						, OccurredOn
						);
				}
				else
				{
					return String.Format
						( "One of your loans has been locked at {1} rate and {2}"
						+ " points on {3}. You can re-login to the system to"
						+ " review loan status and conditions."
						, Rate
						, BrokerFees
						, OccurredOn
						);
				}
			}
		}

		public override Object ArgumentData
		{
			get { return m_Rate; }
		}

		public override string Label
		{
			get { return "ratelocked"; }
		}

	}

}
