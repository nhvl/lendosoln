using System;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Dynamics;
using LendersOffice.Common;
using DataAccess;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
    /// <summary>
    /// This base initialize boiler plate settings for loan
    /// events.  For now, the from address is the same on
    /// all email events.  We can split out a new base if
    /// we need a new from address.
    /// </summary>

    public abstract class LoanNotification : NotifyEvent
    {
        /// <summary>
        /// Provide defaults for custom event writers.  These
        /// values are available after construction.
        /// </summary>

        protected string                loanName = "";
        protected string   lenderAccountExecName = "";
        protected string   lenderAccountExecCall = "";
        protected string   lenderAccountExecMail = "";
        protected string primaryBorrowerFullName = "";
        protected string              openedDate = "";
        protected string              linkedName = "";
        protected string              linkedDate = "";
        protected string              loanStatus = "";
        protected string              brokerName = "";
        protected string              brokerCall = "";
        protected string        receiverFullName = "";
        protected string           m_branchPhone = "";
        protected string     m_branchDisplayName = "";		
        protected bool         sendNotifications = false; // disableNotifications trumps this.
        protected bool      disableNotifications = false; // if this is set to true, then *definitely* do not send. opm 185732
        protected string               aBFirstNm = "";
        protected string                aBLastNm = "";
        protected string           loanOfficerNm = "";
        protected string   pmlOriginatingCompany = "";

        /// <summary>
        /// Override this check if you want to use the generic
        /// broker signature by returning false.
        /// </summary>

        protected virtual bool includeAccountExecInfo
        {
            get { return true; }
        }

        private void LoadLoanInformation(LoanEventData dataLoan, LoanEventData linkedLoan)
        {
            // 12/16/2004 kb - We initialize our variables
            // to contain the currently persisted loan data.
            // If the loan is in flux at the time of this
            // setting, then we use the last saved info.

            CAppData dataApp = dataLoan.GetAppData(0);
            aBFirstNm = dataApp.aBFirstNm;
            aBLastNm = dataApp.aBLastNm;

            loanName = dataLoan.sLNm;
            lenderAccountExecName = dataLoan.sEmployeeLenderAccExecName;
            lenderAccountExecCall = dataLoan.sEmployeeLenderAccExecPhone;
            lenderAccountExecMail = dataLoan.sEmployeeLenderAccExecEmail;
            loanOfficerNm = dataLoan.sEmployeeLoanRepName;
            pmlOriginatingCompany = dataLoan.sEmployeeLoanRepPmlUserBrokerNm;
            primaryBorrowerFullName = dataLoan.sPrimBorrowerFullNm;
            loanStatus = dataLoan.sStatusT_rep;
            openedDate = dataLoan.sOpenedD_rep;

            m_branchPhone = dataLoan.Branch.Phone;
            m_branchDisplayName = dataLoan.Branch.DisplayNm;

            if (dataLoan.sLinkedLoanInfo.IsLoanLinked && linkedLoan != null)
            {
                linkedDate = linkedLoan.sOpenedD_rep;
                linkedName = linkedLoan.sLNm;
            }

            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT || E_sLoanFileT.Test == dataLoan.sLoanFileT)
            {
                disableNotifications = true;
            }
        }


        private CPageData LoadLoanInformation(Guid sLId)
        {
            CPageData dataLoan = null;
            if (Guid.Empty != sLId)
            {
                // 12/16/2004 kb - We initialize our variables
                // to contain the currently persisted loan data.
                // If the loan is in flux at the time of this
                // setting, then we use the last saved info.

                dataLoan = new LoanEventData(sLId);
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();

                CAppData dataApp = dataLoan.GetAppData(0);
                aBFirstNm = dataApp.aBFirstNm;
                aBLastNm = dataApp.aBLastNm;

                loanName = dataLoan.sLNm;
                lenderAccountExecName = dataLoan.sEmployeeLenderAccExecName;
                lenderAccountExecCall = dataLoan.sEmployeeLenderAccExecPhone;
                lenderAccountExecMail = dataLoan.sEmployeeLenderAccExecEmail;
                loanOfficerNm = dataLoan.sEmployeeLoanRepName;
                pmlOriginatingCompany = dataLoan.sEmployeeLoanRepPmlUserBrokerNm;
                primaryBorrowerFullName = dataLoan.sPrimBorrowerFullNm;
                loanStatus = dataLoan.sStatusT_rep;
                openedDate = dataLoan.sOpenedD_rep;
                Guid sBranchId = dataLoan.sBranchId;
                BranchDB branch = new BranchDB(sBranchId, dataLoan.sBrokerId);
                branch.Retrieve();
                m_branchPhone = branch.Phone;
                m_branchDisplayName = branch.DisplayNm;

                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    LoanEventData linkData = new LoanEventData(dataLoan.sLinkedLoanInfo.LinkedLId);

                    linkData.InitLoad();

                    linkedDate = linkData.sOpenedD_rep;
                    linkedName = linkData.sLNm;
                }

                if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT || E_sLoanFileT.Test == dataLoan.sLoanFileT)
                {
                    disableNotifications = true;
                }
            }
            return dataLoan;
        }


        protected EmployeeDetails LoadEmployeeInformation(Guid employeeId) 
        {
            if (Guid.Empty == employeeId)
                throw new CBaseException(ErrorMessages.Generic, "Empty employee id is not allow");

            EmployeeDetails eInfo = new EmployeeDetails();

            eInfo.Retrieve(employeeId);

            if( eInfo.DoNotify == true && eInfo.IsActive == true )
            {
                sendNotifications = true;
            }
            else
            {
                sendNotifications = false;
            }

            receiverFullName = eInfo.FullName;

            this.To = eInfo.Email;

            return eInfo;
        }
        private void LoadBrokerInformationImpl(BrokerDB brokerDB, LoanAssignmentContactTable rList) 
        {
            this.Signature = "";

            BrokerId = brokerDB.BrokerID;

            brokerName = m_branchDisplayName;

            // 11/23/2005 dd - OPM 3344 - Use branch phone number instead of corporate phone number.

            if (m_branchPhone != "") 
                brokerCall = m_branchPhone;
            else
                brokerCall = brokerDB.Phone;

            NotificationRules nRule = brokerDB.NotifRules;

            if( brokerName.TrimWhitespaceAndBOM() != "" || brokerCall.TrimWhitespaceAndBOM() != "" )
            {
                // Initialize the signature.  We put the user's broker's
                // name and phone for contact purposes.

                if (includeAccountExecInfo == true && lenderAccountExecName.TrimWhitespaceAndBOM() != "" && lenderAccountExecCall.TrimWhitespaceAndBOM() != "" )
                {
                    this.Signature = String.Format
                        ( "If you have any questions, please contact {0} at {1}."
                        + "\r\n"
                        + "\r\n"
                        + "{2}"
                        + "\r\n"
                        + "{3}"
                        , lenderAccountExecName.TrimWhitespaceAndBOM()
                        , lenderAccountExecCall.TrimWhitespaceAndBOM()
                        , brokerName.TrimWhitespaceAndBOM()
                        , brokerCall.TrimWhitespaceAndBOM()
                        );
                }
                else if (includeAccountExecInfo == true && lenderAccountExecName.TrimWhitespaceAndBOM() != "" )
                {
                    this.Signature = String.Format
                        ( "If you have any questions, please contact {0}."
                        + "\r\n"
                        + "\r\n"
                        + "{1}"
                        + "\r\n"
                        + "{2}"
                        , lenderAccountExecName.TrimWhitespaceAndBOM()
                        , brokerName.TrimWhitespaceAndBOM()
                        , brokerCall.TrimWhitespaceAndBOM()
                        );
                }
                else
                {
                    this.Signature = String.Format
                        ( "Please contact us if you have any questions."
                        + "\r\n"
                        + "\r\n"
                        + "{0}"
                        + "\r\n"
                        + "{1}"
                        , brokerName.TrimWhitespaceAndBOM()
                        , brokerCall.TrimWhitespaceAndBOM()
                        );
                }
            }
            else
            {
                throw new EventNotifyException( "Broker lacks name and phone." );
            }

            foreach( NotificationRule cRule in nRule )
            {
                // Get the current event's reply-to configuration.  We
                // currently use the same rule for all events.  Soon,
                // each event will be known by its own label.  Reply
                // handling involves setting the reply-to address and
                // the bcc-address, if any.

                if( cRule.EventLabel == m_NotificationEventLabel )
                {
                    if( cRule.DoNotReply == false )
                    {
                        // Look up the role preferences.  If not found,
                        // then use the default.

                        Boolean isSet = false;

                        foreach( String rDesc in cRule.ReplyToList )
                        {
                            EmployeeLoanAssignment eA = rList.FindByRoleDesc( rDesc );

                            if (null != eA && "" != eA.Email )
                            {
                                if( eA.FullName != "" )
                                {
                                    this.From = string.Format( "\"{0}\" <{1}>", eA.FullName, eA.Email);
                                }
                                else
                                {
                                    this.From = string.Format( "{0}", eA.Email);
                                }

                                isSet = true;

                                break;
                            }
                        }

                        if( isSet == false && cRule.DefaultReply != "" )
                        {
                            if( cRule.DefaultLabel != "" )
                            {
                                this.From = String.Format( "\"{0}\" <{1}>", cRule.DefaultLabel, cRule.DefaultReply);
                            }
                            else
                            {
                                this.From = string.Format( "{0}", cRule.DefaultReply);
                            }
                        }
                    }

                    if( cRule.CarbonCopyTo != "" )
                    {
                        this.Bcc = cRule.CarbonCopyTo;
                    }

                    break;
                }
            }

        }

        private void SetClosingDescription(E_ApplicationT appCode) 
        {
            
            if( appCode == E_ApplicationT.LendersOffice )
            {
                this.Closing = "This notification was automatically generated for you by LendingQB (www.lendingqb.com).  Please do not directly reply to this email.";
            }
            else
            {
                this.Closing = "This notification was automatically generated for you.  Please do not directly reply to this email.";
            }
        }

        public void SetDetails(Guid employeeId, LoanEventData dataLoan, LoanEventData linkedLoan, LoanAssignmentContactTable table, E_ApplicationT appCode)
        {
            this.From = "";
            EmployeeDetails eInfo = LoadEmployeeInformation(employeeId);
            LoadLoanInformation(dataLoan, linkedLoan);
            sendNotifications = sendNotifications && eInfo.BrokerId == dataLoan.sBrokerId;
            LoadBrokerInformationImpl(dataLoan.BrokerDB, table);

            if (this.From == "")
            {
                this.From = string.Format("\"Do not reply\" <{0}>", eInfo.Email);
            }

            SetClosingDescription(appCode);
        }


        /// <summary>
        /// Set initial from address and loan information for
        /// loan events we send out as emails.
        /// </summary>
        /// <param name="appCode">
        /// Specify what application is generating this notification.
        /// We currently support "LO" and "PML".
        /// </param>

        public void SetDetails( Guid employeeId , Guid sLId , E_ApplicationT appCode )
        {
            try
            {
                // Load the specified employee's name and notification settings
                EmployeeDetails eInfo = LoadEmployeeInformation(employeeId);
                LoanAssignmentContactTable rList = new LoanAssignmentContactTable();
                this.From = "";
                CPageData data = null;
                if (Guid.Empty != sLId) 
                {
                    data = LoadLoanInformation(sLId);
                    rList.Retrieve(eInfo.BrokerId, sLId);
                }


                if (data != null)
                {
                    sendNotifications = sendNotifications && eInfo.BrokerId == data.sBrokerId;
                }



                LoadBrokerInformationImpl(BrokerDB.RetrieveById(eInfo.BrokerId), rList);

                if ( this.From == "" )
                {
                    this.From = string.Format( "\"Do not reply\" <{0}>", eInfo.Email);
                }

                SetClosingDescription(appCode);

            }
            catch( Exception e )
            {
                throw new EventNotifyException( "Failed to set loan event defaults." , e );
            }
        }

        protected void SetDetailsUsingAgentList(Guid agentId, E_ApplicationT appCode, LoanEventData dataLoan, LoanEventData linkedLoan, LoanAssignmentContactTable contactTable) 
        {
            try
            {
                this.From = "";

                LoadLoanInformation(dataLoan, linkedLoan);

                LoadBrokerInformationImpl(dataLoan.BrokerDB, contactTable  );

                LoadAgentInformation(dataLoan, agentId);

            }
            catch( Exception e )
            {
                throw new EventNotifyException( "Failed to set loan event defaults." , e );
            }
        }

        private void LoadAgentInformation(CPageData dataLoan, Guid agentId) 
        {
            CAgentFields agent = dataLoan.GetAgentFields(agentId);

            sendNotifications = true;

            receiverFullName = agent.AgentName;

            this.To = agent.EmailAddr;

            if (this.From == "") 
            {
                this.From = string.Format("\"Do not reply\" <{0}>", agent.EmailAddr);
            }
        }
        /// <summary>
        /// Send out whatever is assigned.  If from or to list is not
        /// initialized, then we skip sending.  To field can be a
        /// comma-separated list.
        /// </summary>

        public override void Send()
        {
            // Check if we ought to send.  If so, then away we go.
            Subject = Subject.Insert(0, loanName + " - " + aBFirstNm + " " + aBLastNm + " - "); //gf opm 94083
            if( false == disableNotifications && sendNotifications == true && this.To.TrimWhitespaceAndBOM() != "" )
            {
                base.Send();
            }
        }

    }

}
