using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using LendersOffice.Dynamics;
using LendersOffice.Email;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Events
{
	/// <summary>
	/// Thrown when notification mechanism fails.  Filter on this
	/// type and ignore in ui code (unless you care).
	/// </summary>

	public class EventNotifyException : CBaseException
	{
		public EventNotifyException( String message , Exception inner )	: base( message , inner )
		{
		}

		public EventNotifyException( String message ) : base( message , message )
		{
		}

	}

	/// <summary>
	/// ...
	/// </summary>

	public enum E_NotifyOption
	{
		No      = 0 ,
		Yes     = 1 ,
		Default = 2 ,
	}

	/// <summary>
	/// ...
	/// </summary>

	[ XmlRoot]
	public class NotifyOption
	{
		/// <summary>
		/// ...
		/// </summary>

		private E_NotifyOption m_Option = E_NotifyOption.No;
		private String        m_EventId = String.Empty;

		[ XmlElement]
		public E_NotifyOption Option
		{
			set { m_Option = value; }
			get { return m_Option; }
		}

		[ XmlElement ]
		public String EventId
		{
			set { m_EventId = value; }
			get { return m_EventId; }
		}

	}

	/// <summary>
	/// Keep track of options set during configuration.  We
	/// let the broker design how they want to be notified
	/// by each event.
	/// </summary>

	[ XmlRoot ]
	public class NotifyOptionSet
	{
		/// <summary>
		/// Keep track of options set during configuration.
		/// We let the broker design how they want to be
		/// notified by each event.
		/// </summary>

		private ArrayList m_Set = new ArrayList();

		public E_NotifyOption this[ String eventId ]
		{
			// Access member.

			get
			{
				foreach( NotifyOption nO in m_Set )
				{
					if( nO.EventId == eventId )
					{
						return nO.Option;
					}
				}

				return E_NotifyOption.Default;
			}
		}

		[ XmlArray]
		[ XmlArrayItem( typeof( NotifyOption ) )]
		public ArrayList Set
		{
			set { m_Set = value; }
			get { 	return m_Set; }
		}

		/// <summary>
		/// Append a new option configuration entry into our
		/// set.  If redundant, we update the existing.
		/// </summary>

		public void Add( String eventId , E_NotifyOption eOpt )
		{
			// Find a match and update.  If new, append onto
			// the tail of our set.

			NotifyOption nOption = new NotifyOption();

			nOption.EventId = eventId;
			nOption.Option  = eOpt;

			foreach( NotifyOption nO in m_Set )
			{
				if( nO.EventId == eventId )
				{
					nO.Option = eOpt;

					return;
				}
			}

			m_Set.Add( nOption );
		}

		/// <summary>
		/// Return the looping interface for walking.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Get the looping interface for walking.

			return m_Set.GetEnumerator();
		}

		/// <summary>
		/// Convert the xml doc to our set implementation.
		/// </summary>

		public static NotifyOptionSet ToObject( String sXml )
		{
			// Convert the xml doc to our set object.

			return m_Serializer.Deserialize( new System.IO.StringReader( sXml ) ) as NotifyOptionSet;
		}

		/// <summary>
		/// Convert our set implementation to an xml doc.
		/// </summary>

		public override String ToString()
		{
			// Convert our set object to an xml doc.

			StringWriter8 s8 = new StringWriter8();

			m_Serializer.Serialize( s8 , this );

			return s8.ToString();
		}

		/// <summary>
		/// We cache an instance of the serializer so
		/// we're not constructing a new one every time
		/// we pull from the db.  I'm not sure how much
		/// memory this takes, but it can't be much.
		/// </summary>

		private static XmlSerializer m_Serializer = new XmlSerializer
			( typeof( NotifyOptionSet )
			);

	}

	/// <summary>
	/// Provide email interface for forming and sending
	/// simple messages.  Most email-based events will
	/// inherit from this.
	/// </summary>

	public abstract class NotifyEvent
	{
        /// <summary>
        /// Provide simple email container.  Specializing
        /// classes can handle initialization, while users
        /// can inspect and modify.
        /// </summary>

        private Guid    m_brokerId = Guid.Empty;
		private string   m_Subject = String.Empty;
		private string      m_From = String.Empty;
		private string       m_Bcc = String.Empty;
		private string        m_To = String.Empty;
		private string      m_Body = String.Empty;
		private string m_Signature = String.Empty;
		private string   m_Closing = String.Empty;

		protected abstract String m_NotificationEventLabel
		{
			// Access member.
			get;
		}

        public Guid BrokerId
        {
            protected set { m_brokerId = value; }
            get { return m_brokerId; }
        }

		public string Subject
		{
			set { m_Subject = value; }
			get { return m_Subject; }
		}

		public String From
		{
			set { m_From = value; }
			get { return m_From; }
		}

		public String Bcc
		{
			set { m_Bcc = value; }
			get { return m_Bcc; }
		}

		public String To
		{
			set { m_To = value; }
			get { return m_To; }
		}

		public String Body
		{
			set { m_Body = value; }
			get { return m_Body; }
		}

		public String Signature
		{
			set { m_Signature = value; }
			get { return m_Signature; }
		}

		public String Closing
		{
			set { m_Closing = value; }
			get { return m_Closing; }
		}

		/// <summary>
		/// Send out whatever is assigned.  If from or to list is not
		/// initialized, then we skip sending.  To field can be a
		/// comma-separated list.
		/// </summary>

		public virtual void Send()
		{
			// Send out email using email processor so that we get
			// background processing and robust handling.

			try
			{
				// Initialize proxy and queue up send request.  We
				// throw exception on error.

				if( m_From == null || m_From.TrimWhitespaceAndBOM() == "" )
				{
					throw new ArgumentException( "Bad parameter." , "From" );
				}

				if( m_To == null || m_To.TrimWhitespaceAndBOM() == "" )
				{
					throw new ArgumentException( "Bad parameter." , "To" );
				}

				if( m_Body == null || m_Body.TrimWhitespaceAndBOM() == "" )
				{
					throw new ArgumentException( "Bad parameter." , "Body" );
				}

				if( m_Signature != "" && m_Closing != "" )
				{
                    CBaseEmail cbe = new CBaseEmail(m_brokerId)
                    {
                        Subject = m_Subject.TrimWhitespaceAndBOM(),
                        From = m_From.TrimWhitespaceAndBOM(),
                        Bcc = m_Bcc.TrimWhitespaceAndBOM(),
                        To = m_To.TrimWhitespaceAndBOM(),
                        Message = m_Body
                        + "\r\n"
                        + "\r\n"
                        + m_Signature
                        + "\r\n"
                        + "\r\n"
                        + "\r\n"
                        + m_Closing
                    };
                    cbe.Send();
                } 
                else if ( m_Signature != "" )
				{
                    CBaseEmail cbe = new CBaseEmail(m_brokerId)
                    {
                        Subject = m_Subject.TrimWhitespaceAndBOM(),
                        From = m_From.TrimWhitespaceAndBOM(),
                        Bcc = m_Bcc.TrimWhitespaceAndBOM(),
                        To = m_To.TrimWhitespaceAndBOM(),
                        Message = m_Body + "\r\n" + "\r\n" + m_Signature
                    };
                    cbe.Send();
                }
				else if( m_Closing != "" )
				{
                    CBaseEmail cbe = new CBaseEmail(m_brokerId)
                    {
                        Subject = m_Subject.TrimWhitespaceAndBOM(),
                        From = m_From.TrimWhitespaceAndBOM(),
                        Bcc = m_Bcc.TrimWhitespaceAndBOM(),
                        To = m_To.TrimWhitespaceAndBOM(),
                        Message = m_Body + "\r\n" + "\r\n" + "\r\n" + m_Closing
                    };
                    cbe.Send();
                }
				else
				{
                    CBaseEmail cbe = new CBaseEmail(m_brokerId)
                    {
                        Subject = m_Subject.TrimWhitespaceAndBOM(),
                        From = m_From.TrimWhitespaceAndBOM(),
                        Bcc = m_Bcc.TrimWhitespaceAndBOM(),
                        To = m_To.TrimWhitespaceAndBOM(),
                        Message = m_Body
                    };
                    cbe.Send();
				}
			}
			catch( Exception e )
			{
				// Oops!

				throw new EventNotifyException( "Failed to send notify event." , e );
			}
		}

	}

	/// <summary>
	/// ...
	/// </summary>

	public class NotifyOptionCache
	{
		/// <summary>
		/// ...
		/// </summary>

		private Hashtable m_Table = new Hashtable();
		private ArrayList m_Array = new ArrayList();
		private TimeSpan  m_Stale = TimeSpan.FromSeconds( 120 );
		private Int32     m_Limit = 256;
		private Int32     m_Count = 0;

		public NotifyOptionSet this[ Guid uId ]
		{
			// Access member.

			get
			{
				Entry oE = m_Table[ uId ] as Entry;

				if( oE != null )
				{
					return oE.Set;
				}

				return null;
			}
		}

		public Int32 Count
		{
			// Access member.

			get
			{
				return m_Count;
			}
		}

		/// <summary>
		/// ...
		/// </summary>

		public class Entry
		{
			/// <summary>
			///  ...
			/// </summary>

			private NotifyOptionSet   m_Set = new NotifyOptionSet();
			private DateTime m_StaleTimeOut = DateTime.MinValue;
			private Guid               m_Id = Guid.Empty;

			#region ( Entry properties )

			public NotifyOptionSet Set
			{
				// Access member.

				set
				{
					m_Set = value;
				}
				get
				{
					return m_Set;
				}
			}

			public DateTime StaleTimeOut
			{
				// Access member.

				set
				{
					m_StaleTimeOut = value;
				}
				get
				{
					return m_StaleTimeOut;
				}
			}

			public Guid Id
			{
				// Access member.

				set
				{
					m_Id = value;
				}
				get
				{
					return m_Id;
				}
			}

			#endregion

			/// <summary>
			/// Construct default.
			/// </summary>

			public Entry( NotifyOptionSet optionSet , Guid uId )
			{
				// Initialize members.

				m_Set = optionSet;
				m_Id  = uId;
			}

		}

		/// <summary>
		/// Add a new entry or overwrite an existing one.  We
		/// shuffle the cache lists to make this entry look most
		/// recently accessed.
		/// </summary>

		public void Add( NotifyOptionSet optionSet , Guid uId )
		{
			// Add a new entry or overwrite an existing one.
			// We shuffle the cache lists to make this entry
			// look most recently accessed.

			Entry oE = m_Table[ uId ] as Entry;

			if( oE == null )
			{
				while( m_Count >= m_Limit && m_Count > 0 )
				{
					m_Table.Remove( m_Array[ 0 ] );
					m_Array.RemoveAt( 0 );

					--m_Count;
				}

				if( m_Count < m_Limit )
				{
					m_Table.Add( uId , oE = new Entry( optionSet , uId ) );
					m_Array.Add( uId );

					oE.StaleTimeOut = DateTime.Now + m_Stale;

					++m_Count;
				}
			}
			else
			{
				oE.StaleTimeOut = DateTime.Now + m_Stale;

				oE.Set = optionSet;

				m_Array.Remove( uId );
				m_Array.Add( uId );
			}
		}

		/// <summary>
		/// Lookup the identified entry and see if it's stale.  If
		/// found and fresh... great!
		/// </summary>

		public Boolean IsValid( Guid uId , DateTime dCheck )
		{
			// Lookup the identified entry and see if it's stale.
			// If found and fresh... great!

			Entry oE = m_Table[ uId ] as Entry;

			if( oE != null )
			{
				if( oE.StaleTimeOut.CompareTo( dCheck ) > 0 )
				{
					return true;
				}
			}

			return false;
		}

	}

}