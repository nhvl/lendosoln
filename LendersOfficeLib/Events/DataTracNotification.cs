using System;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Events;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
	public enum E_DataTracInteractionT
	{
		ExportToDataTrac = 0,
		ImportFromDataTrac = 1
	}
	
	// Notification for DataTrac events (exporting a loan to DataTrac and importing a loan from DataTrac)
	public class DataTracNotification : LoanNotification
	{
		protected override String m_NotificationEventLabel
		{
			// Access member.

			get
			{
				return "Event";
			}
		}
		
		public void Initialize( Guid toEmployeeId , Guid loanId, E_DataTracInteractionT interactionType, string dataTracLoanNum, string loLoanNum, string sUser, E_ApplicationT appCode )
		{
			SetDetails( toEmployeeId , loanId , appCode );
			switch(interactionType)
			{
				case E_DataTracInteractionT.ExportToDataTrac:
				{
					this.Subject = "Loan was exported to DataTrac.";
					this.Body = string.Format("Loan {0} was exported to DataTrac as {1} by {2}.", loLoanNum.ToUpper(), dataTracLoanNum.ToUpper(), sUser);
					break;				
				}
				case E_DataTracInteractionT.ImportFromDataTrac:
				{
					this.Subject = "Loan was imported from DataTrac.";
					this.Body = string.Format("Loan {0} was imported from DataTrac into {1} by {2}.", dataTracLoanNum.ToUpper(), loLoanNum.ToUpper(), sUser);
					break;
				}
				default:
					throw new EventNotifyException( "Invalid E_DataTracInteractionT value in DataTracNotification.Initialize: " + interactionType);
			}
		}
	}
}