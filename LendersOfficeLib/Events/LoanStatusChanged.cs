using System;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Dynamics;
using LendersOffice.Common;
using DataAccess;
using System.Text;
using System.Collections.Generic;
using LendersOffice.Constants;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
    static class StringExt
    {
        //We do this a whole heckuva lot, so make it a clearer extension to string
        public static bool IsAvailable(this string me)
        {
            return !string.IsNullOrEmpty(me);
        }
    }

    /// <summary>
    /// Send out email detailing that the specified loan has a
    /// new status.
    /// </summary>

    public class LoanStatusChanged : LoanNotification
    {

        private LoanStatusChanged() { }

        /// <summary>
        /// Todo modify so we get dont load the loan once per event...
        /// </summary>
        /// <param name="loanId"></param>
        public static void Send( Guid loanId )
        {
            try
            {
                HashSet<Guid> employeesToNotify = new HashSet<Guid>();
                List<LoanStatusChanged> auditEvents = new List<LoanStatusChanged>();

                LoanEventData dataLoan = new LoanEventData(loanId);
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                LoanEventData linkedLoan = null;

                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    linkedLoan = new LoanEventData(dataLoan.sLinkedLoanInfo.LinkedLId);
                    linkedLoan.AllowLoadWhileQP2Sandboxed = true;
                    linkedLoan.InitLoad();
                }

                LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(PrincipalFactory.CurrentPrincipal.BrokerId, loanId);

                foreach (var assignedEmployee in loanAssignmentTable.Items)
                {
                    if (employeesToNotify.Contains(assignedEmployee.EmployeeId))
                    {
                        continue;
                    }

                    if (assignedEmployee.RoleT == E_RoleT.Administrator)
                    {
                        continue;
                    }

                    employeesToNotify.Add(assignedEmployee.EmployeeId);
                    LoanStatusChanged evt = new LoanStatusChanged();
                    evt.InitializeImpl(PrincipalFactory.CurrentPrincipal, assignedEmployee.EmployeeId, dataLoan, linkedLoan, loanAssignmentTable);
                    evt.Send();
                }

                int nCount = dataLoan.GetAgentRecordCount();
                HashSet<string> agentEmailList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                for (int i = 0; i < nCount; i++)
                {

                    CAgentFields agent = dataLoan.GetAgentFields(i);

                    if (!agent.IsNotifyWhenLoanStatusChange)
                    {
                        continue;
                    }

                    if (agentEmailList.Contains(agent.EmailAddr))
                    {
                        continue;
                    }
                    agentEmailList.Add(agent.EmailAddr.ToLower());


                    LoanStatusChanged loanEvt = new LoanStatusChanged();
                    loanEvt.InitializeWithAgent(dataLoan.sStatusT_rep, PrincipalFactory.CurrentPrincipal.DisplayName,
                        agent.RecordId, PrincipalFactory.CurrentPrincipal.ApplicationType, dataLoan, linkedLoan, loanAssignmentTable);
                    loanEvt.Send();

                }
            }
            catch (CBaseException e)
            {
                Tools.LogErrorWithCriticalTracking(e);
            }


        }

        /// <summary>
        /// Designate a label for this notification for config
        /// matching and event processing.
        /// </summary>

        protected override String m_NotificationEventLabel
        {
            get { return "Event"; }
        }

        private void BuildEmailBody(string newStatus, string sUser) 
        {
            this.Body = "";
			StringBuilder bodySB = new StringBuilder();
			
            if( receiverFullName != "" )
            {
                bodySB.Append(string.Format( "{0},{1}{1}", receiverFullName, Environment.NewLine));
            }

            if (newStatus == null )
            {
                newStatus = "";
            }

            if (sUser == null )
            {
                sUser = "";
            }

            //This is kinda like a generative grammar.
            bodySB.Append(string.Format("{0}{1}{2} on {3}", GetLoanIdent(), 
                GetLoanSpec(), GetChangeDetails(newStatus, sUser), Tools.GetDateTimeNowString()));

			this.Body = bodySB.ToString();
			this.Subject = "Loan status has changed";//av 23816 10 17 08
        }

        /// <summary>
        /// Returns a string that describes the loan as uniquely as possible
        /// </summary>
        /// <returns></returns>
        private string GetLoanIdent()
        {
            //Note that $string.IsAvailable() is an alias to !string.IsNullOrEmpty($string)
            //It's just easier to read like this :)
            if (loanName.IsAvailable())
            {
                //This should always be available
                return "The status of loan number " + loanName.TrimWhitespaceAndBOM();
            }
            else if (primaryBorrowerFullName.IsAvailable())
            {
                //But just in case it isn't
                return "The status of your loan for " + primaryBorrowerFullName;
            }
            else
            {
                //And this would be really bad - a loan whose name we don't know,
                //borrowed by someone we don't know, so we have to be vague
                return "The status of one of your loans";
            }
        }

        /// <summary>
        /// Returns a string that specifies some facts about the loan, 
        /// which might jog the user's memory
        /// </summary>
        /// <returns></returns>
        private string GetLoanSpec()
        {
            string OpeningDetails = GetLoanOpenDetails();
            string AssignmentDetails = GetLoanAssignmentDetails();

            if (OpeningDetails.IsAvailable() && AssignmentDetails.IsAvailable())
            {
                //Neither of them is null, so use both - this is the most common case
                return ", which " + OpeningDetails + " and " + AssignmentDetails + ", ";
            }
            else if(!OpeningDetails.IsAvailable() || !AssignmentDetails.IsAvailable())
            {
                //At least one of them is null, so the null coalesce will take the one that isn't.
                //OpeningDetails, at least, will almost always be not null
                return ", which " + (OpeningDetails ?? AssignmentDetails) + ", ";
            }
            else
            {
                //Otherwise, neither of them is available so we skip this part
                return " ";
            }

        }

        /// <summary>
        /// Returns some details about the loan opening
        /// </summary>
        /// <returns></returns>
        private string GetLoanOpenDetails()
        {
            string preface = "was opened";
            string result = null;
            if (openedDate.IsAvailable())
            {
                //which was opened on $some_day (this should always be true)
                result = preface + " on " + openedDate;
            }

            //the second clause here is to keep us from repeating ourselves, 
            //since loans will be identified by borrower name if loan name is not available.
            if (primaryBorrowerFullName.IsAvailable() && loanName.IsAvailable())
            {
                //"was opened for $some_borrower" or "was opened on $some_day for $some borrower", 
                //depending on if result is null
                //Note that result should basically never be null, 
                //but this is better than crashing if it is
                result = (result ?? preface) + " for " + primaryBorrowerFullName;
            }
            
            return result;
        }

        /// <summary>
        /// Return some details about the officer to whom the loan is assigned
        /// </summary>
        /// <returns></returns>
        private string GetLoanAssignmentDetails()
        {
            string result = null;
            if (loanOfficerNm.IsAvailable())
            {
                //This may occasionally happen, if the loan is unassigned
                result = "is currently assigned to " + loanOfficerNm;

                //Only PML users will have this set
                if (pmlOriginatingCompany.IsAvailable())
                {
                    result = result + " of " + pmlOriginatingCompany;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns details about how the status has changed
        /// </summary>
        /// <param name="newStatus">The new status</param>
        /// <param name="sUser">The user who caused the change</param>
        /// <returns></returns>
        private string GetChangeDetails(string newStatus, string sUser)
        {
            string result = null;
            if (newStatus.IsAvailable())
            {
                //This should be the case that almost always happens
                result = "has been set to " + newStatus;
                if (sUser.IsAvailable())
                {
                    result = result + " by " + sUser;
                }
            }
            else if (sUser.IsAvailable())
            {
                //But if it doesn't, we have some fallbacks
                result = "has been updated by " + sUser;
            }
            else
            {
                result = "has been updated";
            }

            return result;
        }

        private void InitializeWithAgent(string newStatus, string sUser, Guid agentId, E_ApplicationT appCode, LoanEventData dataLoan, LoanEventData linkedLoan, LoanAssignmentContactTable table) 
        {
            try
            {
                // Build up simple message with rate and fees.
                SetDetailsUsingAgentList(agentId, appCode, dataLoan, linkedLoan, table);

                BuildEmailBody(newStatus, sUser);

            }
            catch( Exception e )
            {
                // Oops!

                throw new EventNotifyException( "Bad init for loan status changed." , e );
            }

        }
        /// <summary>
        /// Generate body that describes the new status change
        /// and who performed the change.
        /// </summary>
        public void InitializeImpl(AbstractUserPrincipal p, Guid toEmployeeId, LoanEventData dataLoan, LoanEventData linkedLoan, LoanAssignmentContactTable table )
        {
            // Construct detailed message for assigned agents to consider.

            try
            {
    
                SetDetails( toEmployeeId , dataLoan, linkedLoan, table, p.ApplicationType );
                BuildEmailBody(dataLoan.sStatusT_rep, p.DisplayName);

            }
            catch( Exception e )
            {
                // Oops!

                throw new EventNotifyException( "Bad init for loan status changed." , e );
            }
        }

    }

}
