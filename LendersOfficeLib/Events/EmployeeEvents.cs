using System;
using System.Collections;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Text;

namespace LendersOffice.Events
{
	/// <summary>
	/// This base initialize boiler plate settings for user
	/// events.  For now, the from address is the same on
	/// all email events.  We can split out a new base if
	/// we need a new from address.
	/// </summary>

	public abstract class EmployeeNotification : NotifyEvent
	{
		/// <summary>
		/// Provide defaults for custom event writers.  These
		/// values are available after construction.
		/// </summary>

		protected String receiverFullName = String.Empty;

		/// <summary>
		/// Set the closing and from address for this event notification.
		/// </summary>

		protected void SetDetails( Guid employeeId )
		{
			try
			{
				// Load up the broker and loan info so we can properly
				// provide relevant data to the custom event writers.
                //BrokerAccessInfo  bInfo = new BrokerAccessInfo();
                //NotificationRules nRule = new NotificationRules();
				EmployeeDetails   eInfo = new EmployeeDetails();

				if( employeeId != Guid.Empty )
				{
					eInfo.Retrieve( employeeId );
				}
				else
				{
					throw new ArgumentException( "Empty employee id not accepted." );
				}

                BrokerDB brokerDB = BrokerDB.RetrieveById(eInfo.BrokerId);
                NotificationRules nRule = brokerDB.NotifRules;

                this.BrokerId = eInfo.BrokerId;

				foreach( NotificationRule cRule in nRule )
				{
					// Get the current event's reply-to configuration.  We
					// currently use the same rule for all events.  Soon,
					// each event will be known by its own label.  Reply
					// handling involves setting the reply-to address and
					// the bcc-address, if any.

					if( cRule.EventLabel == m_NotificationEventLabel )
					{
						if( cRule.DoNotReply == false )
						{
							// Look up the role preferences.  If not found,
							// then use the default.

							if( cRule.DefaultReply != "" )
							{
								if( cRule.DefaultLabel != "" )
								{
									this.From = String.Format
										( "\"{0}\" <{1}>"
										, cRule.DefaultLabel
										, cRule.DefaultReply
										);
								}
								else
								{
									this.From = String.Format
										( "{0}"
										, cRule.DefaultReply
										);
								}
							}
						}

						if( cRule.CarbonCopyTo != "" )
						{
							this.Bcc = cRule.CarbonCopyTo;
						}

						break;
					}
				}

				
				this.Signature = String.Format
                    ("Team LendingQB"
					+ "\r\n"
					+ "www.lendingqb.com"
					);

				this.Closing = String.Format
					( "--------------------------------------------------------------------------------"
					+ "\r\n"
					+ "\r\n"
                    + "Copyright � LendingQB | 1124 Bristol Street, Costa Mesa, CA  92626"
					+ "\r\n"
					+ "\r\n"
					+ "The information contained in this message may be privileged and/or"
					+ " confidential.  If the reader of this message is not the intended"
					+ " recipient, or an employee or agent responsible for delivering this"
					+ " message to the intended recipient, you are hereby notified that"
					+ " any dissemination, distribution or copying of this communication"
					+ " is strictly prohibited.  If you have received this communication"
					+ " in error, please notify us immediately by replying to the message"
					+ " and deleting it from your computer."
					+ "\r\n"
					);

				receiverFullName = eInfo.FullName;

				if( this.From == "" )
				{
					this.From = String.Format
						( "\"Do not reply\" <{0}>"
						, eInfo.Email
						);
				}

				this.To = eInfo.Email;
			}
			catch( Exception e )
			{
				throw new EventNotifyException( "Failed to set employee event defaults." , e );
			}
		}

		/// <summary>
		/// Send out whatever is assigned.  If from or to list is not
		/// initialized, then we skip sending.  To field can be a
		/// comma-separated list.
		/// </summary>

		public override void Send()
		{
			// Check if we ought to send.  If so, then away we go.

			if( this.To.TrimWhitespaceAndBOM() != "" )
			{
				base.Send();
			}
		}

	}

	/// <summary>
	/// Inform a user that their account has been locked due to
	/// too many failed login attempts
	/// </summary>
	public class UserAccountLockedEvent : EmployeeNotification
	{
		/// <summary>
		/// Designate a label for this notification for config
		/// matching and event processing.
		/// </summary>
		protected override string m_NotificationEventLabel
		{
			get { return "Admin"; }
		}

		private void InitializeUserMessage(EmployeeDetails eInfo, string lockedLoginName, E_AccountLockedReason reason)
		{
			this.Subject = "Your account has been locked";
			this.To = eInfo.Email;
            this.BrokerId = eInfo.BrokerId;

            if (reason == E_AccountLockedReason.UnsuccessfulLoginAttempts)
            {
                this.Body = String.Format("{0}, \n\nYour LendingQB login account '{1}' has been "
                    + "locked due to too many unsuccessful login attempts.  Please contact your account administrator "
                    + "to have your account unlocked and to reset your password if you cannot remember it.", eInfo.FullName, lockedLoginName);
            }
            else if (reason == E_AccountLockedReason.UnsuccessfulPasswordResetAttempts)
            {
                this.Body = String.Format("{0}, \n\nYour LendingQB login account '{1}' has been "
                    + "locked due to too many unsuccessful password reset requests.  Please contact your account administrator "
                    + "to have your account unlocked and to reset your password if you cannot remember it.  If you did not request "
                    + " to reset your password, please notify your account administrator.", eInfo.FullName, lockedLoginName);
            }
            else
            {
                throw new UnhandledEnumException(reason);
            }

			if( this.From == "" )
			{
				this.From = String.Format
					( "\"Do not reply\" <{0}>"
					, eInfo.Email
					);
			}

			this.Closing = "This notification was automatically generated for you by LendingQB (www.lendingqb.com).  Please do not directly reply to this email.";
		}

		//db - commenting this for now, since a manager doesn't have access to employee accounts.  We may need
		//to update this to an administrator at some point, but as of right now, we do not associated employee
		//accounts with administrators
        /*
        private void InitializeManagerMessage(EmployeeDetails eInfo, string lockedLoginName)
        {
            EmployeeDetails managerInfo = new EmployeeDetails();
            try
            {
                EmployeeDB employee = new EmployeeDB(eInfo.EmployeeId, eInfo.BrokerId);
                employee.Retrieve();
                if(employee.ManagerEmployeeID.Equals(Guid.Empty))
                    return;
                managerInfo = new EmployeeDetails(employee.ManagerEmployeeID);
            }
            catch(Exception e)
            {
                Tools.LogError(string.Format("Unable to send account locked message to manager of employee with locked account {0}", lockedLoginName), e);
            }

            this.Subject = "A supervised account has been locked";

            this.To = managerInfo.Email;

            this.Body = String.Format("{0}, \n\nThe LendingQB login account '{1}' has been "
                + "locked due to too many unsuccessful login attempts.  Please be sure to verify that the valid account owner "
                + "was attempting to access the account before unlocking it.  To unlock the account, "
                + "you can press the \"unlock\" button in the \"System Access\" panel of the Employee Editor.  "
                + "You can also reset the user�s password from the same location.", managerInfo.FullName, lockedLoginName);

            if( this.From == "" )
            {
                this.From = String.Format
                    ( "\"Do not reply\" <{0}>"
                    , eInfo.Email
                    );
            }

            this.Closing = "Please contact your LendingQB system administrator if you have any questions.";
        }*/

        //Take as a parameter the login name in case we want to send the e-mail to someone other than the person whose
		//login is locked (such as their supervisor)
		public void Initialize( Guid employeeId, string lockedLoginName, E_AccountLockedReason reason)
		{
			EmployeeDetails eInfo = new EmployeeDetails();
			if( employeeId != Guid.Empty )
				eInfo.Retrieve( employeeId );
			else
				throw new ArgumentException( "Empty employee id not accepted for UserAccountLockedEvent." );
			InitializeUserMessage(eInfo, lockedLoginName, reason);
		}

		public override void Send()
		{
			if( this.To.TrimWhitespaceAndBOM() != "" )
				base.Send();
		}
	}


	/// <summary>
	/// Send out a welcome notice to the new employee after they
	/// are allowed to login for the first time.
	/// </summary>

	public class NewEmployeeAdded : EmployeeNotification
	{
		/// <summary>
		/// Designate a label for this notification for config
		/// matching and event processing.
		/// </summary>

		protected override string m_NotificationEventLabel
		{
			get { return "Admin"; }
		}

		/// <summary>
		/// Set up body of email.  This letter is pretty generic.
		/// </summary>

		public void Initialize( Guid newEmployeeId )
		{
			// Construct detailed message for new employees.

			try
			{
				// Build up the welcome notice.

				SetDetails( newEmployeeId );

				this.Body = "";
				StringBuilder bodySB = new StringBuilder();

				if( receiverFullName != "" )
				{
					bodySB.AppendFormat
						( "{0},"
						+ "\r\n"
						+ "\r\n"
						, receiverFullName
						);
				}

                bodySB.AppendFormat
                    ( "Your personalized account has been created for LendingQB."
                    + " If you need assistance with your login and password, please contact"
                    + " one of the account administrators at your company."
                    + "\r\n"
                    + "\r\n"
                    + "TRAINING"
                    + "\r\n"
                    + "If you need any personal training from our expert staff, have your system administrator"
                    + " send us an email (training@lendingqb.com) to schedule an appointment."
                    + "\r\n"
                    + "\r\n"
                    + "GETTING ADDITIONAL HELP"
                    + "\r\n"
                    + "If you ever have any problems using LendingQB, we're ready"
                    + " to help you out.  You can find answers in our Knowledgebase"
                    + " by clicking on the \"Support Center\" link in LendingQB"
                    + " (located in the upper right corner of the screen)."
                    + " If you can't find your answer there, please contact your system administrator"
                    + " and they will reach out to LendingQB technical support if they cannot answer your question."
                    + "\r\n"
                    + "\r\n"
                    + "SYSTEM UPDATES"
                    + "\r\n"
                    + "We're constantly adding new features to keep you running at peak"
                    + " efficiency.  Be sure to check out our News page in "
                    + " the Support Center, which we update with every release and hotfix to inform you "
                    + " about the latest changes. After you validate your support center account email, you"
                    + " will receive an email update with every post to the news section."
                    + "\r\n"
                    + "\r\n"
                    + "We appreciate your business and look forward to working with"
                    + " you in the future."
                    );

                this.Subject = "Welcome to LendingQB";
				this.Body = bodySB.ToString();
			}
			catch( Exception e )
			{
				// Oops!

				throw new EventNotifyException( "Bad init for new employee added." , e );
			}
		}
	}
}