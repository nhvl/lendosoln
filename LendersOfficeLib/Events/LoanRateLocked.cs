using System;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Dynamics;
using LendersOffice.Common;
using DataAccess;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
    /// <summary>
    /// Send out email detailing that the specified loan was locked
    /// to the specified rate and fees.
    /// </summary>

    public class LoanRateLocked : LoanNotification
    {
        /// <summary>
        /// Designate a label for this notification for config
        /// matching and event processing.
        /// </summary>

        protected override String m_NotificationEventLabel
        {
            // Access member.

            get
            {
                return "Event";
            }
        }

        /// <summary>
        /// Initialize message with borrower's name and rate and fees
        /// at which  the loan has been locked.  We send to whoever
        /// is specified in the to-list (comma separated list is
        /// accepted).
        /// </summary>

        public void Initialize(string sRate, string sFees, string expirationDate, Guid toEmployeeId, LoanEventData dataLoan, LoanEventData linkedLoan, LoanAssignmentContactTable loanAssignments, E_ApplicationT appCode)
        {
            // Construct detailed message for loan officer to consider.

            try
            {
				string expirationDetails = expirationDate.Length > 0 ? " The rate lock expires on " + expirationDate + ". " : string.Empty;
                // Build up simple message with rate and fees.

                SetDetails(toEmployeeId, dataLoan, linkedLoan, loanAssignments, appCode);

                this.Body = "";
				StringBuilder bodySB = new StringBuilder();

                if( receiverFullName != "" )
                {
                    bodySB.AppendFormat
                        ( "{0},"
                        + "\r\n"
                        + "\r\n"
                        , receiverFullName
                        );
                }

                if( sRate == null )
                {
                    sRate = "";
                }

                if( sFees == null )
                {
                    sFees = "";
                }

                if( primaryBorrowerFullName != "" && loanName != "" && openedDate != "" && sRate != "" && sFees != "" )
                {
                    bodySB.AppendFormat
                        ( "Your loan {0}, which was opened on {1} for {2}, has"
                        + " been locked at {3} rate and {4} points on {5}.{6}You"
                        + " can re-login to the system to review loan status"
                        + " and conditions."
                        , loanName
						, openedDate
                        , primaryBorrowerFullName
                        , sRate
                        , sFees
                        , Tools.GetDateTimeNowString()
						, expirationDetails
                        );
                }
                else
                    if( loanName != "" && openedDate != "" && sRate != "" && sFees != "" )
                {
                    bodySB.AppendFormat
                        ( "Your loan {0}, which was opened on {1}, has been"
                        + " locked at {2} rate and {3} points on {4}.{5}You can"
                        + " re-login to the system to review loan status and"
                        + " conditions."
                        , loanName
						, openedDate
                        , sRate
                        , sFees
                        , Tools.GetDateTimeNowString()
						, expirationDetails
                        );
                }
                else
                    if( primaryBorrowerFullName != "" && sRate != "" && sFees != "" )
                {
                    bodySB.AppendFormat
                        ( "Your loan for {0} has been locked at {1} rate and {2}"
                        + " points on {3}.{4}You can re-login to the system to"
                        + " review loan status and conditions."
                        , primaryBorrowerFullName
                        , sRate
                        , sFees
                        , Tools.GetDateTimeNowString()
						, expirationDetails
                        );
                }
                else
                    if( sRate != "" && sFees != "" )
                {
                    bodySB.AppendFormat
                        ( "One of your loans has been locked at {0} rate and {1}"
                        + " points on {2}.{3}You can re-login to the system to"
                        + " review loan status and conditions."
                        , sRate
                        , sFees
                        , Tools.GetDateTimeNowString()
						, expirationDetails
                        );
                }
                else
                {
                   bodySB.AppendFormat
                        ( "One of your loans has been locked on {0}.{1}You can"
                        + " re-login to the system to review loan status and"
                        + " conditions."
                        , Tools.GetDateTimeNowString()
					   , expirationDetails
                        );
                }

				this.Subject = "Loan rate has been locked"; //av 23816 10 17 08
				this.Body = bodySB.ToString();
            }
            catch( Exception e )
            {
                // Oops!

                throw new EventNotifyException( "Bad init for loan rate locked." , e );
            }
        }

        public static bool Send(
            Guid brokerId,
            Guid loanId,
            string rate,
            string price,
            string expirationDate,
            E_ApplicationT appCode,
            List<E_RoleT> excludedRoles,
            bool continueOnException)
        {
            LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(brokerId, loanId);
            var empList = Utilities.GetLoanAssignments(loanAssignmentTable, excludedRoles);
            LoanEventData primaryLoan = null, linkedLoan = null;
            bool allNotificationsSuccessful = true;

            if (empList.Any())
            {
                primaryLoan = new LoanEventData(loanId);
                primaryLoan.InitLoad();

                if (primaryLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    linkedLoan = new LoanEventData(primaryLoan.sLinkedLoanInfo.LinkedLId);
                    linkedLoan.InitLoad();
                }
            }

            foreach (Guid sendEmployeeId in empList)
            {
                try
                {
                    LoanRateLocked loanEvt = new LoanRateLocked();
                    loanEvt.Initialize(rate, price, expirationDate, sendEmployeeId, primaryLoan, linkedLoan, loanAssignmentTable, appCode);
                    loanEvt.Send();
                }
                catch (Exception e)
                {
                    allNotificationsSuccessful = false;
                    Tools.LogError(e);
                    if (!continueOnException)
                    {
                        throw;
                    }
                }
            }

            return allNotificationsSuccessful;
        }

    }

}
