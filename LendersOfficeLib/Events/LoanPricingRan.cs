using System;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Common;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
	/// <summary>
	/// Using email notify users that a loan they were assigned to was deleted. 
	/// </summary>
	public class LoanPricingRan : LoanNotification
	{
		protected override String m_NotificationEventLabel 
		{
			get { return "Event"; } 
			
		}

		public void Initialize( Guid loanId, string userName, Guid accountExecId ) 
		{
			SetDetails( accountExecId, loanId, E_ApplicationT.PriceMyLoan ); 
            string borrowerName = primaryBorrowerFullName.TrimWhitespaceAndBOM() == string.Empty ? string.Empty : "for " + primaryBorrowerFullName;
			Body = string.Format( "{5},\n\nThe {0} file, which was opened on {1} {2}, has been priced on {3} by {4}. This is the first time the loan has been priced. Note: You will not receive any further pricing notifications for this loan until it has been submitted.", loanName, openedDate, borrowerName, Tools.GetDateTimeDescription( DateTime.Now ), userName, receiverFullName); 
			Subject = "Loan has been priced";
		}
	}
}
