using System;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Dynamics;
using LendersOffice.Common;
using DataAccess;
using System.Text;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Events
{
	/// <summary>
	/// Using email notify users that a loan they were assigned to was deleted. 
	/// </summary>
	public class LoanDeleted : LoanNotification
	{
		protected override String m_NotificationEventLabel 
		{
			get { return "Event"; } 
			
		}

		public void Initialize( Guid loanId, Guid toEmployeeId, bool isLinkedLoan ) 
		{
			SetDetails( toEmployeeId, loanId, E_ApplicationT.LendersOffice ); 
			string borrowerName = primaryBorrowerFullName.TrimWhitespaceAndBOM() == string.Empty ? string.Empty : "for " + primaryBorrowerFullName + " ";
			string linkedLoanText = isLinkedLoan ?  "(This subordinate loan was automatically deleted when the primary loan was re-opened to allow the selection of a new loan program.)" : string.Empty; 
			Body = string.Format( "{5},\n\nThe {4} file that was opened on {0} {1}has been deleted on {2}. {3}", openedDate, borrowerName, Tools.GetDateTimeDescription( DateTime.Now ), linkedLoanText, loanName, receiverFullName); 
			Subject = "Loan has been deleted";
		}
	}
}
