//-----------------------------------------------------------------------
// <summary>
//      Lock Modified Event.
// </summary>
// <copyright file="LoanLockModified.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Events
{
    using System;
    using System.Collections;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Dynamics;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Send out email detailing that the specified loan was lock modified
    /// to the specified rate and fees.
    /// </summary>
    public class LoanLockModified : LoanNotification
    {
        /// <summary>
        /// Designate a label for this notification for config
        /// matching and event processing.
        /// </summary>
        /// <value>Label for this notification.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Naming comes from a class without StyleCop rule enforcement.")]
        protected override string m_NotificationEventLabel
        {
            // Access member.
            get
            {
                return "Event";
            }
        }

        /// <summary>
        /// Initialize message with borrower's name and rate and fees
        /// at which  the loan has been locked.  We send to whoever
        /// is specified in the to-list (comma separated list is
        /// accepted).
        /// </summary>
        /// <param name="rate">Rate chosen.</param>
        /// <param name="fees">Fee chosen.</param>
        /// <param name="employeeId">Employee to send to.</param>
        /// <param name="loanId">Loan's identifier.</param>
        /// <param name="expirationDate">Expiration date.</param>
        /// <param name="appCode">Application code.</param>
        public void Initialize(string rate, string fees, Guid employeeId, Guid loanId, string expirationDate, E_ApplicationT appCode)
        {
            // Construct detailed message for loan officer to consider.
            try
            {
                string expirationDetails = expirationDate.Length > 0 ? " The rate lock expires on " + expirationDate + ". " : string.Empty;

                // Build up simple message with rate and fees.
                this.SetDetails(employeeId, loanId, appCode);

                this.Body = string.Empty;
                StringBuilder bodySB = new StringBuilder();

                if (this.receiverFullName != string.Empty)
                {
                    bodySB.AppendFormat(
                        "{0},"
                        + "\r\n"
                        + "\r\n",
                         this.receiverFullName);
                }

                if (rate == null)
                {
                    rate = string.Empty;
                }

                if (fees == null)
                {
                    fees = string.Empty;
                }

                if (this.primaryBorrowerFullName != string.Empty && this.loanName != string.Empty && this.openedDate != string.Empty && rate != string.Empty && fees != string.Empty)
                {
                    bodySB.AppendFormat(
                        "Your rate lock for loan {0}, which was opened on {1} for {2}, has"
                        + " been modified to {3} rate and {4} points on {5}.{6}You"
                        + " can re-login to the system to review loan status"
                        + " and conditions.",
                         this.loanName,
                         this.openedDate,
                         this.primaryBorrowerFullName,
                         rate,
                         fees,
                         Tools.GetDateTimeNowString(),
                         expirationDetails);
                }
                else
                    if (this.loanName != string.Empty && this.openedDate != string.Empty && rate != string.Empty && fees != string.Empty)
                    {
                        bodySB.AppendFormat(
                            "Your rate lock for loan {0}, which was opened on {1}, has been"
                            + " modified to {2} rate and {3} points on {4}.{5}You can"
                            + " re-login to the system to review loan status and"
                            + " conditions.",
                             this.loanName,
                             this.openedDate,
                             rate,
                             fees,
                             Tools.GetDateTimeNowString(),
                             expirationDetails);
                    }
                    else
                        if (this.primaryBorrowerFullName != string.Empty && rate != string.Empty && fees != string.Empty)
                        {
                            bodySB.AppendFormat(
                                "Your rate lock for {0} has been modified to {1} rate and {2}"
                                + " points on {3}.{4}You can re-login to the system to"
                                + " review loan status and conditions.",
                                 this.primaryBorrowerFullName,
                                 rate,
                                 fees,
                                 Tools.GetDateTimeNowString(),
                                 expirationDetails);
                        }
                        else
                            if (rate != string.Empty && fees != string.Empty)
                            {
                                bodySB.AppendFormat(
                                    "One of your loan rate locks has been modified to {0} rate and {1}"
                                    + " points on {2}.{3}You can re-login to the system to"
                                    + " review loan status and conditions.",
                                    rate,
                                    fees,
                                    Tools.GetDateTimeNowString(),
                                    expirationDetails);
                            }
                            else
                            {
                                bodySB.AppendFormat(
                                    "One of your loan rate locks has been modified on {0}.{1}You can"
                                     + " re-login to the system to review loan status and"
                                     + " conditions.",
                                      Tools.GetDateTimeNowString(),
                                     expirationDetails);
                            }

                this.Subject = "Loan rate lock has been modified";
                this.Body = bodySB.ToString();
            }
            catch (Exception e)
            {
                // Oops!
                throw new EventNotifyException("Bad init for loan rate lock modified.", e);
            }
        }
    }
}
