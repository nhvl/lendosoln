using DataAccess;
using LendersOffice.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using System;
using System.Text;

namespace LendersOffice.Events
{
    /// <summary>
    /// Send out email detailing that the specified loan was submitted
    /// to the associated lender account executive.
    /// </summary>

    public class LoanSubmitted : LoanNotification
    {
        /// <summary>
        /// Designate a label for this notification for config
        /// matching and event processing.
        /// </summary>

        protected override String m_NotificationEventLabel
        {
            get { return "Event"; }
        }

        /// <summary>
        /// Override showing account exec's info because we typically
        /// send this letter to the account exec.  No sense telling
        /// him/her to call him/herself.
        /// </summary>

        protected override bool includeAccountExecInfo
        {
            get { return false; }
        }

        /// <summary>
        /// Construct message details using loan's data.  We form
        /// a message that tells the lender account executive who
        /// is associated with this loan that a loan officer has
        /// submitted the loan.  If no valid lender account exec
        /// is available as part of the loan data, we will just
        /// fizzle out on the send.
        /// </summary>

        public void Initialize( String brokerName , Boolean isRequestingRateLock , Guid toEmployeeId , Guid loanId , E_ApplicationT appCode )
        {
            // Construct detailed message for lender user to consider.

            try
            {
                // If there is no lender account exec, then we can't
                // send the email.  Initialize anyway so that caller
                // can check the parameters and update.

                String requestType = "lock";

                SetDetails( toEmployeeId , loanId , appCode );

                this.Body = "";
				StringBuilder bodySB = new StringBuilder();

                if( isRequestingRateLock == false )
                {
                    requestType = "float";
                }

                if( receiverFullName != "" )
                {
                    bodySB.AppendFormat
                        ( "{0},"
                        + "\r\n"
                        + "\r\n"
                        , receiverFullName
                        );
                }

                if( brokerName == null )
                {
                    brokerName = "";
                }

                if( brokerName != "" && primaryBorrowerFullName != "" && loanName != "" && openedDate != "" && linkedName != "" && linkedDate != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request on {7} for the following"
                        + " linked loans for {6}:"
                        + "\r\n"
                        + "\r\n"
                        + "#1 {2}, which was opened on {3}"
                        + "\r\n"
                        + "#2 {4}, which was opened on {5}"
                        + "\r\n"
                        + "\r\n"
                        + $"Please log into LendingQB "
                        + " to process the request."
                        , brokerName
                        , requestType
                        , loanName
						, openedDate
                        , linkedName
						, linkedDate
                        , primaryBorrowerFullName
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( brokerName != "" && loanName != "" && openedDate != "" && linkedName != "" && linkedDate != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request on {6} for the following"
                        + " linked loans:"
                        + "\r\n"
                        + "\r\n"
                        + "#1 {2}, which was opened on {3}"
                        + "\r\n"
                        + "#2 {4}, which was opened on {5}"
                        + "\r\n"
                        + "\r\n"
                        + $"Please log into LendingQB "
                        + " to process the request."
                        , brokerName
                        , requestType
                        , loanName
						, openedDate
                        , linkedName
						, linkedDate
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( brokerName != "" && primaryBorrowerFullName != "" && loanName != "" && openedDate != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request on {5} for loan {2},"
                        + " which was opened on {3} for {4}. Please log into LendingQB "
                        + $" to process the request."
                        , brokerName
                        , requestType
                        , loanName
						, openedDate
                        , primaryBorrowerFullName
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( brokerName != "" && loanName != "" && openedDate != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request on {4} for loan {2},"
                        + " which was opened on {3}. Please log into LendingQB"
                        + " to process the request."
                        , brokerName
                        , requestType
                        , loanName
						, openedDate
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( brokerName != "" && primaryBorrowerFullName != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request for {2} on {3}. Please"
                        + " log into LendingQB to"
                        + " process the request."
                        , brokerName
                        , requestType
                        , primaryBorrowerFullName
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( primaryBorrowerFullName != "" )
                {
                    bodySB.AppendFormat
                        ( "A rate {0} request for {1} on {2} has been submitted. Please"
                        + " log into LendingQB to"
                        + " process the request."
                        , requestType
                        , primaryBorrowerFullName
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                    if( brokerName != "" )
                {
                    bodySB.AppendFormat
                        ( "{0} has submitted a rate {1} request on {2}. Please log into"
                        + " LendingQB  to process"
                        + " the request."
                        , brokerName
                        , requestType
                        , Tools.GetDateTimeNowString()
                        );
                }
                else
                {
                   bodySB.AppendFormat
                        ( "A rate {0} request for one of your loans has been submitted"
                        + "on {1}. Please log into LendingQB"
                        + " to process the request."
                        , requestType
                        , Tools.GetDateTimeNowString()
                        );
                }

                this.Subject = "";
				StringBuilder subjectSB = new StringBuilder();
                if (this.aBLastNm != "") 
                {
                    subjectSB.Append(this.aBLastNm + " - ");
                }
                subjectSB.Append("Loan has been submitted");
				this.Subject = subjectSB.ToString();
				this.Body = bodySB.ToString();
            }
            catch( Exception e )
            {
                // Oops!

                throw new EventNotifyException( "Bad init for loan submitted." , e );
            }
        }
    }
}