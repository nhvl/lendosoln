﻿// <copyright file="UserDbSearchItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   10/20/2014 5:26:31 PM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// User information display in search result.
    /// </summary>
    public class UserDbSearchItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbSearchItem" /> class.
        /// </summary>
        /// <param name="reader">The sql data reader contains info.</param>
        public UserDbSearchItem(DbDataReader reader)
        {
            this.EmployeeId = (Guid)reader["EmployeeId"];
            this.FullName = (string)reader["FullName"];
            this.Email = (string)reader["Email"];
            this.LoginNm = (string)reader["LoginNm"];
            this.Type = (string)reader["Type"];
            this.BranchNm = (string)reader["BranchNm"];
            this.BrokerNm = (string)reader["BrokerNm"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.UserId = reader.SafeGuid("UserId");
            this.RecentLoginD = reader.SafeDateTime("RecentLoginD");
            this.CanLogin = (bool)reader["CanLogin"];
            this.IsActive = (bool)reader["IsActive"];
            this.BrokerPmlSiteId = (Guid)reader["BrokerPmlSiteId"];
            this.EnableRetailTpoPortalMode = (bool)reader["EnableRetailTpoPortalMode"];
        }

        /// <summary>
        /// Gets the employee id.
        /// </summary>
        /// <value>The employee id.</value>
        public Guid EmployeeId { get; private set; }

        /// <summary>
        /// Gets the full name of user.
        /// </summary>
        /// <value>The full name of user.</value>
        public string FullName { get; private set; }

        /// <summary>
        /// Gets the email of user.
        /// </summary>
        /// <value>The email of user.</value>
        public string Email { get; private set; }

        /// <summary>
        /// Gets the login name of user.
        /// </summary>
        /// <value>The login name of user.</value>
        public string LoginNm { get; private set; }

        /// <summary>
        /// Gets the type of user.
        /// </summary>
        /// <value>The type of user.</value>
        public string Type { get; private set; }

        /// <summary>
        /// Gets the branch user belong.
        /// </summary>
        /// <value>The branch user belong.</value>
        public string BranchNm { get; private set; }

        /// <summary>
        /// Gets the broker user belong.
        /// </summary>
        /// <value>The broker user belong.</value>
        public string BrokerNm { get; private set; }

        /// <summary>
        /// Gets the broker id user belong.
        /// </summary>
        /// <value>The broker id user belong.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public Guid UserId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether user is active.
        /// </summary>
        /// <value>Whether user is active.</value>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets the date user most recent login.
        /// </summary>
        /// <value>Gets the user user most recent login.</value>
        public DateTime RecentLoginD { get; private set; }

        /// <summary>
        /// Gets a value indicating whether user can login.
        /// </summary>
        /// <value>Whether user can login.</value>
        public bool CanLogin { get; private set; }

        /// <summary>
        /// Gets the user broker pml site id.
        /// </summary>
        /// <value>The user broker pml site id.</value>
        public Guid BrokerPmlSiteId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user's broker enables
        /// the retail TPO portal.
        /// </summary>
        /// <value>
        /// True if the retail portal is enabled, false otherwise.
        /// </value>
        public bool EnableRetailTpoPortalMode { get; private set; }
    }
}