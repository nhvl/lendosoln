﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTablePricingEngine : AdminPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanApplyForIneligibleLoanPrograms:
                case Permission.CanRunPricingEngineWithoutCreditReport:
                case Permission.CanSubmitWithoutCreditReport:
                case Permission.CanViewHiddenInformation: //OPM 18269 db - 10/04/07
                case Permission.AllowBypassAlwaysUseBestPrice: //OPM 47238 vm - 04/07/10
                    return true;
            }
            return false;
        }
        #endregion
    }
}
