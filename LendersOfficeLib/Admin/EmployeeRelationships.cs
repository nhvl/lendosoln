﻿// <copyright file="EmployeeRelationships.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: ? geoffreyf refactored
//    Date:   10/2/2014
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Relationships;
    using LendersOffice.Security;

    /// <summary>
    /// List out all the relationships (with type and label) for the
    /// retrieving employee.
    /// </summary>
    public class EmployeeRelationships
    {
        /// <summary>
        /// A collection of the fields we need to load for the LendingQB
        /// relationships.
        /// </summary>
        private static ReadOnlyCollection<RelationshipLoadInfo> lendingQBRelationshipFields = new List<RelationshipLoadInfo>()
        {
            new RelationshipLoadInfo("LenderAccExecEmployeeId", E_RoleT.LenderAccountExecutive, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("LockDeskEmployeeId", E_RoleT.LockDesk, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("ManagerEmployeeId", E_RoleT.Manager, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("UnderwriterEmployeeId", E_RoleT.Underwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("JuniorUnderwriterEmployeeId", E_RoleT.JuniorUnderwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("ProcessorEmployeeId", E_RoleT.Processor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("JuniorProcessorEmployeeId", E_RoleT.JuniorProcessor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("LoanOfficerAssistantEmployeeId", E_RoleT.LoanOfficerAssistant, E_RelationshipType.TeamMember),
        }.AsReadOnly();

        /// <summary>
        /// A collection of the fields we need to load for the PML Broker relationships.
        /// </summary>
        private static ReadOnlyCollection<RelationshipLoadInfo> pmlBrokerFields = new List<RelationshipLoadInfo>()
        {
            new RelationshipLoadInfo("LenderAccExecEmployeeId", E_RoleT.LenderAccountExecutive, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("LockDeskEmployeeId", E_RoleT.LockDesk, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("ManagerEmployeeId", E_RoleT.Manager, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("UnderwriterEmployeeId", E_RoleT.Underwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("JuniorUnderwriterEmployeeId", E_RoleT.JuniorUnderwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("ProcessorEmployeeId", E_RoleT.Processor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("JuniorProcessorEmployeeId", E_RoleT.JuniorProcessor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("BrokerProcessorEmployeeId", E_RoleT.Pml_BrokerProcessor, E_RelationshipType.TeamMember)
        }.AsReadOnly();

        /// <summary>
        /// A collection of the fields we need to load for the PML Mini 
        /// Correspondent relationships.
        /// </summary>
        private static ReadOnlyCollection<RelationshipLoadInfo> pmlMiniCorrespondentFields = new List<RelationshipLoadInfo>()
        {
            new RelationshipLoadInfo("MiniCorrManagerEmployeeId", E_RoleT.Manager, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("MiniCorrProcessorEmployeeId", E_RoleT.Processor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("MiniCorrJuniorProcessorEmployeeId", E_RoleT.JuniorProcessor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("MiniCorrLenderAccExecEmployeeId", E_RoleT.LenderAccountExecutive, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("MiniCorrExternalPostCloserEmployeeId", E_RoleT.Pml_PostCloser, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("MiniCorrUnderwriterEmployeeId", E_RoleT.Underwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrJuniorUnderwriterEmployeeId", E_RoleT.JuniorUnderwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrCreditAuditorEmployeeId", E_RoleT.CreditAuditor, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrLegalAuditorEmployeeId", E_RoleT.LegalAuditor, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrLockDeskEmployeeId", E_RoleT.LockDesk, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrPurchaserEmployeeId", E_RoleT.Purchaser, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("MiniCorrSecondaryEmployeeId", E_RoleT.Secondary, E_RelationshipType.TeamHelper),
        }.AsReadOnly();

        /// <summary>
        /// A collection of the fields we need to load for the PML Correspondent
        /// relationships.
        /// </summary>
        private static ReadOnlyCollection<RelationshipLoadInfo> pmlCorrespondentFields = new List<RelationshipLoadInfo>()
        {
            new RelationshipLoadInfo("CorrManagerEmployeeId", E_RoleT.Manager, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("CorrProcessorEmployeeId", E_RoleT.Processor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("CorrJuniorProcessorEmployeeId", E_RoleT.JuniorProcessor, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("CorrLenderAccExecEmployeeId", E_RoleT.LenderAccountExecutive, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("CorrExternalPostCloserEmployeeId", E_RoleT.Pml_PostCloser, E_RelationshipType.TeamMember),
            new RelationshipLoadInfo("CorrUnderwriterEmployeeId", E_RoleT.Underwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrJuniorUnderwriterEmployeeId", E_RoleT.JuniorUnderwriter, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrCreditAuditorEmployeeId", E_RoleT.CreditAuditor, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrLegalAuditorEmployeeId", E_RoleT.LegalAuditor, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrLockDeskEmployeeId", E_RoleT.LockDesk, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrPurchaserEmployeeId", E_RoleT.Purchaser, E_RelationshipType.TeamHelper),
            new RelationshipLoadInfo("CorrSecondaryEmployeeId", E_RoleT.Secondary, E_RelationshipType.TeamHelper),
        }.AsReadOnly();

        /// <summary>
        /// A map from the different types of relationship sets to the fields 
        /// required to load each set.
        /// </summary>
        private static Dictionary<RelationshipSetType, ReadOnlyCollection<RelationshipLoadInfo>> loadInfoForRelationshipSetType =
            new Dictionary<RelationshipSetType, ReadOnlyCollection<RelationshipLoadInfo>>()
        {
            { RelationshipSetType.LendingQB, lendingQBRelationshipFields },
            { RelationshipSetType.Pml_Broker, pmlBrokerFields },
            { RelationshipSetType.Pml_MiniCorrespondent, pmlMiniCorrespondentFields },
            { RelationshipSetType.Pml_Correspondent, pmlCorrespondentFields },
        };

        /// <summary>
        /// A map from relationship set type to relationship set.
        /// </summary>
        private Dictionary<RelationshipSetType, RelationshipSet> relationshipSetsByType;

        /// <summary>
        /// The type of the user. If there is no user record associated with 
        /// the employee, this will be '?'.
        /// </summary>
        private char userType;

        /// <summary>
        /// Gets the ID of the broker of the employee.
        /// </summary>
        /// <value>The ID of the broker of the employee.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the ID of the employee associated with the relationships.
        /// </summary>
        /// <value>The ID of the employee associated with the relationships.</value>
        public Guid EmployeeId { get; private set; }

        /// <summary>
        /// Sets the value of the IDataRecord from which the relationships 
        /// are constructed. Setting this field will cause all of the relationship
        /// settings to be re-initialized.
        /// </summary>
        /// <value>
        /// The IDataRecord from which the relationships are constructed. Will
        /// be null before retrieve.
        /// </value>
        public IDataRecord Record
        {
            set
            {
                this.relationshipSetsByType = new Dictionary<RelationshipSetType, RelationshipSet>();

                foreach (var setType in loadInfoForRelationshipSetType.Keys)
                {
                    var relationshipSet = new RelationshipSet();

                    foreach (var loadInfo in loadInfoForRelationshipSetType[setType])
                    {
                        object o = value[loadInfo.FieldID];

                        if (o != null && !Convert.IsDBNull(o))
                        {
                            relationshipSet.Add(
                                loadInfo.Role,
                                loadInfo.RelationshipType,
                                (Guid)o);
                        }
                    }

                    this.relationshipSetsByType.Add(setType, relationshipSet);
                }

                this.EmployeeId = (Guid)value["EmployeeId"];
                this.BrokerId = (Guid)value["BrokerId"];
                this.userType = Convert.IsDBNull(value["UserType"]) ? '?' : Convert.ToChar(value["UserType"]);
            }
        }

        /// <summary>
        /// Gets the relationship set for the given set type.
        /// </summary>
        /// <param name="setType">The target relation set type.</param>
        /// <returns>
        /// The relationship set for the type. Will return an empty relationship
        /// set, not null, when no relationships of the given type are set.
        /// </returns>
        public RelationshipSet this[RelationshipSetType setType]
        {
            get { return this.relationshipSetsByType[setType]; }
        }

        /// <summary>
        /// Retrieve a loan's employee assignments with descriptions of
        /// the roles they fulfill for this loan.
        /// </summary>
        /// <param name="brokerId">The ID of the broker for the employee.</param>
        /// <param name="employeeId">The ID of the employee.</param>
        public void Retrieve(Guid brokerId, Guid employeeId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@EmployeeId", employeeId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(
                brokerId,
                "ListEmployeeRelationshipsByEmployeeId",
                parameters))
            {
                if (reader.Read())
                {
                    this.Record = reader;
                }
                else
                {
                    throw new ArgumentException(
                        "Unable to load relationships for employee " + 
                        employeeId + ".");
                }
            }
        }

        /// <summary>
        /// Gets a set of relationships that should be applied to a loan file at submission.
        /// </summary>
        /// <param name="loanAssignments">The current employee assignments for the loan file.</param>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="correspondentProcessT">The correspondent process type of the loan.</param>
        /// <param name="includeLockDesk">Indicates whether the Lock Desk role should be included.</param>
        /// <returns>A RelationshipSet that should be applied to the loan file.</returns>
        public RelationshipSet GetApplicableRelationshipsForSubmission(
            LoanAssignmentContactTable loanAssignments,
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT,
            bool includeLockDesk)
        {
            var relationships = this.GetApplicableRelationshipSet(
                channelT,
                correspondentProcessT);

            if (this.userType == 'P' && channelT == E_BranchChannelT.Correspondent &&
                correspondentProcessT != E_sCorrespondentProcessT.MiniCorrespondent &&
                correspondentProcessT != E_sCorrespondentProcessT.Blank &&
                correspondentProcessT != E_sCorrespondentProcessT.PriorApproved)
            {
                relationships = relationships.Where(
                    r => r.RoleT != E_RoleT.JuniorUnderwriter && r.RoleT != E_RoleT.Underwriter);
            }

            if (includeLockDesk)
            {
                return relationships.Where(r => !loanAssignments.Contains(r.RoleT) &&
                    r.Type == E_RelationshipType.TeamHelper);
            }
            else
            {
                return relationships.Where(r => !loanAssignments.Contains(r.RoleT) &&
                    r.RoleT != E_RoleT.LockDesk &&
                    r.Type == E_RelationshipType.TeamHelper);
            }
        }

        /// <summary>
        /// Gets the relationships to be assigned at loan file creation.
        /// </summary>
        /// <param name="creatingEmployeeRoles">The roles of the creating employee.</param>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="correspondentProcessT">The correspondent process type of the loan.</param>
        /// <returns>A set of relationships that should be assigned to the loan file at creation.</returns>
        public RelationshipSet GetApplicableRelationshipsForCreation(
            EmployeeRoles creatingEmployeeRoles,
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            var relationships = this.GetApplicableRelationshipSet(
                channelT,
                correspondentProcessT);

            return relationships.Where(r => r.Type == E_RelationshipType.TeamMember && 
                !creatingEmployeeRoles.IsInRole(r.RoleT));
        }

        /// <summary>
        /// Gets the relationship set that should be used for the given channel
        /// and correspondent process type.
        /// </summary>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="correspondentProcessT">The correspondent process type of the loan.</param>
        /// <returns>
        /// The relationship set that should be used for the given channel and 
        /// correspondent process type.
        /// </returns>
        public RelationshipSet GetApplicableRelationshipSet(
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            if (this.userType == 'B' || this.userType == '?')
            {
                return this.relationshipSetsByType[RelationshipSetType.LendingQB];
            }
            else if (this.userType == 'P')
            {
                if (channelT == E_BranchChannelT.Correspondent)
                {
                    if (correspondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                    {
                        return this.relationshipSetsByType[RelationshipSetType.Pml_MiniCorrespondent];
                    }
                    else
                    {
                        return this.relationshipSetsByType[RelationshipSetType.Pml_Correspondent];
                    }
                }
                else
                {
                    return this.relationshipSetsByType[RelationshipSetType.Pml_Broker];
                }
            }
            else
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Cannot access relationships of user that is not a B-User or a P-User.");
            }
        }

        /// <summary>
        /// Container for the information required for loading a relationship.
        /// </summary>
        private struct RelationshipLoadInfo
        {
            /// <summary>
            /// The ID to check in the Record.
            /// </summary>
            public readonly string FieldID;

            /// <summary>
            /// The Role associated with the FieldID.
            /// </summary>
            public readonly E_RoleT Role;

            /// <summary>
            /// The type of relationship.
            /// </summary>
            public readonly E_RelationshipType RelationshipType;

            /// <summary>
            /// Initializes a new instance of the <see cref="RelationshipLoadInfo" /> struct.
            /// </summary>
            /// <param name="fieldId">The ID of the field to look for in the Record.</param>
            /// <param name="role">The role for the relationship.</param>
            /// <param name="relationshipType">The type of the relationship.</param>
            public RelationshipLoadInfo(string fieldId, E_RoleT role, E_RelationshipType relationshipType)
            {
                this.FieldID = fieldId;
                this.Role = role;
                this.RelationshipType = relationshipType;
            }
        }
    }
}
