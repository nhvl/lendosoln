﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LendersOffice.Admin
{
    /// <summary>
    /// We track loaded contact information and reload after they get
    /// stale.  The cache will hold any employee from any broker.
    /// Evictions occur when the limit is reached.  Requested employee
    /// is always returned.
    /// </summary>

    public class BrokerEmployeeNameCache
    {
        /// <summary>
        /// We track loaded contact information and reload after
        /// they get stale.  The cache will hold any employee
        /// from any broker.  Evictions occur when the limit is
        /// reached.  Requested employee is always returned.
        /// </summary>

        private Hashtable m_Table = new Hashtable();
        private ArrayList m_Queue = new ArrayList();
        private Int32 m_Limit = Int32.MinValue;
        private TimeSpan m_Stale = TimeSpan.FromSeconds(30);

        public EmployeeDetails this[Guid employeeId]
        {
            // Access member.

            get
            {
                // Pull from our cache.  If stale, we nix it and pretend
                // we never found anything.  If we need to pull a spec
                // from the db, we will (fresh) and cache it if there's
                // room.  We'll make room if we have to.

                AgingEmployeeDetails eS = m_Table[employeeId] as AgingEmployeeDetails;

                if (eS != null && eS.CachedOn.Add(m_Stale).CompareTo(DateTime.Now) <= 0)
                {
                    // Stale...  Evict!

                    m_Table.Remove(employeeId);

                    m_Queue.Remove(employeeId);

                    eS = null;
                }

                if (eS == null)
                {
                    // Load a new one and insert in our cache.

                    eS = new AgingEmployeeDetails();

                    try
                    {
                        eS.Retrieve(employeeId);
                    }
                    catch (Exception e)
                    {
                        // Oops!

                        throw new ArgumentException("Failed to retrieve employee on cache hit.", e);
                    }

                    while (m_Queue.Count >= m_Limit && m_Limit > 0)
                    {
                        m_Table.Remove(m_Queue[0]);

                        m_Queue.RemoveAt(0);
                    }

                    if (m_Limit > 0)
                    {
                        m_Table.Add(employeeId, eS);

                        m_Queue.Add(employeeId);
                    }

                    eS.CachedOn = DateTime.Now;
                }

                return eS;
            }
        }

        /// <summary>
        /// Keep track of individual employee entries in our
        /// cache of aging details.
        /// </summary>

        public class AgingEmployeeDetails : EmployeeDetails
        {
            /// <summary>
            /// Keep track of individual employee entries in
            /// our cache.
            /// </summary>

            private DateTime m_CachedOn = DateTime.MinValue;

            #region ( Aging properties )

            public DateTime CachedOn
            {
                // Access member.

                set
                {
                    m_CachedOn = value;
                }
                get
                {
                    return m_CachedOn;
                }
            }

            #endregion

        }

    }
}
