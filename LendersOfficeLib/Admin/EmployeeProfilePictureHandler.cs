﻿using System.IO;
using System.Reflection;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    public class EmployeeProfilePictureHandler : System.Web.IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        private byte[] GetEmptyProfile()
        {
            Assembly assembly = typeof(EmployeeProfilePictureHandler).Assembly;
            using (Stream stream = assembly.GetManifestResourceStream("LendersOffice.Admin.empty_profile_picture.gif"))
            {
                if (stream == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "LendersOffice.Admin.empty_profile_picture.gif embedded resource is not found.");
                }
                return Tools.ReadFully(stream, 1000);
            }
        }
        public void ProcessRequest(System.Web.HttpContext context)
        {
            string imageId = RequestHelper.GetSafeQueryString("imageid");

            // 6/13/2013 dd - The generation of the key is match with $LendersOfficeApp/los/UploadProfileImage.aspx
            string key = imageId + ".profilepic.jpg";

            byte[] image = null;

            try
            {
                image = FileDBTools.ReadData(E_FileDB.Normal, key);
            }
            catch (FileNotFoundException)
            {
            }

            context.Response.Clear();
            context.Response.ContentType = "image/jpeg";

            if (image != null)
            {
                context.Response.BinaryWrite(image);
            }
            else
            {
                context.Response.BinaryWrite(GetEmptyProfile());
            }
            context.Response.End();

        }

        #endregion
    }
}