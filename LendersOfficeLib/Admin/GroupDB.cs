﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLib;
using DataAccess;
using ConfigSystem;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using CommonProjectLib.Common.Lib;
using System.Web;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public enum GroupType { Employee=1, Branch=2, PmlBroker=3}
    
    public static class GroupDB
    {
        #region Basic Group operations

        public static Guid CreateGroup(Guid brokerId, string name, GroupType type, string description, AbstractUserPrincipal principal = null)
        {
            // principal should only be null when unit tests are being run or default broke settings are being set.
            if (principal != null && principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            SqlParameter paramGuid = new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier);
            paramGuid.Direction = ParameterDirection.Output;

            SqlParameter[] parameters = new SqlParameter[]{
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@GroupName", name.Trim()),
                    new SqlParameter("@GroupType", type),
                    new SqlParameter("@Description", description.Trim()),
                    paramGuid
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "CreateGroup", 0, parameters);

            string groupName = type.ToString();
            if (type == GroupType.PmlBroker)
            {
                groupName = "OC";
            }

            if (principal != null)
            {
                string auditDesc = $"{groupName} Group \"{name}\" added.";

                WorkflowChangeAudit audit = new WorkflowChangeAudit(brokerId, principal, WorkflowAuditType.CreateGroup, auditDesc);
                audit.Save();
            }

            return (Guid)paramGuid.Value;
        }

        /// <summary>
        /// The group should not have any member for deletion
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        public static void DeleteGroup(Guid brokerId, Guid groupId, Group group = null, AbstractUserPrincipal principal = null)
        {
            // principal should only be null when unit tests are being run or default broke settings are being set.
            if (principal != null && principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            SqlParameter[] parameters = {
                 new SqlParameter("@BrokerId", brokerId),
                 new SqlParameter("@GroupId", groupId)
             };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeleteGroup", 0, parameters);

            if (group != null && principal != null)
            {
                string groupName = group.GroupType.ToString();
                if (group.GroupType == GroupType.PmlBroker)
                {
                    groupName = "OC";
                }

                string auditDesc = $"{groupName} Group \"{group.Name}\" deleted.";

                WorkflowChangeAudit audit = new WorkflowChangeAudit(brokerId, principal, WorkflowAuditType.DeleteGroup, auditDesc);
                audit.Save();
            }            
        }

        /// <summary>
        /// Update a group
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="sGroupType"></param>
        /// <param name="sOldName"></param>
        /// <param name="sNewName"></param>
        /// <param name="sOldDesc"></param>
        /// <param name="sNewDesc"></param>
        public static void UpdateGroup(Guid brokerId, AbstractUserPrincipal principal, Guid groupId, string sGroupType, string sOldName, string sNewName, string sOldDesc, string sNewDesc)
        {
            // principal should only be null when unit tests are being run or default broke settings are being set.
            if (principal != null && principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            Guid userId = principal.UserId;

            //If the group is being renamed, rename the group in the release and draft config in the same transaction
            //if (sOldName != sNewName)
            using (var exec = new CommonProjectLib.Database.CStoredProcedureExec(SqlConnectionManager.Get(brokerId)))
            {
                exec.BeginTransactionForWrite();
                try
                {
                    // Trim New Name.
                    string trimmedName = sNewName.Trim();
                    string trimmedDesc = sNewDesc.Trim();

                    //Update the group
                    SqlParameter[] paramArr = new SqlParameter[]{
                         new SqlParameter("@BrokerId", brokerId),
                         new SqlParameter("@GroupId", groupId),
                         new SqlParameter("@GroupName", trimmedName),
                         new SqlParameter("@Description", trimmedDesc)};
                    
                    exec.ExecuteNonQuery("UpdateGroup", 0, paramArr);

                    IConfigRepository configRepo = ConfigHandler.GetRepository(exec);

                    //Rename group name in release config and save
                    SystemConfig releaseConfig = configRepo.LoadActiveRelease(brokerId).Configuration;
                    if (releaseConfig.DoesExistNonParameter(E_TypeToRename.Value, sGroupType, sOldName))
                    {
                        if(trimmedName != sOldName) releaseConfig.RenameNonParameter(E_TypeToRename.Value, sGroupType, sOldName, trimmedName);
                        if(trimmedDesc != sOldDesc) releaseConfig.RenameNonParameter(E_TypeToRename.ValueDesc, sGroupType, sOldDesc, trimmedDesc);
                        configRepo.SaveReleaseConfig(brokerId, releaseConfig, userId);
                    }

                    //Rename group name in draft config and save
                    SystemConfig draftConfig = configRepo.LoadDraft(brokerId).Configuration;
                    if (draftConfig.DoesExistNonParameter(E_TypeToRename.Value, sGroupType, sOldName))
                    {                            
                        if(trimmedName != sOldName) draftConfig.RenameNonParameter(E_TypeToRename.Value, sGroupType, sOldName, trimmedName);
                        if(trimmedDesc != sOldDesc) draftConfig.RenameNonParameter(E_TypeToRename.ValueDesc, sGroupType, sOldDesc, trimmedDesc);
                        configRepo.SaveDraftConfig(brokerId, draftConfig);
                    }
                    exec.CommitTransaction();
                }
                catch (Exception)
                {
                    exec.RollbackTransaction();
                    throw;
                }                

                string auditDesc = $"{sGroupType.Replace("Group", string.Empty)} Group \"{sOldName}\" renamed to \"{sNewName}\".";
                
                WorkflowChangeAudit audit = new WorkflowChangeAudit(brokerId, principal, WorkflowAuditType.RenameGroup, auditDesc);
                audit.Save();
            }
        }

        public static bool DoesGroupExistInConfig(Guid brokerId, string sGroupType, string sGroupName)
        {
            IConfigRepository configRepository = ConfigHandler.GetRepository(brokerId);

            BasicConfigData releaseConfig = configRepository.LoadActiveRelease(brokerId);
            if (releaseConfig.Configuration.DoesExistNonParameter(E_TypeToRename.Value, sGroupType, sGroupName))
                return true;

            BasicConfigData draftConfig = configRepository.LoadDraft(brokerId);
            if (draftConfig.Configuration.DoesExistNonParameter(E_TypeToRename.Value, sGroupType, sGroupName))
                return true;

            return false;
        }        

        public static IEnumerable<Group> GetAllGroups(Guid brokerId, GroupType type)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupType", type)
            };

            List<Group> list = new List<Group>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FetchAllGroups", parameters))
            {
                while (reader.Read())
                {
                    Group item = new Group();
                    item.Id = (Guid)reader["GroupId"];
                    item.Name = (string)reader["GroupName"];
                    item.Description = (string)reader["Description"];
                    item.GroupType = (GroupType)((int)reader["GroupType"]);

                    list.Add(item);
                }
            }

            return list;
        }

        public static IEnumerable<Tuple<Guid, string>> GetAllGroupIdsAndNames(Guid brokerId, GroupType type)
        {
            IEnumerable<Group> groupList = GetAllGroups(brokerId, type);
            foreach (var group in groupList)
            {
                yield return Tuple.Create(group.GroupId, group.GroupName);
            }
        }

        /// <summary>
        /// Gets a group's name by its ID.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the group's broker.
        /// </param>
        /// <param name="groupId">
        /// The ID of the group.
        /// </param>
        /// <returns>
        /// The name of the group.
        /// </returns>
        public static string GetGroupNameById(Guid brokerId, Guid groupId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupId", groupId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetGroupNameById", parameters))
            {
                if (reader.Read())
                {
                    return (string)reader["GroupName"];
                }
            }

            throw new CBaseException("Group not found.", "Group not found. GroupId=" + groupId.ToString());
        }

        public static Group GetGroupById(Guid brokerId, Guid groupId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", groupId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetGroupById", parameters))
            {
                if (reader.Read())
                {
                    Group g = new Group();

                    g.Id = (Guid)reader["GroupId"];
                    g.Name = (string)reader["GroupName"];
                    g.Description = (string)reader["Description"];
                    g.GroupType = (GroupType)((int)reader["GroupType"]);

                    return g;
                }
                
            }

            throw new CBaseException("Group not found.", "Group not found. GroupId=" + groupId.ToString());
        }

        public static bool IfGroupNameExist(Guid brokerId, GroupType type, string sName)
        {
            IEnumerable<Group> groupList = GetAllGroups(brokerId, type);

            foreach (Group group in groupList)
            {
                if (group.GroupName == sName)
                {
                    return true;
                }
                
            }

            return false;
        }

        public static int GetGroupMembersCount(Guid brokerId, Guid groupId)
        {
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Guid);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            SqlParameter[] paramArr = new SqlParameter[]{
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@GroupId", groupId),                    
                    paramReturn
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "GetGroupMembersCount", 0, paramArr);
            return (int)paramReturn.Value;
        }

        #endregion

        #region Fetch operations for group memberships       

        /// <summary>
        /// Gets all the employees with employee's group membership for a given group.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <returns>The value of first column "IsInGroup" in the returned data table is 1 or 0 depending on whether the employee belongs to the given group</returns>
        public static IEnumerable<GroupMembershipEmployee> GetEmployeesWithGroupMembership(Guid brokerId, Guid groupId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetEmployeesWithGroupMembership with brokerid = " + brokerId + " and groupId = " + groupId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupId", groupId)
            };

            List<GroupMembershipEmployee> list = new List<GroupMembershipEmployee>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeesWithGroupMembership", parameters))
            {
                while (reader.Read())
                {
                    GroupMembershipEmployee item = new GroupMembershipEmployee();

                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.EmployeeId = (Guid)reader["EmployeeId"];
                    item.UserFirstNm = (string)reader["UserFirstNm"];
                    item.UserLastNm = (string)reader["UserLastNm"];
                    item.LoginNm = (string)reader["LoginNm"];
                    item.BranchNm = (string)reader["BranchNm"];

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Gets all the branches with employee's group membership for a given group.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <returns>The value of first column "IsInGroup" in the returned data table is 1 or 0 depending on whether the branch belongs to the given group</returns>
        public static IEnumerable<GroupMembershipBranch> GetBranchesWithGroupMembership(Guid brokerId, Guid groupId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetBranchesWithGroupMembership with brokerid = " + brokerId + " and groupId = " + groupId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupId", groupId)
            };

            List<GroupMembershipBranch> list = new List<GroupMembershipBranch>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetBranchesWithGroupMembership", parameters))
            {
                while (reader.Read())
                {
                    GroupMembershipBranch item = new GroupMembershipBranch();
                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.BranchId = (Guid)reader["BranchId"];
                    item.BranchCode = (string)reader["BranchCode"];
                    item.BranchNm = (string)reader["BranchNm"];
                    item.BranchAddr = (string)reader["BranchAddr"];
                    item.BranchCity = (string)reader["BranchCity"];
                    item.BranchState = (string)reader["BranchState"];
                    item.BranchZip = (string)reader["BranchZip"];

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Gets all the branches with PmlBroker's group membership for a given group.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <returns>The value of first column "IsInGroup" in the returned data table is 1 or 0 depending on whether the PmlBroker belongs to the given group</returns>
        public static IEnumerable<GroupMembershipPmlBroker> GetPmlBrokersWithGroupMembership(Guid brokerId, Guid groupId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetPmlBrokersWithGroupMembership with brokerid = " + brokerId + " and groupId = " + groupId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupId", groupId)
            };

            List<GroupMembershipPmlBroker> list = new List<GroupMembershipPmlBroker>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetPmlBrokersWithGroupMembership", parameters))
            {
                while (reader.Read())
                {
                    GroupMembershipPmlBroker item = new GroupMembershipPmlBroker();
                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.Name = (string)reader["Name"];
                    item.PmlBrokerId = (Guid)reader["PmlBrokerId"];
                    item.Addr = (string)reader["Addr"];
                    item.City = (string)reader["City"];
                    item.State = (string)reader["State"];
                    item.Zip = (string)reader["Zip"];

                    list.Add(item);
                }
            }

            return list;
        }

        public static ILookup<Guid, GroupEmployeeRecord> GetBrokerEmployeeGroupMembershipByEmployeeId(Guid brokerId)
        {
            IEnumerable<SqlParameter> parameters = Enumerable.Repeat(new SqlParameter("@BrokerId", brokerId), 1);
            List<GroupEmployeeRecord> brokerGroupMembership = new List<GroupEmployeeRecord>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeeGroupMembershipForBroker", parameters))
            {
                while (reader.Read())
                {
                    GroupEmployeeRecord record = new GroupEmployeeRecord(reader);
                    brokerGroupMembership.Add(record);
                }
            }

            return brokerGroupMembership.ToLookup(p => p.EmployeeId);
        }

        /// <summary>
        /// Gets all the groups with group membership for a given employee.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <returns>The value of first column "IsInGroup" in the returned data table is 1 or 0 depending on whether the given employee belongs to the group</returns>
        public static IEnumerable<MembershipGroup> GetGroupMembershipsForEmployee(Guid brokerId, Guid employeeId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetGroupMembershipsForEmployee with brokerid = " + brokerId + " and employeeid = " + employeeId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@EmployeeId", employeeId)
            };

            List<MembershipGroup> list = new List<MembershipGroup>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetGroupMembershipsForEmployee", parameters))
            {
                while (reader.Read())
                {
                    MembershipGroup item = new MembershipGroup();
                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.GroupId = (Guid)reader["GroupId"];
                    item.GroupName = (string)reader["GroupName"];

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Return inclusive employee group
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<Group> ListInclusiveGroupForEmployee(Guid brokerId, Guid employeeId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] ListInclusiveGroupForEmployee with brokerid = " + brokerId + " and employeeid = " + employeeId);
            }

            // 2/25/2012 dd - For performance reason cache per thread
            List<Group> result = null;
            string key = "__GetGroupMembershipsForEmployee__" + brokerId + ":" + employeeId;

            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                if (context.Items.Contains(key))
                {
                    result = context.Items[key] as List<Group>;
                }
                else
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@EmployeeId", employeeId)
                    };

                    result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForEmployee", parameters);
                    context.Items.Add(key, result);
                }
            }
            else
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@EmployeeId", employeeId)
                };

                result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForEmployee", parameters);
            }
            return result;
        }

        /// <summary>
        /// Return inclusive branch group
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public static List<Group> ListInclusiveGroupForBranch(Guid brokerId, Guid branchId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] ListInclusiveGroupForBranch with brokerid = " + brokerId + " and branchid = " + branchId);
            }

            // 2/25/2012 dd - For performance reason cache per thread.
            List<Group> result = null;

            string key = "__GetGroupMembershipsForBranch__" + brokerId + ":" + branchId;
            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                if (context.Items.Contains(key))
                {
                    result = context.Items[key] as List<Group>;
                }
                else
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@BranchId", branchId)
                    };

                    result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForBranch", parameters);
                    context.Items.Add(key, result);
                }
            }
            else
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@BranchId", branchId)
                };

                result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForBranch", parameters);
            }
            return result;
        }

        public static List<Group> ListInclusiveGroupForPmlBroker(Guid brokerId, Guid pmlBrokerId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] ListInclusiveGroupForPmlBroker with brokerid = " + brokerId + " and pmlbrokerid = " + pmlBrokerId);
            }

            // 2/25/2012 dd - For performance reason cache per thread.
            List<Group> result = null;

            string key = "__GetGroupMembershipsForPmlBroker__" + brokerId + ":" + pmlBrokerId;

            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                if (context.Items.Contains(key))
                {
                    result = context.Items[key] as List<Group>;
                }
                else
                {
                    SqlParameter[] parameters = {
                                new SqlParameter("@BrokerId", brokerId),
                                new SqlParameter("@PmlBrokerId", pmlBrokerId)
                    };

                    result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForPmlBroker", parameters);

                    context.Items.Add(key, result);
                }
            }
            else
            {
                SqlParameter[] parameters = {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@PmlBrokerId", pmlBrokerId)
                };

                result = ListInclusiveGroupImpl(brokerId, "GetGroupMembershipsForPmlBroker", parameters);
            }
            return result;

        }
        private static List<Group> ListInclusiveGroupImpl(Guid brokerId, string procedureName, SqlParameter[] parameters)
        {
            List<Group> list = new List<Group>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, procedureName, parameters))
            {
                while (reader.Read())
                {
                    int isInGroup = (int)reader["IsInGroup"];
                    if (isInGroup == 1)
                    {
                        Group group = new Group();
                        group.Id = (Guid)reader["GroupId"];
                        group.Name = (string)reader["GroupName"];
                        group.Description = (string)reader["Description"];
                        group.GroupType = (GroupType)reader["GroupType"];
                        list.Add(group);
                    }
                }
            }

            return list;

        }

        /// <summary>
        /// Gets all the groups with group membership for a given PmlBroker.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <returns>The value of first column "IsInGroup" in the returned data table is 1 or 0 depending on whether the given PmlBroker belongs to the group</returns>
        public static IEnumerable<MembershipGroup> GetGroupMembershipsForPmlBroker(Guid brokerId, Guid pmlBrokerId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetGroupMembershipsForPmlBroker with brokerid = " + brokerId + " and pmlbrokerid = " + pmlBrokerId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@PmlBrokerId", pmlBrokerId)
            };

            List<MembershipGroup> list = new List<MembershipGroup>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetGroupMembershipsForPmlBroker", parameters))
            {
                while (reader.Read())
                {
                    MembershipGroup item = new MembershipGroup();
                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.GroupId = (Guid)reader["GroupId"];
                    item.GroupName = (string)reader["GroupName"];

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Gets all the groups with group membership for a given branch.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="branchId">
        /// The ID of the branch.
        /// </param>
        /// <returns>
        /// The list of groups with group membership.
        /// </returns>
        public static IEnumerable<MembershipGroup> GetGroupMembershipsForBranch(Guid brokerId, Guid branchId)
        {
            if (ConstStage.EnableGroupLogging)
            {
                Tools.LogInfoWithStackTrace("[Group Logging] GetGroupMembershipsForBranch with brokerid = " + brokerId + " and branchId = " + branchId);
            }

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@BranchId", branchId)
            };

            var list = new List<MembershipGroup>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetGroupMembershipsForBranch", parameters))
            {
                while (reader.Read())
                {
                    MembershipGroup item = new MembershipGroup();
                    item.IsInGroup = (int)reader["IsInGroup"];
                    item.GroupId = (Guid)reader["GroupId"];
                    item.GroupName = (string)reader["GroupName"];

                    list.Add(item);
                }
            }

            return list;
        }
        #endregion

        #region Update operations for group memberships

        /// <summary>
        /// update assignment of employee, branch, pmlbroker to a group
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="groupId"></param>
        /// <param name="csvMemberIds"></param>
        public static void UpdateGroupMembership(Guid brokerId, Guid groupId, string csvMemberIds, AbstractUserPrincipal principal = null)
        {
            // principal should only be null when unit tests are being run or default broke settings are being set.
            if (principal != null && principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            csvMemberIds = csvMemberIds.Replace("'", "");
            csvMemberIds = csvMemberIds.Trim(',');

            using (var exec = new CommonProjectLib.Database.CStoredProcedureExec(SqlConnectionManager.Get(brokerId)))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    int N = 50; //OPM 64519. update 50 at a time. very long value of string (more than 4000 chars) in sql parameter gets truncated
                    List<string> ids = csvMemberIds.Split(',').ToList();
                    int count = ids.Count();
                    int iter = Convert.ToInt32( Math.Ceiling(count/ (double)N)); //Number of times to call the stored procedure

                    for(int i=0; i<iter; i++)
                    {
                        StringBuilder sb = new StringBuilder(N * 33);
                        for(int j=0; j<N && (i*N + j < count); j++)
                        {
                            sb.Append(ids[i*N + j] + ",");
                        }

                        SqlParameter[] paramArr = new SqlParameter[]{
                             new SqlParameter("@BrokerId", brokerId),
                             new SqlParameter("@GroupId", groupId),
                             new SqlParameter("@CommaSeparatedMemberIds", sb.ToString().TrimEnd(',')),
                             new SqlParameter("@IsAddOnly", (i==0) ? "0" : "1"          //Delete existing group associations only in the first update
                                 )};

                        exec.ExecuteNonQuery("UpdateGroupMembership", 0, paramArr);
                    }
                    exec.CommitTransaction();
                }
                catch (SqlException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }
        }

        /// <summary>
        /// Update groups an employee belongs to
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="employeeId"></param>
        /// <param name="csvGroupIds"></param>
        private static void UpdateMemberGroups(Guid brokerId, Guid memberId, GroupType type, string csvGroupIds, AbstractUserPrincipal principal)
        {
            // principal should only be null when unit tests are being run or default broke settings are being set.
            if (principal != null && principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            using (var exec = new CommonProjectLib.Database.CStoredProcedureExec(SqlConnectionManager.Get(brokerId)))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    int N = 50; //OPM 64519. update 50 at a time. very long value of string (more than 4000 chars) in sql parameter gets truncated
                    List<string> ids = csvGroupIds.Split(',').ToList();
                    int count = ids.Count();
                    int iter = Convert.ToInt32(Math.Ceiling(count / (double)N)); //Number of times to call the stored procedure

                    for (int i = 0; i < iter; i++)
                    {
                        StringBuilder sb = new StringBuilder(N * 33);
                        for (int j = 0; j < N && (i * N + j < count); j++)
                        {
                            sb.Append(ids[i * N + j] + ",");
                        }

                        SqlParameter[] paramArr = new SqlParameter[]{
                             new SqlParameter("@BrokerId", brokerId),
                             new SqlParameter("@MemberId", memberId),
                             new SqlParameter("@GroupType", type),
                             new SqlParameter("@CommaSeparatedGroupIds", sb.ToString().TrimEnd(',')),
                             new SqlParameter("@IsAddOnly", (i==0) ? "0" : "1" )         //Delete existing group associations only in the first update
                         };
                        exec.ExecuteNonQuery("UpdateMembersGroup", 0, paramArr);
                    }
                    exec.CommitTransaction();
                }
                catch (SqlException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void UpdateEmployeeGroups(Guid brokerId, Guid employeeId, string csvGroupIds, AbstractUserPrincipal principal = null)
        {
            UpdateMemberGroups(brokerId, employeeId, GroupType.Employee, csvGroupIds, principal);
        }

        /// <summary>
        /// Update groups a branch belongs to
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="branchId"></param>
        /// <param name="csvGroupIds"></param>
        public static void UpdateBranchGroups(Guid brokerId, Guid branchId, string csvGroupIds, AbstractUserPrincipal principal = null)
        {
            UpdateMemberGroups(brokerId, branchId, GroupType.Branch, csvGroupIds, principal);
        }

        /// <summary>
        /// Update groups a PmlBroker belongs to
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="pmlBrokerId"></param>
        /// <param name="csvGroupIds"></param>
        public static void UpdatePmlBrokerGroups(Guid brokerId, Guid pmlBrokerId, string csvGroupIds, AbstractUserPrincipal principal = null)
        {
            UpdateMemberGroups(brokerId, pmlBrokerId, GroupType.PmlBroker, csvGroupIds, principal);
        }
        #endregion        
    }

    public class Group
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public GroupType GroupType { get; set; }

        /// <summary>
        /// This property is just alias for Id. This is need for backward compatibility where old code databind using db column name.
        /// </summary>
        public Guid GroupId
        {
            get { return this.Id; }
        }

        /// <summary>
        /// This property is just alias for Name. This is need for backward compatibility where old code databind using db column name.
        /// </summary>
        public string GroupName
        {
            get { return this.Name; }
        }
    }

    public class GroupMembershipEmployee
    {
        public Guid EmployeeId { get; set; }
        public string UserFirstNm { get; set; }
        public string UserLastNm { get; set; }
        public string LoginNm { get; set; }
        public string BranchNm { get; set; }
        public int IsInGroup { get; set; }
    }

    public class GroupEmployeeRecord
    {
        public Guid EmployeeId { get; set; }
        public Guid BrokerId { get; set; }
        public string GroupName { get; set; }
        public Guid GroupId { get; set; }
        public string GroupDescription { get; set; }

        public GroupEmployeeRecord(DbDataReader reader)
        {
            this.EmployeeId = (Guid)reader["EmployeeId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.GroupName = (string)reader["GroupName"];
            this.GroupId = (Guid)reader["GroupId"];
            this.GroupDescription = (string)reader["Description"];
        }
    }

    public class MembershipGroup
    {
        public Guid GroupId { get; set; }
        public string GroupName { get; set; }
        public int IsInGroup { get; set; }
    }

    public class GroupMembershipBranch
    {
        public Guid BranchId { get; set; }
        public string BranchCode { get; set; }
        public string BranchNm { get; set; }
        public string BranchAddr { get; set; }
        public string BranchCity { get; set; }
        public string BranchState { get; set; }
        public string BranchZip { get; set; }
        public int IsInGroup { get; set; }
    }

    public class GroupMembershipPmlBroker
    {
        public int IsInGroup { get; set; }
        public string Name { get; set; }
        public Guid PmlBrokerId { get; set; }
        public string Addr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
