﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Data containing features that a broker can have.
    /// </summary>
    public class FeatureData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureData" /> class.
        /// Default constructor to run GetAllTrackedFeatures function.
        /// </summary>
        public FeatureData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureData" /> class.
        /// </summary>
        /// <param name="featureType">Type of the feature.</param>
        /// <param name="priority">The priority set by user from DB.</param>
        /// <param name="isHidden">The isHidden field set by user from DB.</param>
        public FeatureData(FeatureType featureType, decimal priority, bool isHidden)
        {
            this.FeatureType = featureType;
            this.Priority = priority;
            this.IsHidden = isHidden;
        }

        /// <summary>
        /// Gets or sets a value indicating the type of the feature.
        /// </summary>
        /// <value>The type of the feature.</value>
        public FeatureType FeatureType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the priority of the feature set by LOAdmin user.
        /// </summary>
        /// <value>The priority of the feature.</value>
        public decimal Priority
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the boolean that determines whether this feature is shown or hidden on the FeatureAdoption page.
        /// Set by LOAdmin user.
        /// </summary>
        /// <value>The boolean that determines whether this feature is shown or hidden.</value>
        public bool IsHidden
        {
            get;
            set;
        }

        /// <summary>
        /// Get all feature data objects from the Tracked_Feature table in the database.
        /// </summary>
        /// <returns>A list of feature data objects.</returns>
        public static List<FeatureData> GetAllTrackedFeatures()
        {
            List<FeatureData> featureDataList = new List<FeatureData>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "GetAllTrackedFeatures", null))
            {
                while (reader.Read())
                {
                    int featureId = (int)reader["FeatureId"];
                    decimal priority = (decimal)reader["Priority"];
                    bool isHidden = (bool)reader["IsHidden"];

                    FeatureData featureData = new FeatureData((FeatureType)featureId, priority, isHidden);
                    featureDataList.Add(featureData);
                }
            }

            return featureDataList;
        }

        /// <summary>
        /// This function checks and adds new rows to the Tracked_Feature table if DB does not contain the new FeatureTypes rows.
        /// </summary>
        public static void CheckAndAddNewTrackedFeatures()
        {
            List<FeatureData> featureDataListFromDB = FeatureData.GetAllTrackedFeatures();

            var featureTypeCollection = Enum.GetValues(typeof(FeatureType));

            if (featureTypeCollection.Length > featureDataListFromDB.Count)
            {
                Feature.AddMissingFeatureTypesToDb(
                    implementedTypes: featureTypeCollection.Cast<FeatureType>(),
                    trackedFeaturesInDb: new HashSet<FeatureType>(featureDataListFromDB.Select(fd => fd.FeatureType)));
            }
        }

        /// <summary>
        /// Add new tracking data to the Tracked_Feature table in the database.
        /// </summary>
        /// <param name="feature">The feature to track.</param>
        public static void AddNewTrackedFeature(FeatureType feature)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@FeatureId", feature.ToString("D"))
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "AddNewTrackedFeature", nRetry: 5, parameters: parameters);
        }

        /// <summary>
        /// Update Tracked_Feature table with current FeatureData information.
        /// </summary>
        public void UpdateTrackedFeature()
        {
            var parameters = new SqlParameter[]
                    {
                        new SqlParameter("@featureId", this.FeatureType),
                        new SqlParameter("@priority", this.Priority),
                        new SqlParameter("@isHidden", this.IsHidden)
                    };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "UpdateTrackedFeature", 0, parameters);
        }
    }
}