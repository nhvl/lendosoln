﻿namespace LendersOffice.Admin
{
    using System;
    using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;    

    /// <summary>
    /// Class that handles signatures regarding an employee.
    /// </summary>
    public class EmployeeSignatureInfo
    {
        /// <summary>
        /// Determines whether the signature exists.
        /// </summary>
        private bool? doesSignatureExist;

        /// <summary>
        /// Signature key.
        /// </summary>
        private Guid signatureKey;

        /// <summary>
        /// Determines whether LQB non-compliant document signature is enabled.
        /// </summary>
        private bool enableLqbNonCompliantDocumentSignatureStamp;

        /// <summary>
        /// Employee Id.
        /// </summary>
        private Guid employeeId;

        /// <summary>
        /// Broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeSignatureInfo"/> class.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="employeeId">Employee Id.</param>
        /// <param name="enableLqbNonCompliantDocumentSignatureStamp">Bool that determines whether LQB non-compliant document signature is enabled.</param>
        private EmployeeSignatureInfo(Guid brokerId, Guid employeeId, bool enableLqbNonCompliantDocumentSignatureStamp)
        {
            this.brokerId = brokerId;
            this.employeeId = employeeId;
            this.enableLqbNonCompliantDocumentSignatureStamp = enableLqbNonCompliantDocumentSignatureStamp;
            this.signatureKey = Guid.Empty;
        }

        /// <summary>
        /// Gets the signature key.
        /// </summary>
        public Guid SignatureKey
        {
            get { return this.signatureKey; }
        }

        /// <summary>
        /// Gets the signature details.
        /// </summary>
        public ImageUtils.ImageDetails SignatureDetails
        {
            get
            {
                if (this.HasUploadedSignature == false)
                {
                    throw CBaseException.GenericException("Dev error there is no signature.");
                }

                return ImageUtils.GetImageDetails(this.SignatureKey.ToString());
            }
        }

        /// <summary>
        /// Gets a value indicating whether current user has an uploaded signature.
        /// </summary>
        public bool HasUploadedSignature
        {
            get
            {
                if (!this.doesSignatureExist.HasValue)
                {
                    this.doesSignatureExist = this.enableLqbNonCompliantDocumentSignatureStamp
                        && this.SignatureKey != Guid.Empty
                        && FileDBTools.DoesFileExist(E_FileDB.Normal, this.SignatureKey.ToString());
                }

                return this.doesSignatureExist.Value;
            }
        }

        /// <summary>
        /// Loads the signature info object and returns it.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="employeeId">Employee Id.</param>
        /// <param name="enableLqbNonCompliantDocumentSignatureStamp">Bool that determines whether LQB non-compliant document signature is enabled.</param>
        /// <returns>Employee signature info object.</returns>
        public static EmployeeSignatureInfo Load(Guid brokerId, Guid employeeId, bool enableLqbNonCompliantDocumentSignatureStamp)
        {
            return new EmployeeSignatureInfo(brokerId, employeeId, enableLqbNonCompliantDocumentSignatureStamp)
            {
                signatureKey = RetrieveEmployeeSignatureFileDBKeyById(brokerId, employeeId)
            };
        }

        /// <summary>
        /// Delete signature.
        /// </summary>
        public void DeleteSignature()
        {
            if (this.signatureKey != Guid.Empty)
            {
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@EmployeeID", this.employeeId)
                };                

                using (var conn = DbConnectionInfo.GetConnection(this.brokerId))
                {
                    StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("DeleteEmployeeSignatureKey").Value, parameters, TimeoutInSeconds.Default);                    
                }

                FileDBTools.Delete(E_FileDB.Normal, this.signatureKey.ToString());
                this.signatureKey = Guid.Empty;
            }
        }

        /// <summary>
        /// Save signature key.
        /// </summary>
        /// <param name="signatureKey">Signature key.</param>
        public void SaveSignatureKey(Guid signatureKey)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@EmployeeID", this.employeeId),
                new SqlParameter("@SignatureKey", signatureKey)
            };

            using (var conn = DbConnectionInfo.GetConnection(this.brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("SaveEmployeeSignatureKey").Value, parameters, TimeoutInSeconds.Default);                   
            }            

            this.signatureKey = signatureKey;
        }

        /// <summary>
        /// Retrieve employee signature from file db key by the employee id.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="employeeId">Employee Id.</param>
        /// <returns>Signature file DB key.</returns>
        private static Guid RetrieveEmployeeSignatureFileDBKeyById(Guid brokerId, Guid employeeId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@employeeId", employeeId)
            };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, StoredProcedureName.Create("RetrieveEmployeeSignatureFileDBKeyById").Value, parameters, TimeoutInSeconds.Default))
                {
                    if (reader.Read())
                    {
                        return (Guid)reader["SignatureFileDBKey"];
                    }
                }
            }

            return Guid.Empty;
        }
    }
}
