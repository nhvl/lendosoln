﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides settings for pipelines available to originating company 
    /// users in the originator portal.
    /// </summary>
    public class OriginatingCompanyUserPipelineSettings
    {
        /// <summary>
        /// Gets or sets the order for the pipeline to appear in
        /// for the picker.
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the ID of the setting.
        /// </summary>
        public Guid SettingId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Gets or sets the ID of the broker for the settings.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the report query for the pipeline.
        /// </summary>
        public Guid QueryId { get; set; }

        /// <summary>
        /// Gets the name of the report the pipeline is based on.
        /// </summary>
        public string PipelineReportName { get; private set; }

        /// <summary>
        /// Gets or sets the name of the pipeline report in the picker.
        /// </summary>
        public string PortalDisplayName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the pipeline report
        /// name is overridden to a different name when displayed in the picker.
        /// </summary>
        public bool OverridePipelineReportName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the report is available
        /// in the broker portal mode.
        /// </summary>
        public bool AvailableForBrokerPortalMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the report is available
        /// in the mini-correspondent portal mode.
        /// </summary>
        public bool AvailableForMiniCorrespondentPortalMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the report is available
        /// in the correspondent portal mode.
        /// </summary>
        public bool AvailableForCorrespondentPortalMode { get; set; }

        /// <summary>
        /// Lists all pipeline settings for the specified broker ordered by
        /// <see cref="DisplayOrder"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the pipeline settings.
        /// </param>
        /// <returns>
        /// The list of pipeline settings for the broker ordered by
        /// <see cref="DisplayOrder"/>.
        /// </returns>
        public static IEnumerable<OriginatingCompanyUserPipelineSettings> ListAll(Guid brokerId)
        {
            var settings = new List<OriginatingCompanyUserPipelineSettings>();

            var procedureName = StoredProcedureName.Create("LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_ListAll").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    settings.Add(new OriginatingCompanyUserPipelineSettings()
                    {
                        DisplayOrder = (int)reader["DisplayOrder"],
                        BrokerId = (Guid)reader["BrokerId"],
                        SettingId = (Guid)reader["SettingId"],
                        QueryId = (Guid)reader["QueryId"],
                        PipelineReportName = (string)reader["PipelineReportName"],
                        PortalDisplayName = (string)reader["PortalDisplayName"],
                        OverridePipelineReportName = (bool)reader["OverridePipelineReportName"],
                        AvailableForBrokerPortalMode = (bool)reader["AvailableForBrokerPortalMode"],
                        AvailableForMiniCorrespondentPortalMode = (bool)reader["AvailableForMiniCorrespondentPortalMode"],
                        AvailableForCorrespondentPortalMode = (bool)reader["AvailableForCorrespondentPortalMode"]
                    });
                }
            }

            return settings.OrderBy(setting => setting.DisplayOrder);
        }

        /// <summary>
        /// Saves the pipeline settings to the database.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        /// <param name="newSettings">
        /// The new settings.
        /// </param>
        public static void Save(Guid brokerId, CStoredProcedureExec executor, IEnumerable<OriginatingCompanyUserPipelineSettings> newSettings)
        {
            var newSettingIds = newSettings.Select(setting => setting.SettingId);
            var existingSettingIds = RetrieveExistingSettingIds(brokerId, executor);

            var deletedSettingIds = existingSettingIds.Except(newSettingIds);
            var addedSettingIds = newSettingIds.Except(existingSettingIds);

            ClearDeletedSettings(brokerId, executor, deletedSettingIds);
            SaveNewSettings(brokerId, executor, newSettings);
            AddNewSettingsToTpoUsers(brokerId, executor, addedSettingIds);
        }

        /// <summary>
        /// Adds existing pipeline settings to a new TPO user.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the user's broker.
        /// </param>
        /// <param name="userId">
        /// The ID of the user.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        public static void AddExistingSettingsToUser(Guid brokerId, Guid userId, CStoredProcedureExec executor)
        {
            foreach (var portalMode in new[] { E_PortalMode.Broker, E_PortalMode.Correspondent, E_PortalMode.MiniCorrespondent })
            {
                SqlParameter[] pipelineReportParameters =
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@UserId", userId),
                    new SqlParameter("@PortalMode", portalMode)
                };

                executor.ExecuteNonQuery("ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_AddAllReportsForNewUser", pipelineReportParameters);
            }
        }

        /// <summary>
        /// Retrives the number of Custom Pipeline Reports available for the user to select.
        /// </summary>
        /// <param name="brokerId">The user's Broker Id.</param>
        /// <param name="portalMode">The portal mode that the user is using.</param>
        /// <returns>A number representing the number of pipeline reports available for the user to use.</returns>
        public static int RetrieveNumAvailableReports(Guid brokerId, E_PortalMode portalMode)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@PortalMode", portalMode)
            };

            var procedureName = StoredProcedureName.Create("LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveNumAvailableByBrokerId").ForceValue();
            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                return (int)StoredProcedureDriverHelper.ExecuteScalar(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
            }
        }

        /// <summary>
        /// Retrieves the IDs of existing settings for the broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        /// <returns>
        /// The list of existing setting IDs.
        /// </returns>
        private static List<Guid> RetrieveExistingSettingIds(Guid brokerId, CStoredProcedureExec executor)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var existingSettingIds = new List<Guid>();

            using (var reader = executor.ExecuteReader("LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveExistingSettingIds", parameters))
            {
                while (reader.Read())
                {
                    existingSettingIds.Add((Guid)reader["SettingId"]);
                }
            }

            return existingSettingIds;
        }

        /// <summary>
        /// Clears deleted settings for the broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        /// <param name="deletedSettingIds">
        /// The list of deleted setting IDs.
        /// </param>
        private static void ClearDeletedSettings(Guid brokerId, CStoredProcedureExec executor, IEnumerable<Guid> deletedSettingIds)
        {
            if (!deletedSettingIds.Any())
            {
                return;
            }

            var deletedSettingsXml = new XElement(
                "root",
                deletedSettingIds.Select(deletedSettingId => new XElement("id", deletedSettingId))).ToString(SaveOptions.DisableFormatting);

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DeletedPipelineSettingIdXml", deletedSettingsXml) { SqlDbType = System.Data.SqlDbType.Xml }
            };

            executor.ExecuteNonQuery("LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_ClearDeletedEntries", parameters);
        }

        /// <summary>
        /// Saves the new settings.
        /// </summary>
        /// /// <summary>
        /// Clears deleted settings for the broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        /// <param name="newSettings">
        /// The list of new settings.
        /// </param>
        private static void SaveNewSettings(Guid brokerId, CStoredProcedureExec executor, IEnumerable<OriginatingCompanyUserPipelineSettings> newSettings)
        {
            foreach (var newSetting in newSettings)
            {
                if (!newSetting.AvailableForBrokerPortalMode &&
                    !newSetting.AvailableForMiniCorrespondentPortalMode &&
                    !newSetting.AvailableForCorrespondentPortalMode)
                {
                    var errorMessage = ErrorMessages.SelectAtLeastOnePortalModeForOriginatorPortalPipelineSetting(newSetting.PortalDisplayName);
                    throw new CBaseException(errorMessage, errorMessage);
                }

                SqlParameter[] parameters =
                {
                    new SqlParameter("@DisplayOrder", newSetting.DisplayOrder),
                    new SqlParameter("@SettingId", newSetting.SettingId),
                    new SqlParameter("@BrokerId", newSetting.BrokerId),
                    new SqlParameter("@QueryId", newSetting.QueryId),
                    new SqlParameter("@PortalDisplayName", newSetting.PortalDisplayName),
                    new SqlParameter("@OverridePipelineReportName", newSetting.OverridePipelineReportName),
                    new SqlParameter("@AvailableForBrokerPortalMode", newSetting.AvailableForBrokerPortalMode),
                    new SqlParameter("@AvailableForMiniCorrespondentPortalMode", newSetting.AvailableForMiniCorrespondentPortalMode),
                    new SqlParameter("@AvailableForCorrespondentPortalMode", newSetting.AvailableForCorrespondentPortalMode)
                };

                executor.ExecuteNonQuery("LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_Save", parameters);
            }
        }

        /// <summary>
        /// Clears deleted settings for the broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor holding a transaction.
        /// </param>
        /// <param name="addedSettingIds">
        /// The list of new settings.
        /// </param>
        private static void AddNewSettingsToTpoUsers(Guid brokerId, CStoredProcedureExec executor, IEnumerable<Guid> addedSettingIds)
        {
            if (!addedSettingIds.Any())
            {
                return;
            }

            var addedSettingsXml = new XElement(
                "root",
                addedSettingIds.Select(addedSettingId => new XElement("id", addedSettingId))).ToString(SaveOptions.DisableFormatting);

            foreach (var portalMode in new[] { E_PortalMode.Broker, E_PortalMode.Correspondent, E_PortalMode.MiniCorrespondent })
            {
                var parameters = new[]
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@AddedPipelineSettingIdXml", addedSettingsXml) { SqlDbType = System.Data.SqlDbType.Xml },
                    new SqlParameter("@PortalMode", portalMode)
                };

                executor.ExecuteNonQuery("ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_AddToExistingUsers", parameters);
            }
        }
    }
}