﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableEmpty : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            return false;
        }
        #endregion
    }
}
