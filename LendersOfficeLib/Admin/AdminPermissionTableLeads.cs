﻿namespace LendersOffice.Admin
{
    using Security;

    /// <summary>
    /// Lead permission table.
    /// </summary>
    public class AdminPermissionTableLeads : AdminPermissionTable
    {
        /// <summary>
        /// Gets a value indicating whether the permission should be included in this permission table.
        /// </summary>
        /// <param name="desc">The permission to check.</param>
        /// <returns>True if the permission should be included. Otherwise, false.</returns>
        protected override bool IsIncluded(Desc desc) => desc.Code == Permission.AllowCreatingNewLeadFiles;
    }
}
