﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Xml.Linq;
using System.Collections.Generic;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    public class OriginatorCompensationAudit
    {
        List<OriginatorCompensationAuditEvent> data;
        LosConvert m_losConvert = new LosConvert();
        XDocument auditXml;
        Dictionary<Guid, string> editorNames;
        
        public OriginatorCompensationAudit(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                auditXml = new XDocument(new XDeclaration("1.0", "utf8", "yes"), new XElement("Audit"));
            }
            else
            {
                auditXml = XDocument.Parse(xml);
            }
        }

        public List<OriginatorCompensationAuditEvent> ParseXml(Guid brokerId)
        {
            if (data == null)
            {
                data = new List<OriginatorCompensationAuditEvent>();
                editorNames = new Dictionary<Guid, string>();
                foreach (XElement auditEvent in auditXml.Element("Audit").Elements("AuditEvent"))
                {
                    data.Insert(0, CreateAuditEvent(auditEvent, brokerId));
                }
            }

            return data;
        }

        /*public OriginatorCompensationAuditEvent GetLatestEvent()
        {
            List<OriginatorCompensationAuditEvent> list = ParseXml();

            OriginatorCompensationAuditEvent latestEvent = null;

            foreach (var evt in list)
            {
                if (latestEvent == null)
                {
                    latestEvent = evt;
                }
                else if (latestEvent.DateTime < evt.DateTime)
                {
                    latestEvent = evt;
                }
            }
            return latestEvent;
        }*/

        public OriginatorCompensationAuditEvent GetAuditEventById(int id, Guid brokerId)
        {
            editorNames = new Dictionary<Guid, string>();
            foreach (XElement auditEvent in auditXml.Element("Audit").Elements("AuditEvent"))
            {
                int currentId = Convert.ToInt32(auditEvent.Element("Id").Value);
                if (currentId == id)
                {
                    return CreateAuditEvent(auditEvent, brokerId);
                }
            }

            throw new CBaseException(ErrorMessages.Generic, "Unable to find audit event");
        }

        private OriginatorCompensationAuditEvent CreateAuditEvent(XElement auditEvent, Guid brokerId)
        {
            string byName = "";
            Guid By = new Guid(auditEvent.Element("By").Value);
            if (By != Guid.Empty)
            {
                if (editorNames.ContainsKey(By))
                {
                    byName = editorNames[By];
                }
                else
                {
                    EmployeeDB empDB = new EmployeeDB(By, brokerId);
                    empDB.Retrieve();
                    byName = empDB.FullName;
                    editorNames.Add(By, byName);
                }
            }

            return new OriginatorCompensationAuditEvent(Convert.ToInt32(auditEvent.Element("Id").Value)
                , new Guid(auditEvent.Element("By").Value)
                , byName
                , auditEvent.Element("Date").Value
                , auditEvent.Element("Rate").Value
                , auditEvent.Element("OnlyFor1stLienOfCombo") == null ? String.Empty : auditEvent.Element("OnlyFor1stLienOfCombo").Value
                , (E_PercentBaseT)Convert.ToInt32(auditEvent.Element("BaseT").Value)
                , auditEvent.Element("Minimum").Value
                , auditEvent.Element("Maximum").Value
                , auditEvent.Element("FixedAmount").Value
                , auditEvent.Element("Notes").Value
                );
        }

        public void AddAuditEvent(XElement auditEvent)
        {
            auditXml.Element("Audit").Add(auditEvent);
            data = null;
        }

        public override string ToString()
        {
            return auditXml.ToString();
        }
    }
}