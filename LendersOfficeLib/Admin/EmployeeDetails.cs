﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Collections.Generic;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;
using LqbGrammar;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Simple reader class for containing an employee's contact
    /// information and status.
    /// </summary>

    public class EmployeeDetails
    {
        /// <summary>
        /// Simple reader class for containing an employee's contact
        /// information and status.
        /// </summary>

        private string m_Type = String.Empty;
        private string m_FirstName = String.Empty;
        private string m_LastName = String.Empty;
        private string m_Phone = String.Empty;
        private string m_Email = String.Empty;
        private string m_Cell = String.Empty; //OPM 2703
        private string m_Pager = String.Empty; //OPM 2703
        private string m_Login = String.Empty;
        private string m_ActualBrokerName = String.Empty;
        private Guid m_LpePriceGroupId = Guid.Empty;
        private Boolean m_IsExtSupervisor = false;
        private Boolean m_DoNotify = false;
        private Boolean m_IsAOwner = false;
        private Boolean m_CanLogin = false;
        private Boolean m_IsActive = false;
        private Guid m_BrokerId = Guid.Empty;
        private Guid m_BranchId = Guid.Empty;
        private Guid m_UserId = Guid.Empty;
        private Guid m_EmployeeId = Guid.Empty;
        private int m_LicenseNumber = -1; // 05/10/06 mf - OPM 4834
        private string m_AgentLicenseNumber = String.Empty; // 11/30/06 db - OPM 8193
        private string m_CommissionPointOfLoanAmount = String.Empty;
        private string m_CommissionPointOfGrossProfit = String.Empty;
        private string m_CommissionMinBase = String.Empty;
        private string m_notesByEmployer = "";
        private string m_sNmLsIdentifier = "";
        private string m_sEmployeeIDInCompany = "";
        private string m_ChumsID = "";
        private bool m_IsCellphoneForMultiFactorOnly = false;

        private bool m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
        private decimal m_OriginatorCompensationPercent;
        private decimal m_OriginatorCompensationMinAmount;
        private decimal m_OriginatorCompensationMaxAmount;
        private decimal m_OriginatorCompensationFixedAmount;
        private string m_OriginatorCompensationNotes = string.Empty;
        private E_PercentBaseT m_OriginatorCompensationBaseT;
        private DateTime? m_OriginatorCompensationLastModifiedDate = null;
        private Guid m_PmlBrokerId;
        private E_OrigiantorCompenationLevelT m_OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.OriginatingCompany;

        private Guid miniCorrespondentBranchID = Guid.Empty;
        private Guid correspondentBranchID = Guid.Empty;

        private string m_LicenseXmlContent;
        private LicenseInfoList m_LicenseInfoList = null;

        #region ( Employee properties )

        public IDataRecord m_Record
        {
            // Access member.

            set
            {
                Object o;

                o = value["Type"];

                if (o != null && o is DBNull == false)
                {
                    m_Type = (String)o;
                }
                else
                {
                    m_Type = String.Empty;
                }

                o = value["UserFirstNm"];

                if (o != null && o is DBNull == false)
                {
                    m_FirstName = (String)o;
                }
                else
                {
                    m_FirstName = String.Empty;
                }

                o = value["UserLastNm"];

                if (o != null && o is DBNull == false)
                {
                    m_LastName = (String)o;
                }
                else
                {
                    m_LastName = String.Empty;
                }

                o = value["Phone"];

                if (o != null && o is DBNull == false)
                {
                    m_Phone = (String)o;
                }
                else
                {
                    m_Phone = String.Empty;
                }

                o = value["Email"];

                if (o != null && o is DBNull == false)
                {
                    m_Email = (String)o;
                }
                else
                {
                    m_Email = String.Empty;
                }

                o = value["CellPhone"];

                if (o != null && o is DBNull == false)
                {
                    m_Cell = (String)o;
                }
                else
                {
                    m_Cell = String.Empty;
                }

                o = value["IsCellphoneForMultiFactorOnly"];
                if (o != null && o is DBNull == false)
                {
                    this.m_IsCellphoneForMultiFactorOnly = (bool)o;
                }

                o = value["Pager"];

                if (o != null && o is DBNull == false)
                {
                    m_Pager = (String)o;
                }
                else
                {
                    m_Pager = String.Empty;
                }

                o = value["LoginNm"];

                if (o != null && o is DBNull == false)
                {
                    m_Login = (String)o;
                }
                else
                {
                    m_Login = String.Empty;
                }

                o = value["PmlBrokerCompany"];

                if (o != null && o is DBNull == false)
                {
                    m_ActualBrokerName = (String)o;
                }
                else
                {
                    m_ActualBrokerName = String.Empty;
                }

                o = value["LpePriceGroupId"];

                if (o != null && o is DBNull == false)
                {
                    m_LpePriceGroupId = (Guid)o;
                }
                else
                {
                    m_LpePriceGroupId = Guid.Empty;
                }

                o = value["IsPmlManager"];

                if (o != null && o is DBNull == false)
                {
                    m_IsExtSupervisor = (Boolean)o;
                }
                else
                {
                    m_IsExtSupervisor = false;
                }

                o = value["DoNotify"];

                if (o != null && o is DBNull == false)
                {
                    m_DoNotify = (Boolean)o;
                }
                else
                {
                    m_DoNotify = false;
                }

                o = value["IsAccountOwner"];

                if (o != null && o is DBNull == false)
                {
                    m_IsAOwner = (Boolean)o;
                }
                else
                {
                    m_IsAOwner = false;
                }

                o = value["CanLogin"];

                if (o != null && o is DBNull == false)
                {
                    m_CanLogin = (Boolean)o;
                }
                else
                {
                    m_CanLogin = false;
                }

                o = value["IsActive"];

                if (o != null && o is DBNull == false)
                {
                    m_IsActive = (Boolean)o;
                }
                else
                {
                    m_IsActive = false;
                }

                o = value["EmployeeId"];

                if (o != null && o is DBNull == false)
                {
                    m_EmployeeId = (Guid)o;
                }
                else
                {
                    m_EmployeeId = Guid.Empty;
                }

                o = value["UserId"];

                if (o != null && o is DBNull == false)
                {
                    m_UserId = (Guid)o;
                }
                else
                {
                    m_UserId = Guid.Empty;
                }

                o = value["BranchId"];

                if (o != null && o is DBNull == false)
                {
                    m_BranchId = (Guid)o;
                }
                else
                {
                    m_BranchId = Guid.Empty;
                }

                o = value["BrokerId"];

                if (o != null && o is DBNull == false)
                {
                    m_BrokerId = (Guid)o;
                }
                else
                {
                    m_BrokerId = Guid.Empty;
                }

                o = value["LicenseNumber"];

                if (o != null && o is DBNull == false)
                {
                    m_LicenseNumber = (int)o;
                }
                else
                {
                    m_LicenseNumber = -1;
                }

                o = value["EmployeeLicenseNumber"];
                if (o != null && o is DBNull == false)
                {
                    m_AgentLicenseNumber = (string)o;
                }
                else
                {
                    m_AgentLicenseNumber = String.Empty;
                }

                o = value["CommissionPointOfLoanAmount"];
                if (o != null && o is DBNull == false)
                {
                    m_CommissionPointOfLoanAmount = o.ToString();
                }
                else
                {
                    m_CommissionPointOfLoanAmount = String.Empty;
                }

                o = value["CommissionPointOfGrossProfit"];
                if (o != null && o is DBNull == false)
                {
                    m_CommissionPointOfGrossProfit = o.ToString();
                }
                else
                {
                    m_CommissionPointOfGrossProfit = String.Empty;
                }

                o = value["CommissionMinBase"];
                if (o != null && o is DBNull == false)
                {
                    m_CommissionMinBase = o.ToString();
                }
                else
                {
                    m_CommissionMinBase = String.Empty;
                }

                o = value["NotesByEmployer"];
                if (o != null && o is DBNull == false)
                {
                    m_notesByEmployer = (string)o;
                }

                o = value["NmLsIdentifier"];
                if (o != null && o is DBNull == false)
                {
                    m_sNmLsIdentifier = (string)o;
                }

                o = value["EmployeeIDInCompany"];
                if (o != null && o is DBNull == false)
                {
                    m_sEmployeeIDInCompany = (string)o;
                }

                o = value["OriginatorCompensationIsOnlyPaidForFirstLienOfCombo"];
                if (o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = (bool)o;
                }

                o = value["OriginatorCompensationPercent"];
                if(o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationPercent = (decimal)o;
                }

                o = value["OriginatorCompensationMinAmount"];
                if(o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationMinAmount = (decimal)o;
                }

                o = value["OriginatorCompensationMaxAmount"];
                if(o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationMaxAmount = (decimal)o;
                }

                o = value["OriginatorCompensationFixedAmount"];
                if(o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationFixedAmount = (decimal)o;
                }

                o = value["OriginatorCompensationNotes"];
                if(o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationNotes = (string)o;
                }

                o = value["OriginatorCompensationBaseT"];
                if (o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationBaseT = (E_PercentBaseT)o;
                }

                o = value["OriginatorCompensationSetLevelT"];
                if (o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationSetLevelT = (E_OrigiantorCompenationLevelT)o;
                }

                o = value["PmlBrokerId"];
                if (o != null && o is DBNull == false)
                {
                    m_PmlBrokerId = (Guid)o;
                }

                o = value["OriginatorCompensationLastModifiedDate"];
                if (o != null && o is DBNull == false)
                {
                    m_OriginatorCompensationLastModifiedDate = Convert.ToDateTime(o);
                }

                o = value["ChumsId"];
                if (o != null && o is DBNull == false)
                {
                    m_ChumsID = (string)o;
                }

                o = value["MiniCorrespondentBranchId"];
                if (o != null && !(o is DBNull))
                {
                    this.miniCorrespondentBranchID = (Guid)o;
                }

                o = value["CorrespondentBranchId"];
                if (o != null && !(o is DBNull))
                {
                    this.correspondentBranchID = (Guid)o;
                }

                o = value["LastUsedIpAddress"];
                if (o != null && !(o is DBNull))
                {
                    this.LastUsedIpAddress = (string)o;
                }

                o = value["RecentLoginD"];
                if (o != null && !(o is DBNull))
                {
                    this.RecentLoginD = (DateTime?)o;
                }

                o = value["EnabledIpRestriction"];
                if (o != null && !(o is DBNull))
                {
                    this.EnabledIpRestriction = (bool)o;
                }

                o = value["MustLogInFromTheseIpAddresses"];
                if (o != null && !(o is DBNull))
                {
                    this.MustLogInFromTheseIpAddresses = (string)o;
                }

                o = value["EnabledMultiFactorAuthentication"];
                if (o != null && !(o is DBNull))
                {
                    this.EnabledMultiFactorAuthentication = (bool)o;
                }

                o = value["EnabledClientDigitalCertificateInstall"];
                if (o != null && !(o is DBNull))
                {
                    this.EnabledClientDigitalCertificateInstall = (bool)o;
                }

                o = value["LicenseXmlContent"];
                if (o != null && o is DBNull == false)
                {
                    m_LicenseXmlContent = (string)o;
                }
                else
                {
                    m_LicenseXmlContent = String.Empty;
                }

                o = value["LastModifiedDate"];
                if (o != null && !(o is DBNull))
                {
                    this.LastModifiedDate = (DateTime)o;
                }
                else
                {
                    this.LastModifiedDate = default(DateTime);
                }

                o = value["EnableAuthCodeViaSms"];

                if (o != null && !(o is DBNull))
                {
                    this.EnableAuthCodeViaSms = (bool)o;
                }
            }
        }

        public String EmployeeStatus
        {
            get
            {
                if (m_IsActive == true)
                {
                    if (m_Type != "")
                    {
                        if (m_CanLogin == true)
                            return "Active";
                        else
                            return "Disabled";
                    }
                    else
                        return "New";
                }
                else
                    return "Inactive";
            }
        }


        public string Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        public string EmployeeName
        {
            get { return m_LastName + " , " + m_FirstName; }
        }

        public string FullName
        {
            get { return m_FirstName + " " + m_LastName; }
        }

        public string FirstName
        {
            set { m_FirstName = value; }
            get { return m_FirstName; }
        }

        public string LastName
        {
            set { m_LastName = value; }
            get { return m_LastName; }
        }

        public string Phone
        {
            set { m_Phone = value; }
            get { return m_Phone; }
        }

        public bool IsCellphoneForMultiFactorOnly
        {
            get
            {
                return this.m_IsCellphoneForMultiFactorOnly;
            }

            set
            {
                this.m_IsCellphoneForMultiFactorOnly = value;
            }
        }

        public string CellPhone
        {
            set
            {
                m_Cell = value;
            }

            get
            {
                return EmployeeDB.CalculatedCellphoneValue(this.m_Cell, this.IsCellphoneForMultiFactorOnly);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the user has a non-blank cell phone number, regardless of privacy of the number.
        /// </summary>
        public bool HasCellPhoneNumber => !string.IsNullOrEmpty(this.m_Cell);


        public string Pager
        {
            set { m_Pager = value; }
            get { return m_Pager; }
        }

        public string Email
        {
            set { m_Email = value; }
            get { return m_Email; }
        }

        public string Login
        {
            set { m_Login = value; }
            get { return m_Login; }
        }

        public string ActualBrokerName
        {
            set { m_ActualBrokerName = value; }
            get { return m_ActualBrokerName; }
        }

        public Guid LpePriceGroupId
        {
            set { m_LpePriceGroupId = value; }
            get { return m_LpePriceGroupId; }
        }

        public bool IsExtSupervisor
        {
            set { m_IsExtSupervisor = value; }
            get { return m_IsExtSupervisor; }
        }

        public bool DoNotify
        {
            set { m_DoNotify = value; }
            get
            {
                if (this.Type == "P")
                    return false;
                else
                    return m_DoNotify;
            }
        }

        public bool IsAOwner
        {
            set { m_IsAOwner = value; }
            get { return m_IsAOwner; }
        }

        public bool CanLogin
        {
            set { m_CanLogin = value; }
            get { return m_CanLogin; }
        }

        public bool IsActive
        {
            set { m_IsActive = value; }
            get { return m_IsActive; }
        }

        public Guid EmployeeId
        {
            set { m_EmployeeId = value; }
            get { return m_EmployeeId; }
        }

        public Guid UserId
        {
            set { m_UserId = value; }
            get { return m_UserId; }
        }

        public Guid BranchId
        {
            set { m_BranchId = value; }
            get { return m_BranchId; }
        }

        public Guid MiniCorrespondentBranchID
        {
            set { this.miniCorrespondentBranchID = value; }
            get { return this.miniCorrespondentBranchID; }
        }

        public Guid CorrespondentBranchID
        {
            set { this.correspondentBranchID = value; }
            get { return this.correspondentBranchID; }
        }

        public Guid BrokerId
        {
            set { m_BrokerId = value; }
            get { return m_BrokerId; }
        }

        public int LicenseNumber
        {
            set { m_LicenseNumber = value; }
            get { return m_LicenseNumber; }
        }

        public string AgentLicenseNumber
        {
            set { m_AgentLicenseNumber = value; }
            get { return m_AgentLicenseNumber; }
        }

        public string CommissionPointOfLoanAmount
        {
            set { m_CommissionPointOfLoanAmount = value; }
            get { return m_CommissionPointOfLoanAmount; }
        }

        public string CommissionPointOfGrossProfit
        {
            set { m_CommissionPointOfGrossProfit = value; }
            get { return m_CommissionPointOfGrossProfit; }
        }

        public string CommissionMinBase
        {
            set { m_CommissionMinBase = value; }
            get { return m_CommissionMinBase; }
        }

        public string NotesByEmployer
        {
            get { return m_notesByEmployer; }
            set { m_notesByEmployer = value; }
        }

        public string LosIdentifier
        {
            get { return m_sNmLsIdentifier; }
            set { m_sNmLsIdentifier = value; }
        }

        public string EmployeeIDInCompany
        {
            get { return m_sEmployeeIDInCompany; }
            set { m_sEmployeeIDInCompany = value; }
        }

        public string ChumsID
        {
            get { return m_ChumsID; }
            set { m_ChumsID = value; }
        }


        //OPM 64852 - 7/2013 pa - Originator Compensation Data used in export, setters not needed (would require more fields as per EmployeeDB)
        public bool OriginatorCompensationIsOnlyPaidForFirstLienOfCombo
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                }
            }
        }

        public decimal OriginatorCompensationPercent
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationPercent;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationPercent;
                }
            }
        }
        public decimal OriginatorCompensationMinAmount
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationMinAmount;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationMinAmount;
                }
            }
        }
        public decimal OriginatorCompensationMaxAmount
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationMaxAmount;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationMaxAmount;
                }
            }
        }
        public decimal OriginatorCompensationFixedAmount
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationFixedAmount;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationFixedAmount;
                }
            }
        }
        public string OriginatorCompensationNotes
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationNotes;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationNotes;
                }
            }
        }
        private E_PercentBaseT OriginatorCompensationBaseT
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty || m_OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.IndividualUser)
                {
                    return m_OriginatorCompensationBaseT;
                }
                else
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);
                    return pmlBroker.OriginatorCompensationBaseT;
                }
            }
        }
        public string OriginatorCompensationBasis
        {
            get
            {
                E_PercentBaseT baseT = OriginatorCompensationBaseT;
                switch(baseT)
                {
                    case E_PercentBaseT.LoanAmount:
                        return "Loan Amount";
                    case E_PercentBaseT.TotalLoanAmount:
                        return "Total Loan Amount";
                    default:
                        throw new UnhandledEnumException(baseT);
                }
            }
        }
        public DateTime? OriginatorCompensationLastModifiedDate
        {
            get
            {
                if (m_PmlBrokerId == Guid.Empty)
                {
                    return m_OriginatorCompensationLastModifiedDate;
                }
                else
                {
                    //the latest plan effective date between the user record and the OC record should be used for the plan effective date.
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(m_PmlBrokerId, m_BrokerId);

                    // If one of these dates is null, return the other. If they're both null, return null.
                    if (!m_OriginatorCompensationLastModifiedDate.HasValue)
                    {
                        return pmlBroker.OriginatorCompensationLastModifiedDate;
                    }
                    else if (!pmlBroker.OriginatorCompensationLastModifiedDate.HasValue)
                    {
                        return m_OriginatorCompensationLastModifiedDate;
                    }
                    else if (m_OriginatorCompensationLastModifiedDate < pmlBroker.OriginatorCompensationLastModifiedDate)
                    {
                        // Otherwise return the most recently modified OriginatorCompensationLastModifiedDate
                        return pmlBroker.OriginatorCompensationLastModifiedDate;
                    }
                    else
                    {
                        return m_OriginatorCompensationLastModifiedDate;
                    }

                }
            }
        }

        public string LastUsedIpAddress { get; private set; }

        public DateTime? RecentLoginD { get; private set; }

        public bool EnabledIpRestriction { get; private set; }

        public string MustLogInFromTheseIpAddresses { get; private set; }

        public bool EnabledMultiFactorAuthentication { get; private set; }

        public bool EnabledClientDigitalCertificateInstall { get; private set; }

        public DateTime LastModifiedDate { get; private set; }

        public bool EnableAuthCodeViaSms { get; private set; }

        public LicenseInfoList LicenseInfoList
        {
            get
            {
                if (m_LicenseInfoList == null && !string.IsNullOrWhiteSpace(m_LicenseXmlContent))
                {
                    try
                    {
                        m_LicenseInfoList = LicenseInfoList.ToObject(m_LicenseXmlContent);
                    }
                    catch (CBaseException e)
                    {
                        Tools.LogWarning("EmployeeDetails failed to parse LicenseInfoList object from LicenseXmlContent. LicenseXmlContent: " + m_LicenseXmlContent, e);
                    }
                }

                return m_LicenseInfoList;
            }
        }

        #endregion

        /// <summary>
        /// Load the employee from the database.  We use the view, so
        /// this should be quick.
        /// </summary>
        public void Retrieve(Guid brokerId, Guid employeeId)
        {
            // Get the employee from the database.
            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeId", employeeId)
                                        };

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeDetailsByEmployeeId", parameters))
            {
                if (sR.Read() == true)
                {
                    m_Record = sR;
                }
                else
                {
                    throw new ArgumentException("Failed to load employee details for " + employeeId + ".");
                }
            }
        }

        public void Retrieve(Guid employeeId)
        {
            Guid brokerId = Guid.Empty;

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeId", employeeId)
                                        };

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(connInfo, "ListEmployeeDetailsByEmployeeId", parameters))
                {
                    if (sR.Read() == true)
                    {
                        m_Record = sR;
                        return;
                    }

                }
            }
            throw new ArgumentException("Failed to load employee details for " + employeeId + ".");
        }

        /// <summary>
        /// Return our id as a helper for databinding to a key.
        /// </summary>
        public override String ToString()
        {
            // Return our id as a key binding help.

            return m_EmployeeId.ToString();
        }

        public Guid GetBranchID(
            E_BranchChannelT branchChannelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            if (this.Type != "P" ||
                branchChannelT != E_BranchChannelT.Correspondent)
            {
                return this.BranchId;
            }
            else if (correspondentProcessT != E_sCorrespondentProcessT.MiniCorrespondent)
            {
                return this.CorrespondentBranchID;
            }
            else
            {
                return this.MiniCorrespondentBranchID;
            }
        }        

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public EmployeeDetails(Guid brokerId, Guid employeeId)
        {
            // Initialize details.

            Retrieve(brokerId, employeeId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EmployeeDetails()
        {
        }

        #endregion

    }
}
