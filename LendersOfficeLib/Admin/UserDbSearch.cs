﻿// <copyright file="UserDbSearch.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   10/20/2014 5:20:31 PM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Searching User information.
    /// </summary>
    public static class UserDbSearch
    {
        /// <summary>
        /// Search for a list of users matching the filter.
        /// </summary>
        /// <param name="userStatus">User Status filter - Possible values are: any, active, inactive, disabled.</param>
        /// <param name="userType">User Type filter - Possible values are: any, B, P.</param>
        /// <param name="loginName">Login name filter.</param>
        /// <param name="fullName">Full name of user filter.</param>
        /// <param name="email">Email filter.</param>
        /// <param name="broker">Broker customer code filter.</param>
        /// <param name="brokerStatus">Broker status filter.</param>
        /// <param name="userId">User id filter.</param>
        /// <param name="employeeId">Employee id filter.</param>
        /// <returns>A list of users matching the filter.</returns>
        public static IEnumerable<UserDbSearchItem> Search(string userStatus, string userType, string loginName, string fullName, string email, string broker, bool? brokerStatus, Guid userId, Guid employeeId)
        {
            List<UserDbSearchItem> list = new List<UserDbSearchItem>();

            IEnumerable<DbConnectionInfo> connectionInfoList = DbConnectionInfo.ListAll();

            foreach (DbConnectionInfo connInfo in connectionInfoList)
            {
                // 11/8/2004 kb - Updating sql to use joins.                
                string storedProcedureName = "FindUser";

                List<SqlParameter> parameters = new List<SqlParameter>();

                if (string.IsNullOrEmpty(loginName) == false)
                {
                    parameters.Add(new SqlParameter("@LoginNm", loginName.Replace("'", "''")));
                }

                if (string.IsNullOrEmpty(fullName) == false)
                {
                    // 11/8/2004 kb - Trying tricky stuff now.  Before, we
                    // would search first name or last name, or both with
                    // the same pattern.  We now split up the query pattern
                    // to see if we have a first and last name.  If we do,
                    // then we match against the respective parts.  If not,
                    // then match either.
                    string[] nameParts = fullName.Replace("'", "''").TrimWhitespaceAndBOM().Split(' ');

                    if (nameParts.Length >= 2)
                    {
                        parameters.Add(new SqlParameter("@UserFirstNm", nameParts[0]));
                        parameters.Add(new SqlParameter("@UserLastNm", nameParts[1]));
                    }
                    else if (nameParts.Length == 1)
                    {
                        parameters.Add(new SqlParameter("@UserNm", nameParts[0]));
                    }
                }
                
                if (userStatus == "active")
                {
                    parameters.Add(new SqlParameter("@IsActive", true));
                    parameters.Add(new SqlParameter("@CanLogin", true));
                }
                else if (userStatus == "inactive")
                {
                    parameters.Add(new SqlParameter("@IsActive", false));
                }
                else if (userStatus == "disabled")
                {
                    parameters.Add(new SqlParameter("@IsActive", true));
                    parameters.Add(new SqlParameter("@CanLogin", false));
                }
                else if (userStatus == "any")
                {
                }
                else
                {
                    throw new CBaseException("Unhandle user status filter=[" + userStatus + "]", "Unhandle user status filter=[" + userStatus + "]");
                }

                if (userType != "any")
                {
                    parameters.Add(new SqlParameter("@Type", userType));
                }

                if (string.IsNullOrEmpty(email) == false)
                {
                    parameters.Add(new SqlParameter("@Email", email.Replace("'", "''").TrimWhitespaceAndBOM()));
                }

                if (string.IsNullOrEmpty(broker) == false)
                {
                    parameters.Add(new SqlParameter("@BrokerNm", broker.Replace("'", "''").TrimWhitespaceAndBOM()));
                }

                if (brokerStatus.HasValue)
                {
                    parameters.Add(new SqlParameter("@BrokerStatus", brokerStatus.Value ? "1" : "0"));
                }

                if (employeeId != Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@EmployeeId", employeeId));
                }

                if (userId != Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@UserId", userId));
                }

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, storedProcedureName, parameters))
                {
                    while (reader.Read())
                    {
                        list.Add(new UserDbSearchItem(reader));
                    }
                }
            }

            return list;
        }
    }
}