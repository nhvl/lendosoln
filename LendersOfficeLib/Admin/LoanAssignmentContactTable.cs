﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// List out the gathered employee contact info details.  Each item
    /// should contain the role they hold on that loan.  One thing to
    /// remember: Loan creation uses this table, so initializing it
    /// may *not* rely on the cache table (it's not ready until after
    /// the new loan file is finalized).
    /// </summary>

    public class LoanAssignmentContactTable
    {
        private IEnumerable<EmployeeLoanAssignment> m_employeeLoanAssignmentList = new List<EmployeeLoanAssignment>();

        public EmployeeLoanAssignment FindByRoleT(E_RoleT roleType)
        {
            foreach (var o in m_employeeLoanAssignmentList)
            {
                if (o.Role.RoleT == roleType)
                {
                    return o;
                }
            }
            return null;

        }

        /// <summary>
        /// List out the gathered employee contact info details.  Each
        /// item should contain the role they hold on that loan.
        /// </summary>
        public IEnumerable<EmployeeLoanAssignment> Items
        {
            get { return m_employeeLoanAssignmentList; }
        }

        public EmployeeLoanAssignment FindByRoleDesc(string roleDesc)
        {
            foreach (var o in m_employeeLoanAssignmentList)
            {
                if (o.RoleDesc.Equals(roleDesc, StringComparison.OrdinalIgnoreCase))
                {
                    return o;
                }
            }
            return null;
        }

        /// <summary>
        /// Retrieve a loan's employee assignments with descriptions of
        /// the roles they fulfill for this loan.
        /// </summary>

        public void Retrieve(Guid brokerId, Guid loanId)
        {
            m_employeeLoanAssignmentList = EmployeeLoanAssignment.ListEmployeeAssignmentByLoan(brokerId, loanId);
        }

        /// <summary>
        /// Check if the entry is contained within our retrieved table.
        /// </summary>

        public bool Contains(E_RoleT roleType)
        {
            // Check if the entry is contained within our retrieved table.
            foreach (var o in m_employeeLoanAssignmentList)
            {
                if (o.RoleT == roleType)
                {
                    return true;
                }
            }
            return false;
        }


        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAssignmentContactTable(Guid brokerId, Guid loanId)
        {
            // Initialize this list.

            Retrieve(brokerId, loanId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAssignmentContactTable()
        {
        }

        #endregion

    }
}
