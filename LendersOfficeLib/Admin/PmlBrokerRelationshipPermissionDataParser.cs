﻿/// <copyright file="PmlBrokerRelationshipPermissionDataParser.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   2/1/2017
/// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Common;
    using Security;

    /// <summary>
    /// Provides a simple container for relationship data.
    /// </summary>
    public struct PmlBrokerRelationshipData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PmlBrokerRelationshipData"/>
        /// struct.
        /// </summary>
        /// <param name="employeeId">
        /// The ID of the employee.
        /// </param>
        /// <param name="name">
        /// The name of the employee.
        /// </param>
        /// <param name="role">
        /// The role of the employee.
        /// </param>
        public PmlBrokerRelationshipData(Guid employeeId, string name, E_RoleT role)
        {
            this.EmployeeId = employeeId;
            this.EmployeeName = name;
            this.EmployeeRole = role;
        }

        /// <summary>
        /// Gets the ID of the employee in the relationship.
        /// </summary>
        /// <value>
        /// The ID of the employee.
        /// </value>
        public Guid EmployeeId { get; }

        /// <summary>
        /// Gets the name of the employee in the relationship.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        public string EmployeeName { get; }

        /// <summary>
        /// Gets the role of the employee in the relationship.
        /// </summary>
        /// <value>
        /// The role of the employee.
        /// </value>
        public E_RoleT EmployeeRole { get; }
    }

    /// <summary>
    /// Provides a means for loading relationship and permission data for an originating company.
    /// </summary>
    public class PmlBrokerRelationshipPermissionDataParser
    {
        /// <summary>
        /// Gets a value indicating whether the user can apply for 
        /// ineligible loan programs.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        public bool CanApplyForIneligibleLoanPrograms { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can run pricing
        /// without a credit report.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        public bool CanRunPricingEngineWithoutCreditReport { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can register/lock
        /// a loan without a credit report.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        public bool CanSubmitWithoutCreditReport { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can access
        /// TOTAL Scorecard.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        public bool CanAccessTotalScorecard { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can order
        /// a 4506-T.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        public bool AllowOrder4506T { get; private set; }

        /// <summary>
        /// Gets the value for the type of task email options set for 
        /// the <see cref="PmlBroker"/>.
        /// </summary>
        /// <value>
        /// The <see cref="E_TaskRelatedEmailOptionT"/> for the <see cref="PmlBroker"/>.
        /// </value>
        public E_TaskRelatedEmailOptionT TaskEmailOption { get; private set; } = E_TaskRelatedEmailOptionT.DontReceiveEmail;

        /// <summary>
        /// Gets a map between OC role and relationship data.
        /// </summary>
        /// <value>
        /// A map between OC role and relationship data.
        /// </value>
        private Dictionary<E_OCRoles, List<PmlBrokerRelationshipData>> RelationshipsByRole { get; } = new Dictionary<E_OCRoles, List<PmlBrokerRelationshipData>>(3)
        {
            [E_OCRoles.Broker] = new List<PmlBrokerRelationshipData>(),
            [E_OCRoles.MiniCorrespondent] = new List<PmlBrokerRelationshipData>(),
            [E_OCRoles.Correspondent] = new List<PmlBrokerRelationshipData>()
        };

        /// <summary>
        /// Loads data from the specified <paramref name="reader"/>.
        /// </summary>
        /// <param name="reader">
        /// The reader with relationship and permission data.
        /// </param>
        /// <remarks>
        /// The expected data sequence is relationships first and then permissions.
        /// </remarks>
        public void Load(IDataReader reader)
        {
            this.LoadRelationshipData(reader["RelationshipXml"]);
            this.LoadPermissionData(reader);
        }
        
        /// <summary>
        /// Obtains relationships for the specified <paramref name="role"/>.
        /// </summary>
        /// <param name="role">
        /// The role to load relationships.
        /// </param>
        /// <returns>
        /// The relationships for the role.
        /// </returns>
        public IEnumerable<PmlBrokerRelationshipData> GetRelationships(E_OCRoles role) => this.RelationshipsByRole[role];

        /// <summary>
        /// Loads relationship information using data from the database.
        /// </summary>
        /// <param name="relationshipXml">
        /// The data from the database.
        /// </param>
        private void LoadRelationshipData(object relationshipXml)
        {
            if (!Convert.IsDBNull(relationshipXml))
            {
                var document = Tools.CreateXmlDoc((string)relationshipXml);

                foreach (System.Xml.XmlNode node in document.SelectNodes("root/row"))
                {
                    var pmlBrokerRole = node.Attributes["OCRoleT"]?.Value.ToNullable<E_OCRoles>(Enum.TryParse);
                    var employeeId = node.Attributes["EmployeeId"]?.Value.ToNullable<Guid>(Guid.TryParse);
                    var employeeRole = node.Attributes["EmployeeRoleT"]?.Value.ToNullable<E_RoleT>(Enum.TryParse);

                    if (!pmlBrokerRole.HasValue || !employeeId.HasValue || !employeeRole.HasValue)
                    {
                        continue;
                    }

                    this.RelationshipsByRole[pmlBrokerRole.Value].Add(new PmlBrokerRelationshipData(
                        employeeId.Value,
                        node.Attributes["EmployeeName"]?.Value,
                        employeeRole.Value));
                }
            }
        }

        /// <summary>
        /// Loads permission information using data from the database.
        /// </summary>
        /// <param name="reader">
        /// The reader containing data from the database.
        /// </param>
        private void LoadPermissionData(IDataReader reader)
        {
            this.CanApplyForIneligibleLoanPrograms = reader["CanApplyForIneligibleLoanPrograms"] as bool? ?? false;
            this.CanRunPricingEngineWithoutCreditReport = reader["CanRunPricingEngineWithoutCreditReport"] as bool? ?? false;
            this.CanSubmitWithoutCreditReport = reader["CanSubmitWithoutCreditReport"] as bool? ?? false;
            this.CanAccessTotalScorecard = reader["CanAccessTotalScorecard"] as bool? ?? false;
            this.AllowOrder4506T = reader["AllowOrder4506T"] as bool? ?? false;

            if (!Convert.IsDBNull(reader["TaskRelatedEmailOptionT"]))
            {
                this.TaskEmailOption = (E_TaskRelatedEmailOptionT)Convert.ToInt32(reader["TaskRelatedEmailOptionT"]);
            }
        }
    }
}
