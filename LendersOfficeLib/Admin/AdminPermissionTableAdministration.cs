﻿// <copyright file="AdminPermissionTableAdministration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author) Scott Kibler
//    Date)   11/6/2014 5)09)00 PM 
// </summary>

namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Security;

    /// <summary>
    /// The class representing the Administration group of the Employee Permissions List.
    /// </summary>
    public class AdminPermissionTableAdministration : BrokerUserPermissionTable
    {
        /// <summary>
        /// Gets the current principal's broker.
        /// </summary>
        private BrokerDB CurrentBroker
        {
            get
            {
                return BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            }
        }

        /// <summary>
        /// Returns true for permissions that belong in the administration group of the employee permissions editor.
        /// </summary>
        /// <param name="desc">The description object for the permission to be considered.</param>
        /// <returns>True iff the permission belongs to the administration group.</returns>
        protected override bool IsIncluded(Desc desc)
        {
            var code = desc.Code;

            if (code == Permission.AllowViewingAndEditingGeneralSettings ||
                code == Permission.AllowViewingAndEditingGlobalIPSettings)
            {
                return true;
            }
            else if (code == Permission.AllowViewingAndEditingTeams)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.EnableTeamsUI;
                }
            }
            else if (code == Permission.AllowViewingAndEditingBranches)
            {
                return true;
            }
            else if (code == Permission.AllowViewingAndEditingConsumerPortalConfigs)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.IsEnableNewConsumerPortal;
                }
            }
            else if (code == Permission.AllowViewingAndEditingEventNotifications)
            {
                return true;
            }
            else if (code == Permission.AllowViewingAndEditingCustomFieldChoices)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.HasLenderDefaultFeatures;
                }
            }
            else if (code == Permission.AllowViewingAndEditingConditionChoices)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.HasLenderDefaultFeatures;
                }
            }
            else if (code == Permission.AllowViewingAndEditingLeadSources)
            {
                return true;
            }
            else if (code == Permission.AllowViewingAndEditingEdocsConfiguration)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.IsEDocsEnabled;
                }
            }
            else if (
                code == Permission.AllowViewingAndEditingArmIndexEntries ||
                code == Permission.AllowViewingAndEditingManualLoanPrograms ||
                code == Permission.AllowViewingAndEditingAdjustmentsAndOtherCreditsSetup ||
                code == Permission.AllowViewingAndEditingFeeTypeSetup || 
                code == Permission.AllowAccessingSecurityEventLogsPage)
            {
                return true;
            }
            else if (code == Permission.AllowViewingAndEditingGFEFeeService)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return this.CurrentBroker.IsEnabledGfeFeeService;
                }
            }
            else if (code == Permission.AllowViewingAndEditingDisablePricing)
            {
                // visibility of link still determined by if any lock policies have "is using rate expiration feature".
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return PrincipalFactory.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                }
            }
            else if (code == Permission.AllowViewingAndEditingPriceGroups)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return PrincipalFactory.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                }
            }
            else if (code == Permission.AllowViewingAndEditingLockDeskQuestions)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    return PrincipalFactory.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                }
            }
            else if (code == Permission.AllowManageNewFeatures)
            {
                return true;
            }
            else if (code == Permission.AllowManagingFailedDocs)
            {
                return PrincipalFactory.CurrentPrincipal is InternalUserPrincipal || this.CurrentBroker.IsBarcodeUploadScriptEnabled;
            }
            else if (code == Permission.AllowAccessingCorporateAdminDocumentCaptureAuditPage ||
                code == Permission.AllowAccessingCorporateAdminDocumentCaptureReviewPage)
            {
                return PrincipalFactory.CurrentPrincipal is InternalUserPrincipal || this.CurrentBroker.EnableKtaIntegration;
            }
            else if (code == Permission.AllowViewingAndEditingCustomNavigation)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
