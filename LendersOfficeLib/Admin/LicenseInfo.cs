﻿using System;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.UI;

namespace LendersOffice.Admin
{
    [Serializable]
    public class LicenseInfo : IEquatable<LicenseInfo>
    {
        // TODO Make expiration date available as a DateTime
        private string m_State = "";
        private string m_ExpirationDate = "";
        private string m_License = "";
        private string m_Addr = "";
        private string m_Zip = "";
        private string m_City = "";
        private string m_AddrState = "";
        private string m_Phone = "";
        private string m_Fax = "";
        private string m_DisplayName = "";
        private bool m_editable = false;
        #region ( License Info Properties )

        public string DefaultDisplayName;
        public string DefaultStreet;
        public string DefaultCity;
        public string DefaultPhone;
        public string DefaultZip;
        public string DefaultAddrState;
        public string DefaultFax;

        [XmlElement]
        public String Street
        {
            set { m_Addr = value; }
            get {
                if (!Editable && DefaultStreet != null)
                    return DefaultStreet;
                return m_Addr;

            }
        }

        [XmlElement]
        public String Zip
        {
            set { m_Zip = value; }
            get {
                if (!Editable && DefaultZip != null)
                    return DefaultZip;
                return m_Zip; }
        }

        [XmlElement]
        public String City
        {
            set { m_City = value; }
            get {
                if (!Editable && DefaultCity != null)
                    return DefaultCity;
                return m_City; }
        }

        [XmlElement]
        public String AddrState
        {
            set { m_AddrState = value; }
            get {
                if (!Editable && DefaultAddrState != null)
                    return DefaultAddrState;
                return m_AddrState; }
        }

        [XmlElement]
        public String Phone
        {
            set { m_Phone = value; }
            get {
                if (!Editable && DefaultPhone != null)
                    return DefaultPhone;
                return m_Phone; }
        }

        [XmlElement]
        public String Fax
        {
            set { m_Fax = value; }
            get {
                if (!Editable && DefaultFax != null)
                    return DefaultFax;
                return m_Fax; }
        }

        [XmlElement]
        public String DisplayName
        {
            set { m_DisplayName = value; }
            get {
                if (!Editable && DefaultDisplayName != null)
                    return DefaultDisplayName;
                return m_DisplayName; }
        }

        [XmlElement]
        [LqbInputModel(required: true)]
        public String State
        {
            set { m_State = value; }
            get { return m_State; }
        }

        [XmlElement]
        [LqbInputModel(type: InputFieldType.Date, required: true)]
        public String ExpD
        {
            set { m_ExpirationDate = value; }
            get { return m_ExpirationDate; }
        }

        [XmlElement]
        [LqbInputModel(required: true)]
        public String License
        {
            set { m_License = value; }
            get { return m_License; }
        }

        [XmlElement]
        public bool Editable
        {
            set { m_editable = value; }
            get { return m_editable; }
        }
        #endregion
        [XmlIgnore]
        public DateTime ExpirationDate
        {
            get
            {
                // 6/15/2009 dd - This return the DateTime of ExpD
                if (string.IsNullOrEmpty(ExpD))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    DateTime dt;
                    if (!DateTime.TryParse(ExpD, out dt))
                    {
                        dt = DateTime.MinValue;
                    }
                    return dt;
                }
            }
        }


        public LicenseInfo()
        {
        }
        /*
         *         private string m_Addr = "";
        private string m_Zip = "";
        private string m_City = "";
        private string m_AddrState = "";
        private string m_Phone = "";
        private string m_Fax = "";
        */
        public LicenseInfo(string state, string expDate, string licenseNum)
        {
            if (null != state)
                m_State = state.ToUpper();
            if (null != expDate)
                m_ExpirationDate = expDate;

            if ((null == licenseNum) || (licenseNum.TrimWhitespaceAndBOM().Equals("")))
                throw new CBaseException("Lending Licenses must contain a License Number", "Lending Licenses must contain a License Number");
            else
                m_License = licenseNum;
        }

        public LicenseInfo(string state, string expDate, string licenseNum, string displayName, string addr, string city, string addrState, string zip, string phone, string fax, bool editable)
        {
            if (null != state)
                m_State = state.ToUpper();
            if (null != expDate)
                m_ExpirationDate = expDate;
            if (null != addr)
                m_Addr = addr;

            if (null != displayName)
                m_DisplayName = displayName;
            if (null != city)
                m_City = city;
            if (null != addrState)
                m_AddrState = addrState;
            if (null != zip)
                m_Zip = zip;
            if (null != phone)
                m_Phone = phone;
            if (null != fax)
                m_Fax = fax;

                m_editable = editable;


            if ((null == licenseNum) || (licenseNum.TrimWhitespaceAndBOM().Equals("")))
                throw new CBaseException("Lending Licenses must contain a License Number", "Lending Licenses must contain a License Number");
            else
                m_License = licenseNum;
        }

        /// <summary>
        /// Checks if two LicenseInfo objects are equal to each other.
        /// </summary>
        /// <param name="other">The other LicenseInfo object to compare against.</param>
        /// <returns>True if all properties are equal, false otherwise.</returns>
        public bool Equals(LicenseInfo other)
        {
            return this.State == other.State &&
                   this.ExpirationDate == other.ExpirationDate &&
                   this.Street == other.Street &&
                   this.DisplayName == other.DisplayName &&
                   this.City == other.City &&
                   this.AddrState == other.AddrState &&
                   this.Zip == other.Zip &&
                   this.Phone == other.Phone &&
                   this.Fax == other.Fax &&
                   this.License == other.License;
        }

        /// <summary>
        /// Overrides the Object.Equals method.
        /// </summary>
        /// <param name="obj">The LicenseInfo to check against.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as LicenseInfo);
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns>The hash code.</returns>
        /// <remarks>
        /// http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
        /// </remarks>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 23;
                int filler = 0;
                hash = hash * 31 + this.State?.GetHashCode() ?? filler;
                hash = hash * 31 + this.ExpirationDate.GetHashCode();
                hash = hash * 31 + this.Street?.GetHashCode() ?? filler;
                hash = hash * 31 + this.DisplayName?.GetHashCode() ?? filler;
                hash = hash * 31 + this.City?.GetHashCode() ?? filler;
                hash = hash * 31 + this.AddrState?.GetHashCode() ?? filler;
                hash = hash * 31 + this.Zip?.GetHashCode() ?? filler;
                hash = hash * 31 + this.Phone?.GetHashCode() ?? filler;
                hash = hash * 31 + this.Fax?.GetHashCode() ?? filler;
                hash = hash * 31 + this.License?.GetHashCode() ?? filler;

                return hash;
            }
        }
    }

}
