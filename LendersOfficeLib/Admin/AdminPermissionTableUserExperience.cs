﻿// <copyright file="AdminPermissionTableUserExperience.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Scott Kibler
//  Date:   1/12/2016 11:37:23 AM
// </summary>
namespace LendersOffice.Admin
{
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// The class represents the User Experience group of the Employee Permissions List.
    /// </summary>
    public class AdminPermissionTableUserExperience : AdminPermissionTable
    {
        /// <summary>
        /// Returns true for permissions that belong in the user experience group of the employee permissions editor.
        /// </summary>
        /// <param name="desc">The description object for the permission to be considered.</param>
        /// <returns>True iff the permission belongs to the user experience group.</returns>
        protected override bool IsIncluded(Desc desc)
        {
            if (desc.Code == Permission.AllowViewingNewLoanEditorUI)
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return true;
                }
                else
                {
                    var currentBroker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                    return E_PermissionType.DeferToMoreGranularLevel == currentBroker.ActualNewLoanEditorUIPermissionType;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
