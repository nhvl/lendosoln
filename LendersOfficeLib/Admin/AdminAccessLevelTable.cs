﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class AdminAccessLevelTable : BrokerUserPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            // Only include our access level settings.

            switch (pDesc.Code)
            {
                case Permission.BrokerLevelAccess:
                case Permission.BranchLevelAccess:
                case Permission.IndividualLevelAccess:
                case Permission.TeamLevelAccess:
                    {
                        return true;
                    }
            }

            return false;
        }

        #endregion

    }

}
