﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    public enum E_EmployeeStatusFilterT
    {
        All = 0,
        ActiveOnly = 1,
        InactiveOnly = 2

    }
}
