﻿// <copyright file="SecurityQuestion.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//      This is SecurityQuestion class.
//      
//      Author: David Dao
//      Date: 4/8/2015
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// A class represent user security question and answer.
    /// </summary>
    public class SecurityQuestion
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="SecurityQuestion" /> class from being created.
        /// </summary>
        private SecurityQuestion()
        {
        }

        /// <summary>
        /// Gets the unique id of the question.
        /// </summary>
        /// <value>The unique id of the question.</value>
        public int QuestionId { get; private set; }

        /// <summary>
        /// Gets the text of the question.
        /// </summary>
        /// <value>The text of the question.</value>
        public string Question { get; private set; }

        /// <summary>
        /// Gets a value indicating whether question is obsolete.
        /// </summary>
        /// <value>Indicating whether the question is obsolete.</value>
        public bool IsObsolete { get; private set; }

        /// <summary>
        /// Gets the friendly description of obsolete.
        /// </summary>
        /// <value>Friendly description of obsolete.</value>
        public string IsObsolete_rep
        {
            get { return this.IsObsolete ? "Yes" : "No"; }
        }

        /// <summary>
        /// Gets a list of system security questions.
        /// </summary>
        /// <param name="isIncludeObsoleteQuestions">Whether to include obsolete question in the list.</param>
        /// <returns>A list of security questions.</returns>
        public static IEnumerable<SecurityQuestion> ListAllSecurityQuestions(bool isIncludeObsoleteQuestions)
        {
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@IncludeObsolete", isIncludeObsoleteQuestions)
                                        };

            List<SecurityQuestion> list = new List<SecurityQuestion>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetAllSecurityQuestions", parameters))
            {
                while (reader.Read())
                {
                    SecurityQuestion item = new SecurityQuestion();
                    item.QuestionId = (int)reader["QuestionId"];
                    item.Question = (string)reader["Question"];
                    item.IsObsolete = (bool)reader["IsObsolete"];

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Find a security question by id. This method will throw exception if id does not match.
        /// </summary>
        /// <param name="id">Id of the question to search.</param>
        /// <returns>A security question object.</returns>
        public static SecurityQuestion RetrieveById(int id)
        {
            // 4/8/2015 dd - Currently only internal user need to call this method AND our list of questions is short, so we just load up the whole list
            // and iterate through to find id. 
            // Once the list get large or this method get call frequently then we optimize it.
            return ListAllSecurityQuestions(true).First(o => o.QuestionId == id);
        }

        /// <summary>
        /// Creates a new security question.
        /// </summary>
        /// <param name="question">The text of the question.</param>
        /// <returns>A new id of the new question.
        /// </returns>
        public static int CreateSecurityQuestion(string question)
        {
            SqlParameter paramQuestion = new SqlParameter("@Question", question);
            SqlParameter paramReturnQuestionId = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturnQuestionId.Direction = ParameterDirection.ReturnValue;

            SqlParameter[] parameters = 
                                        {
                                            paramQuestion,
                                            paramReturnQuestionId
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "CreateSecurityQuestion", 0, parameters);
            return (int)paramReturnQuestionId.Value;
        }

        /// <summary>
        /// Mark a question as obsolete.
        /// </summary>
        /// <param name="questionId">The id of the question.</param>
        /// <param name="isObsolete">Whether the question will be obsolete.</param>
        public static void MarkQuestionObsolete(int questionId, bool isObsolete)
        {
            SqlParameter paramQuestionId = new SqlParameter("@QuestionId", questionId);
            SqlParameter paramIsObsolete = new SqlParameter("@IsObsolete", isObsolete);
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            SqlParameter[] parameters = 
                                        {
                                            paramQuestionId,
                                            paramIsObsolete,
                                            paramReturn
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "MarkQuestionObsolete", 0, parameters);
        } 
    }
}
