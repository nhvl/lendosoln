﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class DeveloperPermissionTable : InternalPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            // Remove all but editable by developers.

            switch (pDesc.Code)
            {
                case Permission.CanModifyLoanPrograms:
                    {
                        return true;
                    }
            }

            return base.IsIncluded(pDesc);
        }

        #endregion

    }
}
