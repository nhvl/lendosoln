﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Load loan's employee assignments from the database.
    /// </summary>

    public class LoanAssignments : ArrayList
    {
        /// <summary>
        /// Provide access to 4 components of an assignment.
        /// </summary>

        public class Spec
        {

            public Spec(E_RoleT roleType, Guid employeeId)
            {
                this.RoleType = roleType;
                this.EmployeeId = employeeId;
                this.RoleName = Role.Get(roleType).Desc;
            }
            /// <summary>
            /// Provide access to 4 components of an assignment.
            /// </summary>
            public E_RoleT RoleType { get; private set; }
            public string RoleName { get; private set; }
            public Guid EmployeeId { get; private set; }


        }

        public Spec this[String sRoleDesc]
        {
            // Lookup matching role, or return null.

            get
            {
                foreach (Spec aSpec in this)
                {
                    if (aSpec.RoleName == sRoleDesc)
                    {
                        return aSpec;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Look up matching role by type. Return null if not found.
        /// </summary>
        /// <param name="roleType">Role Type.</param>
        /// <returns></returns>
        public Spec this[E_RoleT roleType]
        {
            get
            {
                foreach (Spec aSpec in this)
                {
                    if (aSpec.RoleType == roleType)
                    {
                        return aSpec;
                    }
                }

                return null;
            }
        }
        /// <summary>
        /// Append onto the tail.  We don't bother looking for
        /// redundancy because it takes too long.
        /// </summary>

        public void Add(E_RoleT roleType, Guid employeeId)
        {
            if (employeeId == Guid.Empty)
            {
                return;
            }
            // Append onto the tail.  We don't bother looking
            // for redundancy because it takes too long.

            Spec aSpec = new Spec(roleType, employeeId);

            Add(aSpec);
        }

        internal IDataReader m_Record
        {
            // Retrieve access control information.

            set
            {
                Object o;

                o = value["sEmployeeManagerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Manager, (Guid)o);
                }

                o = value["sEmployeeUnderwriterId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Underwriter, (Guid)o);
                }

                o = value["sEmployeeLockDeskId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LockDesk, (Guid)o);
                }

                o = value["sEmployeeProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Processor, (Guid)o);
                }

                o = value["sEmployeeLoanOpenerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOpener, (Guid)o);
                }

                o = value["sEmployeeLoanRepId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOfficer, (Guid)o);
                }

                o = value["sEmployeeLenderAccExecId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LenderAccountExecutive, (Guid)o);
                }

                o = value["sEmployeeRealEstateAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.RealEstateAgent, (Guid)o);
                }

                o = value["sEmployeeCallCenterAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CallCenterAgent, (Guid)o);
                }

                o = value["sEmployeeCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Closer, (Guid)o);
                }

                o = value["sEmployeeBrokerProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_BrokerProcessor, (Guid)o);
                }

                o = value["sEmployeeFunderId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Funder, (Guid)o);
                }

                o = value["sEmployeeShipperId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Shipper, (Guid)o);
                }

                o = value["sEmployeePostCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.PostCloser, (Guid)o);
                }

                o = value["sEmployeeInsuringId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Insuring, (Guid)o);
                }

                o = value["sEmployeeCollateralAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CollateralAgent, (Guid)o);
                }

                o = value["sEmployeeDocDrawerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.DocDrawer, (Guid)o);
                }

                o = value["sEmployeeCreditAuditorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CreditAuditor, (Guid)o);
                }

                o = value["sEmployeeDisclosureDeskId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.DisclosureDesk, (Guid)o);
                }

                o = value["sEmployeeJuniorProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.JuniorProcessor, (Guid)o);
                }

                o = value["sEmployeeJuniorUnderwriterId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.JuniorUnderwriter, (Guid)o);
                }

                o = value["sEmployeeLegalAuditorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LegalAuditor, (Guid)o);
                }

                o = value["sEmployeeLoanOfficerAssistantId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOfficerAssistant, (Guid)o);
                }
                
                o = value["sEmployeePurchaserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Purchaser, (Guid)o);
                }

                o = value["sEmployeeQCComplianceId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.QCCompliance, (Guid)o);
                }

                o = value["sEmployeeSecondaryId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Secondary, (Guid)o);
                }

                o = value["sEmployeeServicingId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Servicing, (Guid)o);
                }

                o = value["sEmployeeExternalSecondaryId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_Secondary, (Guid)o);
                }

                o = value["sEmployeeExternalPostCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_PostCloser, (Guid)o);
                }
            }
        }

        internal DataRow m_RowInfo
        {
            // Retrieve access control information.

            set
            {
                Object o;

                o = value["sEmployeeManagerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Manager, (Guid)o);
                }

                o = value["sEmployeeUnderwriterId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Underwriter, (Guid)o);
                }

                o = value["sEmployeeLockDeskId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LockDesk, (Guid)o);
                }

                o = value["sEmployeeProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Processor, (Guid)o);
                }

                o = value["sEmployeeLoanOpenerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOpener, (Guid)o);
                }

                o = value["sEmployeeLoanRepId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOfficer, (Guid)o);
                }

                o = value["sEmployeeLenderAccExecId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LenderAccountExecutive, (Guid)o);
                }

                o = value["sEmployeeRealEstateAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.RealEstateAgent, (Guid)o);
                }

                o = value["sEmployeeCallCenterAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CallCenterAgent, (Guid)o);
                }

                o = value["sEmployeeCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Closer, (Guid)o);
                }

                o = value["sEmployeeBrokerProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_BrokerProcessor, (Guid)o);
                }

                o = value["sEmployeeFunderId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Funder, (Guid)o);
                }

                o = value["sEmployeeShipperId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Shipper, (Guid)o);
                }

                o = value["sEmployeePostCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.PostCloser, (Guid)o);
                }

                o = value["sEmployeeInsuringId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Insuring, (Guid)o);
                }

                o = value["sEmployeeCollateralAgentId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CollateralAgent, (Guid)o);
                }

                o = value["sEmployeeDocDrawerId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.DocDrawer, (Guid)o);
                }

                o = value["sEmployeeCreditAuditorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.CreditAuditor, (Guid)o);
                }

                o = value["sEmployeeDisclosureDeskId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.DisclosureDesk, (Guid)o);
                }

                o = value["sEmployeeJuniorProcessorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.JuniorProcessor, (Guid)o);
                }

                o = value["sEmployeeJuniorUnderwriterId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.JuniorUnderwriter, (Guid)o);
                }

                o = value["sEmployeeLegalAuditorId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LegalAuditor, (Guid)o);
                }

                o = value["sEmployeeLoanOfficerAssistantId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.LoanOfficerAssistant, (Guid)o);
                }
                
                o = value["sEmployeePurchaserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Purchaser, (Guid)o);
                }

                o = value["sEmployeeQCComplianceId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.QCCompliance, (Guid)o);
                }

                o = value["sEmployeeSecondaryId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Secondary, (Guid)o);
                }

                o = value["sEmployeeServicingId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Servicing, (Guid)o);
                }

                o = value["sEmployeeExternalSecondaryId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_Secondary, (Guid)o);
                }

                o = value["sEmployeeExternalPostCloserId"];

                if (o != null && o is DBNull == false)
                {
                    Add(E_RoleT.Pml_PostCloser, (Guid)o);
                }
            }
        }

        /// <summary>
        /// Clear our set and reload using the given identifier.
        /// </summary>

        public void Retrieve(Guid brokerId, Guid loanId)
        {
            // Clear our set and reload using the given
            // identifier.

            Clear();
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", loanId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetAccessControlLoanAssignments", parameters))
            {
                if (reader.Read() == true)
                {
                    m_Record = reader;
                }
            }
        }

        /// <summary>
        /// Check if the employee is assigned to the given
        /// role in our set.
        /// </summary>
        /// <returns>
        /// If the assignment isn't even present, then we
        /// return 0.  If present and matching the given
        /// employee, then we return 1.  If present and
        /// not matching, then we return -1.
        /// </returns>

        public int TestAssignment(string roleName, Guid employeeId)
        {
            // Search for match and return.

            if (roleName != null)
            {
                foreach (Spec aSpec in this)
                {
                    if (aSpec.RoleName == roleName)
                    {
                        if (aSpec.EmployeeId == employeeId)
                        {
                            return +1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Test if the given role name is part of our set.
        /// </summary>
        public bool HasAssignment(string roleName)
        {
            // Search for match and return.

            foreach (Spec aSpec in this)
            {
                if (aSpec.RoleName == roleName)
                {
                    return true;
                }
            }

            return false;
        }
        public bool HasAssignment(E_RoleT roleType)
        {
            // Search for match and return.

            foreach (Spec aSpec in this)
            {
                if (aSpec.RoleType == roleType)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Test if the given employee is part of our set.
        /// </summary>

        public bool IsAssigned(Guid employeeId)
        {
            // Search for match and return.

            foreach (Spec aSpec in this)
            {
                if (aSpec.EmployeeId == employeeId)
                {
                    return true;
                }
            }

            return false;
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAssignments(Guid brokerId, Guid loanId)
        {
            // Initialize our list of assignments.

            Retrieve(brokerId, loanId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAssignments()
        {
        }

        #endregion

    }
}
