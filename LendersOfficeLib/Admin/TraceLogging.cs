using System;
using System.Collections;
using System.Diagnostics;

namespace LendersOffice.Admin
{
	/// <summary>
	/// Keep a short list of recently written log messages.
	/// </summary>

	public class TraceHistory
	{
		/// <summary>
		/// Keep track of most recent messages.
		/// </summary>

		private ArrayList   m_List = new ArrayList();
		private Int32 m_MaxEntries = 1024;
		private Boolean       m_On = false;

		#region ( Trace history properties )

		public String[] Entries
		{
			// Access member.

			get
			{
				lock( this )
				{
					String[] sList = new String[ m_List.Count ];

					for( int i = 0 ; i < sList.Length ; ++i )
					{
						sList[ i ] = m_List[ i ] as String;
					}

					return sList;
				}
			}
		}

		public Int32 MaxEntries
		{
			// Access member.

			set
			{
				lock( this )
				{
					while( m_List.Count > value )
					{
						m_List.RemoveAt( 0 );
					}

					m_MaxEntries = value;
				}
			}
			get
			{
				lock( this )
				{
					return m_MaxEntries;
				}
			}
		}

		public Boolean On
		{
			// Access member.

			set
			{
				lock( this )
				{
					m_On = value;
				}
			}
			get
			{
				lock( this )
				{
					return m_On;
				}
			}
		}

		#endregion

		#region( Trace history listener )

		/// <summary>
		/// Add all trace requests to our history for the
		/// lifetime of the listener.
		/// </summary>

		private class Listener : TraceListener
		{
			/// <summary>
			/// Keep a reference to our shared history log.
			/// </summary>

			private TraceHistory m_History;

			/// <summary>
			/// Write message to our shared history.
			/// </summary>

			public override void WriteLine( String sMsg )
			{
				// Delegate to embedded logger.

				Write( sMsg );
			}

			/// <summary>
			/// Write message to our shared history.
			/// </summary>

			public override void Write( String sMsg )
			{
				// Delegate to embedded logger.

				m_History.Add( DateTime.Now + " " + sMsg );
			}

			/// <summary>
			/// Construct default.
			/// </summary>

			public Listener( TraceHistory tHistory )
			{
				// Initialize members.

				m_History = tHistory;
			}

		}

		#endregion

		/// <summary>
		/// Append a new one to our list.  We evict off the top
		/// when we reach the limit.
		/// </summary>

		public void Add( String sMsg )
		{
			// Append a new entry to our set.

			lock( this )
			{
				if( m_On == true )
				{
					while( m_List.Count >= m_MaxEntries )
					{
						m_List.RemoveAt( 0 );
					}

					if( m_MaxEntries > 0 )
					{
						m_List.Add( sMsg );
					}
				}
			}
		}

		/// <summary>
		/// Dump all the entries in our shared list.
		/// </summary>

		public void Clear()
		{
			// Dump all the entries in the list.

			lock( this )
			{
				m_List.Clear();
			}
		}

		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public TraceHistory()
		{
			// Initialize members.

			Trace.Listeners.Add( new Listener( this ) );
		}

		#endregion

	}

	/// <summary>
	/// We provide a simple accessor for reading the application's
	/// shared log history.
	/// </summary>

	public class TraceProcessor
	{
		/// <summary>
		/// We provide a simple accessor for reading the
		/// application's shared log history.
		/// </summary>

		private static TraceHistory m_History = new TraceHistory();

		public String[] Entries
		{
			// Access member.

			get
			{
				return m_History.Entries;
			}
		}

		public Int32 MaxEntries
		{
			// Access member.

			set
			{
				m_History.MaxEntries = value;
			}
			get
			{
				return m_History.MaxEntries;
			}
		}

		public Boolean On
		{
			// Access member.

			set
			{
				m_History.On = value;
			}
			get
			{
				return m_History.On;
			}
		}

		/// <summary>
		/// Drop all entries from the list and start fresh.
		/// </summary>

		public void Clear()
		{
			// Delegate to our shared history.

			m_History.Clear();
		}

		#region ( Activation methods )

		/// <summary>
		/// Start the tracing.
		/// </summary>

		public void Start()
		{
			// Turn on our history tracer.

			m_History.On = true;
		}

		/// <summary>
		/// Stop the tracing.
		/// </summary>

		public void Stop()
		{
			// Turn off our history tracer.

			m_History.On = false;
		}

		#endregion

	}

}
