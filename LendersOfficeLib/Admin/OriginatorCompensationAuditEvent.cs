﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using System.Xml.Linq;
using System.Threading;

namespace LendersOffice.Admin
{
    public class OriginatorCompensationAuditEvent
    {
        #region Getters/Setters
        private string by_rep;

        public int Id
        {
            get;
            set;
        }

        private string Date
        {
            get;
            set;
        }

        public string Date_rep
        {
            get { return Date + " PDT"; }
        }

        public DateTime DateTime
        {
            get
            {
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParse(Date, out dt) == false)
                {
                    dt = DateTime.MinValue;
                }
                return dt;
            }
        }
        private Guid By
        {
            get;
            set;
        }

        public string By_rep
        {
            get { return by_rep; }
        }

        public string Rate
        {
            get;
            set;
        }

        public string OnlyFor1stLienOfCombo
        {
            get;
            set;
        }

        private E_PercentBaseT BaseT
        {
            get;
            set;
        }

        public string BaseT_rep
        {
            get { return CPageBase.sPercentBaseT_map_rep(BaseT); }
        }

        public string Minimum
        {
            get;
            set;
        }

        public string Maximum
        {
            get;
            set;
        }

        public string FixedAmount
        {
            get;
            set;
        }

        public string Notes
        {
            get;
            set;
        }
        #endregion
        private const string INTERNAL_EDITOR_NAME = "Internal";

        public OriginatorCompensationAuditEvent(int id, Guid by, string byName, string date, string rate, string onlyFor1stLienOfCombo, E_PercentBaseT baseT, string minimum, string maximum, string fixedAmount, string notes)
        {
            Id = id;
            Date = date;
            Rate = rate;
            OnlyFor1stLienOfCombo = onlyFor1stLienOfCombo;
            BaseT = baseT;
            Minimum = minimum;
            Maximum = maximum;
            FixedAmount = fixedAmount;
            Notes = notes;
            By = by;

            if (By == Guid.Empty)
            {
                by_rep = INTERNAL_EDITOR_NAME;
            }
            else
            {
                by_rep = byName;
            }
        }

        public XElement GetAuditEventXElement()
        {
            return new XElement("AuditEvent"
                , new XElement("Id", Id)
                , new XElement("Date", Date)
                , new XElement("By", By)
                , new XElement("By_rep", by_rep)
                , new XElement("Rate", Rate)
                , new XElement("OnlyFor1stLienOfCombo", OnlyFor1stLienOfCombo)
                , new XElement("BaseT", BaseT.ToString("D"))
                , new XElement("Minimum", Minimum)
                , new XElement("Maximum", Maximum)
                , new XElement("FixedAmount", FixedAmount)
                , new XElement("Notes", Notes)                
                );
        }
    }
}