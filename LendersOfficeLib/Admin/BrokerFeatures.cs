﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOffice.Admin
{
    public enum E_BrokerFeatureT
    {
        PricingEngine,
        PriceMyLoan,
        MarketingTools,
        BasicUser,
        ConsumerWebSite
    }
    /// <summary>
    /// Load broker's active feature subscriptions from the
    /// database.
    /// </summary>
    public class BrokerFeatures
    {
        private List<E_BrokerFeatureT> m_featureList = new List<E_BrokerFeatureT>();

        /// <summary>
        /// Clear our set and reload using the given identifier.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            Retrieve(connInfo, brokerId);
        }


        private void Retrieve(DbConnectionInfo connInfo, Guid brokerId)
        {
            // Clear our set and reload using the given
            // identifier.

            m_featureList = new List<E_BrokerFeatureT>();

            if (brokerId == Guid.Empty)
            {
                return;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetAccessControlBrokerFeatures", parameters))
            {
                while (reader.Read())
                {
                    Guid featureId = (Guid)reader["FeatureId"];

                    E_BrokerFeatureT feature = E_BrokerFeatureT.BasicUser;

                    if (featureId == ConstApp.Feature_PricingEngine)
                    {
                        feature = E_BrokerFeatureT.PricingEngine;
                    }
                    else if (featureId == ConstApp.Feature_PriceMyLoan)
                    {
                        feature = E_BrokerFeatureT.PriceMyLoan;
                    }
                    else if (featureId == ConstApp.Feature_MarketingTools)
                    {
                        feature = E_BrokerFeatureT.MarketingTools;
                    }
                    else if (featureId == ConstApp.Feature_BasicUser)
                    {
                        feature = E_BrokerFeatureT.BasicUser;
                    }
                    else if (featureId == new Guid("158F4E4F-BCB5-4BC6-91E7-7296F413980B")) 
                    {
                        feature = E_BrokerFeatureT.ConsumerWebSite;
                    }
                    else
                    {
                        throw CBaseException.GenericException("FeatureId=[" + featureId + "] is not supported.");
                    }
                    m_featureList.Add(feature);
                }

            }
        }

        /// <summary>
        /// Test if the given feature id is part of our set.
        /// </summary>

        public bool HasFeature(E_BrokerFeatureT feature)
        {
            // Search for match and return.
            foreach (var o in m_featureList)
            {
                if (o == feature)
                {
                    return true;
                }
            }
            return false;

        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerFeatures(DbConnectionInfo connInfo, Guid brokerId)
        {
            // Initialize our list of features.

            Retrieve(connInfo, brokerId);
        }

        public BrokerFeatures(Guid brokerId)
        {
            Retrieve(brokerId);
        }
        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerFeatures()
        {
        }

        #endregion

    }
}
