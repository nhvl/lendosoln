﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using DataAccess;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableEDocs : AdminPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            // Throw out these old/special permissions;
            // not to be used by our customers.

            switch (pDesc.Code)
            {
                case Permission.CanViewEDocs:
                case Permission.CanEditEDocs:
                case Permission.CanViewEDocsInternalNotes:
                case Permission.CanEditEDocsInternalNotes:
                case Permission.AllowScreeningEDocs:
                case Permission.AllowApprovingRejectingEDocs: 
                case Permission.AllowEditingApprovedEDocs:
                case Permission.AllowHideEDocsFromPML:
                case Permission.AllowCreatingDocumentSigningEnvelopes:
                    return true;
                case Permission.ExportDocRequestsToTestingPath:
                    return IsPerTransaction() && VendorHasTestingExport();

                default :
                    return false;
            }

        }

        private bool VendorHasTestingExport()
        {
            if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
            {
                return true;
            }
            return LendersOffice.Integration.DocumentVendor.DocumentVendorFactory.AvailableVendors().Any(v => v.TestingExportPath != null);
        }

        private bool IsPerTransaction()
        {
            if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
            {
                //we should probably do another check
                return true;
            }
            else
            {
                return BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).BillingVersion == E_BrokerBillingVersion.PerTransaction && BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).IsEnablePTMDocMagicSeamlessInterface;
            }
        }

        #endregion
    }
}
