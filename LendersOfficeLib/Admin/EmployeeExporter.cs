﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;

    /// <summary>
    /// Provides functionality for exporting employee information.
    /// </summary>
    public class EmployeeExporter
    {
        /// <summary>
        /// Indicates whether the data has been retrieved from the data source.
        /// </summary>
        private bool loadedData;

        /// <summary>
        /// Map from user id to the date that user's account was deactivated.
        /// </summary>
        private Dictionary<Guid, DateTime> deactivationDateByUserId;

        /// <summary>
        /// Employee data for all employees.
        /// </summary>
        private BrokerEmployeeNameTable employeeDetails;

        /// <summary>
        /// Contact information for all employees.
        /// </summary>
        private BrokerEmployeeContactTable employeeContactInfo;

        /// <summary>
        /// Role membership information for all employees.
        /// </summary>
        private BrokerLoanAssignmentTable employeeRoles;

        /// <summary>
        /// All of the branch names, indexed by id.
        /// </summary>
        private BrokerBranchTable branches;

        /// <summary>
        /// The relationship information for all employees.
        /// </summary>
        private BrokerEmployeeRelationships employeeRelationships;

        /// <summary>
        /// A map from price group id to price group name.
        /// </summary>
        private Dictionary<Guid, string> priceGroupNameById;

        /// <summary>
        /// The client certificates for all employees.
        /// </summary>
        private ClientCertificates clientCertificates;

        /// <summary>
        /// The registered IP addresses for all employees.
        /// </summary>
        private UserRegisteredIpAddresses registeredIpAddresses;

        /// <summary>
        /// A converter for formatting data for export.
        /// </summary>
        private LosConvert losConvert;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeExporter"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The broker id whose employees will be exported.
        /// </param>
        public EmployeeExporter(Guid brokerId)
        {
            this.BrokerId = brokerId;
            this.losConvert = new LosConvert();
        }

        /// <summary>
        /// Gets the user permissions for all employees.
        /// </summary>
        /// <value>The user permissions for all employees.</value>
        protected BrokerUserPermissionsSet UserPermissions { get; private set; }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        protected Guid BrokerId { get; private set; }

        /// <summary>
        /// Exports employee information to csv.
        /// </summary>
        /// <returns>A csv string of employee information.</returns>
        public string ExportCsv()
        {
            if (!this.loadedData)
            {
                this.LoadData();
            }

            var csvTable = new StringBuilder();
            var header = this.GetHeader();
            csvTable.Append(header);

            foreach (EmployeeDetails employee in this.employeeDetails.Values)
            {
                var permissions = this.UserPermissions[employee.EmployeeId];
                if (permissions != null && permissions.IsInternalBrokerUser())
                {
                    continue;
                }
                else if (employee.Type != "B" && employee.Type != string.Empty)
                {
                    continue;
                }

                var row = this.GetEmployeeRow(employee);
                csvTable.Append(row);
            }

            return csvTable.ToString();
        }

        /// <summary>
        /// Gets the header for the csv.
        /// </summary>
        /// <returns>A StringBuilder that contains the header for the csv.</returns>
        protected virtual StringBuilder GetHeader()
        {
            var header = new StringBuilder();
            var baseColumns = "Branch,Status,First Name,Last Name,Login,Start,Final,Address,Phone,Email,Roles,Access,Billing License #,Agent License #,Lending Licenses, Notes,Manager,Lock Desk,Lender Account Executive,Underwriter,Processor,Price Group,Loan Originator NMLS ID,Employee ID,Originator Compensation Plan Effective Date,Originator Compensation Only Paid for the 1st Lien of a Combo,Originator Compensation %,Originator Compensation Basis,Originator Compensation Minimum,Originator Compensation Maximum,Originator Compensation Fixed Amount,Originator Compensation Notes,Junior Underwriter,Junior Processor,Loan Officer Assistant, Chums ID,Last Used IP Address,Last Access Time,IP Address Restriction,IPs on Global Whitelist,Devices with a Certificate,Whitelisted IPs,Multi-Factor Authentication,Allow Installing Digital Certificates,List of Digital Certificates,IP Access,Enable Auth Code via SMS,Has Cell Phone Number";
            header.Append(baseColumns);
            return header;
        }

        /// <summary>
        /// Gets the row for the given employee.
        /// </summary>
        /// <param name="employee">The employee to export.</param>
        /// <returns>A StringBuilder that contains the row.</returns>
        protected virtual StringBuilder GetEmployeeRow(EmployeeDetails employee)
        {
            var row = new StringBuilder();
            row.Append(Environment.NewLine);

            if (this.branches.Contains(employee.BranchId))
            {
                row.Append(this.branches[employee.BranchId].BranchName.Replace(",", string.Empty));
            }
            else
            {
                row.Append("?");
            }

            row.Append(",");

            if (employee.IsActive)
            {
                if (employee.Type != string.Empty)
                {
                    if (employee.CanLogin)
                    {
                        row.Append("Active");
                    }
                    else
                    {
                        row.Append("Disabled");
                    }
                }
                else
                {
                    row.Append("New");
                }
            }
            else
            {
                row.Append("Inactive");
            }

            row.Append(",");
            row.Append(employee.FirstName.Replace(",", string.Empty));
            row.Append(",");
            row.Append(employee.LastName.Replace(",", string.Empty));
            row.Append(",");
            row.Append(employee.Login.Replace(",", string.Empty));
            row.Append(",");

            if (this.employeeContactInfo.Contains(employee.EmployeeId))
            {
                row.Append(this.employeeContactInfo[employee.EmployeeId].StartDate.ToString());
            }

            row.Append(",");

            if (employee.CanLogin != true && this.deactivationDateByUserId.ContainsKey(employee.UserId))
            {
                row.Append(this.deactivationDateByUserId[employee.UserId].ToString());
            }

            row.Append(",");

            if (this.employeeContactInfo.Contains(employee.EmployeeId))
            {
                row.Append(this.employeeContactInfo[employee.EmployeeId].FullAddress.Replace(",", string.Empty));
            }

            row.Append(",");
            row.Append(employee.Phone.Replace(",", string.Empty));
            row.Append(",");
            row.Append(employee.Email.Replace(",", string.Empty));
            row.Append(",");

            if (this.employeeRoles.Contains(employee.EmployeeId))
            {
                int c = 0;
                foreach (var tR in this.employeeRoles[employee.EmployeeId])
                {
                    row.Append((c > 0 ? " + " : string.Empty) + tR.ModifiableDesc);
                    ++c;
                }
            }

            row.Append(",");

            BrokerUserPermissions permissions = this.UserPermissions[employee.EmployeeId];
            if (permissions != null && permissions.HasPermission(Permission.BrokerLevelAccess))
            {
                row.Append("Corporate");
            }
            else if (permissions != null && permissions.HasPermission(Permission.BranchLevelAccess))
            {
                row.Append("Branch");
            }
            else if (permissions != null && permissions.HasPermission(Permission.TeamLevelAccess))
            {
                row.Append("Team");
            }
            else if (permissions != null && permissions.HasPermission(Permission.IndividualLevelAccess))
            {
                row.Append("Individual");
            }
            else
            {
                row.Append("Unknown");
            }

            row.Append(",");
            if (employee.LicenseNumber == -1)
            {
                row.Append(string.Empty);
            }
            else
            {
                row.Append(employee.LicenseNumber);
            }

            // 11/30/06 db - OPM 8513
            row.Append(",");
            row.Append(employee.AgentLicenseNumber);

            // OPM 240160 - Export State Licences
            row.Append(",");
            if (employee.LicenseInfoList != null)
            {
                row.Append(employee.LicenseInfoList.ToCsv());
            }

            row.AppendFormat(",\"{0}\"", employee.NotesByEmployer.Replace("\"", "\"\""));

            // 7/21/08 jk - OPM 20411 Include "Relationship" settings
            EmployeeRelationships rel = this.employeeRelationships[employee.EmployeeId];
            var relationshipSet = rel[RelationshipSetType.LendingQB];

            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.Manager))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.Manager].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.LockDesk))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.LockDesk].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.LenderAccountExecutive))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.LenderAccountExecutive].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");

            if (relationshipSet.Contains(E_RoleT.Underwriter))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.Underwriter].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");

            if (relationshipSet.Contains(E_RoleT.Processor))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.Processor].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");

            if (this.priceGroupNameById.ContainsKey(employee.LpePriceGroupId))
            {
                row.Append(this.priceGroupNameById[employee.LpePriceGroupId].Replace(",", string.Empty));
            }

            // 9/7/11 mp - OPM 67921 Add "Loan Originator Identifier"
            // tbNmlsIdentifier
            row.Append(",");
            row.Append(employee.LosIdentifier);

            row.Append(",");
            row.Append(employee.EmployeeIDInCompany);

            // 6/28/13 pa - OPM 64852 Add Originator Compensation data
            row.Append(",");
            row.Append(employee.OriginatorCompensationLastModifiedDate.HasValue ? this.losConvert.ToDateTimeString(employee.OriginatorCompensationLastModifiedDate.Value) : string.Empty);
            row.Append(",");
            row.Append(employee.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo ? "Yes" : string.Empty);
            row.Append(",");
            row.Append(this.losConvert.ToRateString(employee.OriginatorCompensationPercent).Replace(",", string.Empty));
            row.Append(",");
            row.Append(employee.OriginatorCompensationBasis);
            row.Append(",");
            row.Append(this.losConvert.ToMoneyString(employee.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", string.Empty));
            row.Append(",");
            row.Append(this.losConvert.ToMoneyString(employee.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", string.Empty));
            row.Append(",");
            row.Append(this.losConvert.ToMoneyString(employee.OriginatorCompensationFixedAmount, FormatDirection.ToRep).Replace(",", string.Empty));
            row.Append(",");
            row.AppendFormat("\"{0}\"", employee.OriginatorCompensationNotes.Replace("\"", "\"\"").Replace(Environment.NewLine, " "));

            // 12/12/2013 gf - opm 145015 add the new junior underwriter and junior processor relationships.
            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.JuniorUnderwriter))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.JuniorUnderwriter].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.JuniorProcessor))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.JuniorProcessor].EmployeeId].FullName.Replace(",", string.Empty));
            }

            // 4/14/14 gf - opm 178407
            row.Append(",");
            if (relationshipSet.Contains(E_RoleT.LoanOfficerAssistant))
            {
                row.Append(this.employeeDetails[relationshipSet[E_RoleT.LoanOfficerAssistant].EmployeeId].FullName.Replace(",", string.Empty));
            }

            row.Append(",");
            row.Append(employee.ChumsID);

            row.Append(",");
            row.Append(employee.LastUsedIpAddress);

            row.Append(",");
            row.Append(employee.RecentLoginD.HasValue ? employee.RecentLoginD.Value.ToString() : string.Empty);

            row.Append(",");
            row.Append(employee.EnabledIpRestriction ? "Restricted" : "Unrestricted");

            row.Append(",");
            row.Append(employee.EnabledIpRestriction ? "Yes" : string.Empty);

            row.Append(",");
            row.Append(employee.EnabledIpRestriction ? "Yes" : string.Empty);

            row.Append(",");
            if (!string.IsNullOrEmpty(employee.MustLogInFromTheseIpAddresses))
            {
                row.Append(string.Join("+", employee.MustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s))));
            }

            row.Append(",");
            row.Append(employee.EnabledMultiFactorAuthentication ? "Yes" : "No");

            row.Append(",");
            row.Append(employee.EnabledClientDigitalCertificateInstall ? "Yes" : "No");

            row.Append(",");
            var certificates = this.clientCertificates.GetCertificatesForUser(employee.UserId)
                .OrderByDescending(c => c.CreatedDate)
                .Select(c => c.Description + "::" + c.CreatedDate);
            row.Append(string.Join("+", certificates));

            row.Append(",");
            var ips = this.registeredIpAddresses.GetRegisteredIpsForUser(employee.UserId)
                .Select(ip => ip.IpAddress);
            row.Append(string.Join("+", ips));

            row.Append(",");
            row.Append(employee.EnableAuthCodeViaSms ? "Yes" : "No");

            row.Append(",");
            row.Append(employee.HasCellPhoneNumber ? "Yes" : "No");

            return row;
        }

        /// <summary>
        /// Loads all of the data needed to perform the export.
        /// </summary>
        protected virtual void LoadData()
        {
            Action<Action, string> tryLoadData = (action, errorMessage) =>
            {
                try
                {
                    action();
                }
                catch (SqlException e)
                {
                    Tools.LogError(errorMessage + " BrokerId: " + this.BrokerId, e);
                }
            };

            this.employeeDetails = new BrokerEmployeeNameTable();
            tryLoadData(
                () => this.employeeDetails.Retrieve(this.BrokerId),
                "Failed to load employee name table.");

            this.employeeContactInfo = new BrokerEmployeeContactTable();
            tryLoadData(
                () => this.employeeContactInfo.Retrieve(this.BrokerId),
                "Failed to load employee contact table.");

            this.employeeRoles = new BrokerLoanAssignmentTable();
            tryLoadData(
                () => this.employeeRoles.Retrieve(this.BrokerId),
                "Failed to load loan assignment table.");

            this.branches = new BrokerBranchTable();
            tryLoadData(
                () => this.branches.Retrieve(this.BrokerId),
                "Failed to load broker branches.");

            this.UserPermissions = new BrokerUserPermissionsSet();
            tryLoadData(
                () => this.UserPermissions.Retrieve(this.BrokerId),
                "Failed to load user permissions.");

            this.employeeRelationships = new BrokerEmployeeRelationships();
            tryLoadData(
                () => this.employeeRelationships.Retrieve(this.BrokerId),
                "Failed to load employee relationships.");

            this.priceGroupNameById = new Dictionary<Guid, string>();
            tryLoadData(
                () => this.LoadPriceGroups(),
                "Failed to load Price Groups.");

            this.clientCertificates = new ClientCertificates(this.BrokerId);

            this.registeredIpAddresses = new UserRegisteredIpAddresses(this.BrokerId);

            this.LoadEmployeeDeactivationDates();

            this.loadedData = true;
        }

        /// <summary>
        /// Loads the deactivation dates for users.
        /// </summary>
        private void LoadEmployeeDeactivationDates()
        {
            this.deactivationDateByUserId = new Dictionary<Guid, DateTime>();
            DataSet invoiceUsageEventsDataSet = new DataSet();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerID", this.BrokerId)
            };

            DataSetHelper.Fill(
                invoiceUsageEventsDataSet,
                this.BrokerId,
                "ListInvoiceUsageEvents",
                parameters);

            foreach (DataTable dataTable in invoiceUsageEventsDataSet.Tables)
            {
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    Guid userId = (Guid)dataRow["UserId"];

                    if (dataRow["EventType"].ToString().ToLower() == "f")
                    {
                        DateTime currFinal, eventDate = (DateTime)dataRow["EventDate"];

                        if (this.deactivationDateByUserId.ContainsKey(userId) == false)
                        {
                            this.deactivationDateByUserId.Add(userId, eventDate);
                        }

                        currFinal = this.deactivationDateByUserId[userId];

                        if (currFinal.CompareTo(eventDate) < 0)
                        {
                            this.deactivationDateByUserId[userId] = eventDate;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the price groups.
        /// </summary>
        private void LoadPriceGroups()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(
                this.BrokerId,
                "ListPricingGroupByBrokerId",
                parameters))
            {
                while (reader.Read())
                {
                    string groupNm = (string)reader["LpePriceGroupName"];
                    Guid groupId = (Guid)reader["LpePriceGroupId"];

                    if ((bool)reader["ExternalPriceGroupEnabled"] == false)
                    {
                        groupNm += " (disabled)";
                    }

                    this.priceGroupNameById.Add(groupId, groupNm);
                }
            }
        }
    }
}
