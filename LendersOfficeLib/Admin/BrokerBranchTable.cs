﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Track individual branches by their identifiers.
    /// </summary>

    public class BrokerBranchTable
    {
        /// <summary>
        /// Track individual branches by their identifiers.
        /// </summary>

        private ListDictionary m_Table = new ListDictionary();

        public Spec this[Guid branchId]
        {
            // Access member.

            get
            {
                return m_Table[branchId] as Spec;
            }
        }

        /// <summary>
        /// Track individual branches by name with id.
        /// </summary>

        public class Spec
        {
            /// <summary>
            /// Track branch details for binding and lookup.
            /// </summary>

            private String m_BranchName = String.Empty;
            private Guid m_BranchId = Guid.Empty;

            #region( Spec properties )

            public String BranchName
            {
                // Access member.

                set
                {
                    m_BranchName = value;
                }
                get
                {
                    return m_BranchName;
                }
            }

            public Guid BranchId
            {
                // Access member.

                set
                {
                    m_BranchId = value;
                }
                get
                {
                    return m_BranchId;
                }
            }

            #endregion

        }

        /// <summary>
        /// Load the broker's branches and index them in our
        /// table of specs.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            // Load the broker's branches and index them in our
            // table of specs.
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListBranchByBrokerId", parameters))
            {
                while (sR.Read() == true)
                {
                    Spec bS = new Spec();

                    bS.BranchName = (String)sR["Name"];
                    bS.BranchId = (Guid)sR["BranchId"];

                    m_Table.Add(bS.BranchId, bS);
                }
            }
        }

        /// <summary>
        /// Check our table to see if we loaded the branch.
        /// </summary>

        public Boolean Contains(Guid branchId)
        {
            // Check our table to see if we loaded the branch.

            return m_Table.Contains(branchId);
        }

        /// <summary>
        /// Return the looping interface for added entries.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return the looping interface for added entries.

            return m_Table.Values.GetEnumerator();
        }

        /// <summary>
        /// Handle clearing out the table for reloading.
        /// </summary>

        public void Clear()
        {
            // Handle clearing out the table for reloading.

            m_Table.Clear();
        }

    }
}
