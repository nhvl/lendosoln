﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableDisclosures : BrokerUserPermissionTable
    {
        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.ResponsibleForInitialDiscRetail:
                case Permission.ResponsibleForInitialDiscWholesale:
                case Permission.ResponsibleForRedisclosures:
                    return true;
                default:
                    return false;
            }
        }
    }
}
