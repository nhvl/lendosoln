﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    //OPM 2259
    public class AdminPermissionTableCustomForms : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override Boolean IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanCreateCustomForms:
                case Permission.CanEditOthersCustomForms:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
