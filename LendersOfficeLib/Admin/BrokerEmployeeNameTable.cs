﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Data;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Store a broker's employees' names by their employee ids.  We
    /// cache the entire broker in a Dictionary.
    /// </summary>
    public class BrokerEmployeeNameTable : Dictionary<Guid, EmployeeDetails>
    {
        /// <summary>
        /// Load the employees and index them into our table by
        /// their Employee IDs.
        /// </summary>
        public void Retrieve(Guid brokerId, string searchFilter = "", DateTime? earliestLastModifiedDate = null, string employeeType = null)
        {
            // Load the employees and index them into our table
            // by their ids.

            List<SqlParameter> parameters = new List<SqlParameter>();

            if (searchFilter != null && searchFilter != String.Empty)
            {
                String[] searchParts = searchFilter.Split(' ');

                if (searchParts.Length > 1 && searchParts[1] != String.Empty)
                {
                    parameters.Add(new SqlParameter("@LastFilter", searchParts[1]));
                }

                if (searchParts[0] != String.Empty)
                {
                    parameters.Add(new SqlParameter("@NameFilter", searchParts[0]));
                }
            }

            if (earliestLastModifiedDate.HasValue)
            {
                parameters.Add(new SqlParameter("@EarliestLastModifiedDate", earliestLastModifiedDate.Value));
            }

            if (!string.IsNullOrEmpty(employeeType) && employeeType.Length == 1)
            {
                parameters.Add(new SqlParameter("@EmployeeType", employeeType));
            }


            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeDetailsByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    // Create a new entry for this employee's name spec.
                    EmployeeDetails eS = new EmployeeDetails();
                    eS.m_Record = reader;
                    this.Add(eS.EmployeeId, eS);
                }
            }
        }
    }
}
