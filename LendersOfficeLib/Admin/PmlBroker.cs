﻿// Author: David Dao
// 6/8/2009.
namespace LendersOffice.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using CommonLib;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.LoanFileCache;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    [Serializable]
    public class PmlBroker : IComparable<PmlBroker>
    {
        #region Private Members
        private bool m_isNew = true;
        private bool m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
        private decimal m_OriginatorCompensationPercent;
        private E_PercentBaseT m_OriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount;
        private decimal m_OriginatorCompensationMinAmount;
        private decimal m_OriginatorCompensationMaxAmount;
        private decimal m_OriginatorCompensationFixedAmount;
        private string m_OriginatorCompensationNotes = "";
        private DateTime? m_OriginatorCompensationLastModifiedDate = null;
        private OriginatorCompensationAudit m_OriginatorCompensationAudit = new OriginatorCompensationAudit("");
        private bool m_OriginatorCompensationAuditNeedsUpdate = false;
        private string m_NmlsName = "";

        private Guid brokerId = Guid.Empty;
        private readonly Func<BrokerDB> brokerInitializationFunction;
        private Lazy<BrokerDB> broker;

        private bool useDefaultWholesalePriceGroupId = false;
        private bool useDefaultMiniCorrPriceGroupId = false;
        private bool useDefaultCorrPriceGroupId = false;
        private bool useDefaultWholesaleBranchId = false;
        private bool useDefaultMiniCorrBranchId = false;
        private bool useDefaultCorrBranchId = false;
        private LosConvert losConvert = new LosConvert();
        #endregion

        #region Public Properties
        public Guid BrokerId
        {
            get { return this.brokerId; }
            protected set
            {
                var oldValue = this.brokerId;

                this.brokerId = value;

                if (oldValue != value && this.broker.IsValueCreated)
                {
                    this.broker = new Lazy<BrokerDB>(this.brokerInitializationFunction);
                }
            }
        }
        public Guid PmlBrokerId { get; private set; }
        public bool IsActive { get; set; }
        public bool isDirty { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// The Display Name is value that will fill the Company Name values on the loans, and should contain the name that is
        /// registered with the NMLS.
        /// </summary>
        public string NmlsName {
            get
            {
                if (!NmlsNameLckd)
                {
                    return Name;
                }
                else
                {
                    return m_NmlsName;
                }
            }
            set
            {
                m_NmlsName = value;
            }
        }

        public bool NmlsNameLckd { get; set; }
        public string Addr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string CompanyId { get; set; }
        public int PmlCompanyTierId { get; set; }

        private Guid? encryptionKeyId;
        private string unencryptedTaxId;

        public string TaxId
        {
            get
            {
                EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(this.unencryptedTaxId);
                if (ein.HasValue)
                {
                    return this.losConvert.ToEmployerTaxIdentificationNumberString(ein);
                }

                return this.unencryptedTaxId;
            }
            set
            {
                this.unencryptedTaxId = EmployerTaxIdentificationNumber.Create(value)?.ToString() ?? value;
            }
        }

        private bool taxIdValidOnLoad = true;
        public bool TaxIdValid
        {
            get
            {
                return string.IsNullOrEmpty(this.unencryptedTaxId) || EmployerTaxIdentificationNumber.Create(this.unencryptedTaxId).HasValue;
            }
        }

        public string PrincipalName1 { get; set; }
        public string PrincipalName2 { get; set; }
        public string GroupNameList { get; set; }
        public string GroupIdList { get; set; }
        public string GroupIdList_ImporterUse { get; set; }
        public string NmLsIdentifier { get; set; }

        public LicenseInfoList LicenseInfoList { get; set; }

        public string CustomPricingPolicyField1 { get; set; }
        public string CustomPricingPolicyField2 { get; set; }
        public string CustomPricingPolicyField3 { get; set; }
        public string CustomPricingPolicyField4 { get; set; }
        public string CustomPricingPolicyField5 { get; set; }

        public virtual BrokerDB Broker
        {
            get { return this.broker.Value; }
        }

        public HashSet<E_OCRoles> Roles
        {
            get;
            set;
        }

        public E_UnderwritingAuthority UnderwritingAuthority { get; set; }

        // OPM 175635, 2/17/2016, ML
        private OCStatusType originalBrokerRoleStatusT;
        public OCStatusType BrokerRoleStatusT
        {
            get;
            set;
        }

        private OCStatusType originalMiniCorrRoleStatusT;
        public OCStatusType MiniCorrRoleStatusT
        {
            get;
            set;
        }

        private OCStatusType originalCorrRoleStatusT;
        public OCStatusType CorrRoleStatusT
        {
            get;
            set;
        }

        public string BrokerRoleStatusNm
        {
            get { return ConstApp.FriendlyOCStatusTypes[this.BrokerRoleStatusT]; }
        }

        public string MiniCorrRoleStatusNm
        {
            get { return ConstApp.FriendlyOCStatusTypes[this.MiniCorrRoleStatusT]; }
        }

        public string CorrRoleStatusNm
        {
            get { return ConstApp.FriendlyOCStatusTypes[this.CorrRoleStatusT]; }
        }

        public Guid? TPOPortalConfigID { get; set; }
        public Guid? MiniCorrespondentTPOPortalConfigID { get; set; }
        public Guid? CorrespondentTPOPortalConfigID { get; set; }

        private Guid? m_LpePriceGroupId;
        public Guid? LpePriceGroupId
        {
            get
            {
                if (this.UseDefaultWholesalePriceGroupId)
                {
                    return this.Broker.DefaultWholesalePriceGroupId;
                }

                return this.m_LpePriceGroupId;
            }
            set
            {
                this.m_LpePriceGroupId = value;
                this.m_LpePriceGroupName = null;
                this.useDefaultWholesalePriceGroupId = false;
            }
        }

        public bool UseDefaultWholesalePriceGroupId
        {
            get { return this.useDefaultWholesalePriceGroupId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultWholesalePriceGroupId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultWholesalePriceGroupId' to true when the broker does not have a default defined.");
                    }

                    this.LpePriceGroupId = this.Broker.DefaultWholesalePriceGroupId;
                }

                this.useDefaultWholesalePriceGroupId = value;
            }
        }

        private Guid? m_MiniCorrespondentLpePriceGroupId;
        public Guid? MiniCorrespondentLpePriceGroupId
        {
            get
            {
                if (this.UseDefaultMiniCorrPriceGroupId)
                {
                    return this.Broker.DefaultMiniCorrPriceGroupId;
                }

                return this.m_MiniCorrespondentLpePriceGroupId;
            }
            set
            {
                this.m_MiniCorrespondentLpePriceGroupId = value;
                this.m_MiniCorrespondentLpePriceGroupName = null;
                this.useDefaultMiniCorrPriceGroupId = false;
            }
        }

        public bool UseDefaultMiniCorrPriceGroupId
        {
            get { return this.useDefaultMiniCorrPriceGroupId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultMiniCorrPriceGroupId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultMiniCorrPriceGroupId' to true when the broker does not have a default defined.");
                    }

                    this.MiniCorrespondentLpePriceGroupId = this.Broker.DefaultMiniCorrPriceGroupId;
                }

                this.useDefaultMiniCorrPriceGroupId = value;
            }
        }

        private Guid? m_CorrespondentLpePriceGroupId;
        public Guid? CorrespondentLpePriceGroupId
        {
            get
            {
                if (this.UseDefaultCorrPriceGroupId)
                {
                    return this.Broker.DefaultCorrPriceGroupId;
                }

                return this.m_CorrespondentLpePriceGroupId;
            }
            set
            {
                this.m_CorrespondentLpePriceGroupId = value;
                this.m_CorrespondentLpePriceGroupName = null;
                this.useDefaultCorrPriceGroupId = false;
            }
        }

        public bool UseDefaultCorrPriceGroupId
        {
            get { return this.useDefaultCorrPriceGroupId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultCorrPriceGroupId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultCorrPriceGroupId' to true when the broker does not have a default defined.");
                    }

                    this.CorrespondentLpePriceGroupId = this.Broker.DefaultCorrPriceGroupId;
                }

                this.useDefaultCorrPriceGroupId = value;
            }
        }

        private string m_LpePriceGroupName = null;
        public string LpePriceGroupName
        {
            get
            {
                if (LpePriceGroupId == null)
                {
                    return "";
                }

                if (LpePriceGroupId == Guid.Empty)
                {
                    return "Branch Default";
                }

                if (m_LpePriceGroupName == null)
                {
                    m_LpePriceGroupName = LendersOffice.ObjLib.PriceGroups.PriceGroup.RetrieveByID(LpePriceGroupId.Value, BrokerId).Name;
                }

                return m_LpePriceGroupName;
            }
        }

        private string m_MiniCorrespondentLpePriceGroupName = null;
        public string MiniCorrespondentLpePriceGroupName
        {
            get
            {
                if (MiniCorrespondentLpePriceGroupId == null)
                {
                    return "";
                }

                if (MiniCorrespondentLpePriceGroupId == Guid.Empty)
                {
                    return "Branch Default";
                }

                if (m_MiniCorrespondentLpePriceGroupName == null)
                {
                    m_MiniCorrespondentLpePriceGroupName = LendersOffice.ObjLib.PriceGroups.PriceGroup.RetrieveByID(MiniCorrespondentLpePriceGroupId.Value, BrokerId).Name;
                }

                return m_MiniCorrespondentLpePriceGroupName;
            }
        }

        private string m_CorrespondentLpePriceGroupName = null;
        public string CorrespondentLpePriceGroupName
        {
            get
            {
                if (CorrespondentLpePriceGroupId == null)
                {
                    return "";
                }

                if (CorrespondentLpePriceGroupId == Guid.Empty)
                {
                    return "Branch Default";
                }

                if (m_CorrespondentLpePriceGroupName == null)
                {
                    m_CorrespondentLpePriceGroupName = LendersOffice.ObjLib.PriceGroups.PriceGroup.RetrieveByID(CorrespondentLpePriceGroupId.Value, BrokerId).Name;
                }

                return m_CorrespondentLpePriceGroupName;
            }
        }

        private Guid? m_BranchId;
        public Guid? BranchId
        {
            get
            {
                if (this.UseDefaultWholesaleBranchId)
                {
                    return this.Broker.DefaultWholesaleBranchId;
                }

                return this.m_BranchId;
            }
            set
            {
                this.m_BranchId = value;
                this.m_BranchNm = null;
                this.useDefaultWholesaleBranchId = false;
            }
        }

        public bool UseDefaultWholesaleBranchId
        {
            get { return this.useDefaultWholesaleBranchId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultWholesaleBranchId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultWholesaleBranchId' to true when the broker does not have a default defined.");
                    }

                    this.BranchId = this.Broker.DefaultWholesaleBranchId;
                }

                this.useDefaultWholesaleBranchId = value;
            }
        }

        private string m_BranchNm = null;
        public string BranchNm
        {
            get
            {
                if (BranchId == null)
                {
                    return "";
                }

                if (m_BranchNm == null)
                {
                    BranchDB branch = new BranchDB(BranchId.Value, BrokerId);
                    branch.Retrieve();

                    m_BranchNm = branch.Name;
                }

                return m_BranchNm;
            }
        }

        private Guid? m_MiniCorrespondentBranchId;
        public Guid? MiniCorrespondentBranchId
        {
            get
            {
                if (this.UseDefaultMiniCorrBranchId)
                {
                    return this.Broker.DefaultMiniCorrBranchId;
                }

                return this.m_MiniCorrespondentBranchId;
            }
            set
            {
                this.m_MiniCorrespondentBranchId = value;
                this.m_MiniCorrespondentBranchNm = null;
                this.useDefaultMiniCorrBranchId = false;
            }
        }

        public bool UseDefaultMiniCorrBranchId
        {
            get { return this.useDefaultMiniCorrBranchId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultMiniCorrBranchId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultMiniCorrBranchId' to true when the broker does not have a default defined.");
                    }

                    this.MiniCorrespondentBranchId = this.Broker.DefaultMiniCorrBranchId;
                }

                this.useDefaultMiniCorrBranchId = value;
            }
        }

        private string m_MiniCorrespondentBranchNm = null;
        public string MiniCorrespondentBranchNm
        {
            get
            {
                if (MiniCorrespondentBranchId == null)
                {
                    return "";
                }

                if (m_MiniCorrespondentBranchNm == null)
                {
                    BranchDB branch = new BranchDB(MiniCorrespondentBranchId.Value, BrokerId);
                    branch.Retrieve();

                    m_MiniCorrespondentBranchNm = branch.Name;
                }

                return m_MiniCorrespondentBranchNm;
            }
        }

        private Guid? m_CorrespondentBranchId;
        public Guid? CorrespondentBranchId
        {
            get
            {
                if (this.UseDefaultCorrBranchId)
                {
                    return this.Broker.DefaultCorrBranchId;
                }

                return this.m_CorrespondentBranchId;
            }
            set
            {
                this.m_CorrespondentBranchId = value;
                this.m_CorrespondentBranchNm = null;
                this.useDefaultCorrBranchId = false;
            }
        }

        public bool UseDefaultCorrBranchId
        {
            get { return this.useDefaultCorrBranchId && ConstStage.EnableDefaultBranchesAndPriceGroups; }
            set
            {
                if (value && ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    if (!this.Broker.DefaultCorrBranchId.HasValue)
                    {
                        throw new GenericUserErrorMessageException(
                            "Cannot set 'UseDefaultCorrBranchId' to true when the broker does not have a default defined.");
                    }

                    this.CorrespondentBranchId = this.Broker.DefaultCorrBranchId;
                }

                this.useDefaultCorrBranchId = value;
            }
        }

        private string m_CorrespondentBranchNm = null;
        public string CorrespondentBranchNm
        {
            get
            {
                if (CorrespondentBranchId == null)
                {
                    return "";
                }

                if (m_CorrespondentBranchNm == null)
                {
                    BranchDB branch = new BranchDB(CorrespondentBranchId.Value, BrokerId);
                    branch.Retrieve();

                    m_CorrespondentBranchNm = branch.Name;
                }

                return m_CorrespondentBranchNm;
            }
        }

        public bool OriginatorCompensationIsOnlyPaidForFirstLienOfCombo
        {
            get { return m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo; }
            set
            {
                if (m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo != value)
                {
                    m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        private Guid? tpoColorThemeId = null;
        public Guid? TpoColorThemeId
        {
            get
            {
                return this.tpoColorThemeId;
            }
            set
            {
                if (value == null || value == Guid.Empty)
                {
                    this.tpoColorThemeId = null;
                    return;
                }

                if (value == this.tpoColorThemeId)
                {
                    return;
                }

                var themeCollection = ThemeManager.GetThemeCollection(this.BrokerId, ThemeCollectionType.TpoPortal);
                if (!themeCollection.HasTheme(value.Value))
                {
                    throw new InvalidOperationException("Cannot set selcted OC theme to nonexistent theme " + value);
                }

                this.tpoColorThemeId = value;
            }
        }

        public decimal OriginatorCompensationPercent
        {
            get { return m_OriginatorCompensationPercent; }
            set
            {
                if (m_OriginatorCompensationPercent != value)
                {
                    m_OriginatorCompensationPercent = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        public E_PercentBaseT OriginatorCompensationBaseT
        {
            get { return m_OriginatorCompensationBaseT; }
            set
            {
                if (value != E_PercentBaseT.LoanAmount && value != E_PercentBaseT.TotalLoanAmount)
                {
                    throw new CBaseException(
                        ErrorMessages.InvalidValueForField(value, "OriginatorCompensationBaseT"),
                        ErrorMessages.InvalidValueForField(value, "OriginatorCompensationBaseT")
                        );
                }
                if (m_OriginatorCompensationBaseT != value)
                {
                    m_OriginatorCompensationBaseT = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        public decimal OriginatorCompensationMinAmount
        {
            get { return m_OriginatorCompensationMinAmount; }
            set
            {
                if (m_OriginatorCompensationMinAmount != value)
                {
                    m_OriginatorCompensationMinAmount = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        public decimal OriginatorCompensationMaxAmount
        {
            get { return m_OriginatorCompensationMaxAmount; }
            set
            {
                if (m_OriginatorCompensationMaxAmount != value)
                {
                    m_OriginatorCompensationMaxAmount = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        public decimal OriginatorCompensationFixedAmount
        {
            get { return m_OriginatorCompensationFixedAmount; }
            set
            {
                if (m_OriginatorCompensationFixedAmount != value)
                {
                    m_OriginatorCompensationFixedAmount = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        public string OriginatorCompensationNotes
        {
            get { return m_OriginatorCompensationNotes; }
            set
            {
                if (m_OriginatorCompensationNotes != value)
                {
                    m_OriginatorCompensationNotes = value;
                    m_OriginatorCompensationAuditNeedsUpdate = true;
                }
            }
        }

        /// <summary>
        /// This method was created so that the values can be LOADED without triggering an audit update, which shouldn't happen when 
        /// a pml broker is simply being loaded.
        /// </summary>
        public void LoadOriginatorCompensationValues(Decimal OriginatorCompensationPercent, E_PercentBaseT OriginatorCompensationBaseT, Decimal OriginatorCompensationMinAmount, Decimal OriginatorCompensationMaxAmount, Decimal OriginatorCompensationFixedAmount, string OriginatorCompensationNotes, bool OriginatorCompensationIsOnlyPaidForFirstLienOfCombo)
        {
            m_OriginatorCompensationPercent = OriginatorCompensationPercent;
            m_OriginatorCompensationBaseT = OriginatorCompensationBaseT;
            m_OriginatorCompensationMinAmount = OriginatorCompensationMinAmount;
            m_OriginatorCompensationMaxAmount = OriginatorCompensationMaxAmount;
            m_OriginatorCompensationFixedAmount = OriginatorCompensationFixedAmount;
            m_OriginatorCompensationNotes = OriginatorCompensationNotes;
            m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
        }

        public DateTime? OriginatorCompensationLastModifiedDate
        {
            get { return m_OriginatorCompensationLastModifiedDate; }
            set { m_OriginatorCompensationLastModifiedDate = value; }
        }

        public OriginatorCompensationAudit OriginatorCompensationAuditData
        {
            get
            {
                return m_OriginatorCompensationAudit;
            }
            set
            {
                m_OriginatorCompensationAudit = value;
            }
        }

        private string m_PMLNavigationPermissions = "";
        public string PMLNavigationPermissions
        {
            get
            {
                return m_PMLNavigationPermissions;
            }
            set
            {
                m_PMLNavigationPermissions = value;
            }
        }

        public Guid[] AccessiblePmlNavigationLinksId
        {
            get
            {
                string[] items = m_PMLNavigationPermissions.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return Array.ConvertAll(items, p => new Guid(p));
            }
            set
            {
                m_PMLNavigationPermissions = String.Join(",", Array.ConvertAll(value, p => p.ToString()));
            }
        }

        // OPM 181101, 2/18/2016, ML
        public PmlBrokerPermission Permissions { get; private set; }

        public PmlBrokerRelationship BrokerRoleRelationships { get; private set; }
        public PmlBrokerRelationship MiniCorrRoleRelationships { get; private set; }
        public PmlBrokerRelationship CorrRoleRelationships { get; private set; }

        /// <summary>
        /// Gets the index number of the originating company.
        /// </summary>
        /// <remarks>
        /// This field should behave similarly to a lender-level identity.
        /// </remarks>
        public int IndexNumber { get; private set; }
        #endregion

        #region Static Methods
        /// <summary>
        /// Returns the broker id for given loan id
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        private static Guid GetPmlBrokerIdForLoan(Guid brokerId, Guid sLId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };
            var o = StoredProcedureHelper.ExecuteScalar(brokerId, "GetLoanPmlBrokerId", parameters);
            return o == DBNull.Value ? Guid.Empty : (Guid)o;
        }

        /// <summary>
        /// Retrieves the pml broker for the given loan.
        /// </summary>
        /// <param name="brokerId">The broker the pml broker is in</param>
        /// <param name="sLId">The loan id</param>
        /// <returns>the pml broker for the associated loan or null</returns>
        public static PmlBroker RetrievePmlBrokerByLoanId(Guid brokerId, Guid sLId)
        {
            Guid id = GetPmlBrokerIdForLoan(brokerId, sLId);
            if (id == Guid.Empty)
            {
                return null;
            }

            return RetrievePmlBrokerById(id, brokerId);
        }

        // OPM 32445
        public static string CreatePMLRelationshipInfoCSV(string loginNm)
        {
            RoleFixup roleFixup = new RoleFixup();
            CsvTable csvTable = new CsvTable();

            #region Broker Relationships
            csvTable.SetLayout("Lender Account Exec", "LenderAccExecEmployeeId");
            csvTable.SetLayout("Lender Account Exec Phone", "LenderAccExecPhone");
            csvTable.SetLayout("Lender Account Exec Email", "LenderAccExecEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Lender Account Exec Login", "LenderAccExecLoginNm");
            }
            csvTable.SetLayout("Lock Desk", "LockDeskEmployeeId");
            csvTable.SetLayout("Lock Desk Phone", "LockDeskPhone");
            csvTable.SetLayout("Lock Desk Email", "LockDeskEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Lock Desk Login", "LockDeskLoginNm");
            }
            csvTable.SetLayout("Manager", "ManagerEmployeeId");
            csvTable.SetLayout("Manager Phone", "ManagerPhone");
            csvTable.SetLayout("Manager Email", "ManagerEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Manager Login", "ManagerLoginNm");
            }
            csvTable.SetLayout("Underwriter", "UnderwriterEmployeeId");
            csvTable.SetLayout("Underwriter Phone", "UnderwriterPhone");
            csvTable.SetLayout("Underwriter Email", "UnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Underwriter Login", "UnderwriterLoginNm");
            }
            csvTable.SetLayout("Processor", "ProcessorEmployeeId");
            csvTable.SetLayout("Processor Phone", "ProcessorPhone");
            csvTable.SetLayout("Processor Email", "ProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Processor Login", "ProcessorLoginNm");
            }
            csvTable.SetLayout("Processor (External)", "BrokerProcessorEmployeeId");
            csvTable.SetLayout("Processor (External) Phone", "BrokerProcessorPhone");
            csvTable.SetLayout("Processor (External) Email", "BrokerProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Processor (External) Login", "BrokerProcessorLoginNm");
            }
            csvTable.SetLayout("Junior Underwriter", "JuniorUnderwriterEmployeeId");
            csvTable.SetLayout("Junior Underwriter Phone", "JuniorUnderwriterPhone");
            csvTable.SetLayout("Junior Underwriter Email", "JuniorUnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Junior Underwriter Login", "JuniorUnderwriterLoginNm");
            }
            csvTable.SetLayout("Junior Processor", "JuniorProcessorEmployeeId");
            csvTable.SetLayout("Junior Processor Phone", "JuniorProcessorPhone");
            csvTable.SetLayout("Junior Processor Email", "JuniorProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Junior Processor Login", "JuniorProcessorLoginNm");
            }
            #endregion

            #region Mini-Correspondent Relationships
            csvTable.SetLayout("Mini-Correspondent Manager", "MiniCorrManagerEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Manager Phone", "MiniCorrManagerPhone");
            csvTable.SetLayout("Mini-Correspondent Manager Email", "MiniCorrManagerEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Manager Login", "MiniCorrManagerLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Processor", "MiniCorrProcessorEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Processor Phone", "MiniCorrProcessorPhone");
            csvTable.SetLayout("Mini-Correspondent Processor Email", "MiniCorrProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Processor Login", "MiniCorrProcessorLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Junior Processor", "MiniCorrJuniorProcessorEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Junior Processor Phone", "MiniCorrJuniorProcessorPhone");
            csvTable.SetLayout("Mini-Correspondent Junior Processor Email", "MiniCorrJuniorProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Junior Processor Login", "MiniCorrJuniorProcessorLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Lender Account Exec", "MiniCorrLenderAccExecEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Lender Account Exec Phone", "MiniCorrLenderAccExecPhone");
            csvTable.SetLayout("Mini-Correspondent Lender Account Exec Email", "MiniCorrLenderAccExecEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Lender Account Exec Login", "MiniCorrLenderAccExecLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Post-Closer (External)", "MiniCorrExternalPostCloserEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Post-Closer (External) Phone", "MiniCorrExternalPostCloserPhone");
            csvTable.SetLayout("Mini-Correspondent Post-Closer (External) Email", "MiniCorrExternalPostCloserEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Post-Closer (External) Login", "MiniCorrExternalPostCloserLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Underwriter", "MiniCorrUnderwriterEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Underwriter Phone", "MiniCorrUnderwriterPhone");
            csvTable.SetLayout("Mini-Correspondent Underwriter Email", "MiniCorrUnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Underwriter Login", "MiniCorrUnderwriterLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Junior Underwriter", "MiniCorrJuniorUnderwriterEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Junior Underwriter Phone", "MiniCorrJuniorUnderwriterPhone");
            csvTable.SetLayout("Mini-Correspondent Junior Underwriter Email", "MiniCorrJuniorUnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Junior Underwriter Login", "MiniCorrJuniorUnderwriterLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Credit Auditor", "MiniCorrCreditAuditorEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Credit Auditor Phone", "MiniCorrCreditAuditorPhone");
            csvTable.SetLayout("Mini-Correspondent Credit Auditor Email", "MiniCorrCreditAuditorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Credit Auditor Login", "MiniCorrCreditAuditorLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Legal Auditor", "MiniCorrLegalAuditorEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Legal Auditor Phone", "MiniCorrLegalAuditorPhone");
            csvTable.SetLayout("Mini-Correspondent Legal Auditor Email", "MiniCorrLegalAuditorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Legal Auditor Login", "MiniCorrLegalAuditorLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Lock Desk", "MiniCorrLockDeskEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Lock Desk Phone", "MiniCorrLockDeskPhone");
            csvTable.SetLayout("Mini-Correspondent Lock Desk Email", "MiniCorrLockDeskEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Lock Desk Login", "MiniCorrLockDeskLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Purchaser", "MiniCorrPurchaserEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Purchaser Phone", "MiniCorrPurchaserPhone");
            csvTable.SetLayout("Mini-Correspondent Purchaser Email", "MiniCorrPurchaserEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Purchaser Login", "MiniCorrPurchaserLoginNm");
            }

            csvTable.SetLayout("Mini-Correspondent Secondary", "MiniCorrSecondaryEmployeeId");
            csvTable.SetLayout("Mini-Correspondent Secondary Phone", "MiniCorrSecondaryPhone");
            csvTable.SetLayout("Mini-Correspondent Secondary Email", "MiniCorrSecondaryEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Mini-Correspondent Secondary Login", "MiniCorrSecondaryLoginNm");
            }
            #endregion

            #region Correspondent Relationships
            csvTable.SetLayout("Correspondent Manager", "CorrManagerEmployeeId");
            csvTable.SetLayout("Correspondent Manager Phone", "CorrManagerPhone");
            csvTable.SetLayout("Correspondent Manager Email", "CorrManagerEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Manager Login", "CorrManagerLoginNm");
            }

            csvTable.SetLayout("Correspondent Processor", "CorrProcessorEmployeeId");
            csvTable.SetLayout("Correspondent Processor Phone", "CorrProcessorPhone");
            csvTable.SetLayout("Correspondent Processor Email", "CorrProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Processor Login", "CorrProcessorLoginNm");
            }

            csvTable.SetLayout("Correspondent Junior Processor", "CorrJuniorProcessorEmployeeId");
            csvTable.SetLayout("Correspondent Junior Processor Phone", "CorrJuniorProcessorPhone");
            csvTable.SetLayout("Correspondent Junior Processor Email", "CorrJuniorProcessorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Junior Processor Login", "CorrJuniorProcessorLoginNm");
            }

            csvTable.SetLayout("Correspondent Lender Account Exec", "CorrLenderAccExecEmployeeId");
            csvTable.SetLayout("Correspondent Lender Account Exec Phone", "CorrLenderAccExecPhone");
            csvTable.SetLayout("Correspondent Lender Account Exec Email", "CorrLenderAccExecEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Lender Account Exec Login", "CorrLenderAccExecLoginNm");
            }

            csvTable.SetLayout("Correspondent Post-Closer (External)", "CorrExternalPostCloserEmployeeId");
            csvTable.SetLayout("Correspondent Post-Closer (External) Phone", "CorrExternalPostCloserPhone");
            csvTable.SetLayout("Correspondent Post-Closer (External) Email", "CorrExternalPostCloserEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Post-Closer (External) Login", "CorrExternalPostCloserLoginNm");
            }

            csvTable.SetLayout("Correspondent Underwriter", "CorrUnderwriterEmployeeId");
            csvTable.SetLayout("Correspondent Underwriter Phone", "CorrUnderwriterPhone");
            csvTable.SetLayout("Correspondent Underwriter Email", "CorrUnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Underwriter Login", "CorrUnderwriterLoginNm");
            }

            csvTable.SetLayout("Correspondent Junior Underwriter", "CorrJuniorUnderwriterEmployeeId");
            csvTable.SetLayout("Correspondent Junior Underwriter Phone", "CorrJuniorUnderwriterPhone");
            csvTable.SetLayout("Correspondent Junior Underwriter Email", "CorrJuniorUnderwriterEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Junior Underwriter Login", "CorrJuniorUnderwriterLoginNm");
            }

            csvTable.SetLayout("Correspondent Credit Auditor", "CorrCreditAuditorEmployeeId");
            csvTable.SetLayout("Correspondent Credit Auditor Phone", "CorrCreditAuditorPhone");
            csvTable.SetLayout("Correspondent Credit Auditor Email", "CorrCreditAuditorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Credit Auditor Login", "CorrCreditAuditorLoginNm");
            }

            csvTable.SetLayout("Correspondent Legal Auditor", "CorrLegalAuditorEmployeeId");
            csvTable.SetLayout("Correspondent Legal Auditor Phone", "CorrLegalAuditorPhone");
            csvTable.SetLayout("Correspondent Legal Auditor Email", "CorrLegalAuditorEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Legal Auditor Login", "CorrLegalAuditorLoginNm");
            }

            csvTable.SetLayout("Correspondent Lock Desk", "CorrLockDeskEmployeeId");
            csvTable.SetLayout("Correspondent Lock Desk Phone", "CorrLockDeskPhone");
            csvTable.SetLayout("Correspondent Lock Desk Email", "CorrLockDeskEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Lock Desk Login", "CorrLockDeskLoginNm");
            }

            csvTable.SetLayout("Correspondent Purchaser", "CorrPurchaserEmployeeId");
            csvTable.SetLayout("Correspondent Purchaser Phone", "CorrPurchaserPhone");
            csvTable.SetLayout("Correspondent Purchaser Email", "CorrPurchaserEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Purchaser Login", "CorrPurchaserLoginNm");
            }

            csvTable.SetLayout("Correspondent Secondary", "CorrSecondaryEmployeeId");
            csvTable.SetLayout("Correspondent Secondary Phone", "CorrSecondaryPhone");
            csvTable.SetLayout("Correspondent Secondary Email", "CorrSecondaryEmail");
            if (PrincipalFactory.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            {
                csvTable.SetLayout("Correspondent Secondary Login", "CorrSecondaryLoginNm");
            }
            #endregion

            csvTable.SetLayout("Populate Broker Relationships", "PopulateBrokerRelationships");
            csvTable.SetLayout("Populate Mini-Correspondent Relationships", "PopulateMiniCorrRelationships");
            csvTable.SetLayout("Populate Correspondent Relationships", "PopulateCorrRelationships");

            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", loginNm),
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            try
            {
                roleFixup.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to load role fixup table.", e);
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetPmlRelationshipsByLoginName", parameters))
            {
                csvTable.DataFilter = roleFixup;
                csvTable.DataSource = reader;
                csvTable.DataBind();
            }

            return csvTable.ToString();
        }

        public static string CreateContactInfoCSVForPmlSupervisor(Guid brokerId, Guid supervisorEmployeeId)
        {
            return CreateContactInfoCSVForPmlSupervisor(brokerId, supervisorEmployeeId, "", Guid.Empty);
        }

        private static IEnumerable<Tuple<string, string, Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>>> GetContactInfoCSVForPmlSupervisorMapper()
        {
            LosConvert convertLos = new LosConvert();

            var items = new[]
            {
                Tuple.Create("Login", "LoginNm", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LoginName)),
                Tuple.Create("First Name", "UserFirstNm",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.FirstName)),
                Tuple.Create("Last Name", "UserLastNm",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.LastName)),
                Tuple.Create("Company Name", "PmlBrokerName",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PmlBrokerName)),
                Tuple.Create("Phone", "Phone", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.Phone)),
                Tuple.Create("Cell Phone", "CellPhone",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CellPhone)),
                Tuple.Create("Pager", "Pager", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.Pager)),
                Tuple.Create("Fax", "Fax", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.Fax)),
                Tuple.Create("Email", "Email", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.Email)),
                Tuple.Create("IsActive", "IsActive", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.IsActive)),
                Tuple.Create("Is Loan Officer", "IsLoanOfficer", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.IsLoanOfficer)),
                Tuple.Create("Is Processor (External)", "IsBrokerProcessor", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.IsBrokerProcessor)),
                Tuple.Create("Manager", "ManagerEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.LenderAcctExecEmployeeID)),
                Tuple.Create("Lock Desk", "LockDeskEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.LockDeskEmployeeID)),
                Tuple.Create("Lender Account Exec", "LenderAccExecEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.LenderAcctExecEmployeeID)),
                Tuple.Create("Underwriter", "UnderwriterEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.UnderwriterEmployeeID)),
                Tuple.Create("Processor", "ProcessorEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.ProcessorEmployeeID)),
                Tuple.Create("Access Level", "PmlLevelAccessDesc", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PmlLevelAccessDesc)),
                Tuple.Create("Password Expiration Option", "PasswordExpirationOptionDesc", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PasswordExpirationOptionDesc)),
                Tuple.Create("Password Expiration Date", "PasswordExpirationD",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PasswordExpirationD)),
                Tuple.Create("Password Expiration Period", "PasswordExpirationPeriod", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PasswordExpirationPeriod)),
                Tuple.Create("Company Id", "PmlCompanyId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PmlCompanyId)),
                Tuple.Create("Portal Mode", "PortalMode",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.PortalMode)),
                Tuple.Create("Originator Compensation Plan Level", "OriginatorCompensationSetLevelT",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> FormatLevelT(emp.OriginatorCompensationSetLevelT))),
                Tuple.Create("Originator Compensation Plan Effective Date", "OriginatorCompensationLastModifiedDate",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.OriginatorCompensationLastModifiedDate.HasValue ? convertLos.ToDateTimeString(emp.OriginatorCompensationLastModifiedDate.Value) : "")),
                Tuple.Create("Originator Compensation Only Paid for 1st Lien of a Combo", "OriginatorCompensationIsOnlyPaidForFirstLienOfCombo", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> BoolToYesNo(emp.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo))),
                Tuple.Create("Originator Compensation %", "OriginatorCompensationPercent", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> convertLos.ToRateString(emp.OriginatorCompensationPercent).Replace(",", ""))),
                Tuple.Create("Originator Compensation Basis", "OriginatorCompensationBaseT", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> FormatBaseT(emp.OriginatorCompensationBaseT))),
                Tuple.Create("Originator Compensation Minimum", "OriginatorCompensationMinAmount", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> convertLos.ToMoneyString(emp.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Maximum", "OriginatorCompensationMaxAmount", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> convertLos.ToMoneyString(emp.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Fixed Amount", "OriginatorCompensationFixedAmount",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> convertLos.ToMoneyString(emp.OriginatorCompensationFixedAmount, FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Notes", "OriginatorCompensationNotes", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.OriginatorCompensationNotes.Replace(",", "").Replace(Environment.NewLine, " "))),
                Tuple.Create("Junior Underwriter", "JuniorUnderwriterEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.JuniorUnderwriterEmployeeID)),
                Tuple.Create("Junior Processor", "JuniorProcessorEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.JuniorProcessorEmployeeID)),
                Tuple.Create("Is Secondary (External)", "IsExternalSecondary", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.IsExternalSecondary)),
                Tuple.Create("Is Post-Closer (External)", "IsExternalPostCloser",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.IsExternalPostCloser)),
                Tuple.Create("Mini-Correspondent Manager", "MiniCorrManagerEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrManagerEmployeeId)),
                Tuple.Create("Mini-Correspondent Processor", "MiniCorrProcessorEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrProcessorEmployeeId)),
                Tuple.Create("Mini-Correspondent Junior Processor", "MiniCorrJuniorProcessorEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrJuniorProcessorEmployeeId)),
                Tuple.Create("Mini-Correspondent Lender Account Exec", "MiniCorrLenderAccExecEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrLenderAccExecEmployeeId)),
                Tuple.Create("Mini-Correspondent Post-Closer (External)", "MiniCorrExternalPostCloserEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrExternalPostCloserEmployeeId)),
                Tuple.Create("Mini-Correspondent Underwriter", "MiniCorrUnderwriterEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrUnderwriterEmployeeId)),
                Tuple.Create("Mini-Correspondent Junior Underwriter", "MiniCorrJuniorUnderwriterEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrJuniorUnderwriterEmployeeId)),
                Tuple.Create("Mini-Correspondent Credit Auditor", "MiniCorrCreditAuditorEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrCreditAuditorEmployeeId)),
                Tuple.Create("Mini-Correspondent Legal Auditor", "MiniCorrLegalAuditorEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrLegalAuditorEmployeeId)),
                Tuple.Create("Mini-Correspondent Lock Desk", "MiniCorrLockDeskEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrLockDeskEmployeeId)),
                Tuple.Create("Mini-Correspondent Purchaser", "MiniCorrPurchaserEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrPurchaserEmployeeId)),
                Tuple.Create("Mini-Correspondent Secondary", "MiniCorrSecondaryEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiniCorrSecondaryEmployeeId)),
                Tuple.Create("Correspondent Manager", "CorrManagerEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrManagerEmployeeId)),
                Tuple.Create("Correspondent Processor", "CorrProcessorEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrProcessorEmployeeId)),
                Tuple.Create("Correspondent Junior Processor", "CorrJuniorProcessorEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrJuniorProcessorEmployeeId)),
                Tuple.Create("Correspondent Lender Account Exec", "CorrLenderAccExecEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrLenderAccExecEmployeeId)),
                Tuple.Create("Correspondent Post-Closer (External)", "CorrExternalPostCloserEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrExternalPostCloserEmployeeId)),
                Tuple.Create("Correspondent Underwriter", "CorrUnderwriterEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrUnderwriterEmployeeId)),
                Tuple.Create("Correspondent Junior Underwriter", "CorrJuniorUnderwriterEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrJuniorUnderwriterEmployeeId)),
                Tuple.Create("Correspondent Credit Auditor", "CorrCreditAuditorEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrCreditAuditorEmployeeId)),
                Tuple.Create("Correspondent Legal Auditor", "CorrLegalAuditorEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrLegalAuditorEmployeeId)),
                Tuple.Create("Correspondent Lock Desk", "CorrLockDeskEmployeeId",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrLockDeskEmployeeId)),
                Tuple.Create("Correspondent Purchaser", "CorrPurchaserEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrPurchaserEmployeeId)),
                Tuple.Create("Correspondent Secondary", "CorrSecondaryEmployeeId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.CorrSecondaryEmployeeId)),
                Tuple.Create("Populate Broker Relationships", "PopulateBrokerRelationshipsT",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> PmlBroker.FormatPopulationType(emp.PopulateBrokerRelationshipsT))),
                Tuple.Create("Populate Mini-Correspondent Relationships", "PopulateMiniCorrRelationshipsT", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> PmlBroker.FormatPopulationType(emp.PopulateMiniCorrRelationshipsT))),
                Tuple.Create("Populate Correspondent Relationships", "PopulateCorrRelationshipsT", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> PmlBroker.FormatPopulationType(emp.PopulateCorrRelationshipsT))),
                Tuple.Create("Populate PML Permissions", "PopulatePMLPermissionsT", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> PmlBroker.FormatPopulationType(emp.PopulatePMLPermissionsT))),
                Tuple.Create("Can Apply For Ineligible Loan Programs", "CanApplyForIneligibleLoanPrograms", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> bup.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))),
                Tuple.Create("Middle Name", "MiddleName",  new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiddleName)),
                Tuple.Create("IP Address Restriction", "IPAddressRestriction", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Restricted" : "Unrestricted")),
                Tuple.Create("IPs on Global Whitelist", "IPsOnGlobalWhitelist", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Yes" : string.Empty)),
                Tuple.Create("Devices with a Certificate", "DevicesWithACertificate", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Yes" : string.Empty)),
                Tuple.Create("Whitelisted IPs", "WhitelistedIPs", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    if (string.IsNullOrEmpty(emp.MustLogInFromTheseIpAddresses))
                    {
                        return string.Empty;
                    }

                    return string.Join("+", emp.MustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s)));
                })),
                Tuple.Create("Multi-Factor Authentication", "MultiFactorAuthentication", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnabledMultiFactorAuthentication))),
                Tuple.Create("Allow Installing Digital Certificates", "AllowInstallingDigitalCertificates", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnabledClientDigitalCertificateInstall))),
                Tuple.Create("List of Digital Certificates", "ListOfDigitalCertificates", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    var certificates = certs.GetCertificatesForUser(emp.UserID)
                        .OrderByDescending(c => c.CreatedDate)
                        .Select(c => c.Description + "::" + c.CreatedDate);

                    return string.Join("+", certificates);
                })),
                Tuple.Create("IP Access", "IP Access", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    var ipAddrs = ips.GetRegisteredIpsForUser(emp.UserID)
                        .Select(ip => ip.IpAddress);
                    return string.Join("+", ipAddrs);
                })),
                Tuple.Create("Enable Auth Code via SMS", "EnableAuthCodeviaSMS", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnableAuthCodeViaSms))),
                Tuple.Create("Has Cell Phone Number", "HasCellPhoneNumber", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.HasCellPhoneNumber))),
                Tuple.Create("NMLS ID", "NmlsId", new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LosIdentifier)),
                // If you have to modify EmployeeDB to retrieve more data from the SP you have to update quite a few more areas. See LendOSoln\LendOSolnTest\ContactInfoExportTest.cs.
            };

            return items;
        }

        private static string BoolToYesNo(bool input)
        {
            return input ? "Yes" : "No";
        }

        // OPM 41396
        public static string CreateContactInfoCSVForPmlSupervisor(Guid brokerId, Guid supervisorEmployeeId, string loginName, Guid pmlBrokerId)
        {
            RoleFixup roleFixup = new RoleFixup();
            CsvTable csvTable = new CsvTable();
            var csvData = PmlBroker.GetContactInfoCSVForPmlSupervisorMapper();
            DataTable dt = new DataTable();

            foreach (var column in csvData)
            {
                csvTable.SetLayout(column.Item1, column.Item2);
                dt.Columns.Add(column.Item2);
            }


            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@Type", "P"));
            parameters.Add(new SqlParameter("@SupervisorEmployeeId", supervisorEmployeeId));

            if (!string.IsNullOrEmpty(loginName))
            {
                parameters.Add(new SqlParameter("@LoginName", loginName));
            }
            else if (pmlBrokerId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@PmlBrokerId", pmlBrokerId));
            }

            try
            {
                roleFixup.Retrieve(brokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to load role fixup table.", e);
            }

            string spName = "ListIndividualEmployeeContactInfoByBrokerIdForPmlSupervisor";

            if (string.IsNullOrEmpty(loginName) || pmlBrokerId != Guid.Empty)
            {
                spName = "ListEmployeeContactInfoByBrokerIdForPmlSupervisor";
            }            

            // Retrieve all client certificates and registered IPs manually to 
            // prevent holding the data reader open while they are retrieved.
            var clientCertificates = new ClientCertificates(brokerId);
            clientCertificates.Retrieve();
            var registeredIpAddresses = new UserRegisteredIpAddresses(brokerId);
            registeredIpAddresses.Retrieve();

            Dictionary<Guid, PmlBrokerPermission> cache = new Dictionary<Guid, PmlBrokerPermission>(); 
            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, spName, parameters))
            {
                while (sR.Read())
                {
                    EmployeeDB emp = new EmployeeDB();
                   
                    // BrokerId required for Originator Compensation, relationship, and permission
                    // data, but is not loaded with this stored procedure
                    emp.BrokerID = brokerId;
                    emp.Retrieve(sR);

                    BrokerUserPermissions p = new BrokerUserPermissions();
                    p.Retrieve(brokerId, emp.ID, sR, cache);

                    DataRow row = dt.NewRow();

                    foreach (var column in csvData)
                    {
                        row[column.Item2] = column.Item3(emp, p, clientCertificates, registeredIpAddresses);
                    }

                    dt.Rows.Add(row);
                }

                csvTable.DataFilter = roleFixup;
                csvTable.DataSource = dt.CreateDataReader();
                csvTable.DataBind();
            }

            return csvTable.ToString();
        }

        private static IEnumerable<Tuple<string, string, Func<bool>, Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>>> GetContactInfoMapping(AbstractUserPrincipal principal, bool includeLoanOfficerAssistantRelationship)
        {
            bool isAdmin = principal.HasRole(E_RoleT.Administrator);
            LosConvert convertLos = new LosConvert();

            var mapping = new[] 
            {
                Tuple.Create("Login", "LoginNm", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LoginName)),
                Tuple.Create("First Name", "UserFirstNm", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.FirstName)),
                Tuple.Create("Last Name", "UserLastNm", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LastName)),
                Tuple.Create("Company Name", "PmlBrokerName", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PmlBrokerName)),
                Tuple.Create("Address", "Addr", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Address.StreetAddress)),
                Tuple.Create("City", "City", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Address.City)),
                Tuple.Create("State", "State", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Address.State)),
                Tuple.Create("Zipcode", "Zip", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Address.Zipcode)),
                Tuple.Create("Phone", "Phone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Phone)),
                Tuple.Create("Fax", "Fax", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Fax)),
                Tuple.Create("Cell Phone", "CellPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CellPhone)),
                Tuple.Create("Pager", "Pager", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Pager)),
                Tuple.Create("Email", "Email", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.Email)),
                Tuple.Create("Is Loan Officer", "IsLoanOfficer", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsLoanOfficer)),
                Tuple.Create("Is Processor (External)", "IsBrokerProcessor", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsBrokerProcessor)),
                Tuple.Create("Branch", "BranchId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.BranchID)),
                Tuple.Create("Supervisor", "PmlExternalManagerEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PmlExternalManagerEmployeeId)),
                Tuple.Create("IsSupervisor", "IsPmlManager", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsPmlManager)),
                Tuple.Create("Access Level", "PmlLevelAccessDesc", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PmlLevelAccessDesc)),
                Tuple.Create("Password Expiration Option", "PasswordExpirationOptionDesc", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PasswordExpirationOptionDesc)),
                Tuple.Create("Password Expiration Date", "PasswordExpirationD", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PasswordExpirationD)),
                Tuple.Create("Password Expiration Period", "PasswordExpirationPeriod", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PasswordExpirationPeriod)),
                Tuple.Create("Lender Account Exec", "LenderAccExecEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LenderAcctExecEmployeeID)),
                Tuple.Create("Lender Account Exec Phone", "LenderAccExecPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)), //will be bound in bind cell.
                Tuple.Create("Lender Account Exec Email", "LenderAccExecEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Lender Account Exec Login", "LenderAccExecLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>null)),
                Tuple.Create("Lock Desk", "LockDeskEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LockDeskEmployeeID)),
                Tuple.Create("Lock Desk Phone", "LockDeskPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Lock Desk Email", "LockDeskEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Lock Desk Login", "LockDeskLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Manager", "ManagerEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.ManagerEmployeeID)),
                Tuple.Create("Manager Phone", "ManagerPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Manager Email", "ManagerEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Manager Login", "ManagerLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Underwriter", "UnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.UnderwriterEmployeeID)),
                Tuple.Create("Underwriter Phone", "UnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Underwriter Email", "UnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Underwriter Login", "UnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Processor", "ProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.ProcessorEmployeeID)),
                Tuple.Create("Processor Phone", "ProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Processor Email", "ProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Processor Login", "ProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Price Group", "LpePriceGroupId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LpePriceGroupID)),
                Tuple.Create("IsActive", "IsActive", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsActive)),
                Tuple.Create("Last Login", "RecentLoginD", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.RecentLoginD)),
                Tuple.Create("Lending Licenses", "LicenseXmlContent", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LicenseXmlContent)),
                Tuple.Create("Notes", "NotesByEmployer", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.NotesByEmployer)),
                Tuple.Create("Created", "StartD", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.StartD)),
                Tuple.Create("Company Id", "PmlCompanyId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PmlCompanyId)),
                Tuple.Create("Portal Mode", "PortalMode", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.PortalMode)),
                Tuple.Create("Originator Compensation Plan Level", "OriginatorCompensationSetLevelT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatLevelT(emp.OriginatorCompensationSetLevelT))),
                Tuple.Create("Originator Compensation Plan Effective Date", "OriginatorCompensationLastModifiedDate", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.OriginatorCompensationLastModifiedDate.HasValue ? convertLos.ToDateTimeString(emp.OriginatorCompensationLastModifiedDate.Value) : "")),
                Tuple.Create("Originator Compensation Only Paid for 1st Lien of a Combo", "OriginatorCompensationIsOnlyPaidForFirstLienOfCombo", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo))),
                Tuple.Create("Originator Compensation %", "OriginatorCompensationPercent", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => convertLos.ToRateString(emp.OriginatorCompensationPercent))),
                Tuple.Create("Originator Compensation Basis", "OriginatorCompensationBaseT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatBaseT(emp.OriginatorCompensationBaseT))),
                Tuple.Create("Originator Compensation Minimum", "OriginatorCompensationMinAmount", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => convertLos.ToMoneyString(emp.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Maximum", "OriginatorCompensationMaxAmount", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => convertLos.ToMoneyString(emp.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Fixed Amount", "OriginatorCompensationFixedAmount", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => convertLos.ToMoneyString(emp.OriginatorCompensationFixedAmount,FormatDirection.ToRep).Replace(",", ""))),
                Tuple.Create("Originator Compensation Notes", "OriginatorCompensationNotes", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.OriginatorCompensationNotes.Replace(",", "").Replace(Environment.NewLine, " "))),
                Tuple.Create("Junior Underwriter", "JuniorUnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.JuniorUnderwriterEmployeeID)),
                Tuple.Create("Junior Underwriter Phone", "JuniorUnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Junior Underwriter Email", "JuniorUnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Junior Underwriter Login", "JuniorUnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Junior Processor", "JuniorProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.JuniorProcessorEmployeeID)),
                Tuple.Create("Junior Processor Phone", "JuniorProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Junior Processor Email", "JuniorProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Junior Processor Login", "JuniorProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Loan Officer Assistant", "LoanOfficerAssistantEmployeeId", new Func<bool>(() => includeLoanOfficerAssistantRelationship), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LoanOfficerAssistantEmployeeID)),
                Tuple.Create("Loan Officer Assistant Phone", "LoanOfficerAssistantPhone", new Func<bool>(() => includeLoanOfficerAssistantRelationship), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Loan Officer Assistant Email", "LoanOfficerAssistantEmail", new Func<bool>(() => includeLoanOfficerAssistantRelationship), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Loan Officer Assistant Login", "LoanOfficerAssistantLoginNm", new Func<bool>(() => includeLoanOfficerAssistantRelationship && isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Is Secondary (External)", "IsExternalSecondary", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsExternalSecondary)),
                Tuple.Create("Is Post-Closer (External)", "IsExternalPostCloser", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.IsExternalPostCloser)),
                Tuple.Create("Mini-Correspondent Branch", "MiniCorrespondentBranchId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrespondentBranchID)),
                Tuple.Create("Mini-Correspondent Price Group", "MiniCorrespondentPriceGroupId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrespondentPriceGroupID)),
                Tuple.Create("Mini-Correspondent Manager", "MiniCorrManagerEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrManagerEmployeeId)),
                Tuple.Create("Mini-Correspondent Manager Phone", "MiniCorrManagerPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Manager Email", "MiniCorrManagerEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Manager Login", "MiniCorrManagerLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Processor", "MiniCorrProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrProcessorEmployeeId)),
                Tuple.Create("Mini-Correspondent Processor Phone", "MiniCorrProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Processor Email", "MiniCorrProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Processor Login", "MiniCorrProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Processor", "MiniCorrJuniorProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrJuniorProcessorEmployeeId)),
                Tuple.Create("Mini-Correspondent Junior Processor Phone", "MiniCorrJuniorProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Processor Email", "MiniCorrJuniorProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Processor Login", "MiniCorrJuniorProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lender Account Exec", "MiniCorrLenderAccExecEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrLenderAccExecEmployeeId)),
                Tuple.Create("Mini-Correspondent Lender Account Exec Phone", "MiniCorrLenderAccExecPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lender Account Exec Email", "MiniCorrLenderAccExecEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lender Account Exec Login", "MiniCorrLenderAccExecLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Post-Closer (External)", "MiniCorrExternalPostCloserEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrExternalPostCloserEmployeeId)),
                Tuple.Create("Mini-Correspondent Post-Closer (External) Phone", "MiniCorrExternalPostCloserPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Post-Closer (External) Email", "MiniCorrExternalPostCloserEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Post-Closer (External) Login", "MiniCorrExternalPostCloserLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Underwriter", "MiniCorrUnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrUnderwriterEmployeeId)),
                Tuple.Create("Mini-Correspondent Underwriter Phone", "MiniCorrUnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Underwriter Email", "MiniCorrUnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Underwriter Login", "MiniCorrUnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Underwriter", "MiniCorrJuniorUnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrJuniorUnderwriterEmployeeId)),
                Tuple.Create("Mini-Correspondent Junior Underwriter Phone", "MiniCorrJuniorUnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Underwriter Email", "MiniCorrJuniorUnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Junior Underwriter Login", "MiniCorrJuniorUnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Credit Auditor", "MiniCorrCreditAuditorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrCreditAuditorEmployeeId)),
                Tuple.Create("Mini-Correspondent Credit Auditor Phone", "MiniCorrCreditAuditorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Credit Auditor Email", "MiniCorrCreditAuditorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Credit Auditor Login", "MiniCorrCreditAuditorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Legal Auditor", "MiniCorrLegalAuditorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrLegalAuditorEmployeeId)),
                Tuple.Create("Mini-Correspondent Legal Auditor Phone", "MiniCorrLegalAuditorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Legal Auditor Email", "MiniCorrLegalAuditorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Legal Auditor Login", "MiniCorrLegalAuditorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lock Desk", "MiniCorrLockDeskEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrLockDeskEmployeeId)),
                Tuple.Create("Mini-Correspondent Lock Desk Phone", "MiniCorrLockDeskPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lock Desk Email", "MiniCorrLockDeskEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Lock Desk Login", "MiniCorrLockDeskLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Purchaser", "MiniCorrPurchaserEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrPurchaserEmployeeId)),
                Tuple.Create("Mini-Correspondent Purchaser Phone", "MiniCorrPurchaserPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Purchaser Email", "MiniCorrPurchaserEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Purchaser Login", "MiniCorrPurchaserLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Secondary", "MiniCorrSecondaryEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.MiniCorrSecondaryEmployeeId)),
                Tuple.Create("Mini-Correspondent Secondary Phone", "MiniCorrSecondaryPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Secondary Email", "MiniCorrSecondaryEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Mini-Correspondent Secondary Login", "MiniCorrSecondaryLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Branch", "CorrespondentBranchId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrespondentBranchID)),
                Tuple.Create("Correspondent Price Group", "CorrespondentPriceGroupId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrespondentPriceGroupID)),
                Tuple.Create("Correspondent Manager", "CorrManagerEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrManagerEmployeeId)),
                Tuple.Create("Correspondent Manager Phone", "CorrManagerPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Manager Email", "CorrManagerEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Manager Login", "CorrManagerLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Processor", "CorrProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrProcessorEmployeeId)),
                Tuple.Create("Correspondent Processor Phone", "CorrProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Processor Email", "CorrProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Processor Login", "CorrProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Processor", "CorrJuniorProcessorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrJuniorProcessorEmployeeId)),
                Tuple.Create("Correspondent Junior Processor Phone", "CorrJuniorProcessorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Processor Email", "CorrJuniorProcessorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Processor Login", "CorrJuniorProcessorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lender Account Exec", "CorrLenderAccExecEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrLenderAccExecEmployeeId)),
                Tuple.Create("Correspondent Lender Account Exec Phone", "CorrLenderAccExecPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lender Account Exec Email", "CorrLenderAccExecEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lender Account Exec Login", "CorrLenderAccExecLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Post-Closer (External)", "CorrExternalPostCloserEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrExternalPostCloserEmployeeId)),
                Tuple.Create("Correspondent Post-Closer (External) Phone", "CorrExternalPostCloserPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Post-Closer (External) Email", "CorrExternalPostCloserEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Post-Closer (External) Login", "CorrExternalPostCloserLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Underwriter", "CorrUnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrUnderwriterEmployeeId)),
                Tuple.Create("Correspondent Underwriter Phone", "CorrUnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Underwriter Email", "CorrUnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Underwriter Login", "CorrUnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Underwriter", "CorrJuniorUnderwriterEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrJuniorUnderwriterEmployeeId)),
                Tuple.Create("Correspondent Junior Underwriter Phone", "CorrJuniorUnderwriterPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Underwriter Email", "CorrJuniorUnderwriterEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Junior Underwriter Login", "CorrJuniorUnderwriterLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Credit Auditor", "CorrCreditAuditorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrCreditAuditorEmployeeId)),
                Tuple.Create("Correspondent Credit Auditor Phone", "CorrCreditAuditorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Credit Auditor Email", "CorrCreditAuditorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Credit Auditor Login", "CorrCreditAuditorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Legal Auditor", "CorrLegalAuditorEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrLegalAuditorEmployeeId)),
                Tuple.Create("Correspondent Legal Auditor Phone", "CorrLegalAuditorPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Legal Auditor Email", "CorrLegalAuditorEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Legal Auditor Login", "CorrLegalAuditorLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lock Desk", "CorrLockDeskEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrLockDeskEmployeeId)),
                Tuple.Create("Correspondent Lock Desk Phone", "CorrLockDeskPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lock Desk Email", "CorrLockDeskEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Lock Desk Login", "CorrLockDeskLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Purchaser", "CorrPurchaserEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrPurchaserEmployeeId)),
                Tuple.Create("Correspondent Purchaser Phone", "CorrPurchaserPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Purchaser Email", "CorrPurchaserEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Purchaser Login", "CorrPurchaserLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Secondary", "CorrSecondaryEmployeeId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.CorrSecondaryEmployeeId)),
                Tuple.Create("Correspondent Secondary Phone", "CorrSecondaryPhone", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Secondary Email", "CorrSecondaryEmail", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Correspondent Secondary Login", "CorrSecondaryLoginNm", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => null)),
                Tuple.Create("Opts to use new loan editor UI", "OptsToUseNewLoanEditorUI", new Func<bool>(() => E_PermissionType.HardDisable != principal.BrokerDB.ActualNewLoanEditorUIPermissionType), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.OptsToUseNewLoanEditorUI)),
                Tuple.Create("Populate Broker Relationships", "PopulateBrokerRelationshipsT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatPopulationType(emp.PopulateBrokerRelationshipsT))),
                Tuple.Create("Populate Mini-Correspondent Relationships", "PopulateMiniCorrRelationshipsT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatPopulationType(emp.PopulateMiniCorrRelationshipsT))),
                Tuple.Create("Populate Correspondent Relationships", "PopulateCorrRelationshipsT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatPopulationType(emp.PopulateCorrRelationshipsT))),
                Tuple.Create("Populate PML Permissions", "PopulatePMLPermissionsT", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => PmlBroker.FormatPopulationType(emp.PopulatePMLPermissionsT))),
                Tuple.Create("Can Apply For Ineligible Loan Programs", "CanApplyForIneligibleLoanPrograms", new Func<bool>(()=>true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp, bup, certs, ips)=> bup.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))),
                Tuple.Create("Middle Name", "MiddleName", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips)=> emp.MiddleName)),
                Tuple.Create("IP Address Restriction", "IPAddressRestriction", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Restricted" : "Unrestricted")),
                Tuple.Create("IPs on Global Whitelist", "IPsOnGlobalWhitelist", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Yes" : string.Empty)),
                Tuple.Create("Devices with a Certificate", "DevicesWithACertificate", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.EnabledIpRestriction ? "Yes" : string.Empty)),
                Tuple.Create("Whitelisted IPs", "WhitelistedIPs", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    if (string.IsNullOrEmpty(emp.MustLogInFromTheseIpAddresses))
                    {
                        return string.Empty;
                    }

                    return string.Join("+", emp.MustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s)));
                })),
                Tuple.Create("Multi-Factor Authentication", "MultiFactorAuthentication", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnabledMultiFactorAuthentication))),
                Tuple.Create("Allow Installing Digital Certificates", "AllowInstallingDigitalCertificates", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnabledClientDigitalCertificateInstall))),
                Tuple.Create("List of Digital Certificates", "ListOfDigitalCertificates", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    var certificates = certs.GetCertificatesForUser(emp.UserID)
                        .OrderByDescending(c => c.CreatedDate)
                        .Select(c => c.Description + "::" + c.CreatedDate);

                    return string.Join("+", certificates);
                })),
                Tuple.Create("IP Access", "IP Access", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) =>
                {
                    var ipAddrs = ips.GetRegisteredIpsForUser(emp.UserID)
                        .Select(ip => ip.IpAddress);
                    return string.Join("+", ipAddrs);
                })),
                Tuple.Create("Enable Auth Code via SMS", "EnableAuthCodeviaSMS", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.EnableAuthCodeViaSms))),
                Tuple.Create("Has Cell Phone Number", "HasCellPhoneNumber", new Func<bool>(() => isAdmin), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => BoolToYesNo(emp.HasCellPhoneNumber))),
                Tuple.Create("NMLS ID", "NmlsId", new Func<bool>(() => true), new Func<EmployeeDB, BrokerUserPermissions, ClientCertificates, UserRegisteredIpAddresses, object>((emp,bup,certs,ips) => emp.LosIdentifier)),
                // If you have to modify EmployeeDB to retrieve more data from the SP you have to update quite a few more areas. See LendOSoln\LendOSolnTest\ContactInfoExportTest.cs.
            };

            return mapping;
        }

        public static string CreateContactInfoCSV(AbstractUserPrincipal principal, string type)
        {
            return CreateContactInfoCSV(principal, type, "", Guid.Empty);
        }

        public static string CreateContactInfoCSV(AbstractUserPrincipal principal, string type, string loginName, Guid pmlBrokerId)
        {
            // 7/15/2005 kb - Initialize our csv table using the contact
            // info for all this broker's employees (see case 2386).

            RoleFixup roleFixup = new RoleFixup();
            CsvTable csvTable = new CsvTable();

            bool includeLoanOfficerAssistantRelationship = string.IsNullOrEmpty(type) || type.ToUpper() != "P";
            var mapping = PmlBroker.GetContactInfoMapping(principal, includeLoanOfficerAssistantRelationship);
            DataTable dt = new DataTable();

            foreach (var column in mapping)
            {
                if (column.Item3())
                {
                    csvTable.SetLayout(column.Item1, column.Item2);
                    dt.Columns.Add(column.Item2);
                }
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));

            if (!string.IsNullOrEmpty(type))
            {
                parameters.Add(new SqlParameter("@Type", type));
            }

            if (!string.IsNullOrEmpty(loginName))
            {
                parameters.Add(new SqlParameter("@LoginName", loginName));
            }
            else if (pmlBrokerId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@PmlBrokerId", pmlBrokerId));
            }

            try
            {
                roleFixup.Retrieve(principal.BrokerId);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Unable to load role fixup table.", e);
            }

            string spName =  "ListIndividualEmployeeContactInfoByBrokerId";

            if (string.IsNullOrEmpty(loginName) || pmlBrokerId != Guid.Empty)
            {
                spName = "ListEmployeeContactInfoByBrokerId";
            }

            StringBuilder debugString = new StringBuilder();
            debugString.AppendLine($"Execute {spName} @BrokerId={principal.BrokerId}, @Type= {type} @LoginName= {loginName} @PmlBrokerId= {pmlBrokerId}");
            bool hasError = false;
            Stopwatch sw = Stopwatch.StartNew();
            Dictionary<Guid, PmlBrokerPermission> cache = new Dictionary<Guid, PmlBrokerPermission>();

            var clientCertificates = new ClientCertificates(principal.BrokerId);
            var registeredIpAddresses = new UserRegisteredIpAddresses(principal.BrokerId);

            try
            {
                using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(principal.BrokerId, spName, parameters))
                {
                    while (sR.Read())
                    {
                        EmployeeDB emp = new EmployeeDB();
                        emp.BrokerID = principal.BrokerId;
                        emp.Retrieve(sR);

                        BrokerUserPermissions bup = new BrokerUserPermissions();
                        bup.Retrieve(principal.BrokerId, emp.ID, sR, cache);
                        // BrokerId required for Originator Compensation, relationship, and permission
                        // data, but is not loaded with this stored procedure
                        emp.BrokerID = principal.BrokerId;

                        DataRow row = dt.NewRow();

                        foreach (var column in mapping)
                        {
                            if (column.Item3())
                            {
                                row[column.Item2] = column.Item4(emp, bup, clientCertificates, registeredIpAddresses);
                            }
                        }

                        dt.Rows.Add(row);
                    }

                    csvTable.DataFilter = roleFixup;
                    csvTable.DataSource = dt.CreateDataReader();
                    csvTable.DataBind();
                }
            }
            catch (Exception exc)
            {
                hasError = true;
                debugString.AppendLine("--- ERROR ----");
                debugString.AppendLine(exc.ToString());

                throw;
            }
            finally
            {
                debugString.AppendLine("Execute in " + sw.ElapsedMilliseconds + "ms.");
                if (hasError)
                {
                    Tools.LogError(debugString.ToString());
                }
                else
                {
                    Tools.LogInfo(debugString.ToString());
                }
            }
            return csvTable.ToString();
        }

        public static PmlBroker CreatePmlBroker(Guid brokerId)
        {
            PmlBroker obj = new PmlBroker();
            obj.PmlBrokerId = Guid.NewGuid();
            obj.BrokerId = brokerId;
            obj.LicenseInfoList = new LicenseInfoList();
            obj.m_isNew = true;
            obj.IsActive = true;
            obj.Roles = new HashSet<E_OCRoles>();

            obj.BrokerRoleStatusT = OCStatusType.Blank;
            obj.MiniCorrRoleStatusT = OCStatusType.Blank;
            obj.CorrRoleStatusT = OCStatusType.Blank;

            obj.BrokerRoleRelationships = new PmlBrokerRelationship(brokerId, obj.PmlBrokerId, E_OCRoles.Broker);
            obj.MiniCorrRoleRelationships = new PmlBrokerRelationship(brokerId, obj.PmlBrokerId, E_OCRoles.MiniCorrespondent);
            obj.CorrRoleRelationships = new PmlBrokerRelationship(brokerId, obj.PmlBrokerId, E_OCRoles.Correspondent);

            obj.Permissions = new PmlBrokerPermission(brokerId, obj.PmlBrokerId);

            var canRunPricingEngineWithoutCreditReport = BrokerDB.RetrieveById(brokerId).CanRunPricingEngineWithoutCreditReportByDefault;

            obj.Permissions.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, canRunPricingEngineWithoutCreditReport);
            obj.Permissions.SetPermission(Permission.CanSubmitWithoutCreditReport, canRunPricingEngineWithoutCreditReport);

            return obj;
        } 

        public static List<string> RetrieveLoginNamesByPmlBroker(Guid pmlBroker, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PmlBrokerId", pmlBroker)
                                        };

            List<string> loginNms = new List<string>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLoginNamesByPmlBroker", parameters))
            {
                while (reader.Read())
                {
                    if (reader["LoginNm"] != DBNull.Value)
                    {
                        loginNms.Add(reader["LoginNm"].ToString());
                    }
                }

                return loginNms;
            }
        }

        public static PmlBroker RetrievePmlBrokerByName(string pmlBrokerName, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PmlBrokerNm", pmlBrokerName)
                                            , new SqlParameter("@BrokerId", brokerId)
                                        };


            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokerByName", parameters))
            {
                if (reader.Read())
                {
                    return LoadPmlBroker(reader);
                }
                else
                {
                    throw new PmlBrokerNotFoundException(pmlBrokerName, brokerId);
                }
            }
        }

        public static Guid GetPmlBrokerIdByName(string pmlBrokerName, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PmlBrokerNm", pmlBrokerName)
                                            , new SqlParameter("@BrokerId", brokerId)
                                        };


            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetPmlBrokerIdByName", parameters))
            {
                if (reader.Read() && reader["PmlBrokerId"] != DBNull.Value)
                {
                    return (Guid)reader["PmlBrokerId"];                    
                }
                else
                {
                    throw new PmlBrokerNotFoundException(pmlBrokerName, brokerId);
                }
            }
        }

        public static PmlBroker RetrievePmlBrokerByCompanyId(string companyId, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@CompanyId", companyId)
                                            , new SqlParameter("@BrokerId", brokerId)
                                        };


            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokerByCompanyId", parameters))
            {
                if (reader.Read())
                {
                    return LoadPmlBroker(reader);
                }
                else
                {
                    throw new PmlBrokerNotFoundException(companyId, brokerId);
                }
            }
        }

        /// <summary>
        /// Will throw PmlBrokerNotFoundException exception if no pmlbrokerid could not be found.
        /// </summary>
        /// <param name="pmlBrokerId"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static PmlBroker RetrievePmlBrokerById(Guid pmlBrokerId, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PmlBrokerId", pmlBrokerId)
                                            , new SqlParameter("@BrokerId", brokerId)
                                        };
            PmlBroker broker;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokerById", parameters))
            {
                if (reader.Read())
                {
                    broker = LoadPmlBroker(reader);
                }
                else
                {
                    throw new PmlBrokerNotFoundException(pmlBrokerId, brokerId);
                }
            }

            foreach (var group in GroupDB.ListInclusiveGroupForPmlBroker(brokerId, pmlBrokerId))
            {
                broker.GroupIdList += (broker.GroupNameList == string.Empty ? "" : ",") + group.GroupId.ToString();
                broker.GroupNameList += (broker.GroupNameList == string.Empty ? "" : ",") + group.GroupName.ToString();
            }

            return broker;
        }

        /// <summary>
        /// Retrieves the name of the originating company with the specified
        /// <paramref name="pmlBrokerId"/>.
        /// </summary>
        /// <param name="pmlBrokerId">
        /// The id of the originating company.
        /// </param>
        /// <param name="brokerId">
        /// The id of the broker for the originating company.
        /// </param>
        /// <returns>
        /// The name of the originating company.
        /// </returns>
        /// <exception cref="PmlBrokerNotFoundException">
        /// The originating company could not be found.
        /// </exception>
        public static string RetrievePmlBrokerNameById(Guid pmlBrokerId, Guid brokerId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@PmlBrokerId", pmlBrokerId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokerNameById", parameters))
            {
                if (reader.Read())
                {
                    return (string)reader["Name"];
                }

                throw new PmlBrokerNotFoundException(pmlBrokerId, brokerId);
            }
        }

        /// <summary>
        /// Retrieves the TPO color theme ID of the originating company with the specified
        /// <paramref name="pmlBrokerId"/>.
        /// </summary>
        /// <param name="pmlBrokerId">
        /// The id of the originating company.
        /// </param>
        /// <param name="brokerId">
        /// The id of the broker for the originating company.
        /// </param>
        /// <returns>
        /// The TPO color theme ID of the originating company or null if no specific
        /// color theme is assigned.
        /// </returns>
        /// <exception cref="PmlBrokerNotFoundException">
        /// The originating company could not be found.
        /// </exception>
        public static Guid? RetrieveTpoColorThemeId(Guid pmlBrokerId, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@PmlBrokerId", pmlBrokerId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokerTpoColorThemeId", parameters))
            {
                if (reader.Read())
                {
                    var themeId = (Guid)reader["TpoColorThemeId"];
                    return themeId == Guid.Empty ? default(Guid?) : themeId;
                }

                throw new PmlBrokerNotFoundException(pmlBrokerId, brokerId);
            }
        }

        public static string[] PmlBrokerSearchCriteria = { "Name", "CompanyId", "Addr", "PrincipalName1", "PrincipalName2", "NmLsIdentifier" };
        public static List<PmlBroker> RetrievePmlBrokersBySearchCriteria(Guid brokerId, Dictionary<string, string> filterDictionary, bool allowPartialMatching)
        {
            List<PmlBroker> pmlBrokers = new List<PmlBroker>();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            SqlParameter allowPartialMatchingParam = new SqlParameter("@AllowPartialMatching", SqlDbType.Bit);
            allowPartialMatchingParam.Value = allowPartialMatching ? 1 : 0;
            parameters.Add(allowPartialMatchingParam);
            foreach(string pmlBrokerSearchCriterion in PmlBrokerSearchCriteria)
            {
                string paramValue = null;
                if (filterDictionary.TryGetValue(pmlBrokerSearchCriterion, out paramValue))
                {
                    parameters.Add(new SqlParameter("@"+pmlBrokerSearchCriterion, paramValue));
                }
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrievePmlBrokersBySearchCriteria", parameters))
            {
                bool? skipGroupDbInfo = null;
                while(reader.Read())
                {
                    PmlBroker pmlBroker = LoadPmlBroker(reader);

                    string taxIdFilter;
                    if (filterDictionary.TryGetValue("TaxId", out taxIdFilter) && 
                        !TaxIdSearchCriteriaMatches(pmlBroker.TaxId, taxIdFilter, allowPartialMatching))
                    {
                        continue;
                    }

                    pmlBrokers.Add(pmlBroker);
                    if (skipGroupDbInfo != true)
                    {
                        var groupResult = GroupDB.GetGroupMembershipsForPmlBroker(brokerId, pmlBroker.PmlBrokerId);
                        if (skipGroupDbInfo == null && groupResult.Count() == 0)
                        {
                            skipGroupDbInfo = true; //don't call this stored procedure if broker has no originating company groups.
                            continue;
                        }
                        else
                        {
                            skipGroupDbInfo = false;
                            foreach (var group in groupResult)
                            {
                                if (group.IsInGroup == 1)
                                {
                                    pmlBroker.GroupIdList += (pmlBroker.GroupNameList == string.Empty ? "" : ",") + group.GroupId.ToString();
                                    pmlBroker.GroupNameList += (pmlBroker.GroupNameList == string.Empty ? "" : ",") + group.GroupName.ToString();
                                }
                            }
                        }
                    }
                }
            }

            return pmlBrokers;
        }

        /// <summary>
        /// Determines whether an OC's tax ID matches a tax ID filter.
        /// </summary>
        /// <param name="pmlBrokerTaxId">
        /// The OC's tax ID.
        /// </param>
        /// <param name="taxIdFilter">
        /// The tax ID filter.
        /// </param>
        /// <param name="allowPartialMatching">
        /// True to allow partial matching, false otherwise.
        /// </param>
        /// <returns>
        /// True if the tax ID matches, false otherwise.
        /// </returns>
        private static bool TaxIdSearchCriteriaMatches(string pmlBrokerTaxId, string taxIdFilter, bool allowPartialMatching)
        {
            if (taxIdFilter == null)
            {
                return true; 
            }

            if (allowPartialMatching)
            {
                return pmlBrokerTaxId.Contains(taxIdFilter, StringComparison.OrdinalIgnoreCase);
            }

            return string.Equals(pmlBrokerTaxId, taxIdFilter, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns a dictionary with Name+CompanyId as hash. PmlBrokers in same bucket have same Name+CompanyId
        /// </summary>
        public static Dictionary<string, List<PmlBroker>> GetUniqueBrokersByCompanyIdAndName(Guid brokerId)
        {
            Dictionary<string, List<PmlBroker>> pBrokers = new Dictionary<string, List<PmlBroker>>();
            
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListAllPmlBrokerByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    string cName = (string)reader["Name"];
                    string cId = (string)reader["CompanyId"];
                    string nameId = cName+cId;

                    if (pBrokers.ContainsKey(nameId))
                    {
                        List<PmlBroker> brokerList = pBrokers[nameId];
                        brokerList.Add(LoadPmlBroker(reader));
                    }
                    else
                    {
                        List<PmlBroker> brokerList = new List<PmlBroker>();
                        brokerList.Add(LoadPmlBroker(reader));
                        pBrokers.Add(nameId, brokerList);
                    }
                }
            }

            return pBrokers;
        }

        public static List<PmlBroker> ListAllPmlBrokerByBrokerId(Guid brokerId)
        {
            List<PmlBroker> list = new List<PmlBroker>();

            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListAllPmlBrokerByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(LoadPmlBroker(reader));
                }
            }
            return list;
        }

        public static List<PmlBroker> ListAllActivePmlBrokerByBrokerId(Guid brokerId)
        {
            List<PmlBroker> list = new List<PmlBroker>();

            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListAllActivePmlBrokerByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(LoadPmlBroker(reader));
                }
            }
            return list;
        }

        public static int CountPmlBrokerByBrokerId(
            Guid brokerId,
            string companyNameFilter,
            string companyIDFilter,
            string companyRoleFilter,
            int? underwritingAuthorityFilter,
            int companyIndexFilter,
            int brokerStatusFilter,
            int miniCorrStatusFilter,
            int corrStatusFilter)
        {
            var parameters = PmlBroker.GenerateBasicSearchParameters(
                brokerId,
                companyNameFilter,
                companyIDFilter,
                companyRoleFilter,
                companyIndexFilter,
                brokerStatusFilter,
                miniCorrStatusFilter,
                corrStatusFilter);

            parameters.Add(new SqlParameter("@UnderwritingAuthorityFilter", underwritingAuthorityFilter));

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "PmlCompanyList_GetCount", parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["TotalBrokers"];
                }

                return 0;
            }
        }

        public static List<PmlBroker> ListAllPmlBrokerPageByBrokerId(
            Guid brokerId, 
            string sortBy, 
            bool bAsc, 
            int loIndex, 
            int hiIndex,
            string companyNameFilter,
            string companyIDFilter,
            string companyRoleFilter,
            int? underwritingAuthorityFilter,
            int companyIndexFilter,
            int brokerStatusFilter,
            int miniCorrStatusFilter,
            int corrStatusFilter)
        {
            if (sortBy != "Name" && sortBy != "CompanyId" && sortBy != "LpePriceGroupName" && sortBy != "BranchNm")
            {
                sortBy = "Name";
            }

            if (!bAsc)
            {
                sortBy += " DESC";
            }

            if (loIndex < 1)
            {
                loIndex = 1;
            }

            if (hiIndex < loIndex)
            {
                hiIndex = loIndex;
            }

            var parameters = PmlBroker.GenerateBasicSearchParameters(
                brokerId,
                companyNameFilter,
                companyIDFilter,
                companyRoleFilter,
                companyIndexFilter,
                brokerStatusFilter,
                miniCorrStatusFilter,
                corrStatusFilter);

            parameters.Add(new SqlParameter("@LoIndex", loIndex));
            parameters.Add(new SqlParameter("@HiIndex", hiIndex));

            // For dynamic SQL queries, setting the value of a SQL parameter to null means that
            // parameter will not be included in the query, regardless of adding it to the parameter
            // array. Because of this, we'll need to use string comparison instead of integer comparison
            // in case underwritingAuthorityFilter is null, such as when we search for broker or 
            // mini-correspondent roles.
            parameters.Add(new SqlParameter("@UnderwritingAuthorityFilter", "%" + underwritingAuthorityFilter.ToString() + "%"));

            // SQL Server's ORDER BY clause will not correctly parse a column name passed in a SQL 
            // parameter. Because of this, we'll need to use dynamically generated SQL to select 
            // the correct ORDER BY column, but we can use SQL parameters instead of raw string 
            // concatenation for the broker ID, indicies and filtering strings.
            const string sql = @"
    SELECT * FROM
	(
		SELECT 
            row_number() OVER(ORDER BY {0}) AS row_number, 
            p.*, 
            b.BranchNm, 
            l.LpePriceGroupName,
            perm.CanApplyForIneligibleLoanPrograms, 
            perm.CanRunPricingEngineWithoutCreditReport, 
            perm.CanSubmitWithoutCreditReport, 
            perm.CanAccessTotalScorecard, 
            perm.AllowOrder4506T, 
            perm.TaskRelatedEmailOptionT,
		    dbo.GetPmlBrokerRelationshipsAsXml(p.BrokerId, p.PmlBrokerId) as 'RelationshipXml'
		FROM Pml_Broker p
            LEFT OUTER JOIN PML_BROKER_PERMISSIONS perm ON p.PmlBrokerId = perm.PmlBrokerId
			LEFT OUTER JOIN BRANCH b ON p.BranchId = b.BranchId
			LEFT OUTER JOIN LPE_PRICE_GROUP l ON p.LpePriceGroupId = l.LpePriceGroupId
		WHERE 
			p.BrokerId = @BrokerId AND 
			p.Name LIKE @CompanyNameFilter AND
			p.CompanyId LIKE @CompanyIDFilter AND
            p.UnderwritingAuthority LIKE @UnderwritingAuthorityFilter AND
		    (
			    @CompanyRoleFilter = 'ANY' OR
			    (@CompanyRoleFilter = 'NA' AND (p.Roles IS NULL OR p.Roles = '')) OR
			    p.Roles LIKE @CompanyRoleFilter
		    ) AND
            (@CompanyIndexFilter = -1 OR p.IndexNumber = @CompanyIndexFilter) AND
		    (@BrokerStatusFilter = -1 OR p.BrokerRoleStatusT = @BrokerStatusFilter) AND
		    (@MiniCorrStatusFilter = -1 OR p.MiniCorrRoleStatusT = @MiniCorrStatusFilter) AND
		    (@CorrStatusFilter = -1 OR p.CorrRoleStatusT = @CorrStatusFilter)
	) T
	WHERE row_number BETWEEN @LoIndex AND @HiIndex";

            string sFull = string.Format(sql, sortBy);

            var list = new List<PmlBroker>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    list.Add(LoadPmlBroker(reader));
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sFull, null, parameters, readHandler);

            foreach (var pmlBroker in list)
            {
                foreach (var group in GroupDB.GetGroupMembershipsForPmlBroker(pmlBroker.BrokerId, pmlBroker.PmlBrokerId))
                {
                    if (group.IsInGroup == 1)
                    {
                        pmlBroker.GroupIdList += (pmlBroker.GroupNameList == string.Empty ? string.Empty : ",") + group.GroupId.ToString();
                        pmlBroker.GroupNameList += (pmlBroker.GroupNameList == string.Empty ? string.Empty : ",") + group.GroupName;
                    }
                }
            }

            return list;
        }

        private static List<SqlParameter> GenerateBasicSearchParameters(
            Guid brokerId,
            string companyNameFilter,
            string companyIDFilter,
            string companyRoleFilter,
            int companyIndexFilter,
            int brokerStatusFilter,
            int miniCorrStatusFilter,
            int corrStatusFilter)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@CompanyNameFilter", "%" + companyNameFilter + "%"));
            parameters.Add(new SqlParameter("@CompanyIDFilter", "%" + companyIDFilter + "%"));

            if (companyRoleFilter == "NA" || companyRoleFilter == "ANY")
            {
                parameters.Add(new SqlParameter("@CompanyRoleFilter", companyRoleFilter));
            }
            else
            {
                parameters.Add(new SqlParameter("@CompanyRoleFilter", "%" + companyRoleFilter + "%"));
            }

            parameters.Add(new SqlParameter("@CompanyIndexFilter", companyIndexFilter));
            parameters.Add(new SqlParameter("@BrokerStatusFilter", brokerStatusFilter));         
            parameters.Add(new SqlParameter("@MiniCorrStatusFilter", miniCorrStatusFilter));
            parameters.Add(new SqlParameter("@CorrStatusFilter", corrStatusFilter));

            return parameters;
        }

        private static PmlBroker LoadPmlBroker(IDataReader reader)
        {
            PmlBroker pmlBroker = new PmlBroker();
            pmlBroker.m_isNew = false;
            pmlBroker.PmlBrokerId = (Guid)reader["PmlBrokerId"];
            pmlBroker.BrokerId = (Guid)reader["BrokerId"];
            pmlBroker.IsActive = (bool)reader["IsActive"];
            pmlBroker.Name = (string)reader["Name"];

            pmlBroker.NmlsName = (string)reader["NmlsName"];
            pmlBroker.NmlsNameLckd = (bool)reader["NmlsNameLckd"];

            pmlBroker.Addr = (string)reader["Addr"];
            pmlBroker.City = (string)reader["City"];
            pmlBroker.State = (string)reader["State"];
            pmlBroker.Zip = (string)reader["Zip"];
            pmlBroker.Phone = (string)reader["Phone"];
            pmlBroker.Fax = (string)reader["Fax"];
            pmlBroker.NmLsIdentifier = (string)reader["NmLsIdentifier"];
            string licenseXmlContent = (string)reader["LicenseXmlContent"];
            pmlBroker.LicenseInfoList = LicenseInfoList.ToObject(licenseXmlContent);
            pmlBroker.GroupNameList = string.Empty;
            pmlBroker.GroupIdList = string.Empty;

            pmlBroker.CompanyId = (string)reader["CompanyId"];
            pmlBroker.PmlCompanyTierId = Convert.ToInt32(reader["PmlCompanyTierId"]);

            pmlBroker.SetTaxIdFields(reader);
            pmlBroker.taxIdValidOnLoad = pmlBroker.TaxIdValid;

            pmlBroker.PrincipalName1 = (string)reader["PrincipalName1"];
            pmlBroker.PrincipalName2 = (string)reader["PrincipalName2"];

            var themeId = (Guid)reader["TpoColorThemeId"];
            pmlBroker.tpoColorThemeId = themeId == Guid.Empty ? default(Guid?) : themeId;

            pmlBroker.CustomPricingPolicyField1 = (string)reader["CustomPricingPolicyField1"];
            pmlBroker.CustomPricingPolicyField2 = (string)reader["CustomPricingPolicyField2"];
            pmlBroker.CustomPricingPolicyField3 = (string)reader["CustomPricingPolicyField3"];
            pmlBroker.CustomPricingPolicyField4 = (string)reader["CustomPricingPolicyField4"];
            pmlBroker.CustomPricingPolicyField5 = (string)reader["CustomPricingPolicyField5"];

            pmlBroker.PMLNavigationPermissions = (string)reader["PMLNavigationPermissions"];

            pmlBroker.LoadOriginatorCompensationValues(
                Convert.ToDecimal(reader["OriginatorCompensationPercent"]),
            (E_PercentBaseT)Convert.ToInt32(reader["OriginatorCompensationBaseT"]),
            Convert.ToDecimal(reader["OriginatorCompensationMinAmount"]),
            Convert.ToDecimal(reader["OriginatorCompensationMaxAmount"]),
            Convert.ToDecimal(reader["OriginatorCompensationFixedAmount"]),
            Convert.ToString(reader["OriginatorCompensationNotes"]),
            Convert.ToBoolean(reader["OriginatorCompensationIsOnlyPaidForFirstLienOfCombo"]));

            if (!Convert.IsDBNull(reader["OriginatorCompensationLastModifiedDate"]))
            {
                pmlBroker.OriginatorCompensationLastModifiedDate = SafeConvert.ToDateTime(reader["OriginatorCompensationLastModifiedDate"]);
            }
            pmlBroker.OriginatorCompensationAuditData = new OriginatorCompensationAudit(Convert.ToString(reader["OriginatorCompensationAuditXml"]));
            if (!Convert.IsDBNull(reader["Lender_TPO_LandingPageConfigId"]))
            {
                pmlBroker.TPOPortalConfigID = (Guid)reader["Lender_TPO_LandingPageConfigId"];
            }

            if ((bool)reader["UseBranchLpePriceGroupId"])
            {
                pmlBroker.LpePriceGroupId = Guid.Empty;
            }
            else if (Convert.ToBoolean(reader["UseDefaultWholesalePriceGroupId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultWholesalePriceGroupId = true;
            }
            else if (!Convert.IsDBNull(reader["LpePriceGroupId"]))
            {
                pmlBroker.LpePriceGroupId = (Guid)reader["LpePriceGroupId"];
            }

            if (Convert.ToBoolean(reader["UseDefaultWholesaleBranchId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultWholesaleBranchId = true;
            }
            else if (!Convert.IsDBNull(reader["BranchId"]))
            {
                pmlBroker.BranchId = (Guid)reader["BranchId"];
            }

            if (Convert.ToBoolean(reader["UseDefaultWholesalePriceGroupId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.m_LpePriceGroupName = pmlBroker.Broker.DefaultWholesalePriceGroupName;
            }
            else if (!Convert.IsDBNull(reader["LpePriceGroupName"])) // Note: reader["LpePriceGroupName"] should be NULL for LpePriceGroupId == Guid.Empty
            {
                pmlBroker.m_LpePriceGroupName = (string)reader["LpePriceGroupName"];
            }

            if (Convert.ToBoolean(reader["UseDefaultWholesaleBranchId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.m_BranchNm = pmlBroker.Broker.DefaultWholesaleBranchName;
            }
            else if (!Convert.IsDBNull(reader["BranchNm"]))
            {
                pmlBroker.m_BranchNm = (string)reader["BranchNm"];
            }

            if (!Convert.IsDBNull(reader["Roles"]))
            {
                if (!string.IsNullOrEmpty((string)reader["Roles"]))
                {
                    pmlBroker.Roles = ObsoleteSerializationHelper.JsonDeserialize<HashSet<E_OCRoles>>((string)reader["Roles"]);
                }
                else
                {
                    pmlBroker.Roles = new HashSet<E_OCRoles>();
                }
            }
            else
            {
                pmlBroker.Roles = new HashSet<E_OCRoles>();
            }

            if (Convert.ToBoolean(reader["UseDefaultMiniCorrBranchId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultMiniCorrBranchId = true;
            }
            else if (!Convert.IsDBNull(reader["MiniCorrespondentBranchId"]))
            {
                pmlBroker.MiniCorrespondentBranchId = (Guid)reader["MiniCorrespondentBranchId"];
            }

            if ((bool)reader["UseMiniCorrespondentBranchLpePriceGroupId"])
            {
                pmlBroker.MiniCorrespondentLpePriceGroupId = Guid.Empty;
            }
            else if (Convert.ToBoolean(reader["UseDefaultMiniCorrPriceGroupId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultMiniCorrPriceGroupId = true;
            }
            else if (!Convert.IsDBNull(reader["MiniCorrespondentLpePriceGroupId"]))
            {
                pmlBroker.MiniCorrespondentLpePriceGroupId = (Guid)reader["MiniCorrespondentLpePriceGroupId"];
            }

            if (!Convert.IsDBNull(reader["MiniCorrespondentTPOPortalConfigID"]))
            {
                pmlBroker.MiniCorrespondentTPOPortalConfigID = (Guid)reader["MiniCorrespondentTPOPortalConfigID"];
            }

            if (Convert.ToBoolean(reader["UseDefaultCorrBranchId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultCorrBranchId = true;
            }
            else if (!Convert.IsDBNull(reader["CorrespondentBranchId"]))
            {
                pmlBroker.CorrespondentBranchId = (Guid)reader["CorrespondentBranchId"];
            }

            if ((bool)reader["UseCorrespondentBranchLpePriceGroupId"])
            {
                pmlBroker.CorrespondentLpePriceGroupId = Guid.Empty;
            }
            else if (Convert.ToBoolean(reader["UseDefaultCorrPriceGroupId"]) &&
                ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                pmlBroker.UseDefaultCorrPriceGroupId = true;
            }
            else if (!Convert.IsDBNull(reader["CorrespondentLpePriceGroupId"]))
            {
                pmlBroker.CorrespondentLpePriceGroupId = (Guid)reader["CorrespondentLpePriceGroupId"];
            }

            if (!Convert.IsDBNull(reader["CorrespondentTPOPortalConfigID"]))
            {
                pmlBroker.CorrespondentTPOPortalConfigID = (Guid)reader["CorrespondentTPOPortalConfigID"];
            }

            if (!Convert.IsDBNull(reader["UnderwritingAuthority"]))
            {
                pmlBroker.UnderwritingAuthority = (E_UnderwritingAuthority)Convert.ToInt32(reader["UnderwritingAuthority"]);
            }
            else
            {
                pmlBroker.UnderwritingAuthority = E_UnderwritingAuthority.PriorApproved;
            }

            pmlBroker.BrokerRoleStatusT = (OCStatusType)Convert.ToInt32(reader["BrokerRoleStatusT"]);
            pmlBroker.originalBrokerRoleStatusT = pmlBroker.BrokerRoleStatusT;
            pmlBroker.MiniCorrRoleStatusT = (OCStatusType)Convert.ToInt32(reader["MiniCorrRoleStatusT"]);
            pmlBroker.originalMiniCorrRoleStatusT = pmlBroker.MiniCorrRoleStatusT;
            pmlBroker.CorrRoleStatusT = (OCStatusType)Convert.ToInt32(reader["CorrRoleStatusT"]);
            pmlBroker.originalCorrRoleStatusT = pmlBroker.CorrRoleStatusT;

            var parser = new PmlBrokerRelationshipPermissionDataParser();
            parser.Load(reader);

            pmlBroker.BrokerRoleRelationships = PmlBrokerRelationship.Retrieve(pmlBroker.BrokerId, pmlBroker.PmlBrokerId, E_OCRoles.Broker, parser);
            pmlBroker.MiniCorrRoleRelationships = PmlBrokerRelationship.Retrieve(pmlBroker.BrokerId, pmlBroker.PmlBrokerId, E_OCRoles.MiniCorrespondent, parser);
            pmlBroker.CorrRoleRelationships = PmlBrokerRelationship.Retrieve(pmlBroker.BrokerId, pmlBroker.PmlBrokerId, E_OCRoles.Correspondent, parser);

            pmlBroker.Permissions = PmlBrokerPermission.Retrieve(pmlBroker.BrokerId, pmlBroker.PmlBrokerId, parser);

            pmlBroker.IndexNumber = reader["IndexNumber"] as int? ?? -1;

            return pmlBroker;
        }

        private void SetTaxIdFields(IDataReader reader)
        {
            var encryptedTaxId = reader["TaxIdEncrypted"] as byte[];
            this.encryptionKeyId = (Guid)reader["EncryptionKeyId"];

            var encryptionKey = EncryptionKeyIdentifier.Create(this.encryptionKeyId.Value).Value;
            var decryptedValue = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey, encryptedTaxId);

            // OC tax ID defaults to the empty string.
            this.unencryptedTaxId = decryptedValue ?? string.Empty;
        }

        #endregion

        protected PmlBroker()
        {
            // 6/8/2009 dd - Must use static method CreatePmlBroker or RetrievePmlBroker method.
            CustomPricingPolicyField1 = string.Empty;
            CustomPricingPolicyField2 = string.Empty;
            CustomPricingPolicyField3 = string.Empty;
            CustomPricingPolicyField4 = string.Empty;
            CustomPricingPolicyField5 = string.Empty;

            this.brokerInitializationFunction = () => BrokerDB.RetrieveByIdForceRefresh(this.BrokerId);
            this.broker = new Lazy<BrokerDB>(this.brokerInitializationFunction);
        }

        public bool CanSave(out string errorMessage)
        {
            errorMessage = "";

            if(ConstStage.IsDisableRequireOcFieldsDatalayer)
            {
                return true;
            }

            List<string> errors = new List<string>();
            if (string.IsNullOrEmpty(this.Name))
            {
                errors.Add("Empty Name");
            }
            if (string.IsNullOrEmpty(this.NmlsName))
            {
                errors.Add("Empty NmlsName");
            }
            if (string.IsNullOrEmpty(this.Addr))
            {
                errors.Add("Empty Addr");
            }
            if (string.IsNullOrEmpty(this.City))
            {
                errors.Add("Empty City");
            }
            if (string.IsNullOrEmpty(this.State))
            {
                errors.Add("Empty State");
            }
            if (string.IsNullOrEmpty(this.Zip))
            {
                errors.Add("Empty Zip");
            }
            if (string.IsNullOrEmpty(this.Phone))
            {
                errors.Add("Empty Phone");
            }
            if (this.Roles.Count <= 0)
            {
                errors.Add("No Roles Selected");
            }
            if ((this.Roles.Contains(E_OCRoles.Broker) && this.BrokerRoleStatusT == OCStatusType.Blank))
            {
                errors.Add("Broker Role Selected, but the BrokerRoleStatusT is blank.");
            }
            if ((this.Roles.Contains(E_OCRoles.MiniCorrespondent) && this.MiniCorrRoleStatusT == OCStatusType.Blank))
            {
                errors.Add("MiniCorrespondent Role Selected, but the MiniCorrRoleStatusT is blank.");
            }
            if ((this.Roles.Contains(E_OCRoles.Correspondent) && this.CorrRoleStatusT == OCStatusType.Blank))
            {
                errors.Add("Correspondent Role Selected, but the CorrRoleStatusT is blank.");
            }
            if (!m_isNew && GroupDB.GetGroupMembershipsForPmlBroker(this.BrokerId, this.PmlBrokerId).Count() <= 0 && GroupDB.GetAllGroups(this.BrokerId, GroupType.PmlBroker).Count() > 0)
            {
                errors.Add("No Company Group was chosen.");
            }
            if ((!this.UseDefaultCorrPriceGroupId && !this.CorrespondentLpePriceGroupId.HasValue))
            {
                errors.Add("Could not determine Correspondent Price Group Id.");
            }
            if ((!this.UseDefaultWholesalePriceGroupId && !this.LpePriceGroupId.HasValue))
            {
                errors.Add("Could not determine Wholesale Price Group Id.");
            }
            if ((!this.UseDefaultMiniCorrPriceGroupId && !this.MiniCorrespondentLpePriceGroupId.HasValue ))
            {
                errors.Add("Could not determine MiniCorrespondent Price Group Id.");
            }
            if ((!this.UseDefaultCorrBranchId && (!this.CorrespondentBranchId.HasValue || this.CorrespondentBranchId.Value.Equals(Guid.Empty))))
            {
                errors.Add("Could not determine Correspondent Branch Id.");
            }
            if ((!this.UseDefaultMiniCorrBranchId && (!this.MiniCorrespondentBranchId.HasValue || this.MiniCorrespondentBranchId.Value.Equals(Guid.Empty))))
            {
                errors.Add("Could not determine Mini Corr Branch Id.");
            }
            if ((!this.UseDefaultWholesaleBranchId && (!this.BranchId.HasValue || this.BranchId.Value.Equals(Guid.Empty))))
            {
                errors.Add("Could not determine Wholesale Branch Id.");
            }

            foreach (LicenseInfo license in this.LicenseInfoList.List)
            {
                if(!LicenseInfoList.IsValid(license))
                {
                    errors.Add($"License {license.License} is invalid.");
                }
            }

            if (this.taxIdValidOnLoad && !this.TaxIdValid)
            {
                errors.Add("The Tax ID is incorrectly formatted. Please update it to a 9 digit ID (12-3456789).");
            }

            if(errors.Count > 0)
            {
                errorMessage = "Errors: " + string.Join(", ", errors);
                return false;
            }

            return true;
        }

        public void Save()
        {
            string errorMessage = "";
            if(!this.CanSave(out errorMessage))
            {
                throw new CBaseException(errorMessage, errorMessage);
            }

            if (m_OriginatorCompensationAuditNeedsUpdate)
            {
                AddOriginatorCompensationAuditEvent();
                m_OriginatorCompensationAuditNeedsUpdate = false;
            }

            var parameters = new List<SqlParameter>(){
                                            new SqlParameter("@PmlBrokerId", this.PmlBrokerId)
                                            , new SqlParameter("@BrokerId", this.BrokerId)
                                            , new SqlParameter("@IsActive", this.IsActive)
                                            , new SqlParameter("@Name", this.Name ?? string.Empty)

                                            , new SqlParameter("@NmlsName", this.NmlsName ?? string.Empty)
                                            , new SqlParameter("@NmlsNameLckd", this.NmlsNameLckd)

                                            , new SqlParameter("@Addr", this.Addr ?? string.Empty)
                                            , new SqlParameter("@City", this.City ?? string.Empty)
                                            , new SqlParameter("@State", this.State ?? string.Empty)
                                            , new SqlParameter("@Zip", this.Zip ?? string.Empty)
                                            , new SqlParameter("@Phone", this.Phone ?? string.Empty)
                                            , new SqlParameter("@Fax", this.Fax ?? string.Empty)
                                            , new SqlParameter("@NmLsIdentifier", this.NmLsIdentifier ?? string.Empty)                                            
                                            , new SqlParameter("@LicenseXmlContent", this.LicenseInfoList.ToString() ?? string.Empty)
                                            , new SqlParameter("@CompanyId", this.CompanyId ?? string.Empty)
                                            , new SqlParameter("@PmlCompanyTierId", this.PmlCompanyTierId)
                                            , new SqlParameter("@PrincipalName1", this.PrincipalName1 ?? string.Empty)
                                            , new SqlParameter("@PrincipalName2", this.PrincipalName2 ?? string.Empty)
                                            , new SqlParameter("@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo", m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo)
                                            , new SqlParameter("@OriginatorCompensationPercent", m_OriginatorCompensationPercent)
                                            , new SqlParameter("@OriginatorCompensationBaseT", (int)m_OriginatorCompensationBaseT)
                                            , new SqlParameter("@OriginatorCompensationMinAmount", m_OriginatorCompensationMinAmount)
                                            , new SqlParameter("@OriginatorCompensationMaxAmount", m_OriginatorCompensationMaxAmount)
                                            , new SqlParameter("@OriginatorCompensationFixedAmount", m_OriginatorCompensationFixedAmount)
                                            , new SqlParameter("@OriginatorCompensationNotes", m_OriginatorCompensationNotes)
                                            , new SqlParameter("@OriginatorCompensationAuditXml", m_OriginatorCompensationAudit.ToString())
                                            , new SqlParameter("@OriginatorCompensationLastModifiedDate", m_OriginatorCompensationLastModifiedDate)
                                            , new SqlParameter("@CustomPricingPolicyField1", this.CustomPricingPolicyField1)
                                            , new SqlParameter("@CustomPricingPolicyField2", this.CustomPricingPolicyField2)
                                            , new SqlParameter("@CustomPricingPolicyField3", this.CustomPricingPolicyField3)
                                            , new SqlParameter("@CustomPricingPolicyField4", this.CustomPricingPolicyField4)
                                            , new SqlParameter("@CustomPricingPolicyField5", this.CustomPricingPolicyField5)
                                            , new SqlParameter("@Roles", ObsoleteSerializationHelper.JsonSerialize<HashSet<E_OCRoles>>(Roles))
                                            , new SqlParameter("@UnderwritingAuthority", UnderwritingAuthority)
                                            , new SqlParameter("@PMLNavigationPermissions", PMLNavigationPermissions)
                                            , new SqlParameter("@BrokerRoleStatusT", this.BrokerRoleStatusT)
                                            , new SqlParameter("@MiniCorrRoleStatusT", this.MiniCorrRoleStatusT)
                                            , new SqlParameter("@CorrRoleStatusT", this.CorrRoleStatusT)
                                            , new SqlParameter("@TpoColorThemeId", this.tpoColorThemeId ?? Guid.Empty)
                                        };

            string storedProcedureName = "UpdatePmlBroker";

            if (m_isNew)
            {
                storedProcedureName = "CreatePmlBroker";

                var indexNumber = BrokerDB.GetAndUpdateOriginatingCompanyIndexNumberCounter(this.BrokerId);
                parameters.Add(new SqlParameter("@IndexNumber", indexNumber));
            }

            parameters.Add(new SqlParameter("@LpePriceGroupId", this.LpePriceGroupId));
            parameters.Add(new SqlParameter("@UseDefaultWholesalePriceGroupId", this.UseDefaultWholesalePriceGroupId));

            parameters.Add(new SqlParameter("@BranchId", this.BranchId));
            parameters.Add(new SqlParameter("@UseDefaultWholesaleBranchId", this.UseDefaultWholesaleBranchId));

            parameters.Add(new SqlParameter("@MiniCorrespondentLpePriceGroupId", this.MiniCorrespondentLpePriceGroupId));
            parameters.Add(new SqlParameter("@UseDefaultMiniCorrPriceGroupId", this.UseDefaultMiniCorrPriceGroupId));

            parameters.Add(new SqlParameter("@MiniCorrespondentBranchId", this.MiniCorrespondentBranchId));
            parameters.Add(new SqlParameter("@UseDefaultMiniCorrBranchId", this.UseDefaultMiniCorrBranchId));

            parameters.Add(new SqlParameter("@CorrespondentLpePriceGroupId", this.CorrespondentLpePriceGroupId));
            parameters.Add(new SqlParameter("@UseDefaultCorrPriceGroupId", this.UseDefaultCorrPriceGroupId));

            parameters.Add(new SqlParameter("@CorrespondentBranchId", this.CorrespondentBranchId));
            parameters.Add(new SqlParameter("@UseDefaultCorrBranchId", this.UseDefaultCorrBranchId));

            if (TPOPortalConfigID.HasValue)
            {
                parameters.Add(new SqlParameter("@Lender_TPO_LandingPageConfigId", TPOPortalConfigID.Value));
            }

            if (MiniCorrespondentTPOPortalConfigID.HasValue)
            {
                parameters.Add(new SqlParameter("@MiniCorrespondentTPOPortalConfigID", MiniCorrespondentTPOPortalConfigID.Value));
            }

            if (CorrespondentTPOPortalConfigID.HasValue)
            {
                parameters.Add(new SqlParameter("@CorrespondentTPOPortalConfigID", CorrespondentTPOPortalConfigID.Value));
            }

            if (this.m_isNew)
            {
                var encryptionKeyDriver = GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create();
                var keyId = encryptionKeyDriver.GenerateKey().Key;
                parameters.Add(new SqlParameter("@EncryptionKeyId", keyId.Value));
                this.encryptionKeyId = keyId.Value;
            }

            var encryptionKey = EncryptionKeyIdentifier.Create(this.encryptionKeyId.Value).Value;
            var encryptedValue = string.IsNullOrEmpty(this.unencryptedTaxId) ? new byte[0] : LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(encryptionKey, this.unencryptedTaxId);
            parameters.Add(new SqlParameter("@TaxIdEncrypted", encryptedValue) { IsNullable = true });

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, storedProcedureName, 5, parameters.ToArray());

            // We need to save the PML broker's relationship and permission data
            // after we save the broker itself so that if the broker is new,
            // we will have a record in the PML_BROKER table for the PmlBrokerId 
            // foreign key.
            this.BrokerRoleRelationships.Save();
            this.MiniCorrRoleRelationships.Save();
            this.CorrRoleRelationships.Save();

            this.Permissions.Save();

            try
            {
                this.AddLoansToQueueForCacheUpdate();
            }
            catch (SqlException e)
            {
                // Mostly for sql timeout exception.
                Tools.LogError(e);
            }
        }

        private void AddLoansToQueueForCacheUpdate()
        {
            if ((this.originalBrokerRoleStatusT != this.BrokerRoleStatusT) ||
                (this.originalMiniCorrRoleStatusT != this.MiniCorrRoleStatusT) ||
                (this.originalCorrRoleStatusT != this.CorrRoleStatusT))
            {
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("sPmlBrokerId", this.PmlBrokerId.ToString()),
                    new SqlParameter("BrokerId", this.BrokerId.ToString())
                };

                List<Guid> ids = new List<Guid>();
                using (var reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "ListLoansByPmlBrokerId", param))
                {
                    while (reader.Read())
                    {
                        ids.Add((Guid)reader["sLId"]);
                    }
                }

                foreach (Guid id in ids)
                {
                    WorkflowCacheUpdater.SubmitToUpdateCacheFields(id, NonLoanInfoDependentLoanFields.sPmlBrokerStatusT);
                }
            }
        }

        public void AddOriginatorCompensationAuditEvent()
        {
            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;

            int id = m_OriginatorCompensationAudit.ParseXml(BrokerId).Count + 1;
            m_OriginatorCompensationLastModifiedDate = DateTime.Now;

            OriginatorCompensationAuditEvent newEvent = new OriginatorCompensationAuditEvent(
                id
                , principal.EmployeeId
                , DataAccess.CEmployeeFields.ComposeFullName(principal.FirstName, principal.LastName)
                , m_OriginatorCompensationLastModifiedDate.Value.ToString()
                , this.losConvert.ToRateString(m_OriginatorCompensationPercent)
                , m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.ToString()
                , m_OriginatorCompensationBaseT
                , this.losConvert.ToMoneyString(m_OriginatorCompensationMinAmount, FormatDirection.ToRep)
                , this.losConvert.ToMoneyString(m_OriginatorCompensationMaxAmount, FormatDirection.ToRep)
                , this.losConvert.ToMoneyString(m_OriginatorCompensationFixedAmount, FormatDirection.ToRep)
                , m_OriginatorCompensationNotes
                );
            m_OriginatorCompensationAudit.AddAuditEvent(newEvent.GetAuditEventXElement());
        }

        public string GetCustomPricingPolicyField(int id)
        {
            switch (id)
            {
                case 1: return this.CustomPricingPolicyField1;
                case 2: return this.CustomPricingPolicyField2;
                case 3: return this.CustomPricingPolicyField3;
                case 4: return this.CustomPricingPolicyField4;
                case 5: return this.CustomPricingPolicyField5;
                default: throw new CBaseException(ErrorMessages.Generic, "Trying to access unimplemented custom pricing policy field.");
            }
        }

        public static Comparison<PmlBroker> NameComparison = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p1.Name.CompareTo(p2.Name);
        };

        public static Comparison<PmlBroker> NameComparisonDesc = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p2.Name.CompareTo(p1.Name);
        };

        public static Comparison<PmlBroker> CompanyIdComparison = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p1.CompanyId.CompareTo(p2.CompanyId);
        };

        public static Comparison<PmlBroker> CompanyIdComparisonDesc = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p2.CompanyId.CompareTo(p1.CompanyId);
        };

        public static Comparison<PmlBroker> LpePriceGroupNameComparison = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p1.LpePriceGroupName.CompareTo(p2.LpePriceGroupName);
        };

        public static Comparison<PmlBroker> LpePriceGroupNameComparisonDesc = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p2.LpePriceGroupName.CompareTo(p1.LpePriceGroupName);
        };

        public static Comparison<PmlBroker> BranchNmComparison = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p1.BranchNm.CompareTo(p2.BranchNm);
        };

        public static Comparison<PmlBroker> BranchNmComparisonDesc = delegate(PmlBroker p1, PmlBroker p2)
        {
            return p2.BranchNm.CompareTo(p1.BranchNm);
        };

        public int CompareTo(PmlBroker other)
        {
            return Name.CompareTo(other.Name);
        }

        private static string FormatBaseT(E_PercentBaseT baseT)
        {
            switch (baseT)
            {
                case E_PercentBaseT.LoanAmount:
                    return "Loan Amount";
                case E_PercentBaseT.TotalLoanAmount:
                    return "Total Loan Amount";
                default:
                    throw new UnhandledEnumException(baseT);
            }
        }
        private static string FormatLevelT(E_OrigiantorCompenationLevelT levelT)
        {
            switch (levelT)
            {
                case E_OrigiantorCompenationLevelT.IndividualUser:
                    return "Individual User";
                case E_OrigiantorCompenationLevelT.OriginatingCompany:
                    return "Originating Company";
                default:
                    throw new UnhandledEnumException(levelT);
            }
        }

        public static string FormatPopulationType(EmployeePopulationMethodT populationMethod)
        {
            switch(populationMethod)
            {
                case EmployeePopulationMethodT.IndividualUser:
                    return "Individual User";
                case EmployeePopulationMethodT.OriginatingCompany:
                    return "Originating Company";
                default:
                    throw new UnhandledEnumException(populationMethod);
            }
        }

        /// <summary>
        /// Handle adding a cell to the binding table.  We can override this
        /// interface and do cell replacement as we're binding.
        /// </summary>

        public interface FieldFilter
        {
            /// <summary>
            /// Handle adding a cell to the binding table.  We can override
            /// this interface and do cell replacement as we're binding.
            /// </summary>

            String BindCell(String sKey, String sValue);

        }

        /// <summary>
        /// Keep track of the last bound csv table.  Format is determined
        /// by our installed mapping.
        /// </summary>

        public class CsvTable
        {
            /// <summary>
            /// Keep track of the last bound csv table.  Format is
            /// determined by our installed mapping.
            /// </summary>
            private StringBuilder m_Table = new StringBuilder();
            private List<KeyValuePair<string, string>> m_map = new List<KeyValuePair<string, string>>();
            private FieldFilter m_Filter = null;
            private IDataReader m_Source = null;

            public IDataReader DataSource
            {
                set { m_Source = value; }
            }

            public FieldFilter DataFilter
            {
                set { m_Filter = value; }
            }

            /// <summary>
            /// Add to our mapping given what the user has described.
            /// We store in order of add.
            /// </summary>
            public void SetLayout(string sLabel, string sKey)
            {
                // Add to our mapping given what the user has
                // described.  We store in order of add.
                if (string.IsNullOrEmpty(sKey))
                {
                    throw new ArgumentException("Invalid label - key combination for table mapping.");
                }
                if (string.IsNullOrEmpty(sLabel))
                {
                    sLabel = string.Empty;
                }
                m_map.Add(new KeyValuePair<string, string>(sKey, sLabel));
            }
            /// <summary>
            /// Take the current source and initialize our table
            /// to hold the sourced content.
            /// </summary>
            public void DataBind()
            {
                // Validate the mapping and data source.  If missing,
                // we punt.

                if (m_map.Count == 0)
                {
                    throw new ArgumentException("Invalid column mapping.");
                }

                if (m_Source == null)
                {
                    throw new ArgumentException("Invalid data source.");
                }

                m_Table.Remove(0, m_Table.Length);

                int i = 0;

                foreach (var mCol in m_map)
                {
                    if (i > 0)
                        m_Table.Append(",");

                    if (mCol.Value.IndexOf(',') != -1)
                        m_Table.Append("\"" + mCol.Value + "\"");
                    else
                        m_Table.Append(mCol.Value);

                    ++i;
                }

                m_Table.Append("\r\n");

                while (m_Source.Read() == true)
                {
                    int j = 0;

                    foreach (var mCol in m_map)
                    {
                        string sCell = string.Empty;

                        if (j > 0)
                            m_Table.Append(",");

                        try
                        {
                            // For Case 34667 - Adding strings to the table which contain multiple lines are causing
                            // extra lines to appear in the table and the exported data becomes incorrectly formatted.
                            // Due to this, new lines in the string are removed
                            sCell = m_Source[mCol.Key].ToString().Replace(Environment.NewLine, " ");
                        }
                        catch
                        {
                        }

                        if (m_Filter != null)
                            sCell = m_Filter.BindCell(mCol.Key, sCell);

                        if (sCell.IndexOf(',') != -1)
                            m_Table.Append("\"" + sCell + "\"");
                        else
                            m_Table.Append(sCell);

                        ++j;
                    }

                    m_Table.Append("\r\n");
                }

                m_Source = null;
            }

            /// <summary>
            /// Dump the contents of this csv table.
            /// </summary>
            public override string ToString()
            {
                // Dump our last bound contents.

                return m_Table.ToString();
            }
        }

        /// <summary>
        /// Hook into table binding with this relationship fixup
        /// helper.
        /// </summary>

        public class RoleFixup : FieldFilter
        {
            /// <summary>
            /// Keep track of the broker's employees assigned to
            /// roles during binding.
            /// </summary>
            private BrokerLoanAssignmentTable m_Table = new BrokerLoanAssignmentTable();

            /// <summary>
            /// Keeps a lookup table of price group names by id.
            /// </summary>
            private Dictionary<Guid, string> priceGroupNameById = new Dictionary<Guid, string>();

            /// <summary>
            /// Keeps a lookup table of branch names by id.
            /// </summary>
            private Dictionary<Guid, string> branchNameById = new Dictionary<Guid, string>();

            /// <summary>
            /// Keeps a lookup table of supervisor names by id.
            /// </summary>
            private Dictionary<Guid, string> supervisorNameById = new Dictionary<Guid, string>();

            private string m_LenderAccExecId = Guid.Empty.ToString();
            private string m_LockDeskId = Guid.Empty.ToString();
            private string m_ManagerId = Guid.Empty.ToString();
            private string m_ProcessorId = Guid.Empty.ToString();
            private string m_UnderwriterId = Guid.Empty.ToString();
            private string m_BrokerProcessorId = Guid.Empty.ToString();
            // 12/3/2013 gf - opm 145015
            private string m_JuniorProcessorId = Guid.Empty.ToString();
            private string m_JuniorUnderwriterId = Guid.Empty.ToString();
            private string m_LoanOfficerAssistantId = Guid.Empty.ToString();
            private ReadOnlyCollection<string> IgnoreUnassignedRoleFields = new List<string>()
            {
                "JuniorUnderwriterPhone",
                "JuniorUnderwriterEmail",
                "JuniorUnderwriterLoginNm",
                "JuniorUnderwriterEmployeeId",
                "JuniorProcessorPhone",
                "JuniorProcessorEmail",
                "JuniorProcessorLoginNm",
                "JuniorProcessorEmployeeId",
                "LoanOfficerAssistantPhone",
                "LoanOfficerAssistantEmail",
                "LoanOfficerAssistantLoginNm",
                "LoanOfficerAssistantEmployeeId",
                "MiniCorrManagerPhone",
                "MiniCorrManagerEmail",
                "MiniCorrManagerLoginNm",
                "MiniCorrManagerEmployeeId",
                "MiniCorrProcessorPhone",
                "MiniCorrProcessorEmail",
                "MiniCorrProcessorLoginNm",
                "MiniCorrProcessorEmployeeId",
                "MiniCorrJuniorProcessorPhone",
                "MiniCorrJuniorProcessorEmail",
                "MiniCorrJuniorProcessorLoginNm",
                "MiniCorrJuniorProcessorEmployeeId",
                "MiniCorrLenderAccExecPhone",
                "MiniCorrLenderAccExecEmail",
                "MiniCorrLenderAccExecLoginNm",
                "MiniCorrLenderAccExecEmployeeId",
                "MiniCorrExternalPostCloserPhone",
                "MiniCorrExternalPostCloserEmail",
                "MiniCorrExternalPostCloserLoginNm",
                "MiniCorrExternalPostCloserEmployeeId",
                "MiniCorrUnderwriterPhone",
                "MiniCorrUnderwriterEmail",
                "MiniCorrUnderwriterLoginNm",
                "MiniCorrUnderwriterEmployeeId",
                "MiniCorrJuniorUnderwriterPhone",
                "MiniCorrJuniorUnderwriterEmail",
                "MiniCorrJuniorUnderwriterLoginNm",
                "MiniCorrJuniorUnderwriterEmployeeId",
                "MiniCorrCreditAuditorPhone",
                "MiniCorrCreditAuditorEmail",
                "MiniCorrCreditAuditorLoginNm",
                "MiniCorrCreditAuditorEmployeeId",
                "MiniCorrLegalAuditorPhone",
                "MiniCorrLegalAuditorEmail",
                "MiniCorrLegalAuditorLoginNm",
                "MiniCorrLegalAuditorEmployeeId",
                "MiniCorrLockDeskPhone",
                "MiniCorrLockDeskEmail",
                "MiniCorrLockDeskLoginNm",
                "MiniCorrLockDeskEmployeeId",
                "MiniCorrPurchaserPhone",
                "MiniCorrPurchaserEmail",
                "MiniCorrPurchaserLoginNm",
                "MiniCorrPurchaserEmployeeId",
                "MiniCorrSecondaryPhone",
                "MiniCorrSecondaryEmail",
                "MiniCorrSecondaryLoginNm",
                "MiniCorrSecondaryEmployeeId",
                "CorrManagerPhone",
                "CorrManagerEmail",
                "CorrManagerLoginNm",
                "CorrManagerEmployeeId",
                "CorrProcessorPhone",
                "CorrProcessorEmail",
                "CorrProcessorLoginNm",
                "CorrProcessorEmployeeId",
                "CorrJuniorProcessorPhone",
                "CorrJuniorProcessorEmail",
                "CorrJuniorProcessorLoginNm",
                "CorrJuniorProcessorEmployeeId",
                "CorrLenderAccExecPhone",
                "CorrLenderAccExecEmail",
                "CorrLenderAccExecLoginNm",
                "CorrLenderAccExecEmployeeId",
                "CorrExternalPostCloserPhone",
                "CorrExternalPostCloserEmail",
                "CorrExternalPostCloserLoginNm",
                "CorrExternalPostCloserEmployeeId",
                "CorrUnderwriterPhone",
                "CorrUnderwriterEmail",
                "CorrUnderwriterLoginNm",
                "CorrUnderwriterEmployeeId",
                "CorrJuniorUnderwriterPhone",
                "CorrJuniorUnderwriterEmail",
                "CorrJuniorUnderwriterLoginNm",
                "CorrJuniorUnderwriterEmployeeId",
                "CorrCreditAuditorPhone",
                "CorrCreditAuditorEmail",
                "CorrCreditAuditorLoginNm",
                "CorrCreditAuditorEmployeeId",
                "CorrLegalAuditorPhone",
                "CorrLegalAuditorEmail",
                "CorrLegalAuditorLoginNm",
                "CorrLegalAuditorEmployeeId",
                "CorrLockDeskPhone",
                "CorrLockDeskEmail",
                "CorrLockDeskLoginNm",
                "CorrLockDeskEmployeeId",
                "CorrPurchaserPhone",
                "CorrPurchaserEmail",
                "CorrPurchaserLoginNm",
                "CorrPurchaserEmployeeId",
                "CorrSecondaryPhone",
                "CorrSecondaryEmail",
                "CorrSecondaryLoginNm",
                "CorrSecondaryEmployeeId"
            }.AsReadOnly();

            private string m_MiniCorrLenderAccExecId = Guid.Empty.ToString();
            private string m_MiniCorrLockDeskId = Guid.Empty.ToString();
            private string m_MiniCorrManagerId = Guid.Empty.ToString();
            private string m_MiniCorrProcessorId = Guid.Empty.ToString();
            private string m_MiniCorrUnderwriterId = Guid.Empty.ToString();
            private string m_MiniCorrExternalPostCloserId = Guid.Empty.ToString();
            private string m_MiniCorrJuniorProcessorId = Guid.Empty.ToString();
            private string m_MiniCorrJuniorUnderwriterId = Guid.Empty.ToString();
            private string m_MiniCorrLoanOfficerAssistantId = Guid.Empty.ToString();
            private string m_MiniCorrCreditAuditorId = Guid.Empty.ToString();
            private string m_MiniCorrLegalAuditorId = Guid.Empty.ToString();
            private string m_MiniCorrPurchaserId = Guid.Empty.ToString();
            private string m_MiniCorrSecondaryId = Guid.Empty.ToString();

            private string m_CorrLenderAccExecId = Guid.Empty.ToString();
            private string m_CorrLockDeskId = Guid.Empty.ToString();
            private string m_CorrManagerId = Guid.Empty.ToString();
            private string m_CorrProcessorId = Guid.Empty.ToString();
            private string m_CorrUnderwriterId = Guid.Empty.ToString();
            private string m_CorrExternalPostCloserId = Guid.Empty.ToString();
            private string m_CorrJuniorProcessorId = Guid.Empty.ToString();
            private string m_CorrJuniorUnderwriterId = Guid.Empty.ToString();
            private string m_CorrLoanOfficerAssistantId = Guid.Empty.ToString();
            private string m_CorrCreditAuditorId = Guid.Empty.ToString();
            private string m_CorrLegalAuditorId = Guid.Empty.ToString();
            private string m_CorrPurchaserId = Guid.Empty.ToString();
            private string m_CorrSecondaryId = Guid.Empty.ToString();


            /// <summary>
            /// Handle adding a cell to the binding table.  We can override
            /// this interface and do cell replacement as we're binding.
            /// </summary>
            public string BindCell(string sKey, string sValue)
            {
                // Look for a fixup column by its key.  If we're not binding
                // to one of the relationship columns, then skip it and return
                // what was given.
                IEnumerable<BrokerLoanAssignmentTable.Spec> eList = null;

                // NOTE: in order for these phone and email values to be set properly, we must receive the ID before
                // attempting to retrieve the phone and e-mail info. Therefore, the employeeID field must come before
                // the corresponding phone and e-mail of that employee in the export list.
                // OPM 45221
                switch (sKey)
                {
                    case "LenderAccExecPhone":
                    case "LenderAccExecEmail":
                    case "LenderAccExecLoginNm":
                        sValue = m_LenderAccExecId;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "LenderAccExecEmployeeId":
                        m_LenderAccExecId = sValue;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "LockDeskPhone":
                    case "LockDeskEmail":
                    case "LockDeskLoginNm":
                        sValue = m_LockDeskId;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "LockDeskEmployeeId":
                        m_LockDeskId = sValue;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "ManagerPhone":
                    case "ManagerEmail":
                    case "ManagerLoginNm":
                        sValue = m_ManagerId;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "ManagerEmployeeId":
                        m_ManagerId = sValue;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "UnderwriterPhone":
                    case "UnderwriterEmail":
                    case "UnderwriterLoginNm":
                        sValue = m_UnderwriterId;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "UnderwriterEmployeeId":
                        m_UnderwriterId = sValue;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "ProcessorPhone":
                    case "ProcessorEmail":
                    case "ProcessorLoginNm":
                        sValue = m_ProcessorId;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "ProcessorEmployeeId":
                        m_ProcessorId = sValue;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "BrokerProcessorPhone":
                    case "BrokerProcessorEmail":
                    case "BrokerProcessorLoginNm":
                        sValue = m_BrokerProcessorId;
                        eList = m_Table[E_RoleT.Pml_BrokerProcessor];
                        break;
                    case "BrokerProcessorEmployeeId":
                        m_BrokerProcessorId = sValue;
                        eList = m_Table[E_RoleT.Pml_BrokerProcessor];
                        break;
                    case "JuniorUnderwriterPhone":
                    case "JuniorUnderwriterEmail":
                    case "JuniorUnderwriterLoginNm":
                        sValue = m_JuniorUnderwriterId;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "JuniorUnderwriterEmployeeId":
                        m_JuniorUnderwriterId = sValue;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "JuniorProcessorPhone":
                    case "JuniorProcessorEmail":
                    case "JuniorProcessorLoginNm":
                        sValue = m_JuniorProcessorId;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "JuniorProcessorEmployeeId":
                        m_JuniorProcessorId = sValue;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "LoanOfficerAssistantPhone":
                    case "LoanOfficerAssistantEmail":
                    case "LoanOfficerAssistantLoginNm":
                        sValue = m_LoanOfficerAssistantId;
                        eList = m_Table[E_RoleT.LoanOfficerAssistant];
                        break;
                    case "LoanOfficerAssistantEmployeeId":
                        m_LoanOfficerAssistantId = sValue;
                        eList = m_Table[E_RoleT.LoanOfficerAssistant];
                        break;
                    case "MiniCorrManagerPhone":
                    case "MiniCorrManagerEmail":
                    case "MiniCorrManagerLoginNm":
                        sValue = m_MiniCorrManagerId;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "MiniCorrManagerEmployeeId":
                        m_MiniCorrManagerId = sValue;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "MiniCorrProcessorPhone":
                    case "MiniCorrProcessorEmail":
                    case "MiniCorrProcessorLoginNm":
                        sValue = m_MiniCorrProcessorId;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "MiniCorrProcessorEmployeeId":
                        m_MiniCorrProcessorId = sValue;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "MiniCorrJuniorProcessorPhone":
                    case "MiniCorrJuniorProcessorEmail":
                    case "MiniCorrJuniorProcessorLoginNm":
                        sValue = m_MiniCorrJuniorProcessorId;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "MiniCorrJuniorProcessorEmployeeId":
                        m_MiniCorrJuniorProcessorId = sValue;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "MiniCorrLenderAccExecPhone":
                    case "MiniCorrLenderAccExecEmail":
                    case "MiniCorrLenderAccExecLoginNm":
                        sValue = m_MiniCorrLenderAccExecId;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "MiniCorrLenderAccExecEmployeeId":
                        m_MiniCorrLenderAccExecId = sValue;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "MiniCorrExternalPostCloserPhone":
                    case "MiniCorrExternalPostCloserEmail":
                    case "MiniCorrExternalPostCloserLoginNm":
                        sValue = m_MiniCorrExternalPostCloserId;
                        eList = m_Table[E_RoleT.Pml_PostCloser];
                        break;
                    case "MiniCorrExternalPostCloserEmployeeId":
                        m_MiniCorrExternalPostCloserId = sValue;
                        eList = m_Table[E_RoleT.Pml_PostCloser];
                        break;
                    case "MiniCorrUnderwriterPhone":
                    case "MiniCorrUnderwriterEmail":
                    case "MiniCorrUnderwriterLoginNm":
                        sValue = m_MiniCorrUnderwriterId;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "MiniCorrUnderwriterEmployeeId":
                        m_MiniCorrUnderwriterId = sValue;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "MiniCorrJuniorUnderwriterPhone":
                    case "MiniCorrJuniorUnderwriterEmail":
                    case "MiniCorrJuniorUnderwriterLoginNm":
                        sValue = m_MiniCorrJuniorUnderwriterId;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "MiniCorrJuniorUnderwriterEmployeeId":
                        m_MiniCorrJuniorUnderwriterId = sValue;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "MiniCorrCreditAuditorPhone":
                    case "MiniCorrCreditAuditorEmail":
                    case "MiniCorrCreditAuditorLoginNm":
                        sValue = m_MiniCorrCreditAuditorId;
                        eList = m_Table[E_RoleT.CreditAuditor];
                        break;
                    case "MiniCorrCreditAuditorEmployeeId":
                        m_MiniCorrCreditAuditorId = sValue;
                        eList = m_Table[E_RoleT.CreditAuditor];
                        break;
                    case "MiniCorrLegalAuditorPhone":
                    case "MiniCorrLegalAuditorEmail":
                    case "MiniCorrLegalAuditorLoginNm":
                        sValue = m_MiniCorrLegalAuditorId;
                        eList = m_Table[E_RoleT.LegalAuditor];
                        break;
                    case "MiniCorrLegalAuditorEmployeeId":
                        m_MiniCorrLegalAuditorId = sValue;
                        eList = m_Table[E_RoleT.LegalAuditor];
                        break;
                    case "MiniCorrLockDeskPhone":
                    case "MiniCorrLockDeskEmail":
                    case "MiniCorrLockDeskLoginNm":
                        sValue = m_MiniCorrLockDeskId;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "MiniCorrLockDeskEmployeeId":
                        m_MiniCorrLockDeskId = sValue;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "MiniCorrPurchaserPhone":
                    case "MiniCorrPurchaserEmail":
                    case "MiniCorrPurchaserLoginNm":
                        sValue = m_MiniCorrPurchaserId;
                        eList = m_Table[E_RoleT.Purchaser];
                        break;
                    case "MiniCorrPurchaserEmployeeId":
                        m_MiniCorrPurchaserId = sValue;
                        eList = m_Table[E_RoleT.Purchaser];
                        break;
                    case "MiniCorrSecondaryPhone":
                    case "MiniCorrSecondaryEmail":
                    case "MiniCorrSecondaryLoginNm":
                        sValue = m_MiniCorrSecondaryId;
                        eList = m_Table[E_RoleT.Secondary];
                        break;
                    case "MiniCorrSecondaryEmployeeId":
                        m_MiniCorrSecondaryId = sValue;
                        eList = m_Table[E_RoleT.Secondary];
                        break;
                    case "CorrManagerPhone":
                    case "CorrManagerEmail":
                    case "CorrManagerLoginNm":
                        sValue = m_CorrManagerId;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "CorrManagerEmployeeId":
                        m_CorrManagerId = sValue;
                        eList = m_Table[E_RoleT.Manager];
                        break;
                    case "CorrProcessorPhone":
                    case "CorrProcessorEmail":
                    case "CorrProcessorLoginNm":
                        sValue = m_CorrProcessorId;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "CorrProcessorEmployeeId":
                        m_CorrProcessorId = sValue;
                        eList = m_Table[E_RoleT.Processor];
                        break;
                    case "CorrJuniorProcessorPhone":
                    case "CorrJuniorProcessorEmail":
                    case "CorrJuniorProcessorLoginNm":
                        sValue = m_CorrJuniorProcessorId;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "CorrJuniorProcessorEmployeeId":
                        m_CorrJuniorProcessorId = sValue;
                        eList = m_Table[E_RoleT.JuniorProcessor];
                        break;
                    case "CorrLenderAccExecPhone":
                    case "CorrLenderAccExecEmail":
                    case "CorrLenderAccExecLoginNm":
                        sValue = m_CorrLenderAccExecId;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "CorrLenderAccExecEmployeeId":
                        m_CorrLenderAccExecId = sValue;
                        eList = m_Table[E_RoleT.LenderAccountExecutive];
                        break;
                    case "CorrExternalPostCloserPhone":
                    case "CorrExternalPostCloserEmail":
                    case "CorrExternalPostCloserLoginNm":
                        sValue = m_CorrExternalPostCloserId;
                        eList = m_Table[E_RoleT.Pml_PostCloser];
                        break;
                    case "CorrExternalPostCloserEmployeeId":
                        m_CorrExternalPostCloserId = sValue;
                        eList = m_Table[E_RoleT.Pml_PostCloser];
                        break;
                    case "CorrUnderwriterPhone":
                    case "CorrUnderwriterEmail":
                    case "CorrUnderwriterLoginNm":
                        sValue = m_CorrUnderwriterId;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "CorrUnderwriterEmployeeId":
                        m_CorrUnderwriterId = sValue;
                        eList = m_Table[E_RoleT.Underwriter];
                        break;
                    case "CorrJuniorUnderwriterPhone":
                    case "CorrJuniorUnderwriterEmail":
                    case "CorrJuniorUnderwriterLoginNm":
                        sValue = m_CorrJuniorUnderwriterId;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "CorrJuniorUnderwriterEmployeeId":
                        m_CorrJuniorUnderwriterId = sValue;
                        eList = m_Table[E_RoleT.JuniorUnderwriter];
                        break;
                    case "CorrCreditAuditorPhone":
                    case "CorrCreditAuditorEmail":
                    case "CorrCreditAuditorLoginNm":
                        sValue = m_CorrCreditAuditorId;
                        eList = m_Table[E_RoleT.CreditAuditor];
                        break;
                    case "CorrCreditAuditorEmployeeId":
                        m_CorrCreditAuditorId = sValue;
                        eList = m_Table[E_RoleT.CreditAuditor];
                        break;
                    case "CorrLegalAuditorPhone":
                    case "CorrLegalAuditorEmail":
                    case "CorrLegalAuditorLoginNm":
                        sValue = m_CorrLegalAuditorId;
                        eList = m_Table[E_RoleT.LegalAuditor];
                        break;
                    case "CorrLegalAuditorEmployeeId":
                        m_CorrLegalAuditorId = sValue;
                        eList = m_Table[E_RoleT.LegalAuditor];
                        break;
                    case "CorrLockDeskPhone":
                    case "CorrLockDeskEmail":
                    case "CorrLockDeskLoginNm":
                        sValue = m_CorrLockDeskId;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "CorrLockDeskEmployeeId":
                        m_CorrLockDeskId = sValue;
                        eList = m_Table[E_RoleT.LockDesk];
                        break;
                    case "CorrPurchaserPhone":
                    case "CorrPurchaserEmail":
                    case "CorrPurchaserLoginNm":
                        sValue = m_CorrPurchaserId;
                        eList = m_Table[E_RoleT.Purchaser];
                        break;
                    case "CorrPurchaserEmployeeId":
                        m_CorrPurchaserId = sValue;
                        eList = m_Table[E_RoleT.Purchaser];
                        break;
                    case "CorrSecondaryPhone":
                    case "CorrSecondaryEmail":
                    case "CorrSecondaryLoginNm":
                        sValue = m_CorrSecondaryId;
                        eList = m_Table[E_RoleT.Secondary];
                        break;
                    case "CorrSecondaryEmployeeId":
                        m_CorrSecondaryId = sValue;
                        eList = m_Table[E_RoleT.Secondary];
                        break;
                }

                // 08/16/06 mf.  Adjusted the code so that when we
                // do not find the employee in that role, we display
                // nothing, rather than the ugly Guid in the export.

                if (eList != null)
                {
                    try
                    {
                        Guid eId = new Guid(sValue);

                        foreach (BrokerLoanAssignmentTable.Spec eSpec in eList)
                        {

                            if (eSpec.EmployeeId == eId)
                            {
                                // We match the employee relationship, so return the
                                // full name of the connected employee.
                                if (sKey.ToLower().Contains("phone"))
                                    return eSpec.Phone;
                                else if (sKey.ToLower().Contains("email"))
                                    return eSpec.Email;
                                else if (sKey.ToLower().Contains("login"))
                                    return eSpec.LoginName;
                                else
                                    return eSpec.FullName;
                            }
                        }
                    }
                    catch
                    {
                    }

                    return string.Empty;
                }
                else if (IgnoreUnassignedRoleFields.Contains(sKey))
                {
                    // If there are no employees in the new roles, export empty 
                    // string instead of Guid.Empty -- that seems to be desired
                    // based on note above.
                    return "";
                }

                // 07/08/08 ck - OPM 20750.  Lookup supervisor.
                if (sKey == "PmlExternalManagerEmployeeId")
                {
                    return this.GetSafeNameByIdFrom(this.supervisorNameById, sValue);
                }

                if (sKey == "LpePriceGroupId")
                {
                    return this.GetSafeNameByIdFrom(this.priceGroupNameById, sValue);
                }

                if (sKey == "MiniCorrespondentPriceGroupId" ||
                    sKey == "CorrespondentPriceGroupId")
                {
                    return this.GetSafeNameByIdFrom(this.priceGroupNameById, sValue);
                }

                // 08/16/06 mf - OPM 6957.  Lookup branch.
                if (sKey == "BranchId")
                {
                    return this.GetSafeNameByIdFrom(this.branchNameById, sValue);
                }

                if (sKey == "MiniCorrespondentBranchId" ||
                    sKey == "CorrespondentBranchId")
                {
                    return this.GetSafeNameByIdFrom(this.branchNameById, sValue);
                }

                //format recent login date
                if (sKey == "RecentLoginD")
                {
                    if ((sValue != null) && (sValue != ""))
                    {
                        DateTime rLoginD = DateTime.Parse(sValue);
                        if (rLoginD > DateTime.Now.AddDays(1))
                        {
                            return "";
                        }
                        else
                        {
                            return (rLoginD.ToString() + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(rLoginD) ? " PDT" : " PST"));
                        }
                    }
                }

                if (sKey == "PasswordExpirationD")
                {
                    if ((sValue != null) && (sValue != ""))
                    {
                        DateTime rLoginD = DateTime.Parse(sValue);
                        if (rLoginD.CompareTo(SmallDateTime.MinValue) <= 0 || rLoginD.CompareTo(SmallDateTime.MaxValue) >= 0)
                        {
                            return "";
                        }
                        else
                        {
                            return (rLoginD.ToString() + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(rLoginD) ? " PDT" : " PST"));
                        }
                    }
                }

                if (sKey == "StartD")
                {
                    if (!(string.IsNullOrEmpty(sValue)))
                    {
                        DateTime rStartD = DateTime.Parse(sValue);
                        return (rStartD.ToString() + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(rStartD) ? " PDT" : " PST"));
                    }
                }

                if (sKey == "LicenseXmlContent")
                {
                    string licenseList = "";
                    LicenseInfoList list = new LicenseInfoList();

                    try
                    {
                        list = LicenseInfoList.ToObject(sValue);
                    }
                    catch
                    {
                        return String.Empty;
                    }
                    foreach (LicenseInfo lInfo in list)
                    {
                        string userLicense = "";
                        if (!(lInfo.License.Equals("")))
                        {
                            userLicense += lInfo.License;

                            if (!(lInfo.State.Equals("")))
                            {
                                userLicense += " -State: " + lInfo.State;
                            }
                            if (!(lInfo.ExpD.Equals("")))
                            {
                                userLicense += " -Expires: " + lInfo.ExpD;
                            }
                            licenseList += (licenseList.Equals("")) ? userLicense : " + " + userLicense;
                        }

                    }
                    return licenseList;
                }

                return sValue;
            }

            /// <summary>
            /// Load the role assignment table from the database.
            /// </summary>
            public void Retrieve(Guid brokerId)
            {
                // Initialize our role assignment table with the roles
                // that we bind against.
                //
                // 7/22/2005 kb - Reworked role loading to use new
                // view.  It seems that loading each role individually
                // is better than loading the whole broker when you
                // expect to have *many* loan officers because of pml.

                m_Table.Retrieve(brokerId, E_RoleT.LenderAccountExecutive);
                m_Table.Retrieve(brokerId, E_RoleT.LockDesk);
                m_Table.Retrieve(brokerId, E_RoleT.Manager);
                m_Table.Retrieve(brokerId, E_RoleT.Underwriter);
                m_Table.Retrieve(brokerId, E_RoleT.Processor);
                m_Table.Retrieve(brokerId, E_RoleT.Pml_BrokerProcessor);
                m_Table.Retrieve(brokerId, E_RoleT.JuniorUnderwriter);
                m_Table.Retrieve(brokerId, E_RoleT.JuniorProcessor);
                m_Table.Retrieve(brokerId, E_RoleT.LoanOfficerAssistant);
                m_Table.Retrieve(brokerId, E_RoleT.Pml_PostCloser);
                m_Table.Retrieve(brokerId, E_RoleT.CreditAuditor);
                m_Table.Retrieve(brokerId, E_RoleT.LegalAuditor);
                m_Table.Retrieve(brokerId, E_RoleT.Purchaser);
                m_Table.Retrieve(brokerId, E_RoleT.Secondary);

                // 8/5/2005 kb - Load the pricing groups and decorate
                // the disabled ones.
                SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };
                this.priceGroupNameById.Clear();
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListPricingGroupByBrokerId", parameters))
                {

                    while (sR.Read() == true)
                    {
                        String groupNm = (String)sR["LpePriceGroupName"];
                        Guid groupId = (Guid)sR["LpePriceGroupId"];

                        if ((bool)sR["ExternalPriceGroupEnabled"] == false)
                        {
                            groupNm += " (disabled)";
                        }
                        
                        this.priceGroupNameById[groupId] = groupNm;
                    }
                }

                this.branchNameById.Clear();
                // 08/16/06 mf - OPM 6957.  We now include the user's branch.
                using (IDataReader sR = BranchDB.GetBranches(brokerId))
                {
                    while (sR.Read() == true)
                    {
                        // The store procedures use nolock which has randomly led to duplicates in these calls.
                        this.branchNameById[(Guid)sR["BranchId"]] = (string)sR["Name"];
                    }
                }

                // 07/08/08 ck - OPM 20750.  Include the user's Supervisor/IsSupervisor columns.
                parameters = new[] { new SqlParameter("@BrokerId", brokerId)};

                this.supervisorNameById.Clear();

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListPriceMyLoanManagerByBrokerId", parameters))
                {
                    while (sR.Read() == true)
                    {
                        // The store procedures use nolock which has randomly led to duplicates in these calls.
                        this.supervisorNameById[(Guid)sR["EmployeeId"]] = (string)sR["UserFullName"];
                    }
                }
            }

            /// <summary>
            /// Gets the name specified by the id from the look up dictionary. If the id
            /// is not in the dictionary it returns an empty string.
            /// </summary>
            /// <param name="lookup">A set of id's and their names.</param>
            /// <param name="sId">The string id to lookup.</param>
            /// <returns>The name associated with the id or empty string if not found.</returns>
            private string GetSafeNameByIdFrom(Dictionary<Guid, string> lookup, string sId)
            {
                Guid id;

                if (!Guid.TryParse(sId, out id))
                {
                    return string.Empty;
                }

                string name;

                if (!lookup.TryGetValue(id, out name))
                {
                    return string.Empty;
                }

                return name;
            }
        }

        public List<LendersOffice.Rolodex.RolodexDB> GetAffiliates()
        {
            return LendersOffice.Rolodex.RolodexDB.GetPmlBrokerAffiliates(BrokerId, PmlBrokerId);
        }
    }
}
 