﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Admin
{
    /// <summary>
    /// We sometimes need to map employees to users in bulk, and we don't
    /// always want to add the user id mapping to the payload.  The best
    /// approach is to load the mapping separately from the fast-viewing
    /// views we created.
    /// </summary>

    public class BrokerEmployeeMapping
    {
        /// <summary>
        /// We sometimes need to map employees to users in bulk, and we
        /// don't always want to add the user id mapping to the payload.
        /// The best approach is to load the mapping separately from the
        /// fast-viewing views we created.
        /// </summary>

        private Hashtable m_Table = new Hashtable();

        /// <summary>
        /// Keep mapping relationship in one place for quick lookup
        /// and matching.
        /// </summary>

        public class Spec
        {
            /// <summary>
            /// Keep mapping relationship in one place for quick
            /// lookup and matching.
            /// </summary>

            private Guid m_EmployeeId = Guid.Empty;
            private Guid m_UserId = Guid.Empty;
            private string m_Type = String.Empty;
            private bool m_CanLogin = false;
            private bool m_IsActive = false;

            #region ( Spec properties )

            public bool CanLogin
            {
                set { m_CanLogin = value; }
                get { return m_CanLogin; }
            }

            public bool IsActive
            {
                set { m_IsActive = value; }
                get { return m_IsActive; }
            }

            public string Type
            {
                set { m_Type = value; }
                get { return m_Type; }
            }

            public Guid EmployeeId
            {
                set { m_EmployeeId = value; }
                get { return m_EmployeeId; }
            }

            public Guid UserId
            {
                set { m_UserId = value; }
                get { return m_UserId; }
            }

            #endregion

        }

        public Spec this[Guid employeeId]
        {
            // Access member.

            get
            {
                return m_Table[employeeId] as Spec;
            }
        }

        /// <summary>
        /// Load the broker's employees (all of them) and map the employee
        /// ids to the user state.
        /// </summary>
        /// <param name="brokerId"></param>

        public void Retrieve(Guid brokerId)
        {
            // Load the broker's employees (all of them) and map the
            // employee ids to the user state.

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeMappingByBrokerId", parameters))
            {
                while (sR.Read() == true)
                {
                    Spec eS = new Spec();

                    eS.EmployeeId = (Guid)sR["EmployeeId"];
                    
                    if (sR["IsActive"] is DBNull == false)
                    {
                        eS.IsActive = Convert.ToBoolean(sR["IsActive"]);
                    }

                    if (sR["CanLogin"] is DBNull == false)
                    {
                        eS.CanLogin = Convert.ToBoolean(sR["CanLogin"]);
                    }

                    if (sR["Type"] is DBNull == false)
                    {
                        eS.Type = (String)sR["Type"];
                    }

                    if (sR["UserId"] is DBNull == false)
                    {
                        eS.UserId = (Guid)sR["UserId"];
                    }

                    m_Table.Add(eS.EmployeeId, eS);
                }
            }
        }

    }
}
