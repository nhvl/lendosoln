﻿namespace LendersOffice.Admin
{
    /// <summary>
    /// Indicates which branch groups are selected to access a certain feature.
    /// </summary>
    public enum BranchGroupSelection
    {
        /// <summary>
        /// No branch groups are selected.
        /// </summary>
        None = 0,

        /// <summary>
        /// All branch groups are selected.
        /// </summary>
        All = 1,

        /// <summary>
        /// Specific branch groups are selected.
        /// </summary>
        /// <remarks>The selected branch groups will have to be managed in some fashion.</remarks>
        OnlySelected = 2,
    }
}
