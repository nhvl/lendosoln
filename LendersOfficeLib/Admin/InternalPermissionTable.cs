﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class InternalPermissionTable : BrokerUserPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            // Throw out what a non-developer can edit.
            return pDesc.Code == LendersOffice.Security.Permission.AllowLoadingDeletedLoans;
        }

        #endregion

    }
}
