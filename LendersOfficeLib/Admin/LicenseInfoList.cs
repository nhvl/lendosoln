﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Contains Lending License information - License #, State, Expiration Date
    /// </summary>
    /// 
    [Serializable]
    [XmlRoot]
    public class LicenseInfoList
    {
        private ArrayList m_List = new ArrayList();
        public const int MAX_LICENSES = 100;

        #region ( List Properties )

        [XmlArray]
        [XmlArrayItem(typeof(LicenseInfo))]
        public ArrayList List
        {
            set { m_List = value; }
            get { return m_List; }
        }

        [XmlIgnore]
        public Int32 Count
        {
            get { return m_List.Count; }
        }

        [XmlIgnore]
        public Int32 MaxLicenses
        {
            get { return MAX_LICENSES; }
        }

        #endregion

        /// <summary>
        /// Checks if the this license list has changed from the passed in license list. 
        /// </summary>
        /// <param name="oldLicenseList">The old list to compare against.</param>
        /// <returns>Returns true if this list is different from the old. False otherwise.</returns>
        public bool HashChanged(LicenseInfoList oldLicenseList)
        {
            if (oldLicenseList == null)
            {
                return false;
            }

            Dictionary<string, LicenseInfo> newLicenses = new Dictionary<string, LicenseInfo>(StringComparer.OrdinalIgnoreCase);
            foreach (LicenseInfo newLicenseInfo in this.List)
            {
                // License num + state should be a unqiue key.
                newLicenses.Add($"{newLicenseInfo.License.TrimWhitespaceAndBOM()}_{newLicenseInfo.State.TrimWhitespaceAndBOM()}", newLicenseInfo);
            }

            Dictionary<string, LicenseInfo> oldLicenses = new Dictionary<string, LicenseInfo>(StringComparer.OrdinalIgnoreCase);
            foreach (LicenseInfo oldLicenseInfo in oldLicenseList.List)
            {
                oldLicenses.Add($"{oldLicenseInfo.License.TrimWhitespaceAndBOM()}_{oldLicenseInfo.State.TrimWhitespaceAndBOM()}", oldLicenseInfo);
            }

            foreach (var newLicensePair in newLicenses)
            {
                LicenseInfo newLicense = newLicensePair.Value;
                LicenseInfo oldLicense;
                if (!oldLicenses.TryGetValue(newLicensePair.Key, out oldLicense))
                {
                    // The old license list doesn't have this license, which means it's a new license.
                    return false;
                }
                else
                {
                    if (!newLicense.Equals(oldLicense))
                    {
                        return false;
                    }
                }
            }

            // At this point, we know that 
            //  - There are no added licenses
            //  - All licenses in the new license list exist in the old license list and haven't changed.
            // Checking for count here will check for licenses that have been removed from the old license list.
            if (newLicenses.Count != oldLicenses.Count)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Append to the end.  We remove existing entries.
        /// Specialize this implementation to filter adds.
        /// </summary>
        public virtual void Add(LicenseInfo lInfo)
        {
            // Append to the end.
            if (lInfo == null)
                throw new ArgumentException("Invalid license info item - License info being added is null.");

            if (LicenseInfoList.IsValid(lInfo))
            {
                m_List.Add(lInfo);
            }
            else
            {
                throw new ArgumentException("Unable to save invalid license.");
            }
        }

        public virtual void Add(LicenseInfoList lInfoList)
        {
            // Append to the end.
            if (lInfoList == null)
                throw new ArgumentException("Invalid license info list - License info list being added is null.");
            
            foreach (LicenseInfo lInfo in lInfoList)
            {
                this.Add(lInfo);
            }
        }

        public bool IsFull
        {
            get
            {
                return (m_List.Count >= MAX_LICENSES);
            }
        }

        public bool Exists(string licenseNum)
        {
            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()))
                    return true;
            }
            return false;
        }

        public LicenseInfo Exists(string licenseNum, string state)
        {
            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()) && lInfo.State.Equals(state.TrimWhitespaceAndBOM()))
                {
                    return lInfo;
                }
            }
            return null;
        }

        public bool UpdateExpirationDate(string licenseNum, string state, string expirDate)
        {
            foreach (LicenseInfo lInfo in m_List)
            {
                // These two checks guarantee uniqueness
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()) && lInfo.State.Equals(state.TrimWhitespaceAndBOM()))
                {
                    lInfo.ExpD = expirDate;
                    return true;
                }
            }
            return false;
        }

        public void UpdateLicense(string licenseNum, string state, string expDate)
        {
            if ((null == licenseNum) || (licenseNum.TrimWhitespaceAndBOM().Equals("")))
                throw new ArgumentException("License number cannot be empty");

            if (IsDuplicateLicenseNum(licenseNum))
                throw new CBaseException("Cannot update lending license because there are multiple licenses with the same license number", "Cannot update lending license because there are multiple licenses with the same license number");

            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()))
                {
                    if (null != state)
                        lInfo.State = state.ToUpper();
                    if (null != expDate)
                        lInfo.ExpD = expDate;
                    break;
                }
            }
        }

        public void UpdateLicense(string originalLicenseNum, string originalState, string licenseNum, string state, string expDate, string DisplayName, string Street, string City, string AddrState, string Zip, string Phone, string Fax, string Editable)
        {
            if ((null == licenseNum) || (licenseNum.TrimWhitespaceAndBOM().Equals("")))
                throw new ArgumentException("License number cannot be empty");

            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(originalLicenseNum.TrimWhitespaceAndBOM()) && lInfo.State.Equals(originalState))
                {
                    if (null != licenseNum)
                        lInfo.License = licenseNum;
                    if (null != state)
                        lInfo.State = state;
                    if (null != expDate)
                        lInfo.ExpD = expDate;
                    if (null != DisplayName)
                        lInfo.DisplayName = DisplayName;
                    if (null != Street)
                        lInfo.Street = Street;
                    if (null != City)
                        lInfo.City = City;
                    if (null != AddrState)
                        lInfo.AddrState = AddrState;
                    if (null != Zip)
                        lInfo.Zip = Zip;
                    if (null != Phone)
                        lInfo.Phone = Phone;
                    if (null != Fax)
                        lInfo.Fax = Fax;
                    if (null != Editable)
                        lInfo.Editable = bool.Parse(Editable);
                    break;
                }
            }
        }

        public bool IsDuplicateLicenseNum(string licenseNum)
        {
            int nCount = 0;
            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()))
                    ++nCount;
            }
            return (nCount > 1);
        }

        public bool IsDuplicateLicense(LicenseInfo license)
        {
            if (license == null)
                return false;

            foreach (LicenseInfo lInfo in m_List)
            {
                if (license.License.Equals(lInfo.License) && license.State.Equals(lInfo.State))
                    return true;
            }
            return false;
        }

        public static bool IsValid(LicenseInfo license)
        {
            if (license == null)
                return false;

            if (license.License == null || license.License.Length == 0)
                return false;

            if (license.State == null || !LicenseInfoList.IsValidState(license.State))
                return false;

            if (license.ExpD == null || license.ExpD.Length == 0)
                return false;

            try
            {
                DateTime.Parse(license.ExpD);
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }

        public void DeleteLicense(string licenseNum)
        {
            if ((null == licenseNum) || (licenseNum.TrimWhitespaceAndBOM().Equals("")))
                throw new ArgumentException("License number cannot be empty");

            if (IsDuplicateLicenseNum(licenseNum))
                throw new CBaseException("Cannot delete lending license because there are multiple licenses with the same license number", "Cannot delete lending license because there are multiple licenses with the same license number");

            foreach (LicenseInfo lInfo in m_List)
            {
                if (lInfo.License.Equals(licenseNum.TrimWhitespaceAndBOM()))
                {
                    m_List.Remove(lInfo);
                    break;
                }
            }
        }


        /// <summary>
        /// Load the license info from the xml document.
        /// </summary>

        public void Retrieve(String sXml)
        {
            // Translate document and snatch up the rules
            // from the temp copy.

            if (sXml.TrimWhitespaceAndBOM() != String.Empty)
                m_List = ToObject(sXml).m_List;
            else
                m_List.Clear();
        }

        /// <summary>
        /// Return looping interface pointer for walking.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return looping interface pointer for
            // walking.
            return m_List.GetEnumerator();
        }

        /// <summary>
        /// Reset the set and clear out all entries.
        /// </summary>

        public void Clear()
        {
            m_List.Clear();
        }


        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our set.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>
        public static LicenseInfoList ToObject(String sXml)
        {
            // Deserialize the given string to our list.
            if (sXml.TrimWhitespaceAndBOM().Equals(""))
                return new LicenseInfoList();

            //If the xml data doesn't have a header, give it one (for saving space in the database)
            if (sXml.IndexOf("<?xml") < 0)
            {
                string header = "<?xml version='1.0' encoding='utf-8'?>  <LicenseInfoList xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>";
                sXml = sXml.Insert(0, header);
            }

            return SerializationHelper.XmlDeserialize(sXml, typeof(LicenseInfoList)) as LicenseInfoList;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            // Serialize this instance as an xml document.

            //If there aren't any elements to serialize, don't store any xml tags in the DB as this takes up space
            if (this.Count == 0)
                return "";

            string serialized = SerializationHelper.XmlSerialize(this);

            //save space in the database by not storing the header (it will get added back upon deserialization)
            int index = serialized.IndexOf("<List>");
            serialized = serialized.Substring(index);

            if (serialized.Length > 200000)
            {
                throw new CBaseException("The length of the lending license list has exceeded the maximum allowable length", "The length of the lending license list has exceeded the maximum allowable length");
            }

            return serialized;
        }

        public string ToCsv()
        {
            StringBuilder sb = new StringBuilder();

            foreach (LicenseInfo lInfo in m_List)
            {
                if (!(lInfo.License.Equals("")))
                {
                    sb.Append(lInfo.License);

                    if (!(lInfo.State.Equals("")))
                    {
                        sb.Append(" -State: " + lInfo.State);
                    }
                    if (!(lInfo.ExpD.Equals("")))
                    {
                        sb.Append(" -Expires: " + lInfo.ExpD);
                    }

                    sb.Append(" + ");
                }
            }

            // OPM 242084 - Handle case where sb is empty (ie. don't try to remove final " + ").
            if(sb.Length == 0)
            {
                return string.Empty;
            }

            return sb.ToString(0, sb.Length - 3); // Length - 3 removes the final " + "
        }

        public static bool IsValidState(string licState)
        {
            string[] states = {  "" , "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL",
                                "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD",
                                "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ",
                                "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN",
                                "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"};
            
            if (licState == null)
                return false;

            if (licState.Length == 0)
                return true;

            foreach (string validState in states)
            {
                if (licState == validState)
                    return true;
            }
            return false;
        }

        public static string JsonSerialize(LicenseInfoList LicenseInfoList)
        {
            List<List<string>> stringList = new List<List<string>>();
            if (LicenseInfoList != null)

            {
                foreach (LicenseInfo o in LicenseInfoList)
                {
                    stringList.Add(new List<string>() { o.License, o.State, o.ExpD, o.DisplayName, o.Street, o.City, o.AddrState, o.Zip, o.Phone, o.Fax, o.Editable.ToString() });
                }
            }

            return ObsoleteSerializationHelper.JsonSerialize(stringList);
        }


        public static LicenseInfoList JsonDeserialize(string json)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            LicenseInfoList LicenseInfoList = new LicenseInfoList();
            if (string.IsNullOrEmpty(json))
                return LicenseInfoList;

            List<List<string>> stringList = ObsoleteSerializationHelper.JsonDeserialize<List<List<string>>>(json);

            if (stringList != null)
            {
                foreach (List<string> lendingLicense in stringList)
                {
                    if (lendingLicense[0].TrimWhitespaceAndBOM().Length == 0) // Empty licenses are not allowed. Proceed to the next entry.
                    {
                        string msg = "User '" + principal.LastName + ", " + principal.FirstName + "' (userid: " + principal.UserId + ") " +
                            "from broker '" + principal.BrokerName + "' (brokerid: " + principal.BrokerId + ") " +
                            "tried to add a license with an empty number.";
                        Tools.LogWarning(msg);
                        continue;
                    }

                    bool editable = false;
                    if (!bool.TryParse(lendingLicense[10], out editable))
                    {
                        editable = false;
                    }
                    LicenseInfo licenseInfo = new LicenseInfo(lendingLicense[1], lendingLicense[2], lendingLicense[0], lendingLicense[3], lendingLicense[4], lendingLicense[5], lendingLicense[6], lendingLicense[7], lendingLicense[8], lendingLicense[9], editable);
                    if (licenseInfo.State == "--")
                        licenseInfo.State = "";
                    if (LicenseInfoList.IsValid(licenseInfo) && !LicenseInfoList.IsDuplicateLicense(licenseInfo) && !LicenseInfoList.IsFull)
                    {
                        LicenseInfoList.Add(licenseInfo);
                    }
                    else
                    {
                        string msg = "User '" + principal.LastName + ", " + principal.FirstName + "' (userid: " + principal.UserId + ") from broker '" + principal.BrokerName + "' (brokerid: " + principal.BrokerId + ") could not add the following license: '" + licenseInfo.License + "' | '" + licenseInfo.State + "' | '" + licenseInfo.ExpD + "'. ";
                        if (LicenseInfoList.IsFull)
                            msg += "The max number of licenses has been reached.";
                        else if (!LicenseInfoList.IsValid(licenseInfo))
                            msg += "Invalid license.";
                        else
                            msg += "An existing license with the same id was found.";
                        Tools.LogWarning(msg);
                    }
                }
            }

            return LicenseInfoList;
        }

        #endregion
    }
}
