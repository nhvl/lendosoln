﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Keep track of all employees loaded for this broker, each mapping
    /// to its relationships.
    /// </summary>

    public class BrokerEmployeeRelationships
    {
        /// <summary>
        /// Keep track of all employees loaded for this broker, each
        /// mapping to its relationships.
        /// </summary>

        private Hashtable m_Table = new Hashtable();

        public EmployeeRelationships this[Guid employeeId]
        {
            // Access member.

            get
            {
                return m_Table[employeeId] as EmployeeRelationships;
            }
        }

        /// <summary>
        /// Load the employee permissions of all employees within this
        /// broker.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            // Load the employee permissions of all employees within
            // this broker.
            SqlParameter[] parameters = {
                                             new SqlParameter("@BrokerId", brokerId)
                                        };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeRelationshipsByBrokerId",parameters))
            {
                while (sR.Read() == true)
                {
                    EmployeeRelationships eR = new EmployeeRelationships();

                    eR.Record = sR;

                    m_Table.Add(eR.EmployeeId, eR);
                }
            }
        }

        /// <summary>
        /// Return the looping interface for added entries.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return the looping interface for added
            // entries.

            return m_Table.Keys.GetEnumerator();
        }

        /// <summary>
        /// Erase all records to make room for reloading
        /// new entries.
        /// </summary>

        public void Clear()
        {
            // Remove the entries from our tables.

            m_Table.Clear();
        }

    }
}
