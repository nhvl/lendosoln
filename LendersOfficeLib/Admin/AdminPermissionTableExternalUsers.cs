﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableExternalUsers : AdminPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            //OPM 9721
            switch (pDesc.Code)
            {
                case Permission.AllowTpoPortalConfiguring:
                case Permission.CanAdministrateExternalUsers:
                case Permission.AllowEditingOriginatingCompanyTiers:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
