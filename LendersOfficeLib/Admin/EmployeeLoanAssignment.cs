﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;

namespace LendersOffice.Admin
{

    /// <summary>
    /// Keep track of individual loan assignments including the role details.
    /// </summary>

    public class EmployeeLoanAssignment
    {
        private EmployeeLoanAssignment(DbDataReader reader)
        {
            Init(reader);
        }

        /// <summary>
        /// Return a list of assignment for a given loan.
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        public static IEnumerable<EmployeeLoanAssignment> ListEmployeeAssignmentByLoan(Guid brokerId, Guid sLId)
        {
            List<EmployeeLoanAssignment> list = new List<EmployeeLoanAssignment>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", sLId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoanAssignmentsByLoanId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new EmployeeLoanAssignment(reader));
                }
            }

            return list;
        }

        private void Init(DbDataReader reader)
        {
            Guid roleId = GetGuid(reader["RoleId"]);

            this.Role = Role.Get(roleId);

            EmployeeId = GetGuid(reader["EmployeeId"]);
            UserId = GetGuid(reader["UserId"]);
            PmlBrokerId = GetGuid(reader["PmlBrokerId"]);
            BranchID = GetGuid(reader["BranchId"]);

            FirstName = GetString(reader["UserFirstNm"]);
            LastName = GetString(reader["UserLastNm"]);
            Email = GetString(reader["Email"]);
        }

        #region ( Loan assignment properties )

        public Role Role { get; private set; }

        public E_RoleT RoleT
        {
            get { return Role.RoleT; }
        }

        public string RoleModifiableDesc { get { return Role.ModifiableDesc; } }
        public int RoleImportanceRank { get { return Role.ImportanceRank; } }
        public string RoleDesc { get { return Role.Desc; } }
        public Guid RoleId { get { return Role.Id; } }
        public Guid UserId { get; private set; }
        public Guid EmployeeId { get; private set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
        public string EmployeeName
        {
            get { return LastName + " , " + FirstName; }
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }

        public Guid PmlBrokerId { get; private set; }
        public Guid BranchID { get; private set; }

        public bool IsPmlUser
        {
            get { return PmlBrokerId != Guid.Empty; }
        }
        #endregion

        private string GetString(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (string)o;
            }
            else
            {
                return string.Empty;
            }
        }
        private Guid GetGuid(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (Guid)o;
            }
            else
            {
                return Guid.Empty;
            }
        }

    }
}
