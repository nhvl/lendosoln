﻿using System;
using LendersOffice.Constants;

namespace LendersOffice.Admin
{
    // 12/20/06 mf. OPM 8833. Created this class to allow for
    // easier rule parsing.
    // Should become obsoleted by the new notification framework.

    /// <summary>
    /// Basic helper class for reading Notification Rules.
    /// </summary>

    public class EventNotificationRulesView
    {
        public Guid BrokerId { get; private set; }
        private NotificationRules m_rules;

        public string BccAddress
        {
            get
            {
                string ret = string.Empty;
                foreach (NotificationRule rule in m_rules)
                {
                    if (rule.EventLabel == "Event")
                    {
                        if (rule.CarbonCopyTo != string.Empty)
                        {
                            ret = rule.CarbonCopyTo;
                            break;
                        }
                    }
                }
                return ret;
            }
        }

        public string DefaultReplyToAddress
        {
            get
            {
                string ret = string.Empty;
                foreach (NotificationRule rule in m_rules)
                {
                    if (rule.EventLabel == "Event")
                    {
                        if (rule.DefaultReply != string.Empty)
                        {
                            ret = rule.DefaultReply;
                            break;
                        }
                    }
                }
                return ret;
            }
        }

        public string DefaultReplyToLabel
        {
            get
            {
                string ret = string.Empty;
                foreach (NotificationRule rule in m_rules)
                {
                    if (rule.EventLabel == "Event")
                    {
                        if (rule.DefaultLabel != string.Empty)
                        {
                            ret = rule.DefaultLabel;
                            break;
                        }
                    }
                }
                return ret;
            }
        }

        public string DefaultReplyToString
        {
            get
            {
                string ret = ConstStage.DefaultDoNotReplyAddress; // OPM 42723
                foreach (NotificationRule rule in m_rules)
                {
                    if (rule.EventLabel == "Event")
                    {
                        if (rule.DefaultReply != string.Empty)
                        {
                            if (rule.DefaultLabel != string.Empty)
                            {
                                ret = string.Format("\"{0}\" <{1}>", rule.DefaultLabel, rule.DefaultReply);
                            }
                            else
                            {
                                ret = string.Format("{0}", rule.DefaultReply);
                            }
                            break;
                        }
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// Returns the reply to address string based on the loan assignments.
        /// </summary>
        public string GetFromAddressString(Guid LoanId)
        {
            string fromAddress = ConstStage.DefaultDoNotReplyAddress; // OPM 42723

            LoanAssignmentContactTable rList = new LoanAssignmentContactTable(BrokerId, LoanId);

            foreach (NotificationRule rule in m_rules)
            {
                // Get the current event's reply-to configuration.

                if (rule.EventLabel == "Event")
                {
                    if (rule.DoNotReply == false)
                    {
                        // Look up the role preferences.  If not found,
                        // then use the default.

                        Boolean isSet = false;

                        foreach (String rDesc in rule.ReplyToList)
                        {
                            EmployeeLoanAssignment eA = rList.FindByRoleDesc(rDesc);

                            if (null != eA && "" != eA.Email)
                            {
                                if (eA.FullName != "")
                                {
                                    fromAddress = string.Format("\"{0}\" <{1}>", eA.FullName, eA.Email);
                                }
                                else
                                {
                                    fromAddress = string.Format("{0}", eA.Email);
                                }

                                isSet = true;

                                break;
                            }
                        }

                        if (isSet == false && rule.DefaultReply != "")
                        {
                            if (rule.DefaultLabel != "")
                            {
                                fromAddress = string.Format("\"{0}\" <{1}>", rule.DefaultLabel, rule.DefaultReply);
                            }
                            else
                            {
                                fromAddress = string.Format("{0}", rule.DefaultReply);
                            }
                        }
                    }

                    break;
                }
            }

            return fromAddress;
        }

        public EventNotificationRulesView(Guid brokerId)
        {
            this.BrokerId = brokerId;

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            m_rules = brokerDB.NotifRules;
        }
    }
}
