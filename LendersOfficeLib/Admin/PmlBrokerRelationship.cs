﻿/// <copyright file="PmlBrokerRelationship.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   2/18/2016
/// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Runtime.Serialization;
    using DataAccess;
    using Security;

    /// <summary>
    /// Provides a container for the name and id of an employee
    /// present in a relationship mapping for a <see cref="PmlBroker"/>.
    /// </summary>
    [DataContract]
    public struct PmlBrokerRelationshipEmployeeInfo
    {
        /// <summary>
        /// Represents an empty <see cref="PmlBrokerRelationshipEmployeeInfo"/> record with
        /// an <see cref="Id"/> of <see cref="Guid.Empty"/> and a <see cref="Name"/> of
        /// "None".
        /// </summary>
        public static readonly PmlBrokerRelationshipEmployeeInfo Empty = new PmlBrokerRelationshipEmployeeInfo(
            Guid.Empty,
            "None");

        /// <summary>
        /// Initializes a new instance of the <see cref="PmlBrokerRelationshipEmployeeInfo"/>
        /// struct with the specified <paramref name="employeeId"/> and <paramref name="employeeName"/>.
        /// </summary>
        /// <param name="employeeId">
        /// The id of the employee.
        /// </param>
        /// <param name="employeeName">
        /// The name of the employee.
        /// </param>
        public PmlBrokerRelationshipEmployeeInfo(Guid employeeId, string employeeName) : this()
        {
            this.Id = employeeId;
            this.Name = employeeName;
        }

        /// <summary>
        /// Gets the value for the id of the employee.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> of the employee.
        /// </value>
        [DataMember]
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets the value for the name of the employee.
        /// </summary>
        /// <value>
        /// The string name of the employee.
        /// </value>
        [DataMember]
        public string Name { get; private set; }
    }

    /// <summary>
    /// Provides a container for relationship mappings between
    /// employee roles and employees for a <see cref="PmlBroker"/>.
    /// </summary>
    public class PmlBrokerRelationship
    {
        /// <summary>
        /// Contains the relationship mappings between employee roles and 
        /// employees for a <see cref="PmlBroker"/>.
        /// </summary>
        private readonly Dictionary<E_RoleT, PmlBrokerRelationshipEmployeeInfo> relationships;

        /// <summary>
        /// Initializes a new instance of the <see cref="PmlBrokerRelationship"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the OC's broker.
        /// </param>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> of the OC.
        /// </param>
        /// <param name="role">
        /// The <see cref="E_OCRoles"/> value corresponding
        /// to the role for the OC.
        /// </param>
        public PmlBrokerRelationship(Guid brokerId, Guid pmlBrokerId, E_OCRoles role)
        {
            this.BrokerId = brokerId;
            this.PmlBrokerId = pmlBrokerId;
            this.OCRoleT = role;

            this.relationships = new Dictionary<E_RoleT, PmlBrokerRelationshipEmployeeInfo>()
            {
                { E_RoleT.Manager, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.Processor, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.JuniorProcessor, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.LenderAccountExecutive, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.Underwriter, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.JuniorUnderwriter, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.CreditAuditor, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.LegalAuditor, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.LockDesk, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.Purchaser, PmlBrokerRelationshipEmployeeInfo.Empty },
                { E_RoleT.Secondary, PmlBrokerRelationshipEmployeeInfo.Empty }
            };
        }

        /// <summary>
        /// Gets the value for the ID of the OC's broker.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> for the OC's broker.
        /// </value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the value for the ID of the OC.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> of the OC.
        /// </value>
        public Guid PmlBrokerId { get; private set; }

        /// <summary>
        /// Gets the value for the role of the OC.
        /// </summary>
        /// <value>
        /// The <see cref="E_OCRoles"/> for the OC.
        /// </value>
        public E_OCRoles OCRoleT { get; private set; }

        /// <summary>
        /// Gets the relationship info.
        /// </summary>
        /// <value>The relationship info.</value>
        public IReadOnlyDictionary<E_RoleT, PmlBrokerRelationshipEmployeeInfo> Relationships
        {
            get
            {
                return this.relationships;
            }
        }

        /// <summary>
        /// Obtains the relationship employee information for the specified 
        /// <paramref name="role"/>.
        /// </summary>
        /// <param name="role">
        /// The <see cref="E_RoleT"/> key for the associated mapping.
        /// </param>
        /// <returns>
        /// The <see cref="PmlBrokerRelationshipEmployeeInfo"/> corresponding 
        /// to the specified <paramref name="role"/> or 
        /// <see cref="PmlBrokerRelationshipEmployeeInfo.Empty"/> if the role
        /// has not been set at the oc level.
        /// </returns>
        public PmlBrokerRelationshipEmployeeInfo this[E_RoleT role]
        {
            get
            {
                if (this.relationships.ContainsKey(role))
                {
                    return this.relationships[role];
                }

                return PmlBrokerRelationshipEmployeeInfo.Empty;
            }
        }

        /// <summary>
        /// Retrieves PML broker relationship information for the specified <paramref name="role"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the OC's broker.
        /// </param>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> of the OC.
        /// </param>
        /// <param name="role">
        /// The role used to load relationship information.
        /// </param>
        /// <param name="parser">
        /// The parser for relationship data.
        /// </param>
        /// <returns>
        /// A <see cref="PmlBrokerRelationship"/> object containing relationship information.
        /// </returns>
        public static PmlBrokerRelationship Retrieve(Guid brokerId, Guid pmlBrokerId, E_OCRoles role, PmlBrokerRelationshipPermissionDataParser parser)
        {
            var pmlBrokerRelationship = new PmlBrokerRelationship(brokerId, pmlBrokerId, role);

            foreach (var relationship in parser.GetRelationships(role))
            {
                var employee = new PmlBrokerRelationshipEmployeeInfo(
                    relationship.EmployeeId,
                    relationship.EmployeeName);

                pmlBrokerRelationship.relationships[relationship.EmployeeRole] = employee;
            }

            return pmlBrokerRelationship;
        }

        /// <summary>
        /// Checks if this role is a valid PML broker relationship role.
        /// </summary>
        /// <param name="role">The role to check.</param>
        /// <returns>True if it is valid, false otherwise.</returns>
        public bool IsValidPmlBrokerRelationshipRole(E_RoleT role)
        {
            return this.relationships.ContainsKey(role);
        }

        /// <summary>
        /// Sets a relationship mapping between the specified <paramref name="role"/>
        /// and the employee with the specified <paramref name="employeeId"/> and
        /// <paramref name="employeeName"/>.
        /// </summary>
        /// <param name="role">
        /// The <see cref="E_RoleT"/> for the mapping.
        /// </param>
        /// <param name="employeeId">
        /// The ID of the employee for the mapping.
        /// </param>
        /// <param name="employeeName">
        /// The name of the employee for the mapping.
        /// </param>
        /// <exception cref="ArgumentException">
        /// The supplied <paramref name="employeeName"/> is null or, if the supplied
        /// <paramref name="employeeId"/> is not <see cref="Guid.Empty"/>, the supplied
        /// <paramref name="employeeName"/> is the empty string or whitespace.
        /// </exception>
        public void SetRelationship(E_RoleT role, Guid employeeId, string employeeName)
        {
            if (employeeId != Guid.Empty && string.IsNullOrWhiteSpace(employeeName))
            {
                var msg = "The name of an employee in a relationship cannot be null, the empty string, " +
                    "or whitespace when a non-empty employee ID is specified.";

                throw new ArgumentException(msg, "employeeName");
            }

            this.relationships[role] = new PmlBrokerRelationshipEmployeeInfo(
                employeeId,
                employeeName);
        }

        /// <summary>
        /// Saves the relationship information to the database.
        /// </summary>
        public void Save()
        {
            using (var executor = new CStoredProcedureExec(this.BrokerId))
            {
                executor.BeginTransactionForWrite();

                try
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@BrokerId", this.BrokerId),
                        new SqlParameter("@PmlBrokerId", this.PmlBrokerId),
                        new SqlParameter("@OCRoleT", this.OCRoleT)
                    };

                    executor.ExecuteNonQuery("PMLBROKER_ClearRelationships", parameters);

                    foreach (var employeeKVP in this.relationships)
                    {
                        if (employeeKVP.Value.Id == Guid.Empty)
                        {
                            continue;
                        }

                        parameters = new SqlParameter[]
                        {
                            new SqlParameter("@BrokerId", this.BrokerId),
                            new SqlParameter("@PmlBrokerId", this.PmlBrokerId),
                            new SqlParameter("@OCRoleT", this.OCRoleT),
                            new SqlParameter("@EmployeeRoleT", employeeKVP.Key),
                            new SqlParameter("@EmployeeId", employeeKVP.Value.Id),
                            new SqlParameter("@EmployeeName", employeeKVP.Value.Name)
                        };

                        executor.ExecuteNonQuery("PMLBROKER_SaveRelationship", parameters);
                    }

                    parameters = new SqlParameter[]
                    {
                        new SqlParameter("@PmlBrokerId", this.PmlBrokerId),
                        new SqlParameter("@OCRoleT", this.OCRoleT),
                        new SqlParameter("@ManagerId", this.GetUpdateRelationshipValue(E_RoleT.Manager)),
                        new SqlParameter("@ProcessorId", this.GetUpdateRelationshipValue(E_RoleT.Processor)),
                        new SqlParameter("@JuniorProcessor", this.GetUpdateRelationshipValue(E_RoleT.JuniorProcessor)),
                        new SqlParameter("@LenderAccountExecId", this.GetUpdateRelationshipValue(E_RoleT.LenderAccountExecutive)),
                        new SqlParameter("@UnderwriterId", this.GetUpdateRelationshipValue(E_RoleT.Underwriter)),
                        new SqlParameter("@JuniorUnderwriterId", this.GetUpdateRelationshipValue(E_RoleT.JuniorUnderwriter)),
                        new SqlParameter("@LockDeskId", this.GetUpdateRelationshipValue(E_RoleT.LockDesk)),
                        new SqlParameter("@CreditAuditorId", this.GetUpdateRelationshipValue(E_RoleT.CreditAuditor)),
                        new SqlParameter("@LegalAuditorId", this.GetUpdateRelationshipValue(E_RoleT.LegalAuditor)),
                        new SqlParameter("@PurchaserId", this.GetUpdateRelationshipValue(E_RoleT.Purchaser)),
                        new SqlParameter("@SecondaryId", this.GetUpdateRelationshipValue(E_RoleT.Secondary))
                    };

                    executor.ExecuteNonQuery("PMLBROKER_UpdateUserRelationships", parameters);

                    executor.CommitTransaction();
                }
                catch
                {
                    executor.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Obtains the relationship value for the corresponding 
        /// <paramref name="role"/> or <see cref="DBNull.Value"/> if the relationship
        /// is not defined.
        /// </summary>
        /// <param name="role">
        /// The <see cref="E_RoleT"/> to obtain a relationship
        /// value.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/> for the relationship or <see cref="DBNull.Value"/>
        /// if the relationship has not been defined.
        /// </returns>
        private object GetUpdateRelationshipValue(E_RoleT role)
        {
            if (this.relationships.ContainsKey(role) && 
                this.relationships[role].Id != Guid.Empty)
            {
                return this.relationships[role].Id;
            }

            return DBNull.Value;
        }
    }
}
