﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    /// <summary>
    /// These team types reflect the use of different relationships.
    /// For example, a team-member should be assigned to all loans
    /// created by the relating employee.  A team-helper is assigned
    /// after a specific event occurs in the lifetime of a loan.
    /// </summary>

    public enum E_RelationshipType
    {
        Invalid = 0,
        TeamMember = 1,
        TeamHelper = 2,
        Supervisor = 3,

    }
}
