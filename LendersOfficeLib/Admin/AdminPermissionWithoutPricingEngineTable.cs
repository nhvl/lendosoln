﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class AdminPermissionWithoutPricingEngineTable : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanRunPricingEngineWithoutCreditReport:
                case Permission.CanSubmitWithoutCreditReport:
                case Permission.CanApplyForIneligibleLoanPrograms:
                    {
                        return false;
                    }
            }

            return base.IsIncluded(pDesc);
        }

        #endregion

    }
}
