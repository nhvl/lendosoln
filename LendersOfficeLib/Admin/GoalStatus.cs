﻿namespace LendersOffice.Admin
{
    /// <summary>
    /// Enum representation of the goal. 
    /// Will potentially add "In progress" in the future.
    /// </summary>
    public enum GoalStatus
    {
        /// <summary>
        /// Implementation goal hasn't been met for the feature.
        /// </summary>
        NotMet = 0,

        /// <summary>
        /// Implementation goal has been met for the feature.
        /// </summary>
        Met = 1,
    }
}
