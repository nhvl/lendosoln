﻿namespace LendersOffice.Admin
{
    using System;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Broker options for the TPO loan editor Save button.
    /// </summary>
    public class TpoLoanEditorSaveButtonBrokerOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoLoanEditorSaveButtonBrokerOptions"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The regex match from the temp options.</param>
        public TpoLoanEditorSaveButtonBrokerOptions(Guid brokerId, Match match)
        {
            this.BrokerId = brokerId;

            if (match.Success)
            {
                this.IsEnabledForAll = false;
                this.EnabledEmployeeGroupName = match.Groups["ExceptForThisEmployeeGroup"].Value;
                this.EnabledOcGroupName = match.Groups["ExceptForThisOcGroup"].Value;
            }
            else
            {
                this.IsEnabledForAll = true;
                this.EnabledOcGroupName = string.Empty;
                this.EnabledEmployeeGroupName = string.Empty;
            }
        }

        /// <summary>
        /// Gets the ID of the broker these options are for.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets a value indicating whether the save button is enabled for all TPO users for this broker.
        /// </summary>
        public bool IsEnabledForAll { get; }

        /// <summary>
        /// Gets the name of an employee group the save button is enabled for even if it is not enabled for all users.
        /// </summary>
        public string EnabledEmployeeGroupName { get; }

        /// <summary>
        /// Gets the name of an OC group the save button is enabled for even if it is not enabled for all users.
        /// </summary>
        public string EnabledOcGroupName { get; }   
    }
}
