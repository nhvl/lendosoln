﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Get what's needed to determine origination branch, current
    /// employee assignments, and the current status.
    /// </summary>

    public class LoanAccessInfo
    {
        /// <summary>
        /// Get what's needed to determine origination branch,
        /// current employee assignments, and the current
        /// status.
        /// 
        /// 8/1/2005 kb - To extend this access info class, you
        /// need to add the protected variable, update the two
        /// db reader properties, and update the copy constructor.
        /// Typically, we extend this class when we include more
        /// details from the loan in the access control check.
        /// Don't forget to update the sproc that is called in
        /// the retrieve function.
        /// 
        /// 8/15/2005 kb - The best bet is to find all the
        /// classes the overload (inherit from) this class
        /// and update them.  Currently, loan reporting uses
        /// a specialized form of this class to simplify
        /// access control checking in-batch.
        /// </summary>

        protected Guid m_BrokerId = Guid.Empty;
        protected Guid m_BranchId = Guid.Empty;
        protected Guid m_LoanId = Guid.Empty;
        protected String m_LoanName = String.Empty;
        //protected String m_Borrower = String.Empty;
        protected E_sStatusT m_LoanStatus = E_sStatusT.Loan_Other;
        protected E_sLienPosT m_LienPosition = E_sLienPosT.First;
        //protected DateTime m_OpenedDate = DateTime.MinValue;
        protected decimal m_NoteIRSubmitted = decimal.MinusOne;
        protected Guid m_ExternalManagerId = Guid.Empty;
        protected bool m_IsTemplate = false;
        protected bool m_IsValid = false;
        protected Guid m_sPmlBrokerId = Guid.Empty;
        protected Guid m_PmlExternalBrokerProcessorManagerEmployeeId = Guid.Empty;
        protected Guid m_PmlExternalSecondaryManagerEmployeeId = Guid.Empty;
        protected Guid m_PmlExternalPostCloserManagerEmployeeId = Guid.Empty;

        protected E_sRateLockStatusT m_sRateLockStatusT = E_sRateLockStatusT.NotLocked;

        protected LoanAssignments m_Assignments = new LoanAssignments();

        #region ( Access control bits )

        protected IDataReader m_Record
        {
            set
            {
                m_BrokerId = value.GetSafeValue("sBrokerId", Guid.Empty);
                m_BranchId = value.GetSafeValue("sBranchId", Guid.Empty);
                m_LoanName = value.GetSafeValue("sLNm", string.Empty);
                //m_Borrower = value.GetSafeValue("sPrimBorrowerFullNm", string.Empty);
                //m_OpenedDate = value.GetSafeValue("sOpenedD", DateTime.MinValue);
                m_NoteIRSubmitted = value.GetSafeValue("sNoteIRSubmitted", decimal.MinusOne);

                m_ExternalManagerId = value.GetSafeValue("PmlExternalManagerEmployeeId", Guid.Empty);
                m_IsTemplate = value.GetSafeValue("IsTemplate", false);
                m_IsValid = value.GetSafeValue("IsValid", false);

                m_LoanStatus = value.GetSafeValue("sStatusT", E_sStatusT.Loan_Other);
                m_LienPosition = value.GetSafeValue("sLienPosT", E_sLienPosT.First);
                
                m_sRateLockStatusT = value.GetSafeValue("sRateLockStatusT", E_sRateLockStatusT.NotLocked);
                m_Assignments.m_Record = value;

                m_sPmlBrokerId = value.GetSafeValue("sPmlBrokerId", Guid.Empty);
                m_PmlExternalBrokerProcessorManagerEmployeeId = value.GetSafeValue("PmlExternalBrokerProcessorManagerEmployeeId", Guid.Empty);
                m_PmlExternalSecondaryManagerEmployeeId = value.GetSafeValue("PmlExternalSecondaryManagerEmployeeId", Guid.Empty);
                m_PmlExternalPostCloserManagerEmployeeId = value.GetSafeValue("PmlExternalPostCloserManagerEmployeeId", Guid.Empty);
            }
        }

        protected DataRow m_RowInfo
        {
            set
            {
                m_BrokerId = value.GetSafeValue("sBrokerId", Guid.Empty);
                m_BranchId = value.GetSafeValue("sBranchId", Guid.Empty);
                m_LoanName = value.GetSafeValue("sLNm", string.Empty);
                //m_Borrower = value.GetSafeValue("sPrimBorrowerFullNm", string.Empty);
                //m_OpenedDate = value.GetSafeValue("sOpenedD", DateTime.MinValue);
                m_NoteIRSubmitted = value.GetSafeValue("sNoteIRSubmitted", Decimal.MinusOne);

                m_ExternalManagerId = value.GetSafeValue("PmlExternalManagerEmployeeId", Guid.Empty);
                m_IsTemplate = value.GetSafeValue("IsTemplate", false);
                m_IsValid = value.GetSafeValue("IsValid", false);

                m_LoanStatus = value.GetSafeValue("sStatusT", E_sStatusT.Loan_Other);
                m_LienPosition = value.GetSafeValue("sLienPosT", E_sLienPosT.First);

                m_sRateLockStatusT = value.GetSafeValue("sRateLockStatusT", E_sRateLockStatusT.NotLocked);
                m_Assignments.m_RowInfo = value;

                m_sPmlBrokerId = value.GetSafeValue("sPmlBrokerId", Guid.Empty);
                m_PmlExternalBrokerProcessorManagerEmployeeId = value.GetSafeValue("PmlExternalBrokerProcessorManagerEmployeeId", Guid.Empty);
                m_PmlExternalSecondaryManagerEmployeeId = value.GetSafeValue("PmlExternalSecondaryManagerEmployeeId", Guid.Empty);
                m_PmlExternalPostCloserManagerEmployeeId = value.GetSafeValue("PmlExternalPostCloserManagerEmployeeId", Guid.Empty);
            }
        }

        public Guid BrokerId
        {
            get { 
                //VerifyGuid("sBrokerId", m_BrokerId); 
                return m_BrokerId; }
        }

        public Guid BranchId
        {
            get { 
                //VerifyGuid("sBranchId", m_BranchId); 
                return m_BranchId; }
        }

        public string LoanName
        {
            get { 
                //VerifyString("sLNm", m_LoanName);  
                return m_LoanName; }
        }

        //public string Borrower
        //{
        //    get { 
        //        //VerifyString("sPrimBorrowerFullNm", m_Borrower);  
        //        return m_Borrower; }
        //}

        public E_sStatusT LoanStatus
        {
            get { 
                //VerifyInt("sStatusT", (int)m_LoanStatus); 
                return m_LoanStatus; }
        }

        public Guid PmlExternalBrokerProcessorManagerEmployeeId
        {
            get { 
                //VerifyGuid("PmlExternalBrokerProcessorManagerEmployeeId", m_PmlExternalBrokerProcessorManagerEmployeeId); 
                return m_PmlExternalBrokerProcessorManagerEmployeeId; }
        }

        public Guid PmlExternalSecondaryManagerEmployeeId
        {
            get { return this.m_PmlExternalSecondaryManagerEmployeeId; }
        }

        public Guid PmlExternalPostCloserManagerEmployeeId
        {
            get { return this.m_PmlExternalPostCloserManagerEmployeeId; }
        }

        public Guid sPmlBrokerId
        {
            get { 
                //VerifyGuid("sPmlBrokerId", m_sPmlBrokerId); 
                return m_sPmlBrokerId; }
        }

        public E_sLienPosT LienPosition
        {
            get { 
                //VerifyInt("sLienPosT", (int)m_LienPosition); 
                return m_LienPosition; }
        }

        //public DateTime OpenedDate
        //{
        //    get { 
        //        //VerifyDateTime("sOpenedD", m_OpenedDate); 
        //        return m_OpenedDate; }
        //}

        public decimal NoteIRSubmitted
        {
            get { 
                //VerifyFloat("sNoteIRSubmitted", (float)m_NoteIRSubmitted); 
                return m_NoteIRSubmitted; }
        }

        // 7/27/2009 dd - OPM 32212
        public E_sRateLockStatusT sRateLockStatusT
        {
            get { 
                //VerifyInt("sRateLockStatusT", (int)m_sRateLockStatusT); 
                return m_sRateLockStatusT; }
        }
        public bool IsRateLocked
        {
            get 
            {
                switch (sRateLockStatusT)
                {
                    case E_sRateLockStatusT.LockRequested:
                    case E_sRateLockStatusT.NotLocked:
                    case E_sRateLockStatusT.LockSuspended:
                        return false;
                    case E_sRateLockStatusT.Locked:
                        return true;
                    default:
                        throw new UnhandledEnumException(sRateLockStatusT);
                }
            }
        }

        public Guid ExternalManagerId
        {
            get 
            { 
                //VerifyGuid("PmlExternalManagerEmployeeId", m_ExternalManagerId);  
                return m_ExternalManagerId; }
        }

        public bool IsTemplate
        {
            get { 
                //VerifyBool("IsTemplate", m_IsTemplate); 
                return m_IsTemplate; }
        }

        public bool IsValid
        {
            get { return m_IsValid; }
        }

        public LoanAssignments Assignments
        {
            get { return m_Assignments; }
        }

        public int HasAssignment(string role, Guid employeeId)
        {
            int ret = m_Assignments.TestAssignment(role, employeeId);

            //VerifyRole(role, employeeId, ret);
            return ret;
        }
        public Guid LoanId
        {
            get { return m_LoanId; }
        }

        #endregion

        /// <summary>
        /// Pull contents from existing table.  We use this method
        /// for piggy-backing access control details loading with
        /// another fetch, so that one connection and retrieve
        /// can load all in one payload.
        /// </summary>

        public virtual LoanAccessInfo Initialize(Guid loanId, DataRow dRow)
        {
            // Delegate to initializer.

            m_LoanId = loanId;
            m_RowInfo = dRow;
            //x_dataLoanCache = new LoanFileCache(System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal, loanId, dRow);
            return this;
        }

        /// <summary>
        /// Get the loan access info by its id.  If cached, then
        /// we pull that one.  If not, then we load it using the
        /// cache as our accessor.  The instance should be cached
        /// after we load.
        /// </summary>

        public void Retrieve(Guid loanId)
        {
            // Get the cached instance.  If not found, we load
            // a new instance into this object.

            // Load the employee's details from the database.
            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", loanId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetAccessControlLoanInfo", parameters))
            {
                if (reader.Read())
                {
                    m_LoanId = loanId;
                    m_Record = reader;
                }
                else
                {
                    throw new NotFoundException("Unable to load loan access control info.", "Unable to load loan access control info.");
                }
            }
            //x_dataLoanCache = new LoanFileCache(System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal, loanId, x_fieldList);
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAccessInfo(Guid loanId)
        {
            // Initialize members.

            Retrieve(loanId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanAccessInfo()
        {
        }

        #endregion

    }
}
