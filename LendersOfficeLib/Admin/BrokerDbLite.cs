﻿// <copyright file="BrokerDbLite.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/15/2014 6:40:20 PM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Return read-only information of the broker object.
    /// </summary>
    public class BrokerDbLite
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="BrokerDbLite" /> class from being created.
        /// </summary>
        private BrokerDbLite()
        {
        }

        /// <summary>
        /// Gets the id of broker.
        /// </summary>
        /// <value>Id of broker.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the name of broker.
        /// </summary>
        /// <value>Name of broker.</value>
        public string BrokerNm { get; private set; }

        /// <summary>
        /// Gets the customer code.
        /// </summary>
        /// <value>Customer Code of broker.</value>
        public string CustomerCode { get; private set; }

        /// <summary>
        /// Gets the active status of broker.
        /// </summary>
        /// <value>Active status of broker.</value>
        public int Status { get; private set; }

        /// <summary>
        /// Retrieve broker information from database.
        /// </summary>
        /// <param name="brokerId">Id of broker.</param>
        /// <returns>Broker information object.</returns>
        public static BrokerDbLite RetrieveById(Guid brokerId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_RetrieveLiteInfo", parameters))
            {
                if (reader.Read())
                {
                    BrokerDbLite broker = new BrokerDbLite();
                    broker.BrokerId = brokerId;
                    broker.BrokerNm = (string)reader["BrokerNm"];
                    broker.CustomerCode = (string)reader["CustomerCode"];
                    broker.Status = (int)reader["Status"];
                    return broker;
                }
            }

            throw new BrokerDBNotFoundException(brokerId);
        }

        /// <summary>
        /// Go through all the main databases and return an aggregate list of broker information.
        /// </summary>
        /// <returns>A dictionary of broker information.</returns>
        public static IDictionary<Guid, BrokerDbLite> ListAllSlow()
        {
            Dictionary<Guid, BrokerDbLite> dictionary = new Dictionary<Guid, BrokerDbLite>();

            foreach (DbConnectionInfo connectionInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "BROKER_ListAll", null))
                {
                    while (reader.Read())
                    {
                        BrokerDbLite broker = new BrokerDbLite();
                        broker.BrokerId = (Guid)reader["BrokerId"];
                        broker.BrokerNm = (string)reader["BrokerNm"];
                        broker.CustomerCode = (string)reader["CustomerCode"];
                        broker.Status = (int)reader["Status"];

                        // 7/13/2016 - we are splitting the main  database LendersOffice into LendersOffice_3.
                        // Therefore there will be duplicate brokerid in both LendersOffice and LendersOffice_3.
                        // When there is more than one occurrence, we will use the *only* broker not in an invalid status.
                        BrokerDbLite existingCopy;
                        if (!dictionary.TryGetValue(broker.BrokerId, out existingCopy))
                        {
                            dictionary.Add(broker.BrokerId, broker);
                        }
                        else if (existingCopy.Status == 0 && broker.Status == 1)
                        {
                            dictionary[broker.BrokerId] = broker;
                        }
                        else if (existingCopy.Status == 1 && broker.Status == 1)
                        {
                            throw CBaseException.GenericException("Found two active copies of the broker with id=" + broker.BrokerId);
                        }

                        // The other cases in a truth table (both inactive, dictionary already has the active broker) are no-ops
                    }
                }
            }

            return dictionary;
        }
    }
}