﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Store options for user interface display.  We keep
    /// track of a simple pair-based option.  Not too fancy.
    /// </summary>

    [XmlRoot]
    [Serializable]
    public class ChoiceItem : IComparable
    {
        /// <summary>
        /// Store options for user interface display.
        /// We keep track of a simple pair-based option.
        /// Not too fancy.
        /// </summary>

        private String m_Category = "";
        private String m_Description = "";

        #region ( Item properties )

        [XmlElement]
        public String Category
        {
            set { m_Category = value; }
            get { return m_Category; }
        }

        [XmlElement]
        public String Description
        {
            set { m_Description = value; }
            get { return m_Description; }
        }

        #endregion

        /// <summary>
        /// Compare against other choice item for ordering.
        /// </summary>

        public int CompareTo(object oItem)
        {
            // Compare against other choice item for
            // ordering.

            ChoiceItem cItem = oItem as ChoiceItem; int r;

            if (cItem == null)
            {
                throw new InvalidCastException("Not a choice item.");
            }

            r = m_Category.CompareTo(cItem.m_Category);

            if (r == 0)
            {
                r = m_Description.CompareTo(cItem.m_Description);
            }

            return r;
        }

    }
}
