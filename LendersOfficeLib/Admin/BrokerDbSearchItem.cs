﻿// <copyright file="BrokerDbSearchItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/28/2014 3:33:08 PM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Data.Common;

    /// <summary>
    /// Broker information display in search result.
    /// </summary>
    public class BrokerDbSearchItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerDbSearchItem" /> class.
        /// </summary>
        /// <param name="reader">The sql data reader contains info.</param>
        public BrokerDbSearchItem(DbDataReader reader)
        {
            this.BrokerId = (Guid)reader["BrokerId"];
            this.BrokerNm = (string)reader["BrokerNm"];
            this.BrokerAddress = (string)reader["BrokerAddress"];
            this.BrokerPhone = (string)reader["BrokerPhone"];
            this.BrokerFax = (string)reader["BrokerFax"];
            this.CustomerCode = (string)reader["CustomerCode"];
            this.CreditMornetPlusUserId = (string)reader["CreditMornetPlusUserId"];
            this.Notes = (string)reader["Notes"];
            this.IsActive = (int)reader["Status"] == 1;
            this.EmailAddressesForSystemChangeNotif = (string)reader["EmailAddressesForSystemChangeNotif"];
            this.SuiteType = (BrokerSuiteType)Convert.ToInt32(reader["SuiteType"]);
        }

        /// <summary>
        /// Gets the id of broker.
        /// </summary>
        /// <value>Id of broker.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the name of broker.
        /// </summary>
        /// <value>Name of broker.</value>
        public string BrokerNm { get; private set; }

        /// <summary>
        /// Gets the full address of broker.
        /// </summary>
        /// <value>The Full Address of broker.</value>
        public string BrokerAddress { get; private set; }

        /// <summary>
        /// Gets the phone of broker.
        /// </summary>
        /// <value>The phone of broker.</value>
        public string BrokerPhone { get; private set; }

        /// <summary>
        /// Gets the fax number of broker.
        /// </summary>
        /// <value>The fax number of broker.</value>
        public string BrokerFax { get; private set; }

        /// <summary>
        /// Gets the customer code of broker.
        /// </summary>
        /// <value>The customer code of broker.</value>
        public string CustomerCode { get; private set; }

        /// <summary>
        /// Gets the DU user id of broker.
        /// </summary>
        /// <value>The DU user id of broker.</value>
        public string CreditMornetPlusUserId { get; private set; }

        /// <summary>
        /// Gets the notes of broker.
        /// </summary>
        /// <value>The notes of broker.</value>
        public string Notes { get; private set; }

        /// <summary>
        /// Gets the email addresses for system change notification.
        /// </summary>
        /// <value>Email addresses for system change notification.</value>
        public string EmailAddressesForSystemChangeNotif { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the broker is active.
        /// </summary>
        /// <value>Whether or not the broker is active.</value>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets the suite type for the broker.
        /// </summary>
        public BrokerSuiteType SuiteType { get; private set; }
    }
}