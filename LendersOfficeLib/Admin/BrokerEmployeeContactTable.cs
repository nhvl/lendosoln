﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Store a broker's employees' info by their employee ids.  We
    /// cache the entire broker in a hashtable.
    /// </summary>

    public class BrokerEmployeeContactTable
    {
        /// <summary>
        /// Store a broker's employees' info by their employee ids.
        /// We cache the entire broker in a hashtable.
        /// </summary>

        private Hashtable m_Table = new Hashtable();

        public EmployeeContactInfo this[Guid employeeId]
        {
            // Access member.

            get
            {
                return m_Table[employeeId] as EmployeeContactInfo;
            }
        }

        /// <summary>
        /// Retrieve the broker's employees' address and contact info.
        /// </summary>
        public void Retrieve(Guid brokerId)
        {
            // Retrieve the broker's employees' address and contact info
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeAddressByBrokerId", parameters))
            {
                while (reader.Read() == true)
                {
                    EmployeeContactInfo eC = new EmployeeContactInfo(reader);

                    m_Table.Add(eC.EmployeeId, eC);
                }
            }
        }

        /// <summary>
        /// Look in our table for the given employee entry.
        /// </summary>

        public bool Contains(Guid employeeId)
        {
            // Check if we have the entry in our set.

            return m_Table.Contains(employeeId);
        }

        /// <summary>
        /// Return the looping interface for added entries.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return the looping interface for added entries.

            return m_Table.Values.GetEnumerator();
        }

        /// <summary>
        /// Nix existing entries from our table of employee specs.
        /// </summary>

        public void Clear()
        {
            // Nix existing entries from our table.

            m_Table.Clear();
        }

    }
}
