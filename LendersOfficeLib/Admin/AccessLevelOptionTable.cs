﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class AccessLevelOptionTable : BrokerUserPermissionTable
    {
        public AccessLevelOptionTable(bool showTeam)
            : base()
        {
            if (showTeam) Insert( 2, new Desc(Permission.TeamLevelAccess, "Team - only if team explicitly assigned"));
        }
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>


        protected override bool IsIncluded(Desc pDesc)
        {
            // Throw out everything but access level.

            switch (pDesc.Code)
            {
                case Permission.BrokerLevelAccess:
                case Permission.BranchLevelAccess:
                case Permission.IndividualLevelAccess:
                    {
                        return true;
                    }
            }
            return false;
        }

        #endregion

    }
}
