﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.BrokerAdmin;

    /// <summary>
    /// Class that gets all the feature data from the backend and processes them for UI.
    /// </summary>
    [DataContract]
    public class FeatureResult
    {
        /// <summary>
        /// The feature type.
        /// </summary>
        [DataMember]
        private FeatureType featureType;

        /// <summary>
        /// The priority of the feature set by LOAdmin user.
        /// </summary>
        /// <value>The priority of the feature.</value>
        [DataMember]
        private decimal priority;

        /// <summary>
        /// The boolean that determines whether this feature is shown or hidden on the FeatureAdoption page set by LOAdmin user.
        /// </summary>
        /// <value>The boolean that determines whether this feature is shown or hidden.</value>
        [DataMember]
        private bool isHidden;

        /// <summary>
        /// Name of the feature.
        /// </summary>
        [DataMember]
        private string featureName;

        /// <summary>
        /// String that describes the goal for the feature.
        /// 1. Enable.
        /// 2. Disable.
        /// 3. At least one.
        /// </summary>
        [DataMember]
        private string goal;

        /// <summary>
        /// String that describes whether the goal has been met for the feature.
        /// 1. Yes.
        /// 2. No.
        /// 3. Don't care.
        /// </summary>
        [DataMember]
        private string meetsGoal;

        /// <summary>
        /// String that defines the client type target for the feature.
        /// </summary>
        [DataMember]
        private string clientTypeTarget;        

        /// <summary>
        /// All brokers that meet the goal of the feature.
        /// Only store the brokerId and customerCode - brokerNm.
        /// </summary>
        [DataMember]
        private List<Tuple<string, string>> clientsMeetingGoal;

        /// <summary>
        /// All brokers that do not meet the goal of the feature.
        /// Only store the brokerId and customerCode - brokerNm.
        /// </summary>
        [DataMember]
        private List<Tuple<string, string>> clientsNotMeetingGoal;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureResult" /> class.
        /// Creates a FeatureResult with all broker information.
        /// </summary>
        /// <param name="f">Feature containing all the hard-coded data and DB data.</param>
        /// <param name="clientsMeetingGoal">List of all clients meeting the goal.</param>
        /// <param name="clientsNotMeetingGoal">List of all clients not meeting the goal.</param>
        private FeatureResult(Feature f, List<Tuple<string, string>> clientsMeetingGoal, List<Tuple<string, string>> clientsNotMeetingGoal)
        {
            this.featureType = f.FeatureType;
            this.priority = f.Priority;
            this.isHidden = f.IsHidden;
            this.featureName = f.FeatureName;
            this.goal = f.Goal;
            this.clientTypeTarget = f.ClientTypeTarget;

            this.clientsMeetingGoal = clientsMeetingGoal;
            this.clientsNotMeetingGoal = clientsNotMeetingGoal;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureResult" /> class.
        /// Creates a FeatureResult with a single broker information.
        /// </summary>
        /// <param name="f">Feature containing all the hard-coded data and DB data.</param>
        /// <param name="goal">Goal for the feature..</param>
        /// <param name="meetsGoal">Whether the feature meets the goal.</param>
        private FeatureResult(Feature f, string goal, string meetsGoal)
        {
            this.featureType = f.FeatureType;
            this.priority = f.Priority;
            this.isHidden = f.IsHidden;
            this.featureName = f.FeatureName;
            this.clientTypeTarget = f.ClientTypeTarget;

            this.goal = goal;
            this.meetsGoal = meetsGoal;
        }

        /// <summary>
        /// Gets count of all brokers that meet the goal of the feature.
        /// </summary>
        [DataMember]
        private int ClientsMeetingGoalCount
        {
            get { return this.clientsMeetingGoal?.Count ?? 0; }
        }

        /// <summary>
        /// Gets count of all brokers that do not meet the goal of the feature.
        /// </summary>
        [DataMember]
        private int ClientsNotMeetingGoalCount
        {
            get { return this.clientsNotMeetingGoal?.Count ?? 0; }
        }

        /// <summary>
        /// Gets percentage of brokers that are meeting the goal for the feature.
        /// </summary>
        [DataMember]
        private decimal PercentageMeetingGoal
        {
            get
            {
                if (this.ClientsMeetingGoalCount + this.ClientsNotMeetingGoalCount != 0)
                {
                    return Math.Round(decimal.Divide(this.ClientsMeetingGoalCount, this.ClientsMeetingGoalCount + this.ClientsNotMeetingGoalCount) * 100, 3);
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Sets all FeatureResult objects for a single broker.
        /// </summary>
        /// <param name="brokerdb">BrokerDB object.</param>
        /// <returns>List of feature result objects.</returns>
        public static List<FeatureResult> GetAllFeatureResultsForSingleBroker(BrokerDB brokerdb)
        {
            List<Feature> featureList = Feature.GetAllFeatures();
            List<FeatureResult> featureResultList = new List<FeatureResult>();

            foreach (Feature feature in featureList)
            {
                featureResultList.Add(CreateFeatureResultForSingleBroker(feature, brokerdb));
            }

            return featureResultList;
        }

        /// <summary>
        /// Returns all FeatureResult objects.
        /// </summary>
        /// <returns>List of FeatureResult objects.</returns>
        public static List<FeatureResult> GetAllFeatureResults()
        {
            List<Guid> brokerIds = (List<Guid>)Tools.GetAllActiveBrokers();
            List<BrokerDB> brokerDbs = Tools.GetBrokerDBsFromBrokerIds(brokerIds);
            List<Feature> featureList = Feature.GetAllFeatures();
            List<FeatureResult> featureResultList = new List<FeatureResult>();

            foreach (Feature feature in featureList)
            {
                featureResultList.Add(CreateFeatureResult(feature, brokerDbs));
            }

            return featureResultList;
        }

        /// <summary>
        /// Calculate the necessary Goal values for the feature.
        /// </summary>
        /// <param name="feature">Feature to calculate and return a feature result for.</param>
        /// <param name="brokerDbs">List of broker db objects.</param>
        /// <returns>Returns a new FeatureResult object.</returns>
        private static FeatureResult CreateFeatureResult(Feature feature, List<BrokerDB> brokerDbs)
        {
            List<Tuple<string, string>> clientsMeetingGoal = new List<Tuple<string, string>>();
            List<Tuple<string, string>> clientsNotMeetingGoal = new List<Tuple<string, string>>();

            foreach (BrokerDB brokerdb in brokerDbs)
            {
                GoalStatus meetsGoal = feature.MeetsGoalFunc(brokerdb);
                bool isMatchTypeTarget = feature.IsMatchTypeTarget(brokerdb);

                if (meetsGoal == GoalStatus.Met && isMatchTypeTarget)
                {
                    clientsMeetingGoal.Add(Tuple.Create(brokerdb.BrokerID.ToString(), brokerdb.CustomerCode + " - " + brokerdb.Name));
                }
                else if (meetsGoal == GoalStatus.NotMet && isMatchTypeTarget)
                {
                    clientsNotMeetingGoal.Add(Tuple.Create(brokerdb.BrokerID.ToString(), brokerdb.CustomerCode + " - " + brokerdb.Name));
                }
            }

            return new FeatureResult(feature, clientsMeetingGoal, clientsNotMeetingGoal);
        }

        /// <summary>
        /// Calculate the necessary Goal values for the feature specific to a broker.
        /// </summary>
        /// <param name="feature">A feature object.</param>
        /// <param name="brokerdb">A BrokerDB object that specifies a broker.</param>
        /// <returns>FeatureResult object.</returns>
        private static FeatureResult CreateFeatureResultForSingleBroker(Feature feature, BrokerDB brokerdb)
        {
            string goal = feature.Goal;
            string meetsGoal;
            if (feature.IsMatchTypeTarget(brokerdb))
            {
                if (feature.MeetsGoalFunc(brokerdb) == GoalStatus.Met)
                {
                    meetsGoal = "Yes";
                }
                else
                {
                    meetsGoal = "No";
                }
            }
            else
            {
                goal = "Don't care";
                meetsGoal = "Don't care";
            }

            return new FeatureResult(feature, goal, meetsGoal);
        }
    }
}
