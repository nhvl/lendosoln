﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableCustomReports : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanRunCustomReports:
                case Permission.CanEditOthersCustomReports:
                case Permission.CanPublishCustomReports:
                case Permission.AllowExportingFullSsnViaCustomReports:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
