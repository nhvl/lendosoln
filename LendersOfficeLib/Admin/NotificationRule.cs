﻿using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.Admin
{
    /// <summary>
    /// We track all the possible reply-to scenarios for a given
    /// loan event.
    /// </summary>

    [XmlRoot]
    public class NotificationRule
    {
        /// <summary>
        /// We track all the possible reply-to scenarios for
        /// a given loan event.
        /// </summary>

        private String m_EventLabel = String.Empty;
        private ArrayList m_ReplyToList = new ArrayList();
        private String m_DefaultReply = String.Empty;
        private String m_DefaultLabel = String.Empty;
        private String m_CarbonCopyTo = String.Empty;
        private Boolean m_DoNotReply = false;

        #region ( Rule properties )

        [XmlElement]
        public String EventLabel
        {
            set { m_EventLabel = value; }
            get { return m_EventLabel; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(String))]
        public ArrayList ReplyToList
        {
            set { m_ReplyToList = value; }
            get { return m_ReplyToList; }
        }

        [XmlElement]
        public String DefaultReply
        {
            set { m_DefaultReply = value; }
            get { return m_DefaultReply; }
        }

        [XmlElement]
        public String DefaultLabel
        {
            set { m_DefaultLabel = value; }
            get { return m_DefaultLabel; }
        }

        [XmlElement]
        public String CarbonCopyTo
        {
            set { m_CarbonCopyTo = value; }
            get { return m_CarbonCopyTo; }
        }

        [XmlElement]
        public Boolean DoNotReply
        {
            set { m_DoNotReply = value; }
            get { return m_DoNotReply; }
        }

        #endregion
    }
}
