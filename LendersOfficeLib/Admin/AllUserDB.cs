﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Admin
{
    public class AllUserDB
    {
        public static bool IsUserLocked(Guid brokerId, Guid userId, Guid guidPmlBrokerId)
        {

            DateTime dtExpirationDate = DateTime.MinValue;

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };
            
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoginBlockExpirationDateByUserId", parameters))
            {
                if (reader.Read())
                {
                    dtExpirationDate = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                }
            }
            return (dtExpirationDate == LendersOffice.Common.SmallDateTime.MaxValue);
        }

        public static void LockAccount(Guid brokerId, Guid userId, AbstractUserPrincipal principal)
        {
            SqlParameter[] parameters = {
											new SqlParameter("@UserId", userId),
											new SqlParameter("@NextAvailableLoginTime", LendersOffice.Common.SmallDateTime.MaxValue)
										};
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "SetLoginBlockExpirationDateByUserId", 1, parameters);

            SecurityEventLogHelper.CreateAccountLockLog(principal);
        }

        public static void ExpirePasswordResetRequest(Guid brokerId, Guid guidRequestId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@RequestId", guidRequestId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ExpirePasswordResetRequest", 0, parameters);
        }
    }
}
