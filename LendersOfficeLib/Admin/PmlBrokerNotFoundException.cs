﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    public class PmlBrokerNotFoundException : NotFoundException
    {
        public PmlBrokerNotFoundException(Guid pmlBrokerId, Guid brokerId)
            : base(ErrorMessages.PmlBrokerNotFound, "PmlBrokerId=" + pmlBrokerId + ", BrokerId=" + brokerId + " not found in PML_BROKER.")
        { }

        public PmlBrokerNotFoundException(string pmlBrokerName, Guid brokerId)
            : base(ErrorMessages.PmlBrokerNotFound, "PmlBrokerNm=" + pmlBrokerName + ", BrokerId=" + brokerId + " not found in PML_BROKER.")
        { }
    }
}
