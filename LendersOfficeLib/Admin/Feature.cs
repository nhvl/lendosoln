﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Feature supported by our system.
    /// </summary>
    public class Feature
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Feature" /> class.
        /// Creates a Feature with all broker information.
        /// </summary>
        /// <param name="fd">FeatureData containing the data for the feature from the tracked_feature DB.</param>
        /// <param name="featureName">Name of the feature.</param>
        /// <param name="goal">The Goal of the feature, mostly "Enable" or "Disable".</param>
        /// <param name="meetsGoalFunc">A delegate that accepts a BrokerDB object and returns a GoalStatus.</param>
        /// <param name="clientTypeTargetName">Client Type Target name for the feature.</param>
        /// <param name="isMatchTypeTarget">A delegate that accepts a BrokerDB object and returns a bool.</param>
        public Feature(FeatureData fd, string featureName, string goal, Func<BrokerDB, GoalStatus> meetsGoalFunc, string clientTypeTargetName, Func<BrokerDB, bool> isMatchTypeTarget)
        {
            this.FeatureType = fd.FeatureType;
            this.Priority = fd.Priority;
            this.IsHidden = fd.IsHidden;
            this.FeatureName = featureName;
            this.Goal = goal;
            this.MeetsGoalFunc = meetsGoalFunc;
            this.ClientTypeTarget = clientTypeTargetName;
            this.IsMatchTypeTarget = isMatchTypeTarget;            
        }        

        /// <summary>
        /// Gets or sets the feature type.
        /// </summary>
        /// <value>The type of the feature.</value>
        public FeatureType FeatureType { get; set; }

        /// <summary>
        /// Gets or sets the priority of the feature set by LOAdmin user.
        /// </summary>
        /// <value>The Priority of the feature.</value>
        public decimal Priority { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this feature is shown or hidden on the FeatureAdoption page set by LOAdmin user.
        /// </summary>
        /// <value>The boolean that determines whether this feature is shown or hidden.</value>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Gets or sets the name of the feature.
        /// </summary>
        /// <value>The name of the feature.</value>
        public string FeatureName { get; set; }

        /// <summary>
        /// Gets or sets the string that describes the Goal for the feature.
        /// 1. Enable.
        /// 2. Disable.
        /// 3. At least one.
        /// </summary>
        /// <value>The goal of the feature.</value>
        public string Goal { get; set; }

        /// <summary>
        /// Gets or sets the String that defines the client type target for the feature.
        /// </summary>
        /// <value>The client type target of the feature.</value>
        public string ClientTypeTarget { get; set; }

        /// <summary>
        /// Gets or sets the lambda expression that defines whether the feature meets the Goal.
        /// </summary>
        /// <value>The lambda expression.</value>
        public Func<BrokerDB, GoalStatus> MeetsGoalFunc { get; set; }

        /// <summary>
        /// Gets or sets the lambda expression that defines whether the feature meets the Goal.
        /// </summary>
        /// <value>The lambda expression.</value>
        public Func<BrokerDB, bool> IsMatchTypeTarget { get; set; }

        /// <summary>
        /// Function that gets all features.
        /// </summary>
        /// <returns>List of all features.</returns>
        public static List<Feature> GetAllFeatures()
        {
            List<FeatureData> featureDataList = FeatureData.GetAllTrackedFeatures();
            var featureTypeCollection = Enum.GetValues(typeof(FeatureType));

            if (featureTypeCollection.Length > featureDataList.Count())
            {
                foreach (FeatureType feature in featureTypeCollection.Cast<FeatureType>())
                {
                    HashSet<FeatureType> featureSetFromDB = new HashSet<FeatureType>(featureDataList.Select(fd => fd.FeatureType));

                    if (!featureSetFromDB.Contains(feature))
                    {
                        featureDataList.Add(new FeatureData(feature, 0M, false)); // Default feature settings
                    }
                }
            }

            List<Feature> featureList = new List<Feature>();
            foreach (FeatureData featureData in featureDataList)
            {
                try
                {
                    featureList.Add(CreateNewFeature(featureData));
                }
                catch (CBaseException exc)
                {
                    Tools.LogError(exc);
                    continue;
                }
            }

            return featureList;
        }        

        /// <summary>
        /// Compares the features in the DB to those implemented in the FeatureType enum.
        /// If any enum values exist that aren't represented in the DB, add a reference.
        /// </summary>
        /// <param name="implementedTypes">Feature types handled in the code.</param>
        /// <param name="trackedFeaturesInDb">Features that are currently tracked in the DB.</param>
        public static void AddMissingFeatureTypesToDb(IEnumerable<FeatureType> implementedTypes, HashSet<FeatureType> trackedFeaturesInDb)
        {
            foreach (var feature in implementedTypes)
            {
                if (!trackedFeaturesInDb.Contains(feature))
                {
                    FeatureData.AddNewTrackedFeature(feature);
                }
            }
        }

        /// <summary>
        /// Creates a new feature.
        /// </summary>
        /// <param name="featureData">FeatureData object.</param>
        /// <returns>Feature object instantiated with feature data.</returns>
        public static Feature CreateNewFeature(FeatureData featureData)
        {
            switch (featureData.FeatureType)
            {
                // place to add new features.
                // Each feature constructor will accept 6 parameters.
                // 1. featureData, a FeatureData object
                // 2. Name of the feature.
                // 3. The goal of the feature which is either "Enable" or "Disable" unless otherwise specified.
                // 4. A lambda logic that defines how the goal is calculated. Accepts a BrokerDB object and returns a GoalStatus enum.
                // 5. Client Type Target name for the feature.
                // 6. A lambda logic that defines how client type target is calculated. Accepts a BrokerDB object and returns a bool.
                case FeatureType.PML2:
                    return new Feature(
                        featureData,
                        featureName: "PML 2.0",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.IsNewPmlUIEnabled ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.HistoricalPricing:
                    return new Feature(
                        featureData,
                        featureName: "Historical Pricing",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.UnconditionallyEnableHistoricalPricingAndUiEnhancements ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.SeamlessDu:
                    return new Feature(
                        featureData,
                        featureName: "Seamless DU",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.GetSeamlessDuSettings().SeamlessDuEnabled ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.SeamlessLpa:
                    return new Feature(
                        featureData,
                        featureName: "Seamless LPA",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.GetSeamlessLPASetting().SeamlessLpaEnabled ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.HistoricalPricingTesting:
                    return new Feature(
                        featureData,
                        featureName: "Historical Pricing Testing",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.HistoricalPricingSettings.IsTesting ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.PML3:
                    return new Feature(
                        featureData,
                        featureName: "PML 3",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => brokerdb.IsForcePML3 ? GoalStatus.Met : GoalStatus.NotMet, 
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                case FeatureType.PML3Testing:
                    return new Feature(
                        featureData,
                        featureName: "PML 3 Testing",
                        goal: "Enable",
                        meetsGoalFunc: brokerdb => !brokerdb.IsForcePML3 && brokerdb.EnablePml3TempOptionExists ? GoalStatus.Met : GoalStatus.NotMet,
                        clientTypeTargetName: "PML",
                        isMatchTypeTarget: brokerdb => brokerdb.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                default:
                    throw CBaseException.GenericException("Feature " + featureData.FeatureType + " exists in the database but is not tracked in the code");
            }
        }
    }
}