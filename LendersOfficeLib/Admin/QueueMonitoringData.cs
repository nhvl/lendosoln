﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Represents the data for the monitoring of a queue.
    /// </summary>
    public class QueueMonitoringData
    {
        /// <summary>
        /// The stored procedure name for adding a queue to the database.
        /// </summary>
        private const string AddQueue = "QueueMonitorThreshold_AddAutoEscalationMonitoringToQueue";

        /// <summary>
        /// The stored procedure name for deleting a queue to the database.
        /// </summary>
        private const string DeleteQueue = "QueueMonitorThreshold_DeleteAutoEscalationMonitoringFromQueue";

        /// <summary>
        /// The stored procedure name for listing all queues in the database.
        /// </summary>
        private const string ListAllQueues = "QueueMonitorThreshold_ListAutoEscalationQueues";

        /// <summary>
        /// The stored procedure name for getting a specific queue from the database.
        /// </summary>
        private const string GetSpecificQueue = "QueueMonitorThreshold_SelectSpecificAutoEscalationQueue";

        /// <summary>
        /// The stored procedure name for updating a queue in the database.
        /// </summary>
        private const string UpdateQueue = "QueueMonitorThreshold_UpdateAutoEscalationMonitoring";

        /// <summary>
        /// Initializes a new instance of the <see cref="QueueMonitoringData"/> class.
        /// This constructor is intended only for serialization; use 
        /// <see cref="QueueMonitoringData(DbDataReader)"/> for 
        /// <see cref="QueueMonitoringData"/> object construction.
        /// </summary>
        public QueueMonitoringData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueueMonitoringData"/> class using data 
        /// from the specified <see cref="DbDataReader"/>.
        /// </summary>
        /// <param name="reader">The DbDataReader to read data in from.</param>
        public QueueMonitoringData(DbDataReader reader)
        {
            this.ID = (int)reader["ID"];
            this.Name = (string)reader["Name"];
            this.Description = (string)reader["Description"];
            this.Threshold1QueueLength = (int)reader["Threshold1"];
            this.Threshold1QueueLengthDurationMinutes = (int)reader["Threshold1DurationInMinutes"];
            this.Threshold1OldestItemMinutes = (int)reader["Threshold1OldestItemMinutes"];
            this.Threshold2QueueLength = (int)reader["Threshold2"];
            this.Threshold2QueueLengthDurationMinutes = (int)reader["Threshold2DurationInMinutes"];
            this.Threshold2OldestItemMinutes = (int)reader["Threshold2OldestItemMinutes"];
        }

        /// <summary>
        /// Gets or sets the value for the ID of the monitoring.
        /// </summary>
        /// <value>The integer ID of the <see cref="QueueMonitoringData"/> object.</value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the value for the name of the monitoring.
        /// </summary>
        /// <value>The string name of the queue.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value for the description of the monitoring.
        /// </summary>
        /// <value>The string description of the queue.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets value for the number of items in the queue
        /// before sending an email to support.
        /// </summary>
        /// <value>The integer number of items in the threshold.</value>
        public int Threshold1QueueLength { get; set; }

        /// <summary>
        /// Gets or sets the value for the amount of time an item has
        /// been in the queue before sending an email to support.
        /// </summary>
        /// <value>The integer number of minutes in the threshold.</value>
        public int Threshold1QueueLengthDurationMinutes { get; set; }

        /// <summary>
        /// Gets or sets the value for how many minutes an item can
        /// be in a queue before sending an email to support.
        /// </summary>
        /// <value>The integer number of minutes in the threshold.</value>
        public int Threshold1OldestItemMinutes { get; set; }

        /// <summary>
        /// Gets or sets the value for the number of items in the queue
        /// before sending a critical issue email.
        /// </summary>
        /// <value>The integer number of items in the threshold.</value>
        public int Threshold2QueueLength { get; set; }

        /// <summary>
        /// Gets or sets the value for the amount of time an item has
        /// been in the queue before sending a critical issue email.
        /// </summary>
        /// <value>The integer number of minutes in the threshold.</value>
        public int Threshold2QueueLengthDurationMinutes { get; set; }

        /// <summary>
        /// Gets or sets the value for how many minutes an item can
        /// be in a queue before sending an email to support.
        /// </summary>
        /// <value>The integer number of minutes in the threshold.</value>
        public int Threshold2OldestItemMinutes { get; set; }

        /// <summary>
        /// Loads all of the <see cref="QueueMonitoringData"/> records from the database.
        /// </summary>
        /// <returns>An IEnumerable of the all <see cref="QueueMonitoringData"/> records in the database.</returns>
        public static IEnumerable<QueueMonitoringData> LoadAll()
        {
            var queues = new List<QueueMonitoringData>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, QueueMonitoringData.ListAllQueues, null))
            {
                while (reader.Read())
                {
                    queues.Add(new QueueMonitoringData(reader));
                }
            }

            return queues;
        }

        /// <summary>
        /// Gets a <see cref="QueueMonitoringData"/> record from the database using the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The ID of the <see cref="QueueMonitoringData"/> record to search for.</param>
        /// <returns>The <see cref="QueueMonitoringData"/> record with the specified name or null if no record was found.</returns>
        public static QueueMonitoringData GetQueue(int id)
        {
            var parameters = new SqlParameter[] { new SqlParameter("@ID", id) };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, QueueMonitoringData.GetSpecificQueue, parameters))
            {
                if (reader.Read())
                {
                    return new QueueMonitoringData(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Saves a new <see cref="QueueMonitoringData"/> object to the database.
        /// </summary>
        /// <param name="data">The new <see cref="QueueMonitoringData"/> object to save.</param>
        /// <returns>True if the addition was successful, false otherwise.</returns>
        public static bool AddNewMonitoring(QueueMonitoringData data)
        {
            var parameters = GetDefaultAddModifyParameters(data);

            // We expect exactly one row to be affected by this operation.
            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, QueueMonitoringData.AddQueue, 1, parameters) == 1;
        }

        /// <summary>
        /// Saves a modified <see cref="QueueMonitoringData"/> object to the database.
        /// </summary>
        /// <param name="data">The modified <see cref="QueueMonitoringData"/> object to save.</param>
        /// <returns>True if the save was successful, false otherwise.</returns>
        public static bool ModifyExistingMonitoring(QueueMonitoringData data)
        {
            var parameters = GetDefaultAddModifyParameters(data);

            parameters.Add(new SqlParameter("@ID", data.ID));

            // We expect exactly one row to be affected by this operation.
            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, QueueMonitoringData.UpdateQueue, 1, parameters) == 1;
        }

        /// <summary>
        /// Removes a <see cref="QueueMonitoringData"/> record from the database.
        /// </summary>
        /// <param name="id">The ID of the <see cref="QueueMonitoringData"/> object to delete.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        public static bool RemoveMonitoring(int id)
        {
            var parameters = new SqlParameter[] { new SqlParameter("@ID", id) };

            // We expect exactly one row to be affected by this operation.
            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, QueueMonitoringData.DeleteQueue, 1, parameters) == 1;
        }

        /// <summary>
        /// Determines whether this <see cref="QueueMonitoringData"/> object is missing any required fields.
        /// </summary>
        /// <returns>True if this <see cref="QueueMonitoringData"/> object is missing any required fields, false otherwise.</returns>
        public bool IsMissingFields()
        {
            var reqStringFields = new string[]
            {
                this.Name,
                this.Description
            };

            var reqIntFields = new int[]
            {
                this.ID,
                this.Threshold1QueueLength,
                this.Threshold1QueueLengthDurationMinutes,
                this.Threshold1OldestItemMinutes,
                this.Threshold2QueueLength,
                this.Threshold2QueueLengthDurationMinutes,
                this.Threshold2OldestItemMinutes
            };

            return reqStringFields.Any(stringField => string.IsNullOrWhiteSpace(stringField)) ||
                reqIntFields.Any(intField => intField < 0);
        }

        /// <summary>
        /// Returns a list of the default SQL parameters needed when adding or modifying
        /// a <see cref="QueueMonitoringData"/> object, including the name of the queue, description
        /// of the queue, and the thresholds for monitoring.
        /// </summary>
        /// <param name="data">The <see cref="QueueMonitoringData"/> object to obtain default parameters for.</param>
        /// <returns>
        /// A list of <see cref="SqlParameter"/> objects needed when adding or modifying a 
        /// <see cref="QueueMonitoringData"/> object.
        /// </returns>
        private static List<SqlParameter> GetDefaultAddModifyParameters(QueueMonitoringData data)
        {
            return new List<SqlParameter>()
            {
                new SqlParameter("@NewName", data.Name),
                new SqlParameter("@NewDescription", data.Description),
                new SqlParameter("@NewThreshold1", data.Threshold1QueueLength),
                new SqlParameter("@NewThreshold1DurationInMinutes", data.Threshold1QueueLengthDurationMinutes),
                new SqlParameter("@NewThreshold1OldestItemMinutes", data.Threshold1OldestItemMinutes),
                new SqlParameter("@NewThreshold2", data.Threshold2QueueLength),
                new SqlParameter("@NewThreshold2DurationInMinutes", data.Threshold2QueueLengthDurationMinutes),
                new SqlParameter("@NewThreshold2OldestItemMinutes", data.Threshold2OldestItemMinutes)
            };
        }
    }
}
