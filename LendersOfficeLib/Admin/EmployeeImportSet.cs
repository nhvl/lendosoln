﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;


namespace LendersOffice.Admin
{
    /// <summary>
    /// Contain individual employee descriptions and import them all in
    /// one shot.
    /// </summary>

    public class EmployeeImportSet
    {
        /// <summary>
        /// Contain individual employee descriptions and import them all
        /// in one shot.
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        /// <summary>
        /// Track individual employee descriptions.  We import a loaded
        /// set of descriptors in one shot.
        /// </summary>

        public class Desc
        {
            /// <summary>
            /// Track individual employee descriptions.  We import a
            /// loaded set of descriptors in one shot.
            /// </summary>

            private String m_FirstName = String.Empty;
            private string m_MiddleName = string.Empty;
            private String m_LastName = String.Empty;
            private String m_CompanyName = String.Empty;
            private String m_Street = String.Empty;
            private String m_City = String.Empty;
            private String m_State = String.Empty;
            private String m_Zipcode = String.Empty;
            private String m_Phone = String.Empty;
            private String m_Fax = String.Empty;
            private String m_Cell = String.Empty;
            private String m_Pager = String.Empty;
            private String m_Email = String.Empty;
            private Guid m_LenderAccountExecId = Guid.Empty;
            private Guid m_LockDeskId = Guid.Empty;
            private Guid m_UnderwriterId = Guid.Empty;
            private Guid m_JuniorUnderwriterId = Guid.Empty;
            private Guid m_ProcessorId = Guid.Empty;
            private Guid m_JuniorProcessorId = Guid.Empty;
            private Guid m_BrokerProcessorId = Guid.Empty;
            private Guid m_CorrespondentManagerId = Guid.Empty;
            private Guid m_CorrespondentProcessorId = Guid.Empty;
            private Guid m_CorrespondentJuniorProcessorId = Guid.Empty;
            private Guid m_CorrespondentLenderAccExecId = Guid.Empty;
            private Guid m_CorrespondentExternalPostCloserId = Guid.Empty;
            private Guid m_CorrespondentUnderwriterId = Guid.Empty;
            private Guid m_CorrespondentJuniorUnderwriterId = Guid.Empty;
            private Guid m_CorrespondentCreditAuditorId = Guid.Empty;
            private Guid m_CorrespondentLegalAuditorId = Guid.Empty;
            private Guid m_CorrespondentLockDeskId = Guid.Empty;
            private Guid m_CorrespondentPurchaserId = Guid.Empty;
            private Guid m_CorrespondentSecondaryId = Guid.Empty;
            private Guid m_MiniCorrespondentManagerId = Guid.Empty;
            private Guid m_MiniCorrespondentProcessorId = Guid.Empty;
            private Guid m_MiniCorrespondentJuniorProcessorId = Guid.Empty;
            private Guid m_MiniCorrespondentLenderAccExecId = Guid.Empty;
            private Guid m_MiniCorrespondentExternalPostCloserId = Guid.Empty;
            private Guid m_MiniCorrespondentUnderwriterId = Guid.Empty;
            private Guid m_MiniCorrespondentJuniorUnderwriterId = Guid.Empty;
            private Guid m_MiniCorrespondentCreditAuditorId = Guid.Empty;
            private Guid m_MiniCorrespondentLegalAuditorId = Guid.Empty;
            private Guid m_MiniCorrespondentLockDeskId = Guid.Empty;
            private Guid m_MiniCorrespondentPurchaserId = Guid.Empty;
            private Guid m_MiniCorrespondentSecondaryId = Guid.Empty;
            private bool m_IsSupervisor = false;
            private Guid m_SupervisorId = Guid.Empty;
            private String m_Notes = String.Empty;
            private LicenseInfoList m_LicenseInfoList = new LicenseInfoList();
            private Guid m_PmlBrokerId = Guid.Empty; 
            private E_PmlLoanLevelAccess m_pmlLoanLevelAccess = E_PmlLoanLevelAccess.Individual;

            private bool m_useOriginatingCompanyMiniCorrBranchId = false;
            private Guid m_miniCorrespondentBranchId = Guid.Empty;

            private bool m_useOriginatingCompanyCorrBranchId = false;
            private Guid m_correspondentBranchId = Guid.Empty;

            private Guid? m_tpoLandingPageID = null;
            private Guid? m_miniCorrespondentLandingPageID = null;
            private Guid? m_correspondentLandingPageID = null;

            private DateTime m_PasswordExpirationDate = DateTime.MinValue;
            private Int32 m_PasswordExpiresEvery;
            private String m_CreateWholesaleChannelLoans = String.Empty;
            private String m_CreateMiniCorrespondentChannelLoans = String.Empty;
            private String m_CreateCorrespondentChannelLoans = String.Empty;
            private String m_RunWithoutReport = String.Empty;
            private String m_SubmitWithoutReport = String.Empty;
            private String m_ApplyForIneligibleLoanPrograms = String.Empty;

            private Guid m_ManagerId = Guid.Empty;
            private Guid m_BranchId = Guid.Empty;
            private string m_RoleId = Guid.Empty.ToString();
            private String m_Login = String.Empty;
            private String m_Password = String.Empty;
            private Guid m_PriceGroup = Guid.Empty;
            private String m_UserType = String.Empty;
            private Boolean m_IsSaved = false;

            private bool m_useOriginatingCompanyMiniCorrPriceGroupId = false;
            private Guid m_miniCorrespondentPriceGroupId = Guid.Empty;

            private bool m_useOriginatingCompanyCorrPriceGroupId = false;
            private Guid m_correspondentPriceGroupId = Guid.Empty;

            private string m_nmlsIdentifier = string.Empty;
            private int m_LineNumber = -1;
            
            #region ( Descriptor properties )

            public E_OrigiantorCompenationLevelT? OriginatorCompensationSetLevelT { get; set; } //OPM 172995 - ir: Allow setting of compensation via Web Services

            public bool? OriginatorCompensationIsOnlyPaidForFirstLienOfCombo { get; set; }
            public decimal? OriginatorCompensationPercent { get; set; }
            public E_PercentBaseT? OriginatorCompensationBaseT { get; set; }
            public decimal? OriginatorCompensationMinAmount { get; set; }
            public decimal? OriginatorCompensationMaxAmount { get; set; }
            public decimal? OriginatorCompensationFixedAmount { get; set; }
            public string OriginatorCompensationNotes { get; set; }

            //OPM 172995 - ir: Allow setting of compensation via Web Services 
            public string CustomPricingPolicyField1Fixed { get; set; }
            public string CustomPricingPolicyField2Fixed { get; set; }
            public string CustomPricingPolicyField3Fixed { get; set; }
            public string CustomPricingPolicyField4Fixed { get; set; }
            public string CustomPricingPolicyField5Fixed { get; set; }
            public E_PricingPolicyFieldValueSource? CustomPricingPolicyFieldValueSource { get; set; }

            public string DOAutoLoginName { get; set; }
            public string DOAutoPassword { get; set; }
            public string DUAutoLoginName { get; set; }
            public string DUAutoPassword { get; set; }

            // ir - 8/11/2014, AccessiblePmlNavigationLinksId controls TPO Custom Portal Page Visibility
            public Guid[] AccessiblePmlNavigationLinksId { get; set; }

            // db 5/1/07 - This is just for reporting in case the save fails, it does not get
            // saved in the database along with the rest of the data
            public int LineNumber
            {
                set
                {
                    m_LineNumber = value;
                }
                get
                {
                    return m_LineNumber;
                }
            }

            public Guid PmlBrokerId
            {
                get
                {
                    return m_PmlBrokerId;
                }
                set
                {
                    m_PmlBrokerId = value;
                }
            }

            public string NmlsIdentifier
            {
                get { return m_nmlsIdentifier; }
                set { m_nmlsIdentifier = value; }
            }
            public String FirstName
            {
                // Access member

                set
                {
                    m_FirstName = value;
                }
                get
                {
                    return m_FirstName;
                }
            }

            public string MiddleName
            {
                get
                {
                    return this.m_MiddleName;
                }

                set
                {
                    this.m_MiddleName = value;
                }
            }
            public LicenseInfoList LicenseInformationList
            {
                // Access member

                set
                {
                    m_LicenseInfoList = value;
                }
                get
                {
                    return m_LicenseInfoList;
                }
            }

            public String LastName
            {
                // Access member

                set
                {
                    m_LastName = value;
                }
                get
                {
                    return m_LastName;
                }
            }

            public String Notes
            {
                // Access member

                set
                {
                    m_Notes = value;
                }
                get
                {
                    return m_Notes;
                }
            }

            public String CompanyName
            {
                // Access member

                set
                {
                    m_CompanyName = value;
                }
                get
                {
                    return m_CompanyName;
                }
            }

            public String Street
            {
                // Access member

                set
                {
                    m_Street = value;
                }
                get
                {
                    return m_Street;
                }
            }

            public String City
            {
                // Access member

                set
                {
                    m_City = value;
                }
                get
                {
                    return m_City;
                }
            }

            public String State
            {
                // Access member

                set
                {
                    m_State = value;
                }
                get
                {
                    return m_State;
                }
            }

            public String Zipcode
            {
                // Access member

                set
                {
                    m_Zipcode = value;
                }
                get
                {
                    return m_Zipcode;
                }
            }

            public String Phone
            {
                // Access member

                set
                {
                    m_Phone = value;
                }
                get
                {
                    return m_Phone;
                }
            }

            public String Fax
            {
                // Access member

                set
                {
                    m_Fax = value;
                }
                get
                {
                    return m_Fax;
                }
            }

            public String CellPhone
            {
                // Access member

                set
                {
                    m_Cell = value;
                }
                get
                {
                    return m_Cell;
                }
            }

            public bool? IsCellphoneForMultiFactorOnly
            {
                get;
                set;
            }

            public bool? EnableAuthCodeViaSms
            {
                get;
                set;
            }

            public String Pager
            {
                // Access member

                set
                {
                    m_Pager = value;
                }
                get
                {
                    return m_Pager;
                }
            }

            public String Email
            {
                // Access member

                set
                {
                    m_Email = value;
                }
                get
                {
                    return m_Email;
                }
            }

            public String CreateWholesaleChannelLoans
            {
                // Access member
                set
                {
                    m_CreateWholesaleChannelLoans = value;
                }
                get
                {
                    return m_CreateWholesaleChannelLoans;
                }
            }

            public String CreateMiniCorrespondentChannelLoans
            {
                // Access member
                set
                {
                    m_CreateMiniCorrespondentChannelLoans = value;
                }
                get
                {
                    return m_CreateMiniCorrespondentChannelLoans;
                }
            }

            public String CreateCorrespondentChannelLoans
            {
                // Access member
                set
                {
                    m_CreateCorrespondentChannelLoans = value;
                }
                get
                {
                    return m_CreateCorrespondentChannelLoans;
                }
            }

            public string AllowViewingCorrChannelLoans { get; set; } = string.Empty;

            public string AllowViewingMiniCorrChannelLoans{ get; set; } = string.Empty;

            public string AllowViewingWholesaleChannelLoans { get; set; } = string.Empty;

            public String RunWithoutReport
            {
                // Access member

                set
                {
                    m_RunWithoutReport = value;
                }
                get
                {
                    return m_RunWithoutReport;
                }
            }

            public String SubmitWithoutReport
            {
                // Access member

                set
                {
                    m_SubmitWithoutReport = value;
                }
                get
                {
                    return m_SubmitWithoutReport;
                }
            }

            public String ApplyForIneligibleLoanPrograms
            {
                // Access member

                set
                {
                    m_ApplyForIneligibleLoanPrograms = value;
                }
                get
                {
                    return m_ApplyForIneligibleLoanPrograms;
                }
            }

            public Guid LenderAccountExecId
            {
                // Access member

                set
                {
                    m_LenderAccountExecId = value;
                }
                get
                {
                    return m_LenderAccountExecId;
                }
            }

            public Guid LockDeskId
            {
                // Access member

                set
                {
                    m_LockDeskId = value;
                }
                get
                {
                    return m_LockDeskId;
                }
            }

            public Guid UnderwriterId
            {
                // Access member

                set
                {
                    m_UnderwriterId = value;
                }
                get
                {
                    return m_UnderwriterId;
                }
            }

            public Guid JuniorUnderwriterId
            {
                // Access member

                set
                {
                    m_JuniorUnderwriterId = value;
                }
                get
                {
                    return m_JuniorUnderwriterId;
                }
            }

            public Guid ProcessorId
            {
                // Access member

                set
                {
                    m_ProcessorId = value;
                }
                get
                {
                    return m_ProcessorId;
                }
            }

            public Guid JuniorProcessorId
            {
                // Access member

                set
                {
                    m_JuniorProcessorId = value;
                }
                get
                {
                    return m_JuniorProcessorId;
                }
            }

            public Guid BrokerProcessorId
            {
                // Access member

                set
                {
                    m_BrokerProcessorId = value;
                }
                get
                {
                    return m_BrokerProcessorId;
                }
            }

            public Guid CorrespondentManagerId
            {
                set { m_CorrespondentManagerId = value; }
                get { return m_CorrespondentManagerId; }
            }

            public Guid CorrespondentProcessorId
            {
                set { m_CorrespondentProcessorId = value; }
                get { return m_CorrespondentProcessorId; }
            }

            public Guid CorrespondentJuniorProcessorId
            {
                set { m_CorrespondentJuniorProcessorId = value; }
                get { return m_CorrespondentJuniorProcessorId; }
            }

            public Guid CorrespondentLenderAccExecId
            {
                set { m_CorrespondentLenderAccExecId = value; }
                get { return m_CorrespondentLenderAccExecId; }
            }

            public Guid CorrespondentExternalPostCloserId
            {
                set { m_CorrespondentExternalPostCloserId = value; }
                get { return m_CorrespondentExternalPostCloserId; }
            }

            public Guid CorrespondentUnderwriterId
            {
                set { m_CorrespondentUnderwriterId = value; }
                get { return m_CorrespondentUnderwriterId; }
            }

            public Guid CorrespondentJuniorUnderwriterId
            {
                set { m_CorrespondentJuniorUnderwriterId = value; }
                get { return m_CorrespondentJuniorUnderwriterId; }
            }

            public Guid CorrespondentCreditAuditorId
            {
                set { m_CorrespondentCreditAuditorId = value; }
                get { return m_CorrespondentCreditAuditorId; }
            }

            public Guid CorrespondentLegalAuditorId
            {
                set { m_CorrespondentLegalAuditorId = value; }
                get { return m_CorrespondentLegalAuditorId; }
            }

            public Guid CorrespondentLockDeskId
            {
                set { m_CorrespondentLockDeskId = value; }
                get { return m_CorrespondentLockDeskId; }
            }

            public Guid CorrespondentPurchaserId
            {
                set { m_CorrespondentPurchaserId = value; }
                get { return m_CorrespondentPurchaserId; }
            }

            public Guid CorrespondentSecondaryId
            {
                set { m_CorrespondentSecondaryId = value; }
                get { return m_CorrespondentSecondaryId; }
            }

            public Guid MiniCorrespondentManagerId
            {
                set { m_MiniCorrespondentManagerId = value; }
                get { return m_MiniCorrespondentManagerId; }
            }

            public Guid MiniCorrespondentProcessorId
            {
                set { m_MiniCorrespondentProcessorId = value; }
                get { return m_MiniCorrespondentProcessorId; }
            }

            public Guid MiniCorrespondentJuniorProcessorId
            {
                set { m_MiniCorrespondentJuniorProcessorId = value; }
                get { return m_MiniCorrespondentJuniorProcessorId; }
            }

            public Guid MiniCorrespondentLenderAccExecId
            {
                set { m_MiniCorrespondentLenderAccExecId = value; }
                get { return m_MiniCorrespondentLenderAccExecId; }
            }

            public Guid MiniCorrespondentExternalPostCloserId
            {
                set { m_MiniCorrespondentExternalPostCloserId = value; }
                get { return m_MiniCorrespondentExternalPostCloserId; }
            }

            public Guid MiniCorrespondentUnderwriterId
            {
                set { m_MiniCorrespondentUnderwriterId = value; }
                get { return m_MiniCorrespondentUnderwriterId; }
            }

            public Guid MiniCorrespondentJuniorUnderwriterId
            {
                set { m_MiniCorrespondentJuniorUnderwriterId = value; }
                get { return m_MiniCorrespondentJuniorUnderwriterId; }
            }

            public Guid MiniCorrespondentCreditAuditorId
            {
                set { m_MiniCorrespondentCreditAuditorId = value; }
                get { return m_MiniCorrespondentCreditAuditorId; }
            }

            public Guid MiniCorrespondentLegalAuditorId
            {
                set { m_MiniCorrespondentLegalAuditorId = value; }
                get { return m_MiniCorrespondentLegalAuditorId; }
            }

            public Guid MiniCorrespondentLockDeskId
            {
                set { m_MiniCorrespondentLockDeskId = value; }
                get { return m_MiniCorrespondentLockDeskId; }
            }

            public Guid MiniCorrespondentPurchaserId
            {
                set { m_MiniCorrespondentPurchaserId = value; }
                get { return m_MiniCorrespondentPurchaserId; }
            }

            public Guid MiniCorrespondentSecondaryId
            {
                set { m_MiniCorrespondentSecondaryId = value; }
                get { return m_MiniCorrespondentSecondaryId; }
            }

            public E_PmlLoanLevelAccess PmlLoanLevelAccess
            {
                // Access member

                set
                {
                    m_pmlLoanLevelAccess = value;
                }
                get
                {
                    return m_pmlLoanLevelAccess;
                }
            }

            public Guid SupervisorId
            {
                // Access member

                set
                {
                    m_SupervisorId = value;
                }
                get
                {
                    return m_SupervisorId;
                }
            }

            public bool IsSupervisor
            {
                // Access member

                set
                {
                    m_IsSupervisor = value;
                }
                get
                {
                    return m_IsSupervisor;
                }
            }

            public DateTime PasswordExpirationDate
            {
                // Access member

                set
                {
                    m_PasswordExpirationDate = value;
                }
                get
                {
                    return m_PasswordExpirationDate;
                }
            }

            public Int32 PasswordExpiresEvery
            {
                // Access member

                set
                {
                    m_PasswordExpiresEvery = value;
                }
                get
                {
                    return m_PasswordExpiresEvery;
                }
            }

            public Guid ManagerId
            {
                // Access member

                set
                {
                    m_ManagerId = value;
                }
                get
                {
                    return m_ManagerId;
                }
            }

            public Guid BranchId
            {
                // Access member

                set
                {
                    m_BranchId = value;
                }
                get
                {
                    return m_BranchId;
                }
            }

            public bool UseOriginatingCompanyMiniCorrBranchId
            {
                get { return this.m_useOriginatingCompanyMiniCorrBranchId; }
                set { this.m_useOriginatingCompanyMiniCorrBranchId = value; }
            }

            public Guid MiniCorrespondentBranchId
            {
                get { return this.m_miniCorrespondentBranchId; }
                set { this.m_miniCorrespondentBranchId = value; }
            }

            public bool UseOriginatingCompanyCorrBranchId
            {
                get { return this.m_useOriginatingCompanyCorrBranchId; }
                set { this.m_useOriginatingCompanyCorrBranchId = value; }
            }

            public Guid CorrespondentBranchId
            {
                get { return this.m_correspondentBranchId; }
                set { this.m_correspondentBranchId = value; }
            }

            public string RoleId
            {
                // Access member

                set
                {
                    m_RoleId = value;
                }
                get
                {
                    return m_RoleId;
                }
            }

            public String Login
            {
                // Access member

                set
                {
                    m_Login = value;
                }
                get
                {
                    return m_Login;
                }
            }

            public String Password
            {
                // Access member

                set
                {
                    m_Password = value;
                }
                get
                {
                    return m_Password;
                }
            }

            public Guid PriceGroup
            {
                // Access member

                set
                {
                    m_PriceGroup = value;
                }
                get
                {
                    return m_PriceGroup;
                }
            }

            public bool UseOriginatingCompanyMiniCorrPriceGroupId
            {
                get { return this.m_useOriginatingCompanyMiniCorrPriceGroupId; }
                set { this.m_useOriginatingCompanyMiniCorrPriceGroupId = value; }
            }

            public Guid MiniCorrespondentPriceGroupId
            {
                get { return this.m_miniCorrespondentPriceGroupId; }
                set { this.m_miniCorrespondentPriceGroupId = value; }
            }

            public bool UseOriginatingCompanyCorrPriceGroupId
            {
                get { return this.m_useOriginatingCompanyCorrPriceGroupId; }
                set { this.m_useOriginatingCompanyCorrPriceGroupId = value; }
            }

            public Guid CorrespondentPriceGroupId
            {
                get { return this.m_correspondentPriceGroupId; }
                set { this.m_correspondentPriceGroupId = value; }
            }

            public String UserType
            {
                // Access member

                set
                {
                    m_UserType = value;
                }
                get
                {
                    return m_UserType;
                }
            }

            public Boolean IsSaved
            {
                // Access member.

                set
                {
                    m_IsSaved = value;
                }
                get
                {
                    return m_IsSaved;
                }

            }

            public Guid? TPOLandingPageID 
            {
                get { return this.m_tpoLandingPageID; }
                set { this.m_tpoLandingPageID = value; }
            }

            public Guid? MiniCorrespondentLandingPageID
            {
                get { return this.m_miniCorrespondentLandingPageID; }
                set { this.m_miniCorrespondentLandingPageID = value; }
            }

            public Guid? CorrespondentLandingPageID
            {
                get { return this.m_correspondentLandingPageID; }
                set { this.m_correspondentLandingPageID = value; }
            }

            public EmployeePopulationMethodT PopulateBrokerRelationshipsT { get; set; }
            public EmployeePopulationMethodT PopulateMiniCorrRelationshipsT { get; set; }
            public EmployeePopulationMethodT PopulateCorrRelationshipsT { get; set; }
            public EmployeePopulationMethodT PopulatePMLPermissionsT { get; set; }
            #endregion

        }

        public ArrayList Items
        {
            // Access member.

            set
            {
                m_Set = value;
            }
            get
            {
                return m_Set;
            }
        }

        public int Count
        {
            // Access member.

            get
            {
                return m_Set.Count;
            }
        }

        /// <summary>
        /// Handle each import independent of the others.  Check the is-saved
        /// flag to see who didn't make it.  No 2 employees should share the
        /// same login name.
        /// </summary>

        public void Save(Guid brokerId, Guid whoDoneIt, AbstractUserPrincipal principal, bool isWebserviceCall = false)
        {
            // Handle each import independent of the others.  Check the
            // is-saved flag to see who didn't make it.  We skip entries
            // that are marked as already saved.

            BrokerUserPermissions bUp = new BrokerUserPermissions();
            try
            {
                bUp.Retrieve(brokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import employees into " + brokerId + ".", e);

                return;
            }

            

            foreach (Desc eD in m_Set)
            {
                EmployeeDB empDb = new EmployeeDB();

                empDb.WhoDoneIt = whoDoneIt;
                empDb.BrokerID = brokerId;

                if (eD.IsSaved == false)
                {

                    empDb.UserType = eD.UserType[0];
                    empDb.FirstName = eD.FirstName;
                    empDb.MiddleName = eD.MiddleName;
                    empDb.LastName = eD.LastName;
                    empDb.PmlBrokerId = eD.PmlBrokerId; 
                    empDb.BranchID = eD.BranchId;
                    empDb.LpePriceGroupID = eD.PriceGroup;
                    empDb.MiniCorrespondentBranchID = eD.MiniCorrespondentBranchId;
                    empDb.UseOriginatingCompanyMiniCorrBranchId = eD.UseOriginatingCompanyMiniCorrBranchId;
                    empDb.CorrespondentBranchID = eD.CorrespondentBranchId;
                    empDb.UseOriginatingCompanyCorrBranchId = eD.UseOriginatingCompanyCorrBranchId;

                    if (isWebserviceCall)
                    {
                        if (empDb.UserType == 'P')
                        {
                            empDb.PopulateBrokerRelationshipsT = eD.PopulateBrokerRelationshipsT;
                            empDb.PopulateMiniCorrRelationshipsT = eD.PopulateMiniCorrRelationshipsT;
                            empDb.PopulateCorrRelationshipsT = eD.PopulateCorrRelationshipsT;
                            empDb.PopulatePMLPermissionsT = eD.PopulatePMLPermissionsT;
                        }
                        else
                        {
                            empDb.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                            empDb.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                            empDb.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                            empDb.PopulatePMLPermissionsT = EmployeePopulationMethodT.IndividualUser;
                        }
                    }

                    empDb.MiniCorrespondentPriceGroupID = eD.MiniCorrespondentPriceGroupId;
                    empDb.UseOriginatingCompanyMiniCorrPriceGroupId = eD.UseOriginatingCompanyMiniCorrPriceGroupId;
                    empDb.CorrespondentPriceGroupID = eD.CorrespondentPriceGroupId;
                    empDb.UseOriginatingCompanyCorrPriceGroupId = eD.UseOriginatingCompanyCorrPriceGroupId;

                    empDb.TPOLandingPageID = eD.TPOLandingPageID;
                    empDb.MiniCorrespondentLandingPageID = eD.MiniCorrespondentLandingPageID;
                    empDb.CorrespondentLandingPageID = eD.CorrespondentLandingPageID;

                    empDb.Address.StreetAddress = eD.Street;
                    empDb.Address.City = eD.City;
                    empDb.Address.State = eD.State;
                    empDb.Address.Zipcode = eD.Zipcode;
                    empDb.Phone = eD.Phone;
                    empDb.Fax = eD.Fax;
                    empDb.CellPhone = eD.CellPhone;
                    if (eD.IsCellphoneForMultiFactorOnly.HasValue)
                    {
                        empDb.IsCellphoneForMultiFactorOnly = eD.IsCellphoneForMultiFactorOnly.Value;
                    }
                    if (eD.EnableAuthCodeViaSms.HasValue)
                    {
                        // Only override the default if explictly added.
                        empDb.EnableAuthCodeViaSms = eD.EnableAuthCodeViaSms.Value;
                    }
                    empDb.Pager = eD.Pager;
                    empDb.Email = eD.Email;
                    empDb.IsPmlManager = eD.IsSupervisor;
                    if (!eD.IsSupervisor)
                        empDb.PmlExternalManagerEmployeeId = eD.SupervisorId;

                    empDb.NotesByEmployer = eD.Notes;

                    empDb.LoginName = eD.Login;
                    empDb.Password = eD.Password;
                    empDb.PasswordExpirationD = eD.PasswordExpirationDate;
                    empDb.PasswordExpirationPeriod = eD.PasswordExpiresEvery;
                    empDb.ManagerEmployeeID = eD.ManagerId;
                    empDb.LockDeskEmployeeID = eD.LockDeskId;
                    empDb.LenderAcctExecEmployeeID = eD.LenderAccountExecId;
                    empDb.UnderwriterEmployeeID = eD.UnderwriterId;
                    empDb.JuniorUnderwriterEmployeeID = eD.JuniorUnderwriterId;
                    empDb.ProcessorEmployeeID = eD.ProcessorId;
                    empDb.JuniorProcessorEmployeeID = eD.JuniorProcessorId;
                    empDb.BrokerProcessorEmployeeId = eD.BrokerProcessorId;
                    empDb.CorrManagerEmployeeId = eD.CorrespondentManagerId;
                    empDb.CorrProcessorEmployeeId = eD.CorrespondentProcessorId;
                    empDb.CorrJuniorProcessorEmployeeId = eD.CorrespondentJuniorProcessorId;
                    empDb.CorrLenderAccExecEmployeeId = eD.CorrespondentLenderAccExecId;
                    empDb.CorrExternalPostCloserEmployeeId = eD.CorrespondentExternalPostCloserId;
                    empDb.CorrUnderwriterEmployeeId = eD.CorrespondentUnderwriterId;
                    empDb.CorrJuniorUnderwriterEmployeeId = eD.CorrespondentJuniorUnderwriterId;
                    empDb.CorrCreditAuditorEmployeeId = eD.CorrespondentCreditAuditorId;
                    empDb.CorrLegalAuditorEmployeeId = eD.CorrespondentLegalAuditorId;
                    empDb.CorrLockDeskEmployeeId = eD.CorrespondentLockDeskId;
                    empDb.CorrPurchaserEmployeeId = eD.CorrespondentPurchaserId;
                    empDb.CorrSecondaryEmployeeId = eD.CorrespondentSecondaryId;
                    empDb.MiniCorrManagerEmployeeId = eD.MiniCorrespondentManagerId;
                    empDb.MiniCorrProcessorEmployeeId = eD.MiniCorrespondentProcessorId;
                    empDb.MiniCorrJuniorProcessorEmployeeId = eD.MiniCorrespondentJuniorProcessorId;
                    empDb.MiniCorrLenderAccExecEmployeeId = eD.MiniCorrespondentLenderAccExecId;
                    empDb.MiniCorrExternalPostCloserEmployeeId = eD.MiniCorrespondentExternalPostCloserId;
                    empDb.MiniCorrUnderwriterEmployeeId = eD.MiniCorrespondentUnderwriterId;
                    empDb.MiniCorrJuniorUnderwriterEmployeeId = eD.MiniCorrespondentJuniorUnderwriterId;
                    empDb.MiniCorrCreditAuditorEmployeeId = eD.MiniCorrespondentCreditAuditorId;
                    empDb.MiniCorrLegalAuditorEmployeeId = eD.MiniCorrespondentLegalAuditorId;
                    empDb.MiniCorrLockDeskEmployeeId = eD.MiniCorrespondentLockDeskId;
                    empDb.MiniCorrPurchaserEmployeeId = eD.MiniCorrespondentPurchaserId;
                    empDb.MiniCorrSecondaryEmployeeId = eD.MiniCorrespondentSecondaryId;
                    empDb.LicenseInformationList = eD.LicenseInformationList;
                    empDb.PmlLevelAccess = eD.PmlLoanLevelAccess;
                    empDb.LosIdentifier = eD.NmlsIdentifier;

                    if (eD.OriginatorCompensationSetLevelT.HasValue)
                    {
                        empDb.OriginatorCompensationSetLevelT = eD.OriginatorCompensationSetLevelT.Value;
                    }

                    if ( eD.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.HasValue)
                    {
                        empDb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = eD.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Value;
                    }

                    if (eD.OriginatorCompensationPercent.HasValue)
                    {
                        empDb.OriginatorCompensationPercent = eD.OriginatorCompensationPercent.Value;
                    }
                    if (eD.OriginatorCompensationBaseT.HasValue)
                    {
                        empDb.OriginatorCompensationBaseT = eD.OriginatorCompensationBaseT.Value;
                    }
                    if (eD.OriginatorCompensationMinAmount.HasValue)
                    {
                        empDb.OriginatorCompensationMinAmount = eD.OriginatorCompensationMinAmount.Value;
                    }
                    if (eD.OriginatorCompensationMaxAmount.HasValue)
                    {
                        empDb.OriginatorCompensationMaxAmount = eD.OriginatorCompensationMaxAmount.Value;
                    }
                    if (eD.OriginatorCompensationFixedAmount.HasValue)
                    {
                        empDb.OriginatorCompensationFixedAmount = eD.OriginatorCompensationFixedAmount.Value;
                    }
                    if (eD.OriginatorCompensationNotes != null)
                    {
                        empDb.OriginatorCompensationNotes = eD.OriginatorCompensationNotes;
                    }
                    if (eD.CustomPricingPolicyField1Fixed != null)
                    {
                        empDb.CustomPricingPolicyField1Fixed = eD.CustomPricingPolicyField1Fixed;
                    }
                    if (eD.CustomPricingPolicyField2Fixed != null)
                    {
                        empDb.CustomPricingPolicyField2Fixed = eD.CustomPricingPolicyField2Fixed;
                    }
                    if (eD.CustomPricingPolicyField3Fixed != null)
                    {
                        empDb.CustomPricingPolicyField3Fixed = eD.CustomPricingPolicyField3Fixed;
                    }
                    if (eD.CustomPricingPolicyField4Fixed != null)
                    {
                        empDb.CustomPricingPolicyField4Fixed = eD.CustomPricingPolicyField4Fixed;
                    }
                    if (eD.CustomPricingPolicyField5Fixed != null)
                    {
                        empDb.CustomPricingPolicyField5Fixed = eD.CustomPricingPolicyField5Fixed;
                    }
                    if (eD.CustomPricingPolicyFieldValueSource.HasValue)
                    {
                        empDb.CustomPricingPolicyFieldValueSource = eD.CustomPricingPolicyFieldValueSource.Value;
                    }
                    if (eD.AccessiblePmlNavigationLinksId != null)
                    {
                        empDb.AccessiblePmlNavigationLinksId = eD.AccessiblePmlNavigationLinksId;
                    }
                    if (eD.DOAutoLoginName != null)
                    {
                        empDb.DOAutoLoginName = eD.DOAutoLoginName;
                    }
                    if (eD.DOAutoPassword != null)
                    {
                        empDb.DOAutoPassword = eD.DOAutoPassword;
                    }
                    if (eD.DUAutoLoginName != null)
                    {
                        empDb.DUAutoLoginName = eD.DUAutoLoginName;
                    }
                    if (eD.DUAutoPassword != null)
                    {
                        empDb.DUAutoPassword = eD.DUAutoPassword;
                    }

                    //Commenting per OPM 12941, will delete once it is verified that there was never a valid reason to have this here - db 5/8/07
                    //empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.ReceiveEmail;

                    empDb.Roles = eD.RoleId;

                    empDb.IsSharable = true;
                    empDb.AllowLogin = true;
                    empDb.IsActive = true;

                    try
                    {
                        using (CStoredProcedureExec spExec = new CStoredProcedureExec(brokerId))
                        {
                            try
                            {
                                spExec.BeginTransactionForWrite();
                                empDb.Save(spExec, principal);

                                Dictionary<Permission, string> permissions = new Dictionary<Permission, string>();
                                permissions.Add(Permission.AllowCreatingWholesaleChannelLoans, eD.CreateWholesaleChannelLoans);
                                permissions.Add(Permission.AllowCreatingMiniCorrChannelLoans, eD.CreateMiniCorrespondentChannelLoans);
                                permissions.Add(Permission.AllowCreatingCorrChannelLoans, eD.CreateCorrespondentChannelLoans);
                                permissions.Add(Permission.CanRunPricingEngineWithoutCreditReport, eD.RunWithoutReport);
                                permissions.Add(Permission.CanSubmitWithoutCreditReport, eD.SubmitWithoutReport);
                                permissions.Add(Permission.CanApplyForIneligibleLoanPrograms, eD.ApplyForIneligibleLoanPrograms);
                                permissions.Add(Permission.AllowViewingWholesaleChannelLoans, eD.AllowViewingWholesaleChannelLoans);
                                permissions.Add(Permission.AllowViewingMiniCorrChannelLoans, eD.AllowViewingMiniCorrChannelLoans);
                                permissions.Add(Permission.AllowViewingCorrChannelLoans, eD.AllowViewingCorrChannelLoans);
                                foreach (KeyValuePair<Permission, string> p in permissions)
                                {
                                    if (p.Value != String.Empty && p.Value.ToLower() != "n")
                                    {
                                        bUp.SetPermission(p.Key, true);
                                    }
                                    else
                                    {
                                        bUp.SetPermission(p.Key, false);
                                    }
                                }

                                bUp.Update(spExec, empDb.ID);

                                spExec.CommitTransaction();

                                eD.IsSaved = true;

                            }
                            catch (Exception e)
                            {
                                // 08/23/06 mf. This transaction sometimes gets rolled back
                                // at the server, so we drop the InvalidOperationException
                                try
                                {
                                    spExec.RollbackTransaction();
                                }
                                catch (InvalidOperationException exc)
                                {
                                    Tools.LogError("Could not rollback transaction. Probably rolledback at server.", exc);
                                }

                                throw e;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // Oops!

                        Tools.LogError("Failed to import " + eD.Login + ".", e);
                    }
                }
            }
        }

        /// <summary>
        /// Add a new employee descriptor.  If named employee already
        /// exists in the set, then we ignore the add.
        /// </summary>

        public void Add(Desc eDesc)
        {
            // Add a new employee descriptor.  If employee with login
            // already exists in the set, then we ignore the add.

            foreach (Desc eD in m_Set)
            {
                if (eD.Login.ToLower() == eDesc.Login.ToLower())
                {
                    throw new ArgumentException("Login already part of this set.");
                }
            }

            m_Set.Add(eDesc);
        }

        /// <summary>
        /// Return looping interface for walking descriptors in a
        /// for loop construct.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return looping interface for walking descriptors.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Clear out the import set.  We do this when loading up
        /// a new batch.
        /// </summary>

        public void Clear()
        {
            // Nix all entries from this set.

            m_Set.Clear();
        }

    }
}
