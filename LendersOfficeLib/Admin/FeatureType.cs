﻿namespace LendersOffice.Admin
{
    /// <summary>
    /// Enum representation of tracked features.
    /// The number representation of the enum MUST match the FeatureId column in TRACKED_FEATURE table in DB.
    /// </summary>
    /// <remarks>Be careful not to add enums with same value across different environments and check stash to see if there are other enums being added.</remarks>
    public enum FeatureType
    {
        /// <summary>
        /// Price My Loan 2.0 (PML with new UI).
        /// </summary>
        PML2 = 0,

        /// <summary>
        /// Historical Pricing feature.
        /// </summary>
        HistoricalPricing = 1,

        /// <summary>
        /// Seamless Desktop Underwriter.
        /// </summary>
        SeamlessDu = 2,

        /// <summary>
        /// Seamless Loan Product Advisor.
        /// </summary>
        SeamlessLpa = 3,

        /// <summary>
        /// Historical Pricing feature, but only determines brokers that are testing this feature.
        /// </summary>
        HistoricalPricingTesting = 4,

        /// <summary>
        /// PML 3.0 feature.
        /// </summary>
        PML3 = 5,

        /// <summary>
        /// PML 3.0 feature, but only determines brokers that are testing this feature.
        /// </summary>
        PML3Testing = 6,
    }
}
