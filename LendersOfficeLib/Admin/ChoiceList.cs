﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Keep track of individual items.  We store them in
    /// an array list.  At some point, we should guarantee
    /// uniqueness.
    /// </summary>

    [XmlRoot]
    [Serializable]
    public class ChoiceList
    {
        /// <summary>
        /// Keep track of individual items.  We store
        /// them in an array list.  At some point, we
        /// should guarantee uniqueness.
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        #region ( Set properties )

        [XmlArray]
        [XmlArrayItem(typeof(ChoiceItem))]
        public ArrayList Choices
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        #endregion

        /// <summary>
        /// Add a new choice item if not already present.
        /// </summary>
        public void Push(String sCategory, String sDescription)
        {
            // Add a new choice item only if it is unique.

            if (!IsExisted(sCategory, sDescription))
            {
                ChoiceItem cItem = new ChoiceItem();

                cItem.Category = sCategory;
                cItem.Description = sDescription;
                m_Set.Insert(0, cItem);

            }

        }

        /// <summary>
        /// Add a new choice item if not already present.
        /// </summary>

        public void Add(String sCategory, String sDescription)
        {
            // Add a new choice item only if it is unique.
            if (!IsExisted(sCategory, sDescription))
            {
                ChoiceItem cItem = new ChoiceItem();

                cItem.Category = sCategory;
                cItem.Description = sDescription;
                m_Set.Add(cItem);

            }
        }

        private bool IsExisted(string category, string description)
        {
            foreach (ChoiceItem item in m_Set)
            {
                if (item.Category == category && item.Description == description)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Sort the array using the items as the items as
        /// comparers.
        /// </summary>

        public void Sort(params int[] sortSpec)
        {
            // Sort the array using the items as the
            // comparers.

            ChoiceComparer cC = new ChoiceComparer(sortSpec);

            m_Set.Sort(cC);
        }

        #region ( Comparer implementation )

        /// <summary>
        /// Compare two instances for sorting and return the
        /// relative ordering.
        /// </summary>

        private class ChoiceComparer : IComparer
        {
            /// <summary>
            /// Remember what the sort order is.
            /// </summary>

            private int[] m_SortSpec = new int[0];

            /// <summary>
            /// Compare two instances for sorting and return
            /// the relative ordering.  Note that if no spec
            /// is given, we use the default sorting.
            /// </summary>
            /// <returns>
            /// Result based on compare convention (0,+1,-1).
            /// </returns>

            public int Compare(object oA, object oB)
            {
                ChoiceItem cA = oA as ChoiceItem;
                ChoiceItem cB = oB as ChoiceItem;

                if (cA == null || cB == null)
                {
                    throw new InvalidCastException("Comparing non choice items is not allowed.");
                }

                foreach (int iField in m_SortSpec)
                {
                    int res = 0;

                    if (iField == 1)
                    {
                        res = cA.Description.CompareTo(cB.Description);
                    }
                    else
                        if (iField == 0)
                        {
                            res = cA.Category.CompareTo(cB.Category);
                        }

                    if (res != 0)
                    {
                        return res;
                    }
                }

                if (m_SortSpec.Length == 0)
                {
                    return cA.CompareTo(cB);
                }

                return 0;
            }

            #region ( Constructors )

            /// <summary>
            /// Construct default.
            /// </summary>

            internal ChoiceComparer(int[] sortSpec)
            {
                // Initialize members.

                m_SortSpec = sortSpec;
            }

            #endregion

        }

        #endregion

        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our list.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>

        public static ChoiceList ToObject(String sXml)
        {
            // Deserialize the given string to our list.

            XmlSerializer xS = new XmlSerializer(typeof(ChoiceList));
            StringReader sR = new StringReader(sXml);

            return xS.Deserialize(sR) as ChoiceList;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            // Serialize this instance as an xml document.

            XmlSerializer xS = new XmlSerializer(typeof(ChoiceList));
            StringWriter8 s8 = new StringWriter8();

            xS.Serialize(s8, this);

            return s8.ToString();
        }

        #endregion

    }
}
