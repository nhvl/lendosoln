﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Security;

    /// <summary>
    /// Provides functionality for exporting employee information with permissions.
    /// </summary>
    public class EmployeeExporterWithPermissions : EmployeeExporter
    {
        /// <summary>
        /// The permissions that are available and applicable to this lender's users.
        /// </summary>
        /// <remarks>
        /// The list of permissions displayed in the employee editor is dependent upon
        /// lender settings, so we keep track of which permissions admins will see and
        /// only export those.
        /// </remarks>
        private List<BrokerUserPermissionTable.Desc> availablePermissions;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeExporterWithPermissions"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        public EmployeeExporterWithPermissions(Guid brokerId)
            : base(brokerId)
        {
        }

        /// <summary>
        /// Loads the data required for export.
        /// </summary>
        protected override void LoadData()
        {
            this.LoadPermissionsApplicableToLender();
            base.LoadData();
        }

        /// <summary>
        /// Gets the header for the csv file.
        /// </summary>
        /// <returns>A StringBuilder that contains the header.</returns>
        protected override StringBuilder GetHeader()
        {
            var header = base.GetHeader();
            header.Append(",");
            header.Append(string.Join(",", this.availablePermissions.Select(d => DataParsing.sanitizeForCSV(d.Label))));
            return header;
        }

        /// <summary>
        /// Gets the row to use for the given employee.
        /// </summary>
        /// <param name="eD">The employee details.</param>
        /// <returns>A StringBuilder that contains the row.</returns>
        protected override StringBuilder GetEmployeeRow(EmployeeDetails eD)
        {
            var row = base.GetEmployeeRow(eD);
            var userPermissions = this.UserPermissions[eD.EmployeeId];

            foreach (var permission in this.availablePermissions.Select(d => d.Code))
            {
                row.Append(",");
                row.Append(userPermissions != null && userPermissions.HasPermission(permission) ? "Yes" : "No");
            }

            return row;
        }

        /// <summary>
        /// Loads the permissions that will be available to the lender's users.
        /// </summary>
        private void LoadPermissionsApplicableToLender()
        {
            this.availablePermissions = new List<BrokerUserPermissionTable.Desc>();
            var broker = BrokerDB.RetrieveById(this.BrokerId);
            var permissionSet = new HashSet<Permission>();
            var permissionControls = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription))
                .Cast<BrokerUserPermissionTable.ControlDescription>();

            foreach (var control in permissionControls)
            {
                var table = BrokerUserPermissionTable.GetPermissionTable(
                    control,
                    broker.HasFeatures(E_BrokerFeatureT.PriceMyLoan),
                    broker.IsDataTracIntegrationEnabled,
                    broker.IsEDocsEnabled,
                    broker.IsTotalScorecardEnabled, 
                    broker.IsOCREnabled,
                    broker.HasFeatures(E_BrokerFeatureT.MarketingTools));

                foreach (BrokerUserPermissionTable.Desc desc in table)
                {
                    if (permissionSet.Add(desc.Code))
                    {
                        this.availablePermissions.Add(desc);
                    }
                }
            }
        }
    }
}
