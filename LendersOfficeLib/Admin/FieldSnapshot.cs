﻿// <copyright file="FieldSnapshot.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   7/15/2015
// </summary>

namespace LendersOffice.Admin
{
    using System;

    /// <summary>
    /// Keeps a original field value and a updated value.
    /// </summary>
    public sealed class FieldSnapshot
    {
        /// <summary>
        /// The latest value of the snapshot.
        /// </summary>
        private object latestValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldSnapshot" /> class.
        /// </summary>
        /// <param name="original">The original value of the field.</param>        
        public FieldSnapshot(object original)
        {
            this.OriginalValue = original;
        }

        /// <summary>
        /// Gets the original value.
        /// </summary>
        /// <value>The original value.</value>
        public object OriginalValue { get; private set; }

        /// <summary>
        /// Gets the latest value.
        /// </summary>
        /// <value>The new value.</value>
        public object LatestValue
        {
            get
            {
                return this.latestValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the field has a new value.
        /// </summary>
        /// <value>True if the newest value has changed.</value>
        public bool HasLatestValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current value of the field. Will return latest 
        /// if it has changed otherwise it returns original.
        /// </summary>
        /// <value>The current value of the field.</value>
        public object CurrentValue
        {
            get
            {
                if (this.HasLatestValue)
                {
                    return this.LatestValue;
                }

                return this.OriginalValue;
            }
        }

        /// <summary>
        /// Gets the field type. 
        /// </summary>
        /// <value>The type of the field.</value>
        public Type FieldType { get; private set; }

        /// <summary>
        /// Set the latest value. 
        /// </summary>
        /// <param name="value">The value to set the field to.</param>
        public void SetLatestValue(object value)
        {
            this.latestValue = value;
            this.HasLatestValue = true;
        }

        /// <summary>
        /// Clears the latest value.
        /// </summary>
        public void ClearLatestValue()
        {
            this.latestValue = null;
            this.HasLatestValue = false;
        }
    }
}