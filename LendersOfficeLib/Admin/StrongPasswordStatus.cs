﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    public enum StrongPasswordStatus
    {
        OK,
        PasswordMustBeAlphaNumeric,
        PasswordTooShort,
        PasswordHasUnverifiedCharacters,
        PasswordContainsConsecutiveIdenticalChars,
        PasswordContainsConsecutiveAlphanumericChars,
        PasswordRecycle,
        PasswordContainsLogin
    }
}
