﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using CommonLib;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Events;
using LendersOffice.Security;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Admin
{
    public class UserSecurityQuestionAnswerItem
    {
        public int? SecurityQuestion1Id { get; set; }
        public string SecurityQuestion1Answer { get; set; }

        public int? SecurityQuestion2Id { get; set; }
        public string SecurityQuestion2Answer { get; set; }

        public int? SecurityQuestion3Id { get; set; }
        public string SecurityQuestion3Answer { get; set; }

        public bool HasMissingQuestionAnswer
        {
            get { return this.SecurityQuestion1Id.HasValue == false || this.SecurityQuestion2Id.HasValue == false || this.SecurityQuestion3Id.HasValue == false; }
        }
    }

    public class BrokerUserDB
    {
        private const int MAX_PASS_RESET_ATTEMPTS = 5;

        public static UserSecurityQuestionAnswerItem GetSecurityQuestionAnswer(Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };

            int? question1Id = null, question2Id = null, question3Id = null;
            string question1Answer = string.Empty, question2Answer = string.Empty, question3Answer = string.Empty;
            var maybeEncryptionKeyId = default(LqbGrammar.DataTypes.EncryptionKeyIdentifier?);
            byte[] encryptedQuestion1Answer = null, encryptedQuestion2Answer = null, encryptedQuestion3Answer = null;
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_USER_GetSecurityQuestionAnswer", parameters))
            {
                if (reader.Read())
                {
                    maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]); // EncryptionTODO: force a value here

                    question1Id = reader.GetNullableInt("SecurityQuestion1Id");
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        encryptedQuestion1Answer = (byte[])reader["SecurityQuestion1EncryptedAnswer"];
                    }
                    else
                    {
                        question1Answer = (string)reader["SecurityQuestion1Answer"];
                    }

                    question2Id = reader.GetNullableInt("SecurityQuestion2Id");
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        encryptedQuestion2Answer = (byte[])reader["SecurityQuestion2EncryptedAnswer"];
                    }
                    else
                    {
                        question2Answer = (string)reader["SecurityQuestion2Answer"];
                    }

                    question3Id = reader.GetNullableInt("SecurityQuestion3Id");
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        encryptedQuestion3Answer = (byte[])reader["SecurityQuestion3EncryptedAnswer"];
                    }
                    else
                    {
                        question3Answer = (string)reader["SecurityQuestion3Answer"];
                    }
                }
            }

            return new UserSecurityQuestionAnswerItem()
            {
                SecurityQuestion1Id = question1Id,
                SecurityQuestion1Answer = maybeEncryptionKeyId.HasValue ? EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedQuestion1Answer) : question1Answer,
                SecurityQuestion2Id = question2Id,
                SecurityQuestion2Answer = maybeEncryptionKeyId.HasValue ? EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedQuestion2Answer) : question2Answer,
                SecurityQuestion3Id = question3Id,
                SecurityQuestion3Answer = maybeEncryptionKeyId.HasValue ? EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedQuestion3Answer) : question3Answer,
            };
        }

        public static bool AreSecurityAnswersSetForTheUser(Guid brokerId, Guid guidUserId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
                                            
            parameters.Add(new SqlParameter("@UserId", guidUserId));
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            parameters.Add(paramReturn);

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "SecurityAnswersSavedForUser", 0, parameters);
            return ((int)paramReturn.Value == 1);
        }

        /// <summary>
        /// Retrieves the encryption key identifier for a given user. 
        /// </summary>
        /// <param name="brokerId">The identifier of the lender.</param>
        /// <param name="userId">The identifier of the user.</param>
        /// <returns>The valid encryption key identifier, or null if no valid encryption key identifier was found.</returns>
        public static LqbGrammar.DataTypes.EncryptionKeyIdentifier? RetrieveEncryptionKey(Guid brokerId, Guid userId)
        {
            Guid? encryptionKeyIdRaw = default(Guid?);
            var parameters = new[] { new SqlParameter("@UserId", userId), };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_USER_RetrieveEncryptionKeyId", parameters))
            {
                if (reader.Read())
                {
                    encryptionKeyIdRaw = (Guid)reader["EncryptionKeyId"];
                }
            }

            return encryptionKeyIdRaw.HasValue ? LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create(encryptionKeyIdRaw.Value) : null;
        }
        
        public static bool SaveSecurityQuestionsAnswers(Guid brokerId, Guid userId, int nQuestionId1, string sAnswer1, int nQuestionId2, string sAnswer2, int nQuestionId3, string sAnswer3)
        {
            var encryptKeyId = RetrieveEncryptionKey(brokerId, userId);
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@Question1Id", nQuestionId1),
                new SqlParameter("@Answer1", sAnswer1),
                new SqlParameter("@EncryptedAnswer1", encryptKeyId.HasValue ? EncryptionHelper.EncryptString(encryptKeyId.Value, sAnswer1) : null),
                new SqlParameter("@Question2Id", nQuestionId2),
                new SqlParameter("@Answer2", sAnswer2),
                new SqlParameter("@EncryptedAnswer2", encryptKeyId.HasValue ? EncryptionHelper.EncryptString(encryptKeyId.Value, sAnswer2) : null),
                new SqlParameter("@Question3Id", nQuestionId3),
                new SqlParameter("@Answer3", sAnswer3),
                new SqlParameter("@EncryptedAnswer3", encryptKeyId.HasValue ? EncryptionHelper.EncryptString(encryptKeyId.Value, sAnswer3) : null),
                paramReturn,
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "SaveSecurityQuestionsAnswers", 0, parameters);
            return ((int)paramReturn.Value == 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUserId"></param>
        /// <param name="sAnswer1"></param>
        /// <param name="sAnswer2"></param>
        /// <param name="sAnswer3"></param>
        /// <returns>
        /// 1 : Matches
        /// -1 :User not found/Inactive user
        /// -2 : Answers don't match
        /// </returns>
        public static int MatchSecurityQuestionsAnswers(Guid brokerId, Guid userId, string sAnswer1, string sAnswer2, string sAnswer3, out int nAttempts)
        {
            UserSecurityQuestionAnswerItem answers = GetSecurityQuestionAnswer(brokerId, userId);
            bool passedSecurityQuestions = StringComparer.OrdinalIgnoreCase.Equals(answers.SecurityQuestion1Answer, sAnswer1)
                && StringComparer.OrdinalIgnoreCase.Equals(answers.SecurityQuestion2Answer, sAnswer2)
                && StringComparer.OrdinalIgnoreCase.Equals(answers.SecurityQuestion3Answer, sAnswer3);

            SqlParameter paramOutAttemptsCount = new SqlParameter("@AttemptsCount", SqlDbType.Int);
            paramOutAttemptsCount.Direction = ParameterDirection.Output;

            SqlParameter paramReturn = new SqlParameter("@ReturnValue", SqlDbType.Int);
            paramReturn.Direction = ParameterDirection.ReturnValue;
            var parameters = new[]
            {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@QuestionAnswersMatch", passedSecurityQuestions),
                paramOutAttemptsCount,
                paramReturn,
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BROKER_USER_HandleSecurityQuestionsAnswerResult", 0, parameters);
            nAttempts = (int)paramOutAttemptsCount.Value;
            return (int)paramReturn.Value;
        }

        public static Guid CreatePasswordResetRequest_Broker(string sLogin, string sEmail, out string sUserFullName, out Guid brokerId)
        {
            sUserFullName = string.Empty;
            brokerId = Guid.Empty;

            // ejm OPM 462955 - If no login and email passed in, then don't count this as a bad login.
            if (string.IsNullOrEmpty(sLogin) || string.IsNullOrEmpty(sEmail))
            {
                return Guid.Empty;
            }

            try
            {
                DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoginName(sLogin, out brokerId);

                return CreatePasswordResetRequest_Impl(connInfo, sLogin, sEmail, "B", Guid.Empty, out sUserFullName);
            }
            catch (NotFoundException)
            {
                return Guid.Empty;
            }
        }

        public static Guid CreatePasswordResetRequest_Pml(string sLogin, string sEmail, Guid guidPmlSiteId, out string sUserFullName, out Guid brokerId)
        {
            sUserFullName = string.Empty;
            
            // ejm OPM 462955 - If no login and email passed in, then don't count this as a bad login.
            if (string.IsNullOrEmpty(sLogin) || string.IsNullOrEmpty(sEmail))
            {
                brokerId = Guid.Empty;
                return Guid.Empty;
            }

            brokerId = BrokerDB.GetBrokerIdByBrokerPmlSiteId(guidPmlSiteId);

            if (brokerId == Guid.Empty)
            {
                return Guid.Empty;
            }

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            return CreatePasswordResetRequest_Impl(connInfo, sLogin, sEmail, "P", guidPmlSiteId, out sUserFullName);
        }

        private static Guid CreatePasswordResetRequest_Impl(DbConnectionInfo connInfo, string sLogin, string sEmail, string sType, Guid guidPmlSiteId, out string sUserFullName)
        {
            SqlParameter paramLogin = new SqlParameter("@Login", sLogin);
            SqlParameter paramEmail = new SqlParameter("@Email", sEmail);
            SqlParameter paramType = new SqlParameter("@Type", sType);
            SqlParameter paramPmlSiteId = new SqlParameter("@PmlSiteId", guidPmlSiteId);
        
            SqlParameter paramOutRequestGuid = new SqlParameter("@RequestID", string.Empty);
            paramOutRequestGuid.Direction = ParameterDirection.Output;
            paramOutRequestGuid.SqlDbType = SqlDbType.UniqueIdentifier;

            SqlParameter paramOutUserFullName = new SqlParameter("@UserFullName", string.Empty);
            paramOutUserFullName.Direction = ParameterDirection.Output;
            paramOutUserFullName.SqlDbType = SqlDbType.VarChar;
            paramOutUserFullName.Size = 42;
            

            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            SqlParameter[] parameters = {
                                            paramLogin,
                                            paramEmail,
                                            paramType,
                                            paramPmlSiteId,
                                            paramOutRequestGuid,
                                            paramOutUserFullName,
                                            paramReturn
                                        };

            //piggybacking to get the user full name to avoid an extra db hit. Fullname is used to send out the email
            StoredProcedureHelper.ExecuteNonQuery(connInfo, "CreatePasswordResetRequest", 0, parameters);
            int nRet = (int)paramReturn.Value;
            
            sUserFullName = SafeConvert.ToString(paramOutUserFullName.Value);

            if (nRet == 0)
            {                
                return SafeConvert.ToGuid(paramOutRequestGuid.Value);
            }
            else
            {
                return Guid.Empty;
            }
        }

        public static void IncrementPasswordResetFailureCount(string loginNm, string type, Guid BrokerPmlSiteId, string email)
        {
            // ejm OPM 462955 - If no login or email passed in, then don't count this as a bad login.
            if (string.IsNullOrEmpty(loginNm) || string.IsNullOrEmpty(email))
            {
                return;
            }

            // OPM 246520.  When an attempt to reset a password fails due to
            // the email/login combination being invalid, we must increment
            // the failure counter on:
            //   - The account associated with the login (if it exists)
            //   - ANY accounts associated with the email
            //
            // These accounts are locked if the failure occurs too many times.

            // Basically create a quick anonymous type collection for
            // the account data of who we know was affected.  We need 
            // all of this to correctly lock and email if appropriate.
            // Since we can lock by email, the account list can be
            // cross-lender (and therefore potentially cross-db).
            var affectedAccounts = Enumerable.Empty<object>()
             .Select(p =>
             new
             { 
                 BrokerId = Guid.Empty,
                 UserId = Guid.Empty,
                 EmployeeId = Guid.Empty,
                 FirstNm = string.Empty,
                 LastNm = string.Empty,
                 LoginNm = String.Empty,
                 PasswordResetFailureCount = 0,
                 LoginBlockExpirationD = DateTime.MinValue
             }).ToList();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = 
                    {
                    new SqlParameter("@LoginNm", loginNm),
                    new SqlParameter("@Type", type),
                    new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId),
                    new SqlParameter("@Email", email)
                };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "UpdatePasswordResetFailureCount", parameters))
                {
                    while (reader.Read())
                    {
                        affectedAccounts.Add(
                            new
                            {
                                BrokerId = Guid.Parse(reader["BrokerId"].ToString()),
                                UserId = Guid.Parse(reader["UserId"].ToString()),
                                EmployeeId = Guid.Parse(reader["EmployeeId"].ToString()),
                                FirstNm = reader["UserFirstNm"].ToString(),
                                LastNm = reader["UserLastNm"].ToString(),
                                LoginNm = reader["LoginNm"].ToString(),
                                PasswordResetFailureCount = (int)reader["PasswordResetFailureCount"],
                                LoginBlockExpirationD = (DateTime)reader["LoginBlockExpirationD"]
                            });
                    }
                }
            }

            foreach (var account in affectedAccounts)
            {
                var lockPrincipal = SystemUserPrincipal.SecurityLogPrincipal(account.BrokerId, account.LoginNm, account.FirstNm, account.LastNm, InitialPrincipalTypeT.NonInternal, type, account.UserId);

                if (account.PasswordResetFailureCount >= MAX_PASS_RESET_ATTEMPTS
                    && account.LoginBlockExpirationD != LendersOffice.Common.SmallDateTime.MaxValue) 
                {
                    // This unlocked account has tried too many times.
                    // Lock it and send lock notification email.

                    AllUserDB.LockAccount(account.BrokerId, account.UserId, lockPrincipal);

                    // Note that just like getting locked due to too many login failures,
                    // P users do not get an email notification.
                    if (type == "B")
                    {
                        UserAccountLockedEvent userAccLocked = new UserAccountLockedEvent();
                        userAccLocked.Initialize(account.EmployeeId, account.LoginNm, E_AccountLockedReason.UnsuccessfulPasswordResetAttempts);
                        userAccLocked.Send();
                    }
                }
                else
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(lockPrincipal, LoginSource.Website);
            }
        }
    }
}
}
