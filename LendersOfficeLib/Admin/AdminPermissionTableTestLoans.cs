﻿namespace LendersOffice.Admin
{
    using System;
    using LendersOffice.Security;

    /// <summary>
    /// The class representing the Test Loans group of the Employee Permissions List.
    /// </summary>
    public class AdminPermissionTableTestLoans : AdminPermissionTable
    {
        /// <summary>
        /// Returns true for permissions that belong in the administration group of the employee permissions editor.
        /// </summary>
        /// <param name="desc">The description object for the permission to be considered.</param>
        /// <returns>True iff the permission belongs to the administration group.</returns>
        protected override bool IsIncluded(Desc desc)
        {
            if (desc.Code == Permission.AllowCreatingTestLoans)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
