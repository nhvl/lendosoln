﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableMortgageBanking : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.AllowLockDeskRead:
                case Permission.AllowLockDeskWrite:
                case Permission.AllowCloserRead:
                case Permission.AllowCloserWrite:
                case Permission.AllowAccountantRead:
                case Permission.AllowAccountantWrite:
                case Permission.AllowEditingInvestorInformation:
                case Permission.AllowViewingInvestorInformation:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
