﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Store employee descriptors in lists per role for the specified
    /// broker.
    /// </summary>
    public class BrokerLoanAssignmentTable
    {
        /// <summary>
        /// Store employee descriptors in lists per role for the
        /// specified broker.
        /// </summary>

        private Dictionary<string, List<Spec>> m_Table = new Dictionary<string, List<Spec>>(StringComparer.OrdinalIgnoreCase);

        private Dictionary<Guid, List<Role>> m_Roles = new Dictionary<Guid, List<Role>>();

        /// <summary>
        /// Keep track of individual employee entries.
        /// </summary>

        public class Spec
        {
            /// <summary>
            /// Keep track of individual employee entries.
            /// </summary>

            private string m_FirstName = String.Empty;
            private string m_MiddleName = String.Empty;
            private string m_LastName = String.Empty;
            private string m_Suffix = String.Empty;
            private string m_Email = String.Empty;
            private string m_LoginNm = string.Empty;
            private Guid m_EmployeeId = Guid.Empty;
            private bool m_IsActive = false;
            private string m_addr = "";
            private string m_city = "";
            private string m_state = "";
            private string m_zip = "";
            private string m_phone = "";
            private string m_fax = "";
            private Guid m_branchId;
            private string m_Cellphone = string.Empty;

            private LicenseInfoList m_licenses = new LicenseInfoList();

            #region ( Spec properties )

            public string UserType
            {
                get;
                set;
            }

            public Guid UserId
            {
                get;
                set;
            }

            public string Pager
            {
                get;
                set; 
            }

            public bool IsCellphoneForMultiFactorOnly
            {
                get;
                set;
            }

            public string CellPhone
            {
                get
                {
                    return EmployeeDB.CalculatedCellphoneValue(this.m_Cellphone, this.IsCellphoneForMultiFactorOnly);
                }

                set
                {
                    this.m_Cellphone = value;
                }
            }
            public string EmployeeName
            {
                get { return m_LastName + " , " + m_FirstName; }
            }

            public string FullName
            {
                get { return m_FirstName + " " + m_LastName; }
            }

            public string FullNameWithIsActiveStatus
            {
                get { return string.Format("{0} {1} {2}", m_FirstName, m_LastName, m_IsActive ? "" : "(INACTIVE)"); }
            }

            public String FirstName
            {
                set { m_FirstName = value; }
                get { return m_FirstName; }
            }

            public String MiddleName
            {
                get { return m_MiddleName; }
                set { m_MiddleName = value; }
            }

            public String LastName
            {
                set { m_LastName = value; }
                get { return m_LastName; }
            }

            public String Suffix
            {
                get { return m_Suffix; }
                set { m_Suffix = value; }
            }

            public String LoginName
            {
                set { m_LoginNm = value; }
                get { return m_LoginNm; }
            }

            public String Email
            {
                set { m_Email = value; }
                get { return m_Email; }
            }


            public Guid EmployeeId
            {
                set { m_EmployeeId = value; }
                get { return m_EmployeeId; }
            }

            public bool IsActive
            {
                set { m_IsActive = value; }
                get { return m_IsActive; }
            }
            public string Addr
            {
                set { m_addr = value; }
                get { return m_addr; }
            }
            public string City
            {
                get { return m_city; }
                set { m_city = value; }
            }
            public string State
            {
                get { return m_state; }
                set { m_state = value; }
            }
            public string Zip
            {
                get { return m_zip; }
                set { m_zip = value; }
            }
            public string Phone
            {
                get { return m_phone; }
                set { m_phone = value; }
            }
            public string Fax
            {
                get { return m_fax; }
                set { m_fax = value; }
            }

            public Guid BranchId
            {
                get { return m_branchId; }
                set { m_branchId = value; }
            }

            public string CompanyName { get; set; }

            public Guid PmlBrokerId { get; set; }
            public string  BranchNm { get; set; }
            public string EmployeeLicenseXml 
            {
                get { return m_licenses.ToString(); }
                set { m_licenses = LicenseInfoList.ToObject(value); } 
            }
            public string CompanyLicenseXml { get; set; }

            public LicenseInfoList Licenses
            {
                get { return m_licenses; }
            }

            public void SetLicenseXmlContent(string xmLContent)
            {
                if( string.IsNullOrEmpty(xmLContent) )
                {
                    return;
                }
                m_licenses = LicenseInfoList.ToObject(xmLContent);
            }

            #endregion

        }


        public IEnumerable<Role> this[Guid employeeId]
        {
            get 
            {
                List<Role> list = null;

                if (m_Roles.TryGetValue(employeeId, out list) == false)
                {
                    return null;
                }
                return list;
            }
        }

        public IEnumerable<Spec> this[String sRole]
        {
            get
            {
                List<Spec> list = null;

                if (m_Table.TryGetValue(sRole, out list) == false)
                {
                    return null;
                }
                return list;
            }
        }

        public virtual IEnumerable<Spec> this[E_RoleT roleType] 
        {
            get 
            {
                Role role = Role.Get(roleType);

                return this[role.Desc];
            }
        }

        public void Retrieve(Guid brokerId, Guid branchId, String searchFilter, E_RoleT roleType, E_EmployeeStatusFilterT statusFilterT)
        {
            Role role = Role.Get(roleType);
            Retrieve(brokerId, branchId, searchFilter, role.Desc, statusFilterT, null, null);
        }

        /// <summary>
        /// Load the employees and organize them into lists based on
        /// the roles they support.  Each role lists the employees
        /// that fulfill the role (so an employee may belong to more
        /// than one list).
        /// </summary>
        public void Retrieve(
            Guid brokerId,
            Guid branchId,
            String searchFilter,
            String roleDesc,
            E_EmployeeStatusFilterT statusFilterT,
            E_BranchChannelT? branchChannelT,
            E_sCorrespondentProcessT? correspondentProcessT)
        {
            // List all the employees in their own table, then map them
            // to role specific lists for our lookup table.
            Dictionary<Guid, Spec> empList = new Dictionary<Guid, Spec>();
            List<SqlParameter> pA = new List<SqlParameter>();

            if (branchId != Guid.Empty)
            {
                pA.Add(new SqlParameter("@BranchId", branchId));
            }

            if (statusFilterT == E_EmployeeStatusFilterT.ActiveOnly)
            {
                pA.Add(new SqlParameter("@IsActive", true));
            }
            else if (statusFilterT == E_EmployeeStatusFilterT.InactiveOnly)
            {
                pA.Add(new SqlParameter("@IsActive", false));
            }

            if (searchFilter != null)
            {
                String[] searchParts = searchFilter.TrimWhitespaceAndBOM().Split(' ');

                if (searchParts.Length > 1 && searchParts[1].TrimWhitespaceAndBOM() != "")
                {
                    pA.Add(new SqlParameter("@LastFilter", searchParts[1]));
                }

                pA.Add(new SqlParameter("@NameFilter", searchParts[0]));
            }

            if (roleDesc != null)
            {
                pA.Add(new SqlParameter("@RoleDesc", roleDesc));
            }

            pA.Add(new SqlParameter("@BrokerId", brokerId));

            if (branchChannelT.HasValue)
            {
                pA.Add(new SqlParameter("@BranchChannelT", branchChannelT.Value));
            }

            if (correspondentProcessT.HasValue)
            {
                pA.Add(new SqlParameter("@CorrespondentProcessT", correspondentProcessT.Value));
            }

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeRoleByBrokerId", pA))
            {
                while (sR.Read() == true)
                {
                    // Lookup the specified employee.  Add him/her to a role
                    // list after we pull out the content.
                    Spec eS = null;
                    if (empList.TryGetValue((Guid)sR["EmployeeId"], out eS) == false)
                    {
                        eS = new Spec();

                        eS.UserType = sR.AsNullableString("Type");
                        eS.UserId = sR["UserId"] == DBNull.Value ? Guid.Empty : (Guid)sR["UserId"];
                        eS.FirstName = (string)sR["UserFirstNm"];
                        eS.MiddleName = (string)sR["UserMiddleNm"]; 
                        eS.LastName = (string)sR["UserLastNm"];
                        eS.Suffix = (string)sR["UserSuffix"];
                        eS.Email = (string)sR["Email"];
                        eS.EmployeeId = (Guid)sR["EmployeeId"];
                        eS.IsActive = (bool)sR["IsActive"];
                        eS.Addr = (string)sR["Addr"];
                        eS.City = (string)sR["City"];
                        eS.State = (string)sR["State"];
                        eS.Zip = (string)sR["Zip"];
                        eS.Phone = (string)sR["Phone"];
                        eS.Fax = (string)sR["Fax"];
                        eS.Pager = (string)sR["Pager"];
                        eS.CellPhone = (string)sR["Cellphone"];
                        eS.IsCellphoneForMultiFactorOnly = (bool)sR["IsCellphoneForMultiFactorOnly"];
                        eS.BranchId = (Guid)sR["BranchId"]; //opm 21236 av 04 02 08  
                        eS.CompanyName = (string)sR["CompanyName"]; // 6/30/2009 dd - Add PML Originating Company
                        eS.PmlBrokerId = sR["PmlBrokerId"] == DBNull.Value ? Guid.Empty : (Guid)sR["PmlBrokerId"];
                        if (sR["LoginNm"] != DBNull.Value)
                        {
                            eS.LoginName = (string)sR["LoginNm"]; // 1/27/10 db - Add Login Name OPM 45221
                        }
                        else
                        {
                            eS.LoginName = "";
                        }                    
                        eS.CompanyLicenseXml = sR["CompanyLicenseXml"] == DBNull.Value ? string.Empty : (string)sR["CompanyLicenseXml"];
                        eS.SetLicenseXmlContent(sR["LicenseXmlContent"] == DBNull.Value ? string.Empty : (string)sR["LicenseXmlContent"]);
                        eS.BranchNm = sR["BranchNm"].ToString();
                        empList.Add(eS.EmployeeId, eS);
                    }

                    Role role = Role.Get((Guid)sR["RoleId"]);

                    // Get the corresponding role list.  Note that we use
                    // our table as is.  That means you can accumulative
                    // many entries through multiple retrieves.
                    List<Spec> rList = null;
                    
                    if (m_Table.TryGetValue((string)sR["RoleDesc"], out rList) == false)
                    {
                        rList = new List<Spec>();
                        m_Table.Add((string)sR["RoleDesc"], rList);
                        //m_Table.Add(sR["RoleDesc"], rList = new ArrayList());
                    }

                    rList.Add(eS);

                    // Add the role to the employee's role list so we can
                    // quickly get an employee's assigned roles.
                    Guid employeeId = (Guid) sR["EmployeeId"];

                    List<Role> eList = null;

                    if (m_Roles.TryGetValue(employeeId, out eList) == false)
                    {
                        eList = new List<Role>();
                        m_Roles.Add(employeeId, eList);
                    }
                    eList.Add(role);
                }
            }
        }

        /// <summary>
        /// Load the employees and organize them into lists based on
        /// the roles they support.  Each role lists the employees
        /// that fulfill the role (so an employee may belong to more
        /// than one list).
        /// </summary>
        public void Retrieve(Guid brokerId, String roleDesc)
        {
            Retrieve(brokerId, Guid.Empty, null, roleDesc, E_EmployeeStatusFilterT.All, null, null);
        }

        public virtual void Retrieve(Guid brokerId, E_RoleT roleType)
        {
            Retrieve(brokerId, Guid.Empty, null, roleType, E_EmployeeStatusFilterT.All);
        }

        /// <summary>
        /// Load the employees and organize them into lists based on
        /// the roles they support.  Each role lists the employees
        /// that fulfill the role (so an employee may belong to more
        /// than one list).
        /// </summary>
        public void Retrieve(Guid brokerId, Guid branchId)
        {
            Retrieve(brokerId, branchId, null, null, E_EmployeeStatusFilterT.All, null, null);
        }

        /// <summary>
        /// Load the employees and organize them into lists based on
        /// the roles they support.  Each role lists the employees
        /// that fulfill the role (so an employee may belong to more
        /// than one list).
        /// </summary>
        public void Retrieve(Guid brokerId)
        {
            Retrieve(brokerId, Guid.Empty, null, null, E_EmployeeStatusFilterT.All, null, null);
        }

        /// <summary>
        /// Check if we have the given employee in our set.
        /// </summary>

        public bool Contains(Guid employeeId)
        {
            // Check if we captured this employee.

            return m_Roles.ContainsKey(employeeId);
        }

        /// <summary>
        /// Return the looping interface for added entries.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return the looping interface for added
            // entries.

            return m_Table.Keys.GetEnumerator();
        }

        /// <summary>
        /// Erase all records to make room for reloading
        /// new entries.
        /// </summary>

        public void Clear()
        {
            // Remove the entries from our tables.

            m_Table.Clear();
            m_Roles.Clear();
        }

    }
}
