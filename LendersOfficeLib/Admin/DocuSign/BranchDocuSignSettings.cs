﻿namespace LendersOffice.Admin.DocuSign
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines a set of configuration settings at the branch-level for the DocuSign integration.
    /// </summary>
    public class BranchDocuSignSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BranchDocuSignSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The Broker ID for which these settings apply.</param>
        /// <param name="branchId">The Branch ID for which these settings apply.</param>
        public BranchDocuSignSettings(Guid brokerId, Guid branchId)
        {
            this.BrokerId = brokerId;
            this.BranchId = branchId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BranchDocuSignSettings"/> class from a database record.
        /// </summary>
        /// <param name="record">The record to use to populate the settings object.</param>
        private BranchDocuSignSettings(IDataRecord record)
        {
            this.BrokerId = (Guid)record["BrokerId"];
            this.BranchId = (Guid)record["BranchId"];
            this.BrandIdForBlank = record.AsNullableGuid("BrandIdForBlank");
            this.BrandIdForRetail = record.AsNullableGuid("BrandIdForRetail");
            this.BrandIdForWholesale = record.AsNullableGuid("BrandIdForWholesale");
            this.BrandIdForCorrespondent = record.AsNullableGuid("BrandIdForCorrespondent");
            this.BrandIdForBroker = record.AsNullableGuid("BrandIdForBroker");
            this.BrandNameForBlank = (string)record["BrandNameForBlank"] ?? string.Empty;
            this.BrandNameForRetail = (string)record["BrandNameForRetail"] ?? string.Empty;
            this.BrandNameForWholesale = (string)record["BrandNameForWholesale"] ?? string.Empty;
            this.BrandNameForCorrespondent = (string)record["BrandNameForCorrespondent"] ?? string.Empty;
            this.BrandNameForBroker = (string)record["BrandNameForBroker"] ?? string.Empty;
        }

        /// <summary>
        /// Gets the Broker ID for which these settings apply.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the Branch ID for which these settings apply.
        /// </summary>
        public Guid BranchId { get; }

        /// <summary>
        /// Gets or sets the branch-level brand id to use for loans with a channel value of <see cref="E_BranchChannelT.Blank"/>.
        /// </summary>
        public Guid? BrandIdForBlank { get; set; }

        /// <summary>
        /// Gets or sets the branch-level brand id to use for loans with a channel value of <see cref="E_BranchChannelT.Retail"/>.
        /// </summary>
        public Guid? BrandIdForRetail { get; set; }

        /// <summary>
        /// Gets or sets the branch-level brand id to use for loans with a channel value of <see cref="E_BranchChannelT.Wholesale"/>.
        /// </summary>
        public Guid? BrandIdForWholesale { get; set; }

        /// <summary>
        /// Gets or sets the branch-level brand id to use for loans with a channel value of <see cref="E_BranchChannelT.Correspondent"/>.
        /// </summary>
        public Guid? BrandIdForCorrespondent { get; set; }

        /// <summary>
        /// Gets or sets the branch-level brand id to use for loans with a channel value of <see cref="E_BranchChannelT.Broker"/>.
        /// </summary>
        public Guid? BrandIdForBroker { get; set; }

        /// <summary>
        /// Gets or sets the human-readable name of the brand identified by <see cref="BrandIdForBlank"/>.
        /// </summary>
        public string BrandNameForBlank { get; set; }

        /// <summary>
        /// Gets or sets the human-readable name of the brand identified by <see cref="BrandIdForRetail"/>.
        /// </summary>
        public string BrandNameForRetail { get; set; }

        /// <summary>
        /// Gets or sets the human-readable name of the brand identified by <see cref="BrandIdForWholesale"/>.
        /// </summary>
        public string BrandNameForWholesale { get; set; }

        /// <summary>
        /// Gets or sets the human-readable name of the brand identified by <see cref="BrandIdForCorrespondent"/>.
        /// </summary>
        public string BrandNameForCorrespondent { get; set; }

        /// <summary>
        /// Gets or sets the human-readable name of the brand identified by <see cref="BrandIdForBroker"/>.
        /// </summary>
        public string BrandNameForBroker { get; set; }

        /// <summary>
        /// Retrieves DocuSign branch settings from the database.
        /// </summary>
        /// <param name="brokerId">The broker ID to retrieve settings for.</param>
        /// <param name="branchId">The branch ID to retrieve settings for.</param>
        /// <returns>A new object from the database, or null if none were found.</returns>
        public static BranchDocuSignSettings Retrieve(Guid brokerId, Guid branchId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@BranchId", branchId),
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCUSIGN_BRANCH_CONFIGURATION_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    return new BranchDocuSignSettings(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Saves the DocuSign settings to storage.
        /// </summary>
        public void Save()
        {
            if (this.BrokerId == Guid.Empty || this.BranchId == Guid.Empty)
            {
                throw new InvalidOperationException("Cannot save BranchDocuSignSettings with an invalid BrokerId or BranchId");
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@BranchId", this.BranchId),
                new SqlParameter("@BrandIdForBlank", this.BrandIdForBlank),
                new SqlParameter("@BrandIdForRetail", this.BrandIdForRetail),
                new SqlParameter("@BrandIdForWholesale", this.BrandIdForWholesale),
                new SqlParameter("@BrandIdForCorrespondent", this.BrandIdForCorrespondent),
                new SqlParameter("@BrandIdForBroker", this.BrandIdForBroker),
                new SqlParameter("@BrandNameForBlank", this.BrandNameForBlank),
                new SqlParameter("@BrandNameForRetail", this.BrandNameForRetail),
                new SqlParameter("@BrandNameForWholesale", this.BrandNameForWholesale),
                new SqlParameter("@BrandNameForCorrespondent", this.BrandNameForCorrespondent),
                new SqlParameter("@BrandNameForBroker", this.BrandNameForBroker),
            };
            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(
                    conn: connection,
                    transaction: null,
                    procedureName: StoredProcedureName.Create("DOCUSIGN_BRANCH_CONFIGURATION_Save").ForceValue(),
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Default);
            }
        }
    }
}
