﻿namespace LendersOffice.Admin.DocuSign
{
    /// <summary>
    /// Which DocuSign environment to target.
    /// </summary>
    public enum DocuSignEnvironment
    {
        Unknown = 0,

        Production = 1,

        Test = 2
    }
}
