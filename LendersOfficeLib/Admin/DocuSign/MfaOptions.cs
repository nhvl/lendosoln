﻿namespace LendersOffice.Admin.DocuSign
{
    using System;
    using System.ComponentModel;
    using DataAccess;

    /// <summary>
    /// Options for multi-factor authentication with DocuSign.
    /// </summary>
    [Flags]
    public enum MfaOptions
    {
        /// <summary>
        /// Default value meaning no MFA options have been selected.
        /// </summary>
        [Description("No Selection")]
        NoSelection = 0,

        /// <summary>
        /// Envelope sender determines their own access code to send to the user.
        /// </summary>
        [Description("Access Code")]
        AccessCode = 1 << 0,

        /// <summary>
        /// AKA "Knowledge-Based Authentication". DocuSign asks personal questions to verify recipient identity.
        /// </summary>
        [OrderedDescription("ID Check")]
        [Description("ID Check")]
        IdCheck = 1 << 1,

        /// <summary>
        /// Represents the combination of an email access code and ID check questions for authentication.
        /// </summary>
        [OrderedDescription("Access Code and ID Check")]
        [Description("Access Code and ID Check")]
        AccessCodeAndIdCheck = 1 << 2,

        /// <summary>
        /// Recipient answers a phone call and enters the code to access documents.
        /// </summary>
        [Description("Phone")]
        Phone = 1 << 3,

        /// <summary>
        /// Recipient receives a text message with the document access code.
        /// </summary>
        [OrderedDescription("SMS")]
        [Description("SMS")]
        Sms = 1 << 4,

        /// <summary>
        /// With no MFA, we should only allow in-person signing. Email signing needs MFA.
        /// </summary>
        [OrderedDescription("No MFA")]
        [Description("None")]
        NoMfa = 1 << 5
    }
}
