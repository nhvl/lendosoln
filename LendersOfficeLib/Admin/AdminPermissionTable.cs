﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>

    public class AdminPermissionTable : BrokerUserPermissionTable
    {
        #region ( Inclusion test )

        /// <summary>
        /// Throw out the obsoleted and internal permissions.
        /// </summary>

        protected override bool IsIncluded(Desc pDesc)
        {
            // Throw out these old/special permissions;
            // not to be used by our customers.

            switch (pDesc.Code)
            {
                case Permission.CanModifyAdministrativeItem:
                case Permission.IndividualLevelAccess:
                case Permission.BrokerLevelAccess:
                case Permission.BranchLevelAccess:
                case Permission.TeamLevelAccess:
                case Permission.CanModifyLoanPrograms:
                case Permission.AllowLoadingDeletedLoans:
                    return false;

            }

            return true;
        }

        #endregion

    }
}
