﻿namespace LendersOffice.Admin
{
    using DataAccess;

    /// <summary>
    /// Defines the type of suite a broker represents.
    /// </summary>
    public enum BrokerSuiteType
    {
        /// <summary>
        /// The suite type has not been set.
        /// </summary>
        [OrderedDescription("", Order = 0)]
        Blank = 0,

        /// <summary>
        /// An LQB client.
        /// </summary>
        [OrderedDescription("Client - LQB", Order = 1)]
        ClientLqb = 1,

        /// <summary>
        /// A client that is PML only.
        /// </summary>
        [OrderedDescription("Client - PML Only", Order = 2)]
        ClientPmlOnly = 2,

        /// <summary>
        /// An ILS client.
        /// </summary>
        [OrderedDescription("Client - ILS", Order = 3)]
        ClientIls = 3,

        /// <summary>
        /// An internal demo client.
        /// </summary>
        [OrderedDescription("Internal Demo", Order = 4)]
        InternalDemo = 4,

        /// <summary>
        /// An internal test client.
        /// </summary>
        [OrderedDescription("Internal Test", Order = 5)]
        InternalTest = 5,

        /// <summary>
        /// A load test client.
        /// </summary>
        [OrderedDescription("Load Test", Order = 6)]
        LoadTest = 6,

        /// <summary>
        /// A vendor test client.
        /// </summary>
        [OrderedDescription("Vendor Test", Order = 7)]
        VendorTest = 7
    }
}
