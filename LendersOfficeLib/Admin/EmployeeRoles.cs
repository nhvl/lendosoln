﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Load employee's roles from the database.
    /// </summary>
    public class EmployeeRoles
    {

        public Guid BrokerId { get; private set; }
        public Guid EmployeeId { get; private set; }

        private List<Role> m_list = null;

        public IEnumerable<Role> Items
        {
            get { return m_list; }
        }
        /// <summary>
        /// Clear our set and reload using the given identifier.
        /// </summary>
        private void Retrieve()
        {
            // Clear our set and reload using the given
            // credentials.

            m_list = new List<Role>();

            if (this.EmployeeId == Guid.Empty)
            {
                return;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeId", this.EmployeeId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "ROLE_ASSIGNMENT_ListByEmployeeId", parameters))
            {
                while (reader.Read())
                {
                    Guid roleId = (Guid)reader["RoleId"];
                    m_list.Add(Role.Get(roleId));

                }
            }
        }

        public bool IsInRole(E_RoleT roleType)
        {
            foreach (var role in m_list)
            {
                if (role.RoleT == roleType)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Test if the given role id is part of our set.
        /// </summary>

        public bool IsInRole(Guid roleId)
        {
            // Search for match and return.

            foreach (var role in m_list)
            {
                if (role.Id == roleId)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Construct default.
        /// </summary>
        public EmployeeRoles(Guid brokerId, Guid employeeId)
        {
            this.EmployeeId = employeeId;
            this.BrokerId = brokerId;

            this.Retrieve();

        }

    }
}
