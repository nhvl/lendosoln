﻿// <copyright file="BrokerDBData.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eric Mallare
//  Date:   7/15/2015
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using DataAccess;
    using Newtonsoft.Json;

    /// <summary>
    /// A class holding all data in the Broker table. Used to cache the BrokerDb object. 
    /// The class can handle all fields from a data row result. It will add them to a dictionary 
    /// to retrieve the values. It will also keep track of all changes. 
    /// </summary>
    public class BrokerDBData 
    {
        /// <summary>
        /// A mapping between fields and their new and original data.
        /// </summary>
        private CommonProjectLib.Common.Lib.FriendlyDictionary<string, FieldSnapshot> data;

        /// <summary>
        /// A set of fields that need to be updated.
        /// </summary>
        private HashSet<string> dirtyFields;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerDBData" /> class.
        /// </summary>
        public BrokerDBData()
        {
            this.data = new CommonProjectLib.Common.Lib.FriendlyDictionary<string, FieldSnapshot>(300, StringComparer.OrdinalIgnoreCase);
            this.dirtyFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerDBData" /> class.
        /// </summary>
        /// <param name="record">The data row to load the data from.</param>
        public BrokerDBData(IDataRecord record) : this()
        {
            for (int i = 0; i < record.FieldCount; i++)
            {
                string name = record.GetName(i);
                object value = record.GetValue(i); 

                if (Convert.IsDBNull(value))
                {
                    value = null;
                }

                FieldSnapshot snapshot = new FieldSnapshot(value);
                this.data.Add(name, snapshot);
            }
        }

        /// <summary>
        /// Gets the unique version for this instance.
        /// </summary>
        /// <value>The Version of the object.</value>
        public string Version
        {
            get { return System.Convert.ToBase64String(this.GetByteArrayField("Version")); }
        }

        /// <summary>
        /// Gets the value of the given column.
        /// </summary>
        /// <param name="columnName">The column name to look up.</param>
        /// <returns>The value for the given column.</returns>
        public object this[string columnName]
        {
            get
            {
                return this.data[columnName].CurrentValue;
            }
        }

        /// <summary>
        /// Gets the cache key for the given broker.
        /// </summary>
        /// <param name="brokerId">The broker id whose cache key to generate.</param>
        /// <returns>The cache key.</returns>
        public static string GetKey(Guid brokerId)
        {
            return string.Format("Broker/{0:N}", brokerId);
        }

        /// <summary>
        /// Gets the current value of the given field. This method does not work on database fields that can be null.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when the field is not found.</exception>
        /// <typeparam name="T">The type the field should be casted to before returning it.</typeparam>
        /// <param name="field">The field id to look up.</param>
        /// <returns>The value casted to the requested type.</returns>
        public T GetValue<T>(string field)
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;
            return (T)value;
        }

        /// <summary>
        /// Gets the current value of the given field. Supports null as well. 
        /// </summary>
        /// <param name="field">The field to look up.</param>
        /// <returns>Null or the correct value.</returns>
        /// <typeparam name="T">T has to be a struct for a null value to work.</typeparam>
        public T? GetNullableValue<T>(string field) where T : struct
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;

            if (value == null)
            {
                return new T?();
            }
            else
            {
                return new T?((T)value);
            }
        }
        
        /// <summary>
        /// Gets the current value of the given fiend. Supports null values as well.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>A guid that can be null.</returns>
        public Guid? GetNullableGuid(string field)
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;

            if (value == null)
            {
                return new Guid?();
            }
            else if (value is Guid)
            {
                return (Guid)value;
            }
            else
            {
                return new Guid(value.ToString());
            }
        }

        /// <summary>
        /// Gets the current guid value for the given field. Does not support null.
        /// </summary>
        /// <param name="field">The field id to look up.</param>
        /// <returns>The Guid value.</returns>
        public Guid GetGuid(string field)
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;

            if (typeof(Guid) == value.GetType())
            {
                return (Guid)value;
            }
            else
            {
                return new Guid(value.ToString());
            }
        }

        /// <summary>
        /// Gets the current guid value for the given field. Does not support null.
        /// </summary>
        /// <param name="field">The field id to look up.</param>
        /// <param name="defaultValue">The value to return if current value is null.</param>
        /// <returns>The Guid value.</returns>
        public Guid GetGuid(string field, Guid defaultValue)
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;

            if (value == null)
            {
                return defaultValue;
            }
            else if (typeof(Guid) == value.GetType())
            {
                return (Guid)value;
            }
            else
            {
                return new Guid(value.ToString());
            }
        }

        /// <summary>
        /// Gets the field value as a byte array.
        /// </summary>
        /// <param name="field">The field to look up.</param>
        /// <returns>The data as the byte array.</returns>
        public byte[] GetByteArrayField(string field)
        {
            FieldSnapshot snapshot = this.Get(field);
            object value = snapshot.CurrentValue;

            if (value == null)
            {
                return null;
            }

            byte[] fieldValue = value as byte[];

            if (fieldValue != null)
            {
                return fieldValue;
            }

            if (value.GetType() == typeof(string))
            {
                return Convert.FromBase64String(value.ToString());
            }

            throw new ArgumentException("Cannot convert field " + field + " to byte");
        }

        /// <summary>
        /// Gets the current value of the given field. If its null returns the 
        /// default value. 
        /// </summary>
        /// <param name="field">The field to lookup.</param>
        /// <param name="defaultValue">The default value to return if field is null.</param>
        /// <typeparam name="T">Null fields have to be structs.</typeparam>
        /// <returns>The non null value from the db or default value.</returns>
        public T GetValue<T>(string field, T defaultValue) where T : struct
        {
            T? value = this.GetNullableValue<T>(field);

            if (value.HasValue)
            {
                return value.Value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Sets the given field to the new value. Keeps change tracking enabled.
        /// Warning, this does NO type checking. It will let you put a string in an integer. 
        /// </summary>
        /// <param name="field">The field whose value we have to set.</param>
        /// <param name="newValue">The new value we have to set.</param>
        public void SetValue(string field, object newValue)
        {
            FieldSnapshot snapshot = this.Get(field);

            if (snapshot.OriginalValue == null && newValue == null)
            {
                if (this.dirtyFields.Remove(field))
                {
                    snapshot.ClearLatestValue();
                }

                return; 
            }

            if (snapshot.OriginalValue != null && snapshot.OriginalValue.Equals(newValue))
            {
                if (this.dirtyFields.Remove(field))
                {
                    snapshot.ClearLatestValue();
                }

                return;  
            }

            snapshot.SetLatestValue(newValue);
            this.dirtyFields.Add(field);
        }

        /// <summary>
        /// Takes the given string and deserializes it into the current instance.
        /// </summary>
        /// <param name="data">The string containing the JSON for the current instance.</param>
        public void FromJson(string data)
        {
            JsonTextReader reader = new JsonTextReader(new StringReader(data));

            // Read Start Object.
            reader.Read();

            while (reader.Read() && reader.TokenType == JsonToken.PropertyName)
            {
                string propertyName = (string)reader.Value;
                reader.Read();
                this.data.Add(propertyName, new FieldSnapshot(reader.Value));
            }
        }

        /// <summary>
        /// Serializes the current instance into a JSON string.
        /// </summary>
        /// <returns>A JSON string representing the current instance of this object.</returns>
        public string ToJson()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.WriteStartObject();

                foreach (var item in this.data)
                {
                    writer.WritePropertyName(item.Key);
                    writer.WriteValue(item.Value.CurrentValue);
                }

                writer.WriteEndObject();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets a field snapshot for the given field id. It will throw an exception if
        /// the field is not found.
        /// </summary>
        /// <param name="field">The field to look up.</param>
        /// <returns>A snapshot containing the original and latest data.</returns>
        private FieldSnapshot Get(string field)
        {
            FieldSnapshot snapshot;

            if (!this.data.TryGetValue(field, out snapshot))
            {
                string error = string.Concat("Field : ", field, " was not loaded.");
                throw new ArgumentException(error);
            }

            return snapshot;
        }
    }
}
