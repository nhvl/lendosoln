﻿/// <copyright file="PmlBrokerPermission.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   2/18/2016
/// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Runtime.Serialization;
    using Common;
    using DataAccess;
    using Security;

    /// <summary>
    /// Provides a container for the permissions set
    /// and task email option of a <see cref="PmlBroker"/>.
    /// </summary>
    [DataContract]
    public class PmlBrokerPermission
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PmlBrokerPermission"/> 
        /// class with each permission set to a default value of false and a 
        /// task email option of <see cref="E_TaskRelatedEmailOptionT.DontReceiveEmail"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the OC's broker.
        /// </param>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> of the OC.
        /// </param>
        public PmlBrokerPermission(Guid brokerId, Guid pmlBrokerId)
        {
            this.BrokerId = brokerId;
            this.PmlBrokerId = pmlBrokerId;
            this.TaskEmailOption = E_TaskRelatedEmailOptionT.DontReceiveEmail;
        }
        
        /// <summary>
        /// Gets the value for the ID of the OC's broker.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> for the OC's broker.
        /// </value>
        [IgnoreDataMember]
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the value for the ID of the OC.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> of the OC.
        /// </value>
        [IgnoreDataMember]
        public Guid PmlBrokerId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can apply for 
        /// ineligible loan programs.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        [DataMember]
        public bool CanApplyForIneligibleLoanPrograms { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can run pricing
        /// without a credit report.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        [DataMember]
        public bool CanRunPricingEngineWithoutCreditReport { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can register/lock
        /// a loan without a credit report.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        [DataMember]
        public bool CanSubmitWithoutCreditReport { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can access
        /// TOTAL Scorecard.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        [DataMember]
        public bool CanAccessTotalScorecard { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the user can order
        /// a 4506-T.
        /// </summary>
        /// <value>
        /// True if the user has permission, false otherwise.
        /// </value>
        [DataMember]
        public bool AllowOrder4506T { get; private set; }

        /// <summary>
        /// Gets or sets the value for the type of task email options set for 
        /// the <see cref="PmlBroker"/>.
        /// </summary>
        /// <value>
        /// The <see cref="E_TaskRelatedEmailOptionT"/> for the <see cref="PmlBroker"/>.
        /// </value>
        [DataMember]
        public E_TaskRelatedEmailOptionT TaskEmailOption { get; set; }

        /// <summary>
        /// Determines whether the user has the permission corresponding
        /// to the specified <paramref name="permission"/>.
        /// </summary>
        /// <param name="permission">
        /// The <see cref="Permission"/> to check.
        /// </param>
        /// <returns>
        /// True if the user has the permission, false otherwise.
        /// </returns>
        /// <exception cref="UnhandledEnumException">
        /// The specified <paramref name="permission"/> is not currently 
        /// available at the <see cref="PmlBroker"/> level.
        /// </exception>
        public bool this[Permission permission]
        {
            get
            {
                switch (permission)
                {
                    case Permission.CanApplyForIneligibleLoanPrograms:
                        return this.CanApplyForIneligibleLoanPrograms;
                    case Permission.CanRunPricingEngineWithoutCreditReport:
                        return this.CanRunPricingEngineWithoutCreditReport;
                    case Permission.CanSubmitWithoutCreditReport:
                        return this.CanSubmitWithoutCreditReport;
                    case Permission.CanAccessTotalScorecard:
                        return this.CanAccessTotalScorecard;
                    case Permission.AllowOrder4506T:
                        return this.AllowOrder4506T;
                    default:
                        throw new UnhandledEnumException(permission);
                }
            }
        }

        /// <summary>
        /// Retrieves PML broker permission information for the PML broker with the 
        /// specified <paramref name="pmlBrokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the OC's broker.
        /// </param>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> of the OC.
        /// </param>
        /// <param name="parser">
        /// The parser for permission data.
        /// </param>
        /// <returns>
        /// A <see cref="PmlBrokerPermission"/> object containing relationship information.
        /// </returns>
        public static PmlBrokerPermission Retrieve(Guid brokerId, Guid pmlBrokerId, PmlBrokerRelationshipPermissionDataParser parser = null)
        {
            var pmlBrokerPermission = new PmlBrokerPermission(brokerId, pmlBrokerId);

            if (parser != null)
            {
                pmlBrokerPermission.CanApplyForIneligibleLoanPrograms = parser.CanApplyForIneligibleLoanPrograms;
                pmlBrokerPermission.CanRunPricingEngineWithoutCreditReport = parser.CanRunPricingEngineWithoutCreditReport;
                pmlBrokerPermission.CanSubmitWithoutCreditReport = parser.CanSubmitWithoutCreditReport;
                pmlBrokerPermission.CanAccessTotalScorecard = parser.CanAccessTotalScorecard;
                pmlBrokerPermission.AllowOrder4506T = parser.AllowOrder4506T;
                pmlBrokerPermission.TaskEmailOption = parser.TaskEmailOption;
            }
            else
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@PmlBrokerId", pmlBrokerId)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "PMLBROKER_RetrievePermissions", parameters))
                {
                    if (reader.Read())
                    {
                        pmlBrokerPermission.CanApplyForIneligibleLoanPrograms = (bool)reader["CanApplyForIneligibleLoanPrograms"];
                        pmlBrokerPermission.CanRunPricingEngineWithoutCreditReport = (bool)reader["CanRunPricingEngineWithoutCreditReport"];
                        pmlBrokerPermission.CanSubmitWithoutCreditReport = (bool)reader["CanSubmitWithoutCreditReport"];
                        pmlBrokerPermission.CanAccessTotalScorecard = (bool)reader["CanAccessTotalScorecard"];
                        pmlBrokerPermission.AllowOrder4506T = (bool)reader["AllowOrder4506T"];
                        pmlBrokerPermission.TaskEmailOption = (E_TaskRelatedEmailOptionT)reader["TaskRelatedEmailOptionT"];
                    }
                }
            }

            return pmlBrokerPermission;
        }

        /// <summary>
        /// Checks if the permission is a valid Pml broker permission.
        /// </summary>
        /// <param name="permissionToCheck">The permission to check.</param>
        /// <returns>True if it is a valid permission for modification, false otherwise.</returns>
        public static bool IsValidPmlBrokerPermission(Permission permissionToCheck)
        {
            return permissionToCheck == Permission.CanApplyForIneligibleLoanPrograms ||
                   permissionToCheck == Permission.CanRunPricingEngineWithoutCreditReport ||
                   permissionToCheck == Permission.CanSubmitWithoutCreditReport ||
                   permissionToCheck == Permission.CanAccessTotalScorecard ||
                   permissionToCheck == Permission.AllowOrder4506T;
        }

        /// <summary>
        /// Sets the specified <paramref name="permission"/> to the specified <paramref name="value"/>
        /// for the OC.
        /// </summary>
        /// <param name="permission">
        /// The <see cref="Permission"/> to set.
        /// </param>
        /// <param name="value">
        /// True if the user should have the permission, false otherwise.
        /// </param>
        /// <exception cref="UnhandledEnumException">
        /// The specified <paramref name="permission"/> cannot currently
        /// be set at the <see cref="PmlBroker"/> level.
        /// </exception>
        public void SetPermission(Permission permission, bool value)
        {
            switch (permission)
            {
                case Permission.CanApplyForIneligibleLoanPrograms:
                    this.CanApplyForIneligibleLoanPrograms = value;
                    break;
                case Permission.CanRunPricingEngineWithoutCreditReport:
                    this.CanRunPricingEngineWithoutCreditReport = value;
                    break;
                case Permission.CanSubmitWithoutCreditReport:
                    this.CanSubmitWithoutCreditReport = value;
                    break;
                case Permission.CanAccessTotalScorecard:
                    this.CanAccessTotalScorecard = value;
                    break;
                case Permission.AllowOrder4506T:
                    this.AllowOrder4506T = value;
                    break;
                default:
                    throw new UnhandledEnumException(permission);
            }
        }

        /// <summary>
        /// Saves the permission information to the database.
        /// </summary>
        public void Save()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@PmlBrokerId", this.PmlBrokerId),
                new SqlParameter("@CanApplyForIneligibleLoanPrograms", this.CanApplyForIneligibleLoanPrograms),
                new SqlParameter("@CanRunPricingEngineWithoutCreditReport", this.CanRunPricingEngineWithoutCreditReport),
                new SqlParameter("@CanSubmitWithoutCreditReport", this.CanSubmitWithoutCreditReport),
                new SqlParameter("@CanAccessTotalScorecard", this.CanAccessTotalScorecard),
                new SqlParameter("@AllowOrder4506T", this.AllowOrder4506T),
                new SqlParameter("@TaskRelatedEmailOptionT", this.TaskEmailOption)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "PMLBROKER_SavePermissions", 0, parameters);
        }

        /// <summary>
        /// Converts this instance into a serialized JSON string.
        /// </summary>
        /// <returns>
        /// A serialized JSON string for this <see cref="PmlBrokerPermission"/>
        /// instance.
        /// </returns>
        public override string ToString()
        {
            return ObsoleteSerializationHelper.JsonSerialize(this);
        }
    }
}
