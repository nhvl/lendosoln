﻿using System;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    /// <summary>
    /// We track all the possible reply-to scenarios for all
    /// notification events.
    /// </summary>

    [XmlRoot]
    public class NotificationRules
    {
        /// <summary>
        /// We track all the possible reply-to scenarios
        /// for all notification events.
        /// </summary>

        private ArrayList m_List = new ArrayList();

        #region ( List properties )

        [XmlArray
       ]
        [XmlArrayItem(typeof(NotificationRule))
       ]
        public ArrayList List
        {
            // Access member.

            set
            {
                m_List = value;
            }
            get
            {
                return m_List;
            }
        }

        [XmlIgnore
       ]
        public Int32 Count
        {
            // Access member.

            get
            {
                return m_List.Count;
            }
        }

        #endregion

        /// <summary>
        /// Append to the end.  We remove existing entries.
        /// Specialize this implementation to filter adds.
        /// </summary>

        public virtual void Add(NotificationRule nRule)
        {
            // Append to the end.  We remove existing entries.

            if (nRule == null)
            {
                throw new ArgumentException("Invalid rule item.");
            }

            foreach (NotificationRule nR in m_List)
            {
                if (nR.EventLabel.ToLower() == nRule.EventLabel.ToLower())
                {
                    m_List.Remove(nR);

                    break;
                }
            }

            m_List.Add(nRule);
        }

        /// <summary>
        /// Load the notification rules from the xml document.
        /// </summary>

        public void Retrieve(String sXml)
        {
            // Translate document and snatch up the rules
            // from the temp copy.

            if (sXml.TrimWhitespaceAndBOM() != String.Empty)
            {
                m_List = ToObject(sXml).m_List;
            }
            else
            {
                m_List.Clear();
            }
        }

        /// <summary>
        /// Return looping interface pointer for walking.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return looping interface pointer for
            // walking.

            return m_List.GetEnumerator();
        }

        /// <summary>
        /// Reset the set and clear out all entries.
        /// </summary>

        public void Clear()
        {
            // Reset the set and clear out all.

            m_List.Clear();
        }

        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our set.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>

        public static NotificationRules ToObject(String sXml)
        {
            // Deserialize the given string to our list.

            XmlSerializer xS = new XmlSerializer(typeof(NotificationRules));
            StringReader sR = new StringReader(sXml);

            return xS.Deserialize(sR) as NotificationRules;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            // Serialize this instance as an xml document.

            XmlSerializer xS = new XmlSerializer(typeof(NotificationRules));
            StringWriter8 s8 = new StringWriter8();

            xS.Serialize(s8, this);

            return s8.ToString();
        }

        #endregion

    }
}
