﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
    public class BrokerDBNotFoundException : NotFoundException
    {
        public BrokerDBNotFoundException(Guid brokerId) :
            base(ErrorMessages.FailedToRetrieveBroker, "BrokerId = " + brokerId)
        {
        }
    }
}
