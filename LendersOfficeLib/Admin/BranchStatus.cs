﻿namespace LendersOffice.Admin
{
    /// <summary>
    /// Branch Status Enum.
    /// </summary>
    public enum BranchStatus
    {
        Inactive = 0,

        Active = 1
    }
}
