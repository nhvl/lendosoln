﻿using System;
using System.Data;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Keep simple description of employee's contact information on hand
    /// for quick lookup.
    /// </summary>

    public class EmployeeContactInfo
    {
        private string cellPhone = string.Empty;

        public EmployeeContactInfo(DbDataReader reader)
        {
            Init(reader);
        }
        protected string GetString(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (string)o;
            }
            else
            {
                return string.Empty;
            }
        }
        protected Guid GetGuid(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (Guid)o;
            }
            else
            {
                return Guid.Empty;
            }
        }
        protected int GetInt(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (int)o;
            }
            else
            {
                return int.MinValue;
            }
        }
        protected bool GetBool(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (bool)o;
            }
            else
            {
                return false;
            }
        }
        protected DateTime GetDateTime(object o)
        {
            if ((o != null) && ((o is DBNull) == false))
            {
                return (DateTime)o;
            }
            else
            {
                return SmallDateTime.MinValue;
            }
        }
        #region ( Contact info properties )

        private void Init(DbDataReader reader)
        {
            FirstName = GetString(reader["UserFirstNm"]);
            LastName = GetString(reader["UserLastNm"]);
            Street = GetString(reader["Addr"]);
            City = GetString(reader["City"]);
            State = GetString(reader["State"]);
            Zip = GetString(reader["Zip"]);
            Phone = GetString(reader["Phone"]);
            CellPhone = GetString(reader["CellPhone"]);
            this.IsCellphoneForMultiFactorOnly = GetBool(reader["IsCellphoneForMultiFactorOnly"]);
            Pager = GetString(reader["Pager"]);
            Fax = GetString(reader["Fax"]);
            Email = GetString(reader["Email"]);
            EmployeeId = GetGuid(reader["EmployeeId"]);

            StartDate = GetDateTime(reader["EmployeeStartD"]);
        }

        public string EmployeeName
        {
            get { return LastName + " , " + FirstName; }
        }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public String FullAddress
        {
            get { return Street + " , " + City + " , " + State + "  " + Zip; }
        }

        public string Street { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }
        public string Phone { get; private set; }
        public string Fax { get; private set; }
        public string Email { get; private set; }
        public string Pager { get; private set; }
        public bool IsCellphoneForMultiFactorOnly { get; private set; }
        public string CellPhone
        {
            get
            {
                return EmployeeDB.CalculatedCellphoneValue(this.cellPhone, this.IsCellphoneForMultiFactorOnly);
            }

            private set
            {
                this.cellPhone = value;
            }
        }
        public DateTime StartDate { get; private set; }
        public Guid EmployeeId { get; private set; }

        #endregion

    }
}
