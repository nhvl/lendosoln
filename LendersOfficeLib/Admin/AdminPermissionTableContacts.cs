﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableContacts : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override Boolean IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.AllowReadingFromRolodex:
                case Permission.AllowWritingToRolodex:
                case Permission.AllowManagingContactList: // opm 109122
                    return true;
            }
            return false;
        }
        #endregion
    }
}
