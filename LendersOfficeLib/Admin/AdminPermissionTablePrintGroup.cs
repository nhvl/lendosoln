﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTablePrintGroups : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.AllowViewingPrintGroups:
                case Permission.CanEditPrintGroups:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
