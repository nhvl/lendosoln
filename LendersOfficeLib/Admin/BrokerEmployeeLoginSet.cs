﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Load the employees and index them by login and type.
    /// </summary>

    public class BrokerEmployeeLoginSet
    {
        /// <summary>
        /// Keep track of loaded employees in memory.  This set
        /// will hang out and allowing lookup by login name.
        /// </summary>

        private Hashtable m_Table = new Hashtable();
        private ArrayList m_Set = new ArrayList();

        /// <summary>
        /// Each employee entry takes up one spec.
        /// </summary>

        public class Spec
        {
            /// <summary>
            /// Track employee details.
            /// </summary>

            private String m_Login = String.Empty;
            private String m_FirstName = String.Empty;
            private String m_LastName = String.Empty;
            private String m_Type = String.Empty;
            private Guid m_EmployeeId = Guid.Empty;
            private Guid m_BranchId = Guid.Empty;
            private Guid m_UserId = Guid.Empty;

            #region ( Spec properties )

            public String Login
            {
                // Access member.

                set
                {
                    m_Login = value;
                }
                get
                {
                    return m_Login;
                }
            }

            public String FirstName
            {
                // Access member.

                set
                {
                    m_FirstName = value;
                }
                get
                {
                    return m_FirstName;
                }
            }

            public String LastName
            {
                // Access member.

                set
                {
                    m_LastName = value;
                }
                get
                {
                    return m_LastName;
                }
            }

            public String Type
            {
                // Access member.

                set
                {
                    m_Type = value;
                }
                get
                {
                    return m_Type;
                }
            }

            public Guid EmployeeId
            {
                // Access member.

                set
                {
                    m_EmployeeId = value;
                }
                get
                {
                    return m_EmployeeId;
                }
            }

            public Guid BranchId
            {
                // Access member.

                set
                {
                    m_BranchId = value;
                }
                get
                {
                    return m_BranchId;
                }
            }

            public Guid UserId
            {
                // Access member.

                set
                {
                    m_UserId = value;
                }
                get
                {
                    return m_UserId;
                }
            }

            #endregion

        }

        /// <summary>
        /// Load the specified broker's users from the database.
        /// </summary>

        public void Retrieve(Guid brokerId, String sType)
        {
            // Check inputs and retrieve all employees with logins.

            if (sType != null && sType == String.Empty)
            {
                throw new ArgumentException("Invalid user type.", sType);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId), 
                                            new SqlParameter("@Type", sType)
                                        };

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListBrokerEmployeeLogin", parameters))
            {
                while (sR.Read() == true)
                {
                    // Add each spec one employee at a time.

                    Spec sE = new Spec();

                    try
                    {
                        sE.Login = (String)sR["LoginNm"];
                        sE.FirstName = (String)sR["UserFirstNm"];
                        sE.LastName = (String)sR["UserLastNm"];

                        sE.EmployeeId = (Guid)sR["EmployeeId"];
                        sE.BranchId = (Guid)sR["BranchId"];
                        sE.UserId = (Guid)sR["UserId"];

                        sE.Type = sR["Type"].ToString();
                    }
                    catch
                    {
                        // D'oh!

                        continue;
                    }

                    m_Table.Add(sE.Login.ToLower() + ":" + sE.Type, sE);

                    m_Set.Add(sE);
                }
            }
        }

        /// <summary>
        /// Load the specified broker's users from the database.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            // Delegate to inner implementation.

            Retrieve(brokerId, null);
        }

        /// <summary>
        /// Check for an existing login.  We return true if the
        /// combination is already in our set.
        /// </summary>

        public Boolean IsValid(String sLogin, String sType)
        {
            // Test the combination against our set.  We use the
            // 2 keys to find the unique entry (2 employees with
            // same login but different types may coexist).

            if (sLogin == null)
            {
                throw new ArgumentException("Invalid login parameter.");
            }

            if (sType == null)
            {
                throw new ArgumentException("Invalid type parameter.");
            }

            if (m_Table.Contains(sLogin.ToLower() + ":" + sType) == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Return the looping interface for added entries.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return the looping interface for added entries.

            return m_Set.GetEnumerator();
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerEmployeeLoginSet(Guid brokerId)
        {
            // Initialize this set.

            Retrieve(brokerId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerEmployeeLoginSet()
        {
        }

        #endregion

    }
}
