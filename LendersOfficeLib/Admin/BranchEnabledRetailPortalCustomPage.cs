﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a means for loading the custom retail TPO portal
    /// pages allowed for a branch.
    /// </summary>
    public class BranchEnabledRetailPortalCustomPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BranchEnabledRetailPortalCustomPage"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="branchId">
        /// The ID of the branch.
        /// </param>
        public BranchEnabledRetailPortalCustomPage(Guid brokerId, Guid branchId)
        {
            this.BrokerId = brokerId;
            this.BranchId = branchId;
        }

        /// <summary>
        /// Gets or sets the broker ID for the branch.
        /// </summary>
        /// <value>
        /// The broker ID.
        /// </value>
        private Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the branch ID.
        /// </summary>
        /// <value>
        /// The branch ID.
        /// </value>
        private Guid BranchId { get; set; }

        /// <summary>
        /// Sets the enabled pages for the branch.
        /// </summary>
        /// <param name="executor">
        /// The stored procedure executor holding the transaction.
        /// </param>
        /// <param name="enabledPages">
        /// The list of enabled pages.
        /// </param>
        public void SetEnabledPages(CStoredProcedureExec executor, IEnumerable<Guid> enabledPages)
        {
            var existingPages = this.RetrieveEnabledPages(executor);

            var addedPages = enabledPages.Except(existingPages);
            if (addedPages.Any())
            {
                var addedPageIdXml = new XElement(
                    "r",
                    addedPages.Select(pageId => new XElement("p", pageId)));

                SqlParameter[] parameters =
                {
                    new SqlParameter("BranchId", this.BranchId),
                    new SqlParameter("AddedPageXml", addedPageIdXml.ToString(SaveOptions.DisableFormatting)) { SqlDbType = System.Data.SqlDbType.Xml }
                };

                executor.ExecuteNonQuery("BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Add", parameters);
            }

            var removedPages = existingPages.Except(enabledPages);
            if (removedPages.Any())
            {
                var removedPageXml = new XElement(
                    "r",
                    removedPages.Select(pageId => new XElement("p", pageId)));

                SqlParameter[] parameters =
                {
                    new SqlParameter("BranchId", this.BranchId),
                    new SqlParameter("RemovedPageXml", removedPageXml.ToString(SaveOptions.DisableFormatting)) { SqlDbType = System.Data.SqlDbType.Xml }
                };

                executor.ExecuteNonQuery("BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Remove", parameters);
            }
        }

        /// <summary>
        /// Retrieves the custom page settings.
        /// </summary>
        /// <returns>
        /// The enabled pages.
        /// </returns>
        public HashSet<Guid> RetrieveEnabledPages()
        {
            var procedureName = StoredProcedureName.Create("BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Retrieve").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@BranchId", this.BranchId)
            };

            var links = new HashSet<Guid>();
            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    links.Add((Guid)reader["PageId"]);
                }
            }

            return links;
        }

        /// <summary>
        /// Retrieves the custom page settings.
        /// </summary>
        /// <param name="executor">
        /// The transaction-based procedure executor.
        /// </param>
        /// <returns>
        /// The list of enabled pages.
        /// </returns>
        private IEnumerable<Guid> RetrieveEnabledPages(CStoredProcedureExec executor)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BranchId", this.BranchId)
            };

            var links = new List<Guid>();

            using (var reader = executor.ExecuteReader("BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Retrieve", parameters))
            {
                while (reader.Read())
                {
                    links.Add((Guid)reader["PageId"]);
                }
            }

            return links;
        }
    }
}
