﻿namespace LendersOffice.Admin
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using ObjLib.Conversions;

    /// <summary>
    /// Stores settings representing options for which data to send for ULDD Phase 3.
    /// </summary>
    public class UlddPhase3Settings
    {
        /// <summary>
        /// Regex expression for extracting the temp option parameters.
        /// </summary>
        private const string UlddPhase3OptionPattern = "<option\\s+name=\"(EnableULDDPhase3(ForTestFiles)?)\"\\s+forGseTarget=\"([^\"]*)\"\\s+part=\"([^\"]*)\"\\s+value=\"true\"\\s*\\/?>";

        /// <summary>
        /// A 2-Dimensional Map which maps (whether the loan is a test loan) and (GSE delivery target) to a ULDD Phase 3 part to use for the export.
        /// </summary>
        private Dictionary<bool, Dictionary<E_sGseDeliveryTargetT, UlddPhase3Part>> partMap = 
            new Dictionary<bool, Dictionary<E_sGseDeliveryTargetT, UlddPhase3Part>>
            {
                { true, new Dictionary<E_sGseDeliveryTargetT, UlddPhase3Part>() },
                { false, new Dictionary<E_sGseDeliveryTargetT, UlddPhase3Part>() }
            };

        /// <summary>
        /// Initializes a new instance of the <see cref="UlddPhase3Settings"/> class.
        /// </summary>
        /// <param name="brokerTempOptionXml">The lender's temp option pseudo-XML.</param>
        private UlddPhase3Settings(string brokerTempOptionXml)
        {
            MatchCollection matches = Regex.Matches(brokerTempOptionXml, UlddPhase3OptionPattern, RegexOptions.IgnoreCase);
            foreach (Match match in matches)
            {
                if (match.Groups[1].Success)
                {
                    bool forTest = match.Groups[2].Success;
                    E_sGseDeliveryTargetT? deliveryTarget = match.Groups[3]?.Value.ToNullableEnumDefined<E_sGseDeliveryTargetT>(true);
                    UlddPhase3Part part = match.Groups[4]?.Value.ToNullableEnumDefined<UlddPhase3Part>() ?? UlddPhase3Part.UsePhase2Instead;
                    if (deliveryTarget == null && string.IsNullOrWhiteSpace(match.Groups[3].Value))
                    {
                        if (!this.partMap[forTest].ContainsKey(E_sGseDeliveryTargetT.FannieMae))
                        {
                            this.partMap[forTest].Add(E_sGseDeliveryTargetT.FannieMae, part);
                        }

                        if (!this.partMap[forTest].ContainsKey(E_sGseDeliveryTargetT.FreddieMac))
                        {
                            this.partMap[forTest].Add(E_sGseDeliveryTargetT.FreddieMac, part);
                        }
                    }
                    else if (deliveryTarget != null)
                    {
                        this.partMap[forTest][deliveryTarget.Value] = part;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new instance of the UlddPhase3Settings class.
        /// </summary>
        /// <param name="brokerTempOptionXml">The broker options pseudo-xml blob to create from.</param>
        /// <returns>A new instance of the <see cref="UlddPhase3Settings"/> class for the given broker.</returns>
        public static UlddPhase3Settings Create(string brokerTempOptionXml)
        {
            if (brokerTempOptionXml == null)
            {
                return null;
            }
            else
            {
                return new UlddPhase3Settings(brokerTempOptionXml);
            }
        }

        /// <summary>
        /// Gets the part of ULDD Phase 3 exports configured to send for the given delivery target / test status combination.
        /// </summary>
        /// <param name="deliveryTarget">The delivery target that will be receiving the ULDD export.</param>
        /// <param name="loanIsTest">Whether the loan being exported is a test loan.</param>
        /// <returns>The ULDD Phase 3 part to export, if any.</returns>
        public UlddPhase3Part GetPart(E_sGseDeliveryTargetT deliveryTarget, bool loanIsTest)
        {
            if (deliveryTarget != E_sGseDeliveryTargetT.FannieMae && deliveryTarget != E_sGseDeliveryTargetT.FreddieMac)
            {
                return UlddPhase3Part.UsePhase2Instead;
            }
            else
            {
                UlddPhase3Part part;
                return this.partMap[loanIsTest].TryGetValue(deliveryTarget, out part) ? part : UlddPhase3Part.UsePhase2Instead;
            }
        }
    }
}