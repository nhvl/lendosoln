﻿#region Generated code
namespace LendersOffice.Admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CommonLib;
    using DataAccess;
    using Drivers.ConversationLog;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migrators;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Exceptions;
    using ObjLib.Security.Authorization.ConversationLog;

    /// <summary>
    /// Provides a means for setting default values for
    /// a new broker.
    /// </summary>
    public class BrokerDefaultValueSetter
    {
        /// <summary>
        /// Represents the phone number for LQB.
        /// </summary>
        private const string LqbPhoneNumber = "(714) 957-6335";

        /// <summary>
        /// Provides a list of default groups assigned to a broker on creation.
        /// </summary>
        /// <remarks>
        /// This list excludes testing branch and PML broker groups, as the IDs 
        /// of those groups are needed to update the group membership of the 
        /// broker's default branch and PML broker.
        /// </remarks>
        private static readonly List<DefaultBrokerGroup> DefaultBrokerGroups = new List<DefaultBrokerGroup>()
        {
            new DefaultBrokerGroup(
                "ACCESS: Allow access to PRODUCTION branch loans?",
                GroupType.Employee,
                "Employee will have access to loans in PRODUCTION branches according to their access level (e.g. Corporate, Branch, Individual). [LL]"),

            new DefaultBrokerGroup(
                "Allow accessing employee loans?",
                GroupType.Employee,
                "Employee is authorized to mark a loan as an employee loan and view employee loans. [LL]"),

            new DefaultBrokerGroup(
                "Allow bypassing field restrictions?",
                GroupType.Employee,
                "Employee is not subject to configurable workflow rules and field restrictions. [LL]"),

            new DefaultBrokerGroup(
                "Is business rule tester?",
                GroupType.Employee,
                "Employee can test and validate business rules before they are pushed to all production users. [LL]"),

            new DefaultBrokerGroup(
                "PRODUCTION branch",
                GroupType.Branch,
                "All branches generating live production loans must be a member of this branch group."),

            new DefaultBrokerGroup(
                "ACCESS: Allow access to PRODUCTION branch loans?",
                GroupType.PmlBroker,
                "Originating company will have access to loans in PRODUCTION branches according to their access level. [LL]"),

            new DefaultBrokerGroup(
                "Allow accessing historical pricing?",
                GroupType.Employee,
                "Users in this group will have access to Historical Pricing functionality within Internal Pricing.")
        };

        /// <summary>
        /// Represents the permissions not granted to all default B-Users
        /// of the new broker.
        /// </summary>
        private static readonly Permission[] DefaultNotGrantedPermissions = new[]
        {
            Permission.AllowCapitalMarketsAccess,
            Permission.CanModifyAdministrativeItem,
            Permission.IndividualLevelAccess,
            Permission.BranchLevelAccess,
            Permission.TeamLevelAccess,
            Permission.CanModifyLoanPrograms,
            Permission.AllowLoadingDeletedLoans,
            Permission.AllowViewingNewLoanEditorUI
        };

        /// <summary>
        /// Represents the disclosure permissions not granted to internal 
        /// B-Users for the new broker.
        /// </summary>
        private static readonly Permission[] NotGrantedDisclosurePermissions = new[]
        {
            Permission.ResponsibleForInitialDiscRetail,
            Permission.ResponsibleForInitialDiscWholesale,
            Permission.ResponsibleForRedisclosures
        };

        /// <summary>
        /// Represents all non-consumer roles for condition categories.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> AllRolesList =
            from E_RoleT role in Enum.GetValues(typeof(E_RoleT))
            where role != E_RoleT.Consumer
            select role;

        /// <summary>
        /// Represents the roles used for managed and internal permission
        /// levels.
        /// </summary>
        private static readonly E_RoleT[] ManagedInternalRoleList = new[]
        {
            E_RoleT.Underwriter,
            E_RoleT.Manager,
            E_RoleT.LockDesk
        };

        /// <summary>
        /// The roles for TPO users.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> TpoRoles = new HashSet<E_RoleT>()
        {
            E_RoleT.Pml_Administrator,
            E_RoleT.Pml_BrokerProcessor,
            E_RoleT.Pml_LoanOfficer,
            E_RoleT.Pml_PostCloser,
            E_RoleT.Pml_Secondary
        };

        /// <summary>
        /// The LQB only roles.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> LqbRolesExcludeTpoRoles =
            from E_RoleT role in Enum.GetValues(typeof(E_RoleT))
            where !TpoRoles.Contains(role) && role != E_RoleT.Consumer
            select role;

        /// <summary>
        /// The manage roles for the Condition: Processing permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondProcessingManageRoles = new E_RoleT[]
        {
            E_RoleT.Processor,
            E_RoleT.Underwriter
        };

        /// <summary>
        /// The close roles for the Condition: Processing permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondProcessingCloseRoles = new E_RoleT[]
        {
            E_RoleT.Processor,
            E_RoleT.Underwriter,
            E_RoleT.JuniorUnderwriter
        };

        /// <summary>
        /// The manage roles for the Condition: UW permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondUwManageRoles = new E_RoleT[]
        {
            E_RoleT.Underwriter
        };

        /// <summary>
        /// The close roles for the Condition: UW permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondUwCloseRoles = new E_RoleT[]
        {
            E_RoleT.Underwriter,
            E_RoleT.JuniorUnderwriter
        };

        /// <summary>
        /// The manage and close roles for the Condition: PTF permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondPtfCloseManageRoles = new E_RoleT[]
        {
            E_RoleT.Funder,
            E_RoleT.Underwriter
        };

        /// <summary>
        /// The manage and close roles for the Condition: PTI permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondPtiCloseManageRoles = new E_RoleT[]
        {
            E_RoleT.PostCloser,
            E_RoleT.Underwriter
        };

        /// <summary>
        /// The manage and close roles for the Condition: QC permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> CondQcCloseManageRoles = new E_RoleT[]
        {
            E_RoleT.QCCompliance,
            E_RoleT.Underwriter
        };

        /// <summary>
        /// The manage and close roles for the Task: TPO permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> TaskTpoCloseManageRoles = new E_RoleT[]
        {
            E_RoleT.Administrator,
            E_RoleT.LoanOpener,
            E_RoleT.JuniorUnderwriter,
            E_RoleT.Underwriter,
            E_RoleT.Closer
        };

        /// <summary>
        /// The close roles for the Task: Conversation Log permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> TaskConvoLogCloseRoles = new E_RoleT[]
        {
            E_RoleT.Administrator,
            E_RoleT.PostCloser
        };

        /// <summary>
        /// The manage roles for the Task: Conversation Log permission.
        /// </summary>
        private static readonly IEnumerable<E_RoleT> TaskConvoLogManageRoles = new E_RoleT[]
        {
            E_RoleT.Administrator
        };

        /// <summary>
        /// The names of conversation log categories to be created if we are enabling the conversation log for the broker.
        /// </summary>
        private static readonly string[] ConversationLogDefaultCategoryNames = new string[]
        {
            "ORIGINATING", "PROCESSING", "DISCLOSURES", "UNDERWRITING", "DOCS/FUNDING", "POST-CLOSING", "PURCHASE/ACCOUNTING", "SECONDARY"
        };

        /// <summary>
        /// Represents the <see cref="Address"/> instance corresponding
        /// to the LQB address.
        /// </summary>
        private static readonly Address LqbAddress = ConstApp.GetLendingQBAddress();

        /// <summary>
        /// Represents the id of the default branch for the broker.
        /// </summary>
        private readonly Guid defaultBranchId;
        
        /// <summary>
        /// Represents the PML mode for the broker.
        /// </summary>
        private readonly E_Pml2AsQuickPricerMode pmlMode;

        /// <summary>
        /// Whether or not they intended to enable the conversation log for loans.
        /// </summary>
        private readonly bool enableConversationLogForLoans;

        /// <summary>
        /// Whether or not they intended to enable the conversation log in the tpo portal.
        /// </summary>
        private readonly bool enableConversationLogInTpo;

        /// <summary>
        /// Represents the id of the user creating the new broker.
        /// </summary>
        private readonly Guid editingUserId;

        /// <summary>
        /// Represents the log for errors encountered while setting
        /// broker defaults.
        /// </summary>
        private StringBuilder errorLog;

        /// <summary>
        /// Represents the new broker to set default values for.
        /// </summary>
        private BrokerDB broker;

        /// <summary>
        /// The id of the created group for loan deletion, or null if the group failed to be created.
        /// </summary>
        private Guid? deleteLoanGroupId;

        /// <summary>
        /// The id of the created group that allows test creation, or null if the group failed to be created.
        /// </summary>
        private Guid? allowTestLoanAccessGroupId;

        /// <summary>
        /// Represents the user id of the new broker's super user.
        /// </summary>
        /// <remarks>
        /// This field is used to create an <see cref="AbstractUserPrincipal"/>
        /// instance when assigning default QP and PML templates to
        /// the new broker.
        /// </remarks>
        private Guid superUserId;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerDefaultValueSetter"/>
        /// class.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> id for the new broker.
        /// </param>
        /// <param name="editingUserId">
        /// The <see cref="Guid"/> user id for the user adding the
        /// new broker.
        /// </param>
        /// <param name="pmlMode">
        /// The <see cref="E_Pml2AsQuickPricerMode"/> for the
        /// new broker.
        /// </param>
        /// <param name="enableConversationLogForLoans">
        /// Whether or not to enable the conversation log for loans for the new broker.
        /// </param>
        /// <param name="enableConversationLogInTpo">
        /// Whether or not to enable the conversation log for tpo for the new broker.
        /// </param>
        public BrokerDefaultValueSetter(Guid brokerId, Guid editingUserId, E_Pml2AsQuickPricerMode pmlMode, bool enableConversationLogForLoans, bool enableConversationLogInTpo)
        {
            if (brokerId == Guid.Empty || editingUserId == Guid.Empty)
            {
                var message = $"Failed to set default values for broker using brokerId {brokerId} and editingUserId {editingUserId}.";

                throw new CBaseException(ErrorMessages.FailedToSaveBrokerDefaults, message);
            }

            this.defaultBranchId = BranchDB.GetBranchObjects(brokerId).First().BranchID;
            this.editingUserId = editingUserId;
            this.pmlMode = pmlMode;
            this.enableConversationLogForLoans = enableConversationLogForLoans;
            this.enableConversationLogInTpo = enableConversationLogInTpo;

            this.errorLog = new StringBuilder();
            this.broker = BrokerDB.RetrieveById(brokerId);
        }

        /// <summary>
        /// Sets values for originating companies, users,
        /// groups, and other broker defaults.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> representing the error log
        /// for the default value setter.
        /// </returns>
        public string SetDefaultValues()
        {
            this.SetBrokerDefault(
                this.SetCorporateAdminSettings,
                "Could not set corporate admin settings. ");

            this.SetBrokerDefault(
                this.SetLockPolicy,
                "Could not create default lock policy. This broker requires a migration to add the default lock policy. ");

            this.SetBrokerDefault(
                this.SetOriginatingCompany,
                "Could not create default originating company. ");

            this.SetBrokerDefault(
                this.SetNoDependencyGroups,
                "Could not create default workflow groups. ");

            this.SetBrokerDefault(
                this.SetGroupsThatAreDependedOn,
                "Could not create default Groups.");

            this.SetBrokerDefault(
                this.SetBUsers,
                "Could not create default B users. ");

            if (this.enableConversationLogForLoans)
            {
                this.SetBrokerDefault(
                    this.SetConversationLogDefaults,
                    "Could not enable conversation log.");
            }

            this.SetBrokerDefault(
                this.SetPermissionLevels,
                "Could not create default task permission levels. ");

            this.SetBrokerDefault(
                this.SetPriceGroups,
                "Could not create default price groups. ");

            this.SetBrokerDefault(
                this.SetLoanTemplates,
                "Could not create default loan templates.");

            this.broker.Save();

            return this.errorLog.ToString();
        }

        /// <summary>
        /// Sets the default permission levels and categories when enabling the conversation log feature.
        /// </summary>
        private void SetConversationLogDefaults()
        {
            this.SetBrokerDefault(
                this.SetConversationLogPermissionLevels,
                "Could not set default permission levels for conversation log.");

            SetConversationLogDefaultCategories();
            this.broker.EnableConversationLog(this.enableConversationLogInTpo);
        }

        /// <summary>
        /// Sets the default conversation log permission levels.
        /// </summary>
        private void SetConversationLogPermissionLevels()
        {
            var brokerId = BrokerIdentifier.Create(this.broker.BrokerID.ToString()).Value;
            var allRoleIds = Role.AllRoles.Select(r => r.Id).ToArray();

            this.SetBrokerDefault(
                () =>
                {
                    var level = new ConversationLogPermissionLevel(brokerId)
                    {
                        Name = "ALL",
                        Description = "View, Reply, Post and Hide",
                        IsActive = false
                    };
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.View, allRoleIds);
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.Post, allRoleIds);
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.Reply, allRoleIds);
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.Hide, allRoleIds);
                    level.Save();
                },
                "Could not create the 'All' conversation log permission level.");

            this.SetBrokerDefault(
                () =>
                {
                    var level = new ConversationLogPermissionLevel(brokerId)
                    {
                        Name = "LIMITED",
                        Description = "All Roles Limited Access",
                        IsActive = true
                    };
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.View, allRoleIds);
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.Post, allRoleIds);
                    level.SetRoleIdsForOperationType(ConvoLogOperationType.Reply, allRoleIds);
                    level.Save();
                },
                "Could not create the 'Limited' conversation log permission level.");
        }

        /// <summary>
        /// Sets the conversation log default categories with their default permission level as specified.  Can throw an error on fail.
        /// </summary>
        private void SetConversationLogDefaultCategories()
        {
            var securityToken = SecurityService.CreateTokenOfInternalUser(this.broker.BrokerID.ToString());
            var ownerId = CommentCategoryNamespace.Create(this.broker.BrokerID.ToString()).Value;

            foreach (var categoryName in ConversationLogDefaultCategoryNames)
            {
                var category = new Category()
                {
                    IsActive = true,
                    Identity = new CategoryReference()
                    {
                        Name = CommentCategoryName.Create(categoryName).Value,
                        OwnerId = ownerId,
                        Id = -1
                    },
                    DisplayName = CommentCategoryName.Create(categoryName).Value,
                    DefaultPermissionLevelId = null
                };
                ConversationLogHelper.SetCategory(securityToken, category, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
            }
        }

        /// <summary>
        /// Sets corporate admin settings for the broker.
        /// </summary>
        private void SetCorporateAdminSettings()
        {
            this.broker.IsOnlyAccountantCanModifyTrustAccount = true;
            this.broker.UseRateSheetExpirationFeature = true;

            this.broker.IsAllowExternalUserToCreateNewLoan = false;
            this.broker.TriggerAprRediscNotifForAprDecrease = false;
        }

        /// <summary>
        /// Sets the default lock policy for the new broker.
        /// </summary>
        private void SetLockPolicy()
        {
            LockPolicyAndPriceGroupMigratorFor105723.LockPolicyBrokerMigratorFor105723.MigrateBroker(this.broker.BrokerID);
        }

        /// <summary>
        /// Sets the default originating company for the new broker and
        /// adds a default P-User associated with the originating company.
        /// </summary>
        private void SetOriginatingCompany()
        {
            var testingPmlBrokerGroupId = GroupDB.CreateGroup(
                this.broker.BrokerID,
                "ACCESS: Allow access to TEST branch loans?",
                GroupType.PmlBroker,
                "Originating Company will have access to loans in TEST-related branches. [LL]");

            var defaultCompany = PmlBroker.CreatePmlBroker(this.broker.BrokerID);

            defaultCompany.Name = "PRICEMYLOAN.COM";
            defaultCompany.Phone = BrokerDefaultValueSetter.LqbPhoneNumber;

            defaultCompany.BrokerRoleStatusT = OCStatusType.Approved;
            defaultCompany.MiniCorrRoleStatusT = OCStatusType.Approved;
            defaultCompany.CorrRoleStatusT = OCStatusType.Approved;

            defaultCompany.Roles.Add(E_OCRoles.Broker);
            defaultCompany.Roles.Add(E_OCRoles.Correspondent);
            defaultCompany.Roles.Add(E_OCRoles.MiniCorrespondent);

            defaultCompany.Addr = BrokerDefaultValueSetter.LqbAddress.StreetAddress;
            defaultCompany.City = BrokerDefaultValueSetter.LqbAddress.City;
            defaultCompany.State = BrokerDefaultValueSetter.LqbAddress.State;
            defaultCompany.Zip = BrokerDefaultValueSetter.LqbAddress.Zipcode;

            defaultCompany.BranchId = this.defaultBranchId;
            defaultCompany.MiniCorrespondentBranchId = this.defaultBranchId;
            defaultCompany.CorrespondentBranchId = this.defaultBranchId;

            // We want the company to use the branch setting.
            defaultCompany.LpePriceGroupId = Guid.Empty;
            defaultCompany.MiniCorrespondentLpePriceGroupId = Guid.Empty;
            defaultCompany.CorrespondentLpePriceGroupId = Guid.Empty;

            defaultCompany.Permissions.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, true);
            defaultCompany.Permissions.SetPermission(Permission.CanSubmitWithoutCreditReport, true);

            defaultCompany.Save();

            GroupDB.UpdatePmlBrokerGroups(this.broker.BrokerID, defaultCompany.PmlBrokerId, testingPmlBrokerGroupId.ToString());
        }

        /// <summary>
        /// Creates a new PML user for the new broker.
        /// </summary>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> ID for the pml broker
        /// of the broker.
        /// </param>
        private void CreatePUserForDefaultOriginatingCompany(Guid pmlBrokerId)
        {
            using (var executor = new CStoredProcedureExec(this.broker.BrokerID))
            {
                try
                {
                    executor.BeginTransactionForWrite();

                    var pmlUser = new EmployeeDB(Guid.Empty, this.broker.BrokerID)
                    {
                        UserType = 'P',
                        BranchID = this.defaultBranchId,
                        PmlBrokerId = pmlBrokerId,
                        FirstName = this.broker.Name,
                        LastName = "PmlUser",
                        Email = ConstStage.DefaultDoNotReplyAddress,
                        Phone = BrokerDefaultValueSetter.LqbPhoneNumber,
                        Address = BrokerDefaultValueSetter.LqbAddress,
                        LoginName = this.broker.CustomerCode + "PmlUser",
                        Password = this.broker.CustomerCode,
                        IsActive = true,
                        AllowLogin = true,
                        Roles = CEmployeeFields.s_LoanRepRoleId.ToString(),
                        TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT.DontReceiveEmail,
                        PopulateBrokerRelationshipsT = EmployeePopulationMethodT.OriginatingCompany,
                        PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany,
                        PopulateCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany,
                        PopulatePMLPermissionsT = EmployeePopulationMethodT.OriginatingCompany,
                        WhoDoneIt = SystemUserPrincipal.TaskSystemUser.UserId // this user is in all the databases.
                    };

                    pmlUser.Save(executor);

                    var permissions = new BrokerUserPermissions(this.broker.BrokerID, pmlUser.ID, executor);

                    permissions.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, true);
                    permissions.SetPermission(Permission.CanSubmitWithoutCreditReport, true);

                    permissions.SetPermission(Permission.AllowViewingWholesaleChannelLoans, true);
                    permissions.SetPermission(Permission.AllowViewingMiniCorrChannelLoans, true);
                    permissions.SetPermission(Permission.AllowViewingCorrChannelLoans, true);

                    permissions.SetPermission(Permission.AllowCreatingWholesaleChannelLoans, true);
                    permissions.SetPermission(Permission.AllowCreatingMiniCorrChannelLoans, true);
                    permissions.SetPermission(Permission.AllowCreatingCorrChannelLoans, true);

                    permissions.Update(executor);

                    executor.CommitTransaction();
                }
                catch
                {
                    executor.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Adds default groups that other defaults are not dependent upon
        /// to the new broker and sets the group for the broker's default 
        /// branch. 
        /// </summary>
        private void SetNoDependencyGroups()
        {
            foreach (var defaultGroup in BrokerDefaultValueSetter.DefaultBrokerGroups)
            {
                GroupDB.CreateGroup(this.broker.BrokerID, defaultGroup.Name, defaultGroup.GroupType, defaultGroup.Description);
            }

            var testingBranchGroupId = GroupDB.CreateGroup(
                this.broker.BrokerID,
                "TEST branch",
                GroupType.Branch,
                "Loans in this branch are for testing purposes only and will not appear on production reports or by production user accounts.  An employee's access to loans in a testing branch are controlled by employee group membership.");

            GroupDB.UpdateBranchGroups(this.broker.BrokerID, this.defaultBranchId, testingBranchGroupId.ToString());
        }

        /// <summary>
        /// Adds additional groups that are dependend on.
        /// </summary>
        private void SetGroupsThatAreDependedOn()
        {
            this.SetBrokerDefault(
                () =>
                this.deleteLoanGroupId = GroupDB.CreateGroup(
                    this.broker.BrokerID,
                    "Delete Loan",
                    GroupType.Employee,
                    "Employee can delete loans [LL]"),
                "Could not create 'Delete Loan' group.");

            this.SetBrokerDefault(
                () =>
                this.allowTestLoanAccessGroupId = GroupDB.CreateGroup(
                    this.broker.BrokerID,
                    "ACCESS: Allow access to TEST branch loans?",
                    GroupType.Employee,
                    "Employee will have access to loans in the TEST-related branch(es). [LL]"),
                "Could not create 'ACCESS: Allow access to TEST branch loans?' group.");
        }

        /// <summary>
        /// Sets the default P and B type users for the new broker.
        /// </summary>
        /// <remarks>
        /// We use a <see cref="CStoredProcedureExec"/> in this method
        /// to avoid orphaning users and/or permissions in the event
        /// that we are unable to save a specific user.
        /// </remarks>
        private void SetBUsers()
        {
            var internalUserDeniedPermissions = BrokerDefaultValueSetter.DefaultNotGrantedPermissions.Union(BrokerDefaultValueSetter.NotGrantedDisclosurePermissions);

            var superUserDeniedPermissions = internalUserDeniedPermissions.Except(new[] { Permission.CanModifyLoanPrograms });

            this.SetBrokerDefault(
                () => this.CreateBUserTransactionally(
                        lastName: "ClientLogin",
                        phone: "(123) 456-7890",
                        loginName: this.broker.CustomerCode + "ClientLogin",
                        address: new Address(),
                        isAccountOwner: true,
                        assignTestLoanAccess: false,
                        assignSuperUserId: false,
                        receiveEmailType: E_NewOnlineLoanEventNotifOptionT.ReceiveEmail,
                        ratesheetExpriationBypassType: E_RatesheetExpirationBypassType.BypassAll,
                        permissionsNotGranted: BrokerDefaultValueSetter.DefaultNotGrantedPermissions,
                        isShareable: false),
                "Could not create client login user.");

            this.SetBrokerDefault(
                () => this.CreateBUserTransactionally(
                        lastName: "Support",
                        phone: BrokerDefaultValueSetter.LqbPhoneNumber,
                        loginName: "s" + this.broker.CustomerCode,
                        address: BrokerDefaultValueSetter.LqbAddress,
                        isAccountOwner: false,
                        assignTestLoanAccess: true,
                        assignSuperUserId: false,
                        receiveEmailType: E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail,
                        ratesheetExpriationBypassType: E_RatesheetExpirationBypassType.BypassAll,
                        permissionsNotGranted: superUserDeniedPermissions,
                        isShareable: true),
                "Could not create support user.");

            this.SetBrokerDefault(
                () => this.CreateBUserTransactionally(
                    lastName: "Training",
                    phone: BrokerDefaultValueSetter.LqbPhoneNumber,
                    loginName: "t" + this.broker.CustomerCode,
                    address: BrokerDefaultValueSetter.LqbAddress,
                    isAccountOwner: false,
                    assignTestLoanAccess: true,
                    assignSuperUserId: false,
                    receiveEmailType: E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail,
                    ratesheetExpriationBypassType: E_RatesheetExpirationBypassType.BypassAll,
                    permissionsNotGranted: superUserDeniedPermissions,
                    isShareable: true),
                "Could not create training user.");

            this.SetBrokerDefault(
                () => this.CreateBUserTransactionally(
                    lastName: this.broker.CustomerCode,
                    phone: BrokerDefaultValueSetter.LqbPhoneNumber,
                    loginName: this.broker.CustomerCode,
                    address: BrokerDefaultValueSetter.LqbAddress,
                    isAccountOwner: false,
                    assignTestLoanAccess: true,
                    assignSuperUserId: true,
                    receiveEmailType: E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail,
                    ratesheetExpriationBypassType: E_RatesheetExpirationBypassType.BypassAll,
                    permissionsNotGranted: superUserDeniedPermissions,
                    isShareable: true),
                "Could not create super user.");
        }

        /// <summary>
        /// Creates a new B-type user for the new broker.
        /// </summary>
        /// <param name="executor">
        /// The <see cref="CStoredProcedureExec"/> used to save
        /// the user and the user's permissions.
        /// </param>
        /// <param name="lastName">
        /// The <see cref="string"/> last name of the user.
        /// </param>
        /// <param name="phone">
        /// The <see cref="string"/> phone for the user.
        /// </param>
        /// <param name="loginName">
        /// The <see cref="string"/> login name for the user.
        /// </param>
        /// <param name="address">
        /// The <see cref="Address"/> for the user.
        /// </param>
        /// <param name="isAccountOwner">
        /// True if the user is the broker account owner,
        /// false otherwise.
        /// </param>
        /// <param name="assignTestLoanAccess">
        /// True if the user is assigned to the test loan
        /// access group, false otherwise.
        /// </param>
        /// <param name="assignSuperUserId">
        /// True if the user is a super user that should be used to
        /// create the new broker's default loan templates, false
        /// otherwise.
        /// </param>
        /// <param name="receiveEmailType">
        /// The <see cref="E_NewOnlineLoanEventNotifOptionT"/>
        /// loan event notification type for the user.
        /// </param>
        /// <param name="ratesheetExpriationBypassType">
        /// The ratesheet expiration bypass type.
        /// </param>
        /// <param name="permissionsNotGranted">
        /// An <see cref="IEnumerable{T}"/> containing the permissions
        /// not granted to the user. All other permissions will be granted.
        /// </param>
        /// <param name="isShareable">
        /// Whether or not multiple devices can login at the same time for this user.
        /// </param>
        /// <returns>The id of the created employee, or null if none created.</returns>
        private Guid? CreateBUser(
            CStoredProcedureExec executor,
            string lastName,
            string phone,
            string loginName,
            Address address,
            bool isAccountOwner,
            bool assignTestLoanAccess,
            bool assignSuperUserId,
            E_NewOnlineLoanEventNotifOptionT receiveEmailType,
            E_RatesheetExpirationBypassType? ratesheetExpriationBypassType,
            IEnumerable<Permission> permissionsNotGranted,
            bool isShareable)
        {                
            var employee = new EmployeeDB(Guid.Empty, this.broker.BrokerID)
            {
                UserType = 'B',
                BranchID = this.defaultBranchId,
                FirstName = this.broker.Name,
                LastName = lastName,
                Email = ConstStage.DefaultDoNotReplyAddress,
                Phone = phone,
                Address = address,
                LoginName = loginName,
                Password = this.broker.CustomerCode,
                IsSharable = isShareable,
                IsActive = true,
                AllowLogin = true,
                IsAccountOwner = isAccountOwner,
                Roles = CEmployeeFields.s_AdministratorRoleId + "," + CEmployeeFields.s_ManagerRoleId,
                NewOnlineLoanEventNotifOptionT = receiveEmailType,
                WhoDoneIt = SystemUserPrincipal.TaskSystemUser.UserId, // this user is in all the databases.
                EnableAuthCodeViaSms = false
            };

            if (ratesheetExpriationBypassType.HasValue)
            {
                employee.RatesheetExpirationBypassType = ratesheetExpriationBypassType.Value;
            }

            employee.Save(executor);

            var permissions = new BrokerUserPermissions(this.broker.BrokerID, employee.ID, executor);

            foreach (Permission permission in Enum.GetValues(typeof(Permission)))
            {
                if (permissionsNotGranted.Contains(permission))
                {
                    permissions.SetPermission(permission, false);
                }
                else
                {
                    permissions.SetPermission(permission, true);
                }
            }

            permissions.Update(executor);            

            if (assignSuperUserId)
            {
                this.superUserId = employee.UserID;
            }

            return employee.ID;
        }

        /// <summary>
        /// Creates the user with the specified parameters such that the permissions and user are created in one transaction. <para></para>
        /// Also assigns them to certain groups depending.
        /// </summary>
        /// <param name="lastName">
        /// The <see cref="string"/> last name of the user.
        /// </param>
        /// <param name="phone">
        /// The <see cref="string"/> phone for the user.
        /// </param>
        /// <param name="loginName">
        /// The <see cref="string"/> login name for the user.
        /// </param>
        /// <param name="address">
        /// The <see cref="Address"/> for the user.
        /// </param>
        /// <param name="isAccountOwner">
        /// True if the user is the broker account owner,
        /// false otherwise.
        /// </param>
        /// <param name="assignTestLoanAccess">
        /// True if the user is assigned to the test loan
        /// access group, false otherwise.
        /// </param>
        /// <param name="assignSuperUserId">
        /// True if the user is a super user that should be used to
        /// create the new broker's default loan templates, false
        /// otherwise.
        /// </param>
        /// <param name="receiveEmailType">
        /// The <see cref="E_NewOnlineLoanEventNotifOptionT"/>
        /// loan event notification type for the user.
        /// </param>
        /// <param name="ratesheetExpriationBypassType">
        /// The ratesheet expiration bypass type.
        /// </param>
        /// <param name="permissionsNotGranted">
        /// An <see cref="IEnumerable{T}"/> containing the permissions
        /// not granted to the user. All other permissions will be granted.
        /// </param>
        /// <param name="isShareable">
        /// Whether or not multiple devices can login at the same time for this user.
        /// </param>
        private void CreateBUserTransactionally(
            string lastName,
            string phone,
            string loginName,
            Address address,
            bool isAccountOwner,
            bool assignTestLoanAccess,
            bool assignSuperUserId,
            E_NewOnlineLoanEventNotifOptionT receiveEmailType,
            E_RatesheetExpirationBypassType? ratesheetExpriationBypassType,
            IEnumerable<Permission> permissionsNotGranted,
            bool isShareable)
        {
            Guid? createdEmployeeId = null;
            using (var executor = new CStoredProcedureExec(this.broker.BrokerID))
            {
                try
                {
                    executor.BeginTransactionForWrite();

                    var tmpCreatedEmployeeId = this.CreateBUser(
                        executor,
                        lastName,
                        phone,
                        loginName,
                        address,
                        isAccountOwner,
                        assignTestLoanAccess,
                        assignSuperUserId,
                        receiveEmailType,
                        ratesheetExpriationBypassType,
                        permissionsNotGranted,
                        isShareable);

                    executor.CommitTransaction();

                    createdEmployeeId = tmpCreatedEmployeeId;
                }
                catch
                {
                    executor.RollbackTransaction();

                    throw;
                }
            }

            if (createdEmployeeId.HasValue)
            {
                this.SetBrokerDefault(
                    () =>
                    {
                        var groupAccessIds = assignTestLoanAccess ?
                            this.deleteLoanGroupId.Value + "," + this.allowTestLoanAccessGroupId.Value :
                            this.deleteLoanGroupId.Value.ToString();
                        GroupDB.UpdateEmployeeGroups(this.broker.BrokerID, createdEmployeeId.Value, groupAccessIds);
                    },
                    $"Failed to give user {loginName} the appropriate groups.");
            }
        }

        /// <summary>
        /// Sets task permission levels and conditions categories.
        /// </summary>
        private void SetPermissionLevels()
        {
            var condProcessingPermissionLvlId = this.CreatePermissionLevel(
                "Condition: Processing",
                $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: Processor, Underwriter, Jr. Underwriter{Environment.NewLine}MANAGE: Processor, Underwriter",
                true,
                0,
                LqbRolesExcludeTpoRoles,
                CondProcessingManageRoles,
                CondProcessingCloseRoles);

            var condUwPermissionLvlId = this.CreatePermissionLevel(
                "Condition: UW",
                $"WORK ON: All roles{Environment.NewLine}CLOSE: Underwriter, Jr. Underwriter{Environment.NewLine}MANAGE: Underwriter",
                true,
                1,
                AllRolesList,
                CondUwManageRoles,
                CondUwCloseRoles);

            var condPtfPermissionLvlId = this.CreatePermissionLevel(
                "Condition: PTF",
                $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: Funder, Underwriter{Environment.NewLine}MANAGE: Funder, Underwriter",
                true,
                2,
                LqbRolesExcludeTpoRoles,
                CondPtfCloseManageRoles,
                CondPtfCloseManageRoles);

            var condPtiPermissionLvlId = this.CreatePermissionLevel(
                "Condition: PTI",
                $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: Post-Closer, Underwriter{Environment.NewLine}MANAGE: Post-Closer, Underwriter",
                true,
                3,
                LqbRolesExcludeTpoRoles,
                CondPtiCloseManageRoles,
                CondPtiCloseManageRoles);

            var condQcPermissionLvlId = this.CreatePermissionLevel(
                "Condition: QC",
                $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: QC, Underwriter{Environment.NewLine}MANAGE: QC, Underwriter",
                true,
                4,
                LqbRolesExcludeTpoRoles,
                CondQcCloseManageRoles,
                CondQcCloseManageRoles);

            var taskLosPermissionLvlId = this.CreatePermissionLevel(
                "Task: LOS",
                $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: All LQB roles{Environment.NewLine}MANAGE: All LQB roles",
                false,
                5,
                LqbRolesExcludeTpoRoles,
                AllRolesList,
                AllRolesList);

            var taskTpoPermissionLvlId = this.CreatePermissionLevel(
                "Task: TPO",
                $"WORK ON: All roles{Environment.NewLine}CLOSE: Administrator, Loan Opener, Underwriter, Jr. Underwriter, Closer{Environment.NewLine}MANAGE: Administrator, Loan Opener, Jr. Underwriter, Underwriter, Closer",
                false,
                6,
                AllRolesList,
                TaskTpoCloseManageRoles,
                TaskTpoCloseManageRoles);

            if (!this.broker.EnableConversationLogForLoans)
            {
                var taskConvoLogPermissionLvlId = this.CreatePermissionLevel(
                    "Task: Conversation Log",
                    $"WORK ON: LendingQB user roles (excludes Originating Company user roles){Environment.NewLine}CLOSE: Administrator, Post-Closer{Environment.NewLine}MANAGE: Administrator",
                    false,
                    7,
                    LqbRolesExcludeTpoRoles,
                    TaskConvoLogManageRoles,
                    TaskConvoLogCloseRoles);
            }

            var processingCondition = this.CreateConditionCategory("PROCESSING", condProcessingPermissionLvlId);
            var ptaCondition = this.CreateConditionCategory("PTA", condUwPermissionLvlId);
            var ptdCondition = this.CreateConditionCategory("PTD", condUwPermissionLvlId);
            var suspenseCondition = this.CreateConditionCategory("SUSPENSE", condUwPermissionLvlId);
            var ausCondition = this.CreateConditionCategory("AUS", condUwPermissionLvlId);
            var creditCondition = this.CreateConditionCategory("CREDIT", condUwPermissionLvlId);
            var warningCondition = this.CreateConditionCategory("WARNING", condUwPermissionLvlId);
            var ptfCondition = this.CreateConditionCategory("PTF", condPtfPermissionLvlId);
            var ptiCondition = this.CreateConditionCategory("PTI", condPtiPermissionLvlId);
            var qcontrolCondition = this.CreateConditionCategory("QC", condQcPermissionLvlId);
            var miscCondition = this.CreateConditionCategory("MISC", condUwPermissionLvlId);

            this.broker.DefaultTaskPermissionLevelIdForImportedCategories = condUwPermissionLvlId;
            this.broker.AusImportDefaultConditionCategoryId = ausCondition;
        }

        /// <summary>
        /// Creates a new <see cref="PermissionLevel"/>.
        /// </summary>
        /// <param name="name">
        /// The <see cref="string"/> name for the permission level.
        /// </param>
        /// <param name="description">
        /// The <see cref="string"/> description for the permission level.
        /// </param>
        /// <param name="isAppliesToConditions">
        /// True if the permission level applies to conditions, false otherwise.
        /// </param>
        /// <param name="rank">
        /// The <see cref="int"/> rank for the permission level.
        /// </param>
        /// <param name="workOnRoles">
        /// An <see cref="IEnumerable{T}"/> of roles than can work
        /// on tasks with the permission level.
        /// </param>
        /// <param name="manageRoles">
        /// An <see cref="IEnumerable{T}"/> of roles that can manage
        /// tasks with the permission level.
        /// </param>
        /// <param name="closeRoles">
        /// An <see cref="IEnumerable{T}"/> of roles that can close tasks with 
        /// the permission level.
        /// </param>
        /// <returns>
        /// The <see cref="int"/> id for the new <see cref="PermissionLevel"/>.
        /// </returns>
        private int CreatePermissionLevel(
            string name, 
            string description, 
            bool isAppliesToConditions, 
            int rank,
            IEnumerable<E_RoleT> workOnRoles,
            IEnumerable<E_RoleT> manageRoles,
            IEnumerable<E_RoleT> closeRoles)
        {
            var permissionLevel = new PermissionLevel(this.broker.BrokerID)
            {
                Name = name,
                Description = description,
                IsActive = true,
                IsAppliesToConditions = isAppliesToConditions,
                Rank = rank
            };

            permissionLevel.SetRoles(E_PermissionLevel.WorkOn, workOnRoles);
            permissionLevel.SetRoles(E_PermissionLevel.Close, closeRoles);
            permissionLevel.SetRoles(E_PermissionLevel.Manage, manageRoles);

            permissionLevel.SetGroups(E_PermissionLevel.Manage, new List<Guid>());
            permissionLevel.SetGroups(E_PermissionLevel.Close, new List<Guid>());
            permissionLevel.SetGroups(E_PermissionLevel.WorkOn, new List<Guid>());

            permissionLevel.Save();

            return permissionLevel.Id;
        }

        /// <summary>
        /// Creates a new <see cref="ConditionCategory"/> using the specified
        /// <paramref name="categoryName"/> and <paramref name="defaultTaskPermissionLevelId"/>.
        /// </summary>
        /// <param name="categoryName">
        /// The <see cref="string"/> name for the category.
        /// </param>
        /// <param name="defaultTaskPermissionLevelId">
        /// The integer id for the default <see cref="PermissionLevel"/>.
        /// </param>
        /// <returns>
        /// The integer id for the new category.
        /// </returns>
        private int CreateConditionCategory(string categoryName, int defaultTaskPermissionLevelId)
        {
            var conditionCategory = new ConditionCategory(this.broker.BrokerID)
            {
                Category = categoryName,
                DefaultTaskPermissionLevelId = defaultTaskPermissionLevelId
            };

            conditionCategory.Save();

            return conditionCategory.Id;
        }

        /// <summary>
        /// Sets the default price groups for the new broker.
        /// </summary>
        private void SetPriceGroups()
        {
            Guid? defaultPriceGroupId;
            defaultPriceGroupId = CreatePriceGroup("CORPORATE");
            CreatePriceGroup("SECONDARY (NO MARGINS)");
            CreatePriceGroup("Z-LQB TESTING (DO NOT REMOVE/EDIT)");

            if (defaultPriceGroupId.HasValue)
            {
                this.broker.LpePriceGroupIdDefault = defaultPriceGroupId.Value;
            }
        }

        /// <summary>
        /// Creates the price group with the specified name and returns the id of the created price group, or null if it failed to be created.
        /// </summary>
        /// <param name="priceGroupName">The name of the price group to be created.</param>
        /// <returns>The id of the created price group, or null if it failed to get created.</returns>
        private Guid? CreatePriceGroup(string priceGroupName)
        {
            Guid? priceGroupId = null;
            this.SetBrokerDefault(
                () =>
                {
                    var priceGroup = PriceGroup.CreateNew(this.broker.BrokerID);
                    priceGroup.Name = priceGroupName;
                    priceGroup.Save(SystemUserPrincipal.TaskSystemUser, null);
                    priceGroupId = priceGroup.ID;
                },
                $"Could not create `{priceGroupName}` price group.");

            return priceGroupId;
        }

        /// <summary>
        /// Sets the default PML and QuickPricer loan templates for
        /// the new broker.
        /// </summary>
        private void SetLoanTemplates()
        {
            if (this.superUserId == Guid.Empty)
            {
                throw new InvalidOperationException(
                    "Cannot create loan templates using a nonexistent user.");
            }

            // We need to create the principal used by the employees of this new broker
            // to correctly set up the new broker's default loan templates. To prevent 
            // wiping the current principal, we'll cache the current principal here 
            // and then reset it once the default values have been saved.
            var currentPrincipal = PrincipalFactory.CurrentPrincipal;

            var superUserPrincipal = PrincipalFactory.Create(
                this.broker.BrokerID,
                this.superUserId,
                "B",
                allowDuplicateLogin: true,
                isStoreToCookie: false);

            this.SetPmlTemplate(superUserPrincipal);

            this.SetQuickPricerTemplate(superUserPrincipal);

            System.Threading.Thread.CurrentPrincipal = currentPrincipal;
        }

        /// <summary>
        /// Sets the default PML loan template for the new broker
        /// using the specified <paramref name="superUserPrincipal"/>.
        /// </summary>
        /// <param name="superUserPrincipal">
        /// The <see cref="AbstractUserPrincipal"/> instance for
        /// the super user of the new broker.
        /// </param>
        private void SetPmlTemplate(AbstractUserPrincipal superUserPrincipal)
        {
            var creator = CLoanFileCreator.GetCreator(superUserPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            var pmlTemplateId = creator.CreateBlankLoanTemplate();

            var loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                pmlTemplateId,
                typeof(BrokerDefaultValueSetter));

            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            loanData.SetsLNmWithPermissionBypass("PML Default Template");

            loanData.sProdIncludeNormalProc = true;
            loanData.sProdIncludeFHATotalProc = true;

            loanData.Save();

            this.broker.Pml2AsQuickPricerMode = this.pmlMode;

            this.broker.PmlLoanTemplateID = pmlTemplateId;
            this.broker.MiniCorrLoanTemplateID = pmlTemplateId;
            this.broker.CorrLoanTemplateID = pmlTemplateId;
        }

        /// <summary>
        /// Sets the default QuickPricer loan template for the new broker
        /// using the specified <paramref name="superUserPrincipal"/>.
        /// </summary>
        /// <param name="superUserPrincipal">
        /// The <see cref="AbstractUserPrincipal"/> instance for
        /// a user of the new broker.
        /// </param>
        private void SetQuickPricerTemplate(AbstractUserPrincipal superUserPrincipal)
        {
            var creator = CLoanFileCreator.GetCreator(superUserPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            var quickpricerTemplateId = creator.CreateBlankLoanTemplate();

            var loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                quickpricerTemplateId,
                typeof(BrokerDefaultValueSetter));

            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            loanData.SetsLNmWithPermissionBypass("QuickPricer Template");

            loanData.sProdIncludeNormalProc = true;
            loanData.sProdIncludeFHATotalProc = true;
            loanData.sFfUfMipIsBeingFinanced = true;

            loanData.sProdAvailReserveMonths = 48;

            loanData.sProd3rdPartyUwResultT = E_sProd3rdPartyUwResultT.DU_ApproveEligible;

            if (!loanData.sIsIncomeCollectionEnabled)
            {
                loanData.GetAppData(0).aBBaseI = 500000.00M;
            }
            else
            {
                var borrowerId = loanData.GetAppData(0).aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var baseIncome = new IncomeSource()
                {
                    IncomeType = IncomeType.BaseIncome,
                    MonthlyAmountData = Money.Create(500000.00M)
                };
                loanData.AddIncomeSource(borrowerId, baseIncome);
            }

            loanData.Save();

            broker.IsQuickPricerEnable = true;

            broker.QuickPricerTemplateId = quickpricerTemplateId;
        }

        /// <summary>
        /// Attempts to run the <paramref name="defaultSetter"/> and, if
        /// an error is encountered, appends the error to the <see cref="errorLog"/>.
        /// </summary>
        /// <param name="defaultSetter">
        /// The <see cref="Action"/> used to set a broker default.
        /// </param>
        /// <param name="errorMessageOnFailure">
        /// The error message to append to the <see cref="errorLog"/> if the action
        /// fails.
        /// </param>
        private void SetBrokerDefault(Action defaultSetter, string errorMessageOnFailure)
        {
            try
            {
                defaultSetter();
            }
            catch (CBaseException exc) 
            {
                this.LogBrokerDefaultSetError(exc, errorMessageOnFailure);
            }
            catch (SystemException exc)
            {
                this.LogBrokerDefaultSetError(exc, errorMessageOnFailure);
            }
            catch (DeveloperException exc)
            {
                this.LogBrokerDefaultSetError(exc, errorMessageOnFailure);
            }
        }

        /// <summary>
        /// Logs the specified exception and error message when attempting
        /// to set a broker default fails.
        /// </summary>
        /// <param name="exc">
        /// The <see cref="Exception"/> that was thrown.
        /// </param>
        /// <param name="errorMessageOnFailure">
        /// The <see cref="string"/> error message to log.
        /// </param>
        private void LogBrokerDefaultSetError(Exception exc, string errorMessageOnFailure)
        {
            this.errorLog.Append(errorMessageOnFailure);
            Tools.LogError($"Failed to set broker default: {errorMessageOnFailure}", exc);
        }

        /// <summary>
        /// Provides a simple wrapper for default groups assigned to
        /// a broker on creation.
        /// </summary>
        private struct DefaultBrokerGroup
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DefaultBrokerGroup"/>
            /// struct.
            /// </summary>
            /// <param name="name">
            /// The <see cref="string"/> name of the default broker group.
            /// </param>
            /// <param name="groupType">
            /// The <see cref="GroupType"/> of the default broker group.
            /// </param>
            /// <param name="description">
            /// The <see cref="string"/> description of the default broker group.
            /// </param>
            public DefaultBrokerGroup(string name, GroupType groupType, string description) : this()
            {
                this.Name = name;
                this.GroupType = groupType;
                this.Description = description;
            }

            /// <summary>
            /// Gets the value for the name of the default group.
            /// </summary>
            /// <value>
            /// The <see cref="string"/> name of the default group.
            /// </value>
            public string Name { get; }

            /// <summary>
            /// Gets the value for the type of the default group.
            /// </summary>
            /// <value>
            /// The <see cref="GroupType"/> of the default group.
            /// </value>
            public GroupType GroupType { get; }

            /// <summary>
            /// Gets the value for the description of the default group.
            /// </summary>
            /// <value>
            /// The <see cref="string"/> description of the default group.
            /// </value>
            public string Description { get; }
        }
    }
}
