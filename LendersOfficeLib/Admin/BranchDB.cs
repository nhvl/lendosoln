using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Collections.Generic;
using LendersOffice.Security;
using System.Web;

namespace LendersOffice.Admin
{
	public class BranchDB
	{
        #region Private member variables.
        private Guid m_branchID;
        private Guid m_brokerID;
        private BrokerDB m_broker;
        private string m_name;
		private string m_branchCode;
		private string m_division;
        private CommonLib.Address m_address = new CommonLib.Address();
        private string m_phone;
        private string m_fax;
        private bool m_isNew;
		private Guid m_branchLpePriceGroupIdDefault = Guid.Empty;
        private bool m_IsAllowLOEditLoanUntilUnderwriting;

        private bool m_bIsUseBranchInfoForLoans = false;
        private string m_sLosIdentifier = string.Empty;
        private string m_sLicenseXmlContent = string.Empty;
        private string m_origLicenseXmlContent = null;

        private bool m_bDisplayNmModified;
        private bool m_isFhaLenderIDModified;
        private string m_sDisplayNm;
        private bool m_IsBranchTPO = false;
        private E_BranchChannelT m_branchChannelT;

        private Guid? m_ConsumerPortalId;

        private string m_fhaLenderID = string.Empty;
        private bool m_PopulateFhaAddendumLines = false;

        private string m_LegalEntityIdentifier = string.Empty;
        private bool m_isLegalEntityIdentifierModified;

        #endregion
        #region Public getter/setter
        public bool IsDirty;
        public bool IsNew 
        {
            get 
            {
                return m_isNew;
            }
        }
        public Guid BranchID 
        {
            get 
            {
                return m_branchID;
            }
        }
        public Guid BrokerID 
        {
            get 
            {
                return m_brokerID;
            }
            set 
            {
                m_brokerID = value;
            }
        }

        public BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = BrokerDB.RetrieveById(m_brokerID);
                }

                return m_broker;
            }
        }

        public BranchStatus Status { get; set; }


        public string Name 
        {
            get 
            {
                return m_name;
            }
            set 
            {
                m_name = value;
            }
        }
		public string BranchCode
		{
			get
			{
				return m_branchCode;
			}
			set
			{
				m_branchCode = value;
			}
		}
		public string Division
		{
			get
			{
				return m_division;
			}
			set
			{
				m_division = value;
			}
		}
        public CommonLib.Address Address 
        {
            get 
            {
                return m_address;
            }
            set 
            {
                m_address = value;
            }
        }
        public string Phone 
        {
            get 
            {
                return m_phone;
            }
            set 
            {
                m_phone = value;
            }
        }
        public string Fax
        {
            get 
            {
                return m_fax;
            }
            set 
            {
                m_fax = value;
            }
        }
		public Guid BranchLpePriceGroupIdDefault
		{
			get
			{
				return m_branchLpePriceGroupIdDefault;
			}
			set
			{
				m_branchLpePriceGroupIdDefault = value;
			}
		}        
        public bool IsDisplayNmModified
        {
            get
            {
                return m_bDisplayNmModified;
            }
            set
            {
                m_bDisplayNmModified = value;
            }
        }
        public bool IsFhaLenderIDModified
        {
            get
            {
                return m_isFhaLenderIDModified;
            }
            set
            {
                m_isFhaLenderIDModified = value;
            }
        }
        public bool IsBranchTPO
        {
            get { return m_IsBranchTPO; }
            set { m_IsBranchTPO = value; }
        }
        public string DisplayNm
        {
            get 
            {
                if (IsDisplayNmModified)
                {
                    return m_sDisplayNm;
                }
                else
                {
                    //If the display name is not modified, return the broker name. OPM 62543
                    return Broker.Name;
                }
            }
            set 
            {
                m_sDisplayNm = value;
            }
        }

        public bool IsAllowLOEditLoanUntilUnderwriting
		{
            get { return m_IsAllowLOEditLoanUntilUnderwriting; }
            set { m_IsAllowLOEditLoanUntilUnderwriting = value; }
		}

        public bool IsUseBranchInfoForLoans
        {
            get
            {
                return m_bIsUseBranchInfoForLoans;
            }
            set
            {
                m_bIsUseBranchInfoForLoans = value;
            }
        }
        public string LosIdentifier
        {
            get
            {
                return m_sLosIdentifier;
            }
            set
            {
                m_sLosIdentifier = value;
            }
        }
        public string LicenseXmlContent
        {
            get
            {
                return m_sLicenseXmlContent;
            }
            set
            {
                m_sLicenseXmlContent = value;
            }
        }
        public E_BranchChannelT BranchChannelT
        {
            get
            {
                return m_branchChannelT;
            }
            set
            {
                m_branchChannelT = value;
            }
        }
        public Guid? ConsumerPortalId
        {
            get { return m_ConsumerPortalId; }
            set { m_ConsumerPortalId = value; }
        }
        public string FhaLenderID
        {
            get
            {
                if (IsFhaLenderIDModified)
                {
                    return m_fhaLenderID;
                }
                else
                {
                    return Broker.FhaLenderId;
                }
            }
            set { m_fhaLenderID = value; }
        }

        public bool PopulateFhaAddendumLines
        {
            get { return m_PopulateFhaAddendumLines; }
            set { m_PopulateFhaAddendumLines = value; }
        }

        public string CustomPricingPolicyField1 { get; set; }
        public string CustomPricingPolicyField2 { get; set; }
        public string CustomPricingPolicyField3 { get; set; }
        public string CustomPricingPolicyField4 { get; set; }
        public string CustomPricingPolicyField5 { get; set; }

        /// <summary>
        /// A broker-level branch id. Should behave similarly to a broker-level identity.
        /// </summary>
        public int BranchIdNumber { get; private set; }

        public Guid? RetailTpoLandingPageId { get; set; }
        #endregion

        public static BranchDB RetrieveById(Guid brokerId, Guid branchId)
        {

            HttpContext context = HttpContext.Current;
            string key = "BranchDB_" + brokerId + "_" + branchId;
            BranchDB branch = null;

            if (null != context)
            {
                branch = context.Items[key] as BranchDB;
            }
            if (branch == null)
            {
                branch = new BranchDB(branchId, brokerId);
                branch.Retrieve();
                if (null != context)
                {
                    context.Items[key] = branch;
                }
            }

            return branch;

        }

        public bool IsLegalEntityIdentifierModified
        {
            get
            {
                return m_isLegalEntityIdentifierModified;
            }
            set
            {
                m_isLegalEntityIdentifierModified = value;
            }
        }

        public string LegalEntityIdentifier
        {
            get
            {
                if (IsLegalEntityIdentifierModified)
                {
                    return m_LegalEntityIdentifier;
                }
                else
                {
                    return Broker.LegalEntityIdentifier;
                }
            }
            set { m_LegalEntityIdentifier = value; }
        }

        public BranchDB(Guid branchID, Guid brokerID) 
        {

            m_isNew = false;
            m_branchID = branchID;

            // Yes brokerID is a redudant here, however we need 
            // an extra security feature when retrieve branch information.
            m_brokerID = brokerID;
        }

		public BranchDB()
		{
            m_isNew = true;
            this.Status = BranchStatus.Active;

            this.CustomPricingPolicyField1 = string.Empty;
            this.CustomPricingPolicyField2 = string.Empty;
            this.CustomPricingPolicyField3 = string.Empty;
            this.CustomPricingPolicyField4 = string.Empty;
            this.CustomPricingPolicyField5 = string.Empty;
		}

		private IDataReader m_fieldSet
		{
			set
			{
                try
                {
                    m_brokerID = (Guid)value["BrokerID"];
                }
                catch (IndexOutOfRangeException)
                {
                    // SK this is ok if setting from broker list of branches.
                }

				m_name     = ( String ) value[ "Name"     ];

				try
				{
					m_branchLpePriceGroupIdDefault = ( Guid ) value[ "BranchLpePriceGroupIdDefault" ];
				}
				catch
				{
					m_branchLpePriceGroupIdDefault = Guid.Empty;
				}
				m_branchCode = ( String ) value[ "BranchCode" ];
                m_division = (String)value["Division"];

				m_address = new CommonLib.Address();

				m_address.StreetAddress = ( String ) value[ "Address" ];
				m_address.City          = ( String ) value[ "City"    ];
				m_address.State         = ( String ) value[ "State"   ];
				m_address.Zipcode       = ( String ) value[ "Zipcode" ];

				m_phone = ( String ) value[ "Phone" ];
				m_fax   = ( String ) value[ "Fax"   ];				
                m_IsAllowLOEditLoanUntilUnderwriting = (bool)value["IsAllowLOEditLoanUntilUnderwriting"];
                
                if (m_branchID == null || m_branchID == Guid.Empty) // Adding for H4H project
                {
                    m_branchID = (Guid)value["BranchId"];
                }

                m_bIsUseBranchInfoForLoans = (bool)value["UseBranchInfoForLoans"];
                m_sLosIdentifier = (String)value["NmlsIdentifier"];
                m_sLicenseXmlContent = (String)value["LicenseXmlContent"];
                m_origLicenseXmlContent = (string)value["LicenseXmlContent"];

                try
                {
                    IsDisplayNmModified = (bool)value["DisplayNmLckd"]; 
                }
                catch (IndexOutOfRangeException)
                {
                    IsDisplayNmModified = (bool)value["DisplayNmModified"]; // SK this is ok if setting from broker list of branches.
                }
                IsFhaLenderIDModified = (bool)value["IsFhaLenderIdModified"];
                DisplayNm = (String)value["DisplayNm"];
                IsBranchTPO = (bool)value["IsBranchTPO"];
                BranchChannelT = (E_BranchChannelT)value["BranchChannelT"];

                if (value["ConsumerPortalId"] != DBNull.Value)
                {
                    m_ConsumerPortalId = (Guid)value["ConsumerPortalId"];
                }
                m_fhaLenderID = (string)value["FhaLenderId"];
                m_PopulateFhaAddendumLines = (bool)value["PopulateFhaAddendumLines"];
                this.CustomPricingPolicyField1 = (string)value["CustomPricingPolicyField1"];
                this.CustomPricingPolicyField2 = (string)value["CustomPricingPolicyField2"];
                this.CustomPricingPolicyField3 = (string)value["CustomPricingPolicyField3"];
                this.CustomPricingPolicyField4 = (string)value["CustomPricingPolicyField4"];
                this.CustomPricingPolicyField5 = (string)value["CustomPricingPolicyField5"];
                this.BranchIdNumber = (int)value["BranchIdNumber"];

                IsLegalEntityIdentifierModified = (bool)value["IsLegalEntityIdentifierModified"];
                m_LegalEntityIdentifier = (string)value["LegalEntityIdentifier"];

                this.Status = (BranchStatus)value["Status"];
            }
		}
       
        public bool Retrieve(string branchCode, Guid brokerId)
        {
            bool isExisted = false;
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId),
                                                new SqlParameter("@BranchCode", branchCode)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveBranchByBranchCode", parameters))
                {
                    if (reader.Read() == true)
                    {
                        m_fieldSet = reader;
                        isExisted = true;
                    }
                }
            }
            catch (SqlException sq)
            {
                string msg = "Cannot retrieve branch " + branchCode + ": " + sq.ToString();
                throw new CBaseException(msg, msg);
            }

            return isExisted;
        }

        // Adding for H4H project
        public bool Retrieve(string branchCode)
        {
            return Retrieve(branchCode, m_brokerID);
        }

		public void Retrieve( CStoredProcedureExec spExec )
		{
			if (m_isNew) 
			{
				return;
			}

            using (DbDataReader reader = spExec.ExecuteReader("RetrieveBranchByID", new SqlParameter("@BranchID", m_branchID), new SqlParameter("@BrokerID", m_brokerID)))
            {
                if (reader.Read() == true)
                {
                    m_fieldSet = reader;
                }
                else
                {
                    throw new ArgumentException("Branch not found.", m_branchID.ToString());
                }
            }
		}
		
		public bool Retrieve()
        {
			bool isExisted = false;
			
			if (m_isNew) 
            {
                return false;
            }
            
			try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", this.BrokerID),
                                                new SqlParameter("@BranchId", this.BranchID)
                                            };
				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveBranchByID" , parameters ) )
				{
					if( reader.Read() == true )
					{
						m_fieldSet = reader;

						isExisted = true;
					}
				}
            } 
            catch (SqlException) 
            {
            }

            return isExisted;
        }

		public void Save( CStoredProcedureExec spExec )
		{
			string procedureName = null;
			ArrayList parameters = new ArrayList();
			SqlParameter branchIDParam = new SqlParameter("@BranchID", SqlDbType.UniqueIdentifier);

			branchIDParam.Direction = ParameterDirection.Output;

			if (m_isNew) 
			{
				procedureName = "CreateBranch";
				parameters.Add(branchIDParam);

                var branchIdNumber = BrokerDB.GetAndUpdateBranchIdNumberCounter(this.BrokerID);
                parameters.Add(new SqlParameter("@BranchIdNumber", branchIdNumber));
			} 
			else 
			{
				procedureName = "UpdateBranch";
                
				parameters.Add(new SqlParameter("@BranchID", m_branchID));
			}
			parameters.Add(new SqlParameter("@BrokerID", this.BrokerID));
			parameters.Add(new SqlParameter("@Name", m_name));
			parameters.Add(new SqlParameter("@BranchCode", m_branchCode));
            parameters.Add(new SqlParameter("@Division", m_division));
			parameters.Add(new SqlParameter("@Address", m_address.StreetAddress));
			parameters.Add(new SqlParameter("@City", m_address.City));
			parameters.Add(new SqlParameter("@State", m_address.State));
			parameters.Add(new SqlParameter("@Zipcode", m_address.Zipcode));
			parameters.Add(new SqlParameter("@Phone", m_phone));
			parameters.Add(new SqlParameter("@Fax", m_fax));
			parameters.Add(new SqlParameter("@BranchLpePriceGroupIdDefault", m_branchLpePriceGroupIdDefault));
			parameters.Add(new SqlParameter("@DisplayBranchNameInLoanNotificationEmails", false)); //redundant field
            parameters.Add(new SqlParameter("@IsAllowLOEditLoanUntilUnderwriting", m_IsAllowLOEditLoanUntilUnderwriting));
            parameters.Add(new SqlParameter("@IsUseBranchInfoForLoans", m_bIsUseBranchInfoForLoans));
            parameters.Add(new SqlParameter("@NmlsIdentifier", m_sLosIdentifier));
            parameters.Add(new SqlParameter("@LicenseXmlContent", m_sLicenseXmlContent));
            parameters.Add(new SqlParameter("@DisplayNm", DisplayNm));
            parameters.Add(new SqlParameter("@DisplayNmLckd", IsDisplayNmModified));
            parameters.Add(new SqlParameter("@IsBranchTPO", IsBranchTPO));
            parameters.Add(new SqlParameter("@BranchChannelT", BranchChannelT));
            parameters.Add(new SqlParameter("@ConsumerPortalId", ConsumerPortalId));
            parameters.Add(new SqlParameter("@IsFhaLenderIdModified", IsFhaLenderIDModified));
            parameters.Add(new SqlParameter("@PopulateFhaAddendumLines", PopulateFhaAddendumLines));

            if (IsFhaLenderIDModified)
            {
                parameters.Add(new SqlParameter("@FhaLenderId", FhaLenderID));
            }
            parameters.Add(new SqlParameter("@CustomPricingPolicyField1", this.CustomPricingPolicyField1));
            parameters.Add(new SqlParameter("@CustomPricingPolicyField2", this.CustomPricingPolicyField2));
            parameters.Add(new SqlParameter("@CustomPricingPolicyField3", this.CustomPricingPolicyField3));
            parameters.Add(new SqlParameter("@CustomPricingPolicyField4", this.CustomPricingPolicyField4));
            parameters.Add(new SqlParameter("@CustomPricingPolicyField5", this.CustomPricingPolicyField5));

            parameters.Add(new SqlParameter("@IsLegalEntityIdentifierModified", IsLegalEntityIdentifierModified));
            parameters.Add(new SqlParameter("@LegalEntityIdentifier", LegalEntityIdentifier));

            parameters.Add(new SqlParameter("@Status", this.Status));

            if (this.RetailTpoLandingPageId.HasValue)
            {
                parameters.Add(new SqlParameter("@RetailTpoLandingPageId", this.RetailTpoLandingPageId.Value));
            }

            int count = spExec.ExecuteNonQuery(procedureName, 0, parameters);

            if (count > 0)
            {
                if (m_isNew)
                {
                    m_branchID = (Guid)branchIDParam.Value;
                    m_isNew = false;
                }
                LogIfLicenseXmlChanged();
            }

		}

        public void Save()
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(this.BrokerID))
            {
                spExec.BeginTransactionForWrite();

                try
                {
                    Save(spExec);

                    spExec.CommitTransaction();
                }
                catch
                {
                    spExec.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Only to be called on successful save.
        /// opm 182283.  Ideally we would log it somewhere that is more permanent, but since this has happened this week it should be ok
        /// in pblogs for now.
        /// </summary>
        private void LogIfLicenseXmlChanged()
        {
            try
            {
                if (m_origLicenseXmlContent != m_sLicenseXmlContent)
                {
                    Tools.LogInfoWithUserInfo("Branch_LicenseXmlContent_Changed", "Changed from '" + m_origLicenseXmlContent +
                        "' to '" + m_sLicenseXmlContent + " to branchID: " + m_branchID 
                        + " named: " + m_name);

                    m_origLicenseXmlContent = m_sLicenseXmlContent;  // since it was successfully saved to the DB.
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Branch_LicenseXmlContent_Changed error logging", e);
            }
        }

        public void SaveDocumentVendorCredentials(CStoredProcedureExec spExec, List<DocumentVendorLoginCredentials> credentials )
        {
            spExec.ExecuteNonQuery("DOCUMENT_VENDOR_ClearBranchCredentials", 3, new SqlParameter("@BranchId", m_branchID));
            foreach (DocumentVendorLoginCredentials cred in credentials)
            {
                spExec.ExecuteNonQuery("DOCUMENT_VENDOR_SaveBranchCredentials", 2,
                    new SqlParameter("@BranchId", m_branchID),
                    new SqlParameter("@VendorId", cred.VendorId),
                    new SqlParameter("@Login", cred.Login),
                    new SqlParameter("@Password", cred.Password),
                    new SqlParameter("@AccountId", cred.AccountId)
                    );
            }

        }
        public void SaveSingleDocumentVendorCredentials(DocumentVendorLoginCredentials cred)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BranchId", m_branchID),
                new SqlParameter("@VendorId", cred.VendorId),
                new SqlParameter("@Login", cred.Login),
                new SqlParameter("@Password", cred.Password),
                new SqlParameter("@AccountId", cred.AccountId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerID, "DOCUMENT_VENDOR_SaveBranchCredentials", 2, parameters);
        }

		/// <summary>
        /// Delete branch from database. Note, this will only work correctly, if this branch
        /// does not contains any employee.
        /// </summary>
        /// <returns></returns>
        public void Delete() 
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BranchID", this.BranchID) 
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerID, "DeleteBranch", 0, parameters);
        }

        public string GetCustomPricingPolicyField(int id)
        {
            switch (id)
            {
                case 1: return this.CustomPricingPolicyField1;
                case 2: return this.CustomPricingPolicyField2;
                case 3: return this.CustomPricingPolicyField3;
                case 4: return this.CustomPricingPolicyField4;
                case 5: return this.CustomPricingPolicyField5;
                default: throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "Trying to access unimplemented custom pricing policy field.");
            }
        }

        public static DbDataReader GetBranches(Guid brokerID) 
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerID)
                                        };
            return StoredProcedureHelper.ExecuteReader(brokerID, "ListBranchByBrokerID", parameters);
        }

        public static IEnumerable<BranchDB> GetBranchObjects(Guid brokerID)
        {
            var branches = new LinkedList<BranchDB>();
            using (var reader = GetBranches(brokerID))
            {
                while (reader.Read())
                {
                    var branch = new BranchDB();
                    branch.m_isNew = false;
                    branch.m_fieldSet = reader;
                    /* 
                     * ListBranchByBrokerID has Address as the entire address.
                     * m_fieldSet thinks its only the Street Address
                     * This is the safest place to change it without affecting
                     * all the other things that think one way or the other.
                     */
                    branch.m_address.StreetAddress = reader["Street"].ToString();  
                    branch.m_brokerID = brokerID;
                    branches.AddLast(branch);
                }
            }
            return branches;
        }
	}

    [Serializable]
    public class DocumentVendorLoginCredentials
    {
        public DocumentVendorLoginCredentials(Guid VendorId, string Login, string Password, string AccountId)
        {
            this.VendorId = VendorId;
            this.Login = Login;
            this.Password = Password;
            this.AccountId = AccountId;
        }

        public Guid VendorId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string AccountId { get; set; }
    }
}
