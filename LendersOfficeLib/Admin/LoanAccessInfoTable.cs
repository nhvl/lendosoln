﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace LendersOffice.Admin
{

    /// <summary>
    /// Queue up a table of loan access info details -- great for pulling
    /// the individual objects out of a fat dataset.
    /// </summary>

    public class LoanAccessInfoTable
    {
        /// <summary>
        /// Queue up a table of loan access info details -- great for
        /// pulling the individual objects out of a fat dataset.
        /// </summary>

        private Dictionary<Guid, LoanAccessInfo> m_Table = new Dictionary<Guid, LoanAccessInfo>();

        public LoanAccessInfo this[Guid loanId]
        {
            get
            {
                return m_Table[loanId];
            }
        }

        public int Count
        {
            get { return m_Table.Count; }
        }

        /// <summary>
        /// Load up the entire table, as if reading records from a data
        /// reader.  We insert into the existing state.  Collisions are
        /// ignored.
        /// </summary>

        public void Load(DataTable dT)
        {
            // Load up the entire table, as if reading records from
            // a data reader.

            foreach (DataRow dR in dT.Rows)
            {
                LoanAccessInfo lAi = new LoanAccessInfo();

                lAi.Initialize((Guid)dR["sLId"], dR);
                m_Table.Add(lAi.LoanId, lAi);
            }
        }

        /// <summary>
        /// Load up the entire dataset, as if reading records from a data
        /// reader.  We insert into the existing state.  Collisions are
        /// ignored.
        /// </summary>

        public void Load(DataSet dS)
        {
            // Load up the entire dataset, as if reading records from
            // a data reader.

            foreach (DataTable dT in dS.Tables)
            {
                Load(dT);
            }
        }

    }
}
