﻿using System;
using System.Collections;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// We have several places where permissions are editable, and
    /// each one gets updated when we add a permission.  It is now
    /// time we centralize this definition.
    /// </summary>

    public abstract class BrokerUserPermissionTable : ArrayList
    {
        /// <summary>
        /// Permission descriptor for binding to manipulation ui.
        /// </summary>

        #region ( Permission description )

        public class Desc
        {
            private string m_Label;
            private Permission m_Code;

            #region ( Permission table desc properties )
            public String Label
            {
                get { return m_Label; }
            }

            public String Value
            {
                get { return m_Code.ToString("D"); }
            }

            public Permission Code
            {
                get { return m_Code; }
            }

            #endregion

            /// <summary>
            /// Return code for default binding operations.
            /// </summary>

            public override string ToString()
            {
                // Return code for default binding operations.

                return m_Code.ToString("D");
            }

            #region ( Constructors )

            /// <summary>
            /// Construct default.
            /// </summary>

            public Desc(Permission pValue, String sLabel)
            {
                // Initialize members.

                m_Label = sLabel;
                m_Code = pValue;
            }

            #endregion

        }

        #endregion

        /// <summary>
        /// Override this test to specialize types of permission
        /// tables for ui display.
        /// </summary>

        protected abstract Boolean IsIncluded(Desc pDesc);

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerUserPermissionTable()
        {
            // Initialize the table.  Add to this set with
            // each new permission.

            Desc[] dTable = new Desc[]
				{
					  new Desc( Permission.CanModifyAdministrativeItem            , "Allow modifying administrative item"                       )
					, new Desc( Permission.BrokerLevelAccess                      , "Corporate - originated from any branch"                    )
					, new Desc( Permission.BranchLevelAccess                      , "Branch - originated within own branch"                     )
                    , new Desc( Permission.TeamLevelAccess                        , "Team - only if team explicitly assigned"                  )
					, new Desc( Permission.IndividualLevelAccess                  , "Individual - only if explicitly assigned"                  )
					, new Desc( Permission.CanModifyLoanPrograms                  , "Allow modifying loan programs"                             )
					, new Desc( Permission.AllowLoanAssignmentsToAnyBranch        , "Allow assigning loans to agents of any branch"             )
					, new Desc( Permission.CanAccessClosedLoans                   , "Allow accessing closed loans"                              )
					, new Desc( Permission.CanModifyLoanName                      , "Allow modifying loan numbers"                              )
					, new Desc( Permission.CanDeleteLoan                          , "Allow deleting loans"                                      )
					, new Desc( Permission.CanViewLoanInEditor                    , "Allow viewing loans in editor"                             )
					, new Desc( Permission.CanExportLoan                          , "Allow exporting loans"                                     )
					, new Desc( Permission.CanWriteNonAssignedLoan                , "Allow editing non-assigned loans"                          )
					, new Desc( Permission.CanCreateLoanTemplate                  , "Allow creating loan templates"                             )
					, new Desc( Permission.AllowAccessToTemplatesOfAnyBranch      , "Allow accessing templates of any branch"                   )
					, new Desc( Permission.CanRunCustomReports                    , "Allow accessing custom reports"                            )
					, new Desc( Permission.CanEditOthersCustomReports             , "Allow editing others' custom reports"                      )
					, new Desc( Permission.CanPublishCustomReports                , "Allow publishing custom reports"				            )
					, new Desc( Permission.AllowReadingFromRolodex                , "Allow reading from contacts"                               ) // 02/13/06 dl - opm 4053
					, new Desc( Permission.AllowWritingToRolodex                  , "Allow writing to contacts"                                 ) // 02/13/06 dl - opm 4053
                    , new Desc( Permission.AllowViewingPrintGroups                , "Allow viewing print groups"                                ) // OPM 180788 sk 11/6/2014
					, new Desc( Permission.CanEditPrintGroups                     , "Allow editing print groups"                                )
					, new Desc( Permission.CanApplyForIneligibleLoanPrograms      , "Allow applying for ineligible loan programs"               )
					, new Desc( Permission.CanRunPricingEngineWithoutCreditReport , "Allow running PriceMyLoan w/o credit report"               )
					, new Desc( Permission.CanSubmitWithoutCreditReport           , "Allow to register/lock w/o credit report"	                )
                    , new Desc( Permission.AllowTpoPortalConfiguring              , "Allow Originator Portal Configuration"                            ) // OPM 150384
					, new Desc( Permission.CanAdministrateExternalUsers			  , "Allow administrating Originating Company users"                            ) // OPM 9721, 150384
					, new Desc( Permission.CanAccessCCTemplates					  , "Allow editing closing cost templates"			            ) // 04/06/07 db - OPM 3453
					, new Desc( Permission.CanCreateCustomForms					  , "Allow creating custom forms"					            ) // 05/31/07 db - OPM 2259
					, new Desc( Permission.CanEditOthersCustomForms				  , "Allow editing others' custom forms"			            ) // 05/31/07 db - OPM 2259
					, new Desc( Permission.CanViewHiddenInformation				  , "Allow viewing hidden information"				            ) // 10/03/07 db - OPM 18269
					, new Desc( Permission.CanEditDataTracLPNames				  , "Allow editing DataTrac Loan Program Names"		            ) // av 09 18 08 opm 24638
					, new Desc( Permission.CanViewEDocs				              , "Allow viewing EDocs"	                                    ) // db - OPM 44494
                    , new Desc( Permission.CanEditEDocs				              , "Allow editing EDocs"		                                ) // db - OPM 44494
                    , new Desc( Permission.AllowCreatingNewLoanFiles              , "Allow creating new loan files") // 6/26/2014 gf - opm 179075
                    , new Desc( Permission.CanDuplicateLoans					  , "Allow duplicating loans"		)				              // 04/06/07 db - OPM 3078
					//, new Desc( Permission.CanAccessCommissions				  , "Allow accessing Commissions"					              // 04/09/07 db - OPM 6832
                    , new Desc( Permission.AllowLockDeskRead                      , "Allow viewing pages in the Lock Desk folder"               )
                    , new Desc( Permission.AllowLockDeskWrite                     , "Allow editing pages in the Lock Desk folder"               )
                    , new Desc( Permission.AllowCloserRead                        , "Allow viewing pages in the Funding folder"                  )
                    , new Desc( Permission.AllowCloserWrite                       , "Allow editing pages in the Funding folder"                  )
                    , new Desc( Permission.AllowAccountantRead                    , "Allow viewing pages in the Finance/Servicing/Shipping folders"                 )
                    , new Desc( Permission.AllowAccountantWrite                   , "Allow editing pages in the Finance/Servicing/Shipping folders"                 )
                    , new Desc( Permission.AllowBypassAlwaysUseBestPrice          , "Allow bypass always use 'Best Prices per Program' in PML"  )
                    , new Desc( Permission.CanAccessLendingStaffNotes             , "Allow access to lending staff notes"                       )
                    , new Desc( Permission.CanAccessTotalScorecard                , "Allow access to Total Scorecard Interface"                 ) // 8/9/10 db - OPM 46742
                    , new Desc( Permission.CanViewEDocsInternalNotes              , "Allow viewing EDocs internal notes")
                    , new Desc( Permission.CanEditEDocsInternalNotes              , "Allow editing EDocs internal notes")
                    , new Desc( Permission.AllowCapitalMarketsAccess              , "Allow Capital Markets access")
                    , new Desc( Permission.AllowScreeningEDocs                    , "Allow screening EDocs")
                    , new Desc( Permission.AllowApprovingRejectingEDocs           , "Allow protecting/rejecting EDocs")
                    , new Desc( Permission.AllowEditingApprovedEDocs              , "Allow editing protected EDocs")
                    , new Desc( Permission.AllowCreatingDocumentSigningEnvelopes  , "Allow generating DocuSign envelopes")
                    , new Desc( Permission.AllowDisclosureDocumentGeneration      , "Allow generating disclosures" )  
                    , new Desc( Permission.AllowDocMagicDocumentGeneration_OBSOLETE,"Allow generating documents" )                 
                    , new Desc( Permission.AllowOrderingTitleServices             , "Allow ordering title services" )
                    , new Desc( Permission.AllowUnderwritingAccess                , "Allow viewing and editing pages in the Underwriting folder" )
                    , new Desc( Permission.AllowQualityControlAccess              , "Allow viewing and editing pages in the Quality Control folder" )
                    , new Desc( Permission.AllowOrderingPMIPolicies               , "Allow ordering private mortgage insurance" )
                    , new Desc( Permission.AllowOrderingGlobalDMSAppraisals       , "Allow ordering appraisals")
                    , new Desc( Permission.AllowAppraisalDelivery                 , "Allow delivery of appraisals")
                    , new Desc( Permission.AllowViewingInvestorInformation        , "Allow viewing Investor information")
                    , new Desc( Permission.AllowEditingInvestorInformation        , "Allow editing Investor information")
                    , new Desc( Permission.ExportDocRequestsToTestingPath         , "Send all document requests to Test Environment")
                    , new Desc( Permission.ResponsibleForInitialDiscRetail        , "Responsible for initial disclosures (retail)")
                    , new Desc( Permission.ResponsibleForInitialDiscWholesale     , "Responsible for initial disclosures (wholesale)")
                    , new Desc( Permission.ResponsibleForRedisclosures            , "Responsible for redisclosures")
                    , new Desc( Permission.AllowEditingLoanProgramName            , "Allow manually entering loan program names" )
                    , new Desc( Permission.AllowManagingContactList               , "Allow managing contact list")
                    , new Desc( Permission.AllowOrder4506T                        , "Allow ordering 4506-T")
                    , new Desc( Permission.AllowHideEDocsFromPML                  , "Allow hiding documents from PriceMyLoan users")                    
                    , new Desc( Permission.AllowEnablingCustomFeeDescriptions     , "Allow toggling 'Protect Compliance Settings' for LE/GFE fees")
                    , new Desc( Permission.CanEditLoanTemplates                   , "Allow editing loan templates" )
                    , new Desc( Permission.AllowManuallyArchivingGFE              , "Allow manually archiving" )
                    , new Desc( Permission.AllowEditingOriginatingCompanyTiers	  , "Allow editing Originating Company Tiers" )
                    , new Desc( Permission.AllowEnableGfePaidToManualDesc         , "Allow enabling GFE 'Paid To' descriptions to be set manually (Legacy)" )
                    , new Desc( Permission.AllowViewingAndEditingGeneralSettings   , "Allow viewing and editing general settings" )
                    , new Desc( Permission.AllowViewingAndEditingGlobalIPSettings   , "Allow viewing and editing global IP settings" )
                    , new Desc( Permission.AllowViewingAndEditingTeams   , "Allow viewing and editing teams" ) 
                    , new Desc( Permission.AllowViewingAndEditingBranches   , "Allow viewing and editing branches" )
                    , new Desc( Permission.AllowViewingAndEditingConsumerPortalConfigs   , "Allow viewing and editing consumer portal configuration" )
                    , new Desc( Permission.AllowViewingAndEditingEventNotifications   , "Allow viewing and editing event notification" )
                    , new Desc( Permission.AllowViewingAndEditingCustomFieldChoices   , "Allow viewing and editing custom field choices" )
                    , new Desc( Permission.AllowViewingAndEditingConditionChoices   , "Allow viewing and editing condition choices" )
                    , new Desc( Permission.AllowViewingAndEditingLeadSources   , "Allow viewing and editing lead sources" )
                    , new Desc( Permission.AllowViewingAndEditingEdocsConfiguration   , "Allow viewing and editing EDocs configuration" )
                    , new Desc( Permission.AllowViewingAndEditingArmIndexEntries   , "Allow viewing and editing ARM index entries" )
                    , new Desc( Permission.AllowViewingAndEditingManualLoanPrograms   , "Allow viewing and editing manual loan programs" )
                    , new Desc( Permission.AllowViewingAndEditingAdjustmentsAndOtherCreditsSetup , "Allow viewing and editing Adjustments and Other Credits Setup")
                    , new Desc( Permission.AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox, "Allow toggling the Restrict adjustments and other credits checkbox" )
                    , new Desc( Permission.AllowViewingAndEditingFeeTypeSetup   , "Allow viewing and editing Fee Type Setup" )
                    , new Desc( Permission.AllowViewingAndEditingGFEFeeService   , "Allow viewing and editing GFE Fee Service" )
                    , new Desc( Permission.AllowViewingAndEditingDisablePricing   , "Allow viewing and editing disable pricing" )
                    , new Desc( Permission.AllowViewingAndEditingPriceGroups   , "Allow viewing and editing price groups" )
                    , new Desc( Permission.AllowViewingAndEditingLockDeskQuestions   , "Allow viewing and editing lock desk questions" )
                    , new Desc( Permission.AllowLoadingDeletedLoans, "Allow loading deleted loans" )
				    , new Desc( Permission.AllowManageNewFeatures, "Allow managing new features" )
                    , new Desc( Permission.AllowManagingFailedDocs, "Allow managing documents with missing barcodes")
                    , new Desc( Permission.AllowCreatingTestLoans, "Allow creating test loans")
                    , new Desc( Permission.AllowOverridingContactLicenses, "Allow overriding agent and company licenses on contact records")
                    , new Desc( Permission.AllowViewingNewLoanEditorUI, "Allow using new UI")
                    , new Desc( Permission.AllowAccessingOCRReviewPortal, "Allow accessing OCR document review portal")
                    , new Desc( Permission.AllowCreatingNewLeadFiles, "Allow creating new lead files")
                    , new Desc( Permission.AllowExportingFullSsnViaCustomReports, "Allow exporting full SSN via custom reports")
                    , new Desc( Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage, "Allow accessing the 'Capture Audit' page")
                    , new Desc( Permission.AllowAccessingCorporateAdminDocumentCaptureAuditPage, "Allow accessing the 'Capture Audit' page")
                    , new Desc( Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage, "Allow accessing the 'Capture Review' page")
                    , new Desc( Permission.AllowAccessingCorporateAdminDocumentCaptureReviewPage, "Allow accessing the 'Capture Review' page")
                    , new Desc( Permission.AllowAccessingSecurityEventLogsPage, "Allow accessing the Security Event Log page")
                    , new Desc( Permission.AllowViewingAndEditingCustomNavigation, "Allow viewing and editing Custom Navigation")
                };

            foreach (Desc dItem in dTable)
            {
                if (IsIncluded(dItem) == true)
                {
                    Add(dItem);
                }
            }
        }

        #endregion

		public static BrokerUserPermissionTable GetPermissionTable(ControlDescription cDesc, bool pmlEnabled, bool dataTracEnabled, bool eDocsEnabled, bool totalScorecardEnabled, bool useOCR, bool marketingToolsEnabled)
		{
			switch(cDesc)
			{
				case ControlDescription.Contacts:
					return new AdminPermissionTableContacts();
				case ControlDescription.CustomReports:
					return new AdminPermissionTableCustomReports();
				case ControlDescription.CustomForms:
					return new AdminPermissionTableCustomForms();
				case ControlDescription.ExternalUsers:
					if(!pmlEnabled)
						return new AdminPermissionTableEmpty();
					else
						return new AdminPermissionTableExternalUsers();
				case ControlDescription.Leads:
                    if (marketingToolsEnabled)
                    {
                        return new AdminPermissionTableLeads();
                    }
                    else
                    {
                        return new AdminPermissionTableEmpty();
                    }
                case ControlDescription.Loans:
					return new AdminPermissionTableLoans();
				case ControlDescription.PricingEngine:
					if(!pmlEnabled)
						return new AdminPermissionTableEmpty();
					else
						return new AdminPermissionTablePricingEngine();
				case ControlDescription.PrintGroups:
					return new AdminPermissionTablePrintGroups();
				case ControlDescription.Templates:
					return new AdminPermissionTableTemplates();
				case ControlDescription.CCTemplates:
					return new AdminPermissionTableCCTemplates();
				case ControlDescription.PriceGroups:
				{
					return dataTracEnabled ?   (BrokerUserPermissionTable)(new AdminPermissionTablePriceGroups()) : 
											  (BrokerUserPermissionTable)(new AdminPermissionTableEmpty());
				}
                case ControlDescription.EDocs:
                    return eDocsEnabled ? (BrokerUserPermissionTable)(new AdminPermissionTableEDocs()) : (BrokerUserPermissionTable)(new AdminPermissionTableEmpty());								 
				case ControlDescription.MortgageBanking:
                    return new AdminPermissionTableMortgageBanking();
                case ControlDescription.TotalScorecard:
                    return totalScorecardEnabled? (BrokerUserPermissionTable)(new AdminPermissionTableTotalScorecard()) : (BrokerUserPermissionTable)(new AdminPermissionTableEmpty());
                case ControlDescription.CapitalMarkets:
                    if (System.Threading.Thread.CurrentPrincipal is InternalUserPrincipal)
                    {

                        return new AdminPermissionTableCapitalMarkets();
                    }
                    else
                    {
                        return new AdminPermissionTableEmpty();
                    }
                case ControlDescription.Disclosures:
                    return new AdminPermissionTableDisclosures();
                case ControlDescription.Administration:
                    return new AdminPermissionTableAdministration();
                case ControlDescription.TestLoans:
                    return new AdminPermissionTableTestLoans();
                case ControlDescription.UserExperience:
                    if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return new AdminPermissionTableUserExperience();
                    }
                    else
                    {
                        var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                        if (E_PermissionType.DeferToMoreGranularLevel == broker.ActualNewLoanEditorUIPermissionType)
                        {
                            return new AdminPermissionTableUserExperience();
                        }
                        else
                        {
                            return new AdminPermissionTableEmpty();
                        }
                    }
                case ControlDescription.DocumentReview:
                    if (useOCR)
                    {
                        return new AdminPermissionTableDocumentReview();
                    }
                    else 
                    {
                        return new AdminPermissionTableEmpty();

                    }
                default:
					return null;
			}
		}

        // gf opm 228619 - The employee exporter needed to be able to access the
        // permissions that would be available in the user editor. I pulled the
        // enum and function to get the permission tables out of the page in the
        // app and into the lib to accomplish this. The order referenced in the
        // comment below corresponds to the order on the page.  For the related
        // code see EmployeePermissionsList.ascx.cs.
        // 
        // Note - if adding any ControlDescriptions, they must be added into the correct order
        // (the order that the permission groups are displayed on the screen)
        // THE ORDER DOES REALLY MATTER
        public enum ControlDescription
        {
            Leads,
            Loans,
            Templates,
            CCTemplates,
            CustomReports,
            CustomForms,
            Contacts,
            PriceGroups,
            EDocs,
            Disclosures,
            TotalScorecard,
            PrintGroups,
            ExternalUsers,
            PricingEngine,
            MortgageBanking,
            CapitalMarkets,
            Administration,
            TestLoans,
            UserExperience,
            DocumentReview
        }
    }
}
