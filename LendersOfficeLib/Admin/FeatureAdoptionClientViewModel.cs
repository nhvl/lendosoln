﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class that shows feature information adopted by a specific client.
    /// </summary>
    [DataContract]
    public class FeatureAdoptionClientViewModel
    {
        /// <summary>
        /// List of FeatureResult objects.
        /// </summary>
        [DataMember]
        private List<FeatureResult> featureResults;

        /// <summary>
        /// Name of the broker for whom to display the feature information for.
        /// </summary>
        [DataMember]
        private string clientName;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureAdoptionClientViewModel" /> class.
        /// </summary>
        /// <param name="featureResults">List of FeatureResults.</param>
        /// <param name="clientName">Client name specifying the broker for the FeatureResults.</param>
        public FeatureAdoptionClientViewModel(List<FeatureResult> featureResults, string clientName)
        {
            this.featureResults = featureResults;
            this.clientName = clientName;
        }

        /// <summary>
        /// Returns a FeatureAdoptionClientViewModel, which is used to send results to FeatureAdoptionClientView page.
        /// </summary>
        /// <param name="brokerdb">BrokerDB object.</param>
        /// <returns>FeatureAdoptionClientViewModel object.</returns>
        public static FeatureAdoptionClientViewModel GetFeatureAdoptionClientViewModel(BrokerDB brokerdb)
        {
            List<FeatureResult> featureResultList = FeatureResult.GetAllFeatureResultsForSingleBroker(brokerdb);

            string clientName = brokerdb.CustomerCode + " - " + brokerdb.Name;
            return new FeatureAdoptionClientViewModel(featureResultList, clientName);
        }
    }
}
