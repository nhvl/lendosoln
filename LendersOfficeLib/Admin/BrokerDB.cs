using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using CommonLib;
using ConfigSystem.DataAccess;
using ConfigSystem.Engine;
using DataAccess;
using DocuTech;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using LendersOffice.Conversions.ComplianceEagleIntegration;
using LendersOffice.CustomPmlFields;
using LendersOffice.HttpModule;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Integration.Irs4506T;
using LendersOffice.Integration.MortgageInsurance;
using LendersOffice.Integration.OCR;
using LendersOffice.ObjLib.Conversions.ComplianceEase;
using LendersOffice.ObjLib.DistributeUnderwriting;
using LendersOffice.ObjLib.DU.Seamless;
using LendersOffice.ObjLib.HistoricalPricing;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.ObjLib.PMLNavigation;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.TPO;
using LendersOffice.Security;
using LendersOffice.UI;
using LqbGrammar;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;
using LqbGrammar.Exceptions;
using System.Collections.Concurrent;
using LendersOffice.ObjLib.Conversions.LoanProspector;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Drivers.SqlServerDB;
using static LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration;
using LendersOffice.RatePrice.FileBasedPricing;

namespace LendersOffice.Admin
{
    public enum E_ShowCredcoOptionT 
    {
        DisplayBoth = 0,
        DisplayOnlyDirect = 1,
        DisplayOnlyFNMA = 2
    }

    public enum E_BrokerNamingSchemeT
    {
        Sequential,
        MersNaming
    }

    public enum E_BrokerBillingVersion : int
    {
        NoBilling = 0,
        Classic = 1,
        New = 2,
        PerTransaction = 3
    }

    public enum E_BrokerSsnMaskingOnVerifications
    { 
        DisplayFullSsn = 0,
        DisplayMaskedSsn = 1,
        RemoveSsn = 2
    }

    // 3/27/2014 dd - OPM 174204
    public enum E_BrokerDocumentValidationOptionT
    {
        NoValidation = 0,
        WarningOnValidationMismatch = 1,
        BlockOnValidationMismatch = 2
    }

    public enum E_FeeTypeRequirementT
    {
        Never = 0,
        ForAllNewLoans = 1,
        IfSetInTemplate = 2
    }
    /// <summary>
    /// aka QP 2.0
    /// Disabled = 0, // no user can enable it. <para></para>
    /// PerUser = 1,  // up to admins to set the bit per user. <para></para>
    /// Enabled = 2   // all users at this lender have it enabled. <para></para>
    /// </summary>
    public enum E_Pml2AsQuickPricerMode
    {
        Disabled = 0, // no user can enable it.
        PerUser = 1,  // up to admins to set the bit per user.
        Enabled = 2   // all users at this lender have it enabled.
    }

    /// <summary>
    /// Events that can occur during the life of a loan, at which we want to do
    /// some (usually configurable) thing in response.
    /// </summary>
    public enum E_LoanEvent
    {
        Created = 0,
        Respa6Collected = 1,
        Registered = 2,
        DocumentCheck = 3,
        RateLocked = 4
    }

    public enum LenderRelationshipWithFreddieMac : byte
    {
        /// <summary>
        /// The value is unknown/unspecified/not well defined.
        /// </summary>
        Undefined = 0,
        Seller = 1,
        Correspondent = 2
    }

    public static class BrokerDBExtensions
    {
        public static string AsSimpleStringForLog(this BrokerDB broker)
        {
            return string.Format(
                                "{0} {1}({2})",
                                broker.BrokerID,    // {0}
                                broker.Name,    // {1}
                                broker.CustomerCode); // {2}
        }

        public static Integration.UcdDelivery.UcdLenderService GetUcdConfig(this BrokerDB broker)
        {
            return new Integration.UcdDelivery.UcdLenderService(broker);
        }

        public static CLpeReleaseInfo GetLatestReleaseInfo(this BrokerDB broker)
        {
           return (broker.ActualPricingBrokerId == Guid.Empty) 
                  ? CLpeReleaseInfo.GetLatestReleaseInfo()
                  : CLpeReleaseInfo.GetLatestManualImportInfo();
        }

        public static FileBasedSnapshot RetrieveLatestManualImport(this BrokerDB broker)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty || !ConstSite.EnablePerLenderActualSnapshot)
            {
                return FileBasedSnapshot.RetrieveLatestManualImport();
            }

            return FileBasedSnapshot.RetrieveLatestManualImport(broker.ActualPricingBrokerId, useSubsnapshot: true);                
        }
    }

    public class BrokerDB
    {
        // 08/24/2016 JE - Fileds removed by OPM 247106 but still in DB:
        // BlitzDocIntegrationPassword
        // BlitzDocIntegrationUserName
        // BlitzDocsCompanyId
        // BlitzDocFolderConfigIdsXmlContent
        // IsBlitzDocEnabled

        #region Private member variables.
        private ConcurrentDictionary<string, Lazy<bool>> TempOptionDictionary;
        private readonly bool enableTempOptionRetrievalCaching;
        private readonly bool enableTempOptionRetrievalLogging;
        private string m_sLPQExportUrl;
        private Guid m_brokerID;
        private int m_status;
        private string m_name = null;
        private string m_url = null; // OPM 19976
        private CommonLib.Address m_address = null;
        private string m_phone = null;
        private string m_fax = null;
        private string m_licenseNumber = null;
        private string m_accountManager = null;
        private string m_customerCode = null;
        private decimal m_pricePerSeat = 0;
        private bool m_pricePerSeatSet = false;
        private bool m_isNew;
        private bool m_optionTelemarketerCanOnlyAssignToManager;
        private bool m_isOptionTelemarketerCanOnlyAssignToManagerSet = false;
        private bool m_optionTelemarketerCanRunPrequal;
        private bool m_isOptionTelemarketerCanRunPrequalSet = false;
        private bool m_hasLenderDefaultFeatures;
        private bool m_hasLenderDefaultFeaturesSet = false;
        private bool m_isLOAllowedToEditProcessorAssignedFile;
        private bool m_isLOAllowedToEditProcessorAssignedFileSet = false;
        private bool m_isOnlyAccountantCanModifyTrustAccount;
        private bool m_isOnlyAccountantCanModifyTrustAccountSet = false;
        private bool m_isOthersAllowedToEditUnderwriterAssignedFile;
        private bool m_isOthersAllowedToEditUnderwriterAssignedFileSet = false;
        private bool m_isAllowViewLoanInEditorByDefault;
        private bool m_isAllowViewLoanInEditorByDefaultSet = false;
        private bool m_canRunPricingEngineWithoutCreditReportByDefault;
        private bool m_canRunPricingEngineWithoutCreditReportByDefaultSet = false;
        private bool m_restrictWritingRolodexByDefault;
        private bool m_restrictWritingRolodexByDefaultSet = false;
        private bool m_restrictReadingRolodexByDefault;
        private bool m_restrictReadingRolodexByDefaultSet = false;
        private bool m_cannotDeleteLoanByDefault;
        private bool m_cannotDeleteLoanByDefaultSet = false;
        private bool m_cannotCreateLoanTemplateByDefault;
        private bool m_cannotCreateLoanTemplateByDefaultSet = false;
        private bool m_isDuplicateLoanCreatedWAutonameBit;
        private bool m_isDuplicateLoanCreatedWAutonameBitSet = false;
        private bool m_is2ndLoanCreatedWAutonameBit;
        private bool m_is2ndLoanCreatedWAutonameBitSet = false;
        private bool m_hasLONIntegration;
        private bool m_hasLONIntegrationSet = false;
        private bool m_isBlankTemplateInvisibleForFileCreation;
        private bool m_isBlankTemplateInvisibleForFileCreationSet = false;
        private bool m_isRoundUpLpeRateTo1Eighth;
        private bool m_isRoundUpLpeRateTo1EighthSet = false;
        private bool m_isRateLockedAtSubmission;
        private bool m_isRateLockedAtSubmissionSet = false;
        private bool m_hasManyUsers;
        private bool m_hasManyUsersSet = false;
        private string m_emailAddrSendFromForNotif = null;
        private string m_nameSendFromForNotif = null;
        private int m_lpeLockPeriodAdj;
        private bool m_lpeLockPeriodAdjSet = false;
        private int m_numberBadPasswordsAllowed; // OPM 19920
        private bool m_numberBadPasswordsAllowedSet = false; // OPM 19920
        private int m_maxWebServiceDailyCallVolume;
        private bool m_maxWebServiceDailyCallVolumeSet = false;
        private string m_namingPattern = null;
        private string m_leadNamingPattern = null;
        private long m_namingCounter;
        private bool m_namingCounterSet = false;
        private long m_testNamingCounter;
        private bool m_testNamingCounterSet = false;
        private string m_ecoaAddress = null;
        private string[] m_fairLendingNoticeAddress = null;
        private ChoiceList m_conditionChoices = null;
        private OptionSet m_fieldChoices = null;
        private string m_LeadSourcesXmlContent = "";
        private string m_pmlCompanyTiersXmlContent = string.Empty;
        private NotificationRules m_notifRules = null;
        private Guid m_lpePriceGroupIdDefault = Guid.Empty;
        private bool m_lpePriceGroupIdDefaultSet = false;
        private String m_lpeSubmitAgreement = null;

        // OPM 199082, 9/23/2015, ML
        protected Guid? defaultWholesaleBranchId = null;
        private string defaultWholesaleBranchName = null;
        protected Guid? defaultWholesalePriceGroupId = null;
        private string defaultWholesalePriceGroupName = null;

        protected Guid? defaultMiniCorrBranchId = null;
        protected Guid? defaultMiniCorrPriceGroupId = null;

        protected Guid? defaultCorrBranchId = null;
        protected Guid? defaultCorrPriceGroupId = null;
        // End OPM 199082

        private Guid m_pmlLoanTemplateID = Guid.Empty;
        private bool m_isPmlLoanTemplateIDSet = false;
        private Guid m_miniCorrLoanTemplateID = Guid.Empty;
        private bool m_isMiniCorrLoanTemplateIDSet = false;
        private Guid m_corrLoanTemplateID = Guid.Empty;
        private bool m_isCorrLoanTemplateIDSet = false;

        private Guid? m_retailTpoLoanTemplateID = null;
        private bool m_isRetailTpoLoanTemplateIDSet = false;

        private Guid m_pmlSiteID = Guid.Empty;
        private Guid m_nhcKey = Guid.Empty;
        private bool m_nhcKeySet = false;
        private string m_pdfCustomFormAssignmentXmlContent = null;
        private string m_emailAddressesForSystemChangeNotif = null;
        private string m_notes = null;
        private string m_creditMornetPlusUserID = null;
        private Lazy<string> lazyCreditMornetPlusPassword = new Lazy<string>(() => null);
        private bool creditMornetPlusPasswordSet = false;
        private E_ShowCredcoOptionT m_showCredcoOptionT = E_ShowCredcoOptionT.DisplayOnlyFNMA;
        private bool m_showCredcoOptionTSet = false;
        private E_EDocsEnabledStatusT m_EDocsEnabledStatusT = E_EDocsEnabledStatusT.Yes;
        private bool m_EDocsEnabledStatusTSet = false;
        private bool m_isAEAsOfficialLoanOfficer = false;
        private bool m_isAEAsOfficialLoanOfficerSet = false;
        private Lazy<string> lazyTempOptionXmlContent = new Lazy<string>(() => null);
        private bool isTempOptionXmlContentSet = false;
        //private bool m_isAsk3rdPartyUwResultInPml = false;    // removing opm 132564
        //private bool m_isAsk3rdPartyUwResultInPmlSet = false; // removing opm 132564
        private string m_licensePlanType = null;
        private DateTime m_licensePlanStartD = DateTime.MinValue;
        private string m_blockedCRAsXmlContent = null;
        private bool m_blockedCRAsXmlContentSet = false;
        private bool m_isAutoGenerateLiabilityPayOffConditions = false;  // 4/25/2006 mf - OPM 4182
        private bool m_isAutoGenerateLiabilityPayOffConditionsSet = false;
        private bool m_isAllowExternalUserToCreateNewLoan = false; // 07/27/06 mf - OPM 6682
        private bool m_isAllowExternalUserToCreateNewLoanSet = false;
        private bool m_isShowPriceGroupInEmbeddedPml = false; // 09/05/06 mf - OPM 7259
        private bool m_isShowPriceGroupInEmbeddedPmlSet = false;
        private int m_pmlSubmitRLockDefaultT; // 09/07/06 mf - OPM 7024
        private bool m_pmlSubmitRLockDefaultTSet = false;
        private bool m_isLpeManualSubmissionAllowed; // 11/16/06 - OPM 5048
        private bool m_isLpeManualSubmissionAllowedSet = false;
        private bool m_isPmlPointImportAllowed; // 11/16/06 mf - OPM 4565
        private bool m_isPmlPointImportAllowedSet = false;
        private string m_fthbCustomDefinition = null; // 11/28/06 db - OPM 8340
        private bool m_allowPmlUserOrderNewCreditReport = true; //02/08/07 db - OPM 9780
        private bool m_allowPmlUserOrderNewCreditReportSet = false; //02/08/07 db - OPM 9780
        private bool m_pmlRequireEstResidualIncome = false;
        private bool m_pmlRequireEstResidualIncomeSet = false;
        private bool m_roundUpLpeFee = false; //02/23/07 db - OPM 10612
        private bool m_roundUpLpeFeeSet = false;
        //TODO - create set of defaults from database info
        private decimal m_roundUpLpeFeeInterval = 0.125M;
        private bool m_roundUpLpeFeeIntervalSet = false;
        private bool m_requireVerifyLicenseByState = false;
        private bool m_requireVerifyLicenseByStateSet = false;
        //private bool m_canLpNameBeManuallyModified; // 5/17/07 db - OPM 12061 // REMOVING: OPM 129291
        //private bool m_canLpNameBeManuallyModifiedSet; // 5/17/07 db - OPM 12061 // REMOVING: OPM 129291
        private bool m_isAllPrepaymentPenaltyAllowed; // 5/17/07 db - OPM 12943
        private bool m_isAllPrepaymentPenaltyAllowedSet; // 5/17/07 db - OPM 12943
        private bool m_isLpeDisqualificationEnabled; // 6/19/07 db - OPM 16132
        private bool m_isLpeDisqualificationEnabledSet; // 6/19/07 db - OPM 16132
        private DateTime m_lpeLockDeskWorkHourEndTime = DateTime.MinValue;
        private bool m_lpeLockDeskWorkHourEndTimeSet;
        private int m_lpeMinutesNeededToLockLoan;
        private bool m_lpeMinutesNeededToLockLoanSet;
        private DateTime m_lpeLockDeskWorkHourStartTime = DateTime.MinValue;
        private bool m_lpeLockDeskWorkHourStartTimeSet = false;
        private bool m_lpeIsEnforceLockDeskHourForNormalUser = false;
        private bool m_lpeIsEnforceLockDeskHourForNormalUserSet = false;
        private bool m_displayPmlFeeIn100Format; // 1/3/08 db - OPM 19525
        private bool m_displayPmlFeeIn100FormatSet = false; // 1/3/08 db - OPM 19525
        private bool m_isUsingRateSheetExpirationFeature = false; // 1/9/08 db - OPM 19717
        private bool m_isUsingRateSheetExpirationFeatureSet = false; // 1/9/08 db - OPM 19717
        private bool m_isOptionARMUsedInPml = false; // 1/23/08 db - OPM 19782  04/09/08 av opm 21413 changed to false.
        private bool m_isOptionARMUsedInPmlSet = false; // 1/23/08 db - OPM 19782
        private bool m_isPmlSubmissionAllowed = true; // OPM 19783
        private bool m_isPmlSubmissionAllowedSet = false; // OPM 19783
        private string m_DDLSpecialConfigXmlContent = "";
        private bool m_DDLSpecialConfigXmlContentSet = false;
        private bool m_isStandAloneSecondAllowedInPml = true; // OPM 18430
        private bool m_isStandAloneSecondAllowedInPmlSet = false; // OPM 18430
        private bool m_IsHelocsAllowedInPml = false; // OPM 187671
        private bool m_IsHelocsAllowedInPmlSet = false; // OPM 187671
        private bool m_is8020ComboAllowedInPml = true; // OPM 18430
        private bool m_is8020ComboAllowedInPmlSet = false; // OPM 19784
        private string m_timezoneForRsExpiration = "PST";
        private bool m_timezoneForRsExpirationSet = false;
        private bool m_isBestPriceEnabledSet = false;
        private bool m_isBestPriceEnabled = false;
        private bool m_ShowUnderwriterInPMLCertificate;				//OPM 12711
        private bool m_ShowUnderwriterInPMLCertificateSet = false; //OPM 12711
        private bool m_ShowProcessorInPMLCertificate;			    //OPM 12711
        private bool m_ShowProcessorInPMLCertificateSet = false;     //OPM 12711

        // 11/21/2013 gf - opm 145015
        private bool m_ShowJuniorUnderwriterInPMLCertificate;
        private bool m_ShowJuniorUnderwriterInPMLCertificateSet = false;
        private bool m_ShowJuniorProcessorInPMLCertificate;
        private bool m_ShowJuniorProcessorInPMLCertificateSet = false;

        private bool m_IsAlwaysUseBestPrice;		 //OPM 24214
        private bool m_IsAlwaysUseBestPriceSet = false; //OPM 24214

        private bool m_IsDocMagicBarcodeScannerEnabled = false;
        private bool m_IsDocMagicBarcodeScannerEnabledSet = false;
        private bool m_IsDocuTechBarcodeScannerEnabled = false;
        private bool m_IsDocuTechBarcodeScannerEnabledSet = false;
        private bool m_IsIDSBarcodeScannerEnabled = false;
        private bool m_IsIDSBarcodeScannerEnabledSet = false;

        #region ThirdParty
        private bool m_IsDataTracIntegrationEnabled = false; // OPM 24590
        private bool m_IsDataTracIntegrationEnabledSet = false; // OPM 24590
        private string m_pathToDataTrac = string.Empty; // OPM 24590
        private bool m_IsPathToDataTracSet = false; // OPM 24590
        private string m_dataTracWebserviceURL = string.Empty; // OPM 25798
        private bool m_IsDataTracWebserviceURLSet = false; // OPM 25798

        private bool m_IsExportPricingInfoTo3rdParty = true; // OPM 34444
        private bool m_IsExportPricingInfoTo3rdPartySet = false; // OPM 34444
        #endregion

        private string m_pmlUserAutoLoginOption = ""; // OPM 19618
        private bool m_pmlUserAutoLoginOptionSet = false; // OPM 19618

        private bool m_isQuickPricerEnable = false; // OPM 27239
        private bool m_isQuickPricerEnableSet = false; // OPM 27239

        private Guid m_quickPricerTemplateId = Guid.Empty; // OPM 27239
        private bool m_isQuickPricerTemplateIdSet = false; // OPM 27239
        private string m_quickPricerDisclaimerAtResult = ""; // OPM 27239
        private bool m_isQuickPricerDisclaimerAtResultSet = false;
        private string m_quickPricerNoResultMessage = ""; // OPM 27239
        private bool m_isQuickPricerNoResultMessageSet = false; // OPM 27239
        private bool m_IsRateOptionsWorseThanLowerRateShownWithBestPriceView = false; //opm 24899
        private bool m_IsRateOptionsWorseThanLowerRateShownWithBestPriceViewSet = false; //opm 24899  
        private bool m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView = true;  //opm 24899  
        private bool m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceViewSet = false;  //opm 24899  
        private E_BrokerBillingVersion m_BillingVersion;
        private bool m_BillingVersionSet;
        private LicenseInfoList m_licenseInformationList = null;

        private bool m_IsPricingMultipleAppsSupportedSet = false;    //opm 33530
        private bool m_IsPricingMultipleAppsSupported = false;       //opm 33530

        private bool m_IsForceChangePasswordForPml = false; //opm 34274
        private bool m_IsForceChangePasswordForPmlSet = false; //opm 34274


        private bool m_IsaBEmailRequiredInPmlSet = false;    //opm 33208
        private bool m_IsaBEmailRequiredInPml = false;       //opm 33208

        private bool m_IsAllowSharedPipelineSet = false;    //opm 34381
        private bool m_IsAllowSharedPipeline = false;       //opm 34381

        private bool m_UseFHATOTALProductionAccountSet = false; //OPM 49148
        private bool m_UseFHATOTALProductionAccount = false; //OPM 49148

        private bool m_IsIncomeAssetsRequiredFhaStreamlineSet = false; //OPM 42579
        private bool m_IsIncomeAssetsRequiredFhaStreamline = false; //OPM 42579

        private bool m_IsEDocsEnabledSet = false;
        private bool m_IsEDocsEnabled = false;

        private bool m_IsEncompassIntegrationEnabledSet = false; //opm 45012
        private bool m_IsEncompassIntegrationEnabled = false;    //opm 45012

        private string m_sNmlsIdentifier = string.Empty;

        private bool m_FhaLenderIdSet = false; // OPM 48103
        private string m_FhaLenderId = string.Empty; // OPM 48103

        private bool m_IsHideFhaLenderIdInTotalScorecardSet = false; // OPM 48103
        private bool m_IsHideFhaLenderIdInTotalScorecard = false; // OPM 48103
        private bool m_IsDay1DayOfRateLock = false; //OPM 55513
        private bool m_IsDay1DayOfRateLockSet = false; //OPM 55513

        private bool m_isDocuTechEnabled = false;
        private bool m_isDocuTechEnabledSet = false;
        private bool m_IsDocuTechStageEnabled = false;
        private bool m_IsDocuTechStageEnabledSet = false;

        private Nullable<DateTime> m_OriginatorCompensationMigrationDate = new Nullable<DateTime>();
        private bool m_OriginatorCompensationApplyMinYSPForTPOLoanSet = false;
        private bool m_OriginatorCompensationApplyMinYSPForTPOLoan = false;

        private bool m_defaultLockDeskIDSet = false;
        private Guid m_defaultLockDeskID;

        private bool m_IsUsePriceIncludingCompensationInPricingResult = false; //OPM 64253
        private bool m_IsUsePriceIncludingCompensationInPricingResultSet = false; //OPM 64253

        private bool m_IsDisplayCompensationChoiceInPml = false;
        private bool m_IsDisplayCompensationChoiceInPmlSet = false;

        private bool m_IsHideLOCompPMLCert = false;
        private bool m_IsHideLOCompPMLCertSet = false;

        private bool m_IsHidePricingwithoutLOCompPMLCert = false;
        private bool m_IsHidePricingwithoutLOCompPMLCertSet = false;


        private bool m_IsAlwaysDisplayExactParRateOption = false;
        private bool m_IsAlwaysDisplayExactParRateOptionSet = false;

        private Nullable<int> m_DefaultTaskPermissionLevelIdForImportedCategories = new Nullable<int>();
        //private bool m_DefaultTaskPermissionLevelIdForImportedCategoriesSet = false;
        private string m_EdocsStackOrderList = "";


        private Nullable<int> m_AusImportDefaultConditionCategoryId = new Nullable<int>();
        private bool m_IsUseNewCondition = false;
        private bool m_IsUseNewTaskSystemStaticConditionIds = false;
        private E_RateLockExpirationWeekendHolidayBehavior m_RateLockExpirationWeekendHolidayBehavior = E_RateLockExpirationWeekendHolidayBehavior.PrecedingBusinessDay;

        private bool m_IsTotalAutoPopulateAUSResponse = true;
        private bool m_IsTotalAutoPopulateAUSResponseSet = false;

        private bool m_IsApplyPricingEngineRoundingAfterLoComp = true;
        private bool m_IsApplyPricingEngineRoundingAfterLoCompSet = false;

        private bool m_IsAddPointWithoutOriginatorCompensationToRateLock = false;
        private bool m_IsAddPointWithoutOriginatorCompensationToRateLockSet = false;

        private bool m_IsEnabledGfeFeeService = false;
        private bool m_IsEnabledGfeFeeServiceSet = false;

        private Guid? m_OCRVendorId;

        private string m_OCRUsername;
        private bool m_OCRUsernameSet;

        private Lazy<string> lazyOCRPassword = new Lazy<string>(() => null);
        private bool m_OCRPasswordByesSet;

        //OPM 54841
        private bool m_IsImportDoDuLpFindingsAsConditionsSet;
        private bool m_IsImportDoDuLpFindingsAsConditions;

        private bool m_IsDisplayChoiceUfmipInLtvCalc = false;
        private bool m_IsDisplayChoiceUfmipInLtvCalcSet = false;

        private bool isEnableComplianceEagleIntegration;
        private bool isEnableComplianceEagleIntegrationSet = false;

        private bool isEnablePTMComplianceEagleAuditIndicator;
        private bool isEnablePTMComplianceEagleAuditIndicatorSet = false;

        private string complianceEagleUserName;
        private bool complianceEagleUserNameSet = false;

        private Lazy<string> lazyComplianceEaglePassword = new Lazy<string>(() => null);
        private bool complianceEaglePasswordBytesSet = false;

        private string complianceEagleCompanyID;
        private bool complianceEagleCompanyIDSet = false;

        private bool complianceEagleEnableMismo34;
        private bool complianceEagleEnableMismo34Set = false;

        private string m_ComplianceEaseUserName;
        private bool m_ComplianceEaseUserNameSet = false;

        private Lazy<string> lazyComplianceEasePassword = new Lazy<string>(() => null);
        private bool m_ComplianceEasePasswordSet = false;

        private bool m_DocMagicIsAllowuserCustomLogin;
        private bool m_DocMagicIsAllowuserCustomLoginSet = false;

        private bool m_AllowInvestorDocMagicPlanCodes;
        private bool m_AllowInvestorDocMagicPlanCodesSet = false;

        private bool m_IsOnlyAllowApprovedDocMagicPlanCodes;
        private bool m_IsOnlyAllowApprovedDocMagicPlanCodesSet = false;

        private bool m_EnableDsiPrintAndDeliverOption;
        private bool m_EnableDsiPrintAndDeliverOptionSet = false;

        private bool isCenlarEnabled;
        private bool isCenlarEnabledSet = false;

        //opm 80831 av
        private bool m_LPQBaseUrlSet = false;

        private E_DocMagicDocumentSavingOption m_DocMagicDocumentSavingOption = E_DocMagicDocumentSavingOption.Single;
        private bool m_DocMagicDocumentSavingOptionSet = false;

        private E_DocMagicDocumentSavingOption m_BackupDocumentSavingOption = E_DocMagicDocumentSavingOption.Single;
        private bool m_BackupDocumentSavingOptionSet = false;

        private BranchGroupSelection m_DocumentFrameworkCaptureEnabledBranchGroupT = BranchGroupSelection.All;
        private bool m_DocumentFrameworkCaptureEnabledBranchGroupTSet = false;

        // OPM 236480
        private bool m_DocMagicSplitESignedDocs;
        private bool m_DocMagicSplitESignedDocsSet = false;

        private bool m_DocMagicSplitUnsignedDocs;
        private bool m_DocMagicSplitUnsignedDocsSet = false;

        private Nullable<int> m_DocMagicDefaultDocTypeID;
        private bool m_DocMagicDefaultDocTypeIDSet = false;

        private int? m_ESignedDocumentDocTypeId;

        private bool m_IsAutoGenerateMersMin;
        private bool m_IsAutoGenerateMersMinSet = false;
        private long m_MersCounter;
        private bool m_MersCounterSet = false;

        private string m_MersOrganizationId;
        private bool m_MersOrganizationIdSet = false;

        private bool m_IsEnableDriveIntegration = false;
        private bool m_IsEnableDriveIntegrationSet = false;

        private bool m_IsEnableDriveConditionImporting = false;
        private bool m_IsEnableDriveConditionImportingSet = false;

        private int? m_DriveConditionCategoryID = null;
        private bool m_DriveConditionCategoryIDSet = false;

        private int? m_DriveDocTypeID = null;
        private bool m_DriveDocTypeIDSet = false;

        private int m_DriveDueDateCalculationOffset = 0;
        private bool m_DriveDueDateCalculationOffsetSet = false;

        private string m_DriveDueDateCalculatedFromField = null;
        private bool m_DriveDueDateCalculatedFromFieldSet = false;

        private int? m_DriveTaskPermissionLevelID = null;
        private bool m_DriveTaskPermissionLevelIDSet = false;

        private string m_AvailableLockPeriodOptionsCSV = "";
        private bool m_AvailableLockPeriodOptionsCSVSet = false;
        private string m_AvailableLockPeriodOptionsCSVOriginal = "";

        private bool m_IsEnableComplianceEaseIntegration = false;
        private bool m_IsEnableComplianceEaseIntegrationSet = false;

        // opm 88978
        private bool m_IsEnablePTMComplianceEaseIndicator = false;
        private bool m_IsEnablePTMComplianceEaseIndicatorSet = false;

        private bool m_IsEnablePTMDocMagicSeamlessInterface = false;
        private bool m_IsEnablePTMDocMagicSeamlessInterfaceSet = false;

        private string m_NmlsCallReportOriginationRelatedRevenueJSONContent = "";
        private bool m_NmlsCallReportOriginationRelatedRevenueJSONContentSet = false;

        private bool m_CalculateclosingCostInPMLSet = false;
        private bool m_CalculateclosingCostInPML = false;

        private bool m_ApplyClosingCostToGFESet = false;
        private bool m_ApplyClosingCostToGFE = false;

        private bool m_PmiProviderGenworthRankingSet = false;
        private int m_PmiProviderGenworthRanking = -1;
        private bool m_PmiProviderMgicRankingSet = false;
        private int m_PmiProviderMgicRanking = -1;
        private bool m_PmiProviderRadianRankingSet = false;
        private int m_PmiProviderRadianRanking = -1;

        // OPM 169010 - Arch MI acquired CMG
        private bool pmiProviderArchRankingSet = false;
        private int pmiProviderArchRanking = -1;

        private bool m_PmiProviderEssentRankingSet = false;
        private int m_PmiProviderEssentRanking = -1;

        private int m_PmiProviderNationalMIRanking = -1;
        private bool m_PmiProviderNationalMIRankingSet = false;

        private bool m_IsHighestPmiProviderWins = false;
        private bool m_IsHighestPmiProviderWinsSet = false;

        private bool m_PmiProviderMassHousingRankingSet = false;
        private int m_PmiProviderMassHousingRanking = -1;

        // OPM 84258
        private bool m_IsEnableProvidentFundingSubservicingExport = true;
        private bool m_IsEnableProvidentFundingSubservicingExportSet = false;

        private bool m_IsForceLeadToLoanTemplate = false;
        private bool m_IsForceLeadToUseLoanCounter = false;
        private bool m_IsForceLeadToLoanNewLoanNumber = false;
        private bool m_IsForceLeadToLoanRemoveLeadPrefix = false;

        private bool m_IsEditLeadsInFullLoanEditor = false;
        private bool m_IsEditLeadsInFullLoanEditorSet = false;

        private bool m_AutoSaveDocMagicGeneratedDocs;
        private bool m_AutoSaveDocMagicGeneratedDocsSet = false;

        private bool m_AutoSaveLeCdReceivedDate;
        private bool m_AutoSaveLeCdReceivedDateSet = false;

        private bool m_RequireAllBorrowersToReceiveCD;
        private bool m_RequireAllBorrowersToReceiveCDSet = false;

        private bool m_AutoSaveLeCdSignedDate;
        private bool m_AutoSaveLeCdSignedDateSet = false;

        private bool m_AutoSaveLeSignedDateWhenAnyBorrowerSigns;
        private bool m_AutoSaveLeSignedDateWhenAnyBorrowerSignsSet = false;

        private bool m_DefaultIssuedDateToToday;
        private bool m_DefaultIssuedDateToTodaySet = false;

        private bool m_EnableCocRedisclosureTriggersForHelocLoans;
        private bool m_EnableCocRedisclosureTriggersForHelocLoansSet = false;

        private bool m_UseCustomCocFieldList;
        private bool m_UseCustomCocFieldListSet = false;

        private bool m_IsCreditUnion;
        private bool m_IsCreditUnionSet = false;

        private bool m_IsFederalCreditUnion;
        private bool m_IsFederalCreditUnionSet = false;

        private bool m_IsTaxExemptCreditUnion;
        private bool m_IsTaxExemptCreditUnionSet = false;

        private bool m_IsEnableRateMonitorService = true;
        private bool m_IsEnableRateMonitorServiceSet = false;

        private bool m_EnableDuplicateEDocProtectionChecking = false;
        private bool m_EnableDuplicateEDocProtectionCheckingSet = false;

        private bool m_IsNewPmlUIEnabled;
        private bool m_IsNewPmlUIEnabledSet = false;

        private bool m_IsAllowOrderingGlobalDmsAppraisals;
        private bool m_IsAllowOrderingGlobalDmsAppraisalsSet = false;

        private CustomPmlFieldList m_CustomPmlFieldList = null;
        private bool m_CustomPmlFieldListSet = false;

        private string m_MersFormat;
        private bool m_MersFormatSet;

        private bool m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP;
        private bool m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSPSet = false;

        private bool m_IsUseLayeredFinancing;
        private bool m_IsUseLayeredFinancingSet = false;

        private bool m_EnableAutoPriceEligibilityProcess;
        private bool m_EnableAutoPriceEligibilityProcessSet = false;

        private bool m_EnableRetailTpoPortalMode;
        private bool m_EnableRetailTpoPortalModeSet = false;

        private bool m_EnableRenovationLoanSupport;
        private bool m_EnableRenovationLoanSupportSet = false;

        private bool m_SendAutoLockToLockDesk;
        private bool m_SendAutoLockToLockDeskSet = false;

        private bool m_SaveLockConfOnAutoLock;
        private bool m_SaveLockConfOnAutoLockSet = false;

        private bool m_IsDocVendorTesting;
        private bool m_IsDocVendorTestingSet = false;

        private bool m_defaultLockPolicyIDSet = false;
        private Guid? m_defaultLockPolicyID = null;

        private string m_sCustomFieldDescriptionXml = ""; //opm 117681 
        private bool m_sCustomFieldDescriptionXmlSet = false;
        Dictionary<string, string> m_sBrokerWideCustomFieldDescriptions = new Dictionary<string, string>();

        private bool m_IsEnableNewConsumerPortal = false;
        private bool m_IsEnableNewConsumerPortalSet = false;

        // OPM 145923 - Autonumber trades
        private bool m_IsAutoGenerateTradeNums = false;
        private bool m_IsAutoGenerateTradeNumsSet = false;
        private long m_TradeCounter;
        private bool m_TradeCounterSet = false;

        // OPM 145924 - Autonumber pools
        private long m_PoolCounter;
        private bool m_PoolCounterSet = false;

        #region Flood Integration //OPM 12758
        private bool m_IsEnableFloodIntegration = false;
        private bool m_IsEnableFloodIntegrationSet = false;

        private bool m_IsCCPmtRequiredForFloodIntegration = false;
        private bool m_IsCCPmtRequiredForFloodIntegrationSet = false;

        private short m_FloodProviderId = 0;
        private bool m_FloodProviderIdSet = false;

        private int? m_FloodDocTypeId = null;
        private bool m_FloodDocTypeIdSet = false;

        private string m_FloodAccountId = null;
        private bool m_FloodAccountIdSet = false;

        private Guid m_FloodResellerId = Guid.Empty;
        private bool m_isFloodResellerIdSet = false;

        private string m_EnabledStatusesByChannelAsJsonOnLoad = null;
        private Dictionary<E_BranchChannelT, HashSet<E_sStatusT>> m_EnabledStatusesByChannel = null;

        private string m_EnabledStatusesByCorrespondentProcessTypeAsJsonOnLoad = null;
        private Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>> m_EnabledStatusesByCorrespondentProcessType = null;
        #endregion

        #region DU Import Options //OPM 94140
        private bool m_IsImportDuFindingsByDefault = false;
        private bool m_IsImportDuFindingsByDefaultSet = false;
        private bool m_IsImportDu1003ByDefault = false;
        private bool m_IsImportDu1003ByDefaultSet = false;
        private bool m_IsImportDuCreditReportByDefault = false;
        private bool m_IsImportDuCreditReportByDefaultSet = false;
        #endregion

        private bool m_EnableSequentialNumbering = false;
        private bool m_EnableSequentialNumberingSet = false;
        private long m_SequentialNumberingSeed = 0;
        private bool m_SequentialNumberingSeedSet = false;
        private string m_SequentialNumberingFieldId = "";
        private bool m_SequentialNumberingFieldIdSet = false;

        //opm 120255
        private int? m_ClosingCostTitleVendorId = new int?();

        private List<DocumentVendorBrokerSettings> m_ActiveDocumentVendors = null;
        private bool m_ActiveDocumentVendorsSet = false;

        //private bool m_PopulateGFEWithLockDatesFromFrontEndLock = false; //  opm 134973
        //private bool m_PopulateGFEWithLockDatesFromFrontEndLockSet = false; // opm 134973

        private bool m_TriggerAprRediscNotifForAprDecrease = true;
        private bool m_TriggerAprRediscNotifForAprDecreaseSet = false;


        private bool m_IsRemovePreparedDates = false;               //start opm 145356
        private bool m_IsRemovePreparedDatesSet = false;

        private bool m_IsProtectDisclosureDates = false;
        private bool m_IsProtectDisclosureDatesSet = false;

        private bool m_IsEnableDTPkgBsdRedisclosureDatePop = false;
        private bool m_IsEnableDTPkgBsdRedisclosureDatePopSet = false; //end opm 145356

        private bool m_IsB4AndB6DisabledIn1100And1300OfGfe = false;     // opm 145346
        private bool m_IsB4AndB6DisabledIn1100And1300OfGfeSet = false;

        private bool m_EnableConversationLogForLoans = false;
        private bool m_EnableConversationLogForLoansSet = false;

        private bool m_EnableConversationLogForLoansInTpo = false;
        private bool m_EnableConversationLogForLoansInTpoSet = false;

        private bool m_ForceTemplateOnImport = false;
        private bool m_ForceTemplateOnImportSet = false;

        private bool m_ShowQMStatusInPml2 = false;
        private bool m_ShowQMStatusInPml2Set = false;

        private CustomPmlFieldList m_CustomPricingPolicyFieldList = null;
        private bool m_CustomPricingPolicyFieldListSet = false;

        private string m_PMLNavigationConfig = null;
        private bool m_PMLNavigationConfigSet = false;

        private bool m_allowNonExcludableDiscountPoints = false;
        private bool m_allowNonExcludableDiscountPointsSet = false;

        private string m_activeDirectoryURL = string.Empty; // OPM 106855
        private bool m_IsActiveDirectoryURLSet = false; // OPM 106855

        private E_FeeTypeRequirementT m_feeTypeRequirementT;
        private bool m_feeTypeRequirementTSet = false;

        private bool m_IsAllowAllUsersToUseKayako = false;
        private bool m_IsAllowAllUsersToUseKayakoSet = false;

        private BrokerSuiteType suiteType = BrokerSuiteType.Blank;
        private bool suiteTypeSet = false;

        private string m_SandboxScratchLOXml = "";
        private bool m_SandboxScratchLOXmlSet = false;

        private E_Pml2AsQuickPricerMode m_Pml2AsQuickPricerMode = E_Pml2AsQuickPricerMode.Disabled; // opm 185732
        private bool m_Pml2AsQuickPricerModeSet = false;

        private bool borrPdCompMayNotExceedLenderPdComp = false;
        private bool borrPdCompMayNotExceedLenderPdCompSet = false;

        private IEnumerable<LockPolicy> cachedLockPolicies = null;

        private string m_EmployeeResourcesUrl = string.Empty;

        // opm 189756
        private bool m_AllowRateMonitorForQP2FilesInEmdeddedPml;
        private bool m_AllowRateMonitorForQP2FilesInEmdeddedPmlSet = false;

        private bool m_AllowRateMonitorForQP2FilesInTpoPml;
        private bool m_AllowRateMonitorForQP2FilesInTpoPmlSet = false;

        private bool m_DeterminationRulesXMLContentSet = false;
        private string m_DeterminationRulesXMLContent = "";

        public DeterminationRuleSet m_DeterminationRuleSet = null;

        private E_PermissionType m_selectedNewLoanEditorUIPermissionType = E_PermissionType.HardDisable;
        private bool m_selectedNewLoanEditorUIPermissionTypeSet = false;

        private bool m_IsShowRateOptionsWorseThanLowerRate = false;
        private bool m_IsShowRateOptionsWorseThanLowerRateSet = false;

        private string legalEntityIdentifier = string.Empty;
        private bool legalEntityIdentifierSet = false;

        private bool isUseTasksInTpoPortal = false;
        private bool isUseTasksInTpoPortalSet = false;

        private bool isShowPipelineOfTasksForAllLoansInTpoPortal = false;
        private bool isShowPipelineOfTasksForAllLoansInTpoPortalSet = false;

        private bool isShowPipelineOfConditionsForAllLoansInTpoPortal = false;
        private bool isShowPipelineOfConditionsForAllLoansInTpoPortalSet = false;

        private Lazy<HistoricalPricingSettings> historicalPricingSettings;
        private Lazy<PML3Settings> pmlResults3PricingSettings;
        private Lazy<InternalPricerUiSettings> internalPricerUiSettings;
        private Lazy<ProductCodeFilterSettings> productCodeFilterSettings;
        private Lazy<BrokerSeamlessLPASettings> seamlessLpaSettings;
        private Lazy<NonQmOpSettings> nonQmOpSettings;

        private Lazy<TpoLoanEditorSaveButtonBrokerOptions> tpoLoanEditorSaveButtonBrokerOptions;

        private ComplianceEagleAuditConfigurationData complianceEagleAuditConfigData;
        private bool complianceEagleAuditConfigSet;

        private Guid? tpoNewFeatureAlphaTestingGroup = null;
        private bool tpoNewFeatureAlphaTestingGroupSet = false;

        private Guid? tpoNewFeatureBetaTestingGroup = null;
        private bool tpoNewFeatureBetaTestingGroupSet = false;

        private NewTpoFeatureOcGroupAccess tpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
        private bool tpoRedisclosureAllowedGroupTypeSet = false;

        private TpoRequestFormSource tpoRedisclosureFormSource = TpoRequestFormSource.CustomPdf;
        private bool tpoRedisclosureFormSourceSet = false;

        private Guid? tpoRedisclosureFormCustomPdfId = null;
        private bool tpoRedisclosureFormCustomPdfIdSet = false;

        private string tpoRedisclosureFormHostedUrl = null;
        private bool tpoRedisclosureFormHostedUrlSet = false;

        private int? tpoRedisclosureFormRequiredDocType = null;
        private bool tpoRedisclosureFormRequiredDocTypeSet = false;

        private NewTpoFeatureOcGroupAccess tpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
        private bool tpoInitialClosingDisclosureAllowedGroupTypeSet = false;

        private TpoRequestFormSource tpoInitialClosingDisclosureFormSource = TpoRequestFormSource.CustomPdf;
        private bool tpoInitialClosingDisclosureFormSourceSet = false;

        private OrigPortalAntiSteeringDisclosureAccessType origPortalAntiSteeringDisclosureAccess = OrigPortalAntiSteeringDisclosureAccessType.LenderPaidOriginationOnly;
        private bool origPortalAntiSteeringDisclosureAccessSet = false;

        private string opNonQmMsgReceivingEmail = null;
        private bool opNonQmMsgReceivingEmailSet = false;

        private long opConLogCategoryId = -1;
        private bool opConLogCategoryIdSet = false;

        private Guid? tpoInitialClosingDisclosureFormCustomPdfId = null;
        private bool tpoInitialClosingDisclosureFormCustomPdfIdSet = false;

        private string tpoInitialClosingDisclosureFormHostedUrl = null;
        private bool tpoInitialClosingDisclosureFormHostedUrlSet = false;

        private int? tpoInitialClosingDisclosureFormRequiredDocType = null;
        private bool tpoInitialClosingDisclosureFormRequiredDocTypeSet = false;

        private bool enableKtaIntegration = false;
        private bool enableKtaIntegrationSet = false;

        private bool allowIndicatingVerbalCreditReportAuthorization = false;
        private bool allowIndicatingVerbalCreditReportAuthorizationSet = false;

        private Guid? verbalCreditReportAuthorizationCustomPdfId = null;
        private bool verbalCreditReportAuthorizationCustomPdfIdSet = false;

        private Guid? workflowRulesControllerId = null;
        private bool workflowRulesControllerIdSet = false;

        private bool enableLqbMobileApp = true;
        private bool enableLqbMobileAppSet = false;
        #endregion

        public static BrokerDB NULL { get { return null; } }

        public static Guid GetBrokerIdByBrokerPmlSiteId(Guid brokerPmlSiteId)
        {
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveBrokerIDByLenderSiteID", parameters))
                {
                    if (reader.Read())
                    {
                        return (Guid)reader["BrokerId"];
                    }
                }
            }
            return Guid.Empty;
        }

        private EncryptionKeyIdentifier EncryptionKeyId { get; set; }

        /// <summary>
        /// Tries to get the custom field description for the given field if it exist. Returns success or not. 
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public bool GetCustomFieldDescription(string fieldId, out string description)
        {
            return m_sBrokerWideCustomFieldDescriptions.TryGetValue(fieldId, out description);
        }

        /// <summary>
        /// Gets a value indicating whether the lender enables e-consent monitoring automation.
        /// </summary>
        public bool EnableEConsentMonitoringAutomation { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the lender hides the "EDoc Dropbox" link
        /// in the settings portlet.
        /// </summary>
        public bool HideEdocDropboxInSettingsPortlet => IsStringExistedInTempOption("<option name=\"HideEdocDropboxInSettingsPortlet\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the lender hides the "Status and Agents"
        /// page in the Originator Portal.
        /// </summary>
        public bool HideStatusAndAgentsPageInOriginatorPortal => IsStringExistedInTempOption("<option name=\"HideStatusAndAgentsPageInOriginatorPortal\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the lender hides status submission
        /// buttons in the Originator Portal like "Submit to Document Check".
        /// </summary>
        public bool HideStatusSubmissionButtonsInOriginatorPortal => IsStringExistedInTempOption("<option name=\"HideStatusSubmissionButtonsInOriginatorPortal\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the lender hides the loan status
        /// datapoint on the Rate Lock page in the Originator Portal.
        /// </summary>
        public bool HideRateLockPageLoanStatusInOriginatorPortal => IsStringExistedInTempOption("<option name=\"HideRateLockPageLoanStatusInOriginatorPortal\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the lender is using legacy
        /// loan documentation types.
        /// </summary>
        public bool UsingLegacyDocTypes => IsStringExistedInTempOption("<option name=\"UsingLegacyDocTypes\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the lender is hiding enhanced
        /// loan documentation types.
        /// </summary>
        public bool HideEnhancedDocTypes => IsStringExistedInTempOption("<option name=\"HideEnhancedDocTypes\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the BrowserXT verification
        /// post-log in step is disabled for all B users of the lender.
        /// </summary>
        public bool DisableBrowserXtVerificationPostLogInStep => IsStringExistedInTempOption("<option name=\"DisableBrowserXtVerificationPostLogInStep\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the Disclosure Center will
        /// display borrower-level disclosure tracking data.
        /// </summary>
        public bool EnableBorrowerLevelDisclosureTracking => IsStringExistedInTempOption("<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />");

        /// <summary>
        /// Gets a value indicating whether the client-facing workflow UI is
        /// enabled for the lender.
        /// </summary>
        public bool ExposeClientFacingWorkflowConfiguration => IsStringExistedInTempOption("<option name=\"ExposeClientFacingWorkflowConfiguration\" value=\"true\" />");

        /// <summary>
        /// Gets or sets the user ID of the employee responsible for controlling 
        /// the lender's workflow configuration.
        /// </summary>
        public Guid? WorkflowRulesControllerId
        {
            get { return this.workflowRulesControllerId; }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.workflowRulesControllerId = value;
                this.workflowRulesControllerIdSet = true;
            }
        }

        public bool ExcludeZeroAdjustmentForDocGenInLoXml => IsStringExistedInTempOption(@"<option name=""excludeZeroAdjustmentForDocGenInLoXml"" value=""true"" />");

        public bool IsBarcodeUploadScriptEnabled { get { return IsStringExistedInTempOption("<option name=\"IsBarcodeUploadScriptEnabled\" value=\"true\" />"); } }

        public bool EnableLenderCreditsWhenInLegacyClosingCostMode { get { return IsStringExistedInTempOption("<option name=\"EnableLenderCreditsWhenInLegacyClosingCostMode\" value=\"true\" />"); } }

        public bool RenameAlternativeDocType { get { return IsStringExistedInTempOption("<option name=\"RenameAlternativeDocType\" value=\"true\" />"); } }

        public bool GetCustomFieldName(string fieldId, LendersOffice.QueryProcessor.FieldLookup m_Lookup, out string name)
        {
            if (!haveAllCustomFieldsPopulated)
                populateCustomFieldsCompletely(m_Lookup);
            return m_sBrokerWideCustomFieldNames.TryGetValue(fieldId, out name);
        }

        public bool IsCustomFieldGloballyDefined(string fieldId)
        {
            return m_sBrokerWideCustomFieldDescriptions.ContainsKey(fieldId);
        }

        public Dictionary<String, string> m_sBrokerWideCustomFieldNames = new Dictionary<string, string>();
        public Dictionary<String, String> customFieldPrefixes = new Dictionary<string, string>();
        private bool haveAllCustomFieldsPopulated = false;
        public void populateCustomFieldsCompletely(LendersOffice.QueryProcessor.FieldLookup m_Lookup)
        {
            if (!haveAllCustomFieldsPopulated)
            {
                var fields = m_Lookup.GetAllFields();
                foreach (LendersOffice.QueryProcessor.Field field in fields)
                {
                    //a dummy variable to get tryparse to run
                    int parseOut = 0;
                    //if a field id does contain one of the prefixes, make sure that the chracter following the prefixes location in the id is NOT a number.
                    //If it is a number, don't consider it a prefix.
                    //This prevents sCustomField11Desc from seeing sCustomField1 as a prefix, when only sCustomField11 should be its prefix.
                    var prefixes = customFieldPrefixes.Keys.Where(p => field.Id.Contains(p) && !int.TryParse(field.Id.Substring(p.Length, 1), out parseOut));
                    if (!m_sBrokerWideCustomFieldDescriptions.Keys.Contains(field.Id) && prefixes.Count() > 0)
                    {
                        string prefix = prefixes.Last();
                        string customDesc; // QC Initial Loan
                        GetCustomFieldDescription(prefix + "Desc", out customDesc);

                        string prefixDesc = m_Lookup.LookupById(prefix + "Desc").Name; //Custom Field 17 Description
                        prefixDesc = prefixDesc.Substring(0, prefixDesc.Length - 11); // Custom Field 17 

                        string originalSuffix = field.NamePerm; //Custom Field 17 Date

                        originalSuffix = originalSuffix.Substring(prefixDesc.Length - 1, originalSuffix.Length - prefixDesc.Length + 1);

                        m_sBrokerWideCustomFieldNames.Add(field.Id, customDesc + originalSuffix);

                    }
                }
                haveAllCustomFieldsPopulated = true;
            }
        }
        private void ParseCustomFieldXml()
        {
            if (string.IsNullOrEmpty(m_sCustomFieldDescriptionXml.TrimWhitespaceAndBOM()))
            {
                return; //nothing to do
            }

            try
            {
                m_sBrokerWideCustomFieldDescriptions.Clear();
                XDocument doc = XDocument.Parse(m_sCustomFieldDescriptionXml);
                foreach (XElement customField in doc.Descendants("customfield"))
                {
                    XAttribute fieldIdAttr = customField.Attribute("id");
                    XAttribute descriptionAttr = customField.Attribute("desc");
                    if (fieldIdAttr != null && descriptionAttr != null)
                    {
                        m_sBrokerWideCustomFieldDescriptions.Add(fieldIdAttr.Value.TrimWhitespaceAndBOM(), descriptionAttr.Value.TrimWhitespaceAndBOM());
                    }
                    else
                    {
                        Tools.LogError("Error in custom field xml for broker " + m_brokerID);
                    }
                }
            }
            catch (XmlException e)
            {
                m_sBrokerWideCustomFieldDescriptions.Clear();
                Tools.LogError("brokerid : " + m_brokerID, e);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the title integration will most likely run in pricing.
        /// We cannot be sure unless we pull more data.
        /// </summary>
        /// <param name="canEmployeeUseNewUI">Whether the user can use the new ui (pulled from the principal).</param>
        /// <param name="lLpTemplateId">The template id to check.</param>
        /// <returns>False if we are sure title is not needed, otherwise true.</returns>
        public bool IsTitleRunRequiredForPricing(bool canEmployeeUseNewUI, Guid lLpTemplateId)
        {
            // Fee service has to be enabled and a title integration enabled.
            if (!IsEnableNewFeeService || (!this.EnableEnhancedTitleQuotes && !this.ClosingCostTitleVendorId.HasValue))
            {
                return false;
            }

            var revisionHistory = DataAccess.FeeService.FeeServiceRevisionHistory.Retrieve(this.BrokerID);

            if (revisionHistory.CurrentFeeServiceRevision == null)
            {
                return false;
            }

            // We dont calculate closing cost for Total SCore Card 
            if (lLpTemplateId == ConstStage.TotalScorecardLoanProgramId)
            {
                return false;
            }
            
            if (!this.CalculateclosingCostInPML)
            {
                return false;
            }

            if (!this.IsNewPmlUIEnabled && !canEmployeeUseNewUI)
            {
                return false;
            }

            // if we load the loan we can check the branch channel and the purchase type 
            // and compare against TitleInterfaceEnabledBusinessChannels and TitleInterfaceEnabledLoanPurpose

            return true;
        }

        public string CustomFieldDescriptionXml
        {
            get
            {
                return m_sCustomFieldDescriptionXml; 
            }

            set
            {
                if (!string.IsNullOrEmpty(value.TrimWhitespaceAndBOM()))
                {
                    var doc = XDocument.Parse(value);
                    foreach (XElement field in doc.Descendants("customfield"))
                    {
                        if (field.Attribute("id") != null && field.Attribute("desc") != null)
                        {
                            var id = field.Attribute("id").Value;
                            var desc = field.Attribute("desc").Value;

                            switch (id)
                            {
                                case "sU1LStatDesc":
                                case "sU2LStatDesc":
                                case "sU3LStatDesc":
                                case "sU4LStatDesc":
                                    if (desc.Length > 21)
                                    {
                                        throw CBaseException.GenericException("Custom Event Description cannot be longer than 21 characters.");
                                    }
                                    break;
                                case "sTrust1Desc": case "sTrust2Desc": case "sTrust3Desc": case "sTrust4Desc": case "sTrust5Desc": case "sTrust6Desc": case "sTrust7Desc": case "sTrust8Desc":
                                case "sTrust9Desc": case "sTrust10Desc": case "sTrust11Desc": case "sTrust12Desc": case "sTrust13Desc": case "sTrust14Desc": case "sTrust15Desc": case "sTrust16Desc":
                                    if (desc.Length > 36)
                                    {
                                        throw CBaseException.GenericException("Trust Account Item Descriptions cannot be longer than 36 characters.");
                                    }
                                    break;
                                default:
                                    //customf ield desc have a max length of 50.
                                    if (desc.Length > 50)
                                    {
                                        throw CBaseException.GenericException("Custom Field Descriptions cannot be longer than 50 characters.");
                                    }
                                    break;
                            }
                        }
                    }
                    //ensure its valid xml if not this will throw error
                }

                m_sCustomFieldDescriptionXmlSet = true;
                m_sCustomFieldDescriptionXml = value;
            }
        }

        #region Public getters/setters

        private bool showLoanProductIdentifier;
        private bool showLoanProductIdentifierSet = false;
        public bool ShowLoanProductIdentifier
        {
            get { return this.showLoanProductIdentifier; }

            set
            {
                this.showLoanProductIdentifier = value;
                this.showLoanProductIdentifierSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the lender enables
        /// the LQB mobile app.
        /// </summary>
        public bool EnableLqbMobileApp
        {
            get { return this.enableLqbMobileApp; }
            set
            {
                this.enableLqbMobileApp = value;
                this.enableLqbMobileAppSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the lender allows users to indicate
        /// that credit report authorization was received verbally.
        /// </summary>
        public bool AllowIndicatingVerbalCreditReportAuthorization
        {
            get { return this.allowIndicatingVerbalCreditReportAuthorization; }
            set
            {
                this.allowIndicatingVerbalCreditReportAuthorization = value;
                this.allowIndicatingVerbalCreditReportAuthorizationSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the ID of the custom PDF document for verbal credit authorization.
        /// </summary>
        public Guid? VerbalCreditReportAuthorizationCustomPdfId
        {
            get { return this.verbalCreditReportAuthorizationCustomPdfId; }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.verbalCreditReportAuthorizationCustomPdfId = value;
                this.verbalCreditReportAuthorizationCustomPdfIdSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value for the new UI permission type at the lender level as seen in the UI. <para></para>
        /// You probably want the ActualNewLoanEditorUIPermissionType. <para></para>
        /// </summary>
        /// <value>The new UI permission type as selected for the lender level.</value>
        public E_PermissionType SelectedNewLoanEditorUIPermissionType
        {
            get
            {
                return this.m_selectedNewLoanEditorUIPermissionType;
            }
            set
            {
                this.m_selectedNewLoanEditorUIPermissionTypeSet = true;
                this.m_selectedNewLoanEditorUIPermissionType = value;
            }
        }

        /// <summary>
        /// Gets a value representing the new UI permission type as depending on the stage configuration and the selection. <para/>
        /// If you are setting up UI selecthough, use SelectedNewLoanEditorUIPermissionType.
        /// </summary>
        /// <value>A calculated value using both the selection and the stage value.</value>
        public E_PermissionType ActualNewLoanEditorUIPermissionType
        {
            get
            {
                switch (ConstStage.NewLoanEditorUIPermissionType)
                {
                    case E_PermissionType.HardDisable:
                        return E_PermissionType.HardDisable;
                    case E_PermissionType.HardEnable:
                        return E_PermissionType.HardEnable;
                    case E_PermissionType.DeferToMoreGranularLevel:
                        return this.SelectedNewLoanEditorUIPermissionType;
                    default:
                        throw new UnhandledEnumException(ConstStage.NewLoanEditorUIPermissionType);
                }
            }
        }

        public NavigationConfiguration GetPmlNavigationConfiguration(AbstractUserPrincipal principal)
        {
            NavigationConfiguration config = new NavigationConfiguration();

            var isNonQmOpEnabled = principal == null ? false : IsNonQmOpEnabled(principal);
            config.ReadFromXml(m_PMLNavigationConfig, isNonQmOpEnabled);            
            return config;
        }

        public void SetPmlNavigationConfiguration(AbstractUserPrincipal principal, NavigationConfiguration value)
        {
            var isNonQmOpEnabled = principal == null ? false : IsNonQmOpEnabled(principal);

            m_PMLNavigationConfigSet = true;
            m_PMLNavigationConfig = value.SerializeNavigation(isNonQmOpEnabled);
        }

        public bool ForceTemplateOnImport
        {
            get { return m_ForceTemplateOnImport; }
            set
            {
                m_ForceTemplateOnImport = value;
                m_ForceTemplateOnImportSet = true;
            }
        }

        public bool IsB4AndB6DisabledIn1100And1300OfGfe
        {
            get { return m_IsB4AndB6DisabledIn1100And1300OfGfe; }
            set
            {
                m_IsB4AndB6DisabledIn1100And1300OfGfe = value;
                m_IsB4AndB6DisabledIn1100And1300OfGfeSet = true;
            }
        }

        /// <summary>
        /// This represents the actual enabling / disabling, factoring into account stage config and <see cref="EnableConversationLogForLoans"/>.
        /// </summary>
        public bool ActuallyEnableConversationLogForLoans
        {
            get
            {
                if (ConstStage.DisableConversationLogFeature)
                {
                    return false;
                }
                return this.EnableConversationLogForLoans;
            }
        }

        /// <summary>
        /// This represents the loadmin user's intent for the broker.  Contract with <see cref="ActuallyEnableConversationLogForLoans"/>.
        /// </summary>
        public bool EnableConversationLogForLoans
        {
            get
            {
                if (ConstStage.DisableConversationLogFeature)
                {
                    return false;
                }
                return this.m_EnableConversationLogForLoans;
            }
            private set
            {
                this.m_EnableConversationLogForLoans = value;
                this.m_EnableConversationLogForLoansSet = true;
            }
        }

        /// <summary>
        /// This represents the actual enabling / disabling, factoring into account stage config and <see cref="EnableConversationLogForLoansInTpo"/>.
        /// </summary>
        public bool ActuallyEnableConversationLogForLoansInTpo
        {
            get
            {
                if (ConstStage.DisableConversationLogFeature)
                {
                    return false;
                }
                return this.EnableConversationLogForLoansInTpo;
            }
        }

        /// <summary>
        /// This represents the loadmin user's intent for the broker.  Contract with <see cref="ActuallyEnableConversationLogForLoansInTpo"/>.
        /// </summary>
        public bool EnableConversationLogForLoansInTpo
        {
            get
            {
                if(ConstStage.DisableConversationLogFeature)
                {
                    return false;
                }

                if (this.m_EnableConversationLogForLoansInTpo && !this.EnableConversationLogForLoans)
                {
                    Tools.LogError("Somehow conversation logs were enabled for tpo but not for loans for broker " 
                        + this.AsSimpleStringForLog() + ". Consider checking past direct db updates and exports.");
                }
                return this.m_EnableConversationLogForLoansInTpo;
            }
            private set
            {
                this.m_EnableConversationLogForLoansInTpo = value;
                this.m_EnableConversationLogForLoansInTpoSet = true;
            }
        }

        /// <summary>
        /// Enables conversation log for loans, and optionally enables/disables it for tpo.
        /// </summary>
        /// <param name="tpoEnabledSetting">Null leaves the original setting. Otherwise sets tpo setting.</param>
        public void EnableConversationLog(bool? tpoEnabledSetting)
        {
            this.EnableConversationLogForLoans = true;
            if (tpoEnabledSetting.HasValue)
            {
                this.EnableConversationLogForLoansInTpo = tpoEnabledSetting.Value;
            }
        }

        /// <summary>
        /// Disables conversation log in both loan editor and tpo.
        /// </summary>
        public void DisableConversationLog()
        {
            this.EnableConversationLogForLoans = false;
            this.EnableConversationLogForLoansInTpo = false;
        }

        public bool IsEnableDTPkgBsdRedisclosureDatePop
        {
            get { return m_IsEnableDTPkgBsdRedisclosureDatePop; }
            set
            {
                m_IsEnableDTPkgBsdRedisclosureDatePop = value;
                m_IsEnableDTPkgBsdRedisclosureDatePopSet = true;
            }
        }

        public bool IsProtectDisclosureDates
        {
            get { return m_IsProtectDisclosureDates; }
            set
            {
                m_IsProtectDisclosureDates = value;
                m_IsProtectDisclosureDatesSet = true;
            }
        }

        public bool IsRemovePreparedDates
        {
            get { return m_IsRemovePreparedDates; }
            set
            {
                m_IsRemovePreparedDates = value;
                m_IsRemovePreparedDatesSet = true;
            }
        }

        public bool TriggerAprRediscNotifForAprDecrease // 8/30/2013 GF - OPM 118941
        {
            get
            {
                return m_TriggerAprRediscNotifForAprDecrease;
            }
            set
            {
                m_TriggerAprRediscNotifForAprDecrease = value;
                m_TriggerAprRediscNotifForAprDecreaseSet = true;
            }
        }

        private LenderRelationshipWithFreddieMac relationshipWithFreddieMac;
        private bool relationshipWithFreddieMacSet = false;
        public LenderRelationshipWithFreddieMac RelationshipWithFreddieMac
        {
            get
            {
                return this.relationshipWithFreddieMac;
            }

            set
            {
                this.relationshipWithFreddieMacSet = true;
                this.relationshipWithFreddieMac = value;
            }
        }

        //public bool PopulateGFEWithLockDatesFromFrontEndLock // opm 69449
        //{
        //    get
        //    {
        //        return m_PopulateGFEWithLockDatesFromFrontEndLock;
        //    }
        //    set
        //    {
        //        m_PopulateGFEWithLockDatesFromFrontEndLockSet = true;
        //        m_PopulateGFEWithLockDatesFromFrontEndLock = value;
        //    }
        //}

        public int? ClosingCostTitleVendorId
        {
            get
            {
                return m_ClosingCostTitleVendorId;
            }
            set
            {
                m_ClosingCostTitleVendorId = value;
            }
        }

        public bool AutoSaveDocMagicGeneratedDocs
        {
            get { return m_AutoSaveDocMagicGeneratedDocs; }
            set
            {
                m_AutoSaveDocMagicGeneratedDocs = value;
                m_AutoSaveDocMagicGeneratedDocsSet = true;
            }
        }

        public bool AutoSaveLeCdReceivedDate
        {
            get { return m_AutoSaveLeCdReceivedDate; }
            set
            {
                m_AutoSaveLeCdReceivedDate = value;
                m_AutoSaveLeCdReceivedDateSet = true;
            }
        }

        public bool RequireAllBorrowersToReceiveCD
        {
            get
            {
                if (!this.AutoSaveLeCdReceivedDate)
                {
                    return false;
                }

                if (!AllActiveDocumentVendors.Any())
                {
                    return false;
                }

                return this.m_RequireAllBorrowersToReceiveCD;
            }

            set
            {
                this.m_RequireAllBorrowersToReceiveCD = value;
                this.m_RequireAllBorrowersToReceiveCDSet = true;
            }
        }

        public bool AutoSaveLeCdSignedDate
        {
            get { return m_AutoSaveLeCdSignedDate; }
            set
            {
                m_AutoSaveLeCdSignedDate = value;
                m_AutoSaveLeCdSignedDateSet = true;
            }
        }

        public bool AutoSaveLeSignedDateWhenAnyBorrowerSigns
        {
            get
            {
                if (!this.AutoSaveLeCdSignedDate)
                {
                    return false;
                }

                return this.m_AutoSaveLeSignedDateWhenAnyBorrowerSigns;
            }

            set
            {
                this.m_AutoSaveLeSignedDateWhenAnyBorrowerSigns = value;
                this.m_AutoSaveLeSignedDateWhenAnyBorrowerSignsSet = true;
            }
        }

        public bool UseCustomCocFieldList
        {
            get
            {
                return this.m_UseCustomCocFieldList;
            }
            set
            {
                this.m_UseCustomCocFieldList = value;
                this.m_UseCustomCocFieldListSet = true;
            }
        }

        public bool EnableCocRedisclosureTriggersForHelocLoans
        {
            get
            {
                return this.m_EnableCocRedisclosureTriggersForHelocLoans;
            }
            set
            {
                this.m_EnableCocRedisclosureTriggersForHelocLoans = value;
                this.m_EnableCocRedisclosureTriggersForHelocLoansSet = true;
            }
        }

        public bool DefaultIssuedDateToToday
        {
            get
            {
                return this.m_DefaultIssuedDateToToday;
            }

            set
            {
                this.m_DefaultIssuedDateToToday = value;
                this.m_DefaultIssuedDateToTodaySet = true;
            }
        }

        public bool IsCreditUnion
        {
            get { return m_IsCreditUnion; }
            set
            {
                m_IsCreditUnion = value;
                m_IsCreditUnionSet = true;
            }
        }
        public bool IsFederalCreditUnion
        {
            get { return m_IsFederalCreditUnion; }
            set
            {
                m_IsFederalCreditUnion = value;
                m_IsFederalCreditUnionSet = true;
            }
        }
        public bool IsTaxExemptCreditUnion
        {
            get { return m_IsTaxExemptCreditUnion; }
            set
            {
                m_IsTaxExemptCreditUnion = value;
                m_IsTaxExemptCreditUnionSet = true;
            }
        }

        /// <summary>
        /// Gets whether the Broker is hiding sensitive fields in the PML Certificate.
        /// Implemented in OPM 319351 for PML0288 only.  Do not modify if more Brokers
        /// are needed.  Instead, work on a user configurable solution.
        /// </summary>
        public bool HidePmlCertSensitiveFields
        {
            get
            {
                return this.BrokerID.Equals(new Guid("1c337e9e-080b-4c22-ab48-9186c94a2402"));
            }
        }

        public bool IsAllowOrderingGlobalDmsAppraisals
        {
            get { return m_IsAllowOrderingGlobalDmsAppraisals; }
            set
            {
                m_IsAllowOrderingGlobalDmsAppraisals = value;
                m_IsAllowOrderingGlobalDmsAppraisalsSet = true;
            }
        }

        public bool CalculateclosingCostInPML
        {
            get { return m_CalculateclosingCostInPML; }
            set
            {
                m_CalculateclosingCostInPMLSet = true;
                m_CalculateclosingCostInPML = value;
            }
        }

        public bool ApplyClosingCostToGFE
        {
            get { return m_ApplyClosingCostToGFE; }
            set
            {
                m_ApplyClosingCostToGFESet = true;
                m_ApplyClosingCostToGFE = value;
            }
        }

        public int PmiProviderArchRanking
        {
            get { return pmiProviderArchRanking; }
            set
            {
                pmiProviderArchRanking = value;
                pmiProviderArchRankingSet = true;
            }
        }

        public int PmiProviderNationalMIRanking
        {
            get { return m_PmiProviderNationalMIRanking; }
            set
            {
                m_PmiProviderNationalMIRanking = value;
                m_PmiProviderNationalMIRankingSet = true;
            }
        }

        public int PmiProviderMassHousingRanking
        {
            get { return m_PmiProviderMassHousingRanking; }
            set
            {
                m_PmiProviderMassHousingRankingSet = true;
                m_PmiProviderMassHousingRanking = value;
            }
        }

        public int PmiProviderEssentRanking
        {
            get { return m_PmiProviderEssentRanking; }
            set
            {
                m_PmiProviderEssentRankingSet = true;
                m_PmiProviderEssentRanking = value;
            }
        }

        public int PmiProviderGenworthRanking
        {
            get { return m_PmiProviderGenworthRanking; }
            set
            {
                m_PmiProviderGenworthRankingSet = true;
                m_PmiProviderGenworthRanking = value;
            }
        }

        public int PmiProviderMgicRanking
        {
            get { return m_PmiProviderMgicRanking; }
            set
            {
                m_PmiProviderMgicRankingSet = true;
                m_PmiProviderMgicRanking = value;
            }
        }

        public int PmiProviderRadianRanking
        {
            get { return m_PmiProviderRadianRanking; }
            set
            {
                m_PmiProviderRadianRankingSet = true;
                m_PmiProviderRadianRanking = value;
            }
        }

        public bool ShowQMStatusInPml2
        {
            get { return m_ShowQMStatusInPml2; }
            set { m_ShowQMStatusInPml2Set = true; m_ShowQMStatusInPml2 = value; }
        }

        public bool IsHighestPmiProviderWins
        {
            get { return m_IsHighestPmiProviderWins; }
            set
            {
                m_IsHighestPmiProviderWinsSet = true;
                m_IsHighestPmiProviderWins = value;
            }
        }

        public bool AllowNonExcludableDiscountPoints
        {
            get { return m_allowNonExcludableDiscountPoints; }
            set
            {
                m_allowNonExcludableDiscountPoints = value;
                m_allowNonExcludableDiscountPointsSet = true;
            }
        }

        // OPM 26013.  Consider the LenderUsing PMI Programs if any is enabled.
        public bool HasEnabledPMI
        {
            get 
            {
                return
                    m_PmiProviderEssentRanking != -1
                    || m_PmiProviderGenworthRanking != -1
                    || m_PmiProviderMgicRanking != -1
                    || m_PmiProviderRadianRanking != -1
                    || pmiProviderArchRanking != -1
                    || m_PmiProviderNationalMIRanking != -1
                    || m_PmiProviderMassHousingRanking != -1;
            }
        }

        private Dictionary<int, List<E_PmiCompanyT>> m_pmiRankList = null;
        public Dictionary<int, List<E_PmiCompanyT>> PmiRankList
        {
            get
            {
                if (m_pmiRankList == null)
                {
                    var pmiRankList = new Dictionary<int, List<E_PmiCompanyT>>();

                    Action<E_PmiCompanyT, int> addPmICompany = (pmiCompany, rank) => { if (rank != -1) if (pmiRankList.ContainsKey(rank)) pmiRankList[rank].Add(pmiCompany); else pmiRankList[rank] = new List<E_PmiCompanyT>(new E_PmiCompanyT[] { pmiCompany }); };

                    addPmICompany(E_PmiCompanyT.Essent, m_PmiProviderEssentRanking);
                    addPmICompany(E_PmiCompanyT.Genworth, m_PmiProviderGenworthRanking);
                    addPmICompany(E_PmiCompanyT.MGIC, m_PmiProviderMgicRanking);
                    addPmICompany(E_PmiCompanyT.Radian, m_PmiProviderRadianRanking);
                    addPmICompany(E_PmiCompanyT.Arch, pmiProviderArchRanking);
                    addPmICompany(E_PmiCompanyT.NationalMI, m_PmiProviderNationalMIRanking);
                    addPmICompany(E_PmiCompanyT.MassHousing, m_PmiProviderMassHousingRanking);

                    // Order by rank.
                    m_pmiRankList = pmiRankList.OrderBy(p => p.Key).ToDictionary(entry => entry.Key, entry => entry.Value);
                }
                return m_pmiRankList;
            }
        }

        public HashSet<E_sStatusT> GetEnabledStatusesByChannel(E_BranchChannelT channel)
        {
            return GetEnabledStatusesByChannelAndProcessType(channel, E_sCorrespondentProcessT.Blank);
        }

        /// <summary>
        /// Returns a HashSet of enabled statuses for the given channel.
        /// For Correspondent channel, The set ruturned is the union of enabled
        /// status sets for all exposed Correspondent Process Types.
        /// </summary>
        /// <param name="channel">Branch Channel</param>
        /// <param name="process">Correspondent Channel Process Type</param>
        /// <returns>HashSet of enabled statuses</returns>
        public HashSet<E_sStatusT> GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT channel, E_sCorrespondentProcessT process)
        {
            if (channel != E_BranchChannelT.Correspondent)
            {
                return EnabledStatusesByChannel[channel];
            }
            else if (process != E_sCorrespondentProcessT.Blank) // channel == E_BranchChannelT.Correspondent
            {
                return EnabledStatusesByCorrespondentProcessType[process];
            }
            else //if (channel == E_BranchChannelT.Correspondent && process == E_sCorrespondentProcessT.Blank)
            {
                HashSet<E_sStatusT> res = new HashSet<E_sStatusT>(EnabledStatusesByCorrespondentProcessType[E_sCorrespondentProcessT.Delegated]);
                res.UnionWith(EnabledStatusesByCorrespondentProcessType[E_sCorrespondentProcessT.PriorApproved]);
                res.UnionWith(EnabledStatusesByCorrespondentProcessType[E_sCorrespondentProcessT.MiniCorrespondent]);

                if (ExposeBulkMiniBulkCorrProcessTypeEnums)
                {
                    res.UnionWith(EnabledStatusesByCorrespondentProcessType[E_sCorrespondentProcessT.Bulk]);
                    res.UnionWith(EnabledStatusesByCorrespondentProcessType[E_sCorrespondentProcessT.MiniBulk]);
                }

                return res;
            }
        }

        /// <summary>
        /// Added given status to the enabled statuses set for the given branch channel. 
        /// Throws if user attempts to add statuses to E_BranchChannelT.Correspondent.
        /// </summary>
        /// <param name="status">Status Type to be added.</param>
        /// <param name="channel">Branch Channel.</param>
        public void AddEnabledStatusToBranchChannel(E_sStatusT status, E_BranchChannelT channel)
        {
            // Statuses should not be added directly to Blank process
            if (channel == E_BranchChannelT.Correspondent)
            {
                throw new CBaseException(ErrorMessages.Generic, "Status set for Correspondent channel cannot be edited. Must edit status sets for correspondent process types instead.");
            }

            EnabledStatusesByChannel[channel].Add(status);
        }

        /// <summary>
        /// Removes given status from the enabled statuses set for the given branch channel. 
        /// Throws if user attempts to add statuses to E_BranchChannelT.Correspondent.
        /// </summary>
        /// <param name="status">Status Type to be added.</param>
        /// <param name="channel">Branch Channel.</param>
        public void RemoveEnabledStatusFromBranchChannel(E_sStatusT status, E_BranchChannelT channel)
        {
            // Statuses should not be added directly to Blank process
            if (channel == E_BranchChannelT.Correspondent)
            {
                throw new CBaseException(ErrorMessages.Generic, "Status set for Correspondent channel cannot be edited. Must edit status sets for correspondent process types instead.");
            }

            EnabledStatusesByChannel[channel].Remove(status);
        }

        public bool HasEnabledInAnyChannel(E_sStatusT status)
        {
            foreach (E_BranchChannelT channel in Enum.GetValues(typeof(E_BranchChannelT)))
            {
                if (GetEnabledStatusesByChannel(channel).Contains(status))
                {
                    return true;
                }
            }
            return false;
        }

        private readonly object x_EnabledStatusesByChannel_lockObj = new Object();

        /// <summary>
        /// Any status not in here should be considered disabled.
        /// The disabling is a soft form of disabling, in which we try to hide it on the general status page.
        /// To enable/disable statuses for a broker, just add/remove them to/from the channel's hashset.
        /// opm 145251.
        /// 
        /// NOTE: SHOULD NOT BE ACCESSED DIRECTLY. USE GetEnabledStatusesByChannel, AddEnabledStatusToBranchChannel, 
        /// and RemoveEnabledStatusFromBranchChannel INSTEAD.
        /// OPM 185961.
        private Dictionary<E_BranchChannelT, HashSet<E_sStatusT>> EnabledStatusesByChannel
        {
            get
            {
                lock (x_EnabledStatusesByChannel_lockObj)
                {
                    /*
                     * if the db is empty, load up a default dictionary (this should be modifiable somewhere.)
                     * if a channel isn't in the dictionary, load it from the default.
                     * 
                     */
                    if (m_EnabledStatusesByChannel != null)
                    {
                        return m_EnabledStatusesByChannel;
                    }

                    m_EnabledStatusesByChannel =
                            ObsoleteSerializationHelper.JsonDeserialize<Dictionary<E_BranchChannelT, HashSet<E_sStatusT>>>(m_EnabledStatusesByChannelAsJsonOnLoad);

                    if (m_EnabledStatusesByChannel == null)
                    {
                        m_EnabledStatusesByChannel = new Dictionary<E_BranchChannelT, HashSet<E_sStatusT>>();
                    }
                    foreach (E_BranchChannelT channel in Enum.GetValues(typeof(E_BranchChannelT)))
                    {
                        // Do not add defaults for Correspondent channel. It is an amalgam of the EnabledStatusesByCorrespondentProcessType sets and should not be stored separately.
                        if (channel == E_BranchChannelT.Correspondent)
                        {
                            continue;
                        }

                        if (false == m_EnabledStatusesByChannel.ContainsKey(channel))
                        {
                            m_EnabledStatusesByChannel.Add(channel, DefaultEnabledStatuses);
                        }
                    }

                    return m_EnabledStatusesByChannel;
                }
            }
        }

        /// <summary>
        /// Added given status to the enabled statuses set for the given correspondent process type. 
        /// Throws if user attempts to add statuses to E_sCorrespondentProcessT.Blank.
        /// </summary>
        /// <param name="status">Status Type to be added.</param>
        /// <param name="process">Correspondent Process Type.</param>
        public void AddEnabledStatusToCorrespondentProcessType(E_sStatusT status, E_sCorrespondentProcessT process)
        {
            // Statuses should not be added directly to Blank process
            if (process == E_sCorrespondentProcessT.Blank)
            {
                throw new CBaseException(ErrorMessages.Generic, "Status set for Blank Corespondent Process Type cannot be edited. Can only edit statuses for non-blank correspondent process types.");
            }

            EnabledStatusesByCorrespondentProcessType[process].Add(status);
        }

        /// <summary>
        /// Removes given status from the enabled statuses set for the given correspondent process type. 
        /// Throws if user attempts to remove statuses to E_sCorrespondentProcessT.Blank.
        /// </summary>
        /// <param name="status">Status Type to be added.</param>
        /// <param name="process">Correspondent Process Type.</param>
        public void RemoveEnabledStatusFromCorrespondentProcessType(E_sStatusT status, E_sCorrespondentProcessT process)
        {
            // Statuses should not be added directly to Blank process
            if (process == E_sCorrespondentProcessT.Blank)
            {
                throw new CBaseException(ErrorMessages.Generic, "Status set for Blank Corespondent Process Type cannot be edited. Can only edit statuses for non-blank correspondent process types.");
            }

            EnabledStatusesByCorrespondentProcessType[process].Remove(status);
        }

        public bool HasEnabledInAnyCorrespondentProcessType(E_sStatusT status)
        {
            foreach (E_sCorrespondentProcessT process in Enum.GetValues(typeof(E_sCorrespondentProcessT)))
            {
                if (EnabledStatusesByCorrespondentProcessType[process].Contains(status))
                {
                    return true;
                }
            }
            return false;
        }

        private readonly object x_EnabledStatusesByCorrespondentProcessType_lockObj = new Object();

        /// <summary>
        /// Any status not in here should be considered disabled.
        /// The disabling is a soft form of disabling, in which we try to hide it on the general status page.
        /// To enable/disable statuses for a broker, just add/remove them to/from the channel's hashset.
        /// 
        /// NOTE: SHOULD NOT BE ACCESSED DIRECTLY. USE GetEnabledStatusesByChannelAndProcessType,
        /// AddEnabledStatusToCorrespondentProcessType, and RemoveEnabledStatusFromCorrespondentProcessType INSTEAD.
        /// OPM 185961.
        /// </summary>
        private Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>> EnabledStatusesByCorrespondentProcessType
        {
            get
            {
                lock (x_EnabledStatusesByCorrespondentProcessType_lockObj)
                {
                    /*
                     * if the db is empty, load up a default dictionary (this should be modifiable somewhere.)
                     * if a channel isn't in the dictionary, load it from the default.
                     * 
                     */
                    if (m_EnabledStatusesByCorrespondentProcessType != null)
                    {
                        return m_EnabledStatusesByCorrespondentProcessType;
                    }

                    m_EnabledStatusesByCorrespondentProcessType =
                            ObsoleteSerializationHelper.JsonDeserialize<Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>>>(m_EnabledStatusesByCorrespondentProcessTypeAsJsonOnLoad);

                    if (m_EnabledStatusesByCorrespondentProcessType == null)
                    {
                        m_EnabledStatusesByCorrespondentProcessType = new Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>>();
                    }
                    foreach (E_sCorrespondentProcessT process in Enum.GetValues(typeof(E_sCorrespondentProcessT)))
                    {
                        // Do not add defaults for BLANK. It is an amalgam of the other sets and should not be stored separately.
                        if( process == E_sCorrespondentProcessT.Blank)
                        {
                            continue;
                        }

                        if (false == m_EnabledStatusesByCorrespondentProcessType.ContainsKey(process))
                        {
                            m_EnabledStatusesByCorrespondentProcessType.Add(process, DefaultEnabledStatuses);
                        }
                    }

                    return m_EnabledStatusesByCorrespondentProcessType;
                }
            }
        }

        /// <summary>
        /// returns a new hashset everytime.
        /// statuses that existed prior to opm 145251, except loan other
        /// it does include lead statuses and loan web consumer, but does not include loan_other
        /// *TODO* delete this when we implement a default configuration.
        /// </summary>
        private HashSet<E_sStatusT> DefaultEnabledStatuses 
        {
            get
            {
                var statuses = new HashSet<E_sStatusT>();
                for (int i = 0; i < 29; i++)  // statuses that existed prior to opm 145251
                {
                    var status = (E_sStatusT) i;
                    statuses.Add(status);
                }
                statuses.Remove(E_sStatusT.Loan_Other);
                return statuses;
            }
        }

        public string NmlsCallReportOriginationRelatedRevenueJSONContent
        {
            get
            {
                return m_NmlsCallReportOriginationRelatedRevenueJSONContent;
            }
            set
            {
                m_NmlsCallReportOriginationRelatedRevenueJSONContent = value;
                m_NmlsCallReportOriginationRelatedRevenueJSONContentSet = true;
            }
        }
        public IEnumerable<uint> AvailableLockPeriodOptions
        {
            get {
                if (IsEnabledLockPeriodDropDown)
                {
                    return AvailableLockPeriodOptionsCSV.Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries
                        ).Select((x) => UInt32.Parse(x)
                        ).OrderBy((x) => x);
                }
                else
                {
                    return new List<uint>();
                }
            }
        }
        public string AvailableLockPeriodOptionsCSV
        {
            get { return m_AvailableLockPeriodOptionsCSV; }
            set
            {
                m_AvailableLockPeriodOptionsCSV = value;
                m_AvailableLockPeriodOptionsCSVSet = true;
            }
        }        
        public bool IsEnabledLockPeriodDropDown
        {
            get { return AvailableLockPeriodOptionsCSV != ""; }            
        }

        public int? DriveTaskPermissionLevelID
        {
            get { return m_DriveTaskPermissionLevelID; }
            set
            {
                m_DriveTaskPermissionLevelID = value;
                m_DriveTaskPermissionLevelIDSet = true;
            }
        }
        public bool IsEnableDriveConditionImporting
        {
            get { return m_IsEnableDriveConditionImporting; }
            set
            {
                m_IsEnableDriveConditionImporting = value;
                m_IsEnableDriveConditionImportingSet = true;
            }
        }

        public bool IsEnableDriveIntegration
        {
            get { return m_IsEnableDriveIntegration; }
            set
            {
                m_IsEnableDriveIntegration = value;
                m_IsEnableDriveIntegrationSet = true;
            }
        }


        public int? DriveConditionCategoryID
        {
            get { return m_DriveConditionCategoryID; }
            set
            {
                m_DriveConditionCategoryID = value;
                m_DriveConditionCategoryIDSet = true;
            }
        }


        public int? DriveDocTypeID
        {
            get { return m_DriveDocTypeID; }
            set
            {
                m_DriveDocTypeID = value;
                m_DriveDocTypeIDSet = true;
            }
        }


        public int DriveDueDateCalculationOffset
        {
            get { return m_DriveDueDateCalculationOffset; }
            set
            {
                m_DriveDueDateCalculationOffset = value;
                m_DriveDueDateCalculationOffsetSet = true;
            }
        }
        

        public string DriveDueDateCalculatedFromField
        {
            get { return m_DriveDueDateCalculatedFromField; }
            set
            {
                m_DriveDueDateCalculatedFromField = value;
                m_DriveDueDateCalculatedFromFieldSet = true;
            }
        }

        #region Flood Integration //OPM 12758
        public bool IsEnableFloodIntegration
        {
            get { return m_IsEnableFloodIntegration; }
            set
            {
                m_IsEnableFloodIntegration = value;
                m_IsEnableFloodIntegrationSet = true;
            }
        }

        public bool IsCCPmtRequiredForFloodIntegration
        {
            get { return m_IsCCPmtRequiredForFloodIntegration; }
            set
            {
                m_IsCCPmtRequiredForFloodIntegration = value;
                m_IsCCPmtRequiredForFloodIntegrationSet = true;
            }
        }

        public short FloodProviderID
        {
            get { return m_FloodProviderId; }
            set
            {
                m_FloodProviderId = value;
                m_FloodProviderIdSet = true;
            }
        }

        public int? FloodDocTypeID
        {
            get { return m_FloodDocTypeId; }
            set
            {
                m_FloodDocTypeId = value;
                m_FloodDocTypeIdSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a string uniquely identifying a lender using the flood integration.
        /// </summary>
        /// <value>A string uniquely identifying a lender using the flood integration.</value>
        public string FloodAccountId
        {
            get
            {
                return m_FloodAccountId;
            }
            set
            {
                m_FloodAccountId = value;
                m_FloodAccountIdSet = true;
            }
        }

        public Guid FloodResellerId
        {
            get { return m_FloodResellerId; }
            set
            {
                m_isFloodResellerIdSet = true;
                m_FloodResellerId = value;
            }
        }

        public int MaxWebServiceDailyCallVolume
        {
            get { return m_maxWebServiceDailyCallVolume; }
            set
            {
                m_maxWebServiceDailyCallVolumeSet = true;
                m_maxWebServiceDailyCallVolume = value;
            }
        }
        #endregion

        public bool IsAutoGenerateMersMin
        {
            get { return m_IsAutoGenerateMersMin; }
            set
            {
                m_IsAutoGenerateMersMin = value;
                m_IsAutoGenerateMersMinSet = true;
            }
        }

        

        public long MersCounter
        {
            get { return m_MersCounter; }
            set
            {
                m_MersCounter = value;
                m_MersCounterSet = true;
            }
        }
        public string MersOrganizationId
        {
            get { return m_MersOrganizationId; }
            set
            {
                m_MersOrganizationId = value;
                m_MersOrganizationIdSet = true;
            }
        }

        // OPM 145923 - Autonumber trades
        public bool IsAutoGenerateTradeNums
        {
            get { return m_IsAutoGenerateTradeNums; }
            set
            {
                m_IsAutoGenerateTradeNums = value;
                m_IsAutoGenerateTradeNumsSet = true;
            }
        }

        public long TradeCounter
        {
            get { return m_TradeCounter; }
            set
            {
                m_TradeCounter = value;
                m_TradeCounterSet = true;
            }
        }

        // OPM 145924 - Autonumber Pools
        public long PoolCounter
        {
            get { return m_PoolCounter; }
            set
            {
                m_PoolCounter = value;
                m_PoolCounterSet = true;
            }
        }
        
        public E_DocMagicDocumentSavingOption DocMagicDocumentSavingOption
        {
            get
            {
                return m_DocMagicDocumentSavingOption;
            }
            set
            {
                m_DocMagicDocumentSavingOption = value;
                m_DocMagicDocumentSavingOptionSet = true;
            }
        }

        public E_DocMagicDocumentSavingOption BackupDocumentSavingOption
        {
            get
            {
                if (this.m_DocMagicDocumentSavingOption != E_DocMagicDocumentSavingOption.DocumentCapture)
                {
                    // The backup saving option is only relevant when the primary option is Document Capture.
                    // Otherwise, just pass through the primary option.
                    return this.m_DocMagicDocumentSavingOption;
                }

                return m_BackupDocumentSavingOption;
            }

            set
            {
                m_BackupDocumentSavingOption = value;
                m_BackupDocumentSavingOptionSet = true;
            }
        }

        public BranchGroupSelection DocumentFrameworkCaptureEnabledBranchGroupT
        {
            get
            {
                return m_DocumentFrameworkCaptureEnabledBranchGroupT;
            }

            set
            {
                m_DocumentFrameworkCaptureEnabledBranchGroupT = value;
                m_DocumentFrameworkCaptureEnabledBranchGroupTSet = true;
            }
        }

        public bool DocMagicSplitESignedDocs
        {
            get
            {
                return m_DocMagicSplitESignedDocs;
            }
            set
            {
                m_DocMagicSplitESignedDocs = value;
                m_DocMagicSplitESignedDocsSet = true;
            }
        }

        public bool DocMagicSplitUnsignedDocs
        {
            get
            {
                return m_DocMagicSplitUnsignedDocs;
            }
            set
            {
                m_DocMagicSplitUnsignedDocs = value;
                m_DocMagicSplitUnsignedDocsSet = true;
            }
        }

        public Nullable<int> DocMagicDefaultDocTypeID
        {
            get
            {
                return m_DocMagicDefaultDocTypeID;
            }
            set
            {
                m_DocMagicDefaultDocTypeID = value;
                m_DocMagicDefaultDocTypeIDSet = true;
            }
        }

        public int? ESignedDocumentDocTypeId
        {
            get
            {
                return m_ESignedDocumentDocTypeId;
            }
            set
            {
                m_ESignedDocumentDocTypeId = value;
            }
        }

        public bool HasLPQExportEnabled
        {
            get
            {
                return false == string.IsNullOrEmpty(m_sLPQExportUrl);
            }
            
        }
        public string LPQBaseUrl
        {
            get
            {
                return m_sLPQExportUrl;
            }
            set
            {
                m_LPQBaseUrlSet = true;
                m_sLPQExportUrl = value;
            }
        }

        public bool DocMagicIsAllowuserCustomLogin
        {
            get { return m_DocMagicIsAllowuserCustomLogin; }
            set
            {
                m_DocMagicIsAllowuserCustomLogin = value;
                m_DocMagicIsAllowuserCustomLoginSet = true;
            }
        }

        public bool AllowInvestorDocMagicPlanCodes
        {
            get { return m_AllowInvestorDocMagicPlanCodes; }
            set
            {
                m_AllowInvestorDocMagicPlanCodes = value;
                m_AllowInvestorDocMagicPlanCodesSet = true;
            }
        }

        public bool IsOnlyAllowApprovedDocMagicPlanCodes 
        {
            get { return m_IsOnlyAllowApprovedDocMagicPlanCodes; }
            set
            {
                m_IsOnlyAllowApprovedDocMagicPlanCodes = value;
                m_IsOnlyAllowApprovedDocMagicPlanCodesSet = true;
            }
        }

        public bool IsCenlarEnabled
        {
            get { return isCenlarEnabled; }
            set
            {
                isCenlarEnabled = value;
                isCenlarEnabledSet = true;
            }
        }

        public bool EnableDsiPrintAndDeliverOption
        {
            get { return m_EnableDsiPrintAndDeliverOption; }
            set
            {
                m_EnableDsiPrintAndDeliverOption = value;
                m_EnableDsiPrintAndDeliverOptionSet = true;
            }
        }

        public bool IsEnableComplianceEagleIntegration
        {
            get
            {
                return this.isEnableComplianceEagleIntegration;
            }
            set
            {
                this.isEnableComplianceEagleIntegration = value;
                this.isEnableComplianceEagleIntegrationSet = true;
            }
        }

        public bool IsEnablePTMComplianceEagleAuditIndicator
        {
            get
            {
                return this.isEnablePTMComplianceEagleAuditIndicator;
            }
            set
            {
                this.isEnablePTMComplianceEagleAuditIndicator = value;
                this.isEnablePTMComplianceEagleAuditIndicatorSet = true;
            }
        }

        public string ComplianceEagleUserName
        {
            get
            {
                return this.complianceEagleUserName;
            }
            set
            {
                this.complianceEagleUserName = value;
                this.complianceEagleUserNameSet = true;
            }
        }

        public Sensitive<string> ComplianceEaglePassword
        {
            get
            {
                return this.lazyComplianceEaglePassword.Value;
            }

            set
            {
                if (!string.IsNullOrEmpty(value.Value))
                {
                    this.lazyComplianceEaglePassword = new Lazy<string>(() => value.Value);
                    this.complianceEaglePasswordBytesSet = true;
                }
            }
        }

        public string ComplianceEagleCompanyID
        {
            get
            {
                return this.complianceEagleCompanyID;
            }
            set
            {
                this.complianceEagleCompanyID = value;
                this.complianceEagleCompanyIDSet = true;
            }
        }

        public bool ComplianceEagleEnableMismo34
        {
            get
            {
                return this.complianceEagleEnableMismo34;
            }
            set
            {
                this.complianceEagleEnableMismo34 = value;
                this.complianceEagleEnableMismo34Set = true;
            }
        }

        public Guid? OCRVendorId
        {
            get
            {
                return this.m_OCRVendorId;
            }
            set
            {
                this.m_OCRVendorId = value;
            }
        }

        public string OCRUsername
        {
            get
            {
                return this.m_OCRUsername;
            }
            set
            {
                this.m_OCRUsername = value;
                this.m_OCRUsernameSet = true;
            }
        }

        public Sensitive<string> OCRPassword
        {
            get { return this.lazyOCRPassword.Value; }
            set
            {
                this.lazyOCRPassword = new Lazy<string>(() => value.Value);
                this.m_OCRPasswordByesSet = true;
            }
        }


        public bool IsOCREnabled
        {
            get
            {
                return m_OCRVendorId.HasValue;
            }
        }

        public string OCRVendorName
        {
            get
            {
                string name = string.Empty;

                if (this.IsOCREnabled)
                {
                    OCRDataSource dataSourvce = new OCRDataSource();
                    Vendor vendor = dataSourvce.GetVendors(vendorId: this.m_OCRVendorId.Value).FirstOrDefault();
                    if (vendor != null)
                    {
                        name = vendor.DisplayName;
                    }
                }

                return name;
            }
        }

        public bool OCRHasConfiguredAccount
        {
            get
            {
                return !string.IsNullOrEmpty(this.m_OCRUsername) && !string.IsNullOrEmpty(this.lazyOCRPassword.Value);
            }
        }

        public Sensitive<string> ComplianceEasePassword
        {
            get { return this.lazyComplianceEasePassword.Value; }
            set
            {
                this.lazyComplianceEasePassword = new Lazy<string>(() => value.Value);
                this.m_ComplianceEasePasswordSet = true;
            }
        }
        public string ComplianceEaseUserName
        {
            get { return m_ComplianceEaseUserName; }
            set
            {
                m_ComplianceEaseUserName = value;
                m_ComplianceEaseUserNameSet = true;
            }
        }

        public bool IsEnableComplianceEaseIntegration
        {
            get { return m_IsEnableComplianceEaseIntegration; }
            set
            {
                m_IsEnableComplianceEaseIntegration = value;
                m_IsEnableComplianceEaseIntegrationSet = true;
            }
        }

        public bool IsEnablePTMComplianceEaseIndicator
        {
            get { return m_IsEnablePTMComplianceEaseIndicator; }
            set
            {
                m_IsEnablePTMComplianceEaseIndicator = value;
                m_IsEnablePTMComplianceEaseIndicatorSet = true;
            }
        }
        public bool IsEnablePTMDocMagicSeamlessInterface
        {
            get { return m_IsEnablePTMDocMagicSeamlessInterface; }
            set
            {
                m_IsEnablePTMDocMagicSeamlessInterface = value;
                m_IsEnablePTMDocMagicSeamlessInterfaceSet = true;
            }
        }
        public bool IsEnablePTMDocMagicOnlineInterface
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).DisclosureOnlineInterface; }
        }
        public bool IsEnablePTMDocMagicPartnerBilling
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).DisclosureBilling; }
        }
        public string DocMagicCustomerId
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).AccountId; }
        }
        public Sensitive<string> DocMagicPassword
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).GetPassword(); }
        }
        public string DocMagicUserName
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).Login; }
        }
        public bool DocMagicIsEnableForProductionLoans
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).IsEnableForProductionLoans; }
        }
        public bool DocMagicIsEnableForTestLoans
        {
            get { return DocumentVendorBrokerSettings.GetDocMagicSettings(m_brokerID).IsEnableForTestLoans; }
        }
        
        public bool IsDisplayChoiceUfmipInLtvCalc
        {
            get { return m_IsDisplayChoiceUfmipInLtvCalc; }
            set 
            {
                m_IsDisplayChoiceUfmipInLtvCalc = value;
                m_IsDisplayChoiceUfmipInLtvCalcSet = true;
            }
        }
        public bool IsDocuTechEnabled
        {
            get { return m_isDocuTechEnabled; }
            set
            {
                m_isDocuTechEnabled = value;
                m_isDocuTechEnabledSet = true;
            }
        }
        public bool IsDocuTechStageEnabled
        {
            get { return m_IsDocuTechStageEnabled; }
            set
            {
                m_IsDocuTechStageEnabled = value;
                m_IsDocuTechStageEnabledSet = true;
            }
        }
        public bool UseFHATOTALProductionAccount
        {
            get
            {
                return m_UseFHATOTALProductionAccount;
            }
            set
            {
                m_UseFHATOTALProductionAccount = value;
                m_UseFHATOTALProductionAccountSet = true;
            }
        }

        public bool IsTotalAutoPopulateAUSResponse
        {
            get
            {
                return m_IsTotalAutoPopulateAUSResponse;
            }
            set
            {
                m_IsTotalAutoPopulateAUSResponse = value;
                m_IsTotalAutoPopulateAUSResponseSet = true;
            }
        }
        public bool IsRateOptionsWorseThanLowerRateShownWithBestPriceView
        {
            get
            {
                return m_IsRateOptionsWorseThanLowerRateShownWithBestPriceView; 
            }
            set
            {
                m_IsRateOptionsWorseThanLowerRateShownWithBestPriceView = value;
                m_IsRateOptionsWorseThanLowerRateShownWithBestPriceViewSet = true; 
            }
        }

        public bool IsShowRateOptionsWorseThanLowerRate
        {
            get
            {
                return m_IsShowRateOptionsWorseThanLowerRate;
            }
            set
            {
                m_IsShowRateOptionsWorseThanLowerRate = value;
                m_IsShowRateOptionsWorseThanLowerRateSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the value for the broker's Legal Entity Identifier (LEI).
        /// </summary>
        /// <value>
        /// The legal entity identifier for the broker.
        /// </value>
        public string LegalEntityIdentifier
        {
            get { return this.legalEntityIdentifier; }
            set
            {
                this.legalEntityIdentifierSet = true;
                this.legalEntityIdentifier = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the lender uses tasks in the TPO portal.
        /// </summary>
        /// <value>
        /// True if the lender uses tasks in the TPO portal, false otherwise.
        /// </value>
        /// <remarks>
        /// This value is dependent on the lender using the new task system.
        /// </remarks>
        public bool IsUseTasksInTpoPortal
        {
            get { return this.IsUseNewTaskSystem && this.isUseTasksInTpoPortal; }
            set
            {
                this.isUseTasksInTpoPortal = value;
                this.isUseTasksInTpoPortalSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the task pipeline appears
        /// in the TPO portal for brokers of the lender.
        /// </summary>
        /// <value>
        /// True if the task pipeline appears, false otherwise.
        /// </value>
        /// <remarks>
        /// This value is dependent on the lender using tasks in the TPO portal.
        /// </remarks>
        public bool IsShowPipelineOfTasksForAllLoansInTpoPortal
        {
            get { return this.IsUseTasksInTpoPortal && this.isShowPipelineOfTasksForAllLoansInTpoPortal; }
            set
            {
                this.isShowPipelineOfTasksForAllLoansInTpoPortal = value;
                this.isShowPipelineOfTasksForAllLoansInTpoPortalSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the condition pipeline appears
        /// in the TPO portal for brokers of the lender.
        /// </summary>
        /// <value>
        /// True if the condition pipeline appears, false otherwise.
        /// </value>
        public bool IsShowPipelineOfConditionsForAllLoansInTpoPortal
        {
            get { return this.isShowPipelineOfConditionsForAllLoansInTpoPortal; }
            set
            {
                this.isShowPipelineOfConditionsForAllLoansInTpoPortal = value;
                this.isShowPipelineOfConditionsForAllLoansInTpoPortalSet = true;
            }
        }

        /// <summary>
        /// Gets the counter used to provide unique "System IDs" to 
        /// rolodex entries.
        /// </summary>
        /// <value>
        /// The broker's "System ID" counter.
        /// </value>
        public int RolodexSystemIdCounter { get; private set; } = ConstApp.MinimumContactSystemId;

        public int BranchIdNumberCounter { get; private set; } = ConstApp.MinimumBranchIdNumber;

        public bool IsEnabledGfeFeeService
        {
            get
            {
                return m_IsEnabledGfeFeeService;
            }
            set
            {
                m_IsEnabledGfeFeeService = value;
                m_IsEnabledGfeFeeServiceSet = true;
            }
        }

        public E_BrokerBillingVersion BillingVersion
        {
            get { return m_BillingVersion; }
            set { m_BillingVersion = value; m_BillingVersionSet = true; }
        }

        public bool IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView
        {
            get { return m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView; }
            set { m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView = value; m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceViewSet = true; }
        }
        public bool IsQuickPricerEnable
        {
            get { return m_isQuickPricerEnable; }
            set { m_isQuickPricerEnable = value; m_isQuickPricerEnableSet = true; }
        }
        public Guid QuickPricerTemplateId
        {
            get { return m_quickPricerTemplateId; }
            set { m_quickPricerTemplateId = value; m_isQuickPricerTemplateIdSet = true; }
        }
        public string QuickPricerDisclaimerAtResult
        {
            get { return m_quickPricerDisclaimerAtResult; }
            set { m_quickPricerDisclaimerAtResult = value; m_isQuickPricerDisclaimerAtResultSet = true; }
        }
        public string QuickPricerNoResultMessage
        {
            get { return m_quickPricerNoResultMessage; }
            set { m_quickPricerNoResultMessage = value; m_isQuickPricerNoResultMessageSet = true; }
        }
        public Guid BrokerID 
        {
            get { return m_brokerID; }
        }

        public string PmlUserAutoLoginOption 
        {
            get { return m_pmlUserAutoLoginOption; }
            set { m_pmlUserAutoLoginOption = value; m_pmlUserAutoLoginOptionSet = true; }
        }
        //5/28/08 av 22180
        public bool IsBestPriceEnabled 
        {
            get { return m_isBestPriceEnabled; } 
            set { m_isBestPriceEnabledSet = true; m_isBestPriceEnabled = value; }
        }
        // 1/3/08 db - OPM 19525
        public bool DisplayPmlFeeIn100Format 
        {
            get { return m_displayPmlFeeIn100Format; }
            set 
            {
                m_displayPmlFeeIn100FormatSet = true; 
                m_displayPmlFeeIn100Format = value; 
            }
        }

        public Guid? DefaultLockPolicyID
        {
            get { return m_defaultLockPolicyID; }
            set
            {
                m_defaultLockPolicyIDSet = true;
                m_defaultLockPolicyID = value;
            }
        }

        //OPM 12711 7/15/08
        public bool ShowUnderwriterInPMLCertificate
        {
            get{return m_ShowUnderwriterInPMLCertificate;}
            set
            {
                m_ShowUnderwriterInPMLCertificateSet = true;
                m_ShowUnderwriterInPMLCertificate = value;
            }
        
        }

        // 11/21/2013 gf - opm 145015
        public bool ShowJuniorUnderwriterInPMLCertificate
        {
            get { return m_ShowJuniorUnderwriterInPMLCertificate; }
            set
            {
                m_ShowJuniorUnderwriterInPMLCertificateSet = true;
                m_ShowJuniorUnderwriterInPMLCertificate = value;
            }

        }

        //OPM 12711 7/15/08
        public bool ShowProcessorInPMLCertificate
        {
            get{return m_ShowProcessorInPMLCertificate;}
            set
            {
                m_ShowProcessorInPMLCertificateSet = true;
                m_ShowProcessorInPMLCertificate = value;
            }
        }

        // 11/21/2013 gf - opm 145015
        public bool ShowJuniorProcessorInPMLCertificate
        {
            get { return m_ShowJuniorProcessorInPMLCertificate; }
            set
            {
                m_ShowJuniorProcessorInPMLCertificateSet = true;
                m_ShowJuniorProcessorInPMLCertificate = value;
            }
        }

        // 1/9/08 db - OPM 19717
        /// <summary>
        /// Do not use.  Only keeping around for migration purposes.  Instead use the one for the lockpolicy.
        /// </summary>
        public bool UseRateSheetExpirationFeature
        {
            get { return m_isUsingRateSheetExpirationFeature; }
            set 
            {
                m_isUsingRateSheetExpirationFeatureSet = true; 
                m_isUsingRateSheetExpirationFeature = value;
            }
        }

        public int Status
        {
            get { return m_status; }
        }

        /// <summary>
        /// Gets or sets the suite type for the broker, indicating
        /// the purpose of the broker.
        /// </summary>
        public BrokerSuiteType SuiteType
        {
            get { return this.suiteType; }
            set
            {
                this.suiteType = value;
                this.suiteTypeSet = true;
            }
        }

        public bool IsEditLeadsInFullLoanEditor
        {
            get
            {
                return m_IsEditLeadsInFullLoanEditor;
            }
            set
            {
                m_IsEditLeadsInFullLoanEditor = value;
                m_IsEditLeadsInFullLoanEditorSet = true;
            }
        }

        public bool IsAdvancedFilterOptionsForPricingEnabled(AbstractUserPrincipal principal)
        {
            return this.productCodeFilterSettings.Value.IsEnabled(principal);
        }

        public bool IsSeamlessLpaEnabled
        {
            get
            {
                return this.seamlessLpaSettings.Value.SeamlessLpaEnabled;
            }
        }

        public bool IsNonSeamlessLpaEnabled(AbstractUserPrincipal principal)
        {
            return this.seamlessLpaSettings.Value.IsNonSeamlessFallbackEnabled(principal);
        }

        public bool IncludeLenderPaidOriginatorCompensationInMismo33Payload
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IncludeLenderPaidOriginatorCompensationInMismo33Payload\" value=\"true\" />");
            }
        }

        public bool IncludeLiabilitiesPaidBeforeClosingInDocMagicMismo33Payload
        {
            get { return IsStringExistedInTempOption("<option name=\"IncludeLiabilitiesPaidBeforeClosingInDocMagicMismo33Payload\"/>"); }
        }

        public bool AllowMovingFilesBackToLegacyWithMigratedArchives
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowMoveFilesBackToLegacyWithArchives\" value=\"true\" />");
            }
        }

        public bool CreateLeadFilesFromQuickpricerTemplate
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"createleadfilesfromquickpricertemplate\" value=\"true\" />"); 
            }
        }

        public bool ShowGetRecordingChargesTransferTaxesAndTitleQuoteButton
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ShowGetRecordingChargesTransferTaxesAndTitleQuoteButton\" value=\"true\" />");
            }
        }

        public bool AllowExcelFilesInEDocs
        {
            get
            {
                return !IsStringExistedInTempOption("<option name=\"BlockExcelFilesInEDocs\" value=\"true\" />"); 
            }
        }

        public bool NonUWCanExcludeLiabilities
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"nonuwcanexcludeliab_130395\" value=\"true\" />");
            }
        }

        public bool IsEDocDoctypeMergeEnabled
        {
            get
            {
                return IsStringExistedInTempOption( "<option name=\"edocmerge\" value=\"true\" />" );
            }
        }

        public bool AreAutomatedClosingCostPagesVisible
        {
            get { return CalculateclosingCostInPML || IsStringExistedInTempOption("<option name=\"automatedclosingcost\" value=\"true\" />"); }
        }

        public bool AutomatedClosingCost
        {
            get { return IsStringExistedInTempOption("<option name=\"automatedclosingcost\" value=\"true\" />"); }
        }

        public bool EnableLoanRepurchasePage
        {
            get { return IsStringExistedInTempOption("<option name=\"enableloanrepurchase\" value=\"true\" />"); }
        }

        public bool IsEnableSeamlessLP
        {
            get { return IsStringExistedInTempOption("<option name=\"enableseamlesslp\" value=\"true\" />"); }
        }

        public bool IsEnableHistoricalPricing
        {
            get { return IsStringExistedInTempOption("<option name=\"isenablehistoricalpricing\" value=\"true\" />"); }
        }

        public HistoricalPricingSettings HistoricalPricingSettings
        {
            get
            {
                return this.historicalPricingSettings.Value;
            }
        }

        public bool IsEnableHistoricalPricingAndUiEnhancements(AbstractUserPrincipal principal)
        {
            return this.HistoricalPricingSettings.IsEnabled(principal);
        }

        public bool UnconditionallyEnableHistoricalPricingAndUiEnhancements
        {
            get
            {
                return this.HistoricalPricingSettings.IsUnconditionallyEnabled;
            }
        }

        private PML3Settings PmlResults3PricingSettings
        {
            get
            {
                return this.pmlResults3PricingSettings.Value;
            }
        }

        public bool IsEnablePmlResults3Ui(AbstractUserPrincipal principal)
        {
            return this.PmlResults3PricingSettings.IsUnconditionallyEnabled || this.PmlResults3PricingSettings.IsEnabledForCurrentUser(principal);
        }

        public bool IsForcePML3
        {
            get
            {
                return this.PmlResults3PricingSettings.IsUnconditionallyEnabled;
            }
        }

        public bool EnablePml3TempOptionExists
        {
            get
            {
                return this.PmlResults3PricingSettings.EnablePml3TempOptionExists;
            }
        }

        public NonQmOpSettings NonQmOpSettings
        {
            get
            {
                return this.nonQmOpSettings.Value;
            }
        }

        public bool IsNonQmOpEnabled(AbstractUserPrincipal principal)
        {
            return this.NonQmOpSettings.IsEnabledForCurrentUser(principal);
        }

        public InternalPricerUiSettings InternalPricerUiSettings
        {
            get
            {
                return this.internalPricerUiSettings.Value;
            }
        }

        public bool IsEnableInternalPricerV2Ui(AbstractUserPrincipal principal)
        {
            return this.IsEnableHistoricalPricingAndUiEnhancements(principal)
                || this.InternalPricerUiSettings.IsEnabled(principal);
        }

        public bool UnconditionallyEnableInternalPricerV2Ui
        {
            get
            {
                return this.UnconditionallyEnableHistoricalPricingAndUiEnhancements
                        || this.InternalPricerUiSettings.IsUnconditionallyEnabled;
            }
        }

        public E_PercentBaseT DefaultFHAAnnualMIBaseType
        {
            get
            {
                if (IsStringExistedInTempOption("<option name=\"defaultfhaannualmibasetype\" value=\"loanamount\"/>"))
                {
                    return E_PercentBaseT.LoanAmount;
                }
                else
                {
                    return E_PercentBaseT.AverageOutstandingBalance;
                }
            }
        }

        public bool DisablePMLDuplicateSubmissionBlocker
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"disablepmlduplicatesubmissionblocker\" value=\"true\" />");
            }
        }

        /// <summary>
        /// This is just the brokerdb setting, use IsEnableHelocToggleInQP2
        /// </summary>
        private bool IsEnableHelocToggleOptionInQP2
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsEnableHelocToggleInQP2\" value=\"true\" />");
            }
        }
        /// <summary>
        /// Requires IsEnableHELOC as well as the broker temp option.
        /// </summary>
        public bool IsEnableHelocToggleInQP2
        {
            get
            {
                return IsEnableHELOC && IsEnableHelocToggleOptionInQP2;
            }
        }

        public bool IsEnableLienTToggleInQP2
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsEnableLienTToggleInQP2\" value=\"true\" />");
            }
        }

        public bool EnableStyleEditor
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableStyleEditor\" value=\"true\" />");
            }
        }

        public bool EnablePDDExport
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnablePDDExport\" value=\"true\" />");
            }
        }

        public bool AllowShowingZeroAmountHousingExp
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowShowingZeroAmountHousingExp\" value=\"true\" />");
            }
        }

        public bool AllowAnyXmlToValidateAsUcd
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowAnyXmlToValidateAsUcd\" value=\"true\" />");
            }
        }

        // This is replaced by ConstStage.AllowLoanPurposeToggleInQP2, since ideally it should be on for all.
        //public bool IsEnableLoanPurposeTRadioInQP2
        //{
        //    get
        //    {
        //        return IsStringExistedInTempOption("<option name=\"IsEnableLoanPurposeTRadioInQP2\" value=\"true\" />");
        //    }
        //}


        public bool IsPricingByRateMergeGroupEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsPricingByRateMergeGroupEnabled\" value=\"true\" />");
            }
        }

        public string LpqOrgId
        {
            get
            {
                Regex r = new Regex(@"<option name=""LQBInfo"" OrgId=""([A-Za-z0-9]+)"" LenderId=""([A-Za-z0-9]+)"" />");
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {

                    return "";
                }

                Match matches = r.Match(this.TempOptionXmlContent.Value);
                if (false == matches.Success || matches.Groups.Count != 3)
                {
                    return "";
                }

                return matches.Groups[1].Value;
            }
        }


        public string LpqLenderId
        {
            get
            {
                Regex r = new Regex(@"<option name=""LQBInfo"" OrgId=""([A-Za-z0-9]+)"" LenderId=""([A-Za-z0-9]+)"" />");
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {

                    return "";
                }

                Match matches = r.Match(this.TempOptionXmlContent.Value);
                if (false == matches.Success || matches.Groups.Count != 3)
                {
                    return "";
                }

                return matches.Groups[2].Value;
            }
        }

        public string LpqOrgCode
        {
            get
            {
                Regex r = new Regex(@"<option name=""LPQCodes"" OrgCode=""([A-Za-z0-9]+)"" LenderCode=""([A-Za-z0-9]+)"" />");
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {

                    return "";
                }

                Match matches = r.Match(this.TempOptionXmlContent.Value);
                if (false == matches.Success || matches.Groups.Count != 3)
                {
                    return "";
                }

                return matches.Groups[1].Value;
            }
        }


        public string LpqLenderCode
        {
            get
            {
                Regex r = new Regex(@"<option name=""LPQCodes"" OrgCode=""([A-Za-z0-9]+)"" LenderCode=""([A-Za-z0-9]+)"" />");
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {

                    return "";
                }

                Match matches = r.Match(this.TempOptionXmlContent.Value);
                if (false == matches.Success || matches.Groups.Count != 3)
                {
                    return "";
                }

                return matches.Groups[2].Value;
            }
        }

        public AutoLoginDocuTechInfo DocuTechInfo
        {
            get
            {
                Regex r = new Regex(@"<option name=""AutoDocutech"" PackageType=""([0-9]+)""  UserName=""([a-zA-Z0-9]+)"" Password=""([a-zA-Z0-9]+)"" />");
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {

                    return null;
                }

                Match matches = r.Match(this.TempOptionXmlContent.Value);
                if (false == matches.Success || matches.Groups.Count != 4)
                {
                    return null;
                }

                return new AutoLoginDocuTechInfo()
                {
                    PackageType = (E_DocumentsPackageType)int.Parse( matches.Groups[1].Value),
                    UserName = matches.Groups[2].Value,
                    Password = matches.Groups[3].Value
                };
            }
        }

        public UlddPhase3Settings GetUlddPhase3Settings
        {
            get
            {
                return UlddPhase3Settings.Create(this.TempOptionXmlContent.Value);
            }
        }

        //opm 16821
        public string DDLSpecialConfigXmlContent 
        {
            get { return m_DDLSpecialConfigXmlContent; }
            set 
            {
                if (string.IsNullOrEmpty(value) == false)
                {
                    // 4/25/2014 dd - Verify the string is valid XML format.
                    try
                    {
                        Tools.CreateXmlDoc(value);
                    }
                    catch (XmlException exc)
                    {
                        throw new XmlException("[" + value + "] is not valid XML.", exc);
                    }
                }
                m_DDLSpecialConfigXmlContentSet = true; 
                m_DDLSpecialConfigXmlContent = value; 
            }
        }

        // OPM 20141
        public string TimezoneForRsExpiration
        {
            get { return  m_timezoneForRsExpiration; }
            set 
            { 
                m_timezoneForRsExpiration = value;
                m_timezoneForRsExpirationSet = true;
            }
        }

        // OPM 24590
        public string PathToDataTrac 
        {
            get { return m_pathToDataTrac; }
            set 
            {
                m_pathToDataTrac = value; 
                m_IsPathToDataTracSet = true; 	
            }
        }

        // OPM 25798
        public string DataTracWebserviceURL
        {
            get { return m_dataTracWebserviceURL; }
            set 
            {
                m_dataTracWebserviceURL = value; 
                m_IsDataTracWebserviceURLSet = true; 	
            }
        }

        public bool RoundUpLpeFee 
        {
            get { return m_roundUpLpeFee; }
            set 
            {
                m_roundUpLpeFeeSet = true; 
                m_roundUpLpeFee = value; 
            }
        }

        public decimal RoundUpLpeFeeInterval
        {
            get { return  m_roundUpLpeFeeInterval; }
            set 
            { 
                m_roundUpLpeFeeIntervalSet = true;
                m_roundUpLpeFeeInterval = value;
            }
        }
        
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string Url
        {
            get { return m_url; }
            set { m_url = value; }
        }

        public CommonLib.Address Address 
        {
            get { return m_address; }
            set { m_address = value; }
        }

        public string Phone 
        {
            get { return m_phone; }
            set { m_phone = value; }
        }
        public string Fax 
        {
            get { return m_fax; }
            set { m_fax = value; }
        }
        public string LicenseNumber 
        {
            get { return m_licenseNumber; }
            set { m_licenseNumber = value; }
        }

        public string FthbCustomDefinition
        {
            get { return m_fthbCustomDefinition; }
            set { m_fthbCustomDefinition = value; }
        }

        public decimal PricePerSeat
        {
            get { return  m_pricePerSeat; }
            set
            {
                m_pricePerSeatSet = true;
                m_pricePerSeat = value;
            }
        }

        public string AccountManager 
        {
            get { return m_accountManager; }
            set { m_accountManager = value; }
        }

        public string CustomerCode 
        {
            get { return m_customerCode; }
            set { m_customerCode = value; }
        }

        public bool OptionTelemarketerCanOnlyAssignToManager 
        {
            get { return m_optionTelemarketerCanOnlyAssignToManager; }
            set 
            { 
                m_optionTelemarketerCanOnlyAssignToManager = value; 
                m_isOptionTelemarketerCanOnlyAssignToManagerSet = true;
            }
        }

        public bool OptionTelemarketerCanRunPrequal 
        {
            get { return m_optionTelemarketerCanRunPrequal; }
            set 
            { 
                m_optionTelemarketerCanRunPrequal = value; 
                m_isOptionTelemarketerCanRunPrequalSet = true;
            }
        }


        public bool HasLenderDefaultFeatures
        {
            get { return m_hasLenderDefaultFeatures; }
            set 
            { 
                m_hasLenderDefaultFeaturesSet = true;
                m_hasLenderDefaultFeatures = value; 
            }
        }


        //OPM 10585
        public bool PmlRequireEstResidualIncome
        {
            get { return m_pmlRequireEstResidualIncome; }
            set 
            { 
                m_pmlRequireEstResidualIncomeSet = true;
                m_pmlRequireEstResidualIncome = value; 
            }
        }

        //OPM 2703
        public bool RequireVerifyLicenseByState
        {
            get { return m_requireVerifyLicenseByState; }
            set 
            { 
                m_requireVerifyLicenseByStateSet = true;
                m_requireVerifyLicenseByState = value; 
            }
        }

        //OPM 9780
        public bool AllowPmlUserOrderNewCreditReport
        {
            get { return m_allowPmlUserOrderNewCreditReport; }
            set 
            { 
                m_allowPmlUserOrderNewCreditReportSet = true;
                m_allowPmlUserOrderNewCreditReport = value; 
            }
        }
    
        public bool IsLOAllowedToEditProcessorAssignedFile 
        {
            get { return m_isLOAllowedToEditProcessorAssignedFile; }
            set 
            {
                m_isLOAllowedToEditProcessorAssignedFileSet = true;
                m_isLOAllowedToEditProcessorAssignedFile = value;
            }
        }

        public bool IsOnlyAccountantCanModifyTrustAccount 
        {
            get { return m_isOnlyAccountantCanModifyTrustAccount; }
            set 
            {
                m_isOnlyAccountantCanModifyTrustAccountSet = true;
                m_isOnlyAccountantCanModifyTrustAccount = value;
            }
        }

        public bool IsOthersAllowedToEditUnderwriterAssignedFile
        {
            get { return  m_isOthersAllowedToEditUnderwriterAssignedFile; }
            set
            {
                m_isOthersAllowedToEditUnderwriterAssignedFileSet = true;
                m_isOthersAllowedToEditUnderwriterAssignedFile = value;
            }
        }

        public bool IsAllowViewLoanInEditorByDefault
        {
            get { return  m_isAllowViewLoanInEditorByDefault; }
            set
            {
                m_isAllowViewLoanInEditorByDefaultSet = true;
                m_isAllowViewLoanInEditorByDefault = value;
            }
        }

        public bool CanRunPricingEngineWithoutCreditReportByDefault
        {
            get { return  m_canRunPricingEngineWithoutCreditReportByDefault; }
            set
            {
                m_canRunPricingEngineWithoutCreditReportByDefaultSet = true;
                m_canRunPricingEngineWithoutCreditReportByDefault = value;
            }
        }

        public bool RestrictWritingRolodexByDefault
        {
            get { return  m_restrictWritingRolodexByDefault; }
            set
            {
                m_restrictWritingRolodexByDefaultSet = true;
                m_restrictWritingRolodexByDefault = value;
            }
        }

        public bool RestrictReadingRolodexByDefault
        {
            get { return  m_restrictReadingRolodexByDefault; }
            set
            {
                m_restrictReadingRolodexByDefaultSet = true;
                m_restrictReadingRolodexByDefault = value;
            }
        }

        public bool CannotDeleteLoanByDefault
        {
            get { return  m_cannotDeleteLoanByDefault; }
            set
            {
                m_cannotDeleteLoanByDefaultSet = true;
                m_cannotDeleteLoanByDefault = value;
            }
        }

        public bool CannotCreateLoanTemplateByDefault
        {
            get { return  m_cannotCreateLoanTemplateByDefault; }
            set
            {
                m_cannotCreateLoanTemplateByDefaultSet = true;
                m_cannotCreateLoanTemplateByDefault = value;
            }
        }


        public bool IsDuplicateLoanCreatedWAutonameBit
        {
            get { return m_isDuplicateLoanCreatedWAutonameBit; }
            set
            {
                m_isDuplicateLoanCreatedWAutonameBitSet = true;
                m_isDuplicateLoanCreatedWAutonameBit = value;
            }
        }

        public bool Is2ndLoanCreatedWAutonameBit
        {
            get { return m_is2ndLoanCreatedWAutonameBit; }
            set
            {
                m_is2ndLoanCreatedWAutonameBitSet = true;
                m_is2ndLoanCreatedWAutonameBit = value;
            }
        }

        public bool HasLONIntegration
        {
            get { return m_hasLONIntegration; }
            set
            {
                m_hasLONIntegrationSet = true;
                m_hasLONIntegration = value;
            }
        }

        /// <summary>
        /// Determines whether this broker supports LON integration 
        /// with the specified <paramref name="branchChannel"/>.
        /// </summary>
        /// <param name="branchChannel">
        /// The <see cref="E_BranchChannelT"/> for the loan to
        /// be exported.
        /// </param>
        /// <returns>
        /// True if the broker supports LON integration for the
        /// branch channel, false otherwise.
        /// </returns>
        public bool IsSupportedBranchChannelForLonIntegration(E_BranchChannelT branchChannel)
        {
            var regex = "<option name=\"EnabledLONBranchChannels\" value=\"(.*?)\" />";

            var match = this.IsRegexInTempOption(regex);

            if (match.Success && match.Groups.Count == 2)
            {
                return match.Groups[1].ToString().Contains(
                    branchChannel.ToString("G"), 
                    StringComparison.OrdinalIgnoreCase);
            }

            return true;
        }

        public bool IsBlankTemplateInvisibleForFileCreation
        {
            get { return m_isBlankTemplateInvisibleForFileCreation; }
            set
            {
                m_isBlankTemplateInvisibleForFileCreationSet = true;
                m_isBlankTemplateInvisibleForFileCreation = value;
            }
        }

        public bool IsRoundUpLpeRateTo1Eighth
        {
            get { return m_isRoundUpLpeRateTo1Eighth; }
            set
            {
                m_isRoundUpLpeRateTo1EighthSet = true;
                m_isRoundUpLpeRateTo1Eighth = value;
            }
        }

        public bool IsRateLockedAtSubmission
        {
            get { return m_isRateLockedAtSubmission; }
            set
            {
                if (this.CustomerCode == "PML0278") // Orion
                {
                    // OPM 315019. SAEs have indicated this setting has been toggled
                    // at this lender with nobody claiming responsibility.  We want 
                    // to track down who or what is changing it and when.
                    LogRateLockChangeEventForCase315019(m_isRateLockedAtSubmission, value);
                }

                m_isRateLockedAtSubmissionSet = true;
                m_isRateLockedAtSubmission = value;

                if(m_isRateLockedAtSubmission == false)
                {
                    // OPM 22212 - disable the ratesheet expiration feature if this permission is set to false
                    UseRateSheetExpirationFeature = false; // also update lock policies, see 22212 in the Save( CStoredProcedureExec spExec ) method
                }
            }
        }

        public bool HasManyUsers
        {
            get { return m_hasManyUsers; }
            set
            {
                m_hasManyUsersSet = true;
                m_hasManyUsers = value;
            }
        }

        public string EmailAddrSendFromForNotif
        {
            get { return  m_emailAddrSendFromForNotif; }
            set { m_emailAddrSendFromForNotif = value; }
        }

        public string NameSendFromForNotif
        {
            get { return  m_nameSendFromForNotif; }
            set { m_nameSendFromForNotif = value; }
        }

        public Guid LpePriceGroupIdDefault
        {
            get { return  m_lpePriceGroupIdDefault; }
            set
            {
                m_lpePriceGroupIdDefaultSet = true;
                m_lpePriceGroupIdDefault   = value;
            }
        }

        public String LpeSubmitAgreement
        {
            get { return  m_lpeSubmitAgreement; }
            set	{ m_lpeSubmitAgreement = value; }
        }

        public int LpeLockPeriodAdj
        {
            get { return m_lpeLockPeriodAdj; }
            set
            {
                m_lpeLockPeriodAdjSet = true;
                m_lpeLockPeriodAdj = value;
            }
        }

        public int NumberBadPasswordsAllowed
        {
            get { return m_numberBadPasswordsAllowed; }
            set
            {
                m_numberBadPasswordsAllowedSet = true;
                m_numberBadPasswordsAllowed = value;
            }
        }
        public E_BrokerNamingSchemeT BrokerNamingScheme
        {
            get
            {
                if (string.IsNullOrEmpty(NamingPattern))
                {
                    return E_BrokerNamingSchemeT.Sequential;
                }
                if (NamingPattern.IndexOf("{mers:") != -1 || NamingPattern.Contains("{mers"))
                {
                    return E_BrokerNamingSchemeT.MersNaming;
                }
                else
                {
                    return E_BrokerNamingSchemeT.Sequential;
                }
            }
        }

        public string NamingPattern 
        {
            get { return  m_namingPattern; }
            set { m_namingPattern = value; }
        }

        public long NamingCounter
        {
            get { return  m_namingCounter; }
            set
            {
                m_namingCounterSet = true;
                m_namingCounter = value;
            }
        }

        public string LeadNamingPattern
        {
            get { return m_leadNamingPattern; }
            set { m_leadNamingPattern = value; }
        }

        public long TestNamingCounter
        {
            get { return m_testNamingCounter; }
            set
            {
                m_testNamingCounterSet = true;
                m_testNamingCounter = value;
            }
        }

        public string ECOAAddressDefault 
        {
            get { return m_ecoaAddress; }
            set { m_ecoaAddress = value; }
        }

        public string[] FairLendingNoticeAddress 
        {
            get { return m_fairLendingNoticeAddress; }
            set { m_fairLendingNoticeAddress = value; }
        }

        public bool IsNew 
        {
            get { return m_isNew; }
        }

        public ChoiceList ConditionChoices
        {
            get { return  m_conditionChoices; }
            set { m_conditionChoices = value; }
        }

        public OptionSet FieldChoices
        {
            get { return  m_fieldChoices; }
            set { m_fieldChoices = value; }
        }

        public IEnumerable<string> DealerInvestors
        {
            get
            {
                var opts = m_fieldChoices["DealerInvestor"] as FieldOptions;
                if (opts == null)
                {
                    return new List<string>();
                }

                return opts.Options.OfType<string>();
            }
        }

        public IEnumerable<string> BranchDivisions
        {
            get
            {
                var opts = m_fieldChoices["BranchDivisions"] as FieldOptions;
                if (opts == null)
                {
                    return new List<string>();
                }

                return opts.Options.OfType<string>();
            }
        }

        public LeadSourceList LeadSources
        {
            // 7/16/2015 - EM OPM 220044 - I changed LeadSources' get method so that it always returns a fresh LeadSources object.
            // This was done to prevent caching issues, where updating LeadSources ended up changing the cached object even if no changes
            // had been made.
            get
            {
                try
                {
                    // 7/15/2005 kb - We attempt to convert.  
                    // If it fails, the object
                    // should be null.  See case 2226 for more info.
                    //
                    // 7/15/2005 kb - Per case 1724, we want to avoid
                    // throwing this error for new brokers.

                    if (m_LeadSourcesXmlContent.TrimWhitespaceAndBOM() != "")
                    {
                        return LeadSourceList.ToObject(m_LeadSourcesXmlContent);
                    }
                }
                catch (InvalidOperationException e)
                {
                    Tools.LogError("Failed to convert broker's (" + m_name + ") lead sources.", e);
                }

                // 7/15/2005 kb - We're null and we're not taking
                // it any longer.  This probably occurred because
                // the database data is null or empty string.
                // This should only happen if the broker has never
                // used lead sources before and their cell in the
                // row is empty.  Once saved through the source
                // editor, an empty set object should be written
                // back.

                return new LeadSourceList();

            }
            set 
            {
                m_LeadSourcesXmlContent = value.ToString(); 
            }
        }

        public PmlCompanyTierList PmlCompanyTiers
        {
            // 10/15/2015, ML, 220030
            // As with OPM 220044 and LeadSourceList, caching issues are causing 
            // multiple duplicate blank company tiers to be created. We'll return 
            // a fresh copy to prevent these issues.
            get
            {
                try
                {
                    if (!string.IsNullOrEmpty(m_pmlCompanyTiersXmlContent.TrimWhitespaceAndBOM()))
                    {
                        return PmlCompanyTierList.ToObject(m_pmlCompanyTiersXmlContent);
                    }
                }
                catch (InvalidOperationException e)
                {
                    Tools.LogError("Failed to convert broker's (" + m_name + ") originating company tiers.", e);
                }

                return new PmlCompanyTierList();
            }
            set 
            {
                m_pmlCompanyTiersXmlContent = value.ToString();
            }
        }

        public NotificationRules NotifRules
        {
            get { return  m_notifRules; }
            set { m_notifRules = value; }
        }

        public virtual Guid? DefaultWholesaleBranchId
        {
            get 
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultWholesaleBranchId; 
                }

                return null;
            }
            private set
            {
                this.defaultWholesaleBranchId = value;
                this.defaultWholesaleBranchName = null;
            }
        }

        public string DefaultWholesaleBranchName
        {
            get
            {
                if (!this.DefaultWholesaleBranchId.HasValue)
                {
                    return string.Empty;
                }

                if (this.defaultWholesaleBranchName == null)
                {
                    this.defaultWholesaleBranchName = BranchDB.RetrieveById(this.BrokerID, this.DefaultWholesaleBranchId.Value).Name;
                }

                return this.defaultWholesaleBranchName;
            }
        }

        public virtual Guid? DefaultWholesalePriceGroupId
        {
            get 
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultWholesalePriceGroupId;
                }

                return null;
            }
            private set
            {
                this.defaultWholesalePriceGroupId = value;
                this.defaultWholesalePriceGroupName = null;
            }
        }

        public string DefaultWholesalePriceGroupName
        {
            get
            {
                if (!this.DefaultWholesalePriceGroupId.HasValue)
                {
                    return string.Empty;
                }

                if (this.defaultWholesalePriceGroupName == null)
                {
                    this.defaultWholesalePriceGroupName = PriceGroup.RetrieveByID(this.DefaultWholesalePriceGroupId.Value, this.BrokerID).Name;
                }

                return this.defaultWholesalePriceGroupName;
            }
        }

        public Guid PmlLoanTemplateID 
        {
            get { return m_pmlLoanTemplateID; }
            set 
            { 
                m_isPmlLoanTemplateIDSet = true;
                m_pmlLoanTemplateID = value; 
            }
        }

        public Guid? RetailTPOLoanTemplateID
        {
            get { return m_retailTpoLoanTemplateID; }
            set
            {
                m_retailTpoLoanTemplateID = value;
                m_isRetailTpoLoanTemplateIDSet = true;
            }
        }

        public virtual Guid? DefaultMiniCorrBranchId
        {
            get
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultMiniCorrBranchId;
                }

                return null;
            }
            private set
            {
                this.defaultMiniCorrBranchId = value;
            }
        }

        public virtual Guid? DefaultMiniCorrPriceGroupId
        {
            get 
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultMiniCorrPriceGroupId;
                }

                return null;
            }
            private set
            {
                this.defaultMiniCorrPriceGroupId = value;
            }
        }

        public Guid MiniCorrLoanTemplateID
        {
            get { return m_miniCorrLoanTemplateID; }
            set
            {
                m_isMiniCorrLoanTemplateIDSet = true;
                m_miniCorrLoanTemplateID = value;
            }
        }

        /// <summary>
        /// Gets the PML template id based on the portal mode.
        /// </summary>
        /// <param name="portalMode">The portal mode.</param>
        /// <returns>The pml template id to use.</returns>
        public Guid GetPmlTemplateIdByPortalMode(E_PortalMode portalMode)
        {
            switch (portalMode)
            {
                case E_PortalMode.Broker:
                    return this.PmlLoanTemplateID;
                case E_PortalMode.MiniCorrespondent:
                    return this.MiniCorrLoanTemplateID;
                case E_PortalMode.Correspondent:
                    return this.CorrLoanTemplateID;
                case E_PortalMode.Retail:
                    return this.RetailTPOLoanTemplateID ?? Guid.Empty;
                default:
                    throw new UnhandledEnumException(portalMode);
            }
        }

        public virtual Guid? DefaultCorrBranchId
        {
            get 
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultCorrBranchId;
                }

                return null;
            }
            private set
            {
                this.defaultCorrBranchId = value;
            }
        }

        public virtual Guid? DefaultCorrPriceGroupId
        {
            get 
            {
                if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                {
                    return this.defaultCorrPriceGroupId;
                }

                return null;
            }
            private set
            {
                this.defaultCorrPriceGroupId = value;
            }
        }

        public Guid CorrLoanTemplateID
        {
            get { return m_corrLoanTemplateID; }
            set
            {
                m_isCorrLoanTemplateIDSet = true;
                m_corrLoanTemplateID = value;
            }
        }

        public Guid PmlSiteID 
        {
            get { return m_pmlSiteID; }
        }

        public Guid NHCKey 
        {
            get { return m_nhcKey; }
            set 
            { 
                m_nhcKeySet = true;
                m_nhcKey = value; 
            }
        }
        
        public string PdfCustomFormAssignmentXmlContent 
        {
            get { return m_pdfCustomFormAssignmentXmlContent; }
            set { m_pdfCustomFormAssignmentXmlContent = value; }
        }

        public string EmailAddressesForSystemChangeNotif
        {
            get { return  m_emailAddressesForSystemChangeNotif; }
            set { m_emailAddressesForSystemChangeNotif = value; }
        }

        public string Notes
        {
            get { return  m_notes; }
            set { m_notes = value; }
        }

        public string CreditMornetPlusUserID 
        {
            get { return m_creditMornetPlusUserID; }
            set { m_creditMornetPlusUserID = value; }
        }
        public Sensitive<string> CreditMornetPlusPassword 
        {
            get { return this.lazyCreditMornetPlusPassword.Value ?? string.Empty; }
            set { this.lazyCreditMornetPlusPassword = new Lazy<string>(() => value.Value); this.creditMornetPlusPasswordSet = true; }
        }
        public E_ShowCredcoOptionT ShowCredcoOptionT 
        {
            get { return m_showCredcoOptionT; }
            set 
            { 
                m_showCredcoOptionT = value;
                m_showCredcoOptionTSet = true;
            }
        }

        public E_EDocsEnabledStatusT EDocsEnabledStatusT
        {
            get 
            {
                if (IsEDocsEnabled == false) // OPM 50144
                {
                    return E_EDocsEnabledStatusT.No;
                }
                else
                {
                    return m_EDocsEnabledStatusT;
                }
            }
            set
            {
                m_EDocsEnabledStatusT = value;
                m_EDocsEnabledStatusTSet = true;
            }
        }

        public bool IsAEAsOfficialLoanOfficer 
        {
            get { return m_isAEAsOfficialLoanOfficer; }
            set 
            {
                m_isAEAsOfficialLoanOfficer = value;
                m_isAEAsOfficialLoanOfficerSet = true;
            }
        }
        public Sensitive<string> TempOptionXmlContent 
        {
            get { return this.lazyTempOptionXmlContent.Value; }
            set
            {
                // reset cache for TempOptionXml
                this.TempOptionDictionary.Clear();
                this.lazyTempOptionXmlContent = new Lazy<string>(() => value.Value);
                this.isTempOptionXmlContentSet = true;
            }
        }
        public bool IsAsk3rdPartyUwResultInPml 
        {
            get { return true; } // removed bit opm 132564 
            //set  // removed opm 132564
            //{
            //    m_isAsk3rdPartyUwResultInPmlSet = true;
            //    m_isAsk3rdPartyUwResultInPml = value;
            //}
        }

        /// <summary>
        /// Get the actual broker id when run pricing using snapshot import from LPE production server.
        /// </summary>
        public Guid ActualPricingBrokerId { get; set; }

        public string BlockedCRAsXmlContent 
        {
            get { return m_blockedCRAsXmlContent; }
            set 
            { 
                m_blockedCRAsXmlContent = value; 
                m_blockedCRAsXmlContentSet = true;
            }
        }

        public bool IsAutoGenerateLiabilityPayOffConditions
        {
            get { return m_isAutoGenerateLiabilityPayOffConditions; }
            set 
            {
                m_isAutoGenerateLiabilityPayOffConditions = value;
                m_isAutoGenerateLiabilityPayOffConditionsSet = true;
            }
        }

        private bool? x_isTrackModifiedFile = null;
        public bool IsTrackModifiedFile
        {
            get
            {
                // 6/10/2014 dd - Track Modified is now compute by App Code in BROKER_WEBSERVICE_APP_CODE table.
                if (this.BrokerID == Guid.Empty)
                {
                    return false;
                }

                if (x_isTrackModifiedFile.HasValue == false)
                {
                    x_isTrackModifiedFile = false;

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@BrokerId", this.BrokerID)
                    };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "BROKER_WEBSERVICE_APP_CODE_HasCode", parameters))
                    {
                        if (reader.Read())
                        {
                            x_isTrackModifiedFile = true;
                        }
                    }
                }

                return x_isTrackModifiedFile.Value;
            }
        }

        private ICollection<string> linkedLoanUpdateFields;

        /// <summary>
        /// Gets a collection of all fields to update when updating a linked loan.
        /// </summary>
        public ICollection<string> LinkedLoanUpdateFields
        {
            get
            {
                if (this.linkedLoanUpdateFields == null)
                {
                    this.linkedLoanUpdateFields = new System.Collections.ObjectModel.ReadOnlyCollection<string>(PageDataUtilities.GetSettableFieldsFromString(this.linkedLoanUpdateFieldsString));
                }

                return this.linkedLoanUpdateFields;
            }
        }

        private bool linkedLoanUpdateFieldsStringSet;
        private string linkedLoanUpdateFieldsString;

        /// <summary>
        /// Gets or sets a the string that is parsed to compute <see cref="LinkedLoanUpdateFields"/>
        /// </summary>
        public string LinkedLoanUpdateFieldsString
        {
            get
            {
                return this.linkedLoanUpdateFieldsString;
            }

            set
            {
                this.linkedLoanUpdateFieldsString = value;
                this.linkedLoanUpdateFieldsStringSet = true;
                this.linkedLoanUpdateFields = null;
            }
        }

        public bool IsAllowExternalUserToCreateNewLoan
        {
            get { return m_isAllowExternalUserToCreateNewLoan; }
            set
            {
                m_isAllowExternalUserToCreateNewLoan = value;
                m_isAllowExternalUserToCreateNewLoanSet = true;
            }
        }

        public bool IsShowPriceGroupInEmbeddedPml
        {
            get { return m_isShowPriceGroupInEmbeddedPml; }
            set
            {
                m_isShowPriceGroupInEmbeddedPml = value;
                m_isShowPriceGroupInEmbeddedPmlSet = true;
            }
        }

        public int PmlSubmitRLockDefaultT
        {
            get { return m_pmlSubmitRLockDefaultT; }
            set 
            {
                m_pmlSubmitRLockDefaultT = value;
                m_pmlSubmitRLockDefaultTSet = true;
            }
        }

        public bool IsLpeManualSubmissionAllowed 
        {
            get { return m_isLpeManualSubmissionAllowed; }
            set
            {
                m_isLpeManualSubmissionAllowed = value;
                m_isLpeManualSubmissionAllowedSet = true;
            }
        }

        
        //public bool DO_NOT_USE_CanLpNameBeManuallyModified // removed opm 129291

        public bool IsAllPrepaymentPenaltyAllowed
        {
            get { return m_isAllPrepaymentPenaltyAllowed; }
            set
            {
                m_isAllPrepaymentPenaltyAllowed = value;
                m_isAllPrepaymentPenaltyAllowedSet = true;
            }
        }

        public bool IsLpeDisqualificationEnabled
        {
            get { return m_isLpeDisqualificationEnabled; }
            set
            {
                m_isLpeDisqualificationEnabled = value;
                m_isLpeDisqualificationEnabledSet = true;
            }
        }

        public bool IsPmlPointImportAllowed
        {
            get { return m_isPmlPointImportAllowed; }
            set
            {
                m_isPmlPointImportAllowed = value;
                m_isPmlPointImportAllowedSet = true;
            }
        }
        public LicenseInfoList LicenseInformationList
        {
            get { return m_licenseInformationList; }
            set { m_licenseInformationList = value; }
        }
        private DateTime GetPacificTimeFromLenderTimezone(DateTime dt)
        {
            try
            {
                switch(m_timezoneForRsExpiration)
                {
                    case "EST":
                        if(dt.TimeOfDay < TimeSpan.FromHours(3))
                            return dt.Date;
                        else
                            return dt.AddHours(-3);
                    case "CST":
                        if(dt.TimeOfDay < TimeSpan.FromHours(2))
                            return dt.Date;
                        else
                            return dt.AddHours(-2);
                    case "MST":
                        if(m_address.State.ToUpper() == "AZ" && TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now))
                        {
                            return dt;
                        }
                        if(dt.TimeOfDay < TimeSpan.FromHours(1))
                            return dt.Date;
                        else
                            return dt.AddHours(-1);
                    default:
                        return dt; //assume PST
                }
            }
            catch
            {
                return dt;
            }
        }

        // datetime is assumed to be in Pacific Time
        private DateTime GetLenderTimeFromPacificTime(DateTime dt)
        {
            try
            {
                switch(m_timezoneForRsExpiration)
                {
                    case "EST":
                        return dt.AddHours(3);
                    case "CST":
                        return dt.AddHours(2);
                    case "MST":
                        if(m_address.State.ToUpper() == "AZ" && TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now))
                            return dt;
                        else
                            return dt.AddHours(1);
                    default:
                        return dt; //assume PST
                }
            }
            catch
            {
                return dt;
            }
        }

        public DateTime LpeLockDeskWorkHourEndTimeInLenderTimezone
        {
            get { return m_lpeLockDeskWorkHourEndTime; }
            set 
            {
                m_lpeLockDeskWorkHourEndTime = value;
                m_lpeLockDeskWorkHourEndTimeSet = true;
            }
        }

        // Returned in Pacific Time
        public DateTime LpeLockDeskWorkHourEndTime 
        {
            get { return GetPacificTimeFromLenderTimezone(m_lpeLockDeskWorkHourEndTime); }
            set 
            {
                m_lpeLockDeskWorkHourEndTime = GetLenderTimeFromPacificTime(value);
                m_lpeLockDeskWorkHourEndTimeSet = true;
            }
        }
        public bool HasLpeLockDeskWorkHourEndTime 
        {
            get { return m_lpeLockDeskWorkHourEndTime != DateTime.MinValue && m_lpeLockDeskWorkHourEndTime.CompareTo(new DateTime(1901, 1, 2)) >0; }
        }

        // Returned in Pacific Time
        public DateTime LpeLockDeskWorkHourStartTime 
        {
            get { return GetPacificTimeFromLenderTimezone(m_lpeLockDeskWorkHourStartTime); }
            set 
            {
                m_lpeLockDeskWorkHourStartTime = GetLenderTimeFromPacificTime(value);
                m_lpeLockDeskWorkHourStartTimeSet = true;
            }
        }

        public DateTime LpeLockDeskWorkHourStartTimeInLenderTimezone
        {
            get { return m_lpeLockDeskWorkHourStartTime; }
            set 
            {
                m_lpeLockDeskWorkHourStartTime = value;
                m_lpeLockDeskWorkHourStartTimeSet = true;
            }
        }

        public bool HasLpeLockDeskWorkHourStartTime 
        {
            get { return m_lpeLockDeskWorkHourStartTime != DateTime.MinValue && m_lpeLockDeskWorkHourStartTime.CompareTo(new DateTime(1901, 1, 2)) >0; }
        }

        public int LpeMinutesNeededToLockLoan 
        {
            get { return m_lpeMinutesNeededToLockLoan; }
            set 
            {
                m_lpeMinutesNeededToLockLoan = value;
                m_lpeMinutesNeededToLockLoanSet = true;
            }
        }
        public bool LpeIsEnforceLockDeskHourForNormalUser 
        {
            get { return m_lpeIsEnforceLockDeskHourForNormalUser; }
            set 
            { 
                m_lpeIsEnforceLockDeskHourForNormalUser = value;
                m_lpeIsEnforceLockDeskHourForNormalUserSet = true;
            }
        }

        // OPM 19782
        public bool IsOptionARMUsedInPml
        {
            get { return m_isOptionARMUsedInPml; }
            set 
            { 
                m_isOptionARMUsedInPml = value;
                m_isOptionARMUsedInPmlSet = true;
            }
        }

        // OPM 19783
        public bool IsPmlSubmissionAllowed 
        {
            get { return m_isPmlSubmissionAllowed; }
            set 
            {
                m_isPmlSubmissionAllowed = value;
                m_isPmlSubmissionAllowedSet = true;
            
                if(m_isPmlSubmissionAllowed == false)
                {
                    // OPM 22212 - disable the ratesheet expiration feature if this permission is set to false
                    UseRateSheetExpirationFeature = false; // also update lock policies, see 22212 in the Save( CStoredProcedureExec spExec ) method
                }
            }
        }

        // OPM 18430
        public bool IsStandAloneSecondAllowedInPml
        {
            get { return m_isStandAloneSecondAllowedInPml; }
            set 
            {
                m_isStandAloneSecondAllowedInPml = value;
                m_isStandAloneSecondAllowedInPmlSet = true;
            }
        }

        // OPM 187671
        public bool IsHelocsAllowedInPml
        {
            get { return m_IsHelocsAllowedInPml && IsEnableHELOC; }
            set
            {
                m_IsHelocsAllowedInPml = value;
                m_IsHelocsAllowedInPmlSet = true;
            }
        }

        // OPM 19784
        public bool Is8020ComboAllowedInPml
        {
            get { return m_is8020ComboAllowedInPml; }
            set 
            {
                m_is8020ComboAllowedInPml = value;
                m_is8020ComboAllowedInPmlSet = true;
            }
        }

        // OPM 24590
        public bool IsDataTracIntegrationEnabled 
        {
            get { return m_IsDataTracIntegrationEnabled; }
            set 
            {
                m_IsDataTracIntegrationEnabled = value; 
                m_IsDataTracIntegrationEnabledSet = true; 
            }
        }

        // OPM 34444
        public bool IsExportPricingInfoTo3rdParty
        {
            get { return m_IsExportPricingInfoTo3rdParty; }
            set
            {
                m_IsExportPricingInfoTo3rdParty = value;
                m_IsExportPricingInfoTo3rdPartySet = true;
            }
        }
        
        public bool IsAlwaysUseBestPrice //opm 24214
        {
            get { return m_IsAlwaysUseBestPrice; }
            set { m_IsAlwaysUseBestPrice = value; m_IsAlwaysUseBestPriceSet = true; }
        }

        //opm 33530 fs 07/29/09
        public bool IsPricingMultipleAppsSupported
        {
            get { return m_IsPricingMultipleAppsSupported; }
            set
            {
                m_IsPricingMultipleAppsSupported = value;
                m_IsPricingMultipleAppsSupportedSet = true;
            }
        }

        //opm 33208 fs 08/21/09
        public bool IsaBEmailRequiredInPml
        {
            get { return m_IsaBEmailRequiredInPml; }
            set
            {
                m_IsaBEmailRequiredInPml = value;
                m_IsaBEmailRequiredInPmlSet = true;
            }
        }

        // opm 34274
        public bool IsForceChangePasswordForPml
        {
            get { return m_IsForceChangePasswordForPml; }
            set
            {
                m_IsForceChangePasswordForPml = value;
                m_IsForceChangePasswordForPmlSet = true;
            }
        }

        //opm 34381 fs 08/31/09
        public bool IsAllowSharedPipeline
        {
            get { return m_IsAllowSharedPipeline; }
            set
            {
                m_IsAllowSharedPipeline = value;
                m_IsAllowSharedPipelineSet = true;
            }
        }

        //opm 42579 fs 11/17/09
        public bool IsIncomeAssetsRequiredFhaStreamline
        {
            get { return m_IsIncomeAssetsRequiredFhaStreamline; }
            set
            {
                m_IsIncomeAssetsRequiredFhaStreamline = value;
                m_IsIncomeAssetsRequiredFhaStreamlineSet = true;
            }
        }

        //opm 45012 fs 01/21/10
        public bool IsEncompassIntegrationEnabled
        {
            get { return m_IsEncompassIntegrationEnabled; }
            set
            {
                m_IsEncompassIntegrationEnabled = value;
                m_IsEncompassIntegrationEnabledSet = true;
            }
        }

        [LqbInputModel(maxLength: 10)]
        public string FhaLenderId
        {
            get { return m_FhaLenderId; }
            set
            {
                string newLenderId = value;
                if (newLenderId != null && UseFHATOTALProductionAccount && newLenderId.StartsWith("9"))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot use FHA Connection Production account when FHA LenderID begins with 9. Value: " + newLenderId.ToString());
                }

                m_FhaLenderId = newLenderId;
                m_FhaLenderIdSet = true;
            }
        }

        public bool IsHideFhaLenderIdInTotalScorecard
        {
            get { return m_IsHideFhaLenderIdInTotalScorecard; }
            set
            {
                m_IsHideFhaLenderIdInTotalScorecard = value;
                m_IsHideFhaLenderIdInTotalScorecardSet = true;
            }
        }

        public bool IsEnableSettingInterviewDateForConsumerPortalSubmission
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableSettingInterviewDateForConsumerPortalSubmission\" value=\"true\" />");
            }
        }

        public bool IsEnforceVersionCheck
        {
            get
            {
                // 3/16/2011 dd - By default we want to turn this feature off for all lender. We only turn
                // on per lender request. 
                // When this feature enable it will perform following:
                //     1) Give warning about other user is currently working on loan
                //     2) Throw exception when save if version conflict is detect.

                return IsStringExistedInTempOption("<option name=\"IsEnforceVersionCheck\" value=\"true\" />");
                //if (b == true)
                //{
                //}
                //return true; // 1/26/2011 dd - Temporary disable version check feature.
                //return IsStringExistedInTempOption("<option name=\"IsSkipEnforceVersionCheck\" value=\"true\" />");
            }
        }

        public bool IsAlwaysDisplayExactParRateOption
        {
            get
            {
                return m_IsAlwaysDisplayExactParRateOption;
            }
            set
            {
                m_IsAlwaysDisplayExactParRateOption = value;
                m_IsAlwaysDisplayExactParRateOptionSet = true;
            }
        }

        public bool IsAllowLandsafeDirect 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"landsafe\" value=\"true\" />");
            }
        }
        public bool IsAllowKrollFactualDataDirect 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"factualdata\" value=\"true\" />");
            }
        }


        public bool AllowCustomPdf 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"custompdf\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Indicates whether fees from First American will import as borrower and seller fees separately (true), or only as borrower fees (false).<para/>
        /// </summary>
        public bool ImportFirstAmericanBorrowerAndSellerFees
        {
            get { return IsStringExistedInTempOption("<option name=\"ImportFirstAmericanBorrowerAndSellerFees\""); }
        }

        /// <summary>
        /// Determines if the input state is a state that the FATCO interface will request title and recording fees for but not closing/escrow fees.
        /// The state list is set in the TitleInterfaceTitleRecordingOnly temp option.
        /// </summary>
        /// <param name="state">The subject property state to compare.</param>
        /// <returns>True if the input state is one of the indicated states, false otherwise.</returns>
        public bool TitleInterfaceTitleRecordingOnly(string state)
        {
            var regex = "<option name=\"TitleInterfaceTitleRecordingOnly\" value=\"(.*?)\" />";
            var match = this.IsRegexInTempOption(regex);

            if (match.Success && match.Groups.Count == 2)
            {
                return match.Groups[1].ToString().Split(';').Contains(state, StringComparer.OrdinalIgnoreCase);
            }

            return false;
        }

        /// <summary>
        /// Gets the list of business channels for which the Title interface should pull title quotes when the pricing engine is run.
        /// </summary>
        public List<E_BranchChannelT> TitleInterfaceEnabledBusinessChannels
        {
            get
            {
                if (IsStringExistedInTempOption("<option name=\"TitleInterfaceEnabledBusinessChannels\""))
                {
                    Match match = Regex.Match(this.TempOptionXmlContent.Value, "<option name=\"TitleInterfaceEnabledBusinessChannels\" value=\"([0-9;]+)\" />", RegexOptions.IgnoreCase);

                    if (match.Success && (match.Groups.Count > 1))
                    {
                        try
                        {
                            List<E_BranchChannelT> channels = new List<E_BranchChannelT>();

                            foreach (string channel in match.Groups[1].Value.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries))
                            {
                                channels.Add((E_BranchChannelT)Enum.Parse(typeof(E_BranchChannelT), channel));
                            }

                            return channels;
                        }
                        catch (ArgumentException) { }
                    }
                }

                return new List<E_BranchChannelT> { E_BranchChannelT.Blank, E_BranchChannelT.Broker, E_BranchChannelT.Correspondent, E_BranchChannelT.Retail, E_BranchChannelT.Wholesale };
            }
        }

        /// <summary>
        /// Gets the list of loan purposes for which the Title interface should pull title quotes when the pricing engine is run.
        /// </summary>
        public List<E_sLPurposeT> TitleInterfaceEnabledLoanPurpose
        {
            get
            {
                if (IsStringExistedInTempOption("<option name=\"TitleInterfaceEnabledLoanPurpose\""))
                {
                    Match match = Regex.Match(this.TempOptionXmlContent.Value, "<option name=\"TitleInterfaceEnabledLoanPurpose\" value=\"([0-9;]+)\" />", RegexOptions.IgnoreCase);

                    if (match.Success && (match.Groups.Count > 1))
                    {
                        try
                        {
                            List<E_sLPurposeT> purposes = new List<E_sLPurposeT>();

                            foreach (string purpose in match.Groups[1].Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                purposes.Add((E_sLPurposeT)Enum.Parse(typeof(E_sLPurposeT), purpose));
                            }
                            return purposes;
                        }
                        catch (ArgumentException e) 
                        {
                            Tools.LogError("Failed to parse E_sLPurposeT value from purpose list: \"" + match.Groups[1].Value + "\"", e);
                        }
                    }
                }
                
                // get all possible values of loan purpose enum
                List<E_sLPurposeT> possibleValues = new List<E_sLPurposeT>();
                foreach (var value in Enum.GetValues(typeof(E_sLPurposeT)))
                    possibleValues.Add((E_sLPurposeT)value);

                return possibleValues;
            }
        }

        /// <summary>
        /// Indicates whether the Title Interface is enabled by temp options
        /// </summary>
        /// <param name="channel">Business channels</param>
        /// <param name="purpose">Loan Purpose</param>
        /// <returns>Whether title interface is enabled with given options</returns>
        public bool IsTitleInterfaceEnabled(E_BranchChannelT channel, E_sLPurposeT purpose)
        {
            return
                this.TitleInterfaceEnabledBusinessChannels != null &&
                this.TitleInterfaceEnabledBusinessChannels.Contains(channel) &&
                this.TitleInterfaceEnabledLoanPurpose != null &&
                this.TitleInterfaceEnabledLoanPurpose.Contains(purpose);
        }

        /// <summary>
        /// Gets a value indicating whether First American Title Interface Purpose of Transaction (POT) code should be 61 for refinances.
        /// </summary>
        /// <returns>Returns true or false response depending on whether POT code 61 should be used for refinances.</returns>
        public bool UsePOT61ToPullFirstAmericanLocalRefinanceRates
        {
            get { return this.IsStringExistedInTempOption("<option name=\"UsePOT61ToPullFirstAmericanLocalRefinanceRates\" value=\"true\" />"); }
        }

        private bool isKivaIntegrationEnabled;
        private bool isKivaIntegrationEnabledSet = false;

        /// <summary>
        /// Gets a value indicating whether the Kiva integration to export data to the lender's core system is enabled.
        /// </summary>
        public bool IsKivaIntegrationEnabled
        {
            get
            {
                return this.isKivaIntegrationEnabled;
            }

            set
            {
                this.isKivaIntegrationEnabled = value;
                this.isKivaIntegrationEnabledSet = true;
            }
        }

        /// <summary>
        /// Calculates whether the Symitar integration is enabled for a particular loan file type.
        /// </summary>
        /// <param name="loanType">The loan type to check.</param>
        /// <returns><see langword="true"/> if the lender is a credit union and has a valid Symitar configuration, <see langword="false"/> otherwise.</returns>
        public bool IsSymitarIntegrationEnabled(E_sLoanFileT loanType)
        {
            return this.GetEnabledSymitarLenderConfig(loanType) != null;
        }

        /// <summary>
        /// Retrieves the lender's Symitar configuration, or null if no valid configuration is found.
        /// </summary>
        /// <param name="loanType">The loan type to which the configuration applies.</param>
        /// <returns>The configuration or null if no valid configuration is found.</returns>
        public LendersOffice.Integration.Symitar.LenderConfiguration GetEnabledSymitarLenderConfig(E_sLoanFileT loanType)
        {
            if (!this.IsCreditUnion || (loanType != E_sLoanFileT.Loan && loanType != E_sLoanFileT.Test))
            {
                return null; // Per Kevin, we aren't going to provide anything for the other file types
            }

            string configName = loanType == E_sLoanFileT.Loan ? "SymitarIntegration" : "SymitarTestIntegration";
            return ParseTemporaryOptionForSymitarConfig(this.TempOptionXmlContent.Value, configName, this.CustomerCode);
        }

        /// <summary>
        /// Parses a lender configuration from the temporary options.
        /// </summary>
        /// <param name="nameValue">The value of the name attribute, used to identify this entry.</param>
        /// <returns>The lender configuration specified, or null if no valid config was found.</returns>
        public static LendersOffice.Integration.Symitar.LenderConfiguration ParseTemporaryOptionForSymitarConfig(string temporaryOptionContent, string nameValue, string customerCode)
        {
            int startIndex = temporaryOptionContent.IndexOf("<option name=\"" + nameValue + "\"", StringComparison.OrdinalIgnoreCase);
            if (startIndex < 0)
            {
                return null;
            }

            string closeString = "/>";
            int closeStartIndex = temporaryOptionContent.IndexOf(closeString, startIndex, StringComparison.OrdinalIgnoreCase); // yes, this breaks more easily than I'd like, but deadlines.
            if (closeStartIndex < 0)
            {
                Tools.LogWarning(customerCode + " SymitarConfig: Unable to find self-closing XML tag (\"" + closeString + "\") to //option[@name=\"" + nameValue + "\"] config in temporary options.");
                return null;
            }

            int tagLength = closeStartIndex + closeString.Length - startIndex;
            Dictionary<string, string> attributeDictionary;
            try
            {
                var tag = XElement.Parse(temporaryOptionContent.Substring(startIndex, tagLength));
                attributeDictionary = tag.Attributes()
                        .Where(attribute => !string.Equals(attribute.Name.ToString(), "name", StringComparison.OrdinalIgnoreCase))
                        .ToDictionary(attribute => attribute.Name.LocalName, attribute => attribute.Value, StringComparer.OrdinalIgnoreCase);
            }
            catch (XmlException exc)
            {
                Tools.LogWarning(
                    customerCode + " SymitarConfig: Unable to parse XML for //option[@name=\"" + nameValue + "\"] config in temporary options. Expecting a well-formed, self-closing (\"" + closeString + "\") element.",
                    exc);
                return null;
            }

            var lenderConfig = LendersOffice.Integration.Symitar.LenderConfiguration.TryCreate(attributeDictionary);
            if (lenderConfig == null)
            {
                Tools.LogWarning(customerCode + " SymitarConfig: XML for //option[@name=\"" + nameValue + "\"] successfully parsed, but either a required attribute was missing or an attribute failed to parse as the correct data type.");
            }

            return lenderConfig;
        }

        public bool IsDay1DayOfRateLock 
        {
            get 
            {
                return m_IsDay1DayOfRateLock;
            }
            set
            {
                m_IsDay1DayOfRateLock = value;
                m_IsDay1DayOfRateLockSet = true;
            }
        }

        //todo remove the nullable part
        public Nullable<int> DefaultTaskPermissionLevelIdForImportedCategories
        {
            get
            {
                return m_DefaultTaskPermissionLevelIdForImportedCategories; 
            }
            set
            {
                m_DefaultTaskPermissionLevelIdForImportedCategories = value;
                //m_DefaultTaskPermissionLevelIdForImportedCategoriesSet = true;
            }
        }

        public Nullable<DateTime> OriginatorCompensationMigrationDate
        {
            get
            {
                return m_OriginatorCompensationMigrationDate;
            }
            set
            {
                m_OriginatorCompensationMigrationDate = value;
            }
        }

        public bool OriginatorCompensationApplyMinYSPForTPOLoan
        {
            get
            {
                return m_OriginatorCompensationApplyMinYSPForTPOLoan;
            }
            set
            {
                m_OriginatorCompensationApplyMinYSPForTPOLoan = value;
                m_OriginatorCompensationApplyMinYSPForTPOLoanSet = true;
            }
        }

        public bool IsDisplayCompensationChoiceInPml
        {
            get { return m_IsDisplayCompensationChoiceInPml; }
            set { m_IsDisplayCompensationChoiceInPml = value;
                m_IsDisplayCompensationChoiceInPmlSet = true; }
        }

        public bool IsHideLOCompPMLCert
        {
            get { return m_IsHideLOCompPMLCert; }
            set { m_IsHideLOCompPMLCert = value; m_IsHideLOCompPMLCertSet = true; }
        }

        public bool IsHidePricingwithoutLOCompPMLCert
        {
            get { return m_IsHidePricingwithoutLOCompPMLCert; }
            set { m_IsHidePricingwithoutLOCompPMLCert = value; m_IsHidePricingwithoutLOCompPMLCertSet = true; }
        }

        public bool IsUsePriceIncludingCompensationInPricingResult
        {
            get { return m_IsUsePriceIncludingCompensationInPricingResult; }
            set
            {
                m_IsUsePriceIncludingCompensationInPricingResult = value;
                m_IsUsePriceIncludingCompensationInPricingResultSet = true;
            }
        }

        public static string TaskAliasEmailAddress
        {
            get { return ConstStage.TaskAliasEmailAddress; }
        }

        public Nullable<int> AusImportDefaultConditionCategoryId
        {
            get { return m_AusImportDefaultConditionCategoryId; }
            set { m_AusImportDefaultConditionCategoryId = value; }
        }

        public bool IsDocMagicBarcodeScannerEnabled
        {
            get
            {
                return m_IsDocMagicBarcodeScannerEnabled;
            }
            set
            {
                m_IsDocMagicBarcodeScannerEnabled = value;
                m_IsDocMagicBarcodeScannerEnabledSet = true;
            }
        }
        public bool IsDocuTechBarcodeScannerEnabled
        {
            get
            {
                return m_IsDocuTechBarcodeScannerEnabled;
            }
            set
            {
                m_IsDocuTechBarcodeScannerEnabled = value;
                m_IsDocuTechBarcodeScannerEnabledSet = true;
            }
        }
        public bool IsIDSBarcodeScannerEnabled
        {
            get
            {
                return m_IsIDSBarcodeScannerEnabled;
            }
            set
            {
                m_IsIDSBarcodeScannerEnabled = value;
                m_IsIDSBarcodeScannerEnabledSet = true;
            }
        }

        public bool IsUseNewTaskSystem
        {
            get
            {
                return m_IsUseNewCondition ;
            }
            set 
            {
                m_IsUseNewCondition  = value;
            }
        }

        public bool IsUseNewTaskSystemStaticConditionIds
        {
            get
            {
                return m_IsUseNewTaskSystemStaticConditionIds;
            }
            set
            {
                m_IsUseNewTaskSystemStaticConditionIds = value;
            }
        }

        public E_RateLockExpirationWeekendHolidayBehavior RateLockExpirationWeekendHolidayBehavior
        {
            get
            {
                return m_RateLockExpirationWeekendHolidayBehavior;
            }
            set
            {
                m_RateLockExpirationWeekendHolidayBehavior = value;
            }
        }

        private string m_RateLockQuestionsJson;
        public List<string> RateLockQuestionsJson
        {
            get
            {
                var questions = ObsoleteSerializationHelper.JsonDeserialize<List<string>>(m_RateLockQuestionsJson);
                if (questions == null)
                {
                    return new List<string>(12);
                }

                return questions;
            }
            set
            {
                m_RateLockQuestionsJson = ObsoleteSerializationHelper.JsonSerialize<List<string>>(value);
            }
        }

        public bool IsApplyPricingEngineRoundingAfterLoComp
        {
            get
            {
                return m_IsApplyPricingEngineRoundingAfterLoComp;
            }
            set
            {
                m_IsApplyPricingEngineRoundingAfterLoComp = value;
                m_IsApplyPricingEngineRoundingAfterLoCompSet = true;
            }
        }

        //OPM 71620
        public bool IsAddPointWithoutOriginatorCompensationToRateLock
        {
            get
            {
                return m_IsAddPointWithoutOriginatorCompensationToRateLock;
            }
            set
            {
                m_IsAddPointWithoutOriginatorCompensationToRateLock = value;
                m_IsAddPointWithoutOriginatorCompensationToRateLockSet = true;
            }
        }

        public string EdocsStackOrderList
        {
            get { return m_EdocsStackOrderList; }
            private set { m_EdocsStackOrderList = value; }
        }

        public bool IsImportDoDuLpFindingsAsConditions
        {
            get
            {
                return m_IsImportDoDuLpFindingsAsConditions;
            }

            set
            {
                m_IsImportDoDuLpFindingsAsConditions = value;
                m_IsImportDoDuLpFindingsAsConditionsSet = true;
            }
        }

        private ComplianceEaseExportConfiguration m_CEExportConfig = null;
        private bool m_IsCEExportConfigSet = false;

        public ComplianceEaseExportConfiguration ComplianceEaseExportConfig
        {
            get
            {
                return m_CEExportConfig;
            }

            set
            {
                m_IsCEExportConfigSet = true;
                m_CEExportConfig = value;
            }
        }

        public bool IsEnableProvidentFundingSubservicingExport
        {
            get
            {
                return m_IsEnableProvidentFundingSubservicingExport;
            }
            set
            {
                m_IsEnableProvidentFundingSubservicingExport = value;
                m_IsEnableProvidentFundingSubservicingExportSet = true;
            }
        }

        public bool IsForceLeadToUseLoanCounter
        {
            get
            {
                return m_IsForceLeadToUseLoanCounter;
            }
            set { m_IsForceLeadToUseLoanCounter = value; }
        }

        public bool IsForceLeadToLoanTemplate
        {
            get
            {
                return m_IsForceLeadToLoanTemplate;
            }
            set { m_IsForceLeadToLoanTemplate = value; }
        }

        public bool IsForceLeadToLoanNewLoanNumber
        {
            get { return m_IsForceLeadToLoanNewLoanNumber; }
            set { m_IsForceLeadToLoanNewLoanNumber = value; }
        }

        public bool IsForceLeadToLoanRemoveLeadPrefix
        {
            get { return m_IsForceLeadToLoanRemoveLeadPrefix; }
            set { m_IsForceLeadToLoanRemoveLeadPrefix = value; }
        }

        public bool IsNewPmlUIEnabled
        {
            get { return m_IsNewPmlUIEnabled; }
            set 
            { 
                m_IsNewPmlUIEnabled = value;
                m_IsNewPmlUIEnabledSet = true;
            }
        }

        public Guid? TpoNewFeatureAlphaTestingGroup
        {
            get
            {
                return this.tpoNewFeatureAlphaTestingGroup;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.tpoNewFeatureAlphaTestingGroup = value;
                this.tpoNewFeatureAlphaTestingGroupSet = true;
            }
        }

        public Guid? TpoNewFeatureBetaTestingGroup
        {
            get
            {
                return this.tpoNewFeatureBetaTestingGroup;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.tpoNewFeatureBetaTestingGroup = value;
                this.tpoNewFeatureBetaTestingGroupSet = true;
            }
        }

        public NewTpoFeatureOcGroupAccess TpoRedisclosureAllowedGroupType
        {
            get
            {
                return this.tpoRedisclosureAllowedGroupType;
            }
            set
            {
                this.tpoRedisclosureAllowedGroupType = value;
                this.tpoRedisclosureAllowedGroupTypeSet = true;
            }
        }

        public TpoRequestFormSource TpoRedisclosureFormSource
        {
            get
            {
                return this.tpoRedisclosureFormSource;
            }
            set
            {
                this.tpoRedisclosureFormSource = value;
                this.tpoRedisclosureFormSourceSet = true;
            }
        }

        public Guid? TpoRedisclosureFormCustomPdfId
        {
            get
            {
                return this.tpoRedisclosureFormCustomPdfId;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.tpoRedisclosureFormCustomPdfId = value;
                this.tpoRedisclosureFormCustomPdfIdSet = true;
            }
        }

        public string TpoRedisclosureFormHostedUrl
        {
            get
            {
                return this.tpoRedisclosureFormHostedUrl;
            }
            set
            {
                this.tpoRedisclosureFormHostedUrl = value;
                this.tpoRedisclosureFormHostedUrlSet = true;
            }
        }

        public int? TpoRedisclosureFormRequiredDocType
        {
            get
            {
                return this.tpoRedisclosureFormRequiredDocType;
            }
            set
            {
                if (value == int.MinValue)
                {
                    value = null;
                }

                this.tpoRedisclosureFormRequiredDocType = value;
                this.tpoRedisclosureFormRequiredDocTypeSet = true;
            }
        }

        public NewTpoFeatureOcGroupAccess TpoInitialClosingDisclosureAllowedGroupType
        {
            get
            {
                return this.tpoInitialClosingDisclosureAllowedGroupType;
            }
            set
            {
                this.tpoInitialClosingDisclosureAllowedGroupType = value;
                this.tpoInitialClosingDisclosureAllowedGroupTypeSet = true;
            }
        }

        public TpoRequestFormSource TpoInitialClosingDisclosureFormSource
        {
            get
            {
                return this.tpoInitialClosingDisclosureFormSource;
            }
            set
            {
                this.tpoInitialClosingDisclosureFormSource = value;
                this.tpoInitialClosingDisclosureFormSourceSet = true;
            }
        }

        public OrigPortalAntiSteeringDisclosureAccessType OrigPortalAntiSteeringDisclosureAccess
        {
            get
            {
                return this.origPortalAntiSteeringDisclosureAccess;
            }
            set
            {
                this.origPortalAntiSteeringDisclosureAccess = value;
                this.origPortalAntiSteeringDisclosureAccessSet = true;
            }
        }

        public string OpNonQmMsgReceivingEmail
        {
            get
            {
                return this.opNonQmMsgReceivingEmail;
            }
            set
            {                
                this.opNonQmMsgReceivingEmail = value;
                this.opNonQmMsgReceivingEmailSet = true;
            }
        }

        public long OpConLogCategoryId
        {
            get
            {
                return this.opConLogCategoryId;
            }
            set
            {
                this.opConLogCategoryId = value;
                this.opConLogCategoryIdSet = true;
            }
        }

        public Guid? TpoInitialClosingDisclosureFormCustomPdfId
        {
            get
            {
                return this.tpoInitialClosingDisclosureFormCustomPdfId;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }

                this.tpoInitialClosingDisclosureFormCustomPdfId = value;
                this.tpoInitialClosingDisclosureFormCustomPdfIdSet = true;
            }
        }

        public string TpoInitialClosingDisclosureFormHostedUrl
        {
            get
            {
                return this.tpoInitialClosingDisclosureFormHostedUrl;
            }
            set
            {
                this.tpoInitialClosingDisclosureFormHostedUrl = value;
                this.tpoInitialClosingDisclosureFormHostedUrlSet = true;
            }
        }

        public int? TpoInitialClosingDisclosureFormRequiredDocType
        {
            get
            {
                return this.tpoInitialClosingDisclosureFormRequiredDocType;
            }
            set
            {
                if (value == int.MinValue)
                {
                    value = null;
                }

                this.tpoInitialClosingDisclosureFormRequiredDocType = value;
                this.tpoInitialClosingDisclosureFormRequiredDocTypeSet = true;
            }
        }

        private Lazy<BrokerTpoLoanNavigationHeaderLinkAlignmentSettings> tpoHeaderLinkAlignmentSettings;

        private Lazy<BrokerTpoLoanNavigationSettings> enableDisclosurePageInNewTpoLoanNavigationSettings;

        private Lazy<BrokerTpoLoanNavigationSettings> enableOrderInitialDisclosuresButtonInNewDisclosuresPageSettings;

        private Lazy<TpoInitialDisclosureVendorSettings> tpoDisclosureDocumentVendorSettings;

        public bool IsEnableDisclosurePageInNewTpoLoanNavigation(string type, Guid pmlBrokerId, Guid branchId)
        {
            return this.IsEnableTpoPortalSetting(type, pmlBrokerId, branchId, this.enableDisclosurePageInNewTpoLoanNavigationSettings);
        }

        public bool IsEnableOrderInitialDisclosuresButtonOnTpoDisclosuresPage(string type, Guid pmlBrokerId, Guid branchId)
        {
            return this.TpoInitialDisclosureVendorSettings.Vendor != null && this.IsEnableDisclosurePageInNewTpoLoanNavigation(type, pmlBrokerId, branchId) &&
                this.IsEnableTpoPortalSetting(type, pmlBrokerId, branchId, this.enableOrderInitialDisclosuresButtonInNewDisclosuresPageSettings);
        }

        public TpoInitialDisclosureVendorSettings TpoInitialDisclosureVendorSettings
        {
            get { return this.tpoDisclosureDocumentVendorSettings.Value; }
        }

        private bool IsEnableTpoPortalSetting(string type, Guid pmlBrokerId, Guid branchId, Lazy<BrokerTpoLoanNavigationSettings> settings)
        {
            if (this.BillingVersion != E_BrokerBillingVersion.PerTransaction || !this.IsNewPmlUIEnabled)
            {
                return false;
            }

            if (!settings.Value.HasDisablingTempOption)
            {
                return true;
            }

            if (string.Equals("P", type, StringComparison.OrdinalIgnoreCase))
            {
                return settings.Value.IsEnabledForOriginatingCompany(pmlBrokerId);
            }

            return settings.Value.IsEnabledForBranch(branchId);
        }

        public BrokerTpoLoanNavigationHeaderLinkAlignmentSettings TpoLoanNavigationHeaderLinkAlignmentSettings => this.tpoHeaderLinkAlignmentSettings.Value;

        /// <summary>
        /// Gets a value indicating whether the "Status" column is hidden 
        /// on the TPO portal "E-docs" page.
        /// </summary>
        public bool HideStatusColumnInTpoEdocs
        {
            get { return this.IsStringExistedInTempOption("<option name=\"HideStatusColumnInTpoEdocs\" value=\"true\" />"); }
        }

        /// <summary>
        /// Gets a value indicating whether page display changes (disabling fields,
        /// adding required icons from write rule requirements) via workflow are enabled.
        /// </summary>
        public bool EnablePageDisplayChangesViaWorkflow
        {
            get
            {
                return !this.IsStringExistedInTempOption("<option name=\"DisablePageDisplayChangesViaWorkflow\" value=\"true\" />");
            }
        }

        public bool UseMiVendorIntegrationsInPricing
        {
            get { return this.IsStringExistedInTempOption("<option name=\"UseMiVendorIntegrationsInPricing\" value=\"true\" />"); }
        }

        public CustomPmlFieldList CustomPmlFieldList
        {
            get { return m_CustomPmlFieldList; }
            set
            {
                m_CustomPmlFieldList = value;
                m_CustomPmlFieldListSet = true;
            }
        }

        public bool IsCustomPricingPolicyFieldEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"customlopricingpolicyfieldsenabled\" value=\"true\" />");
            }
        }

        public bool UseRateOptionVariantMI
        {
            get { return this.IsStringExistedInTempOption("<option name=\"UseRateOptionVariantMI\" value=\"true\" />"); }
        }

        public CustomPmlFieldList CustomPricingPolicyFieldList
        {
            get { return m_CustomPricingPolicyFieldList; }
            set
            {
                m_CustomPricingPolicyFieldList = value;
                m_CustomPricingPolicyFieldListSet = true;
            }
        }

        public bool IsEnableRateMonitorService
        {
            get { return m_IsEnableRateMonitorService; }
            set
            {
                m_IsEnableRateMonitorService = value;
                m_IsEnableRateMonitorServiceSet = true;
            }
        }

        public bool EnableDuplicateEDocProtectionChecking
        {
            get { return m_EnableDuplicateEDocProtectionChecking; }
            set
            {
                m_EnableDuplicateEDocProtectionChecking = value;
                m_EnableDuplicateEDocProtectionCheckingSet = true;
            }
        }

        public string MersFormat
        {
            get
            {
                if (m_MersFormatSet || string.IsNullOrEmpty(m_MersFormat)) { }
                return m_MersFormat;
            }

            set
            {
                m_MersFormat = value;
                m_MersFormatSet = true;
            }

        }

        public bool IsDocVendorTesting
        {
            get
            {
                return m_IsDocVendorTesting;
            }

            set
            {
                m_IsDocVendorTesting = value;
                m_IsDocVendorTestingSet = true;
            }
        }

        #region DU Import Options //OPM 94140
        public bool ImportAusFindingsByDefault
        {
            get 
            {
                return m_IsImportDuFindingsByDefault;
            }
            set 
            {
                m_IsImportDuFindingsByDefault = value;
                m_IsImportDuFindingsByDefaultSet = true;
            }
        }

        public bool ImportAus1003ByDefault
        {
            get
            {
                return m_IsImportDu1003ByDefault;
            }
            set
            {
                m_IsImportDu1003ByDefault = value;
                m_IsImportDu1003ByDefaultSet = true;
            }
        }

        public bool ImportAusCreditReportByDefault
        {
            get
            {
                return m_IsImportDuCreditReportByDefault;
            }
            set
            {
                m_IsImportDuCreditReportByDefault = value;
                m_IsImportDuCreditReportByDefaultSet = true;
            }
        }
        #endregion

        public bool EnableSequentialNumbering
        {
            get
            {
                return m_EnableSequentialNumbering;
            }
            set
            {
                m_EnableSequentialNumbering = value;
                m_EnableSequentialNumberingSet = true;
            }
        }

        public long SequentialNumberingSeed
        {
            get
            {
                return m_SequentialNumberingSeed;
            }
            set
            {
                m_SequentialNumberingSeed = value;
                m_SequentialNumberingSeedSet = true;
            }
        }

        public string SequentialNumberingFieldId
        {
            get
            {
                return m_SequentialNumberingFieldId;
            }
            set
            {
                m_SequentialNumberingFieldId = value;
                m_SequentialNumberingFieldIdSet = true;
            }
        }

        public string SandboxScratchLOXml
        {
            get { return m_SandboxScratchLOXml; }
            set
            {
                m_SandboxScratchLOXmlSet = true;
                m_SandboxScratchLOXml = value;
            }
        }

        public string EmployeeResourcesUrl
        {
            get { return m_EmployeeResourcesUrl; }
            set
            {
                m_EmployeeResourcesUrl = value;
            }
        }

        public bool IsSandboxEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableSandbox\" value=\"true\" />");
            }
        }

        public bool Has4506TIntegration
        {
            get
            {
                // 8/9/2013 dd - Return true if it has at least one active 4506-T integration.

                return Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(BrokerID).Any();
            }
        }

        public bool HasMIVendorIntegration
        {
            get
            {
                return MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(BrokerID).Any();
            }
        }

        public bool EnableKtaIntegration
        {
            get { return this.enableKtaIntegration; }
            set
            {
                this.enableKtaIntegration = value;
                this.enableKtaIntegrationSet = true;
            }
        }

        public bool IsEnableFannieMaeEarlyCheck { get; set; }
        public bool IsEnableFreddieMacLoanQualityAdvisor { get; set; }

        public string FnmaEarlyCheckUserName { get; set; }
        private Lazy<string> lazyFnmaEarlyCheckPassword = new Lazy<string>(() => null);
        private bool fnmaEarlyCheckPasswordSet;
        public string FnmaEarlyCheckInstutionId { get; set; }

        public Sensitive<string> FnmaEarlyCheckDecryptedPassword
        {
            get
            {
                return this.lazyFnmaEarlyCheckPassword.Value ?? string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value.Value) == false)
                {
                    this.lazyFnmaEarlyCheckPassword = new Lazy<string>(() => value.Value);
                    this.fnmaEarlyCheckPasswordSet = true;
                }
            }
        }

        // OPM 106855
        public string ActiveDirectoryURL
        {
            get { return m_activeDirectoryURL; }
            set
            {
                m_activeDirectoryURL = value;
                m_IsActiveDirectoryURLSet = true;
            }
        }

        private bool m_IsOptInMultiFactorAuthentication;
        private bool m_IsOptInMultiFactorAuthenticationSet = false;
        public bool IsOptInMultiFactorAuthentication
        {
            get
            {
                return this.m_IsOptInMultiFactorAuthentication;
            }

            set
            {
                this.m_IsOptInMultiFactorAuthentication = value;
                this.m_IsOptInMultiFactorAuthenticationSet = true;
            }
        }

        private string m_OptOutMfaName;
        private bool m_OptOutMfaNameSet = false;
        public string OptOutMfaName
        {
            get
            {
                return this.m_OptOutMfaName;
            }

            set
            {
                this.m_OptOutMfaName = value;
                this.m_OptOutMfaNameSet = true;
            }
        }

        public DateTime m_OptOutMfaDate = DateTime.MinValue;
        public bool m_OptOutMfaDateSet = false;
        public DateTime OptOutMfaDate
        {
            get
            {
                return this.m_OptOutMfaDate;
            }

            set
            {
                this.m_OptOutMfaDate = value;
                this.m_OptOutMfaDateSet = true;
            }
        }

        public bool AllowUserMfaToBeDisabled
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"AllowUserMfaToBeDisabled\" value=\"true\" />");
            }
        }
        
        private bool m_isEnableMultiFactorAuthentication;
        private bool m_isEnableMultiFactorAuthenticationSet = false;
        // 4/21/2014 dd - OPM 125242
        public bool IsEnableMultiFactorAuthentication
        {
            get
            {
                return this.IsOptInMultiFactorAuthentication;
            }
            set
            {
                m_isEnableMultiFactorAuthentication = value;
                m_isEnableMultiFactorAuthenticationSet = true;
            }
        }

        private bool m_isEnableMultiFactorAuthenticationTPO;
        private bool m_isEnableMultiFactorAuthenticationTPOSet = false;
        // 4/21/2014 dd - OPM 125242
        public bool IsEnableMultiFactorAuthenticationTPO
        {
            get
            {
                return this.IsOptInMultiFactorAuthentication;
            }

            set
            {
                m_isEnableMultiFactorAuthenticationTPO = value;
                m_isEnableMultiFactorAuthenticationTPOSet = true;
            }
        }

        private bool m_AllowDeviceRegistrationViaAuthCodePage;
        private bool m_AllowDeviceRegistrationViaAuthCodePageSet = false;
        public bool AllowDeviceRegistrationViaAuthCodePage
        {
            get
            {
                if (!this.IsOptInMultiFactorAuthentication)
                {
                    return false;
                }

                return this.m_AllowDeviceRegistrationViaAuthCodePage;
            }

            set
            {
                this.m_AllowDeviceRegistrationViaAuthCodePage = value;
                this.m_AllowDeviceRegistrationViaAuthCodePageSet = true;
            }
        }

        private E_sClosingCostFeeVersionT m_MinimumDefaultClosingCostDataSet;
        private bool m_MinimumDefaultClosingCostDataSetSet = false;
        public E_sClosingCostFeeVersionT MinimumDefaultClosingCostDataSet
        {
            get
            {
                if (m_MinimumDefaultClosingCostDataSet == E_sClosingCostFeeVersionT.Legacy)
                {
                    return E_sClosingCostFeeVersionT.LegacyButMigrated;
                }
                else
                {
                    return m_MinimumDefaultClosingCostDataSet;
                }
            }

            set
            {
                m_MinimumDefaultClosingCostDataSet = value;
                m_MinimumDefaultClosingCostDataSetSet = true;
            }
        }

        private bool m_EnableCustomaryEscrowImpoundsCalculation;
        private bool m_EnableCustomaryEscrowImpoundsCalculationSet = false;
        public bool EnableCustomaryEscrowImpoundsCalculation
        {
            get
            {
                return m_EnableCustomaryEscrowImpoundsCalculation;
            }

            set
            {
                m_EnableCustomaryEscrowImpoundsCalculation = value;
                m_EnableCustomaryEscrowImpoundsCalculationSet = true;
            }
        }

        public E_FeeTypeRequirementT FeeTypeRequirementT
        {
            get { return m_feeTypeRequirementT; }
            set
            {
                m_feeTypeRequirementT = value;
                m_feeTypeRequirementTSet = true;
            }
        }

        public bool DisableLendingLicenseStips
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableLendingLicenseStips\" value=\"true\" />");
            }
        }

        public bool DisableCreditStips
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableCreditStips\" value=\"true\" />");
            }
        }

        public E_Pml2AsQuickPricerMode Pml2AsQuickPricerMode
        {
            get { return m_Pml2AsQuickPricerMode; }
            set
            {
                m_Pml2AsQuickPricerMode = value;
                m_Pml2AsQuickPricerModeSet = true;
            }
        }

        public bool BorrPdCompMayNotExceedLenderPdComp
        {
            get { return this.borrPdCompMayNotExceedLenderPdComp; }
            set
            {
                this.borrPdCompMayNotExceedLenderPdCompSet = true;
                this.borrPdCompMayNotExceedLenderPdComp = value;
            }
        }

        public byte[] Version { get; private set; }
        private string m_closingCostFeeSetupJsonContent = string.Empty;
        private bool ClosingCostFeeSetupJsonContentSet = false;
        private string m_sanitizedClosingCostFeeSetupJsonContent;

        public string ClosingCostFeeSetupJsonContent
        {
            get
            {
                if (string.IsNullOrEmpty(this.m_sanitizedClosingCostFeeSetupJsonContent))
                {
                    this.m_sanitizedClosingCostFeeSetupJsonContent = SerializationHelper.SanitizeJsonString(this.m_closingCostFeeSetupJsonContent);
                }
                return m_sanitizedClosingCostFeeSetupJsonContent;
            }

            private set
            {
                this. m_closingCostFeeSetupJsonContent = value;
                this.m_sanitizedClosingCostFeeSetupJsonContent = null;
                this.ClosingCostFeeSetupJsonContentSet = true;
            }
        }

        /// <summary>
        /// This method returns a read-only copy of the Closing Cost Set.
        /// </summary>
        /// <returns>A read-only copy of the Closing Cost Set.</returns>
        public FeeSetupClosingCostSet GetUnlinkedClosingCostSet()
        {
            FeeSetupClosingCostSet closingCostSet = null;
            if (string.IsNullOrEmpty(this.ClosingCostFeeSetupJsonContent))
            {
                closingCostSet = new FeeSetupClosingCostSet(string.Empty);
            }
            else
            {
                closingCostSet = new FeeSetupClosingCostSet(this.ClosingCostFeeSetupJsonContent);
            }

            if (closingCostSet.GetFees(null).Count() == 0)
            {
                // 1/6/2015 dd - Create a list of default legacy fee type when it is missing.
                foreach (var fee in DefaultSystemClosingCostFee.LegacyFeeList.Concat(DefaultSystemClosingCostFee.FeeTemplate.SystemFeeList))
                {
                    closingCostSet.AddOrUpdate(fee);
                }

                closingCostSet.SetMismoImportMappings(DefaultSystemClosingCostFee.FeeTemplate.DefaultMismoImportFeeTypeMappings);
            }

            return closingCostSet;
        }

        public void SetClosingCostSet(FeeSetupClosingCostSet set, bool validate)
        {
            if (validate)
            {
                FeeSetupClosingCostSet oldSet = new FeeSetupClosingCostSet(this.ClosingCostFeeSetupJsonContent);
                string message;
                CheckUniqueDiscriptions(set, null);
                bool status = oldSet.ValidateNewSet(set, null, out message, null);

                if (status == false)
                {
                    throw new CBaseException(message, message);
                }
            }

            this.ClosingCostFeeSetupJsonContent = set.ToJson();
        }

        /// <summary>
        /// Sets the given json as the new closing cost fee setup.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public bool SetClosingCostJSON(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                this.ClosingCostFeeSetupJsonContent = string.Empty;
                return true;
            }
            try
            {
                ObsoleteSerializationHelper.JsonDeserialize<List<FeeSetupClosingCostFee>>(json);
            }
            catch (SerializationException)
            {
                return false;
            }

            this.ClosingCostFeeSetupJsonContent = json;
            return true;
        }

        public bool IsClosingCostSetEmpty
        {
            get
            {
                return string.IsNullOrEmpty(this.ClosingCostFeeSetupJsonContent);
            }
        }
        #endregion

        #region Temporary properties to allow certain lenders to beta test DU Integration
        public bool IsAllowImportDoDuInPml 
        {
            get 
            {
                // 8/14/2008 dd - Base on OPM 24177, we will always allow import from DO/DU in PML. We will only disabled the feature base on individual request.
                return !IsStringExistedInTempOption("<option name=\"disableimportdodupml\" value=\"true\" />"); 
            }
        }
        public bool AllowImportFromLpaInPml => !IsStringExistedInTempOption("<option name=\"disableimportfromlpainpml\" value=\"true\" />");
        public bool IsGFEandCoCVersioningEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enablegfeandcocversioning\" value=\"true\" />");
            }
        }
        public bool IsPmlDuEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enablepmldu\" value=\"true\" />");
            }
        }
        public bool IsPmlDoEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enablepmldo\" value=\"true\" />");
            }
        }
        public bool IsEmbeddedPmlDoEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enableembeddedpmldo\" value=\"true\" />");
            }
        }
        public bool IsEmbeddedPmlDuEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enableembeddedpmldu\" value=\"true\" />");
            }
        }
        public bool IsPmlLpEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enablepmllp\" value=\"true\" />");
            }
        }
        public bool IsEmbeddedPmlLpEnabled 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enablembeddedepmllp\" value=\"true\" />");
            }
        }
        public bool IsUsingTestLpAccount 
        {
            get 
            {
                return IsStringExistedInTempOption("<option name=\"usingtestlpaccount\" value=\"true\" />");
            }
        }

        // Obsolete, see OPM 214844
        //public bool IsUsingOldLoanEditorUI
        //{
        //    get
        //    {
        //        return IsStringExistedInTempOption("<option name=\"IsUsingOldLoanEditorUI\" value=\"true\" />");
        //    }
        //}

        public bool IsTotalScorecardEnabled
        {
            get
            {
                return IsStringExistedInTempOption( "<option name=\"enabletotalscorecard\" value=\"true\" />" );
            }
        }
        public bool IsDocMagicESignEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enabledocmagicesign\" value=\"true\" />");
            }
        }
        public bool IsEnableDocMagicLinkWhenPTM
        {
            get { return IsStringExistedInTempOption("<option name=\"enabledocmagiclinkwhenptm\" value=\"true\" />"); }
        }
        public bool IsEnableHomeEquityInLoanPurpose
        {
            get { return IsStringExistedInTempOption("<option name=\"EnableHomeEquityInLoanPurpose\" value=\"True\" />"); }
        }
        public bool IsEnableHELOC
        {
            get { return IsStringExistedInTempOption("<option name=\"EnableHELOC\" value=\"True\" />"); }
        }
        public bool IsSeamlessConformXEnabled
        {
            get { return IsStringExistedInTempOption("<option name=\"UseSeamlessIntegrationForConformX\" value=\"true\" />"); }
        }
        public bool DisableDocMagicLastDiscApr
        {
            get
            {
                // OPM 90968 5/23/13 GF.
                return IsStringExistedInTempOption("<option name=\"disabledocmagiclastdiscapr\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the lender enables the renovation checkbox in PML.
        /// This property is obsolete, use <see cref="EnableRenovationLoanSupport"/> instead.
        /// </summary>
        public bool IsEnableRenovationCheckboxInPML
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enablerenovationcheckboxinpml\" value=\"true\" />");
            }
        }

        public bool IsEnableBatchEdocEditor
        {
            get
            {
                //av opm 88270 Enable Batch Editor For Everyone
                return true;
                //return IsStringExistedInTempOption("<option name=\"enablebatchedoceditor\" value=\"true\" />");
            }
        }

        // TEMP Bit
        public bool IsUsePerRatePricing
        {
            get
            {
                if (BrokerID == new Guid("5029d586-3552-4aa8-a90e-386261356d62") /* PML DUAL TEST */ )
                {
                    AbstractUserPrincipal userPrincipal = PrincipalFactory.CurrentPrincipal;
                    if (userPrincipal != null)
                    {
                        // TEMP allow Overrides for the DUAL TEST Lender.
                        if (userPrincipal.LoginNm.ToLower() == "perrateoptiontesting") 
                            return true;
                        
                        if (userPrincipal.LoginNm.ToLower() == "noperrateoptiontesting") 
                            return false;
                    }
                }

                return IsStringExistedInTempOption("<option name=\"useperratepricing\" value=\"true\" />");
            }
        }

        // OPM 233679. 
        public bool EnableCashToCloseAndReservesDebugInPML { get { return IsStringExistedInTempOption("<option name=\"EnableCashToCloseAndReservesDebugInPML\" value=\"true\" />"); } }

        // OPM 233679. 
        public bool DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans { get { return IsStringExistedInTempOption("<option name=\"DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans\" value=\"true\" />"); } }

        // When true, we do not filter by maxYSP, but show all rates above at MaxYSP
        public bool IsDisplayRateOptionsAboveLowestTriggeringMaxYSP
        {
            get
            {
                return m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP;
            }
            set
            {
                m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP = value;
                m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSPSet = true;
            }
        }

        public bool IsUseLayeredFinancing
        {
            get
            {
                return m_IsUseLayeredFinancing;
            }
            set
            {
                m_IsUseLayeredFinancing = value;
                m_IsUseLayeredFinancingSet = true;
            }
        }

        public bool EnableAutoPriceEligibilityProcess
        {
            get
            {
                return m_EnableAutoPriceEligibilityProcess;
            }
            set
            {
                m_EnableAutoPriceEligibilityProcess = value;
                m_EnableAutoPriceEligibilityProcessSet = true;
            }
        }

        public bool EnableRetailTpoPortalMode
        {
            get
            {
                return m_EnableRetailTpoPortalMode;
            }
            set
            {
                m_EnableRetailTpoPortalMode = value;
                m_EnableRetailTpoPortalModeSet = true;
            }
        }

        public bool EnableRenovationLoanSupport
        {
            get
            {
                return m_EnableRenovationLoanSupport;
            }
            set
            {
                m_EnableRenovationLoanSupport = value;
                m_EnableRenovationLoanSupportSet = true;
            }
        }

        public bool IsSetNegativeAprClosingCostsToZero
        {
            get
            {
                // OPM 109851. Some lenders would not want negative APR-related CC going into APR calculation.
                return IsStringExistedInTempOption("<option name=\"issetnegativeaprclosingcoststozero\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Controls whether DOT fields are unlocked during PML submission, requested by PML0214. OPM 251650
        /// </summary>
        public bool IsAllowUnlockingDotFieldsDuringPmlSubmission
        {
            get { return !this.IsStringExistedInTempOption("<option name=\"DoNotUnlockDOTFieldsDuringPMLSubmission\" value=\"true\" />"); }
        }

        public bool SendAutoLockToLockDesk
        {
            get
            {
                return m_SendAutoLockToLockDesk;
            }
            set
            {
                m_SendAutoLockToLockDesk = value;
                m_SendAutoLockToLockDeskSet = true;
            }
        }

        public bool SaveLockConfOnAutoLock
        {
            get
            {
                return m_SaveLockConfOnAutoLock;
            }
            set
            {
                m_SaveLockConfOnAutoLock = value;
                m_SaveLockConfOnAutoLockSet = true;
            }
        }

        /// <summary>
        /// Searches for the specified pattern within the <see cref="TempOptionXmlContent"/>.
        /// </summary>
        /// <param name="regex">The target pattern of the search.</param>
        /// <returns>The matches found for the specified pattern.</returns>
        /// <remarks>Temporary option searches are case insensitive.</remarks>
        private Match IsRegexInTempOption(string regex)
        {
            if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
            {
                return Match.Empty;
            }

            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            return r.Match(this.TempOptionXmlContent.Value);
        }

        /// <summary>
        /// Searches for the specified substring within the <see cref="TempOptionXmlContent"/>.
        /// </summary>
        /// <param name="str">The target substring of the search.</param>
        /// <returns><see langword="true"/> if the temporary options contains <paramref name="str"/>; <see langword="false"/> otherwise.</returns>
        private bool IsStringExistedInTempOption(string str)
        {
            if (this.TempOptionXmlContent.Value != null) 
            {
                // opm 460807 - cache and/or log TempOptionXml Retrieval. 
                // this is turned on & off through stage constants.
                Stopwatch sw = null;
                bool result;
                if (this.enableTempOptionRetrievalLogging)
                {
                    sw = Stopwatch.StartNew();
                }

                if (this.enableTempOptionRetrievalCaching)
                {
                    result = this.TempOptionDictionary.GetOrAdd(
                            str,
                            new Lazy<bool>(() => this.TempOptionXmlContent.Value.IndexOf(str, StringComparison.OrdinalIgnoreCase) >= 0))
                            .Value;
                }
                else
                {
                    // 9/9/2013 dd - Search is case insensitive.
                    result = this.TempOptionXmlContent.Value.IndexOf(str, StringComparison.OrdinalIgnoreCase) >= 0;                    
                }
                if (this.enableTempOptionRetrievalLogging)
                {
                    sw.Stop();
                    Tools.LogInfo("BrokerDB.IsStringExistedInTempOption for string: " + str + " executed in " + sw.ElapsedTicks + " ticks.");
                }
                return result;
            }
            
            return false;
        }        

        public bool IsEnableVOXConfiguration
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"IsEnableVOXConfiguration\" value=\"true\" />");
            }
        }

        public bool IsEDocsEnabled
        {
            get
            {
                return m_IsEDocsEnabled; 
            }
            set
            {
                m_IsEDocsEnabled = value;
                m_IsEDocsEnabledSet = true;
            }
        }
        public bool HasTotalScorePreviewFeature
        {
            get { return IsStringExistedInTempOption("<option name=\"hastotalscorepreviewfeature\" value=\"true\" />"); }
        }

        public bool IsEDocNotificationsEnabled
        {
            get { return !IsStringExistedInTempOption("<option name=\"disableedocnotifications\" value=\"true\" />"); }
        }

        public bool IsEmployeeStartAndTerminationDEnabled
        {
            get { return IsStringExistedInTempOption("<option name=\"isemployeestartandterminationdenabled\" value=\"true\" />"); }
        }

        public bool IsEmailPMLUserCertificateDisabled
        {
            get { return IsStringExistedInTempOption("<option name=\"disablecertificateemailtopmluser\" value=\"true\" />"); }
        }

        public bool HasLpLiveProductsEnabled
        {
            get { return IsStringExistedInTempOption("<option name=\"HasLpLiveProductsEnabled\" value=\"true\" />"); }
        }
        
        public bool UseUpdatedCompanyAndPersonTemplateForDRIVE
        {
            get { return IsStringExistedInTempOption("<option name=\"UseUpdatedCompanyAndPersonTemplateForDRIVE\" value=\"true\" />"); }
        }

        public bool IsEnableBrokerFaxPackages
        {
            // See OPM 82755 for original setup of this one. PML0132 is currently the only lender using this.
            // This setting will control if pml users have acccess to the fax coversheets link in PML pipeline.
            // Implementor must also set up a coversheet profile for the lender in FaxCoverSheets or get error.
            // The reason we have this option is so that we can show/hide the link as needed.
            get 
            {
                return IsStringExistedInTempOption("<option name=\"enablebrokerfaxpackages\" value=\"true\" />")
                    && BrokerID == new Guid("73A50931-53E7-4BF2-BC54-D385E9F0CE8A"); // PML0132
            }
        }

        public bool IsEnableFhaConnection
        {
            // OPM 44560. Temp bit to expose FHA Connection.
            get
            {
                return true;
            }
        }

        public bool IsEnableNewConsumerPortal
        {
            get { return m_IsEnableNewConsumerPortal; }
            set
            {
                m_IsEnableNewConsumerPortalSet = true;
                m_IsEnableNewConsumerPortal = value;
            }
        }

        private string m_ProLenderServerURL;
        public string ProLenderServerURL
        {
            get
            {
                if (m_ProLenderServerURL == null)
                {
                    if (IsStringExistedInTempOption("<option name=\"prolenderserverurl\""))
                    {
                        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                        doc.LoadXml(this.TempOptionXmlContent.Value);
                        m_ProLenderServerURL = doc.SelectSingleNode("options/option[@name='prolenderserverurl']/@value").Value;
                    }
                    else
                    {
                        m_ProLenderServerURL = "";
                    }
                }
                
                return m_ProLenderServerURL;
            }
        }

        public string AutoLoanProspectorLoginName
        {
            get
            {
                if (IsStringExistedInTempOption("<option name=\"autolp\""))
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(this.TempOptionXmlContent.Value);
                    return doc.SelectSingleNode("options/option[@name='autolp']/@login").Value;

                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public Sensitive<string> AutoLoanProspectorPassword
        {
            get
            {
                if (IsStringExistedInTempOption("<option name=\"autolp\""))
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(this.TempOptionXmlContent.Value);
                    return doc.SelectSingleNode("options/option[@name='autolp']/@password").Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string NmLsIdentifier
        {
            get { return m_sNmlsIdentifier; }
            set { m_sNmlsIdentifier = value; }
        }

        public Guid DefaultLockDeskID
        {
            get { return m_defaultLockDeskID; }
            set { m_defaultLockDeskID = value; m_defaultLockDeskIDSet = true; }
        }

        public List<DocumentVendorBrokerSettings> AllActiveDocumentVendors
        {
            get
            {
                if (m_ActiveDocumentVendors == null)
                    m_ActiveDocumentVendors = DocumentVendorBrokerSettings.ListAllForBroker(m_brokerID);
                if(IsEnablePTMDocMagicSeamlessInterface)
                    return m_ActiveDocumentVendors;
                //If "docmagic" is disabled, then multi document vendors should also be disabled
                return new List<DocumentVendorBrokerSettings>();
            }
            set
            {
                m_ActiveDocumentVendors = value;
                m_ActiveDocumentVendorsSet = true;
            }
        }

        public IEnumerable<DocumentVendorBrokerSettings> GetDocumentVendors(bool isTest, bool isLeftTreeFrame)
        {
            if (isLeftTreeFrame)
            {
                if (isTest)
                {
                    return AllActiveDocumentVendors.Where(v => v.IsEnableForTestLoans);
                }
                else
                {
                    return AllActiveDocumentVendors.Where(v => v.IsEnableForProductionLoans);
                }
            }

            return AllActiveDocumentVendors.Where(v => v.IsEnableForProductionLoans || v.IsEnableForTestLoans);
        }

        public bool IsHidePrequalTextInPml
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"isHideprequaltextinpml\" value=\"true\" />");
            }
        }

        public bool IsGenerateDailyIISReport
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"isGenerateDailyIISReport\" value=\"true\" />");
            }
        }
        #endregion

        private static Dictionary<Guid, KeyValuePair<DateTime, BrokerDB>> x_brokerDBDictionary = new Dictionary<Guid, KeyValuePair<DateTime, BrokerDB>>();
        private static object x_brokerDBDictionaryLock = new object();


        public static BrokerDB RetrieveByIdForceRefresh(Guid brokerId)
        {
            return InternalUseOnlyRetrieveById(brokerId);
        }

        public static bool RetrieveUseCustomCocFieldListBit(Guid brokerId)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, StoredProcedureName.Create("BROKER_RetrieveUseCustomCocFieldListBit").Value, parameters, TimeoutInSeconds.Default))
                {
                    if (reader.Read())
                    {
                        return (bool)reader["UseCustomCocFieldList"];
                    }
                }
            }

            return false;
        }

        // replace this with C# 7 Tuples when we update.
        public static Tuple<bool, bool> RetrieveUseCustomCocFieldBitAndEnableRedisclosureForHelocBit(Guid brokerId)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, StoredProcedureName.Create("BROKER_RetrieveUseCustomCocFieldBitAndEnableRedisclosureForHelocBit").Value, parameters, TimeoutInSeconds.Default))
                {
                    if (reader.Read())
                    {
                        var useCustomCocField = (bool)reader["UseCustomCocFieldList"];
                        var enableRedisclosureForHeloc = (bool)reader["EnableCocRedisclosureTriggersForHelocLoans"];
                        return new Tuple<bool, bool>(useCustomCocField, enableRedisclosureForHeloc);
                    }
                }
            }

            return new Tuple<bool, bool>(false, true);
        }

        /// <summary>
        /// Clear the broker from cache. 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static void RemoveBrokerFromCache(Guid brokerId)
        {

            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                string key = "BrokerDB_" + brokerId;
                HttpContext.Current.Items[key] = null;
            }
            else
            {
                lock (x_brokerDBDictionaryLock)
                {
                    if (x_brokerDBDictionary.ContainsKey(brokerId))
                    {
                        x_brokerDBDictionary.Remove(brokerId);
                    }
                }
            }

        }

        private static Dictionary<Guid, BrokerDB> x_brokerDBDictionaryVersion2 = new Dictionary<Guid, BrokerDB>();
        private static object x_brokerDBDictionaryVersion2Lock = new object();

        private static BrokerDB RetrieveByIdUsingVersion(Guid brokerId)
        {
            lock (x_brokerDBDictionaryVersion2Lock)
            {
                BrokerDB brokerDB = null;

                HttpContext context = HttpContext.Current;

                string key = "BrokerDBVersion2_" + brokerId;

                if (null != context)
                {
                    brokerDB = context.Items[key] as BrokerDB;

                    if (brokerDB != null)
                    {
                        // 1/17/2015 dd - We still always cache the BrokerDB per request.
                        return brokerDB;
                    }

                }

                if (x_brokerDBDictionaryVersion2.TryGetValue(brokerId, out brokerDB) == false)
                {
                    // 1/17/2015 dd - Broker is not in cache yet. Loan from cache.
                    brokerDB = new BrokerDB();
                    brokerDB.m_isNew = false;
                    brokerDB.m_brokerID = brokerId;

                    if (brokerDB.RetrieveOnlyIfNewerThan(new byte[0]) == false)
                    {
                        throw new BrokerDBNotFoundException(brokerId);
                    }
                    x_brokerDBDictionaryVersion2[brokerId] = brokerDB;

                }
                else
                {
                    BrokerDB tempBrokerDB = new BrokerDB();
                    tempBrokerDB.m_isNew = false;
                    tempBrokerDB.m_brokerID = brokerId;

                    if (tempBrokerDB.RetrieveOnlyIfNewerThan(brokerDB.Version) == true)
                    {
                        // 1/17/2015 dd - THere is new version from database. Replace the object in dictionary with this one.
                        brokerDB = tempBrokerDB;
                        x_brokerDBDictionaryVersion2[brokerId] = brokerDB;
                    }

                }

                if (context != null)
                {
                    context.Items[key] = brokerDB;
                }

                return brokerDB;
            }
        }

        private class SimpleBrokerDBLock
        {
            private object m_lock = new object();

            private BrokerDB m_currentDB = null;
            private DateTime m_expiresTime = DateTime.MinValue;
            public Guid BrokerId { get; private set; }

            internal SimpleBrokerDBLock(Guid brokerId)
            {
                this.BrokerId = brokerId;
                this.m_expiresTime = DateTime.MinValue;
            }

            internal BrokerDB GetBrokerThreadSafe()
            {
                Stopwatch sw = Stopwatch.StartNew();
                string status = string.Empty;
                try
                {
                    // 7/1/2015 dd - To reduce number of lock contention even further, we will not even check if the last time we load broker info is within 2 minutes.
                    if (m_expiresTime > DateTime.Now)
                    {
                        status = "Requires No-Lock";
                        return m_currentDB;
                    }

                    lock (m_lock)
                    {
                        if (m_currentDB == null)
                        {
                            m_currentDB = new BrokerDB();
                            m_currentDB.m_isNew = false;
                            m_currentDB.m_brokerID = this.BrokerId;
                            if (m_currentDB.RetrieveOnlyIfNewerThan(new byte[0]) == false)
                            {
                                m_currentDB = null;
                                throw new BrokerDBNotFoundException(this.BrokerId);
                            }
                        }
                        else
                        {
                            BrokerDB tempBrokerDB = new BrokerDB();
                            tempBrokerDB.m_isNew = false;
                            tempBrokerDB.m_brokerID = this.BrokerId;
                            if (tempBrokerDB.RetrieveOnlyIfNewerThan(m_currentDB.Version) == true)
                            {
                                m_currentDB = tempBrokerDB;
                            }
                        }
                        m_expiresTime = DateTime.Now.AddMinutes(2);
                        status = "Requires - Lock";
                        return m_currentDB;
                    }
                }
                finally
                {
                    PerformanceMonitorItem.AddTimingDetailsToCurrent("SimpleBrokerDBLock.GetBrokerThreadSafe - " + status, sw.ElapsedMilliseconds);
                }
            }
        }

        private static Dictionary<Guid, SimpleBrokerDBLock> x_brokerIdDictionaryVersion3 = new Dictionary<Guid, SimpleBrokerDBLock>();

        private static Dictionary<Guid, SimpleBrokerDBLock> x_brokerIdDictionaryVersion3RequiresLock = new Dictionary<Guid, SimpleBrokerDBLock>();
        private static object x_brokerIdDictionaryVersion3Lock = new object();

        public static void InitializeBrokerIdLock()
        {
            if (ConstStage.UsingBrokerDBCacheV2 == 3)
            {
                Stopwatch sw = Stopwatch.StartNew();

                Dictionary<Guid, SimpleBrokerDBLock> dictionary = new Dictionary<Guid, SimpleBrokerDBLock>();

                foreach (var brokerId in BrokerDbLite.ListAllSlow().Keys)
                {
                    dictionary.Add(brokerId, new SimpleBrokerDBLock(brokerId));
                }

                x_brokerIdDictionaryVersion3 = dictionary;
                sw.Stop();
                Tools.LogInfo("BrokerDB.InitializeBrokerIdLock executed in " + sw.ElapsedMilliseconds + "ms.");
            }
        }

        /// <summary>
        /// Calls a stored procedure to retrieve a broker's name from the Database.
        /// </summary>
        /// <param name="brokerId">The id of the broker to retrieve the name of.</param>
        /// <returns>The broker's name.</returns>
        public static string RetrieveNameOnlyFromDb(Guid brokerId)
        {
            Tuple<string, string> nameAndCC = RetrieveNameAndCustomerCodeFromDb(brokerId);
            if (nameAndCC != null)
            {
                return nameAndCC.Item1;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieves the name and customer code from the DB.
        /// </summary>
        /// <param name="brokerId">The broker to look up.</param>
        /// <returns>A tuple. Item 1 contains the broker name, Item 2 contains the customer code.</returns>
        public static Tuple<string, string> RetrieveNameAndCustomerCodeFromDb(Guid brokerId)
        {
            Tuple<string, string> nameAndCC = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetBrokerNmByBrokerId", new SqlParameter[] { new SqlParameter("BrokerId", brokerId) }))
            {
                if (reader.Read())
                {
                    nameAndCC = new Tuple<string, string>((string)reader["BrokerNm"], (string)reader["CustomerCode"]);
                }
            }

            return nameAndCC;
        }

        /// <summary>
        /// Gets a value indicating whether this is an enabled lender.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>True if the lender is enabled. Otherwise, false.</returns>
        public static bool IsEnabled(Guid brokerId)
        {
            var parameters = new[]
            {
                new SqlParameter("@brokerid", brokerId)
            };

            bool enabled = false;

            var procedureName = StoredProcedureName.Create("GetBrokerStatus");
            if (procedureName == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName.Value, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    enabled = (int)reader["status"] == 1;
                }
            }

            return enabled;
        }

        /// <summary>
        /// Retrieves the doc type to use for ESigned documents.
        /// </summary>
        /// <remarks>This is used when a user uploads a signed document and is splitting the document by barcodes.</remarks>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The doc type id for the </returns>
        public static int? GetESignedDocumentDocTypeId(Guid brokerId)
        {
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_GetESignedDocumentDocTypeId", new[] { new SqlParameter("BrokerId", brokerId) }))
            {
                if (reader.Read())
                {
                    return reader.AsNullableInt("ESignedDocumentDocTypeId");
                }
            }

            throw new DeveloperException(ErrorMessage.SystemError);
        }

        private static BrokerDB RetrieveByIdUsingVersion3(Guid brokerId)
        {
            // 6/30/2015 dd - Will only lock per broker.

            BrokerDB brokerDB = null;

            // 6/30/2015 dd - HttpContext.Current is thread-safe for Http request.
            // If it is a http request then try to get from HttpContext.Items dictionary.

            HttpContext context = HttpContext.Current;

            string key = "BrokerDBVersion3_" + brokerId;

            if (null != context)
            {
                brokerDB = context.Items[key] as BrokerDB;

                if (brokerDB != null)
                {
                    // 1/17/2015 dd - We still always cache the BrokerDB per request.
                    return brokerDB;
                }

            }

            SimpleBrokerDBLock simpleBrokerLock = null;

            if (x_brokerIdDictionaryVersion3.TryGetValue(brokerId, out simpleBrokerLock) == false)
            {
                // Broker Id does not exists. Possible cause is it a brand new broker.
                simpleBrokerLock = null;

                // 6/30/2015 dd - For all new broker let put them on second dictionary that require lock.
                lock (x_brokerIdDictionaryVersion3Lock)
                {
                    if (x_brokerIdDictionaryVersion3RequiresLock.TryGetValue(brokerId, out simpleBrokerLock) == false)
                    {
                        simpleBrokerLock = new SimpleBrokerDBLock(brokerId);
                        x_brokerIdDictionaryVersion3RequiresLock.Add(brokerId, simpleBrokerLock);
                    }

                    if (null != context)
                    {
                        // 7/30/2015 - Only interest in this log for Http Request.
                        Tools.LogInfo("BrokerDB.RetrieveByIdUsingVersion3 - Required Second Lock Dictionary - BrokerId=" + brokerId);
                    }
                }

            }

            brokerDB = simpleBrokerLock.GetBrokerThreadSafe();

            if (context != null)
            {
                context.Items[key] = brokerDB;
            }

            return brokerDB;

        }

        private static BrokerDBData GetBrokerDBData(Guid brokerId)
        {
            BrokerDBData data;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveBrokerByID", null, new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) } ))
            {
                if (reader.Read())
                {
                    data = new BrokerDBData(reader);
                }
                else
                {
                    throw new BrokerDBNotFoundException(brokerId);
                }
            }

            return data;
        }

        public static BrokerDB RetrieveById(Guid brokerId)
        {
            int version = ConstStage.UsingBrokerDBCacheV2;

            if (version == 3)
            {
                // 6/30/2015 dd - Will lock per broker.
                return RetrieveByIdUsingVersion3(brokerId);
            }
            else if (version == 2)
            {
                // 6/30/2015 dd - There is a bug in this version that it will hold a lock on all requests.
                return RetrieveByIdUsingVersion(brokerId);
            }
            else
            {
                // 1/17/2015 dd - A way to fallback to old behavior if the new one has bug.
                return RetrieveByIdOld(brokerId);
            }
        }

        /// <summary>
        /// Retrieve broker data from database. BrokerDBNotFoundException will throw if broker id is invalid.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        private static BrokerDB RetrieveByIdOld(Guid brokerId)
        {
            BrokerDB broker = null;

            HttpContext context = HttpContext.Current;
            string key = "BrokerDB_" + brokerId;
            if (null != context)
            {
                broker = context.Items[key] as BrokerDB;

                if (broker == null)
                {
                    broker = InternalUseOnlyRetrieveById(brokerId);
                    context.Items[key] = broker;
                }
            }
            else
            {
                // 3/19/2012 dd - This call is from non Http request. Will use
                // dictionary object.
                lock (x_brokerDBDictionaryLock)
                {
                    broker = null;
                    KeyValuePair<DateTime, BrokerDB> item;
                    if (x_brokerDBDictionary.TryGetValue(brokerId, out item) == true)
                    {
                        if (item.Key < DateTime.Now)
                        {
                            // 3/19/2012 dd - Item already expired.
                            broker = null;
                            x_brokerDBDictionary.Remove(brokerId);
                        }
                        else
                        {
                            broker = item.Value;
                        }
                    }

                    if (broker == null)
                    {
                        broker = InternalUseOnlyRetrieveById(brokerId);
                        x_brokerDBDictionary.Add(brokerId, new KeyValuePair<DateTime, BrokerDB>(DateTime.Now.AddMinutes(5), broker));
                    }
                }
            }

            return broker;
        }

        private static BrokerDB InternalUseOnlyRetrieveById(Guid brokerId)
        {
            BrokerDB broker = new BrokerDB();
            broker.m_isNew = false;
            broker.m_brokerID = brokerId;
            broker.Retrieve();
            return broker;
        }
        /// <summary>
        /// Create new BrokerDB object that does not exist in the database
        /// </summary>
        public BrokerDB()
        {
            TempOptionDictionary = new ConcurrentDictionary<string, Lazy<bool>>();
            enableTempOptionRetrievalCaching = ConstStage.EnableTempOptionRetrievalCaching;
            enableTempOptionRetrievalLogging = ConstStage.EnableTempOptionRetrievalLogging;

            m_isNew = true;
            BlockedCRAsXmlContent = "";
            
            this.tpoHeaderLinkAlignmentSettings = new Lazy<BrokerTpoLoanNavigationHeaderLinkAlignmentSettings>(() => this.GetTpoLoanNavigationHeaderLinkAlignmentSettings());
            this.enableDisclosurePageInNewTpoLoanNavigationSettings = new Lazy<BrokerTpoLoanNavigationSettings>(() => this.GetTpoDisclosurePageSetting());
            this.enableOrderInitialDisclosuresButtonInNewDisclosuresPageSettings = new Lazy<BrokerTpoLoanNavigationSettings>(() => this.GetOrderInitialDisclosuresInTpoSetting());
            this.tpoDisclosureDocumentVendorSettings = new Lazy<TpoInitialDisclosureVendorSettings>(() => this.GetTpoDisclosureDocumentVendorSettings());
            this.historicalPricingSettings = new Lazy<HistoricalPricingSettings>(() => this.GetHistoricalPricingSettings());
            this.internalPricerUiSettings = new Lazy<InternalPricerUiSettings>(() => this.GetInternalPricerUiSettings());
            this.pmlResults3PricingSettings = new Lazy<PML3Settings>(() => this.GetPmlResults3Settings());
            this.productCodeFilterSettings = new Lazy<ProductCodeFilterSettings>(this.GetProductCodeFilterSettings);
            this.seamlessLpaSettings = new Lazy<BrokerSeamlessLPASettings>(this.GetSeamlessLPASetting);
            this.tpoLoanEditorSaveButtonBrokerOptions = new Lazy<TpoLoanEditorSaveButtonBrokerOptions>(this.GetTpoLoanEditorSaveButtonBrokerOptions);
            this.nonQmOpSettings = new Lazy<NonQmOpSettings>(() => this.GetNonQmOpSettings());
        }

        /// <summary>
        /// Applies the default ToString() formatting on a decimal and returns the
        /// resulting string, unless the decimal has integer precision, in which case it
        /// returns "0".
        /// </summary>
        /// <param name="val">The decimal to string-or-zeroify.</param>
        /// <returns>The default string representation of the decimal, or 0 if the decimal is an integer.</returns>
        public static string ToDecimalString(decimal val)
        {
            NumberFormatInfo formatDecimal3Digits = new NumberFormatInfo();
            formatDecimal3Digits.NumberDecimalDigits = 3;
            // Pretend to round to 3 decimal places, but instead do nothing because NumberDecimalDigits
            //   applies only to the "N" and "F" format types, but the default is "G".
            // 
            string decimalString = val.ToString( formatDecimal3Digits);
            int index = decimalString.IndexOf('.');
            // Convert integers to 0.
            if (index < 0)
            {
                return "0";
            }

            try
            {
                if (index == 0 || int.Parse(decimalString.Substring(0, index)) == 0)
                {
                    // If somehow the default system formatting freaks out and gives us a decimal without
                    //   a leading zero and the first character of our string is '.', tack on that missing '0'.
                    // If the first character of our string parses to 0, replace with a shiny new '0'.
                    decimalString = "0" + decimalString.Substring(index, decimalString.Length - 1);
                }
            }
            catch (FormatException) { } //if there's something wrong with the int format (which, I will remind you, is not a string passed
                                        //  in from the UI, it is created by calling .ToString() on a decimal), that's ok, just return what it is.
                                        //  I mean, come on, the system, when asked to format a decimal into a string, put a period somewhere,
                                        //  but the part before the period was not convertible to an int. The only way that happens is if
                                        //  val >= 2147483648 (or less than -214783649), we somehow formatted the decimal as currency above,
                                        //  or the server is possessed. In any of these cases, we should pass along whatever folly we now possess.
            catch (OverflowException) { } // See above--this is actually where we deal with the >= 2147483648 or < -214783649 thing.
            catch (ArgumentOutOfRangeException) { } //if index > 2 and the Substring call fails, that's ok, just return what it is
            return decimalString;
        }

        /// <summary>
        /// 1/17/2015 dd - 
        /// Return true and bind all BrokerDB properties when the version in database is newer than version pass in.
        /// Return false if there is no broker or version in database is same as version pass in.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        private bool RetrieveOnlyIfNewerThan(byte[] version)
        {
            if (m_isNew)
            {
                // Retrieve method can only be call when using constructor that take brokerID.
                return false;
            }

            Stopwatch sw = Stopwatch.StartNew();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", m_brokerID),
                                            new SqlParameter("@Version", version)
                                        };
            string status = string.Empty;

            try
            {
                BrokerDBData data;

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "RetrieveBrokerByIDWithVersion", parameters))
                {
                    if (!reader.Read())
                    {
                        status = "No Change";
                        return false;
                    }

                    data = new BrokerDBData(reader);
                }
                status = "Has New Version Reload";
                BindDataReader(data);
                return true;
            }
            catch (NotFoundException)
            {
                // 7/15/2014 dd - Could not find the connection info for this broker.
            }
            finally {
                PerformanceMonitorItem.AddTimingDetailsToCurrent("BrokerDB.RetrieveOnlyIfNewerThan - " + status, sw.ElapsedMilliseconds);
            }

            return false;
        }

        /// <summary>
        /// Retrieve and populate from database. Need to use constructor that take brokerID.
        /// </summary>
        /// <returns>true - if the record exists and retrieve successfully</returns>
        private bool Retrieve() 
        {
            if (m_isNew) 
            {
                // Retrieve method can only be call when using constructor that take brokerID.
                return false;
            }

            SqlParameter[] parameters = {new SqlParameter("@BrokerID", m_brokerID)};
            bool ret = false;
            try
            {
                BrokerDBData data;
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "RetrieveBrokerByID", parameters))
                {
                    if (reader.Read())
                    {
                        data = new BrokerDBData(reader);
                    }
                    else
                    {
                        throw new BrokerDBNotFoundException(m_brokerID);
                    }
                }

                BindDataReader(data);
                ret = true;
            }
            catch (BrokerDBNotFoundException)
            {
                throw;
            }
            catch (NotFoundException)
            {
                // 7/15/2014 dd - Could not find the connection info for this broker.
                throw new BrokerDBNotFoundException(m_brokerID);
            }

            return ret;
        }

        private void BindDataReader(BrokerDBData reader)
        {
            #region Bind Data from DB
            m_address = new CommonLib.Address();
            m_name = (string)reader["BrokerNm"];
            m_url = (string)reader["Url"];
            m_status = Convert.ToInt32(reader["Status"]);
            m_address.StreetAddress = (string)reader["BrokerAddr"];
            m_address.City = (string)reader["BrokerCity"];
            m_address.State = (string)reader["BrokerState"];
            m_address.Zipcode = (string)reader["BrokerZip"];
            m_phone = (string)reader["BrokerPhone"];
            m_fax = (string)reader["BrokerFax"];
            m_licenseNumber = (string)reader["BrokerLicNum"];
            m_pricePerSeat = Convert.ToDecimal(reader["PricePerSeat"]);
            m_accountManager = (string)reader["AccountManager"];
            m_customerCode = (string)reader["CustomerCode"];
            var maybeEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
            if (maybeEncryptionKeyId.HasValue)
            {
                this.EncryptionKeyId = maybeEncryptionKeyId.Value;
            }

            m_optionTelemarketerCanOnlyAssignToManager = (bool)reader["OptionTelemarketerCanOnlyAssignToManager"];
            m_optionTelemarketerCanRunPrequal = (bool)reader["OptionTelemarketerCanRunPrequal"];
            m_hasLenderDefaultFeatures = (bool)reader["HasLenderDefaultFeatures"];
            m_isLOAllowedToEditProcessorAssignedFile = (bool)reader["IsLOAllowedToEditProcessorAssignedFile"];
            m_isOnlyAccountantCanModifyTrustAccount = (bool)reader["IsOnlyAccountantCanModifyTrustAccount"];
            m_isOthersAllowedToEditUnderwriterAssignedFile = (bool)reader["IsOthersAllowedToEditUnderwriterAssignedFile"];
            m_isAllowViewLoanInEditorByDefault = (bool)reader["IsAllowViewLoanInEditorByDefault"];
            m_canRunPricingEngineWithoutCreditReportByDefault = (bool)reader["NewUserDefault_CanRunLpeWOCreditReport"];
            m_restrictWritingRolodexByDefault = (bool)reader["NewUserDefault_RestrictWritingRolodex"];
            m_restrictReadingRolodexByDefault = (bool)reader["NewUserDefault_RestrictReadingRolodex"];
            m_cannotDeleteLoanByDefault = (bool)reader["NewUserDefault_CannotDeleteLoan"];
            m_cannotCreateLoanTemplateByDefault = (bool)reader["NewUserDefault_CannotCreateLoanTemplate"];
            m_isDuplicateLoanCreatedWAutonameBit = (bool)reader["IsDuplicateLoanCreatedWAutonameBit"];
            m_is2ndLoanCreatedWAutonameBit = (bool)reader["Is2ndLoanCreatedWAutonameBit"];
            m_hasLONIntegration = (bool)reader["HasLONIntegration"];
            m_isBlankTemplateInvisibleForFileCreation = (bool)reader["IsBlankTemplateInvisibleForFileCreation"];
            m_isRoundUpLpeRateTo1Eighth = (bool)reader["IsRoundUpLpeRateTo1Eighth"];
            IsRateLockedAtSubmission = (bool)reader["IsRateLockedAtSubmission"];
            m_hasManyUsers = (bool)reader["HasManyUsers"];
            m_isAEAsOfficialLoanOfficer = (bool)reader["IsAEAsOfficialLoanOfficer"];
            m_emailAddrSendFromForNotif = (string)reader["EmailAddrSendFromForNotif"];
            m_nameSendFromForNotif = (string)reader["NameSendFromForNotif"];
            m_fthbCustomDefinition = (string)reader["FthbCustomDefinition"];
            //02/23/07 db - OPM 10612
            m_roundUpLpeFee = (bool)reader["IsRoundUpLpeFee"];
            m_roundUpLpeFeeInterval = Convert.ToDecimal(reader["RoundUpLpeFeeToInterval"]);

            m_IsDocMagicBarcodeScannerEnabled = (bool)reader["IsDocMagicBarcodeScannerEnabled"];
            m_IsDocuTechBarcodeScannerEnabled = (bool)reader["IsDocuTechBarcodeScannerEnabled"];
            m_IsIDSBarcodeScannerEnabled = (bool)reader["IsIDSBarcodeScannerEnabled"];


            //OPM 20141
            m_timezoneForRsExpiration = (string)reader["TimezoneForRsExpiration"];

            //OPM 9780
            m_allowPmlUserOrderNewCreditReport = (bool)reader["IsPmlUserAllowedToOrderCreditReport"];
            m_pmlRequireEstResidualIncome = (bool)reader["PmlRequireEstResidualIncome"];

            //OPM 2703
            m_requireVerifyLicenseByState = (bool)reader["RequireVerifyLicenseByState"];

            //OPM 19525
            m_displayPmlFeeIn100Format = (bool)reader["DisplayPmlFeeIn100Format"];

            //OPM 19717
            m_isUsingRateSheetExpirationFeature = (bool)reader["IsUsingRateSheetExpirationFeature"];

            //OPM 12711
            m_ShowUnderwriterInPMLCertificate = (bool)reader["IsUnderwriterInfoIncludedInPmlCertificate"];
            m_ShowProcessorInPMLCertificate = (bool)reader["IsProcessorInfoIncludedInPmlCertificate"];
            m_ShowJuniorUnderwriterInPMLCertificate = (bool)reader["IsJuniorUnderwriterInfoIncludedInPmlCertificate"];
            m_ShowJuniorProcessorInPMLCertificate = (bool)reader["IsJuniorProcessorInfoIncludedInPmlCertificate"];

            m_IsAlwaysUseBestPrice = (bool)reader["IsBestPriceAlwaysUsed"]; //opm 24214

            //OPM 84258
            m_IsEnableProvidentFundingSubservicingExport = (bool)reader["IsEnableProvidentFundingSubservicingExport"];

            m_IsEnableRateMonitorService = (bool)reader["IsEnableRateMonitorService"];

            m_lpePriceGroupIdDefault = reader.GetGuid("BrokerLpePriceGroupIdDefault", Guid.Empty);

            if (null != reader["ClosingCostTitleVendorId"])
            {
                m_ClosingCostTitleVendorId = Convert.ToInt32(reader["ClosingCostTitleVendorId"]);
            }

            m_IsEditLeadsInFullLoanEditor = (bool)reader["IsEditLeadsInFullLoanEditor"];

            #region opm 117681
            m_sCustomFieldDescriptionXml = reader["CustomFieldDescriptionXml"].ToString();
            ParseCustomFieldXml();
            #endregion

            //opm 118342
            foreach (string Key in m_sBrokerWideCustomFieldDescriptions.Keys)
            {
                String description;
                GetCustomFieldDescription(Key, out description);
                customFieldPrefixes.Add(Key.Substring(0, Key.Length - 4), description);
            }

            m_lpeLockPeriodAdj = Convert.ToInt32(reader["LpeLockPeriodAdj"]);
            m_numberBadPasswordsAllowed = Convert.ToInt32(reader["NumberBadPasswordsAllowed"]);
            m_lpeSubmitAgreement = (string)reader["LpeSubmitAgreement"];
            m_namingPattern = (string)reader["NamingPattern"];
            m_namingCounter = (long)reader["NamingCounter"];
            m_leadNamingPattern = (string)reader["LeadNamingPattern"];
            m_testNamingCounter = (long)reader["TestNamingCounter"];
            m_ecoaAddress = (string)reader["ECOAAddr"];
            m_emailAddressesForSystemChangeNotif = (string)reader["EmailAddressesForSystemChangeNotif"];
            m_notes = (string)reader["Notes"];
            m_creditMornetPlusUserID = (string)reader["CreditMornetPlusUserId"];

            if (maybeEncryptionKeyId.HasValue)
            {
                byte[] encryptedMornetPlusPassword = (byte[])reader["EncryptedCreditMornetPlusPassword"];
                this.lazyCreditMornetPlusPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedMornetPlusPassword));
            }
            else
            {
                string creditMornetPlusPassword = (string)reader["CreditMornetPlusPassword"];
                this.lazyCreditMornetPlusPassword = new Lazy<string>(() => creditMornetPlusPassword);
            }

            m_showCredcoOptionT = (E_ShowCredcoOptionT)Convert.ToInt32(reader["ShowCredcoOption"]);
            m_EDocsEnabledStatusT = (E_EDocsEnabledStatusT)Convert.ToInt32(reader["EdocsEnabledStatus"]);
            //m_isAsk3rdPartyUwResultInPml = (bool)reader["IsAsk3rdPartyUwResultInPml"]; removing opm 132564
            m_isAutoGenerateLiabilityPayOffConditions = (bool)reader["IsAutoGenerateLiabilityPayOffConditions"];
            m_isAllowExternalUserToCreateNewLoan = (bool)reader["IsAllowExternalUserToCreateNewLoan"];
            m_isShowPriceGroupInEmbeddedPml = (bool)reader["IsShowPriceGroupInEmbeddedPml"];
            m_pmlSubmitRLockDefaultT = Convert.ToInt32(reader["PmlSubmitRLockDefaultT"]);
            m_isLpeManualSubmissionAllowed = (bool)reader["IsLpeManualSubmissionAllowed"];
            m_isPmlPointImportAllowed = (bool)reader["IsPmlPointImportAllowed"];
            m_isAllPrepaymentPenaltyAllowed = (bool)reader["IsAllPrepaymentPenaltyAllowed"];
            m_isLpeDisqualificationEnabled = (bool)reader["IsLpeDisqualificationEnabled"];
            m_AllowInvestorDocMagicPlanCodes = (bool)reader["AllowInvestorDocMagicPlanCodes"];
            m_IsOnlyAllowApprovedDocMagicPlanCodes = (bool)reader["IsOnlyAllowApprovedDocMagicPlanCodes"];
            m_AutoSaveDocMagicGeneratedDocs = (bool)reader["IsAutoSaveDocMagicGeneratedDocs"];
            m_AutoSaveLeCdReceivedDate = (bool)reader["IsAutoSaveLeCdReceivedDate"];
            m_AutoSaveLeCdSignedDate = (bool)reader["IsAutoSaveLeCdSignedDate"];
            this.m_RequireAllBorrowersToReceiveCD = (bool)reader["RequireAllBorrowersToReceiveCD"];
            this.m_AutoSaveLeSignedDateWhenAnyBorrowerSigns = (bool)reader["AutoSaveLeSignedDateWhenAnyBorrowerSigns"];
            this.m_DefaultIssuedDateToToday = (bool)reader["DefaultIssuedDateToToday"];
            this.m_EnableCocRedisclosureTriggersForHelocLoans = (bool)reader["EnableCocRedisclosureTriggersForHelocLoans"];
            this.m_UseCustomCocFieldList = (bool)reader["UseCustomCocFieldList"];

            m_IsCreditUnion = (bool)reader["IsCreditUnion"];
            m_IsFederalCreditUnion = (bool)reader["IsFederalCreditUnion"];
            m_IsTaxExemptCreditUnion = (bool)reader["IsTaxExemptCreditUnion"];

            if (reader["PdfCustomFormAssignmentXmlContent"] != null)
            {
                m_pdfCustomFormAssignmentXmlContent = (string)reader["PdfCustomFormAssignmentXmlContent"];
            }
            if (reader["WholesaleBranchID"] != null)
            {
                this.defaultWholesaleBranchId = reader.GetGuid("WholesaleBranchID");
            }
            if (reader["WholesalePriceGroupID"] != null)
            {
                this.defaultWholesalePriceGroupId = reader.GetGuid("WholesalePriceGroupID");
            }
            if (reader["PmlLoanTemplateId"] != null)
            {
                m_pmlLoanTemplateID = reader.GetGuid("PmlLoanTemplateId");
            }
            if (reader["MiniCorrBranchID"] != null)
            {
                this.defaultMiniCorrBranchId = reader.GetGuid("MiniCorrBranchID");
            }
            if (reader["MiniCorrPriceGroupID"] != null)
            {
                this.defaultMiniCorrPriceGroupId = reader.GetGuid("MiniCorrPriceGroupID");
            }
            if (reader["MiniCorrLoanTemplateID"] != null)
            {
                m_miniCorrLoanTemplateID = reader.GetGuid("MiniCorrLoanTemplateID");
            }
            if (reader["CorrespondentBranchID"] != null)
            {
                this.defaultCorrBranchId = reader.GetGuid("CorrespondentBranchID");
            }
            if (reader["CorrespondentPriceGroupID"] != null)
            {
                this.defaultCorrPriceGroupId = reader.GetGuid("CorrespondentPriceGroupID");
            }
            if (reader["CorrLoanTemplateID"] != null)
            {
                m_corrLoanTemplateID = reader.GetGuid("CorrLoanTemplateID");
            }
            if (reader["BrokerPmlSiteId"] != null)
            {
                m_pmlSiteID = reader.GetGuid("BrokerPmlSiteId");
            }
            if (reader["NHCKey"] != null)
            {
                m_nhcKey = reader.GetGuid("NHCKey");
            }

            if (maybeEncryptionKeyId.HasValue)
            {
                byte[] encryptedTempOptionXmlContent = (byte[])reader["EncryptedTempOptionXmlContent"];
                this.lazyTempOptionXmlContent = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedTempOptionXmlContent));
            }
            else
            {
                string tempOptionXmlContent = (string)reader["TempOptionXmlContent"];
                this.lazyTempOptionXmlContent = new Lazy<string>(() => tempOptionXmlContent);
            }

            if (reader["BlockedCRAsXmlContent"] != null)
            {
                m_blockedCRAsXmlContent = (string)reader["BlockedCRAsXmlContent"];
            }

            if (reader["LpeLockDeskWorkHourEndTime"] != null)
            {
                m_lpeLockDeskWorkHourEndTime = (DateTime)reader["LpeLockDeskWorkHourEndTime"];
            }
            if (reader["LpeLockDeskWorkHourStartTime"] != null)
            {
                m_lpeLockDeskWorkHourStartTime = (DateTime)reader["LpeLockDeskWorkHourStartTime"];
            }

            m_PMLNavigationConfig = (string)reader["PMLNavigationConfig"];

            m_lpeMinutesNeededToLockLoan = Convert.ToInt32(reader["LpeMinutesNeededToLockLoan"]);
            m_lpeIsEnforceLockDeskHourForNormalUser = (bool)reader["LpeIsEnforceLockDeskHourForNormalUser"];
            m_isOptionARMUsedInPml = (bool)reader["IsOptionARMUsedInPml"];
            IsPmlSubmissionAllowed = (bool)reader["IsPmlSubmissionAllowed"];
            m_IsHelocsAllowedInPml = (bool)reader["IsHelocsAllowedInPml"];
            m_isStandAloneSecondAllowedInPml = (bool)reader["IsStandAloneSecondAllowedInPml"];
            m_is8020ComboAllowedInPml = (bool)reader["Is8020ComboAllowedInPml"];

            m_IsDataTracIntegrationEnabled = (bool)reader["IsDataTracIntegrationEnabled"]; // OPM 24590
            m_IsExportPricingInfoTo3rdParty = (bool)reader["IsExportPricingInfoTo3rdParty"]; // OPM 34444

            m_isBestPriceEnabled = (bool)reader["IsBestPriceEnabled"]; //av 22180  5 28 08 

            m_pmlUserAutoLoginOption = (string)reader["PmlUserAutoLoginOption"];

            m_pathToDataTrac = (string)reader["PathToDataTrac"];
            m_dataTracWebserviceURL = (string)reader["DataTracWebserviceUrl"];

            if (reader["QuickPricerTemplateId"] != null)
            {
                m_quickPricerTemplateId = reader.GetGuid("QuickPricerTemplateId");
            }
            m_isQuickPricerEnable = reader["IsQuickPricerEnable"] == null ? false : (bool)reader["IsQuickPricerEnable"];

            m_quickPricerDisclaimerAtResult = (string)reader["QuickPricerDisclaimerAtResult"];
            m_quickPricerNoResultMessage = (string)reader["QuickPricerNoResultMessage"];
            m_IsRateOptionsWorseThanLowerRateShownWithBestPriceView = (bool)reader["IsRateOptionsWorseThanLowerRateShownWithBestPriceView"];
            m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView = (bool)reader["IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView"];
            m_IsShowRateOptionsWorseThanLowerRate = (bool)reader["IsShowRateOptionsWorseThanLowerRate"];
            this.LegalEntityIdentifier = (string)reader["LegalEntityIdentifier"];
            this.isUseTasksInTpoPortal = (bool)reader["IsUseTasksInTpoPortal"];
            this.isShowPipelineOfTasksForAllLoansInTpoPortal = (bool)reader["IsShowPipelineOfTasksForAllLoansInTpoPortal"];
            this.isShowPipelineOfConditionsForAllLoansInTpoPortal = (bool)reader["IsShowPipelineOfConditionsForAllLoansInTpoPortal"];
            m_BillingVersion = (E_BrokerBillingVersion)Convert.ToInt16(reader["BillingVersion"]);
            m_DocMagicDocumentSavingOption = (E_DocMagicDocumentSavingOption)Convert.ToInt32(reader["DocMagicDocumentSavingOptionT"]);

            if (reader["BackupDocumentSavingOptionT"] != null)
            {
                m_BackupDocumentSavingOption = (E_DocMagicDocumentSavingOption)Convert.ToInt32(reader["BackupDocumentSavingOptionT"]);
            }

            if (reader["DocumentFrameworkCaptureEnabledBranchGroupT"] != null)
            {
                m_DocumentFrameworkCaptureEnabledBranchGroupT = (BranchGroupSelection)Convert.ToInt32(reader["DocumentFrameworkCaptureEnabledBranchGroupT"]);
            }

            m_DocMagicSplitESignedDocs = (bool)reader["DocMagicSplitESignedDocs"]; // OPM 236480
            m_DocMagicSplitUnsignedDocs = (bool)reader["DocMagicSplitUnsignedDocs"]; // OPM 236480
            m_IsPricingMultipleAppsSupported = (bool)reader["IsPricingMultipleAppsSupported"]; //opm 33530 fs 07/29/09
            m_IsaBEmailRequiredInPml = (bool)reader["isaBEmailRequiredInPml"]; //opm 33208 fs 08/21/09
            m_IsForceChangePasswordForPml = (bool)reader["IsForceChangePasswordForPml"];
            m_IsEDocsEnabled = (bool)reader["IsEDocsEnabled"];
            m_IsAllowSharedPipeline = (bool)reader["IsAllowSharedPipelineEnabled"]; //opm 34381 fs 08/31/09
            m_UseFHATOTALProductionAccount = (bool)reader["UseFHATOTALProductionAccount"]; // OPM 49148
            m_IsIncomeAssetsRequiredFhaStreamline = (bool)reader["IsIncomeAssetsRequiredFhaStreamline"]; //opm 42579 fs 11/17/09
            m_IsEncompassIntegrationEnabled = (bool)reader["IsEncompassIntegrationEnabled"]; //opm 45012 fs 01/21/10
            //opm 16821
            m_DDLSpecialConfigXmlContent = (string)reader["DDLSpecialConfigXmlContent"];
            m_IsHideFhaLenderIdInTotalScorecard = (bool)reader["IsHideFhaLenderIdInTotalScorecard"]; // OPM 48103
            m_FhaLenderId = (string)reader["FhaLenderId"]; // OPM 48103
            m_IsDay1DayOfRateLock = (bool)reader["IsDay1DayOfRateLock"];
            m_RateLockExpirationWeekendHolidayBehavior = (E_RateLockExpirationWeekendHolidayBehavior)Convert.ToInt32(reader["RateLockExpirationWeekendHolidayBehavior"]);
            m_isDocuTechEnabled = (bool)reader["IsDocuTechEnabled"];
            m_IsDocuTechStageEnabled = (bool)reader["IsDocuTechStageEnabled"];
            m_IsTotalAutoPopulateAUSResponse = (bool)reader["IsTotalAutoPopulateAUSResponse"];

            this.enableKtaIntegration = (bool)reader["EnableKtaIntegration"];

            m_IsEnabledGfeFeeService = (bool)reader["IsEnabledGfeFeeService"];
            IsEnableFannieMaeEarlyCheck = (bool)reader["IsEnableFannieMaeEarlyCheck"];
            IsEnableFreddieMacLoanQualityAdvisor = (bool)reader["IsEnableFreddieMacLoanQualityAdvisor"];

            // 9/20/2013 dd - OPM 106845
            this.FnmaEarlyCheckUserName = (string)reader["FnmaEarlyCheckUserName"];
            var fnmaEarlyCheckEncryptionKeyId = maybeEncryptionKeyId ?? EncryptionKeyIdentifier.FnmaEarlyCheck;
            byte[] fnmaEarlyCheckPasswordBytes = (byte[])reader["FnmaEarlyCheckPassword"];
            this.lazyFnmaEarlyCheckPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(fnmaEarlyCheckEncryptionKeyId, fnmaEarlyCheckPasswordBytes));
            this.FnmaEarlyCheckInstutionId = (string)reader["FnmaEarlyCheckInstutionId"];

            if (reader["OriginatorCompensationMigrationDate"] != null)
            {
                m_OriginatorCompensationMigrationDate = (DateTime)reader["OriginatorCompensationMigrationDate"];
            }
            m_OriginatorCompensationApplyMinYSPForTPOLoan = (bool)reader["OriginatorCompensationApplyMinYSPForTPOLoan"];


            if (reader["DefaultLockDeskId"] != null)
            {
                m_defaultLockDeskID = reader.GetGuid("DefaultLockDeskId");
            }

            m_IsUsePriceIncludingCompensationInPricingResult = (bool)reader["IsUsePriceIncludingCompensationInPricingResult"];
            m_IsDisplayCompensationChoiceInPml = (bool)reader["IsDisplayCompensationChoiceInPml"];

            m_IsHideLOCompPMLCert = (bool)reader["IsHideLOCompPMLCert"];
            m_IsHidePricingwithoutLOCompPMLCert = (bool)reader["IsHidePricingwithoutLOCompPMLCert"];

            m_IsAlwaysDisplayExactParRateOption = (bool)reader["IsAlwaysDisplayExactParRateOption"];
            m_IsUseNewCondition = (bool)reader["IsUseNewCondition"];
            m_IsUseNewTaskSystemStaticConditionIds = (bool)reader["IsUseNewTaskSystemStaticConditionIds"];

            m_RateLockQuestionsJson = (string)reader["RateLockQuestionsJson"];
            m_IsApplyPricingEngineRoundingAfterLoComp = (bool)reader["IsApplyPricingEngineRoundingAfterLoComp"];
            m_IsAddPointWithoutOriginatorCompensationToRateLock = (bool)reader["IsAddPointWithoutOriginatorCompensationToRateLock"];

            m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP = (bool)reader["IsDisplayRateOptionsAboveLowestTriggeringMaxYSP"];
            m_IsUseLayeredFinancing = (bool)reader["IsUseLayeredFinancing"];
            m_EnableAutoPriceEligibilityProcess = (bool)reader["EnableAutoPriceEligibilityProcess"];
            m_SendAutoLockToLockDesk = (bool)reader["SendAutoLockToLockDesk"];
            m_SaveLockConfOnAutoLock = (bool)reader["SaveLockConfOnAutoLock"];

            m_ShowQMStatusInPml2 = (bool)reader["ShowQMStatusInPml2"];

            m_feeTypeRequirementT = (E_FeeTypeRequirementT)Convert.ToInt32(reader["FeeTypeRequirementT"]);

            if (reader["CalculateclosingCostInPML"] != null)
            {
                m_CalculateclosingCostInPML = (bool)reader["CalculateclosingCostInPML"];
            }
            if (reader["ApplyClosingCostToGFE"] != null)
            {
                m_ApplyClosingCostToGFE = (bool)reader["ApplyClosingCostToGFE"];
            }

            if (reader["PmiProviderGenworthRanking"] != null)
            {
                m_PmiProviderGenworthRanking = Convert.ToInt32(reader["PmiProviderGenworthRanking"]);
            }
            if (reader["PmiProviderMgicRanking"] != null)
            {
                m_PmiProviderMgicRanking = Convert.ToInt32(reader["PmiProviderMgicRanking"]);
            }
            if (reader["PmiProviderRadianRanking"] != null)
            {
                m_PmiProviderRadianRanking = Convert.ToInt32(reader["PmiProviderRadianRanking"]);
            }
            if (reader["PmiProviderEssentRanking"] != null)
            {
                m_PmiProviderEssentRanking = Convert.ToInt32(reader["PmiProviderEssentRanking"]);
            }

            if (reader["PmiProviderNationalMIRanking"] != null)
            {
                m_PmiProviderNationalMIRanking = Convert.ToInt32(reader["PmiProviderNationalMIRanking"]);
            }

            if (reader["PmiProviderCMGRanking"] != null)
            {
                pmiProviderArchRanking = Convert.ToInt32(reader["PmiProviderCMGRanking"]);
            }

            if (reader["PmiProviderMassHousingRanking"] != null)
            {
                m_PmiProviderMassHousingRanking = Convert.ToInt32(reader["PmiProviderMassHousingRanking"]);
            }

            if (reader["IsHighestPmiProviderWins"] != null)
            {
                m_IsHighestPmiProviderWins = (bool)reader["IsHighestPmiProviderWins"];
            }

            m_IsImportDoDuLpFindingsAsConditions = (bool)reader["IsImportDuFindings"];

            m_CEExportConfig = ComplianceEaseExportConfiguration.Parse((string)reader["ComplianceEaseConfigXml"]);
            m_IsEnableNewConsumerPortal = (bool)reader["IsEnableNewConsumerPortal"];

            m_sLPQExportUrl = (string)reader["LPQBaseUrl"];

            m_IsAutoGenerateMersMin = (bool)reader["IsAutoGenerateMersMin"];
            m_MersCounter = (long)reader["MersCounter"];
            m_MersOrganizationId = (string)reader["MersOrganizationId"];

            m_IsAutoGenerateTradeNums = (bool)reader["IsAutoGenerateTradeNums"];
            m_TradeCounter = (long)reader["TradeCounter"];

            m_PoolCounter = (long)reader["PoolCounter"];

            if (reader["DefaultTaskPermissionLevelIdForImportedCategories"] != null)
            {
                m_DefaultTaskPermissionLevelIdForImportedCategories = Convert.ToInt32(reader["DefaultTaskPermissionLevelIdForImportedCategories"]);
            }

            if (reader["AusImportDefaultConditionCategoryId"] != null)
            {
                m_AusImportDefaultConditionCategoryId = Convert.ToInt32(reader["AusImportDefaultConditionCategoryId"]);
            }

            m_Pml2AsQuickPricerMode = (E_Pml2AsQuickPricerMode)Convert.ToInt32(reader["Pml2AsQuickPricerMode"]);

            try
            {
                // 11/9/2004 kb - We attempt to load and convert
                // at the same time.  If it fails, the object
                // should be null.
                //
                // 4/28/2005 kb - Per case #1724, we want to avoid
                // throwing this error for new brokers.

                if (reader["ConditionChoicesXmlContent"].ToString().TrimWhitespaceAndBOM() != "")
                {
                    m_conditionChoices = ChoiceList.ToObject((string)reader["ConditionChoicesXmlContent"]);
                }
            }
            catch (InvalidOperationException e)
            {
                Tools.LogError("Failed to convert broker's (" + m_name + ") condition choices.", e);
            }

            if (m_conditionChoices == null)
            {
                // 11/9/2004 kb - We're null and we're not taking
                // it any longer.  This probably occurred because
                // the database data is null or empty string.
                // This should only happen if the broker has never
                // used condition choices before and their cell
                // in the row is empty.  Once saved through the
                // choice editor, an empty set object should be
                // written back.

                m_conditionChoices = new ChoiceList();
            }

            try
            {
                // 12/10/2004 kb - Save custom option set for fixed
                // fields here.
                //
                // 4/28/2005 kb - Per case 1724, we want to avoid
                // throwing this error for new brokers.

                if (reader["FieldChoicesXmlContent"].ToString().TrimWhitespaceAndBOM() != "")
                {
                    m_fieldChoices = OptionSet.ToObject((string)reader["FieldChoicesXmlContent"]);
                }
            }
            catch (InvalidOperationException e)
            {
                Tools.LogError("Failed to convert broker's (" + m_name + ") field choices.", e);
            }

            if (m_fieldChoices == null)
            {
                // 12/10/2004 kb - Initialize what didn't parse so
                // we can have a valid copy during use and on save.
                //
                // Add new custom fields here.  We really need an
                // admin page for managing all the broker's settings.

                m_fieldChoices = new OptionSet();

                m_fieldChoices.Add(new FieldOptions());

                m_fieldChoices[0].FieldName = "sInternalFundingSrcDesc";
                m_fieldChoices[0].Description = "Internal Funding Source";

                m_fieldChoices.Add(new FieldOptions());

                m_fieldChoices[1].FieldName = "sExternalFundingSrcDesc";
                m_fieldChoices[1].Description = "External Funding Source";

                m_fieldChoices.Add(new FieldOptions());

                m_fieldChoices[2].FieldName = "sWarehouseFunderDesc";
                m_fieldChoices[2].Description = "Warehouse Funder";
            }

            m_LeadSourcesXmlContent = (string)reader["LeadSourcesXmlContent"];

            m_pmlCompanyTiersXmlContent = (string)reader["PmlCompanyTiersXmlContent"];

            //OPM 1438
            /*try
            {
                if( reader[ "RateLockPeriodsXmlContent" ].ToString().TrimWhitespaceAndBOM() != "" )
                {
                    m_rateLockPeriods = ItemList.ToObject( ( string ) reader[ "RateLockPeriodsXmlContent" ] );
                }
            }
            catch( Exception e )
            {
                Tools.LogError( "Failed to convert broker's (" + m_name + ") rate lock periods." , e );
            }

            if( m_rateLockPeriods == null )
            {
                m_rateLockPeriods = new ItemList();
            }*/

            try
            {
                // 8/11/2005 kb - We attempt to load and convert
                // at the same time.  If it fails, the object
                // should be null.  See case 2325 for more info.
                //
                // 8/11/2005 kb - Per case 1724, we want to avoid
                // throwing this error for new brokers.

                if (reader["NotifRuleXmlContent"].ToString().TrimWhitespaceAndBOM() != "")
                {
                    m_notifRules = NotificationRules.ToObject((string)reader["NotifRuleXmlContent"]);
                }
            }
            catch (InvalidOperationException e)
            {
                Tools.LogError("Failed to convert broker's (" + m_name + ") notif rules.", e);
            }

            if (m_notifRules == null)
            {
                // 8/11/2005 kb - We're null and we're not taking
                // it any longer.  This probably occurred because
                // the database data is null or empty string.
                // This should only happen if the broker has never
                // used notif rules before and their cell in the
                // row is empty.  Once saved through the source
                // editor, an empty set object should be written
                // back.

                m_notifRules = new NotificationRules();
            }

            m_fairLendingNoticeAddress = new String[10];

            for (int i = 1; i <= 10; i++)
            {
                m_fairLendingNoticeAddress[i - 1] = (string)reader["FairLendingNoticeAddrLine" + i];
            }

            m_licensePlanType = SafeConvert.ToString(reader["LicenseCurrentPlanType"]).TrimWhitespaceAndBOM(); // Trim because Thinh created this field as char instead of varchar
            if (reader["LicenseCurrentPlanStartD"] != null)
            {
                m_licensePlanStartD = Convert.ToDateTime(reader["LicenseCurrentPlanStartD"]);
            }

            m_sNmlsIdentifier = (string)reader["NmlsIdentifier"];

            string licenseXmlContent = (string)reader["LicenseXmlContent"];
            if (string.IsNullOrEmpty(licenseXmlContent))
            {
                LicenseInformationList = new LicenseInfoList();
            }
            else
            {
                LicenseInformationList = LicenseInfoList.ToObject(licenseXmlContent);
            }
            if (reader["EdocsStackOrderList"] != null)
            {
                m_EdocsStackOrderList = (string)reader["EdocsStackOrderList"];
            }

            IsDisplayChoiceUfmipInLtvCalc = (bool)reader["IsDisplayChoiceUfmipInLtvCalc"];

            this.m_EnableDsiPrintAndDeliverOption = (bool)reader["EnableDsiPrintAndDeliverOption"];

            if (reader["IsCenlarEnabled"] != null)
            {
                this.isCenlarEnabled = (bool)reader["IsCenlarEnabled"];
            }

            this.isEnableComplianceEagleIntegration = (bool)reader["IsEnableComplianceEagleIntegration"];
            this.isEnablePTMComplianceEagleAuditIndicator = (bool)reader["IsEnablePTMComplianceEagleAuditIndicator"];
            this.complianceEagleUserName = (string)reader["ComplianceEagleUserName"];
            byte[] complianceEaglePasswordBytes = (byte[])reader["ComplianceEaglePassword"];
            EncryptionKeyIdentifier compEagleEncKeyId = maybeEncryptionKeyId ?? EncryptionKeyIdentifier.ComplianceEagle;
            this.lazyComplianceEaglePassword = new Lazy<string>(() => EncryptionHelper.DecryptString(compEagleEncKeyId, complianceEaglePasswordBytes));

            this.complianceEagleCompanyID = (string)reader["ComplianceEagleCompanyID"];

            if (reader["ComplianceEagleEnableMismo34"] != null)
            {
                this.complianceEagleEnableMismo34 = (bool)reader["ComplianceEagleEnableMismo34"];
            }

            this.complianceEagleAuditConfigData = SerializationHelper.JsonNetDeserialize<ComplianceEagleAuditConfigurationData>(
                (string)reader["ComplianceEagleAuditConfigurationDataJson"]);

            if (reader["OCRVendorId"] != null)
            {
                this.m_OCRVendorId = (Guid)reader["OCRVendorId"];
                this.m_OCRUsername = (string)reader["OCRUsername"];

                byte[] ocrPasswordBytes = (byte[])reader["OCRPassword"];
                var ocrEncKeyId = maybeEncryptionKeyId ?? EncryptionKeyIdentifier.OCR;
                this.lazyOCRPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(ocrEncKeyId, ocrPasswordBytes));
            }

            if (reader["ComplianceEaseUserName"] != null)
            {
                m_ComplianceEaseUserName = (string)reader["ComplianceEaseUserName"];
            }

            if (!maybeEncryptionKeyId.HasValue && reader["ComplianceEasePassword"] != null)
            {
                string complianceEasePassword = (string)reader["ComplianceEasePassword"];
                this.lazyComplianceEasePassword = new Lazy<string>(() => EncryptionHelper.Decrypt(complianceEasePassword));
            }
            else if (maybeEncryptionKeyId.HasValue && reader["EncryptedComplianceEasePassword"] != null)
            {
                byte[] passwordBytes = (byte[])reader["EncryptedComplianceEasePassword"];
                this.lazyComplianceEasePassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, passwordBytes));
            }

            if (reader["DocMagicIsAllowuserCustomLogin"] != null)
            {
                m_DocMagicIsAllowuserCustomLogin = (bool)reader["DocMagicIsAllowuserCustomLogin"];
            }

            if (reader["DocMagicDefaultDocTypeId"] != null)
            {
                m_DocMagicDefaultDocTypeID = Convert.ToInt32(reader["DocMagicDefaultDocTypeId"]);
            }
            else
            {
                m_DocMagicDefaultDocTypeID = null;
            }

            if (reader["ESignedDocumentDocTypeId"] != null)
            {
                m_ESignedDocumentDocTypeId = Convert.ToInt32(reader["ESignedDocumentDocTypeId"]);
            }
            else
            {
                m_ESignedDocumentDocTypeId = null;
            }

            m_IsEnableDriveIntegration = (bool)reader["IsEnableDriveIntegration"];
            m_IsEnableDriveConditionImporting = (bool)reader["IsEnableDriveConditionImporting"];
            if (reader["DriveConditionCategoryID"] != null)
            {
                m_DriveConditionCategoryID = Convert.ToInt32(reader["DriveConditionCategoryID"]);
            }
            if (reader["DriveDocTypeId"] != null)
            {
                m_DriveDocTypeID = Convert.ToInt32(reader["DriveDocTypeId"]);
            }
            m_DriveDueDateCalculationOffset = Convert.ToInt32(reader["DriveDueDateCalculationOffset"]);
            m_DriveDueDateCalculatedFromField = (string)reader["DriveDueDateCalculatedFromField"];
            if (reader["DriveTaskPermissionLevelID"] != null)
            {
                m_DriveTaskPermissionLevelID = Convert.ToInt32(reader["DriveTaskPermissionLevelID"]);
            }

            #region Flood Integration //OPM 12758
            m_IsEnableFloodIntegration = (bool)reader["IsEnableFloodIntegration"];
            m_IsCCPmtRequiredForFloodIntegration = (bool)reader["IsCCPmtRequiredForFloodIntegration"];
            m_FloodProviderId = Convert.ToInt16(reader["FloodProvider"]);
            if (reader["FloodDocTypeId"] != null)
            {
                m_FloodDocTypeId = Convert.ToInt32(reader["FloodDocTypeId"]);
            }
            m_FloodAccountId = (string)reader["FloodAccountId"];

            if (reader["FloodResellerId"] != null)
            {
                m_FloodResellerId = reader.GetGuid("FloodResellerId");
            }
            #endregion

            m_AvailableLockPeriodOptionsCSV = (string)reader["AvailableLockPeriodOptionsCSV"];
            m_AvailableLockPeriodOptionsCSVOriginal = m_AvailableLockPeriodOptionsCSV;

            m_IsEnableComplianceEaseIntegration = (bool)reader["IsEnableComplianceEaseIntegration"];
            m_IsEnablePTMComplianceEaseIndicator = (bool)reader["IsEnablePTMComplianceEaseIndicator"];

            m_IsEnablePTMDocMagicSeamlessInterface = (bool)reader["IsEnablePTMDocMagicSeamlessInterface"];
            m_NmlsCallReportOriginationRelatedRevenueJSONContent = (string)reader["NmlsCallReportOriginationRelatedRevenueJSONContent"];

            this.isKivaIntegrationEnabled = (bool)reader["IsKivaIntegrationEnabled"];

            m_IsForceLeadToLoanTemplate = (bool)reader["IsForceLeadToLoanTemplate"];
            m_IsForceLeadToUseLoanCounter = (bool)reader["IsForceLeadToUseLoanCounter"];
            m_IsForceLeadToLoanNewLoanNumber = (bool)reader["IsForceLeadToLoanNewLoanNumber"];
            m_IsForceLeadToLoanRemoveLeadPrefix = (bool)reader["IsForceLeadToLoanRemoveLeadPrefix"];

            m_IsNewPmlUIEnabled = (bool)reader["IsNewPmlUIEnabled"];
            m_IsAllowOrderingGlobalDmsAppraisals = (bool)reader["IsAllowOrderingGlobalDmsAppraisals"];

            m_CustomPmlFieldList = CustomPmlFieldList.ToObject((string)reader["CustomPmlFieldXML"], ConstApp.MAX_NUMBER_CUSTOM_PML_FIELDS);
            m_MersFormat = (string)reader["MersFormat"];
            m_IsDocVendorTesting = (bool)reader["IsDocVendorTesting"];

            m_CustomPricingPolicyFieldList = CustomPmlFieldList.ToObject((string)reader["CustomPricingPolicyFieldXml"], ConstApp.MAX_NUMBER_CUSTOM_PRICING_POLICY_FIELDS);

            m_EnableDuplicateEDocProtectionChecking = (bool)reader["EnableDuplicateEDocProtectionChecking"];

            m_defaultLockPolicyID = reader.GetNullableGuid("DefaultLockPolicyID");

            m_EnableSequentialNumbering = (bool)reader["EnableSequentialNumbering"];
            m_SequentialNumberingFieldId = (string)reader["SequentialNumberingFieldId"];
            m_SequentialNumberingSeed = (long)reader["SequentialNumberingSeed"];

            #region DU Import Options //OPM 94140
            m_IsImportDuFindingsByDefault = (bool)reader["IsImportDuFindingsByDefault"];
            m_IsImportDu1003ByDefault = (bool)reader["IsImportDu1003ByDefault"];
            m_IsImportDuCreditReportByDefault = (bool)reader["IsImportDuCreditReportByDefault"];
            #endregion

            //m_PopulateGFEWithLockDatesFromFrontEndLock = (bool)reader["PopulateGFEWithLockDatesFromFrontEndLock"];
            m_TriggerAprRediscNotifForAprDecrease = (bool)reader["TriggerAprRediscNotifForAprDecrease"];

            this.relationshipWithFreddieMac = (LenderRelationshipWithFreddieMac)(byte)reader["RelationshipWithFreddieMac"];

            m_IsRemovePreparedDates = (bool)reader["IsRemovePreparedDates"];
            m_IsProtectDisclosureDates = (bool)reader["IsProtectDisclosureDates"];
            m_IsEnableDTPkgBsdRedisclosureDatePop = (bool)reader["IsEnableDTPkgBsdRedisclosureDatePop"];

            m_IsB4AndB6DisabledIn1100And1300OfGfe = (bool)reader["IsB4AndB6DisabledIn1100And1300OfGfe"];
            m_ForceTemplateOnImport = (bool)reader["ForceTemplateOnImport"];

            m_EnableConversationLogForLoans = (bool)reader["EnableConversationLogForLoans"];
            m_EnableConversationLogForLoansInTpo = (bool)reader["EnableConversationLogForLoansInTpo"];

            if (reader["EmployeeResourcesURL"] != null)
            {
                m_EmployeeResourcesUrl = (string)reader["EmployeeResourcesURL"];
            }
            else
            {
                m_EmployeeResourcesUrl = "";
            }

            m_EnabledStatusesByChannelAsJsonOnLoad = (string)reader["EnabledStatusesByChannelAsJson"];

            m_EnabledStatusesByCorrespondentProcessTypeAsJsonOnLoad = (string)reader["EnabledStatusesByCorrespondentProcessTypeAsJson"];

            m_allowNonExcludableDiscountPoints = (bool)reader["AllowNonExcludableDiscountPoints"];

            m_activeDirectoryURL = (string)reader["ActiveDirectoryUrl"];
            m_isEnableMultiFactorAuthentication = (bool)reader["IsEnableMultiFactorAuthentication"];
            m_isEnableMultiFactorAuthenticationTPO = (bool)reader["IsEnableMultiFactorAuthenticationTPO"];
            this.m_AllowDeviceRegistrationViaAuthCodePage = (bool)reader["AllowDeviceRegistrationViaAuthCodePage"]; 
            this.m_IsOptInMultiFactorAuthentication = (bool)reader["IsOptInMultiFactorAuthentication"];
            this.m_OptOutMfaName = (string)reader["OptOutMultiFactorAuthenticationName"];
            this.m_OptOutMfaDate = reader["OptOutMultiFactorAuthenticationDate"] != null ? (DateTime)reader["OptOutMultiFactorAuthenticationDate"] : DateTime.MinValue;

            m_MinimumDefaultClosingCostDataSet = (E_sClosingCostFeeVersionT)Convert.ToInt32(reader["MinimumDefaultClosingCostDataSet"]);
            m_EnableCustomaryEscrowImpoundsCalculation = (bool)reader["EnableCustomaryEscrowImpoundsCalculation"];

            m_IsAllowAllUsersToUseKayako = (bool)reader["AllowAllUsersToUseKayako"];

            this.IsVendorTestSuite = (bool)reader["IsVendorTestSuite"];
            this.suiteType = (BrokerSuiteType)Convert.ToInt32(reader["SuiteType"]);

            m_SandboxScratchLOXml = (string)reader["SandboxScratchLOXml"];

            m_AllowRateMonitorForQP2FilesInTpoPml = (bool)reader["AllowRateMonitorForQP2FilesInTpoPml"];
            m_AllowRateMonitorForQP2FilesInEmdeddedPml = (bool)reader["AllowRateMonitorForQP2FilesInEmdeddedPml"];

            m_DeterminationRulesXMLContent = (string)reader["DeterminationRulesXMLContent"];

            this.borrPdCompMayNotExceedLenderPdComp = (bool)reader["BorrPdCompMayNotExceedLenderPdComp"];
            this.ClosingCostFeeSetupJsonContent = (string)reader["ClosingCostFeeSetupJsonContent"];
            this.Version = reader.GetByteArrayField("Version");
            this.linkedLoanUpdateFieldsString = (string)reader["LinkedLoanUpdateFieldsString"];

            this.m_selectedNewLoanEditorUIPermissionType = (E_PermissionType)reader["SelectedNewLoanEditorUIPermissionType"];
            this.m_maxWebServiceDailyCallVolume = (int)reader["MaxWebServiceDailyCallVolume"];
            this.RolodexSystemIdCounter = (int)reader["RolodexSystemIdCounter"];
            this.ActualPricingBrokerId = (Guid)reader["ActualPricingBrokerId"];

            this.tpoNewFeatureAlphaTestingGroup = reader["TpoNewFeatureAlphaTestingGroup"] as Guid?;
            this.tpoNewFeatureBetaTestingGroup = reader["TpoNewFeatureBetaTestingGroup"] as Guid?;

            this.tpoRedisclosureAllowedGroupType = (NewTpoFeatureOcGroupAccess)reader["TpoRedisclosureAllowedGroupType"];
            this.tpoRedisclosureFormSource = (TpoRequestFormSource)reader["TpoRedisclosureFormSource"];
            this.tpoRedisclosureFormCustomPdfId = reader["TpoRedisclosureFormCustomPdfId"] as Guid?;
            this.tpoRedisclosureFormHostedUrl = reader["TpoRedisclosureFormHostedUrl"] as string;
            this.tpoRedisclosureFormRequiredDocType = reader["TpoRedisclosureFormRequiredDocType"] as int?;

            this.tpoInitialClosingDisclosureAllowedGroupType = (NewTpoFeatureOcGroupAccess)reader["TpoInitialClosingDisclosureAllowedGroupType"];
            this.tpoInitialClosingDisclosureFormSource = (TpoRequestFormSource)reader["TpoInitialClosingDisclosureFormSource"];
            this.origPortalAntiSteeringDisclosureAccess = (OrigPortalAntiSteeringDisclosureAccessType)Convert.ToInt32(reader["OrigPortalAntiSteeringDisclosureAccess"]);
            this.opNonQmMsgReceivingEmail = (string)reader["OpNonQmMsgReceivingEmail"];
            this.opConLogCategoryId = reader["OpConLogCategoryId"] == null ? -1 : (long)reader["OpConLogCategoryId"];
            this.tpoInitialClosingDisclosureFormCustomPdfId = reader["TpoInitialClosingDisclosureFormCustomPdfId"] as Guid?;
            this.tpoInitialClosingDisclosureFormHostedUrl = reader["TpoInitialClosingDisclosureFormHostedUrl"] as string;
            this.tpoInitialClosingDisclosureFormRequiredDocType = reader["TpoInitialClosingDisclosureFormRequiredDocType"] as int?;
            this.workflowRulesControllerId = reader["WorkflowRulesControllerId"] as Guid?;
            this.enableLqbMobileApp = (bool)reader["EnableLqbMobileApp"];

            this.m_EnableRetailTpoPortalMode = (bool)reader["EnableRetailTpoPortalMode"];
            this.m_retailTpoLoanTemplateID = reader["RetailTPOLoanTemplateId"] as Guid?;

            this.m_EnableRenovationLoanSupport = (bool)reader["EnableRenovationLoanSupport"];

            this.allowIndicatingVerbalCreditReportAuthorization = (bool)reader["AllowIndicatingVerbalCreditReportAuthorization"];
            this.verbalCreditReportAuthorizationCustomPdfId = reader["VerbalCreditReportAuthorizationCustomPdfId"] as Guid?;
            this.showLoanProductIdentifier = (bool)reader["ShowLoanProductIdentifier"];
            this.EnableEConsentMonitoringAutomation = (bool)reader["EnableEConsentMonitoringAutomation"];
            #endregion
        }

        /// <summary>
        /// Either update broker information or create new broker
        /// record, depends on IsNew status.
        /// </summary>
        /// <returns>
        /// True - if save successfully
        /// </returns>
        public bool Save()
        {
            // Save this broker as is.  If new, it won't be after this
            // save call.  We use our single transaction and rollback
            // on failure.

            bool ret = false;

            try
            {
                
                using( CStoredProcedureExec spExec = new CStoredProcedureExec(this.BrokerID) )
                {
                    try
                    {
                        spExec.BeginTransactionForWrite();

                        ret = Save( spExec );

                        spExec.CommitTransaction();

                        LogOnSuccessfulSave();
                    }
                    catch
                    {
                        spExec.RollbackTransaction();

                        throw;
                    }
                }
            }
            catch( Exception e )
            {
                Tools.LogError( "Failed to save broker " + m_brokerID + "." , e );
            }

            return ret;
        }

        /// <summary>
        /// Called during save only.
        /// TODO: instead of throwing an exception, maybe have it return a list of errors, then block the save.
        /// </summary>
        private void PreSaveValidate()
        {
            if (false == IsEnableHELOC && IsEnableHelocToggleOptionInQP2)
            {
                var msg = "Cannot enable heloc toggling in qp 2.0 if broker does not have helocs enabled.";
                throw new CBaseException(msg, msg);
            }

            if (!this.UseRespaForNonRetailInitialDisclosures && this.ReqDocCheckDAndRespaForNonRetailInitialDisclosures)
            {
                var msg = "Must enable option to use RESPA for non-retail initial disclosure trigger " +
                    "before enabling the option to also require existence of document check date.";
                throw new CBaseException(msg, msg);
            }

            if (!this.IsOptInMultiFactorAuthentication &&
                (string.IsNullOrEmpty(this.OptOutMfaName) || this.OptOutMfaDate == DateTime.MinValue))
            {
                var msg = "Please fill out all fields under the Opt Out of MFA setting.";
                throw new CBaseException(msg, msg);
            }

            if(this.EnableConversationLogForLoansInTpo && !this.EnableConversationLogForLoans)
            {
                var msg = "You must have 'Enable Conversation Log' checked if you have 'Enable in Originator Portal' checked.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsProductCodeFilterOptionInTempOptions())
            {
                if ((this.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && !this.IsNewPmlUIEnabled) ||
                    (this.IsQuickPricerEnable && this.Pml2AsQuickPricerMode != E_Pml2AsQuickPricerMode.Enabled))
                {
                    var msg = "CANNOT SAVE SETTINGS!! \n\nThe temporary broker bit to enable the advanced filter options requires that the New PML UI be enabled and that, if Quick Pricer is enabled, then QP 2.0 should be used.";
                    throw new CBaseException(msg, msg);
                }
            }

            if (this.IsNonQmOpSettingsInTempOptions())
            {
                if (!this.HasFeatures(E_BrokerFeatureT.PriceMyLoan) || 
                    !this.IsNewPmlUIEnabled ||
                    !this.IsQuickPricerEnable || 
                    this.Pml2AsQuickPricerMode != E_Pml2AsQuickPricerMode.Enabled)
                {
                    var msg = "CANNOT SAVE SETTINGS!! \n\nThe temporary broker bit to enable the non-QM QP in OP requires that the New PML UI and the QP 2.0 enabled.";
                    throw new CBaseException(msg, msg);
                }
            }

            if (!this.HistoricalPricingSettings.ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid employee group in the historical pricing settings.";
                throw new CBaseException(msg, msg);
            }

            if (!this.InternalPricerUiSettings.ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid employee group in the internal pricer ui setting.";
                throw new CBaseException(msg, msg);
            }

            if (!this.NonQmOpSettings.ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid employee group in the non-QM OP settings.";
                throw new CBaseException(msg, msg);
            }

            if (!this.NonQmOpSettings.ValidateOCGroup())
            {
                var msg = "Please provide a valid OC group in the non-QM OP settings.";
                throw new CBaseException(msg, msg);
            }

            var email = EmailAddress.Create(this.OpNonQmMsgReceivingEmail);
            if (this.opNonQmMsgReceivingEmailSet && !string.IsNullOrWhiteSpace(this.OpNonQmMsgReceivingEmail) && !email.HasValue)
            {
                var msg = "The non-QM email address is invalid.";
                throw new CBaseException(msg, msg);
            }

            if (!this.GetCrossBrowserSettings().ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid employee group in the cross browser enabled setting.";
                throw new CBaseException(msg, msg);
            }

            if (!this.PmlResults3PricingSettings.ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid employee group in the beta pml results 3 ui setting.";
                throw new CBaseException(msg, msg);
            }

            if (!this.PmlResults3PricingSettings.ValidateOCGroup())
            {
                var msg = "Please provide a valid OC group in the beta pml results 3 ui setting.";
                throw new CBaseException(msg, msg);
            }

            if (!this.PmlResults3PricingSettings.ValidateForcePML3())
            {
                var msg = "Please provide a valid input for ForcePML3 in the beta pml results 3 ui setting.";
                throw new CBaseException(msg, msg);
            }

            if (!this.seamlessLpaSettings.Value.ValidateOcGroup())
            {
                var msg = "Please provide a valid OC group name for the Seamless LPA temp option.";
                throw new CBaseException(msg, msg);
            }

            if (!this.seamlessLpaSettings.Value.ValidateEmployeeGroup())
            {
                var msg = "Please provide a valid Employee group name for Seamless LPA temp option.";
                throw new CBaseException(msg, msg);
            }

            if (!string.IsNullOrWhiteSpace(this.LegalEntityIdentifier))
            {
                var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
                var driver = factory.Create();

                var isMatch = driver.IsMatch(RegularExpressionString.LegalEntityIdentifier, this.LegalEntityIdentifier);
                if (!isMatch)
                {
                    throw new CBaseException(ErrorMessages.InvalidLegalEntityIdentifier, ErrorMessages.InvalidLegalEntityIdentifier + " LEI: " + this.LegalEntityIdentifier);
                }
            }

            if (this.tpoRedisclosureAllowedGroupTypeSet ||
                this.tpoInitialClosingDisclosureAllowedGroupTypeSet)
            {
                var hasAlphaGroup = this.TpoNewFeatureAlphaTestingGroup.HasValue;
                var hasBetaGroup = this.TpoNewFeatureBetaTestingGroup.HasValue;

                if (this.tpoRedisclosureAllowedGroupTypeSet &&
                    this.TpoRedisclosureAllowedGroupType == NewTpoFeatureOcGroupAccess.Alpha && 
                    !hasAlphaGroup)
                {
                    var message = "Please select an Alpha testing group to allow redisclosure access.";
                    throw new CBaseException(message, message);
                }

                if (this.tpoRedisclosureAllowedGroupTypeSet &&
                    this.TpoRedisclosureAllowedGroupType == NewTpoFeatureOcGroupAccess.AlphaAndBeta &&
                    !(hasAlphaGroup && hasBetaGroup))
                {
                    var message = "Please select an Alpha and Beta testing group to allow redisclosure access.";
                    throw new CBaseException(message, message);
                }

                if (this.tpoInitialClosingDisclosureAllowedGroupTypeSet &&
                    this.TpoInitialClosingDisclosureAllowedGroupType == NewTpoFeatureOcGroupAccess.Alpha &&
                    !hasAlphaGroup)
                {
                    var message = "Please select an Alpha testing group to allow initial closing disclosure access.";
                    throw new CBaseException(message, message);
                }

                if (this.tpoInitialClosingDisclosureAllowedGroupTypeSet &&
                    this.TpoInitialClosingDisclosureAllowedGroupType == NewTpoFeatureOcGroupAccess.AlphaAndBeta &&
                    !(hasAlphaGroup && hasBetaGroup))
                {
                    var message = "Please select an Alpha and Beta testing group to allow initial closing disclosure access.";
                    throw new CBaseException(message, message);
                }
            }

            if (this.tpoRedisclosureFormHostedUrlSet && !string.IsNullOrEmpty(this.tpoRedisclosureFormHostedUrl))
            {
                Uri result;
                if (!Uri.TryCreate(this.tpoRedisclosureFormHostedUrl, UriKind.RelativeOrAbsolute, out result) ||
                    result.Scheme != Uri.UriSchemeHttps ||
                    result.AbsoluteUri.Length > TPOPortalConfig.SCHEMA_URL_LENGTH_LIMIT)
                {
                    var message = "Please provide a valid HTTPS URL for the hosted Originator Portal CoC/Redisclosure form.";
                    throw new CBaseException(message, message + " URI: " + this.tpoRedisclosureFormHostedUrl);
                }
            }

            if (this.tpoInitialClosingDisclosureFormHostedUrlSet && !string.IsNullOrEmpty(this.tpoInitialClosingDisclosureFormHostedUrl))
            {
                Uri result;
                if (!Uri.TryCreate(this.tpoInitialClosingDisclosureFormHostedUrl, UriKind.RelativeOrAbsolute, out result) ||
                    result.Scheme != Uri.UriSchemeHttps ||
                    result.AbsoluteUri.Length > TPOPortalConfig.SCHEMA_URL_LENGTH_LIMIT)
                {
                    var message = "Please provide a valid HTTPS URL for the hosted Originator Portal initial closing disclosure form.";
                    throw new CBaseException(message, message + " URI: " + this.tpoInitialClosingDisclosureFormHostedUrl);
                }
            }

            if (this.suiteTypeSet && this.SuiteType == BrokerSuiteType.Blank)
            {
                throw new CBaseException(ErrorMessages.InvalidBrokerSuiteType, ErrorMessages.InvalidBrokerSuiteType);
            }

            if (this.isTempOptionXmlContentSet)
            {
                if (!this.UsingLegacyDocTypes && this.HideEnhancedDocTypes)
                {
                    var message = "Temp option to hide enhanced loan documentation types is present but temp option to use legacy doc types is missing. " +
                        "This will prevent any documentation types from being displayed and is not allowed.";
                    throw new CBaseException(message, message);
                }
            }
        }

        /// <summary>
        /// Either update broker information or create new broker
        /// record, depends on IsNew status.
        /// </summary>
        /// <returns>
        /// True - if save successfully
        /// </returns>
        public bool Save(CStoredProcedureExec spExec)
        {
            bool ret = false;
            ArrayList parameters = new ArrayList();
            SqlParameter brokerIDParam = new SqlParameter("@BrokerID", SqlDbType.UniqueIdentifier);
            SqlParameter pmlSiteIDParam = new SqlParameter("@BrokerPmlSiteID", SqlDbType.UniqueIdentifier);
            pmlSiteIDParam.Direction = brokerIDParam.Direction = ParameterDirection.Output;

            string procedureName = null;

            if (m_isNew)
            {
                procedureName = "CreateBroker";

                parameters.Add(pmlSiteIDParam);
                parameters.Add(brokerIDParam);

                this.EncryptionKeyId = EncryptionHelper.GenerateNewKey();
                parameters.Add(new SqlParameter("@EncryptionKeyId", this.EncryptionKeyId.Value));
            }
            else
            {
                procedureName = "UpdateBroker";

                parameters.Add(new SqlParameter("@BrokerID", m_brokerID));
            }

            // Reset the lazy fields to ensure that we are validating against the most recent
            // field values.
            this.ResetLazyFields();
            this.PreSaveValidate();

            #region Parameter Setting
            //opm 120255
            parameters.Add(new SqlParameter("@ClosingCostTitleVendorId", m_ClosingCostTitleVendorId));

            // 9/17/2013 dd - OPM 106845
            parameters.Add(new SqlParameter("@IsEnableFannieMaeEarlyCheck", IsEnableFannieMaeEarlyCheck));
            parameters.Add(new SqlParameter("@IsEnableFreddieMacLoanQualityAdvisor", IsEnableFreddieMacLoanQualityAdvisor));

            if (FnmaEarlyCheckUserName != null)
            {
                parameters.Add(new SqlParameter("@FnmaEarlyCheckUserName", FnmaEarlyCheckUserName));
            }
            if (this.fnmaEarlyCheckPasswordSet)
            {
                byte[] earlyCheckPassword = EncryptionHelper.EncryptString(
                    this.EncryptionKeyId == default(EncryptionKeyIdentifier) ? EncryptionKeyIdentifier.FnmaEarlyCheck : this.EncryptionKeyId,
                    this.lazyFnmaEarlyCheckPassword.Value);
                parameters.Add(new SqlParameter("@FnmaEarlyCheckPassword", earlyCheckPassword ?? new byte[0]));
            }
            if (FnmaEarlyCheckInstutionId != null)
            {
                parameters.Add(new SqlParameter("@FnmaEarlyCheckInstutionId", FnmaEarlyCheckInstutionId));
            }
            if (m_name != null)
            {
                parameters.Add(new SqlParameter("@Name", m_name));
            }

            if (m_url != null)
            {
                parameters.Add(new SqlParameter("@Url", m_url));
            }
            if (m_address != null)
            {
                parameters.Add(new SqlParameter("@Address", m_address.StreetAddress));
                parameters.Add(new SqlParameter("@City", m_address.City));
                parameters.Add(new SqlParameter("@State", m_address.State));
                parameters.Add(new SqlParameter("@Zipcode", m_address.Zipcode));
            }
            if (m_phone != null)
            {
                parameters.Add(new SqlParameter("@Phone", m_phone));
            }
            if (m_fax != null)
            {
                parameters.Add(new SqlParameter("@Fax", m_fax));
            }
            if (m_licenseNumber != null)
            {
                parameters.Add(new SqlParameter("@LicenseNumber", m_licenseNumber));
            }
            if (m_fthbCustomDefinition != null)
            {
                parameters.Add(new SqlParameter("@FthbCustomDefinition", m_fthbCustomDefinition));
            }
            if (m_roundUpLpeFeeSet)
            {
                parameters.Add(new SqlParameter("@IsRoundUpLpeFee", m_roundUpLpeFee));
            }
            if (m_displayPmlFeeIn100FormatSet) //OPM 19525
            {
                parameters.Add(new SqlParameter("@DisplayPmlFeeIn100Format", m_displayPmlFeeIn100Format));
            }
            if (m_isUsingRateSheetExpirationFeatureSet) //OPM 19525
            {
                parameters.Add(new SqlParameter("@IsUsingRateSheetExpirationFeature", m_isUsingRateSheetExpirationFeature));
            }
            if (m_roundUpLpeFeeIntervalSet)
            {
                parameters.Add(new SqlParameter("@RoundUpLpeFeeToInterval", m_roundUpLpeFeeInterval));
            }
            if (m_timezoneForRsExpirationSet)
            {
                parameters.Add(new SqlParameter("@TimezoneForRsExpiration", m_timezoneForRsExpiration));
            }
            if (m_pricePerSeatSet)
            {
                parameters.Add(new SqlParameter("@PricePerSeat", m_pricePerSeat));
            }
            if (m_allowPmlUserOrderNewCreditReportSet) //OPM 9780
            {
                parameters.Add(new SqlParameter("@IsPmlUserAllowedToOrderCreditReport", m_allowPmlUserOrderNewCreditReport));
            }
            if (m_pmlRequireEstResidualIncomeSet)
            {
                parameters.Add(new SqlParameter("@PmlRequireEstResidualIncome", m_pmlRequireEstResidualIncome));
            }
            if (m_requireVerifyLicenseByStateSet) //OPM 2703
            {
                parameters.Add(new SqlParameter("@RequireVerifyLicenseByState", m_requireVerifyLicenseByState));
            }
            if (m_accountManager != null)
            {
                parameters.Add(new SqlParameter("@AccountManager", m_accountManager));
            }
            if (m_IsTotalAutoPopulateAUSResponseSet)
            {
                parameters.Add(new SqlParameter("@IsTotalAutoPopulateAUSResponse", m_IsTotalAutoPopulateAUSResponse));
            }
            if (m_IsEnabledGfeFeeServiceSet)
            {
                parameters.Add(new SqlParameter("@IsEnabledGfeFeeService", m_IsEnabledGfeFeeService));
            }
            if (m_customerCode != null)
            {
                parameters.Add(new SqlParameter("@CustomerCode", m_customerCode));
            }
            if (m_isOptionTelemarketerCanOnlyAssignToManagerSet)
            {
                parameters.Add(new SqlParameter("@OptionTelemarketerCanOnlyAssignToManager", m_optionTelemarketerCanOnlyAssignToManager));
            }
            if (m_isOptionTelemarketerCanRunPrequalSet)
            {
                parameters.Add(new SqlParameter("@OptionTelemarketerCanRunPrequal", m_optionTelemarketerCanRunPrequal));
            }
            if (m_EnableConversationLogForLoansSet)
            {
                parameters.Add(new SqlParameter("@EnableConversationLogForLoans", m_EnableConversationLogForLoans));
            }
            if (m_EnableConversationLogForLoansInTpoSet)
            {
                parameters.Add(new SqlParameter("@EnableConversationLogForLoansInTpo", m_EnableConversationLogForLoansInTpo));
            }
            if (m_hasLenderDefaultFeaturesSet) 
            {
                parameters.Add(new SqlParameter("@HasLenderDefaultFeatures", m_hasLenderDefaultFeatures));
            }
            if (m_isLOAllowedToEditProcessorAssignedFileSet) 
            {
                parameters.Add(new SqlParameter("@IsLOAllowedToEditProcessorAssignedFile", m_isLOAllowedToEditProcessorAssignedFile));
            }
            if (m_isOnlyAccountantCanModifyTrustAccountSet) 
            {
                parameters.Add(new SqlParameter("@IsOnlyAccountantCanModifyTrustAccount", m_isOnlyAccountantCanModifyTrustAccount));
            }
            if (m_isOthersAllowedToEditUnderwriterAssignedFileSet)
            {
                parameters.Add(new SqlParameter("@IsOthersAllowedToEditUnderwriterAssignedFile", m_isOthersAllowedToEditUnderwriterAssignedFile));
            }
            if (m_isAllowViewLoanInEditorByDefaultSet)
            {
                parameters.Add(new SqlParameter("@IsAllowViewLoanInEditorByDefault", m_isAllowViewLoanInEditorByDefault));
            }
            if (m_canRunPricingEngineWithoutCreditReportByDefaultSet)
            {
                parameters.Add(new SqlParameter("@NewUserDefault_CanRunLpeWOCreditReport", m_canRunPricingEngineWithoutCreditReportByDefault));
            }
            if (m_restrictWritingRolodexByDefaultSet)
            {
                parameters.Add(new SqlParameter("@NewUserDefault_RestrictWritingRolodex", m_restrictWritingRolodexByDefault));
            }
            if (m_restrictReadingRolodexByDefaultSet)
            {
                parameters.Add(new SqlParameter("@NewUserDefault_RestrictReadingRolodex", m_restrictReadingRolodexByDefault));
            }
            if (m_cannotDeleteLoanByDefaultSet)
            {
                parameters.Add(new SqlParameter("@NewUserDefault_CannotDeleteLoan", m_cannotDeleteLoanByDefault));
            }
            if (m_cannotCreateLoanTemplateByDefaultSet)
            {
                parameters.Add(new SqlParameter("@NewUserDefault_CannotCreateLoanTemplate", m_cannotCreateLoanTemplateByDefault));
            }
            if (m_isDuplicateLoanCreatedWAutonameBitSet)
            {
                parameters.Add(new SqlParameter("@IsDuplicateLoanCreatedWAutonameBit", m_isDuplicateLoanCreatedWAutonameBit));
            }
            if (m_is2ndLoanCreatedWAutonameBitSet)
            {
                parameters.Add(new SqlParameter("@Is2ndLoanCreatedWAutonameBit", m_is2ndLoanCreatedWAutonameBit));
            }
            if (m_hasLONIntegrationSet)
            {
                parameters.Add(new SqlParameter("@HasLONIntegration", m_hasLONIntegration));
            }
            if (m_isBlankTemplateInvisibleForFileCreationSet)
            {
                parameters.Add(new SqlParameter("@IsBlankTemplateInvisibleForFileCreation", m_isBlankTemplateInvisibleForFileCreation));
            }
            if (m_isRoundUpLpeRateTo1EighthSet)
            {
                parameters.Add(new SqlParameter("@IsRoundUpLpeRateTo1Eighth", m_isRoundUpLpeRateTo1Eighth));
            }
            if (m_isRateLockedAtSubmissionSet)
            {
                parameters.Add(new SqlParameter("@IsRateLockedAtSubmission", m_isRateLockedAtSubmission));
            }
            if (m_hasManyUsersSet)
            {
                parameters.Add(new SqlParameter("@HasManyUsers", m_hasManyUsers));
            }
            if( m_conditionChoices != null )
            {
                Tools.LogInfo("Saving condition choices: " + m_conditionChoices.ToString());
                parameters.Add(new SqlParameter("@ConditionChoicesXmlContent", m_conditionChoices.ToString() ));
            }
            if( m_fieldChoices != null )
            {
                parameters.Add(new SqlParameter("@FieldChoicesXmlContent", m_fieldChoices.ToString() ));
            }
            if( m_LeadSourcesXmlContent != null )
            {
                parameters.Add(new SqlParameter("@LeadSourcesXmlContent", m_LeadSourcesXmlContent ));
            }

            if (m_pmlCompanyTiersXmlContent != null)
            {
                parameters.Add(new SqlParameter("@PmlCompanyTiersXmlContent", m_pmlCompanyTiersXmlContent));
            }
            
            //OPM 1438
            /*if( m_rateLockPeriods != null )
            {
                parameters.Add(new SqlParameter("@RateLockPeriodsXmlContent", m_rateLockPeriods.ToString() ));
            }*/
            if( m_notifRules != null )
            {
                parameters.Add(new SqlParameter("@NotifRuleXmlContent", m_notifRules.ToString() ));
            }
            if (m_emailAddrSendFromForNotif != null)
            {
                parameters.Add(new SqlParameter("@EmailAddrSendFromForNotif", m_emailAddrSendFromForNotif));
            }
            if (m_nameSendFromForNotif != null)
            {
                parameters.Add(new SqlParameter("@NameSendFromForNotif", m_nameSendFromForNotif));
            }
            if (m_lpePriceGroupIdDefaultSet)
            {
                parameters.Add(new SqlParameter("@BrokerLpePriceGroupIdDefault", m_lpePriceGroupIdDefault));
            }
            if (m_sCustomFieldDescriptionXmlSet)
            {
                parameters.Add(new SqlParameter("@CustomFieldDescriptionXml", m_sCustomFieldDescriptionXml));
            }

            if (m_lpeSubmitAgreement != null)
            {
                parameters.Add(new SqlParameter("@LpeSubmitAgreement", m_lpeSubmitAgreement));
            }
            if (m_lpeLockPeriodAdjSet)
            {
                parameters.Add(new SqlParameter("@LpeLockPeriodAdj", m_lpeLockPeriodAdj));
            }
            if (m_numberBadPasswordsAllowedSet)
            {
                parameters.Add(new SqlParameter("@NumberBadPasswordsAllowed", m_numberBadPasswordsAllowed));
            }
            if (m_maxWebServiceDailyCallVolumeSet)
            {
                parameters.Add(new SqlParameter("@MaxWebServiceDailyCallVolume", m_maxWebServiceDailyCallVolume));
            }
            if (m_namingPattern != null) 
            {
                parameters.Add(new SqlParameter("@NamingPattern", m_namingPattern));
            }
            if (m_namingCounterSet)
            {
                parameters.Add(new SqlParameter("@NamingCounter", m_namingCounter));
            }
            if (m_leadNamingPattern != null)
            {
                parameters.Add(new SqlParameter("@LeadNamingPattern", m_leadNamingPattern));
            }
            if (m_testNamingCounterSet)
            {
                parameters.Add(new SqlParameter("@TestNamingCounter", m_testNamingCounter));
            }
            if (m_ecoaAddress != null) 
            {
                parameters.Add(new SqlParameter("@ECOAAddr", m_ecoaAddress));
            }
            if (m_pdfCustomFormAssignmentXmlContent != null) 
            {
                parameters.Add(new SqlParameter("@PdfCustomFormAssignmentXmlContent", m_pdfCustomFormAssignmentXmlContent));
            }

            if (this.isTempOptionXmlContentSet) 
            {
                parameters.Add(new SqlParameter("@TempOptionXmlContent", this.lazyTempOptionXmlContent.Value));
                if (this.EncryptionKeyId != default(EncryptionKeyIdentifier))
                {
                    parameters.Add(new SqlParameter(
                        "@EncryptedTempOptionXmlContent",
                        EncryptionHelper.EncryptString(this.EncryptionKeyId, this.lazyTempOptionXmlContent.Value)));
                }
            }

            if (m_creditMornetPlusUserID != null) 
            {
                parameters.Add(new SqlParameter("@CreditMornetPlusUserID", m_creditMornetPlusUserID));
            }
            if (this.creditMornetPlusPasswordSet) 
            {
                parameters.Add(new SqlParameter("@CreditMornetPlusPassword", this.lazyCreditMornetPlusPassword.Value));
                if (this.EncryptionKeyId != default(EncryptionKeyIdentifier))
                {
                    parameters.Add(new SqlParameter(
                        "@EncryptedCreditMornetPlusPassword",
                        EncryptionHelper.EncryptString(this.EncryptionKeyId, this.lazyCreditMornetPlusPassword.Value) ?? new byte[0]));
                }
            }
            if (m_fairLendingNoticeAddress != null) 
            {
                for (int i = 1; i <= m_fairLendingNoticeAddress.Length; i++) 
                {
                    parameters.Add(new SqlParameter("@FairLendingNoticeAddr" + i, m_fairLendingNoticeAddress[i - 1]));
                }
            }
            if (m_isPmlLoanTemplateIDSet) 
            {
                parameters.Add(new SqlParameter("@PmlLoanTemplateID", m_pmlLoanTemplateID));
            }
            if (m_isMiniCorrLoanTemplateIDSet)
            {
                parameters.Add(new SqlParameter("@MiniCorrLoanTemplateID", m_miniCorrLoanTemplateID));
            }
            if (m_isCorrLoanTemplateIDSet)
            {
                parameters.Add(new SqlParameter("@CorrLoanTemplateID", m_corrLoanTemplateID));
            }
            if (m_isRetailTpoLoanTemplateIDSet)
            {
                parameters.Add(new SqlParameter("@RetailTPOLoanTemplateId", m_retailTpoLoanTemplateID));
            }
            if (m_defaultLockDeskIDSet)
                parameters.Add(new SqlParameter("@DefaultLockDeskId", m_defaultLockDeskID));

            //if (m_isAsk3rdPartyUwResultInPmlSet) // removing opm 132564
            //{
            //    parameters.Add(new SqlParameter("@IsAsk3rdPartyUwResultInPml", m_isAsk3rdPartyUwResultInPml));
            //}
            if (m_nhcKeySet)
            {
                parameters.Add(new SqlParameter("@NHCKey", m_nhcKey));
            }
            if (m_emailAddressesForSystemChangeNotif != null)
            {
                parameters.Add(new SqlParameter("@EmailAddressesForSystemChangeNotif", m_emailAddressesForSystemChangeNotif));
            }
            if (m_notes != null)
            {
                parameters.Add(new SqlParameter("@Notes", m_notes));
            }
            if (m_showCredcoOptionTSet) 
            {
                parameters.Add(new SqlParameter("@ShowCredcoOption", m_showCredcoOptionT));
            }
            if (m_EDocsEnabledStatusTSet)
            {
                parameters.Add(new SqlParameter("@EDocsEnabledStatusT", m_EDocsEnabledStatusT));
            }
            if (m_isAEAsOfficialLoanOfficerSet) 
            {
                parameters.Add(new SqlParameter("@IsAEAsOfficialLoanOfficer", m_isAEAsOfficialLoanOfficer));
            }

            if (m_CalculateclosingCostInPMLSet)
            {
                parameters.Add(new SqlParameter("@CalculateclosingCostInPML", m_CalculateclosingCostInPML));
            }

            if (m_ApplyClosingCostToGFESet)
            {
                parameters.Add(new SqlParameter("@ApplyClosingCostToGFE", m_ApplyClosingCostToGFE));
            }

            if (m_PmiProviderGenworthRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderGenworthRanking", m_PmiProviderGenworthRanking));
            }
            if (m_PmiProviderMgicRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderMgicRanking", m_PmiProviderMgicRanking));
            }
            if (m_PmiProviderRadianRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderRadianRanking", m_PmiProviderRadianRanking));
            }
            if (m_PmiProviderEssentRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderEssentRanking", m_PmiProviderEssentRanking));
            }

            if (pmiProviderArchRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderCMGRanking", pmiProviderArchRanking));
            }

            if (m_PmiProviderNationalMIRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderNationalMIRanking", m_PmiProviderNationalMIRanking));
            }

            if (m_PmiProviderMassHousingRankingSet)
            {
                parameters.Add(new SqlParameter("@PmiProviderMassHousingRanking", m_PmiProviderMassHousingRanking));
            }

            if (m_IsHighestPmiProviderWinsSet)
            {
                parameters.Add(new SqlParameter("@IsHighestPmiProviderWins", m_IsHighestPmiProviderWins));
            }

            if (m_licensePlanType != null)
            {
                parameters.Add(new SqlParameter("@LicenseCurrentPlanType", m_licensePlanType)) ;
            }
            if (m_licensePlanStartD != DateTime.MinValue)
                parameters.Add(new SqlParameter("@LicenseCurrentPlanStartD", m_licensePlanStartD)) ;
            
            if (m_blockedCRAsXmlContentSet) 
            {
                parameters.Add(new SqlParameter("@BlockedCRAsXmlContent", m_blockedCRAsXmlContent));
            }
            if (m_isAutoGenerateLiabilityPayOffConditionsSet)
            {
                parameters.Add(new SqlParameter("@IsAutoGenerateLiabilityPayOffConditions", m_isAutoGenerateLiabilityPayOffConditions));
            }

            if (m_isAllowExternalUserToCreateNewLoanSet)
            {
                parameters.Add(new SqlParameter("@IsAllowExternalUserToCreateNewLoan", m_isAllowExternalUserToCreateNewLoan));
            }

            if (m_isShowPriceGroupInEmbeddedPmlSet)
            {
                parameters.Add(new SqlParameter("@IsShowPriceGroupInEmbeddedPml", m_isShowPriceGroupInEmbeddedPml));
            }
            if (m_pmlSubmitRLockDefaultTSet)
            {
                parameters.Add(new SqlParameter("@PmlSubmitRLockDefaultT", m_pmlSubmitRLockDefaultT));
            }
            if (m_isLpeManualSubmissionAllowedSet)
            {
                parameters.Add(new SqlParameter("@IsLpeManualSubmissionAllowed", m_isLpeManualSubmissionAllowed));
            }
            if (m_isPmlPointImportAllowedSet)
            {
                parameters.Add(new SqlParameter("@IsPmlPointImportAllowed", m_isPmlPointImportAllowed));
            }
            if (m_isAllPrepaymentPenaltyAllowedSet)
            {
                parameters.Add(new SqlParameter("@IsAllPrepaymentPenaltyAllowed", m_isAllPrepaymentPenaltyAllowed));
            }
            if (m_isLpeDisqualificationEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsLpeDisqualificationEnabled", m_isLpeDisqualificationEnabled));
            }

            if (m_lpeLockDeskWorkHourEndTimeSet) 
            {
                if(!m_isUsingRateSheetExpirationFeature)
                    parameters.Add(new SqlParameter("@LpeLockDeskWorkHourEndTime", SmallDateTime.MinValue));
                else
                    parameters.Add(new SqlParameter("@LpeLockDeskWorkHourEndTime", m_lpeLockDeskWorkHourEndTime));
            }

            if (m_lpeMinutesNeededToLockLoanSet) 
            {
                parameters.Add(new SqlParameter("@LpeMinutesNeededToLockLoan", m_lpeMinutesNeededToLockLoan));
            }

            if (m_lpeLockDeskWorkHourStartTimeSet)
            {
                if(!m_isUsingRateSheetExpirationFeature || !m_lpeIsEnforceLockDeskHourForNormalUser)
                    parameters.Add(new SqlParameter("@LpeLockDeskWorkHourStartTime", SmallDateTime.MinValue));
                else
                    parameters.Add(new SqlParameter("@LpeLockDeskWorkHourStartTime", m_lpeLockDeskWorkHourStartTime));
            }
            if (m_lpeIsEnforceLockDeskHourForNormalUserSet) 
            {
                parameters.Add(new SqlParameter("@LpeIsEnforceLockDeskHourForNormalUser", m_lpeIsEnforceLockDeskHourForNormalUser));
            }
            if (m_isOptionARMUsedInPmlSet) 
            {
                parameters.Add(new SqlParameter("@IsOptionARMUsedInPml", m_isOptionARMUsedInPml));
            }
            if (m_isPmlSubmissionAllowedSet) 
            {
                parameters.Add(new SqlParameter("@IsPmlSubmissionAllowed", m_isPmlSubmissionAllowed));
            }
            if (m_isStandAloneSecondAllowedInPmlSet) 
            {
                parameters.Add(new SqlParameter("@IsStandAloneSecondAllowedInPml", m_isStandAloneSecondAllowedInPml));
            }
            if (m_IsHelocsAllowedInPmlSet)
            {
                parameters.Add(new SqlParameter("@IsHelocsAllowedInPml", IsHelocsAllowedInPml));
            }
            if (m_is8020ComboAllowedInPmlSet) 
            {
                parameters.Add(new SqlParameter("@Is8020ComboAllowedInPml", m_is8020ComboAllowedInPml));
            }

            if (m_IsExportPricingInfoTo3rdPartySet) // OPM 34444
            {
                parameters.Add(new SqlParameter("@IsExportPricingInfoTo3rdParty", m_IsExportPricingInfoTo3rdParty));
            }

            if ( m_IsDataTracIntegrationEnabledSet )
            {
                parameters.Add( new SqlParameter("@IsDataTracIntegrationEnabled", m_IsDataTracIntegrationEnabled ));
            }
            if ( m_IsPathToDataTracSet ) 
            {
                parameters.Add( new SqlParameter("@PathToDataTrac", m_pathToDataTrac ));
            }
            
            if ( m_IsDataTracWebserviceURLSet ) // OPM 25798 
            {
                parameters.Add( new SqlParameter("@DataTracWebServiceUrl", m_dataTracWebserviceURL ));
            }

            if ( m_isBestPriceEnabledSet ) 
            {
                parameters.Add(new SqlParameter("@IsBestPriceEnabled", m_isBestPriceEnabled));
            }
            if (m_DDLSpecialConfigXmlContentSet) 
            {
                parameters.Add( new SqlParameter("@DDLSpecialConfigXmlContent", m_DDLSpecialConfigXmlContent));
            }
            //OPM 12711
            if(m_ShowUnderwriterInPMLCertificateSet)
            {
                parameters.Add( new SqlParameter("@IsUnderwriterInfoIncludedInPmlCertificate", m_ShowUnderwriterInPMLCertificate));
            }
            if(m_ShowJuniorUnderwriterInPMLCertificateSet)
            {
                parameters.Add( new SqlParameter("@IsJuniorUnderwriterInfoIncludedInPmlCertificate", m_ShowJuniorUnderwriterInPMLCertificate));
            }
            if(m_ShowProcessorInPMLCertificateSet)
            {
                parameters.Add( new SqlParameter("@IsProcessorInfoIncludedInPmlCertificate", m_ShowProcessorInPMLCertificate));
            }
            if(m_ShowJuniorProcessorInPMLCertificateSet)
            {
                parameters.Add( new SqlParameter("@IsJuniorProcessorInfoIncludedInPmlCertificate", m_ShowJuniorProcessorInPMLCertificate));
            }
            if(m_IsAlwaysUseBestPriceSet)
            {
                parameters.Add( new SqlParameter("@IsBestPriceAlwaysUsed", m_IsAlwaysUseBestPrice));
            }
            if (m_pmlUserAutoLoginOptionSet) 
            {
                parameters.Add(new SqlParameter("@PmlUserAutoLoginOption", m_pmlUserAutoLoginOption));
            }
            if (m_isQuickPricerEnableSet)
            {
                parameters.Add(new SqlParameter("@IsQuickPricerEnable", m_isQuickPricerEnable));
            }
            if (m_isQuickPricerTemplateIdSet)
            {
                parameters.Add(new SqlParameter("@QuickPricerTemplateId", m_quickPricerTemplateId));
            }
            if (m_isQuickPricerDisclaimerAtResultSet)
            {
                parameters.Add(new SqlParameter("@QuickPricerDisclaimerAtResult", m_quickPricerDisclaimerAtResult));
            }
            if (m_isQuickPricerNoResultMessageSet)
            {
                parameters.Add(new SqlParameter("@QuickPricerNoResultMessage", m_quickPricerNoResultMessage));
            }
            if (m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceViewSet)
            {
                parameters.Add(new SqlParameter("@IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView", m_IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView));
            }
            if (m_IsRateOptionsWorseThanLowerRateShownWithBestPriceViewSet)
            {
                parameters.Add(new SqlParameter("@IsRateOptionsWorseThanLowerRateShownWithBestPriceView", m_IsRateOptionsWorseThanLowerRateShownWithBestPriceView));
            }
            if (m_IsShowRateOptionsWorseThanLowerRateSet)
            {
                parameters.Add(new SqlParameter("IsShowRateOptionsWorseThanLowerRate", m_IsShowRateOptionsWorseThanLowerRate));
            }
            if (this.legalEntityIdentifierSet)
            {
                parameters.Add(new SqlParameter("@LegalEntityIdentifier", this.legalEntityIdentifier));
            }
            if (this.isUseTasksInTpoPortalSet)
            {
                parameters.Add(new SqlParameter("@IsUseTasksInTpoPortal", this.IsUseTasksInTpoPortal));
            }
            if (this.isShowPipelineOfTasksForAllLoansInTpoPortalSet)
            {
                parameters.Add(new SqlParameter("@IsShowPipelineOfTasksForAllLoansInTpoPortal", this.isShowPipelineOfTasksForAllLoansInTpoPortal));
            }
            if (this.isShowPipelineOfConditionsForAllLoansInTpoPortalSet)
            {
                parameters.Add(new SqlParameter("@IsShowPipelineOfConditionsForAllLoansInTpoPortal", this.isShowPipelineOfConditionsForAllLoansInTpoPortal));
            }
            if (m_BillingVersionSet)
            {
                parameters.Add(new SqlParameter("@BillingVersion", m_BillingVersion));
            }
            parameters.Add(new SqlParameter("@NmlsIdentifier", m_sNmlsIdentifier));
            if (null == m_licenseInformationList)
            {
                parameters.Add(new SqlParameter("@LicenseXmlContent", ""));
            }
            else
            {
                parameters.Add(new SqlParameter("@LicenseXmlContent", m_licenseInformationList.ToString()));
            }
            if (m_IsPricingMultipleAppsSupportedSet) //opm 33530 fs 07/29/09
            {
                parameters.Add(new SqlParameter("@IsPricingMultipleAppsSupported", m_IsPricingMultipleAppsSupported));
            }

            if (m_IsaBEmailRequiredInPmlSet) //opm 33208 fs 08/21/09
            {
                parameters.Add(new SqlParameter("@IsaBEmailRequiredInPml", m_IsaBEmailRequiredInPml));
            }

            if (m_IsForceChangePasswordForPmlSet) // opm 34274
            {
                parameters.Add(new SqlParameter("@IsForceChangePasswordForPml", m_IsForceChangePasswordForPml));
            }

            if (m_UseFHATOTALProductionAccountSet) // OPM 41948
            {
                parameters.Add(new SqlParameter("@UseFHATOTALProductionAccount", m_UseFHATOTALProductionAccount));
            }
            if (m_IsAllowSharedPipelineSet)
            {
                parameters.Add(new SqlParameter("@IsAllowSharedPipelineEnabled", m_IsAllowSharedPipeline)); //opm 34381 fs 08/31/09
            }
            if (m_IsIncomeAssetsRequiredFhaStreamlineSet)
            {
                parameters.Add(new SqlParameter("@IsIncomeAssetsRequiredFhaStreamline", m_IsIncomeAssetsRequiredFhaStreamline)); //opm 42579 fs 11/17/09
            }

            if (m_IsEDocsEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsEDocsEnabled", m_IsEDocsEnabled));
            }

            if (m_IsEncompassIntegrationEnabledSet) //opm 45012 fs 01/21/10
            {
                parameters.Add(new SqlParameter("@IsEncompassIntegrationEnabled", m_IsEncompassIntegrationEnabled));
            }
            if (m_IsEditLeadsInFullLoanEditorSet)
            {
                parameters.Add(new SqlParameter("@IsEditLeadsInFullLoanEditor", m_IsEditLeadsInFullLoanEditor));
            }
            if (m_FhaLenderIdSet) // OPM 48103
            {
                parameters.Add(new SqlParameter("@FhaLenderId", m_FhaLenderId));
            }

            if (m_IsHideFhaLenderIdInTotalScorecardSet) // OPM 48103
            {
                parameters.Add(new SqlParameter("@IsHideFhaLenderIdInTotalScorecard", m_IsHideFhaLenderIdInTotalScorecard));
            }
            if(m_IsDay1DayOfRateLockSet)
            {
                parameters.Add(new SqlParameter("@IsDay1DayOfRateLock", m_IsDay1DayOfRateLock));
            }
            if (m_isDocuTechEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsDocuTechEnabled", m_isDocuTechEnabled));
            }
            if (m_IsDocuTechStageEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsDocuTechStageEnabled", m_IsDocuTechStageEnabled));
            }
            if (m_OriginatorCompensationApplyMinYSPForTPOLoanSet)
            {
                parameters.Add(new SqlParameter("@OriginatorCompensationApplyMinYSPForTPOLoan", m_OriginatorCompensationApplyMinYSPForTPOLoan));
            }

            if (m_LPQBaseUrlSet)
            {
                parameters.Add(new SqlParameter("@LPQBaseUrl", m_sLPQExportUrl));
            }

            //since it can be null always send it
            parameters.Add(new SqlParameter("@OriginatorCompensationMigrationDate", m_OriginatorCompensationMigrationDate));

            if (m_IsDisplayCompensationChoiceInPmlSet)
            {
                parameters.Add(new SqlParameter("@IsDisplayCompensationChoiceInPml", m_IsDisplayCompensationChoiceInPml));
            }

            if (m_IsHideLOCompPMLCertSet)
            {
                parameters.Add(new SqlParameter("@IsHideLOCompPMLCert", m_IsHideLOCompPMLCert));
            }

            if (m_IsHidePricingwithoutLOCompPMLCertSet)
            {
                parameters.Add(new SqlParameter("@IsHidePricingwithoutLOCompPMLCert", m_IsHidePricingwithoutLOCompPMLCert));
            }

            if (m_IsDocMagicBarcodeScannerEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsDocMagicBarcodeScannerEnabled", m_IsDocMagicBarcodeScannerEnabled));
            }
            if (m_IsDocuTechBarcodeScannerEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsDocuTechBarcodeScannerEnabled", m_IsDocuTechBarcodeScannerEnabled));
            }
            if (m_IsIDSBarcodeScannerEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsIDSBarcodeScannerEnabled", m_IsIDSBarcodeScannerEnabled));
            }

            if (m_IsUsePriceIncludingCompensationInPricingResultSet)
            {
                parameters.Add(new SqlParameter("@IsUsePriceIncludingCompensationInPricingResult", m_IsUsePriceIncludingCompensationInPricingResult));
            }

            if (m_IsAlwaysDisplayExactParRateOptionSet)
            {
                parameters.Add(new SqlParameter("@IsAlwaysDisplayExactParRateOption", m_IsAlwaysDisplayExactParRateOption)); 
            }

            if (false == string.IsNullOrEmpty(m_EdocsStackOrderList))
            {
                parameters.Add(new SqlParameter("@EdocsStackOrderList", m_EdocsStackOrderList));
            }
            if (m_IsDisplayChoiceUfmipInLtvCalcSet)
            {
                parameters.Add(new SqlParameter("@IsDisplayChoiceUfmipInLtvCalc", m_IsDisplayChoiceUfmipInLtvCalc));
            }
            if (m_IsImportDoDuLpFindingsAsConditionsSet)
            {
                parameters.Add(new SqlParameter("@IsImportDuFindings", m_IsImportDoDuLpFindingsAsConditions));
            }
            if (m_IsCEExportConfigSet)
            {
                parameters.Add(new SqlParameter("@ComplianceEaseConfigXml", m_CEExportConfig.Serialize()));
            }

            if (m_IsEnableNewConsumerPortalSet)
            {
                parameters.Add(new SqlParameter("@IsEnableNewConsumerPortal", m_IsEnableNewConsumerPortal));
            }

            if (m_PMLNavigationConfigSet)
            {
                parameters.Add(new SqlParameter("@PMLNavigationConfig", m_PMLNavigationConfig));
            }

            if (m_feeTypeRequirementTSet)
            {
                parameters.Add(new SqlParameter("@FeeTypeRequirementT", m_feeTypeRequirementT));
            }

            if (m_EnableDsiPrintAndDeliverOptionSet)
            {
                parameters.Add(new SqlParameter("@EnableDsiPrintAndDeliverOption", m_EnableDsiPrintAndDeliverOption));
            }

            if (this.isCenlarEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsCenlarEnabled", this.isCenlarEnabled));
            }

            //always added because this can be nullable.
            parameters.Add(new SqlParameter("@DefaultTaskPermissionLevelIdForImportedCategories", m_DefaultTaskPermissionLevelIdForImportedCategories));
            parameters.Add(new SqlParameter("@AusImportDefaultConditionCategoryId", m_AusImportDefaultConditionCategoryId));


            parameters.Add(new SqlParameter("@IsUseNewCondition", m_IsUseNewCondition));
            parameters.Add(new SqlParameter("@IsUseNewTaskSystemStaticConditionIds", m_IsUseNewTaskSystemStaticConditionIds));

            parameters.Add(new SqlParameter("@RateLockExpirationWeekendHolidayBehavior", m_RateLockExpirationWeekendHolidayBehavior));

            parameters.Add(new SqlParameter("@RateLockQuestionsJson", m_RateLockQuestionsJson));

            if (m_IsApplyPricingEngineRoundingAfterLoCompSet)
            {
                parameters.Add(new SqlParameter("@IsApplyPricingEngineRoundingAfterLoComp", m_IsApplyPricingEngineRoundingAfterLoComp));
            }

            if (m_IsAddPointWithoutOriginatorCompensationToRateLockSet)
            {
                parameters.Add(new SqlParameter("@IsAddPointWithoutOriginatorCompensationToRateLock", m_IsAddPointWithoutOriginatorCompensationToRateLock));
            }

            if (m_IsAutoGenerateMersMinSet)
            {
                parameters.Add(new SqlParameter("@IsAutoGenerateMersMin", m_IsAutoGenerateMersMin));
            }
            if (m_MersCounterSet)
            {
                parameters.Add(new SqlParameter("@MersCounter", m_MersCounter));
            }
            if (m_MersOrganizationIdSet)
            {
                parameters.Add(new SqlParameter("@MersOrganizationId", m_MersOrganizationId));
            }

            if (m_IsAutoGenerateTradeNumsSet)
            {
                parameters.Add(new SqlParameter("@IsAutoGenerateTradeNums", m_IsAutoGenerateTradeNums));
            }
            if (m_TradeCounterSet)
            {
                parameters.Add(new SqlParameter("@TradeCounter", m_TradeCounter));
            }

            if (m_PoolCounterSet)
            {
                parameters.Add(new SqlParameter("@PoolCounter", m_PoolCounter));
            }

            if (m_IsCreditUnionSet)
            {
                parameters.Add(new SqlParameter("@IsCreditUnion", m_IsCreditUnion));
            }

            if (m_IsFederalCreditUnionSet)
            {
                parameters.Add(new SqlParameter("@IsFederalCreditUnion", m_IsFederalCreditUnion));
            }

            if (m_IsTaxExemptCreditUnionSet)
            {
                parameters.Add(new SqlParameter("@IsTaxExemptCreditUnion", m_IsTaxExemptCreditUnion));
            }

            if (m_IsNewPmlUIEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsNewPmlUIEnabled", m_IsNewPmlUIEnabled));
            }

            if (m_IsAllowOrderingGlobalDmsAppraisalsSet)
            {
                parameters.Add(new SqlParameter("@IsAllowOrderingGlobalDmsAppraisals", m_IsAllowOrderingGlobalDmsAppraisals));
            }
            if (m_CustomPmlFieldListSet)
            {
                parameters.Add(new SqlParameter("@CustomPmlFieldXML", m_CustomPmlFieldList.ToString()));
            }
            if (m_CustomPricingPolicyFieldListSet)
            {
                parameters.Add(new SqlParameter("@CustomPricingPolicyFieldXml", m_CustomPricingPolicyFieldList.ToString()));
            }
            if (m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSPSet)
            {
                parameters.Add(new SqlParameter("@IsDisplayRateOptionsAboveLowestTriggeringMaxYSP", m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP));
            }
            if (m_IsUseLayeredFinancingSet)
            {
                parameters.Add(new SqlParameter("@IsUseLayeredFinancing", m_IsUseLayeredFinancing));
            }
            if (m_EnableAutoPriceEligibilityProcessSet)
            {
                parameters.Add(new SqlParameter("@EnableAutoPriceEligibilityProcess", m_EnableAutoPriceEligibilityProcess));
            }
            if (m_EnableRetailTpoPortalModeSet)
            {
                parameters.Add(new SqlParameter("@EnableRetailTpoPortalMode", m_EnableRetailTpoPortalMode));
            }
            if (m_EnableRenovationLoanSupportSet)
            {
                parameters.Add(new SqlParameter("@EnableRenovationLoanSupport", m_EnableRenovationLoanSupport));
            }
            if (m_SendAutoLockToLockDeskSet)
            {
                parameters.Add(new SqlParameter("@SendAutoLockToLockDesk", m_SendAutoLockToLockDesk));
            }

            if (m_SaveLockConfOnAutoLockSet)
            {
                parameters.Add(new SqlParameter("@SaveLockConfOnAutoLock", m_SaveLockConfOnAutoLock));
            }

            if (m_MersFormatSet)
            {
                parameters.Add(new SqlParameter("@MersFormat", m_MersFormat));
            }
            if (m_IsDocVendorTestingSet)
            {
                parameters.Add(new SqlParameter("@IsDocVendorTesting", m_IsDocVendorTesting));
            }

            if (m_ShowQMStatusInPml2Set)
            {
                parameters.Add(new SqlParameter("@ShowQMStatusInPml2", ShowQMStatusInPml2));
            }

            if (m_Pml2AsQuickPricerModeSet)
            {
                if (E_Pml2AsQuickPricerMode.Disabled != m_Pml2AsQuickPricerMode && (m_quickPricerTemplateId == Guid.Empty))
                {
                    var msg = "Must not enable PML2.0 for quickpricer if there isn't a quickpricer template selected";
                    throw new CBaseException(msg, msg);
                }
                parameters.Add(new SqlParameter("@Pml2AsQuickPricerMode", m_Pml2AsQuickPricerMode));
            }

            parameters.Add(new SqlParameter("@OCRVendorId", this.m_OCRVendorId));

            if (this.m_OCRUsernameSet)
            {
                parameters.Add(new SqlParameter("@OCRUsername", this.m_OCRUsername));
            }

            if (this.m_OCRPasswordByesSet)
            {
                var ocrEncKeyId = this.EncryptionKeyId == default(EncryptionKeyIdentifier) ? EncryptionKeyIdentifier.OCR : this.EncryptionKeyId;
                parameters.Add(new SqlParameter("@OCRPassword", EncryptionHelper.EncryptString(ocrEncKeyId, this.lazyOCRPassword.Value) ?? new byte[0]));
            }

            if (this.isKivaIntegrationEnabledSet)
            {
                parameters.Add(new SqlParameter("@IsKivaIntegrationEnabled", this.isKivaIntegrationEnabled));
            }

            if (E_BrokerBillingVersion.PerTransaction == BillingVersion)
            {
                if (this.isEnableComplianceEagleIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableComplianceEagleIntegration", this.isEnableComplianceEagleIntegration));
                }
                if (this.isEnablePTMComplianceEagleAuditIndicatorSet)
                {
                    parameters.Add(new SqlParameter("@IsEnablePTMComplianceEagleAuditIndicator", this.isEnablePTMComplianceEagleAuditIndicator));
                }
                if (this.complianceEagleUserNameSet)
                {
                    parameters.Add(new SqlParameter("@ComplianceEagleUserName", this.complianceEagleUserName));
                }
                if (this.complianceEaglePasswordBytesSet)
                {
                    parameters.Add(new SqlParameter(
                        "@ComplianceEaglePassword",
                        EncryptionHelper.EncryptString(
                            this.EncryptionKeyId == default(EncryptionKeyIdentifier) ? EncryptionKeyIdentifier.ComplianceEagle : this.EncryptionKeyId,
                            this.lazyComplianceEaglePassword.Value) ?? new byte[0]));
                }
                if (this.complianceEagleCompanyIDSet)
                {
                    parameters.Add(new SqlParameter("@ComplianceEagleCompanyID", this.complianceEagleCompanyID));
                }
                if (this.complianceEagleEnableMismo34Set)
                {
                    parameters.Add(new SqlParameter("@ComplianceEagleEnableMismo34", this.complianceEagleEnableMismo34));
                }
                if (this.complianceEagleAuditConfigSet)
                {
                    parameters.Add(new SqlParameter(
                        "@ComplianceEagleAuditConfigurationDataJson ",
                        SerializationHelper.JsonNetSerialize(this.complianceEagleAuditConfigData)));
                }
                if (m_ComplianceEaseUserNameSet)
                {
                    parameters.Add(new SqlParameter("@ComplianceEaseUserName", m_ComplianceEaseUserName));
                }
                if (m_ComplianceEasePasswordSet)
                {
                    parameters.Add(new SqlParameter("@ComplianceEasePassword", EncryptionHelper.Encrypt(this.lazyComplianceEasePassword.Value)));
                    if (this.EncryptionKeyId != default(EncryptionKeyIdentifier))
                    {
                        parameters.Add(new SqlParameter(
                            "@EncryptedComplianceEasePassword",
                            EncryptionHelper.EncryptString(this.EncryptionKeyId, this.lazyComplianceEasePassword.Value) ?? new byte[0]));
                    }
                }
                if (m_DocMagicIsAllowuserCustomLoginSet)
                {
                    parameters.Add(new SqlParameter("@DocMagicIsAllowuserCustomLogin", m_DocMagicIsAllowuserCustomLogin));
                }
                if (m_AllowInvestorDocMagicPlanCodesSet)
                {
                    parameters.Add(new SqlParameter("@AllowInvestorDocMagicPlanCodes", m_AllowInvestorDocMagicPlanCodes));
                }
                if (m_IsOnlyAllowApprovedDocMagicPlanCodesSet)
                {
                    parameters.Add(new SqlParameter("@IsOnlyAllowApprovedDocMagicPlanCodes", m_IsOnlyAllowApprovedDocMagicPlanCodes));
                }
                if (m_AutoSaveDocMagicGeneratedDocsSet)
                {
                    parameters.Add(new SqlParameter("@IsAutoSaveDocMagicGeneratedDocs", m_AutoSaveDocMagicGeneratedDocs));
                }

                if (m_AutoSaveLeCdReceivedDateSet)
                {
                    parameters.Add(new SqlParameter("@IsAutoSaveLeCdReceivedDate", m_AutoSaveLeCdReceivedDate));
                }
                if (m_EnableCocRedisclosureTriggersForHelocLoansSet)
                {
                    parameters.Add(new SqlParameter("@EnableCocRedisclosureTriggersForHelocLoans", m_EnableCocRedisclosureTriggersForHelocLoans));
                }
                if (m_UseCustomCocFieldListSet)
                {
                    parameters.Add(new SqlParameter("@UseCustomCocFieldList", m_UseCustomCocFieldList));
                }
                if (m_DefaultIssuedDateToTodaySet)
                {
                    parameters.Add(new SqlParameter("@DefaultIssuedDateToToday", m_DefaultIssuedDateToToday));
                }

                if (m_AutoSaveLeCdSignedDateSet)
                {
                    parameters.Add(new SqlParameter("@IsAutoSaveLeCdSignedDate", m_AutoSaveLeCdSignedDate));
                }

                if (this.m_RequireAllBorrowersToReceiveCDSet)
                {
                    parameters.Add(new SqlParameter("@RequireAllBorrowersToReceiveCD", m_RequireAllBorrowersToReceiveCD));
                }

                if (this.m_AutoSaveLeSignedDateWhenAnyBorrowerSignsSet)
                {
                    parameters.Add(new SqlParameter("@AutoSaveLeSignedDateWhenAnyBorrowerSigns", m_AutoSaveLeSignedDateWhenAnyBorrowerSigns));
                }

                if (m_DocMagicDocumentSavingOptionSet)
                {
                    parameters.Add(new SqlParameter("@DocMagicDocumentSavingOptionT", m_DocMagicDocumentSavingOption));
                }

                if (this.m_BackupDocumentSavingOptionSet)
                {
                    parameters.Add(new SqlParameter("@BackupDocumentSavingOptionT", m_BackupDocumentSavingOption));
                }

                if (this.m_DocumentFrameworkCaptureEnabledBranchGroupTSet)
                {
                    parameters.Add(new SqlParameter("@DocumentFrameworkCaptureEnabledBranchGroupT", m_DocumentFrameworkCaptureEnabledBranchGroupT));
                }

                if (m_DocMagicSplitESignedDocsSet)
                {
                    parameters.Add(new SqlParameter("@DocMagicSplitESignedDocs", m_DocMagicSplitESignedDocs));
                }

                if (m_DocMagicSplitUnsignedDocsSet)
                {
                    parameters.Add(new SqlParameter("@DocMagicSplitUnsignedDocs", m_DocMagicSplitUnsignedDocs));
                }

                if (m_DocMagicDefaultDocTypeIDSet)
                {
                    if (m_DocMagicDefaultDocTypeID == null)
                    {
                        parameters.Add(new SqlParameter("@ClearDefaultDocTypeId", 1));
                    }
                    else
                    {
                        parameters.Add(new SqlParameter("@DocMagicDefaultDocTypeId", m_DocMagicDefaultDocTypeID));
                    }
                }

                parameters.Add(new SqlParameter("@ESignedDocumentDocTypeId", m_ESignedDocumentDocTypeId));

                if (m_IsEnableComplianceEaseIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableComplianceEaseIntegration", m_IsEnableComplianceEaseIntegration));
                }

                if (m_IsEnablePTMComplianceEaseIndicatorSet)
                {
                    parameters.Add(new SqlParameter("@IsEnablePTMComplianceEaseIndicator", m_IsEnablePTMComplianceEaseIndicator));
                }

                if (m_IsEnablePTMDocMagicSeamlessInterfaceSet)
                {
                    parameters.Add(new SqlParameter("@IsEnablePTMDocMagicSeamlessInterface", m_IsEnablePTMDocMagicSeamlessInterface));
                }
            }
            else // if (E_BrokerBillingVersion.PerTransaction != BillingVersion)
            {
                if (m_DocMagicIsAllowuserCustomLoginSet
                || m_AllowInvestorDocMagicPlanCodesSet
                || m_IsOnlyAllowApprovedDocMagicPlanCodesSet)
                {
                    string message = "attempting to set DocMagic/ComplianceEase login info at broker level when not using Per Transaction billing.";
                    throw new CBaseException(message, message + "  Developer error.  Should not be allowed through UI");
                }

            }
            if(IsEnableDriveIntegration)
            {
                if (IsEnableDriveConditionImporting &&
                    (string.IsNullOrEmpty(m_DriveDueDateCalculatedFromField)
                    || null == m_DriveTaskPermissionLevelID
                    || null == m_DriveConditionCategoryID))
                {
                    string message = "when enabling drive integration with condition importing, must have filled out calculated field info, task permission level, and a condition category";
                    throw new CBaseException(message, message);
                }
                if(m_IsEnableDriveIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableDriveIntegration", IsEnableDriveIntegration));
                }
                if (m_IsEnableDriveConditionImportingSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableDriveConditionImporting", IsEnableDriveConditionImporting));
                }
                if (m_DriveConditionCategoryIDSet)
                {
                    parameters.Add(new SqlParameter("@DriveConditionCategoryID", DriveConditionCategoryID));
                }
                if(m_DriveDocTypeIDSet)
                {
                    parameters.Add(new SqlParameter("@DriveDocTypeID", DriveDocTypeID));
                }
                if(m_DriveDueDateCalculatedFromFieldSet)
                {
                    parameters.Add(new SqlParameter("@DriveDueDateCalculatedFromField",DriveDueDateCalculatedFromField));
                }
                if(m_DriveDueDateCalculationOffsetSet)
                {
                    parameters.Add(new SqlParameter("@DriveDueDateCalculationOffset", DriveDueDateCalculationOffset));
                }
                if (m_DriveTaskPermissionLevelIDSet)
                {
                    parameters.Add(new SqlParameter("@DriveTaskPermissionLevelID", DriveTaskPermissionLevelID)); 
                }

            }
            else{ // if drive integration not enabled.
                if (m_IsEnableDriveIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableDriveIntegration", IsEnableDriveIntegration));
                }                
                if (     m_DriveDueDateCalculatedFromFieldSet
                    ||   m_DriveDueDateCalculationOffsetSet
                    ||   m_DriveTaskPermissionLevelIDSet
                    ||   m_DriveConditionCategoryIDSet
                    ||   m_IsEnableDriveConditionImportingSet)
                {
                    string message = "when disabling drive integration, can't set any of the associated fields.";
                    throw new CBaseException(message, message);
                }
            }

            #region Flood Integration //OPM 12758
            if (IsEnableFloodIntegration)
            {
                if (null == m_FloodDocTypeId)
                {
                    string message = "When enabling the flood integration, an Edoc DocType must be declared for the flood certificate.";
                    throw new CBaseException(message, message);
                }
                if (m_IsEnableFloodIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableFloodIntegration", IsEnableFloodIntegration));
                }
                if (m_IsCCPmtRequiredForFloodIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsCCPmtRequiredForFloodIntegration", IsCCPmtRequiredForFloodIntegration));
                }
                if (m_FloodProviderIdSet)
                {
                    parameters.Add(new SqlParameter("@FloodProvider", FloodProviderID));
                }
                if (m_FloodDocTypeIdSet)
                {
                    parameters.Add(new SqlParameter("@FloodDocTypeId", FloodDocTypeID));
                }
                if (m_FloodAccountIdSet)
                {
                    parameters.Add(new SqlParameter("@FloodAccountId", FloodAccountId));
                }
                if (m_isFloodResellerIdSet)
                {
                    parameters.Add(new SqlParameter("@FloodResellerId", m_FloodResellerId));
                }
            }
            else
            { // flood integration not disabled
                if (m_IsEnableFloodIntegrationSet)
                {
                    parameters.Add(new SqlParameter("@IsEnableFloodIntegration", IsEnableFloodIntegration));
                }
                if (m_IsCCPmtRequiredForFloodIntegrationSet
                    || m_FloodProviderIdSet
                    || m_FloodDocTypeIdSet)
                {
                    string message = "When disabling the flood integration, the associated fields cannot be changed.";
                    throw new CBaseException(message, message);
                }
            }
            #endregion
            if (m_AvailableLockPeriodOptionsCSVSet)
            {
                // ensure that the values are unsigned ints or blank, and that there aren't too many of them.
                string message = "";
                string[] periodsDays = AvailableLockPeriodOptionsCSV.Split(',');
                if (periodsDays.Length > ConstApp.BROKER_MAX_NUMBER_OF_LOCK_PERIODS)
                {
                    message = "May not have more than " + ConstApp.BROKER_MAX_NUMBER_OF_LOCK_PERIODS + " lock periods per broker.";
                    throw new CBaseException(message, message);
                }
                uint result;
                foreach(string periodDays in periodsDays)
                {
                    if (periodDays == "")
                    {
                        continue;
                    }                    
                    if (false == UInt32.TryParse(periodDays, out result))
                    {
                        message = "Could not parse "+ periodDays + " as uint in "+ AvailableLockPeriodOptionsCSV;
                        throw new CBaseException(message, message);
                    }                   
                }
                if (m_AvailableLockPeriodOptionsCSVOriginal != m_AvailableLockPeriodOptionsCSV)
                {
                    Tools.LogWarning(String.Concat("RateLockPeriodChange by ", PrincipalFactory.CurrentPrincipal.LoginNm, " went from ", m_AvailableLockPeriodOptionsCSVOriginal, " to ", m_AvailableLockPeriodOptionsCSV));
                }
                parameters.Add(new SqlParameter("@AvailableLockPeriodOptionsCSV", AvailableLockPeriodOptionsCSV));
            }
            if(m_NmlsCallReportOriginationRelatedRevenueJSONContentSet)
            {
                parameters.Add(new SqlParameter("@NmlsCallReportOriginationRelatedRevenueJSONContent", NmlsCallReportOriginationRelatedRevenueJSONContent));
            }
            if (m_IsEnableProvidentFundingSubservicingExportSet)
            {
                parameters.Add(new SqlParameter("@IsEnableProvidentFundingSubservicingExport", IsEnableProvidentFundingSubservicingExport));
            }
            if (m_IsEnableRateMonitorServiceSet)
            {
                parameters.Add(new SqlParameter("@IsEnableRateMonitorService", IsEnableRateMonitorService));
            }
            if (m_EnableDuplicateEDocProtectionCheckingSet)
            {
                parameters.Add(new SqlParameter("@EnableDuplicateEDocProtectionChecking", EnableDuplicateEDocProtectionChecking));
            }
            #region DU Import Options //OPM 94140
            if (m_IsImportDuFindingsByDefaultSet)
            {
                parameters.Add(new SqlParameter("@IsImportDuFindingsByDefault", ImportAusFindingsByDefault));
            }
            if (m_IsImportDu1003ByDefaultSet)
            {
                parameters.Add(new SqlParameter("@IsImportDu1003ByDefault", ImportAus1003ByDefault));
            }
            if (m_IsImportDuCreditReportByDefaultSet)
            {
                parameters.Add(new SqlParameter("@IsImportDuCreditReportByDefault", ImportAusCreditReportByDefault));
            }
            #endregion
            
            parameters.Add(new SqlParameter("@IsForceLeadToLoanTemplate", IsForceLeadToLoanTemplate));
            parameters.Add(new SqlParameter("@IsForceLeadToLoanNewLoanNumber", IsForceLeadToLoanNewLoanNumber));            
            parameters.Add(new SqlParameter("@IsForceLeadToLoanRemoveLeadPrefix", IsForceLeadToLoanRemoveLeadPrefix));
            parameters.Add(new SqlParameter("@IsForceLeadToUseLoanCounter", IsForceLeadToUseLoanCounter));

            parameters.Add(new SqlParameter("@EmployeeResourcesUrl", EmployeeResourcesUrl));

            if (m_defaultLockPolicyIDSet)
            {
                parameters.Add(new SqlParameter("@DefaultLockPolicyID", DefaultLockPolicyID));
            }

            // sk Set the Lock Policy UseRateLockExpirations like opm 22212 ( for opm 105723 )
            if ((false == UseRateSheetExpirationFeature)  // because somebody could have set this to true manually.
                && (
                       (m_isPmlSubmissionAllowedSet && false == m_isPmlSubmissionAllowed)
                    || (m_isRateLockedAtSubmissionSet && false == m_isRateLockedAtSubmission)
                   )
                )
            {
                foreach (var lp in LockPolicy.RetrieveAllForBroker(BrokerID))
                {
                    lp.IsUsingRateSheetExpirationFeature = false;
                    lp.Save();
                }
            }

            if (m_EnableSequentialNumberingSet)
            {
                parameters.Add(new SqlParameter("@EnableSequentialNumbering", m_EnableSequentialNumbering));
            }

            if (m_SequentialNumberingFieldIdSet)
            {
                parameters.Add(new SqlParameter("@SequentialNumberingFieldId", m_SequentialNumberingFieldId));
            }

            if (m_SequentialNumberingSeedSet)
            {
                parameters.Add(new SqlParameter("@SequentialNumberingSeed", m_SequentialNumberingSeed));
            }

            //if (m_PopulateGFEWithLockDatesFromFrontEndLockSet) //134973
            //{
            //    parameters.Add(new SqlParameter("@PopulateGFEWithLockDatesFromFrontEndLock", m_PopulateGFEWithLockDatesFromFrontEndLock));
            //}

            if (m_TriggerAprRediscNotifForAprDecreaseSet)
            {
                parameters.Add(new SqlParameter("@TriggerAprRediscNotifForAprDecrease", m_TriggerAprRediscNotifForAprDecrease));
            }

            if (this.relationshipWithFreddieMacSet)
            {
                parameters.Add(new SqlParameter("@RelationshipWithFreddieMac", this.relationshipWithFreddieMac));
            }

            if (m_IsRemovePreparedDatesSet)
            {
                parameters.Add(new SqlParameter("@IsRemovePreparedDates", m_IsRemovePreparedDates));
            }
            if (m_IsProtectDisclosureDatesSet)
            {
                parameters.Add(new SqlParameter("@IsProtectDisclosureDates", m_IsProtectDisclosureDates));
            }
            if (m_IsEnableDTPkgBsdRedisclosureDatePopSet)
            {
                parameters.Add(new SqlParameter("@IsEnableDTPkgBsdRedisclosureDatePop", m_IsEnableDTPkgBsdRedisclosureDatePop));
            }
            if (m_IsB4AndB6DisabledIn1100And1300OfGfeSet)
            {
                parameters.Add(new SqlParameter("@IsB4AndB6DisabledIn1100And1300OfGfe", m_IsB4AndB6DisabledIn1100And1300OfGfe));
            }
            if (m_ForceTemplateOnImportSet)
            {
                parameters.Add(new SqlParameter("@ForceTemplateOnImport", m_ForceTemplateOnImport));
            }
            if (m_allowNonExcludableDiscountPointsSet)
            {
                parameters.Add(new SqlParameter("@AllowNonExcludableDiscountPoints", m_allowNonExcludableDiscountPoints));
            }
            if (m_IsAllowAllUsersToUseKayakoSet)
            {
                parameters.Add(new SqlParameter("@AllowAllUsersToUseKayako", m_IsAllowAllUsersToUseKayako));
            }
            if (this.suiteTypeSet)
            {
                parameters.Add(new SqlParameter("@SuiteType", this.SuiteType));
                parameters.Add(new SqlParameter("@IsVendorTestSuite", this.SuiteType == BrokerSuiteType.VendorTest));
            }
            if (m_SandboxScratchLOXmlSet)
            {
                parameters.Add(new SqlParameter("@SandboxScratchLOXml", m_SandboxScratchLOXml));
            }
            if (m_AllowRateMonitorForQP2FilesInEmdeddedPmlSet)
            {
                parameters.Add(new SqlParameter("@AllowRateMonitorForQP2FilesInEmdeddedPml", m_AllowRateMonitorForQP2FilesInEmdeddedPml));
            }
            if (m_AllowRateMonitorForQP2FilesInTpoPmlSet)
            {
                parameters.Add(new SqlParameter("@AllowRateMonitorForQP2FilesInTpoPml", m_AllowRateMonitorForQP2FilesInTpoPml));
            }
            var currEnabledStatusesByChannelAsJson = ObsoleteSerializationHelper.JsonSerialize(EnabledStatusesByChannel);
            if (m_EnabledStatusesByChannelAsJsonOnLoad != currEnabledStatusesByChannelAsJson)
            {
                parameters.Add(new SqlParameter("@EnabledStatusesByChannelAsJson", currEnabledStatusesByChannelAsJson));
            }
            var currEnabledStatusesByCorrespondentProcessTypeAsJson = ObsoleteSerializationHelper.JsonSerialize(EnabledStatusesByCorrespondentProcessType);
            if (m_EnabledStatusesByCorrespondentProcessTypeAsJsonOnLoad != currEnabledStatusesByCorrespondentProcessTypeAsJson)
            {
                parameters.Add(new SqlParameter("@EnabledStatusesByCorrespondentProcessTypeAsJson", currEnabledStatusesByCorrespondentProcessTypeAsJson));
            }
            if (m_IsActiveDirectoryURLSet)
            {
                parameters.Add(new SqlParameter("@ActiveDirectoryURL", m_activeDirectoryURL));
            }

            if (this.m_IsOptInMultiFactorAuthenticationSet)
            {
                parameters.Add(new SqlParameter("@IsOptInMultiFactorAuthentication", this.m_IsOptInMultiFactorAuthentication));
            }

            if (this.m_OptOutMfaNameSet)
            {
                parameters.Add(new SqlParameter("@OptOutMultiFactorAuthenticationName", this.m_OptOutMfaName));
            }

            if (this.m_OptOutMfaDateSet)
            {
                parameters.Add(new SqlParameter("@OptOutMultiFactorAuthenticationDate", this.m_OptOutMfaDate));
            }

            // Only save if we actually specify to opt in for MFA.
            if (m_isEnableMultiFactorAuthenticationSet && this.m_IsOptInMultiFactorAuthentication)
            {
                parameters.Add(new SqlParameter("@IsEnableMultiFactorAuthentication", this.IsEnableMultiFactorAuthentication));
            }
            if (m_isEnableMultiFactorAuthenticationTPOSet && this.m_IsOptInMultiFactorAuthentication)
            {
                parameters.Add(new SqlParameter("@IsEnableMultiFactorAuthenticationTPO", this.IsEnableMultiFactorAuthenticationTPO));
            }
            if (m_AllowDeviceRegistrationViaAuthCodePageSet)
            {
                parameters.Add(new SqlParameter("@AllowDeviceRegistrationViaAuthCodePage", this.AllowDeviceRegistrationViaAuthCodePage)); 
            }

            if (m_MinimumDefaultClosingCostDataSetSet)
            {
                parameters.Add(new SqlParameter("@MinimumDefaultClosingCostDataSet", m_MinimumDefaultClosingCostDataSet));
            }

            if (m_EnableCustomaryEscrowImpoundsCalculationSet)
            {
                parameters.Add(new SqlParameter("@EnableCustomaryEscrowImpoundsCalculation", m_EnableCustomaryEscrowImpoundsCalculation));
            }

            if (this.borrPdCompMayNotExceedLenderPdCompSet)
            {
                parameters.Add(new SqlParameter("@BorrPdCompMayNotExceedLenderPdComp", this.BorrPdCompMayNotExceedLenderPdComp));
            }

            if (this.m_DeterminationRulesXMLContentSet)
            {
                parameters.Add(new SqlParameter("@DeterminationRulesXMLContent", this.DeterminationRulesXMLContent));
            }

            {
                // 1/6/2015 dd - Save ClosingCostFeeSetupJson if any change.
                if (this.ClosingCostFeeSetupJsonContentSet)
                {
                    parameters.Add(new SqlParameter("@ClosingCostFeeSetupJsonContent", this.ClosingCostFeeSetupJsonContent));
                }
            }

            if (this.linkedLoanUpdateFieldsStringSet)
            {
                parameters.Add(new SqlParameter("@LinkedLoanUpdateFieldsString", this.linkedLoanUpdateFieldsString));
            }

            if (this.m_selectedNewLoanEditorUIPermissionTypeSet)
            {
                parameters.Add(new SqlParameter("SelectedNewLoanEditorUIPermissionType", this.m_selectedNewLoanEditorUIPermissionType));
            }

            parameters.Add(new SqlParameter("@ActualPricingBrokerId", this.ActualPricingBrokerId));

            if (this.tpoNewFeatureAlphaTestingGroupSet)
            {
                parameters.Add(new SqlParameter("@TpoNewFeatureAlphaTestingGroup", this.TpoNewFeatureAlphaTestingGroup));
            }

            if (this.tpoNewFeatureBetaTestingGroupSet)
            {
                parameters.Add(new SqlParameter("@TpoNewFeatureBetaTestingGroup", this.TpoNewFeatureBetaTestingGroup));
            }

            if (this.tpoRedisclosureAllowedGroupTypeSet)
            {
                parameters.Add(new SqlParameter("@TpoRedisclosureAllowedGroupType", this.TpoRedisclosureAllowedGroupType));
            }

            if (this.tpoRedisclosureFormSourceSet)
            {
                parameters.Add(new SqlParameter("@TpoRedisclosureFormSource", this.TpoRedisclosureFormSource));
            }

            if (this.tpoRedisclosureFormCustomPdfIdSet)
            {
                parameters.Add(new SqlParameter("@TpoRedisclosureFormCustomPdfId", this.TpoRedisclosureFormCustomPdfId));
            }

            if (this.tpoRedisclosureFormHostedUrlSet)
            {
                parameters.Add(new SqlParameter("@TpoRedisclosureFormHostedUrl", this.TpoRedisclosureFormHostedUrl));
            }

            if (this.tpoRedisclosureFormRequiredDocTypeSet)
            {
                parameters.Add(new SqlParameter("@TpoRedisclosureFormRequiredDocType", this.TpoRedisclosureFormRequiredDocType));
            }

            if (this.tpoInitialClosingDisclosureAllowedGroupTypeSet)
            {
                parameters.Add(new SqlParameter("@TpoInitialClosingDisclosureAllowedGroupType", this.TpoInitialClosingDisclosureAllowedGroupType));
            }

            if (this.tpoInitialClosingDisclosureFormSourceSet)
            {
                parameters.Add(new SqlParameter("@TpoInitialClosingDisclosureFormSource", this.TpoInitialClosingDisclosureFormSource));
            }

            if (this.origPortalAntiSteeringDisclosureAccessSet)
            {
                parameters.Add(new SqlParameter("@OrigPortalAntiSteeringDisclosureAccess", this.OrigPortalAntiSteeringDisclosureAccess));
            }

            if (this.opNonQmMsgReceivingEmailSet)
            {
                parameters.Add(new SqlParameter("@OpNonQmMsgReceivingEmail", this.OpNonQmMsgReceivingEmail));
            }

            if (this.opConLogCategoryIdSet)
            {
                parameters.Add(new SqlParameter("@OpConLogCategoryId", this.OpConLogCategoryId));
            }

            if (this.tpoInitialClosingDisclosureFormCustomPdfIdSet)
            {
                parameters.Add(new SqlParameter("@TpoInitialClosingDisclosureFormCustomPdfId", this.TpoInitialClosingDisclosureFormCustomPdfId));
            }

            if (this.tpoInitialClosingDisclosureFormHostedUrlSet)
            {
                parameters.Add(new SqlParameter("@TpoInitialClosingDisclosureFormHostedUrl", this.TpoInitialClosingDisclosureFormHostedUrl));
            }

            if (this.tpoInitialClosingDisclosureFormRequiredDocTypeSet)
            {
                parameters.Add(new SqlParameter("@TpoInitialClosingDisclosureFormRequiredDocType", this.TpoInitialClosingDisclosureFormRequiredDocType));
            }

            if (this.enableKtaIntegrationSet)
            {
                parameters.Add(new SqlParameter("@EnableKtaIntegration", this.EnableKtaIntegration));
            }

            if (this.allowIndicatingVerbalCreditReportAuthorizationSet)
            {
                parameters.Add(new SqlParameter("@AllowIndicatingVerbalCreditReportAuthorization", this.AllowIndicatingVerbalCreditReportAuthorization));
            }

            if (this.verbalCreditReportAuthorizationCustomPdfIdSet)
            {
                parameters.Add(new SqlParameter("@VerbalCreditReportAuthorizationCustomPdfId", this.VerbalCreditReportAuthorizationCustomPdfId) { IsNullable = true });
            }

            if (this.workflowRulesControllerIdSet)
            {
                parameters.Add(new SqlParameter("@WorkflowRulesControllerId", this.workflowRulesControllerId) { IsNullable = true });
            }

            if (this.enableLqbMobileAppSet)
            {
                parameters.Add(new SqlParameter("@EnableLqbMobileApp", this.enableLqbMobileApp));
            }

            if (this.showLoanProductIdentifierSet)
            {
                parameters.Add(new SqlParameter("@ShowLoanProductIdentifier", this.showLoanProductIdentifier));
            }

            #endregion

            try
            {
                // This needs longer timeout period because it might modifies the broker options which are stored in 
                // the indexed views. 7/22/05 tn.
                //
                // 8/12/2005 kb - Sixty seconds may not be enough.  Doubling
                // it so we can be sure we save, especially if they kill the
                // client window.

                int count = spExec.ExecuteNonQueryWithCustomTimeout( procedureName , 1 , 120 , false , parameters );
                bool wasCreated = false;

                if (count > 0)
                {
                    ret = true;

                    if ( m_isNew )
                    {
                        m_pmlSiteID = ( Guid ) pmlSiteIDParam.Value;
                        m_brokerID  = ( Guid ) brokerIDParam.Value;
                        m_isNew = false;
                        wasCreated = true;
                    }

                    m_IsCCPmtRequiredForFloodIntegrationSet = m_FloodProviderIdSet = m_FloodDocTypeIdSet = m_isFloodResellerIdSet = false;
                }

                if(wasCreated)
                {
                    // any additional saving that needs to be done non-transactionally for new brokers.
                }

                //OPM 126038 - 7/2013 pa - Save active document vendors using separate stored procedure
                if (!m_isNew && m_ActiveDocumentVendorsSet)
                {
                    DocumentVendorBrokerSettings.SaveAllForBroker(m_brokerID, AllActiveDocumentVendors, spExec);
                }
            } 
            catch (SqlException exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }

            try
            {
                // 8/5/2005 kb - Regardless of what we saved, we're evicting
                // known cached broker items.  For now, we only evict features
                // stored in the current context.  Beware, other applications
                // will still have valid cached entries.  Also, I don't think
                // we should be accessing the "web" cache for our broker
                // details -- what if we use this code in an exe?

                if( HttpContext.Current.Cache != null )
                {
                    HttpContext.Current.Cache.Remove( "FeaturesByBroker" + BrokerID );
                }
            }
            catch
            {
            }

            if (ret == true)
            {
                // 7/28/2014 dd - Sync the customer code in DB_CONNECTION_x_BROKER
                SyncCustomerCodeToDbConnection();
            }

            return ret;
        }

        /// <summary>
        /// On successful save, log things here.
        /// </summary>
        /// <remarks>Though ideally this would go in some long-term storage.</remarks>
        private void LogOnSuccessfulSave()
        {
            if (m_Pml2AsQuickPricerModeSet)
            {
                if (m_Pml2AsQuickPricerMode != E_Pml2AsQuickPricerMode.Disabled)
                {
                    Tools.LogInfo($"qp2_enabled and set to {m_Pml2AsQuickPricerMode.ToString("g")} for broker:" + this.AsSimpleStringForLog());
                }
            }
        }

        /// <summary>
        /// Temp logging method for event described in case 315019.
        /// </summary>
        /// <param name="oldVal">Previous value.</param>
        /// <param name="newVal">Value being set.</param>
        private static void LogRateLockChangeEventForCase315019(bool oldVal, bool newVal)
        {
            if ( oldVal != newVal )
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                string userLogin = (principal != null) ? principal.LoginNm : string.Empty;

                // LogWarning includes the call stack so we can see the page/app.
                Tools.LogWarning($"Case_315019: IsRateLockedAtSubmission modified by user: {userLogin}.  Set to {newVal}.");
            }
        }

        /// <summary>
        /// Resets lazy fields if values have been created.
        /// </summary>
        private void ResetLazyFields()
        {
            if (this.tpoHeaderLinkAlignmentSettings.IsValueCreated)
            {
                this.tpoHeaderLinkAlignmentSettings = new Lazy<BrokerTpoLoanNavigationHeaderLinkAlignmentSettings>(() => this.GetTpoLoanNavigationHeaderLinkAlignmentSettings());
            }

            if (this.enableDisclosurePageInNewTpoLoanNavigationSettings.IsValueCreated)
            {
                this.enableDisclosurePageInNewTpoLoanNavigationSettings = new Lazy<BrokerTpoLoanNavigationSettings>(() => this.GetTpoDisclosurePageSetting());
            }

            if (this.enableOrderInitialDisclosuresButtonInNewDisclosuresPageSettings.IsValueCreated)
            {
                this.enableOrderInitialDisclosuresButtonInNewDisclosuresPageSettings = new Lazy<BrokerTpoLoanNavigationSettings>(() => this.GetOrderInitialDisclosuresInTpoSetting());
            }

            if (this.tpoDisclosureDocumentVendorSettings.IsValueCreated)
            {
                this.tpoDisclosureDocumentVendorSettings = new Lazy<TpoInitialDisclosureVendorSettings>(() => this.GetTpoDisclosureDocumentVendorSettings());
            }

            if (this.historicalPricingSettings.IsValueCreated)
            {
                this.historicalPricingSettings = new Lazy<HistoricalPricingSettings>(() => this.GetHistoricalPricingSettings());
            }

            if (this.internalPricerUiSettings.IsValueCreated)
            {
                this.internalPricerUiSettings = new Lazy<InternalPricerUiSettings>(() => this.GetInternalPricerUiSettings());
            }

            if (this.pmlResults3PricingSettings.IsValueCreated)
            {
                this.pmlResults3PricingSettings = new Lazy<PML3Settings>(() => this.GetPmlResults3Settings());
            }

            if (this.productCodeFilterSettings.IsValueCreated)
            {
                this.productCodeFilterSettings = new Lazy<ProductCodeFilterSettings>(this.GetProductCodeFilterSettings);
            }

            if (this.seamlessLpaSettings.IsValueCreated)
            {
                this.seamlessLpaSettings = new Lazy<BrokerSeamlessLPASettings>(this.GetSeamlessLPASetting);
            }

            if (this.tpoLoanEditorSaveButtonBrokerOptions.IsValueCreated)
            {
                this.tpoLoanEditorSaveButtonBrokerOptions = new Lazy<TpoLoanEditorSaveButtonBrokerOptions>(this.GetTpoLoanEditorSaveButtonBrokerOptions);
            }

            if (this.nonQmOpSettings.IsValueCreated)
            {
                this.nonQmOpSettings = new Lazy<NonQmOpSettings>(() => this.GetNonQmOpSettings());
            }
        }

        /// <summary>
        /// Regex string for parsing temp option attributes.
        /// </summary>
        private const string tempOptionAttributeRegex = @"[-a-zA-Z0-9.,;:'""!$%&?*()\s]*";

        /// <summary>
        /// Obtains the alignment settings for the links in the TPO loan navigation header.
        /// </summary>
        /// <returns>
        /// A <see cref="BrokerTpoLoanNavigationHeaderLinkAlignmentSettings"/> instance with the
        /// broker's settings.
        /// </returns>
        private BrokerTpoLoanNavigationHeaderLinkAlignmentSettings GetTpoLoanNavigationHeaderLinkAlignmentSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"TpoLoanNavigationHeaderLinkAlignmentSettings\" CenterAlignment=\"(?<CenterAlignment>{tempOptionAttributeRegex})\" RightAlignment=\"(?<RightAlignment>{tempOptionAttributeRegex})\" />");

            return new BrokerTpoLoanNavigationHeaderLinkAlignmentSettings(match);
        }

        /// <summary>
        /// Obtains the display settings for the TPO portal "Disclosures" page based
        /// on the value of the broker's <see cref="TempOptionXmlContent"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="BrokerTpoLoanNavigationSettings"/> instance with the
        /// broker's settings.
        /// </returns>
        private BrokerTpoLoanNavigationSettings GetTpoDisclosurePageSetting()
        {
            var match = this.IsRegexInTempOption($"<option name=\"DisableNewTPODisclosurePage\" OCGroupExclusion=\"({tempOptionAttributeRegex})\" value=\"true\" />");

            return new BrokerTpoLoanNavigationSettings(this.BrokerID, this.EnableRetailTpoPortalMode, match);
        }

        /// <summary>
        /// Obtains the display settings for the "Order Initial Disclosures" button 
        /// on the TPO portal "Disclosures" page based on the value of the broker's 
        /// <see cref="TempOptionXmlContent"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="BrokerTpoLoanNavigationSettings"/> instance with the
        /// broker's settings.
        /// </returns>
        private BrokerTpoLoanNavigationSettings GetOrderInitialDisclosuresInTpoSetting()
        {
            var match = this.IsRegexInTempOption($"<option name=\"DisableInitialDisclosuresButtonOnTpoDisclosuresPage\" OCGroupExclusion=\"({tempOptionAttributeRegex})\" value=\"true\" />");

            return new BrokerTpoLoanNavigationSettings(this.BrokerID, this.EnableRetailTpoPortalMode, match);
        }

        /// <summary>
        /// Obtains the DocuTech vendors to which we should send MISMO 3.3 instead of 2.6.
        /// </summary>
        /// <returns>A list of vendor names that accept 3.3</returns>
        public IEnumerable<string> GetEnableSendingDocuTechMISMO33Vendors()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnableSendingDocuTechMISMO33\" vendorname=\"(?<Vendor>{tempOptionAttributeRegex})\" />");
            var tempOptionVendorNames = match.Groups["Vendor"].Value.Trim()
                .Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Where(name => name.Contains("DocuTech"));
            return tempOptionVendorNames;
        }

        /// <summary>
        /// Obtains the document vendor settings used when ordering initial disclosures within the TPO portal.
        /// </summary>
        /// <returns>
        /// The <see cref="LendersOffice.ObjLib.TPO.TpoInitialDisclosureVendorSettings"/> for the broker.
        /// </returns>
        private TpoInitialDisclosureVendorSettings GetTpoDisclosureDocumentVendorSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"TpoInitialDisclosureDocumentVendor\" vendorname=\"(?<Vendor>{tempOptionAttributeRegex})\" packagenumber=\"(?<PackageNumber>{tempOptionAttributeRegex})\" enableedisclosure=\"(?<EnableEDisclosure>{tempOptionAttributeRegex})\" enableesign=\"(?<EnableESign>{tempOptionAttributeRegex})\" />");

            return new TpoInitialDisclosureVendorSettings(this.BrokerID, match);
        }

        private HistoricalPricingSettings GetHistoricalPricingSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnablePhase1HistoricalPricingAndUIEnhancements\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" FeeService=\"(?<FeeService>[a-zA-Z]*)\" PriceGroups=\"(?<PriceGroups>[a-zA-Z]*)\" LenderFees=\"(?<LenderFees>[a-zA-Z]*)\" />");
            return new HistoricalPricingSettings(this.BrokerID, match);
        }

        private PML3Settings GetPmlResults3Settings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnablePmlUiResults3\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" OCGroup=\"(?<OCGroup>{tempOptionAttributeRegex})\" ForcePML3=\"(?<ForcePML3>{tempOptionAttributeRegex})\" />");
            return new PML3Settings(this.BrokerID, match);
        }

        private NonQmOpSettings GetNonQmOpSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnableNonQmPortal\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" OCGroup=\"(?<OCGroup>{tempOptionAttributeRegex})\"/>");
            return new NonQmOpSettings(this.BrokerID, match);
        }

        private InternalPricerUiSettings GetInternalPricerUiSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnablePhase1InternalPricerAndCheckEligibilityUIEnhancements\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" />");
            return new InternalPricerUiSettings(this.BrokerID, match);
        }

        private ProductCodeFilterSettings GetProductCodeFilterSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnableAdvancedFilterOptionsForPriceEngineResults\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" BranchGroup=\"(?<BranchGroup>{tempOptionAttributeRegex})\" value=\"true\" />");
            return new ProductCodeFilterSettings(this.BrokerID, match);
        }

        private bool IsProductCodeFilterOptionInTempOptions()
        {
            return this.IsRegexInTempOption($"<option name=\"EnableAdvancedFilterOptionsForPriceEngineResults\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" BranchGroup=\"(?<BranchGroup>{tempOptionAttributeRegex})\" value=\"true\" />").Success;
        }

        private bool IsNonQmOpSettingsInTempOptions()
        {
            return this.IsRegexInTempOption($"<option name=\"EnableNonQmPortal\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" OCGroup=\"(?<OCGroup>{tempOptionAttributeRegex})\"/>").Success;
        }

        public BrokerSeamlessDuSettings GetSeamlessDuSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnableNewSeamlessDu\" OCGroupWithFallback=\"(?<OCGroupWithFallback>{tempOptionAttributeRegex})\" EmployeeGroupWithFallback=\"(?<EmployeeGroupWithFallback>{tempOptionAttributeRegex})\" value=\"true\" />");

            if (match.Success && match.Groups.Count == 3)
            {
                return new BrokerSeamlessDuSettings(this.BrokerID, seamlessDuEnabled: true, originatingCompanyFallbackGroup: match.Groups["OCGroupWithFallback"].Value, employeeFallbackGroup: match.Groups["EmployeeGroupWithFallback"].Value);
            }
            else
            {
                return new BrokerSeamlessDuSettings(this.BrokerID, false);
            }
        }

        public BrokerSeamlessLPASettings GetSeamlessLPASetting()
        {
            var match = this.IsRegexInTempOption($"<option name=\"EnableNewSeamlessLPA\" OCGroupWithFallback=\"(?<OCGroupWithFallback>{tempOptionAttributeRegex})\" EmployeeGroupWithFallback=\"(?<EmployeeGroupWithFallback>{tempOptionAttributeRegex})\" value=\"true\" />");
            if (match.Success && match.Groups.Count == 3)
            {
                return new BrokerSeamlessLPASettings(this.BrokerID, seamlessLpaEnabled: true, nonSeamlessEnabledOcGroup: match.Groups["OCGroupWithFallback"].Value, nonSeamlessEnabledEmployeeGroup: match.Groups["EmployeeGroupWithFallback"].Value);
            }
            else
            {
                return new BrokerSeamlessLPASettings(this.BrokerID, seamlessLpaEnabled: false);
            }
        }

        public CrossBrowserSettings GetCrossBrowserSettings()
        {
            var match = this.IsRegexInTempOption($"<option name=\"IsCrossBrowserEnabled\" EmployeeGroup=\"(?<EmployeeGroup>{tempOptionAttributeRegex})\" />");
            return new CrossBrowserSettings(this.BrokerID, match);
        }

        public bool IsAllowOldIe
        {
            get { return IsStringExistedInTempOption("<option name=\"IsAllowOldIe\" value=\"true\" />"); }
        }

        /// <summary>
        /// Updates the values for the default branches and price groups for this
        /// broker.
        /// </summary>
        /// <remarks>
        /// To avoid a database performance hit, we separate this method from the 
        /// normal Save() method so that a trigger is unnecessary and the update
        /// to originating companies that use this default only happens when
        /// the TPO portal configuration is updated.
        /// </remarks>
        /// <param name="executor">
        /// The <see cref="CStoredProcedureExec"/> that will commit the transaction.
        /// </param>
        /// <param name="newDefaultWholesaleBranchId">
        /// The new default branch id for the wholesale portal mode.
        /// </param>
        /// <param name="newDefaultWholesalePriceGroupId">
        /// The new default price group id for the wholesale portal mode.
        /// </param>
        /// <param name="newDefaultMiniCorrBranchId">
        /// The new default branch id for the mini-correspondent portal mode.
        /// </param>
        /// <param name="newDefaultMiniCorrPriceGroupId">
        /// The new default price group id for the  mini-correspondent portal mode.
        /// </param>
        /// <param name="newDefaultCorrBranchId">
        /// The new default branch id for the correspondent portal mode.
        /// </param>
        /// <param name="newDefaultCorrPriceGroupId">
        /// The new default price group id for the correspondent portal mode.
        /// </param>
        public virtual void UpdateDefaultBranchPriceGroupValues(
            CStoredProcedureExec executor, 
            Guid newDefaultWholesaleBranchId, 
            Guid newDefaultWholesalePriceGroupId, 
            Guid newDefaultMiniCorrBranchId, 
            Guid newDefaultMiniCorrPriceGroupId, 
            Guid newDefaultCorrBranchId, 
            Guid newDefaultCorrPriceGroupId)
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                return;
            }

            // Provide an initial capacity of eight so that the list is not expanded
            // when all seven items are added.
            var parameters = new List<SqlParameter>(8)
            {
                new SqlParameter("@BrokerId", this.BrokerID)
            };

            if (this.IsValidNewDefaultValue("DefaultWholesaleBranchId", this.DefaultWholesaleBranchId, newDefaultWholesaleBranchId))
            {
                parameters.Add(new SqlParameter("@DefaultWholesaleBranchId", newDefaultWholesaleBranchId));
                this.DefaultWholesaleBranchId = newDefaultWholesaleBranchId;
            }

            if (this.IsValidNewDefaultValue("DefaultWholesalePriceGroupId", this.DefaultWholesalePriceGroupId, newDefaultWholesalePriceGroupId))
            {
                parameters.Add(new SqlParameter("@DefaultWholesalePriceGroupId", newDefaultWholesalePriceGroupId));
                this.DefaultWholesalePriceGroupId = newDefaultWholesalePriceGroupId;
            }

            if (this.IsValidNewDefaultValue("DefaultMiniCorrBranchId", this.DefaultMiniCorrBranchId, newDefaultMiniCorrBranchId))
            {
                parameters.Add(new SqlParameter("@DefaultMiniCorrBranchId", newDefaultMiniCorrBranchId));
                this.DefaultMiniCorrBranchId = newDefaultMiniCorrBranchId;
            }

            if (this.IsValidNewDefaultValue("DefaultMiniCorrPriceGroupId", this.DefaultMiniCorrPriceGroupId, newDefaultMiniCorrPriceGroupId))
            {
                parameters.Add(new SqlParameter("@DefaultMiniCorrPriceGroupId", newDefaultMiniCorrPriceGroupId));
                this.DefaultMiniCorrPriceGroupId = newDefaultMiniCorrPriceGroupId;
            }

            if (this.IsValidNewDefaultValue("DefaultCorrBranchId", this.DefaultCorrBranchId, newDefaultCorrBranchId))
            {
                parameters.Add(new SqlParameter("@DefaultCorrBranchId", newDefaultCorrBranchId));
                this.DefaultCorrBranchId = newDefaultCorrBranchId;
            }

            if (this.IsValidNewDefaultValue("DefaultCorrPriceGroupId", this.DefaultCorrPriceGroupId, newDefaultCorrPriceGroupId))
            {
                parameters.Add(new SqlParameter("@DefaultCorrPriceGroupId", newDefaultCorrPriceGroupId));
                this.DefaultCorrPriceGroupId = newDefaultCorrPriceGroupId;
            }

            // As with Save(), we want to use a timeout of two minutes.
            executor.ExecuteNonQueryWithCustomTimeout(
                "Broker_UpdateDefaultBranchPGValues", 
                nRetry: 1,
                nTimeoutSeconds: 120,
                bSendOnError: false,
                parameters: parameters.ToArray());
        }

        /// <summary>
        /// Determines if a new default branch or price group value is valid. A
        /// new value is considered valid if it is not <see cref="Guid.Empty"/>.
        /// </summary>
        /// <param name="defaultName">
        /// The name of the default field being set, used when throwing an
        /// <see cref="InvalidOperationException"/> to clarify which field
        /// was being reset.
        /// </param>
        /// <param name="existingValue">
        /// The existing default value for the broker or null if no value has 
        /// been set.
        /// </param>
        /// <param name="newValue">
        /// The new default value for the broker.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// An existing value is defined and the new value is <see cref="Guid.Empty"/>.
        /// </exception>
        protected bool IsValidNewDefaultValue(string defaultName, Guid? existingValue, Guid newValue)
        {
            if (!existingValue.HasValue && newValue == Guid.Empty)
            {
                return false;
            }
            else if (existingValue.HasValue && newValue == Guid.Empty)
            {
                var errorMessage = string.Format(
                    "Once a broker's {0} has been set, it cannot be reset.",
                    defaultName);

                throw new InvalidOperationException(errorMessage);
            }

            return true;
        }

        private void SyncCustomerCodeToDbConnection()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", this.BrokerID),
                                            new SqlParameter("@CustomerCode", this.CustomerCode)
                                            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DB_CONNECTION_x_BROKER_Update", 2, parameters);


        }

        /// <summary>
        /// We only want to use this in one specific scenario.  Try instead LockPolicy.IsOneOfLockDeskClosures
        /// <para></para>
        /// Determine if the time is fall in define holiday closure for the lender.
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        [Obsolete]
        public bool DO_NOT_USE_IsLockDeskClosure(DateTime now) 
        {

            foreach (LockDeskClosureItem item in m_lockDeskClosureList) 
            {
                if (item.ClosureDate.Year == now.Year && item.ClosureDate.Month == now.Month && item.ClosureDate.Day == now.Day) 
                {
                    if (item.IsClosedAllDay)
                        return true;
                    else 
                    {
                        DateTime openTime = item.LockDeskOpenTime;
                        DateTime closeTime = item.LockDeskCloseTime;

                        DateTime _open = new DateTime(now.Year, now.Month, now.Day, openTime.Hour, openTime.Minute, openTime.Second); // Strip out the date of openTime.
                        DateTime _close = new DateTime(now.Year, now.Month, now.Day, closeTime.Hour, closeTime.Minute, closeTime.Minute); // Strip out the date of closeTime.
                        if (now.CompareTo(_open) < 0 || now.CompareTo(_close) > 0) 
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private ArrayList x_lockDeskClosureList = null;
        private class LockDeskClosureItem 
        {
            public DateTime ClosureDate = DateTime.MinValue;
            public bool IsClosedAllDay;
            public DateTime LockDeskOpenTime = DateTime.MinValue;
            public DateTime LockDeskCloseTime = DateTime.MinValue;
        }

        // Returned in Pacific Time
        [Obsolete]
        private ArrayList m_lockDeskClosureList 
        {
            get 
            {
                if (x_lockDeskClosureList == null) 
                {
                    x_lockDeskClosureList = new ArrayList();
                    SqlParameter[] parameters = { new SqlParameter("@BrokerId", this.BrokerID) };
            
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "ListLockDeskClosureByBrokerId", parameters)) 
                    {
                        while (reader.Read()) 
                        {
                            LockDeskClosureItem item = new LockDeskClosureItem();

                            item.ClosureDate = (DateTime) reader["ClosureDate"];
                            item.IsClosedAllDay = (bool) reader["IsClosedAllDay"];

                            item.LockDeskOpenTime = GetPacificTimeFromLenderTimezone((DateTime) reader["LockDeskOpenTime"]);
                            item.LockDeskCloseTime = GetPacificTimeFromLenderTimezone((DateTime)  reader["LockDeskCloseTime"]);
                            x_lockDeskClosureList.Add(item);

                        }
                    }
                }
                return x_lockDeskClosureList;
            }
        }

        public IEnumerable<KeyValuePair<Guid, string>> ListEnablePriceGroups()
        {
            List<KeyValuePair<Guid, string>> list = new List<KeyValuePair<Guid, string>>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", this.BrokerID),
                                            new SqlParameter("@ExternalPriceGroupEnabled", true)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Guid id = (Guid)reader["LpePriceGroupId"];
                    string name = (string)reader["LpePriceGroupName"];

                    list.Add(new KeyValuePair<Guid, string>(id, name));
                }
            }

            return list;
        }

        //private List<Guid> m_featureList = null;
        private BrokerFeatures m_brokerFeatures = null;

        public bool HasFeatures(E_BrokerFeatureT feature)
        {
            if (null == m_brokerFeatures)
            {
                m_brokerFeatures = new BrokerFeatures(this.BrokerID);
            }
            return m_brokerFeatures.HasFeature(feature);
        }

        private Guid? x_accountOwnerUserId = null;
        public Guid AccountOwnerUserId
        {
            get
            {
                if (x_accountOwnerUserId.HasValue == false)
                {
                    SqlParameter[] parameters = { new SqlParameter("@BrokerID", this.BrokerID) };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "GetAdminBrokerIdByBrokerId", parameters))
                    {
                        if (reader.Read())
                        {
                            x_accountOwnerUserId = (Guid)reader["UserId"];
                        }
                        else
                        {
                            x_accountOwnerUserId = Guid.Empty;
                        }
                    }
                }
                return x_accountOwnerUserId.Value;
            }
        }

        private Dictionary<int, string> x_listTaskTriggerTemplate = null;
        public IReadOnlyDictionary<int, string> GetTaskTriggerTemplateIdToCreationTriggerNameMap()
        {
            // 12/27/2011 dd - TODO: NEED TO CACHE THIS LIST SO IT DOES NOT HIT DATABASE EACH TIME.
            if (x_listTaskTriggerTemplate == null)
            {
                x_listTaskTriggerTemplate = new Dictionary<int, string>();

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", m_brokerID)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "TASK_TRIGGER_TEMPLATE_FETCH", parameters))
                {
                    while (reader.Read())
                    {
                        x_listTaskTriggerTemplate.Add((int)reader["AutoTaskTemplateId"], (string)reader["TriggerName"]);
                    }
                }
            }
            return x_listTaskTriggerTemplate;
        }

        // OBSOLETE, see OPM 230834
        //public bool IsInstantSupportEnabled
        //{
        //    get { return m_IsInstantSupportEnabled; }
        //    set 
        //    { 
        //        m_IsInstantSupportEnabled = value; 
        //        m_IsInstantSupportEnabledSet = true; 
        //    } 
        //}

        public bool IsAllowAllUsersToUseKayako
        {
            get
            {
                return m_IsAllowAllUsersToUseKayako;
            }
            set
            {
                m_IsAllowAllUsersToUseKayako = value;
                m_IsAllowAllUsersToUseKayakoSet = true;
            }
        }

        /// <summary>
        /// [OBSOLETE] Gets a value indicating whether the broker is being
        /// used as a vendor's test suite. Use <see cref="SuiteType"/> instead.
        /// </summary>
        /// <value>
        /// True if the broker is being used as a vendor's test suite,
        /// false otherwise.
        /// </value>
        public bool IsVendorTestSuite { get; private set; }

        private List<KeyValuePair<int, string>> x_listTaskTriggerTemplateContainsDateParameter = null;
        public IEnumerable<KeyValuePair<int, string>> ListTaskTriggerTemplateContainsDateParameter()
        {
            if (x_listTaskTriggerTemplateContainsDateParameter == null)
            {
                x_listTaskTriggerTemplateContainsDateParameter = new List<KeyValuePair<int, string>>();

                IConfigRepository configRepository = ConfigHandler.GetRepository(m_brokerID);

                Stopwatch sw = Stopwatch.StartNew();
                ExecutingEngine engine = ExecutingEngine.GetEngineByBrokerId(configRepository, m_brokerID);
                sw.Stop();
                PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
                if (monitorItem != null)
                {
                    monitorItem.AddTimingDetail("ExecutingEngine.GetEngineByBrokerId::" + m_brokerID, sw.ElapsedMilliseconds);
                }

                ExecutingEngine systemEngine = engine.SystemExecutingEngine;
                foreach (var o in GetTaskTriggerTemplateIdToCreationTriggerNameMap())
                {
                    if (engine.HasCustomVariable(o.Value))
                    {
                        if (engine.IsTriggerContainDateParameters(o.Value))
                        {
                            x_listTaskTriggerTemplateContainsDateParameter.Add(o);
                        }
                    }
                    else
                    {
                        if (systemEngine != null)
                        {
                            if (systemEngine.IsTriggerContainDateParameters(o.Value))
                            {
                                x_listTaskTriggerTemplateContainsDateParameter.Add(o);
                            }
                        }
                    }
                }

            }
            return x_listTaskTriggerTemplateContainsDateParameter;
        }

        public IDictionary<int, int> GetStackOrderByDocTypeID()
        {
            Dictionary<int, int> stackOrderByDocTypeID = new Dictionary<int, int>();
            if (string.IsNullOrEmpty(EdocsStackOrderList.TrimWhitespaceAndBOM()))
            {
                return stackOrderByDocTypeID;
            }
            int[] orderedIDs = EdocsStackOrderList.Split(',').Select((s) => Int32.Parse(s)).ToArray();
            for (int i = 0; i < orderedIDs.Length; i++)
            {
                stackOrderByDocTypeID.Add(orderedIDs[i], i);
            }
            return stackOrderByDocTypeID;
        }

        public void SetStackOrder(string order)
        {
            EdocsStackOrderList = string.Join(",",order.Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries).Select((s) => Int32.Parse(s.TrimWhitespaceAndBOM())).Select((i)=> i.ToString()).Distinct().ToArray());
        }

        public bool IsUseDODLimitsEnabled
        {
            get { return IsStringExistedInTempOption("<option name=\"isusedodlimitsenabled\" value=\"true\" />"); }
        }

        // Obsolete, see OPM 214844
        //public bool IsExpandedLeadEditorEnabled
        //{
        //    get
        //    {
        //        return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditor\" value=\"true\" />");
        //    }
        //}

        public string LeadPrefix
        {
            get
            {
                bool useNoLeadPrefix = IsStringExistedInTempOption("<option name=\"useNoLeadPrefix\" value=\"true\" />");

                // OPM 243181 - Prefix should be blank if broker is set to use loan numbering for leads.
                if (useNoLeadPrefix || this.IsForceLeadToUseLoanCounter)
                {
                    return string.Empty;
                }
                return "LEAD";
            }
        }


        public bool DisableNameEditing
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"disablenameediting\" value=\"true\" />");
            }
        }

        public bool IsExpandedLeadEditorDoEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditordo\" value=\"true\" />");
            }
        }
        public bool IsExpandedLeadEditorDuEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditordu\" value=\"true\" />");
            }
        }
        public bool IsExpandedLeadEditorLpEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditorlp\" value=\"true\" />");
            }
        }
        public bool IsExpandedLeadEditorTotalEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditortotal\" value=\"true\" />");
            }
        }
        public bool IsExpandedLeadEditorCustomFieldsEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"enableexpandedleadeditorcustomfields\" value=\"true\" />");
            }
        }

        public bool PopulateLateFeesFromPml_139391
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"populatelatefeesfrompml_139391\" value=\"true\" />");
            }
        }

        public bool AllowHelocZeroInitialDraw_141171
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowHelocZeroInitialDraw_141171\" value=\"true\" />");
            }
        }

        public bool HasMigratedToNewRoles_135241
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"LenderHasMigratedToNewRoles_Case135241\" value=\"true\" />");
            }
        }

        public bool IsEnableSeamlessDu
        {
            get { return IsStringExistedInTempOption("<option name=\"IsEnableSeamlessDu\" value=\"true\" />"); }
        }

        public bool UsingLegacyDUCredentials
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UsingLegacyDUCredentials\" value=\"true\" />");
            }
        }

        public bool UsingLegacyLPACredentials
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UsingLegacyLPACredentials\" value=\"true\" />");
            }
        }

        // OBSOLETE, see OPM 230834
        //public bool EnableNewSupportCenter
        //{
        //    get { return IsStringExistedInTempOption("<option name=\"EnableNewSupportCenter\" value=\"true\" />"); }
        //}

        public bool EnableLenderFeeBuyout
        {
            get
            {
                if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {
                    return false;
                }

                Regex r = new Regex(@"<option name=""EnableLenderFeeBuyout"" value=""true""\s*(sBranchChannelT=""([0-9](;[0-9])*)""\s*)?/>");
                Match matches = r.Match(this.TempOptionXmlContent.Value);

                return matches.Success;
            }
        }

        public bool IsLenderFeeBuyoutEnabledForBranchChannel(E_BranchChannelT branchChannel)
        {
            if (string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
            {
                return false;
            }

            Regex r = new Regex(@"<option name=""EnableLenderFeeBuyout"" value=""true""\s*(sBranchChannelT=""([0-9](;[0-9])*)""\s*)?/>");
            Match matches = r.Match(this.TempOptionXmlContent.Value);

            if (!matches.Success || matches.Groups.Count != 4)
            {
                return false;
            }

            if (string.IsNullOrEmpty(matches.Groups[1].Value))
            {
                return true;
            }
            else
            {
                return matches.Groups[2].Value.Split(';').Contains(branchChannel.ToString("d"));
            }
        }

        public bool IsFundedDatePopulatedToHmdaActionTaken
        {
            // Watch out! The temp option is "Disabled", so need to negate.
            get { return !IsStringExistedInTempOption("<option name=\"IsFundedDatePopulatedToHmdaActionTakenDisabled\" value =\"true\" />"); }
        }
        public bool IsCanceledDatePopulatedToHmdaActionTaken
        {
            // Watch out! The temp option is "Disabled", so need to negate.
            get { return !IsStringExistedInTempOption("<option name=\"IsCanceledDatePopulatedToHmdaActionTakenDisabled\" value =\"true\" />"); }
        }

        public bool UseClearToCloseStatusInsteadOfApprovedForLoanCanceledActionTaken
        {
            get { return IsStringExistedInTempOption("<option name=\"UseClearToCloseStatusInsteadOfApprovedForLoanCanceledActionTaken\" value=\"true\" />"); }
        }

        public bool IsWithdrawnDatePopulatedToHmdaActionTaken
        {
            // Watch out! The temp option is "Disabled", so need to negate.
            get { return !IsStringExistedInTempOption("<option name=\"IsWithdrawnDatePopulatedToHmdaActionTakenDisabled\" value =\"true\" />"); }
        }
        public bool IsDeniedDatePopulatedToHmdaActionTaken
        {
            get { return IsStringExistedInTempOption("<option name=\"IsDeniedDatePopulatedToHmdaActionTaken\" value =\"true\" />"); }
        }

        public bool IsUseIrregularFirstPeriodAprCalc
        {
            get { return IsStringExistedInTempOption("<option name=\"UseIrregularFirstPeriodAprCalc_69485\" value =\"true\" />"); }
        }

        public bool UseCustomXsltCertificate
        {
            get { return IsStringExistedInTempOption("<option name=\"UseCustomXsltCertificate\" value=\"true\" />"); }
        }

        public bool CopyInternalUsersToOfficalContactsByDefault
        {
            get { return IsStringExistedInTempOption("<option name=\"CopyInternalUsersToOfficalContactsByDefault\" value=\"true\" />"); }
        }

        public bool EnableAdditionalSection1000CustomFees
        {
            get { return IsStringExistedInTempOption("<option name=\"EnableAdditionalSection1000CustomFees\" value=\"true\" />"); }
        }

        public bool AddLeadSourceDropdownToInternalQuickPricer
        {
            get { return IsStringExistedInTempOption("<option name=\"AddLeadSourceDropdownToInternalQuickPricer_Case173059\" value=\"true\" />"); }
        }

        public E_BrokerSsnMaskingOnVerifications BrokerSsnMaskingOnVerifications
        {
            get 
            { 
                if (IsStringExistedInTempOption("<option name=\"RemoveSsnOnVerifications\" value=\"true\" />"))
                {
                    return E_BrokerSsnMaskingOnVerifications.RemoveSsn;
                } 
                else if (IsStringExistedInTempOption("<option name=\"MaskSsnOnVerifications\" value=\"true\" />"))
                {
                    return E_BrokerSsnMaskingOnVerifications.DisplayMaskedSsn;
                }
                else 
                {
                    return E_BrokerSsnMaskingOnVerifications.DisplayFullSsn;
                }
            }
        }

        public bool EnabledBPMISinglePremium_Case115836
        {
            
            get { return ConstSite.DisplayPolicyAndRuleIdOnLpeResult // Per OPM 115836, if the "Author setting" is on, always show the temp optional MI settings.
                || IsStringExistedInTempOption("<option name=\"EnabledBPMISinglePremium_Case115836\" value=\"true\" />"); }
        }

        public bool EnableBPMISplitPremium_Case115836
        {
            get { return ConstSite.DisplayPolicyAndRuleIdOnLpeResult // Per OPM 115836, if the "Author setting" is on, always show the temp optional MI settings.
                || IsStringExistedInTempOption("<option name=\"EnableBPMISplitPremium_Case115836\" value=\"true\" />"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower-paid originator
        /// compensation source is defaulted to "Total Loan Amount".
        /// </summary>
        public bool DefaultBorrPaidOrigCompSourceToTotalLoanAmount
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"DefaultBorrPaidOrigCompSourceToTotalLoanAmount\" value=\"true\" />");
            }
        }

        public bool ExcludeCashDepositFromAssetTotals
        {
            get { return IsStringExistedInTempOption("<option name=\"ExcludeCashDepositFromAssetTotals\" value=\"true\" />"); }
        }

        public bool ShouldApplyCompPlan(E_LoanEvent loanEvent, bool isCompPlanApplied)
        {
            switch (loanEvent)
            {
                case E_LoanEvent.Created:
                    return false;
                case E_LoanEvent.DocumentCheck:
                    return !isCompPlanApplied && this.ApplyCompWhenDocCheckDateEntered;
                case E_LoanEvent.Registered:
                    return !isCompPlanApplied && this.ApplyCompAtRegistration;
                case E_LoanEvent.RateLocked:
                    return !isCompPlanApplied && this.ApplyCompAtRateLock;
                case E_LoanEvent.Respa6Collected:
                    return !isCompPlanApplied && this.ApplyCompWhenRespa6Collected;
                default:
                    throw new UnhandledEnumException(loanEvent);
            }
        }

        // OPM 184828
        public bool ApplyCompWhenDocCheckDateEntered
        {
            get { return IsStringExistedInTempOption("<option name=\"ApplyCompWhenDocCheckDateEntered\" value=\"true\" />"); }
        }

        /// <summary>
        /// OPM 457457: Apply the originator comp plan when the RESPA 6 items are collected. Uses the same trigger the disclosure pipeline.
        /// </summary>
        public bool ApplyCompWhenRespa6Collected
        {
            get { return IsStringExistedInTempOption("<option name=\"ApplyCompWhenRespa6Collected\" value=\"true\" />"); }
        }

        /// <summary>
        /// Defaults to true--add this option to disable applying the originator comp plan to the loan file at PML registration.
        /// Note that the ApplyCompWhenDocCheckDateEntered option currently also disables this option.
        /// </summary>
        public bool ApplyCompAtRegistration
        {
            get { return !this.ApplyCompWhenDocCheckDateEntered && !IsStringExistedInTempOption("<option name=\"ApplyCompAtRegistration\" value=\"false\" />"); }
        }

        /// <summary>
        /// Defaults to true--add this option to disable applying the comp plan to the loan file at rate lock.
        /// Note that the ApplyCompWhenDocCheckDateEntered option currently also disables this option.
        /// </summary>
        public bool ApplyCompAtRateLock
        {
            get { return !this.ApplyCompWhenDocCheckDateEntered && !IsStringExistedInTempOption("<option name=\"ApplyCompAtRateLock\" value=\"false\" />"); }
        }

        public class AutoLoginDocuTechInfo
        {
            public E_DocumentsPackageType PackageType { get; set; }
            public String UserName { get; set; }
            public Sensitive<string> Password { get; set; }


        }

        public bool Enable1003EditorInTPOPortal
        {
            get { return !IsStringExistedInTempOption("<option name=\"Disable1003EditorInTPOPortal\" value=\"true\" />"); }
        }

        public bool RemoveMessageToLenderTPOPortal
        {
            get { return IsStringExistedInTempOption("<option name=\"RemoveMessageToLenderTPOPortal_Case171312\" value=\"true\" />"); }
        }

        public bool RemoveMessageToLenderEmbeddedPML
        {
            get { return IsStringExistedInTempOption("<option name=\"RemoveMessageToLenderEmbeddedPML_Case215651\" value=\"true\" />"); }
        }

        public bool HideAllowOnlineSigningOptionForFHA
        {
            get { return IsStringExistedInTempOption("<option name=\"HideAllowOnlineSigningOptionForFHA\" value=\"true\" />"); }
        }

        public bool ShowAllowOnlineSigningOptionForUSDA
        {
            get { return IsStringExistedInTempOption("<option name=\"ShowAllowOnlineSigningOptionForUSDA\" value=\"true\" />"); }
        }

        public bool IsActiveDirectoryAuthenticationEnabled
        {
            get { return IsStringExistedInTempOption("<option name=\"EnableActiveDirectoryAuthentication\" value=\"true\" />"); }
        }

        public E_BrokerDocumentValidationOptionT DocumentValidationOptionT
        {
            get 
            {
                if (IsStringExistedInTempOption("<option name=\"DocumentValidationOptionT\" value=\"BlockOnMismatch\" />"))
                {
                    return E_BrokerDocumentValidationOptionT.BlockOnValidationMismatch;
                }
                else if (IsStringExistedInTempOption("<option name=\"DocumentValidationOptionT\" value=\"WarningOnMismatch\" />"))
                {
                    return E_BrokerDocumentValidationOptionT.WarningOnValidationMismatch;
                }
                else
                {
                    return E_BrokerDocumentValidationOptionT.NoValidation;
                }
            }
        }

        public bool ShowCRMID
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ShowCRMID\" value=\"true\">");
            }
        }

        public bool Export_sHas1stTimeBuyer_as_FTHB_for_ULDD
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"Export_sHas1stTimeBuyer_as_FTHB_for_ULDD\" value=\"true\" />");
            }
        }

        public bool AppendCompanyIDtoCompanyListInUserAdmin
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AppendCompanyIDtoCompanyListInUserAdmin\" value=\"true\">");
            }
        }

        public bool ExpandSubservicerContacts
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ExpandSubservicerContacts\" value=\"true\">");
            }
        }

        public bool UseLegacyBehaviorForSubjPropStateInNewCPORTAL
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UseLegacyBehaviorForSubjPropStateInNewCPORTAL\" value=\"true\">");
            }
        }

        public bool DisplayNoteRateAndDtiOnCertificate
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisplayNoteRateAndDtiOnCertificate\" value=\"true\" />");
            }
        }

        public bool EnableTeamsUI
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableTeamsUI\" value=\"true\" />");
            }
        }

        public bool EnableLqbNonCompliantDocumentSignatureStamp => IsStringExistedInTempOption("<option name=\"EnableLqbNonCompliantDocumentSignatureStamp\"");

        public bool EnableDropboxBarcodeScanning
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableDropboxBarcodeScanning\" value=\"true\" />");
            }
        }

        public bool UseRespaForNonRetailInitialDisclosures
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UseRespaForNonRetailInitialDisclosures\" value=\"true\" />");
            }
        }

        public bool ReqDocCheckDAndRespaForNonRetailInitialDisclosures
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ReqDocCheckDAndRespaForNonRetailInitialDisclosures\" value=\"true\" />");
            }
        }

        public bool DisableFeeEditorInTPOPortalCase183419
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableFeeEditorInTPOPortalCase183419\" value=\"true\" />");
            }
        }

        public bool AssumePmlQmEligibleForGse
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AssumePmlQmEligibleForGse\" value=\"true\" />");
            }
        }

        public bool ExposeBulkMiniBulkCorrProcessTypeEnums
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ExposeBulkMiniBulkCorrProcessTypeEnums\" value=\"true\" />");
            }
        }

        public bool Opm149774TempBit
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"Opm149774TempBit\" value=\"true\" />");
            }
        }

        public bool HideServicesDashboardInTPOPortal
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"HideServicesDashboardInTPOPortal\" value=\"true\" />");
            }
        }

        public bool EnableDeletedPageEDocCreation
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableDeletedPageEDocCreation\" value=\"true\" />");
            }
        }

        public bool UseManualApr
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UseManualApr\" value=\"true\" />");
            }
        }

        public bool AllowManualApr
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowManualApr\" value=\"true\" />");
            }
        }

        public bool ShowEmployeeResourcesLinkInLeadEditor
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ShowEmployeeResourcesLinkInLeadEditor\" value=\"true\" />");
            }
        }

        public bool IsAddUliTo1003Pages
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AddUliTo1003Pages\" value=\"true\" />");
            }
        }
        
        /// <summary>
        /// Returns the Temp option value for which Document Vendor to use when clicking on the MISMO 3.3 Link in the Export folder in the loan editor.
        /// </summary>
        public E_DocumentVendor Mismo33SidebarLinkDocumentVendor
        {
            get
            {
                E_DocumentVendor vendor = E_DocumentVendor.IDS;
                if (!string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {
                    Match match = Regex.Match(this.TempOptionXmlContent.Value, "<option name=\"Mismo33SidebarLinkDocumentVendor\" value=\"(.*?)\" />", RegexOptions.IgnoreCase);
                    if (match.Success && match.Groups.Count > 1)
                    {
                        if (!Enum.TryParse(match.Groups[1].Value, out vendor))
                        {
                            vendor = E_DocumentVendor.IDS;
                        }
                    }
                }

                return vendor;
            }
        }

        public E_AgentRoleT DocutechEsignNotificationEmailAgent
        {
            get
            {
                E_AgentRoleT agent = E_AgentRoleT.LoanOfficer;
                if (!string.IsNullOrEmpty(this.TempOptionXmlContent.Value))
                {
                    Match match = Regex.Match(this.TempOptionXmlContent.Value, "<option name=\"DocutechEsignNotificationEmailAgent\" value=\"([\\S]+)\" />", RegexOptions.IgnoreCase);
                    if (match.Success && (match.Groups.Count > 1))
                    {
                        try
                        {
                            agent = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), match.Groups[1].Value, true);
                        }
                        catch (ArgumentException) { }
                    }
                }

                return agent;
            }
        }



        /// <summary>
        /// Also requires IsEnableRateMonitorService to be true.
        /// </summary>
        public bool AllowRateMonitorForQP2FilesInEmdeddedPml
        {
            get
            {
                return IsEnableRateMonitorService &&
                    m_AllowRateMonitorForQP2FilesInEmdeddedPml;
            }
            set
            {
                m_AllowRateMonitorForQP2FilesInEmdeddedPml = value;
                m_AllowRateMonitorForQP2FilesInEmdeddedPmlSet = true;
            }
        }

        /// <summary>
        /// Also requires IsEnableRateMonitorService to be true.
        /// </summary>
        public bool AllowRateMonitorForQP2FilesInTpoPml
        {
            get
            {
                return IsEnableRateMonitorService &&
                    m_AllowRateMonitorForQP2FilesInTpoPml;
            }
            set
            {
                m_AllowRateMonitorForQP2FilesInTpoPml = value;
                m_AllowRateMonitorForQP2FilesInTpoPmlSet = true;
            }
        }

        public string DeterminationRulesXMLContent
        {
            get
            {
                return m_DeterminationRulesXMLContent;
            }
            set
            {
                m_DeterminationRulesXMLContentSet = true;
                m_DeterminationRulesXMLContent = value;
            }
        }

        public DeterminationRuleSet DeterminationRuleSet
        {
            get
            {
                return new DeterminationRuleSet(DeterminationRulesXMLContent);
            }
        }

        public bool IsSetAllFieldsOnPoolAssignmentByDefault
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsSetAllFieldsOnPoolAssignmentByDefault\" value=\"true\" />");
            }
        }

        public bool IsEnableDocuTechAutoDisclosure
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableDocuTechAutoDisclosure\" value=\"true\" />");
            }
        }

        public bool DisplayQMResultsForCorrespondentFiles
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisplayQMResultsForCorrespondentFiles\" value=\"true\" />");
            }
        }

        public Guid? AutoDisclosureBranchID
        {
            get
            {
                // Referenced http://stackoverflow.com/a/11040993 when writing regex.
                var regexString = "<option name=\"DocuTechAutoDisclosureBranchId\" value=\"(?<BranchId>[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12})\" />";

                var match = Regex.Match(
                    this.TempOptionXmlContent.Value,
                    regexString,
                    RegexOptions.IgnoreCase);

                if (match.Success && match.Groups["BranchId"].Success)
                {
                    return new Guid(match.Groups["BranchId"].Value);
                }
                else
                {
                    return null;
                }
            }
        }

        public bool IsEnableGfe2015
        {
            get
            {
                return !IsStringExistedInTempOption("<option name=\"DisableGfe2015\" value=\"true\" />");
            }
        }

        public bool IsEnableNewFeeService
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableNewFeeService\" value=\"true\" />");
            }
        }

        public bool MigrateToLegacyButMigratedOnPricing
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"MigrateToLegacyButMigratedOnPricing\" value=\"true\" />");
            }
        }

        public bool AllowCitrixVPrinterDownload
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"AllowCitrixVPrinterDownload\" value=\"true\" />");
            }
        }

        public bool IsEnableBigLoanPage
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableBigLoanPage\" value=\"true\"/>");
            }
        }

        public bool IsNotifyUponRateLockModified
        {
            get { return IsStringExistedInTempOption("<option name=\"IsNotifyUponRateLockModified\" value=\"true\" />"); }
        }

        public bool AllowQMEditTPO
        {
            get { return IsStringExistedInTempOption("<option name=\"AllowQMEditTPO\" value=\"true\" />"); }
        }

        public bool HideUploadAndFaxLinksInTpoLoanNavigationEdocs
        {
            get { return this.IsStringExistedInTempOption("<option name=\"HideUploadAndFaxLinksInTpoLoanNavigationEdocs\" value=\"true\" />"); }
        }

        public bool HideRateLockHistoryGridInTpoPortal
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"HideRateLockHistoryGridInTPOPortal\" value=\"true\" />");
            }
        }

        public bool AutoExtensionForceWorstOptionForLockExtensionDateTies
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"AutoExtensionForceWorstOptionForLockExtensionDateTies\" value=\"true\" />");
            }
        }

        public bool UseMergeGroupNameInsteadOfLoanProgramNameOnPrintedIFW
        {
            get { return IsStringExistedInTempOption("<option name=\"UseMergeGroupNameInsteadOfLoanProgramNameOnPrintedIFW\" value=\"true\" />"); }
        }


        // OPM 184017. To turn on non-purchasing spouse feature
        public bool IsUseNewNonPurchaseSpouseFeature
        {
            get { return IsStringExistedInTempOption("<option name=\"IsUseNewNonPurchaseSpouseFeature\" value=\"true\" />"); }
        }

        public bool IsDisableAutoCalcOfTexas50a6
        {
            get { return this.IsStringExistedInTempOption("<option name=\"IsDisableAutoCalcOfTexas50a6\" value=\"true\" />"); }
        }

        public bool IsResetSystemFeesOnRemoval
        {
            get
            {
                return !this.IsStringExistedInTempOption("<option name=\"IsResetSystemFeesOnRemoval\" value=\"false\" />");
            }
        }

        /// <summary>
        /// Populate the funded date when the user clicks the submit button on the OSI page. OPM 200121.
        /// </summary>
        public bool SetFundedDateOnOSISubmission
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"SetFundedDateOnOSISubmission\" value=\"true\" />");
            }
        }

        /// <summary>
        /// 5/5/2016 dd - To test Amazon Edocs feature.
        /// </summary>
        public bool IsTestingAmazonEdocs
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsTestingAmazonEdocs\" value=\"true\" />") ||
                    IsStringExistedInTempOption("<option name=\"IsTestingAmazonEdocs\" value=\"true\" png=\"disabled\" />");
            }
        }

        public bool IsDisablePngGeneration
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"IsTestingAmazonEdocs\" value=\"true\" png=\"disabled\" />");
            }
        }

        private TpoLoanEditorSaveButtonBrokerOptions GetTpoLoanEditorSaveButtonBrokerOptions()
        {
            Match match = this.IsRegexInTempOption($"<option name=\"DisableTpoLoanEditorSaveButton\" value=\"true\" (ExceptForThisEmployeeGroup=\"(?<ExceptForThisEmployeeGroup>{tempOptionAttributeRegex})\")? (ExceptForThisOcGroup=\"(?<ExceptForThisOcGroup>{tempOptionAttributeRegex})\")? />");
            return new TpoLoanEditorSaveButtonBrokerOptions(this.BrokerID, match);
        }

        public TpoLoanEditorSaveButtonBrokerOptions TpoLoanEditorSaveButtonBrokerOptions
        {
            get
            {
                return this.tpoLoanEditorSaveButtonBrokerOptions.Value;
            }
        }

        private Nullable<E_FirstAmericanFeeImportSource> m_firstAmericanImportSource = new Nullable<E_FirstAmericanFeeImportSource>();

        public E_FirstAmericanFeeImportSource FirstAmericanImportSource
        {
            get
            {
                if (!m_firstAmericanImportSource.HasValue)
                {
                    m_firstAmericanImportSource = E_FirstAmericanFeeImportSource.Normal;
                    Match match = Regex.Match(this.TempOptionXmlContent.Value, "<option name=\"OnlyUseBuyerAmountFeesFromFirstAmericanQuote_Case182187\" value=\"(Normal|Lender|Buyer)\" />", RegexOptions.IgnoreCase);
                    if (match.Success && match.Groups.Count > 1)
                    {
                        try
                        {
                            m_firstAmericanImportSource = (E_FirstAmericanFeeImportSource)Enum.Parse(typeof(E_FirstAmericanFeeImportSource), match.Groups[1].Value);
                        }
                        catch (ArgumentException) { }
                    }
                }
                return m_firstAmericanImportSource.Value;
            }
        }

        public ComplianceEagleAuditConfiguration ComplianceEagleAuditConfiguration
        {
            get
            {
                // Will be either the value they've set from the temp option or the
                // intended default.
                E_sStatusT beginAuditsStatus = ComplianceEagleAuditConfiguration.DefaultBeginStatus;
                IEnumerable<E_sStatusT> excludedStatuses = ComplianceEagleAuditConfiguration.DefaultExcludedStatuses;

                if (this.complianceEagleAuditConfigData != null)
                {
                    beginAuditsStatus = complianceEagleAuditConfigData.BeginRunningAuditsStatus;
                    excludedStatuses = complianceEagleAuditConfigData.ExcludedStatuses;
                }

                return new ComplianceEagleAuditConfiguration(beginAuditsStatus, excludedStatuses);
            }

            set
            {
                if (value != null)
                {
                    this.complianceEagleAuditConfigData = new ComplianceEagleAuditConfigurationData()
                    {
                        BeginRunningAuditsStatus = value.BeginRunningAuditsStatus,
                        ExcludedStatuses = value.ExcludedStatuses
                    };
                }
                else
                {
                    this.complianceEagleAuditConfigData = null;
                }

                this.complianceEagleAuditConfigSet = true;
            }
        }

        /// <summary>
        /// ComplianceEase: Override Section G and Hud 902-903 fee paid to Lender/Broker and set to Default instead. New Data layer only. See OPM 226552.
        /// </summary>
        public bool ComplianceEaseAllowOverridePaidToForHOEPAFeeTest
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ComplianceEaseAllowOverridePaidToForHOEPAFeeTest\" value=\"true\" />");
            }
        }

        /// <summary>
        /// ComplianceEase: Suppress Lender NMLS ID for wholesale channel loans. This is a workaround to avoid unnecessary audit if originator and lender hold state licenses from different issuing authorities (i.e. Cal DBO / Cal BRE).
        /// </summary>
        /// <remarks>Refer to https://lqbopm/default.asp?232395 for more details.</remarks>
        public bool ComplianceEaseSuppressLenderNMLSForWholesaleChannel
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"ComplianceEaseSuppressLenderNMLSForWholesaleChannel\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Allows a client to overwrite the ESignNotification element of the LQBDocumentRequest with a value
        /// defined in custom field 60.
        /// </summary>
        public bool UseCustomField60AsEsignNotificationList
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"UseCustomField60AsEsignNotificationList\" value=\"true\" />");
            }
        }

        /// <summary>
        /// If true, pressing the Pull 1003 Details into Adjustments will migrate the adjustments from 1003 details, but will not set sLoads1003LineLFromAdjustments = true.
        /// </summary>
        public bool DontLink1003ToAdjustmentsOnPull
        {
            get { return IsStringExistedInTempOption("<option name=\"DontLink1003ToAdjustmentsOnPull\" value=\"true\" />"); }
        }

        public bool DisableAutoPopulationOfCdReceivedDate
        {
            get { return IsStringExistedInTempOption("<option name=\"DisableAutoPopulationOfCdReceivedDate\" value=\"true\" />"); }
        }

        public bool BypassEmployeeAssignmentRestrictionForPricing
        {
            get { return IsStringExistedInTempOption("<option name=\"BypassEmployeeAssignmentRestrictionForPricing\" value=\"true\" />"); }
        }

        public bool Force2016ComboAndProrationUpdatesTo1003Details
        {
            get {  return IsStringExistedInTempOption("<option name=\"Force2016ComboAndProrationUpdatesTo1003Details\" value=\"true\" />"); }
        }

        public bool IsEnableMobileDeviceRegistrationTPO
        {
            get
            {
                return this.EnableLqbMobileApp && IsStringExistedInTempOption("<option name=\"IsEnableMobileDeviceRegistrationTPO\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Prevents VPrinter from uploading into OCR framework.
        /// </summary>
        public bool DisableVPrinterOCRFrameworkCapture
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableVPrinterOCRFrameworkCapture\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Disables the refresh of the Housing Expense JSON content with live data on archive creation and refresh.
        /// </summary>
        public bool DisableArchiveHousingExpenseRefresh
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableArchiveHousingExpenseRefresh\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Prevents the hardcoded DocMagic Esign email from being set during document generation.
        /// </summary>
        public bool DisableESignNotificationEmailFromDocumentFrameworkRequests
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableESignNotificationEmailFromDocumentFrameworkRequests\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Disables EDocs auto-obsolete functionality for MI Quote EDocs when MI Policies are ordered.
        /// </summary>
        public bool DisableEDocsMIQuoteAutoObsoleteAndUnObsolete
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableEDocsMIQuoteAutoObsoleteAndUnObsolete\" value=\"true\" />");
            }
        }        

        public bool SetRespa6DateForNoIncomeLoans
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"Case455957_SetRESPA6DateForNoIncomeLoans\" value=\"true\" />");
            }
        }

        public EditorStatus RetrieveEditorStatusForTempBit(string tempBitId)
        {
            var regex = "<option name=\"" + tempBitId + "\" value=\"([0-9])\" />";
            var match = this.IsRegexInTempOption(regex);

            if (match.Success)
            {
                var value = match.Groups[1].Value;
                EditorStatus editorStatus;
                if(Enum.TryParse(value, out editorStatus))
                {
                    return editorStatus;
                }
            }

            return EditorStatus.Never;
        }

        public EditorStatus LcrHideEstimatedDTI
        {
            get
            {
                return RetrieveEditorStatusForTempBit("LcrHideEstimatedDTI");
            }
        }

        public EditorStatus LcrHide1stLienLifetimeFinanceCharge
        {
            get
            {
                return RetrieveEditorStatusForTempBit("LcrHide1stLienLifetimeFinanceCharge");
            }
        }

        public EditorStatus LcrHide2ndLienLifetimeFinanceCharge
        {
            get
            {
                return RetrieveEditorStatusForTempBit("LcrHide2ndLienLifetimeFinanceCharge");
            }
        }

        public EditorStatus LcrHideEstimatedReserves
        {
            get
            {
                return RetrieveEditorStatusForTempBit("LcrHideEstimatedReserves");
            }
        }

        public EditorStatus LcrHideHorizon
        {
            get
            {
                return RetrieveEditorStatusForTempBit("LcrHideHorizon");
            }
        }

        public bool EnableSubFees
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableSubFees\" value=\"true\" />");
            }
        }

        public bool EnableEnhancedTitleQuotes
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableEnhancedTitleQuotes\" value=\"true\" />");
            }
        }

        public bool DisableNewTitleFramework
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"DisableNewTitleFramework\" value=\"true\" />");
            }
        }

        public bool IsCustomWordFormsEnabled
        {
            get
            {
                return IsStringExistedInTempOption("<option name=\"EnableCustomWordForms\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Gets a value indicating whether new blank files (loans and leads) should use the 
        /// ULAD data layer.
        /// </summary>
        /// <remarks>
        /// This is intentionally excluded from the broker editor. This functionality is not
        /// ready for production, and accidentally enabling it through the broker editor on 
        /// prod would be a disaster. It is intended to support feature development and allow
        /// testing before the ULAD changes go live.
        /// </remarks>
        public bool CreateBlankFilesOnUladDataLayer
        {
            get
            {
                // If we're not letting users manually change files to use the ULAD data layer,
                // we don't want to create files directly onto it either.
                if (ConstStage.DisableBorrowerApplicationCollectionTUpdates)
                {
                    return false;
                }

                return IsStringExistedInTempOption("<option name=\"CreateBlankFilesOnUladDataLayer\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the ULDD exporter should send the first non bought-down payment as the
        /// InitialPrincipalAndInterestPaymentAmount value.
        /// </summary>
        public bool ExportFirstNonBoughtDownPaymentAsInitialPaymentInUldd
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"ExportFirstNonBoughtDownPaymentAsInitialPaymentInUldd\" value=\"true\" />");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the lender is using the legacy findings HTML for the seamless DU integration.
        /// </summary>
        public bool UsingLegacyFindingsForSeamlessDU
        {
            get
            {
                return this.IsStringExistedInTempOption("<option name=\"UsingLegacyFindingsForSeamlessDU\" value=\"true\" />");
            }
        }

        private IEnumerable<LockPolicy> LockPolicies
        {
            get 
            {
                if (this.cachedLockPolicies == null)
                {
                    this.cachedLockPolicies = LockPolicy.RetrieveAllForBroker(this.BrokerID);
                }

                return this.cachedLockPolicies;
            }
        }

        /// <summary>
        /// Determines if the lender is open for business on a given day based on lock policies.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns>
        /// True if ANY of the lender's lock policies are open for business on 
        /// the given day and it is not one of the holiday business closures.
        /// </returns>
        public bool IsBusinessDay(DateTime date)
        {
            return this.LockPolicies.Any(lp => lp.IsOpenForBusiness(date));
        }

        private DateTime? lastBranchUpdateTime = null;
        private List<KeyValuePair<Guid, string>> cachedBranchInfo = null;
        public IEnumerable<KeyValuePair<Guid, string>> GetBranches()
        {
            if (this.cachedBranchInfo == null ||
                !lastBranchUpdateTime.HasValue ||
                DateTime.Now > lastBranchUpdateTime.Value.AddMinutes(5))
            {
                this.cachedBranchInfo = new List<KeyValuePair<Guid, string>>();

                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@BrokerId", this.BrokerID)
                };

                using (IDataReader dataReader = StoredProcedureHelper.ExecuteReader(
                    this.BrokerID,
                    "ListBranchByBrokerId", 
                    parameters))
                {
                    while (dataReader.Read())
                    {
                        this.cachedBranchInfo.Add(
                            new KeyValuePair<Guid, string>(
                                new Guid(dataReader["BranchID"].ToString()),
                                dataReader["Name"].ToString()));
                    }
                }

                lastBranchUpdateTime = DateTime.Now;
            }

            return this.cachedBranchInfo;
        }

        private DateTime? lastPriceGroupUpdateTime = null;
        private List<KeyValuePair<Guid, string>> cachedPriceGroupInfo = null;
        public IEnumerable<KeyValuePair<Guid, string>> GetPriceGroups()
        {
            if (this.cachedPriceGroupInfo == null || 
                !lastPriceGroupUpdateTime.HasValue || 
                DateTime.Now > lastPriceGroupUpdateTime.Value.AddMinutes(5))
            {
                this.cachedPriceGroupInfo = new List<KeyValuePair<Guid, string>>();

                var parameters = new SqlParameter[] 
                {
                    new SqlParameter("@BrokerId", this.BrokerID),
                    new SqlParameter("@ExternalPriceGroupEnabled", 1)
                };

                using(IDataReader dataReader = StoredProcedureHelper.ExecuteReader(
                    this.BrokerID,
                    "ListPricingGroupByBrokerId",
                    parameters))
                {
                    while (dataReader.Read())
                    {
                        this.cachedPriceGroupInfo.Add(
                            new KeyValuePair<Guid, string>(
                                new Guid(dataReader["LpePriceGroupId"].ToString()),
                                dataReader["LpePriceGroupName"].ToString()));
                    }
                }

                lastPriceGroupUpdateTime = DateTime.Now;
            }

            return this.cachedPriceGroupInfo;
        }

        private List<KeyValuePair<Guid, string>> cachedLandingPageInfo = null;
        public IEnumerable<KeyValuePair<Guid, string>> GetLandingPages()
        {
            if (this.cachedLandingPageInfo == null)
            {
                this.cachedLandingPageInfo = new List<KeyValuePair<Guid, string>>();

                foreach (var config in LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(this.BrokerID))
                {
                    this.cachedLandingPageInfo.Add(
                        new KeyValuePair<Guid,string>(
                            config.ID.Value,
                            config.Name));
                }
            }

            return this.cachedLandingPageInfo;
        }

        private static Guid? pmlMaster;
        private static Guid? adminBr;

        public static Guid PmlMaster
        {
            get
            {
                if (!pmlMaster.HasValue)
                {
                    LoadSpecialBrokerIDs();
                }

                return pmlMaster.Value;
            }
        }

        public static Guid AdminBr
        {
            get
            {
                if (!adminBr.HasValue)
                {
                    LoadSpecialBrokerIDs();
                }

                return adminBr.Value;
            }
        }

        private static void LoadSpecialBrokerIDs()
        {
            // Retrieve a predefine list of special broker by customer code.
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DB_CONNECTION_x_BROKER_ListSpecialBrokerId"))
            {
                while (reader.Read())
                {
                    string customerCode = (string)reader["CustomerCode"];
                    Guid brokerId = (Guid)reader["BrokerId"];

                    if (customerCode == "PMLMASTER")
                    {
                        pmlMaster = brokerId;
                    }
                    else if (customerCode == "ADMINBR")
                    {
                        adminBr = brokerId;
                    }
                    else
                    {
                        throw new CBaseException("Unhandle Customer Code:[" + customerCode + "] for special broker", "Unhandle Customer Code:[" + customerCode + "] for special broker");
                    }
                }
            }
        }

        /// <summary>
        /// Checks that all fee descriptions in the fee setup are unique.
        /// </summary>
        /// <param name="set">The broker fee setup.</param>
        /// <param name="feeSetupSetFilter">The filter for the passed in set.</param>
        private void CheckUniqueDiscriptions(FeeSetupClosingCostSet set, Func<BaseClosingCostFee, bool> feeSetupSetFilter)
        {
            HashSet<string> descriptions = new HashSet<string>();   // Fee descriptions seen so far.
            foreach (FeeSetupClosingCostFee fee in set.GetFees(feeSetupSetFilter))
            {
                // We want check to ignore case, so convert description to upper case before running check.
                string descUpper = fee.Description.ToUpper();

                // If we've encountered this description before, then descriptions aren't unique.
                if (descriptions.Contains(descUpper))
                {
                    throw new CBaseException("Fee descriptions must be unique.", "Cannot save Fee Setup because fee descriptions are not unique.");
                }

                // Add descritption to set of descriptions seen so far.
                descriptions.Add(descUpper);
            }
        }

        /// <summary>
        /// Obtains the rolodex system ID counter for the broker, incrementing
        /// the counter at the same time.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="exec">
        /// The context of the stored procedure call.
        /// </param>
        /// <returns>
        /// The current rolodex system ID counter for the broker prior to 
        /// incrementing the value.
        /// </returns>
        public static int GetAndUpdateRolodexSystemIdCounter(Guid brokerId, CStoredProcedureExec exec)
        {
            var storedProcedureName = StoredProcedureName.Create("BROKER_GetAndUpdateRolodexSystemIdCounter");
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = exec.ExecuteReader(storedProcedureName.Value.ToString(), parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["Counter"];
                }

                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Obtains the branch ID number counter for the broker, incrementing
        /// the counter at the same time.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The current branch ID number counter for the broker prior to 
        /// incrementing the value.
        /// </returns>
        public static int GetAndUpdateBranchIdNumberCounter(Guid brokerId)
        {
            var storedProcedureName = StoredProcedureName.Create("BROKER_GetAndUpdateBranchIdNumberCounter");
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, storedProcedureName.Value, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return (int)reader["BranchIdNumberCounter"];
                }

                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Obtains the originating company index number counter for the lender,
        /// incrementing the counter at the same time.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The current originating company index number counter for the broker 
        /// prior to incrementing the value.
        /// </returns>
        public static int GetAndUpdateOriginatingCompanyIndexNumberCounter(Guid brokerId)
        {
            var storedProcedureName = StoredProcedureName.Create("BROKER_GetAndUpdateOriginatingCompanyIndexNumberCounter");
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, storedProcedureName.Value, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return (int)reader["OriginatingCompanyIndexNumberCounter"];
                }

                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }

    /// <summary>
    /// Maintain a set of customizeable options for the
    /// specified field.  We use field ids as names.
    /// </summary>

    [XmlRoot]
    public class FieldOptions
    {
        /// <summary>
        /// Maintain a set of customizeable options for
        /// the specified field.  We use field ids as
        /// names.
        /// </summary>

        private String   m_FieldName = "";
        private String m_Description = "";
        private ArrayList  m_Options = new ArrayList();

        #region ( Item properties )

        [ XmlElement
        ]
        public String FieldName
        {
            // Access member.

            set
            {
                m_FieldName = value;
            }
            get
            {
                return m_FieldName;
            }
        }

        [ XmlElement
        ]
        public String Description
        {
            // Access member.

            set
            {
                m_Description = value;
            }
            get
            {
                return m_Description;
            }
        }

        [ XmlArray
        ]
        [ XmlArrayItem( typeof( String ) )
        ]
        public ArrayList Options
        {
            // Access member.

            set
            {
                m_Options = value;
            }
            get
            {
                return m_Options;
            }
        }

        #endregion

        /// <summary>
        /// Append a new option to our set.  Unique only
        /// allowed.
        /// </summary>

        public void Add( String sOption )
        {
            // Append a new option to our set.  Unique
            // only allowed.

            String sOpt;

            if( sOption == null )
            {
                return;
            }

            sOpt = sOption.TrimWhitespaceAndBOM();

            foreach( String sO in m_Options )
            {
                if( sO == sOpt )
                {
                    return;
                }
            }

            m_Options.Add( sOpt );
        }

    }

    /// <summary>
    /// Keep track of complete field options set in one
    /// place for a single broker.
    /// </summary>

    [XmlRoot]
    public class OptionSet
    {
        /// <summary>
        /// Keep track of complete field options set in
        /// one place for a single broker.
        /// </summary>

        private ArrayList m_List = new ArrayList();

        #region ( Set properties )

        [ XmlIgnore
        ]
        public FieldOptions this[ String sFieldName ]
        {
            // Access member.

            get
            {
                foreach( FieldOptions fOpts in m_List )
                {
                    if( fOpts.FieldName == sFieldName )
                    {
                        return fOpts;
                    }
                }

                return null;
            }
        }

        [ XmlIgnore
        ]
        public FieldOptions this[ int iOption ]
        {
            // Access member.

            get
            {
                if( iOption >= 0 && iOption < m_List.Count )
                {
                    return m_List[ iOption ] as FieldOptions;
                }

                return null;
            }
        }

        [ XmlArray
        ]
        [ XmlArrayItem( typeof( FieldOptions ) )
        ]
        public ArrayList List
        {
            // Access member.

            set
            {
                m_List = value;
            }
            get
            {
                return m_List;
            }
        }

        #endregion

        /// <summary>
        /// Insert this option list into our set.  Existing
        /// entries are replaced.
        /// </summary>

        public void Add( FieldOptions fOptions )
        {
            // Insert this option list into our set.
            // Existing entries are replaced.

            if( fOptions == null || fOptions.FieldName == null )
            {
                return;
            }

            foreach( FieldOptions fOpts in m_List )
            {
                if( fOpts.FieldName == fOptions.FieldName )
                {
                    fOpts.Description = fOptions.Description;
                    fOpts.Options     = fOptions.Options;

                    return;
                }
            }

            m_List.Add( fOptions );
        }

        /// <summary>
        /// Return looping interface pointer for walking.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return looping interface pointer for walking.

            return m_List.GetEnumerator();
        }

        /// <summary>
        /// Reset the set and clear out all entries.
        /// </summary>

        public void Clear()
        {
            // Reset the set and clear out all entries.

            m_List.Clear();
        }

        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our set.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>

        public static OptionSet ToObject( String sXml )
        {
            // Deserialize the given string to our list.

            XmlSerializer xS = new XmlSerializer( typeof( OptionSet ) );
            StringReader  sR = new StringReader( sXml );

            return xS.Deserialize( sR ) as OptionSet;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            // Serialize this instance as an xml document.

            XmlSerializer xS = new XmlSerializer( typeof( OptionSet ) );
            StringWriter8 s8 = new StringWriter8();

            xS.Serialize( s8 , this );

            return s8.ToString();
        }

        #endregion

    }

    public class LeadSource : IComparable<LeadSource>
    {
        [XmlTextAttribute]
        public string Name;

        [XmlAttribute]
        public bool Deleted;

        [XmlAttribute]
        public int Id;

        #region IComparable<LeadSource> Members

        public int CompareTo(LeadSource other)
        {
            if (this.Name == null && other.Name == null) return 0;
            if (this.Name == null) return int.MaxValue;
            if (other.Name == null) return int.MinValue;
            return this.Name.CompareTo(other.Name);
        }

        #endregion
    }

    //Make sure it's called an ItemList for backwards compat.
    [XmlRoot(ElementName="ItemList")]
    public class LeadSourceList
    {
        private List<LeadSource> m_List = new List<LeadSource>();

        #region ( List properties )

        //Each element is serialized as <string> for backwards compatibility.
        [XmlArray, XmlArrayItem("string")]
        public List<LeadSource> List
        {
            set
            {
                m_List = value;
            }
            get
            {
                return m_List;
            }
        }

        #endregion

        private int GetNextId()
        {
            var nonZero = m_List.Where(a => a.Id >= 0);
            if (nonZero.Count() == 0) return 1;

            return nonZero.Max(a => a.Id) + 1;
        }

        /// <summary>
        /// Returns the first lead source with the given id, or null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LeadSource GetById(int id)
        {
            return m_List.Where(a => a.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Returns the first lead source with the given name, or null
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public LeadSource GetByName(string name)
        {
            return m_List.Where(a => (a.Name ?? "").Equals(name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
        }

        /// <summary>
        /// Adds sItem to the end of the list.
        /// </summary>
        /// <param name="sItem">Item to add. Will throw an exception if null.</param>
        public void Add(LeadSource sItem)
        {
            if (sItem == null)
            {
                throw new ArgumentException("Invalid list item.");
            }
            if (sItem.Name == ConstApp.SystemDefualtBlankLeadSourceName && sItem.Id == ConstApp.SystemDefaultBlankLeadSourceId)
            {
                throw new CBaseException(ErrorMessages.Generic, "The default lead source should not be added to the LeadSourceList in this manner.");
            }
            if (sItem.Id <= 0) sItem.Id = GetNextId();

            m_List.Add(sItem);
        }

        /// <summary>
        /// Return looping interface pointer for walking.
        /// </summary>

        public IEnumerator<LeadSource> GetEnumerator()
        {
            return m_List.Where(a => !a.Deleted).OrderBy(a => a.Name).GetEnumerator();
        }

        /// <summary>
        /// Reset the set and clear out all entries.
        /// </summary>

        public void Clear()
        {
            m_List.Clear();
        }

        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our set.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>

        public static LeadSourceList ToObject(String sXml)
        {
            XmlSerializer xS = new XmlSerializer(typeof(LeadSourceList));
            StringReader sR = new StringReader(sXml);

            return xS.Deserialize(sR) as LeadSourceList;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            XmlSerializer xS = new XmlSerializer(typeof(LeadSourceList));
            StringWriter8 s8 = new StringWriter8();

            xS.Serialize(s8, this);

            return s8.ToString();
        }

        #endregion

    }

    public class PmlCompanyTier : IComparable<PmlCompanyTier>
    {
        [XmlTextAttribute]
        public string Name;

        [XmlAttribute]
        public bool Deleted;

        [XmlAttribute]
        public int Id;

        #region IComparable<LeadSource> Members

        public int CompareTo(PmlCompanyTier other)
        {
            if (this.Name == null && other.Name == null) return 0;
            if (this.Name == null) return int.MaxValue;
            if (other.Name == null) return int.MinValue;
            return this.Name.CompareTo(other.Name);
        }

        #endregion
    }

    //Make sure it's called an ItemList for backwards compat.
    [XmlRoot(ElementName = "ItemList")]
    public class PmlCompanyTierList
    {
        private List<PmlCompanyTier> m_List = new List<PmlCompanyTier>();

        #region ( List properties )

        //Each element is serialized as <string> for backwards compatibility.
        [XmlArray, XmlArrayItem("string")]
        public List<PmlCompanyTier> List
        {
            set
            {
                m_List = value;
            }
            get
            {
                return m_List;
            }
        }

        #endregion

        private int GetNextId()
        {
            var nonZero = m_List.Where(a => a.Id >= 0);
            if (nonZero.Count() == 0) return 1;

            return nonZero.Max(a => a.Id) + 1;
        }

        /// <summary>
        /// Returns the first originating company tier with the given id, or null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PmlCompanyTier GetById(int id)
        {
            return m_List.Where(a => a.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Returns the first originating company tier with the given name, or null
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PmlCompanyTier GetByName(string name)
        {
            return m_List.Where(a => (a.Name ?? "").Equals(name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
        }

        /// <summary>
        /// Adds sItem to the end of the list.
        /// </summary>
        /// <param name="sItem">Item to add. Will throw an exception if null.</param>
        public void Add(PmlCompanyTier sItem)
        {
            if (sItem == null)
            {
                throw new ArgumentException("Invalid list item.");
            }
            if (sItem.Name == ConstApp.SystemDefualtBlankPmlCompanyTierName && sItem.Id == ConstApp.SystemDefaultBlankPmlCompanyTierId)
            {
                throw new CBaseException(ErrorMessages.Generic, "The default originating company tier should not be added to the PmlCompanyTierList in this manner.");
            }
            if (sItem.Id <= 0) sItem.Id = GetNextId();

            m_List.Add(sItem);
        }

        /// <summary>
        /// Return looping interface pointer for walking.
        /// </summary>

        public IEnumerator<PmlCompanyTier> GetEnumerator()
        {
            return m_List.Where(a => !a.Deleted).OrderBy(a => a.Name).GetEnumerator();
        }

        /// <summary>
        /// Reset the set and clear out all entries.
        /// </summary>

        public void Clear()
        {
            m_List.Clear();
        }

        #region ( Serialization api )

        /// <summary>
        /// Deserialize the given string to our set.
        /// </summary>
        /// <param name="sXml">
        /// Xml document as string.
        /// </param>
        /// <returns>
        /// Deserialized instance.
        /// </returns>

        public static PmlCompanyTierList ToObject(String sXml)
        {
            XmlSerializer xS = new XmlSerializer(typeof(PmlCompanyTierList));
            StringReader sR = new StringReader(sXml);

            return xS.Deserialize(sR) as PmlCompanyTierList;
        }

        /// <summary>
        /// Serialize this instance as an xml document.
        /// </summary>
        /// <returns>
        /// String representation of this list as xml.
        /// </returns>

        public override String ToString()
        {
            XmlSerializer xS = new XmlSerializer(typeof(PmlCompanyTierList));
            StringWriter8 s8 = new StringWriter8();

            xS.Serialize(s8, this);

            return s8.ToString();
        }

        #endregion

    }

    public struct BranchInfo
    {
        public readonly string Name;
        public readonly Guid ID;

        public BranchInfo(string name, Guid id)
        {
            this.Name = name;
            this.ID = id;
        }
    }
}
