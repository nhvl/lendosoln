﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Admin
{
    public enum E_RatesheetExpirationBypassType
    {
        NoBypass = 0,
        BypassAllExceptInvCutoff = 1,
        BypassAll = 2
    }
}
