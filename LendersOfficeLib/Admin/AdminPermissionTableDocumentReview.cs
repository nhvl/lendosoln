﻿// <copyright file="AdminPermissionTableDocumentReview.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The document review permission table.
    /// </summary>
    public class AdminPermissionTableDocumentReview : AdminPermissionTable
    {
        /// <summary>
        /// Geta a value indicating whether the passed in description is part of this table.
        /// </summary>
        /// <param name="pDesc">The description that is being checked.</param>
        /// <returns>True if the description is part of this permission table.</returns>
        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Security.Permission.AllowAccessingOCRReviewPortal:
                    return true;
                default:
                    return false;
            }
        }
    }
}
