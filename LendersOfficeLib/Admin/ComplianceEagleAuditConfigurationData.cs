﻿namespace LendersOffice.Admin
{
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Contains the lender-level configuration data for the automated ComplianceEagle export.
    /// </summary>
    public class ComplianceEagleAuditConfigurationData
    {
        /// <summary>
        /// Gets or sets the loan status at which automated updates should begin to run.
        /// </summary>
        /// <value>The loan status at which automated updates should begin to run.</value>
        public E_sStatusT BeginRunningAuditsStatus { get; set; }

        /// <summary>
        /// Gets or sets the loan statuses the lender would like to exclude from automated compliance runs.
        /// </summary>
        /// <value>The loan statuses the lender would like to exclude from automated compliance runs.</value>
        public IEnumerable<E_sStatusT> ExcludedStatuses { get; set; }
    }
}
