﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    /// <summary>
    /// Provide public version of the permission table for binding
    /// to ui that our customers will use.
    /// </summary>


    public class AdminPermissionTableLoans : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.AllowLoanAssignmentsToAnyBranch:
                case Permission.CanAccessClosedLoans:
                case Permission.CanModifyLoanName:
                //case Permission.CanDeleteLoan: // 11/2/2010 dd - This permission is obsolete use the User Group instead.
                case Permission.CanViewLoanInEditor:
                case Permission.CanExportLoan:
                case Permission.CanWriteNonAssignedLoan:
                case Permission.CanDuplicateLoans: // OPM 3078
                case Permission.CanAccessLendingStaffNotes:
                case Permission.AllowOrderingTitleServices:
                case Permission.AllowUnderwritingAccess:
                case Permission.AllowQualityControlAccess:
                case Permission.AllowEditingLoanProgramName:
                case Permission.AllowEnablingCustomFeeDescriptions:
                case Permission.AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox:
                case Permission.AllowCreatingNewLoanFiles:
                case Permission.AllowEnableGfePaidToManualDesc:
                case Permission.AllowOverridingContactLicenses:
                case Permission.AllowAppraisalDelivery:
                    return true;
                case Permission.AllowOrderingPMIPolicies:
                {
                    if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return true;
                    }
                    else
                    {
                        BrokerDB CurrentBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                        return CurrentBroker.HasMIVendorIntegration;
                    }

                }
                case Permission.AllowOrder4506T:
                    if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return true;
                    }
                    else
                    {
                        BrokerDB CurrentBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                        return CurrentBroker.Has4506TIntegration;

                    }
                case Permission.AllowOrderingGlobalDMSAppraisals:
                    if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return true;
                    }
                    else
                    {
                        BrokerDB CurrentBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                        return CurrentBroker.IsAllowOrderingGlobalDmsAppraisals ? true : false;
                    }
                case Permission.AllowManuallyArchivingGFE:
                    if(PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return true;
                    }
                    else
                    {
                        BrokerDB CurrentBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                        return CurrentBroker.IsGFEandCoCVersioningEnabled;
                    }
                case Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage:
                case Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage:
                    if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                    {
                        return true;
                    }
                    else
                    {
                        BrokerDB CurrentBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                        return CurrentBroker.EnableKtaIntegration;
                    }
            }
            return false;
        }
        #endregion
    }
}
