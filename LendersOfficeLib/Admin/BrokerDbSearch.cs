﻿// <copyright file="BrokerDbSearch.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/28/2014 3:32:15 PM 
// </summary>
namespace LendersOffice.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Searching Broker information.
    /// </summary>
    public static class BrokerDbSearch
    {
        /// <summary>
        /// Search for a list of brokers matching the filter.
        /// </summary>
        /// <param name="name">Partial match of broker name. Leave empty to search for all.</param>
        /// <param name="fannieUserId">Partial match of DU user id. Leave empty to search for all.</param>
        /// <param name="isActive">Broker status. Pass null for search for all status.</param>
        /// <param name="customerCode">Customer code of broker.</param>
        /// <param name="brokerId">Search by broker id. Leave Guid.Empty to search for all.</param>
        /// <param name="suiteType">Suite type of the broker or null for all suite types.</param>
        /// <returns>List of brokers matching the filter.</returns>
        public static IEnumerable<BrokerDbSearchItem> Search(string name, string fannieUserId, bool? isActive, string customerCode, Guid brokerId, BrokerSuiteType? suiteType)
        {
            var brokerDictionary = new Dictionary<Guid, BrokerDbSearchItem>();

            IEnumerable<DbConnectionInfo> connectionInfoList = null;

            if (brokerId == Guid.Empty)
            {
                connectionInfoList = DbConnectionInfo.ListAll();
            }
            else
            {
                DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

                connectionInfoList = new DbConnectionInfo[] { connInfo };
            }

            foreach (DbConnectionInfo connInfo in connectionInfoList)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                if (string.IsNullOrEmpty(name) == false)
                {
                    parameters.Add(new SqlParameter("@BrokerName", name));
                }

                if (string.IsNullOrEmpty(fannieUserId) == false)
                {
                    parameters.Add(new SqlParameter("@DuUserId", fannieUserId));
                }

                if (isActive.HasValue)
                {
                    SqlParameter isActiveParameter = new SqlParameter("@BrokerStatus", SqlDbType.Int);
                    isActiveParameter.Value = isActive.Value ? 1 : 0;

                    parameters.Add(isActiveParameter);
                }

                if (string.IsNullOrEmpty(customerCode) == false)
                {
                    parameters.Add(new SqlParameter("@CustomerCode", customerCode));
                }

                if (brokerId != Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@BrokerId", brokerId));
                }

                if (suiteType.HasValue)
                {
                    parameters.Add(new SqlParameter("@SuiteType", suiteType.Value));
                }

                try
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokers", parameters))
                    {
                        while (reader.Read())
                        {
                            var broker = new BrokerDbSearchItem(reader);

                            // 7/20/2016 - The DB split for LO to LO3 may result in the same broker being present in both DBs
                            // In the event that we have such a duplication, the appropriate behavior is to take the only active broker.
                            BrokerDbSearchItem existingCopy;
                            if (!brokerDictionary.TryGetValue(broker.BrokerId, out existingCopy))
                            {
                                brokerDictionary.Add(broker.BrokerId, broker);
                            }
                            else if (!existingCopy.IsActive && broker.IsActive)
                            {
                                brokerDictionary[broker.BrokerId] = broker;
                            }
                            else if (existingCopy.IsActive && broker.IsActive)
                            {
                                throw CBaseException.GenericException("Found two active copies of the broker with id=" + broker.BrokerId);
                            }
                        }
                    }
                }
                catch (SqlException exc)
                {
                    Tools.LogWarning("BrokerDbSearch failed for " + connInfo.Description, exc);
                }
            }

            return brokerDictionary.Values.OrderBy(o => o.BrokerNm);
        }

        /// <summary>
        /// Return a list of all brokers.
        /// </summary>
        /// <returns>A full list of brokers.</returns>
        public static IEnumerable<BrokerDbSearchItem> SearchAll()
        {
            return Search(string.Empty, string.Empty, null, string.Empty, Guid.Empty, suiteType: null);
        }
    }
}