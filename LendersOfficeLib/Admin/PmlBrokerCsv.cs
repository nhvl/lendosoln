﻿namespace LendersOffice.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Common;
    using Common.TextImport;
    using DataAccess;
    using ObjLib.TPO;
    using ObjLib.UI.Themes;
    using Security;

    /// <summary>
    /// Manages the generation of CSV data about PML brokers.
    /// </summary>
    public static class PmlBrokerCsv
    {
        /// <summary>
        /// Writes data to a file about PML brokers associated with a given broker ID.
        /// </summary>
        /// <param name="brokerId">The lender whose PML brokers should be exported.</param>
        /// <returns>The filename where the CSV data is stored.</returns>
        public static string ExportAllPmlBrokersToFile(Guid brokerId)
        {
            string tempFile = TempFileUtils.NewTempFilePath();
            using (StreamWriter writer = new StreamWriter(tempFile, false, Encoding.UTF8))
            {
                WritePmlBrokerData(writer, brokerId);
            }

            return tempFile;
        }

        /// <summary>
        /// Writes data to a file about PML brokers associated with a given broker ID.
        /// </summary>
        /// <param name="brokerId">The lender whose PML brokers should be exported.</param>
        /// <returns>The filename where the CSV data is stored.</returns>
        public static string ExportAllPmlBrokers(Guid brokerId)
        {
            using (StringWriter writer = new StringWriter())
            {
                WritePmlBrokerData(writer, brokerId);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Retrieves the PML brokers corresponding to a lender and exports their data.
        /// </summary>
        /// <param name="writer">The stream to write to.</param>
        /// <param name="brokerId">The lender whose PML brokers we should export.</param>
        private static void WritePmlBrokerData(TextWriter writer, Guid brokerId)
        {
            var brokers = PmlBroker.ListAllPmlBrokerByBrokerId(brokerId);
            var themeCollection = ThemeManager.GetThemeCollection(brokerId, ThemeCollectionType.TpoPortal);
            bool headersWritten = false;
            foreach (var broker in brokers)
            {
                var brokerData = PrepareBrokerData(broker, themeCollection);

                if (!headersWritten)
                {
                    WriteCsvLine(writer, brokerData.Keys);
                    headersWritten = true;
                }

                WriteCsvLine(writer, brokerData.Values);
            }
        }

        /// <summary>
        /// Writes a CSV line from an enumerated list of values.
        /// </summary>
        /// <param name="writer">A text writer.</param>
        /// <param name="data">A list of values to join into a single CSV line.</param>
        private static void WriteCsvLine(TextWriter writer, ICollection data)
        {
            var escapedData = from string value in data
                              select DataParsing.sanitizeForCSV(value);

            writer.Write(string.Join(",", escapedData));
            writer.WriteLine();
        }

        /// <summary>
        /// Prepares the broker data and converts to string representations as needed.
        /// </summary>
        /// <param name="pmlBroker">The PML broker to export.</param>
        /// <param name="themeCollection">The lender's collection of color themes.</param>
        /// <returns>An ordered dictionary containing the datapoints to export.</returns>
        private static OrderedDictionary PrepareBrokerData(PmlBroker pmlBroker, ThemeCollection themeCollection)
        {
            var brokerData = new OrderedDictionary();
            
            brokerData["Name"] = pmlBroker.Name;
            brokerData["Address"] = pmlBroker.Addr;
            brokerData["City"] = pmlBroker.City;
            brokerData["State"] = pmlBroker.State;
            brokerData["Zip"] = pmlBroker.Zip;
            brokerData["NmlsIdentifier"] = pmlBroker.NmLsIdentifier;
            brokerData["Phone"] = pmlBroker.Phone;
            brokerData["Fax"] = pmlBroker.Fax;
            brokerData["TaxId"] = pmlBroker.TaxId;
            brokerData["CompanyId"] = pmlBroker.CompanyId;
            brokerData["OriginatorCompensationBaseT"] = pmlBroker.OriginatorCompensationBaseT.ToString();
            brokerData["OriginatorCompensationFixedAmount"] = pmlBroker.OriginatorCompensationFixedAmount.ToString();
            brokerData["OriginatorCompensationLastModifiedDate"] = pmlBroker.OriginatorCompensationLastModifiedDate.ToString();
            brokerData["OriginatorCompensationMaxAmount"] = pmlBroker.OriginatorCompensationMaxAmount.ToString();
            brokerData["OriginatorCompensationMinAmount"] = pmlBroker.OriginatorCompensationMinAmount.ToString();
            brokerData["OriginatorCompensationPercent"] = pmlBroker.OriginatorCompensationPercent.ToString();
            brokerData["OriginatorCompensationIsOnlyPaidForFirstLienOfCombo"] = pmlBroker.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.ToString();
            brokerData["OriginatorCompensationNotes"] = pmlBroker.OriginatorCompensationNotes;
            brokerData["IsActive"] = pmlBroker.IsActive.ToString();
            brokerData["NMLS Name Locked"] = pmlBroker.NmlsNameLckd.ToString();
            brokerData["NMLS Name"] = pmlBroker.NmlsName;
            brokerData["Principal Name 1"] = pmlBroker.PrincipalName1;
            brokerData["Principal Name 2"] = pmlBroker.PrincipalName2;

            var brokerDb = BrokerDB.RetrieveById(pmlBroker.BrokerId);
            brokerData["CustomPricingPolicy1"] = RetrieveCustomPricingPolicyValue(pmlBroker.CustomPricingPolicyField1, 1, brokerDb);
            brokerData["CustomPricingPolicy2"] = RetrieveCustomPricingPolicyValue(pmlBroker.CustomPricingPolicyField2, 2, brokerDb);
            brokerData["CustomPricingPolicy3"] = RetrieveCustomPricingPolicyValue(pmlBroker.CustomPricingPolicyField3, 3, brokerDb);
            brokerData["CustomPricingPolicy4"] = RetrieveCustomPricingPolicyValue(pmlBroker.CustomPricingPolicyField4, 4, brokerDb);
            brokerData["CustomPricingPolicy5"] = RetrieveCustomPricingPolicyValue(pmlBroker.CustomPricingPolicyField5, 5, brokerDb);
            brokerData["Company Tier"] = RetrieveCompanyTierName(pmlBroker.PmlCompanyTierId, brokerDb);

            // Broker
            bool hasBrokerRole = pmlBroker.Roles.Contains(E_OCRoles.Broker);
            brokerData["Has Broker Role?"] = hasBrokerRole.ToString();
            brokerData["Broker Status"] = hasBrokerRole ? pmlBroker.BrokerRoleStatusNm : OCStatusType.Inactive.ToString();
            brokerData["Broker Branch"] = pmlBroker.BranchNm;
            brokerData["Broker Price Group"] = pmlBroker.LpePriceGroupName;
            brokerData["Broker Landing Page"] = RetrieveLandingPageName(pmlBroker.TPOPortalConfigID, pmlBroker.BrokerId);
            brokerData["Broker Relationship: Manager"] = pmlBroker.BrokerRoleRelationships[E_RoleT.Manager].Name;
            brokerData["Broker Relationship: Processor"] = pmlBroker.BrokerRoleRelationships[E_RoleT.Processor].Name;
            brokerData["Broker Relationship: Junior Processor"] = pmlBroker.BrokerRoleRelationships[E_RoleT.JuniorProcessor].Name;
            brokerData["Broker Relationship: Account Exec"] = pmlBroker.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive].Name;
            brokerData["Broker Relationship: Underwriter"] = pmlBroker.BrokerRoleRelationships[E_RoleT.Underwriter].Name;
            brokerData["Broker Relationship: Junior Underwriter"] = pmlBroker.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter].Name;
            brokerData["Broker Relationship: Lock Desk"] = pmlBroker.BrokerRoleRelationships[E_RoleT.LockDesk].Name;

            // Mini-Correspondent
            bool hasMiniCorrRole = pmlBroker.Roles.Contains(E_OCRoles.MiniCorrespondent);
            brokerData["Has Mini-Correspondent Role?"] = hasMiniCorrRole.ToString();
            brokerData["Mini-Correspondent Status"] = hasMiniCorrRole ? pmlBroker.MiniCorrRoleStatusNm : OCStatusType.Inactive.ToString();
            brokerData["Mini-Correspondent Branch"] = pmlBroker.MiniCorrespondentBranchNm;
            brokerData["Mini-Correspondent Price Group"] = pmlBroker.MiniCorrespondentLpePriceGroupName;
            brokerData["Mini-Correspondent Landing Page"] = RetrieveLandingPageName(pmlBroker.MiniCorrespondentTPOPortalConfigID, pmlBroker.BrokerId);
            brokerData["Mini-Corr Relationship: Manager"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Name;
            brokerData["Mini-Corr Relationship: Processor"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.Processor].Name;
            brokerData["Mini-Corr Relationship: Junior Processor"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor].Name;
            brokerData["Mini-Corr Relationship: Account Exec"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive].Name;
            brokerData["Mini-Corr Relationship: Underwriter"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.Underwriter].Name;
            brokerData["Mini-Corr Relationship: Junior Underwriter"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter].Name;
            brokerData["Mini-Corr Relationship: Credit Auditor"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.CreditAuditor].Name;
            brokerData["Mini-Corr Relationship: Legal Auditor"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.LegalAuditor].Name;
            brokerData["Mini-Corr Relationship: Lock Desk"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.LockDesk].Name;
            brokerData["Mini-Corr Relationship: Purchaser"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.Purchaser].Name;
            brokerData["Mini-Corr Relationship: Secondary"] = pmlBroker.MiniCorrRoleRelationships[E_RoleT.Secondary].Name;

            // Correspondent
            bool hasCorrRole = pmlBroker.Roles.Contains(E_OCRoles.Correspondent);
            brokerData["Has Correspondent Role?"] = hasCorrRole.ToString();
            brokerData["Correspondent Underwriting Authority"] = pmlBroker.UnderwritingAuthority.ToString();
            brokerData["Correspondent Status"] = hasCorrRole ? pmlBroker.CorrRoleStatusNm : OCStatusType.Inactive.ToString();
            brokerData["Correspondent Branch"] = pmlBroker.CorrespondentBranchNm;
            brokerData["Correspondent Price Group"] = pmlBroker.CorrespondentLpePriceGroupName;
            brokerData["Correspondent Landing Page"] = RetrieveLandingPageName(pmlBroker.CorrespondentTPOPortalConfigID, pmlBroker.BrokerId);
            brokerData["Correspondent Relationship: Manager"] = pmlBroker.CorrRoleRelationships[E_RoleT.Manager].Name;
            brokerData["Correspondent Relationship: Processor"] = pmlBroker.CorrRoleRelationships[E_RoleT.Processor].Name;
            brokerData["Correspondent Relationship: Junior Processor"] = pmlBroker.CorrRoleRelationships[E_RoleT.JuniorProcessor].Name;
            brokerData["Correspondent Relationship: Account Exec"] = pmlBroker.CorrRoleRelationships[E_RoleT.LenderAccountExecutive].Name;
            brokerData["Correspondent Relationship: Underwriter"] = pmlBroker.CorrRoleRelationships[E_RoleT.Underwriter].Name;
            brokerData["Correspondent Relationship: Junior Underwriter"] = pmlBroker.CorrRoleRelationships[E_RoleT.JuniorUnderwriter].Name;
            brokerData["Correspondent Relationship: Credit Auditor"] = pmlBroker.CorrRoleRelationships[E_RoleT.CreditAuditor].Name;
            brokerData["Correspondent Relationship: Legal Auditor"] = pmlBroker.CorrRoleRelationships[E_RoleT.LegalAuditor].Name;
            brokerData["Correspondent Relationship: Lock Desk"] = pmlBroker.CorrRoleRelationships[E_RoleT.LockDesk].Name;
            brokerData["Correspondent Relationship: Purchaser"] = pmlBroker.CorrRoleRelationships[E_RoleT.Purchaser].Name;
            brokerData["Correspondent Relationship: Secondary"] = pmlBroker.CorrRoleRelationships[E_RoleT.Secondary].Name;

            brokerData["Licenses"] = FormatLicenses(pmlBroker.LicenseInfoList);

            brokerData["TPO Portal Color Theme"] = RetrieveColorThemeName(pmlBroker.TpoColorThemeId ?? themeCollection.SelectedThemeId, themeCollection);
            brokerData["Company Index Number"] = pmlBroker.IndexNumber.ToString();

            return brokerData;
        }

        /// <summary>
        /// Retrieves the value of a custom pricing policy field.
        /// </summary>
        /// <param name="fieldValue">The value of the custom field.</param>
        /// <param name="index">The index of the custom pricing field.</param>
        /// <param name="brokerDb">The broker configuration holding the custom fields.</param>
        /// <returns>The value of the corresponding custom pricing policy field, or the empty string if none exists.</returns>
        private static string RetrieveCustomPricingPolicyValue(string fieldValue, int index, BrokerDB brokerDb)
        {
            if (string.IsNullOrEmpty(fieldValue))
            {
                return string.Empty;
            }

            if (brokerDb.CustomPricingPolicyFieldList.IsFieldDefined(index))
            {
                var customField = brokerDb.CustomPricingPolicyFieldList.Get(index);

                if (customField.EnumMap.Any())
                {
                    // This is a dropdown and we have to get the mapped value.
                    int enumIndex;
                    if (int.TryParse(fieldValue, out enumIndex) && customField.EnumMap.ContainsKey(enumIndex))
                    {
                        return customField.EnumMap[enumIndex];
                    }
                }
                else
                {
                    return fieldValue;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the company tier name corresponding to the given ID.
        /// </summary>
        /// <param name="companyTierId">The company tier ID.</param>
        /// <param name="brokerDb">The broker data.</param>
        /// <returns>The corresponding company tier name, or the empty string if none was found.</returns>
        private static string RetrieveCompanyTierName(int companyTierId, BrokerDB brokerDb)
        {
            var companyTier = brokerDb.PmlCompanyTiers.GetById(companyTierId);
            return companyTier?.Name ?? string.Empty;
        }

        /// <summary>
        /// Retrieves the name of a TPO landing page, if applicable.
        /// </summary>
        /// <param name="portalConfigId">The TPO portal config ID corresponding to the page.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <returns>The name of a TPO landing page, or the empty string if none was found.</returns>
        private static string RetrieveLandingPageName(Guid? portalConfigId, Guid brokerId)
        {
            if (portalConfigId == null || portalConfigId.Value == Guid.Empty)
            {
                return string.Empty;
            }

            try
            {
                var portalConfig = TPOPortalConfig.RetrieveByID(portalConfigId.Value, brokerId);
                return portalConfig.Name;
            }
            catch (NotFoundException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Formats a set of licenses for export.
        /// </summary>
        /// <param name="licenses">A set of licenses.</param>
        /// <returns>A formatted string containing all licenses in the set.</returns>
        private static string FormatLicenses(LicenseInfoList licenses)
        {
            var licenseData = new List<string>();
            foreach (LicenseInfo license in licenses.List)
            {
                licenseData.Add($"{license.License} -State: {license.State} -Expires {license.ExpD}");
            }

            return string.Join(" + ", licenseData);
        }

        /// <summary>
        /// Retrieves the name of the color theme.
        /// </summary>
        /// <param name="selectedThemeId">
        /// The selected color theme ID.
        /// </param>
        /// <param name="themeCollection">
        /// The lender's theme collection.
        /// </param>
        /// <returns>
        /// The name of the theme.
        /// </returns>
        private static string RetrieveColorThemeName(Guid selectedThemeId, ThemeCollection themeCollection)
        {
            var selectedTheme = themeCollection.GetTheme(selectedThemeId);
            return selectedTheme.Name;
        }
    }
}
