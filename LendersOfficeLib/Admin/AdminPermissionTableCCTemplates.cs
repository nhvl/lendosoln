﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    // OPM 3453
    public class AdminPermissionTableCCTemplates : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanAccessCCTemplates:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
