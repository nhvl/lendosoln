﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableCapitalMarkets : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.AllowCapitalMarketsAccess:
                    return true;
            }
            return false;
        }
        #endregion
    }
}
