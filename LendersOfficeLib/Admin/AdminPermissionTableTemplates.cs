﻿using System;
using System.Collections;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
    public class AdminPermissionTableTemplates : AdminPermissionTable
    {
        #region ( Inclusion test )

        protected override bool IsIncluded(Desc pDesc)
        {
            switch (pDesc.Code)
            {
                case Permission.CanCreateLoanTemplate:
                case Permission.AllowAccessToTemplatesOfAnyBranch:
                case Permission.CanEditLoanTemplates:
                    return true;
            }
            return false;
        }

        #endregion
    }
}
