using System;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using DataAccess;
using System.Data;
using System.Globalization;

namespace Toolbox.Distributed
{

	public class CLpeLoadingDeniedException : CBaseException
	{
		public CLpeLoadingDeniedException( string errUserMsg, string errDevMsg ) : base( errUserMsg, errDevMsg ) {}
	}



	public class CSingletonDistributed : IDisposable
	{
		public enum SingletonObjectT
		{
			LpeDataAccessTicket = 0,
		}
		static public string LPE_DATA_ACCESS_TICKET = "LPE_DATA_ACCESS_TICKET";

        // Prefix name for some special holder that can change lpe data.
        const string LpeUpdatePrefix        = "LpeUpdate";
        const string LoAdminMigrationPrefix = "LoAdminMigration";

        public enum HolderType // who currently owns the singleton
        {
            None,               // nobody owns singleton
            LpeUpdate,  
            LoAdminMigration,   // look at LOAdmin/Manage/Update.aspx
            Other
        }

        static public string GetCurrentHolder( SingletonObjectT objT )
        {
            Tools.Assert( SingletonObjectT.LpeDataAccessTicket == objT, "CSingletonDistributed only supports LPE_DATA_ACCESS_TICKET at this point" );

            // 11/16/2007 dd - Review and safe
            using( DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetSingletonCurrentHolder") )
            {
                if( reader.Read() == true )
                    return (string)reader["HolderId"];
            }
            return "";

        }

        static public string GetCurrentHolder( SingletonObjectT objT, out HolderType holderType )
        {
            string holder = GetCurrentHolder( CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket );
                
            if( holder.IndexOf( "LPEUpdate" ) >= 0)
                holderType = HolderType.LpeUpdate; 
            else if( holder.IndexOf( "LoAdminMigration" ) >= 0)
                holderType = HolderType.LoAdminMigration; 
            else if( holder == "" )
                holderType = HolderType.None; 
            else
                holderType = HolderType.Other; 

            return holder;
        }




		/// <summary>
		/// 
		/// </summary>
		/// <param name="objId">SingletonObjectT enum value</param>
		/// <param name="holderId">site id, for example</param>
		/// <param name="secondsToExpiration">Must be at least 5 seconds, number of seconds that if the site/holder crashes, singleton will be killed/expired automatically</param>
		/// <returns></returns>
		static public CSingletonDistributed RequestSingleton( SingletonObjectT objT, string holderId, int secondsToExpiration )
		{
			// To avoid this restriction, we would have to create a hash table of singleton objects we 
			// provide access ticket to. The limit is caused by the fact that we can't pass in the objId
			// to the start proc of a thread.
			Tools.Assert( SingletonObjectT.LpeDataAccessTicket == objT, "CSingletonDistributed only supports LPE_DATA_ACCESS_TICKET at this point" );
			if( 5 > secondsToExpiration )
			{
				string errDevMsg = string.Format( "{0}: CSingletonDistributed.RequestSingleton cannot work with expiration time less than 5 seconds.", holderId );
				Tools.LogErrorWithCriticalTracking( errDevMsg );
				throw new CBaseException( LendersOffice.Common.ErrorMessages.GenericNotified, errDevMsg );

			}
			return new CSingletonDistributed( LPE_DATA_ACCESS_TICKET, holderId, secondsToExpiration );
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objId">SingletonObjectT enum value</param>
		/// <param name="holderId">site id, for example</param>
		/// <returns></returns>
		static public CSingletonDistributed RequestSingletonNoExpiration( SingletonObjectT objT, string holderId )
		{
			// To avoid this restriction, we would have to create a hash table of singleton objects we 
			// provide access ticket to. The limit is caused by the fact that we can't pass in the objId
			// to the start proc of a thread.
			Tools.Assert( SingletonObjectT.LpeDataAccessTicket == objT, "CSingletonDistributed only supports LPE_DATA_ACCESS_TICKET at this point" );
			return new CSingletonDistributed( LPE_DATA_ACCESS_TICKET, holderId, -1 );
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="objId">SingletonObjectT enum value</param>
		/// <param name="holderId">site id, for example</param>
		/// <returns></returns>
		static public void RemoveSingletonNoExpiration( SingletonObjectT objT )
		{
			// To avoid this restriction, we would have to create a hash table of singleton objects we 
			// provide access ticket to. The limit is caused by the fact that we can't pass in the objId
			// to the start proc of a thread.
			Tools.Assert( SingletonObjectT.LpeDataAccessTicket == objT, "CSingletonDistributed only supports LPE_DATA_ACCESS_TICKET at this point" );

            SqlParameter[] parameters = {
                                            new SqlParameter( "@ObjId", LPE_DATA_ACCESS_TICKET )
                                        };

			int nResult = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LOShare, "RemoveSingletonDistributedNoExpiration", 0, parameters );

			if( nResult <= 0 )
			{
				string errDevMsg = string.Format( "CSingletonDistributed.RemoveSingletonNoExpiration failed. No row got affected." );
				Tools.LogErrorWithCriticalTracking( errDevMsg );
				throw new CBaseException( LendersOffice.Common.ErrorMessages.GenericNotified, errDevMsg );
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="objId">SingletonObjectT enum value</param>
		/// <param name="holderId">site id, for example</param>
		/// <returns></returns>
		static public void RemoveSingletonOfAnyType( SingletonObjectT objT )
		{
			// To avoid this restriction, we would have to create a hash table of singleton objects we 
			// provide access ticket to. The limit is caused by the fact that we can't pass in the objId
			// to the start proc of a thread.
			Tools.Assert( SingletonObjectT.LpeDataAccessTicket == objT, "CSingletonDistributed only supports LPE_DATA_ACCESS_TICKET at this point" );

            SqlParameter[] parameters = {
                                            new SqlParameter( "@ObjId", LPE_DATA_ACCESS_TICKET )
                                        };

			int nResult = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LOShare, "RemoveSingletonDistributedOfAnyType", 0, parameters );

			if( nResult <= 0 )
			{
				string errDevMsg = "CSingletonDistributed.RemoveSingletonDistributedOfAnyType failed. No row got affected.";
				Tools.LogErrorWithCriticalTracking( errDevMsg );
				throw new CBaseException( LendersOffice.Common.ErrorMessages.GenericNotified, errDevMsg );
			}
		}



		//static 
        private void ReportingProc()
		{
            //int debugCounter = 0;

            int SecondsToExpire = 20; // num of seconds
			while( true )
			{
              

                Tools.LogRegTest( string.Format( "{0} feed the singleton [{1}].",  // "{0} : Attempting to feed the singleton [{1}].", 
                                                 m_holderId, DurationToString() ) );


                const bool bSendEmail = true;
                const int  nRetry     = 3;

                SqlParameter[] parameters = {
                                                new SqlParameter( "@ObjId", m_objId /*LPE_DATA_ACCESS_TICKET*/ ),
					                            new SqlParameter( "@SecondsToExpire", SecondsToExpire ),
                                                new SqlParameter( "@HolderId", m_holderId )
                                            };
                int result = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "FeedSingletonDistributedWithHolderConstrain", bSendEmail, nRetry, parameters);

                if( result <= 0 )
                {
                    // opm 7544

                    if( m_alive == false )
                        return; // finish this reprot thread

                    Tools.LogRegTest( string.Format( "{0} *** FeedSingletonDistributed failed. Try to recover the singleton [{1}].",
                                                    m_holderId, DurationToString() ) );

                    parameters = new SqlParameter[] {
                                                   new SqlParameter( "@ObjId", m_objId ),
                                                    new SqlParameter( "@HolderId", m_holderId ),
                                                    new SqlParameter( "@SecondsToExpire", m_secondsToExpire > 0 ? SecondsToExpire : m_secondsToExpire) 
                                               };
                    // try to get SingletonDistributed again if no one owns it.
                    result = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LOShare, "RequestSingletonDistributedWithHolderConstrain", 0, parameters );

                    if( result > 0 )
                    {
                        Tools.LogRegTest( string.Format( "{0} *** successfully recover the singleton [{1}].",
                                                           m_holderId, DurationToString() ) );
                    }

                }

                if( result <= 0 )
                {
                    // This is a tricky situation. Database is probably locked up for maintanance activities (could be either manual or automatic)
                    // If we throw up here w/o trying to revive the singleton then the lock holding calc server might be trying to load lpe info while 
                    // lpeupdate or an author is updating. That is fine in term of memory management as lock holding calc server now does check for version 
                    // consistency - if doesn't match it would throw away all old ones and start loading again. The worst case here is when there 
                    // is another calc server eventually pick up the lock and it too will be busy loading the info -> 2 almost out-of-service calc servers; 
                    // I can imgine that there might more than 2 can get into this situation, but that would be extremely rare.

                    string errMsg = String.Format( "{0} : CSingletonDistributed is still alive but the db table singleton_tracker indicates that it has expired." +  
                                                   " Database is probably locked for auto-backup or other maintenance activities."
                                                   , m_holderId);
                    Tools.LogErrorWithCriticalTracking( errMsg );
					throw new CBaseException( LendersOffice.Common.ErrorMessages.PricingSystemBeingUpdated, 
						errMsg );
				}
				
				//exc.CommitTransaction();
				
                //Tools.LogRegTest( string.Format( "{0}: Succesfully fed the singleton", m_holderId) );
				LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 2000 );
			}
		}

		private bool     m_alive = false;
		private string   m_objId;
		private string   m_holderId;
		private Thread   m_reportingThread;
		private int      m_secondsToExpire;
        private DateTime m_startTime = DateTime.Now;

	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="objId"></param>
		/// <param name="holderId"></param>
		/// <param name="secondsToExpire">Passing a negative number if you want a singleton without expiration</param>
		private CSingletonDistributed( string objId, string holderId, int secondsToExpire )
		{

			if( 0 <= secondsToExpire && secondsToExpire < 5 )
			{
				string errDevMsg = string.Format( "{0}: CSingletonDistributed cannot work with expiration time less than 5 seconds.", holderId );
				Tools.LogErrorWithCriticalTracking( errDevMsg );
				throw new CBaseException( LendersOffice.Common.ErrorMessages.GenericNotified, errDevMsg );
			}

			m_objId           = objId;
			m_holderId        = holderId;
			m_secondsToExpire = secondsToExpire;

			// Check to see if it's available and grab it if it is, all within a transaction

			int result = 0;
			try
			{
                SqlParameter[] parameters = {
                                                new SqlParameter( "@ObjId", objId ),
					new SqlParameter( "@HolderId", holderId ),
					new SqlParameter( "@SecondsToExpire", secondsToExpire ) 
                                            };
				result = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LOShare, "RequestSingletonDistributed", 0, parameters);
			}
			catch( Exception ex )
			{
				string devErr = string.Format( "{0}: Exception encountered during executing RequestSingletonDistributed.", holderId );
				Tools.LogErrorWithCriticalTracking( devErr, ex );
				throw new CBaseException( LendersOffice.Common.ErrorMessages.PricingSystemBeingUpdated, devErr );
			}
			if( 0 < result )
			{
				Tools.LogRegTest( string.Format( "{0}: just got the singleton.", holderId ) );
			}
			else
				throw new CLpeLoadingDeniedException( "Can not get singleton.", string.Format( "{0}: RequestSingletonDistributed returns false, meaning the singleton has been taken.", holderId ) );
			
		
			m_alive = true;
            m_startTime = DateTime.Now;

			// We don't want to feed singleton with no expiration
			if( secondsToExpire > 0 )
				StartReportAgent();
		}
		
		private void StartReportAgent()
		{
			m_reportingThread = new Thread( new ThreadStart(ReportingProc));

			// Start ThreadProc.  On a uniprocessor, the thread does not get 
			// any processor time until the main thread yields.  Uncomment 
			// the LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( that follows t.Start() to see the difference.
			m_reportingThread.Start();
		}

		public void Kill()
		{
			if( m_alive && m_secondsToExpire > 0 )
			{
                Thread thread = m_reportingThread;
				if( null != thread )
				{
					thread.Abort();
					m_reportingThread = null;
				}

				int ret = DataAccess.StoredProcedureHelper.ExecuteNonQuery( DataSrc.LOShare,  "KillSingletonDistributed", 0, 
					new System.Data.SqlClient.SqlParameter[] { new System.Data.SqlClient.SqlParameter( "@ObjId", m_objId ) } );

				m_alive = false;

                Tools.LogRegTest( string.Format( "{0} : KillSingletonDistributed storeproc returns {1} [{2}].", 
                    m_holderId, ret.ToString(),  DurationToString() ) );
				
			}		
		}

		// Implement IDisposable.
		// Do not make this method virtual.
		// A derived class should not be able to override this method.
		public void Dispose()
		{
			Kill();
			
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~CSingletonDistributed()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Kill();
		}

        private string DurationToString()
        {
            TimeSpan  duration = DateTime.Now - m_startTime;
            duration = new TimeSpan( (duration.Ticks / TimeSpan.TicksPerSecond) * TimeSpan.TicksPerSecond);

            string durationStr = duration.TotalSeconds.ToString( "N0" );

            StringBuilder sb = new StringBuilder();
            if( duration.TotalSeconds < 60 )
                sb.AppendFormat("{0} seconds", durationStr);
            else
                sb.AppendFormat("{0} seconds = {1}", durationStr, duration.ToString());
            return sb.ToString();
        }

        public bool IsAlive // opm 7649
        {
            get
            {
                Thread thread = m_reportingThread;
                if( m_alive == false || thread == null )
                    return false;


                if( thread.IsAlive )
                    return true;

                if( (thread.ThreadState & ThreadState.Unstarted) != 0  )
                {
                    TimeSpan  duration = DateTime.Now - m_startTime;
                    if( duration.TotalSeconds > 60 )
                    {
                        thread.Abort(); // for safety
                        return false;
                    }
                    return true;
                }

                return false; // thread.ThreadState =  Abort or Stopped
            }
        }

	}
}