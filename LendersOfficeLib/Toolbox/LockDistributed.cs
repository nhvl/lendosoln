using System;
using DataAccess;
#if obsolete
namespace Toolbox.Distributed
{
	class CExample
	{
		static void Example()
		{
			// Server 1 
			// Request for a lock
			using( CLockDistributed lkd = CLockDistributed.RequestLock( CLockDistributed.LOCKID_LPE_AUTHOR_UPDATE ) )
			{
				// do some work here
			}
		}
	}

	class CLockDistributed : IDisposable
	{
		static public string LOCKID_LPE_AUTHOR_UPDATE = "LPE_AUTHOR_UPDATE";

		static CLockDistributed RequestLock( string objId, CDateTime expiredTime )
		{
			return new CLockDistributed();
		}

		private bool m_lockReleased = false;
		
		private CLockDistributed()
		{}
		
		public void ReleaseLock()
		{
			// Make call to db

			m_lockReleased = true;
		}

		// Implement IDisposable.
		// Do not make this method virtual.
		// A derived class should not be able to override this method.
		public void Dispose()
		{
			DisposePrivate();
			
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void DisposePrivate()
		{
			if(!m_lockReleased)
			{
				ReleaseLock();
			}	       
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~CLockDistributed()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			DisposePrivate();
		}

	}
}
#endif // 0