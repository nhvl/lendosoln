using System;
using System.Collections;
using System.Xml;

using DataAccess;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace Toolbox
{

    public class CertConditionObsolete : IComparable<CertConditionObsolete> 
    {
        public string Category { get; private set; }
        public string DateDone { get; private set; }
        public string Text { get; private set; }
        public string DebugString { get; private set; }

        public CertConditionObsolete(LendersOffice.Reminders.CLoanConditionObsolete condtion, CCategoryStip stip)
        {
            ReadFrom(condtion, stip);
        }

        public CertConditionObsolete(LendersOffice.Reminders.CLoanConditionObsolete condition)
        {
            ReadFrom(condition, null);
        }

        public CertConditionObsolete(CCategoryStip stip)
        {
            ReadFrom(null, stip);
        }

        public CertConditionObsolete(LendersOffice.ObjLib.Task.Task task)
        {
            ReadFromTask(task, null);
        }

        public CertConditionObsolete(LendersOffice.ObjLib.Task.Task task, CCategoryStip stip)
        {
            ReadFromTask(task, stip);
        }


        private string CategoryForSorting
        {
            get
            {
                switch (Category.ToLower())
                {
                    case "warning":
                        return "0";
                    case "misc":
                        return "9";
                    default:
                        return "1" + Category;
                }
            }
        }

        private void ReadFromTask(LendersOffice.ObjLib.Task.Task task, CCategoryStip stip)
        {
            if (null != task)
            {
                if (task.TaskStatus == LendersOffice.ObjLib.Task.E_TaskStatus.Closed && task.TaskClosedDate.HasValue)
                {
                    DateDone = task.TaskClosedDate.Value.ToShortDateString();
                }
                Category = task.CondCategoryId_rep;
                Text = task.TaskSubject;
            }
            if (null != stip)
            {
                ReadFrom(null, stip);
            }
        }

        private void ReadFrom(LendersOffice.Reminders.CLoanConditionObsolete condition, CCategoryStip stip)
        {
            if (null != condition)
            {
                if (condition.CondStatus == LendersOffice.Reminders.E_CondStatus.Done)
                {
                    DateDone = condition.CondStatusDate.ToShortDateString();
                }
                Category = condition.CondCategoryDesc;
                Text = condition.CondDesc;
            }
            if (null != stip)
            {
                Category = stip.Category;
                Text = stip.Stip.DescriptionForCert;
                DebugString = stip.Stip.DebugString;
            }
        }


        #region IComparable<CertCondition> Members

        //sorts by warning then regular then misc
        //then text is used to do secondary sort
        public int CompareTo(CertConditionObsolete other)
        {
            if (null == other)
            {
                return 1;
            }
            if (Category.Equals(other.Category, StringComparison.InvariantCultureIgnoreCase))
            {
                return Text.CompareTo(other.Text);
            }
            else
            {
                return CategoryForSorting.CompareTo(other.CategoryForSorting);
            }
        }

        #endregion
    }

    public class CCategoryStip
    {
        public string Category { get; set; }
        public CStipulation Stip { get; set; }
    }
    public class CCategoryVisibilityStip : CCategoryStip
    {
        public bool IsHidden { get; set; }
    }

    public class CStipulation : IComparable
    {
        private string m_description;
        private string m_debugString;


        public E_RoleT? AssignedToRole { get; set; }
        public E_RoleT? OwnerRole { get; set; }
        public Guid? ApplicationId { get; private set; }
        public string RequiredDocTypeName { get; set; }

        public CStipulation(string desc, string debugString)
            : this(desc, debugString, null)
        {
        }

        public CStipulation(string desc, string debugString, Guid? applicationId) 
        {
            m_debugString = debugString;
            this.ApplicationId = applicationId;

            int upIndex = desc.IndexOf('^',0);
            if (upIndex >= 0)
            {
                m_description = desc.Substring(0, upIndex);
                string taskDetails = desc.Substring(upIndex).Replace("^", "");
                string[] keyvaluePair = taskDetails.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string keyValue in keyvaluePair)
                {
                    string[] sides = keyValue.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    if (sides.Length == 2)
                    {
                        if (sides[0].Equals("AssignedTo", StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                AssignedToRole = (E_RoleT)Enum.Parse(typeof(E_RoleT), sides[1]);
                            }
                            catch (ArgumentException) { } 
                        }
                        else if (sides[0].Equals("Owner", StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                OwnerRole = (E_RoleT)Enum.Parse(typeof(E_RoleT), sides[1]);
                                
                            }
                            catch (ArgumentException) { }
                        }
                        else if (sides[0].Equals("RequiredDocType", StringComparison.OrdinalIgnoreCase))
                        {
                            RequiredDocTypeName = sides[1];
                        }
                        else if (sides[0].Equals("DocRequestType", StringComparison.OrdinalIgnoreCase)
                            && !sides[1].Equals("SendInADoc", StringComparison.OrdinalIgnoreCase))
                        {
                            this.m_debugString += " Invalid value for DocRequestType key.";
                        }
                    }
                }

                if (this.AssignedToRole.HasValue && this.AssignedToRole.Value == E_RoleT.Consumer
                    && string.IsNullOrEmpty(this.RequiredDocTypeName))
                {
                    this.m_debugString += " No RequiredDocTypeName value found for doc request.";
                }
            }
            else
            {
                m_description = desc;
            }
        }

        public string Description 
        {
            get { return m_description; }
        }

		// 06/18/08 mf. OPM 22530. In the cert, we display urls as links.
		public string DescriptionForCert
		{
			get
			{
				string ret = m_description;

				// Since we expect that SAEs would not want to 'trick' this system,
				// we make this a quick and fairly dumb search for urls, avoiding the
				// cost of regular expression matching.
				// Any word that starts with 'http' will be considered a url and need a link.
				string[] descriptionTokens = ret.Split(' ');
				foreach (string token in descriptionTokens )
				{
					if (token.ToLower().StartsWith("http") ) // includes 'https'
					{						
						ret = ret.Replace(token, "<a href='" + LendersOffice.Common.Utilities.SafeJsString( token ) + "' target='_blank' >" + token + "</a>");
					}
				}

				return ret;
			}

		}
        public void UpdateDescriptionForTotal(Dictionary<string, string> replacements)
        {
            // Per OPM 105626, in special TOTAL case we have to modify the description
            foreach (string alias in replacements.Keys)
            {
                m_description = Regex.Replace(m_description, Regex.Escape(alias), replacements[alias].Replace("$","$$"), RegexOptions.IgnoreCase);
            }
        }

		// 06/18/08 mf. OPM 22530. Loan condition editor does not allow any html for security reasons,
		// but SAEs need to add line breaks to stipulations, so we allow only <br> tag and strip them 
		// out when converting stipulations to conditions.
		public string DescriptionForCondition
		{
			get { return m_description.Replace("<br>", Environment.NewLine).Replace("<BR>", Environment.NewLine); }
		}

        public string DebugString 
        {
            get { return m_debugString; }
        }

        public int CompareTo(object o) 
        {
            CStipulation _o = o as CStipulation;
            
            if (_o != null) 
            {
                return m_description.CompareTo(_o.Description);
            }
            throw new LendersOffice.Common.GenericUserErrorMessageException("Unable to compare CStipulation:: " + o);
        }
    }
    public class CStipulationCategoryComparer : IComparer
    {
        public int Compare(object x, object y) 
        {
            string _x = x as string;
            string _y = y as string;

            if (_x != null && _y != null) 
            {
                if (_x == "MISC")
                    _x = "9MISC";
                else if (_x == "WARNING")
                    _x = "0WARNING";
                else 
                    _x = "1" + _x;

                if (_y == "MISC")
                    _y = "9MISC";
                else if (_y == "WARNING")
                    _y = "0WARNING";
                else
                    _y = "1" + _y;

                return _x.CompareTo(_y);

            } 
            else 
            {
                return -1; // What to do here?
            }
        }

    }
	/// <summary>
	/// Similar to SortedList but allow duplicate key and not duplicate singleItem. Default SortedList doesn't allow duplicate key
	/// </summary>
	public class CSortedListOfGroups
	{

        private string m_comparerClassName = "";
		private SortedList m_sortedList; 
        public CSortedListOfGroups(IComparer comparer, int capacity) 
        {
            if (null != comparer) 
                m_comparerClassName = comparer.GetType().FullName;

            m_sortedList = new SortedList(comparer, capacity);
        }
		public CSortedListOfGroups( int capacity )
		{
			m_sortedList = new SortedList( capacity );
		}

        public string ComparerClassName 
        {
            get { return m_comparerClassName; }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="key">key to be sorted by</param>
		/// <param name="singleItem">unique single item</param>
		public void Add( object key, object singleItem )
		{
			if( !m_sortedList.ContainsKey( key ) )
				m_sortedList[ key ] = new Hashtable( 3 ); 

			( (Hashtable) m_sortedList[key] )[ singleItem ] = null;
		}

		public bool HasKey( object key )
		{
			return m_sortedList.ContainsKey( key );
		}

		public ICollection Keys
		{
			get
			{
				return m_sortedList.Keys;
			}
		}

		public bool HasSingleItem( object singleItem )
		{
			foreach( Hashtable group in m_sortedList.Values )
			{
				if( group.Contains( singleItem ) )
					return true;
			}
			return false;
		}
		

		public Hashtable GetGroupByKey( object key )
		{
			if( m_sortedList.ContainsKey( key ) )
				return (Hashtable) m_sortedList[key];
			return new Hashtable();
		}

        public ArrayList GetGroupByKeyAndSort( object key ) 
        {
            if (m_sortedList.ContainsKey(key)) 
            {
                ArrayList list = new ArrayList();
                Hashtable hash = (Hashtable) m_sortedList[key];

                foreach (object o in hash.Keys)
                    list.Add(o);
                list.Sort();

                return list;
            }
            return new ArrayList();
        }
		public ArrayList MergedArrayBySortedOrder
		{
			get
			{
				ArrayList r = new ArrayList();
				foreach( Hashtable group in m_sortedList.Values )
				{
					r.AddRange( group.Keys );
				}
				return r;
			}
		}

        public int EstimateSize()
        {
            int size = 12;
            if( m_sortedList != null )
            {
                size += 52*m_sortedList.Count;
				foreach( Hashtable group in m_sortedList.Values )
                    if( group != null )
                        size += (16 /* guid size */  /* give up box size */ ) * group.Count;
            }
            return size;
        }


        public void Append( CSortedListOfGroups other)
        {
            foreach( object key in  other.m_sortedList.Keys )
            {
                Hashtable table = (Hashtable)other.m_sortedList[key];
                foreach( object item in table.Keys )
                    Add( key, item);
            }
        }

        public void Trim()
        {
            if( m_sortedList != null )
                m_sortedList.TrimToSize();
        }

	}
}
