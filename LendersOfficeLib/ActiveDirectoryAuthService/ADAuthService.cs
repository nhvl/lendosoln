﻿// <copyright file="ADAuthService.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   10/3/2014 1:48:31 AM 
// </summary>
namespace LendersOffice.ActiveDirectoryAuthService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;

    /// <summary>
    /// On CMG Server the ActiveDirectory web service is randomly failing after each code update.
    /// After searching on the web the potential issue is may cause by keepAlive is true.
    /// </summary>
    public partial class ADAuthService
    {
        /// <summary>
        /// Override the GetWebRequest so KeepAlive is false.
        /// </summary>
        /// <param name="uri">Url of the service.</param>
        /// <returns>A web request object.</returns>
        protected override WebRequest GetWebRequest(Uri uri)
        {
            var webRequest = (HttpWebRequest)base.GetWebRequest(uri);
            webRequest.KeepAlive = false;
            return webRequest;
        }
    }
}