<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:tns="http://www.complianceease.com/2015/03/ComplianceAuditService" elementFormDefault="qualified" targetNamespace="http://www.complianceease.com/2015/03/ComplianceAuditService" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation xml:lang="en">
			$Id: ComplianceAuditService.xsd 16856 2015-02-07 01:32:46Z gshao $

			Copyright (c) 2008-2016 LogicEase Solutions Inc.
			One Bay Plaza Suite 520
			1350 Old Bayshore Highway
			Burlingame, California 94010, U.S.A.
			All rights reserved.
		</xs:documentation>
  </xs:annotation>
  <xs:simpleType name="CEID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The identifier assigned by the ComplianceEase ACS to be referenced as the unique loan ID.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9A-Za-z]{10}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="DisclosureID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The identifier assigned by the ComplianceEase ACS to be referenced as the unique disclosure ID.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9A-Za-z]{10}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="VersionIdentifier">
    <xs:annotation>
      <xs:documentation xml:lang="en">The identifier used to distinguish a particular version of an audit report. The versionIdentifier consists of one or more digits followed by an optional 'L' character (indicating a 'Lite' report).</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="\d+(L)?" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="DisclosureType">
    <xs:annotation>
      <xs:documentation xml:lang="en">Recognized disclosure types.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="LoanEstimate" />
      <xs:enumeration value="ClosingDisclosure" />
      <xs:enumeration value="PostConsummationRevisedClosingDisclosure" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="IssuanceType">
    <xs:annotation>
      <xs:documentation xml:lang="en">Recognized issuance types.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Initial" />
      <xs:enumeration value="Revised" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="LoanIdentifier" abstract="true">
    <xs:sequence />
  </xs:complexType>
  <xs:complexType name="CELoanIdentifier">
    <xs:complexContent mixed="false">
      <xs:extension base="tns:LoanIdentifier">
        <xs:sequence>
          <xs:element name="CEID" type="tns:CEID" />
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="LenderLoanIdentifier">
    <xs:complexContent mixed="false">
      <xs:extension base="tns:LoanIdentifier">
        <xs:sequence>
          <xs:element name="LenderLoanNumber" type="xs:string">
            <xs:annotation>
              <xs:documentation xml:lang="en">The identifier assigned by the lender to be referenced as the unique loan ID.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="LoanType">
            <xs:simpleType>
              <xs:restriction base="xs:NMTOKEN">
                <xs:enumeration value="HELOC" />
                <xs:enumeration value="MortgageLoan" />
              </xs:restriction>
            </xs:simpleType>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="DisclosureIdentifier" abstract="true">
    <xs:sequence />
  </xs:complexType>
  <xs:complexType name="CEDisclosureIdentifier">
    <xs:complexContent mixed="false">
      <xs:extension base="tns:DisclosureIdentifier">
        <xs:sequence>
          <xs:element name="DisclosureID" type="tns:DisclosureID" />
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="LenderDisclosureIdentifier">
    <xs:complexContent mixed="false">
      <xs:extension base="tns:DisclosureIdentifier">
        <xs:sequence>
          <xs:element name="CEID" type="tns:CEID" />
          <xs:element name="LenderLoanNumber" type="xs:string">
            <xs:annotation>
              <xs:documentation xml:lang="en">The identifier assigned by the lender to be referenced as the unique loan ID.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="LenderName" type="xs:string">
            <xs:annotation>
              <xs:documentation xml:lang="en">The lender name.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="DisclosureType" type="tns:DisclosureType">
            <xs:annotation>
              <xs:documentation xml:lang="en">The disclosure type.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="IssuanceType" type="xs:string">
            <xs:annotation>
              <xs:documentation xml:lang="en">The issuance type.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="DisclosureDate" type="xs:date">
            <xs:annotation>
              <xs:documentation xml:lang="en">The date associated with the disclosure.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="SequenceNumber" type="xs:integer">
            <xs:annotation>
              <xs:documentation xml:lang="en">The sequence number associated with the disclosure.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AuditLoanDataType">
    <xs:sequence>
      <xs:element name="RequestXML" type="xs:string" />
      <xs:element name="CEID" type="tns:CEID" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AuditLoanDataResponseType">
    <xs:sequence>
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AuditReportType">
    <xs:sequence>
      <xs:element name="EmbeddedFile">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="Document" type="xs:base64Binary" />
          </xs:sequence>
          <xs:attribute name="fileName" type="xs:string" />
          <xs:attribute name="fileExtension" type="xs:string" />
          <xs:attribute name="MIMEType" type="xs:string" />
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="format">
      <xs:simpleType>
        <xs:restriction base="xs:NMTOKEN">
          <xs:enumeration value="HTML" />
          <xs:enumeration value="PDF" />
          <xs:enumeration value="XML" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="type">
      <xs:simpleType>
        <xs:restriction base="xs:NMTOKEN">
          <xs:enumeration value="Full" />
          <xs:enumeration value="Lite" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="AuditSummaryType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="RiskIndicator" type="xs:string" />
      <xs:element name="APR" type="xs:double" />
      <xs:element name="Version" type="tns:VersionIdentifier" />
      <xs:element name="TimeStamp" type="xs:dateTime" />
      <xs:element name="Indicators">
        <xs:complexType>
          <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="unbounded" name="Indicator">
              <xs:complexType>
                <xs:simpleContent>
                  <xs:extension base="xs:string">
                    <xs:attribute name="name" type="xs:string" use="required" />
                  </xs:extension>
                </xs:simpleContent>
              </xs:complexType>
            </xs:element>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GetAuditReportType">
    <xs:sequence>
      <xs:element name="LoanIdentifier" type="tns:LoanIdentifier" />
      <xs:element minOccurs="0" name="Version" type="tns:VersionIdentifier" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GetAuditReportResponseType">
    <xs:sequence>
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SaveLoanDataType">
    <xs:sequence>
      <xs:element name="RequestXML" type="xs:string" />
      <xs:element name="CEID" type="tns:CEID" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SaveLoanDataResponseType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AddDisclosureType">
    <xs:sequence>
      <xs:element name="RequestXML" type="xs:string" />
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="Audit" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AddDisclosureResponseType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UpdateDisclosureType">
    <xs:sequence>
      <xs:element name="RequestXML" type="xs:string" />
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="Audit" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UpdateDisclosureResponseType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SaveDisclosureType">
    <xs:sequence>
      <xs:element name="RequestXML" type="xs:string" />
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="Audit" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SaveDisclosureResponseType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RemoveDisclosureType">
    <xs:sequence>
      <xs:element name="DisclosureIdentifier" type="tns:DisclosureIdentifier" />
      <xs:element name="Audit" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RemoveDisclosureResponseType">
    <xs:sequence>
      <xs:element name="CEID" type="tns:CEID" />
      <xs:element name="DisclosureID" type="tns:DisclosureID" />
      <xs:element name="AuditSummary" type="tns:AuditSummaryType" />
      <xs:element maxOccurs="unbounded" name="AuditReport" type="tns:AuditReportType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="DisclosureDataType">
    <xs:sequence>
      <xs:element name="EmbeddedFile">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="Document" type="xs:base64Binary" />
          </xs:sequence>
          <xs:attribute name="fileName" type="xs:string" />
          <xs:attribute name="fileExtension" type="xs:string" />
          <xs:attribute name="MIMEType" type="xs:string" />
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="format">
      <xs:simpleType>
        <xs:restriction base="xs:NMTOKEN">
          <xs:enumeration value="HTML" />
          <xs:enumeration value="PDF" />
          <xs:enumeration value="XML" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="GetDisclosureType">
    <xs:sequence>
      <xs:element name="DisclosureIdentifier" type="tns:DisclosureIdentifier" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GetDisclosureResponseType">
    <xs:sequence>
      <xs:element name="Disclosure" type="tns:DisclosureDataType" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name="AuditLoanData" type="tns:AuditLoanDataType" />
  <xs:element name="AuditLoanDataResponse" type="tns:AuditLoanDataResponseType" />
  <xs:element name="GetAuditReport" type="tns:GetAuditReportType" />
  <xs:element name="GetAuditReportResponse" type="tns:GetAuditReportResponseType" />
  <xs:element name="SaveLoanData" type="tns:SaveLoanDataType" />
  <xs:element name="SaveLoanDataResponse" type="tns:SaveLoanDataResponseType" />
  <xs:element name="AddDisclosure" type="tns:AddDisclosureType" />
  <xs:element name="AddDisclosureResponse" type="tns:AddDisclosureResponseType" />
  <xs:element name="UpdateDisclosure" type="tns:UpdateDisclosureType" />
  <xs:element name="UpdateDisclosureResponse" type="tns:UpdateDisclosureResponseType" />
  <xs:element name="SaveDisclosure" type="tns:SaveDisclosureType" />
  <xs:element name="SaveDisclosureResponse" type="tns:SaveDisclosureResponseType" />
  <xs:element name="RemoveDisclosure" type="tns:RemoveDisclosureType" />
  <xs:element name="RemoveDisclosureResponse" type="tns:RemoveDisclosureResponseType" />
  <xs:element name="GetDisclosure" type="tns:GetDisclosureType" />
  <xs:element name="GetDisclosureResponse" type="tns:GetDisclosureResponseType" />
</xs:schema>