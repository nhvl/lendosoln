﻿<#@ assembly name="System.Xml.Linq" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Xml.Linq" #>
<#@ import namespace="Microsoft.VisualStudio.TextTemplating" #>
<#@ include file="..\..\LqbGrammar\TextTemplates\PathUtility.ttinclude" once="true" #><##>
<#@ include file="..\..\LqbGrammar\TextTemplates\StringUtility.ttinclude" once="true" #><##>
<#+
public class AssociationDataReader
{
	private XElement associationElem;

	private AssociationDataReader(XElement elem)
	{
		this.associationElem = elem;
	}

	public string Name
	{
		get
		{
			return this.associationElem.Element("Name").Value;
		}
	}

    public string AssociationType => this.associationElem.Element("AssociationType")?.Value;

    public bool IsDerived => this.AssociationType == "Derived";

	public bool IsOwnership => this.associationElem.Element("IsOwnership")?.Value == "true";

	public bool SuppressMethodCodeGeneration => this.associationElem.Element("SuppressMethodCodeGeneration")?.Value == "true";

	public IEnumerable<string> UsingNamespaces
	{
		get
		{
			return this.associationElem.Descendants("UsingNamespace").Select(ns => ns.Value);
		}
	}

	public string CollectionName
	{
		get
		{
			return StringUtility.Pluralize(this.Name.Replace("Association", string.Empty));
		}
	}

	public string DataFieldName
	{
		get
		{
			return StringUtility.Uncapitalize(this.CollectionName);
		}
	}

	public EntityReference EntityOne
	{
		get
		{
			return new EntityReference(this.associationElem.Element("EntityOne"));
		}
	}

	public EntityReference EntityTwo
	{
		get
		{
			return new EntityReference(this.associationElem.Element("EntityTwo"));
		}
	}

	public IEnumerable<FieldInfo> Fields
	{
		get
		{
			return this.associationElem.Descendants("Field").Select(f => new FieldInfo(f));
		}
	}

    public bool ContainsEntity(string entityName)
    {
        return this.EntityOne.Name == entityName || this.EntityTwo.Name == entityName;
    }

	public static IEnumerable<string> ReadAssociationNames(ITextTemplatingEngineHost host)
	{
		var doc = LoadAssociationXml(host);
		return doc.Descendants("Association").Select(e => e.Element("Name").Value);
	}

	public static IEnumerable<AssociationDataReader> ReadAllAssociations(ITextTemplatingEngineHost host)
	{
		var doc = LoadAssociationXml(host);
		return doc.Descendants("Association").Select(a => new AssociationDataReader(a));
	}

	public static AssociationDataReader ReadAssociation(string associationName, string solnRelativePath, ITextTemplatingEngineHost host)
	{
		string solnPath = PathUtility.GetLendOSolnDirectory(host);
		string projPath = Path.Combine(solnPath, solnRelativePath);

		var doc = XDocument.Load(projPath);
		return ReadNamedAssociation(associationName, doc);
	}

	public static AssociationDataReader ReadAssociation(string associationName, ITextTemplatingEngineHost host)
	{
		var doc = LoadAssociationXml(host);
		return ReadNamedAssociation(associationName, doc);
	}

	private static AssociationDataReader ReadNamedAssociation(string associationName, XDocument doc)
	{
		var assocElement = doc.Descendants("Association")
			.Single(e => e.Element("Name").Value == associationName);
		return new AssociationDataReader(assocElement);
	}

	private static XDocument LoadAssociationXml(ITextTemplatingEngineHost host)
	{
		string solnPath = PathUtility.GetLendOSolnDirectory(host);
		string entityPath = Path.Combine(solnPath, @"LendersOfficeLib\LendingQB\Core\Data\AssociationData.xml");

		return XDocument.Load(entityPath);
	}

	public class EntityReference
	{
		XElement elem;

		public EntityReference(XElement elem)
		{
			this.elem = elem;
		}

		public string Name
		{
			get
			{
				return this.elem.Element("Name").Value;
			}
		}

		public string Type
		{
			get
			{
				return this.elem.Element("Type").Value;
			}
		}

		public string Cardinality
		{
			get
			{
				return this.elem.Element("Cardinality").Value;
			}
		}

		public string CollectionName
		{
			get
			{
				return StringUtility.Pluralize(this.Name);
			}
		}

		public string DataFieldName
		{
			get
			{
				return StringUtility.Uncapitalize(this.CollectionName);
			}
		}
	}

	public class FieldInfo
	{
		XElement elem;

		public FieldInfo(XElement elem)
		{
			this.elem = elem;
		}

		public string Id
		{
			get
			{
				return this.elem.Element("Name").Value;
			}
		}

		public string Name => Id;

		public string Type
		{
			get
			{
				return this.elem.Element("Type").Value;
			}
		}

		public bool IsAssociationLevel
		{
			get
			{
				return this.elem.Element("Level").Value == this.Id;
			}
		}

		public bool CanGet
		{
			get
			{
				return this.elem.Element("Get").Value == "true";
			}
		}

		public bool CanSet
		{
			get
			{
				return this.elem.Element("Set").Value == "true";
			}
		}

		public string AccessType
		{
			get
			{
				return this.elem.Element("AccessType")?.Value;
			}
		}

		public bool IsDbOnly
		{
			get
			{
				return this.AccessType == "db_only";
			}
		}

		public bool IsConstructorArgument
		{
			get
			{
				return this.AccessType == "constructor";
			}
		}

		public bool IsNullable
		{
			get
			{
				return this.elem.Element("IsNullable") == null || this.elem.Element("IsNullable").Value == "true";
			}
		}

		public string SingularOn
		{
			get
			{
				return this.elem.Element("SingularOn")?.Value;
			}
		}

		public bool IsSingularRequired
		{
			get
			{
				return this.elem.Element("IsSingularRequired")?.Value == "true";
			}
		}
	}
}
#>