﻿<#@ assembly name="System.Xml.Linq" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Xml.Linq" #>
<#@ import namespace="Microsoft.VisualStudio.TextTemplating" #>
<#@ include file="..\..\LqbGrammar\TextTemplates\PathUtility.ttinclude" once="true" #><##>
<#+
public class AssociationMapperDataReader
{
	private XElement mapperElem;

	private AssociationMapperDataReader(XElement elem)
	{
		this.mapperElem = elem;
	}

	public string Name
	{
		get
		{
			return this.mapperElem.Element("Name").Value;
		}
	}

	public string AssociationName
	{
		get
		{
			return this.mapperElem.Element("AssociationName").Value;
		}
	}

	public string KeyType
	{
		get
		{
			return this.mapperElem.Element("KeyType").Value;
		}
	}

	public string LoadAll
	{
		get
		{
			return this.mapperElem.Element("LoadAll").Value;
		}
	}

	public string Create
	{
		get
		{
			return this.mapperElem.Element("Create").Value;
		}
	}

	public string Update
	{
		get
		{
			return this.mapperElem.Element("Update").Value;
		}
	}

	public string Delete
	{
		get
		{
			return this.mapperElem.Element("Delete").Value;
		}
	}

	public IEnumerable<FilterParameter> FilterParameters
	{
		get
		{
			return this.mapperElem.Descendants("FilterParameter").Select(e => new FilterParameter(e));
		}
	}

	public static IEnumerable<string> ReadMapperNames(ITextTemplatingEngineHost host)
	{
		var doc = LoadMapperXml(host);
		return doc.Descendants("Mapper").Select(e => e.Element("Name").Value);
	}

	public static IEnumerable<AssociationMapperDataReader> ReadAllMappers(ITextTemplatingEngineHost host)
	{
		var doc = LoadMapperXml(host);
		return doc.Descendants("Mapper").Select(m => new AssociationMapperDataReader(m));
	}

	public static AssociationMapperDataReader ReadMapper(string mapperName, ITextTemplatingEngineHost host)
	{
		var doc = LoadMapperXml(host);
		var mapperElement = doc.Descendants("Mapper")
			.Single(e => e.Element("Name").Value == mapperName);
		return new AssociationMapperDataReader(mapperElement);
	}

	private static XDocument LoadMapperXml(ITextTemplatingEngineHost host)
	{
		string solnPath = PathUtility.GetLendOSolnDirectory(host);
		string mapperPath = Path.Combine(solnPath, @"LendersOfficeLib\LendingQB\Core\Mapping\AssociationMapperData.xml");

		return XDocument.Load(mapperPath);
	}

	public class FilterParameter
	{
		private XElement elem;

		public FilterParameter(XElement elem)
		{
			this.elem = elem;
		}

		public string TValueKind
		{
			get
			{
				return this.elem.Element("TValueKind").Value;
			}
		}

		public string TIdValue
		{
			get
			{
				return this.elem.Element("TIdValue").Value;
			}
		}

		public string Name
		{
			get
			{
				return this.elem.Element("Name").Value;
			}
		}

		public string Type
		{
			get
			{
				return string.Format("DataObjectIdentifier<DataObjectKind.{0}, {1}>", this.TValueKind, this.TIdValue);
			}
		}

		public string MemberName
		{
			get
			{
				return this.Name.First().ToString().ToLower() + this.Name.Substring(1);
			}
		}
	}
}
#>