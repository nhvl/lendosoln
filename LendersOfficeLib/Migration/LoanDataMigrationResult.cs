﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds information of the results from running a migration.
    /// </summary>
    [DataContract]
    public class LoanDataMigrationResult : ICloneable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationResult"/> class.
        /// </summary>
        /// <param name="loanId">The id of the loan this migration was performed on.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="userName">The name of the user who ran this migration.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="name">The name of the migration ran.</param>
        /// <param name="timePerformed">The time that this migration was performed.</param>
        /// <param name="result">The results of the migration.</param>
        /// <param name="version">The version of the migration.</param>
        /// <param name="hasPermissionErrors">A bit indicating whether the migration result has a permission error.</param>
        public LoanDataMigrationResult(Guid loanId, Guid brokerId, string userName, Guid userId, string name, DateTime timePerformed, MigrationResultT result, LoanVersionT version, bool hasPermissionErrors)
        {
            this.LoanId = loanId;
            this.BrokerId = brokerId;
            this.UserName = userName;
            this.UserId = userId;
            this.TimePerformed = timePerformed;
            this.Name = name;
            this.ResultStatus = result;
            this.AssociatedVersion = version;
            this.LoanFileFieldValues = new List<LoanDataMigrationFieldValue>();
            this.ApplicationFieldValues = new Dictionary<Guid, List<LoanDataMigrationFieldValue>>();
            this.AppIdToAppName = new Dictionary<Guid, string>();
            this.ConditionErrorMessages = new List<string>();
            this.CollectionValues = new Dictionary<string, LoanDataMigrationCollectionValue>();
            this.HasPermissionErrors = hasPermissionErrors;
        }

        /// <summary>
        /// Gets the time the migration was performed.
        /// </summary>
        /// <value>The time the migration was performed.</value>
        [DataMember]
        public DateTime TimePerformed
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        [DataMember]
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the loan id that this migration was performed on.
        /// </summary>
        /// <value>The loan id that this migration was performed on.</value>
        [DataMember]
        public Guid LoanId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the user who ran the migration.
        /// </summary>
        /// <value>The name of the user who ran the migration.</value>
        [DataMember]
        public string UserName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the user who ran the migration.
        /// </summary>
        /// <value>The id of the user who ran the migration.</value>
        [DataMember]
        public Guid UserId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the version number of the migration that this result is for.
        /// </summary>
        /// <value>The version number of the mgiration that this result is for.</value>
        [DataMember]
        public LoanVersionT AssociatedVersion
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the migration that was ran.
        /// </summary>
        /// <value>The name of the migration that was ran.</value>
        [DataMember]
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the list of fields and their values.
        /// </summary>
        /// <value>The list of fields and their values.</value>
        [DataMember]
        public List<LoanDataMigrationFieldValue> LoanFileFieldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the dictionary mapping application ids to their list of fields.
        /// </summary>
        /// <value>The dictionary mapping application ids to their list of fields.</value>
        [DataMember]
        public Dictionary<Guid, List<LoanDataMigrationFieldValue>> ApplicationFieldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the list of condition errors if the migration failed to meet the running conditions.
        /// </summary>
        /// <value>The condition errors if the migration failed to meet the running conditions.</value>
        [DataMember]
        public List<string> ConditionErrorMessages
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the collection values gathered during this migration.
        /// </summary>
        /// <value>The collection values gathered for this migration.</value>
        [DataMember]
        public Dictionary<string, LoanDataMigrationCollectionValue> CollectionValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the UI should only show the old values or not.
        /// </summary>
        /// <value>Whether the UI will only show the old values or not.</value>
        [DataMember]
        public bool OnlyShowOldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether only the new values should always be shown in the UI,
        /// even if the migration does not pass it's failure conditions.
        /// </summary>
        [DataMember]
        public bool AlwaysShowNewValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the result of the migration. Will indiciate which info is valid.
        /// </summary>
        /// <value>The result of the migration.</value>
        [DataMember]
        public MigrationResultT ResultStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the migration has permission errors.
        /// </summary>
        [DataMember]
        public bool HasPermissionErrors
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a mapping from app id to app name.
        /// </summary>
        /// <value>Mapping from app id to app name.</value>
        [DataMember]
        internal Dictionary<Guid, string> AppIdToAppName
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the composite key for the migration audit history stored in FileDb for the loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="partialKey">A guid that makes up half of the key. This is the guid stored in the database.</param>
        /// <returns>The composite key.</returns>
        /// <remarks>
        /// The following is the key produced by this function: 
        /// [partialKey]_sLoanDataMigrationAuditHistory_[loanId].
        /// </remarks>
        public static string GetFileDbKey(Guid loanId, Guid partialKey)
        {
            return partialKey.ToString("N") + "_sLoanDataMigrationAuditHistory_" + loanId.ToString("N");
        }

        /// <summary>
        /// Clones this object.
        /// </summary>
        /// <returns>The cloned object.</returns>
        public object Clone()
        {
            LoanDataMigrationResult clonedResult = new LoanDataMigrationResult(this.LoanId, this.BrokerId, this.UserName, this.UserId, this.Name, this.TimePerformed, this.ResultStatus, this.AssociatedVersion, this.HasPermissionErrors);
            clonedResult.ConditionErrorMessages = new List<string>(this.ConditionErrorMessages);
            clonedResult.LoanFileFieldValues = this.LoanFileFieldValues.Select(values => (LoanDataMigrationFieldValue)values.Clone()).ToList();
            clonedResult.AppIdToAppName = new Dictionary<Guid, string>(this.AppIdToAppName);
            clonedResult.OnlyShowOldValues = this.OnlyShowOldValues;
            clonedResult.AlwaysShowNewValues = this.AlwaysShowNewValues;

            if (this.CollectionValues != null)
            {
                foreach (var collectionValue in this.CollectionValues)
                {
                    clonedResult.CollectionValues.Add(collectionValue.Key, (LoanDataMigrationCollectionValue)collectionValue.Value.Clone());
                }
            }

            foreach (var appEntry in this.ApplicationFieldValues)
            {
                var clonedValues = appEntry.Value.Select(values => (LoanDataMigrationFieldValue)values.Clone()).ToList();
                clonedResult.ApplicationFieldValues.Add(appEntry.Key, clonedValues);
            }

            return clonedResult;
        }
    }
}
