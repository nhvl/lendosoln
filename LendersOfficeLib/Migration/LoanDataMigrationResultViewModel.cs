﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// A view model to represent an individual migration result.
    /// </summary>
    [DataContract]
    public class LoanDataMigrationResultViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationResultViewModel"/> class.
        /// Will transform the result paramter into a view model.
        /// </summary>
        /// <param name="result">The migration result to populate this view model with.</param>
        public LoanDataMigrationResultViewModel(LoanDataMigrationResult result)
        {
            this.ConditionErrorMessages = new List<string>();
            this.LoanFileFieldValues = new List<FieldValueModel>();
            this.AppFieldValues = new List<KeyValuePair<string, List<FieldValueModel>>>();
            this.CollectionValues = new List<CollectionValueModel>();

            this.ToViewModel(result);
        }

        /// <summary>
        /// Gets or sets a general error if something in the migration errored out.
        /// </summary>
        /// <value>A general error if something in the migration errored.</value>
        [DataMember]
        public string GeneralError
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the migration's version number.
        /// </summary>
        /// <value>The migration's version number.</value>
        [DataMember]
        public LoanVersionT Version
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the migration's name.
        /// </summary>
        /// <value>The migration's name.</value>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user who ran this migration.
        /// </summary>
        /// <value>The user who ran this migration.</value>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the date the migration was ran.
        /// </summary>
        /// <value>The date the migration was ran.</value>
        [DataMember]
        public string DateRan
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the migration result status.
        /// </summary>
        /// <value>The migration's result status.</value>
        [DataMember]
        public MigrationResultT Result
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of errors due to migration conditions triggering.
        /// </summary>
        /// <value>Gets or sets a list of errords due to migration conditions triggering.</value>
        [DataMember]
        public List<string> ConditionErrorMessages
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the UI will only display the old values.
        /// </summary>
        /// <value>Indicates whether the UI will only display the old values.</value>
        [DataMember]
        public bool OnlyShowOldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether only the new values should always be shown in the UI,
        /// even if the migration does not pass it's failure conditions.
        /// </summary>
        [DataMember]
        public bool AlwaysShowNewValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the migration has permission errors.
        /// </summary>
        [DataMember]
        public bool HasPermissionErrors
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the list of collection values for this migration.
        /// </summary>
        /// <value>The list of collection values for this migration.</value>
        [DataMember]
        private List<CollectionValueModel> CollectionValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of fields with their old and new values and the page that it appears in.
        /// </summary>
        /// <value>A list of fields with their old and new values and the page that it appears in.</value>
        [DataMember]
        private List<FieldValueModel> LoanFileFieldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a dictionary of apps and the field changes to display for each one.
        /// </summary>
        /// <value>A dictionary of apps and the field changes to display for each one.</value>
        [DataMember]
        private List<KeyValuePair<string, List<FieldValueModel>>> AppFieldValues
        {
            get;
            set;
        }

        /// <summary>
        /// Populates this view model using the result paramter.
        /// </summary>
        /// <param name="result">The migration result used to populate this view model.</param>
        public void ToViewModel(LoanDataMigrationResult result)
        {
            this.Name = result.Name;
            this.Version = result.AssociatedVersion;
            this.Result = result.ResultStatus;
            this.DateRan = result.TimePerformed.ToString("M/dd/yyyy hh:mm:ss tt");
            this.UserName = result.UserName;
            this.OnlyShowOldValues = result.OnlyShowOldValues;
            this.AlwaysShowNewValues = result.AlwaysShowNewValues;
            this.HasPermissionErrors = result.HasPermissionErrors;
            this.CollectionValues = this.PopulateCollectionModel(result.CollectionValues);

            foreach (LoanDataMigrationFieldValue fieldValue in result.LoanFileFieldValues)
            {
                this.LoanFileFieldValues.Add(new FieldValueModel(fieldValue));
            }

            foreach (KeyValuePair<Guid, List<LoanDataMigrationFieldValue>> appFieldsMap in result.ApplicationFieldValues)
            {
                string appName = string.Empty;
                result.AppIdToAppName.TryGetValue(appFieldsMap.Key, out appName);

                var appFieldValuesModel = new List<FieldValueModel>();
                foreach (var fieldValue in appFieldsMap.Value)
                {
                    appFieldValuesModel.Add(new FieldValueModel(fieldValue));
                }

                this.AppFieldValues.Add(new KeyValuePair<string, List<FieldValueModel>>(appName, appFieldValuesModel));
            }

            if (this.Result == MigrationResultT.IncorrectVersionForMigration)
            {
                this.GeneralError = "There are no data conflicts but this migration cannot be ran due to unsuccessful earlier migrations.";
                return;
            }
            else if (this.Result == MigrationResultT.MigrationError)
            {
                this.GeneralError = "There was an error with this migration.";
                return;
            }
            else if (this.Result == MigrationResultT.NoAttempt)
            {
                this.GeneralError = "This migration was not attempted. Previous required migrations may not have been successful.";
                return;
            }
            else if (this.Result == MigrationResultT.IncorrectVersionForConditions)
            {
                this.GeneralError = "This loan is not on the proper version to run pre-migration checks.";
            }

            if (result.ConditionErrorMessages != null)
            {
                this.ConditionErrorMessages = new List<string>(result.ConditionErrorMessages);
            }
        }

        /// <summary>
        /// Returns the list of field Ids for this result model.
        /// </summary>
        /// <returns>The list of field Ids for this result model.</returns>
        public IEnumerable<string> GetDependentFieldIds()
        {
            List<string> fieldIds = new List<string>();
            fieldIds.AddRange(this.LoanFileFieldValues.Select(fieldValue => fieldValue.FieldId));
            fieldIds.AddRange(this.CollectionValues.Select(item => item.ItemId));
            if (this.AppFieldValues.Count != 0)
            {
                fieldIds.AddRange(this.AppFieldValues[0].Value.Select(appFieldValue => appFieldValue.FieldId));
            }

            return fieldIds;
        }

        /// <summary>
        /// Populates the field value Pages property using the provided dictionary.
        /// </summary>
        /// <param name="idsToPages">The dictionary containing the info needed to populate the Pages property.</param>
        public void SetFieldPageUrls(Dictionary<string, Dictionary<string, string>> idsToPages)
        {
            foreach (FieldValueModel valueModel in this.LoanFileFieldValues)
            {
                Dictionary<string, string> pages = null;
                if (idsToPages.TryGetValue(valueModel.FieldId, out pages))
                {
                    valueModel.Pages = pages.ToList();
                }
            }

            foreach (var appToFieldValues in this.AppFieldValues)
            {
                foreach (var appValueModel in appToFieldValues.Value)
                {
                    Dictionary<string, string> appPages = null;
                    if (idsToPages.TryGetValue(appValueModel.FieldId, out appPages))
                    {
                        appValueModel.Pages = appPages.ToList();
                    }
                }
            }

            foreach (var collectionItem in this.CollectionValues)
            {
                Dictionary<string, string> pages = null;
                if (idsToPages.TryGetValue(collectionItem.ItemId, out pages))
                {
                    collectionItem.Pages = pages.ToList();
                }
            }
        }

        /// <summary>
        /// Produces a collection model.
        /// </summary>
        /// <param name="collectionResults">The results to produce a collection model from.</param>
        /// <returns>The collection model.</returns>
        private List<CollectionValueModel> PopulateCollectionModel(Dictionary<string, LoanDataMigrationCollectionValue> collectionResults)
        {
            List<CollectionValueModel> collectionModel = new List<CollectionValueModel>();

            foreach (var collectionItem in collectionResults.Values)
            {
                List<FieldValueModel> properties = null;
                if (collectionItem.FinalValues != null)
                {
                    properties = collectionItem.FinalValues.Values.Select(value => new FieldValueModel(value)).ToList();
                }

                List<CollectionValueModel> childItems = null;
                if (collectionItem.ChildCollectionItems != null)
                {
                    childItems = this.PopulateCollectionModel(collectionItem.ChildCollectionItems);
                }

                collectionModel.Add(new CollectionValueModel(collectionItem.Id, collectionItem.FriendlyName, collectionItem.ItemType, collectionItem.UsesItemTypeAsLabel, childItems, properties));
            }

            return collectionModel;
        }

        /// <summary>
        /// Model for collections.
        /// </summary>
        [DataContract]
        private class CollectionValueModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="CollectionValueModel"/> class.
            /// </summary>
            /// <param name="id">The id of the collection.</param>
            /// <param name="friendlyName">A friendly display name.</param>
            /// <param name="itemType">The type of the collection item.</param>
            /// <param name="usesItemTypeLabel">Whether the collection item should use the item type as a label.</param>
            /// <param name="collectionItems">The child collection items.</param>
            /// <param name="values">The values to display.</param>
            public CollectionValueModel(string id, string friendlyName, string itemType, bool usesItemTypeLabel, List<CollectionValueModel> collectionItems, List<FieldValueModel> values)
            {
                this.ItemId = id;
                this.FriendlyName = friendlyName;
                this.CollectionValues = collectionItems;
                this.FinalValues = values;
                this.ItemType = itemType;
                this.UsesItemTypeAsLabel = usesItemTypeLabel;
            }

            /// <summary>
            /// Gets or sets a value indicating whether this collection item uses its item type as a label.
            /// </summary>
            /// <value>A value indicating whether this collection item uses its item type as a label.</value>
            [DataMember(Name = "UsesItemType")]
            public bool UsesItemTypeAsLabel
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the type of the collection item.
            /// </summary>
            /// <value>The type of the collection item.</value>
            [DataMember]
            public string ItemType
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets an id for this collection item.
            /// </summary>
            /// <value>The id for this collection item.</value>
            [DataMember]
            public string ItemId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the friendly name for this collection item.
            /// </summary>
            /// <value>The friendly name for this collection item.</value>
            [DataMember]
            public string FriendlyName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a list of pages that this collection item appears on.
            /// </summary>
            /// <value>A list of pages that this collection item appears on.</value>
            /// <remarks>
            /// For collections, this only really counts for the first set of collections.
            /// </remarks>
            [DataMember]
            public IEnumerable<KeyValuePair<string, string>> Pages
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the property values to display for this collection item.
            /// </summary>
            [DataMember]
            public List<FieldValueModel> FinalValues
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the child collections for this item.
            /// </summary>
            /// <value>The child collections for this item.</value>
            [DataMember]
            public List<CollectionValueModel> CollectionValues
            {
                get;
                set;
            }
        }

        /// <summary>
        /// View model that represents the migration's field values.
        /// </summary>
        [DataContract]
        private class FieldValueModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FieldValueModel"/> class.
            /// </summary>
            /// <param name="fieldValue">The migration values to convert.</param>
            public FieldValueModel(LoanDataMigrationFieldValue fieldValue)
            {
                this.ToViewModel(fieldValue);
            }

            /// <summary>
            /// Gets or sets the field id.
            /// </summary>
            /// <value>The field id.</value>
            [DataMember]
            public string FieldId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the field's friendly name.
            /// </summary>
            /// <value>The field's friendly name.</value>
            [DataMember]
            public string FieldName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the field's old value.
            /// </summary>
            /// <value>The field's old value.</value>
            [DataMember]
            public string OldValue
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the field's new value.
            /// </summary>
            /// <value>The field's new value.</value>
            [DataMember]
            public string NewValue
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the pages that this field can be found in.
            /// </summary>
            /// <value>The pages that this field can be found in.</value>
            [DataMember]
            public IEnumerable<KeyValuePair<string, string>> Pages
            {
                get;
                set;
            }

            /// <summary>
            /// Populates this view model using the migration result's field values.
            /// </summary>
            /// <param name="fieldValues">The migration values from the migration results.</param>
            private void ToViewModel(LoanDataMigrationFieldValue fieldValues)
            {
                this.FieldName = fieldValues.FriendlyName;
                this.FieldId = fieldValues.FieldId;
                this.OldValue = fieldValues.OldValue;
                this.NewValue = fieldValues.NewValue;
            }
        }
    }
}
