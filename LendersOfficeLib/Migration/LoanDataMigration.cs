﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// The base class for all migrations going into the migration page.
    /// </summary>
    public abstract class LoanDataMigration
    {
        /// <summary>
        /// The fields that are always needed when running a loan data migration.
        /// </summary>
        public static readonly ReadOnlyCollection<string> ConstantFieldsForLoanDataMigration = new ReadOnlyCollection<string>(new string[] 
        {
            "sLoanVersionT",
            "sLoanDataMigrationAuditHistory",
            "aAppId",
            "aAppNm"
        });

        /// <summary>
        /// Whether this migration is being automatically ran by the system.
        /// </summary>
        private bool isSystemMigration = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigration"/> class.
        /// The data loan will need to be made using something other than CreateUsingSmartDependency.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being automatically performed by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        protected LoanDataMigration(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
        {
            this.UserName = userName;
            this.UserId = userId;
            this.BrokerId = brokerId;
            this.isSystemMigration = isSystemMigration;
            this.DataLoan = dataLoan;
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public abstract string Name
        {
            get;
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public abstract LoanVersionT AssociatedVersion
        {
            get;
        }

        /// <summary>
        /// Gets the minimum loan version in order to run the pre run failure conditions and to get the old values of the fields. Defaults to the version before the AssociatedVersion.
        /// </summary>
        /// <value>The minimum loan version in order to run the pre-run failure conditions.</value>
        public virtual LoanVersionT MinimumRequiredVersionForConditionCheck
        {
            get
            {
                int previousVersion = (int)this.AssociatedVersion - 1;
                if (Enum.IsDefined(typeof(LoanVersionT), previousVersion))
                {
                    return (LoanVersionT)previousVersion;
                }
                else
                {
                    return LoanVersionT.Version0;
                }
            }
        }

        /// <summary>
        /// Gets the data loan that will be used with this migration.
        /// </summary>
        /// <value>The data loan that will be used with this migration.</value>
        internal CPageData DataLoan
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a list of loan level fields to display on the migration page for comparison.
        /// This is used to create the list of fields for a before and after comparison.
        /// These fields will also be saved in the results and will be available for any rollbacks implemented in the future.
        /// </summary>
        /// <value>List of fields to display on the migration page for comparison.</value>
        protected virtual IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get;
        }

        /// <summary>
        /// Gets a list of app level fields to display on the migration page for comparison.
        /// This will be used to create the list of app level fields for a before and after comparison.
        /// These fields will also be saved in the results and will be available for any rollbacks implemented in the future.
        /// </summary>
        /// <value>List of app level fields to display on the migration page for comparison.</value>
        protected virtual IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get;
        }

        /// <summary>
        /// Gets a list of collection items to display on the migration page for comparison.
        /// The collection values will be saved in the results for any future rollbacks.
        /// </summary>
        /// <value>A list of collections to display and compare.</value>
        protected virtual IEnumerable<CollectionItem> CollectionItems
        {
            get;
        }

        /// <summary>
        /// Gets a list of conditions that will prevent a migration from happening.
        /// The condition function should return a list of error messages if the migration should NOT be ran.
        /// The PDEs will be giving us the conditions in that format according to the template.
        /// </summary>
        /// <value>The list of conditions that will prevent a migration from happening.</value>
        protected virtual IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether only the old values will be shown in the UI.
        /// </summary>
        /// <value>A value indicating whether only the old values will be shown in the UI.</value>
        protected virtual bool OnlyShowOldValues
        {
            get;
        }

        /// <summary>
        /// Gets or sets the name of the user who will run this migration.
        /// </summary>
        /// <value>The name of the user who will run this migration.</value>
        protected string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the user who will run this migration..
        /// </summary>
        /// <value>The id of the user who will run this migration.</value>
        protected Guid UserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        protected Guid BrokerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether only the new values should always be shown in the UI,
        /// even if the migration does not pass it's failure conditions.
        /// </summary>
        /// <remarks>
        /// Only set this to true if you are sure running PerformMigration will NOT throw.
        /// </remarks>
        protected virtual bool AlwaysShowNewValues
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the migration has permission errors.
        /// </summary>
        protected bool HasPermissionErrors
        {
            get;
            set;
        }

        /// <summary>
        /// This function will process the failure conditions, construct a list of pre-migration values, run the migration, and make a list of post-migration values if needed.
        /// </summary>
        /// <returns>Returns an object that contains information about the migration results.</returns>
        public LoanDataMigrationResult PreviewFullMigration()
        {
            DateTime now = DateTime.Now;

            if (this.DataLoan == null)
            {
                var noDataLoanResult = new LoanDataMigrationResult(Guid.Empty, this.BrokerId, this.UserName, this.UserId, this.Name, now, MigrationResultT.NoDataLoan, this.AssociatedVersion, false);
                return noDataLoanResult;
            }

            this.DataLoan.sIsSystemBatchMigration = this.isSystemMigration;

            // We can grab the old values and check the failure conditions as long as the data loan's version is greater than the specified minimum version for this migration.
            int minVersionAsInt = (int)this.MinimumRequiredVersionForConditionCheck;
            if ((int)this.DataLoan.sLoanVersionT < minVersionAsInt)
            {
                var conditionCheckVersionNotMetResult = new LoanDataMigrationResult(this.DataLoan.sLId, this.BrokerId, this.UserName, this.UserId, this.Name, now, MigrationResultT.IncorrectVersionForConditions, this.AssociatedVersion, false);
                return conditionCheckVersionNotMetResult;
            }

            var loanValues = new Dictionary<string, LoanDataMigrationFieldValue>();
            var appIdToAppValues = new Dictionary<Guid, Dictionary<string, LoanDataMigrationFieldValue>>();
            var appIdToAppName = new Dictionary<Guid, string>();
            var collectionValues = new Dictionary<string, LoanDataMigrationCollectionValue>();

            try
            {
                // Grab the old values of the specified fields.                
                if (this.LoanFileFieldItems != null)
                {
                    foreach (FieldValueItem loanItem in this.LoanFileFieldItems)
                    {
                        LoanDataMigrationFieldValue value = new LoanDataMigrationFieldValue(loanItem.FieldId, loanItem.FriendlyName);
                        value.OldValue = loanItem.Getter(this.DataLoan, null);
                        loanValues.Add(loanItem.FieldId, value);
                    }
                }

                // For app level fields.
                if (this.ApplicationFieldItems != null)
                {
                    for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
                    {
                        CAppData app = this.DataLoan.GetAppData(appIndex);
                        appIdToAppValues.Add(app.aAppId, new Dictionary<string, LoanDataMigrationFieldValue>());
                        appIdToAppName.Add(app.aAppId, app.aAppNm);

                        Dictionary<string, LoanDataMigrationFieldValue> appFieldValues = appIdToAppValues[app.aAppId];
                        foreach (FieldValueItem appItem in this.ApplicationFieldItems)
                        {
                            LoanDataMigrationFieldValue appFieldValue = new LoanDataMigrationFieldValue(appItem.FieldId, appItem.FriendlyName);
                            appFieldValue.OldValue = appItem.Getter(this.DataLoan, app);
                            appFieldValues.Add(appItem.FieldId, appFieldValue);
                        }
                    }
                }

                if (this.CollectionItems != null)
                {
                    collectionValues = this.GetOldCollectionValues(this.CollectionItems);
                }
            }
            catch (PermissionException e)
            {
                Tools.LogError(e);
                this.HasPermissionErrors = true;

                loanValues = new Dictionary<string, LoanDataMigrationFieldValue>();
                appIdToAppValues = new Dictionary<Guid, Dictionary<string, LoanDataMigrationFieldValue>>();
                appIdToAppName = new Dictionary<Guid, string>();
                collectionValues = new Dictionary<string, LoanDataMigrationCollectionValue>();
            }            
            
            // Check the failure conditions to see if the migration should even run.
            List<string> errorMessages = new List<string>();

            if (this.FailureConditions != null)
            {
                foreach (PreRunFailureCondition condition in this.FailureConditions)
                {
                    List<string> conditionErrors = condition.RunCondition();
                    if (conditionErrors != null && conditionErrors.Count != 0)
                    {
                        errorMessages.AddRange(conditionErrors);
                    }
                }
            }

            bool hasFailureCondition = errorMessages.Any();
            if (hasFailureCondition && !this.AlwaysShowNewValues)
            {
                if (this.HasPermissionErrors)
                {
                    errorMessages = new List<string>() { ErrorMessages.SmallMigrationFrameworkPermissionErrorMsg };
                }

                return this.CreateConditionFailureResult(loanValues, appIdToAppValues, appIdToAppName, collectionValues, errorMessages);
            }

            // The conditions may have passed but the actual migration should not be ran unless we are exactly one version behind this migration's associated version.
            int versionAsInt = (int)this.AssociatedVersion;
            if (versionAsInt - 1 != (int)this.DataLoan.sLoanVersionT) 
            {
                var incorrectVersionResult = this.ConstructMigrationResult(now, MigrationResultT.IncorrectVersionForMigration, loanValues, appIdToAppValues, appIdToAppName, collectionValues);
                return incorrectVersionResult;
            }

            List<string> errors;

            // Create results based on if the migration was successful or not.
            bool isMigrationSuccessful = this.PerformMigration(out errors);
            LoanDataMigrationResult finalResult;
            if (isMigrationSuccessful || this.AlwaysShowNewValues)
            {
                // We want to increment the version number before getting the new values since new calculations may be behind the version number.
                this.DataLoan.sLoanVersionT = this.AssociatedVersion;
                this.PerformAfterVersionUpdate();

                if (!this.HasPermissionErrors)
                {
                    if (this.LoanFileFieldItems != null)
                    {
                        foreach (FieldValueItem loanItem in this.LoanFileFieldItems)
                        {
                            LoanDataMigrationFieldValue value = loanValues[loanItem.FieldId];
                            value.NewValue = loanItem.Getter(this.DataLoan, null);
                        }
                    }

                    if (this.ApplicationFieldItems != null)
                    {
                        foreach (var appToFields in appIdToAppValues)
                        {
                            CAppData appData = this.DataLoan.GetAppData(appToFields.Key);
                            Dictionary<string, LoanDataMigrationFieldValue> appValues = appToFields.Value;

                            foreach (FieldValueItem appItem in this.ApplicationFieldItems)
                            {
                                appValues[appItem.FieldId].NewValue = appItem.Getter(this.DataLoan, appData);
                            }
                        }
                    }

                    if (this.CollectionItems != null)
                    {
                        this.GetNewCollectionValues(collectionValues, this.CollectionItems);
                    }
                }                
            }

            if (hasFailureCondition)
            {
                if (this.HasPermissionErrors)
                {
                    errorMessages = new List<string>() { ErrorMessages.SmallMigrationFrameworkPermissionErrorMsg };
                }

                return this.CreateConditionFailureResult(loanValues, appIdToAppValues, appIdToAppName, collectionValues, errorMessages);
            }

            if (isMigrationSuccessful)
            {
                finalResult = this.ConstructMigrationResult(now, MigrationResultT.Success, loanValues, appIdToAppValues, appIdToAppName, collectionValues);

                // Add the results to the audit field.
                this.DataLoan.sLoanDataMigrationAuditHistory.Add(finalResult);
            }
            else
            {
                if (errors != null && errors.Count != 0)
                {
                    foreach (string error in errors)
                    {
                        Tools.LogError(error);
                    }
                }

                finalResult = this.ConstructMigrationResult(now, MigrationResultT.MigrationError, loanValues, appIdToAppValues, appIdToAppName, collectionValues);
            }

            return finalResult;
        }

        /// <summary>
        /// This function will do the same thing as <see cref="PreviewFullMigration"/> but it will also save the loan changes if the migration is successful.
        /// </summary>
        /// <returns>The migration results.</returns>
        public LoanDataMigrationResult ApplyFullMigration()
        {
            LoanDataMigrationResult results = this.PreviewFullMigration();
            if (results.ResultStatus == MigrationResultT.Success)
            {
                this.DataLoan.Save();
            }

            return results;
        }

        /// <summary>
        /// The function that will run the actual migration. This will not do error checking, result formatting, saving, whatever. This should strictly do the migration.
        /// </summary>
        /// <param name="errors">Any errors you wish to log after the migration has ran.</param>
        /// <returns>Returns true if the migration is considered properly processed. False otherwise.</returns>
        protected abstract bool PerformMigration(out List<string> errors);

        /// <summary>
        /// This function will run after sLoanVersionT has been updated during the migration.
        /// Use this if you need to force update something after sLoanVersionT has been updated.
        /// </summary>
        protected virtual void PerformAfterVersionUpdate()
        {
        }

        /// <summary>
        /// Creates a <see cref="LoanDataMigrationResult"/> for use when migration failure conditions have been triggered.
        /// </summary>
        /// <param name="loanValues">Pre and post migration loan field values mapped by field name.</param>
        /// <param name="appIdToAppValues">Pre and post migration app field values mapped by app ID and field name.</param>
        /// <param name="appIdToAppName">A mapping of app IDs to app names.</param>
        /// <param name="collectionValues">Pre and post migration collection values mapped by field name.</param>
        /// <param name="errorMessages">Output list of string error messages.</param>
        /// <returns>An instance of <see cref="LoanDataMigrationResult"/>.</returns>
        private LoanDataMigrationResult CreateConditionFailureResult(Dictionary<string, LoanDataMigrationFieldValue> loanValues, Dictionary<Guid, Dictionary<string, LoanDataMigrationFieldValue>> appIdToAppValues, Dictionary<Guid, string> appIdToAppName, Dictionary<string, LoanDataMigrationCollectionValue> collectionValues, List<string> errorMessages)
        {
            var conditionFailResult = this.ConstructMigrationResult(DateTime.Now, MigrationResultT.FailureConditionTriggered, loanValues, appIdToAppValues, appIdToAppName, collectionValues);
            conditionFailResult.ConditionErrorMessages = errorMessages;
            return conditionFailResult;
        }

        /// <summary>
        /// Gets the old values for the collection items.
        /// </summary>
        /// <param name="collectionItems">The list of collections to evaluate.</param>
        /// <returns>A dictionary mapping the collection id to a collection value.</returns>
        private Dictionary<string, LoanDataMigrationCollectionValue> GetOldCollectionValues(IEnumerable<CollectionItem> collectionItems)
        {
            var values = new Dictionary<string, LoanDataMigrationCollectionValue>();

            foreach (CollectionItem collectionItem in collectionItems)
            {
                string id = collectionItem.Id;

                Dictionary<string, LoanDataMigrationFieldValue> propertyValues = null;
                if (collectionItem.PropertyValueItems != null)
                {
                    propertyValues = new Dictionary<string, LoanDataMigrationFieldValue>();

                    foreach (var propertyItem in collectionItem.PropertyValueItems)
                    {
                        var propertyValue = new LoanDataMigrationFieldValue(propertyItem.FieldId, propertyItem.FriendlyName);
                        propertyValue.OldValue = propertyItem.Getter(null, null);
                        propertyValues.Add(propertyItem.FieldId, propertyValue);
                    }
                }

                Dictionary<string, LoanDataMigrationCollectionValue> childCollectionValues = null;
                if (collectionItem.ChildCollectionItems != null)
                {
                    childCollectionValues = this.GetOldCollectionValues(collectionItem.ChildCollectionItems);
                }

                values.Add(id, new LoanDataMigrationCollectionValue(id, collectionItem.FriendlyName, collectionItem.ItemType, collectionItem.UsesItemTypeAsLabel, childCollectionValues, propertyValues));
            }

            return values;
        }

        /// <summary>
        /// Populates the collection dictionary with the new values after a successful migration.
        /// </summary>
        /// <param name="oldValues">The dictionary with the old collection values.</param>
        /// <param name="collectionItems">The collection items to loop through.</param>
        private void GetNewCollectionValues(Dictionary<string, LoanDataMigrationCollectionValue> oldValues, IEnumerable<CollectionItem> collectionItems)
        {
            foreach (CollectionItem collectionItem in collectionItems)
            {
                string id = collectionItem.Id;
                LoanDataMigrationCollectionValue matchedCollectionValue = oldValues[id];

                if (collectionItem.PropertyValueItems != null)
                {
                    foreach (var propertyItem in collectionItem.PropertyValueItems)
                    {
                        var propertyValue = matchedCollectionValue.FinalValues[propertyItem.FieldId];
                        propertyValue.NewValue = propertyItem.Getter(null, null);
                    }
                }

                if (collectionItem.ChildCollectionItems != null)
                {
                    this.GetNewCollectionValues(matchedCollectionValue.ChildCollectionItems, collectionItem.ChildCollectionItems);
                }
            }
        }

        /// <summary>
        /// Constructs the migration result.
        /// </summary>
        /// <param name="time">The time the migration was performed.</param>
        /// <param name="errorStatus">The migration status.</param>
        /// <param name="loanValues">The loan values to display.</param>
        /// <param name="appValues">The app values to display.</param>
        /// <param name="appIdToAppName">Maps from app id to map name.</param>
        /// <param name="collectionValues">The collection values to save.</param>
        /// <returns>The migration result.</returns>
        private LoanDataMigrationResult ConstructMigrationResult(
            DateTime time, 
            MigrationResultT errorStatus, 
            Dictionary<string, LoanDataMigrationFieldValue> loanValues, 
            Dictionary<Guid, Dictionary<string, LoanDataMigrationFieldValue>> appValues,
            Dictionary<Guid, string> appIdToAppName,
            Dictionary<string, LoanDataMigrationCollectionValue> collectionValues)
        {
            var result = new LoanDataMigrationResult(this.DataLoan.sLId, this.BrokerId, this.UserName, this.UserId, this.Name, time, errorStatus, this.AssociatedVersion, this.HasPermissionErrors);
            result.OnlyShowOldValues = this.OnlyShowOldValues;
            result.AlwaysShowNewValues = this.AlwaysShowNewValues;
            result.LoanFileFieldValues = new List<LoanDataMigrationFieldValue>(loanValues.Values);
            result.CollectionValues = collectionValues;
            result.AppIdToAppName = appIdToAppName;
            var appValuesForResult = new Dictionary<Guid, List<LoanDataMigrationFieldValue>>();
            result.ApplicationFieldValues = appValuesForResult;
            foreach (var oldAppValues in appValues)
            {
                appValuesForResult.Add(oldAppValues.Key, new List<LoanDataMigrationFieldValue>(oldAppValues.Value.Values));
            }

            return result;
        }

        /// <summary>
        /// Class to help display collections in the UI. 
        /// </summary>
        /// <remarks>
        /// This class will act as a node in a tree that represents how a collection will be displayed.
        /// The list of FieldValueItems are the properties that will be compared in the UI.
        /// The list of CollectionItems are the child collection items that need to be further expanded to get deeper properties out.
        /// Also, for the getter in the FieldValueItem, the function in it requires the app and loan as parameters. However, you're not gonna use them. I just wanted to reuse
        /// the FieldValueItem since it was already there. 
        /// </remarks>
        protected class CollectionItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="CollectionItem"/> class.
            /// </summary>
            /// <param name="friendlyName">The friendly name to display.</param>
            /// <param name="id">The id for this collection.</param>
            /// <param name="itemType">The type of item this collection item is.</param>
            /// <param name="shouldUseItemTypeLabel">Whether the UI will use the itemType as a label or just number the collection items.</param>
            /// <param name="collectionItems">Any child collection items.</param>
            /// <param name="propertyItems">The final property items.</param>
            public CollectionItem(string friendlyName, string id, string itemType, bool shouldUseItemTypeLabel, List<CollectionItem> collectionItems, List<FieldValueItem> propertyItems)
            {
                this.Id = id;
                this.FriendlyName = friendlyName;
                this.ChildCollectionItems = collectionItems;
                this.PropertyValueItems = propertyItems;
                this.ItemType = itemType;
                this.UsesItemTypeAsLabel = shouldUseItemTypeLabel;
            }

            /// <summary>
            /// Gets or sets a value indicating whether this item uses its item type as a label.
            /// </summary>
            /// <value>A value indicating whether this item uses its item type as a label.</value>
            public bool UsesItemTypeAsLabel
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the type of this collection item.
            /// </summary>
            /// <value>The type of this collection item.</value>
            public string ItemType
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the identifying name for this collection.
            /// </summary>
            /// <value>The identifying name for this collection.</value>
            public string Id
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the friendly name to display for the collection.
            /// </summary>
            /// <value>The friendly name to display for the collection.</value>
            public string FriendlyName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets the getters for the properties to display.
            /// </summary>
            /// <value>The properties to display.</value>
            public List<FieldValueItem> PropertyValueItems
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a list of child collection items that still need to be expanded.
            /// </summary>
            /// <value>A list of child collection items that still need to be expanded.</value>
            public List<CollectionItem> ChildCollectionItems
            {
                get;
                private set;
            }
        }

        /// <summary>
        /// Container holding info needed to get a field's value.
        /// </summary>
        protected class FieldValueItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FieldValueItem"/> class.
            /// </summary>
            /// <param name="fieldId">The id of the field.</param>
            /// <param name="friendlyName">The display name of the field.</param>
            /// <param name="getter">The getter for the field.</param>
            public FieldValueItem(string fieldId, string friendlyName, Func<CPageData, CAppData, string> getter)
            {
                this.FieldId = fieldId;
                this.FriendlyName = friendlyName;
                this.Getter = getter;
            }

            /// <summary>
            /// Gets the field's field id.
            /// </summary>
            /// <value>The field's field id.</value>
            public string FieldId
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the field's friendly name.
            /// </summary>
            /// <value>The field's friendly name.</value>
            public string FriendlyName
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a function that will retrieve the field's value.
            /// </summary>
            /// <value>A function that retrieves the field's value.</value>
            public Func<CPageData, CAppData, string> Getter
            {
                get;
                private set;
            }
        }

        /// <summary>
        /// A class to contain the conditions needed to run before a migration is attempted.
        /// </summary>
        protected class PreRunFailureCondition
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PreRunFailureCondition"/> class.
            /// </summary>
            /// <param name="conditionToRun">The condition that should be ran.</param>
            public PreRunFailureCondition(Func<List<string>> conditionToRun)
            {
                this.Condition = conditionToRun;
            }

            /// <summary>
            /// Gets or sets the condition that should be ran to consider running the actual migration.
            /// This condition should return a list of strings containing error messages if the migration should NOT be ran.
            /// Otherwise, it should return null.
            /// This is how the PDEs will give us the conditions, so it'll be better if we code it that way.
            /// </summary>
            /// <value>The condition to run.</value>
            private Func<List<string>> Condition
            {
                get;
                set;
            }

            /// <summary>
            /// Runs the condition.
            /// </summary>
            /// <returns>Returns a list of string error messages if the condition triggered. Returns null otherwise.</returns>
            public List<string> RunCondition()
            {
                return this.Condition();
            }
        }
    }
}
