﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.Migration
{
    /// <summary>
    /// Utility functions for migrations, such as loan and broker retrieval.
    /// </summary>
    public static class MigrationUtils
    {
        /// <summary>
        /// Returns a enumeration of the guids of all the active brokers
        /// </summary>
        public static IEnumerable<Guid> GetAllActiveBrokers()
        {
            var brokerIdList = new List<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokerIds", null))
                {
                    while (reader.Read())
                    {
                        brokerIdList.Add((Guid)reader["BrokerId"]);
                    }
                }
            }

            return brokerIdList;
        }

        /// <summary>
        /// Returns a enumeration of the guids of all the brokers, active or inactive.
        /// </summary>
        public static IEnumerable<Guid> GetAllActiveAndInactiveBrokerIds()
        {
            // change this to a hashset as per DD, we can have duplicates in different dbs.  
            // at most one is active.
            var brokerIdList = new HashSet<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListActiveAndInactiveBrokerIds", null))
                {
                    while (reader.Read())
                    {
                        brokerIdList.Add((Guid)reader["BrokerId"]);
                    }
                }
            }

            return brokerIdList;
        }

        public static Dictionary<Guid, Guid> GetAllLoansWithBrokerId(bool? isTemplate, bool? isValid)
        {
            var brokerIdList = Tools.GetAllActiveBrokers();
            return GetAllLoansWithBrokerIdUsingGivenBrokers(isTemplate, isValid, brokerIdList);
        }

        public static Dictionary<Guid, Guid> GetAllLoansWithBrokerIdUsingGivenBrokers(bool? isTemplate, bool? isValid, IEnumerable<Guid> brokerIds)
        {
            var brokerIdByLoanId = new Dictionary<Guid, Guid>();
            foreach (var brokerId in brokerIds)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId),
                                                new SqlParameter("@IsTemplate", isTemplate),
                                                new SqlParameter("@IsValid", isValid)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        brokerIdByLoanId.Add((Guid)reader["LoanId"], brokerId);
                    }
                }
            }

            return brokerIdByLoanId;
        }

        public static List<Guid> GetAllLoans(bool? isTemplate, bool? isValid, bool excludeQp)
        {
            var brokerIdList = Tools.GetAllActiveBrokers();
            return GetAllLoansUsingGivenBrokers(isTemplate, isValid, excludeQp, brokerIdList);
        }

        public static List<Guid> GetAllLoansUsingGivenBrokers(bool? isTemplate, bool? isValid, bool excludeQp, IEnumerable<Guid> brokerIdList)
        {
            var loanIdList = new List<Guid>();
            foreach (var brokerId in brokerIdList)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId),
                                                new SqlParameter("@IsTemplate", isTemplate),
                                                new SqlParameter("@IsValid", isValid),
                                                new SqlParameter("@ExcludeQp", excludeQp)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        loanIdList.Add((Guid)reader["LoanId"]);
                    }
                }
            }
            return loanIdList;
        }

        public static List<Guid> GetAllCCTemplatesUsingGivenBrokers(IEnumerable<Guid> brokerIdList)
        {
            var templateIdList = new List<Guid>();
            foreach (var brokerId in brokerIdList)
            {
                SqlParameter[] parameters = { new SqlParameter("@BrokerID", brokerId) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveAllCcTemplatesByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        templateIdList.Add(new Guid(reader["cCcTemplateId"].ToString()));
                    }
                }
            }
            return templateIdList;
        }

        public static List<Guid> GetAllLoansUsingGivenBranches(bool? isTemplate, bool? isValid, Guid brokerId, IEnumerable<Guid> branchIdList)
        {
            var loanIdList = new List<Guid>();
            foreach (var branchId in branchIdList)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId),
                                                new SqlParameter("@BranchID", branchId),
                                                new SqlParameter("@IsTemplate", isTemplate),
                                                new SqlParameter("@IsValid", isValid)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBranchId", parameters))
                {
                    while (reader.Read())
                    {
                        loanIdList.Add((Guid)reader["LoanId"]);
                    }
                }
            }
            return loanIdList;
        }

        public static IEnumerable<Guid> GetAllQueryIDsFromBroker(Guid brokerID)
        {
            var queryIDList = new List<Guid>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerID)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "GetReportQueryDetails", parameters))
            {
                while (reader.Read())
                {
                    queryIDList.Add((Guid)reader["queryId"]);
                }
            }
            return queryIDList;
        }

        public static List<Guid> GetAllActivePmlUsersUsingGivenBrokers(IEnumerable<Guid> brokerIdList)
        {
            var employeeIdList = new List<Guid>();
            string procedureName = "ListActivePmlUsersByBrokerId";

            foreach (var brokerId in brokerIdList)
            {

                SqlParameter[] parameters = {
                                                new SqlParameter( "@LimitResultSet", false ),
                                                new SqlParameter( "@BrokerID", brokerId),
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, procedureName, parameters))
                {
                    while (reader.Read())
                    {
                        var employeeId = (Guid)reader["EmployeeId"];
                        employeeIdList.Add(employeeId);
                    }
                }
            }
            return employeeIdList;
        }

        public static List<Guid> GetAllEdocsFromLoan(Guid brokerId, Guid loanId)
        {
            var edocs = new List<Guid>();

            var sqlParameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@IsValid", true)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_FetchDocs", sqlParameters))
            {
                while (reader.Read())
                {
                    // apps. skip.
                }
                reader.NextResult();
                while (reader.Read())
                {
                    edocs.Add((Guid)reader["DocumentId"]);
                }
            }

            return edocs;
        }
    }
}
