﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds the collection values to save in the results file.
    /// </summary>
    [DataContract]
    public class LoanDataMigrationCollectionValue : ICloneable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationCollectionValue"/> class.
        /// </summary>
        /// <param name="id">The id for the collection item.</param>
        /// <param name="friendlyName">The friendly name for display.</param>
        /// <param name="itemType">The item type for this collection item.</param>
        /// <param name="useItemTypeLabel">Whether this collection item uses its item type as a label.</param>
        /// <param name="childItems">The child collection items we want to further expand.</param>
        /// <param name="finalValues">The values we want to display.</param>
        public LoanDataMigrationCollectionValue(string id, string friendlyName, string itemType, bool useItemTypeLabel, Dictionary<string, LoanDataMigrationCollectionValue> childItems, Dictionary<string, LoanDataMigrationFieldValue> finalValues)
        {
            this.Id = id;
            this.FriendlyName = friendlyName;
            this.ChildCollectionItems = childItems;
            this.FinalValues = finalValues;
            this.ItemType = itemType;
            this.UsesItemTypeAsLabel = useItemTypeLabel;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this collection item uses its item type as a label.
        /// </summary>
        /// <value>A value indicating whether this collection item uses its item type as a label.</value>
        [DataMember(Name = "UsesItemType")]
        public bool UsesItemTypeAsLabel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the item type for the collection item.
        /// </summary>
        /// <value>The item type for the collection item.</value>
        [DataMember]
        public string ItemType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id for the collection item.
        /// </summary>
        /// <value>The id for the collection item.</value>
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display name for the collection item.
        /// </summary>
        /// <value>The display name for the collection item.</value>
        [DataMember]
        public string FriendlyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the values of the properties we want to display.
        /// </summary>
        /// <value>The values of the properties we want to display.</value>
        [DataMember]
        public Dictionary<string, LoanDataMigrationFieldValue> FinalValues
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the child items we want to futher expand.
        /// </summary>
        /// <value>The child items we want to further expand.</value>
        [DataMember]
        public Dictionary<string, LoanDataMigrationCollectionValue> ChildCollectionItems
        {
            get;
            private set;
        }

        /// <summary>
        /// Clones the values in this object.
        /// </summary>
        /// <returns>The cloned object.</returns>
        public object Clone()
        {
            Dictionary<string, LoanDataMigrationFieldValue> clonedFinalValues = null;
            if (this.FinalValues != null)
            {
                clonedFinalValues = new Dictionary<string, LoanDataMigrationFieldValue>();
                foreach (var finalValue in this.FinalValues)
                {
                    clonedFinalValues.Add(finalValue.Key, (LoanDataMigrationFieldValue)finalValue.Value.Clone());
                }
            }

            Dictionary<string, LoanDataMigrationCollectionValue> clonedChildCollections = null;
            if (this.ChildCollectionItems != null)
            {
                clonedChildCollections = new Dictionary<string, LoanDataMigrationCollectionValue>();
                foreach (var childCollection in this.ChildCollectionItems)
                {
                    clonedChildCollections.Add(childCollection.Key, (LoanDataMigrationCollectionValue)childCollection.Value.Clone());
                }
            }

            LoanDataMigrationCollectionValue clonedValue = new LoanDataMigrationCollectionValue(this.Id, this.FriendlyName, this.ItemType, this.UsesItemTypeAsLabel, clonedChildCollections, clonedFinalValues);

            return clonedValue;
        }
    }
}
