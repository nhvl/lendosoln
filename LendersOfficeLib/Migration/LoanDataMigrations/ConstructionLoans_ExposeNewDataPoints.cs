﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Common;

    /// <summary>
    /// Exposes new data points to the UI and pre populates them to make migrating to the next version easier.
    /// </summary>
    public class ConstructionLoans_ExposeNewDataPoints : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionLoans_ExposeNewDataPoints"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">A value indicating whether this migration is being run as a system migration.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public ConstructionLoans_ExposeNewDataPoints(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the version number associated with this migration.
        /// </summary>
        /// <value>The version number associated with this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints;
            }
        }

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name
        {
            get
            {
                return "Expose and Pre-Populate New Construction Loan Data Points";
            }
        }

        /// <summary>
        /// Gets a value indicating whether only the new values should always be shown in the UI,
        /// even if the migration does not pass it's failure conditions.
        /// </summary>
        /// <remarks>
        /// Only set this to true if you are sure running PerformMigration will NOT throw.
        /// </remarks>
        protected override bool AlwaysShowNewValues
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the app fields for display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    // OPM 471792 - Change field values.
                    new FieldValueItem(
                        nameof(CPageData.sConstructionPurposeT),
                        "Construction Purpose",
                        (loan, app) => loan.sConstructionPurposeT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sLotOwnerT),
                        "Lot Owner",
                        (loan, app) => loan.sLotOwnerT.ToString()),

                    // OPM 471564 - Merge sLotImproveC and sConstructionImprovementAmt.
                    new FieldValueItem(
                        nameof(CPageData.sLotImprovC),
                        "Lot Improvement Cost",
                        (loan, app) => loan.sLotImprovC_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionImprovementAmt),
                        "Construction Improvement Amount",
                        (loan, app) => loan.sConstructionImprovementAmt_rep),

                    // OPM 474171 - Consolidated small migration changes.
                    new FieldValueItem(
                        nameof(CPageData.sPurchPrice),
                        "Purchase Price",
                        (loan, app) => loan.sPurchPrice_rep),

                    new FieldValueItem(
                        nameof(CPageData.sPurchasePrice1003),
                        "Purchase Price (1003)",
                        (loan, app) => loan.sPurchasePrice1003_rep),

                    new FieldValueItem(
                        nameof(CPageData.sLandCost),
                        "Land Cost",
                        (loan, app) => loan.sLandCost_rep),

                    new FieldValueItem(
                        nameof(CPageData.sLandIfAcquiredSeparately1003),
                        "Land (if acquired separately)",
                        (loan, app) => loan.sLandIfAcquiredSeparately1003_rep),

                    new FieldValueItem(
                        nameof(CPageData.sRefPdOffAmt1003),
                        "Refi (incl. debts to be paid off)",
                        (loan, app) => loan.sRefPdOffAmt1003_rep),

                    new FieldValueItem(
                        nameof(CPageData.sAltCost),
                        "Alterations, improvements, repairs",
                        (loan, app) => loan.sAltCost_rep)
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;

            this.DataLoan.sConstructionPurposeT = ConstructionLoanUtilities.CalculateConstructionPurpose(this.DataLoan);

            // NOTE: Depends on sConstructionPurposeT migration.
            this.DataLoan.sLotOwnerT = ConstructionLoanUtilities.CalculateLotOwner(this.DataLoan);

            if (this.DataLoan.sIsConstructionLoan && this.DataLoan.sPurchPrice > 0M && this.DataLoan.sLotImprovC > 0M && this.DataLoan.sLandCost == 0M)
            {
                this.DataLoan.sLandCost = Tools.SumMoney(new decimal[] { this.DataLoan.sPurchPrice, -this.DataLoan.sLotImprovC });
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }

            if (this.DataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                return null;
            }

            if (!this.DataLoan.sIsConstructionLoan)
            {
                return null;
            }

            List<string> migrationErrors = new List<string>();
            migrationErrors.AddRange(this.GetConstructionImprovementAmtMigrationErrors());
            migrationErrors.AddRange(this.GetLandCostMigrationErrors());
            migrationErrors.AddRange(this.GetRefPdOffAmt1003MigrationErrors());
            migrationErrors.AddRange(this.GetAltCostMigrationErrors());

            return migrationErrors;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetConstructionImprovementAmtMigrationErrors()
        {
            if (this.DataLoan.sLotImprovC == this.DataLoan.sConstructionImprovementAmt)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The \"Lot Improvement Cost\" must match the \"Construction Improvement Amount\"."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetLandCostMigrationErrors()
        {
            List<string> errors = new List<string>();

            if (this.DataLoan.sPurchPrice > 0M && this.DataLoan.sLandCost > 0M && this.DataLoan.sPurchPrice != this.DataLoan.sLotImprovC)
            {
                errors.Add("If \"Purchase Price\" is greater than $0.00 and \"Land Cost\" is greater than $0.00, then \"Purchase Price\" must equal the \"Lot Improvement Cost\".");
            }

            return errors;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetRefPdOffAmt1003MigrationErrors()
        {
            if (this.DataLoan.sRefPdOffAmt1003Lckd)
            {
                return new List<string>();
            }

            if (!this.DataLoan.sConstructionPurposeT.EqualsOneOf(ConstructionPurpose.ConstructionAndLotRefinance, ConstructionPurpose.ConstructionOnOwnedLot))
            {
                return new List<string>();
            }

            if (this.DataLoan.sLotImprovC == 0M)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The \"Refi (incl. debts to be paid off)\" amount will change after migration unless it is locked, Construction Purpose is changed from \"Construction And Lot Refinance\" or Lot Improvement Cost is set to $0.00."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetAltCostMigrationErrors()
        {
            if (!this.DataLoan.sIsRenovationLoan || this.DataLoan.sAltCostLckd)
            {
                return new List<string>();
            }

            if (this.DataLoan.sAltCost == 0M)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "For construction loans, the \"Alterations, improvements, repairs\" must be locked of set to $0.00"
            };
        }
    }
}
