﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using ObjLib.AdjustmentsAndOtherCredits;

    /// <summary>
    /// Categorizes adjustments description from user input into E_AdjustmentT enum values.
    /// </summary>
    public class ApplyPredefinedAdjustments : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplyPredefinedAdjustments"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public ApplyPredefinedAdjustments(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Applies a set of predefined adjustments to the existing adjustments for a loan.";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments;
            }
        }

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be dispalyed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sIsRestrictAdjustmentsAndOtherCreditsDescriptions),
                        "Is Restrict Adjustments and Other Credits Descriptions",
                        (loan, app) => loan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions.ToString())
                };
            }
        }

        /// <summary>
        /// Gets the collection items to display.
        /// </summary>
        /// <value>The collection items to display.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                List<CollectionItem> adjustmentListChildren = new List<CollectionItem>();                

                int i = 1;
                foreach (Adjustment adj in this.DataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit())
                {
                    List<FieldValueItem> adjustmentFields = new List<FieldValueItem>();
                    adjustmentFields.Add(
                        new FieldValueItem(
                        "AdjustmentType",
                        "Adjustment Type",
                        (loan, app) => adj.AdjustmentType.ToString()));
                    adjustmentFields.Add(
                        new FieldValueItem(
                        "Description",
                        "Description",
                        (loan, app) => adj.Description));

                    adjustmentListChildren.Add(
                        new CollectionItem(
                                "sAdjustmentList.CustomItems[" + i + "]",
                                "sAdjustmentList.CustomItems[" + i + "]",
                                "Adjustment",
                                shouldUseItemTypeLabel: true,
                                collectionItems: null,
                                propertyItems: adjustmentFields));
                    i++;
                }

                yield return new CollectionItem(
                    "Adjustment List", 
                    "sAdjustmentList", 
                    "Adjustment",
                    shouldUseItemTypeLabel: true, 
                    collectionItems: adjustmentListChildren, 
                    propertyItems: null);
            }
        }

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// This function is for running the actual migration.
        /// You don't need to update sLoanVersionT. That gets done automatically later on.
        /// So sometimes, if the the version number is used to just enable a calculation, this function can just return true without doing anything else.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = this.GetMigrationErrors();

            if (errors.Count > 0)
            {
                return false;
            }

            this.DataLoan.sAdjustmentList.SetLoanVersionNumber(LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments);

            Tuple<E_AdjustmentT, string> value;

            // string match the description and convert to a enum value if possible, if not, just set the type to "Other".
            foreach (Adjustment adj in this.DataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCreditAndAssets())
            {
                if (AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap.TryGetValue(adj.Description, out value))
                {
                    adj.AdjustmentType = value.Item1;
                    adj.Description = value.Item2;
                } 
                else
                {
                    adj.AdjustmentType = E_AdjustmentT.Other;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            var errors = new List<string>();

            if (this.DataLoan.sIsSystemBatchMigration && !this.DataLoan.IsTemplate)
            {
                errors.Add("Loan is not a template");
            }

            return errors;
        }
    }
}
