﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// This is a sample migration for the small data layer migration framework.
    /// </summary>
    public class SampleMigration : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> fieldGetters;

        /// <summary>
        /// List of app level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> appFieldGetters;

        /// <summary>
        /// List of collections to display in the UI and saved in the results.
        /// </summary>
        private List<CollectionItem> collections;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleMigration"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public SampleMigration(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {

            /* 
               ==== IMPORTANT NOTE! ====
               Due to the unit tests for the SMF, please make sure to check for a non-null DataLoan object
               before using it to initialize the getters.
            */
            this.fieldGetters = this.InitializeLoanGetters();
            this.appFieldGetters = this.InitializeAppGetters();
            this.conditions = this.InitializeConditions();
            this.collections = this.InitializeCollections();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Test Migration 1";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.TestMigration1;
            }
        }

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be dispalyed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.fieldGetters;
            }
        }

        /// <summary>
        /// Gets a list of app level fields that are to be displayed on the UI and saved to the results.
        /// </summary>
        /// <value>A list of app level fields that are to be displayed on the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return this.appFieldGetters;
            }
        }

        /// <summary>
        /// Gets the collection items to display.
        /// </summary>
        /// <value>The collection items to display.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                return this.collections;
            }
        }

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// This function is for running the actual migration.
        /// You don't need to update sLoanVersionT. That gets done automatically later on.
        /// So sometimes, if the the version number is used to just enable a calculation, this function can just return true without doing anything else.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;

            if (string.IsNullOrEmpty(this.DataLoan.sFHAPreviousCaseNum) &&
                !string.IsNullOrEmpty(this.DataLoan.sSpFhaCaseNumCurrentLoan))
            {
                this.DataLoan.sFHAPreviousCaseNum = this.DataLoan.sSpFhaCaseNumCurrentLoan;
            }

            return true;
        }

        /// <summary>
        /// Initializes the collection items.
        /// </summary>
        /// <returns>The list of collection items to parse.</returns>
        /// <remarks>
        /// Not the nicest way to do collections but here it is.
        /// The goal is to essentially make some trees. Each of the CollectionItems in the returned list will be the root nodes.
        /// Each CollectionItem can hold two lists. The list of FieldValueItems will act as child leaf nodes.
        /// The list of CollectionItems will be child nodes that can go and expand further if needed.
        /// Obviously, you eventually want to get it so that the CollectionItems only have FieldValueItems or we're gonna have a bad time.
        /// </remarks>
        private List<CollectionItem> InitializeCollections()
        {
            var feesToDisplay = new List<CollectionItem>();
            if (this.DataLoan == null)
            {
                return feesToDisplay;
            }

            var fees = this.DataLoan.sClosingCostSet.GetFees(fee => fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionF);

            foreach (BorrowerClosingCostFee fee in fees)
            {
                List<FieldValueItem> properties = new List<FieldValueItem>()
                {
                    new FieldValueItem("UniqueId", "Unique Id", (loan, app) => fee.UniqueId.ToString()),
                    new FieldValueItem("IsApr", "Apr", (loan, app) => fee.IsApr.ToString())
                };

                List<CollectionItem> payments = new List<CollectionItem>();
                foreach (BorrowerClosingCostFeePayment payment in fee.Payments)
                {
                    List<FieldValueItem> paymentProps = new List<FieldValueItem>()
                    {
                        new FieldValueItem("Amount", "Amount", (loan, app) => payment.Amount_rep),
                        new FieldValueItem("PaymentDate", "Date Paid", (loan, app) => payment.PaymentDate_rep)
                    };

                    payments.Add(new CollectionItem("Payment", payment.Id.ToString(), "Payment", false, null, paymentProps));
                }

                CollectionItem feeItem = new CollectionItem(fee.Description, fee.ClosingCostFeeTypeId.ToString(), "Fee", true, payments, properties);
                feesToDisplay.Add(feeItem);
            }

            List<CollectionItem> collectionItems = new List<CollectionItem>()
            {
                new CollectionItem("Closing Costs", "sClosingCostSet", "Borrower Fee Set", true, feesToDisplay, null)
            };

            return collectionItems;
        }

        /// <summary>
        /// Initializes the app field items.
        /// </summary>
        /// <returns>The app field items.</returns>
        /// <remarks>
        /// As you can see, the function takes in both a loan and an app. That's because I didn't feel like making a new 
        /// FieldValueItem class just for apps.
        /// </remarks>
        private List<FieldValueItem> InitializeAppGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem("aBNm", "Borrower Name", (loan, app) => app.aBNm));
            getters.Add(new FieldValueItem("aCNm", "CoBorrower Name", (loan, app) => app.aCNm));

            return getters;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items..</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem("sFHAPreviousCaseNum", "Test Name 1", (loan, app) => loan.sFHAPreviousCaseNum));
            getters.Add(new FieldValueItem("sSpFhaCaseNumCurrentLoan", "Test Name 2", (loan, app) => loan.sSpFhaCaseNumCurrentLoan));

            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            PreRunFailureCondition condition1 = new PreRunFailureCondition(this.Condition1);
            PreRunFailureCondition condition2 = new PreRunFailureCondition(() => 
            {
                if (this.DataLoan.sIsSystemBatchMigration)
                {
                    return new List<string>()
                    {
                        "Cannot run this migration as part of a system operation. Please manually migrate."
                    };
                }
                else
                {
                    return null;
                }
            });

            conditions.Add(condition1);
            conditions.Add(condition2);

            return conditions;
        }

        /// <summary>
        /// The first condition to run for this migration.
        /// </summary>
        /// <returns>Returns a list of strings containing the error message if the failure condition is triggered. Null otherwise.</returns>
        private List<string> Condition1()
        {
            List<string> errors = new List<string>();

            bool test1 = this.DataLoan.sFHAPreviousCaseNum != this.DataLoan.sSpFhaCaseNumCurrentLoan;
            bool test2 = !string.IsNullOrEmpty(this.DataLoan.sFHAPreviousCaseNum) && string.IsNullOrEmpty(this.DataLoan.sSpFhaCaseNumCurrentLoan);
            bool test3 = string.IsNullOrEmpty(this.DataLoan.sFHAPreviousCaseNum) && !string.IsNullOrEmpty(this.DataLoan.sSpFhaCaseNumCurrentLoan);

            if (!(test2 || test3) && test1)
            {
                errors.Add("Conflicting values for \"Case number for previous case\" on FHA Connection page and \"FHA Case Number for Current Loan\" on Subject Property Details page. Please remove the incorrect value.");
                return errors;
            }

            return null;
        }
    }
}