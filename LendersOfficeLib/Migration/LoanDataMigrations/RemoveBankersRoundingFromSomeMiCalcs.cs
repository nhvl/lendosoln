﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// SMF migration to remove bankers rounding from sProMInsBaseMonthlyPremium, sLenderUfmip, and sProMIns2.
    /// </summary>
    public class RemoveBankersRoundingFromSomeMiCalcs : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoveBankersRoundingFromSomeMiCalcs"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public RemoveBankersRoundingFromSomeMiCalcs(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>The loan version for this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V20_RemoveBankersRoundingFromSomeMiCalcs;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Get rid of bankers rounding for initial and renewal monthly MIP, and LPMI";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sProMInsBaseMonthlyPremium),
                        "Initial monthly MIP",
                        (loan, app) => loan.sProMInsBaseMonthlyPremium_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sLenderUfmip),
                        "LPMI",
                        (loan, app) => loan.sLenderUfmip_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sProMIns2),
                        "Renewal monthly MIP",
                        (loan, app) => loan.sProMIns2_rep)
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        /// <value>The failure conditions for the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }
            else if (this.DataLoan.sLoanFileT == E_sLoanFileT.Loan && !this.DataLoan.sIsSystemBatchMigration)
            {
                return null;
            }
            else if (this.DataLoan.sLoanFileT != E_sLoanFileT.Loan)
            {
                return null;
            }

            return new List<string>()
            {
                "Loan files must be manually reviewed in order to complete this migration."
            };
        }
    }
}
