﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Prevents invalid SSN and EIN values from being saved to a loan file.
    /// </summary>
    public class RequireValidApplicantTinFormats : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequireValidApplicantTinFormats"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public RequireValidApplicantTinFormats(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V27_RequireValidApplicantTinFormats;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        public override string Name => "Require Valid Formats for Applicant TIN Fields";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sHmdaCoApplicantSourceSsn),
                        "HMDA Co-Applicant Source SSN",
                        (loan, app) => loan.sHmdaCoApplicantSourceSsn),
                    new FieldValueItem(
                        nameof(CPageBase.sCombinedBorSsn),
                        "FHA Combined Borrower SSN",
                        (loan, app) => loan.sCombinedBorSsn),
                    new FieldValueItem(
                        nameof(CPageBase.sCombinedCoborSsn),
                        "FHA Combined Co-Borrower SSN",
                        (loan, app) => loan.sCombinedCoborSsn),
                    new FieldValueItem(
                        nameof(CPageBase.sTax1098PayerSsn),
                        "Tax Form 1098 Payer/Borrower SSN",
                        (loan, app) => loan.sTax1098PayerSsn),
                };
            }
        }

        /// <summary>
        /// Gets the application fields to display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CAppBase.aBSsn),
                        "Borrower Social Security Number",
                        (loan, app) => app.aBSsn),
                    new FieldValueItem(
                        nameof(CAppBase.aCSsn),
                        "Co-Borrower Social Security Number",
                        (loan, app) => app.aCSsn),
                    new FieldValueItem(
                        nameof(CAppBase.aB4506TSsnTinEin),
                        "Borrower 4506-T SSN, TIN, or EIN",
                        (loan, app) => app.aB4506TSsnTinEin),
                    new FieldValueItem(
                        nameof(CAppBase.aC4506TSsnTinEin),
                        "Co-Borrower 4506-T SSN, TIN, or EIN",
                        (loan, app) => app.aC4506TSsnTinEin),
                    new FieldValueItem(
                        nameof(CAppBase.aVaServ1Ssn),
                        "VA Veteran Status Request SSN #1",
                        (loan, app) => app.aVaServ1Ssn),
                    new FieldValueItem(
                        nameof(CAppBase.aVaServ2Ssn),
                        "VA Veteran Status Request SSN #2",
                        (loan, app) => app.aVaServ2Ssn),
                    new FieldValueItem(
                        nameof(CAppBase.aVaServ3Ssn),
                        "VA Veteran Status Request SSN #3",
                        (loan, app) => app.aVaServ3Ssn),
                    new FieldValueItem(
                        nameof(CAppBase.aVaServ4Ssn),
                        "VA Veteran Status Request SSN #4",
                        (loan, app) => app.aVaServ4Ssn),
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = this.GetMigrationErrors();
            return !errors.Any();
        }

        /// <summary>
        /// Gets the list of migration errors, if any.
        /// </summary>
        /// <returns>
        /// The list of errors.
        /// </returns>
        private List<string> GetMigrationErrors()
        {
            var errors = new List<string>();

            foreach (var app in this.DataLoan.Apps)
            {
                this.VerifyApplicationFields(errors, app);
            }

            this.VerifyLoanFields(errors);

            return errors;
        }

        /// <summary>
        /// Checks the formatting of the application-level SSN and EIN fields.
        /// </summary>
        /// <param name="errors">
        /// The list of errors for the migration.
        /// </param>
        /// <param name="app">
        /// The application to check.
        /// </param>
        private void VerifyApplicationFields(List<string> errors, CAppData app)
        {
            if (!string.IsNullOrEmpty(app.aBSsn) && !SocialSecurityNumber.Create(app.aBSsn).HasValue)
            {
                errors.Add($"The value for the borrower's Social Security Number on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aCSsn) && !SocialSecurityNumber.Create(app.aCSsn).HasValue)
            {
                errors.Add($"The value for the co-borrower's Social Security Number on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aB4506TSsnTinEin) &&
                !string.Equals(app.aBSsn, app.aB4506TSsnTinEin, StringComparison.Ordinal) &&
                !SocialSecurityNumber.Create(app.aB4506TSsnTinEin).HasValue &&
                !EmployerTaxIdentificationNumber.Create(app.aB4506TSsnTinEin).HasValue)
            {
                errors.Add($"The value for the borrower's 4506-T SSN, TIN, or EIN on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aC4506TSsnTinEin) &&
                !string.Equals(app.aCSsn, app.aC4506TSsnTinEin, StringComparison.Ordinal) &&
                !SocialSecurityNumber.Create(app.aC4506TSsnTinEin).HasValue &&
                !EmployerTaxIdentificationNumber.Create(app.aC4506TSsnTinEin).HasValue)
            {
                errors.Add($"The value for the co-borrower's 4506-T SSN, TIN, or EIN on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aVaServ1Ssn) && !SocialSecurityNumber.Create(app.aVaServ1Ssn).HasValue)
            {
                errors.Add($"The value for VA Veteran Status Request SSN #1 on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aVaServ2Ssn) && !SocialSecurityNumber.Create(app.aVaServ2Ssn).HasValue)
            {
                errors.Add($"The value for VA Veteran Status Request SSN #2 on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aVaServ3Ssn) && !SocialSecurityNumber.Create(app.aVaServ3Ssn).HasValue)
            {
                errors.Add($"The value for VA Veteran Status Request SSN #3 on application {app.aBLastFirstNm} is in an incorrect format.");
            }

            if (!string.IsNullOrEmpty(app.aVaServ4Ssn) && !SocialSecurityNumber.Create(app.aVaServ4Ssn).HasValue)
            {
                errors.Add($"The value for VA Veteran Status Request SSN #4 on application {app.aBLastFirstNm} is in an incorrect format.");
            }
        }

        /// <summary>
        /// Checks the formatting of the loan-level SSN and EIN fields.
        /// </summary>
        /// <param name="errors">
        /// The list of errors for the migration.
        /// </param>
        private void VerifyLoanFields(List<string> errors)
        {
            if (this.DataLoan.sHmdaCoApplicantSourceSsnLckd &&
                !string.IsNullOrEmpty(this.DataLoan.sHmdaCoApplicantSourceSsn) &&
                !SocialSecurityNumber.Create(this.DataLoan.sHmdaCoApplicantSourceSsn).HasValue)
            {
                errors.Add($"The value for the loan's HMDA co-applicant source is in an incorrect format.");
            }

            if (this.DataLoan.sCombinedBorInfoLckd &&
                !string.IsNullOrEmpty(this.DataLoan.sCombinedBorSsn) &&
                !SocialSecurityNumber.Create(this.DataLoan.sCombinedBorSsn).HasValue)
            {
                errors.Add($"The value for the borrower's FHA Combined Social Security Number is in an incorrect format.");
            }

            if (this.DataLoan.sCombinedBorInfoLckd &&
                !string.IsNullOrEmpty(this.DataLoan.sCombinedCoborSsn) &&
                !SocialSecurityNumber.Create(this.DataLoan.sCombinedCoborSsn).HasValue)
            {
                errors.Add($"The value for the co-borrower's FHA Combined Social Security Number is in an incorrect format.");
            }

            if (this.DataLoan.sTax1098PayerSsnLckd &&
                !string.IsNullOrEmpty(this.DataLoan.sTax1098PayerSsn) &&
                !SocialSecurityNumber.Create(this.DataLoan.sTax1098PayerSsn).HasValue)
            {
                errors.Add($"The value for the Tax Form 1098 Payer/Borrower Social Security Number is in an incorrect format.");
            }
        }
    }
}
