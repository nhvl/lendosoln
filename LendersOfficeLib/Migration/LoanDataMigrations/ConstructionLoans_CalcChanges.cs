﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Core.Construction;

    /// <summary>
    /// This migration gates off calculation changes made to existing loan file fields
    /// as part of our Construction Loan overhaul.
    /// </summary>
    public class ConstructionLoans_CalcChanges : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionLoans_CalcChanges"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">A value indicating whether this migration is being run as a system migration.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public ConstructionLoans_CalcChanges(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the version number associated with this migration.
        /// </summary>
        /// <value>The version number associated with this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V32_ConstructionLoans_CalcChanges;
            }
        }

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name
        {
            get
            {
                return "Update Calculations for Construction Loans";
            }
        }

        /// <summary>
        /// Gets the app fields for display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    // OPM 473877 - Change Per Diem Interest Calculation
                    new FieldValueItem(
                        nameof(CPageData.sIPiaDy),
                        "Per Diem Interest",
                        (loan, app) => loan.sIPiaDy_rep),

                    // OPM 471784 - Calculate sLotAcqYr from sLotAcquiredD.
                    new FieldValueItem(
                        nameof(CPageData.sLotAcqYr),
                        "Lot Acquired Yr",
                        (loan, app) => loan.sLotAcqYr),

                    new FieldValueItem(
                        nameof(CPageData.sLotAcquiredD),
                        "Lot Acquired Date",
                        (loan, app) => loan.sLotAcquiredD_rep),

                    // OPM 471792 - Calculate sLotLien from Liabilities.
                    new FieldValueItem(
                        nameof(CPageData.sLotLien),
                        "Existing Liens",
                        (loan, app) => loan.sLotLien_rep),

                    new FieldValueItem(
                        nameof(CAppData.aLiaCollection),
                        "Subject Property Liens",
                        (loan, app) => loan.m_convertLos.ToMoneyString(loan.CalculateLotLien(), FormatDirection.ToRep)),

                    // OPM 473936 - Update First Payment Date for Construction to Permanent Loans
                    new FieldValueItem(
                        nameof(CPageData.sSchedDueD1),
                        "First Payment Date",
                        (loan, app) => loan.sSchedDueD1_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionPeriodEndD),
                        "Construction Period End Date",
                        (loan, app) => loan.sConstructionPeriodEndD_rep),

                    // OPM 474171 - Consolidated small migration changes.
                    new FieldValueItem(
                        nameof(CPageData.sLotWImprovTot),
                        "Lot Value with Improvements Total",
                        (loan, app) => loan.sLotWImprovTot_rep),

                    new FieldValueItem(
                        nameof(CPageData.sLotVal),
                        "Lot Value",
                        (loan, app) => loan.sLotVal_rep),

                    // OPM 474171 - Purchase Price and related changes.
                    new FieldValueItem(
                        nameof(CPageData.sLandCost),
                        "Land Cost",
                        (loan, app) => loan.sLandCost_rep),

                    new FieldValueItem(
                        nameof(CPageData.sPurchPrice),
                        "Purchase Price",
                        (loan, app) => loan.sPurchPrice_rep),

                    new FieldValueItem(
                        nameof(CPageData.sTRIDCashToCloseTotalPaidOnBehalfOfBorrower),
                        "Cash to Close Total Paid on Behalf of Borrowers",
                        (loan, app) => loan.sTRIDCashToCloseTotalPaidOnBehalfOfBorrower_rep),

                    new FieldValueItem(
                        nameof(CPageData.sTRIDTotalDueFromBorrowerAtClosing),
                        "Total Due from Borrower at Closing",
                        (loan, app) => loan.sTRIDTotalDueFromBorrowerAtClosing_rep),

                    new FieldValueItem(
                        nameof(CPageData.sTotTransC),
                        "Total Costs",
                        (loan, app) => loan.sTotTransC_rep),

                    // Other Misc. Fields.
                    new FieldValueItem(
                        nameof(CPageData.sLoanTransactionInvolvesSeller),
                        "Loan Transaction Involves Seller",
                        (loan, app) => loan.sLoanTransactionInvolvesSeller.ToString()),

                    // Construction Fields.
                    new FieldValueItem(
                        nameof(CPageData.sConstructionPurposeT),
                        "Construction Purpose",
                        (loan, app) => loan.sConstructionPurposeT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionToPermanentClosingT),
                        "Construction To Permanent Closing",
                        (loan, app) => loan.sConstructionToPermanentClosingT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionDiscT),
                        "Construction Disclosure Type",
                        (loan, app) => loan.sConstructionDiscT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionAmortT),
                        "Construction Amortization Type",
                        (loan, app) => loan.sConstructionAmortT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdj1stCapR),
                        "Construction 1st Adj Cap",
                        (loan, app) => loan.sConstructionRAdj1stCapR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdj1stCapMon),
                        "Construction 1st Change (mths)",
                        (loan, app) => loan.sConstructionRAdj1stCapMon_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjCapR),
                        "Construction Adj Cap",
                        (loan, app) => loan.sConstructionRAdjCapR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjCapMon),
                        "Construction Adj Period (mths)",
                        (loan, app) => loan.sConstructionRAdjCapMon_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjLifeCapR),
                        "Construction Life Adj Cap",
                        (loan, app) => loan.sConstructionRAdjLifeCapR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjMarginR),
                        "Construction Margin",
                        (loan, app) => loan.sConstructionRAdjMarginR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionArmIndexNameVstr),
                        "Construction Index Name",
                        (loan, app) => loan.sConstructionArmIndexNameVstr),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjIndexR),
                        "Construction Index",
                        (loan, app) => loan.sConstructionRAdjIndexR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjFloorCalcT),
                        "Construction Rate Floor Calc Type",
                        (loan, app) => loan.sConstructionRAdjFloorCalcT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjFloorAddR),
                        "Construction Rate Floor Add Rate",
                        (loan, app) => loan.sConstructionRAdjFloorAddR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjFloorR),
                        "Construction Rate Floor",
                        (loan, app) => loan.sConstructionRAdjFloorR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjRoundT),
                        "Construction Round Type",
                        (loan, app) => loan.sConstructionRAdjRoundT.ToString()),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionRAdjRoundToR),
                        "Construction Round",
                        (loan, app) => loan.sConstructionRAdjRoundToR_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionPeriodMon),
                        "Construction Period (mths)",
                        (loan, app) => loan.sConstructionPeriodMon_rep),

                    new FieldValueItem(
                        nameof(CPageData.sConstructionPeriodIR),
                        "Construction Period Interest Rate",
                        (loan, app) => loan.sConstructionPeriodIR_rep),
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether only the new values should always be shown in the UI,
        /// even if the migration does not pass it's failure conditions.
        /// </summary>
        /// <remarks>
        /// Only set this to true if you are sure running PerformMigration will NOT throw.
        /// </remarks>
        protected override bool AlwaysShowNewValues
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();

            // Check values are equal before and after migration.
            Dictionary<string, object> oldValues = new Dictionary<string, object>();

            // For all loans.
            var fieldInfo = new List<Tuple<string, Func<object>, Func<string>>>();
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Per Diem Interest", () => this.DataLoan.sIPiaDy, () => this.DataLoan.sIPiaDy_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("First Payment Date", () => this.DataLoan.sSchedDueD1, () => this.DataLoan.sSchedDueD1_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Purchase Price", () => this.DataLoan.sPurchPrice, () => this.DataLoan.sPurchPrice_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Cash to Close Total Paid on Behalf of Borrowers", () => this.DataLoan.sTRIDCashToCloseTotalPaidOnBehalfOfBorrower, () => this.DataLoan.sTRIDCashToCloseTotalPaidOnBehalfOfBorrower_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Total Due From Borrower At Closing", () => this.DataLoan.sTRIDTotalDueFromBorrowerAtClosing, () => this.DataLoan.sTRIDTotalDueFromBorrowerAtClosing_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Total Costs", () => this.DataLoan.sTotTransC, () => this.DataLoan.sTotTransC_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Refi (incl. debts to be paid off)", () => this.DataLoan.sRefPdOffAmt1003, () => this.DataLoan.sRefPdOffAmt1003_rep));
            fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Alterations, improvements, repairs", () => this.DataLoan.sAltCost, () => this.DataLoan.sAltCost_rep));

            // For all construction loans.
            if (this.DataLoan.sIsConstructionLoan)
            {
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Lot Value with Improvements Total", () => this.DataLoan.sLotWImprovTot, () => this.DataLoan.sLotWImprovTot_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Lot Acquired Yr", () => this.DataLoan.sLotAcqYr, () => this.DataLoan.sLotAcqYr));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Existing Liens", () => this.DataLoan.sLotLien, () => this.DataLoan.sLotLien_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Period End Date", () => this.DataLoan.sConstructionPeriodEndD, () => this.DataLoan.sConstructionPeriodEndD_rep));
            }

            // For construction only loans.
            if (this.DataLoan.sIsConstructionLoan && this.DataLoan.sLPurposeT == E_sLPurposeT.Construct)
            {
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction To Permanent Closing", () => this.DataLoan.sConstructionToPermanentClosingT, () => this.DataLoan.sConstructionToPermanentClosingT.ToString()));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Disclosure Type", () => this.DataLoan.sConstructionDiscT, () => this.DataLoan.sConstructionDiscT.ToString()));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Amortization Type", () => this.DataLoan.sConstructionAmortT, () => this.DataLoan.sConstructionAmortT.ToString()));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction 1st Adj Cap", () => this.DataLoan.sConstructionRAdj1stCapR, () => this.DataLoan.sConstructionRAdj1stCapR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction 1st Change (mths)", () => this.DataLoan.sConstructionRAdj1stCapMon, () => this.DataLoan.sConstructionRAdj1stCapMon_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Adj Cap)", () => this.DataLoan.sConstructionRAdjCapR, () => this.DataLoan.sConstructionRAdjCapR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Adj Period (mths)", () => this.DataLoan.sConstructionRAdjCapMon, () => this.DataLoan.sConstructionRAdjCapMon_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Life Adj Cap", () => this.DataLoan.sConstructionRAdjLifeCapR, () => this.DataLoan.sConstructionRAdjLifeCapR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Margin", () => this.DataLoan.sConstructionRAdjMarginR, () => this.DataLoan.sConstructionRAdjMarginR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Index Name", () => this.DataLoan.sConstructionArmIndexNameVstr, () => this.DataLoan.sConstructionArmIndexNameVstr));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Index", () => this.DataLoan.sConstructionRAdjIndexR, () => this.DataLoan.sConstructionRAdjIndexR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Rate Floor Calc Type", () => this.DataLoan.sConstructionRAdjFloorCalcT, () => this.DataLoan.sConstructionRAdjFloorCalcT.ToString()));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Rate Floor Add Rate", () => this.DataLoan.sConstructionRAdjFloorAddR, () => this.DataLoan.sConstructionRAdjFloorAddR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Rate Floor", () => this.DataLoan.sConstructionRAdjFloorR, () => this.DataLoan.sConstructionRAdjFloorR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Round Type", () => this.DataLoan.sConstructionRAdjRoundT, () => this.DataLoan.sConstructionRAdjRoundT.ToString()));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Round", () => this.DataLoan.sConstructionRAdjRoundToR, () => this.DataLoan.sConstructionRAdjRoundToR_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Period (mths)", () => this.DataLoan.sConstructionPeriodMon, () => this.DataLoan.sConstructionPeriodMon_rep));
                fieldInfo.Add(new Tuple<string, Func<object>, Func<string>>("Construction Period Interest Rate", () => this.DataLoan.sConstructionPeriodIR, () => this.DataLoan.sConstructionPeriodIR_rep));
            }

            foreach (var tuple in fieldInfo)
            {
                var oldVal = tuple.Item2();
                var oldVal_rep = tuple.Item3();

                this.DataLoan.sLoanVersionT = LoanVersionT.V32_ConstructionLoans_CalcChanges;

                var newVal = tuple.Item2();
                var newVal_rep = tuple.Item3();

                if (!oldVal.Equals(newVal))
                {
                    errors.Add($"\"{tuple.Item1}\" value changes after migration. Before migration = [{oldVal_rep}]. After migration = [{newVal_rep}].");
                }

                this.DataLoan.sLoanVersionT = LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints; // restore old value.
            }

            if (errors.Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }

            if (this.DataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                return null;
            }

            if (!this.DataLoan.sIsConstructionLoan)
            {
                return null;
            }

            List<string> migrationErrors = new List<string>();
            migrationErrors.AddRange(this.GetPerDiemInterestMigrationErrors());
            migrationErrors.AddRange(this.GetLotAcquiredYearMigrationErrors());
            migrationErrors.AddRange(this.GetLotLienMigrationErrors());
            migrationErrors.AddRange(this.GetFirstPaymentDateMigrationErrors());
            migrationErrors.AddRange(this.GetLotWImprovTotMigrationErrors());
            migrationErrors.AddRange(this.GetConstructionPeriodMonMigrationErrors());
            migrationErrors.AddRange(this.GetConstructionPeriodIRMigrationErrors());
            migrationErrors.AddRange(this.GetPurchPriceMigrationErrors());

            return migrationErrors;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetPerDiemInterestMigrationErrors()
        {
            if (this.DataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm)
            {
                return new List<string>();
            }

            if (this.DataLoan.sIPiaDyLckd)
            {
                return new List<string>();
            }

            if (this.DataLoan.sConsummationD.CompareTo(this.DataLoan.sConstructionPeriodEndD, false) == 0)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The \"Per-diem Interest Start Date\" must equal \"Construction Period End Date\", or the \"Per-diem Interest\" must be locked."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetLotAcquiredYearMigrationErrors()
        {
            if (string.IsNullOrWhiteSpace(this.DataLoan.sLotAcqYr))
            {
                return new List<string>();
            }

            if (this.DataLoan.sLotAcquiredD.IsValid && string.Equals(this.DataLoan.sLotAcquiredD.DateTimeForComputation.Year.ToString(), this.DataLoan.sLotAcqYr))
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The year in the \"Lot Acquired Date\" must match the year specified in \"Lot Acquired Yr\"."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetLotLienMigrationErrors()
        {
            if (!this.DataLoan.sIsSystemBatchMigration && this.DataLoan.sLotLien == 0M)
            {
                return new List<string>();
            }

            if (this.DataLoan.sLotLien == this.DataLoan.CalculateLotLien())
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The Existing Liens amount must equal the sum of all liens associated with the subject property."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetFirstPaymentDateMigrationErrors()
        {
            if (this.DataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm)
            {
                return new List<string>();
            }

            if (this.DataLoan.sSchedDueD1Lckd)
            {
                return new List<string>();
            }

            if (this.DataLoan.sEstCloseD.CompareTo(this.DataLoan.sConstructionPeriodEndD, false) == 0)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The \"Estimate Closing Date\" must equal \"Construction Period End Date\", or the \"First Payment Date\" must be locked."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetLotWImprovTotMigrationErrors()
        {
            if (this.DataLoan.sLotWImprovTot == Tools.SumMoney(new decimal[] { this.DataLoan.sLotImprovC, this.DataLoan.sPresentValOfLot }))
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "The \"Lot Value with Improvements Total\" amount will change after migration unless Construction Purpose is changed from \"Construction And Lot Purchase\" or \"Land Cost\" is set equal to \"Lot Value\"."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetConstructionPeriodMonMigrationErrors()
        {
            if (this.DataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                return new List<string>();
            }

            if (this.DataLoan.sConstructionPeriodMon == this.DataLoan.sTerm)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "For non-Construction Perm loans \"Construction Period (mths)\" should match \"Term\"."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetConstructionPeriodIRMigrationErrors()
        {
            if (this.DataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                return new List<string>();
            }

            if (this.DataLoan.sConstructionPeriodIR == this.DataLoan.sNoteIR)
            {
                return new List<string>();
            }

            return new List<string>()
            {
                "For non-Construction Perm loans \"Construction Period Interest Rate\" must match \"Note Rate\"."
            };
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetPurchPriceMigrationErrors()
        {
            List<string> errors = new List<string>();

            // sPurchPrice
            if (this.DataLoan.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase
                && this.DataLoan.sPurchPrice != Tools.SumMoney(new decimal[] { this.DataLoan.sLandCost, this.DataLoan.sLotImprovC }))
            {
                errors.Add("For Construction loans with Construction Purpose set to \"Construction And Lot Purchase\", \"Purchase Price\" must equal \"Land Cost\" + \"Lot Improvement Cost\".");
            }

            if (this.DataLoan.sConstructionPurposeT != ConstructionPurpose.ConstructionAndLotPurchase
                && this.DataLoan.sPurchPrice != 0M)
            {
                errors.Add("For Construction loans with Construction Purpose not set to \"Construction And Lot Purchase\", \"Purchase Price\" must equal $0.00.");
            }

            return errors;
        }
    }
}
