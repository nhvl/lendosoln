﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Migration;

    /// <summary>
    /// Migrations some VA housing expenses to a new calculation.
    /// </summary>
    public class CorrectVaLoanHousingExpCalculation : LoanDataMigration
    {
        /// <summary>
        /// List of loan fields to display.
        /// </summary>
        private IEnumerable<FieldValueItem> loanFieldItems;

        /// <summary>
        /// List of conditions to check.
        /// </summary>
        private IEnumerable<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectVaLoanHousingExpCalculation"/> class.
        /// </summary>
        /// <param name="userName">The user who ran this migiration.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id of the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The loan to perform the migration on.</param>
        public CorrectVaLoanHousingExpCalculation(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.loanFieldItems = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the version the loan will be in after this migration is ran.
        /// </summary>
        /// <value>The version the loan will be in after this migration is ran.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V15_CorrectVaLoanHousingExpCalculation_Opm246907;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Correct VA Loan Non-PI Housing Expense Calculation";
            }
        }

        /// <summary>
        /// Gets the list of loan file fields to display on the migration page.
        /// </summary>
        /// <value>The list of loan file fields to display on the migration page.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanFieldItems;
            }
        }

        /// <summary>
        /// Gets the list of failure conditions for this migration.
        /// </summary>
        /// <value>The list of failure conditions for this migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Performs the migration. Doesn't actually do anything.
        /// </summary>
        /// <param name="errors">Any errors that occured during migration.</param>
        /// <returns>Always returns true since this migration can never fail.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Constructs the loan field list that will be displayed on the page.
        /// </summary>
        /// <returns>The loan field list that will be displayed on the page.</returns>
        private IEnumerable<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> loanFieldGetters;
            if (this.DataLoan == null)
            {
                loanFieldGetters = new List<FieldValueItem>();
            }
            else
            {
                loanFieldGetters = new List<FieldValueItem>()
                {
                    new FieldValueItem("sVaProRealETx", "Realty Taxes", (loan, app) => loan.sVaProRealETx_rep),
                    new FieldValueItem("sTotalRealtyTaxesPITI", "Total Monthly Amount (PITI) for Taxes", (loan, app) => loan.sTotalRealtyTaxesPITI_rep),
                    new FieldValueItem("sVaProHazIns", "Hazard Insurance", (loan, app) => loan.sVaProHazIns_rep),
                    new FieldValueItem("sTotalPropertyInsurancePITI", "Total Monthly Amount (PITI) for Property Insurance", (loan, app) => loan.sTotalPropertyInsurancePITI_rep),
                    new FieldValueItem("sVaMaintainAssessPmt", "Other (HOA, Condo fees, etc.)", (loan, app) => loan.sVaMaintainAssessPmt_rep),
                    new FieldValueItem("sTotalOtherHousingExpensesPITI", "Total Monthly Amount (PITI) for Other Expenses", (loan, app) => loan.sTotalOtherHousingExpensesPITI_rep),
                    new FieldValueItem("sVaProMonthlyPmt", "Total Monthly Shelter Expenses", (loan, app) => loan.sVaProMonthlyPmt_rep)
                };
            }

            return loanFieldGetters;
        }

        /// <summary>
        /// Constructs the condition list that will be used to check if the migration is possible.
        /// </summary>
        /// <returns>The condition list.</returns>
        private IEnumerable<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions;
            if (this.DataLoan == null)
            {
                conditions = new List<PreRunFailureCondition>();
            }
            else
            {
                conditions = new List<PreRunFailureCondition>()
                {
                    new PreRunFailureCondition(this.GetFailureReason)
                };
            }

            return conditions;
        }

        /// <summary>
        /// Checks the loan for failure conditions.
        /// </summary>
        /// <returns>The failure message if this loan cannot be migrated.</returns>
        private List<string> GetFailureReason()
        {
            /* Valid Migrations:
                If sLoanFileT =! Loan, then PASS.
                Else, if sLT =! VA, then PASS.
                Else, if ( sVaProRealETx == sTotalRealtyTaxesPITI ) && ( sVaProHazIns == sTotalPropertyInsurancePITI ) && ( sVaMaintainAssessPmt == sTotalOtherHousingExpensesPITI ), then PASS
                Else, if ( sIsSystemBatchMigration == false ), then PASS.
            */

            if (!this.DataLoan.sIsSystemBatchMigration ||
                this.DataLoan.sLoanFileT != E_sLoanFileT.Loan ||
                this.DataLoan.sLT != E_sLT.VA)
            {
                return null;
            }

            if ((this.DataLoan.sVaProRealETxLckd || this.DataLoan.sVaProRealETx == this.DataLoan.sTotalRealtyTaxesPITI) &&
                (this.DataLoan.sVaProHazInsLckd || this.DataLoan.sVaProHazIns == this.DataLoan.sTotalPropertyInsurancePITI) &&
                (this.DataLoan.sVaMaintainAssessPmtLckd || this.DataLoan.sVaMaintainAssessPmt == this.DataLoan.sTotalOtherHousingExpensesPITI))
            {
                return null;
            }

            return new List<string>() { "This file requires manual review in order to complete the migration." };
        }
    }
}
