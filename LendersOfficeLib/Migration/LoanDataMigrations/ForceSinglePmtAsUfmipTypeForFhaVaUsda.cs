﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Introduces a calculation to sMipFrequency.
    /// Forces sMipFrequency to 'Single Payment' if the loan type is FHA, VA, or USDA.
    /// </summary>
    public class ForceSinglePmtAsUfmipTypeForFhaVaUsda : LoanDataMigration
    {
        /// <summary>
        /// The loan file fields to display.
        /// </summary>
        private List<FieldValueItem> loanFieldItems;

        /// <summary>
        /// The failure conditions to run.
        /// </summary>
        private List<PreRunFailureCondition> failureConditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForceSinglePmtAsUfmipTypeForFhaVaUsda"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerid">The broker id.</param>
        /// <param name="isSystem">Whether this is a system migration.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public ForceSinglePmtAsUfmipTypeForFhaVaUsda(string userName, Guid userId, Guid brokerid, bool isSystem, CPageData dataLoan)
            : base(userName, userId, brokerid, isSystem, dataLoan)
        {
            this.loanFieldItems = this.InitializeLoanFieldItems();
            this.failureConditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the version the loan will be in after this migration.
        /// </summary>
        /// <value>The version the loan will be in after this migraion.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V4_ForceSinglePmtUfmipTypeForFhaVaUsda_Opm237060;
            }
        }

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name
        {
            get
            {
                return "Force \"Single Payment\" as the UFMIP/FF Type Paid At Closing for FHA/VA/USDA Loans";
            }
        }

        /// <summary>
        /// Gets the loan file fields to display.
        /// </summary>
        /// <value>The loan file fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanFieldItems;
            }
        }

        /// <summary>
        /// Gets the failure conditions to run.
        /// </summary>
        /// <value>The failure conditions to run.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.failureConditions;
            }
        }

        /// <summary>
        /// Performs the migration. This only needs to enable a calculation so nothing needs to happen here.
        /// </summary>
        /// <param name="errors">The errors to return.</param>
        /// <returns>Always returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Constructs the list of loan file field items to display.
        /// </summary>
        /// <returns>The list of loan file field items to display.</returns>
        private List<FieldValueItem> InitializeLoanFieldItems()
        {
            return new List<FieldValueItem>()
            {
                new FieldValueItem("sLT", "Loan Type", (loan, app) => Tools.ConvertsLtToString(loan.sLT)),
                new FieldValueItem("sMipFrequency", "UFMIP/FF paid at closing type", (loan, app) => loan.sMipFrequency == E_MipFrequency.SinglePayment ? "Single Payment" : "Annual rate for " + loan.sMipPiaMon_rep + " months"),
                new FieldValueItem("sMipPiaMon", "UFMIP/FF months paid at closing", (loan, app) => loan.sMipPiaMon_rep)
            };
        }

        /// <summary>
        /// Constructs the failure conditions to run.
        /// </summary>
        /// <returns>The failure conditions to run.</returns>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            return new List<PreRunFailureCondition>()
            {
                new PreRunFailureCondition(() =>
                {
                    if ((this.DataLoan.sLT == E_sLT.FHA || this.DataLoan.sLT == E_sLT.VA || this.DataLoan.sLT == E_sLT.UsdaRural) &&
                        this.DataLoan.sMipFrequency == E_MipFrequency.AnnualRateXMonths)
                    {
                        var message = "This file is marked as a " + Tools.ConvertsLtToString(this.DataLoan.sLT) + " loan. This loan type requires the upfront MIP/FF to be marked as a \"Single Payment\" instead of marked as" +
                                      " a prepayment at the \"Annual Rate for " + this.DataLoan.sMipPiaMon_rep + " months.\" Please correct this setting on Upfront MIP/FF page in order to proceed with the migration.";

                        return new List<string>() { message };
                    }

                    return null;
                })
            };
        }
    }
}
