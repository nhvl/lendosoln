﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// This migration converts the Level of Property Review data in the 1008 to be 
    /// calculated from the Property Valuation Method and Appraisal Form Type of the
    /// Subject Property Details page.
    /// </summary>
    public class CalculateLevelOfPropertyReviewSection : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> loanFields;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculateLevelOfPropertyReviewSection"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The ID of the user who will run this migration.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public CalculateLevelOfPropertyReviewSection(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.loanFields = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Calculate the Level of Property Review section of the 1008 from the "
            + "Property Valuation Method and Appraisal Form Type of the Subject Property Details page.";

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V12_CalculateLevelOfPropertyReviewSection;

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be displayed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems => this.loanFields;

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions => this.conditions;

        /// <summary>
        /// No processing is needed for this migration so it just returns true.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>Always returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items.</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sApprFull), "Full Appraisal", (loan, app) => loan.sApprFull.ToString()));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sApprDriveBy), "Drive-By Appraisal", (loan, app) => loan.sApprDriveBy.ToString()));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sIsSpReviewNoAppr), "No Appraisal", (loan, app) => loan.sIsSpReviewNoAppr.ToString()));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sSpReviewFormNum), "Property Review Form Number", (loan, app) => loan.sSpReviewFormNum));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sSpValuationMethodT), "Valuation Method", (loan, app) => loan.sSpValuationMethodT.ToString()));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sSpAppraisalFormT), "Appraisal Form Type", (loan, app) => loan.sSpAppraisalFormT.ToString()));

            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.OnlyOneReviewCheckboxSet));
            conditions.Add(new PreRunFailureCondition(this.DataMatchesBetween1008AndSubjectPropertyDetails));

            return conditions;
        }

        /// <summary>
        /// Validates the checkboxes indicating full, drive-by, or no appraisal to ensure that at most one is checked.
        /// </summary>
        /// <returns>Returns a list of strings containing the error message if the failure condition is triggered. Null otherwise.</returns>
        private List<string> OnlyOneReviewCheckboxSet()
        {
            List<string> errors = new List<string>();

            if (!this.DataLoan.IsTemplate)
            {
                bool full = this.DataLoan.sApprFull;
                bool driveBy = this.DataLoan.sApprDriveBy;
                bool none = this.DataLoan.sIsSpReviewNoAppr;

                bool multipleReviewCheckboxesSet = (full && driveBy) || (driveBy && none) || (full && none);

                if (multipleReviewCheckboxesSet)
                {
                    errors.Add("At most one checkbox in the Level of Property Review section of the 1008 page should be checked.");
                    return errors;
                }
            }

            return null;
        }

        /// <summary>
        /// Validates the appraisal types listed on the 1008 and Subject Property Details pages to ensure that they
        /// don't contain conflicting information.
        /// </summary>
        /// <returns>A list of strings containing error messages if the failure condition is triggered. Null otherwise.</returns>
        private List<string> DataMatchesBetween1008AndSubjectPropertyDetails()
        {
            List<string> errors = new List<string>();

            if (!this.DataLoan.IsTemplate)
            {
                bool full = this.DataLoan.sApprFull;
                bool driveBy = this.DataLoan.sApprDriveBy;
                bool none = this.DataLoan.sIsSpReviewNoAppr;

                bool conflictingValuesExist;
                switch (this.DataLoan.sSpValuationMethodT)
                {
                    case E_sSpValuationMethodT.LeaveBlank:
                        conflictingValuesExist = full || driveBy || none;
                        break;
                    case E_sSpValuationMethodT.AutomatedValuationModel:
                    case E_sSpValuationMethodT.DesktopAppraisal:
                    case E_sSpValuationMethodT.None:
                        conflictingValuesExist = full || driveBy;
                        break;
                    case E_sSpValuationMethodT.DriveBy:
                    case E_sSpValuationMethodT.FieldReview:
                        conflictingValuesExist = full || none;
                        break;
                    case E_sSpValuationMethodT.FullAppraisal:
                        conflictingValuesExist = driveBy || none;
                        break;
                    case E_sSpValuationMethodT.Other:
                    case E_sSpValuationMethodT.DeskReview:
                    case E_sSpValuationMethodT.PriorAppraisalUsed:
                        conflictingValuesExist = false;
                        break;
                    default:
                        throw new UnhandledEnumException(this.DataLoan.sSpValuationMethodT);
                }            

                if (conflictingValuesExist)
                {
                    errors.Add("Checkboxes in the Level of Property Review section of the 1008 page must match the value "
                        + "of Property Valuation Method dropdown on the Subject Property Details page.");
                    return errors;
                }
            }

            return null;
        }
    }
}