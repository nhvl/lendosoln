﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Corrects the USDA Guaranty Fee calculation to be based off the actual total loan amount.
    /// </summary>
    public class CorrectUsdaGuaranteeFeeOffTotalLoanAmt : LoanDataMigration
    {
        /// <summary>
        /// The loan items to display.
        /// </summary>
        private List<FieldValueItem> loanItems;

        /// <summary>
        /// The failure conditions to run.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectUsdaGuaranteeFeeOffTotalLoanAmt"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystem">Whether this is a system operation or not.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public CorrectUsdaGuaranteeFeeOffTotalLoanAmt(string userName, Guid userId, Guid brokerId, bool isSystem, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystem, dataLoan)
        {
            this.loanItems = this.InitializeLoanFields();
            this.conditions = this.InitializeFailureConditions();
        }

        /// <summary>
        /// Gets the version that the loan will be in after this migration.
        /// </summary>
        /// <value>The version that the loan will be in after this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V5_CorrectUsdaGuaranteeFeeOffTotalLoanAmt_Opm237060;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Correct USDA Guaranty Fee Calculation to Be Based Off Actual Total Loan Amount";
            }
        }

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanItems;
            }
        }

        /// <summary>
        /// Gets the failure conditions to run.
        /// </summary>
        /// <value>The failure conditions to run.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Performs the migration. We only want to advance the version number to enable a calculation.
        /// </summary>
        /// <param name="errors">The errors to show.</param>
        /// <returns>Always true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Constructs the loan field items to display.
        /// </summary>
        /// <returns>The list of loan field items to display.</returns>
        private List<FieldValueItem> InitializeLoanFields()
        {
            return new List<FieldValueItem>()
            {
                new FieldValueItem("sLT", "Loan Type", (loan, app) => Tools.ConvertsLtToString(loan.sLT)),
                new FieldValueItem("sFfUfMipIsBeingFinanced", "Is UFMIP/FF being financed?", (loan, app) => loan.sFfUfMipIsBeingFinanced.ToString()),
                new FieldValueItem("sFfUfmip1003", "Total UFMIP / FF", (loan, app) => loan.sFfUfmip1003_rep),
                new FieldValueItem("sFfUfmipFinanced", "UFMIP/FF financed amount", (loan, app) => loan.sFfUfmipFinanced_rep),
                new FieldValueItem("sUfCashPd", "Paid in Cash", (loan, app) => loan.sUfCashPd_rep),
                new FieldValueItem("sFinalLAmt", "Total Loan Amount", (loan, app) => loan.sFinalLAmt_rep)
            };
        }

        /// <summary>
        /// Constructs the failure conditions to run.
        /// </summary>
        /// <returns>The list of failure conditions to run.</returns>
        private List<PreRunFailureCondition> InitializeFailureConditions()
        {
            return new List<PreRunFailureCondition>()
            {
                new PreRunFailureCondition(() =>
                {
                    if (this.DataLoan.sIsSystemBatchMigration && this.DataLoan.sFfUfMipIsBeingFinanced &&
                       this.DataLoan.sLT == E_sLT.UsdaRural && this.DataLoan.sUfCashPd > 0)
                    {
                        return new List<string>() { "Migration cannot be run as a system batch migration" };
                    }

                    return null;
                })
            };
        }
    }
}
