﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Populates application-level credit model names from the credit report
    /// on file for the application.
    /// </summary>
    public class PopulateApplicationLevelCreditModelNames : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateApplicationLevelCreditModelNames"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public PopulateApplicationLevelCreditModelNames(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>
        /// The loan version for this migration.
        /// </value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V19_PopulateApplicationLevelCreditModelNames;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>
        /// The name of the migration.
        /// </value>
        public override string Name => "Populate Application Credit Model Names from Credit Report";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>
        /// The loan fields to display.
        /// </value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageData.sExperianModelName),
                        "Experian Model Name",
                        (loan, app) => loan.sExperianModelName),
                    new FieldValueItem(
                        nameof(CPageData.sTransUnionModelName),
                        "TransUnion Model Name",
                        (loan, app) => loan.sTransUnionModelName),
                    new FieldValueItem(
                        nameof(CPageData.sEquifaxModelName),
                        "Equifax Model Name",
                        (loan, app) => loan.sEquifaxModelName)
                };
            }
        }

        /// <summary>
        /// Gets the application fields to display.
        /// </summary>
        /// <value>
        /// The application fields to display.
        /// </value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CAppData.aBExperianModelName),
                        "Borrower Experian Model Name",
                        (loan, app) => app.aBExperianModelName),
                    new FieldValueItem(
                        nameof(CAppData.aBTransUnionModelName),
                        "Borrower TransUnion Model Name",
                        (loan, app) => app.aBTransUnionModelName),
                    new FieldValueItem(
                        nameof(CAppData.aBEquifaxModelName),
                        "Borrower Equifax Model Name",
                        (loan, app) => app.aBEquifaxModelName),

                    new FieldValueItem(
                        nameof(CAppData.aCExperianModelName),
                        "Co-Borrower Experian Model Name",
                        (loan, app) => app.aCExperianModelName),
                    new FieldValueItem(
                        nameof(CAppData.aCTransUnionModelName),
                        "Co-Borrower TransUnion Model Name",
                        (loan, app) => app.aCTransUnionModelName),
                    new FieldValueItem(
                        nameof(CAppData.aCEquifaxModelName),
                        "Co-Borrower Equifax Model Name",
                        (loan, app) => app.aCEquifaxModelName),
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        /// <value>
        /// The failure conditions for the migration.
        /// </value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">
        /// The errors that occurred during the migration.
        /// </param>
        /// <returns>
        /// True if the file was successfully migrated. Otherwise, false.
        /// </returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = this.GetMigrationErrors();

            if (errors.Any())
            {
                return false;
            }

            foreach (var app in this.DataLoan.Apps)
            {
                if (app.aIsCreditReportOnFile)
                {
                    var creditReport = app.CreditReportData.Value;

                    app.aBExperianModelName = creditReport.BorrowerScore.ExperianModelName;
                    app.aBTransUnionModelName = creditReport.BorrowerScore.TransUnionModelName;
                    app.aBEquifaxModelName = creditReport.BorrowerScore.EquifaxModelName;

                    app.aCExperianModelName = creditReport.CoborrowerScore.ExperianModelName;
                    app.aCTransUnionModelName = creditReport.CoborrowerScore.TransUnionModelName;
                    app.aCEquifaxModelName = creditReport.CoborrowerScore.EquifaxModelName;
                }
                else
                {
                    app.aBExperianModelName = this.DataLoan.sExperianModelName;
                    app.aBTransUnionModelName = this.DataLoan.sTransUnionModelName;
                    app.aBEquifaxModelName = this.DataLoan.sEquifaxModelName;

                    app.aCExperianModelName = this.DataLoan.sExperianModelName;
                    app.aCTransUnionModelName = this.DataLoan.sTransUnionModelName;
                    app.aCEquifaxModelName = this.DataLoan.sEquifaxModelName;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>
        /// A list of errors that are preventing the migration from running.
        /// </returns>
        private List<string> GetMigrationErrors()
        {
            var errors = new List<string>();

            foreach (var app in this.DataLoan.Apps.Where(app => app.aIsCreditReportOnFile))
            {
                var creditReport = app.CreditReportData.Value;

                if (!string.IsNullOrWhiteSpace(this.DataLoan.sExperianModelName) &&
                    !string.Equals(creditReport.BorrowerScore.ExperianModelName, this.DataLoan.sExperianModelName, StringComparison.OrdinalIgnoreCase))
                {
                    errors.Add("The Experian model name on file does not match the credit report for application " + app.aBLastFirstNm);
                }

                if (!string.IsNullOrWhiteSpace(this.DataLoan.sTransUnionModelName) &&
                    !string.Equals(creditReport.BorrowerScore.TransUnionModelName, this.DataLoan.sTransUnionModelName, StringComparison.OrdinalIgnoreCase))
                {
                    errors.Add("The TransUnion model name on file does not match the credit report for application " + app.aBLastFirstNm);
                }

                if (!string.IsNullOrWhiteSpace(this.DataLoan.sEquifaxModelName) &&
                    !string.Equals(creditReport.BorrowerScore.EquifaxModelName, this.DataLoan.sEquifaxModelName, StringComparison.OrdinalIgnoreCase))
                {
                    errors.Add("The Equifax model name on file does not match the credit report for application " + app.aBLastFirstNm);
                }
            }

            return errors;
        }
    }
}
