﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// SMF migration to fix FHLMC and TOTAL reserves calculations.
    /// </summary>
    public class CorrectReservesCalculationsForFhlmcAndTotal : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectReservesCalculationsForFhlmcAndTotal"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public CorrectReservesCalculationsForFhlmcAndTotal(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>The loan version for this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V23_CorrectReservesCalcForFhlmcAndTotal;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Correct Reserves Calculations for FHLMC and TOTAL Reserves Fields";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sTotalScoreAssetsAfterClosing),
                        "FHA TOTAL Scorecard Assets After Closing",
                        (loan, app) => loan.sTotalScoreAssetsAfterClosing_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sTotalScoreReserves),
                        "FHA TOTAL Scorecard Reserves Months",
                        (loan, app) => loan.sTotalScoreReserves_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sFredieReservesAmt),
                        "Freddie Reserves",
                        (loan, app) => loan.sFredieReservesAmt_rep)
                };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }
            else if (!this.DataLoan.sIsSystemBatchMigration)
            {
                return null;
            }

            return new List<string>()
            {
                "Migration cannot be run as a system batch migration."
            };
        }
    }
}
