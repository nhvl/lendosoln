﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;

    /// <summary>
    /// Migrator to stop guessing rescindability.
    /// </summary>
    public class StopGuessingRescindability : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> fieldGetters;

        /// <summary>
        /// Initializes a new instance of the <see cref="StopGuessingRescindability"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public StopGuessingRescindability(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan) 
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.fieldGetters = this.InitializeLoanGetters();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Fix dates that depend on rescindability.";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V29_StopGuessingRescindability;
            }
        }        

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be displayed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.fieldGetters;
            }
        }

        /// <summary>
        /// The migration function for this migration. This migration only wants to enable a calculation, so 
        /// we can simply return true and let the framework update the version number.
        /// </summary>
        /// <param name="errors">Any errors. There shouldn't be any.</param>
        /// <returns>Returns true since this migration should always be successful.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items.</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>()
            {
                new FieldValueItem(
                    nameof(CPageBase.sDocMagicCancelD),
                    "Doc magic cancel date.",
                    (loan, app) => loan.sDocMagicCancelD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sRescissionD),
                    "Rescission date.",
                    (loan, app) => loan.sRescissionD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sSettlementD),
                    "Settlement Date",
                    (loan, app) => loan.sSettlementD_rep),
                new FieldValueItem(
                    "sConsummationD",
                    "Consummation Date.",
                    (loan, app) => loan.sConsummationD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sDocMagicDisbursementD),
                    "Doc magic disbursement date.",
                    (loan, app) => loan.sDocMagicDisbursementD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sLoanRescindableT),
                    "Loan rescindable type.",
                    (loan, app) => loan.sLoanRescindableT.ToString()),
            };

            return getters;
        }
    }
}
