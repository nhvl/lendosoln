﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Migration;

    /// <summary>
    /// Migration for Opm 204251. This enables the calculation for sVaFfExemptTri.
    /// </summary>
    public class CalculateVAFfExempt : LoanDataMigration
    {
        /// <summary>
        /// The list of app level fields.
        /// </summary>
        private List<FieldValueItem> appFieldItems;

        /// <summary>
        /// The list of loan level field items.
        /// </summary>
        private List<FieldValueItem> loanFieldItems;

        /// <summary>
        /// The list of conditions to check before running the migration.
        /// </summary>
        private List<PreRunFailureCondition> conditionItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculateVAFfExempt"/> class.
        /// </summary>
        /// <param name="userName">The name of the user.</param>
        /// <param name="userId">The id of the user.</param>
        /// <param name="brokerId">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a migration done by a system operation.</param>
        /// <param name="dataLoan">The data loan to perform the migration on.</param>
        public CalculateVAFfExempt(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.appFieldItems = this.InitializeAppFields();
            this.loanFieldItems = this.InitializeLoanFields();
            this.conditionItems = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the version number for this migration.
        /// </summary>
        /// <value>The version number for this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V2_CalcVaFundFeeExempt_Opm204251;
            }
        }

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name
        {
            get
            {
                return "Calculate VA Funding Fee Exemption";
            }
        }

        /// <summary>
        /// Gets the list of application fields to display.
        /// </summary>
        /// <value>The list of application fields.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return this.appFieldItems;
            }
        }

        /// <summary>
        /// Gets a list of loan fields to display.
        /// </summary>
        /// <value>The list of loan fields to dipslay.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanFieldItems;
            }
        }

        /// <summary>
        /// Gets a list of failure conditions to check before running the migration.
        /// </summary>
        /// <value>The list of failure conditions to check before running the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditionItems;
            }
        }

        /// <summary>
        /// This migration only needs to return true.
        /// </summary>
        /// <param name="errors">Any error messages.</param>
        /// <returns>Returns true. This only updates the version number.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initializes the app level fields for the UI.
        /// </summary>
        /// <returns>A list of app level fields for the UI.</returns>
        private List<FieldValueItem> InitializeAppFields()
        {
            List<FieldValueItem> appFields = new List<FieldValueItem>()
            {
                new FieldValueItem("aIsBVAElig", "Is borrower eligible for VA loan?", (loan, app) => app.aIsBVAElig.ToString()),
                new FieldValueItem("aIsCVAElig", "Is co-borrower eligible for VA loan?", (loan, app) => app.aIsCVAElig.ToString()),
                new FieldValueItem("aIsBVAFFEx", "Is borrower VA funding fee exempt?", (loan, app) => app.aIsBVAFFEx.ToString()),
                new FieldValueItem("aIsCVAFFEx", "Is co-borrower VA funding fee exempt?", (loan, app) => app.aIsCVAFFEx.ToString())
            };

            return appFields;
        }

        /// <summary>
        /// Initializes the loan level fields for the UI.
        /// </summary>
        /// <returns>A list of loan level fields for the UI.</returns>
        private List<FieldValueItem> InitializeLoanFields()
        {
            List<FieldValueItem> loanFields = new List<FieldValueItem>()
            {
                new FieldValueItem(
                    "sVAFfExemptTri",
                    "Is funding fee exempt?",
                    (loan, app) =>
                    {
                        if (loan.sVaFfExemptTri == E_TriState.Yes)
                        {
                            return "True";
                        }
                        else if (loan.sVaFfExemptTri == E_TriState.No)
                        {
                            return "False";
                        }
                        else
                        {
                            return "Blank";
                        }
                    })
            };

            return loanFields;
        }

        /// <summary>
        /// Initializes the conditions for this migration.
        /// </summary>
        /// <returns>The conditions for this migration.</returns>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.VAFfExemptTriTrueCondition));
            conditions.Add(new PreRunFailureCondition(this.VAFfExemptTriFalseCondition));
            return conditions;
        }

        /// <summary>
        /// Checks the conditions for when sVAFfExemptTri is "yes".
        /// </summary>
        /// <returns>A list of error messages for this condition.</returns>
        private List<string> VAFfExemptTriTrueCondition()
        {
            if (this.DataLoan.sVaFfExemptTri != E_TriState.Yes)
            {
                return null;
            }

            string isExemptTrueMessage = "{0} is not marked as VA Funding Fee exempt but the file is marked as VA Funding Fee exempt in the \"VA - Loan Summary\" screen. " +
                                         "Please either mark {0} as VA Funding Fee Exempt on the \"Borrower Info\" screen or change the \"VA - Loan Summary\" screen to reflect funding fee as \"Not Exempt\".";

            List<string> messages = new List<string>();
            for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
            {
                CAppData app = this.DataLoan.GetAppData(appIndex);

                if (app.aIsBVAElig && !app.aIsBVAFFEx)
                {
                    messages.Add(string.Format(isExemptTrueMessage, app.aBNm));
                }

                if (app.aIsCVAElig && !app.aIsCVAFFEx)
                {
                    messages.Add(string.Format(isExemptTrueMessage, app.aCNm));
                }
            }

            return messages;
        }

        /// <summary>
        /// The condition for when sVaFfExemptTri is "no".
        /// </summary>
        /// <returns>List of strings with the error message.</returns>
        private List<string> VAFfExemptTriFalseCondition()
        {
            List<string> errorMessages = new List<string>();

            if (this.DataLoan.sVaFfExemptTri != E_TriState.No)
            {
                return null;
            }

            for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
            {
                CAppData app = this.DataLoan.GetAppData(appIndex);

                if (app.aIsBVAElig && !app.aIsBVAFFEx)
                {
                    return null;
                }

                if (app.aIsCVAElig && !app.aIsCVAFFEx)
                {
                    return null;
                }
            }

            errorMessages.Add("All VA eligible borrowers on file are marked as VA Funding Fee exempt but the file is marked as VA Funding Fee \"Non-Exempt\" in the \"VA - Loan Summary\" screen. " +
                              "Please update the \"VA - Loan Summary\" screen to reflect VA Funding Fee \"Exempt\", or indicate which borrower/s are not exempt in the \"PML 2.0 Application Tab\".");

            return errorMessages;
        }
    }
}
