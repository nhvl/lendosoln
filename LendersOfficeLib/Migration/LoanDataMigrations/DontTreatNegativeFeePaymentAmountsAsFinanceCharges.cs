﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// SMF migration to stop treating negative fee payment amounts as finance charges.
    /// </summary>
    public class DontTreatNegativeFeePaymentAmountsAsFinanceCharges : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DontTreatNegativeFeePaymentAmountsAsFinanceCharges"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public DontTreatNegativeFeePaymentAmountsAsFinanceCharges(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>The loan version for this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V21_DontTreatNegativeFeePaymentAmountsAsFinanceCharges;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Do not treat negative fee payment amounts as finance charges.";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sAprIncludedCc),
                        "APR Related Closing Costs",
                        (loan, app) => loan.sAprIncludedCc_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sFinancedAmt),
                        "Amount Financed",
                        (loan, app) => loan.sFinancedAmt_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sFinCharge),
                        "Finance Charge",
                        (loan, app) => loan.sFinCharge_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sApr),
                        "APR",
                        (loan, app) => loan.sApr_rep),
                    new FieldValueItem(
                        "sQMMaxPointAndFeesAllowedAmt",
                        "Allowable QM Point and Fees",
                        (loan, app) => loan.sQMMaxPointAndFeesAllowedAmt_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sQMStatusT),
                        "Qualified Mortgage Status",
                        (loan, app) => CPageBase.sQMStatusT_map_rep(loan.sQMStatusT)),
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        /// <value>The failure conditions for the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }
            else if (!this.DataLoan.sIsSystemBatchMigration)
            {
                return null;
            }

            return new List<string>()
            {
                "Migration cannot be run as a system batch migration."
            };
        }
    }
}
