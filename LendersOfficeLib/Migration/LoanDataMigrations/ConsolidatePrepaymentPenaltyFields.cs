﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Migration to consolidate sGfeMaxPpmtPenaltyAmt and sQMMaxPrePmntPenalty for case 238278.
    /// </summary>
    public class ConsolidatePrepaymentPenaltyFields : LoanDataMigration
    {
        /// <summary>
        /// The message if the migration condition is not satisfied.
        /// </summary>
        private const string FailureMessage = 
            "The maximum prepayment amount within 36 months entered on the GFE " +
            "conflicts with the one entered on the QM summary. Please resolve " +
            "this discrepancy.";

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsolidatePrepaymentPenaltyFields"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">A value indicating whether this migration is being run as a system migration.</param>
        /// <param name="loan">The loan to migrate.</param>
        public ConsolidatePrepaymentPenaltyFields(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData loan)
            : base(userName, userId, brokerId, isSystemMigration, loan)
        {
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get { return "Consolidate Max Prepayment Penalty Fields"; }
        }

        /// <summary>
        /// Gets the version the loan will be migrated to if the migration is successful.
        /// </summary>
        /// <value>The version the loan will be migrated to if the migration is successful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get { return LoanVersionT.V3_ConsolidateMaxPrepaymentPenaltyFields_Opm238278; }
        }

        /// <summary>
        /// Gets the conditions to check before running the migration.
        /// </summary>
        /// <value>The conditions to check before running the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new List<PreRunFailureCondition>
                {
                    new PreRunFailureCondition(() => this.GetMigrationFailureReasons())
                };
            }
        }

        /// <summary>
        /// Gets the loan fields for display.
        /// </summary>
        /// <value>The loan fields for display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new List<FieldValueItem>
                {
                    new FieldValueItem(
                        nameof(this.DataLoan.sGfeMaxPpmtPenaltyAmt),
                        "Maximum Prepayment Penalty",
                        (loan, app) => loan.sGfeMaxPpmtPenaltyAmt_rep),
                    new FieldValueItem(
                        nameof(this.DataLoan.sQMMaxPrePmntPenalty),
                        "QM Maximum Prepayment Penalty",
                        (loan, app) => loan.sQMMaxPrePmntPenalty_rep),
                };
            }
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="errors">The errors that occurred. Will be set to null.</param>
        /// <returns>Returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;

            if (this.DataLoan.sGfeMaxPpmtPenaltyAmt != 0
                && this.DataLoan.sQMMaxPrePmntPenalty == 0)
            {
                this.DataLoan.sQMMaxPrePmntPenalty = this.DataLoan.sGfeMaxPpmtPenaltyAmt;
            }

            return true;
        }

        /// <summary>
        /// Gets the reasons the migration can't run.
        /// </summary>
        /// <returns>The reasons the migration can't run. Null if the migration can run.</returns>
        private List<string> GetMigrationFailureReasons()
        {
            if (this.DataLoan.sGfeMaxPpmtPenaltyAmt == 0 
                || this.DataLoan.sQMMaxPrePmntPenalty == 0 
                || this.DataLoan.sQMMaxPrePmntPenalty == this.DataLoan.sGfeMaxPpmtPenaltyAmt)
            {
                return null;
            }

            return new List<string> { FailureMessage };
        }
    }
}
