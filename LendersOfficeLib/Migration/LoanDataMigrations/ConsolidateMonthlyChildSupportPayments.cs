﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Consolidates the calculations for monthly child support payments
    /// to use the child support valued entered in the liability collection 
    /// for a loan file.
    /// </summary>
    public class ConsolidateMonthlyChildSupportPayments : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsolidateMonthlyChildSupportPayments"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public ConsolidateMonthlyChildSupportPayments(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        public override string Name => "Consolidate the fields representing monthly child support payments.";

        /// <summary>
        /// Gets the app fields for display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CAppData.aFHAChildSupportPmt),
                        "Child Support Payment on FHA Transmittal Summary",
                        (loan, app) => app.aFHAChildSupportPmt_rep),

                    new FieldValueItem(
                        nameof(CAppData.aLiaCollection),
                        "Child Support Payment on Liabilities Page",
                        (loan, app) => loan.m_convertLos.ToMoneyString(app.aLiaCollection.GetChildSupport(false)?.Pmt ?? 0, FormatDirection.ToRep))
                };
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }

            if (this.DataLoan.sIsSystemBatchMigration)
            {
                return new List<string>()
                {
                    "Migration cannot be run as a system batch migration."
                };
            }

            var errorList = new List<string>(this.DataLoan.nApps);
            foreach (var app in this.DataLoan.Apps)
            {
                var childSupportFromLiabilities = app.aLiaCollection.GetChildSupport(false);
                var childSupportPaymentFromFhaField = app.aFHAChildSupportPmt;

                var missingChildSupportInLiabilities = childSupportFromLiabilities == null && childSupportPaymentFromFhaField != 0M;
                var paymentMismatch = childSupportFromLiabilities != null && childSupportFromLiabilities.Pmt != childSupportPaymentFromFhaField;

                if (missingChildSupportInLiabilities || paymentMismatch)
                {
                    errorList.Add($"The monthly child support amounts for application {app.aBLastFirstNm} must match.");
                }
            }

            return errorList;
        }
    }
}
