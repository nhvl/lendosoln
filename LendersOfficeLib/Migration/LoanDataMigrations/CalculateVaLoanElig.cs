﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Migration;

    /// <summary>
    /// Migration for OPM 204251.
    /// This migration aims to advance the loan version number in order to turn the aIsVAElig fields into calculated fields and
    /// enable some new UI.
    /// </summary>
    public class CalculateVaLoanElig : LoanDataMigration
    {
        /// <summary>
        /// The application field items we want to display. 
        /// </summary>
        private List<FieldValueItem> appFieldItems;

        /// <summary>
        /// The conditions that, if true, will cause the migration to fail.
        /// </summary>
        private List<PreRunFailureCondition> failureConditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculateVaLoanElig" /> class.
        /// </summary>
        /// <param name="userName">The name of the user.</param>
        /// <param name="userId">The id of the user.</param>
        /// <param name="brokerId">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration.</param>
        /// <param name="dataLoan">The loan to perform the migration on.</param>
        public CalculateVaLoanElig(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.appFieldItems = this.InitializeAppFields();
            this.failureConditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the associated version for this migration.
        /// </summary>
        /// <value>The associated version for this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V1_CalcVaLoanElig_Opm204251;
            }
        }

        /// <summary>
        /// Gets the description for this migration.
        /// </summary>
        /// <value>The description for this migration.</value>
        public override string Name
        {
            get
            {
                return "Calculate VA Loan Eligibility";
            }
        }

        /// <summary>
        /// Gets the app level fields we want to display for this migration.
        /// </summary>
        /// <value>The app level fields we want to display for this migration.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return this.appFieldItems;
            }
        }

        /// <summary>
        /// Gets the failure conditions for this migration.
        /// </summary>
        /// <value>The failure conditions for this migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.failureConditions;
            }
        }

        /// <summary>
        /// The migration function for this migration. This migration only wants to enable a calculation, so 
        /// we can simply return true and let the framework update the version number.
        /// </summary>
        /// <param name="errors">Any errors. There shouldn't be any.</param>
        /// <returns>Returns true since this migration should always be successful.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initializes the app field item list that we want to display in the UI.
        /// </summary>
        /// <returns>The app fields we want to display.</returns>
        private List<FieldValueItem> InitializeAppFields()
        {
            var appFields = new List<FieldValueItem>()
            {
                new FieldValueItem("aIsBVAElig", "Is borrower eligible for VA loan?", (loan, app) => app.aIsBVAElig.ToString()),
                new FieldValueItem("aIsCVAElig", "Is co-borrower eligible for VA loan?", (loan, app) => app.aIsCVAElig.ToString()),
                new FieldValueItem("aBIsVeteran", "Is borrower veteran?", (loan, app) => app.aBIsVeteran.ToString()),
                new FieldValueItem("aCIsVeteran", "Is co-borrower veteran?", (loan, app) => app.aCIsVeteran.ToString()),
                new FieldValueItem("aBIsSurvivingSpouseOfVeteran", "Is borrower surviving spouse of veteran?", (loan, app) => app.aBIsSurvivingSpouseOfVeteran.ToString()),
                new FieldValueItem("aCIsSurvivingSpouseOfVeteran", "Is co-borrower surviving spouse of veteran?", (loan, app) => app.aCIsSurvivingSpouseOfVeteran.ToString())
            };

            return appFields;
        }

        /// <summary>
        /// Initializes the conditions to run for this migration.
        /// </summary>
        /// <returns>A list of conditions to run for this migration.</returns>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.MigrationCondition));

            return conditions;
        }

        /// <summary>
        /// The failure condition for this migration.
        /// </summary>
        /// <returns>A list of error messages.</returns>
        private List<string> MigrationCondition()
        {
            List<string> errors = new List<string>();

            if (!this.DataLoan.sHasAnyAppsFilled)
            {
                return null;
            }

            string eligErrorMessage = "{0} is marked as VA Loan eligible but is not marked as either a veteran or as the surviving spouse of a veteran. Please mark {0} as at least one of those two " +
                                      "options in the \"Borrower Info\" screen, OR mark as not being VA loan eligible within the application tab of the PML screen in order to proceed with the migration.";

            for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
            {
                CAppData app = this.DataLoan.GetAppData(appIndex);

                if (app.aIsBVAElig && !app.aBIsVeteran && !app.aBIsSurvivingSpouseOfVeteran)
                {
                    errors.Add(string.Format(eligErrorMessage, app.aBNm));
                }

                if (app.aIsCVAElig && !app.aCIsVeteran && !app.aCIsSurvivingSpouseOfVeteran)
                {
                    errors.Add(string.Format(eligErrorMessage, app.aCNm));
                }
            }

            return errors;
        }
    }
}
