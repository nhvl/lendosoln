﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;

    /// <summary>
    /// Migrator for case 242490. This will introduce a check on the adjustment descriptions that will limit them to 60 characters.
    /// </summary>
    public class LimitAdjustmentLength : LoanDataMigration
    {
        /// <summary>
        /// The collection items to display and save.
        /// </summary>
        private List<CollectionItem> collectionItems;

        /// <summary>
        /// The conditions to check before running the migration.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="LimitAdjustmentLength"/> class.
        /// </summary>
        /// <param name="userName">The name of the user.</param>
        /// <param name="userId">The id of the user.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this is part of a system migration.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public LimitAdjustmentLength(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.collectionItems = this.InitializeCollections();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the version the loan will be in after this migration has finished.
        /// </summary>
        /// <value>The version the loan will be in after this migration has finished.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V13_LimitAdjustmentLength;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Limit Field Length for 1003 Credits & AOCs";
            }
        }

        /// <summary>
        /// Gets the collection items to display and store in the results.
        /// </summary>
        /// <value>The collection items to dispaly and store in the results.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                return this.collectionItems;
            }
        }

        /// <summary>
        /// Gets the conditions to check before the migration is ran.
        /// </summary>
        /// <value>The conditions to check before the migration is ran.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">Any errors during the migration.</param>
        /// <returns>Will always be true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initializes the collections for this migration.
        /// </summary>
        /// <returns>The collection items for this migration.</returns>
        private List<CollectionItem> InitializeCollections()
        {
            List<CollectionItem> collections = new List<CollectionItem>();

            if (this.DataLoan == null)
            {
                return collections;
            }

            List<CollectionItem> adjustments = new List<CollectionItem>();
            int adjustmentCount = 0;
            foreach (Adjustment adj in this.DataLoan.sAdjustmentList)
            {
                List<FieldValueItem> adjustmentItems = new List<FieldValueItem>()
                {
                    new FieldValueItem("Description", "Description", (loan, app) => adj.Description)
                };

                CollectionItem adjustmentItem = new CollectionItem("Adjustment", $"{adj.Description}_{adj.AdjustmentType.ToString("D")}_{adjustmentCount}", "Adjustment", false, null, adjustmentItems);
                adjustments.Add(adjustmentItem);
                adjustmentCount++;
            }

            CollectionItem adjustmentCollection = new CollectionItem("Adjustments and other credits", "sAdjustmentList", "Adjustment List", false, adjustments, null);
            collections.Add(adjustmentCollection);

            return collections;
        }

        /// <summary>
        /// Gets the conditions for this migration.
        /// </summary>
        /// <returns>The conditions to check for this migration.</returns>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            PreRunFailureCondition descriptionLengthCheck = new PreRunFailureCondition(() =>
            {
                List<string> errors = new List<string>();

                if (this.DataLoan.sAdjustmentList.Any(adjustment => adjustment.Description.Length > 60))
                {
                    errors.Add("Descriptions of adjustments on the Adjustments and Other Credits page cannot exceed 60 characters. Please truncate (or otherwise reduce) the descriptions to be less than 60 characters each.");
                    return errors;
                }

                return null;                
            });

            conditions.Add(descriptionLengthCheck);

            return conditions;
        }
    }
}
