﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;

    /// <summary>
    /// Enhance veteran service calculation migrator.
    /// </summary>
    public class PassthroughMigration : LoanDataMigration
    {
        /// <summary>
        /// Passthrough migration version.
        /// </summary>
        private LoanVersionT passthroughMigrationVersion;

        /// <summary>
        /// Initializes a new instance of the <see cref="PassthroughMigration"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        /// <param name="loanVersionT">The loan version of the migration that will be pass through.</param>
        public PassthroughMigration(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan, LoanVersionT loanVersionT) : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.passthroughMigrationVersion = loanVersionT;
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "This migration has been intentionally left blank.";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return this.passthroughMigrationVersion;
            }
        }

        /// <summary>
        /// Runs the migration. 
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }
    }
}
