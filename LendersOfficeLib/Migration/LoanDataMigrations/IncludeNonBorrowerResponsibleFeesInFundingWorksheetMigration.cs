﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;

    /// <summary>
    /// This migration will include non-borrower responsible fees that are marked as DFLP or paid by lender to be included in the funding worksheet.
    /// </summary>
    public class IncludeNonBorrowerResponsibleFeesInFundingWorksheetMigration : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncludeNonBorrowerResponsibleFeesInFundingWorksheetMigration"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">A value indicating whether this migration is being run as a system migration.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public IncludeNonBorrowerResponsibleFeesInFundingWorksheetMigration(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan) : 
            base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name
        {
            get
            {
                return "Include Non-Borrower Responsible Fees in the Funding Worksheet";
            }
        }

        /// <summary>
        /// Gets the version number associated with this migration.
        /// </summary>
        /// <value>The version number associated with this migration.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet;
            }
        }

        /// <summary>
        /// Gets the loan fields for display.
        /// </summary>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        "sSettlementChargesDedFromLoanProc",
                        "Settlement Charges Deducted From Loan Proceeds",
                        (loan, app) => loan.sSettlementChargesDedFromLoanProc_rep),
                    new FieldValueItem(
                        nameof(CPageData.sSettlementTotalFundByLenderAtClosing),
                        "Settlement Total Funded by Lender at Closing",
                        (loan, app) => loan.sSettlementTotalFundByLenderAtClosing_rep),
                    new FieldValueItem(
                        "sAmtReqToFund",
                        "Amount Required to Fund",
                        (loan, app) => loan.sAmtReqToFund_rep),
                    new FieldValueItem(
                        nameof(CPageData.sFundReqForShortfall),
                        "Funding Required for Shortfall",
                        (loan, app) => loan.sFundReqForShortfall_rep),
                    new FieldValueItem(
                        nameof(CPageData.sChkDueFromClosing),
                        "Check Due From Closing",
                        (loan, app) => loan.sChkDueFromClosing_rep)
                };
            }
        }        

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Run the actual migration.
        /// </summary>
        /// <param name="errors">Any errors that came up during the migration.</param>
        /// <returns>Whether the migration was successful or not.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = this.GetMigrationErrors();

            if (errors.Count != 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            var errors = new List<string>();

            if (this.DataLoan.IsTemplate)
            {
                return errors;
            }

            if (!this.DataLoan.sIsSystemBatchMigration && !this.HasPermissionErrors)
            {
                return errors;
            }

            if (this.DataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                return errors;
            }

            var oldLoanVersion = this.DataLoan.sLoanVersionT;

            this.DataLoan.sLoanVersionT = LoanVersionT.V32_ConstructionLoans_CalcChanges;
            var oldGfeTotalFundByLender = this.DataLoan.sGfeTotalFundByLender;
            var oldSettlementTotalDedFromLoanProc = this.DataLoan.sSettlementTotalDedFromLoanProc;

            this.DataLoan.sLoanVersionT = LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet;
            var newGfeTotalFundByLender = this.DataLoan.sGfeTotalFundByLender;
            var newSettlementTotalDedFromLoanProc = this.DataLoan.sSettlementTotalDedFromLoanProc;

            this.DataLoan.sLoanVersionT = oldLoanVersion;

            if (oldGfeTotalFundByLender != newGfeTotalFundByLender)
            {
                errors.Add("Please remove Non-Borrower Responsible Fees marked as Lender Paid with a non-zero amount.");
            }

            if (oldSettlementTotalDedFromLoanProc != newSettlementTotalDedFromLoanProc)
            {
                errors.Add("Please remove Non-Borrower Responsible Fees marked as DFLP with a non-zero amount.");
            }

            return errors;
        }
    }
}
