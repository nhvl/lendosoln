﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Currently we have two FHA case number fields that represent the same data point,
    /// causing confusion for users. This migration will consolidate them.
    /// </summary>
    public class ConsolidateFHACaseNumberFields : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> loanFieldItems;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsolidateFHACaseNumberFields"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public ConsolidateFHACaseNumberFields(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.loanFieldItems = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Consolidate two FHA case number fields that represent the same datapoint";

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V11_ConsolidateFHACaseNumberFields_Opm186897;

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be displayed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems => this.loanFieldItems;

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions => this.conditions;

        /// <summary>
        /// Runs the migration. If only "FHA Case Number for Current Loan" has a value, it should
        /// be propagated to "Case number of previous case" as we want that to be the primary
        /// field going forward. Otherwise, no change.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;

            if (string.IsNullOrEmpty(this.DataLoan.sFHAPreviousCaseNum) &&
                !string.IsNullOrEmpty(this.DataLoan.sSpFhaCaseNumCurrentLoan))
            {
                this.DataLoan.sFHAPreviousCaseNum = this.DataLoan.sSpFhaCaseNumCurrentLoan;
            }

            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items.</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sFHAPreviousCaseNum), "Case number of previous case", (loan, app) => loan.sFHAPreviousCaseNum));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sSpFhaCaseNumCurrentLoan), "FHA Case Number for Current Loan", (loan, app) => loan.sSpFhaCaseNumCurrentLoan));

            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.FHACaseNumberFieldConditions));
            return conditions;
        }

        /// <summary>
        /// Checks whether the FHA case number fields both have values. If so, we do not want to
        /// cause data loss by overwriting one, so we error out until the user removes the incorrect value.
        /// </summary>
        /// <returns>Returns a list of strings containing the error message if the failure condition is triggered. Null otherwise.</returns>
        private List<string> FHACaseNumberFieldConditions()
        {
            List<string> errors = new List<string>();

            bool caseNumberFieldsHaveConflictingValues =
                !string.IsNullOrEmpty(this.DataLoan.sFHAPreviousCaseNum)
                && !string.IsNullOrEmpty(this.DataLoan.sSpFhaCaseNumCurrentLoan)
                && !this.DataLoan.sFHAPreviousCaseNum.Equals(this.DataLoan.sSpFhaCaseNumCurrentLoan, StringComparison.OrdinalIgnoreCase);

            if (caseNumberFieldsHaveConflictingValues)
            {
                errors.Add("Conflicting values for \"Case number for previous case\" on FHA Connection page and \"FHA Case Number for Current Loan\" on Subject Property Details page. Please remove the incorrect value.");
                return errors;
            }

            return null;
        }
    }
}