﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Will zero out any hidden 'Other Housing' expenses. 
    /// After the migration is ran, only 'Other Housing' expenses that have a zero annual amount can be hidden.
    /// </summary>
    public class HideOtherHousingExpenses : LoanDataMigration
    {
        /// <summary>
        /// The collection items to display.
        /// </summary>
        private List<CollectionItem> collectionItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="HideOtherHousingExpenses"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystem">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public HideOtherHousingExpenses(string userName, Guid userId, Guid brokerId, bool isSystem, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystem, dataLoan)
        {
            this.collectionItems = this.InitializeCollectionItems();
        }

        /// <summary>
        /// Gets the version that the loan will be in after this migration is successful.
        /// </summary>
        /// <value>The version that the loan will bein after this miggiration is successful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V10_HideOtherHousingExpenses_Opm235669;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Do not allow conflicts in Other Housing Expenses";
            }
        }

        /// <summary>
        /// Gets the collection items to display.
        /// </summary>
        /// <value>The collection items to display.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                return this.collectionItems;
            }
        }

        /// <summary>
        /// Will zero out any hidden 'Other Housing' expenses.
        /// </summary>
        /// <param name="errors">The errors encountered during this migration.</param>
        /// <returns>Will always return true.</returns>
        /// <remarks>
        /// Will zero out the monthly fixed amount and monthly percent for expenses in Calculator mode.
        /// Will zero out the disbursement amounts if in Disbursement mode.
        /// </remarks>
        protected override bool PerformMigration(out List<string> errors)
        {
            List<BaseHousingExpense> expensesToZeroOut = new List<BaseHousingExpense>();
            if (this.DataLoan.sLine1008Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1008Exp)
            {
                expensesToZeroOut.Add(this.DataLoan.sUnassigned1008Expense);    
            }

            if (this.DataLoan.sLine1009Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1009Exp)
            {
                expensesToZeroOut.Add(this.DataLoan.sUnassigned1009Expense);
            }

            if (this.DataLoan.sLine1010Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1010Exp)
            {
                expensesToZeroOut.Add(this.DataLoan.sUnassigned1010Expense);
            }

            if (this.DataLoan.sLine1011Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1011Exp)
            {
                expensesToZeroOut.Add(this.DataLoan.sUnassigned1011Expense);
            }

            foreach (BaseHousingExpense expense in expensesToZeroOut)
            {
                expense.MonthlyAmtFixedAmt = 0;
                expense.AnnualAmtCalcBasePerc = 0;

                if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    foreach (SingleDisbursement disb in expense.ActualDisbursementsInOneYear)
                    {
                        disb.DisbursementAmt = 0;
                    }
                }
            }

            errors = null;
            return true;
        }

        /// <summary>
        /// Initializes the collection items to display.
        /// </summary>
        /// <returns>The collection items to display.</returns>
        private List<CollectionItem> InitializeCollectionItems()
        {
            List<BaseHousingExpense> expensesToDisplay = new List<BaseHousingExpense>();
            if (this.DataLoan == null)
            {
                return new List<CollectionItem>();
            }

            if (this.DataLoan.sLine1008Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1008Exp)
            {
                expensesToDisplay.Add(this.DataLoan.sUnassigned1008Expense);
            }

            if (this.DataLoan.sLine1009Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1009Exp)
            {
                expensesToDisplay.Add(this.DataLoan.sUnassigned1009Expense);
            }

            if (this.DataLoan.sLine1010Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1010Exp)
            {
                expensesToDisplay.Add(this.DataLoan.sUnassigned1010Expense);
            }

            if (this.DataLoan.sLine1011Expense.CalculatedHousingExpenseType != E_HousingExpenseTypeCalculatedT.Line1011Exp)
            {
                expensesToDisplay.Add(this.DataLoan.sUnassigned1011Expense);
            }

            List<CollectionItem> unassignedExpenseCollectionItems = new List<CollectionItem>();
            foreach (BaseHousingExpense expense in expensesToDisplay)
            {
                List<FieldValueItem> expenseProperties = new List<FieldValueItem>()
                {
                    new FieldValueItem("AnnualAmtCalcType", "Calculation Source", (loan, app) => this.AnnualAmtCalcTypeToString(expense.AnnualAmtCalcType)),
                    new FieldValueItem("MonthlyAmtFixedAmt", "Monthly Amount (PITI) Base Amount", (loan, app) => expense.MonthlyAmtFixedAmt_rep),
                    new FieldValueItem("AnnualAmtCalcBasePerc", "Monthly Amount (PITI) Percentage", (loan, app) => expense.AnnualAmtCalcBasePerc_rep),
                };

                List<CollectionItem> disbursementItems = null;
                if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    disbursementItems = new List<CollectionItem>();
                    foreach (SingleDisbursement disb in expense.ActualDisbursementsInOneYear)
                    {
                        List<FieldValueItem> collectionProperties = new List<FieldValueItem>()
                        {
                            new FieldValueItem("DisbursementAmt", "Due Amount", (loan, app) => disb.DisbursementAmt_rep)
                        };

                        disbursementItems.Add(new CollectionItem("Disbursement", disb.DisbId.ToString(), "Disbursement", false, null, collectionProperties));
                    }
                }

                unassignedExpenseCollectionItems.Add(new CollectionItem(expense.ExpenseDescription, expense.CalculatedHousingExpenseType.ToString(), "Other Housing Expense", true, disbursementItems, expenseProperties));
            }

            return new List<CollectionItem>()
            {
                new CollectionItem("Other Housing Expenses", "sHousingExpenses", "Housing Expenses", false, unassignedExpenseCollectionItems, null)
            };
        }

        /// <summary>
        /// Converts the calculation mode enum to a string.
        /// </summary>
        /// <param name="type">The enum to convert.</param>
        /// <returns>The string representation of the enum.</returns>
        private string AnnualAmtCalcTypeToString(E_AnnualAmtCalcTypeT type)
        {
            if (type == E_AnnualAmtCalcTypeT.Disbursements)
            {
                return "Disbursements";
            }
            else
            {
                return "Calculator";
            }
        }
    }
}
