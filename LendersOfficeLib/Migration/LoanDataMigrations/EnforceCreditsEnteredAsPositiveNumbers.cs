﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// This migration ensures that the lender credit and tolerance cure values are entered
    /// as positive numbers. Once the migration runs, the affected fields will default to 0.00
    /// if the user enters a negative value.
    /// </summary>
    public class EnforceCreditsEnteredAsPositiveNumbers : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> loanFieldItems;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnforceCreditsEnteredAsPositiveNumbers"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public EnforceCreditsEnteredAsPositiveNumbers(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.loanFieldItems = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Enforce that the lender credit and tolerance cure amounts are entered as positive numbers";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V8_EnforceCreditsEnteredAsPositiveNumbers_Opm225917;
            }
        }

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be dispalyed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanFieldItems;
            }
        }

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Runs the migration. No logic is necessary here. The migration condition verifies that the lender credit and
        /// tolerance cure fields are not negative prior to the migration, and once the loan is at Version 8 they cannot
        /// be set to negative values in the future.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True always, since no processing is necessary for this migration.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items.</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sLenderCustomCredit1Amount), "Custom Lender Credit Amount 1", (loan, app) => loan.sLenderCustomCredit1Amount_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sLenderCustomCredit2Amount), "Custom Lender Credit Amount 2", (loan, app) => loan.sLenderCustomCredit2Amount_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sToleranceZeroPercentCure), "0% Tolerance Cure", (loan, app) => loan.sToleranceZeroPercentCure_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sToleranceTenPercentCure), "10% Tolerance Cure", (loan, app) => loan.sToleranceTenPercentCure_rep));

            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.LenderCreditsAndTolerancesCondition));
            return conditions;
        }

        /// <summary>
        /// Checks that the lender credit and tolerance cure fields are positive before the migration. An error will be returned
        /// if any of the fields have a negative value.
        /// </summary>
        /// <returns>Returns a list of strings containing the error message if the failure condition is triggered. Null otherwise.</returns>
        private List<string> LenderCreditsAndTolerancesCondition()
        {
            List<string> errors = new List<string>();

            bool lenderCredit1IsNegative = this.DataLoan.sLenderCustomCredit1Amount < 0m;
            bool lenderCredit2IsNegative = this.DataLoan.sLenderCustomCredit2Amount < 0m;
            bool zeroPercentToleranceCureIsNegative = this.DataLoan.sToleranceZeroPercentCure < 0m;
            bool tenPercentToleranceCureIsNegative = this.DataLoan.sToleranceTenPercentCure < 0m;

            if (lenderCredit1IsNegative || lenderCredit2IsNegative || zeroPercentToleranceCureIsNegative || tenPercentToleranceCureIsNegative)
            {
                errors.Add("Manually entered lender credits and tolerance cures must be input as a positive number on the \"Lender Credits / Discount\" page.");
                return errors;
            }

            return null;
        }
    }
}
