﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Updates the way we population the application submitted date and enables
    /// and application-level interview date.
    /// </summary>
    public class UpdateApplicationSubmittedDateCalc : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateApplicationSubmittedDateCalc"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public UpdateApplicationSubmittedDateCalc(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>The loan version for this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V17_UpdateApplicationSubmittedCalculation;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Improve flow and population of Application Submitted and Interview dates";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sAppSubmittedD),
                        "Application Submitted Date",
                        (loan, app) => loan.sAppSubmittedD_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sApp1003InterviewerPrepareDate),
                        "Interview Date",
                        (loan, app) => loan.sApp1003InterviewerPrepareDate_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sLTotI),
                        "Total Income",
                        (loan, app) => loan.sLTotI_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sLAmtCalc),
                        "Loan Amount",
                        (loan, app) => loan.sLAmtCalc_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sApprVal),
                        "Appraised Value",
                        (loan, app) => loan.sApprVal_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sPurchPrice),
                        "Purchase Price",
                        (loan, app) => loan.sPurchPrice_rep),
                    new FieldValueItem(
                        nameof(CPageBase.sSpAddr),
                        "Subject Property Address",
                        (loan, app) => loan.sSpAddr)
                };
            }
        }

        /// <summary>
        /// Gets the application fields to display.
        /// </summary>
        /// <value>The application fields to display.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CAppBase.aBNm),
                        "Borrower Name",
                        (loan, app) => app.aBNm),
                    new FieldValueItem(
                        nameof(CAppBase.aBSsn),
                        "Borrower SSN",
                        (loan, app) => app.aBSsn),
                    new FieldValueItem(
                        nameof(CAppBase.a1003InterviewD),
                        "Interview Date",
                        (loan, app) => app.a1003InterviewD_rep)
                };
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();

            bool blankFile = this.DataLoan.Apps.All(a => string.IsNullOrWhiteSpace(a.aBNm) && string.IsNullOrWhiteSpace(a.aBSsn))
                && this.DataLoan.sLTotI == 0
                && (string.IsNullOrWhiteSpace(this.DataLoan.sSpAddr) || this.DataLoan.sSpAddrTBD)
                && this.DataLoan.sApprVal == 0
                && this.DataLoan.sPurchPrice == 0
                && this.DataLoan.sLAmtCalc == 0
                && !this.DataLoan.sAppSubmittedD.IsValid
                && !this.DataLoan.sApp1003InterviewerPrepareDate.IsValid;

            if (blankFile)
            {
                return true;
            }
            else if (!this.DataLoan.sAppSubmittedDLckd)
            {
                // If the App Submitted Date is not locked, then it will be the
                // same as the 1003 Interviewer Prepared Date.
                this.DataLoan.LocksRespa6DToValueWithVersionBypass(this.DataLoan.sAppSubmittedD);
            }
            else if (this.DataLoan.sAppSubmittedDLckd)
            {
                this.DataLoan.LocksRespa6DToValueWithVersionBypass(this.DataLoan.sAppSubmittedD);

                foreach (var app in this.DataLoan.Apps)
                {
                    app.Locka1003InterviewDToValueWithVersionBypass(this.DataLoan.sApp1003InterviewerPrepareDate);
                }

                this.DataLoan.sAppSubmittedDLckd = false;
            }

            return true;
        }
    }
}
