﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Migration to calculate the per-diem interest start date as the first
    /// business day on or after rescission expires.
    /// </summary>
    public class UpdatePerDiemInterestStartDateCalculation : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdatePerDiemInterestStartDateCalculation"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">A value indicating whether this migration is being run as a system migration.</param>
        /// <param name="loan">The loan to migrate.</param>
        public UpdatePerDiemInterestStartDateCalculation(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData loan)
            : base(userName, userId, brokerId, isSystemMigration, loan)
        {
        }

        /// <summary>
        /// Gets the version number associated with this migration.
        /// </summary>
        /// <value>The version number associated with this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V9_UpdatePerDiemInterestStartDateCalc_Opm179289;

        /// <summary>
        /// Gets the name of this migration.
        /// </summary>
        /// <value>The name of this migration.</value>
        public override string Name => "Calculate the per-diem interest start "
            + "date as the first business day on or after rescission expires";

        /// <summary>
        /// Gets the conditions to check before running the migration.
        /// </summary>
        /// <value>The conditions to check before running the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new List<PreRunFailureCondition>
                {
                    new PreRunFailureCondition(() => this.GetMigrationFailureReasons())
                };
            }
        }

        /// <summary>
        /// Gets the loan fields for display.
        /// </summary>
        /// <value>The loan fields for display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new List<FieldValueItem>
                {
                    new FieldValueItem(
                        "sConsummationD",
                        "Per-diem Interest Start Date",
                        (loan, app) => loan.sConsummationD_rep),
                    new FieldValueItem(
                        nameof(this.DataLoan.sConsummationDLckd),
                        "Per-diem Interest Start Date Locked",
                        (loan, app) => loan.sConsummationDLckd ? "Yes" : "No"),
                    new FieldValueItem(
                        nameof(this.DataLoan.sIPiaDy),
                        "Number of Prepaid Interest Days",
                        (loan, app) => loan.sIPiaDy_rep),
                    new FieldValueItem(
                        nameof(this.DataLoan.sIPia),
                        "Interest Amount Paid in Advance",
                        (loan, app) => loan.sIPia_rep)
                };
            }
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="errors">Will be set to null.</param>
        /// <returns>Returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Gets the reasons the migration can't run.
        /// </summary>
        /// <returns>The reasons the migration can't run. Null if the migration can run.</returns>
        private List<string> GetMigrationFailureReasons()
        {
            if (!this.DataLoan.sIsSystemBatchMigration)
            {
                return null;
            }
            else if (this.DataLoan.sConsummationDLckd)
            {
                return null;
            }
            else if (this.DataLoan.IsTemplate)
            {
                return null;
            }
            else
            {
                var originalVersion = this.DataLoan.sLoanVersionT;
                var originalValue = this.DataLoan.sConsummationD_rep;
                this.DataLoan.sLoanVersionT = LoanVersionT.V9_UpdatePerDiemInterestStartDateCalc_Opm179289;
                var newValue = this.DataLoan.sConsummationD_rep;
                this.DataLoan.sLoanVersionT = originalVersion;

                if (newValue == originalValue)
                {
                    return null;
                }
            }

            return new List<string> { "Migration cannot be run as a system batch migration." };
        }
    }
}
