﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// This migration ensures that the APR-related closing cost total cannot be set
    /// to a negative value.
    /// </summary>
    public class PreventNegativeAprRelatedClosingCosts : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> loanFields;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreventNegativeAprRelatedClosingCosts"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public PreventNegativeAprRelatedClosingCosts(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.loanFields = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Do not allow total APR-Related closing costs to be negative.";

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V14_PreventNegativeAprRelatedClosingCosts_Opm235692;

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be displayed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems => this.loanFields;

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions => this.conditions;

        /// <summary>
        /// Runs the migration. No processing is necessary here. The migration conditions verify that
        /// the APR-related closing cost total is not negative, and after the migration it cannot
        /// become negative in the future.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True always, since no processing is needed at this point.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items.</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sAprIncludedCc), "Total APR-Related closing costs", (loan, app) => loan.sAprIncludedCc_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sApr), "APR", (loan, app) => loan.sApr_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sFinancedAmt), "Total financed amount", (loan, app) => loan.sFinancedAmt_rep));
            getters.Add(new FieldValueItem(nameof(this.DataLoan.sFinCharge), "Financing charge", (loan, app) => loan.sFinCharge_rep));

            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.PositiveAPRClosingCostsForBatchMigrationCondition));
            return conditions;
        }

        /// <summary>
        /// Ensures that the APR-Related closing cost total is a positive number if this loan
        /// is being migrated as a batch. We do not want to batch update any loans where the total
        /// is negative as it will be forced to 0 afterward, and may cause unexpected changes
        /// to other loan fields.
        /// </summary>
        /// <returns>A list of errors generated by the condition.</returns>
        private List<string> PositiveAPRClosingCostsForBatchMigrationCondition()
        {
            if (this.DataLoan.sAprIncludedCc < 0 && this.DataLoan.sIsSystemBatchMigration)
            {
                var errors = new List<string>();
                errors.Add("Migration cannot be run as a system batch migration.");
                return errors;
            }

            return null;
        }
    }
}