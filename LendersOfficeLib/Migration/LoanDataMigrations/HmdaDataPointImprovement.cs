﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using LendersOffice.ObjLib.Hmda;

    /// <summary>
    /// Migration for OPM 451705.
    /// </summary>
    public class HmdaDataPointImprovement : LoanDataMigration
    {
        /// <summary>
        /// Set of old HMDA Action Taken string values.
        /// </summary>
        private static HashSet<string> oldHmdaActionTakenValues = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "Loan originated",
            "App approved but not accepted by applicant",
            "Application denied",
            "Application withdrawn",
            "File closed for incompleteness",
            "Loan purchased by your institution",
            "Preapproval request denied",
            "Preapproval request approved but not accepted"
        };

        /// <summary>
        /// Set of old HMDA Denial Reason string values.
        /// </summary>
        private static HashSet<string> oldHmdaDenialReasonValues = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "1 Debt-to-income ratio",
            "2 Employment history",
            "3 Credit history",
            "4 Collateral",
            "5 Insufficient cash",
            "6 Unverifiable information",
            "7 Credit app incomplete",
            "8 Mortgage ins denied",
            "9 Other"
        };

        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> fieldGetters;

        /// <summary>
        /// List of conditions that will be checked before running the migrations.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="HmdaDataPointImprovement"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public HmdaDataPointImprovement(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.fieldGetters = this.InitializeLoanGetters();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V22_HmdaDataPointImprovement;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "HMDA Data Point Improvement Migration";
            }
        }

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be dispalyed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.fieldGetters;
            }
        }

        /// <summary>
        /// Gets the conditions that will prevent this migration from running.
        /// </summary>
        /// <value>The conditions to prevent this migration from running.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// This function is for running the actual migration.
        /// You don't need to update sLoanVersionT. That gets done automatically later on.
        /// So sometimes, if the the version number is used to just enable a calculation, this function can just return true without doing anything else.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            try
            {
                // Set values to blank for all templates.
                if (this.DataLoan.IsTemplate)
                {
                    this.DataLoan.sHmdaActionTakenT = HmdaActionTaken.Blank;
                    this.DataLoan.sHmdaDenialReasonsList = new List<HmdaDenialReason>()
                    {
                        HmdaDenialReason.Blank,
                        HmdaDenialReason.Blank,
                        HmdaDenialReason.Blank,
                        HmdaDenialReason.Blank
                    };

                    this.DataLoan.sHmdaLienT = E_sHmdaLienT.LeaveBlank;
                    return true;
                }

                // Migrate non-template loans.
                this.DataLoan.sHmdaActionTakenT = Tools.MapStringToHmdaActionTaken(this.DataLoan.sHmdaActionTaken);

                // Migrate Denial reasons.
                string[] stringReasons =
                {
                    this.DataLoan.sHmdaDenialReason1.Trim(),
                    this.DataLoan.sHmdaDenialReason2.Trim(),
                    this.DataLoan.sHmdaDenialReason3.Trim(),
                    this.DataLoan.sHmdaDenialReason4.Trim()
                };

                List<HmdaDenialReason> denialReasonList = new List<HmdaDenialReason>();

                foreach (string reason in stringReasons)
                {
                    if (string.IsNullOrWhiteSpace(reason))
                    {
                        continue;
                    }

                    if (!oldHmdaDenialReasonValues.Contains(reason))
                    {
                        denialReasonList.Add(HmdaDenialReason.Other);
                        this.DataLoan.sHmdaDenialReasonOtherDescription = Tools.TruncateString(reason, 255);
                    }
                    else
                    {
                        denialReasonList.Add(Tools.MapStringToHmdaDenialReason(reason));
                    }
                }

                this.DataLoan.sHmdaDenialReasonsList = denialReasonList;

                //// Note: sHmdaLienT does not need an actual migration.

                return true;
            }
            catch (CBaseException e)
            {
                Tools.LogError("HMDADataPointImprovement migration encountered an unexpected exception.", e);

                errors.Add(e.Message);
                return false;
            }
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items..</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>();
            getters.Add(new FieldValueItem("sHmdaLienT", "HMDA Lien Status", (loan, app) => loan.sHmdaLienT.ToString()));
            getters.Add(new FieldValueItem("sHmdaActionTaken", "HMDA Action Taken", (loan, app) => loan.sHmdaActionTaken));
            getters.Add(new FieldValueItem("sHmdaActionTakenT", "HMDA Action Taken Type", (loan, app) => Tools.MapHmdaActionTakenToString(loan.sHmdaActionTakenT)));
            getters.Add(new FieldValueItem("sHmdaDenialReason1", "HMDA Denial Reason #1", (loan, app) => loan.sHmdaDenialReason1));
            getters.Add(new FieldValueItem("sHmdaDenialReason2", "HMDA Denial Reason #2", (loan, app) => loan.sHmdaDenialReason2));
            getters.Add(new FieldValueItem("sHmdaDenialReason3", "HMDA Denial Reason #3", (loan, app) => loan.sHmdaDenialReason3));
            getters.Add(new FieldValueItem("sHmdaDenialReason4", "HMDA Denial Reason #4", (loan, app) => loan.sHmdaDenialReason4));
            getters.Add(new FieldValueItem(
                "sHmdaDenialReasonsList",
                "HMDA Denial Reasons List",
                (loan, app) => $"{{{string.Join(",", loan.sHmdaDenialReasonsList.Select(hdr => Tools.MapHmdaDenialReasonToString(hdr)))}}}"));
            return getters;
        }

        /// <summary>
        /// Initializes the conditions to check before running this migration.
        /// </summary>
        /// <returns>The conditions to check before running this migration.</returns>
        /// <remarks>
        /// The function for the conditions do not take in any parameters, so you can really make any condition you want.
        /// Though adding required parameters doesn't prevent you from doing whatever you want anyways.
        /// The condition function should return a list of error messages if it has been triggered. Null or empty otherwise.
        /// </remarks>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            List<PreRunFailureCondition> conditions = new List<PreRunFailureCondition>();
            conditions.Add(new PreRunFailureCondition(this.CheckHmdaActionTaken));
            conditions.Add(new PreRunFailureCondition(this.CheckHmdaDenialReasonForDuplicates));
            conditions.Add(new PreRunFailureCondition(this.CheckHmdaDenialReasonCanBeMapped));
            conditions.Add(new PreRunFailureCondition(this.CheckHmdaLienT));

            return conditions;
        }

        /// <summary>
        /// Checks if we can migrate sHmdaActionTaken.
        /// </summary>
        /// <returns>Failure message.</returns>
        private List<string> CheckHmdaActionTaken()
        {
            if (!this.DataLoan.IsTemplate && !string.IsNullOrWhiteSpace(this.DataLoan.sHmdaActionTaken) && !oldHmdaActionTakenValues.Contains(this.DataLoan.sHmdaActionTaken.Trim()))
            {
                return new List<string>()
                {
                    "The HMDA Action Taken is not a valid choice, please select from the list and try again."
                };
            }

            return null;
        }

        /// <summary>
        /// Checks if we can migrate sHmdaDenialReason.
        /// </summary>
        /// <returns>Failure message.</returns>
        private List<string> CheckHmdaDenialReasonForDuplicates()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }

            string[] denialReasons =
            {
                this.DataLoan.sHmdaDenialReason1.Trim(),
                this.DataLoan.sHmdaDenialReason2.Trim(),
                this.DataLoan.sHmdaDenialReason3.Trim(),
                this.DataLoan.sHmdaDenialReason4.Trim()
            };

            List<string> errors = new List<string>();
            for (int i = 0; i < denialReasons.Length - 1; i++)
            {
                if (string.IsNullOrWhiteSpace(denialReasons[i]))
                {
                    continue;
                }

                for (int j = i + 1; j < denialReasons.Length; j++)
                {
                    if (string.Equals(denialReasons[i], denialReasons[j]))
                    {
                        errors.Add($"If not blank, HMDA Denial Reasons must be unique. Reason #{i+1} == Reason #{j+1}");
                    }
                }
            }

            if (errors.Count > 0)
            {
                return errors;
            }

            return null;
        }

        /// <summary>
        /// Checks if we can migrate sHmdaDenialReason.
        /// </summary>
        /// <returns>Failure message.</returns>
        private List<string> CheckHmdaDenialReasonCanBeMapped()
        {
            if (this.DataLoan.IsTemplate)
            {
                return null;
            }

            string[] denialReasons =
            {
                this.DataLoan.sHmdaDenialReason1.Trim(),
                this.DataLoan.sHmdaDenialReason2.Trim(),
                this.DataLoan.sHmdaDenialReason3.Trim(),
                this.DataLoan.sHmdaDenialReason4.Trim()
            };

            bool foundOther = false;
            foreach (string reason in denialReasons)
            {
                // Check if can convert to enum.
                if (!string.IsNullOrWhiteSpace(reason) && (!oldHmdaDenialReasonValues.Contains(reason) || Tools.MapStringToHmdaDenialReason(reason) == HmdaDenialReason.Other))
                {
                    if (foundOther)
                    {
                        return new List<string>()
                        {
                            "Only one 'Other' option is supported."
                        };
                    }

                    foundOther = true;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if we can migrate sHmdaLienT.
        /// </summary>
        /// <returns>Failure message.</returns>
        private List<string> CheckHmdaLienT()
        {
            DateTime date = new DateTime(2018, 1, 1);
            if (!this.DataLoan.IsTemplate
                && this.DataLoan.sHmdaLienTLckd // Only matters if it's locked.
                && (this.DataLoan.sHmdaLienT == E_sHmdaLienT.NotSecuredByLien || this.DataLoan.sHmdaLienT == E_sHmdaLienT.NotApplicablePurchaseLoan)
                && ((this.DataLoan.sHmdaActionD.IsValid && this.DataLoan.sHmdaActionD.DateTimeForComputation > date) || DateTime.Today > date))
            {
                return new List<string>()
                {
                     $"The HMDA Lien Type is set to {this.DataLoan.sHmdaLienT.ToString()} which is not valid for the action taken date."
                };
            }

            return null;
        }
    }
}
