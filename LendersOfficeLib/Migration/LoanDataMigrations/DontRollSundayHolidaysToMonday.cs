﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Migration that changes whether business days should be calculated by observing holidays that fall on a Sunday
    /// on the following Monday. After migration date fields will count the Monday after a Sunday holiday as a work day.
    /// </summary>
    /// <remarks>
    /// Note: No fields are actually changed as part of this migration. Instead, bumping up the version number changes the
    /// logic used to calculate a number of calculated date fields.
    /// </remarks>
    public class DontRollSundayHolidaysToMonday : LoanDataMigration
    {
        /// <summary>
        /// List of loan level fields that will be displayed in the UI and saved in the results.
        /// </summary>
        private List<FieldValueItem> fieldGetters;

        /// <summary>
        /// Initializes a new instance of the <see cref="DontRollSundayHolidaysToMonday"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public DontRollSundayHolidaysToMonday(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.fieldGetters = this.InitializeLoanGetters();
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "For date calculations, don't observe holidays that fall on Sunday on the following Monday";
            }
        }

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is successful.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V28_DontRollSundayHolidaysToMonday;
            }
        }

        /// <summary>
        /// Gets a list of loan level fields that will be displayed in the UI and saved to the results.
        /// </summary>
        /// <value>The list of loan level fields that will be displayed in the UI and saved to the results.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.fieldGetters;
            }
        }

        /// <summary>
        /// This function is for running the actual migration.
        /// You don't need to update sLoanVersionT. That gets done automatically later on.
        /// So sometimes, if the version number is used to just enable a calculation, this function can just return true without doing anything else.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>True if the loan has been successfully processed. False otherwise.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Initialize the loan field items.
        /// </summary>
        /// <returns>The loan field items..</returns>
        private List<FieldValueItem> InitializeLoanGetters()
        {
            List<FieldValueItem> getters = new List<FieldValueItem>()
            {
                new FieldValueItem(
                    nameof(CPageBase.sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD),
                    "Deadline for Borrower to Receive Revised Loan Estimate",
                    (loan, app) => loan.sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD),
                    "Deadline (from Closing Date) to Mail or Deliver Initial Loan Estimate",
                    (loan, app) => loan.sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sInitialClosingDisclosureMailedDeadlineD),
                    "Deadline to Mail Initial Closing Disclosure",
                    (loan, app) => loan.sInitialClosingDisclosureMailedDeadlineD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sInitialClosingDisclosureReceivedDeadlineD),
                    "Deadline for Borrower to Receive Initial Closing Disclosure",
                    (loan, app) => loan.sInitialClosingDisclosureReceivedDeadlineD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sSettlementD),
                    "Settlement Date",
                    (loan, app) => loan.sSettlementD_rep),
                new FieldValueItem(
                    "sConsummationD",
                    "Consummation Date",
                    (loan, app) => loan.sConsummationD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sDisclosuresDueD),
                    "Disclosures Due Date",
                    (loan, app) => loan.sDisclosuresDueD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sTilGfeDueD),
                    "TIL Disclosure / GFE Due Date",
                    (loan, app) => loan.sTilGfeDueD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sRescissionD),
                    "Rescission Date",
                    (loan, app) => loan.sRescissionD_rep),
                new FieldValueItem(
                    nameof(CPageBase.sEstCloseD),
                    "Estimated Closing Date",
                    (loan, app) => loan.sEstCloseD_rep)
            };

            return getters;
        }
    }
}