﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Consolidates sFHASponsoredOriginatorEIN and sSponsoredOriginatorEIN.
    /// </summary>
    public class ConsolidateTaxIdFields : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsolidateTaxIdFields"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public ConsolidateTaxIdFields(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the loan version for this migration.
        /// </summary>
        /// <value>The loan version for this migration.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V18_ConsolidateTaxIdFields;

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Consolidate Tax ID fields and Automatically format Sponsored Originator EIN";

        /// <summary>
        /// Gets the loan fields to display.
        /// </summary>
        /// <value>The loan fields to display.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageBase.sFHASponsoredOriginatorEIN),
                        "FHA Sponsored Originator EIN",
                        (loan, app) => loan.sFHASponsoredOriginatorEIN),
                    new FieldValueItem(
                        nameof(CPageBase.sSponsoredOriginatorEIN),
                        "Sponsored Originator EIN",
                        (loan, app) => loan.sSponsoredOriginatorEIN),
                };
            }
        }

        /// <summary>
        /// Gets the collection fields to display for the migration.
        /// </summary>
        /// <value>The collection fields to display for the migration.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                var agentTaxIds = this.AgentsWithTaxIds
                    .Select((agent, index) => new FieldValueItem(
                        $"{index}_{agent.AgentRoleT}.TaxId",
                        $"{agent.AgentRoleDescription} Tax ID",
                        (loan, app) => agent.TaxId));

                if (agentTaxIds.Any())
                {
                    yield return new CollectionItem("Agents", "sAgents", "string", shouldUseItemTypeLabel: false, collectionItems: null, propertyItems: agentTaxIds.ToList());
                }

                // We only care about locked preparers when selecting here
                // because preparers that are not locked will just return the
                // value from an Agent, which will be in the agentsCollection.
                var preparerTaxIds = this.PreparersWithTaxIds
                    .Where(preparer => preparer.IsLocked)
                    .Select((preparer, index) => new FieldValueItem(
                        $"{index}_{preparer.PreparerFormT}.TaxId",
                        $"{preparer.PreparerFormT} Tax ID",
                        (loan, app) => preparer.TaxId));

                if (preparerTaxIds.Any())
                {
                    yield return new CollectionItem("Preparers", "sPreparers", "string", shouldUseItemTypeLabel: false, collectionItems: null, propertyItems: preparerTaxIds.ToList());
                }
            }
        }

        /// <summary>
        /// Gets the failure conditions for the migration.
        /// </summary>
        /// <value>The failure conditions for the migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return new[] { new PreRunFailureCondition(this.GetMigrationErrors) };
            }
        }

        /// <summary>
        /// Gets the agents from the loan that have a non-blank TaxId.
        /// </summary>
        /// <value>The agents from the loan that have a non-blank TaxId.</value>
        private IEnumerable<CAgentFields> AgentsWithTaxIds
        {
            get
            {
                return this.DataLoan.sAgents.Where(agent => !string.IsNullOrEmpty(agent.TaxId));
            }
        }

        /// <summary>
        /// Gets the agents from the loan that have a non-blank TaxId.
        /// </summary>
        /// <value>The agents from the loan that have a non-blank TaxId.</value>
        private IEnumerable<IPreparerFields> PreparersWithTaxIds
        {
            get
            {
                return this.DataLoan.PreparerCollection.Values.Where(preparer => !string.IsNullOrEmpty(preparer.TaxId));
            }
        }

        /// <summary>
        /// Performs the migration.
        /// </summary>
        /// <param name="errors">The errors that occurred during the migration.</param>
        /// <returns>True if the file was successfully migrated. Otherwise, false.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = this.GetMigrationErrors();

            if (errors.Any())
            {
                return false;
            }

            if (!this.DataLoan.sFhaSponsoredOriginatorEinLckd
                && !string.IsNullOrEmpty(this.DataLoan.sFHASponsoredOriginatorEIN))
            {
                // If there are no errors, then we know the values have a valid format.
                // If the value is not locked, then it will pull from the agent value.
                // It is possible that the agent value is valid but contains > 9 characters
                // because it will allow a dash.
                // In this case, we want to set the value to the column without a dash to
                // make sure that we aren't storing a truncated version of the EIN.
                var rawEin = this.DataLoan.sFHASponsoredOriginatorEIN;
                EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(rawEin);
                this.DataLoan.sFHASponsoredOriginatorEIN = ein.ToString();
            }

            // For agents and preparers, we will re-set the value to strip the dash and ensure
            // consistent storage formatting for the new loan version.
            foreach (var agent in this.AgentsWithTaxIds)
            {
                EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(agent.TaxId);
                agent.TaxId = ein.ToString();
            }

            foreach (var preparer in this.PreparersWithTaxIds)
            {
                EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(preparer.TaxId);
                preparer.TaxId = ein.ToString();
            }

            return true;
        }

        /// <summary>
        /// Gets a list of errors that are preventing the migration from running.
        /// </summary>
        /// <returns>A list of errors that are preventing the migration from running.</returns>
        private List<string> GetMigrationErrors()
        {
            var errors = new List<string>();

            EmployerTaxIdentificationNumber? fhaSponsoredOriginatorEin = EmployerTaxIdentificationNumber.Create(this.DataLoan.sFHASponsoredOriginatorEIN);
            EmployerTaxIdentificationNumber? sponsoredOriginatorEin = EmployerTaxIdentificationNumber.Create(this.DataLoan.sSponsoredOriginatorEIN);

            if ((!string.IsNullOrEmpty(this.DataLoan.sFHASponsoredOriginatorEIN) && !fhaSponsoredOriginatorEin.HasValue)
                || (!string.IsNullOrEmpty(this.DataLoan.sSponsoredOriginatorEIN) && !sponsoredOriginatorEin.HasValue)
                || fhaSponsoredOriginatorEin.ToString() != sponsoredOriginatorEin.ToString())
            {
                errors.Add("The two Tax ID fields are not equal or at least one of them has incorrect formatting. "
                    + "They must be 9 digits formatted as either 123456789 or 12-3456789.");
            }

            var invalidTaxIdContacts = this.AgentsWithTaxIds
                .Where(agent => !agent.TaxIdValid)
                .Select(agent => agent.AgentRoleDescription);

            if (invalidTaxIdContacts.Any())
            {
                errors.Add("The Tax ID field of the following official contacts has invalid formatting: " + string.Join(", ", invalidTaxIdContacts));
            }

            // We only care about locked preparers when getting the errors
            // because preparers that are not locked will just return the
            // value from an Agent, which will be in the invalidTaxIdContacts.
            var invalidTaxIdPreparers = this.PreparersWithTaxIds
                .Where(preparer => preparer.IsLocked && !preparer.TaxIdValid)
                .Select(preparer => preparer.PreparerFormT);

            if (invalidTaxIdPreparers.Any())
            {
                errors.Add("The Tax ID field of the following preparers has invalid formatting: " + string.Join(", ", invalidTaxIdPreparers));
            }

            return errors;
        }
    }
}
