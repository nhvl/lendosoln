﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Replaces the string credit score model name fields with enum fields, to ensure better data consistency.
    /// </summary>
    public class ReplaceFreeformCreditModelsWithEnums : LoanDataMigration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReplaceFreeformCreditModelsWithEnums"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run this migration.</param>
        /// <param name="userId">The id of the user who will run this migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemMigration">Whether this migration is being ran by the system.</param>
        /// <param name="dataLoan">The loan that this migration will be performed on.</param>
        public ReplaceFreeformCreditModelsWithEnums(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name => "Replace credit score model textboxes with enums.";

        /// <summary>
        /// Gets the migration's version number. This is the version the loan will end up in if this migration is successful.
        /// </summary>
        /// <value>The version the loan will end up in if this migration is sucessful.</value>
        public override LoanVersionT AssociatedVersion => LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums;

        /// <summary>
        /// Retrieves the loan fields involved in this migration.
        /// </summary>
        /// <value>The loan fields involved in this migration.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CPageData.sReliedOnCreditScoreModelName),
                        "HMDA Relied-On Credit Score Model Name",
                        (loan, app) => loan.sReliedOnCreditScoreModelName),
                    new FieldValueItem(
                        nameof(CPageData.sReliedOnCreditScoreModelNameLckd),
                        "HMDA Relied-On Credit Score Model Name Locked",
                        (loan, app) => loan.sReliedOnCreditScoreModelNameLckd.ToString()),
                    new FieldValueItem(
                        nameof(CPageData.sReliedOnCreditScoreModelT),
                        "HMDA Relied-On Credit Score Model Type",
                        (loan, app) => loan.sReliedOnCreditScoreModelT.ToString()),
                    new FieldValueItem(
                        nameof(CPageData.sReliedOnCreditScoreModelTOtherDescription),
                        "HMDA Relied-On Credit Score Model Type Other Description",
                        (loan, app) => loan.sReliedOnCreditScoreModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CPageData.sReliedOnCreditScoreModelTLckd),
                        "HMDA Relied-On Credit Score Model Type Locked",
                        (loan, app) => loan.sReliedOnCreditScoreModelTLckd.ToString())
                };
            }
        }

        /// <summary>
        /// Retrieves the application fields involved in this migration.
        /// </summary>
        /// <value>The application fields involved in this migration.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return new[]
                {
                    new FieldValueItem(
                        nameof(CAppBase.aBEquifaxModelName),
                        "Borrower's Equifax Credit Model Name",
                        (loan, app) => app.aBEquifaxModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aBEquifaxModelT),
                        "Borrower's Equifax Credit Model Type",
                        (loan, app) => app.aBEquifaxModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aBEquifaxModelTOtherDescription),
                        "Borrower's Equifax Credit Model Other Description",
                        (loan, app) => app.aBEquifaxModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CAppBase.aBExperianModelName),
                        "Borrower's Experian Credit Model Name",
                        (loan, app) => app.aBExperianModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aBExperianModelT),
                        "Borrower's Experian Credit Model Type",
                        (loan, app) => app.aBExperianModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aBExperianModelTOtherDescription),
                        "Borrower's Experian Credit Model Other Description",
                        (loan, app) => app.aBExperianModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CAppBase.aBTransUnionModelName),
                        "Borrower's TransUnion Credit Model Name",
                        (loan, app) => app.aBTransUnionModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aBTransUnionModelT),
                        "Borrower's TransUnion Credit Model Type",
                        (loan, app) => app.aBTransUnionModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aBTransUnionModelTOtherDescription),
                        "Borrower's TransUnion Credit Model Other Description",
                        (loan, app) => app.aBTransUnionModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CAppBase.aCEquifaxModelName),
                        "Coborrower's Equifax Credit Model Name",
                        (loan, app) => app.aCEquifaxModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aCEquifaxModelT),
                        "Coborrower's Equifax Credit Model Type",
                        (loan, app) => app.aCEquifaxModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aCEquifaxModelTOtherDescription),
                        "Coborrower's Equifax Credit Model Other Description",
                        (loan, app) => app.aCEquifaxModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CAppBase.aCExperianModelName),
                        "Coborrower's Experian Credit Model Name",
                        (loan, app) => app.aCExperianModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aCExperianModelT),
                        "Coborrower's Experian Credit Model Type",
                        (loan, app) => app.aCExperianModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aCExperianModelTOtherDescription),
                        "Coborrower's Experian Credit Model Other Description",
                        (loan, app) => app.aCExperianModelTOtherDescription),
                    new FieldValueItem(
                        nameof(CAppBase.aCTransUnionModelName),
                        "Coborrower's TransUnion Credit Model Name",
                        (loan, app) => app.aCTransUnionModelName),
                    new FieldValueItem(
                        nameof(CAppBase.aCTransUnionModelT),
                        "Coborrower's TransUnion Credit Model Type",
                        (loan, app) => app.aCTransUnionModelT.ToString()),
                    new FieldValueItem(
                        nameof(CAppBase.aCTransUnionModelTOtherDescription),
                        "Coborrower's TransUnion Credit Model Other Description",
                        (loan, app) => app.aCTransUnionModelTOtherDescription)
                };
            }
        }

        /// <summary>
        /// Runs the migration. Attempts to map existing model names to enum values. If a
        /// mapping cannot be determined, the model type will default to Other and the
        /// model name will be placed in the other description. 
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();

            foreach (var app in this.DataLoan.Apps)
            {
                app.aBEquifaxModelT = Tools.MapCreditModelStringToEnum(app.aBEquifaxModelName);
                if (app.aBEquifaxModelT == E_CreditScoreModelT.Other)
                {
                    app.aBEquifaxModelTOtherDescription = app.aBEquifaxModelName;
                }

                app.aBExperianModelT = Tools.MapCreditModelStringToEnum(app.aBExperianModelName);
                if (app.aBExperianModelT == E_CreditScoreModelT.Other)
                {
                    app.aBExperianModelTOtherDescription = app.aBExperianModelName;
                }

                app.aBTransUnionModelT = Tools.MapCreditModelStringToEnum(app.aBTransUnionModelName);
                if (app.aBTransUnionModelT == E_CreditScoreModelT.Other)
                {
                    app.aBTransUnionModelTOtherDescription = app.aBTransUnionModelName;
                }

                app.aCEquifaxModelT = Tools.MapCreditModelStringToEnum(app.aCEquifaxModelName);
                if (app.aCEquifaxModelT == E_CreditScoreModelT.Other)
                {
                    app.aCEquifaxModelTOtherDescription = app.aCEquifaxModelName;
                }

                app.aCExperianModelT = Tools.MapCreditModelStringToEnum(app.aCExperianModelName);
                if (app.aCExperianModelT == E_CreditScoreModelT.Other)
                {
                    app.aCExperianModelTOtherDescription = app.aCExperianModelName;
                }

                app.aCTransUnionModelT = Tools.MapCreditModelStringToEnum(app.aCTransUnionModelName);
                if (app.aCTransUnionModelT == E_CreditScoreModelT.Other)
                {
                    app.aCTransUnionModelTOtherDescription = app.aCTransUnionModelName;
                }
            }

            return true;
        }
    }
}
