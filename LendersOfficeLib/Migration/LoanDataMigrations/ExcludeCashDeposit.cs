﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Migration;

    /// <summary>
    /// Migrator for OPM 236245. If successful, all loan files will automatically exclude the cash deposit from asset totals regardless of the temp broker bit.
    /// </summary>
    public class ExcludeCashDeposit : LoanDataMigration
    {
        /// <summary>
        /// The application fields to display.
        /// </summary>
        private List<FieldValueItem> applicationFields;

        /// <summary>
        /// The conditions to check before running this migration.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcludeCashDeposit"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who ran the migration.</param>
        /// <param name="userId">The id of the user who ran the migration.</param>
        /// <param name="brokerId">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is being ran as a system migration.</param>
        /// <param name="dataLoan">The data loan to apply this migration to.</param>
        public ExcludeCashDeposit(string userName, Guid userId, Guid brokerId, bool isSystemMigration, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystemMigration, dataLoan)
        {
            this.applicationFields = this.InitializeApplicationfields();
            this.conditions = this.InitializeConditions();
        }

        /// <summary>
        /// Gets the version number that the loan will be in if this migration is successful.
        /// </summary>
        /// <value>The associated version number.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V7_ExcludeCashDeposit_Opm236245;
            }
        }

        /// <summary>
        /// Gets the name for this migration.
        /// </summary>
        /// <value>The name for this migration.</value>
        public override string Name
        {
            get
            {
                return "Do not include earnest money deposits in asset totals";
            }
        }

        /// <summary>
        /// Gets the application fields to display.
        /// </summary>
        /// <value>The application fields to display.</value>
        protected override IEnumerable<FieldValueItem> ApplicationFieldItems
        {
            get
            {
                return this.applicationFields;
            }
        }

        /// <summary>
        /// Gets the conditions to check before running this migration.
        /// </summary>
        /// <value>The conditions to check before running this migration.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Performs the actual migration.
        /// There is no action to do for this migration. It simply needs to update the migration bit, which is automatically done.
        /// </summary>
        /// <param name="errors">Any errors that should be logged.</param>
        /// <returns>Always returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = new List<string>();
            return true;
        }

        /// <summary>
        /// Will update each asset collection after the version number has been incremented.
        /// </summary>
        protected override void PerformAfterVersionUpdate()
        {
            for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
            {
                CAppData appData = this.DataLoan.GetAppData(appIndex);
                appData.aAssetCollection.Flush();
            }
        }

        /// <summary>
        /// Initializes the conditions for this migration.
        /// </summary>
        /// <returns>A list of conditions to check before running this migration.</returns>
        private List<PreRunFailureCondition> InitializeConditions()
        {
            var conditions = new List<PreRunFailureCondition>();

            var batchMigrationCondition = new PreRunFailureCondition(() =>
            {
                if (this.DataLoan.sIsSystemBatchMigration && this.DataLoan.sTotCashDeposit != 0)
                {
                    return new List<string>() { "Migration cannot be run as a system batch migration." };
                }

                return null;
            });

            conditions.Add(batchMigrationCondition);

            return conditions;
        }

        /// <summary>
        /// Initializes the app fields to display.
        /// </summary>
        /// <returns>A list of app fields to display.</returns>
        private List<FieldValueItem> InitializeApplicationfields()
        {
            var appFields = new List<FieldValueItem>();

            appFields.Add(new FieldValueItem("aAsstLiqTot", "Total liquid assets", (loan, app) => app.aAsstLiqTot_rep));
            appFields.Add(new FieldValueItem("aAsstValTot", "Total assets", (loan, app) => app.aAsstValTot_rep));
            appFields.Add(new FieldValueItem("aNetWorth", "Net worth", (loan, app) => app.aNetWorth_rep));

            return appFields;
        }
    }
}
