﻿namespace LendersOffice.Migration.LoanDataMigrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Correct the FHA/VA Ufmip/FF paidn in cash to result in whole dollar total loan amount.
    /// </summary>
    public class CorrectFhaVaUfmipPaidInCash : LoanDataMigration
    {
        /// <summary>
        /// The loan items to display.
        /// </summary>
        private List<FieldValueItem> loanItems;

        /// <summary>
        /// The conditions to run.
        /// </summary>
        private List<PreRunFailureCondition> conditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectFhaVaUfmipPaidInCash"/> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystem">Whether this is a system operation.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        public CorrectFhaVaUfmipPaidInCash(string userName, Guid userId, Guid brokerId, bool isSystem, CPageData dataLoan)
            : base(userName, userId, brokerId, isSystem, dataLoan)
        {
            this.loanItems = this.InitializeLoanFields();
            this.conditions = this.InitializeFailureConditions();
        }

        /// <summary>
        /// Gets the version this loan will be in after the migration is ran.
        /// </summary>
        /// <value>The version this loan will be in after the migration is ran.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V6_CorrectFhaVaUfmipPaidInCash_Opm237060;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Correct FHA/VA UFMIP/FF paid in cash to result in whole dollar total loan amount";
            }
        }

        /// <summary>
        /// Gets the loan items to display.
        /// </summary>
        /// <value>The failure conditions to run.</value>
        protected override IEnumerable<FieldValueItem> LoanFileFieldItems
        {
            get
            {
                return this.loanItems;
            }
        }

        /// <summary>
        /// Gets the failure conditions to run.
        /// </summary>
        /// <value>The failure conditions to run.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.conditions;
            }
        }

        /// <summary>
        /// Performs the migration. This migration only enables a new calculation, so nothing has to be done here.
        /// </summary>
        /// <param name="errors">Any errors to log.</param>
        /// <returns>Always returns true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Constructs the loan field items to display.
        /// </summary>
        /// <returns>The list of loan field items to display.</returns>
        private List<FieldValueItem> InitializeLoanFields()
        {
            return new List<FieldValueItem>()
            {
                new FieldValueItem("sLT", "Loan Type", (loan, app) => Tools.ConvertsLtToString(loan.sLT)),
                new FieldValueItem("sFfUfMipIsBeingFinanced", "Is UFMIP/FF being financed?", (loan, app) => loan.sFfUfMipIsBeingFinanced.ToString()),
                new FieldValueItem("sFfUfmip1003", "Total UFMIP / FF", (loan, app) => loan.sFfUfmip1003_rep),
                new FieldValueItem("sFfUfmipFinanced", "UFMIP/FF financed amount", (loan, app) => loan.sFfUfmipFinanced_rep),
                new FieldValueItem("sUfCashPd", "Paid in Cash", (loan, app) => loan.sUfCashPd_rep),
                new FieldValueItem("sFinalLAmt", "Total Loan Amount", (loan, app) => loan.sFinalLAmt_rep)
            };
        }

        /// <summary>
        /// Constructs the failure conditions to run.
        /// </summary>
        /// <returns>The list of failure conditions to run.</returns>
        private List<PreRunFailureCondition> InitializeFailureConditions()
        {
            return new List<PreRunFailureCondition>()
            {
                new PreRunFailureCondition(() =>
                {
                    if (this.DataLoan.sIsSystemBatchMigration && this.DataLoan.sFfUfMipIsBeingFinanced &&
                        (this.DataLoan.sLT == E_sLT.FHA || this.DataLoan.sLT == E_sLT.VA) && 
                        this.DataLoan.sFinalLAmt % 1 != 0)
                    {
                        return new List<string>() { "Migration cannot be run as a system batch migration" };
                    }

                    return null;
                })
            };
        }
    }
}
