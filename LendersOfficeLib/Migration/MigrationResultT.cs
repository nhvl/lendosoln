﻿namespace LendersOffice.Migration
{
    /// <summary>
    /// Possible migration results.
    /// </summary>
    public enum MigrationResultT
    {
        /// <summary>
        /// There was something wrong even starting the process.
        /// </summary>
        NoAttempt = 0,

        /// <summary>
        /// One of the conditions was triggered so the migration was not started.
        /// </summary>
        FailureConditionTriggered = 1,

        /// <summary>
        /// Something went wrong in the migration.
        /// </summary>
        MigrationError = 2,

        /// <summary>
        /// Migration went through without any problems.
        /// </summary>
        Success = 3,

        /// <summary>
        /// The migration was attempted without a loan.
        /// </summary>
        NoDataLoan = 4,

        /// <summary>
        /// The loan did not have the correct version number to perform the actual migration.
        /// </summary>
        IncorrectVersionForMigration = 5,

        /// <summary>
        /// The loan did not have the correct version number to check the conditions for failure.
        /// </summary>
        IncorrectVersionForConditions = 6
    }
}
