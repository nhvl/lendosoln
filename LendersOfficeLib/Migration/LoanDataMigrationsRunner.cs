﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration.LoanDataMigrations;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to run a whole bunch of migrations.
    /// </summary>
    public class LoanDataMigrationsRunner
    {
        /// <summary>
        /// The max number of migrations to run at a time.
        /// </summary>
        private const int MaxMigrationsToRun = 10;

        /// <summary>
        /// Whether this migration runner is being ran as part of a system batch migration.
        /// </summary>
        private bool isSystemBatchMigration = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationsRunner"/> class.
        /// </summary>
        /// <param name="userName">The name of the user who will run these migrations.</param>
        /// <param name="userId">The id of the user who will run these migrations.</param>
        /// <param name="loanId">The loan ID to run the migrations on.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="isSystemBatchMigration">Whether this migration runner is being ran as part of a system batch migration.</param>
        public LoanDataMigrationsRunner(string userName, Guid userId, Guid loanId, Guid brokerId, bool isSystemBatchMigration)
        {
            this.UserName = userName;
            this.UserId = userId;
            this.LoanId = loanId;
            this.BrokerId = brokerId;
            this.isSystemBatchMigration = isSystemBatchMigration;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationsRunner"/> class.
        /// </summary>
        /// <param name="principal">The principal to get user info from.</param>
        /// <param name="loanId">The id of the loan to migrate.</param>
        /// <param name="isSystemBatchMigration">Whether this migration runner is being ran as part of a system batch migration.</param>
        public LoanDataMigrationsRunner(AbstractUserPrincipal principal, Guid loanId, bool isSystemBatchMigration)
            : this(principal.DisplayName, principal.UserId, loanId, principal.BrokerId, isSystemBatchMigration)
        {
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the user who ran these migrations.
        /// </summary>
        /// <value>The name of the user who ran these migrations.</value>
        public string UserName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the user who ran these migrations.
        /// </summary>
        /// <value>The id of the user who ran these migrations.</value>
        public Guid UserId
        {
            get;
            private set;
        }

        /// <summary>
        /// Method with new DB call syntax, extracted for unit testing.
        /// </summary>
        /// <param name="brokerId">The brokerId.</param>
        /// <param name="selectStatement">The select statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The current loan version.</returns>
        public static int? SelectLoanVersion(Guid brokerId, string selectStatement, SqlParameter[] parameters)
        {
            int? loanVersion = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    loanVersion = (int)reader["sLoanVersionT"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, selectStatement, null, parameters, readHandler);
            return loanVersion;
        }

        /// <summary>
        /// Gets the migration associated with the version number.
        /// </summary>
        /// <param name="version">The version number to use to get the migration.</param>
        /// <returns>
        /// A tuple. The first item contains the type of the migration. 
        /// The second item is a function that takes in a data loan. This function will call the constructor for the migration.
        /// </returns>
        /// <remarks>
        /// There's a good chance that a data loan made using SmartDependency cannot be passed into the function returned here. So only use the manually made data loan objects.
        /// </remarks>
        public Tuple<Type, Func<CPageData, LoanDataMigration>> GetMigrationbyVersionNumber(LoanVersionT version)
        {
            switch (version)
            {
                case LoanVersionT.V1_CalcVaLoanElig_Opm204251:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CalculateVaLoanElig),
                        loan => new CalculateVaLoanElig(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V2_CalcVaFundFeeExempt_Opm204251:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CalculateVAFfExempt),
                        loan => new CalculateVAFfExempt(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V3_ConsolidateMaxPrepaymentPenaltyFields_Opm238278:
                    return Tuple.Create<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConsolidatePrepaymentPenaltyFields),
                        loan => new ConsolidatePrepaymentPenaltyFields(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V4_ForceSinglePmtUfmipTypeForFhaVaUsda_Opm237060:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ForceSinglePmtAsUfmipTypeForFhaVaUsda),
                        loan => new ForceSinglePmtAsUfmipTypeForFhaVaUsda(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V5_CorrectUsdaGuaranteeFeeOffTotalLoanAmt_Opm237060:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CorrectUsdaGuaranteeFeeOffTotalLoanAmt),
                        loan => new CorrectUsdaGuaranteeFeeOffTotalLoanAmt(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V6_CorrectFhaVaUfmipPaidInCash_Opm237060:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CorrectFhaVaUfmipPaidInCash),
                        loan => new CorrectFhaVaUfmipPaidInCash(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V7_ExcludeCashDeposit_Opm236245:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ExcludeCashDeposit),
                        loan => new ExcludeCashDeposit(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V8_EnforceCreditsEnteredAsPositiveNumbers_Opm225917:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(EnforceCreditsEnteredAsPositiveNumbers),
                        loan => new EnforceCreditsEnteredAsPositiveNumbers(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V9_UpdatePerDiemInterestStartDateCalc_Opm179289:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(UpdatePerDiemInterestStartDateCalculation),
                        loan => new UpdatePerDiemInterestStartDateCalculation(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V10_HideOtherHousingExpenses_Opm235669:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(HideOtherHousingExpenses),
                        loan => new HideOtherHousingExpenses(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V11_ConsolidateFHACaseNumberFields_Opm186897:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConsolidateFHACaseNumberFields),
                        loan => new ConsolidateFHACaseNumberFields(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V12_CalculateLevelOfPropertyReviewSection:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CalculateLevelOfPropertyReviewSection),
                        loan => new CalculateLevelOfPropertyReviewSection(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V13_LimitAdjustmentLength:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(LimitAdjustmentLength),
                        loan => new LimitAdjustmentLength(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V14_PreventNegativeAprRelatedClosingCosts_Opm235692:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(PreventNegativeAprRelatedClosingCosts),
                        loan => new PreventNegativeAprRelatedClosingCosts(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V15_CorrectVaLoanHousingExpCalculation_Opm246907:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CorrectVaLoanHousingExpCalculation),
                        loan => new CorrectVaLoanHousingExpCalculation(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V16_EnforceDisbursementMonthTotalExpenses:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(EnforceExpenseDisbursementSchedTotal),
                        loan => new EnforceExpenseDisbursementSchedTotal(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V17_UpdateApplicationSubmittedCalculation:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(UpdateApplicationSubmittedDateCalc),
                        loan => new UpdateApplicationSubmittedDateCalc(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V18_ConsolidateTaxIdFields:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConsolidateTaxIdFields),
                        loan => new ConsolidateTaxIdFields(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V19_PopulateApplicationLevelCreditModelNames:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(PopulateApplicationLevelCreditModelNames),
                        loan => new PopulateApplicationLevelCreditModelNames(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V20_RemoveBankersRoundingFromSomeMiCalcs:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(RemoveBankersRoundingFromSomeMiCalcs),
                        loan => new RemoveBankersRoundingFromSomeMiCalcs(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V21_DontTreatNegativeFeePaymentAmountsAsFinanceCharges:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(DontTreatNegativeFeePaymentAmountsAsFinanceCharges),
                        loan => new DontTreatNegativeFeePaymentAmountsAsFinanceCharges(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V22_HmdaDataPointImprovement:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(HmdaDataPointImprovement),
                        loan => new HmdaDataPointImprovement(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V23_CorrectReservesCalcForFhlmcAndTotal:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(CorrectReservesCalculationsForFhlmcAndTotal),
                        loan => new CorrectReservesCalculationsForFhlmcAndTotal(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ApplyPredefinedAdjustments),
                        loan => new ApplyPredefinedAdjustments(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConsolidateMonthlyChildSupportPayments),
                        loan => new ConsolidateMonthlyChildSupportPayments(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ReplaceFreeformCreditModelsWithEnums),
                        loan => new ReplaceFreeformCreditModelsWithEnums(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V27_RequireValidApplicantTinFormats:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(RequireValidApplicantTinFormats),
                        loan => new RequireValidApplicantTinFormats(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V28_DontRollSundayHolidaysToMonday:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(DontRollSundayHolidaysToMonday),
                        loan => new DontRollSundayHolidaysToMonday(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V29_StopGuessingRescindability:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(StopGuessingRescindability),
                        loan => new StopGuessingRescindability(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V30_PassthroughMigration:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(PassthroughMigration),
                        loan => new PassthroughMigration(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan, LoanVersionT.V30_PassthroughMigration));
                case LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConstructionLoans_ExposeNewDataPoints),
                        loan => new ConstructionLoans_ExposeNewDataPoints(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V32_ConstructionLoans_CalcChanges:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(ConstructionLoans_CalcChanges),
                        loan => new ConstructionLoans_CalcChanges(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet:
                    return new Tuple<Type, Func<CPageData, LoanDataMigration>>(
                        typeof(IncludeNonBorrowerResponsibleFeesInFundingWorksheetMigration),
                        loan => new IncludeNonBorrowerResponsibleFeesInFundingWorksheetMigration(this.UserName, this.UserId, this.BrokerId, this.isSystemBatchMigration, loan));
                case LoanVersionT.Version0:
                    return null;
                default:
                    throw new UnhandledEnumException(version);
            }
        }

        /// <summary>
        /// Runs a set of migrations and produces a view model for UI. No saving.
        /// The most number of migrations that will be run at once is dictated by <see cref="MaxMigrationsToRun"/>.
        /// If the system version is beyond that number, this will only perform <see cref="MaxMigrationsToRun"/> migrations.
        /// </summary>
        /// <returns>The model for the UI.</returns>
        public LoanDataMigrationViewModel RunMigrationsToDisplay()
        {
            return this.RunMigrationsToDisplay(LoanDataMigrationUtils.GetLatestVersion());
        }

        /// <summary>
        /// Runs a set of migrations and produces a view model for UI. This does not save.
        /// The most number of migrations that will be run at once is dictated by <see cref="MaxMigrationsToRun"/>.
        /// If the specified final version is beyond that number, this will only perform <see cref="MaxMigrationsToRun"/> migrations.
        /// </summary>
        /// <param name="finalVersion">A specified version to migration up to.</param>
        /// <returns>A model for the UI.</returns>
        public LoanDataMigrationViewModel RunMigrationsToDisplay(LoanVersionT finalVersion)
        {
            int currentLoanVersionAsInt = (int)this.GetCurrentLoanVersion();
            int targetVersionAsInt = (int)finalVersion;
            int maxVersionAsInt = Math.Min(currentLoanVersionAsInt + MaxMigrationsToRun, (int)LoanDataMigrationUtils.GetLatestVersion());
            int lastVersionForDdlAsInt = maxVersionAsInt;
            LoanDataMigrationViewModel model;

            if (targetVersionAsInt > (int)LoanDataMigrationUtils.GetLatestVersion())
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot have a target version greater than the latest system version.");
            }

            if (currentLoanVersionAsInt == targetVersionAsInt)
            {
                // We are already on the target version.
                CPageData dataLoan = new NotEnforceAccessControlPageData(this.LoanId, new List<string> { "sLoanDataMigrationAuditHistory" });
                dataLoan.InitLoad();
                model = new LoanDataMigrationViewModel(new List<LoanDataMigrationResult>(), dataLoan.sLoanDataMigrationAuditHistory);
                model.AvailableVersions.Add((LoanVersionT)currentLoanVersionAsInt);
            }
            else
            {
                if (targetVersionAsInt > maxVersionAsInt)
                {
                    targetVersionAsInt = maxVersionAsInt;
                }

                List<LoanDataMigrationResult> pastResults = null;
                List<LoanDataMigrationResult> results = this.MigrateLoanToVersion(currentLoanVersionAsInt, targetVersionAsInt, out pastResults);
                model = new LoanDataMigrationViewModel(results, pastResults);
            }
            
            for (int index = currentLoanVersionAsInt + 1; index <= lastVersionForDdlAsInt; index++)
            {
                model.AvailableVersions.Add((LoanVersionT)index);
            }

            model.CurrentVersion = (LoanVersionT)currentLoanVersionAsInt;
            model.TargetVersion = (LoanVersionT)targetVersionAsInt;

            return model;
        }

        /// <summary>
        /// Apply the migrations and attempt to save.
        /// </summary>
        /// <param name="targetVersion">The version to migrate to.</param>
        /// <param name="shouldSaveAtEnd">Whether we only save once all migrations have successfully completed or if we attempt to save after every success migration.</param>
        /// <param name="canUpdateToMax">Whether or not we can apply migrations up to the latest system version, regardless of how far behind a loan is.</param>
        /// <param name="failedMigrationNumber">The associated loan version of the migration that failed.</param>
        /// <returns>True if all the migrations were successful, false otherwise.</returns>
        public bool ApplyMigrations(LoanVersionT targetVersion, bool shouldSaveAtEnd, bool canUpdateToMax, out LoanVersionT? failedMigrationNumber)
        {
            failedMigrationNumber = null;
            int currentLoanVersionAsInt = (int)this.GetCurrentLoanVersion();
            int targetVersionAsInt = (int)targetVersion;
            int maxVersionAsInt = canUpdateToMax ? (int)LoanDataMigrationUtils.GetLatestVersion() : Math.Min(currentLoanVersionAsInt + MaxMigrationsToRun, (int)LoanDataMigrationUtils.GetLatestVersion());

            if (targetVersionAsInt > maxVersionAsInt)
            {
                throw new CBaseException(ErrorMessages.Generic, "Target loan version must be less than or equal to version " + maxVersionAsInt);
            }

            if (currentLoanVersionAsInt == targetVersionAsInt)
            {
                return true;
            }

            List<LoanDataMigrationResult> results = this.MigrateLoanToVersionAndSave(currentLoanVersionAsInt, targetVersionAsInt, shouldSaveAtEnd: shouldSaveAtEnd);
            var success = results.TrueForAll(result => result.ResultStatus == MigrationResultT.Success);
            if (!success)
            {
                var lastResult = results.LastOrDefault((result) => result.ResultStatus != MigrationResultT.Success);
                if (lastResult != null)
                {
                    failedMigrationNumber = lastResult.AssociatedVersion;
                }
            }

            return success;
        }

        /// <summary>
        /// Gets the current loan file version.
        /// </summary>
        /// <returns>The loan file version.</returns>
        public LoanVersionT GetCurrentLoanVersion()
        {
            string selectStatement = "SELECT sLoanVersionT FROM LOAN_FILE_B WHERE sLId=@sLId";
            SqlParameter[] parameters =
            {
                new SqlParameter("@sLId", this.LoanId)
            };

            int? loanVersion = SelectLoanVersion(this.BrokerId, selectStatement, parameters);

            if (loanVersion.HasValue && Enum.IsDefined(typeof(LoanVersionT), loanVersion.Value))
            {
                return (LoanVersionT)loanVersion;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "sLoanVersionT for this loan is not found or is invalid.");
            }
        }

        /// <summary>
        /// Runs the migrations and saves, depending on the shouldSaveAtEnd parameter.
        /// </summary>
        /// <param name="currentVersion">The current version of the loan.</param>
        /// <param name="finalVersion">The target version.</param>
        /// <param name="shouldSaveAtEnd">Whether we only save once all migrations have successfully completed or if we attempt to save after every success migration.</param>
        /// <returns>The list of migration results.</returns>
        private List<LoanDataMigrationResult> MigrateLoanToVersionAndSave(int currentVersion, int finalVersion, bool shouldSaveAtEnd)
        {
            List<string> dependentFields;
            List<Func<CPageData, LoanDataMigration>> migrationConstructors = this.PrepareMigrations(currentVersion, finalVersion, out dependentFields);

            CPageData dataLoan = new NotEnforceAccessControlPageData(this.LoanId, dependentFields);
            dataLoan.DisableFieldEnforcement();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sLoanVersionT != (LoanVersionT)currentVersion)
            {
                throw new CBaseException(ErrorMessages.Generic, "The loan is not on specified current version <" + currentVersion + ">");
            }

            List<LoanDataMigrationResult> migrationResults = new List<LoanDataMigrationResult>();
            bool anyMigrationFailed = false;
            bool shouldRecreate = false;
            for (int index = 0; index < migrationConstructors.Count; index++)
            {
                // This is a bit weird. In order to be able to save after every migration finished, we need to call InitSave again.
                // However, due to all the private member variables we have the loan, it's probably better to just recreate the entire object.
                if (shouldRecreate)
                {
                    dataLoan = new NotEnforceAccessControlPageData(this.LoanId, dependentFields);
                    dataLoan.DisableFieldEnforcement();
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                }

                LoanDataMigration migration = migrationConstructors[index](dataLoan);
                LoanDataMigrationResult result;
                if (!shouldSaveAtEnd)
                {
                    result = migration.ApplyFullMigration();
                    shouldRecreate = true;
                }
                else
                {
                    result = migration.PreviewFullMigration();
                }

                migrationResults.Add(result);

                // Don't go any further once a migration has failed.
                if (result.ResultStatus != MigrationResultT.Success)
                {
                    anyMigrationFailed = true;
                    break;
                }
            }

            if (!anyMigrationFailed && shouldSaveAtEnd)
            {
                dataLoan.Save();
            }

            return migrationResults;
        }

        /// <summary>
        /// Attempts to run the migrations to move the loan to the specified final version. Will not save the data loan.
        /// </summary>
        /// <param name="currentVersion">The version that the loan should be in prior to these migrations.</param>
        /// <param name="finalVersion">The version the loan should be in after these migrations are finished running.</param>
        /// <param name="migrationAuditHistory">The results of the migrations that have been ran in the past.</param>
        /// <returns>The list of migration results.</returns>
        private List<LoanDataMigrationResult> MigrateLoanToVersion(int currentVersion, int finalVersion, out List<LoanDataMigrationResult> migrationAuditHistory)
        {
            List<string> dependentFields;
            List<Func<CPageData, LoanDataMigration>> migrationConstructors = this.PrepareMigrations(currentVersion, finalVersion, out dependentFields);

            CPageData dataLoan = new NotEnforceAccessControlPageData(this.LoanId, dependentFields);
            dataLoan.DisableFieldEnforcement();
            dataLoan.InitLoad();

            if (dataLoan.sLoanVersionT != (LoanVersionT)currentVersion)
            {
                throw new CBaseException(ErrorMessages.Generic, "The loan is not on specified current version <" + currentVersion + ">");
            }

            migrationAuditHistory = new List<LoanDataMigrationResult>();
            foreach (LoanDataMigrationResult auditResult in dataLoan.sLoanDataMigrationAuditHistory.OrderByDescending(result => result.AssociatedVersion).Take(MaxMigrationsToRun))
            {
                migrationAuditHistory.Add((LoanDataMigrationResult)auditResult.Clone());
            }

            List<LoanDataMigrationResult> migrationResults = new List<LoanDataMigrationResult>();
            for (int index = 0; index < migrationConstructors.Count; index++)
            {
                LoanDataMigration migration = migrationConstructors[index](dataLoan);

                // We can let this run all the migrations. If one migration fails, the later ones can still check their failure conditions.
                // If the later ones' conditions don't trigger, there is still a stop to prevent the actual migration from running if the previous was not successful.
                LoanDataMigrationResult result = migration.PreviewFullMigration();
                migrationResults.Add(result);
            }

            return migrationResults;
        }

        /// <summary>
        /// Makes sure the input versions are valid.
        /// Gets the migrations to run based on those versions.
        /// Gets the fields needed to run those migrations.
        /// </summary>
        /// <param name="currentVersion">The current version.</param>
        /// <param name="targetVersion">The version to migrate to.</param>
        /// <param name="dependentFields">The fields that are needed to run the set of migrations.</param>
        /// <returns>Returns a list of functions that take in a data loan object. The functions will contain the contructors for the migrations to run.</returns>
        private List<Func<CPageData, LoanDataMigration>> PrepareMigrations(int currentVersion, int targetVersion, out List<string> dependentFields)
        {
            LoanVersionT latestVersion = LoanDataMigrationUtils.GetLatestVersion();

            if (targetVersion < 0 || currentVersion < 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Specified versions are negative.");
            }

            if (targetVersion > (int)latestVersion || currentVersion >= (int)latestVersion)
            {
                throw new CBaseException(ErrorMessages.Generic, "Specified versions cannot be greater than the last defined loan version.");
            }

            if (targetVersion <= currentVersion)
            {
                throw new CBaseException(ErrorMessages.Generic, "The specified final version is less than or equal to the passed in current version.");
            }

            List<Func<CPageData, LoanDataMigration>> migrationConstructors = new List<Func<CPageData, LoanDataMigration>>();
            dependentFields = new List<string>(LoanDataMigration.ConstantFieldsForLoanDataMigration);

            // We want to run all migrations at the same time without making a new loan everytime.
            // So we need to gather all the dependencies for each migration and use all of them to make a single loan.
            for (int version = currentVersion + 1; version <= targetVersion; version++)
            {
                if (Enum.IsDefined(typeof(LoanVersionT), version))
                {
                    Tuple<Type, Func<CPageData, LoanDataMigration>> migrationContainer = this.GetMigrationbyVersionNumber((LoanVersionT)version);
                    dependentFields.AddRange(CPageData.GetCPageBaseAndCAppDataDependencyList(migrationContainer.Item1));
                    migrationConstructors.Add(migrationContainer.Item2);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Version <" + version + "> is not a valid loan migration version.");
                }
            }

            return migrationConstructors;
        }
    }
}
