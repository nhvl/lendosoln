﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Migration that will enforce that the disbursement schedules for the expenses and MI total up to 12 months.
    /// </summary>
    public class EnforceExpenseDisbursementSchedTotal : LoanDataMigration
    {
        /// <summary>
        /// The collection items to display.
        /// </summary>
        private IEnumerable<CollectionItem> collectionItems;

        /// <summary>
        /// The failure conditions to check.
        /// </summary>
        private IEnumerable<PreRunFailureCondition> failureConditions;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnforceExpenseDisbursementSchedTotal"/> class.
        /// </summary>
        /// <param name="username">The user name of the user who performed the migration.</param>
        /// <param name="userid">The user id of the user.</param>
        /// <param name="brokerid">The broker id for the user.</param>
        /// <param name="isSystemMigration">Whether this is a system migration or not.</param>
        /// <param name="dataLoan">The data loan to migrate.</param>
        public EnforceExpenseDisbursementSchedTotal(string username, Guid userid, Guid brokerid, bool isSystemMigration, CPageData dataLoan)
            : base(username, userid, brokerid, isSystemMigration, dataLoan)
        {
            this.collectionItems = this.InitializeCollectionItems();
            this.failureConditions = this.InitializeFailureConditions();
        }

        /// <summary>
        /// Gets the associated loan version number.
        /// </summary>
        /// <value>The associated loan version number.</value>
        public override LoanVersionT AssociatedVersion
        {
            get
            {
                return LoanVersionT.V16_EnforceDisbursementMonthTotalExpenses;
            }
        }

        /// <summary>
        /// Gets the name of the migration.
        /// </summary>
        /// <value>The name of the migration.</value>
        public override string Name
        {
            get
            {
                return "Enforce disbursement months to add up to 12";
            }
        }

        /// <summary>
        /// Gets the collection items to track.
        /// </summary>
        /// <value>The collection items to track.</value>
        protected override IEnumerable<CollectionItem> CollectionItems
        {
            get
            {
                return this.collectionItems;
            }
        }

        /// <summary>
        /// Gets the conditions to check.
        /// </summary>
        /// <value>The conditions to check.</value>
        protected override IEnumerable<PreRunFailureCondition> FailureConditions
        {
            get
            {
                return this.failureConditions;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not this migration will only display the old values in the UI.
        /// </summary>
        /// <value>Returns true.</value>
        protected override bool OnlyShowOldValues
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Performs the migration. This does nothing.
        /// </summary>
        /// <param name="errors">No errors will be returned.</param>
        /// <returns>Will always return true.</returns>
        protected override bool PerformMigration(out List<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        /// Initializes the failure conditions.
        /// </summary>
        /// <returns>The list of conditions to check.</returns>
        private IEnumerable<PreRunFailureCondition> InitializeFailureConditions()
        {
            List<PreRunFailureCondition> failureConditions = new List<PreRunFailureCondition>();
            if (this.DataLoan == null)
            {
                return failureConditions;
            }

            failureConditions.Add(new PreRunFailureCondition(this.CheckMiDisbursementSchedule));
            failureConditions.Add(new PreRunFailureCondition(this.CheckExpenseDisbursementSchedule));

            return failureConditions;
        }

        /// <summary>
        /// Checks if the MI disbursement schedule is valid.
        /// </summary>
        /// <returns>A list of error messages if the MI schedule isn't valid. Null or empty otherwise.</returns>
        private List<string> CheckMiDisbursementSchedule()
        {
            if (this.DataLoan == null)
            {
                return null;
            }

            if (this.DataLoan.sHousingExpenses.MIEscrowSchedule.Skip(1).Sum() != 12)
            {
                return new List<string>() { "Manually entered disbursement months for MI must add up to 12." };
            }

            return null;
        }

        /// <summary>
        /// Checks if the expense schedules are valid.
        /// </summary>
        /// <returns>Returns a list of invalid expenses if there are any.</returns>
        private List<string> CheckExpenseDisbursementSchedule()
        {
            if (this.DataLoan == null)
            {
                return null;
            }

            List<string> errors = new List<string>();
            foreach (BaseHousingExpense expense in this.DataLoan.sHousingExpenses.ExpensesToUse)
            {
                if (expense.DisbursementScheduleMonths.Skip(1).Sum() != 12)
                {
                    errors.Add($"Manually entered disbursement months for {expense.ExpenseDescription} must add up to 12.");
                }
            }

            return errors;
        }

        /// <summary>
        /// Initializes the collection items.
        /// This keeps track of the expenses and their disbursement schedules and the MI schedule.
        /// </summary>
        /// <returns>The collection items to track.</returns>
        private IEnumerable<CollectionItem> InitializeCollectionItems()
        {
            List<CollectionItem> collectionItems = new List<CollectionItem>();

            if (this.DataLoan == null)
            {
                return collectionItems;
            }

            foreach (BaseHousingExpense expense in this.DataLoan.sHousingExpenses.ExpensesToUse)
            {
                E_HousingExpenseTypeCalculatedT expenseId = expense.CalculatedHousingExpenseType;
                string expenseIdString = expenseId.ToString("D");
                string equivalentField = string.Empty;
                HousingExpenses.ExpenseToFieldMap.TryGetValue(expenseId, out equivalentField);
                int[] disbursementSchedule = expense.DisbursementScheduleMonths;

                List<FieldValueItem> disbursementSchedMonths = new List<FieldValueItem>()
                {
                    new FieldValueItem($"{expenseIdString}_Jan", "Disbursement Schedule - January", (loan, app) => disbursementSchedule[1].ToString()),
                    new FieldValueItem($"{expenseIdString}_Feb", "Disbursement Schedule - February", (loan, app) => disbursementSchedule[2].ToString()),
                    new FieldValueItem($"{expenseIdString}_Mar", "Disbursement Schedule - March", (loan, app) => disbursementSchedule[3].ToString()),
                    new FieldValueItem($"{expenseIdString}_Apr", "Disbursement Schedule - April", (loan, app) => disbursementSchedule[4].ToString()),
                    new FieldValueItem($"{expenseIdString}_May", "Disbursement Schedule - May", (loan, app) => disbursementSchedule[5].ToString()),
                    new FieldValueItem($"{expenseIdString}_Jun", "Disbursement Schedule - June", (loan, app) => disbursementSchedule[6].ToString()),
                    new FieldValueItem($"{expenseIdString}_Jul", "Disbursement Schedule - July", (loan, app) => disbursementSchedule[7].ToString()),
                    new FieldValueItem($"{expenseIdString}_Aug", "Disbursement Schedule - August", (loan, app) => disbursementSchedule[8].ToString()),
                    new FieldValueItem($"{expenseIdString}_Sep", "Disbursement Schedule - September", (loan, app) => disbursementSchedule[9].ToString()),
                    new FieldValueItem($"{expenseIdString}_Oct", "Disbursement Schedule - October", (loan, app) => disbursementSchedule[10].ToString()),
                    new FieldValueItem($"{expenseIdString}_Nov", "Disbursement Schedule - November", (loan, app) => disbursementSchedule[11].ToString()),
                    new FieldValueItem($"{expenseIdString}_Dec", "Disbursement Schedule - December", (loan, app) => disbursementSchedule[12].ToString()),
                };

                collectionItems.Add(new CollectionItem(expense.ExpenseDescription, equivalentField, "Expense", false, null, disbursementSchedMonths));
            }

            int[] miDisbSched = this.DataLoan.sHousingExpenses.MIEscrowSchedule;
            List<FieldValueItem> miSchedMonths = new List<FieldValueItem>()
            {
                new FieldValueItem($"MI_Jan", "Disbursement Schedule - January", (loan, app) => miDisbSched[1].ToString()),
                new FieldValueItem($"MI_Feb", "Disbursement Schedule - February", (loan, app) => miDisbSched[2].ToString()),
                new FieldValueItem($"MI_Mar", "Disbursement Schedule - March", (loan, app) => miDisbSched[3].ToString()),
                new FieldValueItem($"MI_Apr", "Disbursement Schedule - April", (loan, app) => miDisbSched[4].ToString()),
                new FieldValueItem($"MI_May", "Disbursement Schedule - May", (loan, app) => miDisbSched[5].ToString()),
                new FieldValueItem($"MI_Jun", "Disbursement Schedule - June", (loan, app) => miDisbSched[6].ToString()),
                new FieldValueItem($"MI_Jul", "Disbursement Schedule - July", (loan, app) => miDisbSched[7].ToString()),
                new FieldValueItem($"MI_Aug", "Disbursement Schedule - August", (loan, app) => miDisbSched[8].ToString()),
                new FieldValueItem($"MI_Sep", "Disbursement Schedule - September", (loan, app) => miDisbSched[9].ToString()),
                new FieldValueItem($"MI_Oct", "Disbursement Schedule - October", (loan, app) => miDisbSched[10].ToString()),
                new FieldValueItem($"MI_Nov", "Disbursement Schedule - November", (loan, app) => miDisbSched[11].ToString()),
                new FieldValueItem($"MI_Dec", "Disbursement Schedule - December", (loan, app) => miDisbSched[12].ToString()),
            };

            collectionItems.Add(new CollectionItem("Mortgage Insurance", "sMortgageInsurance", "MI", false, null, miSchedMonths));

            return collectionItems;
        }
    }
}
