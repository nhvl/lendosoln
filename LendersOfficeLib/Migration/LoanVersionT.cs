﻿namespace LendersOffice.Migration
{
    /// <summary>
    /// Version number used by the data layer migrations system.
    /// </summary>
    public enum LoanVersionT
    {
        /// <summary>
        /// The first version.
        /// </summary>
        Version0 = 0,

        /// <summary>
        /// Migration for case 204251. Enables the calculations for the aIsVaElig fields.
        /// </summary>
        V1_CalcVaLoanElig_Opm204251 = 1,

        /// <summary>
        /// Migration for case 204251. Enables the calculations for sVaFfExemptTri field.
        /// </summary>
        V2_CalcVaFundFeeExempt_Opm204251 = 2,

        /// <summary>
        /// Consolidate sQMMaxPrePmntPenalty, sGfeMaxPpmtPenaltyAmt.
        /// </summary>
        V3_ConsolidateMaxPrepaymentPenaltyFields_Opm238278 = 3,

        /// <summary>
        /// Introduces a calculation to sMipFrequency. 
        /// Forces sMipFrequency to 'Single Payment' if the loan type is FHA, VA, or USDA.
        /// </summary>
        V4_ForceSinglePmtUfmipTypeForFhaVaUsda_Opm237060 = 4,

        /// <summary>
        /// Corrects the USDA Guaranty Fee calculation to be based off the actual total loan amount.
        /// </summary>
        V5_CorrectUsdaGuaranteeFeeOffTotalLoanAmt_Opm237060 = 5,

        /// <summary>
        /// Correct the FHA/VA Ufmip/FF paidn in cash to result in whole dollar total loan amount.
        /// </summary>
        V6_CorrectFhaVaUfmipPaidInCash_Opm237060 = 6,

        /// <summary>
        /// Migration for case 236245. Will always exclude the cash deposit from asset totals.
        /// </summary>
        V7_ExcludeCashDeposit_Opm236245 = 7,

        /// <summary>
        /// Ensure that the lender credit amounts and cures are entered as positive numbers.
        /// </summary>
        V8_EnforceCreditsEnteredAsPositiveNumbers_Opm225917 = 8,

        /// <summary>
        /// Take lender business days into consideration in calculation of per diem interest start date.
        /// </summary>
        V9_UpdatePerDiemInterestStartDateCalc_Opm179289 = 9,

        /// <summary>
        /// Fixes up how we show the "Other Housing" expenses.
        /// </summary>
        V10_HideOtherHousingExpenses_Opm235669 = 10,

        /// <summary>
        /// Consolidate sSpFhaCaseNumCurrentLoan and sFHAPreviousCaseNum, which represent the same datapoint.
        /// </summary>
        V11_ConsolidateFHACaseNumberFields_Opm186897 = 11,
        
        /// <summary>
        /// Converts the Level of Property Review section of the 1008 to be calculated from the
        /// Property Valuation Method and Appraisal Form Type of the Subject Property Details page.
        /// </summary>
        V12_CalculateLevelOfPropertyReviewSection = 12,

        /// <summary>
        /// Limits the adjustment length to 60 characters.
        /// </summary>
        V13_LimitAdjustmentLength = 13,

        /// <summary>
        /// Ensure that total APR-related closing costs are not negative.
        /// </summary>
        V14_PreventNegativeAprRelatedClosingCosts_Opm235692 = 14,

        /// <summary>
        /// Corrects the calculation of the VA Loan Non-PI Housing Expense fields.
        /// </summary>
        V15_CorrectVaLoanHousingExpCalculation_Opm246907 = 15,

        /// <summary>
        /// Enforces that the total disbursment months is 12.
        /// </summary>
        V16_EnforceDisbursementMonthTotalExpenses = 16,

        /// <summary>
        /// Updates the application date calculation.
        /// </summary>
        V17_UpdateApplicationSubmittedCalculation = 17,

        /// <summary>
        /// Consolidates sFHASponsoredOriginatorEin and sSponsoredOriginatorEIN.
        /// </summary>
        V18_ConsolidateTaxIdFields = 18,

        /// <summary>
        /// Populates credit model names at the application level from the on-file credit report.
        /// </summary>
        V19_PopulateApplicationLevelCreditModelNames = 19,

        /// <summary>
        /// Gets rid of bankers rounding for initial and renewal monthly MIP and LPMI.
        /// </summary>
        V20_RemoveBankersRoundingFromSomeMiCalcs = 20,

        /// <summary>
        /// Prevents treating negative fee payments as finance charges.
        /// </summary>
        V21_DontTreatNegativeFeePaymentAmountsAsFinanceCharges = 21,

        /// <summary>
        /// Migrates HMDA Action Taken and HMDA Denial Reasons from string fields to enum fields.
        /// </summary>
        V22_HmdaDataPointImprovement = 22,

        /// <summary>
        /// Corrects the reserves calculations for FHLMC and TOTAL reserves fields.
        /// </summary>
        V23_CorrectReservesCalcForFhlmcAndTotal = 23,

        /// <summary>
        /// Migrates user typed adjustments into predefined adjustments for a loan.
        /// </summary>
        V24_PredefinedDescriptionsForLoanAdjustments = 24,

        /// <summary>
        /// Updates the calculation of child support payments to use the child support
        /// value entered in the liability collection for a loan file.
        /// </summary>
        V25_ConsolidateMonthlyChildSupportPayments = 25,

        /// <summary>
        /// Migrates the datapoints a[B|C][Experian|TransUnion|Equifax]ModelName to be replaced
        /// by a[B|C][Experian|TransUnion|Equifax]ModelT, which use an enum instead of freeform text.
        /// </summary>
        V26_ReplaceFreeformCreditModelsWithEnums = 26,

        /// <summary>
        /// Requires users to enter correctly formatted SSN (XXX-XX-XXXX) 
        /// or EIN (XX-XXXXXXX) values.
        /// </summary>
        V27_RequireValidApplicantTinFormats = 27,

        /// <summary>
        /// If true, calculated dates will no longer count the Monday
        /// after a holiday that fell on a Sunday as a holiday.
        /// </summary>
        V28_DontRollSundayHolidaysToMonday = 28,

        /// <summary>
        /// Fix loan fields that were guessing rescindability incorrectly
        /// by using sLoanRescindabilityT field.
        /// </summary>
        V29_StopGuessingRescindability = 29,

        /// <summary>
        /// This migration has been pulled.
        /// We will leave it in, but it won't do anything.
        /// </summary>
        V30_PassthroughMigration = 30,

        /// <summary>
        /// Exposes new data points to the UI and pre populates them to make migrating to the next version easier.
        /// </summary>
        V31_ConstructionLoans_ExposeNewDataPoints = 31,

        /// <summary>
        /// This migration gates off calculation changes made to existing loan file fields
        /// as part of our Construction Loan overhaul.
        /// </summary>
        V32_ConstructionLoans_CalcChanges = 32,

        /// <summary>
        /// This migration includes non-borrower responsible fees to be included in the funding worksheet.
        /// </summary>
        V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet = 33
    }
}
