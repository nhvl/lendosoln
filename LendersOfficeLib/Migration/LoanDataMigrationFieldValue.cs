﻿namespace LendersOffice.Migration
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class that contains the old and potentially new value of a field.
    /// </summary>
    [DataContract]
    public class LoanDataMigrationFieldValue : ICloneable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationFieldValue"/> class.
        /// </summary>
        /// <param name="fieldId">The Id of the field that this is keeping track of.</param>
        /// <param name="friendlyName">The friendly name of the field.</param>
        public LoanDataMigrationFieldValue(string fieldId, string friendlyName)
        {
            this.FieldId = fieldId;
            this.FriendlyName = friendlyName;
        }

        /// <summary>
        /// Gets the field's id.
        /// </summary>
        /// <value>The field's id.</value>
        [DataMember]
        public string FieldId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the field's friendly name for display.
        /// </summary>
        /// <value>The field's friendly name for display.</value>
        [DataMember]
        public string FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the old value of the field.
        /// </summary>
        /// <value>The old value of the field.</value>
        [DataMember]
        public string OldValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the new value of the field.
        /// </summary>
        /// <value>The new value of the field.</value>
        [DataMember]
        public string NewValue
        {
            get;
            set;
        }

        /// <summary>
        /// Clones the values in this object.
        /// </summary>
        /// <returns>The cloned object.</returns>
        public object Clone()
        {
            LoanDataMigrationFieldValue clonedValues = new LoanDataMigrationFieldValue(this.FieldId, this.FriendlyName);
            clonedValues.OldValue = this.OldValue;
            clonedValues.NewValue = this.NewValue;

            return clonedValues;
        }
    }
}
