﻿namespace LendersOffice.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// The model that will be used to populate the Data Layer migration page.
    /// </summary>
    [DataContract]
    public class LoanDataMigrationViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationViewModel"/> class.
        /// </summary>
        /// <param name="results">The results used to create this view model.</param>
        /// <param name="pastResults">The results of migrations that have already been ran.</param>
        public LoanDataMigrationViewModel(List<LoanDataMigrationResult> results, List<LoanDataMigrationResult> pastResults)
        {
            this.DisplayRollback = false;
            this.AvailableVersions = new List<LoanVersionT>();
            this.NewestVersion = LoanDataMigrationUtils.GetLatestVersion();

            this.MigrationResults = this.CreateMigrationResultsViewModel(results);
            this.MigrationAuditHistory = this.CreateMigrationResultsViewModel(pastResults);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataMigrationViewModel"/> class.
        /// </summary>
        public LoanDataMigrationViewModel()
            : this(new List<LoanDataMigrationResult>(), new List<LoanDataMigrationResult>())
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether we should display the rollback feature.
        /// </summary>
        /// <value>Whether we should display the rollback feature.</value>
        [DataMember]
        public bool DisplayRollback
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the virtual root.
        /// </summary>
        /// <value>The virtual root.</value>
        [DataMember]
        public string VRoot
        {
            get
            {
                return Tools.VRoot;
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets the loan's current version.
        /// </summary>
        /// <value>The loan's current version.</value>
        [DataMember]
        public LoanVersionT CurrentVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the loan's target version.
        /// </summary>
        /// <value>The loan's target version.</value>
        [DataMember]
        public LoanVersionT TargetVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of versions that this loan can be bulk migrated to.
        /// </summary>
        /// <value>A list of versions that this loan can be bulk migrated to.</value>
        [DataMember]
        public List<LoanVersionT> AvailableVersions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the absolute newest version for the entire system.
        /// </summary>
        /// <value>The absolute newest version for the entire system.</value>
        [DataMember]
        public LoanVersionT NewestVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of migration result view models.
        /// </summary>
        /// <value>A list of migration result view models.</value>
        [DataMember]
        public List<LoanDataMigrationResultViewModel> MigrationResults
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether all migrations are successful.
        /// </summary>
        /// <value>A value indicating whether all migrations are successful.</value>
        [DataMember]
        public bool AreAllMigrationsSuccessful
        {
            get
            {
                if (this.MigrationResults != null && this.MigrationResults.Count > 0)
                {
                    return this.MigrationResults.TrueForAll(resultModel => resultModel.Result == MigrationResultT.Success);
                }
                else
                {
                    return false;
                }
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets a list of migration results that have already been ran.
        /// </summary>
        /// <value>A list of migration results that have already been ran.</value>
        [DataMember]
        public List<LoanDataMigrationResultViewModel> MigrationAuditHistory
        {
            get;
            set;
        }

        /// <summary>
        /// Creates the migration result view models.
        /// </summary>
        /// <param name="results">The results to use to create the migration result view models.</param>
        /// <returns>The migration result view models.</returns>
        private List<LoanDataMigrationResultViewModel> CreateMigrationResultsViewModel(List<LoanDataMigrationResult> results)
        {
            List<LoanDataMigrationResultViewModel> viewModel = new List<LoanDataMigrationResultViewModel>();
            foreach (LoanDataMigrationResult result in results)
            {
                LoanDataMigrationResultViewModel model = new LoanDataMigrationResultViewModel(result);
                viewModel.Add(model);
            }

            return viewModel;
        }
    }
}
