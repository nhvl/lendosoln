﻿namespace LendersOffice.Migration
{
    using System;
    using System.Linq;

    /// <summary>
    /// Utility functions for the loan data migration framework.
    /// </summary>
    public static class LoanDataMigrationUtils
    {
        /// <summary>
        /// Gets the max value in the LoanVersionT enum. This will be the latest system version.
        /// </summary>
        /// <returns>The max value in the LoanVersionT enum.</returns>
        public static LoanVersionT GetLatestVersion()
        {
            return Enum.GetValues(typeof(LoanVersionT)).Cast<LoanVersionT>().Max();
        }

        /// <summary>
        /// Checks if the specified loan version is greater than or equal to the target version.
        /// </summary>
        /// <param name="loanVersion">The loan's version enum.</param>
        /// <param name="targetVersion">The absolute minimum loan version for this to be true.</param>
        /// <returns>True if the loan version is greater than or equal to the target version. False otherwise.</returns>
        public static bool IsOnOrBeyondLoanVersion(LoanVersionT loanVersion, LoanVersionT targetVersion)
        {
            return (int)loanVersion >= (int)targetVersion;
        }
    }
}
