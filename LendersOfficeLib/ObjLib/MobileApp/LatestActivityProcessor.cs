﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using LendersOffice.Constants;
using CommonProjectLib.Common;
using System.Xml.Linq;
using LendersOffice.Security;
using System.Threading;

namespace LendersOffice.MobileApp
{
    public class LatestActivityProcessor : CommonProjectLib.Runnable.IRunnable
    {
        #region IRunnable Members

        public string Description
        {
            get
            {
                return "LatestActivityProcessor - " + ConstStage.MSMQ_MobileLoanActivity;
            }
        }

        public void Run()
        {
            using (var queue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_MobileLoanActivity, false))
            {
                Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
                while (true)
                {
                    try
                    {
                        DateTime arrivalTime;
                        XDocument xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);
                        if (xdoc == null)
                        {
                            break; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                        }

                        DataAccess.Tools.LogInfo("[LatestActivityProcessor] xdoc: " + xdoc);
                        LatestActivity.UpdateLatestActivityList(xdoc);
                    }
                    catch (MessageQueueException e)
                    {
                        if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                        {
                            break;
                        }
                        throw;
                    }
                }
            }
        }

        #endregion
    }
}
