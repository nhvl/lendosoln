﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Xml;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using System.Messaging;
using CommonProjectLib.Common;
using LendersOffice.Security;

namespace LendersOffice.MobileApp
{
    public static class LatestActivity
    {
        private const string MSG_TYPE_ACTIVITY = "Activity";
        private const string MSG_TYPE_STATUS = "Status";

        //4/11/2014 bs - Add activity type for mobile app v2.4
        public static void Send(Guid sLId, string activity, string activityType)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_MobileLoanActivity) == true)
            {
                return;
            }

            XDocument xdoc = new XDocument(new XElement("msg",
                new XAttribute("sLId", sLId.ToString()),
                new XAttribute("type", MSG_TYPE_ACTIVITY),
                new XAttribute("date", DateTime.Now.ToString("MM/dd/yyyy")),
                new XAttribute("desc", activity),
                new XAttribute("activityT", activityType)
            ));

            Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_MobileLoanActivity, null, xdoc);
        }

        public static void UpdateStatus(Guid sLId)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_MobileLoanActivity) == true)
            {
                return;
            }

            XDocument xdoc = new XDocument(new XElement("msg",
                new XAttribute("sLId", sLId.ToString()),
                new XAttribute("type", MSG_TYPE_STATUS)
                ));

            Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_MobileLoanActivity, null, xdoc);
        }

        private static XElement GenerateStatusDateXml(Guid sLId, out DateTime latestDate)
        {
            MobileStatusWithSecurityByPassData dataLoan = new MobileStatusWithSecurityByPassData(sLId);
            dataLoan.InitLoad();

            //4/11/2014 bs - add activity type and new fields for v2.4 spec
            /*Activity type: 0 - null
                             1 - Task
             *               2 - Condition
             *               3 - Status
             *               4 - Lock Status
             *               5 - Event
             *               6 - Document
             *               7 - EDoc
             *               8 - Lock Exp
             *               9 - Doc Exp
             *               10 - GFE
             *               11 - TIL
             */
            var list = new[] {
                new { FieldId="sRLckdD", Description="Rate Lock", Value=dataLoan.sRLckdD_rep, ActivityType="4"},
                new { FieldId="sRLckdExpiredD", Description="Rate Lock Expiration", Value=dataLoan.sRLckdExpiredD_rep, ActivityType="8"},
                new { FieldId="sLeadD", Description="Lead New", Value=dataLoan.sLeadD_rep, ActivityType="3"},
                new { FieldId="sOpenedD", Description="Loan Open", Value=dataLoan.sOpenedD_rep, ActivityType="3"},
                new { FieldId="sPreQualD", Description="Pre-qual", Value=dataLoan.sPreQualD_rep, ActivityType="3"},
                new { FieldId="sSubmitD", Description="Registered", Value=dataLoan.sSubmitD_rep, ActivityType="3"},
                new { FieldId="sProcessingD", Description="Processing", Value=dataLoan.sProcessingD_rep, ActivityType="3"},
                new { FieldId="sPreApprovD", Description="Pre-approved", Value=dataLoan.sPreApprovD_rep, ActivityType="3"},
                new { FieldId="sEstCloseD", Description="Estimated Closing", Value=dataLoan.sEstCloseD_rep, ActivityType="3"},
                new { FieldId="sUnderwritingD", Description="In Underwriting", Value=dataLoan.sUnderwritingD_rep, ActivityType="3"},
                new { FieldId="sApprovD", Description="Approved", Value=dataLoan.sApprovD_rep, ActivityType="3"},
                new { FieldId="sFinalUnderwritingD", Description="Final Underwriting", Value=dataLoan.sFinalUnderwritingD_rep, ActivityType="3"},
                new { FieldId="sClearToCloseD", Description="Clear To Close", Value=dataLoan.sClearToCloseD_rep, ActivityType="3"},
                new { FieldId="sDocsD", Description="Docs Out", Value=dataLoan.sDocsD_rep, ActivityType="3"},
                new { FieldId="sDocsBackD", Description="Docs Back", Value=dataLoan.sDocsBackD_rep, ActivityType="3"},
                new { FieldId="sFundingConditionsD", Description="Scheduled Funding", Value=dataLoan.sFundingConditionsD_rep, ActivityType="3"},
                new { FieldId="sFundD", Description="Funded", Value=dataLoan.sFundD_rep, ActivityType="3"},
                new { FieldId="sRecordedD", Description="Recorded", Value=dataLoan.sRecordedD_rep, ActivityType="3"},
                new { FieldId="sFinalDocsD", Description="Final Docs", Value=dataLoan.sFinalDocsD_rep, ActivityType="3"},
                new { FieldId="sClosedD", Description="Loan Closed", Value=dataLoan.sClosedD_rep, ActivityType="3"},
                new { FieldId="sOnHoldD", Description="Loan On-Hold", Value=dataLoan.sOnHoldD_rep, ActivityType="3"},
                new { FieldId="sCanceledD", Description="Loan Canceled", Value=dataLoan.sCanceledD_rep, ActivityType="3"},
                new { FieldId="sRejectD", Description="Loan Denied", Value=dataLoan.sRejectD_rep, ActivityType="3"},
                new { FieldId="sSuspendedD", Description="Loan Suspended", Value=dataLoan.sSuspendedD_rep, ActivityType="3"},
                new { FieldId="sShippedToInvestorD", Description="Loan Shipped", Value=dataLoan.sShippedToInvestorD_rep, ActivityType="3"},
                new { FieldId="sLPurchaseD", Description="Loan Sold", Value=dataLoan.sLPurchaseD_rep, ActivityType="3"},
                new { FieldId="sGoodByLetterD", Description="Good-bye Letter Date", Value=dataLoan.sGoodByLetterD_rep, ActivityType="3"},
                new { FieldId="sServicingStartD", Description="Servicing Start Date", Value=dataLoan.sServicingStartD_rep, ActivityType="3"},
                new { FieldId="sU1LStatD", Description=dataLoan.sU1LStatDesc, Value=dataLoan.sU1LStatD_rep, ActivityType="5"},
                new { FieldId="sU2LStatD", Description=dataLoan.sU2LStatDesc, Value=dataLoan.sU2LStatD_rep, ActivityType="5"},
                new { FieldId="sU3LStatD", Description=dataLoan.sU3LStatDesc, Value=dataLoan.sU3LStatD_rep, ActivityType="5"},
                new { FieldId="sU4LStatD", Description=dataLoan.sU4LStatDesc, Value=dataLoan.sU4LStatD_rep, ActivityType="5"},
                new { FieldId="sPrelimRprtOd", Description="Preliminary Report", Value=dataLoan.sPrelimRprtOd_rep, ActivityType="6"},
                new { FieldId="sApprRprtOd", Description="Appraisal Report", Value=dataLoan.sApprRprtOd_rep, ActivityType="6"},
                new { FieldId="sAppSubmittedD", Description="App Submitted", Value=dataLoan.sAppSubmittedD_rep, ActivityType="6"},
                new { FieldId="sGfeInitialDisclosureD", Description="GFE Initial Disclosure Date", Value=dataLoan.sGfeInitialDisclosureD_rep, ActivityType="10"},
                new { FieldId="sGfeRedisclosureD", Description="GFE Redisclosure Date", Value=dataLoan.sGfeRedisclosureD_rep, ActivityType="10"},
                new { FieldId="sTilInitialDisclosureD", Description="TIL Initial Disclosure Date", Value=dataLoan.sTilInitialDisclosureD_rep, ActivityType="11"},
                new { FieldId="sTilRedisclosureD", Description="TIL Redisclosure Date", Value=dataLoan.sTilRedisclosureD_rep, ActivityType="11"},
                new { FieldId="sApprRprtExpD", Description="Appraisal Report Expiration Date", Value=dataLoan.sApprRprtExpD_rep, ActivityType="9"},
                new { FieldId="sIncomeDocExpD", Description="Income Doc Expiration Date", Value=dataLoan.sIncomeDocExpD_rep, ActivityType="9"},
                new { FieldId="sCrExpD", Description="Credit Report Expiration Date", Value=dataLoan.sCrExpD_rep, ActivityType="9"},
                new { FieldId="sAssetExpD", Description="Asset Doc Expiration Date", Value=dataLoan.sAssetExpD_rep, ActivityType="9"},
                new { FieldId="sBondDocExpD", Description="Bond Document Expiration Date", Value=dataLoan.sBondDocExpD_rep, ActivityType="9"}
            };

            XElement root = new XElement("Statuses");
            latestDate = DateTime.MinValue;
            string latestActivityType = null;
            foreach (var item in list)
            {
                if (string.IsNullOrEmpty(item.Value) == false)
                {
                    DateTime dt;
                    if (DateTime.TryParse(item.Value, out dt) == true)
                    {
                        if ((item.ActivityType.Equals("8")) || (item.ActivityType.Equals("9")))
                        {
                            if ((dt - DateTime.Now).Days <= 5)
                            {
                                dt = DateTime.Now;
                            }
                            else
                            {
                                dt = DateTime.MinValue;
                            }
                        }
                        if (dt >= latestDate)
                        {
                            latestDate = dt;
                            latestActivityType = item.ActivityType;
                        }
                    }
                    root.Add(new XElement("item",
                        new XAttribute("id", item.FieldId),
                        new XAttribute("desc", item.Description),
                        new XAttribute("date", item.Value),
                        new XAttribute("activityT", item.ActivityType)
                        ));
                }
            }
            root.SetAttributeValue("status_latest_date", latestDate.ToString("MM/dd/yyyy"));
            root.SetAttributeValue("status_latest_activityType", latestActivityType);

            DataAccess.Tools.LogInfo("[GenerateStatusDateXml] root for sLId: " + sLId.ToString() + ". " + root.ToString());
            return root;

        }


        internal static void UpdateLatestActivityList(XDocument msg)
        {
            if (null == msg)
            {
                return;
            }

            XElement el = msg.Element("msg");

            Guid sLId = new Guid(el.Attribute("sLId").Value);

            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            XDocument xdoc = null;
            XElement statusElement = null;
            XElement activityElement = null;

            DateTime statusLatestDate = DateTime.MinValue;
            DateTime activityLatestDate = DateTime.MinValue;
            int statusLatestActivityType = 0;
            int activityLatestActivityType = 0;

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLoanFileActivity", parameters))
            {
                if (reader.Read()) 
                {
                    string xmlContent = (string) reader["XmlContent"];
                    if (string.IsNullOrEmpty(xmlContent) == false) 
                    {
                        ///Summary
                        ///xmlContent will look like the following format:
                        ///<root version="1" status_latest_date="....." activity_latest_date="....">
                        ///     <Statuses status_latest_date="...." status_latest_activityType="...">
                        ///         <item id="...." desc="...." date="...." activityT="...." />
                        ///         <item id="...." desc="...." date="...." activityT="...." />
                        ///     </Statuses>
                        ///     <Activities activity_latest_date="...." activity_latest_type="....">
                        ///         <item date="...." desc=".... TaskId = ...." activityT="...." />
                        ///     </Activities>
                        ///</root>
                        xdoc = XDocument.Parse(xmlContent);
                    }
                }
            }

            if (null != xdoc)
            {
                statusElement = xdoc.Root.Element("Statuses");
                activityElement = xdoc.Root.Element("Activities");

                if (xdoc.Root.Attribute("status_latest_date") != null)
                {
                    if (DateTime.TryParse(xdoc.Root.Attribute("status_latest_date").Value, out statusLatestDate) == false)
                    {
                        statusLatestDate = DateTime.MinValue;
                    }
                }
                if (xdoc.Root.Attribute("activity_latest_date") != null)
                {
                    if (DateTime.TryParse(xdoc.Root.Attribute("activity_latest_date").Value, out activityLatestDate) == false)
                    {
                        activityLatestDate = DateTime.MinValue;
                    }
                }
            }
            else
            {
                xdoc = new XDocument(new XElement("root",
                    new XAttribute("version", "1")));

            }
            string type = el.Attribute("type").Value;

            if (statusElement == null)
            {
                statusElement = GenerateStatusDateXml(sLId, out statusLatestDate);
                xdoc.Root.Add(statusElement);
            }
            else if (statusElement.Attribute("status_latest_activityType") != null)
            {
                Int32.TryParse(statusElement.Attribute("status_latest_activityType").Value, out statusLatestActivityType);
            }

            if (activityElement == null)
            {
                activityElement = new XElement("Activities");
                xdoc.Root.Add(activityElement);
            }
            else if (activityElement.Attribute("activity_latest_type") != null)
            {
                Int32.TryParse(activityElement.Attribute("activity_latest_type").Value, out activityLatestActivityType);
            }

            if (type == MSG_TYPE_STATUS)
            {
                // Refresh Status.
                statusElement.ReplaceWith(GenerateStatusDateXml(sLId, out statusLatestDate));
            }
            else if (type == MSG_TYPE_ACTIVITY)
            {
                // Refresh Activity.
                string dateDesc = el.Attribute("date").Value;
                string desc = el.Attribute("desc").Value;
                string activityType = el.Attribute("activityT").Value;
                
                DateTime dt;

                if (DateTime.TryParse(dateDesc, out dt) == true)
                {
                    if (dt >= activityLatestDate)
                    {
                        activityLatestDate = dt;
                        Int32.TryParse(activityType, out activityLatestActivityType);
                    }
                }
                activityElement.SetAttributeValue("activity_latest_date", activityLatestDate.ToString("MM/dd/yyyy"));
                activityElement.SetAttributeValue("activity_latest_type", activityLatestActivityType);

                activityElement.Add(new XElement("item",
                    new XAttribute("date", dateDesc),
                    new XAttribute("desc", desc), 
                    new XAttribute("activityT", activityType)));
            }

            DateTime latestActivityDate = DateTime.MinValue;
            int latestActivityType = 0;

            //DataAccess.Tools.LogInfo("[UpdateLatestActivityList] statusElement: " + statusElement.ToString());
            if (statusElement.Attribute("status_latest_date") != null)
            {
                DateTime.TryParse(statusElement.Attribute("status_latest_date").Value, out statusLatestDate);
            }
            if (statusElement.Attribute("status_latest_activityType") != null)
            {
                Int32.TryParse(statusElement.Attribute("status_latest_activityType").Value, out statusLatestActivityType);
            }
            
            if (statusLatestDate > activityLatestDate)
            {
                latestActivityDate = statusLatestDate;
                latestActivityType = statusLatestActivityType;
            }
            else
            {
                latestActivityDate = activityLatestDate;
                latestActivityType = activityLatestActivityType;
            }

            xdoc.Root.SetAttributeValue("status_latest_date", statusLatestDate.ToString("MM/dd/yyyy"));
            xdoc.Root.SetAttributeValue("activity_latest_date", activityLatestDate.ToString("MM/dd/yyyy"));

            // Save to database
            parameters = new SqlParameter[] {
                new SqlParameter("@sLId", sLId),
                new SqlParameter("@XmlContent", xdoc.ToString(SaveOptions.DisableFormatting)),
                new SqlParameter("@sLatestActivityDateForMobileApp", latestActivityDate),
                new SqlParameter("@sLatestActivityType", latestActivityType)};

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateLoanFileActivity", 3, parameters);
        }

        public static string GenerateLoanActivityXml(Guid sLId, AbstractUserPrincipal principal)
        {
            XDocument xdoc = null;

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveLoanFileActivity", parameters))
            {
                if (reader.Read())
                {
                    string xmlContent = (string)reader["XmlContent"];
                    if (string.IsNullOrEmpty(xmlContent) == false)
                    {
                        xdoc = XDocument.Parse(xmlContent);
                    }
                }
            }

            XElement statusElement = null;
            XElement activityElement = null;
            if (null != xdoc)
            {
                statusElement = xdoc.Root.Element("Statuses");
                activityElement = xdoc.Root.Element("Activities");
            }

            if (statusElement == null)
            {
                // 11/30/2011 dd - If not status date then pull from loan file.
                DateTime dt;
                statusElement = GenerateStatusDateXml(sLId, out dt);
            }

            List<KeyValuePair<DateTime, string>> list = new List<KeyValuePair<DateTime, string>>();
            if (statusElement != null)
            {
                foreach (var item in statusElement.Elements("item"))
                {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(item.Attribute("date").Value, out dt);
                    string activityType = null;
                    if (item.Attribute("activityT") != null)
                        activityType = item.Attribute("activityT").Value;
                    else
                        activityType = "0";
                    list.Add(new KeyValuePair<DateTime, string>(dt, "Activity Type: " + activityType + ". Description: " + item.Attribute("desc").Value));
                }
            }

            if (activityElement != null)
            {
                foreach (var item in activityElement.Elements("item"))
                {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(item.Attribute("date").Value, out dt);
                    string activityType = null;
                    if (item.Attribute("activityT") != null)
                        activityType = item.Attribute("activityT").Value;
                    else
                        activityType = "0";
                    list.Add(new KeyValuePair<DateTime, string>(dt, "Activity Type: " + activityType + ". Description: " + item.Attribute("desc").Value));
                }
            }

            // 4/16/2014 bs - List all edocs access by current user (mobile spec v2.4).
            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == true)
            {
                EDocs.EDocumentRepository repository = EDocs.EDocumentRepository.GetUserRepository();

                var eDocList = repository.GetDocumentsByLoanId(sLId);
                foreach (var eDocs in eDocList)
                {
                    list.Add(new KeyValuePair<DateTime, string>(eDocs.CreatedDate, "Activity Type: 7. Description: " + eDocs.CopiedFromFileName));
                }
            }

            XDocument resultXDoc = new XDocument(new XElement("list"));
            foreach (var item in list.OrderByDescending(o => o.Key))
            {
                resultXDoc.Root.Add(new XElement("item",
                    new XAttribute("date", item.Key.ToString("MM/dd/yyyy")),
                    new XAttribute("description", item.Value)));
            }

            return resultXDoc.ToString(SaveOptions.DisableFormatting);
        }

    }
}
