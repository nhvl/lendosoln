﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using System.Collections;

namespace LendersOffice.ObjLib.CapitalMarkets
{
    public enum E_TradeType
    {
        TBA,
        CallOption,
        PutOption
    }
    public enum E_TradeDirection
    {
        Buy,
        Sell
    }
    public enum TradeStatus
    {
        Open,
        Closed
    }

    public static class TradeExtensions
    {
        public static IEnumerable<Trade> Filter(this IEnumerable<Trade> trades, TradeFilter filter)
        {
            return from t in trades where filter.Matches(t) select t;
        }

        public static IEnumerable<TradeDTO> ToDTO(this IEnumerable<Trade> trades)
        {
            return from t in trades select new TradeDTO(t);
        }

        public static TradeDTO ToDTO(this Trade t)
        {
            return new TradeDTO(t);
        }

        public static void ApplyTransactions(this Trade me, IEnumerable<TransactionDTO> newTransactions)
        {
            var transactions = me.GetTransactions().ToDictionary(a => a.TransactionId.ToString().ToLower());
            foreach (var t in newTransactions)
            {
                var id = (t.TransactionId ?? "").ToLower();

                Transaction editing;
                if (!transactions.TryGetValue(id, out editing))
                {
                    editing = new Transaction(me);
                }

                if (t.Deleted)
                {
                    editing.Delete();
                    continue;
                }
                
                //Don't need to save if we haven't changed.
                if (t.Matches(editing)) continue; 

                editing.ApplyDTO(t);
                if(!t.Deleted) editing.Save();
            }
        }
    }

    public abstract class TradeFilter
    {
        public abstract Guid BrokerId { get; }
        public abstract TradeStatus? Status { get; }
        public abstract string TradeNum { get; }
        public abstract bool TradeNumPartialMatch { get; }
        public abstract E_TradeType? Type { get; }
        public abstract E_TradeDirection? Direction { get; }
        public abstract int? Month { get; }
        public abstract Guid? DescriptionId { get; }
        public abstract string Coupon { get; }
        public abstract bool FilterTradesInPools { get; }
        public abstract DateTime? SettlementDateLowerBound { get; }
        public abstract DateTime? SettlementDateUpperBound { get; }

        private static bool MaybePartialMatch(string mine, string theirs, bool doPartial)
        {
            //Don't want to try and trim a null string
            if (mine == null || string.IsNullOrEmpty(mine.TrimWhitespaceAndBOM())) return true;

            if(doPartial) return theirs.Contains(mine);

            return theirs.Equals(mine, StringComparison.CurrentCultureIgnoreCase);
        }

        private static bool MatchOrNull<T>(T mine, T other)
        {
            if (mine == null) return true;

            return mine.Equals(other);
        }

        private static bool MatchOrNull<T>(T? mine, T other) where T : struct
        {
            if (!mine.HasValue) return true;

            return mine.Value.Equals(other);
        }

        private static bool MatchOrNull(Guid? mine, Guid other)
        {
            if (!mine.HasValue) return true;

            return mine.Value == other;
        }

        private static bool MatchOrNull(DateTime? lowerBound, DateTime? upperBound, DateTime? other)
        {
            if (!lowerBound.HasValue && !upperBound.HasValue) return true;
            else if (!other.HasValue) return false;
            else if (lowerBound.HasValue && !upperBound.HasValue) return other.Value >= lowerBound.Value;
            else if (!lowerBound.HasValue && upperBound.HasValue) return other.Value <= upperBound.Value;
            else return other.Value >= lowerBound.Value && other.Value <= upperBound.Value;
        }

        public bool Matches(Trade other)
        {
            return this.BrokerId == other.BrokerId &&
                    MaybePartialMatch(this.TradeNum, other.TradeNum_rep, TradeNumPartialMatch) &&
                    MatchOrNull(this.Status, other.Status) &&
                    MatchOrNull(this.Type, other.Type) &&
                    MatchOrNull(this.Direction, other.Direction) &&
                    MatchOrNull(this.Month, other.Month) &&
                    MatchOrNull(this.DescriptionId, other.DescriptionObj.DescriptionId) &&
                    MaybeFilterTradesInPools(FilterTradesInPools, other.AssociatedPoolId) &&
                    MaybePartialMatch(this.Coupon, other.Coupon_rep, true) &&
                    MatchOrNull(this.SettlementDateLowerBound, this.SettlementDateUpperBound, other.SettlementDate);
        }

        private static bool MaybeFilterTradesInPools(bool filter, long? poolId)
        {
            return !filter || !poolId.HasValue;
        }
    }

    public class TradeGroup
    {
        public IEnumerable<Trade> Trades { get; private set; }
        public string Header { get; private set; }

        public TradeGroup(string header, IEnumerable<Trade> trades)
        {
            Header = header;
            Trades = trades;
        }
    }

    public class TradeDTO
    {
        public readonly Guid TradeId;
        public string TradeNum;
        public E_TradeType? Type;
        public E_TradeDirection? Direction;
        public string DealerInvestor;
        public string AssignedTo;
        public string Month;
        public string Description;
        public string Coupon;
        public string TradeDate;
        public string NotificationDate;
        public string SettlementDate;
        public string Amount;
        public string Price;
        public string TotalExpense;
        public string TotalGainLoss;
        public string OpenAmount;
        public string PreClosingAmount;
        public string Type_rep
        {
            get
            {
                return Type.HasValue ? Type.ToString() : "undefined";
            }
        }

        public string Direction_rep
        {
            get
            {
                return Direction.HasValue ? Direction.ToString() : "undefined";
            }
        }

        public readonly IEnumerable<TransactionDTO> Transactions;

        public TradeDTO(Trade other)
        {
            if (other == default(Trade)) return;

            this.TotalGainLoss = other.TotalGainLoss_rep;
            this.TotalExpense = other.TotalExpense_rep;
            this.Price = other.Price_rep;
            this.TradeId = other.TradeId;
            this.TradeNum = other.TradeNum_rep;
            this.Type = other.Type;
            this.Direction = other.Direction;
            this.DealerInvestor = other.DealerInvestor;
            this.AssignedTo = other.AssignedTo;
            this.Month = other.Month_rep;
            this.Description = other.Description_rep;
            this.Coupon = other.Coupon_rep;
            this.TradeDate = other.TradeDate_rep;
            this.NotificationDate = other.NotificationDate_rep;
            this.SettlementDate = other.SettlementDate_rep;
            this.Amount = other.Amount_rep;
            this.PreClosingAmount = other.PreClosingAmount.ToString();
            Transactions = other.Transactions.ToDTO();
        }

        public TradeDTO() { } //for serialization
    }

    public class TradeStatisticsDTO : TradeDTO 
    {
        public string LatestTradeDate
        {
            get
            {
                return base.TradeDate;
            }
        }

        public string EarliestNotificationDate
        {
            get
            {
                return base.NotificationDate;
            }
            }
        public string EarliestSettlementDate
        {
            get
            { 
                return base.SettlementDate; 
            }

        }

        public string WeightedAvgPrice
        {
            get
            {
                return base.Price;
            }
        }

        public TradeStatisticsDTO(IEnumerable<Trade> trades) : base(trades.FirstOrDefault())
        {
            decimal weightedPriceSum = 0;
            decimal gainLossSum = 0;
            decimal expenseSum = 0;
            decimal totalOpenAmount = 0;
            DateTime? latestTradeDate = DateTime.MinValue;
            DateTime? earliestNotifDate = DateTime.MaxValue;
            DateTime? earliestSettleDate = DateTime.MaxValue;
            DealerInvestor = null;

            foreach (var t in trades)
            {
                if (this.Type != t.Type) this.Type = null;
                if (this.Direction != t.Direction) this.Direction = null;
                if (this.DealerInvestor != t.DealerInvestor) this.DealerInvestor = "[Multiple]";
                if (this.AssignedTo != t.AssignedTo) this.AssignedTo = "";
                if (this.Month != t.Month_rep) this.Month = "";
                if (this.Description != t.Description_rep) this.Description = "";
                if (this.Coupon != t.Coupon_rep) this.Coupon = "";

                latestTradeDate = DateMax(latestTradeDate, t.TradeDate);
                earliestNotifDate = DateMin(earliestNotifDate, t.NotificationDate);
                earliestSettleDate = DateMin(earliestSettleDate, t.SettlementDate);

                weightedPriceSum += t.Amount * t.Price;
                gainLossSum += t.TotalGainLoss;
                expenseSum += t.TotalExpense;
                totalOpenAmount += t.Amount;
            }

            if (DealerInvestor == null) DealerInvestor = "[None]";
            if (latestTradeDate != DateTime.MinValue) TradeDate = latestTradeDate.Value.ToShortDateString();
            if (earliestNotifDate != DateTime.MaxValue) NotificationDate = earliestNotifDate.Value.ToShortDateString();
            if (earliestSettleDate != DateTime.MaxValue) SettlementDate = earliestSettleDate.Value.ToShortDateString();

            var converter = new LosConvert();
            if (totalOpenAmount != 0)
            {
                this.Price = converter.ToDecimalString(weightedPriceSum / Math.Abs(totalOpenAmount), FormatDirection.ToRep);
            }
            else
            {
                this.Price = "No open amount";
            }
            this.TotalExpense = converter.ToMoneyString(expenseSum, FormatDirection.ToRep);
            this.TotalGainLoss = converter.ToMoneyString(gainLossSum, FormatDirection.ToRep);
            this.Amount = converter.ToMoneyString(totalOpenAmount, FormatDirection.ToRep);
        }

        private static bool BothNull<T>(T? a, T? b, out T? value) where T : struct
        {
            value = null;
            if (!a.HasValue && !b.HasValue)
            {
                return true;
            }

            if (a.HasValue && !b.HasValue)
            {
                value = a;
                return true;
            }

            if (b.HasValue && !a.HasValue)
            {
                value = b;
                return true;
            }

            return false;
        }

        private static DateTime? DateMax(DateTime? a, DateTime? b)
        {
            DateTime? ret;
            if(BothNull(a, b, out ret)) return ret;

            return a > b ? a : b;
        }

        private static DateTime? DateMin(DateTime? a, DateTime? b)
        {
            DateTime? ret;
            if (BothNull(a, b, out ret)) return ret;

            return a < b ? a : b;
        }
    }

    
    public class Trade
    {
        private static string FETCHTRADE_SP = "MBS_FetchTrades";
        private static string FETCHTRADEBYNUM_SP = "MBS_FetchTradeByTradeNum";
        private static string UPDATETRADE_SP = "MBS_UpdateTrade";
        private static string CREATETRADE_SP = "MBS_CreateTrade";
        private static string DELETETRADE_SP = "MBS_DeleteTrade";
        private static string DELETEALLTRANSACTIONS_SP = "MBS_DeleteTransactionsByTradeId";
        private static string GETTRADECOUNTER_SP = "MBS_GetTradeCounter";

        #region Member variables
        private bool m_isNew;
        private LosConvert m_losConvert = new LosConvert();

        private Guid m_TradeId;
        private Guid m_BrokerId;
        private int? m_TradeNum;
        private string m_DealerInvestor;
        private string m_AssignedTo;
        private E_TradeType m_Type;
        private E_TradeDirection m_Direction;
        private int m_Month;
        private Description m_Description;
        private decimal m_Coupon;

        private bool m_IsAllocatedLoansCalculated;
        private decimal m_AllocatedLoans;
        private decimal m_Price;

        private DateTime? m_TradeDate;
        private DateTime? m_NotificationDate;
        private DateTime? m_SettlementDate;

        public long? AssociatedPoolId { get; set; }

        public List<Transaction> Transactions;

        public Guid TradeId
        {
            get { return m_TradeId; }
        }
        public Guid BrokerId
        {
            get { return m_BrokerId; }
        }
        public int? TradeNum
        {
            get { return m_TradeNum.Value; }
        }

        public string TradeNum_rep
        {
            get { return m_TradeNum.ToString(); }
            set { m_TradeNum = Convert.ToInt32(value); }
        }
        public string DealerInvestor
        {
            get { return m_DealerInvestor; }
            set { m_DealerInvestor = value; }
        }
        public string AssignedTo
        {
            get { return m_AssignedTo; }
            set { m_AssignedTo = value; }
        }
        public string Type_rep
        {
            get
            {
                return m_Type.ToString().Replace("Option", "");
            }
        }
        public E_TradeType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public E_TradeDirection Direction
        {
            get { return m_Direction; }
            set { m_Direction = value; }
        }
        public int Month
        {
            get { return m_Month; }
            set { m_Month = value; }
        }
        public string Month_rep
        {
            get
            {
                if (m_Month == 0)
                {
                    return "";
                }
                return Tools.GetMonthFromInt(m_Month);
            }
        }

        public string Types
        {
            get
            {
                return Direction + " " + Type_rep;
            }

        }

        public string Stats
        {
            get
            {
                return Description_rep + " " + Coupon_rep + " " + Month_rep;
            }
        }

        public Description DescriptionObj
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        public string Description_rep
        {
            get { return m_Description.DescriptionName; }
        }
        public string Coupon_rep
        {
            get { return string.Format("{0:0.00}", m_Coupon); }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_Coupon = Convert.ToDecimal(value);
                }
                else
                {
                    m_Coupon = 0;
                }
            }
        }

        public DateTime? TradeDate
        {
            get
            {
                return m_TradeDate;
            }
        }

        public DateTime? NotificationDate
        {
            get
            {
                return m_NotificationDate;
            }
        }

        public DateTime? SettlementDate
        {
            get
            {
                return m_SettlementDate;
            }
        }

        public string TradeDate_rep
        {
            get
            {
                if (m_TradeDate.HasValue)
                {
                    return m_TradeDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_TradeDate = Convert.ToDateTime(value);
                }
                else
                {
                    m_TradeDate = null;
                }
            }
        }
        public string NotificationDate_rep
        {
            get
            {
                if (m_NotificationDate.HasValue)
                {
                    return m_NotificationDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_NotificationDate = Convert.ToDateTime(value);
                }
                else
                {
                    m_NotificationDate = null;
                }
            }
        }
        public string SettlementDate_rep
        {
            get
            {
                if (m_SettlementDate.HasValue)
                {
                    return m_SettlementDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_SettlementDate = Convert.ToDateTime(value);
                }
                else
                {
                    m_SettlementDate = null;
                }
            }
        }

        // If no transactions, values are 0
        public decimal Amount
        {
            get
            {
                return GetTransactionSum(t => true);
            }
        }

        public decimal PreClosingAmount
        {
            get
            {
                return GetTransactionSum(t => t.Type != E_TransactionType.Closing);
            }
        }

        private decimal GetTransactionSum(Func<Transaction, bool> selector)
        {
            decimal totalAmount = 0;
            foreach (Transaction transaction in Transactions.Where(selector))
            {
                totalAmount += transaction.Amount;
                if (transaction.Type == E_TransactionType.Closing) return 0;
            }
            return totalAmount;
        }

        public string Amount_rep
        {
            get
            {
                return m_losConvert.ToMoneyString(Amount, FormatDirection.ToRep);
            }
        }

        public bool IsAllocatedLoansCalculated
        {
            get { return m_IsAllocatedLoansCalculated; }
            set { m_IsAllocatedLoansCalculated = value; }
        }
        public string AllocatedLoans_rep
        {
            get
            {
                if (m_IsAllocatedLoansCalculated)
                {
                    if (!AssociatedPoolId.HasValue) return "";

                    var pool = new MortgagePool.MortgagePool(AssociatedPoolId.Value);
                    var trades = pool.GetTrades().OrderByDescending(t => t.Price);;

                    if (trades.Count() == 1) return pool.TotalCurrentBalance_rep;

                    decimal remainingBalance = pool.TotalCurrentBalance;
                    decimal ret = 0;
                    foreach (var t in trades)
                    {
                        ret = Math.Min(t.Amount, remainingBalance);
                        remainingBalance -= ret;
                        if (remainingBalance <= 0) break;
                    }

                    return m_losConvert.ToMoneyString(ret, FormatDirection.ToRep);
                }
                else
                {
                    return m_losConvert.ToMoneyString(m_AllocatedLoans, FormatDirection.ToRep);
                }
            }
            set { m_AllocatedLoans = m_losConvert.ToMoney(value); }
        }

        public decimal Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }
        // The price is usually equal to that of the first transaction
        // However, a trade's price is independent of any transactions
        public string Price_rep
        {
            get { return m_Price.ToString(); }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_Price = Convert.ToDecimal(value);
                }
                else
                {
                    m_Price = 0;
                }
            }
        }

        public decimal TotalGainLoss
        {
            get
            {
                decimal totalGainLoss = 0;
                foreach (Transaction transaction in Transactions)
                {
                    totalGainLoss += transaction.GainLossAmount;
                }
                return totalGainLoss;
            }
        }

        public string TotalGainLoss_rep
        {
            get
            {
                return m_losConvert.ToMoneyString(TotalGainLoss, FormatDirection.ToRep);
            }
        }

        public decimal TotalExpense
        {
            get
            {
                decimal totalExpense = 0;
                foreach (Transaction transaction in Transactions)
                {
                    totalExpense += transaction.Expense;
                }

                return totalExpense;
            }
        }

        public string TotalExpense_rep
        {
            get
            {
                return m_losConvert.ToMoneyString(TotalExpense, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// BuyAmount and SellAmount are determined by TypeB and Amount
        /// </summary>
        public string BuyAmount_rep
        {
            get
            {
                if (m_Direction == E_TradeDirection.Buy)
                {
                    return Amount_rep;
                }
                else
                {
                    return "";
                }
            }
        }
        public string SellAmount_rep
        {
            get
            {
                if (m_Direction == E_TradeDirection.Sell)
                {
                    return Amount_rep;
                }
                else
                {
                    return "";
                }
            }
        }

        public TradeStatus Status
        {
            get
            {
                return (Amount != 0 || Transactions == null || Transactions.Count <= 1) ? TradeStatus.Open : TradeStatus.Closed;
            }
        }
        #endregion

        public Trade(Guid brokerId)
        {
            m_isNew = true;

            m_TradeId = Guid.NewGuid();
            m_BrokerId = brokerId;
            m_Type = E_TradeType.TBA;
            m_Direction = E_TradeDirection.Sell;

            m_IsAllocatedLoansCalculated = false; // default to false until we add the allocated loans feature

            Transactions = new List<Transaction>();
            m_Description = new Description(Guid.Empty, "");
        }

        /// <summary>
        /// Create a new trade using a previously reserved tradeId.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="tradeId"></param>
        public Trade(Guid brokerId, Guid tradeId)
        {
            m_isNew = true;
            
            m_TradeId = tradeId;
            m_BrokerId = brokerId;
            m_Type = E_TradeType.TBA;
            m_Direction = E_TradeDirection.Sell;

            m_IsAllocatedLoansCalculated = false; // default to false until we add the allocated loans feature

            Transactions = new List<Transaction>();
            m_Description = new Description(Guid.Empty, "");
        }

        /// <summary>
        /// Used to instantiate documents using an sql data reader. Only static functions in this class should be able to load existing trades. 
        /// </summary>
        /// <param name="reader"></param>
        internal Trade(DbDataReader reader)
        {
            LoadFromReader(reader);
            Guid descriptionId = (Guid)reader["DescriptionId"];
            try
            {
                m_Description = Description.GetDescriptionById(descriptionId);
            }
            catch (NotFoundException)
            {
                m_Description = new Description(descriptionId, "DESC_MISSING");
            }
            Transactions = GetTransactions();
        }
        private void LoadFromReader(DbDataReader reader)
        {
            m_isNew = false;

            m_TradeId = (Guid)reader["TradeId"];
            m_BrokerId = (Guid)reader["BrokerId"];

            m_TradeNum = (int)reader["TradeNum"]; // This cannot be null
            m_DealerInvestor = (string)reader["DealerInvestor"];
            m_AssignedTo = (string)reader["AssignedTo"];

            m_Type = (E_TradeType) Convert.ToInt32(reader["TypeA"]);
            m_Direction = (E_TradeDirection) Convert.ToInt32(reader["TypeB"]);
            m_Month = Convert.ToInt32(reader["Month"]);

            m_Coupon = (decimal)reader["Coupon"];

            m_IsAllocatedLoansCalculated = (bool)reader["IsAllocatedLoansCalculated"];
            m_AllocatedLoans = (decimal)reader["AllocatedLoans"];
            m_Price = (decimal)reader["Price"];

            var pool = reader["PoolId"];
            if (pool == DBNull.Value) AssociatedPoolId = null;
            else AssociatedPoolId = (long)pool;

            m_TradeDate = reader["TradeDate"] == DBNull.Value ? new DateTime?() : (DateTime) reader["TradeDate"];
            m_SettlementDate = reader["SettlementDate"] == DBNull.Value ? new DateTime?() : (DateTime)reader["SettlementDate"];
            m_NotificationDate = reader["NotificationDate"] == DBNull.Value ? new DateTime?() : (DateTime)reader["NotificationDate"];
            
        }

        public List<Transaction> GetTransactions()
        {
            return Transaction.GetTransactionsByTrade(this);
        }
        public Transaction GetTransactionById(Guid transactionId)
        {
            return Transactions.Find((Transaction tr) => tr.TransactionId == transactionId);
        }
        public void DeleteTransaction(Guid transactionId)
        {
            int selectedTransactionIndex = Transactions.FindIndex((Transaction t) => t.TransactionId == transactionId);
            if (selectedTransactionIndex >= 0)
            {
                Transactions[selectedTransactionIndex].Delete();
                Transactions.RemoveAt(selectedTransactionIndex);
            } // Otherwise, there is no transaction to delete. 
        }
        public void Save()
        {
            if (m_isNew)
            {
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, CREATETRADE_SP, 3, GenerateSaveParameters());
            }
            else
            {
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, UPDATETRADE_SP, 3, GenerateSaveParameters());
            }
        }
        private SqlParameter[] GenerateSaveParameters()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@TradeId", m_TradeId));
            parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
            parameters.Add(new SqlParameter("@TradeNum", m_TradeNum));
            parameters.Add(new SqlParameter("@DealerInvestor", m_DealerInvestor ?? ""));
            parameters.Add(new SqlParameter("@AssignedTo", m_AssignedTo ?? ""));
            parameters.Add(new SqlParameter("@TypeA", (int) m_Type));
            parameters.Add(new SqlParameter("@TypeB", (int) m_Direction));
            parameters.Add(new SqlParameter("@Month", m_Month));
            parameters.Add(new SqlParameter("@DescriptionId", m_Description.DescriptionId));
            parameters.Add(new SqlParameter("@Coupon", Math.Min(m_Coupon, 99.90M)));
            parameters.Add(new SqlParameter("@TradeDate", m_TradeDate));
            parameters.Add(new SqlParameter("@NotificationDate", m_NotificationDate));
            parameters.Add(new SqlParameter("@SettlementDate", m_SettlementDate));
            parameters.Add(new SqlParameter("@Price", m_Price));
            parameters.Add(new SqlParameter("@IsAllocatedLoansCalculated", m_IsAllocatedLoansCalculated));
            parameters.Add(new SqlParameter("@AllocatedLoans", m_AllocatedLoans));

            object poolValue = DBNull.Value;
            if (AssociatedPoolId.HasValue) poolValue = AssociatedPoolId.Value;
            parameters.Add(new SqlParameter("@PoolId", poolValue));
            return parameters.ToArray();
        }

        public void Delete()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TradeId", m_TradeId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, DELETEALLTRANSACTIONS_SP, 3, parameters);

            parameters = new SqlParameter[] {
                                            new SqlParameter("@TradeId", m_TradeId)
                        };
            StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, DELETETRADE_SP, 3, parameters);
        }
        public static Trade RetrieveById(Guid tradeId, Guid brokerId)
        {
            // UNTESTED
            var trades = GetTrades(brokerId, FETCHTRADE_SP, 
                                   new SqlParameter("@TradeId", tradeId),
                                   new SqlParameter("@BrokerId", brokerId));
            if (trades.Count == 0)
            {
                throw new NotFoundException(ErrorMessages.Generic, "The trade was not found");
            }
            return trades.First();
        }

        public static Trade RetrieveByTradeNum(int tradeNum, Guid brokerId)
        {
            // UNTESTED
            var trades = GetTrades(brokerId, FETCHTRADEBYNUM_SP,
                                   new SqlParameter("@TradeNum", tradeNum),
                                   new SqlParameter("@BrokerId", brokerId));
            if (trades.Count == 0)
            {
                throw new NotFoundException(ErrorMessages.Generic, "The trade was not found");
            }
            return trades.First();
        }

        public static List<Trade> GetTradesByBroker(Guid brokerId)
        {
            return GetTrades(brokerId, FETCHTRADE_SP, new SqlParameter("@BrokerId", brokerId));
        }

        private static List<Trade> GetTrades(Guid brokerId, string storedProcedure, params SqlParameter[] parameters)
        {
            var trades = new List<Trade>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, storedProcedure, parameters))
            {
                while (reader.Read())
                {
                    trades.Add(new Trade(reader));
                }
            }
            return trades;
        }

        public static long GetTradeAutoNum(Guid brokerId)
        {
            SqlParameter TradeCounterParameter = new SqlParameter("@TradeCounter", SqlDbType.BigInt);
            TradeCounterParameter.Direction = ParameterDirection.Output;

            List<SqlParameter> parameters = new List<SqlParameter>(3);
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(TradeCounterParameter);

            long tradeCounter = 0;  // return 0 if IsAutoGenerateTradeNums == false
            StoredProcedureHelper.ExecuteNonQuery(brokerId, GETTRADECOUNTER_SP, 5, parameters);
            if (TradeCounterParameter.Value != DBNull.Value)
            {
                tradeCounter = (long)TradeCounterParameter.Value;
            }

            return tradeCounter;
        }
    }
}
