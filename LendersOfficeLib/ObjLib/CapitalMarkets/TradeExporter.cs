﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using LendersOffice.Common.TextImport;
using System.Data;
using DataAccess;

namespace LendersOffice.ObjLib.CapitalMarkets
{
    /// <summary>
    /// Class for exporting basic trade reports.
    /// TODO: This is way too tightly coupled. Refactor/generalize when time permits.
    /// </summary>
    public class TradeExporter
    {
        private static List<string> BaseExportFields = new List<string>()
        {
            "TradeNum_rep",
            "Description_rep",
            "Type_rep",
            "Month_rep",
            "Coupon_rep",
            "BuyAmount_rep",
            "SellAmount_rep",
            "Price_rep",
            "AllocatedLoans_rep",
            "SettlementDate_rep",
            "NotificationDate_rep",
            "DealerInvestor",
            "TradeDate_rep",
            
        };

        private static List<string> ExpandedExportFields = new List<string>()
        {
            "TotalGainLoss_rep",
            "TotalExpense_rep"
        };

        private static Dictionary<string, string> BaseFriendlyNameMap = new Dictionary<string, string>()
        {
            { "TradeNum_rep", "Trade #" },
            { "Description_rep", "Description" },
            { "Type_rep", "Type" },
            { "Month_rep", "Month" },
            { "Coupon_rep", "Coupon" },
            { "BuyAmount_rep", "Buy Amount" },
            { "SellAmount_rep", "Sell Amount" },
            { "Price_rep", "Price" },
            { "AllocatedLoans_rep", "Associated Loans" },
            { "SettlementDate_rep", "Settlement Date" },
            { "NotificationDate_rep", "Notification Date" },
            { "DealerInvestor", "Dealer/Investor" },
            { "TradeDate_rep", "Trade Date" }
        };

        // This cannot have any duplicates from the base map. 
        // Otherwise, exception will be thrown in getter.
        private static Dictionary<string, string> ExpandedFriendlyNameMap = new Dictionary<string, string>()
        {
            { "TotalGainLoss_rep", "Gain/Loss" },
            { "TotalExpense_rep", "Expense" }
        };

        public string ReportName { get; set; }
        public IEnumerable<TradeGroup> TradeGroups { get; set; }
        public bool IsExpandedReport { get; set; }
        
        private List<string> ExportFields
        {
            get
            {
                return IsExpandedReport ? BaseExportFields.Concat(ExpandedExportFields).ToList() :
                    BaseExportFields;
            }
        }

        private Dictionary<string, string> FriendlyNameMap
        {
            get
            {
                return IsExpandedReport ? BaseFriendlyNameMap.Concat(ExpandedFriendlyNameMap).ToDictionary(kvp => kvp.Key, kvp => kvp.Value) :
                    BaseFriendlyNameMap;
            }
        }

        public TradeExporter(string reportName, IEnumerable<TradeGroup> tradeGroups, bool isExpanded)
        {
            ReportName = reportName;
            TradeGroups = tradeGroups;
            IsExpandedReport = isExpanded;
        }

        public TradeExporter()
        {
        }

        public string ExportToCsv()
        {
            var csvBuilder = new StringBuilder();

            foreach (var group in TradeGroups)
            {
                csvBuilder.AppendLine(group.Header);
                var dataTable = group.Trades.ToList()
                    .ToDataTable(ExportFields)
                    .RenameColumns(FriendlyNameMap);
                csvBuilder.Append(dataTable.ToCSVWithHeader());

                if (IsExpandedReport)
                {
                    csvBuilder.AppendLine(GetTotalsRow(group, dataTable, ','));
                }

                csvBuilder.AppendLine();
            }

            return csvBuilder.ToString();
        }

        public string ExportToTxt()
        {
            var txtBuilder = new StringBuilder();

            foreach (var group in TradeGroups)
            {
                var dataTable = group.Trades.ToList()
                    .ToDataTable(ExportFields)
                    .RenameColumns(FriendlyNameMap);

                // Write the TradeGroup header and all of the column names out.
                txtBuilder.AppendLine(group.Header);
                var underlineBuilder = new StringBuilder();
                foreach (DataColumn col in dataTable.Columns)
                {
                    txtBuilder.AppendFormat("{0}\t", SanitizeForTsv(col.ColumnName));
                    underlineBuilder.AppendFormat("{0}\t", "".PadLeft(col.ColumnName.Length, '-'));
                }
                txtBuilder.Length -= 1; // remove trailing tab from header & underline
                underlineBuilder.Length -= 1;
                txtBuilder.AppendLine();
                txtBuilder.AppendLine(underlineBuilder.ToString());

                // Write the actual trade data.
                foreach (DataRow row in dataTable.Rows)
                {
                    foreach (DataColumn col in dataTable.Columns)
                    {
                        txtBuilder.AppendFormat("{0}\t", SanitizeForTsv(row[col].ToString()));
                    }
                    txtBuilder.Length -= 1; // remove trailing tab
                    txtBuilder.AppendLine();
                }

                if (IsExpandedReport)
                {
                    txtBuilder.AppendLine(GetTotalsRow(group, dataTable, '\t'));
                }

                txtBuilder.AppendLine();
            }

            return txtBuilder.ToString();
        }

        private string GetTotalsRow(TradeGroup tradeGroup, DataTable dataTable, char delimiter)
        {
            var convert = new LosConvert();
            var totalGainLoss = convert.ToMoneyString(tradeGroup.Trades.Sum(t => t.TotalGainLoss), FormatDirection.ToRep);
            var totalExpense = convert.ToMoneyString(tradeGroup.Trades.Sum(t => t.TotalExpense), FormatDirection.ToRep);
            if (delimiter == ',')
            {
                totalGainLoss = DataParsing.sanitizeForCSV(totalGainLoss);
                totalExpense = DataParsing.sanitizeForCSV(totalExpense);
            }
            else if (delimiter == '\t')
            {
                totalGainLoss = SanitizeForTsv(totalGainLoss);
                totalExpense = SanitizeForTsv(totalExpense);
            }

            var gainLossIndex = dataTable.Columns.IndexOf(FriendlyNameMap["TotalGainLoss_rep"]);
            var expenseIndex = dataTable.Columns.IndexOf(FriendlyNameMap["TotalExpense_rep"]);
            
            var delta = Math.Abs(gainLossIndex - expenseIndex);
            var first = Math.Min(gainLossIndex, expenseIndex);
            var last = Math.Max(gainLossIndex, expenseIndex);
            return string.Format("Totals:{0}{1}{2}{3}{4}",
                "".PadLeft(first, delimiter),
                first == gainLossIndex ? totalGainLoss : totalExpense,
                "".PadLeft(delta, delimiter),
                first == gainLossIndex ? totalExpense : totalGainLoss,
                "".PadLeft((dataTable.Columns.Count - 1) - last, delimiter));
        }

        private string SanitizeForTsv(string val)
        {
            return val.Replace('\t', ' ');
        }
    }
}
