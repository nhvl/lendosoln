﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Common.TextImport;

namespace LendersOffice.ObjLib.CapitalMarkets
{
    public enum E_TransactionType
    {
        Opening,
        PairOff,
        Closing
    }

    public static class TransactionExensions
    {
        public static IEnumerable<TransactionDTO> ToDTO(this IEnumerable<Transaction> me)
        {
            var converter = new LosConvert();
            decimal runningTotal = 0;
            foreach (var t in me.OrderBy(a => OrderTransaction(a)))
            {
                runningTotal += t.Amount;
                if (t.Type == E_TransactionType.Closing) runningTotal = 0;
                yield return new TransactionDTO(t, converter.ToMoneyString(runningTotal, FormatDirection.ToRep));
            }
        }

        private static DateTime OrderTransaction(Transaction t)
        {
            //Opening and closing transactions should always be at the beginning and end respectively
            if (t.Type == E_TransactionType.Opening) return DateTime.MinValue;
            if (t.Type == E_TransactionType.Closing) return DateTime.MaxValue;

            //Other transactions should be whever their dates put them; if there's no date, we'll put them just past the opening transaction.
            return t.Date ?? DateTime.MinValue.AddDays(1);
        }
    }

    public class TransactionDTO
    {
        public readonly string TransactionId;
        public readonly string RunningTotal;
        public readonly string Type;
        public readonly string Date;
        public readonly string Amount;
        public readonly string Price;
        public readonly string GainLossAmount;
        public readonly string CalculatedGainLossAmount;
        public readonly bool IsGainLossCalculated;
        public readonly bool IsAmountCalculated;
        public readonly string Expense;
        public readonly string LinkedName;
        public readonly string LinkedId;
        public readonly string Memo;
        public readonly bool Deleted;

        public E_TransactionType ParsedType
        {
            get
            {
                try
                {
                    return (E_TransactionType)Enum.Parse(typeof(E_TransactionType), Type ?? "");
                }
                catch (ArgumentException)
                {
                    return default(E_TransactionType);
                }
            }
        }

        public TransactionDTO(Transaction t, string runningTotal)
        {
            //This may be set to true by the deserializer
            this.Deleted = false;

            this.TransactionId = t.TransactionId.ToString();
            this.Type = t.Type.ToString();
            this.Date = t.Date_rep;
            this.Amount = t.Amount_rep;
            this.Price = t.Price_rep;
            this.GainLossAmount = t.GainLossAmount_rep;
            this.IsGainLossCalculated = t.IsGainLossCalculated;
            this.IsAmountCalculated = t.IsAmountCalculated;
            this.Expense = t.Expense_rep;
            this.RunningTotal = runningTotal;
            this.LinkedId = "";
            this.LinkedName = "";
            this.CalculatedGainLossAmount = t.CalculatedGainLossAmount_rep;

            if(t.LinkedTransactionId.HasValue)
            {
                this.LinkedId = t.LinkedTrade.TradeId.ToString();
                this.LinkedName = "Trade # " + t.LinkedTrade.TradeNum;
            }
            this.Memo = t.Memo;
        }

        private bool checkGuid(string idStr, Guid? id)
        {
            Guid toCheck;
            //Does it look like we have a guid?
            if (DataParsing.TryParseGuid(idStr, out toCheck))
            {
                //Yes, so true if they match
                if (id.HasValue && id.Value == toCheck) return true;
                return false;
            }
            
            //No, we don't have a guid in the string. They don't match if we have an id.
            if (id.HasValue) return false;

            //Otherwise no guid in the string and no guid in the id means they match.
            return true;
            
        }


        public bool Matches(Transaction t)
        {
            return this.Type.TrimWhitespaceAndBOM() == t.Type.ToString() &&
                   this.Date.TrimWhitespaceAndBOM() == t.Date_rep &&
                   this.Amount.TrimWhitespaceAndBOM() == t.Amount_rep &&
                   this.Price.TrimWhitespaceAndBOM() == t.Price_rep &&
                   this.GainLossAmount.TrimWhitespaceAndBOM() == t.GainLossAmount_rep &&
                   this.IsGainLossCalculated == t.IsGainLossCalculated &&
                   this.IsAmountCalculated == t.IsAmountCalculated &&
                   this.Expense.TrimWhitespaceAndBOM() == t.Expense_rep &&
                   this.Memo.TrimWhitespaceAndBOM() == t.Memo &&
                   checkGuid(this.LinkedId, t.LinkedTransactionId);
        }

        //For serialization
        public TransactionDTO() { }
    }


    public class Transaction
    {
        private static string FETCHTRANSACTION_SP = "MBS_FetchTransactions";
        private static string FETCHTRANSACTIONBYNUM_SP = "MBS_FetchTransactionsByTransactionNum";
        private static string UPDATETRANSACTION_SP = "MBS_UpdateTransaction";
        private static string CREATETRANSACTION_SP = "MBS_CreateTransaction";
        private static string DELETETRANSACTION_SP = "MBS_DeleteTransaction";

        #region Member variables
        internal bool m_isNew;
        private LosConvert m_losConvert = new LosConvert();

        private Guid m_TransactionId;
        private Guid m_TradeId;
        public readonly Trade MyTrade;

        private int? m_TransactionNum;

        private DateTime? m_Date;
        private decimal m_Amount;
        private decimal m_Price;
        private bool m_IsGainLossCalculated;
        private decimal m_GainLossAmount;
        private decimal m_Expense;
        private string m_Memo;
        public E_TransactionType Type { get; set; }

        private bool m_isAmountCalculated;
        //This only matters for Closing transactions.
        public bool IsAmountCalculated
        {
            get
            {
                if (Type == E_TransactionType.Closing) return m_isAmountCalculated;
                return false;
            }
            set 
            {
                if (Type == E_TransactionType.Closing) m_isAmountCalculated = value;
            }
        }
        public bool HasLink { get { return LinkedTransactionId.HasValue; } }

        public Guid? LinkedTransactionId { get; set; }
        
        private Guid? TradeToLink;

        private Transaction x_linkedTransaction = null;
        public Transaction LinkedTransaction
        {
            get
            {
                if (x_linkedTransaction != null || !LinkedTransactionId.HasValue) return x_linkedTransaction;
                return x_linkedTransaction = Transaction.Retrieve(this.BrokerId, LinkedTransactionId.Value);
            }
        }

        public Trade LinkedTrade
        {
            get
            {
                if (!LinkedTransactionId.HasValue) return null;
                return LinkedTransaction.MyTrade;
            }
        }

        public Guid TransactionId
        {
            get { return m_TransactionId; }
        }
        public Guid TradeId
        {
            get { return m_TradeId; }
        }

        public Guid BrokerId { get; private set; }
        public int? TransactionNum
        {
            get { return m_TransactionNum.Value; }
        }
        public string TransactionNum_rep
        {
            get { return m_TransactionNum.ToString(); }
        }

        public DateTime? Date
        {
            get
            {
                return m_Date;
            }
        }

        public string Date_rep
        {
            get
            {
                if (m_Date.HasValue)
                {
                    return m_Date.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_Date = Convert.ToDateTime(value);
                }
                else
                {
                    m_Date = null;
                }
            }
        }
        public decimal Amount
        {
            get {
                if (IsAmountCalculated)
                {
                    return -MyTrade.PreClosingAmount;
                }
                
                return m_Amount; 
            }
        }
        public string Amount_rep
        {
            get { return m_losConvert.ToMoneyString(Amount, FormatDirection.ToRep); }
            set { m_Amount = m_losConvert.ToMoney(value); }
        }
        public string Price_rep
        {
            get { return m_Price.ToString(); }
            set { m_Price = m_losConvert.ToDecimal(value); }
        }
        public bool IsGainLossCalculated
        {
            get { return m_IsGainLossCalculated; }
            set { m_IsGainLossCalculated = value; }
        }
        
        private readonly E_TradeDirection direction;
        private readonly decimal price;

        public string CalculatedGainLossAmount_rep
        {
            get { return m_losConvert.ToMoneyString(CalculatedGainLossAmount, FormatDirection.ToRep); }
        }

        private decimal CalculatedGainLossAmount
        {
            get
            {
                return Transaction.CalculateGainLossAmount(m_Price, m_Amount, direction, price);
            }
        }

        public decimal GainLossAmount
        {
            get
            {
                if (m_IsGainLossCalculated)
                {
                    return CalculatedGainLossAmount;
                }
                else
                {
                    return m_GainLossAmount;
                }
            }
        }
        public string GainLossAmount_rep
        {
            get { return m_losConvert.ToMoneyString(GainLossAmount, FormatDirection.ToRep); }
            set { m_GainLossAmount = m_losConvert.ToMoney(value); }
        }
        public decimal Expense
        {
            get { return m_Expense; }
        }
        public string Expense_rep
        {
            get { return m_losConvert.ToMoneyString(m_Expense, FormatDirection.ToRep); }
            set { m_Expense = m_losConvert.ToMoney(value); }
        }
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }
        #endregion

        public Transaction(Trade trade)
        {
            m_isNew = true;
            m_TransactionId = Guid.NewGuid();
            m_TradeId = trade.TradeId;
            direction = trade.Direction;
            price = trade.Price;
            m_Memo = "";
            MyTrade = trade;
            this.BrokerId = trade.BrokerId;
        }

        private Transaction(DbDataReader reader, Trade trade)
        {
            MyTrade = trade;
            this.BrokerId = trade.BrokerId;
            m_isNew = false;
            direction = trade.Direction;
            price = trade.Price;
            LoadFromReader(reader);
        }

        private void LoadFromReader(DbDataReader reader)
        {
            m_TransactionId = (Guid)reader["TransactionId"];
            m_TradeId = (Guid)reader["TradeId"];

            m_Date = (DateTime)reader["Date"];
            m_Amount = (decimal)reader["Amount"];
            m_Price = (decimal)reader["Price"];
            m_IsGainLossCalculated = (bool)reader["IsGainLossCalculated"];
            m_GainLossAmount = (decimal)reader["GainLossAmount"];
            m_Expense = (decimal)reader["Expense"];
            m_Memo = (string)reader["Memo"];
            Type = (E_TransactionType)reader["Type"];
            m_isAmountCalculated = (bool)reader["IsAmountCalculated"];

            m_TransactionNum = (int)reader["TransactionNum"]; // This cannot be null

            object linked = reader["LinkedTransaction"];
            LinkedTransactionId = linked == DBNull.Value ? null : (Guid?)linked;
        }

        public void Save()
        {
            if (m_isNew)
            {
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, CREATETRANSACTION_SP, 3, GenerateSaveParameters());

                SqlParameter[] parameters = {
                                                new SqlParameter("@TransactionId", m_TransactionId)
                                            };
                // OPM 130491 - Update transaction number now that it has one
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, FETCHTRANSACTION_SP, parameters))
                {
                    if (reader.Read())
                    {
                        m_TransactionNum = (int)reader["TransactionNum"];
                    }
                }

                m_isNew = false;
            }
            else
            {
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, UPDATETRANSACTION_SP, 3, GenerateSaveParameters());
            }

            //Order matters for this
            //We need to be in the database before we can create a transaction that links to us
            //And our linked transaction needs to be in the database before we can link to it.
            //The trick is to put ourselves in the database first with no link (which just happend)
            //Then put our link-ee in there linked to us, then link us to him.
            if (TradeToLink.HasValue)
            {
                LinkToTrade(TradeToLink.Value);
            }
        }

        public void LinkToTrade(Guid tradeId)
        {
            if (HasLink)
            {
                //If we're already linked to this trade, just ignore it.
                if (tradeId == LinkedTrade.TradeId) return;

                //Otherwise, we'll need to delete and unlink from the transaction we're currently linked to in order to proceed.
                DeleteAndUnlink(this.BrokerId, LinkedTransactionId.Value, m_TransactionId);
            }

            //Create a new transaction in the trade we're supposed to link to
            Trade toLink = Trade.RetrieveById(tradeId, this.BrokerId);
            var linked = new Transaction(toLink);
            
            linked.LinkedTransactionId = this.m_TransactionId;
            linked.Memo = this.Memo;
            linked.Date_rep = this.Date_rep;
            linked.Amount_rep = this.Amount_rep;
            linked.Type = E_TransactionType.PairOff;
            linked.Save();

            //Once the guy we're linking to is in the database, we can reference it and save. 
            //We can forget the trade to link at this point, otherwise we'll end up checking it again.
            this.TradeToLink = null;
            this.LinkedTransactionId = linked.TransactionId;
            this.Save();
            
        }

        public void Delete()
        {
            // If it's new, then we just never save it
            if (!m_isNew)
            {
                DeleteAndUnlink(this.BrokerId, m_TransactionId, LinkedTransactionId);
                if (HasLink)
                {
                    LinkedTransaction.InternalDelete();
                }
            }
        }

        public static void DeleteAndUnlink(Guid brokerId, Guid ToDelete, Guid? ToUnlink)
        {
            var parameters = new List<SqlParameter>() { 
                new SqlParameter("@TransactionId", ToDelete)
            };

            if (ToUnlink.HasValue)
            {
                parameters.Add(new SqlParameter("@LinkedTransactionId", ToUnlink));
            }

            try
            {
                StoredProcedureHelper.ExecuteNonQuery(brokerId, DELETETRANSACTION_SP, 1, parameters.ToArray());
            }
            catch (SqlException)
            { 
                //If it didn't work, try forcing a scrub of all transactions that could link to the one we're trying to delete.
                parameters.Add(new SqlParameter("@ForceScrubAllLinked", 1));
                StoredProcedureHelper.ExecuteNonQuery(brokerId, DELETETRANSACTION_SP, 3, parameters.ToArray());
            }
        }

        private void InternalDelete()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionId", m_TransactionId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, DELETETRANSACTION_SP, 3, parameters);
        }

        private SqlParameter[] GenerateSaveParameters()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@TransactionId", m_TransactionId));
            parameters.Add(new SqlParameter("@TradeId", m_TradeId));

            parameters.Add(new SqlParameter("@Date", m_Date));
            parameters.Add(new SqlParameter("@Amount", m_Amount));
            parameters.Add(new SqlParameter("@Price", m_Price));
            parameters.Add(new SqlParameter("@IsGainLossCalculated", m_IsGainLossCalculated));
            parameters.Add(new SqlParameter("@GainLossAmount", m_GainLossAmount));
            parameters.Add(new SqlParameter("@Expense", m_Expense));
            parameters.Add(new SqlParameter("@Memo", m_Memo));
            parameters.Add(new SqlParameter("@Type", Type));
            parameters.Add(new SqlParameter("@IsAmountCalculated", IsAmountCalculated));

            object linked = DBNull.Value;
            if (LinkedTransactionId.HasValue) linked = LinkedTransactionId.Value;
            parameters.Add(new SqlParameter("@LinkedTransaction", linked));

            return parameters.ToArray();
        }

        public static decimal CalculateGainLossAmount(decimal transactionPrice, decimal transactionAmount, E_TradeDirection tradeTypeB, decimal tradePrice)
        {
            switch (tradeTypeB)
            {
                case E_TradeDirection.Sell:
                    return ((transactionPrice - tradePrice) / 100) * transactionAmount;
                case E_TradeDirection.Buy:
                    return ((tradePrice - transactionPrice) / 100) * transactionAmount;
                default:
                    throw new UnhandledEnumException(tradeTypeB);
            }
        }

        public static Transaction Retrieve(Guid transactionId, Trade trade)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TradeId", trade.TradeId),
                                            new SqlParameter("@TransactionId", transactionId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(trade.BrokerId, FETCHTRANSACTION_SP, parameters))
            {
                while (reader.Read())
                {
                    return new Transaction(reader, trade);
                }
            }

            throw new CBaseException("Transaction not found", "Was looking for transaction with id [" + transactionId + "] in trade with id [" + trade.TradeId + "] and did not find it");
        }

        /// <summary>
        /// Retrieves a transaction using only its id. This is slower than the other methods.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public static Transaction Retrieve(Guid brokerId, Guid transactionId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionId", transactionId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, FETCHTRANSACTION_SP, parameters))
            {
                while (reader.Read())
                {
                    var tradeId = (Guid)reader["TradeId"];
                    var trade = Trade.RetrieveById(tradeId, brokerId);
                    return new Transaction(reader, trade);
                }
            }

            throw new CBaseException("Transaction not found", "Was looking for transaction with id [" + transactionId + "] and did not find it");
        }


        public static Transaction Retrieve(Guid brokerId, Guid transactionId, Guid tradeId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TradeId", tradeId),
                                            new SqlParameter("@TransactionId", transactionId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, FETCHTRANSACTION_SP, parameters))
            {
                while (reader.Read())
                {
                    var trade = Trade.RetrieveById(tradeId, brokerId);
                    return new Transaction(reader, trade);
                }
            }

            throw new CBaseException("Transaction not found", "Was looking for transaction with id [" + transactionId + "] in trade with id [" + tradeId + "] and did not find it");
        }

        public static Transaction RetrieveByTransactionNum(Guid brokerId, int transactionNum)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionNum", transactionNum)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, FETCHTRANSACTIONBYNUM_SP, parameters))
            {
                while (reader.Read())
                {
                    var tradeId = (Guid)reader["TradeId"];
                    var trade = Trade.RetrieveById(tradeId, brokerId);
                    return new Transaction(reader, trade);
                }
            }

            throw new CBaseException("Transaction not found", "Was looking for transaction with transactionNum [" + transactionNum + "] and did not find it");
        }

        public static List<Transaction> GetTransactionsByTrade(Trade trade)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TradeId", trade.TradeId)
                                        };
            var transactions = new List<Transaction>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(trade.BrokerId, FETCHTRANSACTION_SP, parameters))
            {
                while (reader.Read())
                {
                    transactions.Add(new Transaction(reader, trade));
                }
            }
            return transactions;
        }

        internal void ApplyDTO(TransactionDTO t)
        {
            Type = t.ParsedType;
            Date_rep = t.Date;
            Amount_rep = t.Amount;
            Price_rep = t.Price;
            GainLossAmount_rep = t.GainLossAmount;
            IsGainLossCalculated = t.IsGainLossCalculated;
            IsAmountCalculated = t.IsAmountCalculated;
            Expense_rep = t.Expense;
            Memo = t.Memo;
            Guid toLink;
            if (DataParsing.TryParseGuid(t.LinkedId, out toLink))
            {
                TradeToLink = toLink;
            }
        }
    }

    public class Description
    {
        private Guid m_DescriptionId;
        private string m_DescriptionName;

        public Guid DescriptionId { get { return m_DescriptionId; } }
        public string DescriptionName { get { return m_DescriptionName; } }

        public Description(Guid id, string name)
        {
            m_DescriptionId = id;
            m_DescriptionName = name;
        }

        public Description(DbDataReader reader)
        {
            m_DescriptionId = (Guid)reader["DescriptionId"];
            m_DescriptionName = reader["DescriptionName"].ToString();
        }

        public static List<Description> GetDescriptions()
        {
            var descriptions = new List<Description>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MBS_RetrieveDescriptions"))
            {
                while (reader.Read())
                {
                    descriptions.Add(new Description(reader));
                }
            }
            return descriptions;
        }

        public static void DeleteDescription(Guid id)
        {

            SqlParameter[] parameters = {
                                            new SqlParameter("@DescriptionId", id)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "MBS_DeleteDescription", 3, parameters);
        }

        public static void CreateDescription(string descriptionName)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DescriptionName", descriptionName)
                                        };


            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "MBS_CreateDescription", 3, parameters);
        }

        public static Description GetDescriptionById(Guid id)
        {
            Description description;
            SqlParameter[] parameters = {
                                            new SqlParameter("@DescriptionId", id)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MBS_RetrieveDescriptionById", parameters))
            {
                if (reader.Read())
                {
                    description = new Description(reader);
                }
                else
                {
                    throw new NotFoundException(ErrorMessages.Generic, ErrorMessages.InvalidMBSDescriptionId(id));
                }
            }
            return description;
        }

        public static Description GetDescriptionByName(string name)
        {
            Description description;
            SqlParameter[] parameters = {
                                            new SqlParameter("@DescriptionName", name)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MBS_RetrieveDescriptionByName", parameters))
            {
                if (reader.Read())
                {
                    description = new Description(reader);
                }
                else
                {
                    throw new NotFoundException(ErrorMessages.Generic, ErrorMessages.InvalidMBSDescriptionName(name));
                }
            }
            return description;
        }
    }
}
