﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using ExpertPdf.HtmlToPdf;
using ExpertPdf.HtmlToPdf.PdfDocument;
using LendersOffice.Constants;

namespace LendersOffice.ObjLib.CapitalMarkets
{
    public static class TradeReportUtilities
    {
        public static Document GetPdfFromHtml(string baseUrl, string html)
        {
            try
            {
                var pdfConverter = new PdfConverter();
                if (!string.IsNullOrEmpty(ConstStage.PdfConverterLicenseKey)) pdfConverter.LicenseKey = ConstStage.PdfConverterLicenseKey;

                pdfConverter.PdfDocumentOptions.LeftMargin = pdfConverter.PdfDocumentOptions.RightMargin =
                    pdfConverter.PdfDocumentOptions.TopMargin = pdfConverter.PdfDocumentOptions.BottomMargin = 10;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Landscape;
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
            
                return pdfConverter.GetPdfDocumentObjectFromHtmlString(html, baseUrl);
            }
            catch (Exception ex)
            {
                // Restart PDF Conversion Process
                Tools.RestartPdfConversionProcess();
                throw ex;
            }
        }

        public static string GenerateHtmlForPdf(string reportTitle, string logoSrc, string html, List<string> stylesheets, bool isExpandedReport)
        {
            var htmlBuilder = new StringBuilder();

            htmlBuilder.Append(@"<html><head>");
            htmlBuilder.Append(@"<title>Trades</title>");
            foreach (var stylesheet in stylesheets)
            {
                htmlBuilder.Append(@"<link href=""" + stylesheet + @""" type=""text/css"" rel=""stylesheet"" />");
            }
            if (!isExpandedReport)
            {
                htmlBuilder.Append(@"<style type=""text/css"">.expanded { display: none; }</style>");
            }
            htmlBuilder.Append(@"</head><body>");
            htmlBuilder.Append(@"<div class=""report-header"">");
            if (!string.IsNullOrEmpty(logoSrc))
            {
                htmlBuilder.Append(@"<img src=""" + logoSrc + @""" alt=""Company Logo"" />");
            }
            htmlBuilder.Append(@"<h1>" + reportTitle + @"</h1>");
            htmlBuilder.Append(@"</div>");
            htmlBuilder.Append(html);
            htmlBuilder.Append(@"</body></html>");

            return htmlBuilder.ToString();
        }

        public static bool HasLogo(Guid pmlSiteId)
        {
            return FileDBTools.DoesFileExist(E_FileDB.Normal, pmlSiteId.ToString().ToLower() + ".logo.gif");
        }
    }
}
