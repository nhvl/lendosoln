﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOffice.FFIEC
{
    /// <summary>
    /// // 9/12/2013 dd - OPM 67223 - Create a class that store the yield rate from FFIEC.
    /// </summary>
    public class YieldTable
    {
        private class DescendingDateComparer : IComparer<DateTime>
        {
            #region IComparer<DateTime> Members

            public int Compare(DateTime x, DateTime y)
            {
                return y.CompareTo(x);
            }

            #endregion
        }

        private class Record
        {
            public DateTime Date { get; set; }
            public List<decimal> RateList = new List<decimal>(50);
        }

        public static void DownloadFromWebsite()
        {
            DownloadFromWebsite("https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableFixed.txt", "https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableAdjustable.txt", false);
        }

        /// <summary>
        /// Not all of our production server can access the AWS S3 directly. Therefore on those machines we will download through LoansPQ proxy.
        /// </summary>
        public static void DownloadFromLoansPQProxy()
        {
            string fixedUrl = "http://LPQProxy.middleearth.com/Proxy.ashx?url=https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableFixed.txt";
            string adjustableUrl = "http://LPQProxy.middleearth.com/Proxy.ashx?url=https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableAdjustable.txt";
            DownloadFromWebsite(fixedUrl, adjustableUrl, true);
        }
        private static void DownloadFromWebsite(string fixedUrl, string adjustableUrl, bool isLoansPQProxy)
        {
            // 9/13/2013 dd - Download data from these two sites
            // 01/03/2018 je - URLs updated for CFPB data.
            // https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableFixed.txt
            // https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableAdjustable.txt

            // Only update our database if content change. 

            string fixedChecksum = string.Empty;
            string armChecksum = string.Empty;

            DateTime fixedLastDownload = DateTime.MinValue;
            DateTime armLastDownload = DateTime.MinValue;

            SqlParameter[] parameters = {
                                            new SqlParameter("@DataSource", YieldTableSource.CFPB),
                                            new SqlParameter("@IncludeContent", false)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "FFIEC_YIELD_TABLE_ListAll", parameters))
            {
                while (reader.Read())
                {
                    string type = (string)reader["Type"];
                    DateTime dt = (DateTime)reader["LastModifiedDate"];
                    string checksum = (string)reader["MD5Checksum"];

                    if (type == "ARM")
                    {
                        armChecksum = checksum;
                        armLastDownload = dt;
                    }
                    else if (type == "FIXED")
                    {
                        fixedChecksum = checksum;
                        fixedLastDownload = dt;
                    }
                }
            }

            if (DownloadAndUpdateImpl("ARM", adjustableUrl, armChecksum, isLoansPQProxy) == false)
            {
                if (armLastDownload.AddDays(8) < DateTime.Today && ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
                {
                    // 9/13/2013 dd - Send out Critical Email if we have not update this file for more than 8 days.
                    EmailUtilities.SendToCritical("YieldTableAdjustable.CSV is more than 8 days old",
                        "Unable to get latest data from https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableAdjustable.txt. Last download is " + armLastDownload);
                }
            }

            if (DownloadAndUpdateImpl("FIXED", fixedUrl, fixedChecksum, isLoansPQProxy) == false)
            {
                if (fixedLastDownload.AddDays(8) < DateTime.Today && ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
                {
                    // 9/13/2013 dd - Send out Critical Email if we have not update this file for more than 8 days.
                    EmailUtilities.SendToCritical("YieldTableFixed.CSV is more than 8 days old",
                        "Unable to get latest data from https://s3.amazonaws.com/cfpb-hmda-public/prod/apor/YieldTableFixed.txt. Last download is " + fixedLastDownload);
                }
            }


        }

        private static bool DownloadAndUpdateImpl(string type, string url, string lastChecksum, bool isLoansPQProxy)
        {
            // 9/13/2013 dd - Download data from URL.
            // If the checksum is modify then insert into database.

            // Return true if data is modify, else false.
            

            string data = null;
            using (var client = new WebClient())
            {
                if (isLoansPQProxy)
                {
                    var base64String = client.DownloadString(url);
                    var bytes = Convert.FromBase64String(base64String);

                    data = System.Text.Encoding.UTF8.GetString(bytes);
                }
                else
                {
                    data = client.DownloadString(url);
                }
            }

            string checksum = EncryptionHelper.ComputeMD5Hash(data);

            if (checksum == lastChecksum)
            {
                return false; // 9/13/2013 dd - Data did not change.
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@DataSource", YieldTableSource.CFPB),
                                            new SqlParameter("@Type", type),
                                            new SqlParameter("@MD5Checksum", checksum),
                                            new SqlParameter("@Content", data)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "FFIEC_YIELD_TABLE_Update", 3, parameters);

            return true;

        }

        private static SortedList<DateTime, Record> s_ffiecFixedTableList = null;
        private static SortedList<DateTime, Record> s_ffiecArmTableList = null;

        private static SortedList<DateTime, Record> s_cfpbFixedTableList = null;
        private static SortedList<DateTime, Record> s_cfpbArmTableList = null;

        private static DateTime s_lastLoaded = DateTime.MinValue;
        private static object s_lock = new object();

        private static readonly DateTime CfpbCutoffDate = new DateTime(2018, 01, 01);

        private static void Initialize()
        {
            s_ffiecFixedTableList = new SortedList<DateTime, Record>(new DescendingDateComparer());
            s_ffiecArmTableList = new SortedList<DateTime, Record>(new DescendingDateComparer());
            s_cfpbFixedTableList = new SortedList<DateTime, Record>(new DescendingDateComparer());
            s_cfpbArmTableList = new SortedList<DateTime, Record>(new DescendingDateComparer());

            string ffiecArmData = string.Empty;
            string ffiecFixedData = string.Empty;
            string cfpbArmData = string.Empty;
            string cfpbFixedData = string.Empty;

            SqlParameter[] parameters = {
                                            new SqlParameter("@IncludeContent", true)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "FFIEC_YIELD_TABLE_ListAll", parameters))
            {
                while (reader.Read())
                {
                    YieldTableSource dataSource = (YieldTableSource)reader["DataSource"];
                    string type = (string)reader["Type"];
                    DateTime lastModifiedDate = (DateTime)reader["LastModifiedDate"];

                    if (type == "ARM")
                    {
                        if (dataSource == YieldTableSource.FFIEC)
                        {
                            ffiecArmData = (string)reader["Content"];
                        }
                        else if (dataSource == YieldTableSource.CFPB)
                        {
                            cfpbArmData = (string)reader["Content"];

                            if (lastModifiedDate.AddDays(8) < DateTime.Today)
                            {
                                EmailUtilities.SendToCritical(ConstAppDavid.ServerName + " - YieldTableAdjustable.CSV is more than 8 days old",
                                    "YieldTableAdjustable.CSV data is OLD. Last download is " + lastModifiedDate);

                            }
                        }
                    }
                    else if (type == "FIXED")
                    {
                        if (dataSource == YieldTableSource.FFIEC)
                        {
                            ffiecFixedData = (string)reader["Content"];
                        }
                        else if (dataSource == YieldTableSource.CFPB)
                        {
                            cfpbFixedData = (string)reader["Content"];

                            if (lastModifiedDate.AddDays(8) < DateTime.Today)
                            {
                                EmailUtilities.SendToCritical(ConstAppDavid.ServerName + " - YieldTableFixed.CSV is more than 8 days old",
                                    "YieldTableFixed.CSV data is OLD. Last download is " + lastModifiedDate);

                            }
                        }
                    }
                }
            }

            FillData(YieldTableSource.FFIEC, s_ffiecFixedTableList, ffiecFixedData);
            FillData(YieldTableSource.FFIEC, s_ffiecArmTableList, ffiecArmData);
            FillData(YieldTableSource.CFPB, s_cfpbFixedTableList, cfpbFixedData);
            FillData(YieldTableSource.CFPB, s_cfpbArmTableList, cfpbArmData);

            s_lastLoaded = DateTime.Today;
        }

        private static void FillData(YieldTableSource dataSource, SortedList<DateTime, Record> list, string csvData)
        {
            string[] lines = csvData.Split('\r', '\n');

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                DateTime dt = DateTime.MinValue;

                char splitChar = dataSource == YieldTableSource.FFIEC ? ',' : '|';

                string[] parts = line.Split(splitChar);

                if (DateTime.TryParse(parts[0], out dt) == true)
                {
                    Record record = new Record();
                    record.Date = dt;
                    for (int i = 1; i < parts.Length; i++)
                    {
                        // 9/12/2013 dd - I am using Parse instead of TryParse because I expect each value is a 
                        // valid decimal. Since the position of decimal is important, therefore if any invalid value
                        // NEED TO STOP parsing data and inspect manually.
                        decimal value = decimal.Parse(parts[i]);

                        record.RateList.Add(value);
                    }
                    list.Add(dt, record);
                }
            }
        }

        /// <summary>
        /// Return the value in Yield Table. Use the _rep version from the dataloan object.
        /// This method return decimal.MinValue if value could not be look up.
        /// </summary>
        /// <param name="sFinMethT"></param>
        /// <param name="sRLckD"></param>
        /// <param name="sDue"></param>
        /// <param name="sRAdj1stCapMon"></param>
        /// <param name="sHmdaActionD">HMDA Action Date. Determines which table is used.</param>
        /// <returns></returns>
        public static decimal LookUp(E_sFinMethT sFinMethT, string sRLckD, string sDue, string sRAdj1stCapMon, CDateTime sHmdaActionD)
        {
            decimal invalidValue = decimal.MinValue;

            #region Validation of input
            if (string.IsNullOrEmpty(sRLckD))
            {
                return invalidValue;
            }

            DateTime dt = DateTime.MinValue;
            int year = 0;
            if (DateTime.TryParse(sRLckD, out dt) == false)
            {
                return invalidValue;
            }

            dt = dt.Date; // Only interest in date component.

            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed:
                    {
                        int _sDue = 0;
                        if (int.TryParse(sDue, out _sDue) == false)
                        {
                            return invalidValue;
                        }

                        year = Math.Max(1, (int)Math.Floor(((decimal)_sDue / 12.0M + 5.0M / 12.0M)));
                    }
                    break;
                case E_sFinMethT.ARM:
                    {
                        int _sRAdj1stCapMon = 0;
                        if (int.TryParse(sRAdj1stCapMon, out _sRAdj1stCapMon) == false)
                        {
                            return invalidValue;
                        }

                        year = Math.Max(1, (int)Math.Floor(((decimal)_sRAdj1stCapMon / 12.0M + 5.0M / 12.0M)));
                    }
                    break;
                case E_sFinMethT.Graduated:
                    return invalidValue; // 9/12/2013 dd - No supported.
                default:
                    throw new UnhandledEnumException(sFinMethT);
            }

            if (year > 50)
            {
                return invalidValue;
            }

            #endregion

            lock (s_lock)
            {
                // 9/12/2013 dd - Make it thread-safe.
                if (s_lastLoaded.AddDays(1) < DateTime.Today)
                {
                    // 9/12/2013 dd - Cache for 1 day.
                    Initialize();
                }

                YieldTableSource dataSource = sHmdaActionD.IsValid && sHmdaActionD.DateTimeForComputation < CfpbCutoffDate ? YieldTableSource.FFIEC : YieldTableSource.CFPB;
                SortedList<DateTime, Record> list = null;

                if (sFinMethT == E_sFinMethT.Fixed && dataSource == YieldTableSource.FFIEC)
                {
                    list = s_ffiecFixedTableList;
                }
                else if (sFinMethT == E_sFinMethT.Fixed && dataSource == YieldTableSource.CFPB)
                {
                    list = s_cfpbFixedTableList;
                }
                else if (sFinMethT == E_sFinMethT.ARM && dataSource == YieldTableSource.FFIEC)
                {
                    list = s_ffiecArmTableList;
                }
                else if (sFinMethT == E_sFinMethT.ARM && dataSource == YieldTableSource.CFPB)
                {
                    list = s_cfpbArmTableList;
                }
                else
                {
                    throw new UnhandledEnumException(sFinMethT);
                }

                foreach (Record record in list.Values)
                {
                    // 9/12/2013 dd - List is in descending order by date.
                    if (record.Date > dt)
                    {
                        continue; // Go to next record.
                    }
                    else if (record.Date.AddDays(6) >= dt)
                    {
                        return record.RateList[year - 1];
                    }
                    else
                    {
                        return invalidValue;
                    }
                }
            } // end lock

            return invalidValue;
        }
    }
}
