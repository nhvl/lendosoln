﻿namespace LendersOffice.FFIEC
{
    /// <summary>
    /// Yield Table Source.
    /// </summary>
    public enum YieldTableSource
    {
        /// <summary>
        /// Yield table sourced from FFIEC.
        /// </summary>
        FFIEC = 0,

        /// <summary>
        /// Yield table sourced from CFPB.
        /// </summary>
        CFPB = 1
    }
}
