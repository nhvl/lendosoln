﻿// <copyright file="DevNote.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   9/17/2014 3:23:46 PM 
// </summary>
namespace LendersOffice.ObjLib.CustomAttributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents a private development note about a property. This is required 
    /// for all new properties in Loan File Fields. 
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class DevNote : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevNote" /> class.
        /// </summary>
        /// <param name="note">Comments about the property.</param>
        public DevNote(string note)
        {
            this.Note = note;
        }

        /// <summary>
        /// Gets the note field will be exposed in the field search and will be available to support.
        /// </summary>
        /// <value>The note exposed in the <code>http://lodevserver/search.aspx</code> page for the field.</value>
        public string Note { get; private set; }
    }
}