﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Integration.DocumentVendor;

    /// <summary>
    /// Provides a simple container for initial disclosure vendor settings in the TPO portal.
    /// </summary>
    public class TpoInitialDisclosureVendorSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoInitialDisclosureVendorSettings"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the settings.
        /// </param>
        /// <param name="match">
        /// The match for the broker temp option.
        /// </param>
        public TpoInitialDisclosureVendorSettings(Guid brokerId, Match match)
        {
            if (!match.Success)
            {
                Tools.LogWarning("[DocumentAuditSettings] Unable to match broker temp option TPO disclosure settings. Match: " + match.Value);
                return;
            }

            if (match.Groups.Count != 5 ||
                string.IsNullOrWhiteSpace(match.Groups["Vendor"].Value) ||
                string.IsNullOrWhiteSpace(match.Groups["PackageNumber"].Value) ||
                string.IsNullOrWhiteSpace(match.Groups["EnableEDisclosure"].Value) ||
                string.IsNullOrWhiteSpace(match.Groups["EnableESign"].Value))
            {
                Tools.LogWarning("[DocumentAuditSettings] Missing required information for vendor, package, e-disclosure, and/or e-sign. Match: " + match.Value);
                return;
            }

            // Looking up all of the broker's vendors is expensive, but this settings method is expected
            // to only run once the first time Lazy.Value is invoked and this temp option should be 
            // phased out relatively soon in favor of actual settings UI.
            var tempOptionVendorName = match.Groups["Vendor"].Value.Trim();
            var selectedVendor = DocumentVendorFactory.CreateVendorsFor(brokerId).FirstOrDefault(vendor => string.Equals(tempOptionVendorName, vendor.Config.VendorName, StringComparison.OrdinalIgnoreCase));

            if (selectedVendor == null)
            {
                Tools.LogWarning("[DocumentAuditSettings] Could not find vendor by name. Match: " + match.Value);
                return;
            }

            if (string.IsNullOrWhiteSpace(selectedVendor.Config.PreviewExportPath?.AbsoluteUri))
            {
                Tools.LogWarning("[DocumentAuditSettings] Document vendor is not set up to support previews. Match: " + match.Value);
                return;
            }

            var packageNumber = match.Groups["PackageNumber"].Value.Trim();
            if (!selectedVendor.Config.IsInitialDisclosurePackage(packageNumber))
            {
                Tools.LogWarning("[DocumentAuditSettings] Selected package is not an initial disclosure package for the vendor. Match: " + match.Value);
                return;
            }

            this.Vendor = selectedVendor;
            this.Package = packageNumber;
            this.EnableEDisclosure = string.Equals("Yes", match.Groups["EnableEDisclosure"].Value.Trim(), StringComparison.OrdinalIgnoreCase);
            this.EnableESign = string.Equals("Yes", match.Groups["EnableESign"].Value.Trim(), StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets or sets the vendor for the audit.
        /// </summary>
        /// <value>
        /// The vendor for the audit.
        /// </value>
        public IDocumentVendor Vendor { get; set; }

        /// <summary>
        /// Gets or sets the package for the audit.
        /// </summary>
        /// <value>
        /// The package for the audit.
        /// </value>
        /// <remarks>
        /// Terminology is package number for DocMagic, package ID for other vendors.
        /// </remarks>
        public string Package { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether E-Disclosure is enabled.
        /// </summary>
        /// <value>
        /// True if E-Disclosure is enabled, false otherwise.
        /// </value>
        public bool EnableEDisclosure { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether E-Sign is enabled.
        /// </summary>
        /// <value>
        /// True if E-Sign is enabled, false otherwise.
        /// </value>
        public bool EnableESign { get; set; }
    }
}
