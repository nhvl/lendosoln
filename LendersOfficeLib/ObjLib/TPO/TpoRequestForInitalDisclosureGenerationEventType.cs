﻿namespace LendersOffice.ObjLib.TPO
{
    /// <summary>
    /// Provides an indicator for the type of the initial disclosure
    /// generation event.
    /// </summary>
    public enum TpoRequestForInitialDisclosureGenerationEventType
    {
        /// <summary>
        /// There are no initial disclosure generation requests on file.
        /// </summary>
        NoRequest = 0,

        /// <summary>
        /// The initial disclosure generation has been requested.
        /// </summary>
        Requested = 1,

        /// <summary>
        /// The initial disclosure generation has been cancelled.
        /// </summary>
        Cancelled = 2,

        /// <summary>
        /// The initial disclosure generation has been completed.
        /// </summary>
        Completed = 3
    }
}