﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a collection of initial closing disclosure generation events.
    /// </summary>
    public class TpoRequestForInitialClosingDisclosureGenerationEventCollection : LoanEventCollection<TpoRequestForInitialClosingDisclosureGenerationEventType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForInitialClosingDisclosureGenerationEventCollection"/> class.
        /// </summary>
        /// <param name="json">
        /// The JSON string containing the events.
        /// </param>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        public TpoRequestForInitialClosingDisclosureGenerationEventCollection(string json, LosConvert losConvert, Action<AbstractAuditItem> auditAddAction) 
            : base(json, losConvert, auditAddAction)
        {
        }

        /// <summary>
        /// Gets the status of the latest event on file.
        /// </summary>
        /// <value>
        /// The status of the latest event on file or <see cref="TpoRequestForInitialDisclosureGenerationEventType.NoRequest"/>
        /// if no events have been logged.
        /// </value>
        public override TpoRequestForInitialClosingDisclosureGenerationEventType LatestEventStatus => this.LatestEvent?.Type ?? TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest;

        /// <summary>
        /// Gets the status of the latest event on file in string format.
        /// </summary>
        /// <value>
        /// The status of the latest event on file in string format.
        /// </value>
        public override string LatestEventStatusRep
        {
            get
            {
                var latestEventStatus = this.LatestEventStatus;
                switch (latestEventStatus)
                {
                    case TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest:
                        return "None / NA";
                    case TpoRequestForInitialClosingDisclosureGenerationEventType.Active:
                    case TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled:
                    case TpoRequestForInitialClosingDisclosureGenerationEventType.Completed:
                        return latestEventStatus.ToString();
                    default:
                        throw new UnhandledEnumException(latestEventStatus);
                }
            }
        }

        /// <summary>
        /// Determines whether the latest event for this collection can transition to
        /// the specified <paramref name="newEventType"/>.
        /// </summary>
        /// <param name="newEventType">
        /// The new event type.
        /// </param>
        /// <returns>
        /// True if the transition is allowed, false otherwise.
        /// </returns>
        protected override bool IsAllowedTransitionToNextType(TpoRequestForInitialClosingDisclosureGenerationEventType newEventType)
        {
            var transitionDictionary = Tools.GetAvailableTransitionsForTpoInitialClosingDisclosureRequestGeneration(this.LatestEventStatus);
            return transitionDictionary[newEventType];
        }

        /// <summary>
        /// Creates and returns new <see cref="TpoRequestForInitialClosingDisclosureGenerationEventAuditItem"/> instance.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit item.
        /// </param>
        /// <param name="eventForAudit">
        /// The new event.
        /// </param>
        /// <returns>
        /// A new <see cref="TpoRequestForInitialClosingDisclosureGenerationEventAuditItem"/> instance.
        /// </returns>
        protected override AbstractAuditItem CreateAuditItem(AbstractUserPrincipal principal, LoanEvent<TpoRequestForInitialClosingDisclosureGenerationEventType> eventForAudit)
        {
            return new TpoRequestForInitialClosingDisclosureGenerationEventAuditItem(principal, eventForAudit.Type, eventForAudit.Notes);
        }
    }
}
