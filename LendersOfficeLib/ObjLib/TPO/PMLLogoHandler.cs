namespace LendersOffice.ObjLib.TPO
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Web;
    using DataAccess;

    /// <summary>
    /// Handles requests to retrieve logos in PML.
    /// </summary>
    [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Legacy code copied to new namespace.")]
    public class PMLLogoHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reuseable.
        /// </summary>
        public bool IsReusable => true;

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "image/gif";
            Guid id;
            Guid branchId = Guid.Empty;
            try
            {
                string str = context.Request.QueryString["id"];

                if (str.IndexOf("_") > 0)
                {
                    //// 8/29/2012 dd - In order to support different logo for REMN depends on Branch,
                    //// we are concat the branch id to query string. However we will only hardcode by
                    //// checkin hardcode Pml Lender Site ID for REMN.

                    string[] parts = str.Split('_');
                    id = new Guid(parts[0]);

                    branchId = new Guid(parts[1]);
                }
                else
                {
                    id = new Guid(str);
                }
            }
            catch (Exception e)
            {
                Tools.LogWarning("PmlLogoHandler.cs was not able to retrieve the pml site id from the logo request url", e);
                this.FileNotFound(context);
                return;
            }

            try
            {
                string fileDBKey = string.Empty;
                if (branchId == Guid.Empty)
                {
                    fileDBKey = id.ToString().ToLower() + ".logo.gif";
                }
                else
                {
                    fileDBKey = id.ToString().ToLower() + "_" + branchId.ToString().ToLower() + ".logo.gif";
                }

                byte[] buffer = FileDBTools.ReadData(E_FileDB.Normal, fileDBKey);
                context.Response.BinaryWrite(buffer);
            }
            catch (Exception e)
            {
                Tools.LogWarning("PMLLogo Handler ran into an error with the request", e);
                this.FileNotFound(context);
                return;
            }
        }

        /// <summary>
        /// Sends a "File not found" response.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private void FileNotFound(HttpContext context)
        {
            context.Response.Clear();
            context.Response.StatusCode = 404;
            context.Response.StatusDescription = "File not Found";
        }
    }
}
