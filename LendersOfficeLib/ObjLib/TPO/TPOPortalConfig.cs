﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Web;

namespace LendersOffice.ObjLib.TPO
{
    public class InvalidUserInputException : CBaseException
    {
        public InvalidUserInputException(string userMessage)
            : base(userMessage, userMessage)
        {
        }
    }
    public class ExpectedDBHitException : CBaseException
    {
        public ExpectedDBHitException(string userMessage, Exception srcException)
            :base(userMessage, srcException)
        {
            IsEmailDeveloper = false;
        }
    }
    public class TPOPortalConfig
    {
        #region Fields
        private string m_Url;
        private string m_Name;
        #endregion

        #region Properties
        public string Name
        {
            get { return m_Name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new InvalidUserInputException("Name cannot be empty");
                }
                if (value.Length > SCHEMA_NAME_LENGTH_LIMIT)
                {
                    throw new InvalidUserInputException(string.Format("Name exceeds limit of {0} characters.", SCHEMA_NAME_LENGTH_LIMIT));
                }
                m_Name = value;
            }
        }
        public string Url 
        { 
            get { return m_Url; } 
            set 
            {
                if (!string.IsNullOrEmpty(value) && value.Length > SCHEMA_URL_LENGTH_LIMIT)
                {
                    throw new InvalidUserInputException(string.Format("URL exceeds limit of {0} characters.", SCHEMA_URL_LENGTH_LIMIT));
                }
                if (!Tools.IsValidUrl(value, new string[] { Uri.UriSchemeHttps }))
                {
                    throw new InvalidUserInputException(
                       string.Format("URL {0} must be a valid https url ", value)
                   );
                }
                m_Url = value;
            } 
        }
        public Guid BrokerID { get; private set; }
        public Guid? ID { get; private set; } // null until saved the first time, then never null.
        public bool IsDeleted { get; private set; }
        #endregion

        #region Constructors
        public TPOPortalConfig(Guid brokerID, string name, string url)
        {
            BrokerID = brokerID;
            Name = name;
            Url = url;
        }

        /// <summary>
        ///  reader should come from one of the two retrieval sprocs defined here.
        /// </summary>
        private TPOPortalConfig(DbDataReader reader)
        {
            Name = (string)reader["Name"];
            Url = (string)reader["Url"];
            ID = (Guid)reader["ID"];
            BrokerID = (Guid)reader["BrokerID"];
            IsDeleted = (bool)reader["IsDeleted"];
        }
        #endregion               

        #region DB interaction
        /// <summary>
        /// Might return null, if there's no (nondeleted) config associated with the user or it's originating company.
        /// </summary>
        public static TPOPortalConfig RetrieveByUserID(Guid userID, Guid brokerID)
        {
            var sqlParameters = new SqlParameter[]{
                new SqlParameter("@UserID", userID),
                new SqlParameter("@BrokerID", brokerID),
                new SqlParameter("@PortalMode", PrincipalFactory.CurrentPrincipal.PortalMode)
            };
            using(var reader = StoredProcedureHelper.ExecuteReader(brokerID, PROCNAME_RETRIEVEBYUSERID, sqlParameters))
            {
                if(reader.Read())
                {
                    return new TPOPortalConfig(reader);
                }
                else
                {
                    return null;
                }
            }
        }

        public static TPOPortalConfig RetrieveByBranchId(Guid branchId, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BranchId", branchId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, PROCNAME_RETRIEVEBYBRANCHID, parameters))
            {
                if (reader.Read())
                {
                    return new TPOPortalConfig(reader);
                }

                return null;
            }
        }

        public static DBHitInfo Delete(Guid id, Guid brokerID, BrokerUserPrincipal bp)
        {
            if (!bp.HasPermission(Permission.AllowTpoPortalConfiguring))
            {
                return new DBHitInfo(false, "user lacks permission to delete Originator Portal config");
            }
            var sqlParameters = new SqlParameter[]{
                new SqlParameter("@ID", id),
                new SqlParameter("@BrokerID", brokerID)
            };
            var numRowsAffected = StoredProcedureHelper.ExecuteNonQuery(brokerID, PROCNAME_DELETE, 0, sqlParameters);

            return DBHitInfo.CreateFromRowsAffectedWhenExpectingOne(numRowsAffected, "Was not able to delete.  Please retry.",
                PROCNAME_DELETE, sqlParameters);
        }

        public static TPOPortalConfig RetrieveByID(Guid id, Guid brokerID)
        {
            var sqlParameters = new SqlParameter[]{
                new SqlParameter("@ID", id),
                new SqlParameter("@BrokerID", brokerID)
            };
            using(var reader = StoredProcedureHelper.ExecuteReader(brokerID, PROCNAME_RETRIEVE_BYID, sqlParameters))
            {
                if(reader.Read())
                {
                    return new TPOPortalConfig(reader);
                }
                else
                {
                    throw new NotFoundException("Originator portal configuration not found.",
                        "no TPOPortalConfig with id: " + id + " broker id: " + brokerID);
                }
            }
        }

        /// <summary>
        /// ignores deleted configs
        /// </summary>
        public static IEnumerable<TPOPortalConfig> RetrieveAllActiveAtBroker(Guid brokerID)
        {
            var configs = new List<TPOPortalConfig>();
            var sqlParameters = new SqlParameter[]{
                new SqlParameter("@BrokerID", brokerID)
            };
            
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, PROCNAME_RETRIEVE_BYBROKERID, sqlParameters))
            {
                while (reader.Read())
                {
                    configs.Add(new TPOPortalConfig(reader));
                }
            }
            
            return configs.OrderBy(c => c.Name);
        }

        /// <summary>
        /// Parameters that are common between Create and Update
        /// </summary>
        private List<SqlParameter> CommonParams()
        {
            return new List<SqlParameter>()
            {
                 new SqlParameter("@Name", Name)
                ,new SqlParameter("@Url", Url)
                ,new SqlParameter("@BrokerID", BrokerID)
            };
        }

        /// <summary>
        /// Doesn't update deletion state.  For that, use Delete
        /// </summary>
        public DBHitInfo Save(BrokerUserPrincipal bp)
        {
            if (!bp.HasPermission(Permission.AllowTpoPortalConfiguring))
            {
                return new DBHitInfo(false, "user lacks permission to create/update Originator Portal config");
            }

            var sqlParams = CommonParams();
            var newGuid = Guid.NewGuid(); // only for when we add it to the DB the first time.
            string procName;
            if (ID == null)
            {
                procName = PROCNAME_CREATE;
                sqlParams.Add(new SqlParameter("@ID", newGuid));
            }
            else
            {
                procName = PROCNAME_UPDATE;
                sqlParams.Add(new SqlParameter("@ID", ID.Value));
            }

            int numRowsAffected = 0;
            try
            {
                numRowsAffected = StoredProcedureHelper.ExecuteNonQuery(bp.BrokerId, procName, 0, sqlParams.ToList());
            }
            catch (SqlException sqe)
            {
                bool hasNonUniqueName = false;
                foreach (SqlError error in sqe.Errors)
                {
                    if (error.Number == 547  // constraint failure
                        && error.Message.Contains("Lender_TPO_LandingPageConfig_CheckUniqueNameForNonDeleted"))
                    {
                        hasNonUniqueName = true;
                        break;
                    }
                }
                if (hasNonUniqueName)
                {
                    throw new ExpectedDBHitException("Please rename your configuration so it is unique.", sqe);
                }
                else
                {
                    throw;
                }
            }
            if (numRowsAffected == 1)
            {
                if (ID == null)
                {
                    ID = newGuid;
                }
            }
            return DBHitInfo.CreateFromRowsAffectedWhenExpectingOne(numRowsAffected, "save", procName, sqlParams);
        }
        private static readonly string PROCNAME_CREATE = "TPOPortalConfig_Create";
        private static readonly string PROCNAME_UPDATE = "TPOPortalConfig_Update";
        private static readonly string PROCNAME_RETRIEVE_BYID = "TPOPortalConfig_RetrieveByID";
        private static readonly string PROCNAME_RETRIEVE_BYBROKERID = "TPOPortalConfig_RetrieveAllActiveByBrokerID";
        private static readonly string PROCNAME_DELETE = "TPOPortalConfig_Delete";
        private static readonly string PROCNAME_RETRIEVEBYUSERID = "TPOPortalConfig_RetrieveByUserID";
        private static readonly string PROCNAME_RETRIEVEBYBRANCHID = "TPOPortalConfig_RetrieveByBranchId";
        public static readonly int SCHEMA_NAME_LENGTH_LIMIT = 1000;
        public static readonly int SCHEMA_URL_LENGTH_LIMIT = 2048;
        #endregion
    }
}
