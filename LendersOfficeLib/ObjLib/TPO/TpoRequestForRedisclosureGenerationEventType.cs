﻿namespace LendersOffice.ObjLib.TPO
{
    /// <summary>
    /// Provides an indicator for the type of the re-disclosure generation event.
    /// </summary>
    public enum TpoRequestForRedisclosureGenerationEventType
    {
        /// <summary>
        /// There are no redisclosure generation requests on file.
        /// </summary>
        NoRequest = 0,

        /// <summary>
        /// The redisclosure generation request is active.
        /// </summary>
        Active = 1,

        /// <summary>
        /// The redisclosure generation has been cancelled.
        /// </summary>
        Cancelled = 2,

        /// <summary>
        /// The redisclosure generation has been completed.
        /// </summary>
        Completed = 3
    }
}
