﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a collection of re-disclosure generation events.
    /// </summary>
    public class TpoRequestForRedisclosureGenerationEventCollection : LoanEventCollection<TpoRequestForRedisclosureGenerationEventType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForRedisclosureGenerationEventCollection"/> class.
        /// </summary>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        public TpoRequestForRedisclosureGenerationEventCollection(LosConvert losConvert, Action<AbstractAuditItem> auditAddAction)
            : base(json: string.Empty, losConvert: losConvert, auditAddAction: auditAddAction)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForRedisclosureGenerationEventCollection"/> class.
        /// </summary>
        /// <param name="events">
        /// The events for the collection.
        /// </param>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        public TpoRequestForRedisclosureGenerationEventCollection(List<LoanEvent<TpoRequestForRedisclosureGenerationEventType>> events, LosConvert losConvert, Action<AbstractAuditItem> auditAddAction) 
            : base(events, losConvert, auditAddAction)
        {
        }

        /// <summary>
        /// Gets the status of the latest event on file.
        /// </summary>
        /// <value>
        /// The status of the latest event on file or <see cref="TpoRequestForRedisclosureGenerationEventType.NoRequest"/>
        /// if no events have been logged.
        /// </value>
        public override TpoRequestForRedisclosureGenerationEventType LatestEventStatus => this.LatestEvent?.Type ?? TpoRequestForRedisclosureGenerationEventType.NoRequest;

        /// <summary>
        /// Gets the status of the latest event on file in string format.
        /// </summary>
        /// <value>
        /// The status of the latest event on file in string format.
        /// </value>
        public override string LatestEventStatusRep
        {
            get
            {
                var latestEventStatus = this.LatestEventStatus;
                switch (latestEventStatus)
                {
                    case TpoRequestForRedisclosureGenerationEventType.NoRequest:
                        return "None / NA";
                    case TpoRequestForRedisclosureGenerationEventType.Active:
                    case TpoRequestForRedisclosureGenerationEventType.Cancelled:
                    case TpoRequestForRedisclosureGenerationEventType.Completed:
                        return latestEventStatus.ToString();
                    default:
                        throw new UnhandledEnumException(latestEventStatus);
                }
            }
        }

        /// <summary>
        /// Determines whether the latest event for this collection can transition to
        /// the specified <paramref name="newEventType"/>.
        /// </summary>
        /// <param name="newEventType">
        /// The new event type.
        /// </param>
        /// <returns>
        /// True if the transition is allowed, false otherwise.
        /// </returns>
        protected override bool IsAllowedTransitionToNextType(TpoRequestForRedisclosureGenerationEventType newEventType)
        {
            var transitionDictionary = Tools.GetAvailableTransitionsForTpoRedisclosureRequestGeneration(this.LatestEventStatus);
            return transitionDictionary[newEventType];
        }

        /// <summary>
        /// Creates and returns new <see cref="TpoRequestForRedisclosureGenerationAuditItem"/> instance.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit item.
        /// </param>
        /// <param name="eventForAudit">
        /// The new event.
        /// </param>
        /// <returns>
        /// A new <see cref="TpoRequestForRedisclosureGenerationAuditItem"/> instance.
        /// </returns>
        protected override AbstractAuditItem CreateAuditItem(AbstractUserPrincipal principal, LoanEvent<TpoRequestForRedisclosureGenerationEventType> eventForAudit)
        {
            return new TpoRequestForRedisclosureGenerationAuditItem(principal, eventForAudit.Type, eventForAudit.Notes);
        }
    }
}
