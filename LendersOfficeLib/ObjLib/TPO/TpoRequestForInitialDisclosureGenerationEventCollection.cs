﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a collection of initial disclosure generation events.
    /// </summary>
    public class TpoRequestForInitialDisclosureGenerationEventCollection : LoanEventCollection<TpoRequestForInitialDisclosureGenerationEventType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForInitialDisclosureGenerationEventCollection"/> class.
        /// </summary>
        /// <param name="json">
        /// The JSON string containing the events.
        /// </param>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        public TpoRequestForInitialDisclosureGenerationEventCollection(string json, LosConvert losConvert, Action<AbstractAuditItem> auditAddAction) 
            : base(json, losConvert, auditAddAction)
        {
        }

        /// <summary>
        /// Gets the status of the latest event on file.
        /// </summary>
        /// <value>
        /// The status of the latest event on file or <see cref="TpoRequestForInitialDisclosureGenerationEventType.NoRequest"/>
        /// if no events have been logged.
        /// </value>
        public override TpoRequestForInitialDisclosureGenerationEventType LatestEventStatus => this.LatestEvent?.Type ?? TpoRequestForInitialDisclosureGenerationEventType.NoRequest;

        /// <summary>
        /// Gets the status of the latest event on file in string format.
        /// </summary>
        /// <value>
        /// The status of the latest event on file in string format.
        /// </value>
        public override string LatestEventStatusRep
        {
            get
            {
                var latestEventStatus = this.LatestEventStatus;
                switch (latestEventStatus)
                {
                    case TpoRequestForInitialDisclosureGenerationEventType.NoRequest:
                        return "None / NA";
                    case TpoRequestForInitialDisclosureGenerationEventType.Requested:
                        return "Active";
                    case TpoRequestForInitialDisclosureGenerationEventType.Cancelled:
                    case TpoRequestForInitialDisclosureGenerationEventType.Completed:
                        return latestEventStatus.ToString();
                    default:
                        throw new UnhandledEnumException(latestEventStatus);
                }
            }
        }

        /// <summary>
        /// Determines whether the latest event for this collection can transition to
        /// the specified <paramref name="newEventType"/>.
        /// </summary>
        /// <param name="newEventType">
        /// The new event type.
        /// </param>
        /// <returns>
        /// True if the transition is allowed, false otherwise.
        /// </returns>
        protected override bool IsAllowedTransitionToNextType(TpoRequestForInitialDisclosureGenerationEventType newEventType)
        {
            var transitionDictionary = Tools.GetAvailableTransitionsForTpoInitialDisclosureRequestGeneration(this.LatestEventStatus);
            return transitionDictionary[newEventType];
        }

        /// <summary>
        /// Creates and returns new <see cref="TpoRequestForInitialDisclosureGenerationEventAuditItem"/> instance.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit item.
        /// </param>
        /// <param name="eventForAudit">
        /// The new event.
        /// </param>
        /// <returns>
        /// A new <see cref="TpoRequestForInitialDisclosureGenerationEventAuditItem"/> instance.
        /// </returns>
        protected override AbstractAuditItem CreateAuditItem(AbstractUserPrincipal principal, LoanEvent<TpoRequestForInitialDisclosureGenerationEventType> eventForAudit)
        {
            return new TpoRequestForInitialDisclosureGenerationEventAuditItem(principal, eventForAudit);
        }
    }
}
