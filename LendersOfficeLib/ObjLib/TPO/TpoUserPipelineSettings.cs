﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a means for loading and saving TPO user
    /// pipeline settings.
    /// </summary>
    public class TpoUserPipelineSettings
    {
        /// <summary>
        /// The ID of the broker for the user.
        /// </summary>
        private readonly Guid brokerId;

        /// <summary>
        /// The ID of the user.
        /// </summary>
        private readonly Guid userId;

        /// <summary>
        /// The portal mode of the user.
        /// </summary>
        private readonly E_PortalMode portalMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="TpoUserPipelineSettings"/> class.
        /// </summary>
        /// <param name="principal">
        /// The user for the pipeline settings.
        /// </param>
        public TpoUserPipelineSettings(AbstractUserPrincipal principal)
        {
            this.brokerId = principal.BrokerId;
            this.userId = principal.UserId;
            this.portalMode = principal.PortalMode;
        }

        /// <summary>
        /// Retrieves the pipeline report settings for the user.
        /// </summary>
        /// <returns>
        /// The list of pipeline report settings.
        /// </returns>
        public IEnumerable<TpoUserPipelineReport> Retrieve()
        {
            var procedureName = StoredProcedureName.Create("ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_Retrieve").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@UserId", this.userId),
                new SqlParameter("@PortalMode", this.portalMode)
            };

            var resultSet = new List<Dictionary<string, object>>();

            using (var connection = DbAccessUtils.GetConnection(this.brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    resultSet.Add(reader.ToDictionary());
                }
            }

            return resultSet.Select(resultDict =>
            {
                var rowId = (int)resultDict["RowId"];
                var queryId = (Guid)resultDict["QueryId"];
                var queryName = (string)resultDict["QueryName"];
                var portalDisplayName = (string)resultDict["PortalDisplayName"];
                var overridePipelineReportName = (bool)resultDict["OverridePipelineReportName"];

                return new TpoUserPipelineReport()
                {
                    QueryId = queryId,
                    Name = overridePipelineReportName ? portalDisplayName : queryName,
                    DisplayOrder = rowId
                };
            })
            .OrderBy(report => report.DisplayOrder);
        }

        /// <summary>
        /// Retrieves the name of a report based on the user's settings.
        /// </summary>
        /// <param name="reportId">
        /// The ID of the report.
        /// </param>
        /// <returns>
        /// The name of the report.
        /// </returns>
        public string RetrieveReportNameByReportId(Guid reportId)
        {
            var procedureName = StoredProcedureName.Create("ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveReportNameByReportId").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@UserId", this.userId),
                new SqlParameter("@ReportId", reportId),
                new SqlParameter("@PortalMode", this.portalMode)
            };

            using (var connection = DbAccessUtils.GetConnection(this.brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    var queryName = (string)reader["QueryName"];
                    var portalDisplayName = (string)reader["PortalDisplayName"];
                    var overridePipelineReportName = (bool)reader["OverridePipelineReportName"];

                    return overridePipelineReportName ? portalDisplayName : queryName;
                }

                throw new CBaseException(
                    ErrorMessages.Generic,
                    $"Failed to retrieve name for report {reportId} for user {this.userId}, which does not exist.");
            }
        }

        /// <summary>
        /// Updates the settings for the user.
        /// </summary>
        /// <param name="newReportIds">
        /// The list of new setting IDs.
        /// </param>
        public void Update(IEnumerable<Guid> newReportIds)
        {
            var procedureName = StoredProcedureName.Create("ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_Update").ForceValue();
            var newReportIdXml = new XElement("root", newReportIds.Select(id => new XElement("id", id))).ToString(SaveOptions.DisableFormatting);

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@UserId", this.userId),
                new SqlParameter("@PortalMode", this.portalMode),
                new SqlParameter("@NewReportIdXml", newReportIdXml) { SqlDbType = System.Data.SqlDbType.Xml } 
            };

            using (var connection = DbAccessUtils.GetConnection(this.brokerId).OpenDbConnection())
            {
                DbTransaction transaction = null;

                try
                {
                    transaction = connection.BeginTransaction(System.Data.IsolationLevel.RepeatableRead);

                    StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Thirty);

                    transaction.Commit();
                }
                catch
                {
                    transaction?.Rollback();
                    throw;
                }
            }
        }
    }
}
