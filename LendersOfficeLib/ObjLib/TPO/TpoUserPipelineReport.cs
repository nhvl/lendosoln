﻿namespace LendersOffice.ObjLib.TPO
{
    using System;

    /// <summary>
    /// Provides a simple container for TPO user pipeline report data.
    /// </summary>
    public class TpoUserPipelineReport
    {
        /// <summary>
        /// Gets or sets the ID of the query for the report.
        /// </summary>
        public Guid QueryId { get; set; }

        /// <summary>
        /// Gets or sets the name of the report.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display order for the report.
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
