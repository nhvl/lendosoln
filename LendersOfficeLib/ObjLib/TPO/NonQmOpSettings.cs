﻿namespace LendersOffice.ObjLib.TPO
{
    using System;
    using System.Text.RegularExpressions;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Non-QM Originator Portal settings.
    /// </summary>
    public class NonQmOpSettings
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The employee group name that is enabled, if it is restricted to a specific group.
        /// </summary>
        private string enabledEmployeeGroupName;

        /// <summary>
        /// The OC group name that is enabled, if it is restricted to a specific group.
        /// </summary>
        private string enabledOCGroupName;

        /// <summary>
        /// The boolean that determines if this bit exists in the temp options.
        /// </summary>
        private bool enableNonQmOpSettingsExists;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonQmOpSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The match from the lender's temp option XML content.</param>
        public NonQmOpSettings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.enableNonQmOpSettingsExists = true;

                this.enabledEmployeeGroupName = match.Groups["EmployeeGroup"].Value;
                this.enabledOCGroupName = match.Groups["OCGroup"].Value;
            }
            else
            {
                this.enableNonQmOpSettingsExists = false;
            }
        }

        /// <summary>
        /// Determines if the updated internal pricer UI is enabled for the given principal.
        /// </summary>
        /// <param name="principal">The principal to evaluate.</param>
        /// <returns>True if the user has access. Otherwise, false.</returns>
        public bool IsEnabledForCurrentUser(AbstractUserPrincipal principal)
        {
            if (!this.enableNonQmOpSettingsExists)
            {
                return false;
            }

            bool blankEmployeeName = string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName);
            bool blankOCName = string.IsNullOrWhiteSpace(this.enabledOCGroupName);

            if (principal.Type == "B")
            {
                if (blankEmployeeName)
                {
                    return true;
                }

                if (this.enabledEmployeeGroupName == "EmptyGroup")
                {
                    return false;
                }

                return principal.IsInEmployeeGroup(this.enabledEmployeeGroupName);
            }
            else if (principal.Type == "P")
            {
                if (blankOCName)
                {
                    return true;
                }

                if (this.enabledOCGroupName == "EmptyGroup")
                {
                    return false;
                }

                return principal.IsInPmlBrokerGroup(this.enabledOCGroupName);
            }
            else
            {
                return this.enableNonQmOpSettingsExists;
            }
        }

        /// <summary>
        /// Validates that the employee group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateEmployeeGroup()
        {
            return !this.enableNonQmOpSettingsExists
                || string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName)
                || this.enabledEmployeeGroupName == "EmptyGroup"
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.Employee, this.enabledEmployeeGroupName);
        }

        /// <summary>
        /// Validates that the OC group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateOCGroup()
        {
            return !this.enableNonQmOpSettingsExists
                || string.IsNullOrWhiteSpace(this.enabledOCGroupName)
                || this.enabledOCGroupName == "EmptyGroup"
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.PmlBroker, this.enabledOCGroupName);
        }
    }
}
