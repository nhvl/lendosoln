﻿namespace LendersOffice.ObjLib.TPO.PipelineReport
{
    using System.Collections.Generic;
    using LendersOffice.QueryProcessor;

    /// <summary>
    /// Models an originator portal pipeline report.
    /// </summary>
    public class OriginatorPortalPipelineReportModel
    {
        /// <summary>
        /// Gets or sets the title of the pipeline report.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the number of records in the report.
        /// </summary>
        public int TotalRecordCount { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of viewable records 
        /// in the report.
        /// </summary>
        public int MaxViewableRecords { get; set; }

        /// <summary>
        /// Gets or sets the tables in the pipeline report.
        /// </summary>
        public IEnumerable<OriginatorPortalPipelineReportTableModel> Tables { get; set; }

        /// <summary>
        /// Gets or sets the mappings between column index and column type.
        /// </summary>
        public Dictionary<string, Field.E_ClassType> ColumnTypesByIndex { get; set; }
    }
}
