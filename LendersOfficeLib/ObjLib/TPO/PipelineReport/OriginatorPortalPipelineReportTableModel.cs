﻿namespace LendersOffice.ObjLib.TPO.PipelineReport
{
    using System.Collections.Generic;

    /// <summary>
    /// Models an originator portal pipeline report table.
    /// </summary>
    public class OriginatorPortalPipelineReportTableModel
    {
        /// <summary>
        /// Gets or sets the header for the table.
        /// </summary>
        public IEnumerable<string> Header { get; set; }

        /// <summary>
        /// Gets or sets the rows in the table.
        /// </summary>
        public IEnumerable<OriginatorPortalPipelineReportRowModel> Rows { get; set; }

        /// <summary>
        /// Gets or sets the sub-table containing calculations.
        /// </summary>
        public OriginatorPortalPipelineReportTableModel CalculationTable { get; set; }
    }
}
