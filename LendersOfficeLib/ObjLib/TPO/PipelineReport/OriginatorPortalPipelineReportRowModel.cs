﻿namespace LendersOffice.ObjLib.TPO.PipelineReport
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Models a single row in an originator portal pipeline report.
    /// </summary>
    public class OriginatorPortalPipelineReportRowModel
    {
        /// <summary>
        /// Gets or sets the ID of the loan associated with the row.
        /// </summary>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the cells for the row.
        /// </summary>
        public List<OriginatorPortalPipelineReportCellModel> Cells { get; set; }
    }
}
