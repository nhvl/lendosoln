﻿namespace LendersOffice.ObjLib.TPO.PipelineReport
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Constants;
    using LendersOffice.Common;
    using LendersOffice.QueryProcessor;

    /// <summary>
    /// Generates models for pipeline reports in the Originator Portal.
    /// </summary>
    public class OriginatorPortalPipelineReportModelGenerator
    {
        /// <summary>
        /// The report used to generate a model.
        /// </summary>
        private readonly Report report;

        /// <summary>
        /// Gets or sets the title of the pipeline report.
        /// </summary>
        private readonly string reportTitle;

        /// <summary>
        /// The column names in the report, cached to 
        /// prevent repeatedly iterating the column list.
        /// </summary>
        private readonly ReadOnlyCollection<string> columnNames;

        /// <summary>
        /// The columns in the report that are right-aligned.
        /// </summary>
        private readonly HashSet<int> rightAlignedColumnIndices;

        /// <summary>
        /// The mappings between column index and column type.
        /// </summary>
        private readonly Dictionary<string, Field.E_ClassType> columnTypesByIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="OriginatorPortalPipelineReportModelGenerator"/>
        /// class.
        /// </summary>
        /// <param name="report">
        /// The report used to generate a model.
        /// </param>
        /// <param name="reportTitle">
        /// The title of the pipeline report.
        /// </param>
        public OriginatorPortalPipelineReportModelGenerator(Report report, string reportTitle)
        {
            this.report = report;
            this.reportTitle = reportTitle;

            var reportColumns = report.Columns.Items.Cast<Column>().Select((col, index) => new KeyValuePair<int, Column>(index, col));
            var rightAlignedClassTypes = new[] 
            {
                Field.E_ClassType.Cal,
                Field.E_ClassType.Cnt,
                Field.E_ClassType.Csh,
                Field.E_ClassType.Pct
            };

            this.columnNames = reportColumns.Select(c => c.Value.Name).ToList().AsReadOnly();
            this.columnTypesByIndex = reportColumns.ToDictionary(kvp => kvp.Key.ToString(), kvp => kvp.Value.Kind);
            this.rightAlignedColumnIndices = new HashSet<int>(reportColumns.SelectIndicesWhere(col => 
                col.Value.Kind.EqualsOneOf(rightAlignedClassTypes)));
        }

        /// <summary>
        /// Generates the model for the report.
        /// </summary>
        /// <returns>
        /// The generated model.
        /// </returns>
        public OriginatorPortalPipelineReportModel Generate()
        {
            return new OriginatorPortalPipelineReportModel()
            {
                Title = this.reportTitle,
                TotalRecordCount = this.report.Stats.HitCount,
                MaxViewableRecords = ConstAppDavid.MaxViewableRecordsInPipeline,
                Tables = this.GenerateTables(),
                ColumnTypesByIndex = this.columnTypesByIndex
            };
        }

        /// <summary>
        /// Generates the list of rows for the report.
        /// </summary>
        /// <returns>
        /// The list of rows.
        /// </returns>
        private IEnumerable<OriginatorPortalPipelineReportTableModel> GenerateTables()
        {
            var tables = this.report.Tables;

            if (tables.Count == 0)
            {
                return Enumerable.Empty<OriginatorPortalPipelineReportTableModel>();
            }

            var containerTable = tables[0] as Rows;
            if (containerTable.Count == 0)
            {
                return Enumerable.Empty<OriginatorPortalPipelineReportTableModel>();
            }

            var tableList = new List<OriginatorPortalPipelineReportTableModel>();

            // If containerTable's children are of type Row (not of type Rows), then containerTable
            // is the actual report table and not a dummy table. This situation occurs if the report
            // is comprised of only a single table.
            if (containerTable[0] != null && containerTable[0] is Row)
            {
                this.AddTable(tableList, containerTable);
            }
            else
            {
                this.ProcessReportRows(tableList, containerTable);
            }

            return tableList;
        }

        /// <summary>
        /// Processes the rows in the report.
        /// </summary>
        /// <param name="tableList">
        /// The list of tables in the model.
        /// </param>
        /// <param name="currentReportTable">
        /// The current report table.
        /// </param>
        private void ProcessReportRows(List<OriginatorPortalPipelineReportTableModel> tableList, Rows currentReportTable)
        {
            foreach (object entry in currentReportTable)
            {
                if (entry is Row)
                {
                    this.AddTable(tableList, currentReportTable);
                    return;
                }

                this.ProcessReportRows(tableList, (Rows)entry);
            }

            // If specified in the report, we add another table containing calculated subtotal results.
            if (currentReportTable.GroupBy.Count > 0 && currentReportTable.Results.Count > 0)
            {
                this.AddSubtotalResultsToTableGroup(tableList, currentReportTable);
            }
        }

        /// <summary>
        /// Adds a table to the model.
        /// </summary>
        /// <param name="tableList">
        /// The current list of tables.
        /// </param>
        /// <param name="currentReportTable">
        /// The current table for the report.
        /// </param>
        private void AddTable(List<OriginatorPortalPipelineReportTableModel> tableList, Rows currentReportTable)
        {
            var tableModel = new OriginatorPortalPipelineReportTableModel()
            {
                Header = this.columnNames,
                Rows = this.GenerateRows(currentReportTable)
            };

            if (currentReportTable.Results.Count != 0)
            {
                this.AddSubtotalResultsToCurrentTable(currentReportTable.Results, tableModel);
            }

            tableList.Add(tableModel);
        }

        /// <summary>
        /// Generates a set of row models from the current table row.
        /// </summary>
        /// <param name="currentTableRow">
        /// The current table row.
        /// </param>
        /// <returns>
        /// The generated list of row models.
        /// </returns>
        private IEnumerable<OriginatorPortalPipelineReportRowModel> GenerateRows(Rows currentTableRow)
        {
            return currentTableRow.Items.Cast<Row>().Select(row => new OriginatorPortalPipelineReportRowModel()
            {
                LoanId = row.Key,
                Cells = row.Items.Select((value, index) => new OriginatorPortalPipelineReportCellModel()
                {
                    Text = value.Text,
                    AlignRight = this.rightAlignedColumnIndices.Contains(index)
                }).ToList()
            });
        }

        /// <summary>
        /// Adds subtotal results to a report table group.
        /// </summary>
        /// <param name="tableList">
        /// The list of report table models.
        /// </param>
        /// <param name="currentReportTable">
        /// The current report table.
        /// </param>
        private void AddSubtotalResultsToTableGroup(List<OriginatorPortalPipelineReportTableModel> tableList, Rows currentReportTable)
        {
            var groupSubtotalTable = new OriginatorPortalPipelineReportTableModel()
            {
                Header = this.columnNames
            };

            this.AddSubtotalResultsToCurrentTable(currentReportTable.Results, groupSubtotalTable);

            tableList.Add(groupSubtotalTable);
        }

        /// <summary>
        /// Adds subtotal results to the current table.
        /// </summary>
        /// <param name="calculationResults">
        /// The calculation results for the report.
        /// </param>
        /// <param name="tableModel">
        /// The model for the report table.
        /// </param>
        private void AddSubtotalResultsToCurrentTable(CalculationResults calculationResults, OriginatorPortalPipelineReportTableModel tableModel)
        {
            var subtotalTable = new OriginatorPortalPipelineReportTableModel();
            var groupedResults = calculationResults.Items.Cast<CalculationResult>().GroupBy(result => result.Column);

            var rowList = Enumerable.Range(0, groupedResults.Max(group => group.Count())).Select(groupIndex => new OriginatorPortalPipelineReportRowModel()
            {
                Cells = Enumerable.Range(0, this.columnNames.Count).Select(cellIndex => new OriginatorPortalPipelineReportCellModel()
                {
                    Text = string.Empty,
                    AlignRight = this.rightAlignedColumnIndices.Contains(cellIndex)
                }).ToList()
            }).ToList();

            foreach (var grouping in groupedResults)
            {
                var rowIndex = 0;
                var columnIndex = grouping.Key;

                foreach (var calculationResult in grouping)
                {
                    rowList[rowIndex].Cells[columnIndex].Text = this.CreateSubtotalCell(calculationResult, this.report.Columns[columnIndex]);
                    ++rowIndex;
                }
            }

            subtotalTable.Rows = rowList;
            tableModel.CalculationTable = subtotalTable;
        }

        /// <summary>
        /// Creates a subtotal cell.
        /// </summary>
        /// <param name="result">
        /// The calculation result for the report.
        /// </param>
        /// <param name="column">
        /// The column for the calculation result.
        /// </param>
        /// <returns>
        /// The subtotal cell content.
        /// </returns>
        private string CreateSubtotalCell(CalculationResult result, Column column)
        {
            var subtotalType = this.GetSubtotalType(result);
            var format = this.GetSubtotalFormat(column);

            if (result.What == E_FunctionType.Av && column.Kind == Field.E_ClassType.Cnt)
            {
                format = "N3";
            }

            return subtotalType + " = " + result.GetFormattedResult(format);
        }

        /// <summary>
        /// Gets the subtotal type for the result.
        /// </summary>
        /// <param name="result">
        /// The calculation result.
        /// </param>
        /// <returns>
        /// The subtotal type.
        /// </returns>
        private string GetSubtotalType(CalculationResult result)
        {
            switch (result.What)
            {
                case E_FunctionType.Av:
                    return "Avg";
                case E_FunctionType.Sm:
                    return "Tot";
                case E_FunctionType.Mn:
                    return "Min";
                case E_FunctionType.Mx:
                    return "Max";
                case E_FunctionType.Ct:
                    return "Num";
                default:
                    throw new DataAccess.UnhandledEnumException(result.What);
            }
        }

        /// <summary>
        /// Gets the subtotal format for the column.
        /// </summary>
        /// <param name="column">
        /// The column.
        /// </param>
        /// <returns>
        /// The subtotal format.
        /// </returns>
        private string GetSubtotalFormat(Column column)
        {
            if (column.Kind == Field.E_ClassType.Pct)
            {
                return "N3";
            }

            if (column.Kind == Field.E_ClassType.Csh)
            {
                return "C2";
            }

            if (column.Kind == Field.E_ClassType.Cnt)
            {
                return "N0";
            }

            return "N";
        }
    }
}
