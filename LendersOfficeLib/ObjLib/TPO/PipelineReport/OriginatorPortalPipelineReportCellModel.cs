﻿namespace LendersOffice.ObjLib.TPO.PipelineReport
{
    /// <summary>
    /// Models a single cell in an originator portal pipeline report.
    /// </summary>
    public class OriginatorPortalPipelineReportCellModel
    {
        /// <summary>
        /// Gets or sets the text of the cell.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the cell's text is right-aligned.
        /// </summary>
        public bool AlignRight { get; set; }
    }
}
