﻿namespace LendersOffice.ObjLib.QualifyingBorrower
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The model for the QualifyingBorrower interface.
    /// </summary>
    public class QualifyingBorrowerModel
    {
        /// <summary>
        /// Gets or sets the loan's sPurchasePrice1003 value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sPurchasePrice1003 { get; set; }

        /// <summary>
        /// Gets or sets the loan's sLandIfAcquiredSeparately1003 value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLandIfAcquiredSeparately1003 { get; set; }

        /// <summary>
        /// Gets or sets the loan's sRefTransMortgageBalancePayoffAmt value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sRefTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the loan's sRefNotTransMortgageBalancePayoffAmt value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sRefNotTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotEstBorrCostUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstBorrCostUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotTransCUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotTransCUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sLAmtCalc value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLAmtCalc { get; set; }

        /// <summary>
        /// Gets or sets the loan's sFfUfmipFinanced value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sFfUfmipFinanced { get; set; }

        /// <summary>
        /// Gets or sets the loan's sFinalLAmt value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sFinalLAmt { get; set; }

        /// <summary>
        /// Gets or sets the loan's sONewFinBal value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sONewFinBal { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotMortLAmtUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotMortLAmtUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sSellerCreditsUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sSellerCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotOtherCreditsUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotOtherCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotCreditsUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotMortLTotCreditUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotMortLTotCreditUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTransNetCashUlad value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTransNetCashUlad { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTRIDSellerCredits value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTRIDSellerCredits { get; set; }

        /// <summary>
        /// Gets or sets the loan's sAltCost value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sAltCost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sAltCostLckd value is true.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sAltCostLckd { get; set; }

        /// <summary>
        /// Gets or sets the loan's sLDiscnt1003 value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sLDiscnt1003Lckd value is true.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sLDiscnt1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotEstCcNoDiscnt1003 value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstCcNoDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sTotEstCc1003Lckd value is true.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotEstCc1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotEstPp1003 value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstPp1003 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sTotEstPp1003Lckd value is true.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotEstPp1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the loan's sTotCcPbs value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotCcPbs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sTotCcPbsLocked value.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotCcPbsLocked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's sAltCostLckdDisabled value is true.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sAltCostLckdDisabled { get; set; }
    }
}
