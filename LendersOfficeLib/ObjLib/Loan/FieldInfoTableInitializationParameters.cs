﻿// <copyright file="FieldInfoTableInitializationParameters.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Michael Leinweaver
//  Date:   12/2/2016
// </summary>
namespace LendersOffice.ObjLib.Loan
{
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Provides a simple wrapper for <see cref="CFieldInfoTable"/>
    /// initialization parameters.
    /// </summary>
    public class FieldInfoTableInitializationParameters
    {
        /// <summary>
        /// Gets the list of obsolete property names.
        /// </summary>
        /// <value>
        /// The <see cref="List{T}"/> of obsolete property names.
        /// </value>
        public List<string> ObsoletePropertyNameList { get; } = new List<string>();

        /// <summary>
        /// Gets the list of property name - <see cref="DependsOnAttribute"/> pairs.
        /// </summary>
        /// <value>
        /// The <see cref="List{T}"/> of pairs.
        /// </value>
        public List<KeyValuePair<string, DependsOnAttribute>> DependsOnAttributes { get; } = new List<KeyValuePair<string, DependsOnAttribute>>();
    }
}
