﻿// <copyright file="LoanReadPermissionResolver.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.Loan.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using ConfigSystem;
    using ConfigSystem.Operations;
    using Constants;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// The class checks whether the given user can access the give list of loans. The class is optimized to handle reads by 
    /// batch loading all the data in one query and then running workflow on the entire set. The class currently takes a list of loans 
    /// and only handles read only checks.
    /// </summary>
    public sealed class LoanReadPermissionResolver
    {
        /// <summary>
        /// The id of the broker.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The list of execution engine fields needed for read loan.
        /// </summary>
        private IEnumerable<string> fields;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanReadPermissionResolver"/> class.
        /// </summary>
        /// <param name="brokerId">The id of the broker for the loans.</param>
        public LoanReadPermissionResolver(Guid brokerId)
        {
            this.brokerId = brokerId;
            this.fields = LendingQBExecutingEngine.GetDependencyFieldsByOperation(brokerId, WorkflowOperations.ReadLoanOrTemplate);
        }

        /// <summary>
        /// Populate a dataset based on the input loan ids.
        /// </summary>
        /// <param name="brokerId">The brokerId defines the target DB.</param>
        /// <param name="loanIds">The set of loans that are of interest.</param>
        /// <param name="fields">The list of execution engine fields needed for read loan.</param>
        /// <param name="datasetToFill">The data set to fill.</param>
        /// <param name="enableLogging">True to log the sql query, false otherwise.</param>
        public static void FillLoanList(Guid brokerId, IEnumerable<Guid> loanIds, IEnumerable<string> fields, DataSet datasetToFill, bool enableLogging)
        {
            string sqlQuery = GenerateSelectStatementForReadOnlyOp(loanIds, fields);
            if (enableLogging)
            {
                Tools.LogInfo("AccessControlQuery : " + sqlQuery);
            }

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            DBSelectUtility.FillDataSet(brokerId, datasetToFill, sqlQuery, null, parameters);
        }

        /// <summary>
        /// Checks whether the user has access to all the specified loan ids.
        /// </summary>
        /// <param name="principal">The principal to check for read access.</param>
        /// <param name="loanIds">The loan ids to check read only access to.</param>
        /// <param name="includeTemplates">Whether template workflow permission should be checked.</param>
        /// <returns>The set of loan ids the user can read from loanIds.</returns>
        public HashSet<Guid> GetReadableLoans(AbstractUserPrincipal principal, IEnumerable<Guid> loanIds, bool includeTemplates = true)
        {
            WorkflowOperation op = includeTemplates ? WorkflowOperations.ReadLoanOrTemplate : WorkflowOperations.ReadLoan;

            HashSet<Guid> loansWithReadAccess = new HashSet<Guid>();

            if (!loanIds.Any())
            {
                return loansWithReadAccess;
            }

            DataTable dataTable = this.GetLoanDataTable(loanIds);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                Guid loanId = (Guid)dataRow["sLId"];
                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(loanId, dataRow, this.fields, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
                if (LendingQBExecutingEngine.CanPerform(op, valueEvaluator))
                {
                    loansWithReadAccess.Add(loanId);
                }
            }

            return loansWithReadAccess;
        }

        /// <summary>
        /// Checks read access for loans against multiple principals.
        /// </summary>
        /// <param name="principals">The principals to check for access.</param>
        /// <param name="loanIds">The loan ids.</param>
        /// <returns>A set of loan ids to which a user has read access for each employee id.</returns>
        public Dictionary<Guid, HashSet<Guid>> GetReadableLoans(IEnumerable<AbstractUserPrincipal> principals, IEnumerable<Guid> loanIds)
        {
            var loansWithReadAccessByEmployeeId = new Dictionary<Guid, HashSet<Guid>>();

            if (!loanIds.Any() || !principals.Any())
            {
                return loansWithReadAccessByEmployeeId;
            }

            var dataTable = this.GetLoanDataTable(loanIds);

            var executingEnginePrincipals = principals.Select(p => ExecutingEnginePrincipal.CreateFrom(p));

            foreach (DataRow dataRow in dataTable.Rows)
            {
                Guid loanId = (Guid)dataRow["sLId"];
                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(loanId, dataRow, this.fields, null);

                foreach (var principal in executingEnginePrincipals)
                {
                    valueEvaluator.SetEvaluatingPrincipal(principal);

                    if (LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator))
                    {
                        HashSet<Guid> loansWithReadAccess;

                        if (loansWithReadAccessByEmployeeId.TryGetValue(principal.EmployeeId, out loansWithReadAccess))
                        {
                            loansWithReadAccess.Add(loanId);
                        }
                        else
                        {
                            loansWithReadAccess = new HashSet<Guid> { loanId };
                            loansWithReadAccessByEmployeeId.Add(principal.EmployeeId, loansWithReadAccess);
                        }
                    }
                }
            }

            return loansWithReadAccessByEmployeeId;
        }

        /// <summary>
        /// Takes a list of loan ids and generates a select statement to load all the data required
        /// to run the workflow engine.
        /// </summary>
        /// <param name="loanIds">The set of loan ids.</param>
        /// <param name="fields">The list of execution engine fields needed for read loan.</param>
        /// <returns>A select statement that will load all the needed data for workflow evaluation.</returns>
        private static string GenerateSelectStatementForReadOnlyOp(IEnumerable<Guid> loanIds, IEnumerable<string> fields)
        {
            HashSet<string> selectColumns = new HashSet<string>();

            string[] alwaysLoadFields = new string[]
            {
                "sBrokerId",
                "sBranchId",
                "IsTemplate"
            };

            StringBuilder selectStatementBuilder = new StringBuilder("SELECT fc.sLId, ");

            foreach (var o in fields.Union(alwaysLoadFields))
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);

                if (field == null || !field.IsCachedField)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }

                string prefix;

                switch (field.m_cacheTable)
                {
                    case E_DbCacheTable.Loan_Cache_1:
                        prefix = "fc.";
                        break;
                    case E_DbCacheTable.Loan_Cache_2:
                        prefix = "fc2.";
                        break;
                    case E_DbCacheTable.Loan_Cache_3:
                        prefix = "fc3.";
                        break;
                    case E_DbCacheTable.Loan_Cache_4:
                        prefix = "fc4.";
                        break;
                    case E_DbCacheTable.None:
                    default:
                        throw new UnhandledEnumException(field.m_cacheTable);
                }

                string fieldName = prefix + o;
                if (selectColumns.Add(fieldName))
                {
                    selectStatementBuilder.AppendFormat("{0}{1}, ", prefix, o);
                }
            }

            selectStatementBuilder.Remove(selectStatementBuilder.Length - 2, 2);
            selectStatementBuilder.Append(" FROM Loan_File_Cache fc with(nolock) ");
            selectStatementBuilder.Append("JOIN Loan_File_Cache_2 fc2 with (nolock) on fc2.sLid = fc.sLid ");
            selectStatementBuilder.Append("JOIN LOAN_FILE_CACHE_3 fc3 WITH(NOLOCK) on fc3.slid = fc.slid ");
            selectStatementBuilder.Append("JOIN LOAN_FILE_CACHE_4 fc4 WITH(NOLOCK) on fc4.sLId = fc.sLId ");
            selectStatementBuilder.Append("WHERE fc.sbrokerid = @brokerid and fc.sLID in (");
            
            foreach (Guid loanId in loanIds)
            {
                selectStatementBuilder.Append("'");
                selectStatementBuilder.Append(loanId);
                selectStatementBuilder.Append("', ");
            }

            selectStatementBuilder.Remove(selectStatementBuilder.Length - 2, 2);
            selectStatementBuilder.Append(")");

            return selectStatementBuilder.ToString();
        }

        /// <summary>
        /// Retrieves the data table that will be used to evaluate the read checks.
        /// </summary>
        /// <param name="loanIds">The loan ids to retrieve.</param>
        /// <returns>A populated data table.</returns>
        private DataTable GetLoanDataTable(IEnumerable<Guid> loanIds)
        {
            DataSet ds = new DataSet();

            try
            {
                FillLoanList(this.brokerId, loanIds, this.fields, ds, true);
            }
            catch (LqbGrammar.Exceptions.LqbException e) when (e.InnerException is SqlException)
            {
                Tools.LogError(e.InnerException);
                throw new Common.GenericUserErrorMessageException(e.InnerException);
            }
            catch (SqlException e)
            {
                Tools.LogError(e);
                throw new Common.GenericUserErrorMessageException(e);
            }

            return ds.Tables[0];
        }
    }
}
