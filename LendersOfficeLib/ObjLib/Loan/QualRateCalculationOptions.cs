﻿namespace LendersOffice.ObjLib.Loan
{
    using System;
    using DataAccess;

    /// <summary>
    /// Options for calculating the qualifying rate.
    /// </summary>
    public class QualRateCalculationOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QualRateCalculationOptions"/> class.
        /// </summary>
        public QualRateCalculationOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QualRateCalculationOptions"/> class using
        /// data from the specified <paramref name="product"/> and the specified <paramref name="noteRate"/>
        /// and <paramref name="margin"/>.
        /// </summary>
        /// <param name="product">
        /// The loan program template containing qualifying rate calculation options and index values.
        /// </param>
        /// <param name="noteRate">
        /// The note rate for the calculation.
        /// </param>
        /// <param name="margin">
        /// The margin for the calculation.
        /// </param>
        public QualRateCalculationOptions(ILoanProgramTemplate product, decimal noteRate, decimal margin)
        {
            this.UseQualRate = product.lHasQRateInRateOptions;
            this.CalculationType = product.lQualRateCalculationT;
            this.CalculationField1 = product.lQualRateCalculationFieldT1;
            this.CalculationField2 = product.lQualRateCalculationFieldT2;
            this.Adjustment1 = product.lQualRateCalculationAdjustment1;
            this.Adjustment2 = product.lQualRateCalculationAdjustment2;
            this.IndexRate = Math.Max(product.lRAdjIndexR, 0);
            this.NoteRate = noteRate;
            this.MarginRate = margin;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the qualifying rate should be used.
        /// </summary>
        /// <value>
        /// True if the qualifying rate should be used, false otherwise.
        /// </value>
        public bool UseQualRate { get; set; }

        /// <summary>
        /// Gets or sets the type of the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The type of the calculation.
        /// </value>
        public QualRateCalculationT CalculationType { get; set; }

        /// <summary>
        /// Gets or sets the first field for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The first field for the qualifying rate calculation.
        /// </value>
        public QualRateCalculationFieldT CalculationField1 { get; set; }

        /// <summary>
        /// Gets or sets the second field for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The second field for the qualifying rate calculation.
        /// </value>
        public QualRateCalculationFieldT CalculationField2 { get; set; }

        /// <summary>
        /// Gets or sets the first adjustment for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The first adjustment for the qualifying rate calculation.
        /// </value>
        public decimal Adjustment1 { get; set; }

        /// <summary>
        /// Gets or sets the second adjustment for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The second adjustment for the qualifying rate calculation.
        /// </value>
        public decimal Adjustment2 { get; set; }

        /// <summary>
        /// Gets or sets the note rate for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The note rate for the qualifying rate calculation.
        /// </value>
        public decimal NoteRate { get; set; }

        /// <summary>
        /// Gets the fully-indexed rate for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The fully-indexed rate for the qualifying rate calculation.
        /// </value>
        public decimal FullyIndexedRate
        {
            get { return this.IndexRate + this.MarginRate; }
        }

        /// <summary>
        /// Gets or sets the index rate for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The index rate for the qualifying rate calculation.
        /// </value>
        public decimal IndexRate { get; set; }

        /// <summary>
        /// Gets or sets the margin rate for the qualifying rate calculation.
        /// </summary>
        /// <value>
        /// The margin rate for the qualifying rate calculation.
        /// </value>
        public decimal MarginRate { get; set; }
    }
}
