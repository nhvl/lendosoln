/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
	public class UpdateFromFnmaAuditItem : AbstractAuditItem
	{
        private bool m_isImport1003;
        private bool m_isImportDuFindings;
        private bool m_isImportCreditReport;
		private string m_loginName;

		public UpdateFromFnmaAuditItem(AbstractUserPrincipal principal, bool isImport1003, bool isImportDuFindings, bool isImportCreditReport)
            : base(principal, "Update From DO/DU", "UpdateFromFnma.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
            m_isImport1003 = isImport1003;
            m_isImportDuFindings = isImportDuFindings;
            m_isImportCreditReport = isImportCreditReport;
			m_loginName = principal.LoginNm;

		}
        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "_1003", m_isImport1003 ? "Yes" : "No");
            WriteSafeAttr(writer, "findings", m_isImportDuFindings ? "Yes" : "No");
            WriteSafeAttr(writer, "credit", m_isImportCreditReport ? "Yes" : "No");
			WriteSafeAttr(writer, "login", m_loginName);
            writer.WriteEndElement();
        }


	}
}
