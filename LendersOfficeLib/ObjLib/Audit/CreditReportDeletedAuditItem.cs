﻿using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;
using System.Text;

namespace LendersOffice.Audit
{
    public class CreditReportDeletedAuditItem : AbstractAuditItem
    {
        public enum E_CreditReportDeletionT
        {
            Manual,
            Import,
            Other
        }

        private string LoginName { get; set; }
        private string BorrName { get; set; }
        private string CoborrName { get; set; }
        private DateTime DeleteDate { get; set; }
        private E_CreditReportDeletionT DeletionType { get; set; }

        public CreditReportDeletedAuditItem(AbstractUserPrincipal principal, string borrowerName, string coborrowerName, DateTime deletedDate, E_CreditReportDeletionT type) :
            base(principal, "", "CreditReportDeleted.xslt", DataAccess.E_AuditItemCategoryT.CreditReport)
        {
            LoginName = principal.LoginNm;
            BorrName = borrowerName;
            CoborrName = coborrowerName;
            DeleteDate = deletedDate;
            DeletionType = type;

            switch(type)
            {
                case E_CreditReportDeletionT.Import:
                case E_CreditReportDeletionT.Other:
                    SetDescription("Credit Report Deleted");
                    break;
                case E_CreditReportDeletionT.Manual:
                    SetDescription("Credit Report Deleted (Pricing Engine Run with Manual Credit)");
                    break;
            }
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "login", LoginName);

            if (DeletionType == E_CreditReportDeletionT.Manual)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("The credit report for {0}", BorrName);
                if (!string.IsNullOrEmpty(CoborrName))
                {
                    sb.AppendFormat(" and {0}", CoborrName);
                }
                sb.AppendFormat(" has been deleted from the file on {0} because the pricing engine was run using manual entry of credit info for that application.",
                    DeleteDate.ToShortDateString());

                WriteSafeAttr(writer, "change", sb.ToString());
            }
            writer.WriteEndElement();
        }
    }
}
