using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class LoanSubmissionAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_FIRST_LPNAME = "_1Nm";
        private const string ATTR_FIRST_LPRATE = "_1Rate";
        private const string ATTR_FIRST_LPPOINT = "_1Point";
        private const string ATTR_FIRST_LPPAYMENT = "_1Payment";
        private const string ATTR_FIRST_LPMARGIN = "_1Margin";
        private const string ATTR_FIRST_LPCERTIFICATE = "_1Cert";
        private const string ATTR_FIRST_LPRESULT = "_1Result";
        private const string ATTR_FIRST_PAR = "_ParDebug";

        private const string ATTR_SECOND_LPNAME = "_2Nm";
        private const string ATTR_SECOND_LPRATE = "_2Rate";
        private const string ATTR_SECOND_LPPOINT = "_2Point";
        private const string ATTR_SECOND_LPPAYMENT = "_2Payment";
        private const string ATTR_SECOND_LPMARGIN = "_2Margin";
        private const string ATTR_SECOND_LPCERTIFICATE = "_2Cert";
        private const string ATTR_SECOND_LPRESULT = "_2Result";


        private const string ATTR_DTI = "dti";
        private const string ATTR_IS_RATE_LOCK = "isLckd";
        #endregion

        #region Private member variables
        private string m_first_lpName;
        private string m_first_lpRate;
        private string m_first_lpPoint;
        private string m_first_lpPayment;
        private string m_first_lpMargin;
        private string m_first_lpCertificate;
        private string m_second_lpName;
        private string m_second_lpRate;
        private string m_second_lpPoint;
        private string m_second_lpPayment;
        private string m_second_lpMargin;
        private string m_second_lpCertificate;
        private XDocument m_par1;
        private string m_dti;
        private bool m_isRateLock = false;
        private string m_first_lpResult;
        private string m_second_lpResult;
		private string m_loginName="";
        #endregion

        public LoanSubmissionAuditItem(AbstractUserPrincipal principal,
            string actionDescription,
            string first_lpName, string first_lpRate, string first_lpPoint, string first_lpPayment, string first_lpMargin, string first_lpCertificate,
            string second_lpName, string second_lpRate, string second_lpPoint, string second_lpPayment, string second_lpMargin, string second_lpCertificate,
            string dti, bool isRateLock, string first_lpResult, string second_lpResult, XDocument par1) : base(principal, actionDescription, "LoanSubmission.xslt", E_AuditItemCategoryT.RateLock )
        {
            m_first_lpName = first_lpName;
            m_first_lpRate = first_lpRate;
            m_first_lpPoint = first_lpPoint;
            m_first_lpPayment = first_lpPayment;
            m_first_lpMargin = first_lpMargin;
            m_first_lpCertificate = first_lpCertificate;
            m_first_lpResult = first_lpResult;

            m_second_lpName = second_lpName;
            m_second_lpRate = second_lpRate;
            m_second_lpPoint = second_lpPoint;
            m_second_lpPayment = second_lpPayment;
            m_second_lpMargin = second_lpMargin;
            m_second_lpCertificate = second_lpCertificate;
            m_second_lpResult = second_lpResult;
            m_par1 = par1;
            m_dti = dti;
            m_isRateLock = isRateLock;

			m_loginName = principal.LoginNm;

        }

        protected override void GenerateDetailXml(XmlWriter writer )
        {


            writer.WriteStartElement("data");
            bool isPmlResults3 = (!string.IsNullOrEmpty(m_first_lpResult) && m_first_lpResult.Substring(0, 50).Contains("PricingResultsContainer")) || (!string.IsNullOrEmpty(m_second_lpResult) && m_second_lpResult.Substring(0, 50).Contains("PricingResultsContainer"));
            WriteSafeAttr(writer, "IsPml3Results", isPmlResults3.ToString());
            WriteSafeAttr(writer, "login", m_loginName);
            WriteSafeAttr(writer, ATTR_FIRST_LPNAME, m_first_lpName);
            WriteSafeAttr(writer, ATTR_FIRST_LPRATE, m_first_lpRate);
            WriteSafeAttr(writer, ATTR_FIRST_LPPOINT, m_first_lpPoint);
            WriteSafeAttr(writer, ATTR_FIRST_LPPAYMENT, m_first_lpPayment);
            WriteSafeAttr(writer, ATTR_FIRST_LPMARGIN, m_first_lpMargin);

            WriteSafeAttr(writer, ATTR_SECOND_LPNAME, m_second_lpName);
            WriteSafeAttr(writer, ATTR_SECOND_LPRATE, m_second_lpRate);
            WriteSafeAttr(writer, ATTR_SECOND_LPPOINT, m_second_lpPoint);
            WriteSafeAttr(writer, ATTR_SECOND_LPPAYMENT, m_second_lpPayment);
            WriteSafeAttr(writer, ATTR_SECOND_LPMARGIN, m_second_lpMargin);

            WriteSafeAttr(writer, ATTR_DTI, m_dti);
            WriteSafeAttr(writer, ATTR_IS_RATE_LOCK, m_isRateLock);
			           
            if (null != m_first_lpCertificate) 
            {
                writer.WriteStartElement(ATTR_FIRST_LPCERTIFICATE);
                writer.WriteCData(m_first_lpCertificate);
                writer.WriteEndElement();
            }

            if (null != m_second_lpCertificate) 
            {
                writer.WriteStartElement(ATTR_SECOND_LPCERTIFICATE);
                writer.WriteCData(m_second_lpCertificate);
                writer.WriteEndElement();
            }

            if (null != m_first_lpResult && m_first_lpResult != "" ) 
            {
                writer.WriteStartElement(ATTR_FIRST_LPRESULT);
                writer.WriteCData(m_first_lpResult);
                writer.WriteEndElement();
            }

            if (null != m_second_lpResult && m_second_lpResult != "") 
            {
                writer.WriteStartElement(ATTR_SECOND_LPRESULT);
                writer.WriteCData(m_second_lpResult);
                writer.WriteEndElement();
            }

            if (null != m_par1)
            {
                writer.WriteStartElement(ATTR_FIRST_PAR);
                HtmlTable parRateDebugInfo = new HtmlTable();
                HtmlTableRow headerRow = new HtmlTableRow();
                headerRow.Attributes.Add("class", "GridHeader");
                headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Loan Program"});
                headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Rate" });
                headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Point" });
                parRateDebugInfo.Rows.Add(headerRow);
                bool isAlt = false;
                foreach (XElement el in m_par1.Root.Elements("RateOption"))
                {
                    HtmlTableRow row = new HtmlTableRow();
                    row.Cells.Add(new HtmlTableCell() { InnerText = el.Attribute("Name").Value});
                    row.Cells.Add(new HtmlTableCell() { InnerText = el.Attribute("Rate").Value });
                    row.Cells.Add(new HtmlTableCell() { InnerText = el.Attribute("Point").Value });

             
                    if (isAlt)
                    {
                        row.Attributes.Add("class","GridAlternatingItem");
                    }
                    else
                    {
                        row.Attributes.Add("class","GridItem");
                    }

                    if (el.Attribute("Winner") != null)
                    {
                        row.Attributes["class"] = row.Attributes["class"] + " winner";
                    }
                    parRateDebugInfo.Rows.Add(row);
                    isAlt = !isAlt;
                }

                using (StringWriter sw = new StringWriter())
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.Attributes.Add("class", "ProgramName");
                    span.InnerText = m_par1.Root.Attribute("Name").Value;
                    span.RenderControl(hw);
                    parRateDebugInfo.RenderControl(hw);
                    hw.Flush();
                    writer.WriteCData(sw.ToString());
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement(); //</data>
        }

	}
}
