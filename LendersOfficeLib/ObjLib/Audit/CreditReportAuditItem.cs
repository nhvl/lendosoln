using System;
using System.Xml;

using LendersOffice.Security;
using LendersOffice.Common;
namespace LendersOffice.Audit
{
    public enum E_CreditReportAuditSourceT 
    {
        OrderNew,
        Reissue,
        ImportFromDoDu,
        ImportMergedFromLp,
        ImportInfileFromLp,
		Upgrade, //OPM 12769 - jM 
        ImportFileFromLoansPQ,
        OrderLqiReport, // OPM 238673 - JE
        ReissueLqiReport,
        RemoveBorrower,
        RemoveCoBorrower,
        RetrieveSupplement,
        RetrieveBillingReport,
    }

	public class CreditReportAuditItem : AbstractAuditItem
	{
        private const string ATTR_FILEDBKEY = "filedbkey";
        private Guid m_fileDbKey = Guid.Empty;
		private string m_loginName;

		public CreditReportAuditItem(AbstractUserPrincipal principal, Guid fileDbKey, E_CreditReportAuditSourceT creditReportAuditSource) :
            base (principal, GetDescription(creditReportAuditSource), "CreditReport.xslt", DataAccess.E_AuditItemCategoryT.CreditReport)
		{
            m_fileDbKey = fileDbKey;
			m_loginName = principal.LoginNm;
		}

        private CreditReportAuditItem(AbstractUserPrincipal principal, E_CreditReportAuditSourceT creditReportAuditSource)
            : base(principal, GetDescription(creditReportAuditSource), null, DataAccess.E_AuditItemCategoryT.CreditReport)
        {
            this.HasDetails = false;
            this.m_loginName = principal.LoginNm;
        }

        /// <summary>
        /// Creates an audit entry that will not have details or show the credit report XML results.
        /// </summary>
        /// <param name="principal">The user who the audit is recording.</param>
        /// <param name="creditReportAuditSource">The type of credit report audit to generate.</param>
        /// <returns>An audit event.</returns>
        public static CreditReportAuditItem CreateSimpleAudit(AbstractUserPrincipal principal, E_CreditReportAuditSourceT creditReportAuditSource)
        {
            return new CreditReportAuditItem(principal, creditReportAuditSource);
        }

        private static string GetDescription(E_CreditReportAuditSourceT creditReportAuditSource)
        {
            string description = "";
            switch (creditReportAuditSource)
            {
                case E_CreditReportAuditSourceT.OrderNew:
                    description = "Order New Credit Report";
                    break;
                case E_CreditReportAuditSourceT.Reissue:
                    description = "Reissue Credit Report";
                    break;
                case E_CreditReportAuditSourceT.ImportFromDoDu:
                    description = "Import credit report from DO / DU";
                    break;
                case E_CreditReportAuditSourceT.ImportMergedFromLp:
                    description = "Import merged credit report from LPA";
                    break;
                case E_CreditReportAuditSourceT.ImportInfileFromLp:
                    description = "Import credit infile from LPA";
                    break;
                case E_CreditReportAuditSourceT.Upgrade: //OPM 12769 - jM
                    description = "Upgrade Credit Report";
                    break;
                case E_CreditReportAuditSourceT.ImportFileFromLoansPQ:
                    description = "Import credit from LoansPQ";
                    break;
                case E_CreditReportAuditSourceT.OrderLqiReport:
                    description = "Order New LQI Credit Report";
                    break;
                case E_CreditReportAuditSourceT.ReissueLqiReport:
                    description = "Reissue LQI Credit Report";
                    break;
                case E_CreditReportAuditSourceT.RemoveBorrower:
                    description = "Remove Credit Report Borrower";
                    break;
                case E_CreditReportAuditSourceT.RemoveCoBorrower:
                    description = "Remove Credit Report Co-Borrower";
                    break;
                case E_CreditReportAuditSourceT.RetrieveSupplement:
                    description = "Retrieve Credit Report Supplement";
                    break;
                case E_CreditReportAuditSourceT.RetrieveBillingReport:
                    description = "Retrieve Credit Report Billing Report";
                    break;
            }

            return description;
        }

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_FILEDBKEY, m_fileDbKey);
			WriteSafeAttr(writer, "login", m_loginName);
            writer.WriteEndElement();
        }
	}
}
