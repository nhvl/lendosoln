﻿namespace LendersOffice.Audit
{
    using System.Xml;
    using DataAccess;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit item for TPO initial disclosure generation requests.
    /// </summary>
    public class TpoRequestForInitialDisclosureGenerationEventAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForInitialDisclosureGenerationEventAuditItem"/> class.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit item.
        /// </param>
        /// <param name="eventToAudit">
        /// The event to audit.
        /// </param>
        public TpoRequestForInitialDisclosureGenerationEventAuditItem(AbstractUserPrincipal principal, LoanEvent<TpoRequestForInitialDisclosureGenerationEventType> eventToAudit)
            : base(principal, GetDescription(eventToAudit.Type), "TpoRequestForInitialDisclosureGenerationEvent.xslt", E_AuditItemCategoryT.DisclosureESign)
        {
            this.Event = eventToAudit;
        }

        /// <summary>
        /// Gets or sets the event to audit.
        /// </summary>
        /// <value>
        /// The event to audit.
        /// </value>
        private LoanEvent<TpoRequestForInitialDisclosureGenerationEventType> Event { get; set; }

        /// <summary>
        /// Generates the XML for the audit item.
        /// </summary>
        /// <param name="writer">
        /// The writer to write XML.
        /// </param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            LoanEventXmlGenerator.GenerateXml(writer, this.Event);
        }

        /// <summary>
        /// Gets the description for the audit item based on the <paramref name="eventType"/>.
        /// </summary>
        /// <param name="eventType">
        /// The type of the event.
        /// </param>
        /// <returns>
        /// The description for the audit item.
        /// </returns>
        private static string GetDescription(TpoRequestForInitialDisclosureGenerationEventType eventType)
        {
            switch (eventType)
            {
                case TpoRequestForInitialDisclosureGenerationEventType.Requested:
                    return "Originator portal user has requested lender to generate initial disclosure package.";
                case TpoRequestForInitialDisclosureGenerationEventType.Cancelled:
                    return "Pending request for initial disclosure generation has been cancelled.";
                case TpoRequestForInitialDisclosureGenerationEventType.Completed:
                    return "Request for initial disclosure generation has been completed.";
                case TpoRequestForInitialDisclosureGenerationEventType.NoRequest:
                default:
                    throw new UnhandledEnumException(eventType);
            }
        }
    }
}
