﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Audit;

namespace LendersOffice.ObjLib.Audit
{
    public sealed class H4HSubmissionAuditItem : AbstractAuditItem
    {
        #region ATTR Constants
        #endregion

        private string m_sH4HRequestXml;
        private string m_sLoginName;

        public H4HSubmissionAuditItem(string loginName, string h4hXml) :
            base("Update to FHA Connection", "H4HSubmissionAuditItem.xslt", DataAccess.E_AuditItemCategoryT.Integration)
        {
            m_sLoginName = loginName;
            m_sH4HRequestXml = h4hXml;
        }

        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "login", m_sLoginName);
            writer.WriteStartElement("H4HMismo");
            writer.WriteCData(m_sH4HRequestXml);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
