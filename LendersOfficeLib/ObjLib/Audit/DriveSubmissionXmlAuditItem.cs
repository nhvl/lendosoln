﻿using System;
using System.Xml;

using LendersOffice.Security;
using LendersOffice.Common;
using System.Security;
namespace LendersOffice.Audit
{
    public class DriveSubmissionXmlAuditItem : AbstractAuditItem
    {
        private string m_xml;
        private string m_loginName;

        public DriveSubmissionXmlAuditItem(AbstractUserPrincipal principal, string xml) :
            base(principal, "DataVerify DRIVE Response (XML)", "DRIVESubmissionXml.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
            m_xml = xml;
            m_loginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");

            WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_xml)
            {
                if (false == m_xml.Contains("]]>"))
                {
                    writer.WriteStartElement("xml");
                    writer.WriteCData(m_xml); // Next time... just write a text node
                    writer.WriteEndElement();
                }
                else
                {
                    writer.WriteStartElement("escaped_xml");
                    writer.WriteCData(SecurityElement.Escape(m_xml));
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement(); // </data>
        }

    }
}
