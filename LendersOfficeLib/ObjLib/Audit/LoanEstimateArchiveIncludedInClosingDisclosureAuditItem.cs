﻿namespace LendersOffice.ObjLib.Audit
{
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Audit for when LE archive is put into "Included in Closing Disclosure" status.
    /// </summary>
    public class LoanEstimateArchiveIncludedInClosingDisclosureAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEstimateArchiveIncludedInClosingDisclosureAuditItem"/> class.
        /// </summary>
        public LoanEstimateArchiveIncludedInClosingDisclosureAuditItem()
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, DataAccess.E_AuditItemCategoryT.LoanEstimateArchived)
        {
            this.HasDetails = false;
        }

        /// <summary>
        /// Creates an audit item for when the user manually selects the status on the LE archive page.
        /// </summary>
        /// <param name="archiveDate">The date of the archive.</param>
        /// <returns>The audit item.</returns>
        public static LoanEstimateArchiveIncludedInClosingDisclosureAuditItem ManuallyMarkedAsIncluded(string archiveDate)
        {
            var audit = new LoanEstimateArchiveIncludedInClosingDisclosureAuditItem();
            audit.SetDescription(
                "Archive from " + archiveDate + "'s status was manually changed to \"Included in Closing Disclosure\".");
            return audit;
        }

        /// <summary>
        /// Creates an audit item for when the user updates the status via the seamless document generation prompt.
        /// </summary>
        /// <param name="archiveDate">The date of the archive.</param>
        /// <returns>The audit item.</returns>
        public static LoanEstimateArchiveIncludedInClosingDisclosureAuditItem IncludedViaDocumentGeneration(string archiveDate)
        {
            var audit = new LoanEstimateArchiveIncludedInClosingDisclosureAuditItem();
            audit.SetDescription(
                "Archive from " + archiveDate + "'s status was changed to \"Included in Closing Disclosure\" " +
                "because a Closing Disclosure was created and \"Include in Closing Disclosure\" was selected.");
            return audit;
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="writer">The XmlWriter to which nothing will be done.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
        }
    }
}
