﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// A helper class to manage the creation of disclosure metadata audit events.
    /// </summary>
    public class DisclosureMetadataUpdateAuditHelper
    {
        /// <summary>
        /// A list of fields in <see cref="LoanEstimateDates"/> and <see cref="ClosingDisclosureDates"/> that should be tracked.
        /// </summary>
        private static readonly string[] TrackedFields = new string[]
        {
            "CreatedDate_rep",
            "IssuedDate_rep",
            "DeliveryMethod_rep",
            "ReceivedDate_rep",
            "IsInitial_rep",
            "IsPreview_rep",
            "IsFinal_rep",
            "LastDisclosedTRIDLoanProductDescription",
            "DocVendorApr_rep",
            "SignedDate_rep",
            "DocCode",
            "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower_rep",
            "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller_rep",
            "IsDisclosurePostClosingDueToNonNumericalClericalError_rep",
            "IsDisclosurePostClosingDueToCureForToleranceViolation_rep",
            "PostConsummationRedisclosureReasonDate_rep",
            "PostConsummationKnowledgeOfEventDate_rep"
        };

        /// <summary>
        /// Creates a disclosure metadata update event for a loan estimate.
        /// </summary>
        /// <param name="oldData">The old data.</param>
        /// <param name="newData">The new data.</param>
        /// <param name="borrowerNamesByConsumerId">A collection of borrower names keyed by consumer ID.</param>
        /// <returns>An audit item.</returns>
        public static DisclosureMetadataUpdateAuditItem CreateAuditEvent(LoanEstimateDates oldData, LoanEstimateDates newData, Dictionary<Guid, string> borrowerNamesByConsumerId)
        {
            var auditDescription = "Loan Estimate Update";
            var fieldChanges = RecordChangedData(oldData, newData);

            var borrowerFieldChanges = new Dictionary<Guid, List<AuditFieldChange>>();
            foreach (var newBorrowerDataKvp in newData.DisclosureDatesByConsumerId)
            {
                var consumerId = newBorrowerDataKvp.Key;
                var oldBorrowerDataKvp = oldData.DisclosureDatesByConsumerId.FirstOrDefault(kvp => kvp.Key == consumerId);
                var borrowerChanges = RecordChangedData(oldBorrowerDataKvp.Value, newBorrowerDataKvp.Value);
                borrowerFieldChanges[consumerId] = borrowerChanges;
            }

            if (fieldChanges.Any() || borrowerFieldChanges.Any(kvp => kvp.Value.Any()))
            {
                return new DisclosureMetadataUpdateAuditItem(auditDescription, newData.TransactionId, fieldChanges, borrowerFieldChanges, borrowerNamesByConsumerId);
            }

            return null;
        }

        /// <summary>
        /// Creates a disclosure metadata update event for a closing disclosure.
        /// </summary>
        /// <param name="oldData">The old data.</param>
        /// <param name="newData">The new data.</param>
        /// <param name="borrowerNamesByConsumerId">A collection of borrower names keyed by consumer ID.</param>
        /// <returns>An audit item.</returns>
        public static DisclosureMetadataUpdateAuditItem CreateAuditEvent(ClosingDisclosureDates oldData, ClosingDisclosureDates newData, Dictionary<Guid, string> borrowerNamesByConsumerId)
        {
            var auditDescription = "Closing Disclosure Update";
            var fieldChanges = RecordChangedData(oldData, newData);

            var borrowerFieldChanges = new Dictionary<Guid, List<AuditFieldChange>>();
            foreach (var newBorrowerDataKvp in newData.DisclosureDatesByConsumerId)
            {
                var consumerId = newBorrowerDataKvp.Key;
                var oldBorrowerDataKvp = oldData.DisclosureDatesByConsumerId.FirstOrDefault(kvp => kvp.Key == consumerId);
                var borrowerChanges = RecordChangedData(oldBorrowerDataKvp.Value, newBorrowerDataKvp.Value);
                borrowerFieldChanges[consumerId] = borrowerChanges;
            }

            if (fieldChanges.Any() || borrowerFieldChanges.Any(kvp => kvp.Value.Any()))
            {
                return new DisclosureMetadataUpdateAuditItem(auditDescription, newData.TransactionId, fieldChanges, borrowerFieldChanges, borrowerNamesByConsumerId);
            }

            return null;
        }

        /// <summary>
        /// Records the field changes between old and new data.
        /// </summary>
        /// <param name="oldData">The old data.</param>
        /// <param name="newData">The new data.</param>
        /// <returns>A list of changes.</returns>
        private static List<AuditFieldChange> RecordChangedData(object oldData, object newData)
        {
            if (oldData.GetType() != newData.GetType())
            {
                throw new CBaseException(ErrorMessages.Generic, "Both objects must share a type.");
            }

            var changes = new List<AuditFieldChange>();
            var type = oldData.GetType();

            foreach (PropertyInfo propertyInfo in type.GetProperties())
            {
                var fieldName = propertyInfo.Name;
                if (propertyInfo.CanRead && TrackedFields.Contains(fieldName))
                {
                    var oldValue = propertyInfo.GetValue(oldData)?.ToString() ?? string.Empty;
                    var newValue = propertyInfo.GetValue(newData)?.ToString() ?? string.Empty;

                    if (oldValue != newValue)
                    {
                        // For better readability, don't include the "_rep" portion of any field names in the audit itself.
                        var change = new AuditFieldChange(fieldName.Replace("_rep", string.Empty), oldValue, newValue);
                        changes.Add(change);
                    }
                }
            }

            return changes;
        }
    }
}
