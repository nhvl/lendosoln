﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using DataAccess;

    /// <summary>
    /// Represents a system update to the metadata for an LE or CD.
    /// </summary>
    public class DisclosureMetadataUpdateAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisclosureMetadataUpdateAuditItem"/> class.
        /// </summary>
        /// <param name="description">The audit description.</param>
        /// <param name="transactionId">The transaction ID of the disclosure being modified.</param>
        /// <param name="fieldChanges">A list of field changes.</param>
        /// <param name="borrowerChanges">A collection of borrower-level changes.</param>
        /// <param name="borrowerNamesByConsumerId">A collection of borrower names by consumer ID.</param>
        public DisclosureMetadataUpdateAuditItem(string description, string transactionId, List<AuditFieldChange> fieldChanges, Dictionary<Guid, List<AuditFieldChange>> borrowerChanges, Dictionary<Guid, string> borrowerNamesByConsumerId)
            : base(description, "DisclosureMetadataUpdate.xslt", E_AuditItemCategoryT.Integration)
        {
            this.HasDetails = true;
            this.UserName = "System User"; // This audit can only be triggered by a doc vendor postback, not by a user.
            this.TransactionId = transactionId;
            this.DisclosureFieldChanges = fieldChanges;
            this.BorrowerFieldChanges = borrowerChanges;
            this.BorrowerNamesByConsumerId = borrowerNamesByConsumerId;
        }

        /// <summary>
        /// Gets the transaction ID of the modified disclosure.
        /// </summary>
        protected string TransactionId { get; private set; }

        /// <summary>
        /// Gets the field changes that occurred.
        /// </summary>
        protected List<AuditFieldChange> DisclosureFieldChanges { get; private set; }

        /// <summary>
        /// Gets the borrower-level field changes that occurred.
        /// </summary>
        protected Dictionary<Guid, List<AuditFieldChange>> BorrowerFieldChanges { get; private set; }

        /// <summary>
        /// Gets a collection of borrower names by consumer ID.
        /// </summary>
        protected Dictionary<Guid, string> BorrowerNamesByConsumerId { get; private set; }

        /// <summary>
        /// Generates the audit detail XML.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "transactionId", this.TransactionId);

            foreach (var change in this.DisclosureFieldChanges)
            {
                writer.WriteStartElement("change");
                this.WriteSafeAttr(writer, "field", change.Field);
                this.WriteSafeAttr(writer, "old", change.OldValue);
                this.WriteSafeAttr(writer, "new", change.NewValue);
                writer.WriteEndElement(); // </change>
            }

            foreach (var kvp in this.BorrowerFieldChanges)
            {
                if (!kvp.Value.Any())
                {
                    // No changes to record for this borrower.
                    continue;
                }

                writer.WriteStartElement("borrower");
                this.WriteSafeAttr(writer, "name", this.BorrowerNamesByConsumerId[kvp.Key]);

                foreach (var borrowerChange in kvp.Value)
                {
                    writer.WriteStartElement("change");
                    this.WriteSafeAttr(writer, "field", borrowerChange.Field);
                    this.WriteSafeAttr(writer, "old", borrowerChange.OldValue);
                    this.WriteSafeAttr(writer, "new", borrowerChange.NewValue);
                    writer.WriteEndElement(); // </change>
                }

                writer.WriteEndElement(); // </borrower>
            }

            writer.WriteEndElement(); // </data>
        }
    }
}
