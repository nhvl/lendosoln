﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using LendersOffice.Security;

    /// <summary>
    /// An audit item representing an exchange between LendingQB and Symitar.
    /// </summary>
    public class SymitarAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// A value indicating whether this audit represents an import or export.
        /// </summary>
        private bool isImport;

        /// <summary>
        /// A comparison of the values before and after import. Item 1 is the description, item 2 is the old value, item 3 is the new value.
        /// </summary>
        private IList<Tuple<string, string, string>> importValueComparison;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarAuditItem"/> class.
        /// </summary>
        /// <param name="principal">The principal who is performing the exchange.</param>
        /// <param name="isImport">A value indicating whether this audit represents an import or export.</param>
        /// <param name="importValueComparison">A comparison of the values before and after import. Item 1 is the description, item 2 is the old value, item 3 is the new value.</param>
        private SymitarAuditItem(AbstractUserPrincipal principal, bool isImport, IList<Tuple<string, string, string>> importValueComparison = null)
            : base(
                  principal,
                  isImport ? "Import from Symitar" : "Export to Symitar",
                  "SymitarImportExport.xslt",
                  DataAccess.E_AuditItemCategoryT.Integration)
        {
            this.isImport = isImport;
            this.importValueComparison = importValueComparison;
        }

        /// <summary>
        /// Creates an audit item representing an import of data from Symitar into a LendingQB loan.
        /// </summary>
        /// <param name="principal">The principal who is performing the import.</param>
        /// <param name="valueComparison">A comparison of the values before and after import. Item 1 is the description, item 2 is the old value, item 3 is the new value.</param>
        /// <returns>An audit item representing the specified import event.</returns>
        public static SymitarAuditItem CreateImportAuditItem(AbstractUserPrincipal principal, IList<Tuple<string, string, string>> valueComparison)
        {
            return new SymitarAuditItem(principal, isImport: true, importValueComparison: valueComparison);
        }

        /// <summary>
        /// Creates an audit item representing an export of data to Symitar from a LendingQB loan.
        /// </summary>
        /// <param name="principal">The principal who is performing the export.</param>
        /// <returns>An audit item representing the specified export event.</returns>
        public static SymitarAuditItem CreateExportAuditItem(AbstractUserPrincipal principal)
        {
            return new SymitarAuditItem(principal, isImport: false);
        }

        /// <summary>
        /// Generates XML representing the details of the event.
        /// </summary>
        /// <param name="writer">The writer receiving the XML data.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            var root = new XElement("data");
            if (this.isImport)
            {
                root.Add(new XElement(
                    "import",
                    this.importValueComparison.Select(item =>
                        new XElement(
                            "val",
                            new XAttribute("desc", item.Item1),
                            new XAttribute("old", item.Item2),
                            new XAttribute("new", item.Item3)))));
            }
            else
            {
                root.Add(new XElement("export"));
            }

            root.WriteTo(writer);
        }
    }
}
