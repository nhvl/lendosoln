using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Events;

namespace LendersOffice.Audit
{

	/// <summary>
	/// Summary description for DataTracAuditItem.
	/// </summary>
	public class DataTracAuditItem : AbstractAuditItem
	{
		#region ATTR Constants
		private const string ATTR_LOGIN = "login";
		private const string ATTR_EVENT_TYPE = "op";
		private const string ATTR_EVENT = "event";
		private const string ATTR_DATATRAC_LOAN_NAME = "dtName";
		private const string ATTR_DATATRAC_PROGRAM_NAME = "dtProgram";
		
        #endregion

		#region Variables
		private E_DataTracInteractionT m_eventType;
		private string m_loginName;
		private string m_loanNumber;
		private string m_DataTracLoanName;
		private string m_DataTracProgramName;

		#endregion

		public DataTracAuditItem(AbstractUserPrincipal principal, E_DataTracInteractionT type, string lNumber, string dtLoanName, string dtProgName) : base(principal, (type == E_DataTracInteractionT.ExportToDataTrac) ? "Export to DataTrac" : "Import from DataTrac", "DataTracEvent.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
			m_eventType = type; //import or export
			m_loginName = principal.LoginNm;//user login
			m_loanNumber = lNumber; //loan number
			m_DataTracLoanName = dtLoanName;
			m_DataTracProgramName = dtProgName;
		}
		protected override void GenerateDetailXml(XmlWriter writer) 
		{
			writer.WriteStartElement("data");
			WriteSafeAttr(writer, ATTR_LOGIN, m_loginName);

			if (!m_DataTracLoanName.Equals(m_loanNumber))
				WriteSafeAttr(writer, ATTR_DATATRAC_LOAN_NAME, " as " + m_DataTracLoanName + ".");
			WriteSafeAttr(writer, ATTR_DATATRAC_PROGRAM_NAME, m_DataTracProgramName);
						
			string title = "";
			string DTevent = "";


			switch (m_eventType) 
			{
				case E_DataTracInteractionT.ExportToDataTrac:
					title = "Export";
					DTevent = "Loan exported to DataTrac";
					break;
				case E_DataTracInteractionT.ImportFromDataTrac:
					title = "Import";
					DTevent = "Loan imported from DataTrac";
					break;
			}
			if (!m_DataTracLoanName.Equals(m_loanNumber))
				DTevent += " as \'" + m_DataTracLoanName + "\'";
			DTevent += ".";

			WriteSafeAttr(writer, ATTR_EVENT, DTevent);
			WriteSafeAttr(writer, ATTR_EVENT_TYPE, title);

			writer.WriteEndElement(); // </data>



		}
	}
}
