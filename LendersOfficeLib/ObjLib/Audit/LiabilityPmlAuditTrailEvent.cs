﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// A POD class representing a PML event in this history of a liabilty.
    /// </summary>
    public sealed class LiabilityPmlAuditTrailEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityPmlAuditTrailEvent" /> class.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="loginId">The login identifier.</param>
        /// <param name="action">The action.</param>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="eventDate">The event date.</param>
        public LiabilityPmlAuditTrailEvent(string userName, string loginId, string action, string field, string value, string eventDate)
        {
            this.UserName = userName;
            this.LoginId = loginId;
            this.Action = action;
            this.Field = field;
            this.Value = value;
            this.EventDate = eventDate;
        }

        /// <summary>
        /// Gets the user name that executed the action.
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// Gets the LoginId of the user that executed the action.
        /// </summary>
        public string LoginId { get; private set; }

        /// <summary>
        /// Gets the Action that was executed.
        /// </summary>
        public string Action { get; private set; }

        /// <summary>
        /// Gets the field that was subject of the action.
        /// </summary>
        public string Field { get; private set; }

        /// <summary>
        /// Gets the value that was subject of the action.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the date the event took place.
        /// </summary>
        public string EventDate { get; private set; }
    }
}
