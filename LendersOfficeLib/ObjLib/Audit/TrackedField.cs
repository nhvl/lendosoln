﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Audit
{
    public class TrackedField
    {
        private static IDictionary<string, TrackedField> x_TrackedFields = null;
        private static object x_lock = new object();

        public string FieldId { get; private set; }
        public string Description { get; private set; }
        public bool RequiresLockDeskPermission { get; private set; }
        public bool RequiresCloserPermission { get; private set; }
        public bool RequiresAccountantPermision { get; private set; }
        public bool RequiresInvestorInfoPermission { get; private set; }
        public DateTime TrackedSince { get; private set; }

        public void setDescription(String description)
        {
            Description = description;
        }

        private TrackedField(DbDataReader reader)
        {
            FieldId = (string)reader["sFieldId"];
            Description = (string)reader["Description"];
            RequiresAccountantPermision = ((int)reader["RequiresAccountantPermision"]) == 1;
            RequiresCloserPermission = ((int)reader["RequiresCloserPermission"] )== 1;
            RequiresLockDeskPermission = ((int)reader["RequiresLockDeskPermission"]) == 1;
            RequiresInvestorInfoPermission = ((int)reader["RequiresInvestorInfoPermission"]) == 1;
            TrackedSince = (DateTime)reader["TrackedSince"];
        }


        public static IDictionary<string, TrackedField> Retrieve()
        {
            lock (x_lock)
            {
                if (x_TrackedFields == null)
                {
                    IDictionary<string, TrackedField> fields = new Dictionary<string, TrackedField>(StringComparer.OrdinalIgnoreCase);
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "AUDIT_RETRIEVE_TRACKED_FIELDS"))
                    {
                        while (reader.Read())
                        {
                            TrackedField tf = new TrackedField(reader);
                            fields.Add(tf.FieldId, tf);
                        }
                    }
                    x_TrackedFields = fields;
                }
            }
            return x_TrackedFields;
        }
    }

}
