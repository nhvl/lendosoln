﻿namespace LendersOffice.ObjLib.Audit
{
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    /// <summary>
    /// Audit message for when we save the temporary closing cost archive onto the loan file.
    /// </summary>
    public class TemporaryClosingCostArchiveSavedAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemporaryClosingCostArchiveSavedAuditItem"/> class.
        /// </summary>
        /// <param name="type">The type of the archive.</param>
        /// <param name="auditMessage">The audit message.</param>
        private TemporaryClosingCostArchiveSavedAuditItem(ClosingCostArchive.E_ClosingCostArchiveType type, string auditMessage)
            : base(PrincipalFactory.CurrentPrincipal, auditMessage, null, MapArchiveTypeToCategory(type))
        {
            this.HasDetails = false;
        }

        /// <summary>
        /// Creates a new audit indicating that the temporary archive was saved as a new archive.
        /// </summary>
        /// <param name="type">The type of the archive.</param>
        /// <param name="packageGenerated">The document package that was generated to trigger the save.</param>
        /// <returns>The audit item.</returns>
        public static TemporaryClosingCostArchiveSavedAuditItem SavedAsNewArchive(
            ClosingCostArchive.E_ClosingCostArchiveType type,
            string packageGenerated)
        {
            var archiveType = GetArchiveDescription(type);
            var description = archiveType + " was archived because document package " + packageGenerated + " was generated.";

            return new TemporaryClosingCostArchiveSavedAuditItem(type, description);
        }

        /// <summary>
        /// Creates a new audit indicating that the temporary archive replaced a pending archive.
        /// </summary>
        /// <param name="type">The closing cost archive type.</param>
        /// <param name="packageGenerated">The document package that was generated to trigger the save.</param>
        /// <param name="dateOfReplacedArchive">The date of the pending archive.</param>
        /// <returns>The audit item.</returns>
        public static TemporaryClosingCostArchiveSavedAuditItem ReplacedPendingArchive(
            ClosingCostArchive.E_ClosingCostArchiveType type,
            string packageGenerated,
            string dateOfReplacedArchive)
        {
            if (type != ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Only loan estimate archives are expected to be in the pending status.");
            }

            var archiveType = GetArchiveDescription(type);
            var description = "The " + archiveType + " archive from " + dateOfReplacedArchive
                + " was updated because document package " + packageGenerated + " was generated.";

            return new TemporaryClosingCostArchiveSavedAuditItem(type, description);
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
        }

        /// <summary>
        /// Maps a closing cost archive type to an audit message category.
        /// </summary>
        /// <param name="type">The closing cost archive type.</param>
        /// <returns>The audit item category.</returns>
        private static E_AuditItemCategoryT MapArchiveTypeToCategory(ClosingCostArchive.E_ClosingCostArchiveType type)
        {
            switch (type)
            {
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015:
                    return E_AuditItemCategoryT.GFEArchived;
                case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate:
                    return E_AuditItemCategoryT.LoanEstimateArchived;
                case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure:
                    return E_AuditItemCategoryT.ClosingDisclosureArchived;
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2010:
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Gets a friendly description of the archive type.
        /// </summary>
        /// <param name="type">The closing cost archive type.</param>
        /// <returns>The friendly description.</returns>
        private static string GetArchiveDescription(ClosingCostArchive.E_ClosingCostArchiveType type)
        {
            switch (type)
            {
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015:
                    return "GFE";
                case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate:
                    return "Loan Estimate";
                case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure:
                    return "Closing Disclosure";
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2010:
                default:
                    throw new UnhandledEnumException(type);
            }
        }
    }
}