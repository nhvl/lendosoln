using System;
using System.Xml;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.Audit
{
	public class PmlLoanSummaryEmailAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_EMAIL = "email";
        private const string ATTR_PMLSUMMARY = "pml";
        private const string ATTR_FROM = "from"; 
        #endregion

        private string m_email;
        private string m_pmlSummary;
		private string m_loginName;
        private string m_from; 
		public PmlLoanSummaryEmailAuditItem(AbstractUserPrincipal principal, string email, string from, string pmlSummary) :
            base(principal, "Email PML Loan Summary", "PmlLoanSummaryEmail.xslt", DataAccess.E_AuditItemCategoryT.Email)
		{
            m_email = email;
            m_pmlSummary = pmlSummary;
			m_loginName = principal.LoginNm;
            m_from = from; 

		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_EMAIL, m_email);
            WriteSafeAttr(writer, ATTR_FROM, m_from);
			WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_pmlSummary) 
            {
                writer.WriteStartElement(ATTR_PMLSUMMARY);
                writer.WriteCData(m_pmlSummary);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
	}
}
