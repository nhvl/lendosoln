using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{

	public class FieldModifiedAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_FIELD_NAME = "name";
        private const string ATTR_OLD_VALUE = "old";
        private const string ATTR_NEW_VALUE = "new";
        #endregion

        private string m_fieldDescription;
        private string m_oldValue;
        private string m_newValue;
		private string m_loginName;
		public FieldModifiedAuditItem(AbstractUserPrincipal principal, string fieldDescription, string oldValue, string newValue) :
            base(principal, string.Format("{0} Modification", fieldDescription), "FieldModified.xslt")
		{
            m_oldValue = oldValue;
            m_newValue = newValue;
            m_fieldDescription = fieldDescription;
			m_loginName = principal.LoginNm;
		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_FIELD_NAME, m_fieldDescription);
            WriteSafeAttr(writer, ATTR_OLD_VALUE, m_oldValue);
            WriteSafeAttr(writer, ATTR_NEW_VALUE, m_newValue);
			WriteSafeAttr(writer, "login", m_loginName);
            writer.WriteEndElement(); // </data>

        }
	}
}
