/// Author: David Dao

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
	public class UpdateFromLoanProspectorAuditItem : AbstractAuditItem
	{
        private string m_lpLoanId = "";
        private string m_lpTransactionId = "";
        private string m_lpAusKey = "";
		private string m_loginName;
        private readonly IReadOnlyCollection<string> selectedImportOptions;

		public UpdateFromLoanProspectorAuditItem(AbstractUserPrincipal principal, string lpLoanId, string lpTransactionId, string lpAusKey, IReadOnlyCollection<string> selectedImportOptions)
            : base (principal, "Update From Loan Product Advisor", "UpdateFromLp.xslt", DataAccess.E_AuditItemCategoryT.Integration) 
		{
            m_lpLoanId = lpLoanId;
            m_lpTransactionId = lpTransactionId;
            m_lpAusKey = lpAusKey;
			m_loginName = principal.LoginNm;
            this.selectedImportOptions = selectedImportOptions ?? new string[0];
		}

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "LoanId", m_lpLoanId);
            WriteSafeAttr(writer, "TransId", m_lpTransactionId);
            WriteSafeAttr(writer, "AusKey", m_lpAusKey);
			WriteSafeAttr(writer, "login", m_loginName);
            foreach (var option in this.selectedImportOptions)
            {
                writer.WriteElementString("selectedImportOption", option);
            }

            writer.WriteEndElement();
        }

	}
}
