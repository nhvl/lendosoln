﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Provides an audit for assets being imported from VOA.
    /// </summary>
    public class AssetImportAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetImportAuditItem"/> class.
        /// </summary>
        /// <param name="user">
        /// The user saving the verbal authorization data.
        /// </param>
        /// <param name="assetUpdates">
        /// Updates made to the assets on the loan file.
        /// </param>
        public AssetImportAuditItem(AbstractUserPrincipal user, IEnumerable<AssetUpdate> assetUpdates)
            : base(user, "Assets Imported from VOA", "AssetImportAuditItem.xslt", E_AuditItemCategoryT.Integration)
        {
            IEnumerable<XElement> addedElements = assetUpdates.Where(update => update.UpdateT == UpdateType.New).Select(newAsset =>
                new XElement(
                    "asset",
                    new XElement("accNum", newAsset.AccountNumber),
                    new XElement("institution", newAsset.NewInstitution),
                    new XElement("balance", newAsset.NewBalance),
                    new XElement("type", newAsset.AssetType)));
            IEnumerable<XElement> modifiedElements = assetUpdates.Where(update => update.UpdateT == UpdateType.Modify).Select(updatedAsset =>
                new XElement(
                    "asset",
                    new XElement("accNum", updatedAsset.AccountNumber),
                    new XElement("institutionOld", updatedAsset.Institution),
                    new XElement("institutionNew", updatedAsset.NewInstitution),
                    new XElement("balanceOld", updatedAsset.Balance),
                    new XElement("balanceNew", updatedAsset.NewBalance),
                    new XElement("type", updatedAsset.AssetType)));
            IEnumerable<XElement> removedElements = assetUpdates.Where(update => update.UpdateT == UpdateType.Remove).Select(removedAsset =>
                new XElement(
                    "asset",
                    new XElement("accNum", removedAsset.AccountNumber),
                    new XElement("institution", removedAsset.Institution),
                    new XElement("balance", removedAsset.Balance),
                    new XElement("type", removedAsset.AssetType)));
            this.DataToSave = new XElement(
                "data",
                addedElements.Any() ? new XElement("added", addedElements) : null,
                modifiedElements.Any() ? new XElement("modified", modifiedElements) : null,
                removedElements.Any() ? new XElement("removed", removedElements) : null);
        }

        /// <summary>
        /// Types of updates.
        /// </summary>
        public enum UpdateType
        {
            /// <summary>
            /// A new asset record was added.
            /// </summary>
            New,

            /// <summary>
            /// An existing asset record was modified.
            /// </summary>
            Modify,

            /// <summary>
            /// An existing asset record was removed.
            /// </summary>
            Remove
        }

        /// <summary>
        /// Gets or sets a value indicating whether the "Close" button
        /// should be rendered for the audit item.
        /// </summary>
        public bool RenderCloseButton { get; set; } = true;

        /// <summary>
        /// Gets the data to save for the audit entry.
        /// </summary>
        private XElement DataToSave { get; }

        /// <summary>
        /// Generates detail XML for the audit item.
        /// </summary>
        /// <param name="writer">
        /// The writer to write XML.
        /// </param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            this.DataToSave.WriteTo(writer);
        }

        /// <summary>
        /// Keeps track of information relating to a change to an asset from import.
        /// </summary>
        public class AssetUpdate
        {
            /// <summary>
            /// Gets or sets the type of update made.
            /// </summary>
            public UpdateType UpdateT { get; set; }

            /// <summary>
            /// Gets or sets the account number of the asset updated.
            /// </summary>
            public string AccountNumber { get; set; }

            /// <summary>
            /// Gets or sets the institution name of the asset before the update.
            /// </summary>
            public string Institution { get; set; }

            /// <summary>
            /// Gets or sets the institution name of the asset after the update.
            /// </summary>
            public string NewInstitution { get; set; }

            /// <summary>
            /// Gets or sets the balance of the asset before the update.
            /// </summary>
            public string Balance { get; set; }

            /// <summary>
            /// Gets or sets the balance of the asset after the update.
            /// </summary>
            public string NewBalance { get; set; }

            /// <summary>
            /// Gets or sets the account type of the asset.
            /// </summary>
            public string AssetType { get; set; }
        }
    }
}
