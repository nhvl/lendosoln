﻿//-----------------------------------------------------------------------
// <summary>
//       Represents an audit for the reason that it was archived, 
//       along with inherited properties from AbstractAuditItem
// </summary>
// <copyright file="LoanEstimateArchivedAuditItem.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LendersOffice.ObjLib.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit for the reason that it was archived, 
    /// along with inherited properties from AbstractAuditItem.
    /// </summary>
    public sealed class LoanEstimateArchivedAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The XML attribute name for the archived reason type.
        /// </summary>
        private const string AttrReasonArchiveD = "GFEArchivedReasonT";        

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEstimateArchivedAuditItem" /> class.
        /// Represents an audit for the reason that it was archived, 
        /// along with inherited properties from AbstractAuditItem.
        /// </summary>
        /// <param name="gfeArchivedReasonT">The reason it was archived.</param>      
        public LoanEstimateArchivedAuditItem(E_GFEArchivedReasonT gfeArchivedReasonT)
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, DataAccess.E_AuditItemCategoryT.LoanEstimateArchived)
        {
            this.HasDetails = false;
            this.GFEArchivedReasonT = gfeArchivedReasonT;

            if (E_GFEArchivedReasonT.NotYetDetermined == gfeArchivedReasonT)
            {
                Tools.LogErrorWithCriticalTracking("Programming Error: Loan Estimate archived without a known reason.");
            }
            
            this.SetDescription("Loan Estimate was archived because " + Tools.Get_GFEArchivedReason_rep(this.GFEArchivedReasonT));
        }

        /// <summary>
        /// Gets or sets the audit's type of reason that it was archived.
        /// </summary>
        private E_GFEArchivedReasonT GFEArchivedReasonT { get; set; }

        /// <summary>
        /// Writes the reason to the writer inside a data element.
        /// </summary>
        /// <param name="writer">The XML writer to be written to.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, AttrReasonArchiveD, (int)this.GFEArchivedReasonT);
            writer.WriteEndElement();
        }
    }
}
