<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Loan Delete Audit Detail</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Loan Delete Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
   

        </table>
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>

  