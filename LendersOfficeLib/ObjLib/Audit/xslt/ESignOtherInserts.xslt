<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Other Inserts information received from document vendor.</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Other Inserts information received from document vendor.</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
        </table>
        
        <table width="100%" border="1" style="margin-left:10px; table-layout:fixed;">
          <tr>
            <td style="font-weight:bold;">Other Inserts</td>
          </tr>
          <xsl:for-each select="body/OtherInserts/OtherInsert">
            <tr valign="top">
              <td style="word-wrap: break-word"><xsl:value-of select="."/></td>
            </tr>
          </xsl:for-each>
        </table>
        
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>


  