<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <xsl:variable name="PageTitle">
      <xsl:value-of select="concat(@desc, ' Audit Detail')"/>
    </xsl:variable>
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
</xsl:text>
    <html>
      <head>
        <title>
          <xsl:value-of select="$PageTitle"/>
        </title>
        <style type="text/css"><xsl:text disable-output-escaping="yes">
      table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
      }
      div {
          padding: 10px 0px;
      }
    </xsl:text>
        </style>
      </head>
      <body>
        <h3>
          <xsl:value-of select="$PageTitle"/>
        </h3>

        <table border="0">
          <tr>
            <td>Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp"/>
            </td>
          </tr>
          <tr>
            <td>User Name:</td>
            <td>
              <xsl:value-of select="@uname"/>
            </td>
          </tr>
          <tr>
            <td>Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
        </table>

        <xsl:apply-templates select="body/data/*" mode="messageBody"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="import" mode="messageBody">
    <div>The following data was imported from Symitar:</div>
    <table>
      <tr>
        <th>Description</th>
        <th>Value before import</th>
        <th>Value after import</th>
      </tr>
      <xsl:apply-templates select="val" mode="importPage"/>
    </table>
  </xsl:template>
  <xsl:template match="val" mode="importPage">
    <tr>
      <td>
        <xsl:value-of select="@desc"/>
      </td>
      <td>
        <xsl:value-of select="@old"/>
      </td>
      <td>
        <xsl:value-of select="@new"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="export" mode="messageBody">
    <div>Loan data was exported to Symitar.</div>
  </xsl:template>
</xsl:stylesheet>