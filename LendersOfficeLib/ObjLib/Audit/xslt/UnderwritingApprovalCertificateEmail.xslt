﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="VirtualRoot"/>
	<xsl:param name="Timestamp"/>
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

	<xsl:template match="/">
		<xsl:apply-templates select="item"/>
	</xsl:template>

	<xsl:template match="item">
		<html>
			<head>
				<title>Underwriting Approval Certificate Email Audit Detail</title>
				<style>
					<xsl:text disable-output-escaping="yes">
        td {
          font-size: 11px;
	        line-height: 16px;    
          RIGHT: 0px;
          font-family: Arial, Helvetica, sans-serif;
        }      
        input {
	        font-family : Verdana, Arial, Helvetica, sans-serif;
	        font-size : 11px;
          font-weight: bold;
          color: Black;        
        }
        
      </xsl:text>
				</style>
			</head>

			<body scroll="yes" onload="Show();">
				<table width="100%"><tr><td align="center"><h3>Underwriting Approval Certificate Email Audit Detail</h3></td></tr></table>

				<table border="0">
					<tr><td style="font-weight:bold;color:black">Timestamp:</td><td style="color:black"><xsl:value-of select="$Timestamp" /></td></tr>
					<tr><td style="font-weight:bold;color:black">User Name:</td><td style="color:black"><xsl:value-of select="@uname" /></td></tr>
					<tr><td style="font-weight:bold;color:black">Login:</td><td style="color:black"><xsl:value-of select="body/data/@login"/></td></tr>
					<tr><td style="font-weight:bold;color:black">Event:</td><td style="color:black"><xsl:value-of select="@desc"/></td></tr>
					<tr><td style="font-weight:bold;color:black">Recipients:</td><td style="color:black"><xsl:value-of select="body/data/@email"/></td></tr>
					<tr><td style="font-weight:bold;color:black">From:</td><td style="color:black"><xsl:value-of select="body/data/@from"/></td></tr>
				</table>
				<hr />
                <iframe id="myFrame" frameborder="0"  vspace="0"  hspace="0"  marginwidth="0" marginheight="0" width="100%" scrolling="no" ></iframe>

                <div id="contents">
                    <xsl:comment >
                        <xsl:value-of select="body/data/approvalCert" disable-output-escaping="yes"/>
                    </xsl:comment>
                </div>
                
                <xsl:text disable-output-escaping="yes">
                <![CDATA[ 
                <script type="text/javascript">
               
                    function Show()
                    {
                    window.resizeTo(750,600)
                    var tDiv = document.getElementById("contents");
                    var FirstElement = tDiv.firstChild;
                    var v = FirstElement.nodeValue;
                    var tFrame = document.getElementById("myFrame");
                    var doc = tFrame.contentDocument;
                    if (doc == undefined || doc == null)
                    doc = tFrame.contentWindow.document;
                    doc.open();
                    doc.write(v);
                    doc.close();

                    tFrame.style.height = Math.max(doc.body.offsetHeight, doc.body.scrollHeight);
                    }

                    function pageY(elem) {
                    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
                    }
                </script>
                ]]> 
                </xsl:text>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>