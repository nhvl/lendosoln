<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <html>
      <xsl:apply-templates select="item" mode="Head"/>
      <xsl:apply-templates select="item" mode="Body"/>
    </html>
  </xsl:template>
  
  <xsl:template match="item" mode="Head">
    <head>
      <title>D1C Reference Number Change Audit Detail</title>
      <style>
        <xsl:text disable-output-escaping="yes"><![CDATA[
        h3 {
          text-align: center;
        }
        table.update-table {
          border-collapse: collapse;
        }
        .update-table th, .update-table td {
          padding: 4px;
          border: 1px solid black;
        }
        table th {
          text-align: left;
        }
      ]]></xsl:text>
      </style>
    </head>
  </xsl:template>

  <xsl:template match="item" mode="Body">
    <body>
      <h3>D1C Reference Number Change Audit Detail</h3>

      <table>
        <tr>
          <th>Timestamp:</th>
          <td>
            <xsl:value-of select="$Timestamp"/>
          </td>
        </tr>
        <tr>
          <th>User Name:</th>
          <td>
            <xsl:value-of select="@uname"/>
          </td>
        </tr>
        <tr>
          <th>Event:</th>
          <td>
            <xsl:value-of select="@desc"/>
          </td>
        </tr>
      </table>

      <xsl:apply-templates select="body/data/added" mode="CreateModificationData"/>
      <xsl:apply-templates select="body/data/modified" mode="CreateAssetModifiedData"/>
      <xsl:apply-templates select="body/data/removed" mode="CreateModificationData"/>
      <xsl:if test="not(body/data/added) and not(body/data/modified) and not(body/data/removed)">
        No assets changed.
      </xsl:if>
    </body>
  </xsl:template>
  
  <xsl:template match="*" mode="CreateModificationData">
    <xsl:param name="TypeName" select="local-name()"/>
    <div>
      <h4>The following assets were <xsl:value-of select="$TypeName"/></h4>
      <table class="update-table">
        <tr>
          <th>Account Number</th>
          <th>Institution</th>
          <th>Balance</th>
          <th>Asset Type</th>
        </tr>
        <xsl:apply-templates select="asset" mode="CreateAssetRow"/>
      </table>
    </div>
  </xsl:template>
  
  <xsl:template match="*" mode="CreateAssetModifiedData">
    <xsl:param name="TypeName" select="local-name()"/>
    <div>
      <h4>The following assets were modified</h4>
      <table class="update-table">
        <tr>
          <th>Account Number</th>
          <th>Former Institution Name</th>
          <th>Updated Institution Name</th>
          <th>Former Balance</th>
          <th>Updated Balance</th>
          <th>Asset Type</th>
        </tr>
        <xsl:apply-templates select="asset" mode="CreateAssetRowModified"/>
      </table>
    </div>
  </xsl:template>
  
  <xsl:template match="asset" mode="CreateAssetRow">
    <tr>
      <td>
        <xsl:value-of select="accNum"/>
      </td>
      <td>
        <xsl:value-of select="institution"/>
      </td>
      <td>
        <xsl:value-of select="balance"/>
      </td>
      <td>
        <xsl:value-of select="type"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="asset" mode="CreateAssetRowModified">
    <tr>
      <td>
        <xsl:value-of select="accNum"/>
      </td>
      <td>
        <xsl:value-of select="institutionOld"/>
      </td>
      <td>
        <xsl:value-of select="institutionNew"/>
      </td>
      <td>
        <xsl:value-of select="balanceOld"/>
      </td>
      <td>
        <xsl:value-of select="balanceNew"/>
      </td>
      <td>
        <xsl:value-of select="type"/>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>