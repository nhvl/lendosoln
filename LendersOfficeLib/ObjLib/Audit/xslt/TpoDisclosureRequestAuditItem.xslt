<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <html>
      <head>
        <title>Originator Portal Request for <xsl:value-of select="body/data/@Title"/> Audit Details</title>
        <style type="text/css">
          <xsl:text disable-output-escaping="yes">
            .bold { font-weight: bold; }
            .full-width { width: 100%; }
            .no-border { border: 0; }
          </xsl:text>
        </style>
      </head>
      <body>
        <table class="full-width">
          <tr>
            <td align="center">
              <h3>Originator Portal Request for <xsl:value-of select="body/data/@Title"/> Audit Details</h3>
            </td>
          </tr>
        </table>

        <table class="no-border">
          <tr>
            <td class="bold">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td class="bold">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td class="bold">Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <tr>
            <td class="bold">Notes:</td>
            <td>
              <xsl:value-of select="body/data/@Notes"/>
            </td>
          </tr>
        </table>

        <table class="full-width">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


