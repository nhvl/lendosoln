﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <html>
      <head>
        <title>Verbal Credit Report Authorization</title>
        <style type="text/css">
          <xsl:text disable-output-escaping="yes">
            .bold { font-weight: bold; }
          </xsl:text>
        </style>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>Verbal Credit Report Authorization Audit Details</h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td class="bold">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <xsl:if test="body/data/@LenderName">
          <tr>
            <td class="bold">Lender Name:</td>
            <td>
              <xsl:value-of select="body/data/@LenderName" />
            </td>
          </tr>
          </xsl:if>
          <tr>
            <td class="bold">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td class="bold">Event:</td>
            <td>
              Verbal authorization to pull credit for 
              <xsl:if test="body/data/@BorrowerName">
                <xsl:value-of select="body/data/@BorrowerName" />
              </xsl:if>
              <xsl:if test="body/data/@BorrowerName and body/data/@CoborrowerName">
                and 
              </xsl:if>
              <xsl:if test="body/data/@CoborrowerName">
                <xsl:value-of select="body/data/@CoborrowerName" />
              </xsl:if>
              was reported by <xsl:value-of select="@uname" />.
            </td>
          </tr>
          <tr>
            <td class="bold">Authorization Date:</td>
            <td>
              <xsl:value-of select="body/data/@AuthorizationDate" />
            </td>
          </tr>
          <xsl:if test="body/data/@BorrowerName">
            <tr>
              <td class="bold">Borrower Name:</td>
              <td>
                <xsl:value-of select="body/data/@BorrowerName" />
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="body/data/@CoborrowerName">
            <tr>
              <td class="bold">Co-Borrower Name:</td>
              <td>
                <xsl:value-of select="body/data/@CoborrowerName" />
              </td>
            </tr>
          </xsl:if>
        </table>

        <xsl:if test="body/data/@RenderCloseButton = 1">
        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
        </xsl:if>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


