<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Clear Rate Lock Request Audit Detail</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Clear Rate Lock Request Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
          <tr><td style="font-weight:bold;">Requested Rate:</td><td><xsl:value-of select="body/data/@rate"/></td></tr>
          <tr><td style="font-weight:bold;">Requested Fee:</td><td><xsl:value-of select="body/data/@fee"/></td></tr>
          <tr><td style="font-weight:bold;">Requested Margin:</td><td><xsl:value-of select="body/data/@margin"/></td></tr>
          <xsl:if test="body/data/@qualRate">
			<tr><td style="font-weight:bold;">Requested Qualify Rate:</td><td><xsl:value-of select="body/data/@qualRate"/></td></tr>
			<tr><td style="font-weight:bold;">Requested Teaser Rate:</td><td><xsl:value-of select="body/data/@teaseRate"/></td></tr>
			<tr><td style="font-weight:bold;">Requested Option ARM:</td><td><xsl:value-of select="body/data/@optionArm"/></td></tr>
			<tr><td style="font-weight:bold;">Requested Term:</td><td><xsl:value-of select="body/data/@term"/></td></tr>
			<tr><td style="font-weight:bold;">Requested Due:</td><td><xsl:value-of select="body/data/@due"/></td></tr>
			<tr><td style="font-weight:bold;">Requested Amortization:</td><td><xsl:value-of select="body/data/@amortMethod"/></td></tr>
          </xsl:if>

        </table>
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>

  