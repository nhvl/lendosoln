﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <html>
      <head>
        <title>Submit to Fannie Mae EarlyCheck Audit</title>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>Submit to Fannie Mae EarlyCheck Audit</h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td style="font-weight:bold;">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>

        </table>
        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>

