﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title><xsl:value-of select="@desc"/> Audit Detail</title></head>
      <body scroll="yes" onload="Show();" >
        <table width="100%"><tr><td align="center"><h3>FHA Connection <xsl:value-of select="@desc"  /> Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td>FHA Connection <xsl:value-of select="@desc"/></td></tr>
          <tr><td style="font-weight:bold;">FHA Case Number:</td><td><xsl:value-of select="body/data/@caseNum"/></td></tr>
          <tr><td style="font-weight:bold;">XML Response:</td><td><a href="#" onclick="Modal.ShowPopup('response', null, 1, 1);">View XML</a>
            <div id="response" style="word-wrap: break-word; word-break: break-all; line-height: 1em; height: 550x; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;">
              <table>
                <tr><td><div style="overflow: scroll; height: 470px"><xsl:value-of select="body/data/xmlResponse" /></div></td></tr>
                <tr><td align="center" ><input type="button" id="closeBtn" value=" Close " onclick="Modal.Hide();" /></td></tr>
              </table>
            </div>
          </td></tr>

        </table>
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>

        <iframe id="myFrame" frameborder="0"  vspace="0"  hspace="0"  marginwidth="0"
marginheight="0" width="100%" scrolling="no" ></iframe>
        
        <div id="contents">
          <xsl:comment >
            <xsl:value-of select="body/data/htmlResult" disable-output-escaping="yes"/>
          </xsl:comment>
        </div>
        <xsl:text disable-output-escaping="yes">
                <![CDATA[ 
                <script type="text/javascript">
               
                    function Show()
                    {
                    window.resizeTo(750,600)
                    var tDiv = document.getElementById("contents");
                    var FirstElement = tDiv.firstChild;
                    var v = FirstElement.nodeValue;
                    var tFrame = document.getElementById("myFrame");
                    var doc = tFrame.contentDocument;
                    if (doc == undefined || doc == null)
                    doc = tFrame.contentWindow.document;
                    doc.open();
                    doc.write(v);
                    doc.close();

                    tFrame.style.height = Math.max(doc.body.offsetHeight, doc.body.scrollHeight);
                    }

                    function pageY(elem) {
                    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
                    }
  
var Modal =
{
	activeModal : '',  
	selectMask : 'selectMask', 
	Show : function(id) 
	{	
		this.Hide();
		var obj = document.getElementById(id); 
		obj.style.display = 'block';
		this.activeModal = id;
		this.ShowFrameOverObject(obj);					
	},
	GetSelectionFrame : function () 
	{
		return document.getElementById(this.selectMask);
	},
	ShowFrameOverObject : function( object )
	{
		var frame = this.GetSelectionFrame(); 
		if ( !frame ) return; 
		
		frame.style.width   = object.offsetWidth;  
		frame.style.height  = object.offsetHeight; 
		frame.style.top     = object.offsetTop;
		frame.style.left    = object.offsetLeft;
		frame.style.zIndex = object.style.zIndex -1 ; 
		frame.style.display = 'block';
	},
	HideFrame : function() 
	{
		var frame = this.GetSelectionFrame(); 
		if ( !frame ) return; 
		frame.style.display = 'none';
	},
	Hide : function() 
	{
		if( this.activeModal == '' ) return;
		document.getElementById(this.activeModal).style.display = 'none'; 
		this.activeModal = ''; 
		this.HideFrame();
	}, 
	ShowPopup : function ( id, focusid, x, y ) 
	{
		this.Hide();
		this.activeModal = id;
		var xpos = x || window.event.clientX;
		var ypos = y || window.event.clientY;
		var elem = document.getElementById(id);
		
		elem.style.display = "block";
		var top = ypos - elem.offsetHeight - 10 + document.documentElement.scrollTop;  
		var left = xpos - ( elem.offsetWidth /2 ) + document.documentElement.scrollLeft;   
		if (  document.documentElement.scrollTop > top ) top = document.documentElement.scrollTop + ypos + 10 ;
		if ( left -10  < 0 ) left = xpos + 10 ; 
		elem.style.top = top + 'px';
		elem.style.left = left + 'px';
		this.ShowFrameOverObject( elem );
		if ( focusid != null && focusid != '' ) document.getElementById(focusid).focus();
	}

};


                </script>
                ]]> 
                </xsl:text>
        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>