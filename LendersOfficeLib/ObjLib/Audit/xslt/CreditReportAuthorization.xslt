﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
    <xsl:text disable-output-escaping="yes">&lt;!--</xsl:text>
    <xsl:copy-of select="*"/>
    <xsl:text disable-output-escaping="yes">--&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="item">
    <xsl:variable name="PageTitle">
      <xsl:value-of select="'Credit Report Authorization Detail'"/>
    </xsl:variable>
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
</xsl:text>
    <html>
      <head>
        <title>
          <xsl:value-of select="$PageTitle"/>
        </title>
        <style type="text/css">
          <xsl:text disable-output-escaping="yes">
      .title {
          text-align: center;
      }
      table {
          border: 0;
      }
      tr td:first-child {
          font-weight:bold;
      }
    </xsl:text>
        </style>
      </head>
      <body>
        <h3 class="title">
          <xsl:value-of select="$PageTitle"/>
        </h3>

        <table>
          <tr>
            <td>Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp"/>
            </td>
          </tr>
          <tr>
            <td>User Name:</td>
            <td>
              <xsl:value-of select="@uname"/>
            </td>
          </tr>
          <tr>
            <td>Login:</td>
            <td>
              <xsl:value-of select="body/data/@login"/>
            </td>
          </tr>
          <tr>
            <td>Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <xsl:apply-templates select="body/data/borr"/>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="borr">
    <tr>
      <td>
        <xsl:if test="@isCo = 1">Co-</xsl:if>Borrower Name:
      </td>
      <td>
        <xsl:value-of select="@name"/>
      </td>
    </tr>
    <tr>
      <td>
        <xsl:if test="@isCo = 1">Co-</xsl:if>Borrower SSN:
      </td>
      <td>
        <xsl:value-of select="@ssn"/>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
