<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head>
        <title>Credit Report Audit Detail</title>
        <script type="text/javascript">
          function printCreditReport()
          {
              if (document.queryCommandSupported('print')) {
                  frmCredit.document.execCommand('print', false, null);
              }
              else {
                  frmCredit.focus();
                  window.print();
              }
          }
        </script>
      </head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Credit Report Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
        </table>
        <table width="100%">
          <tr><td align="middle"><input type="button" value="Print Credit Report" onclick="printCreditReport();" />
          <input type="button" value="Close" onclick="self.close();"/></td></tr>
        </table> 
        <iframe id="frmCredit" width="100%" border="1" height="70%">
          <xsl:attribute name="src">
          <xsl:value-of select="$VirtualRoot"/>/newlos/Services/ViewCredit.aspx?dbfilekey=<xsl:value-of select="body/data/@filedbkey"/>
          </xsl:attribute>
        </iframe>
        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>

  