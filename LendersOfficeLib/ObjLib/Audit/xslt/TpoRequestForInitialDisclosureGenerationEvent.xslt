<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <html>
      <head>
        <title>Originator Portal Request for Initial Disclosure Generation Audit Details</title>
        <style type="text/css">
          <xsl:text disable-output-escaping="yes">
            .bold { font-weight: bold; }
            img.pass { width: 24px; }
            table.border { width:100%; border: 1px solid #000; border-spacing: 0; border-collapse: collapse; }
            table.border td { border-bottom: 1px solid #000; border-right: 1px solid #000; }
            td.supplementary-title { padding-right: 25px; white-space: nowrap; }
          </xsl:text>
        </style>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>Originator Portal Request for Initial Disclosure Generation Audit Details</h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td class="bold">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td class="bold">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td class="bold">Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <tr>
            <td class="bold">Notes:</td>
            <td>
              <xsl:value-of select="body/data/@Notes"/>
            </td>
          </tr>
          <tr>
            <td class="bold">Submitted From:</td>
            <td>
              <xsl:value-of select="body/data/@Source"/>
            </td>
          </tr>
          <xsl:if test="body/data/@CreationReason">
            <tr>
              <td class="bold">Block Reason:</td>
              <td>
                <xsl:value-of select="body/data/@CreationReason"/>
              </td>
            </tr>
          </xsl:if>

          <xsl:choose>
            <xsl:when test="body/data/requirements">
              <tr>
                <td class="bold supplementary-title">Requirements Checklist:</td>
                <td>
                  <table class="border">
                    <xsl:for-each select="body/data/requirements/item">
                      <tr>
                        <td>
                          <img>
                            <xsl:choose>
                              <xsl:when test="@Result = 'True'">
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/images/success.png
                                </xsl:attribute>
                                <xsl:attribute name="class">
                                  pass
                                </xsl:attribute>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/images/fail.png
                                </xsl:attribute>
                              </xsl:otherwise>
                            </xsl:choose>
                          </img>
                        </td>
                        <td>
                          <xsl:value-of select="@Description"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
            </xsl:when>
            <xsl:when test="body/data/audit">
              <tr>
                <td class="bold supplementary-title">Document Vendor Audit Findings:</td>
                <td>
                  <table class="border">
                    <xsl:for-each select="body/data/audit/item">
                      <tr>
                        <td>
                          <img>
                            <xsl:attribute name="src">
                              <xsl:choose>
                                <xsl:when test="@Severity = 'Fatal'">
                                  <xsl:value-of select="$VirtualRoot" />/images/fail.png
                                </xsl:when>
                                <xsl:when test="@Severity = 'Critical' or @Severity = 'Warning'">
                                  <xsl:value-of select="$VirtualRoot" />/images/warn.png
                                </xsl:when>
                              </xsl:choose>
                            </xsl:attribute>
                          </img>
                          <xsl:value-of select="@Severity"/>
                        </td>
                        <td>
                          <xsl:value-of select="@Message"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </table>

        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


