<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Loan Status Change Audit Detail</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Loan Status Change Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
          <tr><td style="font-weight:bold;">Status Change:</td><td><xsl:value-of select="body/data/@old"/> => <xsl:value-of select="body/data/@new"/></td></tr>
        </table>

        <xsl:if test="body/data/cleared_statuses" >
          Data cleared from file:
          <table width="100%" border="1" style="margin-left:10px; table-layout:fixed;">
            <tr>
              <td style="font-weight:bold; width:100px;">Status</td>
              <td style="font-weight:bold; width:100px;">Status Date</td>
              <td style="font-weight:bold;">Status Comment</td>
            </tr>
            <xsl:for-each select="body/data/cleared_statuses/status">
              <tr valign="top">
                <td style="word-wrap: break-word">
                  <xsl:value-of select="@name"/>
                </td>
                <td>
                  <xsl:value-of select="@date"/>
                </td>
                <td style="word-wrap: break-word">
                  <xsl:value-of select="@comment"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </xsl:if>
        
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>