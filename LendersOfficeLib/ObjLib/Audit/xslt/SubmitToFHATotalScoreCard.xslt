﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="VirtualRoot"/>
    <xsl:param name="Timestamp"/>
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:apply-templates select="item"/>
    </xsl:template>

    <xsl:template match="item">
        <html>
            <head>
                <title>FHA TOTAL Scorecard Audit Detail</title>
                <style type="text/css">
                    td { vertical-align: top; padding-bottom: 10px;}
                </style>
            </head>
            <body scroll="yes" onload="Show();">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <h3>Submit to FHA TOTAL Scorecard Audit Detail</h3>
                        </td>
                    </tr>
                </table>

                <table border="0" >
                    <tr>
                        <td style="font-weight:bold;width:160px">Timestamp:</td>
                        <td>
                            <xsl:value-of select="$Timestamp" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">User Name:</td>
                        <td>
                            <xsl:value-of select="@uname" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">Login:</td>
                        <td>
                            <xsl:value-of select="body/data/@login"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">Event:</td>
                        <td>
                            <xsl:value-of select="@desc"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">FHA Case Number:</td>
                        <td>
                            <xsl:value-of select="body/data/@fhaCaseNumber"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">Upload 1003?</td>
                        <td>
                            <xsl:value-of select="body/data/@Sent1003"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;width:160px">Upload FHA Transmittal Summary?</td>
                        <td>
                            <xsl:value-of select="body/data/@SentFHATransmittal"/>
                        </td>
                    </tr>

                </table>
                <table width="100%" style="padding-bottom:10px">
                    <tr>
                        <td align="center">
                            <input type="button" value="Close" onclick="self.close();"></input>
                        </td>
                    </tr>
                </table>
          

                <iframe id="myFrame" frameborder="0"  vspace="0"  hspace="0"  marginwidth="0"
marginheight="0" width="100%" scrolling="no" ></iframe>

              <div id="contents">
                <xsl:comment >
                  <xsl:value-of select="body/data/totalCertificate" disable-output-escaping="yes"/>
                </xsl:comment>
              </div>
              <xsl:text disable-output-escaping="yes">
                <![CDATA[ 
                <script type="text/javascript">
               
                    function Show()
                    {
                    window.resizeTo(750,600)
                    var tDiv = document.getElementById("contents");
                    var FirstElement = tDiv.firstChild;
                    var v = FirstElement.nodeValue;
                    var tFrame = document.getElementById("myFrame");
                    var doc = tFrame.contentDocument;
                    if (doc == undefined || doc == null)
                    doc = tFrame.contentWindow.document;
                    doc.open();
                    doc.write(v);
                    doc.close();

                    tFrame.style.height = Math.max(doc.body.offsetHeight, doc.body.scrollHeight);
                    }

                    function pageY(elem) {
                    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
                    }
                </script>
                ]]> 
                </xsl:text>
            
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>


