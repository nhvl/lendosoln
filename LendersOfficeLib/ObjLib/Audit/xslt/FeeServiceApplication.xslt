<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Fee Service Application Audit Details</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Fee Service Application Audit Details</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
          
          <tr>
            <td style="font-weight:bold;">File Used:</td>
            <td>
              <xsl:choose>
                <xsl:when test="body/data/@usedTestFile = 1">
                  Test
                </xsl:when>
                <xsl:otherwise>
                  Production
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>

          <tr>
            <td style="font-weight:bold;">Has Prior Submission:</td>
            <td>
              <xsl:choose>
                <xsl:when test="body/data/@priorSubmission = 1">
                  Yes
                </xsl:when>
                <xsl:otherwise>
                  No
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>

          <tr>
            <td style="font-weight:bold;">Update Mode:</td>
            <td>
              <xsl:choose>
                <xsl:when test="body/data/@automationUpdateT = 0">
                  Update the fees whose conditions no longer apply
                </xsl:when>
                <xsl:when test="body/data/@automationUpdateT = 1">
                  Preserve fees on file
                </xsl:when>
                <xsl:otherwise>
                  update all closing costs
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>

        <xsl:choose>
          <xsl:when test="body/data/rules" >
            <table width="100%" border="1" style="margin-left:10px; table-layout:fixed;">
              <tr>
                <td style="font-weight:bold; width:100px;">Value</td>
                <td style="font-weight:bold; width:75px;">Hud Line</td>
                <td style="font-weight:bold;">Field ID</td>
                <td style="font-weight:bold;">Condition</td>
                <td style="font-weight:bold; width:275px;">Fee ID</td>
              </tr>
              <xsl:for-each select="body/data/rules/rule">
                <tr valign="top">
                  <td style="word-wrap: break-word"><xsl:value-of select="@value"/></td>
                  <td><xsl:value-of select="@hudLine"/></td>
                  <td style="word-wrap: normal"><xsl:value-of select="@fieldId"/></td>
                  <td style="word-wrap: normal"><xsl:value-of select="@conditions"/></td>

                  <td>
                    <xsl:choose>
                      <xsl:when test="@feeId">
                        <xsl:value-of select="@feeId"/>
                      </xsl:when>
                      <xsl:otherwise>
                        N/A
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </xsl:when>
          <xsl:otherwise>
            <table width="100%"><tr><td align="center">No Rules Applied</td></tr></table>
          </xsl:otherwise>
        </xsl:choose>
        
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>


  