<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <html>
      <xsl:apply-templates select="item" mode="Head"/>
      <xsl:apply-templates select="item" mode="Body"/>
    </html>
  </xsl:template>
  
  <xsl:template match="item" mode="Head">
    <head>
      <title>D1C Reference Number Change Audit Detail</title>
      <style>
        <xsl:text disable-output-escaping="yes"><![CDATA[
        h3 {
          text-align: center;
        }
      ]]></xsl:text>
      </style>
    </head>
  </xsl:template>

  <xsl:template match="item" mode="Body">
    <body>
      <h3>D1C Reference Number Change Audit Detail</h3>

      <table>
        <tr>
          <th>Timestamp:</th>
          <td>
            <xsl:value-of select="$Timestamp"/>
          </td>
        </tr>
        <tr>
          <th>User Name:</th>
          <td>
            <xsl:value-of select="@uname"/>
          </td>
        </tr>
        <tr>
          <th>Event:</th>
          <td>
            <xsl:value-of select="@desc"/>
          </td>
        </tr>
      </table>

      <xsl:apply-templates select="body/data/added" mode="CreateModificationData"/>
      <xsl:apply-templates select="body/data/removed" mode="CreateModificationData"/>
    </body>
  </xsl:template>
  
  <xsl:template match="*" mode="CreateModificationData">
    <xsl:param name="TypeName" select="local-name()"/>
    <div>
      <h4>The following D1C reference numbers were <xsl:value-of select="$TypeName"/></h4>
      <table>
        <tr>
          <th>3rd Party Data Provider Name</th>
          <th>Reference Number</th>
        </tr>
        <xsl:apply-templates select="r" mode="CreateReferenceNumberRow"/>
      </table>
    </div>
  </xsl:template>
  
  <xsl:template match="r" mode="CreateReferenceNumberRow">
    <tr>
      <td>
        <xsl:value-of select="provider"/>
      </td>
      <td>
        <xsl:value-of select="number"/>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>