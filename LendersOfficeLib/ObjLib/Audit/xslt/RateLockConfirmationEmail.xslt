﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="VirtualRoot"/>
	<xsl:param name="Timestamp"/>
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

	<xsl:template match="/">
		<xsl:apply-templates select="item"/>
	</xsl:template>

	<xsl:template match="item">
		<html>
			<head>
				<title>Rate Lock Confirmation Email Audit Detail</title>
				<style>
					<xsl:text disable-output-escaping="yes">
        td {
          font-size: 11px;
	        line-height: 16px;    
          RIGHT: 0px;
          font-family: Arial, Helvetica, sans-serif;
        }      
        input {
	        font-family : Verdana, Arial, Helvetica, sans-serif;
	        font-size : 11px;
          font-weight: bold;
          color: Black;        
        }
        
      </xsl:text>
				</style>
			</head>

			<body scroll="yes">
				<table width="100%"><tr><td align="center"><h3>Rate Lock Confirmation Email Audit Detail</h3></td></tr></table>

				<table border="0">
					<tr><td style="font-weight:bold;color:black">Timestamp:</td><td style="color:black"><xsl:value-of select="$Timestamp" /></td></tr>
					<tr><td style="font-weight:bold;color:black">User Name:</td><td style="color:black"><xsl:value-of select="@uname" /></td></tr>
					<tr><td style="font-weight:bold;color:black">Login:</td><td style="color:black"><xsl:value-of select="body/data/@login"/></td></tr>
					<tr><td style="font-weight:bold;color:black">Event:</td><td style="color:black"><xsl:value-of select="@desc"/></td></tr>
					<tr><td style="font-weight:bold;color:black">Recipients:</td><td style="color:black"><xsl:value-of select="body/data/@email"/></td></tr>
					<tr><td style="font-weight:bold;color:black">From:</td><td style="color:black"><xsl:value-of select="body/data/@from"/></td></tr>
				</table>
				<hr />
				<div style="padding:5px;border:groove 1px black">
					<xsl:value-of select="body/data/ratelock" disable-output-escaping="yes"/>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>