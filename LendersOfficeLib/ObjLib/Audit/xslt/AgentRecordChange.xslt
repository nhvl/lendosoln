﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
        <title>Agent Contact Record Change Detail</title>
        <style>
        .ContactName {
          white-space: pre;
        }
        </style>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>Agent Contact Record Change Detail</h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td style="font-weight:bold;">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <xsl:if test="body/data/@includelogin = '1'">
            <tr>
              <td style="font-weight:bold;">Login:</td>
              <td>
                <xsl:value-of select="body/data/@login"/>
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td style="font-weight:bold;">Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <tr>
            <td valign="top" style="font-weight:bold;">
              <xsl:value-of select="body/data/@changesheader" />
            </td>
            <td>
              <table width="100%">
                <xsl:for-each select="body/data/change">
                  <tr>
                    <td>
                      <xsl:value-of select="@field" />:
                    </td>
                    <xsl:if test="../@changetype != '0'">
                      <td>
                        <span class="ContactName"><xsl:value-of select="@old" /></span>
                      </td>
                    </xsl:if>
                    <xsl:if test="../@changetype = '1'">
                      <td>=></td>
                    </xsl:if>
                    <xsl:if test="../@changetype != '2'">
                      <td>
                        <span class="ContactName"><xsl:value-of select="@new" /></span>
                      </td>
                    </xsl:if>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>