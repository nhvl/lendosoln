﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
        <title>Disclosure Metadata Update Detail</title>
        <style>
          .FieldName {
            width: 250px:
          }
        </style>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>Disclosure Metadata Update Detail</h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td style="font-weight:bold;">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">Transaction ID:</td>
            <td>
              <xsl:value-of select="body/data/@transactionId"/>
            </td>
          </tr>
          <xsl:if test="body/data/change">
            <tr>
              <td valign="top" style="font-weight:bold;" colspan="2">
                Updated values for disclosure:
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <table width="100%">
                  <xsl:for-each select="body/data/change">
                    <tr>
                      <td style="width:250px;">
                        <xsl:value-of select="@field" />:
                      </td>
                      <td>
                          <xsl:value-of select="@old" />
                      </td>
                      <td>=></td>
                      <td>
                          <xsl:value-of select="@new" />
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </xsl:if>
          <xsl:for-each select="body/data/borrower">
            <tr>
              <td valign="top" style="font-weight:bold;">
                Updated values for borrower <xsl:value-of select="@name" />:
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%">
                  <xsl:for-each select="change">
                    <tr>
                      <td style="width:250px;">
                        <xsl:value-of select="@field" />:
                      </td>
                      <td>
                          <xsl:value-of select="@old" />
                      </td>
                      <td>=></td>
                      <td>
                          <xsl:value-of select="@new" />
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>