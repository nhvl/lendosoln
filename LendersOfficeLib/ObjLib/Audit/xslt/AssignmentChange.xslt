<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head><title>Assignment Change Audit Detail</title></head>
      <body>
        <table width="100%"><tr><td align="center"><h3>Assignment Change Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;">Timestamp:</td><td><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;">User Name:</td><td><xsl:value-of select="@uname" /></td></tr>
          <tr><td style="font-weight:bold;">Login:</td><td><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;">Event:</td><td><xsl:value-of select="@desc"/></td></tr>
          <tr><td valign="top" style="font-weight:bold;">Change:</td>
            <td>
            <table width="100%">
            <xsl:for-each select="body/data/change">
            <tr>
              <td><xsl:value-of select="@role" />:</td>
              <td><xsl:value-of select="@old" /></td>
              <td>=></td>
              <td><xsl:value-of select="@new" /></td>
            </tr>
            </xsl:for-each>
            </table>
            </td></tr>
        </table>
        <table width="100%"><tr><td align="center"><input type="button" value="Close" onclick="self.close();"></input></td></tr></table>        
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>

  