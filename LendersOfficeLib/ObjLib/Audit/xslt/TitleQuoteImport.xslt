﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <xsl:variable name="Title">
      <xsl:choose>
        <xsl:when test="body/@Title">
          <xsl:value-of select="body/@Title"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'First American Title Quote Details'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <title>
          <xsl:value-of select="$Title"/>
        </title>
        <style type="text/css">
          <xsl:text disable-output-escaping="yes">
      * {
        font-family: Helvetica, Arial, sans-serif;
      }
      table.AuditHeaderInformation {
        text-align: left;
        border-collapse: collapse;
      }
      h1.AuditBodyHeader {
        text-align: center;
      }
      .AuditHeaderInformation td {
        padding: .3em;
      }
          </xsl:text>

          <xsl:if test="not(body/htmlContent)">
            <xsl:text disable-output-escaping="yes">

      h2 {
        text-decoration: underline;
      }
      .QuoteBreakdown {
        border-collapse: collapse;
        font-size: 11px;
        border: 1px solid;
      }
      .QuoteBreakdown .LqbFee {
        background-color: rgb(8, 83, 147);
        color: white;
        font-weight: bold;
      }
      .QuoteBreakdown td, .QuoteBreakdown th {
        padding: .3em;
        border-left: 1px solid black;
        border-right: 1px solid black;/*IE8 quirk*/
        min-width: 8em;
        max-width: 25em;
      }
      .QuoteBreakdown th {
        text-align: left;
        background-color: lightgray;
      }
      .QuoteBreakdown .LqbFee td {
        padding-top: 1em;
        padding-bottom: 1em;
      }
      .QuoteBreakdown tr:not(.LqbFee) td:nth-child(1) {
        padding-left: 2em;
      }
      .QuoteBreakdown td:nth-child(n+2) {
        text-align: right;
      }
            </xsl:text>
          </xsl:if>
        </style>
      </head>

      <body>
        <h1 class="AuditBodyHeader">
          <xsl:value-of select="$Title"/>
        </h1>

        <table class="AuditHeaderInformation">
          <tr>
            <td>Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td>User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td>Login:</td>
            <td>
              <xsl:value-of select="body/@login"/>
            </td>
          </tr>
          <tr>
            <td>Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
        </table>

        <xsl:choose>
          <xsl:when test="body/htmlContent">
            <xsl:value-of select="body/htmlContent" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
        <h2>Title Quote Breakdown</h2>
        <table class="QuoteBreakdown">
          <tr>
            <th>Closing Cost</th>
            <th>Borrower Amount</th>
            <th>Seller Amount</th>
            <th>Other Amount</th>
          </tr>
          <xsl:for-each select="body/fee">
            <tr class="LqbFee">
              <td>
                <xsl:value-of select="@desc"/>
              </td>
              <td>
                <xsl:value-of select="@borrAmt"/>
              </td>
              <td>
                <xsl:value-of select="@sellAmt"/>
              </td>
              <td>
                <xsl:value-of select="@otherAmt"/>
              </td>
            </tr>
            <xsl:for-each select="cost">
              <tr>
                <td>
                  <xsl:value-of select="@desc"/>
                </td>
                <td>
                  <xsl:value-of select="@borrAmt"/>
                </td>
                <td>
                  <xsl:value-of select="@sellAmt"/>
                </td>
                <td>
                  <xsl:value-of select="@otherAmt"/>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:for-each>
        </table>
          </xsl:otherwise>
        </xsl:choose>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
