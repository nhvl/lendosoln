﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>

  <xsl:template match="item">
    <html>
      <head>
        <title>
          <xsl:value-of select="@desc"/> Audit Detail
        </title>
      </head>
      <body>
        <table width="100%">
          <tr>
            <td align="center">
              <h3>
                <xsl:value-of select="@desc"/> Audit Detail
              </h3>
            </td>
          </tr>
        </table>

        <table border="0">
          <tr>
            <td style="font-weight:bold;">Timestamp:</td>
            <td>
              <xsl:value-of select="$Timestamp" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">User Name:</td>
            <td>
              <xsl:value-of select="@uname" />
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">Login:</td>
            <td>
              <xsl:value-of select="body/data/@login"/>
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">Event:</td>
            <td>
              <xsl:value-of select="@desc"/>
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold;">Fields</td>
            <td>
              <ul style="list-style-type: none; padding: 0; margin: 0;">
                <xsl:for-each select="body/data/field">
                  <li>
                    <xsl:value-of select="@name"></xsl:value-of>: <xsl:value-of select="@value"></xsl:value-of>
                  </li>
                </xsl:for-each>
              </ul>
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td align="center">
              <input type="button" value="Close" onclick="self.close();"></input>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


