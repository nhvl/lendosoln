<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="VirtualRoot"/>
  <xsl:param name="Timestamp"/>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=10" />
        <title>Loan Submission Audit Detail</title>
      </head>
      <link type="text/css" rel="stylesheet">
        <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot"/>/embeddedpml/css/style.css</xsl:attribute>
      </link>
      <xsl:if test="body/data/@IsPml3Results = 'True'">
        <link type="text/css" rel="stylesheet">
          <xsl:attribute name="href">
            <xsl:value-of select="$VirtualRoot"/>/embeddedpml/css/PricingResults.min.css
          </xsl:attribute>
        </link>
        <script type="text/javascript" rel="stylesheet">
          <xsl:attribute name="src">
            <xsl:value-of select="$VirtualRoot"/>/embeddedpml/inc/angular-1.4.8.min.js
          </xsl:attribute>
        </script>
        <script type="text/javascript" rel="stylesheet">
          <xsl:attribute name="src">
            <xsl:value-of select="$VirtualRoot"/>/embeddedpml/inc/PricingResults.v3.js
          </xsl:attribute>
        </script>
        <script type="text/javascript" rel="stylesheet">
          <xsl:attribute name="src">
            <xsl:value-of select="$VirtualRoot"/>/embeddedpml/inc/Pml_Submit.js
          </xsl:attribute>
        </script>
      </xsl:if>
     
      <style type="text/css">
      <xsl:text disable-output-escaping="yes">
        td {
          font-size: 11px;
	        line-height: 16px;    
          RIGHT: 0px;
          font-family: Arial, Helvetica, sans-serif;
        }      
        input {
	        font-family : Verdana, Arial, Helvetica, sans-serif;
	        font-size : 11px;
          font-weight: bold;
          color: Black;        
        }
        
        .winner { 
            font-weight: bolder !important;
        }
        
        .GridItem {
            color: black;
            font: 11;
            background-color: white;
        }
        .GridAlternatingItem { 
            color: black;
            font: 11;
            background-color: rgb(204, 204, 204);
        }

      #PricingResultsContainer1, #PricingResultsContainer2 {
        font-size: 11px;
        }
      </xsl:text>
      </style>
      <script type="text/javascript">
        <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot"/>/inc/jquery-1.7.1.min.js</xsl:attribute>
      </script>
      <script type="text/javascript" language="javascript">
        <xsl:text disable-output-escaping="yes">
        var $j = jQuery.noConflict();

        function $() {
          var elements = new Array();

          for (var i = 0; i &lt; arguments.length; i++) {
            var element = arguments[i];
            if (typeof element == 'string')
              element = document.getElementById(element);

            if (arguments.length == 1)
              return element;

            elements.push(element);
          }

          return elements;
        }
        var list = new Array("FirstCert", "SecondCert", "FirstResult", "SecondResult", "ParData");
        function _init() {
          if (typeof(f_renderRateMergeTable) != 'undefined') {
            f_renderRateMergeTable(0);
            f_renderRateMergeTable(1);
            
          }
        }
        function f_display(id) {
          if(typeof IsPricing2nd != 'undefined') {
            IsPricing2nd = id == 'SecondResult';
          }
          for (var i = 0; i &lt; list.length; i++) {
            var s = list[i];
            var o = document.getElementById(s);
            if (o == null) continue;
            o.style.display = s == id ? "" : "none";
          }
        }
        function f_L(id, cb) {

          document.getElementById("M_" + id).style.display = 'none';
          document.getElementById("L_" + id).style.display = '';
          cb.checked = true;
        }
        function f_M(id, cb, index) {
        
          var oTBodyLess = document.getElementById("L_" + id);
        
          var oTBody = document.getElementById("M_" + id);
          oTBody.className = oTBodyLess.childNodes[0].className;
          
            if (id.match(/_e0|_i0/)) {
              if (typeof(f_buildHiddenRates0) != 'undefined') {
                f_buildHiddenRates0(oTBody, gData0[index], id);
              }
            } else {
              if (typeof(f_buildHiddenRates1) != 'undefined') {
                f_buildHiddenRates1(oTBody, gData1[index], id);
              }
            }
          oTBody.style.display = '';
          oTBodyLess.style.display = 'none';        

          cb.checked = false;
        }  
        function f_notSupport() {
            alert('Not supported in audit history.');
            return false;
        }
        
        function f00() { return f_notSupport(); }
        function printLoanProduct(productid)  { return f_notSupport(); }
        function f01() { return f_notSupport(); }
        function submitRateOption() { return f_notSupport(); }
        function f_submitManual2nd() { return f_notSupport(); }
        function submitSkip2nd() { return f_notSupport(); }
        function f_rerun1stLoan() { return f_notSupport(); }
        function f_openHelp() { return f_notSupport(); }
        function f_submitManually() { return f_notSupport(); }
        function f_displayMatrix() { return f_notSupport(); }
        function f_previewRateMerge() { return f_notSupport(); }
        function f_submitRateMerge() { return f_notSupport(); }
        function f_displayMatrixById() { return f_notSupport(); }
        function editPinState() { return f_notSupport(); }
        function updateSize() { }

        var PML = { 'options': { 'IsLead': false } };

        jQuery(document).delegate('a.showlooser', 'click', function(){
            return f_notSupport();
        });
        jQuery(document).delegate('a.displayCostsLink', 'click', function(e){
          return f_notSupport();
        });
        jQuery(document).delegate('a.displayMsgLink', 'click', function(e){
          return f_notSupport();
        });
        </xsl:text>
      </script>
      <body scroll="yes" onload="_init();">
        <table width="100%"><tr><td align="center"><h3>Loan Submission Audit Detail</h3></td></tr></table>
        
        <table border="0">
          <tr><td style="font-weight:bold;color:black">Timestamp:</td><td style="color:black"><xsl:value-of select="$Timestamp" /></td></tr>
          <tr><td style="font-weight:bold;color:black">User Name:</td><td style="color:black"><xsl:value-of select="@uname" /></td></tr>
		  <tr><td style="font-weight:bold;color:black">Login:</td><td style="color:black"><xsl:value-of select="body/data/@login"/></td></tr>
          <tr><td style="font-weight:bold;color:black">Event:</td><td style="color:black"><xsl:value-of select="@desc"/></td></tr>
                   
          <tr><td colspan="2"> </td></tr>
          <tr>
            <td style="font-weight:bold;color:black">1<sup>st</sup> Product Name:</td><td style="color:black"><xsl:value-of select="body/data/@_1Nm"/></td>
            <td style="font-weight:bold;color:black">2<sup>nd</sup> Product Name:</td><td style="color:black"><xsl:value-of select="body/data/@_2Nm"/></td>
          </tr>
          <tr>
            <td style="font-weight:bold;color:black">1<sup>st</sup> Rate:</td><td style="color:black"><xsl:value-of select="body/data/@_1Rate"/></td>
            <td style="font-weight:bold;color:black">2<sup>nd</sup> Rate:</td><td style="color:black"><xsl:value-of select="body/data/@_2Rate"/></td>            
          </tr>
          <tr>
            <td style="font-weight:bold;color:black">1<sup>st</sup> Point:</td><td style="color:black"><xsl:value-of select="body/data/@_1Point"/></td>
            <td style="font-weight:bold;color:black">2<sup>nd</sup> Point:</td><td style="color:black"><xsl:value-of select="body/data/@_2Point"/></td>            
          </tr>          
          <tr>
            <td style="font-weight:bold;color:black">1<sup>st</sup> Margin:</td><td style="color:black"><xsl:value-of select="body/data/@_1Margin"/></td>
            <td style="font-weight:bold;color:black">2<sup>nd</sup> Margin:</td><td style="color:black"><xsl:value-of select="body/data/@_2Margin"/></td>            
          </tr>          
          <tr>
            <td style="font-weight:bold;color:black">1<sup>st</sup> Payment:</td><td style="color:black"><xsl:value-of select="body/data/@_1Payment"/></td>
            <td style="font-weight:bold;color:black">2<sup>nd</sup> Payment:</td><td style="color:black"><xsl:value-of select="body/data/@_2Payment"/></td>            
          </tr>          
          <tr></tr>          
          <tr><td colspan="4" align="center">
            <input type="button" value="Display 1st Certificate" onclick="f_display('FirstCert');" style="width:170px"></input>
            <input type="button" value="Display 2nd Certificate" onclick="f_display('SecondCert');" style="width:170px"></input>
            <input type="button" value="Display 1st Result" onclick="f_display('FirstResult');" style="width:150px"></input>            
            <input type="button" value="Display 2nd Result" onclick="f_display('SecondResult');" style="width:150px"></input>
              <input type="button" value="Display 1st Par Rate Calc" onclick="f_display('ParData');" style="width:220px;"></input>
            <input type="button" value="Close" onclick="self.close();"></input>
          </td></tr>
          
        </table>
        <hr />
        <div id="FirstCert">
        <xsl:value-of select="body/data/_1Cert" disable-output-escaping="yes"/>
        </div>
        <div id="SecondCert" style="display:none">
        <xsl:value-of select="body/data/_2Cert" disable-output-escaping="yes"/>
        </div>
        <div id="FirstResult" style="display:none">
          <xsl:value-of select="body/data/_1Result" disable-output-escaping="yes"/>
        </div>
        <div id="SecondResult" style="display:none">
          <xsl:value-of select="body/data/_2Result" disable-output-escaping="yes"/>
        </div>
          <div id="ParData" style="display:none"  >
              <xsl:value-of select="body/data/_ParDebug" disable-output-escaping="yes"/>
          </div>
      </body>
    </html>
  </xsl:template>  
</xsl:stylesheet>

  