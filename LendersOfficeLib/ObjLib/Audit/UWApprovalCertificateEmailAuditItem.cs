﻿using System;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;


namespace LendersOffice.Audit
{
    public class UWApprovalCertificateEmailAuditItem : AbstractAuditItem
    {
        #region ATTR Constants
        private const string ATTR_EMAIL = "email";
        private const string ATTR_APPROVAL = "approvalCert";
        private const string ATTR_FROM = "from"; 
        #endregion

        private string m_email;
        private string m_certContent;
		private string m_loginName;
        private string m_from;
        public UWApprovalCertificateEmailAuditItem(AbstractUserPrincipal principal, string email, string from, string certContent) :
            base(principal, "Email Underwriting Approval Certificate", "UnderwritingApprovalCertificateEmail.xslt", DataAccess.E_AuditItemCategoryT.Email)
		{
            m_email = email;
            m_certContent = certContent;
			m_loginName = principal.LoginNm;
            m_from = from; 

		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_EMAIL, m_email);
            WriteSafeAttr(writer, ATTR_FROM, m_from);
			WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_certContent) 
            {
                writer.WriteStartElement(ATTR_APPROVAL);
                writer.WriteCData(m_certContent);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
    }
}
