﻿//-----------------------------------------------------------------------
// <summary>
//       Represents an audit for the reason that it was archived, 
//       along with inherited properties from AbstractAuditItem
// </summary>
// <copyright file="GFEArchivedAuditItem.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <author>Scott Kibler</author>
//-----------------------------------------------------------------------
namespace LendersOffice.ObjLib.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit for the reason that it was archived, 
    /// along with inherited properties from AbstractAuditItem.
    /// </summary>
    public sealed class GFEArchivedAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The XML attribute name for the archived reason type.
        /// </summary>
        private const string AttrReasonArchiveD = "GFEArchivedReasonT";

        /// <summary>
        /// Value indicating if this audit item is for archiving a 2015 GFE. Default is false. Value must be changed in constructor.
        /// </summary>
        private bool is2015GFE = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GFEArchivedAuditItem" /> class.
        /// Represents an audit for the reason that it was archived, 
        /// along with inherited properties from AbstractAuditItem.
        /// </summary>
        /// <param name="gfeArchivedReasonT">The reason it was archived.</param>      
        public GFEArchivedAuditItem(E_GFEArchivedReasonT gfeArchivedReasonT)
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, DataAccess.E_AuditItemCategoryT.GFEArchived)
        {
            this.Initialize(gfeArchivedReasonT);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GFEArchivedAuditItem" /> class.
        /// Represents an audit for the reason that it was archived, 
        /// along with inherited properties from AbstractAuditItem.
        /// </summary>
        /// <param name="gfeArchivedReasonT">The reason it was archived.</param>      
        /// <param name="isNewGFE">Whether this is an audit for archiving a new GFE or not.</param>
        public GFEArchivedAuditItem(E_GFEArchivedReasonT gfeArchivedReasonT, bool isNewGFE)
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, DataAccess.E_AuditItemCategoryT.GFEArchived)
        {
            this.is2015GFE = isNewGFE;

            this.Initialize(gfeArchivedReasonT);
        }

        /// <summary>
        /// Gets or sets the audit's type of reason that it was archived.
        /// </summary>
        private E_GFEArchivedReasonT GFEArchivedReasonT { get; set; }

        /// <summary>
        /// Writes the reason to the writer inside a data element.
        /// </summary>
        /// <param name="writer">The XML writer to be written to.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, AttrReasonArchiveD, (int)this.GFEArchivedReasonT);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Initializes this audit item.
        /// </summary>
        /// <param name="gfeArchivedReasonT">The reason it was archived.</param>      
        private void Initialize(E_GFEArchivedReasonT gfeArchivedReasonT)
        {
            this.HasDetails = false;
            this.GFEArchivedReasonT = gfeArchivedReasonT;

            if (E_GFEArchivedReasonT.NotYetDetermined == gfeArchivedReasonT)
            {
                Tools.LogErrorWithCriticalTracking("Programming Error: GFEArchived without a known reason.");
            }

            if (this.is2015GFE)
            {
                this.SetDescription("GFE was archived because " + Tools.Get_GFEArchivedReason_rep(this.GFEArchivedReasonT));
            }
            else
            {
                this.SetDescription("Legacy GFE was archived because " + Tools.Get_GFEArchivedReason_rep(this.GFEArchivedReasonT));
            }
        }
    }
}
