﻿using System;
using System.Xml;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class TitlePolicySubmissionXmlAuditItem : AbstractAuditItem
    {
        private string m_sXML;
        private string m_sLoginName;

        public TitlePolicySubmissionXmlAuditItem(AbstractUserPrincipal principal, string xml) :
            base(principal, "Title Policy Order Response (XML)", "TitlePolicySubmissionXml.xslt", DataAccess.E_AuditItemCategoryT.Integration)
        {
            m_sXML = xml;
            m_sLoginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");

            WriteSafeAttr(writer, "login", m_sLoginName);
            if (null != m_sXML)
            {
                writer.WriteStartElement("xml");
                writer.WriteCData(m_sXML);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
    }
}