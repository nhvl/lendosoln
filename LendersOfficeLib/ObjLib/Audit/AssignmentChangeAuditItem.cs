using System;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Collections;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOffice.Audit
{
	/// <summary>
	/// Audit event item for assignment changes in a loan.
	/// </summary>
	public class AssignmentChangeAuditItem : AbstractAuditItem
	{
		
		#region ATTR Constants
		private const string ATTR_ROLE = "role";
		private const string ATTR_PREVIOUS_EMPLOYEE = "old";
		private const string ATTR_NEW_EMPLOYEE = "new";
		#endregion

		private RoleAssignmentChangeList m_changes;
		private string m_loginName;
			
		public AssignmentChangeAuditItem(AbstractUserPrincipal principal, RoleAssignmentChangeList changes ) :
			base(principal, "Role Assignment Change", "AssignmentChange.xslt", E_AuditItemCategoryT.RoleAssignment)
		{
			m_changes = changes;
			m_loginName = principal.LoginNm;
		}

		protected override void GenerateDetailXml(XmlWriter writer) 
		{
			writer.WriteStartElement("data");
			WriteSafeAttr(writer, "login", m_loginName);
			foreach( RoleAssignmentChangeList.RoleAssignmentChange change in m_changes )
			{
				writer.WriteStartElement("change");
				WriteSafeAttr(writer, ATTR_ROLE, change.Role);
				WriteSafeAttr(writer, ATTR_PREVIOUS_EMPLOYEE, change.OldEmployeeName );
				WriteSafeAttr(writer, ATTR_NEW_EMPLOYEE, change.NewEmployeeName );
				writer.WriteEndElement(); // </change>				
			}			
			writer.WriteEndElement(); // </data>
		}

		// Light class to simplify enumeration of assignment changes
		public class RoleAssignmentChangeList : IEnumerable
		{
			private ArrayList m_changeList = new ArrayList();

			public IEnumerator GetEnumerator()
			{
				return m_changeList.GetEnumerator();
			}

			public void Add(string role, string previousEmployee, string newEmployee)
			{
				m_changeList.Add( new RoleAssignmentChange( role, previousEmployee, newEmployee ) );
			}

			public struct RoleAssignmentChange
			{
				public string Role, OldEmployeeName, NewEmployeeName;
				public RoleAssignmentChange( string role, string oldEmployeeName, string newEmployeeName )
				{
					Role = role;
					OldEmployeeName = oldEmployeeName;
					NewEmployeeName = newEmployeeName;
				}
			}
		}
	}

    public class TeamAssignmentChangeAuditItem : AbstractAuditItem
    {

        #region ATTR Constants
        private const string ATTR_ROLE = "role";
        private const string ATTR_PREVIOUS_EMPLOYEE = "old";
        private const string ATTR_NEW_EMPLOYEE = "new";
        #endregion

        private TeamAssignmentChangeList m_changes;
        private string m_loginName;

        public TeamAssignmentChangeAuditItem(AbstractUserPrincipal principal, TeamAssignmentChangeList changes) :
            base(principal, "Team Assignment Change", "AssignmentChange.xslt", E_AuditItemCategoryT.RoleAssignment)
        {
            m_changes = changes;
            m_loginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "login", m_loginName);
            foreach (TeamAssignmentChangeList.TeamAssignmentChange change in m_changes)
            {
                writer.WriteStartElement("change");
                WriteSafeAttr(writer, ATTR_ROLE, change.Role);
                WriteSafeAttr(writer, ATTR_PREVIOUS_EMPLOYEE, change.OldTeamName);
                WriteSafeAttr(writer, ATTR_NEW_EMPLOYEE, change.NewTeamName);
                writer.WriteEndElement(); // </change>				
            }
            writer.WriteEndElement(); // </data>
        }

        // Light class to simplify enumeration of assignment changes
        public class TeamAssignmentChangeList : IEnumerable
        {
            private ArrayList m_changeList = new ArrayList();

            public IEnumerator GetEnumerator()
            {
                return m_changeList.GetEnumerator();
            }

            public void Add(string role, string previousTeam, string newTeam)
            {
                m_changeList.Add(new TeamAssignmentChange(role, previousTeam, newTeam));
            }

            public struct TeamAssignmentChange
            {
                public string Role, OldTeamName, NewTeamName;
                public TeamAssignmentChange(string role, string oldTeamName, string newTeamName)
                {
                    Role = role;
                    OldTeamName = oldTeamName;
                    NewTeamName = newTeamName;
                }
            }
        }
    }

	public class AssignmentChangeAuditHelper
	{
		/// <summary>
		/// Create the assignment change audit event based on the modified role
		/// list and the current assignments of the loan.
		/// </summary>
		public static void CreateAuditEvents(AbstractUserPrincipal principal, Guid sLId, IDictionary modifiedRoleList )
		{
			if ( modifiedRoleList.Count > 0 )
			{
                CPageData dataLoan = new CEmployeeData(sLId);
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
				dataLoan.InitLoad();

				AssignmentChangeAuditItem.RoleAssignmentChangeList changes = new AssignmentChangeAuditItem.RoleAssignmentChangeList();
				
				// We now build a list of all changes made by this save and create only
				// one audit event for all of them.

				foreach ( DictionaryEntry roleChange in modifiedRoleList )
				{
					changes.Add( 
						Role.Get(roleChange.Key.ToString()).ModifiableDesc
						, GetEmployeeName( roleChange.Value.ToString() )
						, GetAssignedEmployee( dataLoan, roleChange.Key.ToString() )
						);
				}

				AbstractAuditItem auditItem = new AssignmentChangeAuditItem( principal, changes	);
				AuditManager.RecordAudit( sLId, auditItem );

			}
		}

        public static void CreateTeamAuditEvents(AbstractUserPrincipal principal, Guid sLId, IDictionary modifiedTeamList)
        {
            if (modifiedTeamList.Count > 0)
            {
                CPageData dataLoan = new CFullAccessPageData(sLId, new string[] { "sTeamCallCenterAgentName", "sTeamLenderAccExecName", "sTeamLoanRepName", "sTeamLoanOpenerName", "sTeamLockDeskName", "sTeamManagerName", "sTeamProcessorName", "sTeamRealEstateAgentName", "sTeamUnderwriterName", "sTeamCloserName", "sTeamShipperName", "sTeamFunderName", "sTeamPostCloserName", "sTeamInsuringName", "sTeamCollateralAgentName", "sTeamDocDrawerName", "sTeamCreditAuditorName", "sTeamDisclosureDeskName", "sTeamJuniorProcessorName", "sTeamJuniorUnderwriterName", "sTeamLegalAuditorName", "sTeamLoanOfficerAssistantName", "sTeamPurchaserName", "sTeamQCComplianceName", "sTeamSecondaryName", "sTeamServicingName" });
                dataLoan.InitLoad();

                TeamAssignmentChangeAuditItem.TeamAssignmentChangeList changes = new TeamAssignmentChangeAuditItem.TeamAssignmentChangeList();

                // We now build a list of all changes made by this save and create only
                // one audit event for all of them.

                foreach (DictionaryEntry teamChange in modifiedTeamList)
                {
                    changes.Add(
                        Role.Get(teamChange.Key.ToString()).ModifiableDesc
                        , GetTeamName(teamChange.Value.ToString())
                        , GetAssignedTeam(dataLoan, teamChange.Key.ToString())
                        );
                }

                AbstractAuditItem auditItem = new TeamAssignmentChangeAuditItem(principal, changes);
                AuditManager.RecordAudit(sLId, auditItem);

            }
        }


		/// <summary>
		/// Map from role description to actual employee.
		/// </summary>
        private static string GetAssignedEmployee(CPageData dataLoan, string roleDesc)
		{
			string employeeName = string.Empty;
			switch ( roleDesc )
			{
				case ConstApp.ROLE_CALL_CENTER_AGENT:         employeeName = dataLoan.sEmployeeCallCenterAgentName; break;
				case ConstApp.ROLE_LENDER_ACCOUNT_EXEC:       employeeName = dataLoan.sEmployeeLenderAccExecName; break;
				case ConstApp.ROLE_LOAN_OFFICER:              employeeName = dataLoan.sEmployeeLoanRepName; break;
				case ConstApp.ROLE_LOAN_OPENER:               employeeName = dataLoan.sEmployeeLoanOpenerName; break;
				case ConstApp.ROLE_LOCK_DESK:                 employeeName = dataLoan.sEmployeeLockDeskName; break;
				case ConstApp.ROLE_MANAGER:                   employeeName = dataLoan.sEmployeeManagerName; break;
				case ConstApp.ROLE_PROCESSOR:                 employeeName = dataLoan.sEmployeeProcessorName; break;
				case ConstApp.ROLE_REAL_ESTATE_AGENT:         employeeName = dataLoan.sEmployeeRealEstateAgentName; break;
				case ConstApp.ROLE_UNDERWRITER:               employeeName = dataLoan.sEmployeeUnderwriterName; break;
                case ConstApp.ROLE_CLOSER:                    employeeName = dataLoan.sEmployeeCloserName; break;
                case ConstApp.ROLE_BROKERPROCESSOR:           employeeName = dataLoan.sEmployeeBrokerProcessorName; break;
                case ConstApp.ROLE_SHIPPER:                   employeeName = dataLoan.sEmployeeShipperName; break;
                case ConstApp.ROLE_FUNDER:                    employeeName = dataLoan.sEmployeeFunderName; break;
                case ConstApp.ROLE_POSTCLOSER:                employeeName = dataLoan.sEmployeePostCloserName; break;
                case ConstApp.ROLE_INSURING:                  employeeName = dataLoan.sEmployeeInsuringName; break;
                case ConstApp.ROLE_COLLATERALAGENT:           employeeName = dataLoan.sEmployeeCollateralAgentName; break;
                case ConstApp.ROLE_DOCDRAWER:                 employeeName = dataLoan.sEmployeeDocDrawerName; break;
                case ConstApp.ROLE_CREDITAUDITOR:             employeeName = dataLoan.sEmployeeCreditAuditorName; break;
                case ConstApp.ROLE_DISCLOSUREDESK:            employeeName = dataLoan.sEmployeeDisclosureDeskName; break;
                case ConstApp.ROLE_JUNIORPROCESSOR:           employeeName = dataLoan.sEmployeeJuniorProcessorName; break;
                case ConstApp.ROLE_JUNIORUNDERWRITER:         employeeName = dataLoan.sEmployeeJuniorUnderwriterName; break;
                case ConstApp.ROLE_LEGALAUDITOR:              employeeName = dataLoan.sEmployeeLegalAuditorName; break;
                case ConstApp.ROLE_LOANOFFICERASSISTANT:      employeeName = dataLoan.sEmployeeLoanOfficerAssistantName; break;
                case ConstApp.ROLE_PURCHASER:                 employeeName = dataLoan.sEmployeePurchaserName; break;
                case ConstApp.ROLE_QCCOMPLIANCE:              employeeName = dataLoan.sEmployeeQCComplianceName; break;
                case ConstApp.ROLE_SECONDARY:                 employeeName = dataLoan.sEmployeeSecondaryName; break;
                case ConstApp.ROLE_SERVICING:                 employeeName = dataLoan.sEmployeeServicingName; break;
                case ConstApp.ROLE_EXTERNAL_SECONDARY:        employeeName = dataLoan.sEmployeeExternalSecondaryName; break;
                case ConstApp.ROLE_EXTERNAL_POST_CLOSER:      employeeName = dataLoan.sEmployeeExternalPostCloserName; break;

				default: Tools.LogBug( "Unknown Role: " + roleDesc ); break;
			}
			return GetEmployeeName( employeeName );
		}

		/// <summary>
		/// Return "None" if blank.
		/// </summary>
		private static string GetEmployeeName ( string name )
		{
			return ( name == string.Empty ) ? "None" : name;
		}

        /// <summary>
        /// Map from role description to actual employee.
        /// </summary>
        private static string GetAssignedTeam(CPageData dataLoan, string roleDesc)
        {
            string TeamName = string.Empty;
            switch (roleDesc)
            {
                case ConstApp.ROLE_CALL_CENTER_AGENT: TeamName = dataLoan.sTeamCallCenterAgentName; break;
                case ConstApp.ROLE_LENDER_ACCOUNT_EXEC: TeamName = dataLoan.sTeamLenderAccExecName; break;
                case ConstApp.ROLE_LOAN_OFFICER: TeamName = dataLoan.sTeamLoanRepName; break;
                case ConstApp.ROLE_LOAN_OPENER: TeamName = dataLoan.sTeamLoanOpenerName; break;
                case ConstApp.ROLE_LOCK_DESK: TeamName = dataLoan.sTeamLockDeskName; break;
                case ConstApp.ROLE_MANAGER: TeamName = dataLoan.sTeamManagerName; break;
                case ConstApp.ROLE_PROCESSOR: TeamName = dataLoan.sTeamProcessorName; break;
                case ConstApp.ROLE_REAL_ESTATE_AGENT: TeamName = dataLoan.sTeamRealEstateAgentName; break;
                case ConstApp.ROLE_UNDERWRITER: TeamName = dataLoan.sTeamUnderwriterName; break;
                case ConstApp.ROLE_CLOSER: TeamName = dataLoan.sTeamCloserName; break;
                case ConstApp.ROLE_SHIPPER: TeamName = dataLoan.sTeamShipperName; break;
                case ConstApp.ROLE_FUNDER: TeamName = dataLoan.sTeamFunderName; break;
                case ConstApp.ROLE_POSTCLOSER: TeamName = dataLoan.sTeamPostCloserName; break;
                case ConstApp.ROLE_INSURING: TeamName = dataLoan.sTeamInsuringName; break;
                case ConstApp.ROLE_COLLATERALAGENT: TeamName = dataLoan.sTeamCollateralAgentName; break;
                case ConstApp.ROLE_DOCDRAWER: TeamName = dataLoan.sTeamDocDrawerName; break;
                case ConstApp.ROLE_CREDITAUDITOR: TeamName = dataLoan.sTeamCreditAuditorName; break;
                case ConstApp.ROLE_DISCLOSUREDESK: TeamName = dataLoan.sTeamDisclosureDeskName; break;
                case ConstApp.ROLE_JUNIORPROCESSOR: TeamName = dataLoan.sTeamJuniorProcessorName; break;
                case ConstApp.ROLE_JUNIORUNDERWRITER: TeamName = dataLoan.sTeamJuniorUnderwriterName; break;
                case ConstApp.ROLE_LEGALAUDITOR: TeamName = dataLoan.sTeamLegalAuditorName; break;
                case ConstApp.ROLE_LOANOFFICERASSISTANT: TeamName = dataLoan.sTeamLoanOfficerAssistantName; break;
                case ConstApp.ROLE_PURCHASER: TeamName = dataLoan.sTeamPurchaserName; break;
                case ConstApp.ROLE_QCCOMPLIANCE: TeamName = dataLoan.sTeamQCComplianceName; break;
                case ConstApp.ROLE_SECONDARY: TeamName = dataLoan.sTeamSecondaryName; break;
                case ConstApp.ROLE_SERVICING: TeamName = dataLoan.sTeamServicingName; break;

                default: Tools.LogBug("Unknown Role: " + roleDesc); break;
            }
            return GetTeamName(TeamName);
        }

        /// <summary>
        /// Return "None" if blank.
        /// </summary>
        private static string GetTeamName(string name)
        {
            return (name == string.Empty) ? "None" : name;
        }


	}
}
