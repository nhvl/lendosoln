using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOffice.Audit
{
	/// <summary>
	/// Summary description for LoanStatusChangeAuditItem.
	/// </summary>
	public class LoanStatusChangeAuditItem : AbstractAuditItem
	{
		#region ATTR Constants
		private const string ATTR_PREVIOUS_STATUS = "old";
		private const string ATTR_NEW_STATUS = "new";
		#endregion

		private string m_previousStatus;
		private string m_newStatus;
		private string m_loginName;

        /// <summary>
        /// List of cleared statuses.
        /// </summary>
        private IEnumerable<ClearedStatusAuditDetails> ClearedStatuses;

        public LoanStatusChangeAuditItem( AbstractUserPrincipal principal, string previousStatus, string newStatus ) :
			base( principal, "Loan Status Change", "LoanStatusChange.xslt", E_AuditItemCategoryT.LoanStatus )
		{
			m_previousStatus = previousStatus;
			m_newStatus = newStatus;
			m_loginName = principal.LoginNm;

            this.ClearedStatuses = Enumerable.Empty<ClearedStatusAuditDetails>();
        }

        public LoanStatusChangeAuditItem(AbstractUserPrincipal principal, string previousStatus, string newStatus, IEnumerable<ClearedStatusAuditDetails> clearedStatuses) :
            base(principal, "Loan Status Change", "LoanStatusChange.xslt", E_AuditItemCategoryT.LoanStatus)
        {
            m_previousStatus = previousStatus;
            m_newStatus = newStatus;
            m_loginName = principal.LoginNm;

            this.ClearedStatuses = clearedStatuses;
        }
        protected override void GenerateDetailXml(XmlWriter writer) 
		{

			writer.WriteStartElement("data");
			WriteSafeAttr(writer, ATTR_PREVIOUS_STATUS, m_previousStatus);
			WriteSafeAttr(writer, ATTR_NEW_STATUS, m_newStatus);
			WriteSafeAttr(writer, "login", m_loginName);

            if (this.ClearedStatuses.Any())
            {
                writer.WriteStartElement("cleared_statuses");
                foreach (ClearedStatusAuditDetails clearedStatus in this.ClearedStatuses)
                {
                    writer.WriteStartElement("status");

                    this.WriteSafeAttr(writer, "name", clearedStatus.Name);
                    this.WriteSafeAttr(writer, "date", clearedStatus.Date);
                    this.WriteSafeAttr(writer, "comment", clearedStatus.Comment);

                    writer.WriteEndElement(); // </status>
                }

                writer.WriteEndElement(); // </cleared_statuses>
            }

            writer.WriteEndElement(); // </data>
		}

        /// <summary>
        /// Audit details for cleared statuses.
        /// </summary>
        public class ClearedStatusAuditDetails
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ClearedStatusAuditDetails" /> class.
            /// </summary>
            /// <param name="name">Friendly status name.</param>
            /// <param name="date">Status set date in string format.</param>
            /// <param name="comment">Status comment.</param>
            public ClearedStatusAuditDetails(string name, string date, string comment)
            {
                this.Name = name;
                this.Date = date;
                this.Comment = comment;
            }

            /// <summary>
            /// Gets or sets status name.
            /// </summary>
            /// <value>Status name.</value>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets status date.
            /// </summary>
            /// <value>Status date.</value>
            public string Date { get; set; }

            /// <summary>
            /// Gets or sets status comment.
            /// </summary>
            /// <value>Status comment.</value>
            public string Comment { get; set; }
        }
    }
}
