﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class FannieMaeEarlyCheckAuditItem : AbstractAuditItem
    {
        public FannieMaeEarlyCheckAuditItem(AbstractUserPrincipal principal)
            : base(principal, "Fannie Mae EarlyCheck", "FannieMaeEarlyCheck.xslt", DataAccess.E_AuditItemCategoryT.Integration)
        {
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            
        }
    }
}
