﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Audit;
using LendersOffice.Security;
using DataAccess;

namespace LendersOffice.ObjLib.Audit
{
    public class DisclosureESignAuditItem : AbstractAuditItem
    {
        public DisclosureESignAuditItem(AbstractUserPrincipal principal, string desc)
            : base(principal, desc, "", E_AuditItemCategoryT.DisclosureESign)
        {
            HasDetails = false;
        }

        public DisclosureESignAuditItem(Guid userId, string displayName, string desc)
            : base(userId, displayName, desc, E_AuditItemCategoryT.DisclosureESign)
        {
            HasDetails = false;
        }

        public DisclosureESignAuditItem(string desc)
            : base(desc, "", E_AuditItemCategoryT.DisclosureESign)
        {
            HasDetails = false;
        }



        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
        }

    }
}
