﻿namespace LendersOffice.ObjLib.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Provides an audit record for when a loan file is migrated
    /// off a disclosure state that requires e-consent disclosure
    /// monitoring.
    /// </summary>
    public class DisableEConsentDisclosureMonitoringAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisableEConsentDisclosureMonitoringAuditItem"/> class.
        /// </summary>
        /// <param name="fieldSnapshot">
        /// The snapshot of fields prior to running the associated migration.
        /// Expected format is Field Name, Original, Current.
        /// </param>
        public DisableEConsentDisclosureMonitoringAuditItem(List<Tuple<string, string, string>> fieldSnapshot) 
            : base(SystemUserPrincipal.TaskSystemUser.UserId, "System User", "E-Disclosure Monitoring Disabled", E_AuditItemCategoryT.DisclosureESign)
        {
            this.SetXsltFile("DisableEConsentDisclosureMonitoring.xslt");
            this.FieldSnapshot = fieldSnapshot;
        }

        /// <summary>
        /// Gets the snapshot of fields prior to running the associated migration.
        /// </summary>
        private List<Tuple<string, string, string>> FieldSnapshot { get; }

        /// <summary>
        /// Generates the detail XML for the audit item.
        /// </summary>
        /// <param name="writer">
        /// The XML writer to write data.
        /// </param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");

            foreach (var snapshot in this.FieldSnapshot)
            {
                writer.WriteStartElement("field");

                this.WriteSafeAttr(writer, "id", snapshot.Item1);
                this.WriteSafeAttr(writer, "old", string.IsNullOrEmpty(snapshot.Item2) ? "(No Value)" : snapshot.Item2);
                this.WriteSafeAttr(writer, "new", string.IsNullOrEmpty(snapshot.Item3) ? "(No Value)" : snapshot.Item3);

                writer.WriteEndElement(); // </field>
            }

            writer.WriteEndElement(); // </data>
        }
    }
}
