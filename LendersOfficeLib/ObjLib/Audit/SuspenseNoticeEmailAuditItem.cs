﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Audit;
using LendersOffice.Security;
using System.Xml;

namespace LendersOffice.Audit
{
    public class SuspenseNoticeEmailAuditItem : AbstractAuditItem
    {
        #region ATTR Constants
        private const string ATTR_EMAIL = "email";
        private const string ATTR_SUSPENSE = "suspenseNotice";
        private const string ATTR_FROM = "from"; 
        #endregion

        private string m_email;
        private string m_suspenseNoticeContent;
		private string m_loginName;
        private string m_from;
        public SuspenseNoticeEmailAuditItem(AbstractUserPrincipal principal, string email, string from, string noticeContent) :
            base(principal, "Email Suspense Notice", "SuspenseNoticeEmail.xslt", DataAccess.E_AuditItemCategoryT.Email)
		{
            m_email = email;
            m_suspenseNoticeContent = noticeContent;
			m_loginName = principal.LoginNm;
            m_from = from; 
		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_EMAIL, m_email);
            WriteSafeAttr(writer, ATTR_FROM, m_from);
			WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_suspenseNoticeContent) 
            {
                writer.WriteStartElement(ATTR_SUSPENSE);
                writer.WriteCData(m_suspenseNoticeContent);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
    }
}
