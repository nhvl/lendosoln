﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using System.Xml;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.Audit
{
    public sealed class TrackedFieldModifiedAuditItem : AbstractAuditItem
    {


        private const string ATTR_FRIENDLY_NAME = "FriendlyFieldName";
        public const string ATTR_OLD_VALUE = "FieldOldValue";
        public const string ATTR_NEW_VALUE = "FieldNewValue";
        private const string ATTR_FIELD_ID = "FieldId";
        private const string ATTR_CHANGED_AT = "ChangedAt";


        private string FieldFriendlydName { get; set; }
        private string OriginalValue { get; set; }
        private string NewValue { get; set; }
        private string FieldId { get; set; }
        private string ChangedAt { get; set; }



        public TrackedFieldModifiedAuditItem(TrackedField field, Guid userId, string displayName, 
            string oldValue, string newValue, string changedAt, bool isautomatic)
            : base(userId, displayName, "", DataAccess.E_AuditItemCategoryT.FieldChange)
        {
            StringBuilder sb = new StringBuilder();
            if (isautomatic)
            {
                sb.Append("Automatic: ");
            }

            HasDetails = false;
            sb.AppendFormat(
                @"{0} changed from ""{1}"" to ""{2}""", 
                field.Description, 
                string.IsNullOrEmpty(oldValue) ? "(No Value)" : oldValue, 
                string.IsNullOrEmpty(newValue) ? "(No Value)" : newValue);

            SetDescription(sb.ToString());

            FieldFriendlydName = field.Description;
            OriginalValue = oldValue;
            NewValue = newValue;
            FieldId = field.FieldId;
            ChangedAt = changedAt;

            List<Permission> requiredPermissions = new List<Permission>();
            if (field.RequiresAccountantPermision)
            {
                requiredPermissions.Add(Permission.AllowAccountantRead);
            }
            if (field.RequiresCloserPermission)
            {
                requiredPermissions.Add(Permission.AllowCloserRead);
            }
            if (field.RequiresLockDeskPermission)
            {
                requiredPermissions.Add(Permission.AllowLockDeskRead);
            }
            if (field.RequiresInvestorInfoPermission)
            {
                requiredPermissions.Add(Permission.AllowViewingInvestorInformation);
            }

            SetPermissions(requiredPermissions);

            if (field.FieldId == "sStatusT")
            {
                this.SetCategory(DataAccess.E_AuditItemCategoryT.LoanStatus);
            }

        }

        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_FRIENDLY_NAME, FieldFriendlydName);
            WriteSafeAttr(writer, ATTR_OLD_VALUE, OriginalValue);
            WriteSafeAttr(writer, ATTR_NEW_VALUE, NewValue);
            WriteSafeAttr(writer, ATTR_FIELD_ID, FieldId);
            WriteSafeAttr(writer, ATTR_CHANGED_AT, ChangedAt);
            writer.WriteEndElement();
        }
    }
}
