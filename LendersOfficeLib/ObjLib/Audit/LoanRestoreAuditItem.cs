﻿using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class LoanRestoreAuditItem : AbstractAuditItem
    {
        private string m_restoreRequestUserFirstName;
        private string m_restoreRequestUserLastName;
        public LoanRestoreAuditItem(string requestByFirstName, string requestByLastName): base("Loan Restore", "LoanRestore.xslt", DataAccess.E_AuditItemCategoryT.LoanStatus)
        {
            m_restoreRequestUserFirstName = requestByFirstName;
            m_restoreRequestUserLastName = requestByLastName;
        }


        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "restoreRequestUserFirstName", m_restoreRequestUserFirstName);
            WriteSafeAttr(writer, "restoreRequestUserLastName", m_restoreRequestUserLastName);
            writer.WriteEndElement();
        }
    }
}
