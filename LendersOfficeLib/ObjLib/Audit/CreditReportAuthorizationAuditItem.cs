﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    /// <summary>
    /// An audit item for the authorization of a credit report by one or more borrower.
    /// </summary>
    public class CreditReportAuthorizationAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The login of the user recording this audit event.
        /// </summary>
        private string login;

        /// <summary>
        /// The actions necessary to write the borrower data to the <seealso cref="XmlWriter"/> during serialization.
        /// </summary>
        private IList<Action<XmlWriter>> borrowerElementWriteActions;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportAuthorizationAuditItem"/> class.
        /// </summary>
        /// <param name="principal">The principal recording this event.</param>
        public CreditReportAuthorizationAuditItem(Security.AbstractUserPrincipal principal)
            : base(
                  principal,
                  "Credit Report Authorization received",
                  "CreditReportAuthorization.xslt",
                  DataAccess.E_AuditItemCategoryT.CreditReport)
        {
            this.login = principal.LoginNm;
            this.borrowerElementWriteActions = new List<Action<XmlWriter>>();
        }

        /// <summary>
        /// Adds a borrower to the list of borrowers authorizing a credit report.
        /// </summary>
        /// <param name="name">The name of the borrower.</param>
        /// <param name="ssn">The social security number of the borrower.</param>
        /// <param name="isCoborrower">A boolean value indicating whether the borrower is a co-borrower.</param>
        public void AddBorrower(string name, string ssn, bool isCoborrower)
        {
            this.borrowerElementWriteActions.Add(writer =>
            {
                writer.WriteStartElement("borr");
                this.WriteSafeAttr(writer, "name", name);
                this.WriteSafeAttr(writer, "ssn", ssn);
                this.WriteSafeAttr(writer, "isCo", isCoborrower);
                writer.WriteEndElement();
            });
        }

        /// <summary>
        /// Generates the XML of the audit event's details.
        /// </summary>
        /// <param name="writer">The <see cref="XmlWriter"/> recording the audit event.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "login", this.login);
            foreach (var action in this.borrowerElementWriteActions)
            {
                action(writer);
            }

            writer.WriteEndElement();
        }
    }
}
