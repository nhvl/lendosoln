﻿namespace LendersOffice.Audit
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using ObjLib.TitleProvider;
    using Security;

    /// <summary>
    /// Represents an audit entry for the import of a title quote onto the loan.
    /// </summary>
    public class TitleQuoteImportAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The user's login.
        /// </summary>
        private readonly string login;

        /// <summary>
        /// The title of the audit detail page.
        /// </summary>
        private readonly string detailTitle;

        /// <summary>
        /// The fees that were applied to the loan file.
        /// </summary>
        private readonly IEnumerable<TitleFeeQuoteApplicationRecord> appliedFees;

        /// <summary>
        /// The fee comparison html to display to the user.
        /// </summary>
        private readonly string htmlContent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleQuoteImportAuditItem"/> class.  This constructor is used to create a First American audit entry.
        /// </summary>
        /// <param name="principal">The user who applied the title quote.</param>
        /// <param name="appliedFees">The fees applied to the loan file.</param>
        public TitleQuoteImportAuditItem(AbstractUserPrincipal principal, IEnumerable<TitleFeeQuoteApplicationRecord> appliedFees)
            : base(principal, "First American Title Quote Imported", "TitleQuoteImport.xslt", E_AuditItemCategoryT.Integration)
        {
            this.login = principal.LoginNm;
            this.detailTitle = "First American Title Quote Details";
            this.appliedFees = appliedFees;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleQuoteImportAuditItem"/> class.  This constructor is used to create a generic audit entry.
        /// </summary>
        /// <param name="principal">The user who applied the title quote.</param>
        /// <param name="htmlContent">The html content to display to the user.</param>
        public TitleQuoteImportAuditItem(AbstractUserPrincipal principal, string htmlContent)
            : base(principal, "Title quote applied", "TitleQuoteImport.xslt", E_AuditItemCategoryT.Integration)
        {
            this.login = principal.LoginNm;
            this.detailTitle = "Title Quote Details";
            this.htmlContent = htmlContent;
        }

        /// <summary>
        /// Generates the details of the audit event to save.
        /// </summary>
        /// <param name="writer">The XML writer that is recording the audit event.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            this.WriteSafeAttr(writer, "login", this.login);
            this.WriteSafeAttr(writer, "Title", this.detailTitle);
            if (this.htmlContent != null)
            {
                writer.WriteStartElement("htmlContent");
                writer.WriteCData(this.htmlContent);
                writer.WriteEndElement();
            }

            if (this.appliedFees != null)
            {
                var converter = new LosConvert();
                var feeXml = this.appliedFees
                    .OrderBy(fee => fee.SortPositionData)
                    .ThenBy(fee => fee.FeeName)
                    .Select(fee => new XElement(
                    "fee",
                    new XAttribute("desc", fee.FeeName),
                    new XAttribute("borrAmt", FormatAsMoney(fee.BorrowerAmount, converter)),
                    new XAttribute("sellAmt", FormatAsMoney(fee.SellerAmount, converter)),
                    new XAttribute("otherAmt", "N/A"),
                    fee.CostComponents.Select(cost => new XElement(
                        "cost",
                        new XAttribute("desc", cost.Name),
                        new XAttribute("borrAmt", FormatAsMoney(cost.BorrowerResponsibleAmount ?? 0M, converter)),
                        new XAttribute("sellAmt", FormatAsMoney(cost.SellerResponsibleAmount ?? 0M, converter)),
                        new XAttribute("otherAmt", FormatAsMoney(cost.OtherResponsibleAmount ?? 0M, converter))))));
                foreach (var fee in feeXml)
                {
                    fee.WriteTo(writer);
                }
            }
        }

        /// <summary>
        /// Formats a value as the dollar amount, or "N/A" if there is no value.
        /// </summary>
        /// <param name="value">The value to format.</param>
        /// <param name="converter">The format converter to use.</param>
        /// <returns>The string representation of the dollar amount.</returns>
        private static string FormatAsMoney(decimal? value, LosConvert converter)
        {
            return value.HasValue ? converter.ToMoneyString(value.Value, FormatDirection.ToRep) : "N/A";
        }
    }
}
