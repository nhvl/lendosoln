/// Author: David Dao

using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
	public class SendToLoanProspectorAuditItem : AbstractAuditItem
	{
		private string m_loginName;
		public SendToLoanProspectorAuditItem(AbstractUserPrincipal principal) : base(principal, "Send to Loan Product Advisor", "SendToLp.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
			m_loginName = principal.LoginNm;
		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
			writer.WriteStartElement("data");
			WriteSafeAttr(writer, "login", m_loginName);
			writer.WriteEndElement(); // </data>
        }
	}
}
