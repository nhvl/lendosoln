﻿using System;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class RateLockConfirmationEmailAuditItem : AbstractAuditItem
    {
        #region ATTR Constants
        private const string ATTR_EMAIL = "email";
        private const string ATTR_RATELOCK = "ratelock";
        private const string ATTR_FROM = "from"; 
        #endregion

        private string m_email;
        private string m_rLockEmailBody;
		private string m_loginName;
        private string m_from;
        public RateLockConfirmationEmailAuditItem(AbstractUserPrincipal principal, string email, string from, string rLockEmail) :
            base(principal, "Email Rate Lock Confirmation", "RateLockConfirmationEmail.xslt", DataAccess.E_AuditItemCategoryT.Email)
		{
            m_email = email;
            m_rLockEmailBody = rLockEmail;
			m_loginName = principal.LoginNm;
            m_from = from; 

		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_EMAIL, m_email);
            WriteSafeAttr(writer, ATTR_FROM, m_from);
			WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_rLockEmailBody) 
            {
                writer.WriteStartElement(ATTR_RATELOCK);
                writer.WriteCData(m_rLockEmailBody);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
    }
}
