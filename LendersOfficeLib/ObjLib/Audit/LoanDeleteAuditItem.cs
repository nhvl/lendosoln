using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
	public class LoanDeleteAuditItem : AbstractAuditItem
	{
		private string m_loginName;
		public LoanDeleteAuditItem(AbstractUserPrincipal principal) : base(principal, "Loan Delete", "LoanDelete.xslt", DataAccess.E_AuditItemCategoryT.LoanStatus)
		{
			m_loginName = principal.LoginNm;
		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
			writer.WriteStartElement("data");
			WriteSafeAttr(writer, "login", m_loginName);
			writer.WriteEndElement();
        }
	}
}
