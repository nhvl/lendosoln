﻿namespace LendersOffice.Audit
{
    /// <summary>
    /// Indicates the type of action being recorded to the audit history.
    /// </summary>
    public enum AgentRecordChangeType
    {
        /// <summary>
        /// Indicates the audit is for the creation of an agent record.
        /// </summary>
        CreateRecord = 0,

        /// <summary>
        /// Indicates the audit is for an edit on an agent record.
        /// </summary>
        EditRecord = 1,

        /// <summary>
        /// Indicates the audit is for the deletion of an agent record.
        /// </summary>
        DeleteRecord = 2
    }
}
