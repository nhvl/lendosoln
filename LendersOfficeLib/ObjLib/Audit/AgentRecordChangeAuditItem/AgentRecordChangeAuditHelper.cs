﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    /// <summary>
    /// A helper class to manage the creation of agent record change audit events.
    /// </summary>
    public static class AgentRecordChangeAuditHelper
    {
        /// <summary>
        /// A list of agent fields that should be tracked in audits.
        /// </summary>
        private static readonly string[] TrackedFields = new string[]
        {
            "AgentRoleT",
            "AgentName",
            "CaseNum",
            "CompanyName",
            "StreetAddr",
            "City",
            "State",
            "Zip",
            "County",
            "Phone",
            "CellPhone",
            "PagerNum",
            "FaxNum",
            "EmployeeIDInCompany",
            "CompanyId",
            "EmailAddr",
            "DepartmentName",
            "IsLenderAffiliate",
            "PhoneOfCompany",
            "FaxOfCompany",
            "LendingLicenseXmlContent",
            "CompanyLendingLicenseXmlContent",
            "ChumsId",
            "LoanOriginatorIdentifier",
            "CompanyLoanOriginatorIdentifier",
            "BranchName",
            "IsLender",
            "TaxId"
        };

        /// <summary>
        /// Creates an audit event with information about a changed agent record.
        /// </summary>
        /// <param name="agent">A data row representing the agent.</param>
        /// <param name="subjectPropertyState">The state where the subject property is located.</param>
        /// <param name="changeType">An enum indicating what action is being taken on the agent.</param>
        /// <param name="principal">Optional. The principal to use for the audit item.</param>
        /// <returns>The audit event or null if no changes were made to the agent.</returns>
        public static AgentRecordChangeAuditItem CreateAuditEvent(DataRow agent, string subjectPropertyState, AgentRecordChangeType changeType, AbstractUserPrincipal principal = null)
        {
            var changes = RetrieveChanges(agent, subjectPropertyState, changeType);

            AbstractUserPrincipal principalToUse = principal ?? PrincipalFactory.CurrentPrincipal;

            if (changes.Any())
            {
                var agentType = GetAgentRoleType(agent);
                var auditItem = new AgentRecordChangeAuditItem(principalToUse, agentType, changeType, changes);
                return auditItem;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the agent role type from a data row.
        /// </summary>
        /// <param name="agent">A data row representing the agent.</param>
        /// <returns>The role type for the agent.</returns>
        private static E_AgentRoleT GetAgentRoleType(DataRow agent)
        {
            E_AgentRoleT roleType;
            bool hasRole = Enum.TryParse(agent["AgentRoleT", DataRowVersion.Current].ToString(), out roleType);
            return hasRole ? roleType : E_AgentRoleT.Other;
        }

        /// <summary>
        /// Retrieves a list of field changes on an agent.
        /// </summary>
        /// <param name="agent">A data row representing the agent.</param>
        /// <param name="subjectPropertyState">The state where the subject property is located.</param>
        /// <param name="changeType">An enum indicating what action is being taken on the agent.</param>
        /// <returns>A list of <see cref="AuditFieldChange"/> objects, each representing a single field change.</returns>
        private static List<AuditFieldChange> RetrieveChanges(DataRow agent, string subjectPropertyState, AgentRecordChangeType changeType)
        {
            var changes = new List<AuditFieldChange>();

            foreach (DataColumn col in agent.Table.Columns)
            {
                if (!TrackedFields.Contains(col.ColumnName))
                {
                    continue;
                }

                var columnName = col.ColumnName;
                string oldValue, newValue;

                switch (changeType)
                {
                    case AgentRecordChangeType.CreateRecord:
                        oldValue = string.Empty;
                        newValue = GetDataSetValue(agent, col, DataRowVersion.Current);
                        break;
                    case AgentRecordChangeType.EditRecord:
                        oldValue = GetDataSetValue(agent, col, DataRowVersion.Original);
                        newValue = GetDataSetValue(agent, col, DataRowVersion.Current);
                        break;
                    case AgentRecordChangeType.DeleteRecord:
                        oldValue = GetDataSetValue(agent, col, DataRowVersion.Current);
                        newValue = string.Empty;
                        break;
                    default:
                        throw new UnhandledEnumException(changeType);
                }

                if (columnName.Equals("AgentRoleT", StringComparison.OrdinalIgnoreCase))
                {
                    columnName = "AgentRoleType";
                    oldValue = GetAgentRoleDescription(oldValue);
                    newValue = GetAgentRoleDescription(newValue);
                }

                if (!oldValue.Equals(newValue, StringComparison.Ordinal))
                {
                    if (columnName.Equals("CompanyLendingLicenseXmlContent", StringComparison.Ordinal))
                    {
                        ProcessLicenseChanges(changes, subjectPropertyState, oldValue, newValue, isCompanyData: true);
                    }
                    else if (columnName.Equals("LendingLicenseXmlContent", StringComparison.Ordinal))
                    {
                        ProcessLicenseChanges(changes, subjectPropertyState, oldValue, newValue, isCompanyData: false);
                    }
                    else
                    {
                        changes.Add(new AuditFieldChange(columnName, oldValue, newValue));
                    }
                }
            }

            return changes;
        }

        /// <summary>
        /// Processes license XML to extract certain datapoints that should be audited and adds them
        /// to the changelist.
        /// </summary>
        /// <param name="changes">The list of changes for the audit event.</param>
        /// <param name="subjectPropertyState">The state where the subject property is located.</param>
        /// <param name="oldLicenseData">The old license data XML.</param>
        /// <param name="newLicenseData">The new license data XML.</param>
        /// <param name="isCompanyData">True if the license data is for a company, false if it is for an individual.</param>
        private static void ProcessLicenseChanges(List<AuditFieldChange> changes, string subjectPropertyState, string oldLicenseData, string newLicenseData, bool isCompanyData)
        {
            var oldLicenseDictionary = ExtractLicenseData(oldLicenseData, subjectPropertyState);
            var newLicenseDictionary = ExtractLicenseData(newLicenseData, subjectPropertyState);

            var licenseNumberChange = CreateLicenseFieldChange(oldLicenseDictionary, newLicenseDictionary, "License", isCompanyData);
            if (licenseNumberChange != null)
            {
                changes.Add(licenseNumberChange);
            }

            var stateChange = CreateLicenseFieldChange(oldLicenseDictionary, newLicenseDictionary, "State", isCompanyData);
            if (stateChange != null)
            {
                changes.Add(stateChange);
            }

            var expirationChange = CreateLicenseFieldChange(oldLicenseDictionary, newLicenseDictionary, "ExpD", isCompanyData);
            if (expirationChange != null)
            {
                changes.Add(expirationChange);
            }
        }

        /// <summary>
        /// Creates an <see cref="AuditFieldChange" /> for a license datapoint.
        /// </summary>
        /// <param name="oldLicenseDictionary">The existing license data.</param>
        /// <param name="newLicenseDictionary">The updated license data.</param>
        /// <param name="key">The datapoint to retrieve.</param>
        /// <param name="isCompanyData">True if the license data is for a company, false if it is for an individual.</param>
        /// <returns>A <see cref="AuditFieldChange"/> representing a change in a license datapoint.</returns>
        private static AuditFieldChange CreateLicenseFieldChange(Dictionary<string, string> oldLicenseDictionary, Dictionary<string, string> newLicenseDictionary, string key, bool isCompanyData)
        {
            string oldLicenseValue = GetDictionaryValue(oldLicenseDictionary, key);
            string newLicenseValue = GetDictionaryValue(newLicenseDictionary, key);
            if (!string.Equals(oldLicenseValue, newLicenseValue, StringComparison.Ordinal))
            {
                string licenseColumnName = string.Empty;
                
                if (key.Equals("License"))
                {
                    licenseColumnName = isCompanyData ? "CompanyLendingLicenseNumber" : "LendingLicenseNumber";
                }
                else if (key.Equals("State"))
                {
                    licenseColumnName = isCompanyData ? "CompanyLendingLicenseState" : "LendingLicenseState";
                }
                else if (key.Equals("ExpD"))
                {
                    licenseColumnName = isCompanyData ? "CompanyLendingLicenseExpD" : "LendingLicenseExpD";
                }

                return new AuditFieldChange(licenseColumnName, oldLicenseValue, newLicenseValue);
            }

            return null;
        }

        /// <summary>
        /// Extracts the license number, state, and expiration date for a given state.
        /// </summary>
        /// <param name="rawLicenseXml">XML representing a set of licenses.</param>
        /// <param name="subjectPropertyState">The state for which we should retrieve a license.</param>
        /// <returns>A dictionary holding the number, state, and expiration for the relevant license.</returns>
        private static Dictionary<string, string> ExtractLicenseData(string rawLicenseXml, string subjectPropertyState)
        {
            var licenseDataPoints = new Dictionary<string, string>();

            // Retrieve the license in the subject property state with the latest
            // expiration date, as that's the one that shows up in the UI.
            var licenses = from LicenseInfo l in LicenseInfoList.ToObject(rawLicenseXml).List
                           where l.State.Equals(subjectPropertyState)
                           orderby l.ExpirationDate descending
                           select l;
            var license = licenses.FirstOrDefault();
            
            if (license != null)
            {
                licenseDataPoints["License"] = license.License;
                licenseDataPoints["State"] = license.State;
                licenseDataPoints["ExpD"] = license.ExpD;
            }

            return licenseDataPoints;
        }

        /// <summary>
        /// Gets the string version of the data at the given row and column.
        /// </summary>
        /// <param name="row">The row where the data is located.</param>
        /// <param name="col">The column where the data is located.</param>
        /// <param name="version">The data version to use.</param>
        /// <returns>The data at the given row and column.</returns>
        private static string GetDataSetValue(DataRow row, DataColumn col, DataRowVersion version)
        {
            return row[col, version].ToString();
        }

        /// <summary>
        /// Get the value from a dictionary with the given key, defaulting to an empty string if the
        /// key does not exist.
        /// </summary>
        /// <param name="dictionary">The dictionary to search.</param>
        /// <param name="key">The key to retrieve.</param>
        /// <returns>The value at the key, or an empty string if the key does not exist.</returns>
        private static string GetDictionaryValue(Dictionary<string, string> dictionary, string key)
        {
            if (dictionary == null || !dictionary.Any())
            {
                return string.Empty;
            }

            return dictionary.ContainsKey(key) ? dictionary[key] : string.Empty;
        }

        /// <summary>
        /// Retrieves the string description of an agent role type.
        /// </summary>
        /// <param name="role">The agent role.</param>
        /// <returns>The string description of an agent role type.</returns>
        private static string GetAgentRoleDescription(string role)
        {
            if (!string.IsNullOrEmpty(role))
            {
                E_AgentRoleT roleType;
                if (Enum.TryParse(role, out roleType))
                {
                    return RolodexDB.GetTypeDescription(roleType);
                }
            }

            return string.Empty;
        }
    }
}
