﻿namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit item recording information about a modified agent record.
    /// </summary>
    public class AgentRecordChangeAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// A list of field changes on the agent record.
        /// </summary>
        private List<AuditFieldChange> changes;

        /// <summary>
        /// The user who performed an action on an agent record.
        /// </summary>
        private string loginName;

        /// <summary>
        /// The type of action taken on an agent record.
        /// </summary>
        private AgentRecordChangeType changeType;

        /// <summary>
        /// Indicates whether the audit is due to a system update rather than an edit by a user.
        /// </summary>
        private bool isSystemUpdate;

        /// <summary>
        /// Initializes a new instance of the <see cref="AgentRecordChangeAuditItem"/> class.
        /// </summary>
        /// <param name="principal">Represents the user who performed an action on an agent record.</param>
        /// <param name="agentType">The contact type for this agent record.</param>
        /// <param name="changeType">The type of action taken on an agent record.</param>
        /// <param name="changes">A list of field changes on the agent record.</param>
        public AgentRecordChangeAuditItem(AbstractUserPrincipal principal, E_AgentRoleT agentType, AgentRecordChangeType changeType, List<AuditFieldChange> changes)
            : base(principal, GenerateDescription(agentType, changeType, changes), "AgentRecordChange.xslt", E_AuditItemCategoryT.AgentContactRecordChange)
        {
            this.changes = changes;
            this.changeType = changeType;

            if (principal == null || principal is SystemUserPrincipal)
            {
                this.isSystemUpdate = true;
                this.loginName = string.Empty;
                this.UserName = "System User";
            }
            else
            {
                this.isSystemUpdate = false;
                this.loginName = principal.LoginNm;
            }
        }

        /// <summary>
        /// Generates an audit item description based on the agent and change type.
        /// </summary>
        /// <param name="agentType">The type of agent being modified.</param>
        /// <param name="changeType">The type of modification being performed.</param>
        /// <param name="changes">The list of changes being made to the record.</param>
        /// <returns>A string description for the audit item.</returns>
        public static string GenerateDescription(E_AgentRoleT agentType, AgentRecordChangeType changeType, List<AuditFieldChange> changes)
        {
            if (changeType == AgentRecordChangeType.EditRecord)
            {
                var roleChange = changes.SingleOrDefault(
                    change => change.Field.Equals("AgentRoleType", StringComparison.OrdinalIgnoreCase));

                if (roleChange != null)
                {
                    // Special case: if the role has been changed for an existing agent, we
                    // need to explicitly state both the old and new role types.
                    return $" {roleChange.OldValue} removed and {roleChange.NewValue} added";
                }
            }

            string role = RolodexDB.GetTypeDescription(agentType);
            string action;

            switch (changeType)
            {
                case AgentRecordChangeType.EditRecord:
                    action = "Change";
                    break;
                case AgentRecordChangeType.CreateRecord:
                    action = "Added";
                    break;
                case AgentRecordChangeType.DeleteRecord:
                    action = "Removed";
                    break;
                default:
                    throw new UnhandledEnumException(changeType);
            }

            return role + " Contact " + action;
        }

        /// <summary>
        /// Generates XML for the audit item detail modal.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "includelogin", !this.isSystemUpdate);
            this.WriteSafeAttr(writer, "login", this.loginName);
            this.WriteSafeAttr(writer, "changetype", this.changeType.ToString("d"));

            string changeTypeAttribute = "changesheader";
            switch (this.changeType)
            {
                case AgentRecordChangeType.EditRecord:
                    this.WriteSafeAttr(writer, changeTypeAttribute, "Change:");
                    break;
                case AgentRecordChangeType.CreateRecord:
                    this.WriteSafeAttr(writer, changeTypeAttribute, "Added fields and values:");
                    break;
                case AgentRecordChangeType.DeleteRecord:
                    this.WriteSafeAttr(writer, changeTypeAttribute, "Removed fields and values:");
                    break;
                default:
                    throw new UnhandledEnumException(this.changeType);
            }

            foreach (var change in this.changes)
            {
                writer.WriteStartElement("change");
                this.WriteSafeAttr(writer, "field", change.Field);
                this.WriteSafeAttr(writer, "old", change.OldValue);
                this.WriteSafeAttr(writer, "new", change.NewValue);
                writer.WriteEndElement(); // </change>
            }

            writer.WriteEndElement(); // </data>
        }
    }
}
