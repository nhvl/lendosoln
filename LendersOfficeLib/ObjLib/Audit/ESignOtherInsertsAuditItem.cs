﻿// <copyright file="ESignOtherInsertsAuditItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//      Author: Jhairo Erazo
//      Date: 10/11/2016
// </summary>
namespace LendersOffice.ObjLib.Audit
{
    using System.Xml;

    using LendersOffice.Audit;

    /// <summary>
    /// ESign Update Other Inserts Received Audit Item.
    /// </summary>
    public class ESignOtherInsertsAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Array of Other Insert strings.
        /// </summary>
        private string[] otherInserts;

        /// <summary>
        /// Initializes a new instance of the <see cref="ESignOtherInsertsAuditItem" /> class.
        /// </summary>
        /// <param name="otherInserts">An array of strings containing the descriptions for the ESign Other Inseert items.</param>
        public ESignOtherInsertsAuditItem(string[] otherInserts) :
            base("Other Inserts information received from document vendor.", "ESignOtherInserts.xslt", DataAccess.E_AuditItemCategoryT.DisclosureESign)
        {
            this.UserName = "System Notification";  // At the moment this audit can only be created by a system process.
            this.otherInserts = otherInserts;
        }

        /// <summary>
        /// Generates the XML used for the Audit Details page.
        /// </summary>
        /// <param name="writer">XML writer.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("OtherInserts");

            foreach (string otherInsert in this.otherInserts)
            {
                writer.WriteElementString("OtherInsert", otherInsert);
            }

            writer.WriteEndElement(); // </OtherInserts>
        }
    }
}
