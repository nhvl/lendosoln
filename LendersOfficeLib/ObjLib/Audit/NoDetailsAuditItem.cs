﻿namespace LendersOffice.Audit
{
    using Security;

    /// <summary>
    /// Represents an audit event without details, which thus does not require any of
    /// the inheritance-based features of <see cref="AbstractAuditItem"/>. 
    /// </summary>
    public class NoDetailsAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoDetailsAuditItem"/> class.
        /// </summary>
        /// <param name="principal">The user who performed the event being audited.</param>
        /// <param name="description">The description of the audit event.</param>
        /// <param name="category">The category of the audit event.</param>
        private NoDetailsAuditItem(AbstractUserPrincipal principal, string description, DataAccess.E_AuditItemCategoryT category)
            : base(principal, description, null, category)
        {
            this.HasDetails = false;
        }

        /// <summary>
        /// Creates an audit event for an export to a core system via KIVA.
        /// </summary>
        /// <param name="user">The user who performed the event being audited.</param>
        /// <returns>The audit event recording the event.</returns>
        public static AbstractAuditItem CreateKivaExportAuditItem(AbstractUserPrincipal user)
        {
            return new NoDetailsAuditItem(
                user,
                "Exported to core system",
                DataAccess.E_AuditItemCategoryT.Integration);
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="writer">The XmlWriter to which nothing will be done.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
        }
    }
}
