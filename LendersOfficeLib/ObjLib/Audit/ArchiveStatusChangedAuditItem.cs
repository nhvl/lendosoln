﻿/// <summary>
/// Audit for when an archive has changed its status.
/// </summary>
namespace LendersOffice.ObjLib.Audit
{
    using System.Xml;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit item indicating when an archive has had its status changed.
    /// </summary>
    public sealed class ArchiveStatusChangedAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The archive's old status.
        /// </summary>
        private ClosingCostArchive.E_ClosingCostArchiveStatus oldArchiveStatus;

        /// <summary>
        /// The archive's new status.
        /// </summary>
        private ClosingCostArchive.E_ClosingCostArchiveStatus newArchivestatus;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArchiveStatusChangedAuditItem" /> class.
        /// </summary>
        /// <param name="category">The audit item category.</param>
        /// <param name="archiveType">The archive type.</param>
        /// <param name="archiveTime">The time when the archive was made.</param>
        /// <param name="archiveOldStatus">The archive's old status.</param>
        /// <param name="archiveNewStatus">The archive's new status.</param>
        public ArchiveStatusChangedAuditItem(E_AuditItemCategoryT category, ClosingCostArchive.E_ClosingCostArchiveType archiveType, string archiveTime, ClosingCostArchive.E_ClosingCostArchiveStatus archiveOldStatus, ClosingCostArchive.E_ClosingCostArchiveStatus archiveNewStatus)
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, category)
        {
            this.HasDetails = false;
            this.oldArchiveStatus = archiveOldStatus;
            this.newArchivestatus = archiveNewStatus;

            string archiveTypeAsString = this.ArchiveTypeToString(archiveType);
            string archiveOldStatusString = this.ArchiveStatusToString(archiveOldStatus);
            string archiveNewStatusString = this.ArchiveStatusToString(archiveNewStatus);

            this.SetDescription(string.Format("{0} archive from {1} status changed from {2} to {3}.", archiveTypeAsString, archiveTime, archiveOldStatusString, archiveNewStatusString));
        }

        /// <summary>
        /// Writes the old and new status in a data element.
        /// </summary>
        /// <param name="writer">The xml writer to use.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "OldArchiveStatusT", (int)this.oldArchiveStatus);
            this.WriteSafeAttr(writer, "NewArchiveStatusT", (int)this.newArchivestatus);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Converts the archive type to a nice string.
        /// </summary>
        /// <param name="archiveType">The archive type.</param>
        /// <returns>The archive type as a string.</returns>
        private string ArchiveTypeToString(ClosingCostArchive.E_ClosingCostArchiveType archiveType)
        {
            switch (archiveType)
            {
                case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure:
                    return "Closing Disclosure";
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2010:
                    return "GFE 2010";
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015:
                    return "GFE 2015";
                case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate:
                    return "Loan Estimate";
                default:
                    throw new UnhandledEnumException(archiveType);
            }
        }

        /// <summary>
        /// Converts the archive status to a nice string.
        /// </summary>
        /// <param name="archiveStatus">The archive status.</param>
        /// <returns>The archive status as a string.</returns>
        private string ArchiveStatusToString(ClosingCostArchive.E_ClosingCostArchiveStatus archiveStatus)
        {
            switch (archiveStatus)
            {
                case ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed:
                    return "Disclosed";
                case ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure:
                    return "Included in Closing Disclosure";
                case ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid:
                    return "Invalid";
                case ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration:
                    return "Pending Document Generation";
                case ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC:
                    return "Superseded CoC";
                case ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown:
                    return "Unknown";
                default:
                    throw new UnhandledEnumException(archiveStatus);
            }
        }
    }
}
