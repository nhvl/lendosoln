﻿namespace LendersOffice.ObjLib.Audit
{
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// An audit when the user sets an Unknown archive to the Pending status to re-submit a CoC.
    /// </summary>
    public class UnknownArchiveSetToPendingAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnknownArchiveSetToPendingAuditItem"/> class.
        /// </summary>
        /// <param name="invalidArchiveDate">The date of the invalid archive that replaced the unknown archive.</param>
        /// <param name="pendingArchiveDate">The date of the pending archive.</param>
        public UnknownArchiveSetToPendingAuditItem(string invalidArchiveDate, string pendingArchiveDate)
            : base(PrincipalFactory.CurrentPrincipal, string.Empty, null, DataAccess.E_AuditItemCategoryT.LoanEstimateArchived)
        {
            this.HasDetails = false;
            var msg = "Unknown archive from " + invalidArchiveDate + " was marked as Valid CoC for later disclosure. "
                + "Pending Document Generation archive " + pendingArchiveDate + " was created.";
            this.SetDescription(msg);
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="writer">The XmlWriter to which nothing will be done.</param>
        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
        }
    }
}
