﻿using System;
using System.Xml;

using LendersOffice.Security;
using LendersOffice.Common;
namespace LendersOffice.Audit
{
    public class DriveSubmissionAuditItem : AbstractAuditItem
    {
        private string m_html;
        private string m_loginName;

        public DriveSubmissionAuditItem(AbstractUserPrincipal principal, string html) :
            base(principal, "DataVerify DRIVE Response", "DRIVESubmission.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
            m_loginName = principal.LoginNm;
            m_html = html;
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");

            WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_html)
            {
                writer.WriteStartElement("html");
                writer.WriteCData(m_html);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }

    }
}
