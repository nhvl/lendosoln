/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
	public class RemoveRequestedRateAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_INTEREST_RATE = "rate";
        private const string ATTR_FEE = "fee";
        private const string ATTR_MARGIN = "margin";
		private const string ATTR_QUALIFYING_RATE = "qualRate";
		private const string ATTR_TEASER_RATE = "teaseRate";
		private const string ATTR_OPTION_ARM = "optionArm";
		private const string ATTR_TERM = "term";
		private const string ATTR_DUE = "due";
		private const string ATTR_AMORTIZATION_METHOD = "amortMethod";

        #endregion

        private string m_sNoteIRSubmitted;
        private string m_sLOrigFPcSubmitted;
        private string m_sRAdjMarginRSubmitted;
		//opm 19109 fs 06/16/2008
		private string m_sQualIRSubmitted;
		private string m_sOptionArmTeaserRSubmitted;
		private string m_isOptionArmSubmitted;
		private string m_sTerm;
		private string m_sDue;
		private string m_sFinMethT;
		private string m_loginName;


		public RemoveRequestedRateAuditItem(AbstractUserPrincipal principal, string sNoteIRSubmitted, string sLOrigFPcSubmitted, string sRAdjMarginRSubmitted, string sQualIRSubmitted, string sOptionArmTeaserRSubmitted, bool sIsOptionArmSubmitted, string sTerm, string sDue, string sFinMethT) :
			base(principal, "Clear Rate Lock Request", "RemoveRequestedRate.xslt", DataAccess.E_AuditItemCategoryT.RateLock)
		{
			m_sNoteIRSubmitted = sNoteIRSubmitted;
			m_sLOrigFPcSubmitted = sLOrigFPcSubmitted;
			m_sRAdjMarginRSubmitted = sRAdjMarginRSubmitted;
			//opm 19109 fs 06/16/2008
			m_sQualIRSubmitted  = sQualIRSubmitted;
			m_sOptionArmTeaserRSubmitted = sOptionArmTeaserRSubmitted;
			m_isOptionArmSubmitted = sIsOptionArmSubmitted ? "Yes" : "No";
			m_sTerm = sTerm;
			m_sDue = sDue;
			m_sFinMethT = sFinMethT;			
			m_loginName = principal.LoginNm;

		}

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_INTEREST_RATE, m_sNoteIRSubmitted);
            WriteSafeAttr(writer, ATTR_FEE, m_sLOrigFPcSubmitted);
			WriteSafeAttr(writer, ATTR_MARGIN, m_sRAdjMarginRSubmitted);
			//opm 19109 fs 06/16/2008
			WriteSafeAttr(writer, ATTR_QUALIFYING_RATE, m_sQualIRSubmitted);
			WriteSafeAttr(writer, ATTR_TEASER_RATE, m_sOptionArmTeaserRSubmitted);
			WriteSafeAttr(writer, ATTR_OPTION_ARM, m_isOptionArmSubmitted);
			WriteSafeAttr(writer, ATTR_TERM, m_sTerm);
			WriteSafeAttr(writer, ATTR_DUE, m_sDue);
			WriteSafeAttr(writer, ATTR_AMORTIZATION_METHOD, m_sFinMethT);
			WriteSafeAttr(writer, "login", m_loginName);

            writer.WriteEndElement(); // </data>
        }
	}
}
