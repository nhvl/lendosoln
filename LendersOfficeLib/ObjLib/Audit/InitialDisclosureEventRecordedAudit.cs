﻿namespace LendersOffice.ObjLib.Audit
{
    using System.Collections.Generic;
    using System.Xml;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Audit for recording an initial disclosure event.
    /// </summary>
    public sealed class InitialDisclosureEventRecordedAudit : AbstractAuditItem
    {
        /// <summary>
        /// The data used when recording the Initial Disclosure Event.
        /// </summary>
        private Dictionary<string, string> dataValues;

        /// <summary>
        /// The login name of the user who triggered this audit.
        /// </summary>
        private string loginName;

        /// <summary>
        /// Initializes a new instance of the <see cref="InitialDisclosureEventRecordedAudit"/> class.
        /// </summary>
        /// <param name="principal">The user who triggered the event.</param>
        /// <param name="dataValues">The data used for recording the event.</param>
        public InitialDisclosureEventRecordedAudit(AbstractUserPrincipal principal, Dictionary<string, string> dataValues)
            : base(principal, string.Empty, "RecordInitialDisclosureEvent.xslt", DataAccess.E_AuditItemCategoryT.DisclosureESign)
        {
            this.dataValues = dataValues;
            this.loginName = principal.LoginNm;
            this.SetDescription("Initial Disclosure Event Recorded");
        }

        /// <summary>
        /// Generates the detail xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "login", this.loginName);
            foreach (var pair in this.dataValues)
            {
                writer.WriteStartElement("field");
                this.WriteSafeAttr(writer, "name", pair.Key);
                this.WriteSafeAttr(writer, "value", pair.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
    }
}
