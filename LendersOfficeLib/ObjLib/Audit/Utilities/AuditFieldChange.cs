﻿namespace LendersOffice.Audit
{
    /// <summary>
    /// Represents a single audited field change.
    /// </summary>
    public class AuditFieldChange
    {
        /// <summary>
        /// A static string that should be substituted for a non-existent value.
        /// </summary>
        public static readonly string EmptyValue = "None";

        /// <summary>
        /// The old value of the field.
        /// </summary>
        private string oldValue;

        /// <summary>
        /// The new value of the field.
        /// </summary>
        private string newValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditFieldChange"/> class. An instance
        /// of this class represents a single field change.
        /// </summary>
        /// <param name="field">The name of the field that was changed.</param>
        /// <param name="oldValue">The old value of the field.</param>
        /// <param name="newValue">The new value of the field.</param>
        public AuditFieldChange(string field, string oldValue, string newValue)
        {
            this.Field = field;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        /// <summary>
        /// Gets the name of the field that was changed.
        /// </summary>
        /// <value>The name of the field.</value>
        public string Field { get; private set; }

        /// <summary>
        /// Gets the old value of the field. On retrieval, substitute the static
        /// <see cref="EmptyValue"/> if the string is null or empty.
        /// </summary>
        /// <value>The old value of the field.</value>
        public string OldValue
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.oldValue) ? EmptyValue : this.oldValue;
            }

            private set
            {
                this.oldValue = value;
            }
        }

        /// <summary>
        /// Gets the new value of the field. On retrieval, substitute the static
        /// <see cref="EmptyValue"/> if the string is null or empty.
        /// </summary>
        /// <value>The new value of the field.</value>
        public string NewValue
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.newValue) ? EmptyValue : this.newValue;
            }

            private set
            {
                this.newValue = value;
            }
        }
    }
}
