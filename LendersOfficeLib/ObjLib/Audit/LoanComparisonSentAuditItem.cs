﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using DataAccess;


namespace LendersOffice.Audit
{
    public sealed class LoanComparisonSentAuditItem : AbstractAuditItem
    {

        private const string ATTR_Emails = "Emails";
        private const string Element_RequestXml = "RequestXml";
        private const string Element_ResultXml = "ResultXml";
        private const string Element_LoamComparisonHtml = "LoanComparisonHtml";
        private const string ATTR_From = "From";
        private const string EL_BorrowerMsg = "BorrMsg";

        public string Emails { get; private set; }
        public string RequestXml { get; private set; }
        public string ResultXml { get; private set; }
        public string LoanComparisonHtml { get; private set; }
        public string FromAddress { get; private set; }
        public string MessageToBorrower { get; private set; }

        public LoanComparisonSentAuditItem(AbstractUserPrincipal principal, string from, string emails, string requestXml, string resultXml, string certHtml, string messageToBorrower) :
            base(principal, "Email Loan Comparison Report", "LoanComparisonSent.xslt", E_AuditItemCategoryT.Email)
        {
            Emails = emails;
            RequestXml = requestXml;
            ResultXml = resultXml;
            LoanComparisonHtml = certHtml;
            FromAddress = from;
            MessageToBorrower = messageToBorrower;
        }

        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_Emails, Emails);
            WriteSafeAttr(writer, ATTR_From, FromAddress);

            writer.WriteStartElement(EL_BorrowerMsg);
            writer.WriteCData(Tools.SantizeXmlString(MessageToBorrower));
            writer.WriteEndElement();

            writer.WriteStartElement(Element_RequestXml);
            writer.WriteCData(RequestXml);
            writer.WriteEndElement();

            writer.WriteStartElement(Element_ResultXml);
            writer.WriteCData(ResultXml);
            writer.WriteEndElement();

            writer.WriteStartElement(Element_LoamComparisonHtml);
            writer.WriteCData(LoanComparisonHtml);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
}
