/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.Audit
{

	public class PmlCertificateEmailAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_FROM_EMAIL = "from";
        private const string ATTR_TO_EMAIL = "to";
        private const string ATTR_PML_CERTIFICATE = "pml";
        private const string ATTR_SUBJECT = "subject";
        #endregion
        private string m_fromEmailAddr;
        private string m_toEmailAddr;
        private string m_pmlCertificate;
        private string m_subject;
		private string m_loginName;

        public PmlCertificateEmailAuditItem(AbstractUserPrincipal principal, string fromEmailAddr, string toEmailAddr, string subject, string pmlCertificate) :
            base (principal, "Email PML Certificate", "PmlCertificateEmail.xslt", DataAccess.E_AuditItemCategoryT.Email) 
        {
            m_fromEmailAddr = fromEmailAddr;
            m_toEmailAddr = toEmailAddr;
            m_pmlCertificate = pmlCertificate;
            m_subject = subject;
			m_loginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_FROM_EMAIL, m_fromEmailAddr);
            WriteSafeAttr(writer, ATTR_TO_EMAIL, m_toEmailAddr);
            WriteSafeAttr(writer, ATTR_SUBJECT, m_subject);
			WriteSafeAttr(writer, "login", m_loginName);
            if (null != m_pmlCertificate) 
            {
                writer.WriteStartElement(ATTR_PML_CERTIFICATE);
                writer.WriteCData(m_pmlCertificate);
                writer.WriteEndElement();
            }			
            writer.WriteEndElement(); // </data>
        }

	}
}
