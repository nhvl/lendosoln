﻿namespace LendersOffice.Audit
{
    using System.Xml;
    using DataAccess;
    using ObjLib.TPO;
    using Security;

    /// <summary>
    /// Represents an audit item for TPO re-disclosure generation requests.
    /// </summary>
    public class TpoRequestForRedisclosureGenerationAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoRequestForRedisclosureGenerationAuditItem"/> class.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit item.
        /// </param>
        /// <param name="type">
        /// The type of the request.
        /// </param>
        /// <param name="notes">
        /// The notes for the request.
        /// </param>
        public TpoRequestForRedisclosureGenerationAuditItem(AbstractUserPrincipal principal, TpoRequestForRedisclosureGenerationEventType type, string notes)
            : base(principal, GetRequestDescription(type), "TpoDisclosureRequestAuditItem.xslt", E_AuditItemCategoryT.DisclosureESign)
        {
            this.Notes = notes;
        }

        /// <summary>
        /// Gets or sets the notes for the request.
        /// </summary>
        /// <value>
        /// The notes for the request.
        /// </value>
        private string Notes { get; set; }

        /// <summary>
        /// Writes the XML for the audit item.
        /// </summary>
        /// <param name="writer">
        /// The XML writer to write data.
        /// </param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "Title", "Redisclosure");
            this.WriteSafeAttr(writer, "Notes", this.Notes);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets the description for the request.
        /// </summary>
        /// <param name="type">
        /// The type of the event.
        /// </param>
        /// <returns>
        /// The request description.
        /// </returns>
        private static string GetRequestDescription(TpoRequestForRedisclosureGenerationEventType type)
        {
            switch (type)
            {
                case TpoRequestForRedisclosureGenerationEventType.Active:
                    return "Originator portal user has requested lender to generate redisclosures.";
                case TpoRequestForRedisclosureGenerationEventType.Cancelled:
                    return "Pending request for redisclosures generation has been cancelled.";
                case TpoRequestForRedisclosureGenerationEventType.Completed:
                    return "Request for redisclosures generation has been completed.";
                case TpoRequestForRedisclosureGenerationEventType.NoRequest:
                default:
                    throw new UnhandledEnumException(type);
            }
        }
    }
}
