﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Audit;
using LendersOffice.Security;
using System.IO;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Audit
{
    public class FHATotalScoreCardAuditItem : AbstractAuditItem
    {
        private string m_sSent1003;
        private string m_sSentFhaTransmittal;
        private string m_sCertXml;
        private string m_sFhaCaseNumber;
        private string m_sLoginName;
        private bool m_isH4H;


        public FHATotalScoreCardAuditItem(AbstractUserPrincipal principal, bool sent1003, bool sentFhaTransmittal, string certXml, string fhaCaseNumber) :
            base(principal, "Submit to FHA TOTAL Scorecard", "SubmitToFHATotalScoreCard.xslt", DataAccess.E_AuditItemCategoryT.Integration) 
        {
            init(sent1003, sentFhaTransmittal, certXml, fhaCaseNumber, principal.LoginNm, false);
        }


        public FHATotalScoreCardAuditItem(AbstractUserPrincipal principal, string certXml, string fhaCaseNumber) :
            base(principal, "Submit to FHA TOTAL Scorecard", "SubmitToFHATotalScoreCard.xslt", DataAccess.E_AuditItemCategoryT.Integration)
        {
            init(false, true, certXml, fhaCaseNumber, principal.LoginNm, true);
        }

        private void init(bool sent1003, bool sentFhaTransmittal, string sCertXml, string sFhaCaseNumber, string sLoginName, bool ish4h)
        {
            m_sSent1003 = sent1003 ? "Yes" : "No";
            m_sSentFhaTransmittal = sentFhaTransmittal ? "Yes" : "No";
            m_sCertXml = sCertXml;
            m_sFhaCaseNumber = sFhaCaseNumber;
            m_sLoginName = sLoginName;
            m_isH4H = ish4h;
        }


        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            string certHtml; 
            string stylesheet = m_isH4H ? "LendersOffice.Integration.TotalScorecard.H4HFindings.xslt.config" : "LendersOffice.Integration.TotalScorecard.Findings.xslt.config";
            using (MemoryStream ms = new MemoryStream())
            {
                System.Xml.Xsl.XsltArgumentList args = new System.Xml.Xsl.XsltArgumentList();
                args.AddParam("currentYear", "", DateTime.Now.Year);
                XslTransformHelper.TransformFromEmbeddedResource(stylesheet, m_sCertXml, ms, args);
                ms.Position = 0;
                using (StreamReader reader = new StreamReader(ms))
                {
                    certHtml = reader.ReadToEnd();
                }
            }

            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "login", m_sLoginName);
            WriteSafeAttr(writer, "Sent1003", m_sSent1003);
            WriteSafeAttr(writer, "SentFHATransmittal", m_sSentFhaTransmittal);
            WriteSafeAttr(writer, "fhaCaseNumber", m_sFhaCaseNumber);
            writer.WriteStartElement("totalCertificate");
            writer.WriteCData(certHtml);
            writer.WriteEndElement();
            writer.WriteEndElement();
         
        }
    }
}
