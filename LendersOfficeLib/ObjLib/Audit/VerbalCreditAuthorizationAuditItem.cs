﻿namespace LendersOffice.Audit
{
    using System.Xml;
    using LendersOffice.Security;

    /// <summary>
    /// Provides an audit for verbal credit authorization.
    /// </summary>
    public class VerbalCreditAuthorizationAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Gets the resource name of the XSLT file for the audit item.
        /// </summary>
        public const string XsltResourceName = "LendersOffice.ObjLib.Audit.xslt.VerbalCreditAuthorizationAuditItem.xslt";

        /// <summary>
        /// Initializes a new instance of the <see cref="VerbalCreditAuthorizationAuditItem"/> class.
        /// </summary>
        /// <param name="user">
        /// The user saving the verbal authorization data.
        /// </param>
        /// <param name="date">
        /// The verbal authorization date.
        /// </param>
        /// <param name="borrowerName">
        /// The name of the borrower verbally authorizing credit.
        /// </param>
        /// <param name="coborrowerName">
        /// The name of the co-borrower verbally authorizing credit.
        /// </param>
        public VerbalCreditAuthorizationAuditItem(AbstractUserPrincipal user, string date, string borrowerName, string coborrowerName)
            : base(user, "Verbal Credit Report Authorization", "VerbalCreditAuthorizationAuditItem.xslt", DataAccess.E_AuditItemCategoryT.CreditReport)
        {
            this.AuthorizationDate = date;
            this.BorrowerName = borrowerName;
            this.CoborrowerName = coborrowerName;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the "Close" button
        /// should be rendered for the audit item.
        /// </summary>
        public bool RenderCloseButton { get; set; } = true;

        /// <summary>
        /// Gets or sets the name of the lender for the audit item.
        /// </summary>
        public string LenderName { get; set; }

        /// <summary>
        /// Gets or sets the authorization date for the audit item.
        /// </summary>
        private string AuthorizationDate { get; set; }

        /// <summary>
        /// Gets or sets the borrower name for the audit item.
        /// </summary>
        private string BorrowerName { get; set; }

        /// <summary>
        /// Gets or sets the co-borrower name for the audit item.
        /// </summary>
        private string CoborrowerName { get; set; }

        /// <summary>
        /// Generates detail XML for the audit item.
        /// </summary>
        /// <param name="writer">
        /// The writer to write XML.
        /// </param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "AuthorizationDate", this.AuthorizationDate);
            this.WriteSafeAttr(writer, "BorrowerName", this.BorrowerName);
            this.WriteSafeAttr(writer, "CoborrowerName", this.CoborrowerName);
            this.WriteSafeAttr(writer, "RenderCloseButton", this.RenderCloseButton);
            this.WriteSafeAttr(writer, "LenderName", this.LenderName);
            writer.WriteEndElement();
        }
    }
}
