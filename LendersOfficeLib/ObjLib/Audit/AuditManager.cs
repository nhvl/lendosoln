namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;

    public class AuditManager
    {
        private const int MINIMUM_SIZE_FOR_FILEDB = 100000;

        /// <summary>
        /// Records a list of audit events into loan.
        /// </summary>
        /// <param name="sLId">sLId of the loan.</param>
        /// <param name="items">List of audit items.</param>
        public static void RecordAudit(Guid sLId, params AbstractAuditItem[] items) 
        {
            if (items.Length == 0)
            {
                //nothing to do
                return; 
            }

            // 9/25/2014 dd - Attempt to retrieve the broker id.
            // If none of the audit item contains broker id then look up broker id from sLId.

            Guid brokerId = Guid.Empty;
            foreach (AbstractAuditItem item in items)
            {
                if (item.BrokerId != Guid.Empty)
                {
                    brokerId = item.BrokerId;
                    break;
                }
            }

            if (brokerId == Guid.Empty)
            {
                DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            }

            if(Tools.GetAuditHistoryMigrated(sLId, brokerId))
            {
                RecordAuditVer2_Unsafe(brokerId, sLId, items);
                return;
            }
            else
            {
                try
                {
                    StringBuilder sb = new StringBuilder(10000);

                    using (XmlWriter writer = new XmlTextWriter(new StringWriter(sb)))
                    {
                        foreach (AbstractAuditItem item in items)
                        {
                            item.GenerateXml(writer);
                        }
                    }

                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@LoanId", sLId));

                    string auditTrailXmlContent = "";
                    string filedbKey = "";

                    if (sb.Length > MINIMUM_SIZE_FOR_FILEDB)
                    {
                        CPmlFIdGenerator generator = new CPmlFIdGenerator();
                        filedbKey = "audit_tmp_" + generator.GenerateNewFriendlyId();
                        FileDBTools.WriteData(E_FileDB.Normal, filedbKey, sb.ToString());
                    }
                    else
                    {
                        auditTrailXmlContent = sb.ToString();
                    }

                    SqlParameter parm = new SqlParameter("@AuditTrailXmlContent", SqlDbType.Text);
                    parm.Value = auditTrailXmlContent;
                    parameters.Add(parm);
                    parameters.Add(new SqlParameter("@FileDBKey", filedbKey));

                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "InsertLoanAudit", 5, parameters);
                }
                catch (Exception exc)
                {
                    Tools.LogErrorWithCriticalTracking(exc);
                }
            }
        }

        // Note: The caller should know about Loan_File_A.sIsAuditHistoryMigrated's status when calling this method.
        // With migration process, require sIsAuditHistoryMigrated = false when calling this method.
        // Otherwise, require sIsAuditHistoryMigrated = true.
        private static void RecordAuditVer2_Unsafe(Guid brokerId, Guid sLId, AbstractAuditItem[] items)
        {
            StringBuilder sb = new StringBuilder(10000);

            foreach (AbstractAuditItem item in items)
            {
                string detailContent = string.Empty;

                if(item is AuditItemForExport)
                {
                    detailContent = (item as AuditItemForExport).XmlContent; // will remove soon.
                }
                else
                {
                    sb.Length = 0;
                    using (XmlWriter writer = new XmlTextWriter(new StringWriter(sb)))
                    {
                        item.GenerateXml(writer);
                    }

                    detailContent = sb.ToString();
                }

                string filedbKey = item.ID.ToString();
                FileDBTools.WriteData(E_FileDB.Normal, filedbKey, detailContent);
                string classType = string.IsNullOrEmpty(item.AuditType) ? item.GetType().FullName : item.AuditType;

                SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@LoanId", sLId),
                        new SqlParameter("@Category", item.CategoryT),
                        new SqlParameter("@ClassType", classType),
                        new SqlParameter("@EventDescription", item.Description),

                        new SqlParameter("@UserName", item.UserName),
                        new SqlParameter("@HasDetails", item.HasDetails ),
                        new SqlParameter("@ReadPermissionsREquiredJsonContent", SerializationHelper.JsonNetSerialize(item.ReadPermissionsRequired.ToList())),
                        new SqlParameter("@DetailContentKey", filedbKey),
                        new SqlParameter("@EventDate", item.Timestamp),
                    };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "LOAN_AUDIT_TRAIL_Insert", 3, parameters);
            }
        }

        public static IEnumerable<LightWeightAuditItem> RetrieveAuditList(Guid sLId)
        {
            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal;

            try
            {
                Guid brokerId;
                DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
                if (!Tools.GetAuditHistoryMigrated(sLId, brokerId))
                {
                    return RetrieveAuditListImpl(sLId);
                }
                else
                {
                    return RetrieveAuditListImplVer2(sLId, brokerId);
                }
            }
            catch (XmlException e)
            {
                if (false == e.Message.Contains("0x00"))
                {
                    throw;
                }

                #region fixed for 0x00
                string fixedPath = TempFileUtils.NewTempFilePath();

                lock (s_lock)
                {
                    string path = FileDBTools.CreateCopy(E_FileDB.Normal, "audit_" + sLId);

                    Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream oldFileStream)
                    {
                        Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream newFileStream)
                        {
                            int newFileWriteIndex = 0;
                            int blockSize = 8 * 1024;
                            byte[] existingFileBuffer = new byte[blockSize], newFileBuffer = new byte[blockSize];

                            while (true)
                            {
                                int readCount = oldFileStream.Stream.Read(existingFileBuffer, 0, existingFileBuffer.Length);
                                if (readCount <= 0)
                                {
                                    if (newFileWriteIndex != 0)
                                    {
                                        newFileStream.Stream.Write(newFileBuffer, 0, newFileWriteIndex);
                                    }
                                    break;
                                }

                                for (int i = 0; i < readCount; i++)
                                {
                                    if (existingFileBuffer[i] == 0x00)
                                    {
                                        continue;
                                    }

                                    newFileBuffer[newFileWriteIndex] = existingFileBuffer[i];
                                    if (newFileWriteIndex == blockSize - 1)
                                    {
                                        newFileStream.Stream.Write(newFileBuffer, 0, newFileBuffer.Length);
                                        newFileWriteIndex = 0;
                                        continue;
                                    }
                                    newFileWriteIndex++;
                                }
                            }
                        };

                        BinaryFileHelper.OpenNew(fixedPath, writeHandler);
                    };

                    BinaryFileHelper.OpenRead(path, readHandler);

                    Tools.LogWarning(String.Format("[AuditFix] Created backup audit for loan {0} in temp filedb.", sLId ));
                    FileDBTools.WriteFile(E_FileDB.Temp, "backup_audit_" + sLId, path);
                    FileDBTools.WriteFile(E_FileDB.Normal, "audit_" + sLId, fixedPath);

                }

                List<LightWeightAuditItem> auditEntries = new List<LightWeightAuditItem>();
                using (FileStream fixedFs = new FileStream(fixedPath, FileMode.Open, FileAccess.Read))
                using (XmlReader fixedReader = new XmlTextReader(fixedFs, XmlNodeType.Element, null))
                {
                    if (fixedReader.Read())
                    {
                        while (fixedReader.Name == AbstractAuditItem.ELEMENT_AUDIT_ITEM && fixedReader.NodeType == XmlNodeType.Element)
                        {
                            LightWeightAuditItem item = new LightWeightAuditItem(fixedReader);

                            if (item.ReadPermissionsRequired.Count() > 0 && false == item.ReadPermissionsRequired.Any(perm => p.HasPermission(perm)))
                            {
                                continue;
                            }
                            auditEntries.Add(item);
                        }
                    }
                }
                return auditEntries;
                #endregion
            }
        }

        /// <summary>
        /// Generate the audit list in CSV format for a given loan.
        /// </summary>
        /// <param name="loanIdentifier">A unique loan identifier.</param>
        /// <param name="principal">The user exporting the audit list.</param>
        /// <returns>The loan's audit list as a CSV-formatted string.</returns>
        public static string CreateAuditListCSV(Guid loanIdentifier, AbstractUserPrincipal principal)
        {
            var auditList = RetrieveAuditList(loanIdentifier);
            var csv = new StringBuilder();
            Func<string, string> sanitize = s => "\"" + s.Replace("\"", "\"\"") + "\"";

            csv.Append("Timestamp,UserName,Category,Description");            
            foreach (var auditItem in auditList)
            {
                if (auditItem.ReadPermissionsRequired.Any(p => !principal.HasPermission(p)))
                {
                    continue;
                }

                csv.Append(Environment.NewLine);
                csv.Append(sanitize(auditItem.TimestampDescription));
                csv.Append(",");
                csv.Append(sanitize(auditItem.UserName));
                csv.Append(",");
                csv.Append(sanitize(auditItem.Category));
                csv.Append(",");
                csv.Append(sanitize(auditItem.Description));
            }

            return csv.ToString();
        }

        public static string RetrieveAuditRaw(Guid sLId)
        {
            MoveToPermanentStorage(sLId);
            var ret = FileDBTools.ReadData(E_FileDB.Normal, "audit_" + sLId);
            Encoding temp = Encoding.UTF8;

            return temp.GetString(ret);
        }

        private static IEnumerable<LightWeightAuditItem> RetrieveAuditListImpl(Guid sLId)
        {
            MoveToPermanentStorage(sLId);

            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal;
            List<LightWeightAuditItem> auditEntries = new List<LightWeightAuditItem>();

            var key = "audit_" + sLId;

            Action<Stream> readHandler = delegate (Stream stream)
            {
                using (XmlReader xmlReader = new XmlTextReader(stream, XmlNodeType.Element, null))
                {
                    if (xmlReader.Read())
                    {
                        while (xmlReader.Name == AbstractAuditItem.ELEMENT_AUDIT_ITEM && xmlReader.NodeType == XmlNodeType.Element)
                        {
                            LightWeightAuditItem item = new LightWeightAuditItem(xmlReader);

                            if (item.ReadPermissionsRequired.Count() > 0 && false == item.ReadPermissionsRequired.Any(perm => p.HasPermission(perm)))
                            {
                                continue;
                            }
                            auditEntries.Add(item);
                        }
                    }
                }
            };

            try
            {
                FileDBTools.ReadData(E_FileDB.Normal, key, readHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP - Audit is not available yet.
            }

            return auditEntries;
        }

        private static IEnumerable<LightWeightAuditItem> RetrieveAuditListImplVer2(Guid sLId, Guid brokerId)
        {
            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal;
            List<LightWeightAuditItem> auditEntries = new List<LightWeightAuditItem>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoanId", sLId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "LOAN_AUDIT_TRAIL_ListByLoanId", parameters))
            {
                while (reader.Read())
                {
                    LightWeightAuditItem item = new LightWeightAuditItem(reader);

                    if (item.ReadPermissionsRequired.Count() > 0 && false == item.ReadPermissionsRequired.Any(perm => p.HasPermission(perm)))
                    {
                        continue;
                    }

                    auditEntries.Add(item);
                }
            }

            return auditEntries;
        }

        public static bool RenderAuditDetail(Guid sLId, Guid auditId, Stream outputStream) 
        {
            bool isValid = false;

            string _auditId = auditId.ToString("N");

            Action<Stream> readHandler = delegate (Stream stream)
            {
                using (XmlReader xmlReader = new XmlTextReader(stream, XmlNodeType.Element, null))
                {
                    if (xmlReader.Read())
                    {
                        while (xmlReader.Name == AbstractAuditItem.ELEMENT_AUDIT_ITEM && xmlReader.NodeType == XmlNodeType.Element)
                        {
                            if (_auditId == xmlReader.GetAttribute(AbstractAuditItem.ATTR_ID))
                            {
                                string xsltFile = xmlReader.GetAttribute(AbstractAuditItem.ATTR_XSLT_FILE);
                                DateTime dt = new DateTime(long.Parse(xmlReader.GetAttribute(AbstractAuditItem.ATTR_TIME_STAMP)));
                                Transform(xmlReader.ReadOuterXml(), xsltFile, outputStream, dt);
                                isValid = true;
                                break;
                            }
                            else
                            {
                                // Consume the body
                                xmlReader.MoveToElement();
                                xmlReader.ReadInnerXml();
                            }
                        }
                    }
                }
            };

            var key = "audit_" + sLId;

            try
            {
                FileDBTools.ReadData(E_FileDB.Normal, key, readHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP - Audit is not available yet.
            }

            return isValid;

        }

        public static bool RenderAuditDetailVer2(Guid brokerId, Guid sLId, Guid auditId, Stream outputStream) // new audit format
        {
            return RenderAuditDetail(brokerId, sLId, auditId, 0 /*auditSeq*/, outputStream);
        }

        public static bool RenderAuditDetailVer2(Guid brokerId, Guid sLId, long auditSeq, Stream outputStream)
        {
            return RenderAuditDetail(brokerId, sLId, Guid.Empty /*auditId*/, auditSeq, outputStream);
        }

        private static bool RenderAuditDetail(Guid brokerId, Guid sLId, Guid auditId, long auditSeq, Stream outputStream)
        {
            SqlParameter[] parameters = null;
            string storedProcedure = string.Empty;

            if(auditSeq != 0)
            {
                parameters = new SqlParameter[] {
                    new SqlParameter("@Id", auditSeq),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@LoanId", sLId)
                };

                storedProcedure = "LOAN_AUDIT_TRAIL_GetDetailContent";
            }
            else
            {
                parameters = new SqlParameter[] {
                    new SqlParameter("@DetailContentKey", auditId.ToString()),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@LoanId", sLId)
                };

                storedProcedure = "[LOAN_AUDIT_TRAIL_GetDetailContent_ByDetailContentKey]";
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, storedProcedure, parameters))
            {
                if (reader.Read())
                {
                    string key = (string)reader["DetailContentKey"];
                    var content = FileDBTools.ReadDataText(E_FileDB.Normal, key);

                    using (XmlReader xmlReader = new XmlTextReader(content, XmlNodeType.Element, null))
                    {
                        if (xmlReader.Read())
                        {
                            string xsltFile = xmlReader.GetAttribute(AbstractAuditItem.ATTR_XSLT_FILE);
                            DateTime dt = new DateTime(long.Parse(xmlReader.GetAttribute(AbstractAuditItem.ATTR_TIME_STAMP)));
                            Transform(xmlReader.ReadOuterXml(), xsltFile, outputStream, dt);
                        }
                        return true;
                    }
                }
            }

            return false;
        }

        private static void Transform(string xml, string xsltFile, Stream outputStream, DateTime dt) 
        {
            HttpContext currentContext = HttpContext.Current;

            if (null == currentContext)
                return;
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", DataAccess.Tools.VRoot);
            args.AddParam("Timestamp", "", Tools.GetDateTimeDescription(dt));
            XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Audit.xslt." + xsltFile, xml, outputStream, args, Encoding.UTF8 );

        }


        public static void MoveToPermanentStorage(Guid sLId) 
        {
            string key = "audit_" + sLId;

            Action<FileInfo> existingFileHandler = delegate (FileInfo fi)
            {
                WriteAuditToPermanentStorage(fi.FullName, key, sLId);
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Normal, key, existingFileHandler);
            }
            catch (FileNotFoundException)
            {
                string tempPath = TempFileUtils.NewTempFilePath();
                WriteAuditToPermanentStorage(tempPath, key, sLId);

                try
                {
                    // Clean up temp file.
                    FileOperationHelper.Delete(tempPath);
                }
                catch (IOException)
                {
                    // NO-OP.
                }
            }
        }

        private static object s_lock = new object();
        private static void WriteAuditToPermanentStorage(string filePath, string key, Guid sLId) 
        {
            lock(s_lock) 
            {
                Guid brokerId = Guid.Empty;
                DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

                DateTime dt = DateTime.MinValue;

                bool hasNewAudit = false;

                using (FileStream stream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None)) 
                {
                    SqlParameter[] parameters = { new SqlParameter("@LoanId", sLId) };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListAuditTrailByLoanId", parameters)) 
                    {
                        while (reader.Read()) 
                        {
                            hasNewAudit = true;
                            string str = (string) reader["AuditTrailXmlContent"];

                            string fileDbKey = "";
                            if (! (reader["FileDBKey"] is DBNull)) 
                            {
                                fileDbKey = (string) reader["FileDBKey"];
                            }


                            DateTime logStartTime = (DateTime) reader["LogStartTime"];

                            if (logStartTime.CompareTo(dt) > 0)
                                dt = logStartTime;

                            if (str == "" && fileDbKey != "")
                            {
                                str = FileDBTools.ReadDataText(E_FileDB.Normal, fileDbKey);  
                            }
                            else if (str == "" && fileDbKey == "")
                            {
                                Tools.LogBug("A record in AUDIT_TRAIL_DAILY with empty AuditTrailXmlContent and FileDBKey");
                            }
                         
                            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(str);
                            stream.Write(buffer, 0, buffer.Length);
                        }
                    }
                }


                if (hasNewAudit) 
                {
                    // 4/11/2007 dd - To prevent temporary issue with FileDB, I'll try to execute this 3 times before throw exception to user.
                    int numOfTries = 0;
                    while (true) {
                        try 
                        {
                            FileDBTools.WriteFile(E_FileDB.Normal, key, filePath);
                        } 
                        catch 
                        {
                            if (numOfTries < 3) 
                            {
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1000);
                                numOfTries++;
                                continue;
                            } 
                            else 
                            {
                                throw;
                            }

                        }
                        break;
                    }

                    if (dt != DateTime.MinValue) 
                    {
                        // Delete audit trail in AUDIT_TRAIL_DAILY table.
                        SqlParameter[] parameters = new SqlParameter[] {
                                                                           new SqlParameter("@LoanId", sLId),
                                                                           new SqlParameter("@LastLogTime", dt)
                                                                       };
                        StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeleteProcessedAuditTrail", 0, parameters);
                    }
                }
            }
            
        }


        //// This following migration code will remove soon. 
        //// Based on David suggestion, we don't worry about duplicate code.
        public static void MigrationHistoryAudit(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: MigrationHistoryAudit inputFileName");
                return;
            }

            Guid brokerId, loanId;
            foreach (string line in File.ReadAllLines(args[1]))
            {
                string[] tokens = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length == 0)
                {
                    continue;
                }

                if (tokens.Length != 2 || !Guid.TryParse(tokens[0].Trim(), out brokerId) || !Guid.TryParse(tokens[1].Trim(), out loanId))
                {
                    Console.WriteLine($"Ignore line: '{line}'");
                    continue;
                }

                MigrationExecute(brokerId, loanId);
            }
        }

        private static void MigrationExecute(Guid brokerId, Guid sLId)
        {
            Guid expectedBrokerId;
            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out expectedBrokerId);
            if (expectedBrokerId != brokerId || Tools.GetAuditHistoryMigrated(sLId, brokerId))
            {
                return;
            }

            try
            {
                var items = RetrieveAuditListForMigration(sLId).ToArray<AbstractAuditItem>();

                SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@LoanId", sLId),
                        new SqlParameter("@BrokerId", brokerId),
                };

                if ( items.Length > 0)
                {
                    // remove previous audit records if re-migration
                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "LOAN_AUDIT_TRAIL_Delete_ByLoanId", 3, parameters);
                }

                RecordAuditVer2_Unsafe(brokerId, sLId, items);

                parameters = new SqlParameter[]
                {
                    new SqlParameter("@sLId", sLId),
                    new SqlParameter("@sIsAuditHistoryMigrated", true)
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateAuditHistoryMigratedByLoanId", 3, parameters);
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw;
            }
        }

        private static IEnumerable<AuditItemForExport> RetrieveAuditListForMigration(Guid sLId)
        {
            try
            {
                return RetrieveAuditListImplForMigration(sLId);
            }
            catch (XmlException e)
            {
                if (false == e.Message.Contains("0x00"))
                {
                    throw;
                }

                #region fixed for 0x00
                string fixedPath = TempFileUtils.NewTempFilePath();

                lock (s_lock)
                {
                    string path = FileDBTools.CreateCopy(E_FileDB.Normal, "audit_" + sLId);

                    Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream oldFileStream)
                    {
                        Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream newFileStream)
                        {
                            int newFileWriteIndex = 0;
                            int blockSize = 8 * 1024;
                            byte[] existingFileBuffer = new byte[blockSize], newFileBuffer = new byte[blockSize];

                            while (true)
                            {
                                int readCount = oldFileStream.Stream.Read(existingFileBuffer, 0, existingFileBuffer.Length);
                                if (readCount <= 0)
                                {
                                    if (newFileWriteIndex != 0)
                                    {
                                        newFileStream.Stream.Write(newFileBuffer, 0, newFileWriteIndex);
                                    }
                                    break;
                                }

                                for (int i = 0; i < readCount; i++)
                                {
                                    if (existingFileBuffer[i] == 0x00)
                                    {
                                        continue;
                                    }

                                    newFileBuffer[newFileWriteIndex] = existingFileBuffer[i];
                                    if (newFileWriteIndex == blockSize - 1)
                                    {
                                        newFileStream.Stream.Write(newFileBuffer, 0, newFileBuffer.Length);
                                        newFileWriteIndex = 0;
                                        continue;
                                    }
                                    newFileWriteIndex++;
                                }
                            }
                        };

                        BinaryFileHelper.OpenNew(fixedPath, writeHandler);
                    };

                    BinaryFileHelper.OpenRead(path, readHandler);

                    Tools.LogWarning(String.Format("[AuditFix] Created backup audit for loan {0} in temp filedb.", sLId));
                    FileDBTools.WriteFile(E_FileDB.Temp, "backup_audit_" + sLId, path);
                    FileDBTools.WriteFile(E_FileDB.Normal, "audit_" + sLId, fixedPath);

                }

                List<AuditItemForExport> auditEntries = new List<AuditItemForExport>();
                using (FileStream fixedFs = new FileStream(fixedPath, FileMode.Open, FileAccess.Read))
                using (XmlReader fixedReader = new XmlTextReader(fixedFs, XmlNodeType.Element, null))
                {
                    if (fixedReader.Read())
                    {
                        while (fixedReader.Name == AbstractAuditItem.ELEMENT_AUDIT_ITEM && fixedReader.NodeType == XmlNodeType.Element)
                        {
                            AuditItemForExport item = new AuditItemForExport(fixedReader);
                            auditEntries.Add(item);
                        }
                    }
                }
                return auditEntries;
                #endregion
            }
        }

        private static IEnumerable<AuditItemForExport> RetrieveAuditListImplForMigration(Guid sLId)
        {
            MoveToPermanentStorage(sLId);

            List<AuditItemForExport> auditEntries = new List<AuditItemForExport>();

            var key = "audit_" + sLId;

            Action<Stream> readHandler = delegate (Stream stream)
            {
                using (XmlReader xmlReader = new XmlTextReader(stream, XmlNodeType.Element, null))
                {
                    if (xmlReader.Read())
                    {
                        while (xmlReader.Name == AbstractAuditItem.ELEMENT_AUDIT_ITEM && xmlReader.NodeType == XmlNodeType.Element)
                        {
                            AuditItemForExport item = new AuditItemForExport(xmlReader);
                            auditEntries.Add(item);
                        }
                    }
                }
            };

            try
            {
                FileDBTools.ReadData(E_FileDB.Normal, key, readHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP - Audit is not available yet.
            }

            return auditEntries;
        }

        private class AuditItemForExport : AbstractAuditItem
        {
            public string XmlContent { get; private set; }

            public AuditItemForExport(XmlReader reader) : base(reader)
            {
            }

            protected override void ParseBodyContent(XmlReader reader)
            {
                // Consume the whole item
                reader.MoveToElement();
                this.XmlContent = reader.ReadOuterXml();
            }

            protected override void GenerateDetailXml(XmlWriter writer)
            {
            }
        }
    }
}
