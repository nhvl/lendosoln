﻿// <summary>
//    Author: Eric Mallare
//    Date:   12/15/2015 
// </summary>
namespace LendersOffice.Audit
{
    using System;
    using System.Xml;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Audits when a lead has been converted to a loan.
    /// </summary>
    public class LoanCreationFromLeadAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// The login name to record.
        /// </summary>
        private string loginName;

        /// <summary>
        /// The old lead number.
        /// </summary>
        private string oldLeadNum;

        /// <summary>
        /// The new loan number.
        /// </summary>
        private string newLoanNum;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCreationFromLeadAuditItem" /> class.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="oldNum">The number of the old lead.</param>
        /// <param name="newNum">The number of the new loan.</param>
        public LoanCreationFromLeadAuditItem(AbstractUserPrincipal principal, string oldNum, string newNum) :
            base(principal, "Loan Creation from Lead", "LoanCreationFromLead.xslt", DataAccess.E_AuditItemCategoryT.LoanStatus)
        {
            this.loginName = principal.LoginNm;
            this.oldLeadNum = oldNum;
            this.newLoanNum = newNum;
        }

        /// <summary>
        /// Generates the xml to use for the details popup.
        /// </summary>
        /// <param name="writer">The xml writer to use.</param>
        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");

            this.WriteSafeAttr(writer, "login", this.loginName);
            this.WriteSafeAttr(writer, "oldName", this.oldLeadNum);
            this.WriteSafeAttr(writer, "newName", this.newLoanNum);

            writer.WriteEndElement(); // </data>
        }
    }
}
