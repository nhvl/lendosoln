namespace LendersOffice.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Security;
    using Common;
    using System.Data.Common;

    public class LightWeightAuditItem : AbstractAuditItem
    {
        string m_fieldId = string.Empty;
        string m_newValue = string.Empty;

        public string FieldId
        {   get
            {
                LoadDetailContentIfNeed();
                return m_fieldId;
            }
        }
        public string NewValue
        {
            get
            {
                LoadDetailContentIfNeed();
                return m_newValue;
            }
        }

        public LightWeightAuditItem(XmlReader reader) : base(reader)
        {
        }

        public LightWeightAuditItem(DbDataReader reader) : base(reader)
        {
        }


        protected override void ParseBodyContent(XmlReader reader) 
        {
            // Consume the body
            reader.MoveToElement();
            var bodyXmlString = reader.ReadInnerXml();

            // Retrieve the id of the changed field.
            if (this.CategoryT == E_AuditItemCategoryT.FieldChange || this.CategoryT == E_AuditItemCategoryT.LoanStatus)
            {
                var doc = Tools.CreateXmlDoc(bodyXmlString);

                var dataNodes = doc.GetElementsByTagName("data");
                if (dataNodes.Count > 0)
                {
                    var dataNode = dataNodes.Item(0);
                    this.m_fieldId = dataNode.Attributes.GetNamedItem("FieldId")?.Value;
                    this.m_newValue = dataNode.Attributes.GetNamedItem(TrackedFieldModifiedAuditItem.ATTR_NEW_VALUE)?.Value;
                }
            }
        }

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
        }
    }

    public abstract class AbstractAuditItem
	{
        public const string ATTR_ID = "id";
        public const string ATTR_TIME_STAMP = "ts";
        public const string ATTR_DESCRIPTION = "desc";
        public const string ATTR_USER_ID = "uid";
        public const string ATTR_USER_NAME = "uname";
        public const string ATTR_CLASS_TYPE = "type";
        public const string ATTR_XSLT_FILE = "xslt";
        public const string ATTR_PERMISSIONS = "perm";
        public const string ATTR_DETAILS = "hasDetails";
        private const string ATTR_PERMISSION_VERSION = "permVersion";
        private const string ATTR_CATEGORY = "category";


        public const string ELEMENT_BODY = "body";
        public const string ELEMENT_AUDIT_ITEM = "item";


        private Guid m_id;
        private DateTime m_timestamp;
        private string m_description;
        private Guid m_userID;
        private string m_userName;
        private string m_xsltFile;
        private bool m_renderDetailsLink = true;
        private string m_PermissionVersion;
        private string m_classType;
        private E_AuditItemCategoryT m_categoryT;
        private string detailContentKey = string.Empty; // fileDB
        private bool m_detailLoaded = true;

        private List<Permission> m_requiredReadPermission = new List<Permission> { };

        public bool HasDetails
        {
            get { return m_renderDetailsLink; }
            protected set { m_renderDetailsLink = value; }
        }

        public IEnumerable<Permission> ReadPermissionsRequired
        {
            get
            {
                return m_requiredReadPermission;
            }
        }

        protected void SetPermissions(IEnumerable<Permission> permissions)
        {
            m_requiredReadPermission = permissions.ToList();
            m_PermissionVersion = "2";  //if you change this you need to handle the version you are changing from in the read xml function
        }

        public string Category
        {
            get 
            { 
                switch (m_categoryT)
                {
                    case E_AuditItemCategoryT.CreditReport:
                        return "Credit Report";
                    case E_AuditItemCategoryT.DisclosureESign:
                        return "Disclosure/E-Sign";
                    case E_AuditItemCategoryT.Email:
                        return "Email";
                    case E_AuditItemCategoryT.FieldChange:
                        return "Field Change";
                    case E_AuditItemCategoryT.Integration:
                        return "Integration";
                    case E_AuditItemCategoryT.LoanStatus:
                        return "Loan Status";
                    case E_AuditItemCategoryT.RateLock:
                        return "Rate Lock";
                    case E_AuditItemCategoryT.RoleAssignment:
                        return "Role Assignment";
                    case E_AuditItemCategoryT.TrailingDocument:
                        return "Trailing Document";
                    case E_AuditItemCategoryT.GFEArchived:
                        return "GFE Archived";
                    case E_AuditItemCategoryT.LoanEstimateArchived:
                        return "Loan Estimate Archived";
                    case E_AuditItemCategoryT.ClosingDisclosureArchived:
                        return "Closing Disclosure Archived";
                    case E_AuditItemCategoryT.FeeService:
                        return "Fee Service";
                    case E_AuditItemCategoryT.AgentContactRecordChange:
                        return "Agent Contact Record Change";
                    default: throw new UnhandledEnumException(m_categoryT);
                }
            }
        }

        public E_AuditItemCategoryT CategoryT
        {
            get { return m_categoryT; }
        }

        public DateTime Timestamp 
        {
            get { return m_timestamp; }
        }

        public string TimestampDescription 
        {
            get { return Tools.GetDateTimeDescription(m_timestamp); }
        }

        public string Description 
        {
            get { return m_description; }
        }
        public Guid UserID 
        {
            get
            {
                LoadDetailContentIfNeed();
                return m_userID;
            }
        }

        public Guid BrokerId { get; private set; }
        public Guid ID 
        {
            get { return m_id; }
        }
        public string UserName 
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        public long AuditSeq { get; private set; } = 0; // autdit id:  old version uses Guid ID, new version uses AuditSeq
        public string AuditType => this.m_classType;

        public string AuditClassType 
        {
            get { return this.GetType().FullName; }
        }
        public string XsltFile 
        {
            get
            {
                LoadDetailContentIfNeed();
                return m_xsltFile;
            }
        }

        protected AbstractAuditItem(Guid userId, string displayName, string description, E_AuditItemCategoryT category)
        {
            Init(userId, displayName, description, category);
        }
        protected AbstractAuditItem(string description, string xsltFile, E_AuditItemCategoryT category)
        {
            Initialize(description, xsltFile, category);
        }
        protected AbstractAuditItem(AbstractUserPrincipal principal, string description, string xsltFile, E_AuditItemCategoryT category) 
        {
            Initialize(principal, description, xsltFile, category);
        }

        protected void SetDescription(string description) 
        {
            m_description = description;
        }

        protected void SetCategory(E_AuditItemCategoryT category)
        {
            m_categoryT = category;
        }

        protected void SetXsltFile(string xsltFile)
        {
            this.m_xsltFile = xsltFile;
        }

        protected AbstractAuditItem(XmlReader reader) 
        {
            try 
            {
                m_timestamp = new DateTime(long.Parse(reader.GetAttribute(ATTR_TIME_STAMP)));
                m_id = new Guid(reader.GetAttribute(ATTR_ID));
                m_description = reader.GetAttribute(ATTR_DESCRIPTION);
                m_userID = new Guid(reader.GetAttribute(ATTR_USER_ID));
                m_userName = reader.GetAttribute(ATTR_USER_NAME);
                m_xsltFile = reader.GetAttribute(ATTR_XSLT_FILE);
                string permissions = reader.GetAttribute(ATTR_PERMISSIONS);
                m_PermissionVersion = reader.GetAttribute(ATTR_PERMISSION_VERSION);
                m_classType = reader.GetAttribute(ATTR_CLASS_TYPE);
                string category = reader.GetAttribute(ATTR_CATEGORY);

                // If the category is blank, then this is an older audit
                // item and we need to parse it from the class/description.
                if (string.IsNullOrEmpty(category))
                {
                    m_categoryT = GetCategoryFromClassAndDescription(m_classType, m_description);
                    if (m_categoryT == E_AuditItemCategoryT.FieldChange)
                    {
                        m_description = m_description.Replace("Field Change: ", "");
                        m_description = m_description.Replace("Automatic", "Automatic:");
                    }
                }
                else
                {
                    m_categoryT = (E_AuditItemCategoryT)Enum.Parse(typeof(E_AuditItemCategoryT), category);
                }

                if (false == string.IsNullOrEmpty(permissions))
                {
                    m_requiredReadPermission = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Permission>>(permissions);
                }
                
                //av 4/3/2012 adding this because of opm  81569 
                // i mistakenly got the bits confused in the sp so in order to fix the xml from the hotfix forward but not have to migrate the existing stuff
                // i'm adding a version to new audit events  if there is no version that means its old and we need to do a swap  this might be a problem if 
                //the permissions change /
                // if there s a version its been fixed :)

                if (string.IsNullOrEmpty(m_PermissionVersion))
                {
                    List<Permission> newPermissionList = new List<Permission>();
                    foreach (Permission p in m_requiredReadPermission)
                    {
                        if (p == Permission.AllowAccountantRead)
                        {
                            newPermissionList.Add(Permission.AllowLockDeskRead);
                        }
                        else if (p == Permission.AllowLockDeskRead)
                        {
                            newPermissionList.Add(Permission.AllowAccountantRead);
                        }
                        else
                        {
                            newPermissionList.Add(p);
                        }
                    }

                    m_requiredReadPermission = newPermissionList;
                }
                //else if (m_PermissionVersion == 2)
                //{

                //}
   
                string details = reader.GetAttribute(ATTR_DETAILS);

                if (false == string.IsNullOrEmpty(details))
                {
                    m_renderDetailsLink = bool.Parse(details);
                }

            } 
            catch (Exception exc) 
            {
                Tools.LogError(exc);
            }
            ParseBodyContent(reader);


        }

        protected AbstractAuditItem(DbDataReader reader)
        {
            try
            {
                m_timestamp = (DateTime)reader["EventDate"];
                m_description = (string)reader["EventDescription"];
                m_userName = (string)reader["UserName"];
                m_renderDetailsLink = (bool)reader["HasDetails"];
                m_categoryT = (E_AuditItemCategoryT)(int)reader["Category"];
                m_classType = (string)reader["ClassType"]; 
                m_id = new Guid((string)reader["DetailContentKey"]);
                var requiredReadPermission = SerializationHelper.JsonNetDeserialize<List<Permission>>((string)reader["ReadPermissionsREquiredJsonContent"]);
                SetPermissions(requiredReadPermission); // this method set m_PermissionVersion = "2"


                AuditSeq = (long)reader["Id"];
                m_detailLoaded = false;
                

                // get from fileDb detailContent
                m_userID = Guid.Empty;
                m_xsltFile = string.Empty;
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
            }          

        }

        protected void LoadDetailContentIfNeed()
        {
            if (this.m_detailLoaded)
            {
                return;
            }

            try
            {
                string detailContentKey = ID.ToString();
                var content = FileDBTools.ReadDataText(E_FileDB.Normal, detailContentKey);
                using (XmlReader reader = new XmlTextReader(content, XmlNodeType.Element, null))
                {
                    if (reader.Read())
                    {
                        // m_id = new Guid(reader.GetAttribute(ATTR_ID));
                        m_userID = new Guid(reader.GetAttribute(ATTR_USER_ID));
                        m_xsltFile = reader.GetAttribute(ATTR_XSLT_FILE);
                        ParseBodyContent(reader);
                        this.m_detailLoaded = true;
                    }
                }
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
            }
        }

        /// <summary>
        /// Old audit items will not have the category assigned. This will
        /// look at the class and, if necessary, the description to determine
        /// the category.
        /// Note: Don't have to check for Disclosure/ESign becuase these audit items are new.
        /// </summary>
        /// <returns>The category as parsed from the class type and description.</returns>
        private E_AuditItemCategoryT GetCategoryFromClassAndDescription(string classType, string description)
        {
            // The class type will be the fully resolved (including namespace) type.
            // Split it and get the last piece (just the class name).
            string className = classType.Split(new char[] { '.' }).Last();
            switch (className)
            {
                case "TrackedFieldModifiedAuditItem": 
                case "FieldModifiedAuditItem":
                    return E_AuditItemCategoryT.FieldChange;
                case "AssignmentChangeAuditItem":
                    return E_AuditItemCategoryT.RoleAssignment;
                case "LoanSubmissionAuditItem":
                case "RemoveRequestedRateAuditItem":
                    return E_AuditItemCategoryT.RateLock;
                case "LoanStatusChangeAuditItem":
                case "LoanCreationAuditItem":
                case "LoanDeleteAuditItem":
                case "LoanRestoreAuditItem":
                    return E_AuditItemCategoryT.LoanStatus;
                case "UWApprovalCertificateEmailAuditItem":
                case "RateLockConfirmationEmailAuditItem":
                case "PmlLoanSummaryEmailAuditItem":
                case "PmlCertificateEmailAuditItem":
                case "LoanComparisonSentAuditItem":
                    return E_AuditItemCategoryT.Email;
                case "CreditReportAuditItem":
                    return E_AuditItemCategoryT.CreditReport;
                case "UpdateFromLoanProspectorAuditItem":
                case "UpdateFromFnmaAuditItem":
                case "SubmitToFnmaAuditItem":
                case "SendToLoanProspectorAuditItem":
                case "H4HSubmissionAuditItem":
                case "FloodSubmissionXmlAuditItem":
                case "FHATotalScoreCardAuditItem":
                case "FHAConnectionAuditItem":
                case "DriveSubmissionXmlAuditItem":
                case "DriveSubmissionAuditItem":
                case "DataTracAuditItem":
                    return E_AuditItemCategoryT.Integration;
            }

            // If it still hasn't found a match, try to do some basic matching
            // based on the audit item's description.
            string desc = description.ToUpper();
            if (description.StartsWith("FIELD CHANGE") ||
                description.StartsWith("AUTOMATIC FIELD CHANGE"))
            {
                return E_AuditItemCategoryT.FieldChange;
            }
            else if (description.Contains("ROLE ASSIGNMENT CHANGE"))
            {
                return E_AuditItemCategoryT.RoleAssignment;
            }
            else if (description.StartsWith("EMAIL"))
            {
                return E_AuditItemCategoryT.Email;
            }
            else if (description.Contains("LOAN STATUS") ||
                     description.Contains("LOAN DELETE") ||
                     description.Contains("LOAN CREATION"))
            {
                return E_AuditItemCategoryT.LoanStatus;
            }
            else if (description.Contains("CREDIT REPORT"))
            {
                return E_AuditItemCategoryT.CreditReport;
            }
            else if (description.Contains("RATE LOCK"))
            {
                return E_AuditItemCategoryT.RateLock;
            }
            else
            {
                return E_AuditItemCategoryT.Integration;
            }

        }

        protected virtual void ParseBodyContent(XmlReader reader) 
        {
        }

        private void Initialize(AbstractUserPrincipal principal, string description, string xsltFile, E_AuditItemCategoryT category) 
        {
            m_id = Guid.NewGuid();
            m_timestamp = DateTime.Now;
            if (principal != null)
            {
                m_userID = principal.UserId;
                m_userName = principal.DisplayName;
                this.BrokerId = principal.BrokerId;
            }
            else
            {
                Tools.LogErrorWithCriticalTracking("principal was null, auditing anyway.");
                m_userName = "System User";
            }
            m_description = description;
            m_xsltFile = xsltFile;
            m_categoryT = category;
        }
        private void Init(Guid userId, string displayName, string description, E_AuditItemCategoryT category)
        {
            m_id = Guid.NewGuid();
            m_timestamp = DateTime.Now;
            m_userID = userId;
            m_userName = displayName;
            m_description = description;
            m_categoryT = category;
        }
        private void Initialize(string description, string xsltFile, E_AuditItemCategoryT category)
        {
            m_id = Guid.NewGuid();
            m_timestamp = DateTime.Now;
            m_userID = Guid.Empty;
            m_userName = "";
            m_description = description;
            m_xsltFile = xsltFile;
            m_categoryT = category;
        }

        public void GenerateXml(XmlWriter writer) 
        {

            // 7/15/2005 dd - TO optimize space, I sacrific long description with strange acronymn.
            writer.WriteStartElement(ELEMENT_AUDIT_ITEM);
            WriteSafeAttr(writer, ATTR_ID, m_id);
            writer.WriteAttributeString(ATTR_TIME_STAMP, m_timestamp.Ticks.ToString());
            WriteSafeAttr(writer, ATTR_DESCRIPTION, m_description);
            WriteSafeAttr(writer, ATTR_USER_ID, m_userID);
            WriteSafeAttr(writer, ATTR_USER_NAME, m_userName);
            WriteSafeAttr(writer, ATTR_CLASS_TYPE, this.GetType().FullName);
            WriteSafeAttr(writer, ATTR_XSLT_FILE, m_xsltFile);

            WriteSafeAttr(writer, ATTR_PERMISSIONS, ObsoleteSerializationHelper.JavascriptJsonSerialize(m_requiredReadPermission));
            WriteSafeAttr(writer, ATTR_DETAILS, m_renderDetailsLink.ToString());
            WriteSafeAttr(writer, ATTR_PERMISSION_VERSION, m_PermissionVersion);

            WriteSafeAttr(writer, ATTR_CATEGORY, m_categoryT.ToString("d"));

            writer.WriteStartElement(ELEMENT_BODY);
            GenerateDetailXml(writer);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Always remove "-" when writing Guid to save 4 characters per Guid.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="o"></param>
        protected void WriteSafeAttr(XmlWriter writer, string attr, Guid o) 
        {
            writer.WriteAttributeString(attr, o.ToString("N"));
        }
        /// <summary>
        /// Can be used with (int)enums to save space and to allow enum renames.
        /// </summary>
        protected void WriteSafeAttr(XmlWriter writer, string attr, int value)
        {
            writer.WriteAttributeString(attr, value.ToString());
        }

        protected void WriteSafeAttr(XmlWriter writer, string attr, string value) 
        {
            if (null != value && "" != value)
                writer.WriteAttributeString(attr, value);
        }
        protected void WriteSafeAttr(XmlWriter writer, string attr, bool value) 
        {
            writer.WriteAttributeString(attr, value ? "1" : "0");
        }

        protected abstract void GenerateDetailXml(XmlWriter writer);

	}
}
