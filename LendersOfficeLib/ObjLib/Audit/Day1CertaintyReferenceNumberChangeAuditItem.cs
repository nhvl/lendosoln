﻿namespace LendersOffice.Audit
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess.FannieMae;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an audit item for a change in the DU Day 1 Certainty reference numbers.
    /// </summary>
    public class Day1CertaintyReferenceNumberChangeAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Represents the XML to be saved in the body element of the audit history.
        /// </summary>
        private readonly XElement dataToSave;

        /// <summary>
        /// Initializes a new instance of the <see cref="Day1CertaintyReferenceNumberChangeAuditItem"/> class.
        /// </summary>
        /// <param name="principal">The user making the change.</param>
        /// <param name="additions">The additions to the list of reference numbers.</param>
        /// <param name="removals">The removals from the list of reference numbers.</param>
        private Day1CertaintyReferenceNumberChangeAuditItem(
            AbstractUserPrincipal principal,
            IEnumerable<FannieMaeThirdPartyProvider> additions,
            IEnumerable<FannieMaeThirdPartyProvider> removals)
            : base(
                  principal,
                  description: "D1C reference numbers have changed",
                  xsltFile: "Day1CertaintyReferenceNumberChange.xsl",
                  category: DataAccess.E_AuditItemCategoryT.FieldChange)
        {
            IEnumerable<XElement> addedElements = additions.Select(p => new XElement("r", new XElement("provider", p.Name), new XElement("number", p.ReferenceNumber)));
            IEnumerable<XElement> removedElements = removals.Select(p => new XElement("r", new XElement("provider", p.Name), new XElement("number", p.ReferenceNumber)));
            this.dataToSave = new XElement(
                "data",
                addedElements.Any() ? new XElement("added", addedElements) : null,
                removedElements.Any() ? new XElement("removed", removedElements) : null);
        }

        /// <summary>
        /// Creates an audit item for a change, or returns null if there is no audit item to record.
        /// </summary>
        /// <param name="principal">The user making the change.</param>
        /// <param name="originalList">The original (pre-edit) list of reference numbers.</param>
        /// <param name="newList">The new (post-edit) list of reference numbers.</param>
        /// <returns>The audit item representing the specified change, or null if no change was found.</returns>
        public static AbstractAuditItem CreateAuditItemForChange(
            AbstractUserPrincipal principal,
            FannieMaeThirdPartyProviderList originalList,
            FannieMaeThirdPartyProviderList newList)
        {
            List<FannieMaeThirdPartyProvider> originalItems = originalList.GetProviders();
            List<FannieMaeThirdPartyProvider> newItems = newList.GetProviders();

            // 2018-03 tj - This algorithm's just about as dumb as it can be, but we're dealing
            // with an unsorted collection that doesn't care about changes in order and I don't
            // have time to think more.
            var additions = new List<FannieMaeThirdPartyProvider>();
            var removals = new List<FannieMaeThirdPartyProvider>();
            foreach (var item in originalItems)
            {
                if (!newItems.Contains(item))
                {
                    removals.Add(item);
                }
            }

            foreach (var item in newItems)
            {
                if (!originalItems.Contains(item))
                {
                    additions.Add(item);
                }
            }

            if (additions.Count == 0 && removals.Count == 0)
            {
                return null;
            }

            return new Day1CertaintyReferenceNumberChangeAuditItem(principal, additions: additions, removals: removals);
        }

        /// <summary>
        /// Generates the XML for the current audit item.
        /// </summary>
        /// <param name="writer">The writer to receive the XML.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            this.dataToSave.WriteTo(writer);
        }
    }
}
