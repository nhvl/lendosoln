﻿namespace LendersOffice.ObjLib.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Fee Service Application Audit Item.
    /// </summary>
    public class FeeServiceApplicationAuditItem : AbstractAuditItem
    {
        /// <summary>
        /// Login Name.
        /// </summary>
        private string loginName;

        /// <summary>
        /// True if fee service test file was used.
        /// </summary>
        private bool usedTestFile;

        /// <summary>
        /// True if there was a prior submission where fee service rules were applied.
        /// </summary>
        private bool priorSubmission;

        /// <summary>
        /// Automation update mode.
        /// </summary>
        private E_sClosingCostAutomationUpdateT automationUpdateT;

        /// <summary>
        /// List of fee service rules that were applied.
        /// </summary>
        private IEnumerable<RuleAppliedAuditDetails> rulesApplied;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeeServiceApplicationAuditItem" /> class.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="usedTestFile">True if fee service test file was used.</param>
        /// <param name="priorSubmission">True if there was a prior submission where fee service rules were applied.</param>
        /// <param name="automationUpdateT">Automation update mode.</param>
        /// <param name="rulesApplied">List of fee service rules that were applied.</param>
        public FeeServiceApplicationAuditItem(AbstractUserPrincipal principal, bool usedTestFile, bool priorSubmission, E_sClosingCostAutomationUpdateT automationUpdateT, IEnumerable<RuleAppliedAuditDetails> rulesApplied) :
            base(principal, "Fee service applied", "FeeServiceApplication.xslt", DataAccess.E_AuditItemCategoryT.FeeService)
        {
            this.loginName = principal.LoginNm;
            this.usedTestFile = usedTestFile;
            this.priorSubmission = priorSubmission;
            this.automationUpdateT = automationUpdateT;
            this.rulesApplied = rulesApplied;
        }

        /// <summary>
        /// Generates the XML used for the Audit Details page.
        /// </summary>
        /// <param name="writer">XML writer.</param>
        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");
            this.WriteSafeAttr(writer, "login", this.loginName);
            this.WriteSafeAttr(writer, "usedTestFile", this.usedTestFile);
            this.WriteSafeAttr(writer, "priorSubmission", this.priorSubmission);
            this.WriteSafeAttr(writer, "automationUpdateT", (int)this.automationUpdateT);

            if (this.rulesApplied.Count() > 0)
            {
                writer.WriteStartElement("rules");
                foreach (RuleAppliedAuditDetails rule in this.rulesApplied)
                {
                    writer.WriteStartElement("rule");

                    this.WriteSafeAttr(writer, "value", rule.Value);
                    this.WriteSafeAttr(writer, "hudLine", rule.HudLine);
                    this.WriteSafeAttr(writer, "fieldId", rule.FieldId);
                    this.WriteSafeAttr(writer, "conditions", string.IsNullOrEmpty(rule.Conditions) ? "\u00A0" : rule.Conditions); // Replace empty string with nbsp (fixes html issues).

                    if (rule.FeeId != Guid.Empty)
                    {
                        this.WriteSafeAttr(writer, "feeId", rule.FeeId);
                    }

                    writer.WriteEndElement(); // </rule>
                }

                writer.WriteEndElement(); // </rules>
            }

            writer.WriteEndElement(); // </data>
        }

        /// <summary>
        /// Audit details for individual fee service rules.
        /// </summary>
        public class RuleAppliedAuditDetails
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RuleAppliedAuditDetails" /> class.
            /// </summary>
            /// <param name="value">Value applied.</param>
            /// <param name="hudLine">Hud line affected.</param>
            /// <param name="fieldId">Rule Field ID.</param>
            /// <param name="conditions">Rule conditions.</param>
            /// <param name="feeId">FeeId if applicable.</param>
            public RuleAppliedAuditDetails(string value, string hudLine, string fieldId, string conditions, Guid feeId)
            {
                this.Value = value;
                this.HudLine = hudLine;
                this.FieldId = fieldId;
                this.Conditions = conditions;
                this.FeeId = feeId;
            }

            /// <summary>
            /// Gets or sets Value.
            /// </summary>
            /// <value>Rule Value.</value>
            public string Value { get; set; }

            /// <summary>
            /// Gets or sets Hud Line.
            /// </summary>
            /// <value>Rule Hud Line.</value>
            public string HudLine { get; set; }

            /// <summary>
            /// Gets or sets Field ID.
            /// </summary>
            /// <value>Rule Field ID.</value>
            public string FieldId { get; set; }

            /// <summary>
            /// Gets or sets Conditions.
            /// </summary>
            /// <value>Rule Conditions.</value>
            public string Conditions { get; set; }

            /// <summary>
            /// Gets or sets Fee ID.
            /// </summary>
            /// <value>Rule Fee ID.</value>
            public Guid FeeId { get; set; }
        }
    }
}
