﻿using System;
using System.Xml;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public class FloodSubmissionXmlAuditItem : AbstractAuditItem
    {
        private string m_sXML;
        private string m_sLoginName;

        public FloodSubmissionXmlAuditItem(AbstractUserPrincipal principal, string xml) :
            base(principal, "Flood Integration Response (XML)", "FloodSubmissionXml.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
            m_sXML = xml;
            m_sLoginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer)
        {
            writer.WriteStartElement("data");

            WriteSafeAttr(writer, "login", m_sLoginName);
            if (null != m_sXML)
            {
                writer.WriteStartElement("xml");
                writer.WriteCData(m_sXML);
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // </data>
        }
    }
}
