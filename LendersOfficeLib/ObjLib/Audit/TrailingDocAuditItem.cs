﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using System.Xml;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.Audit
{
    public sealed class TrailingDocAuditItem : AbstractAuditItem
    {


        private const string ATTR_FRIENDLY_NAME = "FriendlyFieldName";
        private const string ATTR_OLD_VALUE = "FieldOldValue";
        private const string ATTR_NEW_VALUE = "FieldNewValue";
        private const string ATTR_FIELD_ID = "FieldId";
        private const string ATTR_CHANGED_AT = "ChangedAt";


        private string FieldFriendlydName { get; set; }
        private string OriginalValue { get; set; }
        private string NewValue { get; set; }
        private string FieldId { get; set; }
        private string ChangedAt { get; set; }



        public TrailingDocAuditItem(TrackedField field, Guid userId, string displayName,
            string oldValue, string newValue, string changedAt, bool isautomatic)
            : base(userId, displayName, "", DataAccess.E_AuditItemCategoryT.TrailingDocument)
        {
            StringBuilder sb = new StringBuilder();
            if (isautomatic)
            {
                sb.Append("Automatic: ");
            }

            HasDetails = false;
            sb.AppendFormat(@"{0} changed from ""{1}"" to ""{2}""", field.Description, oldValue, newValue);
            SetDescription(sb.ToString());

            FieldFriendlydName = field.Description;
            OriginalValue = oldValue;
            NewValue = newValue;
            FieldId = field.FieldId;
            ChangedAt = changedAt;

            List<Permission> requiredPermissions = new List<Permission>();
            requiredPermissions.Add(Permission.AllowAccountantRead);

            SetPermissions(requiredPermissions);

        }

        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_FRIENDLY_NAME, FieldFriendlydName);
            WriteSafeAttr(writer, ATTR_OLD_VALUE, OriginalValue);
            WriteSafeAttr(writer, ATTR_NEW_VALUE, NewValue);
            WriteSafeAttr(writer, ATTR_FIELD_ID, FieldId);
            WriteSafeAttr(writer, ATTR_CHANGED_AT, ChangedAt);
            writer.WriteEndElement();
        }
    }
}
