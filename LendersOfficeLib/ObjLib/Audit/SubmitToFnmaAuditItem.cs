/// Author: David Dao

using System;
using System.Xml;

using LendersOffice.Security;
namespace LendersOffice.Audit
{
	public class SubmitToFnmaAuditItem : AbstractAuditItem
	{
        private bool m_isDo;
        private string m_mornetUserId;
		private string m_loginName;
		public SubmitToFnmaAuditItem(AbstractUserPrincipal principal, bool isDo, string mornetUserId)
            : base(principal, "Submit to DO/DU", "SubmitToFnma.xslt", DataAccess.E_AuditItemCategoryT.Integration)
		{
            m_isDo = isDo;
            m_mornetUserId = mornetUserId;
			m_loginName = principal.LoginNm;
		}
        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "type", m_isDo ? "DO" : "DU");
            WriteSafeAttr(writer, "mornet", m_mornetUserId);
			WriteSafeAttr(writer, "login", m_loginName);
            writer.WriteEndElement();
        }
	}
}
