using System;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Audit
{
    public enum E_LoanCreationSource 
    {
        LeadCreate,
        UserCreateFromBlank,
        FromTemplate,
        PointImport,
        FannieMaeImport,
        FreddieMacImport,
        FromSpinOff,
        MismoImport,
        FileDuplication,
        NhcImport,
        LeadConvertToLoanUsingTemplate,
        CreateLeadUsingWebservice,
        CreateBlankLoanUsingWebservice,
        CreateFromTemplateUsingWebservice,
        ImportFromDoDu,
        ImportFromLoansPQ,
        ImportFromULDD, 
        Sandbox,
        CreateTest
    }
	public class LoanCreationAuditItem : AbstractAuditItem
	{
        #region ATTR Constants
        private const string ATTR_SOURCE = "src";
        private const string ATTR_TEMPLATE_NAME = "lpNm";
		
        #endregion

        private E_LoanCreationSource m_source;
        private string m_templateName;
		private string m_loginName;

        public LoanCreationAuditItem(AbstractUserPrincipal principal, E_LoanCreationSource source, string templateName, string description) :
            base (principal, description, "LoanCreation.xslt", DataAccess.E_AuditItemCategoryT.LoanStatus)
        {
            m_templateName = templateName;
            m_source = source;
			m_loginName = principal.LoginNm;
        }

        protected override void GenerateDetailXml(XmlWriter writer) 
        {
            writer.WriteStartElement("data");
            WriteSafeAttr(writer, ATTR_TEMPLATE_NAME, m_templateName);
						
            string s = "";

            switch (m_source) 
            {
                case E_LoanCreationSource.UserCreateFromBlank:
                    s = "User create from blank file";
                    break;
                case E_LoanCreationSource.FromTemplate:
                    s = "User create from template";
                    break;
                case E_LoanCreationSource.PointImport:
                    s = "Import from Calyx Point";
                    break;
                case E_LoanCreationSource.FannieMaeImport:
                    s = "Import from Fannie Mae";
                    break;
                case E_LoanCreationSource.FreddieMacImport:
                    s = "Import from Freddie Mac";
                    break;
                case E_LoanCreationSource.LeadCreate:
                    s = "Lead creation";
                    break;
                case E_LoanCreationSource.FromSpinOff:
                    s = "User create subfinancing file";
                    break;
                case E_LoanCreationSource.FileDuplication:
                    s = "File Duplication";
                    break;
                case E_LoanCreationSource.MismoImport:
                    s = "Import from Mismo";
                    break;
                case E_LoanCreationSource.NhcImport:
                    s = "Import from NHC";
                    break;
                case E_LoanCreationSource.LeadConvertToLoanUsingTemplate:
                    s = "Lead converts to loan using template";
                    break;
                case E_LoanCreationSource.CreateLeadUsingWebservice:
                    s = "Create lead using web service.";
                    break;
                case E_LoanCreationSource.CreateBlankLoanUsingWebservice:
                    s = "Create blank file using web service.";
                    break;
                case E_LoanCreationSource.CreateFromTemplateUsingWebservice:
                    s = "Create from template using web service.";
                    break;
                case E_LoanCreationSource.ImportFromDoDu:
                    s = "Import from DO / DU";
                    break;
                case E_LoanCreationSource.ImportFromLoansPQ:
                    s = "Import from LoansPQ";
                    break;
                case E_LoanCreationSource.ImportFromULDD:
                    s = "Import from ULDD";
                    break;
                case E_LoanCreationSource.Sandbox:
                    s = "Sandbox";
                    break;
                case E_LoanCreationSource.CreateTest:
                    s = "Create test file";
                    break;
                default:
                    s = "Unknown Source";
                    DataAccess.Tools.LogWarning(m_source + " is not handled.");
                    break;
            }
            WriteSafeAttr(writer, ATTR_SOURCE, s);
			WriteSafeAttr(writer, "login", m_loginName);

            writer.WriteEndElement(); // </data>
        }

	}
}
