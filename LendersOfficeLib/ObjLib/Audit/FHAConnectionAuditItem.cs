﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Audit;
using LendersOffice.Security;
using System.IO;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.ObjLib.Audit
{
    public class FHAConnectionAuditItem : AbstractAuditItem
    {
        private string m_resultXml;
        private string m_userName;
        private string m_loginName;
        private string m_caseNum;
        private string m_eventType;

        public FHAConnectionAuditItem(AbstractUserPrincipal principal, string eventType, string resultXml, string caseNum ) :
            base(principal, eventType, "SubmitToFHAConnection.xslt", E_AuditItemCategoryT.Integration)
        {
            m_resultXml = resultXml;
            m_loginName = principal.LoginNm;
            m_userName = principal.DisplayName;
            m_caseNum = caseNum;
            m_eventType = eventType;
            
        }


        protected override void GenerateDetailXml(System.Xml.XmlWriter writer)
        {
            string resultHtml;
            string xslt = GetXslt();

            using (MemoryStream ms = new MemoryStream())
            {
                XslTransformHelper.TransformFromEmbeddedResource(xslt, m_resultXml, ms, null);
                ms.Position = 0;
                using (StreamReader reader = new StreamReader(ms))
                {
                    resultHtml = reader.ReadToEnd();
                }
            }

            writer.WriteStartElement("data");
            WriteSafeAttr(writer, "uname", m_userName);
            WriteSafeAttr(writer, "login", m_loginName);
            WriteSafeAttr(writer, "desc", m_eventType);
            WriteSafeAttr(writer, "caseNum", m_caseNum);
            writer.WriteStartElement("xmlResponse");
            writer.WriteCData(m_resultXml.Replace("]]>", "]]&gt;<![CDATA[")); // There can be nested  CDATA
            writer.WriteEndElement(); //xmlresponse
            writer.WriteStartElement("htmlResult");
            writer.WriteCData(resultHtml);
            writer.WriteEndElement(); // htmlResult
            writer.WriteEndElement(); //data
        }

        private string GetXslt()
        {
            switch (m_eventType)
            {
                case "New Case Number Assignment": return "LendersOffice.ObjLib.FHAConnection.xslt.caseNumberAssignment.xslt";
                case "Update an Existing Case": return "LendersOffice.ObjLib.FHAConnection.xslt.caseNumberAssignment.xslt";
                case "Holds Tracking": return "LendersOffice.ObjLib.FHAConnection.xslt.holdsTracking.xslt";
                case "Case Query": return "LendersOffice.ObjLib.FHAConnection.xslt.caseQuery.xslt";
                case "CAIVRS Authorization": return "LendersOffice.ObjLib.FHAConnection.xslt.caivrsAuthorization.xslt";
                default: throw new CBaseException(ErrorMessages.Generic, "Unknown Event Type: " + m_eventType);
            }
        }
    }
}
