﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using LendersOffice.Email;
using System.Collections;
using System.Diagnostics;
using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;
using LendersOffice.Migration;
using LendersOffice.Admin;
using System.Reflection;
using CommonProjectLib.Runnable;

namespace LendersOffice.ObjLib.SequentialNumbering
{
    /// <summary>
    /// Nightly runner to add a sequential numbering to files based on broker settings.
    /// See OPM 118600
    /// </summary>
    public class SequentialNumberingRunner : IRunnable
    {
        public string Description
        {
            get
            {
                return "Nightly runner to add a sequential numbering to files based on broker settings. See OPM 118600"; 
            }
        }
        public void Run()
        {
            var timer = Stopwatch.StartNew();
            var timingInfo = new StringBuilder();
            timingInfo.AppendFormat("[NightlySeqNum]{0}", Environment.NewLine);

            var brokerIds = GetBrokersThatRequireSequentialNumbering();

            foreach (var brokerId in brokerIds)
            {
                var brokerTimer = Stopwatch.StartNew();

                var brokerDb = BrokerDB.RetrieveById(brokerId);
                var fieldId = brokerDb.SequentialNumberingFieldId;
                var seed = brokerDb.SequentialNumberingSeed;
                
                var loanRetrievalTimer = Stopwatch.StartNew();
                var loanIds = GetLoansThatNeedNumbering(brokerId, fieldId);
                loanRetrievalTimer.Stop();
                
                int errorCount = 0;
                foreach (var loanId in loanIds)
                {
                    try
                    {
                        CPageData dataLoan = new CSequentialNumberingData(loanId);
                        dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        PageDataUtilities.SetValue(dataLoan, null, fieldId, seed.ToString("d"));
                        dataLoan.Save();
                        seed++;
                    }
                    catch (Exception exc)
                    {
                        errorCount++;
                        var msg = String.Format("[NightlySeqNum] Unexpected error when updating target field "
                            + "for Broker Id: {0}, Loan Id: {1}, Field Id: {2}.", brokerId, loanId, fieldId);
                        Tools.LogError(msg, exc);
                    }
                }

                bool brokerUpdated = true;
                if (seed != brokerDb.SequentialNumberingSeed)
                {
                    brokerDb.SequentialNumberingSeed = seed;

                    try
                    {
                        brokerDb.Save();
                    }
                    catch (Exception exc)
                    {
                        brokerUpdated = false;
                        var msg = String.Format("[NightlySeqNum] Unexpected error when trying to save broker "
                            + "for Broker Id: {0}, Counter Value: {1}.", brokerId, seed);
                        Tools.LogErrorWithCriticalTracking(msg, exc);
                    }
                }
                brokerTimer.Stop();

                timingInfo.AppendFormat("  **Loans numbered for brokerId {0} in {1} ms.{2}  Loans retrieved in {3} ms."
                    + " Found {4} loans that needed numbering. {5} errors. Broker {6}updated successfully.{2}", 
                    brokerId, brokerTimer.ElapsedMilliseconds, Environment.NewLine, loanRetrievalTimer.ElapsedMilliseconds,
                    loanIds.Count, errorCount, brokerUpdated ? "" : "not ");
            }

            Tools.LogInfo(timingInfo.ToString());
        }

        private List<Guid> GetBrokersThatRequireSequentialNumbering()
        {
            var brokerIds = new List<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokersForSequentialNumbering", null))
                {
                    while (sR.Read())
                    {
                        brokerIds.Add((Guid)sR["BrokerId"]);
                    }
                }
            }

            return brokerIds;
        }

        /// <summary>
        /// Gets a list of loan ids for loans that need to be numbered.
        /// </summary>
        /// <param name="brokerId">Check the loans for this broker id.</param>
        /// <param name="fieldId">The target field id.</param>
        /// <returns>The loan ids for files that have a blank target field.</returns>
        private List<Guid> GetLoansThatNeedNumbering(Guid brokerId, string fieldId)
        {
            var loanIds = new List<Guid>();
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId), 
                                                new SqlParameter("@FieldId", fieldId)
                                            };
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansThatNeedNumberingForBroker", parameters))
                {
                    while (sR.Read())
                    {
                        loanIds.Add((Guid)sR["sLId"]);
                    }
                }
            }
            catch (SqlException exc)
            {
                var msg = String.Format("[NightlySeqNum] Error when retrieving loans that need numbering for brokerId: {0}",
                    brokerId);
                Tools.LogErrorWithCriticalTracking(msg, exc);
            }
            
            return loanIds;
        }
    }

    public class CSequentialNumberingData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CSequentialNumberingData()
        {
            var list = new StringList();

            #region Target Fields
            list.Add("sCustomField1Notes");
            list.Add("sCustomField2Notes");
            list.Add("sCustomField3Notes");
            list.Add("sCustomField4Notes");
            list.Add("sCustomField5Notes");
            list.Add("sCustomField6Notes");
            list.Add("sCustomField7Notes");
            list.Add("sCustomField8Notes");
            list.Add("sCustomField9Notes");
            list.Add("sCustomField10Notes");
            list.Add("sCustomField11Notes");
            list.Add("sCustomField12Notes");
            list.Add("sCustomField13Notes");
            list.Add("sCustomField14Notes");
            list.Add("sCustomField15Notes");
            list.Add("sCustomField16Notes");
            list.Add("sCustomField17Notes");
            list.Add("sCustomField18Notes");
            list.Add("sCustomField19Notes");
            list.Add("sCustomField20Notes");
            list.Add("sCustomField21Notes");
            list.Add("sCustomField22Notes");
            list.Add("sCustomField23Notes");
            list.Add("sCustomField24Notes");
            list.Add("sCustomField25Notes");
            list.Add("sCustomField26Notes");
            list.Add("sCustomField27Notes");
            list.Add("sCustomField28Notes");
            list.Add("sCustomField29Notes");
            list.Add("sCustomField30Notes");
            list.Add("sCustomField31Notes");
            list.Add("sCustomField32Notes");
            list.Add("sCustomField33Notes");
            list.Add("sCustomField34Notes");
            list.Add("sCustomField35Notes");
            list.Add("sCustomField36Notes");
            list.Add("sCustomField37Notes");
            list.Add("sCustomField38Notes");
            list.Add("sCustomField39Notes");
            list.Add("sCustomField40Notes");
            list.Add("sCustomField41Notes");
            list.Add("sCustomField42Notes");
            list.Add("sCustomField43Notes");
            list.Add("sCustomField44Notes");
            list.Add("sCustomField45Notes");
            list.Add("sCustomField46Notes");
            list.Add("sCustomField47Notes");
            list.Add("sCustomField48Notes");
            list.Add("sCustomField49Notes");
            list.Add("sCustomField50Notes");
            list.Add("sCustomField51Notes");
            list.Add("sCustomField52Notes");
            list.Add("sCustomField53Notes");
            list.Add("sCustomField54Notes");
            list.Add("sCustomField55Notes");
            list.Add("sCustomField56Notes");
            list.Add("sCustomField57Notes");
            list.Add("sCustomField58Notes");
            list.Add("sCustomField59Notes");
            list.Add("sCustomField60Notes");

            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );
        }

        public CSequentialNumberingData(Guid fileId)
            : base(fileId, "CSequentialNumberingData", s_selectProvider)
        {
        }

        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }
    }

}
