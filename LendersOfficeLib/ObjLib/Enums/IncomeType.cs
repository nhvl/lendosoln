﻿namespace LqbGrammar.DataTypes
{
    using DataAccess;

    /// <summary>
    /// Identifies the type of income.
    /// </summary>
    public enum IncomeType
    {
        /// <summary>
        /// The base income from a job.
        /// </summary>
        [OrderedDescription("Base Income")]
        BaseIncome = 0,

        /// <summary>
        /// Overtime income from a job.
        /// </summary>.
        [OrderedDescription("Overtime")]
        Overtime = 1,

        /// <summary>
        /// Bonus income from a job.
        /// </summary>
        [OrderedDescription("Bonuses")]
        Bonuses = 2,

        /// <summary>
        /// Comission income from a job.
        /// </summary>
        [OrderedDescription("Comission")]
        Commission = 3,

        /// <summary>
        /// Dividend or interest from investments.
        /// </summary>
        [OrderedDescription("Dividends or Interest")]
        DividendsOrInterest = 4,

        /// <summary>
        /// Income type that does not fit in any of the other enumerations.
        /// </summary>
        [OrderedDescription("Other")]
        Other = 5,

        /// <summary>
        /// Income from being in the military. 
        /// </summary>
        [OrderedDescription("Military Base Pay")]
        MilitaryBasePay = 6,

        /// <summary>
        /// Income from military rations.
        /// </summary>
        [OrderedDescription("Military Rations Allowance")]
        MilitaryRationsAllowance = 7,

        /// <summary>
        /// Income from military flight pay.
        /// </summary>
        [OrderedDescription("Military Flight Pay")]
        MilitaryFlightPay = 8,

        /// <summary>
        /// Income from military hazard pay. 
        /// </summary>
        [OrderedDescription("Military Hazard Pay")]
        MilitaryHazardPay = 9,

        /// <summary>
        /// Income from military for clothes allowance.
        /// </summary>
        [OrderedDescription("Military Clothes Allowance")]
        MilitaryClothesAllowance = 10,

        /// <summary>
        /// Income from military quarter allowance. 
        /// </summary>
        [OrderedDescription("Military Quarters Allowance")]
        MilitaryQuartersAllowance = 11,

        /// <summary>
        /// Income from military prop pay.
        /// </summary>
        [OrderedDescription("Military Prop Pay")]
        MilitaryPropPay = 12,

        /// <summary>
        /// Income from military overseas pay. 
        /// </summary>
        [OrderedDescription("Military Overseas Pay")]
        MilitaryOverseasPay = 13,

        /// <summary>
        /// Income from military combat pay.
        /// </summary>
        [OrderedDescription("Military Combat Pay")]
        MilitaryCombatPay = 14,

        /// <summary>
        /// Income from military variable housing allowance.
        /// </summary>
        [OrderedDescription("Military Variable Housing Allowance")]
        MilitaryVariableHousingAllowance = 15,

        /// <summary>
        /// Income from alimony or child support.
        /// </summary>
        [OrderedDescription("Alimony / Child Support")]
        AlimonyChildSupport = 16,

        /// <summary>
        /// Income from notes.
        /// </summary>
        [OrderedDescription("Notes Receivable Installment")]
        NotesReceivableInstallment = 17,

        /// <summary>
        /// Income from pension/retirement.
        /// </summary>
        [OrderedDescription("Pension Retirement")]
        PensionRetirement = 18,

        /// <summary>
        /// Income from social security.
        /// </summary>
        [OrderedDescription("Social Security Disability")]
        SocialSecurityDisability = 19,

        /// <summary>
        /// Income from real estate mortgage differentials.
        /// </summary>
        [OrderedDescription("Real Estate Mortgage Differential")]
        RealEstateMortgageDifferential = 20,

        /// <summary>
        /// Income from trust.
        /// </summary>
        [OrderedDescription("Trust")]
        Trust = 21,

        /// <summary>
        /// Income due to unemployment benefits.
        /// </summary>
        [OrderedDescription("Unemployment / Welfare")]
        UnemploymentWelfare = 22,

        /// <summary>
        /// Income from an automobile  expense account.
        /// </summary>
        [OrderedDescription("Automobile Expense Account")]
        AutomobileExpenseAccount = 23,

        /// <summary>
        /// Income from foster care. 
        /// </summary>
        [OrderedDescription("Forest Care")]
        FosterCare = 24,

        /// <summary>
        /// Income from va benefits.
        /// </summary>
        [OrderedDescription("VA Benefits (non-educational)")]
        VABenefitsNonEducation = 25,

        /// <summary>
        /// Income from capital gains. 
        /// </summary>
        [OrderedDescription("Capital Gains")]
        CapitalGains = 26,

        /// <summary>
        /// Income from employment related assets.
        /// </summary>
        [OrderedDescription("Employment Related Assets")]
        EmploymentRelatedAssets = 27,

        /// <summary>
        /// Income from foreign income.
        /// </summary>
        [OrderedDescription("Foreign Income")]
        ForeignIncome = 28,

        /// <summary>
        /// Income from royalty payment.
        /// </summary>
        [OrderedDescription("Royalty Payment")]
        RoyaltyPayment = 29,

        /// <summary>
        /// Income from seasonal employment.
        /// </summary>
        [OrderedDescription("Seasonal Income")]
        SeasonalIncome = 30,

        /// <summary>
        /// Income from temporary leave.
        /// </summary>
        [OrderedDescription("Temporary Leave")]
        TemporaryLeave = 31,

        /// <summary>
        /// Income from tips.
        /// </summary>
        [OrderedDescription("Trip Income")]
        TipIncome = 32,

        /// <summary>
        /// Income from renting out lodgings.
        /// </summary>
        [OrderedDescription("Boarder Income")]
        BoarderIncome = 33,

        /// <summary>
        /// Income from a tax credit on a mortgage.
        /// </summary>
        [OrderedDescription("Mortgage Credit Certificate")]
        MortgageCreditCertificate = 34,

        /// <summary>
        /// Employment income for coborrower that may not be available after move.
        /// </summary>
        [OrderedDescription("Trailing Co-Borrower Income")]
        TrailingCoBorrowerIncome = 35,

        /// <summary>
        /// Income from illegal apartment.
        /// </summary>
        [OrderedDescription("Accessory Unit Income")]
        AccessoryUnitIncome = 36,

        /// <summary>
        /// Income from person who lives in property but is not on mortgage.
        /// </summary>
        [OrderedDescription("Non-Borrower Household Income")]
        NonBorrowerHouseholdIncome = 37,

        /// <summary>
        /// Income from government for housing assitance.
        /// </summary>
        [OrderedDescription("Housing Choice Voucher")]
        HousingChoiceVoucher = 38,

        /// <summary>
        /// Income from social security.
        /// </summary>
        [OrderedDescription("Social Security")]
        SocialSecurity = 39,

        /// <summary>
        /// Income from being disabled.
        /// </summary>
        [OrderedDescription("Disability")]
        Disability = 40,

        /// <summary>
        /// Income from alimony.
        /// </summary>
        [OrderedDescription("Alimony")]
        Alimony = 41,

        /// <summary>
        /// Income from child support.
        /// </summary>
        [OrderedDescription("Child Support")]
        ChildSupport = 42,

        /// <summary>
        /// Income from projects.
        /// </summary>
        [OrderedDescription("Contract Basis")]
        ContractBasis = 43,

        /// <summary>
        /// Income from contribution plans.
        /// </summary>
        [OrderedDescription("Defined Contribution Plan")]
        DefinedContributionPlan = 44,

        /// <summary>
        /// Income for housing allowance for ministers.
        /// </summary>
        [OrderedDescription("Housing Allowance")]
        HousingAllowance = 45,

        /// <summary>
        /// Miscellaneous income.
        /// </summary>
        [OrderedDescription("Miscellaneous Income")]
        MiscellaneousIncome = 46,

        /// <summary>
        /// Income from public assistance.
        /// </summary>
        [OrderedDescription("Public Assistance")]
        PublicAssistance = 47,

        /// <summary>
        /// Income from works compensation.
        /// </summary>
        [OrderedDescription("Workers' Compensation")]
        WorkersCompensation = 48
    }
}
