﻿namespace LendersOffice.ObjLib.Email
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net.Mail;

    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utilities class for manipulating authorized email domains.
    /// </summary>
    public static class AuthorizedEmailDomainUtilities
    {
        /// <summary>
        /// Set of Whitelisted domains that do not need to be checked for authorization.
        /// Mainly used for emails sent from us directly.
        /// </summary>
        private static HashSet<string> authorizedEmailDomainWhitelist = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "meridianlink.com",
            "lendingqb.com",
            "task.lendingqb.com"
        };

        /// <summary>
        /// Gets a set of Whitelisted domains that do not need to be checked for authorization.
        /// </summary>
        /// <value>A set of Whitelisted domains that do not need to be checked for authorization.</value>
        public static IEnumerable<string> AuthorizedEmailDomainWhitelist
        {
            get
            {
                return authorizedEmailDomainWhitelist;
            }
        }

        /// <summary>
        /// Checks if an email address is from an Authorized Domain.
        /// </summary>
        /// <param name="address">Email address as .Net <see cref="MailAddress"/>.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>True, if the address is from an Authorized Domain.</returns>
        public static bool IsAddressFromAuthorizedDomain(MailAddress address, Guid brokerId)
        {
            if (address == null)
            {
                return false;
            }

            return IsAddressFromAuthorizedDomain(address.Address, brokerId);
        }

        /// <summary>
        /// Checks if an email address is from an Authorized Domain.
        /// </summary>
        /// <param name="address">Email address as <see cref="EmailAddress"/>.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>True, if the address is from an Authorized Domain.</returns>
        public static bool IsAddressFromAuthorizedDomain(EmailAddress address, Guid brokerId)
        {
            if (address == null)
            {
                return false;
            }

            return IsAddressFromAuthorizedDomain(address.ToString(), brokerId);
        }

        /// <summary>
        /// Retrieves an enumerable of authorized email domains for the given broker.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>An enumerable of authorized email domains for the given broker.</returns>
        public static IEnumerable<string> GetAuthorizedEmailDomains(Guid brokerId)
        {
            HashSet<string> authorizedEmailDomains = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            using (IDataReader reader = StoredProcedureDriverHelper.ExecuteReader(
                conn,
                null,
                StoredProcedureName.Create("AUTHORIZED_EMAIL_DOMAIN_RetrieveByBrokerId").Value,
                parameters,
                TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    authorizedEmailDomains.Add((string)reader["DomainName"]);
                }
            }

            return authorizedEmailDomains;
        }

        /// <summary>
        /// Saves an authorized email domain for the given broker.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="domainName">Domain Name.</param>
        public static void SaveAuthorizedEmailDomain(Guid brokerId, string domainName)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DomainName", domainName),
            };

            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("AUTHORIZED_EMAIL_DOMAIN_Create").Value, parameters, TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Deletes an authorized email domain belonging to the broker.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// /// <param name="domainName">Domain Name.</param>
        public static void DeleteAuthorizedEmailDomain(Guid brokerId, string domainName)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DomainName", domainName),
            };

            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("AUTHORIZED_EMAIL_DOMAIN_Delete").Value, parameters, TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Checks if an email address is from an Authorized Domain.
        /// </summary>
        /// <param name="address">Email address in string from.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>True, if the address is from an Authorized Domain.</returns>
        /// <remarks>
        /// Do not use this overload directly as it does not parse Outlook formatted
        /// addresses (<![CDATA["User Name <email@address.com>"]]>). Rather than implementing parsing code
        /// in this method, just pass in the pre-parsed address from one of the two email classes
        /// we use, as they both parse Outlook formatted addresses. No need to duplicate code here.
        /// </remarks>
        private static bool IsAddressFromAuthorizedDomain(string address, Guid brokerId)
        {
            // Assume domain is authorized if this is a system email.
            if (brokerId == ConstAppDavid.SystemBrokerGuid)
            {
                return true;
            }

            // Check for blank address. Lets assume blank address is unauthorized.
            // Let email send routines deal with handling emails with blank from addresses.
            if (string.IsNullOrWhiteSpace(address))
            {
                return false;
            }

            string domain = address.Substring(address.IndexOf("@") + 1);

            // Assume domains on the whitelist are authorized (whitelist is for our own domains).
            if (AuthorizedEmailDomainUtilities.AuthorizedEmailDomainWhitelist.Contains(domain, StringComparer.OrdinalIgnoreCase))
            {
                return true;
            }

            // Otherwise check if the domain is in the broker's list of authorized email domains.
            IEnumerable<string> authorizedDomains = AuthorizedEmailDomainUtilities.GetAuthorizedEmailDomains(brokerId);
            return authorizedDomains.Contains(domain, StringComparer.OrdinalIgnoreCase);
        }
    }
}
