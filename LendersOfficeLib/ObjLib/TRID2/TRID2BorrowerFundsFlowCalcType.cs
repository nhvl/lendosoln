﻿namespace LendersOffice.ObjLib.TRID2
{    
    /// <summary>
    /// TRID 2017 Borrower funds flow calculation type.
    /// </summary>
    public enum TRID2BorrowerFundsFlowCalcType
    {
        /// <summary>
        /// Simple calculation mode.
        /// </summary>
        Simple = 0,

        /// <summary>
        /// Complex calculation mode.
        /// </summary>
        Complex = 1
    }
}
