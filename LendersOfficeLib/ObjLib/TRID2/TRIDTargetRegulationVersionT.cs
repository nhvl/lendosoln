﻿namespace LendersOffice.ObjLib.TRID2
{
    /// <summary>
    /// Enum class for TRID regulation version.
    /// </summary>
    public enum TridTargetRegulationVersionT
    {
        /// <summary>
        /// No version selected.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// 2015 TRID version.
        /// </summary>
        TRID2015 = 1,

        /// <summary>
        /// 2017 TRID version.
        /// </summary>
        TRID2017 = 2
    }
}
