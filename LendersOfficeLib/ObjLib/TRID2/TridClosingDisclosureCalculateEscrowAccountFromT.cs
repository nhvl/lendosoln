﻿namespace LendersOffice.ObjLib.TRID2
{
    /// <summary>
    /// Enum value that allow lenders to decide if the escrow account analysis is done for one year from consummation or one year from first payment date.
    /// </summary>
    public enum TridClosingDisclosureCalculateEscrowAccountFromT
    {
        /// <summary>
        /// One year from first payment date. default value.
        /// </summary>
        FirstPaymentDate = 0,

        /// <summary>
        /// One year from consummation.
        /// </summary>
        Consummation = 1,

        /// <summary>
        /// Not applicable.
        /// </summary>
        NA = 2
    }
}
