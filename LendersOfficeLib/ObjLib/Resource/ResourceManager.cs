﻿namespace LendersOffice.ObjLib.Resource
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Constants;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Responsible for retrieving files needed in the code such as certificates. It also support custom files per client. 
    /// </summary>
    public sealed class ResourceManager
    {
        /// <summary>
        /// A singleton instance to the resource manager. 
        /// </summary>
        public static readonly ResourceManager Instance = new ResourceManager();

        /// <summary>
        /// The root path to the client specific resources that override the generic resources.
        /// </summary>
        private string customLocation;

        /// <summary>
        /// The location for the standard set of resources used by the solution.
        /// </summary>
        private string genericLocation;

        /// <summary>
        ///  Prevents a default instance of the <see cref="ResourceManager" /> class from being created.
        /// </summary>
        private ResourceManager()
        {
            this.genericLocation = this.TryToMapPath(ConstSite.GenericResourceLocation);
            this.customLocation = this.TryToMapPath(ConstSite.CustomResourceLocation);
        }

        /// <summary>
        /// Gets the path to generic resource types. Throws exception if the resource type can be overwritten by a custom resource.
        /// </summary>
        /// <param name="resourceType">The resource type to find.</param>
        /// <returns>The path to the resource.</returns>
        public string GetGenericResourcePath(ResourceType resourceType)
        {
            if (this.CanBeCustom(resourceType))
            {
                throw CBaseException.GenericException("Cannot use GetGenericResource to retrieve files that can be overwritten.");
            }

            return this.GetGenericFilePath(resourceType);
        }

        /// <summary>
        /// Gets the contents for the given resource. The resource can only be generic.
        /// </summary>
        /// <param name="resourceType">The resource type to lookup.</param>
        /// <returns>The contents of the resource.</returns>
        public string GetGenericResourceContents(ResourceType resourceType)
        {
            return File.ReadAllText(this.GetGenericResourcePath(resourceType));
        }

        /// <summary>
        /// Gets the path for the given resource. The resource can be the generic or custom one depending
        /// on the broker.
        /// </summary>
        /// <param name="resourceType">The resource type to lookup.</param>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="noCustom">Don't use custom.</param>
        /// <returns>The path to the resource.</returns>
        public string GetResourcePath(ResourceType resourceType, Guid brokerId, bool noCustom = false)
        {
            if (!this.CanBeCustom(resourceType) || noCustom)
            {
                return this.GetGenericFilePath(resourceType);
            }

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            if (!brokerDB.UseCustomXsltCertificate)
            {
                return this.GetGenericFilePath(resourceType);
            }

            string filePath = this.GetCustomFilePath(resourceType, brokerDB.CustomerCode);

            if (File.Exists(filePath))
            {
                return filePath;
            }

            return this.GetGenericFilePath(resourceType);
        }

        /// <summary>
        /// Gets the contents for the given resource. The resource can be the generic or custom one depending
        /// on the broker.
        /// </summary>
        /// <param name="resourceType">The resource type to lookup.</param>
        /// <param name="brokerId">The broker identifier.</param>
        /// <returns>The contents of the resource.</returns>
        public string GetResourceContents(ResourceType resourceType, Guid brokerId)
        {
            string path = this.GetResourcePath(resourceType, brokerId);

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            return File.ReadAllText(path);
        }

        /// <summary>
        /// Gets a value indicating whether the generic and custom resource location exist.  It also checks
        /// to make sure each generic resource exist.
        /// </summary>
        /// <returns>True if both custom and generic resource locations are configured and if all the generic resources exists.</returns>
        public bool AreGenericResourcesAvailable()
        {
            if (!Directory.Exists(this.genericLocation) || !Directory.Exists(this.customLocation))
            {
                return false;
            }

            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
            {
                string genericResourcePath = this.GetGenericFilePath(resourceType);
                if (!File.Exists(genericResourcePath))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the file name of the given resource type.
        /// </summary>
        /// <param name="resourceType">The resource type to look up.</param>
        /// <returns>The filename.</returns>
        private string GetFileName(ResourceType resourceType)
        {
            switch (resourceType)
            {
                case ResourceType.PmlApprovalLetterXslt:
                    return "PmlApprovalLetter.xslt.config";
                case ResourceType.SuspenseNoticeXslt:
                    return "SuspenseNotice.xslt.config";
                case ResourceType.PmlCertificateXslt:
                    return "pml_certificate.xslt.config";
                case ResourceType.PmlLoanViewXslt:
                    return "pml_loanview.xslt.config";
                case ResourceType.PmlRateLockConfirmationXslt:
                    return "PmlRateLockConfirmation.xslt.config";
                case ResourceType.FindingsXslt:
                    return "Findings.xslt.config";
                case ResourceType.LoanComparisonCertXslt:
                    return "LoanComparisonCert.xslt.config";
                case ResourceType.LpCreditInfileXslt:
                    return "LpCreditInfile.xsl.config";
                case ResourceType.LpDocCheckListXslt:
                    return "LpDocCheckList.xsl.config";
                case ResourceType.LpFullFeedbackXslt:
                    return "LpFullFeedback.xsl.config";
                case ResourceType.LpMergedCreditXslt:
                    return "LpMergedCredit.xsl.config";
                case ResourceType.RateMonitorCertXslt:
                    return "RateMonitorCert.xslt.config";
                case ResourceType.FaccCountyInfoXml:
                    return "facc_county_info.xml";
                case ResourceType.CertificateCss:
                    return "certificate.css";
                case ResourceType.SigFromCreditDataXsltFile:
                    return "SIG_From_CreditData.xslt.config";
                case ResourceType.CreditDataFromMismo231XsltFile:
                    return "CreditData_From_Mismo231.xslt.config";
                default:
                    throw new UnhandledEnumException(resourceType);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given resource type can be unique per client. 
        /// Currently pml_custom has files that are custom css and custom logos. The certificate style sheet should not be overwritten
        /// because it is used by many certificates.
        /// </summary>
        /// <param name="resourceType">The resource type to check.</param>
        /// <returns>True if the resource can be customized.</returns>
        private bool CanBeCustom(ResourceType resourceType)
        {
            switch (resourceType)
            {
                case ResourceType.SuspenseNoticeXslt:
                case ResourceType.PmlRateLockConfirmationXslt:
                case ResourceType.PmlApprovalLetterXslt:
                case ResourceType.PmlLoanViewXslt:
                case ResourceType.PmlCertificateXslt:
                    return true;
                case ResourceType.FindingsXslt:
                case ResourceType.LoanComparisonCertXslt:
                case ResourceType.LpCreditInfileXslt:
                case ResourceType.LpDocCheckListXslt:
                case ResourceType.LpFullFeedbackXslt:
                case ResourceType.LpMergedCreditXslt:
                case ResourceType.RateMonitorCertXslt:
                case ResourceType.FaccCountyInfoXml:
                case ResourceType.CertificateCss:
                case ResourceType.CreditDataFromMismo231XsltFile:
                case ResourceType.SigFromCreditDataXsltFile:
                    return false;
                default:
                    throw new UnhandledEnumException(resourceType);
            }
        }

        /// <summary>
        /// Sees if the path exist if so it returns it otherwise it tries to map and returns that. 
        /// If the mapping fails it returns the original path.
        /// </summary>
        /// <param name="path">A absolute or relative path to.</param>
        /// <returns>The real path.</returns>
        private string TryToMapPath(string path)
        {
            if (Directory.Exists(path))
            {
                return path;
            }

            try
            {
                return Tools.GetServerMapPath(path);
            }
            catch (HttpException)
            {
                return path;
            }
        }

        /// <summary>
        /// Gets the subpath for the given resource type. The PmlShared folder uses folder structure
        /// to separate the files. It is only one level deep.
        /// </summary>
        /// <param name="resourceType">The resource to look for.</param>
        /// <returns>The sub directory in the generic resource directory.</returns>
        private string GetGenericFilePath(ResourceType resourceType)
        {
            string name = this.GetFileName(resourceType);
            string folder = string.Empty;

            switch (resourceType)
            {
                case ResourceType.PmlApprovalLetterXslt:
                case ResourceType.SuspenseNoticeXslt:
                case ResourceType.PmlCertificateXslt:
                case ResourceType.PmlLoanViewXslt:
                case ResourceType.PmlRateLockConfirmationXslt:
                case ResourceType.FindingsXslt:
                case ResourceType.LoanComparisonCertXslt:
                case ResourceType.LpCreditInfileXslt:
                case ResourceType.LpDocCheckListXslt:
                case ResourceType.LpFullFeedbackXslt:
                case ResourceType.LpMergedCreditXslt:
                case ResourceType.RateMonitorCertXslt:
                    folder = "xslt";
                    break;
                case ResourceType.FaccCountyInfoXml:
                    folder = "title";
                    break;
                case ResourceType.CertificateCss:
                    folder = "css";
                    break;
                case ResourceType.SigFromCreditDataXsltFile:
                case ResourceType.CreditDataFromMismo231XsltFile:
                    folder = "credit";
                    break;
                default:
                    throw new UnhandledEnumException(resourceType);
            }

            return Path.Combine(this.genericLocation, folder, name);
        }

        /// <summary>
        /// Generates and returns the custom path for the given resource and customer code.
        /// </summary>
        /// <param name="resourceType">The resource type.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>A rooted path to the custom resource.</returns>
        private string GetCustomFilePath(ResourceType resourceType, string customerCode)
        {
            string name = this.GetFileName(resourceType);
            return Path.Combine(this.customLocation, customerCode, name);
        }
    }
}
