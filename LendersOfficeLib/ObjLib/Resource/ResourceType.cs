﻿namespace LendersOffice.ObjLib.Resource
{
    /// <summary>
    /// An enumeration with the types of files currently in the PmlShared folder. 
    /// </summary>
    public enum ResourceType
    {
        /// <summary>
        /// The xslt used for generating PML approval certificates.
        /// </summary>
        PmlApprovalLetterXslt = 0,

        /// <summary>
        /// The xslt used for generating suspense notices.
        /// </summary>
        SuspenseNoticeXslt = 1,

        /// <summary>
        /// The xslt used for generating the PML certificates.
        /// </summary>
        PmlCertificateXslt = 2,

        /// <summary>
        /// The xslt used for generating PML status summary certificates. 
        /// </summary>
        PmlLoanViewXslt = 3,

        /// <summary>
        /// The xslt used for generating a rate lock confirmation certificates.
        /// </summary>
        PmlRateLockConfirmationXslt = 4,

        /// <summary>
        /// The xslt used for generating DU findings certificates.
        /// </summary>
        FindingsXslt = 5,

        /// <summary>
        /// The xslt used for generating loan comparison certificates.
        /// </summary>
        LoanComparisonCertXslt = 6,

        /// <summary>
        /// The xslt used for LP credit.
        /// </summary>
        LpCreditInfileXslt = 7,

        /// <summary>
        /// The xslt used for LP document certificates.
        /// </summary>
        LpDocCheckListXslt = 8,

        /// <summary>
        /// The xslt used for LP full feedback certificates.
        /// </summary>
        LpFullFeedbackXslt = 9,

        /// <summary>
        /// The xslt used for LP merged credit certificates.
        /// </summary>
        LpMergedCreditXslt = 10,

        /// <summary>
        /// The xslt used for rate monitor certificates.
        /// </summary>
        RateMonitorCertXslt = 11,

        /// <summary>
        /// The list of counties that are supported by first american.
        /// </summary>
        FaccCountyInfoXml = 12,

        /// <summary>
        /// The stylesheet used for some of the certificates.
        /// </summary>
        CertificateCss = 13,

        /// <summary>
        /// A xslt to convert from our credit report to a mismo credit report.
        /// </summary>
        CreditDataFromMismo231XsltFile = 14,

        /// <summary>
        /// A xslt to convert from our credit report to a Fannie Mae credit report.
        /// </summary>
        SigFromCreditDataXsltFile = 15,
    }
}
