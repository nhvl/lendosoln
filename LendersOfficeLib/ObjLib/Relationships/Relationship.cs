﻿// <copyright file="Relationship.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/2/2014 11:25:51 AM 
// </summary>
namespace LendersOffice.ObjLib.Relationships
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a relationship to another user.
    /// </summary>
    public class Relationship
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Relationship" /> class.
        /// Each relationship is extracted and added as an individual entry.
        /// </summary>
        /// <param name="roleType">The role of the relationship.</param>
        /// <param name="relationshipType">The type of the relationship.</param>
        /// <param name="employeeId">The employee id of the user in the relationship.</param>
        internal Relationship(E_RoleT roleType, E_RelationshipType relationshipType, Guid employeeId)
        {
            this.Type = relationshipType;
            this.EmployeeId = employeeId;

            // 8/29/2013 dd - E_RelationshipType.Invalid and E_RelationshipType.Supervisor
            // are included here for as support for legacy. I don't know which code using
            // this. Updated by gf 10/2/14
            switch (relationshipType)
            {
                case E_RelationshipType.Invalid:
                    this.Role = null;
                    break;
                case E_RelationshipType.Supervisor: 
                    this.Role = null;
                    break;
                case E_RelationshipType.TeamMember:
                case E_RelationshipType.TeamHelper:
                    this.Role = Role.Get(roleType);
                    break;
                default:
                    throw new UnhandledEnumException(relationshipType);
            }
        }

        /// <summary>
        /// Gets the Role for this relationship.
        /// </summary>
        /// <value>
        /// The Role for this relationship.
        /// </value>
        public Role Role { get; private set; }

        /// <summary>
        /// Gets the E_RoleT of the relationship.
        /// </summary>
        /// <value>The E_RoleT of the relationship.</value>
        public E_RoleT RoleT
        {
            get
            {
                if (Role == null)
                {
                    throw CBaseException.GenericException("Not a valid role. Type=[" + this.Type + "]");
                }

                return Role.RoleT;
            }
        }

        /// <summary>
        /// Gets the type of the relationship.
        /// </summary>
        /// <value>The type of the relationship.</value>
        public E_RelationshipType Type { get; private set; }

        /// <summary>
        /// Gets the id of the employee in the relationship.
        /// </summary>
        /// <value>The id of the employee in the relationship.</value>
        public Guid EmployeeId { get; private set; }
    }
}
