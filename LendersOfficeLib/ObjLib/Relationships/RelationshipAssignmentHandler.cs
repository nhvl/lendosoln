﻿// <copyright file="RelationshipAssignmentHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/23/2014 5:14:40 PM 
// </summary>
namespace LendersOffice.ObjLib.Relationships
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.LockPolicies;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.Security;

    /// <summary>
    /// Provides functionality to assign roles to a loan file based on employee relationships.
    /// </summary>
    public class RelationshipAssignmentHandler
    {
        /// <summary>
        /// The id of the loan.
        /// </summary>
        private Guid loanID;

        /// <summary>
        /// The id of the broker for the loan.
        /// </summary>
        private Guid brokerID;

        /// <summary>
        /// The id of the employee performing the action that requires 
        /// relationship assignment.
        /// </summary>
        private Guid employeeID;

        /// <summary>
        /// The loan assignments that will be used for lazy loading.
        /// </summary>
        private LoanAssignmentContactTable cachedLoanAssignments = null;

        /// <summary>
        /// The broker user permissions used for lazy loading.
        /// </summary>
        private BrokerUserPermissions cachedBrokerUserPermissions = null;

        /// <summary>
        /// The employee roles used for lazy loading.
        /// </summary>
        private EmployeeRoles cachedEmployeeRoles = null;

        /// <summary>
        /// Initializes a new instance of the RelationshipAssignmentHandler class.
        /// </summary>
        /// <param name="brokerID">The id of the broker.</param>
        /// <param name="loanID">The id of the loan.</param>
        /// <param name="employeeID">
        /// The employee performing the action that requires relationship assignment.
        /// </param>
        public RelationshipAssignmentHandler(Guid brokerID, Guid loanID, Guid employeeID)
        {
            this.brokerID = brokerID;
            this.loanID = loanID;
            this.employeeID = employeeID;
        }

        /// <summary>
        /// Gets the assignments of the loan.
        /// </summary>
        /// <value>The assignments of the loan.</value>
        protected LoanAssignmentContactTable LoanAssignments
        {
            get
            {
                if (this.cachedLoanAssignments == null)
                {
                    this.cachedLoanAssignments = new LoanAssignmentContactTable(this.brokerID, this.loanID);
                }

                return this.cachedLoanAssignments;
            }
        }

        /// <summary>
        /// Gets the permissions of the user that initiated the relationship assignment.
        /// </summary>
        /// <value>The permissions of the user that initiated the relationship assignment.</value>
        protected BrokerUserPermissions BrokerUserPermissions
        {
            get
            {
                if (this.cachedBrokerUserPermissions == null)
                {
                    this.cachedBrokerUserPermissions = new BrokerUserPermissions();
                    this.cachedBrokerUserPermissions.Retrieve(
                        this.brokerID,
                        this.employeeID);
                }

                return this.cachedBrokerUserPermissions;
            }
        }

        /// <summary>
        /// Gets the roles of the employee who initiated relationship assignment.
        /// </summary>
        /// <value>The roles of the employee who initiated relationship assignment.</value>
        protected EmployeeRoles EmployeeRoles
        {
            get
            {
                if (this.cachedEmployeeRoles == null)
                {
                    this.cachedEmployeeRoles = new EmployeeRoles(
                        this.brokerID,
                        this.employeeID);
                }

                return this.cachedEmployeeRoles;
            }
        }

        /// <summary>
        /// Determines which relationships need to be assigned to the loan file
        /// and saves the loan after assignment.
        /// </summary>
        /// <param name="priceGroupID">The id of the price group.</param>
        /// <param name="isRequestingRateLock">Indicates whether a rate lock is being requested.</param>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="correspondentProcessT">The correspondent process type of the loan.</param>
        public void AssignSubmissionRelationships(
            Guid? priceGroupID,
            bool isRequestingRateLock,
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            Guid? includeLockDeskEmployeeID = null;

            var employeeIDs = this.GetEmployeesToCheckForSubmission(
                channelT,
                out includeLockDeskEmployeeID);

            var finalRelationshipSet = new RelationshipSet();

            foreach (var employeeID in employeeIDs)
            {
                var relationships = new EmployeeRelationships();
                relationships.Retrieve(this.brokerID, employeeID);

                bool includeLockDesk = includeLockDeskEmployeeID.HasValue && 
                    employeeID == includeLockDeskEmployeeID;

                var relationshipSet = relationships.GetApplicableRelationshipsForSubmission(
                    this.LoanAssignments,
                    channelT,
                    correspondentProcessT,
                    includeLockDesk);

                finalRelationshipSet.Union(relationshipSet);
            }

            if (!this.LoanAssignments.Contains(E_RoleT.LockDesk) &&
                !finalRelationshipSet.Contains(E_RoleT.LockDesk) &&
                priceGroupID.HasValue && isRequestingRateLock)
            {
                var defaultLockDeskRelationship = this.GetDefaultLockDeskRelationship(priceGroupID.Value);
                finalRelationshipSet.Add(defaultLockDeskRelationship);
            }

            this.AssignRolesToFile(finalRelationshipSet, fromSubmission: true);
        }

        /// <summary>
        /// Assigns employees and teams to file based on creation relationships.
        /// </summary>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="correspondentProcessT">The correspondent process type of the loan.</param>
        public void AssignCreationRelationships(
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            var employeeRelationships = new EmployeeRelationships();
            employeeRelationships.Retrieve(this.brokerID, this.employeeID);

            var relationshipSet = employeeRelationships.GetApplicableRelationshipsForCreation(
                this.EmployeeRoles,
                channelT,
                correspondentProcessT);
            var finalRelationshipSet = new RelationshipSet();

            var teamsByEmployeeId = new Dictionary<Guid, IEnumerable<Team>>();

            foreach (var relationship in relationshipSet.Relationships)
            {
                var otherEmployee = new EmployeeDetails();
                otherEmployee.Retrieve(this.brokerID, relationship.EmployeeId);

                if (otherEmployee.IsActive)
                {
                    finalRelationshipSet.Add(relationship);

                    IEnumerable<Team> teams;

                    if (!teamsByEmployeeId.TryGetValue(relationship.EmployeeId, out teams))
                    {
                        teams = Team.ListPrimaryTeamByUserInRole(this.brokerID, relationship.EmployeeId, null);
                        teamsByEmployeeId.Add(relationship.EmployeeId, teams);
                    }

                    Team teamToAssign = teams.FirstOrDefault(p => p.RoleId == relationship.Role.Id);

                    if (teamToAssign != null)
                    {
                        Team.SetLoanAssignment(this.brokerID, teamToAssign.Id, this.loanID);
                    }
                }
            }

            this.AssignRolesToFile(finalRelationshipSet);
        }

        /// <summary>
        /// Gets the ids of the employees whose relationships we will want to check
        /// for the submission relationships.
        /// </summary>
        /// <param name="channelT">The channel of the loan.</param>
        /// <param name="includeLockDeskEmployeeID">Indicates whether the lock desk relationship should be included.</param>
        /// <returns>The ids of the employees whose relationships need to be checked.</returns>
        protected IEnumerable<Guid> GetEmployeesToCheckForSubmission(
            E_BranchChannelT channelT,
            out Guid? includeLockDeskEmployeeID)
        {
            var employeeIDs = new HashSet<Guid>();

            includeLockDeskEmployeeID = null;

            if (this.LoanAssignments.Contains(E_RoleT.LoanOfficer) &&
                channelT != E_BranchChannelT.Correspondent)
            {
                includeLockDeskEmployeeID = this.LoanAssignments.FindByRoleT(E_RoleT.LoanOfficer).EmployeeId;
                employeeIDs.Add(includeLockDeskEmployeeID.Value);
            }
            else if (channelT == E_BranchChannelT.Correspondent &&
                this.LoanAssignments.Contains(E_RoleT.Pml_Secondary))
            {
                includeLockDeskEmployeeID = this.LoanAssignments.FindByRoleT(E_RoleT.Pml_Secondary).EmployeeId;
                employeeIDs.Add(includeLockDeskEmployeeID.Value);
            }

            if (this.LoanAssignments.Contains(E_RoleT.LenderAccountExecutive))
            {
                var accountExecEmployeeID = this.LoanAssignments.FindByRoleT(E_RoleT.LenderAccountExecutive).EmployeeId;
                employeeIDs.Add(accountExecEmployeeID);
            }

            employeeIDs.Add(this.employeeID);

            return employeeIDs;
        }

        /// <summary>
        /// Gets the default lock desk relationship from the lock policy.
        /// </summary>
        /// <param name="priceGroupID">The id of the price group.</param>
        /// <returns>A default lock desk Relationship.</returns>
        protected Relationship GetDefaultLockDeskRelationship(Guid priceGroupID)
        {
            var priceGroup = PriceGroup.RetrieveByID(priceGroupID, this.brokerID);
            var lockPolicy = LockPolicy.Retrieve(this.brokerID, priceGroup.LockPolicyID.Value);

            return new Relationship(
                E_RoleT.LockDesk,
                E_RelationshipType.TeamHelper,
                lockPolicy.DefaultLockDeskIDOrInheritedID);
        }

        /// <summary>
        /// Assigns the relationships and saves the loan.
        /// </summary>
        /// <param name="finalRelationshipSet">The set of relationships to apply.</param>
        /// <param name="fromSubmission">The bool that determines whether the function call is from submission.</param>
        protected void AssignRolesToFile(RelationshipSet finalRelationshipSet, bool fromSubmission = false)
        {
            if (!finalRelationshipSet.Relationships.Any())
            {
                return;
            }

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(RelationshipAssignmentHandler));
            loan.AllowSaveWhileQP2Sandboxed = true;
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            foreach (var relationship in finalRelationshipSet.Relationships)
            {
                switch (relationship.RoleT)
                {
                    case E_RoleT.Manager:
                        loan.sEmployeeManagerId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Processor:
                        loan.sEmployeeProcessorId = relationship.EmployeeId;
                        break;
                    case E_RoleT.JuniorProcessor:
                        loan.sEmployeeJuniorProcessorId = relationship.EmployeeId;
                        break;
                    case E_RoleT.LenderAccountExecutive:
                        loan.sEmployeeLenderAccExecId = relationship.EmployeeId;
                        break;
                    case E_RoleT.LoanOfficerAssistant:
                        loan.sEmployeeLoanOfficerAssistantId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Pml_BrokerProcessor:
                        loan.sEmployeeBrokerProcessorId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Pml_PostCloser:
                        loan.sEmployeeExternalPostCloserId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Underwriter:
                        loan.sEmployeeUnderwriterId = relationship.EmployeeId;
                        if (fromSubmission)
                        {
                            this.AssignOfficialContact(E_AgentRoleT.Underwriter, loan, relationship.EmployeeId);
                        }

                        break;
                    case E_RoleT.JuniorUnderwriter:
                        loan.sEmployeeJuniorUnderwriterId = relationship.EmployeeId;
                        if (fromSubmission)
                        {
                            this.AssignOfficialContact(E_AgentRoleT.JuniorUnderwriter, loan, relationship.EmployeeId);
                        }

                        break;
                    case E_RoleT.LockDesk:
                        loan.sEmployeeLockDeskId = relationship.EmployeeId;
                        break;
                    case E_RoleT.CreditAuditor:
                        loan.sEmployeeCreditAuditorId = relationship.EmployeeId;
                        break;
                    case E_RoleT.LegalAuditor:
                        loan.sEmployeeLegalAuditorId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Purchaser:
                        loan.sEmployeePurchaserId = relationship.EmployeeId;
                        break;
                    case E_RoleT.Secondary:
                        loan.sEmployeeSecondaryId = relationship.EmployeeId;
                        break;
                    default:
                        throw new UnhandledEnumException(relationship.RoleT);
                }
            }

            loan.Save();
        }

        /// <summary>
        /// Assigns the role to the file directly via the database.
        /// </summary>
        /// <param name="roleID">The id of the role to assign.</param>
        /// <param name="employeeID">The id of the employee to assign to the role.</param>
        /// <param name="loanID">The id of the loan.</param>
        protected void AssignRoleToFile(Guid roleID, Guid employeeID, Guid loanID)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@RoleId", roleID),
                new SqlParameter("@EmployeeId", employeeID),
                new SqlParameter("@LoanId", loanID)
            };

            int numRetries = 3;

            StoredProcedureHelper.ExecuteNonQuery(
                this.brokerID,
                "AssignRoleToLoan",
                numRetries,
                parameters);
        }

        /// <summary>
        /// Assigns official contact information for a given role and employee id.
        /// </summary>
        /// <param name="agentRole">Role of the agent.</param>
        /// <param name="loan">Loan to assign official contact to.</param>
        /// <param name="employeeId">Employee id to get the data for agent from.</param>
        protected void AssignOfficialContact(E_AgentRoleT agentRole, CPageData loan, Guid employeeId)
        {
            CAgentFields agent = loan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.CreateNew);
            if (agent.IsNewRecord)
            {
                CommonFunctions.CopyEmployeeInfoToAgent(this.brokerID, agent, employeeId);
                agent.Update();
            }
        }
    }
}
