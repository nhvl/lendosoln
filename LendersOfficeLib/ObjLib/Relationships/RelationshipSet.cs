﻿// <copyright file="RelationshipSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/2/2014 11:24:18 AM 
// </summary>
namespace LendersOffice.ObjLib.Relationships
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Contains a set of relationships.
    /// </summary>
    public class RelationshipSet
    {
        /// <summary>
        /// Maps role type to relationship.
        /// </summary>
        private Dictionary<E_RoleT, Relationship> relationshipsByRole;

        /// <summary>
        /// Initializes a new instance of the <see cref="RelationshipSet"/> class.
        /// </summary>
        public RelationshipSet()
        {
            this.relationshipsByRole = new Dictionary<E_RoleT, Relationship>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RelationshipSet"/> class.
        /// </summary>
        /// <param name="relationships">The initial set of relationships.</param>
        protected RelationshipSet(IEnumerable<Relationship> relationships)
        {
            if (relationships == null)
            {
                throw new ArgumentNullException();
            }
            else if (!relationships.Any())
            {
                this.relationshipsByRole = new Dictionary<E_RoleT, Relationship>();
            }
            else
            {
                this.relationshipsByRole = relationships.ToDictionary(r => r.RoleT);
            }
        }

        /// <summary>
        /// Gets an enumerable of all of the relationships in the set.
        /// </summary>
        /// <value>An enumerable of all of the relationships in the set.</value>
        public IEnumerable<Relationship> Relationships
        {
            get { return this.relationshipsByRole.Values; }
        }

        /// <summary>
        /// Gets the relationship for the role type.
        /// </summary>
        /// <param name="roleType">The role for the target relationship.</param>
        /// <returns>Returns the relationship, if it exists. Otherwise, null.</returns>
        public Relationship this[E_RoleT roleType]
        {
            get
            {
                Relationship r = null;

                this.relationshipsByRole.TryGetValue(roleType, out r);

                return r;
            }
        }

        /// <summary>
        /// Adds a relationship to the set. Enforces that there is only one 
        /// relationship per role.
        /// </summary>
        /// <param name="roleType">The role for the relationship.</param>
        /// <param name="relationshipType">The type of the relationship.</param>
        /// <param name="employeeID">The id of the employee in the relationship.</param>
        public void Add(E_RoleT roleType, E_RelationshipType relationshipType, Guid employeeID)
        {
            var relationship = new Relationship(roleType, relationshipType, employeeID);

            this.Add(relationship);
        }

        /// <summary>
        /// Returns a value indicating whether this set contains a relationship 
        /// for the given role.
        /// </summary>
        /// <param name="roleType">The role of the relationship to check.</param>
        /// <returns>True if the role is already in the set.</returns>
        public bool Contains(E_RoleT roleType)
        {
            // TODO: The null check is to support legacy code. Decide what we
            // want to do here...
            return this.relationshipsByRole.ContainsKey(roleType)
                && this.relationshipsByRole[roleType].Role != null;
        }

        /// <summary>
        /// Unions the relationships with the other set of relationships. Adds
        /// relationships whose roles were not already included in this set of
        /// relationships.
        /// </summary>
        /// <param name="otherRelationships">The other set of relationships.</param>
        public void Union(RelationshipSet otherRelationships)
        {
            var newRelationships = otherRelationships.Relationships.Where(r => !this.Contains(r.RoleT));

            foreach (var relationship in newRelationships)
            {
                this.Add(relationship);
            }
        }

        /// <summary>
        /// Returns a new RelationshipSet where each relationship satisfies the filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>A new RelationshipSet where each relationship satisfies the filter.</returns>
        public RelationshipSet Where(Func<Relationship, bool> filter)
        {
            var applicableRelationships = this.Relationships.Where(r => filter(r));

            return new RelationshipSet(applicableRelationships);
        }

        /// <summary>
        /// Adds a relationship to the set. Enforces that there is only one 
        /// relationship per role.
        /// </summary>
        /// <param name="relationship">The relationship to add.</param>
        public void Add(Relationship relationship)
        {
            this.relationshipsByRole.Add(relationship.RoleT, relationship);
        }
    }
}
