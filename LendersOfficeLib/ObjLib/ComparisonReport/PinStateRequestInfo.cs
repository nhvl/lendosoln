﻿namespace LendersOffice.ObjLib.ComparisonReport
{
    using System;

    /// <summary>
    /// Represents a mapping between a pin state and a pricing submission request.
    /// </summary>
    public sealed class PinStateRequestInfo
    {
        public Guid StateId { get; set; }
        public bool HasSecondLoanInfo { get; set; }
        public Guid FirstLoanRequestId { get; set; }
        public Guid? SecondLoanRequestId { get; set; }
    }
}

