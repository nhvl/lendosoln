﻿namespace LendersOffice.ObjLib.ComparisonReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ComparisonRunData
    {
        public List<PinStateRequestInfo> Requests { get; set; }

        public Guid sProdLpePriceGroupId { get; set; }

        public ComparisonRunData()
        {

        }

        public ComparisonRunData(Guid sProdLpePriceGroupId, List<PinStateRequestInfo> requests)
        {
            this.Requests = requests;
            this.sProdLpePriceGroupId = sProdLpePriceGroupId;
        }
    }
}
