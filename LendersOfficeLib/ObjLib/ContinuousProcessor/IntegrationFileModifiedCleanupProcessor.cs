﻿// <copyright file="IntegrationFileModifiedCleanupProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   6/10/2014 3:44:39 PM 
// </summary>
namespace LendersOffice.ContinuousProcessor
{
    using System;
    using CommonProjectLib.Runnable;
    using DataAccess;

    /// <summary>
    /// A continuous process that clean up modified loans past 3 months period. See OPM 183920.
    /// </summary>
    public class IntegrationFileModifiedCleanupProcessor : IRunnable
    {
        /// <summary>
        /// Gets the description of the continuous process.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Clean up modified loans past 3 months period."; }
        }

        /// <summary>
        /// Delete modified loans past 3 months period.
        /// </summary>
        public void Run()
        {
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                StoredProcedureHelper.ExecuteNonQuery(connInfo, "Integration_File_Modified_CleanUp", 2, null);
            }
        }
    }
}