﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace LendersOffice.PdfLayout
{
    public class PdfFormLayout
    {

        public PdfFormLayout()
        {

        }

        public PdfFormLayout(string xml)
        {
            using (StringReader sr = new StringReader(xml))
            using (XmlReader reader = XmlReader.Create(sr))
            {
                LoadImpl(reader);
            }
        }

        private List<PdfField> m_fieldList = new List<PdfField>();
        public void Clear()
        {
            m_fieldList = new List<PdfField>();
        }

        public bool IsContainSignature
        {
            get { return IsContainBorrowerSignature || IsContainCoborrowerSignature; }
        }

        public bool IsContainBorrowerSignature
        {
            get { return IsContains(PdfFieldType.Signature, "aBSignature"); }
        }

        public bool IsContainCoborrowerSignature
        {
            get { return IsContains(PdfFieldType.Signature, "aCSignature"); }
        }

        private bool IsContains(PdfFieldType type, string fieldId)
        {
            foreach (var field in m_fieldList)
            {
                if (field.Type == type && field.Name.Equals(fieldId, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
        public void Add(PdfField field)
        {
            if (null == field || string.IsNullOrEmpty(field.Name))
            {
                return;
            }
            m_fieldList.Add(field);
        }
        public void Load(string path)
        {
            using (XmlReader reader = new XmlTextReader(new StreamReader(path)))
            {
                LoadImpl(reader);
            }
        }
        public void Load(Stream stream)
        {
            using (XmlReader reader = new XmlTextReader(stream))
            {
                LoadImpl(reader);
            }
        }
        public void LoadContent(string content)
        {
            using (XmlReader reader = new XmlTextReader(new StringReader(content)))
            {
                LoadImpl(reader);
            }
        }

        private void LoadImpl(XmlReader reader)
        {
            m_fieldList = new List<PdfField>();
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "pdf_fields")
            {
                reader.Read();
                if (reader.EOF) return;

            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "pdf_fields") return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name)
                {
                    case "pdf_field":
                        PdfField o = new PdfField();
                        o.ReadXml(reader);
                        Add(o);
                        break;

                }
            }

        }
        public string GetXmlContent()
        {
            using (StringWriter writer = new StringWriter())
            {
                Save(writer);
                writer.Flush();
                return writer.ToString();
            }
        }
        public void Save(string outputPath)
        {
            using (XmlTextWriter writer = new XmlTextWriter(outputPath, Encoding.ASCII))
            {
                SaveImpl(writer);
            }
        }
        public void Save(Stream stream)
        {
            using (XmlWriter writer = new XmlTextWriter(stream, Encoding.ASCII))
            {
                SaveImpl(writer);
            }
        }
        public void Save(TextWriter output)
        {
            using (XmlWriter writer = new XmlTextWriter(output))
            {
                SaveImpl(writer);
            }
        }
        private void SaveImpl(XmlWriter writer)
        {
            writer.WriteStartElement("pdf_fields");
            foreach (PdfField o in m_fieldList)
            {
                o.WriteXml(writer);
            }
            writer.WriteEndElement();

        }

        public IEnumerable<PdfField> FieldList
        {
            get { return m_fieldList; }
        }

        public PdfField FindSpecialField(string fieldId)
        {
            foreach (var field in m_fieldList)
            {
                if (field.Name.Equals(fieldId, StringComparison.OrdinalIgnoreCase))
                {
                    return field;
                }
            }
            return null;
        }
    }
}
