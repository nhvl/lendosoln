﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.PdfLayout
{
    public enum PdfFieldVAlignType
    {
        Top,
        Center,
        Bottom
    }
}
