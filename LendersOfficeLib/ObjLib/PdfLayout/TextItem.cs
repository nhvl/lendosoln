﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;

namespace LendersOffice.PdfLayout
{
    class TextItem
    {
        public BaseFont Font { get; private set; }
        public float FontSize { get; private set; }
        public string Content { get; private set; }

        public TextItem(BaseFont font, float fontSize, string content)
        {
            this.Font = font;
            this.FontSize = fontSize;
            this.Content = content;
        }
        public float Width
        {
            get
            {
                return Font.GetWidthPoint(Content, FontSize);
            }
        }
        public float Height
        {
            get
            {
                if (Content == string.Empty)
                {
                    return Font.GetHeightPoint("I", FontSize); // 7/11/2011 dd - Return height of the tallest letter.
                }
                return Font.GetHeightPoint(Content, FontSize);
            }
        }
    }
}
