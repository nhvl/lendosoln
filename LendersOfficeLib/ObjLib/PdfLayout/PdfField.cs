﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CustomFormField;


namespace LendersOffice.PdfLayout
{
    [DataContract]
    public class PdfField
    {
        private string setDescription = null;

        public PdfField() { }
        /// <summary>
        /// Copy constructor, creates a copy of other.
        /// </summary>
        /// <param name="other"></param>
        public PdfField(PdfField other)
        {
            this.Name = other.Name;
            this.Value = other.Value;
            this.Type = other.Type;
            this.Rectangle = other.Rectangle;
            this.lx = other.lx;
            this.ly = other.ly;
            this.ux = other.ux;
            this.uy = other.uy;
            this.HasBorder = other.HasBorder;
            this.Align = other.Align;
            this.VAlign = other.VAlign;
            this.Font = other.Font;
            this.FontSize = other.FontSize;
            this.PageNumber = other.PageNumber;
        }

        private const int SignatureMinWidth = 200;
        private const int SignatureMinHeight = 20;
        private const int InitialsMinWidth = 130;
        private const int InitialsMinHeight = 12;
        private const int SignatureDateMinWidth = 130;
        private const int SignatureDateMinHeight = 12;
        [DataMember]
        public string Name { get; set; }

        public string NameUseForRendering
        {
            get
            {
                if (Type == PdfFieldType.Checkbox)
                {
                    return Name.Split(':')[0];
                }
                else
                {
                    return Name;
                }
            }
        }
        public string ValueUseForRendering
        {
            get
            {
                if (Type == PdfFieldType.Checkbox)
                {
                    string[] parts = Name.Split(':');
                    if (parts.Length == 2)
                    {
                        return Name.Split(':')[1];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        [DataMember]
        private string Description
        {
            get
            {
                FormField field = FormField.RetrieveById(Name);
                if (null == field)
                {
                    if (string.IsNullOrEmpty(this.setDescription))
                    {
                        return Name;
                    }
                    else
                    {
                        return this.setDescription;
                    }
                }
                else
                {
                    return field.FriendlyName;
                }
            }
            set
            {
                this.setDescription = value;
            }
        }


        public PdfFieldType Type { get; set; }

        // 5/20/2010 dd - Only for JSON Serialization and pdfeditor.
        [DataMember]
        private string cssClass
        {
            get
            {
                switch (Type)
                {
                    case PdfFieldType.Text:
                        return "pdf-field-text";
                    case PdfFieldType.Checkbox:
                        return "pdf-field-checkbox";
                    case PdfFieldType.Signature:
                        return "pdf-field-signature";
                    case PdfFieldType.Initial:
                        return "pdf-field-initial";
                    case PdfFieldType.SignatureDate:
                        return "pdf-field-signature-date";
                    case PdfFieldType.TodaysDate:
                        return "pdf-field-date";
                    default:
                        throw new DataAccess.UnhandledEnumException(Type);
                }
            }
            set
            {
                switch (value)
                {
                    case "pdf-field-text":
                        Type = PdfFieldType.Text;
                        break;
                    case "pdf-field-checkbox":
                        Type = PdfFieldType.Checkbox;
                        break;
                    case "pdf-field-signature":
                    case "pdf-field-signature-img":
                        Type = PdfFieldType.Signature;
                        break;
                    case "pdf-field-initial":
                        Type = PdfFieldType.Initial;
                        break;
                    case "pdf-field-signature-date":
                        Type = PdfFieldType.SignatureDate;
                        break;
                    case "pdf-field-date":
                        Type = PdfFieldType.TodaysDate;
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unhandle cssClass for PdfField. cssClass=" + value);
                }
            }
        }

        [DataMember]
        public string Value { get; set; }


        public string TestValue
        {
            get
            {
                switch (Type)
                {
                    case PdfFieldType.Text:
                        E_PageDataFieldType dataFieldType = E_PageDataFieldType.Unknown;
                        if (PageDataUtilities.GetFieldType(Name, out dataFieldType))
                        {
                            switch (dataFieldType)
                            {
                                case E_PageDataFieldType.Money:
                                    return "999,999.99";
                                case E_PageDataFieldType.Percent:
                                    return "99.999";
                                case E_PageDataFieldType.Ssn:
                                    return "111-11-1111";
                                case E_PageDataFieldType.Phone:
                                    return "(111) 111-1111";
                                case E_PageDataFieldType.Integer:
                                    return "9999";
                                case E_PageDataFieldType.DateTime:
                                    return "01/01/2010";
                                case E_PageDataFieldType.Decimal:
                                    return "9999.999";
                                default:
                                    return Name;
                            }
                        }
                        else
                        {
                            return Name;
                        }

                    case PdfFieldType.Checkbox:
                        if (string.IsNullOrEmpty(Value))
                        {
                            return "yes";
                        }
                        return Value;
                    case PdfFieldType.Signature:
                        return string.Empty;
                    case PdfFieldType.Initial:
                        return "JJ";
                    case PdfFieldType.SignatureDate:
                        return "01/01/2010";
                    default:
                        throw new UnhandledEnumException(Type);
                }

            }
        }

        private Rectangle m_rectangle = null;

        public Rectangle Rectangle
        {
            get
            {
                if (null == m_rectangle)
                {
                    m_rectangle = new Rectangle(0, 0, 0, 0);
                }
                return m_rectangle;
            }
            set
            {
                m_rectangle = value;
            }
        }

        [DataMember]
        private float lx
        {
            get { return Rectangle.Left; }
            set { Rectangle.Left = value; }
        }

        [DataMember]
        private float ly
        {
            get { return Rectangle.Bottom; }
            set { Rectangle.Bottom = value; }
        }

        [DataMember]
        private float ux
        {
            get { return Rectangle.Right; }
            set { Rectangle.Right = value; }
        }

        [DataMember]
        private float uy
        {
            get { return Rectangle.Top; }
            set { Rectangle.Top = value; }
        }
        [DataMember]
        public bool HasBorder
        {
            get
            {
                return Rectangle.Border == Rectangle.BOX;
            }
            set
            {
                Rectangle.Border = value ? Rectangle.BOX : Rectangle.NO_BORDER;
                Rectangle.BorderWidth = value ? .2f : 0;
                Rectangle.BorderColor = BaseColor.BLACK;
            }
        }
        [DataMember]
        public PdfFieldAlignType Align { get; set; }

        [DataMember]
        public PdfFieldVAlignType VAlign { get; set; }

        [DataMember]
        public string Font { get; set; }

        [DataMember]
        public float FontSize { get; set; }

        [DataMember(Name = "pg")]
        public int PageNumber { get; set; }


        public override string ToString()
        {
            return string.Format("{0}: Id={1}, Rectangle=[{2},{3},{4},{5}], Font={6},{7}, Border={8}, Page={9}, Value={10}",
                Type, // 0
                Name, // 1
                Rectangle.Left, // 2
                Rectangle.Bottom, // 3
                Rectangle.Right, // 4
                Rectangle.Top, // 5
                Font, // 6
                FontSize, // 7
                HasBorder, // 8
                PageNumber, // 9
                Value // 10
                );
        }

        #region IXmlSerializable Members

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "pdf_field")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            Name = ReadString(reader, "name");
            Value = ReadString(reader, "value");
            Type = ReadEnum(reader, "type", PdfFieldType.Text);
            Align = ReadEnum(reader, "align", PdfFieldAlignType.Left);
            VAlign = ReadEnum(reader, "valign", PdfFieldVAlignType.Top);
            this.Description = ReadString(reader, "description");

            float lx = ReadFloat(reader, "lx", 0);
            float ly = ReadFloat(reader, "ly", 0);
            float ux = ReadFloat(reader, "ux", 0);
            float uy = ReadFloat(reader, "uy", 0);

            // 5/12/2010 dd - Normalize coordinate so that lx < ux and ly < uy
            Rectangle.Left = lx <= ux ? lx : ux;
            Rectangle.Right = lx <= ux ? ux : lx;
            Rectangle.Bottom = ly <= uy ? ly : uy;
            Rectangle.Top = ly <= uy ? uy : ly;

            this.Type = EDocs.PdfFormToEDocConversion.DeterminePdfFieldType(this.Name, this.Type);

            HasBorder = ReadBool(reader, "border");

            Font = ReadString(reader, "font");
            FontSize = ReadFloat(reader, "font_size", -1);
            PageNumber = ReadInt(reader, "page", 1);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "pdf_field")
                {
                    return;
                }
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("pdf_field");
            WriteAttribute(writer, "name", Name);
            WriteAttribute(writer, "description", this.Description);
            WriteAttribute(writer, "type", Type);
            WriteAttribute(writer, "value", Value);
            WriteAttribute(writer, "border", HasBorder);
            if (null != Rectangle)
            {
                WriteAttribute(writer, "lx", Rectangle.Left);
                WriteAttribute(writer, "ly", Rectangle.Bottom);
                WriteAttribute(writer, "ux", Rectangle.Right);
                WriteAttribute(writer, "uy", Rectangle.Top);
            }
            WriteAttribute(writer, "align", Align);
            WriteAttribute(writer, "valign", VAlign);
            WriteAttribute(writer, "font", Font);
            WriteAttribute(writer, "font_size", FontSize);
            WriteAttribute(writer, "page", PageNumber);
            writer.WriteEndElement();
        }
        #endregion

        private T ReadEnum<T>(string s, T defaultValue) where T : struct
        {
            T value = defaultValue;

            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    value = (T)Enum.Parse(typeof(T), s);
                }
                catch { }
            }
            return value;


        }

        private T ReadEnum<T>(XmlReader reader, string attributeName, T defaultValue) where T : struct
        {
            string s = reader.GetAttribute(attributeName);
            return ReadEnum(s, defaultValue);
        }
        private float ReadFloat(XmlReader reader, string attributeName, float defaultValue)
        {
            string s = reader.GetAttribute(attributeName);

            float value;
            if (!float.TryParse(s, out value)) {
                value = defaultValue;
            }
            return value;
        }
        private int ReadInt(XmlReader reader, string attributeName, int defaultValue)
        {
            string s = reader.GetAttribute(attributeName);

            int value;
            if (!int.TryParse(s, out value))
            {
                value = defaultValue;
            }
            return value;
        }
        private bool ReadBool(XmlReader reader, string attributeName)
        {
            string s = reader.GetAttribute(attributeName);
            bool v = false;

            if (!string.IsNullOrEmpty(s))
            {
                s = s.ToLower();
                if (s == "true")
                {
                    v = true;
                }
                else if (s == "false")
                {
                    v = false;
                }
            }
            return v;
        }
        private string ReadString(XmlReader reader, string attributeName)
        {
            string s = reader.GetAttribute(attributeName);
            if (string.IsNullOrEmpty(s))
            {
                s = string.Empty;
            }
            return s;
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, float v)
        {
            WriteAttribute(writer, attributeName, v.ToString());
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, string attributeValue)
        {
            if (!string.IsNullOrEmpty(attributeValue))
            {
                writer.WriteAttributeString(attributeName, attributeValue);
            }
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, Enum type)
        {
            WriteAttribute(writer, attributeName, type.ToString());
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, bool b)
        {
            WriteAttribute(writer, attributeName, b ? "true" : "false");
        }
    }
}
