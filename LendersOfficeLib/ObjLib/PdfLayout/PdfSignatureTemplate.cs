﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.Common;


namespace LendersOffice.PdfLayout
{
    public class DuplicatePdfSignatureTemplateException : CBaseException
    {
        public DuplicatePdfSignatureTemplateException(string name)
            : base("Template " + name + " already existed", "Template " + name + " already existed")
        {
        }
    }
    public class PdfSignatureTemplate
    {
        private PdfFormLayout m_pdfFormLayout = new PdfFormLayout();
        private bool m_isNew = false;

        public Guid BrokerId { get; private set; }
        public Guid OwnerUserId { get; private set; }
        public Guid TemplateId { get; private set; }
        
        public string Name { get; set; }

        public PdfFormLayout FormLayout
        {
            get { return m_pdfFormLayout; }
        }

        private bool HasDuplicateName
        {
            get
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@TemplateId", TemplateId),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@OwnerUserId", OwnerUserId),
                                            new SqlParameter("@Name", Name)
                                        };

                bool ret = false;
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "SignatureTemplate_CheckName", parameters))
                {
                    if (reader.Read())
                    {
                        ret = true;
                    }
                }

                return ret;
            }
        }
        public void Save()
        {
            if (HasDuplicateName)
            {
                throw new DuplicatePdfSignatureTemplateException(Name);
            }
            else
            {
                string storedProcedureName = m_isNew ? "SignatureTemplate_Create" : "SignatureTemplate_Update";

                SqlParameter[] parameters = {
                                            new SqlParameter("@TemplateId", TemplateId),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@OwnerUserId", OwnerUserId),
                                            new SqlParameter("@Name", Name),
                                            new SqlParameter("@FieldMetadata", FormLayout.GetXmlContent())
                                        };
                StoredProcedureHelper.ExecuteNonQuery(BrokerId, storedProcedureName, 3, parameters);
            }
        }

        public void Delete()
        {
            SqlParameter[] paramters = {
                                            new SqlParameter("@TemplateId", TemplateId),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@OwnerUserId", OwnerUserId)
                                       };

            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "SignatureTemplate_DeleteById", 3, paramters);
        }

        public static PdfSignatureTemplate RetrieveByTemplateId(Guid templateId)
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.Generic, "Principal cannot be null");
            }

            PdfSignatureTemplate template = null;

            SqlParameter[] parameters = {
                                            new SqlParameter("@TemplateId", templateId),
                                            new SqlParameter("@BrokerId", principal.BrokerId),
                                            new SqlParameter("@OwnerUserId", principal.UserId)

                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "SignatureTemplate_RetrieveById", parameters))
            {
                if (reader.Read())
                {
                    template = new PdfSignatureTemplate();
                    template.BrokerId = principal.BrokerId;
                    template.OwnerUserId = principal.UserId;
                    template.TemplateId = templateId;
                    template.m_isNew = false;
                    template.Name = (string)reader["Name"];
                    template.FormLayout.LoadContent((string)reader["FieldMetadata"]);
                }
            }
            if (null == template)
            {
                throw new NotFoundException("Signature template is not found");
            }
            else
            {
                return template;
            }
        }

        public static IEnumerable<PdfSignatureTemplate> ListTemplatesByCurrentUser()
        {
            List<PdfSignatureTemplate> list = new List<PdfSignatureTemplate>();

            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.Generic, "Principal cannot be null");
            }


            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", principal.BrokerId),
                                            new SqlParameter("@OwnerUserId", principal.UserId)

                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "SignatureTemplate_ListByOwnerId", parameters))
            {
                while (reader.Read())
                {
                    PdfSignatureTemplate template = new PdfSignatureTemplate();
                    template.BrokerId = principal.BrokerId;
                    template.OwnerUserId = principal.UserId;
                    template.m_isNew = false;
                    template.TemplateId = (Guid)reader["TemplateId"];
                    template.Name = (string)reader["Name"];
                    template.FormLayout.LoadContent((string)reader["FieldMetadata"]);
                    list.Add(template);
                }
            }
            return list;
        }
        public static PdfSignatureTemplate Create()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.Generic, "Principal cannot be null");
            }

            PdfSignatureTemplate template = new PdfSignatureTemplate();
            template.m_isNew = true;
            template.TemplateId = Guid.NewGuid();
            template.BrokerId = principal.BrokerId;
            template.OwnerUserId = principal.UserId;
            return template;
        }
    }
}
