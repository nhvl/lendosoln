﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace LendersOffice.PdfLayout
{
    public class SignatureUtilities
    {
        public static byte[] ConvertToInitialImage(string text, int width, int height)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Bitmap bitmap = new Bitmap(width, height);
                bitmap.SetResolution(73.0F, 73.0F);

                float signatureFontSize = 11;

                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    Font f = new Font("Lucida Handwriting", signatureFontSize, FontStyle.Regular);

                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                    g.DrawString(text, f, new SolidBrush(Color.Black), 0, bitmap.Height - 12);

                }
                bitmap.Save(stream, ImageFormat.Png);
                return stream.GetBuffer();
            }
        }
        public static byte[] ConvertToSignatureImage(string text, int width, int height)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Bitmap bitmap = new Bitmap(width, height);
                bitmap.SetResolution(73.0F, 73.0F);

                float signatureFontSize = 11;
                float signatureDateFontSize = 5;

                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    Font f = new Font("Lucida Handwriting", signatureFontSize, FontStyle.Regular);

                    float signatureHeight = f.GetHeight();
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                    g.DrawString(text, f, new SolidBrush(Color.Black), 0, 0);

                    f = new Font("Arial", signatureDateFontSize, FontStyle.Regular);
                    g.DrawString(DateTime.Now.ToShortDateString(), f, new SolidBrush(Color.Black), 0, bitmap.Height - 5);

                    g.DrawLine(new Pen(new SolidBrush(Color.Blue), .25f), 0, signatureHeight - 7, width, signatureHeight - 7);

                }
                bitmap.Save(stream, ImageFormat.Png);
                return stream.GetBuffer();
            }
        }
    }
}
