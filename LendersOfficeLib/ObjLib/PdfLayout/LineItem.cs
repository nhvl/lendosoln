﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.PdfLayout
{
    class LineItem
    {
        private List<TextItem> m_list = new List<TextItem>();

        public LineItem()
        {
        }
        public void Add(TextItem item)
        {
            m_list.Add(item);
        }
        public IEnumerable<TextItem> TextItemList
        {
            get { return m_list; }
        }
        public float Width
        {
            get
            {
                float v = 0;
                foreach (var item in TextItemList)
                {
                    v += item.Width;
                }
                return v;
            }
        }

        public float Height
        {
            get
            {
                float height = 0;
                foreach (var item in TextItemList)
                {
                    if (item.Height > height)
                    {
                        height = item.Height;
                    }
                }
                return height;
            }
        }
        public List<LineItem> Split(float maxWidth)
        {
            List<LineItem> lines = new List<LineItem>();
            LineItem currentLine = new LineItem();

            foreach (var item in TextItemList)
            {
                float currentLineWidth = currentLine.Width;
                if (currentLineWidth + item.Width > maxWidth)
                {
                    StringBuilder word = new StringBuilder();
                    StringBuilder sb = new StringBuilder();
                    foreach (char ch in item.Content.ToCharArray())
                    {
                        if (ch == ' ')
                        {
                            string tempLine = sb.ToString() + " " + word.ToString();
                            if (currentLineWidth + item.Font.GetWidthPoint(tempLine, item.FontSize) > maxWidth)
                            {
                                TextItem textItem = new TextItem(item.Font, item.FontSize, sb.ToString());
                                currentLine.Add(textItem);
                                lines.Add(currentLine);
                                currentLineWidth = 0;
                                currentLine = new LineItem();
                                sb = new StringBuilder();
                            }
                            sb.Append(word).Append(" ");
                            word = new StringBuilder();
                        }
                        else
                        {
                            word.Append(ch);
                        }
                    }
                    if (sb.Length + word.Length > 0)
                    {
                        TextItem textItem = new TextItem(item.Font, item.FontSize, sb + " " + word);
                        currentLine.Add(textItem);

                    }
                }
                else
                {
                    currentLine.Add(item);
                }
            }
            lines.Add(currentLine);
            return lines;
        }
    }

}
