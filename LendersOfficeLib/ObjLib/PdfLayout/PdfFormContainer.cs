﻿namespace LendersOffice.PdfLayout
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using DataAccess;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Sample code to populate the pdf.
    /// 
    ///using (FileStream outputStream = new FileStream(outputPdf, FileMode.Create))
    ///{
    ///    
    ///    Document document = new Document();
    ///    
    ///    PdfCopy writer = new PdfCopy(document, outputStream);
    ///    document.Open();
    ///
    ///    PdfFormLayout layout = new PdfFormLayout();
    ///    layout.Load(layoutFileLocation);
    ///    PdfFormContainer container = new PdfFormContainer(inputPdf, layout);
    ///
    ///    foreach (var o in values)
    ///    {
    ///        container.SetFieldValue(o.Key, o.Value);
    ///    }
    ///    container.SetFieldValue("signature", new PdfSignatureValue() { Name = "David Dao", SignedDate = DateTime.Now, TransactionNumber = "ABCDEF" });
    ///    container.AppendTo(writer);
    ///
    ///    document.Close();
    ///}
    /// </summary>
    public class PdfFormContainer
    {
        private const string SpecialOutstandingConditionFieldsId = "sOutstandingConditionHtml"; //"sOutstandingConditionHtml";
        private byte[] m_lenderSignatureImg = null;
        private static byte[] SignatureFontTTF
        {
            get
            {
                using (Stream stream = typeof(PdfFormContainer).Assembly.GetManifestResourceStream("LendersOffice.ObjLib.PdfLayout.LHANDW.TTF"))
                {
                    byte[] buffer = new byte[8192];
                    using (MemoryStream s = new MemoryStream())
                    {
                        while (true)
                        {
                            int read = stream.Read(buffer, 0, buffer.Length);
                            if (read < 1)
                            {
                                break;
                            }
                            s.Write(buffer, 0, read);
                        }
                        return s.ToArray();
                    }
                }
            }
        }
        private static BaseFont SIGNATURE_SUBTITLE_FONT = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        private static BaseFont HELVETICA_BOLD = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        private static BaseFont HELVETICA = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        private static BaseFont SIGNATURE_FONT = BaseFont.CreateFont("HandWriting.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED, true, SignatureFontTTF, null);
        private const int LINE_SPACING = 200;

        private Dictionary<string, string> m_fieldValueDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, PdfSignatureValue> m_signatureFieldValueDictionary = new Dictionary<string, PdfSignatureValue>();


        private string m_pdfFileName;
        private byte[] m_pdfContent;
        private PdfFormLayout m_layout;
        private Stream m_pdfStream;

        public PdfFormContainer(string pdfFileName, PdfFormLayout layout)
        {
            m_pdfStream = null;
            m_pdfFileName = pdfFileName;
            m_pdfContent = null;
            m_layout = layout;
            IsTestMode = false;
        }
        public PdfFormContainer(Stream pdfStream, PdfFormLayout layout)
        {
            m_pdfStream = pdfStream;
            m_pdfFileName = null;
            m_pdfContent = null;
            m_layout = layout;
            IsTestMode = false;
        }

        public PdfFormContainer(byte[] pdfContent, PdfFormLayout layout)
        {
            m_pdfStream = null;
            m_pdfFileName = null;
            m_pdfContent = pdfContent;
            m_layout = layout;
            IsTestMode = false;
        }

        public bool IsTestMode { get; set; }
        public bool ShouldSkipESignTags
        {
            get;
            set;
        }

        public void SetFieldValue(string fieldName, string fieldValue)
        {
            m_fieldValueDictionary[fieldName] = fieldValue;
        }
        public void SetFieldValue(string fieldName, PdfSignatureValue value)
        {
            m_signatureFieldValueDictionary[fieldName] = value;
        }

        public void AppendTo(PdfCopy writer)
        {
            PdfReader reader = null;
            if (m_pdfFileName != null)
            {
                reader = new PdfReader(m_pdfFileName);
            }
            else if (m_pdfContent != null)
            {
                reader = new PdfReader(m_pdfContent);
            }
            else if (m_pdfStream != null)
            {
                reader = new PdfReader(m_pdfStream);
            }
            if (!reader.IsOpenedWithFullPermissions)
            {
                throw new InsufficientPdfPermissionException();
            }

            string tempPath = TempFileUtils.NewTempFilePath();
            int numOfPages;

            using (FileStream stream = File.Create(tempPath))
            {
                PdfStamper stamper = new PdfStamper(reader, stream);
                stamper.FormFlattening = true;
                numOfPages = reader.NumberOfPages;
                for (int i = 1; i <= numOfPages; i++)
                {
                    Merge(stamper.GetOverContent(i), i, reader.GetPageSize(i));
                }

                PdfField field = m_layout.FindSpecialField(SpecialOutstandingConditionFieldsId);
                if (field != null)
                {
                    // 7/11/2011 dd - The overflow special field is currently only design to work
                    // with single OutstandingCondition field. In order to support multiple overflow fields, we
                    // need to calculate the new page number foreach overflow field.
                    string htmlValue = string.Empty;
                    if (!m_fieldValueDictionary.TryGetValue(field.Name, out htmlValue))
                    {
                        htmlValue = string.Empty;
                    }
                    List<LineItem> lines = ConvertToStructureString(htmlValue);
                    List<List<LineItem>> lineSet = BreakText(lines, field.Rectangle.Width, field.Rectangle.Height);
                    Rectangle boundingRectangle = reader.GetPageSize(field.PageNumber);
                    for (int index = 1; index < lineSet.Count; index++)
                    {

                        stamper.InsertPage(field.PageNumber + index, reader.GetPageSize(field.PageNumber));
                        PdfImportedPage p = stamper.GetImportedPage(reader, field.PageNumber);
                        stamper.GetOverContent(field.PageNumber + index).AddTemplate(p, 0, 0);
                        Merge(stamper.GetOverContent(field.PageNumber + index), field.PageNumber, reader.GetPageSize(field.PageNumber + index));
                    }
                    for (int index = 0; index < lineSet.Count; index++)
                    {
                        // 5/28/2010 dd - Some pdf page will have bounding page that start at different coordinate
                        // other than 0,0. We need to factor in this page offset for the field.
                        field.Rectangle.Left += boundingRectangle.Left;
                        field.Rectangle.Top += boundingRectangle.Bottom;
                        field.Rectangle.Right += boundingRectangle.Left;
                        field.Rectangle.Bottom += boundingRectangle.Bottom;
                        DrawText(stamper.GetOverContent(field.PageNumber + index), field, lineSet[index]);
                    }
                }
                stamper.Close();
                reader.Close();
            }

            // OPM 219992.
            reader = new PdfReader(tempPath);
            numOfPages = reader.NumberOfPages;
            for (int i = 1; i <= numOfPages; i++)
            {
                writer.AddPage(writer.GetImportedPage(reader, i));
            }
            reader.Close();

        }

        private void DrawGrid(PdfContentByte content)
        {
            float width = content.PdfDocument.PageSize.Width;
            float height = content.PdfDocument.PageSize.Height;
            content.SaveState();
            content.SetColorStroke(BaseColor.BLACK);
            for (float x = 0; x < width; x += 50)
            {
                if (x % 100 == 0)
                {
                    content.SetLineWidth(.1f);
                    content.SetLineDash(new float[0], 0);
                }
                else if (x % 50 == 0)
                {
                    content.SetLineWidth(.1f);
                    content.SetLineDash(5, 2, 0);
                }
                content.MoveTo(x, 0);
                content.LineTo(x, height);
                content.Stroke();
            }

            for (float y = 0; y < height; y += 50)
            {
                if (y % 100 == 0)
                {

                    content.SetLineWidth(.1f);
                    content.SetLineDash(new float[0], 0);

                }
                else if (y % 50 == 0)
                {
                    content.SetLineWidth(.1f);
                    content.SetLineDash(5, 2, 0);
                }
                content.MoveTo(0, y);
                content.LineTo(width, y);
                content.Stroke();
            }

            for (float x = 0; x < width; x += 10)
            {
                if (x % 50 == 0)
                    continue;

                for (float y = 0; y < height; y += 10)
                {
                    if (y % 50 == 0)
                        continue;

                    content.Circle(x, y, .5f);
                    content.Fill();

                }
            }
            content.RestoreState();
        }
        private void Merge(PdfContentByte contentByte, int pageNum, Rectangle boundingRectangle)
        {
            string value = string.Empty;
            foreach (PdfField field in m_layout.FieldList)
            {
                if (field.Name.Equals(SpecialOutstandingConditionFieldsId, StringComparison.OrdinalIgnoreCase))
                {
                    // 7/11/2011 dd - Skip special overflow field. This field will handle separately.
                    continue;
                }
     
                if (field.PageNumber != pageNum)
                {
                    continue;
                }
                if (IsTestMode && field.Name.Equals("LenderSignature", StringComparison.OrdinalIgnoreCase) == false)
                {
                    value = field.TestValue;
                }
                else
                {
                    if (!m_fieldValueDictionary.TryGetValue(field.Name, out value))
                    {
                        value = string.Empty;
                    }
                    if (value == string.Empty && field.Type == PdfFieldType.Checkbox)
                    {
                        // 6/14/2010 dd - The only reason this block exist is for checkbox value that are neither Enumeration or Boolean.
                        // i.e: aBDecJudgment which return "Y" or "N" instead of "1" or "0"
                        if (!m_fieldValueDictionary.TryGetValue(field.NameUseForRendering, out value))
                        {
                            value = string.Empty;
                        }
                    }
                }
                // 5/28/2010 dd - Some pdf page will have bounding page that start at different coordinate
                // other than 0,0. We need to factor in this page offset for the field.
                field.Rectangle.Left += boundingRectangle.Left;
                field.Rectangle.Top += boundingRectangle.Bottom;
                field.Rectangle.Right += boundingRectangle.Left;
                field.Rectangle.Bottom += boundingRectangle.Bottom;

                contentByte.Rectangle(field.Rectangle);
                contentByte.Stroke();

                switch (field.Type)
                {

                    case PdfFieldType.Text:
                        if (field.Name.Equals("LenderSignature", StringComparison.OrdinalIgnoreCase))
                        {
                            DrawLenderSignature(contentByte, field, value);
                        }
                        else
                        {
                            DrawText(contentByte, field, value);
                        }
                        break;
                    case PdfFieldType.Checkbox:
                        DrawCheckbox(contentByte, field, value);
                        break;
                    case PdfFieldType.Initial:
                        if (!this.ShouldSkipESignTags)
                        {
                            DrawInitial(contentByte, field, value);
                        }
                        break;
                    case PdfFieldType.Signature:
                        if (!this.ShouldSkipESignTags)
                        {
                            // 4/20/2010 dd - We do not draw signature base on simple string.
                            PdfSignatureValue signature = null;
                            if (IsTestMode)
                            {
                                signature = new PdfSignatureValue() { Name = "David Dao", SignedDate = DateTime.Now, TransactionNumber = "1234567890" };
                            }
                            else
                            {
                                if (!m_signatureFieldValueDictionary.TryGetValue(field.Name, out signature))
                                {
                                    signature = null;
                                }
                            }
                            if (null != signature)
                            {
                                DrawSignature(contentByte, field, signature);
                            }
                        }
                        break;
                    case PdfFieldType.SignatureDate:
                        break; // 462582 tj - as we're looking to eliminate signing from our system, we'll leave the signing date action as a no-op
                }
            }

        }
        private void DrawLenderSignature(PdfContentByte contentByte, PdfField field, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }
            if (m_lenderSignatureImg == null)
            {
                try
                {
                    m_lenderSignatureImg = FileDBTools.ReadData(E_FileDB.Normal, value);
                }
                catch (FileNotFoundException)
                {
                    return;
                }
            }

            Image img = Image.GetInstance(m_lenderSignatureImg);
            img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
            img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
            contentByte.AddImage(img);
        }

        private void DrawInitial(PdfContentByte contentByte, PdfField field, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            BaseFont initialFont = SIGNATURE_FONT;
            int initialFontSize = 11;

            float initialHeight = initialFont.GetHeightPoint(value, initialFontSize);
            float x = field.Rectangle.Left;
            float y = (field.Rectangle.Height - initialHeight) + field.Rectangle.Bottom;

            contentByte.DrawText(value, x, y, initialFont, initialFontSize);

        }
        private void DrawSignature(PdfContentByte contentByte, PdfField field, PdfSignatureValue value)
        {
            if (string.IsNullOrEmpty(value.Name))
            {
                return;
            }

            string url = string.Empty;

            if (ConstStage.EVaultVerifyUrl != "")
            {
                url = ConstStage.EVaultVerifyUrl + "?id=" + value.TransactionNumber;
            }

            string debugMsg = string.Empty;

            BaseFont signatureFont = SIGNATURE_FONT;
            int signatureFontSize = 11;
            int gap = 2;
            BaseFont signatureDateFont = SIGNATURE_SUBTITLE_FONT;
            int signatureDateFontSize = 5;
            string signatureDateText = value.SignedDate.ToShortDateString() + " - " + value.TransactionNumber;
            string signatureText = value.Name;

            float signatureDateHeight = signatureDateFont.GetHeightPoint(signatureDateText, signatureDateFontSize);

            float x = field.Rectangle.Left;
            float y = field.Rectangle.Bottom + (signatureDateHeight + gap * 2);
            contentByte.DrawText(signatureText, x, y, signatureFont, signatureFontSize);
            debugMsg = "SignatureDateHeight: " + signatureDateHeight + ". Signature (x,y): (" + x + "," + y + "). Date (x,y): (" + field.Rectangle.Left + "," + field.Rectangle.Bottom + ").";
            x = field.Rectangle.Left;
            y = field.Rectangle.Bottom;
            contentByte.DrawText(signatureDateText, x, y, signatureDateFont, signatureDateFontSize);


            if (url != string.Empty)
            {
                contentByte.SetAction(new PdfAction(url), field.Rectangle.Left, field.Rectangle.Bottom, field.Rectangle.Right, field.Rectangle.Top);
            }

            // Draw line
            contentByte.SetColorStroke(BaseColor.BLUE);
            contentByte.SetLineWidth(.5f);
            contentByte.MoveTo(field.Rectangle.Left, field.Rectangle.Bottom + (signatureDateHeight + gap));
            contentByte.LineTo(field.Rectangle.Right, field.Rectangle.Bottom + (signatureDateHeight + gap));
            contentByte.Stroke();

            debugMsg += "BlueLine y=" + (field.Rectangle.Bottom + (signatureDateHeight + gap));

            //Tools.LogError(debugMsg);

        }
        private void DrawCheckbox(PdfContentByte contentByte, PdfField field, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            if (field.ValueUseForRendering == value || value.ToLower() == "yes")
            {
                Rectangle rect = field.Rectangle;
                contentByte.SetColorFill(BaseColor.BLACK);
                contentByte.SetColorStroke(BaseColor.BLACK);
                contentByte.SetLineWidth(1f);
                contentByte.MoveTo(rect.Left + 2, rect.Bottom + 2);
                contentByte.LineTo(rect.Right - 2, rect.Top - 2);
                contentByte.MoveTo(rect.Left + 2, rect.Top - 2);
                contentByte.LineTo(rect.Right - 2, rect.Bottom + 2);
            }
            contentByte.Stroke();

        }

        /// <summary>
        /// Gets a value indicating whether the upper case case version of the given value will fit in the given form field.
        /// </summary>
        /// <param name="value">The value for the field.</param>
        /// <param name="field">The field.</param>
        /// <returns>True if the upper case version of value will fit without truncating content. Otherwise, false.</returns>
        public static bool DoesValueFitInField(string value, PdfField field)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }

            BaseFont bf = GetFont(field.Font);
            float fontSize = GetFontSize(field);

            // We send pretty much all text to upper before populating the form,
            // so we will do the same here.
            var wrappedLines = WrapTextToFitFieldWidth(field, value.ToUpper(), bf, fontSize);
            var wrappedLinesThatFitHeight = GetLinesThatFitFieldHeight(field, bf, fontSize, wrappedLines);

            return wrappedLines.Count == wrappedLinesThatFitHeight.Count;
        }

        private void DrawText(PdfContentByte contentByte, PdfField field, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            BaseFont bf = GetFont(field.Font);
            float fontSize = GetFontSize(field);

            // 6/14/2010 dd - Determine if the line need to wrap.
            // OPM 242786, 8/3/2016, ML - normalize newlines to make sure we account for all types of line breaks.
            List<string> wrappedLines = WrapTextToFitFieldWidth(field, value, bf, fontSize);
            List<string> wrappedLinesThatFitHeight = GetLinesThatFitFieldHeight(field, bf, fontSize, wrappedLines);

            // 6/14/2010 dd - Make sure there is at least one line print.
            if (wrappedLines.Count > 0 && wrappedLinesThatFitHeight.Count == 0)
            {
                wrappedLinesThatFitHeight.Add(wrappedLines[0]);
            }
            wrappedLinesThatFitHeight.Reverse();

            float height = bf.GetTotalHeightPoint(wrappedLinesThatFitHeight, fontSize);

            float y = 0;

            switch (field.VAlign)
            {
                case PdfFieldVAlignType.Top:
                    y = (field.Rectangle.Height - height) + field.Rectangle.Bottom;
                    break;
                case PdfFieldVAlignType.Center:
                    y = (field.Rectangle.Height - height) / 2 + field.Rectangle.Bottom;
                    break;
                case PdfFieldVAlignType.Bottom:
                    y = field.Rectangle.Bottom;

                    break;
                default:
                    throw new UnhandledEnumException(field.VAlign);
            }

            foreach (string s in wrappedLinesThatFitHeight)
            {
                float x = 0;

                float widthInPoint = bf.GetWidthPoint(s, fontSize);
                float heightInPoint = bf.GetHeightPoint(s, fontSize);
                switch (field.Align)
                {
                    case PdfFieldAlignType.Left:
                        x = field.Rectangle.Left;
                        break;
                    case PdfFieldAlignType.Right:
                        x = field.Rectangle.Right - widthInPoint;
                        break;
                    case PdfFieldAlignType.Center:
                        x = field.Rectangle.Left + (field.Rectangle.Width - widthInPoint) / 2;
                        break;
                    default:
                        throw new UnhandledEnumException(field.Align);

                }
                contentByte.DrawText(s, x, y, bf, fontSize);
                y += heightInPoint;
            }

        }

        private static float GetFontSize(PdfField field)
        {
            float fontSize = 8;
            if (field.FontSize > 0)
            {
                fontSize = field.FontSize;
            }

            return fontSize;
        }

        private static List<string> GetLinesThatFitFieldHeight(PdfField field, BaseFont bf, float fontSize, List<string> wrappedLines)
        {
            List<string> lines = new List<string>();
            float maxHeight = field.Rectangle.Height;
            float currentHeight = 0;

            // Vertical cropping of text base on maximum height.
            foreach (string str in wrappedLines)
            {
                float lineHeight = bf.GetHeightPoint(str, fontSize);
                if (currentHeight + lineHeight > maxHeight)
                {
                    break;
                }
                lines.Add(str);
                currentHeight += lineHeight;
            }

            return lines;
        }

        private static List<string> WrapTextToFitFieldWidth(PdfField field, string value, BaseFont bf, float fontSize)
        {
            List<string> lines = new List<string>();
            float maxWidth = field.Rectangle.Width;
            foreach (string str in value.NormalizeNewLines().Split(new string[] { "\r\n" }, StringSplitOptions.None))
            {
                if (bf.GetWidthPoint(str, fontSize) > maxWidth)
                {
                    // If the current line is too long and has no spaces,
                    // we'll break it into sections and move on to the
                    // next string.
                    if (str.IndexOf(' ') == -1)
                    {
                        lines.AddRange(CreateLineBreaks(str, bf, fontSize, maxWidth));
                        continue;
                    }

                    // 6/14/2010 dd - Perform line wrap here.
                    char[] charArray = str.ToCharArray();
                    StringBuilder sb = new StringBuilder();
                    StringBuilder word = new StringBuilder();
                    for (int i = 0; i < str.Length; i++)
                    {
                        char ch = charArray[i];
                        if (ch == ' ')
                        {
                            string line = sb.ToString() + " " + word.ToString();
                            if (bf.GetWidthPoint(line, fontSize) > maxWidth)
                            {
                                lines.Add(sb.ToString());
                                sb = new StringBuilder();
                            }
                            sb.Append(word).Append(" ");
                            word = new StringBuilder();
                        }
                        else
                        {
                            var currentLine = sb.ToString();
                            var currentWord = word.ToString();

                            var sizeWithAdditionalCharacter = bf.GetWidthPoint(currentLine, fontSize) +
                                bf.GetWidthPoint(currentWord, fontSize) +
                                bf.GetWidthPoint(ch, fontSize);

                            // If the word we're adding will become too long
                            // with the current character, add the existing 
                            // line and continue on the next line.
                            if (sizeWithAdditionalCharacter > maxWidth)
                            {
                                lines.Add(currentLine);

                                sb = new StringBuilder();
                            }

                            word.Append(ch);
                        }
                    }

                    if (sb.Length + word.Length > 0)
                    {
                        lines.Add(sb.Append(word).Append(" ").ToString());
                    }
                }
                else
                {
                    lines.Add(str);
                }
            }

            return lines;
        }

        /// <summary>
        /// Breaks a line into segments with a rendered width less than
        /// or equal to <paramref name="maxWidth"/>.
        /// </summary>
        /// <param name="stringToBreak">
        /// The string that will be broken into segments.
        /// </param>
        /// <param name="bf">
        /// The <see cref="BaseFont"/> used to determine rendered width.
        /// </param>
        /// <param name="fontSize">
        /// The size of the font.
        /// </param>
        /// <param name="maxWidth">
        /// The maximum width of the container.
        /// </param>
        /// <returns>
        /// A <see cref="List{T}"/> containing the segments for the string.
        /// </returns>
        private static List<string> CreateLineBreaks(string stringToBreak, BaseFont bf, float fontSize, float maxWidth)
        {
            var segments = new List<string>();

            var builder = new StringBuilder();

            foreach (var character in stringToBreak.ToCharArray())
            {
                var currentString = builder.ToString();

                if (bf.GetWidthPoint(currentString, fontSize) + bf.GetWidthPoint(character, fontSize) > maxWidth)
                {
                    segments.Add(currentString);

                    builder = new StringBuilder();
                }

                builder.Append(character);
            }

            if (builder.Length > 0)
            {
                segments.Add(builder.ToString());
            }

            return segments;
        }

        private void DrawText(PdfContentByte contentByte, PdfField field, List<LineItem> lines)
        {
            if (lines == null || lines.Count == 0)
            {
                return;
            }

            lines.Reverse();

            float height = 0;
            foreach (var line in lines)
            {
                height += line.Height;
            }

            float y = 0;

            switch (field.VAlign)
            {
                case PdfFieldVAlignType.Top:
                    y = (field.Rectangle.Height - height) + field.Rectangle.Bottom;
                    break;
                case PdfFieldVAlignType.Center:
                    y = (field.Rectangle.Height - height) / 2 + field.Rectangle.Bottom;
                    break;
                case PdfFieldVAlignType.Bottom:
                    y = field.Rectangle.Bottom;

                    break;
                default:
                    throw new UnhandledEnumException(field.VAlign);
            }

            foreach (var line in lines)
            {
                float x = 0;

                float widthInPoint = line.Width;
                float heightInPoint = line.Height;
                switch (field.Align)
                {
                    case PdfFieldAlignType.Left:
                        x = field.Rectangle.Left;
                        break;
                    case PdfFieldAlignType.Right:
                        x = field.Rectangle.Right - widthInPoint;
                        break;
                    case PdfFieldAlignType.Center:
                        x = field.Rectangle.Left + (field.Rectangle.Width - widthInPoint) / 2;
                        break;
                    default:
                        throw new UnhandledEnumException(field.Align);

                }
                foreach (var word in line.TextItemList)
                {
                    contentByte.DrawText(word.Content, x, y, word.Font, word.FontSize);
                    x += word.Width;
                }
                y += heightInPoint;
            }

        }

        public static BaseFont GetFont(string fontName)
        {
            string normFontName = string.Empty;
            if (string.IsNullOrEmpty(fontName) == false)
            {
                normFontName = fontName.ToLower();
            }
            switch (normFontName)
            {
                case "helv":
                    return HELVETICA;
                default:
                    return HELVETICA_BOLD;
            }


        }

        private static List<LineItem> ConvertToStructureString(string htmlText)
        {
            // 7/11/2011 dd - This is a fake support of HTML. The only tag I supported is <b></b> and <b> must be start of the line and </b> at end of the line.
            //
            // Example:
            //     <b>This line will be bold</b>
            //     This <b>word</b> WILL NOT BE BOLD.
            //
            List<LineItem> results = new List<LineItem>();
            foreach (string str in htmlText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
            {
                LineItem lineItem = new LineItem();
                BaseFont font = null;
                float fontSize = 8;
                string newText = str;
                if (str.StartsWith("<b>") && str.EndsWith("</b>"))
                {
                    font = HELVETICA_BOLD;
                    newText = str.Substring(3, str.Length - 7);
                }
                else
                {
                    font = HELVETICA;
                }
                lineItem.Add(new TextItem(font, fontSize, newText));
                results.Add(lineItem);
            }
            return results;
        }
        private static List<List<LineItem>> BreakText(List<LineItem> htmlText, float maxWidth, float maxHeight)
        {

            List<LineItem> lines = new List<LineItem>();

            foreach (LineItem str in htmlText)
            {
                if (str.Width > maxWidth)
                {
                    lines.AddRange(str.Split(maxWidth));
                }
                else
                {
                    lines.Add(str);
                }
            }
            float currentHeight = 0;

            List<List<LineItem>> returnSet = new List<List<LineItem>>();

            List<LineItem> tmpLines = lines;
            lines = new List<LineItem>();
            // Vertical cropping of text base on maximum height.
            foreach (LineItem str in tmpLines)
            {
                float lineHeight = str.Height;
                if (currentHeight > 0 && (currentHeight + lineHeight) > maxHeight)
                {
                    returnSet.Add(lines);
                    lines = new List<LineItem>();
                    currentHeight = 0;
                }
                lines.Add(str);
                currentHeight += lineHeight;
            }


            returnSet.Add(lines);

            return returnSet;

        }


    }

    

    static class PdfContentByteExtension
    {
        private const int LINE_SPACING = 200;
        public static void DrawText(this PdfContentByte contentByte, string text, float x, float y, BaseFont bf, float fontSize)
        {
            contentByte.BeginText();
            contentByte.SetFontAndSize(bf, fontSize);
            contentByte.SetTextMatrix(x, y);
            contentByte.ShowText(text);
            contentByte.EndText();
        }
        public static float GetHeightPoint(this BaseFont bf, string text, float fontSize)
        {
            return (bf.GetAscent(text) - bf.GetDescent(text) + LINE_SPACING) * 0.001F * fontSize;
        }
        public static float GetTotalHeightPoint(this BaseFont bf, IEnumerable<string> lines, float fontSize)
        {
            float height = 0;
            foreach (string s in lines)
            {
                height += GetHeightPoint(bf, s, fontSize);
            }
            return height;
        }
    }
}
