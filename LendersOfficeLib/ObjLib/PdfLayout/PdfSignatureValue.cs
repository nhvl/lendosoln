﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.PdfLayout
{
    public class PdfSignatureValue
    {
        public string Name { get; set; }
        public DateTime SignedDate { get; set; }
        public string TransactionNumber { get; set; }
    }
}
