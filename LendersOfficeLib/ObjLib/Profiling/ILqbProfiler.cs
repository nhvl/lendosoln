﻿namespace LendersOffice.ObjLib.Profiling
{
    using System;
    using System.Data.Common;

    /// <summary>
    /// A profiler for LendingQB applications.
    /// </summary>
    public interface ILqbProfiler
    {
        /// <summary>
        /// Returns a timer for use with profiling a using block.
        /// </summary>
        /// <param name="name">
        /// The name for the timing.
        /// </param>
        /// <returns>
        /// The timer.
        /// </returns>
        IDisposable Profile(string name);

        /// <summary>
        /// Wraps the specified <paramref name="connection"/>.
        /// </summary>
        /// <param name="connection">
        /// The connection to wrap for profiling.
        /// </param>
        /// <returns>
        /// The wrapped connection.
        /// </returns>
        DbConnection Wrap(DbConnection connection);

        /// <summary>
        /// Wraps the specified <paramref name="factory"/>.
        /// </summary>
        /// <param name="factory">
        /// The factory to wrap for profiling.
        /// </param>
        /// <returns>
        /// The wrapped factory.
        /// </returns>
        DbProviderFactory Wrap(DbProviderFactory factory);
    }
}
