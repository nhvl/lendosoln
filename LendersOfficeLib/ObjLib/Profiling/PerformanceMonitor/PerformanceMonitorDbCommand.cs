﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System.Data;
    using System.Data.Common;
    using DataAccess;

    /// <summary>
    /// Wraps a database command with performance monitor profiling.
    /// </summary>
    /// <remarks>
    /// This class has been adapted from the MiniProfiler library,
    /// <c>https://github.com/MiniProfiler/dotnet/tree/v3</c>.
    /// </remarks>
    internal class PerformanceMonitorDbCommand : DbCommand
    {
        /// <summary>
        /// The handler for state change events.
        /// </summary>
        private readonly PerformanceMonitorStateChangeHandler handler;

        /// <summary>
        /// The wrapped command.
        /// </summary>
        private DbCommand wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorDbCommand"/> class.
        /// </summary>
        /// <param name="wrapped">
        /// The wrapped command.
        /// </param>
        public PerformanceMonitorDbCommand(DbCommand wrapped)
        {
            this.wrapped = wrapped;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorDbCommand"/> class.
        /// </summary>
        /// <param name="handler">
        /// The handler for state change events.
        /// </param>
        /// <param name="wrapped">
        /// The wrapped command.
        /// </param>
        public PerformanceMonitorDbCommand(PerformanceMonitorStateChangeHandler handler, DbCommand wrapped)
            : this(wrapped)
        {
            this.handler = handler;
        }

        /// <summary>
        /// Gets the underlying internal command.
        /// </summary>
        public DbCommand InternalCommand => this.wrapped;

        /// <summary>
        /// Gets or sets the command text.
        /// </summary>
        public override string CommandText
        {
            get { return this.wrapped.CommandText; }
            set { this.wrapped.CommandText = value; }
        }

        /// <summary>
        /// Gets or sets the command timeout.
        /// </summary>
        public override int CommandTimeout
        {
            get { return this.wrapped.CommandTimeout; }
            set { this.wrapped.CommandTimeout = value; }
        }

        /// <summary>
        /// Gets or sets the command type.
        /// </summary>
        public override CommandType CommandType
        {
            get { return this.wrapped.CommandType; }
            set { this.wrapped.CommandType = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command is design time visible.
        /// </summary>
        public override bool DesignTimeVisible
        {
            get { return this.wrapped.DesignTimeVisible; }
            set { this.wrapped.DesignTimeVisible = value; }
        }

        /// <summary>
        /// Gets or sets the updated row source.
        /// </summary>
        public override UpdateRowSource UpdatedRowSource
        {
            get { return this.wrapped.UpdatedRowSource; }
            set { this.wrapped.UpdatedRowSource = value; }
        }

        /// <summary>
        /// Gets or sets the database connection.
        /// </summary>
        protected override DbConnection DbConnection
        {
            get
            {
                return this.wrapped.Connection;
            }

            set
            {
                if (value is PerformanceMonitorDatabaseConnection)
                {
                    this.wrapped.Connection = ((PerformanceMonitorDatabaseConnection)value).InternalConnection;
                }
                else
                {
                    this.wrapped.Connection = value;
                }
            }
        }

        /// <summary>
        /// Gets the database parameter collection.
        /// </summary>
        protected override DbParameterCollection DbParameterCollection
        {
            get { return this.wrapped.Parameters; }
        }

        /// <summary>
        /// Gets or sets the database transaction.
        /// </summary>
        protected override DbTransaction DbTransaction
        {
            get { return this.wrapped.Transaction; }
            set { this.wrapped.Transaction = value; }
        }

        /// <summary>
        /// Cancels the command.
        /// </summary>
        public override void Cancel() => this.wrapped.Cancel();

        /// <summary>
        /// Prepares the command.
        /// </summary>
        public override void Prepare() => this.wrapped.Prepare();

        /// <summary>
        /// Executes a non-query.
        /// </summary>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public override int ExecuteNonQuery()
        {
            this.PrepStatistics(nameof(this.ExecuteNonQuery));

            var result = 0;
            this.MeasureSqlExecuteTime(() => result = this.wrapped.ExecuteNonQuery());

            return result;
        }

        /// <summary>
        /// Executes a scalar query.
        /// </summary>
        /// <returns>
        /// The result of the query.
        /// </returns>
        public override object ExecuteScalar()
        {
            this.PrepStatistics(nameof(this.ExecuteScalar));

            object result = null;
            this.MeasureSqlExecuteTime(() => result = this.wrapped.ExecuteScalar());

            return result;
        }

        /// <summary>
        /// Creates a database parameter.
        /// </summary>
        /// <returns>
        /// The <see cref="DbParameter"/>.
        /// </returns>
        protected override DbParameter CreateDbParameter() => this.wrapped.CreateParameter();

        /// <summary>
        /// Executes a query that returns a data reader.
        /// </summary>
        /// <param name="behavior">
        /// The command behavior for the reader.
        /// </param>
        /// <returns>
        /// The data reader.
        /// </returns>
        protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
        {
            this.PrepStatistics(nameof(this.ExecuteDbDataReader));

            DbDataReader result = null;
            this.MeasureSqlExecuteTime(() => result = this.wrapped.ExecuteReader(behavior));

            return result;
        }

        /// <summary>
        /// Disposes the command.
        /// </summary>
        /// <param name="disposing">
        /// False if this is being disposed in a <c>finalizer</c>.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.wrapped != null)
            {
                this.wrapped.Dispose();
            }

            this.wrapped = null;
            base.Dispose(disposing);
        }

        /// <summary>
        /// Prepares statistics for the command.
        /// </summary>
        /// <param name="method">
        /// The method to be called by the command.
        /// </param>
        private void PrepStatistics(string method)
        {
            if (this.handler == null)
            {
                return;
            }

            this.handler.Method = method;
            this.handler.QueryName = this.CommandText;
            this.handler.ClearParameters();

            if (this.Parameters != null)
            {
                foreach (DbParameter parameter in this.Parameters)
                {
                    string fieldType = parameter.DbType.ToString();

                    if (parameter is System.Data.SqlClient.SqlParameter)
                    {
                        var p = (System.Data.SqlClient.SqlParameter)parameter;
                        fieldType = p.SqlDbType.ToString();
                    }

                    this.handler.AddParameter(parameter.ParameterName, parameter.Value, fieldType);
                }
            }

            var conn = this.Connection as System.Data.SqlClient.SqlConnection;
            if (conn != null)
            {
                conn.ResetStatistics();
            }
        }

        /// <summary>
        /// Measure execute time for running action().
        /// </summary>
        /// <param name="action">The sql action to measure.</param>
        private void MeasureSqlExecuteTime(System.Action action)
        {
            if (this.handler != null)
            {
                this.handler.ExecuteTimingStart();
            }

            try
            {
                action();
            }
            finally
            {
                if (this.handler != null)
                {
                    this.handler.ExecuteTimingStopAndAddPerfLog(this.Connection as System.Data.SqlClient.SqlConnection);
                }
            }
        }
    }
}
