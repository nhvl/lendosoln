﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using HttpModule;

    /// <summary>
    /// Provides a handler for connection state changes that records
    /// statistics about the connection.
    /// </summary>
    internal class PerformanceMonitorStateChangeHandler
    {
        /// <summary>
        /// The field that keeps track of what users to do full query logging.
        /// </summary>
        private static volatile HashSet<string> fullQueryLogUsers = new HashSet<string>();
     
        /// <summary>
        /// The field that keeps track of what is too long for the expanded logging.
        /// </summary>
        private static volatile int fullQueryThresholdLoggingInMs = 10000;

        /// <summary>
        /// A bit to track whether or not we are currently logging information about a query. <para></para> 
        /// If so we don't want to attempt to log deeper, as we will get a stack overflow.
        /// </summary>
        private static System.Threading.ThreadLocal<bool> disableLoggingWithinLogging = new System.Threading.ThreadLocal<bool>();

        /// <summary>
        /// The ID for the handler.
        /// </summary>
        private readonly Guid id;

        /// <summary>
        /// The parameters for any queries run on the connection.
        /// </summary>
        private List<ParameterInformation> parameters;

        /// <summary>
        /// The query's execution watch.
        /// </summary>
        private Stopwatch executeStopWatch = null;

        /// <summary>
        /// How many log items.
        /// </summary>
        private int logCount = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorStateChangeHandler"/> class.
        /// </summary>
        public PerformanceMonitorStateChangeHandler()
        {
            this.id = Guid.NewGuid();
            this.parameters = new List<ParameterInformation>();
        }

        /// <summary>
        /// Gets or sets the threshold for enabling the expanded query logging.
        /// </summary>
        public static int FullQueryThresholdLoggingInMs
        {
            get
            {
                return fullQueryThresholdLoggingInMs;
            }

            set
            {
                fullQueryThresholdLoggingInMs = value;
            }
        }

        /// <summary>
        /// Gets or sets the users who will get full query logs. 
        /// </summary>
        public static HashSet<string> FullQueryThresholdLoggingUsers
        {
            get
            {
                return fullQueryLogUsers;
            }

            set
            {
                fullQueryLogUsers = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the query method used.
        /// </summary>
        /// <value>
        /// The query method used.
        /// </value>
        public string Method { get; set; }

        /// <summary>
        /// Gets or sets the name of the database that query execute.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the query name for the connection.
        /// </summary>
        /// <value>
        /// The query name.
        /// </value>
        public string QueryName { get; set; }

        /// <summary>
        /// Gets or sets a value for how long the query executed in milliseconds.
        /// </summary>
        /// <value>
        /// The query's execution time in milliseconds.
        /// </value>
        public long ExecuteInMs { get; set; }

        /// <summary>
        /// Gets or sets a value for how long the connection was open in milliseconds.
        /// </summary>
        /// <value>
        /// How long the connection was open in milliseconds.
        /// </value>
        private long ConectionOpenInMs { get; set; }

        /// <summary>
        /// Gets or sets the bytes received by the connection.
        /// </summary>
        private long BytesReceived { get; set; }

        /// <summary>
        /// Gets or sets the bytes sent by the connection.
        /// </summary>
        private long BytesSent { get; set; }

        /// <summary>
        /// Gets or sets the network server time for the request.
        /// </summary>
        private long NetworkServerTime { get; set; }

        /// <summary>
        /// Adds a parameter to the handler.
        /// </summary>
        /// <param name="key">
        /// The key of the parameter.
        /// </param>
        /// <param name="value">
        /// The string representation of the parameter.
        /// </param>
        /// <param name="type">The database type of the parameter.</param>
        public void AddParameter(string key, object value, string type)
        {
            ParameterInformation p = new ParameterInformation(key, type, value);
            this.parameters.Add(p);
        }

        /// <summary>
        /// Clear parameters.
        /// </summary>
        public void ClearParameters()
        {
            this.parameters.Clear();
        }

        /// <summary>
        /// Handles the connection's state change event.
        /// </summary>
        /// <param name="sender">
        /// The connection.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        public void OnChange(object sender, StateChangeEventArgs e)
        {
            if (e.CurrentState == ConnectionState.Open)
            {
                PerformanceMonitorItem item = PerformanceMonitorItem.Current;
                item?.TrackSqlConnection(this.id, this.QueryName ?? "No Query Specified", e.CurrentState);
                SqlConnection conn = (SqlConnection)sender;
                if (conn != null)
                {
                    // dd - 5/23/2018 - Need to set the database name in the Open event. conn.Database may not be available in the Closed event.
                    this.DatabaseName = conn.Database;
                }
            }
            else if (e.CurrentState == ConnectionState.Closed)
            {
                PerformanceMonitorItem.Current?.TrackSqlConnection(this.id, this.QueryName ?? "No Query Specified", e.CurrentState);

                SqlConnection conn = (SqlConnection)sender;
                if (this.executeStopWatch != null)
                {
                    // 5/9/2018 with sql time out, the sql close event happens before throw sql exception.
                    this.ExecuteTimingStopAndAddPerfLog(conn);
                }
                else if (this.logCount == 0 && PerformanceMonitorItem.Current != null && !string.IsNullOrEmpty(this.QueryName))
                {
                    //// expect never happen
                    this.AddQueryPerfInfo(conn);
                }
            }
        }

        /// <summary>
        /// Start timing for sql execute. 
        /// </summary>
        public void ExecuteTimingStart()
        {
            this.executeStopWatch = new Stopwatch();
            this.executeStopWatch.Start();
        }

        /// <summary>
        /// Stop timing for sql execute. 
        /// </summary>
        /// <param name="conn">Sql Connection.</param>
        public void ExecuteTimingStopAndAddPerfLog(SqlConnection conn)
        {
            var stopWatch = this.executeStopWatch;
            this.executeStopWatch = null;

            if (stopWatch != null && stopWatch.IsRunning)
            {
                stopWatch.Stop();
                this.ExecuteInMs = stopWatch.ElapsedMilliseconds;
                this.AddQueryPerfInfo(conn);
            }
        }

        /// <summary>
        /// Add Sql Query Performance Item.
        /// </summary>
        /// <param name="conn">Sql Connection.</param>
        private void AddQueryPerfInfo(SqlConnection conn)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;

            if (string.IsNullOrEmpty(this.QueryName) || conn == null)
            {
                return;
            }
            
            string currentUserName = LendersOffice.Security.PrincipalFactory.CurrentPrincipal?.LoginNm;
            bool includeFullQuery = this.ExecuteInMs >= FullQueryThresholdLoggingInMs || FullQueryThresholdLoggingUsers.Contains(currentUserName);
            string query;

            if (!includeFullQuery)
            {
                query = DataAccess.CLoanFileNamerExtensions.Truncate(this.QueryName, Constants.ConstApp.MaxSqlCommandTextCharacters);
            }
            else
            {
                query = this.QueryName;
            }

            if (item != null)
            {
                item.AddTimingDetail(query, this.ExecuteInMs);
                if (conn.StatisticsEnabled)
                {
                    this.GetStats(conn.RetrieveStatistics());
                    var log = this.GetConnectionResult(query, includeFullQuery);
                    item.AddQuery(log);
                }

                this.logCount++;
            }
            else if (System.Web.HttpContext.Current == null)
            {
                // also, item is null
                if (disableLoggingWithinLogging.IsValueCreated && disableLoggingWithinLogging.Value == true)
                {
                    // this is needed to prevent stack overflows where we try to send to the message queue, but need to pull from the db to get the stage config values. (or similar).
                    return;
                }

                disableLoggingWithinLogging.Value = true;

                IDictionary stats = null;

                if (conn.StatisticsEnabled)
                {
                    stats = conn.RetrieveStatistics();
                }

                var log = this.GetSqlQueryLog(query, includeFullQuery, stats, currentUserName);
                this.SendQueryToElasticSearch(log);

                disableLoggingWithinLogging.Value = false;
            }
        }

        /// <summary>
        /// Gets the statistics for the handler from the connection's statistic dictionary.
        /// </summary>
        /// <param name="stats">
        /// The connection's statistic dictionary.
        /// </param>
        private void GetStats(IDictionary stats)
        {
            if (stats.Contains("BytesReceived"))
            {
                this.BytesReceived = (long)stats["BytesReceived"];
            }

            if (stats.Contains("NetworkServerTime"))
            {
                this.NetworkServerTime = (long)stats["NetworkServerTime"];
            }

            if (stats.Contains("BytesSent"))
            {
                this.BytesSent = (long)stats["BytesSent"];
            }
        }

        /// <summary>
        /// Sends the information about the query to elastic search. <para></para>
        /// The intention is that this is for non http request only.
        /// </summary>
        /// <param name="queryLog">A class with the relevant information about the query's execution.</param>
        private void SendQueryToElasticSearch(SqlQueryLog queryLog)
        {
            if (queryLog == null)
            {
                throw new ArgumentNullException(nameof(queryLog));
            }

            string elasticSearchIndexLabel = $"ml_lqb_sql_queries-{DateTime.UtcNow:yyyy.MM.dd}/sql";

            Drivers.Gateways.MessageQueueHelper.SendJSON(Constants.ConstStage.MSMQ_ElasticSearch, elasticSearchIndexLabel, queryLog);
        }

        /// <summary>
        /// Gets the connection result as an XML element.
        /// </summary>
        /// <returns>
        /// The connection result XML element.
        /// </returns>
        /// <param name="queryName">The query that was executed.</param>
        /// <param name="includeFullQuery">Whether to include the entire query in the log.</param>
        private XElement GetConnectionResult(string queryName, bool includeFullQuery)
        {
            var queryXml = new XElement(
                "query",
                new XAttribute("name", queryName),
                new XAttribute("db", this.DatabaseName),
                new XAttribute("method", this.Method),
                new XAttribute("bytesreceived", this.BytesReceived),
                new XAttribute("execute", this.ExecuteInMs),
                new XAttribute("conopentime", this.ConectionOpenInMs),
                new XAttribute("bytessent", this.BytesSent),
                new XAttribute("networkservertime", this.NetworkServerTime),
                new XAttribute("p", EncryptionHelper.ComputeMD5Hash(this.parameters.Select(i => new[] { i.DatabaseType, i.Name, this.GetSafeValue(i.Value) }).SelectMany(p => p), toLower: true)));

            if (includeFullQuery)
            {
                queryXml.Add(new XAttribute("parameters", SerializationHelper.JsonNetSerialize(this.parameters)));
            }
   
            return queryXml;
        }

        /// <summary>
        /// Gets a representation of the query execution run that is ready for elastic search logging.
        /// </summary>
        /// <param name="queryName">The name of the query.</param>
        /// <param name="includeFullQuery">Whether or not to include all the parameters of the query.</param>
        /// <param name="stats">The stats associated with executing the query.  Can be null.</param>
        /// <param name="currentUserName">The current login name of the user of the thread.</param>
        /// <returns>A representation of the query execution run that is ready for elastic search.</returns>
        private SqlQueryLog GetSqlQueryLog(string queryName, bool includeFullQuery, IDictionary stats, string currentUserName)
        {
            // note this is based off of GetConnectionResult and GetStats methods but it lacks NetworkServerTime, method, and connopentime. These are not present in the elastic search index.
            SqlQueryLog l = new SqlQueryLog()
            {
                Type = "sql",
                Department = "lqb",
                Name = queryName,
                HashParameters = EncryptionHelper.ComputeMD5Hash(this.parameters.Select(i => new[] { i.DatabaseType, i.Name, this.GetSafeValue(i.Value) }).SelectMany(p => p), toLower: true),
                Db = this.DatabaseName,
                ProcessingTime = this.ExecuteInMs,
                UserName = currentUserName,
                Correlation = DataAccess.Tools.GetLogCorrelationId().ToString(),
                Timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"),
                CustomerCode = LendersOffice.Security.PrincipalFactory.CurrentPrincipal?.BrokerDB?.CustomerCode,
                Hostname = Environment.MachineName.ToLower()
            };

            if (stats != null)
            {
                if (stats.Contains("BytesReceived"))
                {
                    l.ReceivedBytes = (long)stats["BytesReceived"];
                }

                if (stats.Contains("BytesSent"))
                {
                    l.SentBytes = (long)stats["BytesSent"];
                }
            }

            if (includeFullQuery)
            {
                l.Parameters = SerializationHelper.JsonNetSerialize(this.parameters);
            }

            return l;
        }

        /// <summary>
        /// Gets a non null value from the given object. If it is null, the string "null" is returned.
        /// </summary>
        /// <param name="val">The value to turn into a string.</param>
        /// <returns>The string representation of the value or null otherwise.</returns>
        private string GetSafeValue(object val)
        {
            if (val == null || Convert.IsDBNull(val))
            {
                return "null";
            }
            else
            {
                return val.ToString();
            }
        }
    }
}
