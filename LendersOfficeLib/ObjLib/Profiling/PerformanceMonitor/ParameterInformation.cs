﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a parameter with specific information.
    /// </summary>
    [DataContract]
    internal class ParameterInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterInformation"/> class.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="databaseType">The database type of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        public ParameterInformation(string name, string databaseType, object value)
        {
            int precision, scale, size;
            this.Name = name;
            this.DatabaseType = databaseType;
            this.Value = this.GetValue(name, value, out precision, out scale, out size);
            this.Precision = precision;
            this.Scale = scale;
            this.Size = size;
        }

        /// <summary>
        /// Gets the name of the parameter.
        /// </summary>
        [DataMember(Name = "n")]
        public string Name { get; private set; }

        /// <summary>
        /// Gets the database type of the parameter.
        /// </summary>
        [DataMember(Name = "t")]
        public string DatabaseType { get; private set; }

        /// <summary>
        /// Gets the value of the parameter.
        /// </summary>
        [DataMember(Name = "v")]
        public string Value { get; private set; }

        /// <summary>
        /// Gets the precision of the parameter. Only applies to decimal.
        /// </summary>
        [DataMember(Name = "p", EmitDefaultValue = false)]
        public int Precision { get; private set; }

        /// <summary>
        /// Gets the size of the parameter. Only applies to text fields.
        /// </summary>
        [DataMember(Name = "l", EmitDefaultValue = false)]
        public int Size { get; private set; }

        /// <summary>
        /// Gets the scale of the parameter. Only applies to decimal.
        /// </summary>
        [DataMember(Name = "s", EmitDefaultValue = false)]
        public int Scale { get; private set; }

        /// <summary>
        /// Gets the value that will be serialized. It also mask login and
        /// passwords. If the type is a decimal, the precision and scale will be set. If the 
        /// field is a string the length will be set.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <param name="precision">The precision of the decimal.</param>
        /// <param name="scale">The scale of the decimal.</param>
        /// <param name="size">The size (only applies to strings).</param>
        /// <returns>The safe string value.</returns>
        private string GetValue(string name, object value, out int precision, out int scale, out int size)
        {
            precision = 0;
            scale = 0;
            size = 0;

            string[] maskNames = { "login", "password", "ssn", "eid", "taxid", "encrypt" };

            if (value == null || Convert.IsDBNull(value))
            {
                return null;
            }

            string result = value.ToString(); 

            if (maskNames.Any(p => name.ToLower().Contains(p)) || value is byte[])
            {
                result = "**MASKED**";
            }

            if (value.GetType().IsEnum)
            {
                result = ((Enum)value).ToString("d");
            }
            else if (value is bool)
            {
                result = ((bool)value) ? "1" : "0";
            }
            else if (value is decimal)
            {
                var sd = new System.Data.SqlTypes.SqlDecimal((decimal)value);
                precision = sd.Precision;
                scale = sd.Scale;
            }
            else if (value is string)
            {
                size = result.Length;
            }

            return result;
        }
    }
}
