﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System;
    using System.Data.Common;
    using HttpModule;

    /// <summary>
    /// A profiler using the <see cref="PerformanceMonitorItem"/> framework.
    /// </summary>
    internal class PerformanceMonitorProfiler : ILqbProfiler
    {
        /// <summary>
        /// Provides a disposable timer for profiling an operation.
        /// </summary>
        /// <param name="name">
        /// The name of the operation.
        /// </param>
        /// <returns>
        /// A disposable timer for profiling.
        /// </returns>
        public IDisposable Profile(string name) => PerformanceMonitorItem.Time(name);

        /// <summary>
        /// Wraps a database provider factory.
        /// </summary>
        /// <param name="factory">
        /// The provider factory to wrap.
        /// </param>
        /// <returns>
        /// The wrapped provider factory.
        /// </returns>
        public DbProviderFactory Wrap(DbProviderFactory factory)
        {
            return new PerformanceMonitorDbProviderFactory(factory);
        }

        /// <summary>
        /// Wraps a database connection.
        /// </summary>
        /// <param name="connection">
        /// The connection to wrap.
        /// </param>
        /// <returns>
        /// The wrapped connection.
        /// </returns>
        public DbConnection Wrap(DbConnection connection)
        {
            return new PerformanceMonitorDatabaseConnection(connection);
        }
    }
}
