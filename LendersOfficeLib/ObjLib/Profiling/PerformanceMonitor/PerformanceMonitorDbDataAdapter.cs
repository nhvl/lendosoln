﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    /// Wraps a data adapter with performance monitor profiling.
    /// </summary>
    /// <remarks>
    /// This class has been adapted from the MiniProfiler library,
    /// <c>https://github.com/MiniProfiler/dotnet/tree/v3</c>.
    /// </remarks>
    internal class PerformanceMonitorDbDataAdapter : DbDataAdapter
    {
        /// <summary>
        /// The wrapped adapter.
        /// </summary>
        private readonly DbDataAdapter wrappedAdapter;

        /// <summary>
        /// The select command.
        /// </summary>
        private DbCommand selectCommand;

        /// <summary>
        /// The insert command.
        /// </summary>
        private DbCommand insertCommand;

        /// <summary>
        /// The update command.
        /// </summary>
        private DbCommand updateCommand;

        /// <summary>
        /// The delete command.
        /// </summary>
        private DbCommand deleteCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorDbDataAdapter"/> class.
        /// </summary>
        /// <param name="wrappedAdapter">
        /// The wrapped adapter.
        /// </param>
        public PerformanceMonitorDbDataAdapter(DbDataAdapter wrappedAdapter)
        {
            this.wrappedAdapter = wrappedAdapter;
            this.InitCommands();
        }

        /// <summary>
        /// Gets the underlying adapter.
        /// </summary>
        public DbDataAdapter InternalAdapter
        {
            get { return this.wrappedAdapter; }
        }

        /// <summary>
        /// Gets or sets whether unmapped source tables or columns are passed with their source names in order to be filtered or to raise an error.
        /// </summary>
        /// <returns>One of the <see cref="T:System.Data.MissingMappingAction"/> values. The default is Passthrough.</returns>
        public new MissingMappingAction MissingMappingAction
        {
            get { return this.wrappedAdapter.MissingMappingAction; }
            set { this.wrappedAdapter.MissingMappingAction = value; }
        }

        /// <summary>
        /// Gets or sets whether missing source tables, columns, and their relationships are added to the dataset schema, ignored, or cause an error to be raised.
        /// </summary>
        /// <returns>One of the <see cref="T:System.Data.MissingSchemaAction"/> values. The default is Add.</returns>
        /// <exception cref="T:System.ArgumentException">The value set is not one of the <see cref="T:System.Data.MissingSchemaAction"/> values. </exception>
        public new MissingSchemaAction MissingSchemaAction
        {
            get { return this.wrappedAdapter.MissingSchemaAction; }
            set { this.wrappedAdapter.MissingSchemaAction = value; }
        }

        /// <summary>
        /// Gets how a source table is mapped to a dataset table.
        /// </summary>
        /// <returns>A collection that provides the master mapping between the returned records and the <see cref="T:System.Data.DataSet"/>. The default value is an empty collection.</returns>
        public new ITableMappingCollection TableMappings
        {
            get { return this.wrappedAdapter.TableMappings; }
        }

        /// <summary>
        /// Gets or sets an SQL statement used to select records in the data source.
        /// </summary>
        /// <returns>An <see cref="T:System.Data.DbCommand"/> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)"/> to select records from data source for placement in the data set.</returns>
        public new DbCommand SelectCommand
        {
            get
            {
                return this.selectCommand;
            }

            set
            {
                this.selectCommand = value;

                var cmd = value as PerformanceMonitorDbCommand;
                this.wrappedAdapter.SelectCommand = cmd == null ? value : cmd.InternalCommand;
            }
        }

        /// <summary>
        /// Gets or sets an SQL statement used to insert new records into the data source.
        /// </summary>
        /// <returns>An <see cref="T:System.Data.DbCommand"/> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)"/> to insert records in the data source for new rows in the data set.</returns>
        public new DbCommand InsertCommand
        {
            get
            {
                return this.insertCommand;
            }

            set
            {
                this.insertCommand = value;

                var cmd = value as PerformanceMonitorDbCommand;
                this.wrappedAdapter.InsertCommand = cmd == null ? value : cmd.InternalCommand;
            }
        }

        /// <summary>
        /// Gets or sets an SQL statement used to update records in the data source.
        /// </summary>
        /// <returns>An <see cref="T:System.Data.DbCommand"/> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)"/> to update records in the data source for modified rows in the data set.</returns>
        public new DbCommand UpdateCommand
        {
            get
            {
                return this.updateCommand;
            }

            set
            {
                this.updateCommand = value;

                var cmd = value as PerformanceMonitorDbCommand;
                this.wrappedAdapter.UpdateCommand = cmd == null ? value : cmd.InternalCommand;
            }
        }

        /// <summary>
        /// Gets or sets an SQL statement for deleting records from the data set.
        /// </summary>
        /// <returns>An <see cref="T:System.Data.DbCommand"/> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)"/> to delete records in the data source for deleted rows in the data set.</returns>
        public new DbCommand DeleteCommand
        {
            get
            {
                return this.deleteCommand;
            }

            set
            {
                this.deleteCommand = value;

                var cmd = value as PerformanceMonitorDbCommand;
                this.wrappedAdapter.DeleteCommand = cmd == null ? value : cmd.InternalCommand;
            }
        }

        /// <summary>
        /// Gets the parameters set by the user when executing an SQL SELECT statement.
        /// </summary>
        /// <returns>
        /// An array of <see cref="T:System.Data.IDataParameter"/> objects that contains the parameters set by the user.
        /// </returns>
        public new IDataParameter[] GetFillParameters()
        {
            return this.wrappedAdapter.GetFillParameters();
        }

        /// <summary>
        /// Initializes the commands for the wrapper by obtaining
        /// the wrapped adapter's commands.
        /// </summary>
        private void InitCommands()
        {
            if (this.wrappedAdapter.SelectCommand != null)
            {
                this.selectCommand = this.wrappedAdapter.SelectCommand;
            }

            if (this.wrappedAdapter.DeleteCommand != null)
            {
                this.deleteCommand = this.wrappedAdapter.DeleteCommand;
            }

            if (this.wrappedAdapter.UpdateCommand != null)
            {
                this.updateCommand = this.wrappedAdapter.UpdateCommand;
            }

            if (this.wrappedAdapter.InsertCommand != null)
            {
                this.insertCommand = this.wrappedAdapter.InsertCommand;
            }
        }
    }
}
