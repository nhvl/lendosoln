﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class for logging sql queries to elastic search.  See opm 478211.
    /// </summary>
    [DataContract]
    public class SqlQueryLog
    {
        [DataMember(Name = "@timestamp")]
        public string Timestamp { get; set; }

        [DataMember(Name = "hostname")]
        public string Hostname { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "department")]
        public string Department { get; set; }

        [DataMember(Name = "customer_code")]
        public string CustomerCode { get; set; }

        [DataMember(Name = "username")]
        public string UserName { get; set; }

        [DataMember(Name = "correlation")]
        public string Correlation { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "received_bytes")]
        public long ReceivedBytes { get; set; }

        [DataMember(Name = "processing_time")]
        public long ProcessingTime { get; set; }

        [DataMember(Name = "sent_bytes")]
        public long SentBytes { get; set; }

        [DataMember(Name = "hash_parameters")]
        public string HashParameters { get; set; }

        [DataMember(Name = "db")]
        public string Db { get; set; }

        [DataMember(Name = "parameters")]
        public string Parameters { get; set; }
    }
}