﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System.Data.Common;
    using System.Security;

    /// <summary>
    /// Wraps a database provider factory with performance monitoring.
    /// </summary>
    /// <remarks>
    /// This class has been adapted from the MiniProfiler library,
    /// <c>https://github.com/MiniProfiler/dotnet/tree/v3</c>.
    /// </remarks>
    internal class PerformanceMonitorDbProviderFactory : DbProviderFactory
    {
        /// <summary>
        /// The wrapped provider factory.
        /// </summary>
        private DbProviderFactory wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorDbProviderFactory"/> class.
        /// </summary>
        /// <param name="wrapped">
        /// The wrapped provider factory.
        /// </param>
        public PerformanceMonitorDbProviderFactory(DbProviderFactory wrapped)
        {
            this.wrapped = wrapped;
        }

        /// <summary>
        /// Gets a value indicating whether a data source enumerator can be created.
        /// </summary>
        public override bool CanCreateDataSourceEnumerator => this.wrapped.CanCreateDataSourceEnumerator;

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <returns>The <see cref="DbCommand"/>.</returns>
        public override DbCommand CreateCommand()
        {
            var command = this.wrapped.CreateCommand();
            return new PerformanceMonitorDbCommand(command);
        }

        /// <summary>
        /// Create the command builder.
        /// </summary>
        /// <returns>
        /// The <see cref="DbCommandBuilder"/>.
        /// </returns>
        public override DbCommandBuilder CreateCommandBuilder()
        {
            return this.wrapped.CreateCommandBuilder();
        }

        /// <summary>
        /// Create the connection.
        /// </summary>
        /// <returns>The <see cref="DbConnection"/>.</returns>
        public override DbConnection CreateConnection()
        {
            var connection = this.wrapped.CreateConnection();
            return new PerformanceMonitorDatabaseConnection(connection);
        }

        /// <summary>
        /// Create the connection string builder.
        /// </summary>
        /// <returns>
        /// The <see cref="DbConnectionStringBuilder"/>.
        /// </returns>
        public override DbConnectionStringBuilder CreateConnectionStringBuilder()
        {
            return this.wrapped.CreateConnectionStringBuilder();
        }

        /// <summary>
        /// Create the data adapter.
        /// </summary>
        /// <returns>
        /// The <see cref="DbDataAdapter"/>.
        /// </returns>
        public override DbDataAdapter CreateDataAdapter()
        {
            var dataAdapter = this.wrapped.CreateDataAdapter();
            return new PerformanceMonitorDbDataAdapter(dataAdapter);
        }

        /// <summary>
        /// Create the data source enumerator.
        /// </summary>
        /// <returns>The <see cref="DbDataSourceEnumerator"/>.</returns>
        public override DbDataSourceEnumerator CreateDataSourceEnumerator()
        {
            return this.wrapped.CreateDataSourceEnumerator();
        }

        /// <summary>
        /// Create the parameter.
        /// </summary>
        /// <returns>The <see cref="DbParameter"/>.</returns>
        public override DbParameter CreateParameter()
        {
            return this.wrapped.CreateParameter();
        }

        /// <summary>
        /// Create the permission.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <returns>The <see cref="CodeAccessPermission"/>.</returns>
        public override CodeAccessPermission CreatePermission(System.Security.Permissions.PermissionState state)
        {
            return this.wrapped.CreatePermission(state);
        }
    }
}
