﻿namespace LendersOffice.ObjLib.Profiling.PerformanceMonitor
{
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using HttpModule;

    /// <summary>
    /// Wraps a database connection with performance monitor profiling.
    /// </summary>
    /// <remarks>
    /// This class has been adapted from the MiniProfiler library,
    /// <c>https://github.com/MiniProfiler/dotnet/tree/v3</c>.
    /// </remarks>
    internal class PerformanceMonitorDatabaseConnection : DbConnection
    {
        /// <summary>
        /// The wrapped connection.
        /// </summary>
        private DbConnection wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceMonitorDatabaseConnection"/> class.
        /// </summary>
        /// <param name="wrapped">
        /// The wrapped connection.
        /// </param>
        public PerformanceMonitorDatabaseConnection(DbConnection wrapped)
        {
            this.wrapped = wrapped;

            var wrappedSqlConnection = wrapped as SqlConnection;
            if (wrappedSqlConnection != null)
            {
                wrappedSqlConnection.StatisticsEnabled = true;

                this.ChangeHandler = new PerformanceMonitorStateChangeHandler();
                wrappedSqlConnection.StateChange += new StateChangeEventHandler(this.ChangeHandler.OnChange);
            }
        }

        /// <summary>
        /// Gets the underlying connection.
        /// </summary>
        public DbConnection InternalConnection => this.wrapped;

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public override string ConnectionString
        {
            get { return this.wrapped.ConnectionString; }
            set { this.wrapped.ConnectionString = value; }
        }

        /// <summary>
        /// Gets the connection timeout.
        /// </summary>
        public override int ConnectionTimeout => this.wrapped.ConnectionTimeout;

        /// <summary>
        /// Gets the database.
        /// </summary>
        public override string Database => this.wrapped.Database;

        /// <summary>
        /// Gets the data source.
        /// </summary>
        public override string DataSource => this.wrapped.DataSource;

        /// <summary>
        /// Gets the server version.
        /// </summary>
        public override string ServerVersion => this.wrapped.ServerVersion;

        /// <summary>
        /// Gets the state.
        /// </summary>
        public override ConnectionState State => this.wrapped.State;

        /// <summary>
        /// Gets or sets the change handler for the connection.
        /// </summary>
        private PerformanceMonitorStateChangeHandler ChangeHandler { get; set; }

        /// <summary>
        /// Change the database.
        /// </summary>
        /// <param name="databaseName">
        /// The new database name.
        /// </param>
        public override void ChangeDatabase(string databaseName) => this.wrapped.ChangeDatabase(databaseName);

        /// <summary>
        /// Closes the connection.
        /// </summary>
        public override void Close() => this.wrapped.Close();

        /// <summary>
        /// Opens the connection.
        /// </summary>
        public override void Open() => this.wrapped.Open();

        /// <summary>
        /// Gets the schema.
        /// </summary>
        /// <returns>
        /// The <see cref="DataTable"/> for the schema.
        /// </returns>
        public override DataTable GetSchema() => this.wrapped.GetSchema();

        /// <summary>
        /// Gets the schema.
        /// </summary>
        /// <param name="collectionName">
        /// The schema name.
        /// </param>
        /// <returns>
        /// The <see cref="DataTable"/> for the schema.
        /// </returns>
        public override DataTable GetSchema(string collectionName) => this.wrapped.GetSchema(collectionName);

        /// <summary>
        /// Gets the schema.
        /// </summary>
        /// <param name="collectionName">
        /// The schema name.
        /// </param>
        /// <param name="restrictionValues">
        /// The restriction values.
        /// </param>
        /// <returns>
        /// The <see cref="DataTable"/> for the schema.
        /// </returns>
        public override DataTable GetSchema(string collectionName, string[] restrictionValues)
        {
            return this.wrapped.GetSchema(collectionName, restrictionValues);
        }

        /// <summary>
        /// Begins the database transaction.
        /// </summary>
        /// <param name="isolationLevel">
        /// The isolation level.
        /// </param>
        /// <returns>
        /// The <see cref="DbTransaction"/>.
        /// </returns>
        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel) => this.wrapped.BeginTransaction(isolationLevel);

        /// <summary>
        /// Creates a database command.
        /// </summary>
        /// <returns>
        /// The <see cref="DbCommand"/>.
        /// </returns>
        protected override DbCommand CreateDbCommand()
        {
            var wrappedCommand = this.wrapped.CreateCommand();
            return new PerformanceMonitorDbCommand(this.ChangeHandler, wrappedCommand);
        }

        /// <summary>
        /// Dispose the underlying connection.
        /// </summary>
        /// <param name="disposing">
        /// False if pre-empted from a <c>finalizer</c>
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.wrapped != null)
            {
                this.wrapped.Dispose();
            }

            this.wrapped = null;
            base.Dispose(disposing);
        }
    }
}
