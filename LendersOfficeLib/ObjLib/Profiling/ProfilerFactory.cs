﻿namespace LendersOffice.ObjLib.Profiling
{
    using PerformanceMonitor;

    /// <summary>
    /// Provides a means for instantiating profilers.
    /// </summary>
    public static class ProfilerFactory
    {
        /// <summary>
        /// Creates a profiler.
        /// </summary>
        /// <returns>
        /// The profiler.
        /// </returns>
        public static ILqbProfiler Create()
        {
#if LQB_NOPROFILE
            return new NullProfiler();
#else
            return new PerformanceMonitorProfiler();
#endif
        }
    }
}
