﻿namespace LendersOffice.ObjLib.Profiling
{
    using System;
    using System.Data.Common;

    /// <summary>
    /// Provides a profiler that does not perform any actual profiling.
    /// </summary>
    internal class NullProfiler : ILqbProfiler
    {
        /// <summary>
        /// Returns a disposable object that does not perform any profiling.
        /// </summary>
        /// <param name="name">
        /// The name for the timing.
        /// </param>
        /// <returns>
        /// The disposable object.
        /// </returns>
        public IDisposable Profile(string name) => new NullTimer();

        /// <summary>
        /// Returns the specified <paramref name="connection"/>.
        /// </summary>
        /// <param name="connection">
        /// The connection to return.
        /// </param>
        /// <returns>
        /// The original connection, as no wrapping is performed.
        /// </returns>
        public DbConnection Wrap(DbConnection connection) => connection;

        /// <summary>
        /// Returns the specified <paramref name="factory"/>.
        /// </summary>
        /// <param name="factory">
        /// The factory to return.
        /// </param>
        /// <returns>
        /// The original factory, as no wrapping is performed.
        /// </returns>
        public DbProviderFactory Wrap(DbProviderFactory factory) => factory;

        /// <summary>
        /// Provides a disposable timer that performs no actual timing.
        /// </summary>
        private class NullTimer : IDisposable
        {
            /// <summary>
            /// Disposes the timer.
            /// </summary>
            public void Dispose()
            {
            }
        }
    }
}
