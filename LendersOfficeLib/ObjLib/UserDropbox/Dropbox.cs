﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using DataAccess;
using EDocs;
using iTextSharp.text.pdf;
using LendersOffice.Edocs.DocMagic;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using System.Data.Common;

namespace LendersOffice.ObjLib.UserDropbox
{
    public class Dropbox
    {
        public static readonly string DropboxPrefix = "DROPBOX_";
        public static string FileDBName(Guid FileId)
        {
            return DropboxPrefix + FileId.ToString();
        }
        public static void AddFile(Guid UserId, Guid BrokerId, string FileName, DateTime Uploaded, byte[] FileBytes)
        {
            Guid FileId = Guid.NewGuid();
            int SizeBytes = FileBytes.Length;
            bool isESigned = IsESigned(UserId, BrokerId, FileName, FileBytes);

            var parms = new[]
            {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@FileId", FileId),
                new SqlParameter("@FileName", FileName),
                new SqlParameter("@SizeBytes", SizeBytes),
                new SqlParameter("@UploadedOn", Uploaded),
                new SqlParameter("@IsESigned", isESigned)
            };

            FileDBTools.WriteData(E_FileDB.Normal, FileDBName(FileId), FileBytes);
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "DROPBOX_AddFile", 2, parms);
        }

        /// <summary>
        /// Determines whether the file is an esigned pdf.
        /// </summary>
        /// <param name="UserId">The user id.</param>
        /// <param name="BrokerId">The broker id.</param>
        /// <param name="FileName">The file name.</param>
        /// <param name="FileBytes">The file content.</param>
        /// <returns></returns>
        private static bool IsESigned(Guid UserId, Guid BrokerId, string FileName, byte[] FileBytes)
        {
            // We cannot fully trust the file extension to determine whether we check
            // if the file is an esigned pdf. Since we already have the bytes loaded
            // into memory and (I'm guessing) the majority of the documents that come
            // through are pdfs, I don't think this technique will cause issues.
            try
            {
                var reader = EDocumentViewer.CreatePdfReader(FileBytes);
                return EDocument.ContainsSignature(reader);
            }
            catch (InsufficientPdfPermissionException)
            {
                Tools.LogWarning("Insufficient permissions to determine if dropbox file is esigned.");
            }
            catch (Exception e) when (e is InvalidPDFFileException || e is CBaseException)
            {
                if (InputMayBePdf(FileName))
                {
                    var msg = "Unable to load pdf to determine if it was signed. File may not be a pdf. "
                        + $"Filename: {FileName}, User ID: {UserId.ToString()}, Broker ID: {BrokerId.ToString()}";
                    Tools.LogError(msg, e);
                }
            }

            return false;
        }

        /// <summary>
        /// Tries to guess if we expect a file to be a pdf based on the file name.
        /// </summary>
        /// <param name="filename">The file name.</param>
        /// <returns>True if we think the file may be a pdf.</returns>
        private static bool InputMayBePdf(string filename)
        {
            filename = filename.ToLower();

            if (filename.EndsWith(".xml")
                || filename.EndsWith(".xls")
                || filename.EndsWith(".xlsx"))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Special add function used for uploading dropbox files via web service.
        /// Runs docs through PDFProcessor before saving to EDocs or adding to Dropbox
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="FileName"></param>
        /// <param name="Uploaded"></param>
        /// <param name="FileBytes"></param>
        public static void UploadFile(Guid UserId, Guid BrokerId, string FileName, DateTime Uploaded, byte[] FileBytes)
        {
            // Make sure the doc is a pdf
            var pdf = EDocumentViewer.CreatePdfReader(FileBytes);

            // Write to fileDB
            Guid key = Guid.NewGuid();
            FileDBTools.WriteData(E_FileDB.Temp, key.ToString(), FileBytes);

            DocMagicPDFData uploadData = new DocMagicPDFData()
            {
                UploadingUserId = UserId,
                AppId = Guid.Empty,     // will get from barcode (maybe)
                BrokerId = BrokerId,
                LoanId = Guid.Empty,    // will get from barcode (maybe)
                InternalNotes = "",     // string.Empty, because leaving null causes errors
                Description = "",       // string.Empty, because leaving null causes errors
                FileDB = E_FileDB.Temp,
                FileDBKey = key,
                PageCount = pdf.NumberOfPages,

                // Dropbox specific fields
                isDropboxFile = true,
                DropboxFileName = FileName,
                DropboxUploadD = Uploaded
            };

            DBMessageQueue mainQ = new DBMessageQueue(ConstMsg.DocMagicDocSplitterQueue);
            mainQ.Send(key.ToString(), uploadData.Serialize());
        }

        /// <summary>
        /// Removes a file from a user's dropbox and from the FileDB. 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="FileId"></param>
        /// <returns>True if the file was present in the DB and was deleted; false if the file was not present in the DB</returns>
        public static bool RemoveFile(DbConnectionInfo connInfo, Guid UserId, Guid FileId)
        {
            var parms = new[]
            {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@FileId", FileId),
            };

            int count = 0;

            try
            {
                count = StoredProcedureHelper.ExecuteNonQuery(connInfo, "DROPBOX_RemoveFile", 1, parms);
            }
            catch (SqlException e)
            {
                Tools.LogErrorWithCriticalTracking(e);
            }

            try
            {
                FileDBTools.Delete(E_FileDB.Normal, FileDBName(FileId));
            }
            catch (FileNotFoundException e)
            {
                Tools.LogError(e);
            }
            catch (System.Net.Sockets.SocketException e)
            {
                Tools.LogErrorWithCriticalTracking("FileDB does not appear to be running. Tried to delete key " + FileDBName(FileId), e);
            }

            return count > 0;
        }

        public static IEnumerable<DropboxFile> GetFilesForUser(DbConnectionInfo connInfo, Guid UserId, IEnumerable<Guid> docIds)
        {
            var allFiles = GetFilesForUser(connInfo, UserId);
            return from id in docIds select allFiles.Where(a => a.FileId == id).First();
        }

        public static IEnumerable<DropboxFile> GetFilesForUser(DbConnectionInfo connInfo, Guid UserId)
        {
            var parms = new[] 
            {
                new SqlParameter("@UserId", UserId)
            };
            
            using (var datareader = StoredProcedureHelper.ExecuteReader(connInfo, "DROPBOX_RetrieveFiles", parms))
            {
                while (datareader.Read())
                {
                    yield return new DropboxFile(datareader);
                }
            }
        }

        /// <summary>
        /// Counts the number of files in Drobox and LostDocs for this broker.
        /// </summary>
        /// <param name="connInfo"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int CountFilesInDropbox(DbConnectionInfo connInfo, Guid UserId)
        {
            var parms = new[] 
            {
                new SqlParameter("@UserId", UserId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "DROPBOX_CountFiles", parms))
            {
                if (reader.Read())
                {
                    return (int)reader["NumFiles"];
                }
            }


            return 0;
        }

        public class DropboxFile
        {

            public Guid UserId { get; private set; }

            /// <summary>
            /// The file Id used by FileDB
            /// </summary>
            public Guid FileId { get; private set; }

            /// <summary>
            /// The name the user provided for this file (show this to the user)
            /// </summary>
            public string FileName { get; private set; }
            
            public int SizeBytes { get; private set; }

            public DateTime UploadedOn { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the pdf content contains signatures.
            /// </summary>
            public bool IsESigned { get; private set; }

            internal DropboxFile(DbDataReader dr)
            {
                UserId = new Guid(dr["UserId"].ToString());
                FileId = new Guid(dr["FileId"].ToString());
                FileName = dr["FileName"].ToString();
                SizeBytes = int.Parse(dr["SizeBytes"].ToString());
                UploadedOn = DateTime.Parse(dr["UploadedOn"].ToString());
                this.IsESigned = (bool)dr["IsESigned"];
            }

            public byte[] GetFileBytes()
            {
                return FileDBTools.ReadData(E_FileDB.Normal, FileDBName(FileId));
            }

            public string GetPdfFile()
            {
                return FileDBTools.CreateCopy(E_FileDB.Normal, FileDBName(FileId));
            }
        }
    }
}