namespace LendersOffice.ObjLib.CustomMergeFields
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Xml;

    public class CustomMergeCategory 
    {
        private string m_name;
        private string m_special;
        public CustomMergeCategory(string name, string special) 
        {
            m_name = name;
            m_special = special;
        }
        public string Name 
        {
            get { return m_name; }
        }
        public string Special 
        {
            get { return m_special; }
        }
        public override bool Equals(object o) 
        {
            CustomMergeCategory a = o as CustomMergeCategory;
            return a != null && a.Name == this.Name;
        }
        public override int GetHashCode() 
        {
            return m_name.GetHashCode();
        }
    }
    public class CustomMergeField 
    {
        private string m_wordCode;
        private string m_fieldID;

        public CustomMergeField(string wordCode, string fieldID) 
        {
            m_wordCode = wordCode;
            m_fieldID = fieldID;
        }

        public string WordCode 
        {
            get { return m_wordCode; }
        }

        public string FieldID 
        {
            get { return m_fieldID; }
        }

        public string FormattedWordCodeField => $"�{this.WordCode}�";

        public override bool Equals(object o) 
        {
            CustomMergeField a = o as CustomMergeField;
            return a != null && a.WordCode == this.WordCode && a.FieldID == this.FieldID;
        }
        public override int GetHashCode() 
        {
            return (m_wordCode + ":" + m_fieldID).GetHashCode();
        }
    }
    /// <summary>
    /// Summary description for CustomMergeFields.
    /// </summary>
    public class CustomMergeFieldRepository
    {
        private static ArrayList s_allFields;
        private static ArrayList s_allCategories;
        private static Hashtable s_hash;
        private static readonly Dictionary<string, string> fieldIdsByWordCode;
        static CustomMergeFieldRepository() 
        {
            s_allCategories = new ArrayList();
            s_allFields = new ArrayList();
            s_hash = new Hashtable();
            fieldIdsByWordCode = new Dictionary<string, string>(System.StringComparer.OrdinalIgnoreCase);

            XmlDocument doc = new XmlDocument();

            const string resourceName = "LendersOffice.ObjLib.CustomMergeFields.CustomMergeFields.xml.config";
            using (var stream = typeof(CustomMergeFieldRepository).Assembly.GetManifestResourceStream(resourceName))
            {
                doc.Load(stream);
            }

            XmlElement root = (XmlElement) doc.SelectSingleNode("//mergefields");

            foreach (XmlElement categoryElement in root.ChildNodes) 
            {
                string categoryName = categoryElement.GetAttribute("name");
                string special = categoryElement.GetAttribute("special");
                CustomMergeCategory category = new CustomMergeCategory(categoryName, special);

                if (!s_allCategories.Contains(category))
                    s_allCategories.Add(category);
                foreach (XmlElement fieldElement in categoryElement.ChildNodes) 
                {   
                    CustomMergeField field = new CustomMergeField(fieldElement.GetAttribute("word_code"), fieldElement.GetAttribute("field_id"));
                    addToList(field, categoryName);
                        
                }
            }

        }


        //OPM 7876 3/25/09 jk - New fields will now get sorted as they move into the list. In the UI, each list will be sorted, not
        // in the same order they are in in the xml file.
        private static void addToList(CustomMergeField field, string categoryName)
        {
            if (!s_allFields.Contains(field))
            {
                s_allFields.Add(field);
                fieldIdsByWordCode.Add(field.WordCode, field.FieldID);
            }

            ArrayList list = (ArrayList)s_hash[categoryName];
            if (null == list)
            {
                list = new ArrayList();
                s_hash[categoryName] = list;
            }

            int fieldToAdd = -1;
            if (categoryName.ToLower().Equals("custom fields"))
            {
                fieldToAdd = GetCustomFieldNumber(field.WordCode);
            }

            CustomMergeField cmField = null;
            for (int i = 0; i < list.Count; i++)
            {
                cmField = ((CustomMergeField)list[i]);

                if (categoryName.ToLower().Equals("custom fields"))
                {
                    int currentField = GetCustomFieldNumber(cmField.WordCode);
                    if (fieldToAdd < currentField)
                    {
                        list.Insert(i, field);
                        return;
                    }
                    else if (fieldToAdd == currentField)
                    {
                        if (field.WordCode.CompareTo(cmField.WordCode.ToString()) < 0)
                        {
                            list.Insert(i, field);
                            return;
                        }
                    }
                }
                else
                {
                    if (field.WordCode.CompareTo(cmField.WordCode.ToString()) < 0)
                    {
                        list.Insert(i, field);
                        return;
                    }
                }
            }
           list.Add(field);
        }

        private static int GetCustomFieldNumber(string customFieldName)
        {
            string[] tokens = customFieldName.Split('_');

            if (tokens.Length >= 4)
            {
                try
                {
                    // Example field name: Custom_Field_1_Amount
                    // The 3rd token is this format is the digit we are looking for
                    string sfieldNumber = tokens[2];
                    return int.Parse(sfieldNumber);
                }
                catch { }
            }
            return -1;
        }


        /// <summary>
        /// Return a flatten list of all available merge fields. This list is unique.
        /// </summary>
        public static ArrayList AllFields 
        {
            get { return s_allFields; }
        }

        public static ArrayList AllCategories
        {
            get { return s_allCategories; }
        }

        public static ArrayList GetFields(string category) 
        {
            return (ArrayList) s_hash[category];
        }

        public static IReadOnlyDictionary<string, string> FieldIdsByWordCode => new ReadOnlyDictionary<string, string>(fieldIdsByWordCode);
    }
}
