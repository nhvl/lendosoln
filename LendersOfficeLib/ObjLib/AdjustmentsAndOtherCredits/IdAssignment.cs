﻿namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// The way of getting the id given another instance.
    /// </summary>
    public enum IdAssignment
    {
        /// <summary>
        /// It would be rare to need this unless duplicating an id, since we only care about uniqueness of ids per loan.  Perhaps from templates / another loan.
        /// </summary>
        GenerateNew,

        /// <summary>
        /// Copies the id from the other instance.  Essentially when it's from the same loan.  E.g. it's a lead being converted to a loan.
        /// </summary>
        Copy,

        /// <summary>
        /// When we don't want to copy or generate a new id.
        /// </summary>
        LeaveIdUnchanged,
    }
}
