﻿namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using Migration;
    using Newtonsoft.Json;

    /// <summary>
    /// Base class for Adjustments in Adjustments and Other Credits page.
    /// </summary>
    public class AdjustmentBase
    {
        /// <summary>
        /// A bit indicating whether the adjustment is deducted from loan proceeds.
        /// </summary>
        private bool dflp;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdjustmentBase"/> class. 
        /// Default constructor for adjustment base.
        /// </summary>
        public AdjustmentBase()
        {
            this.PaidFromParty = E_PartyT.Blank;
            this.PaidToParty = E_PartyT.Blank;
            this.PopulateTo = E_PopulateToT.DoNotPopulate;
            this.POC = false;
            this.DFLP = false;
            this.AdjustmentType = E_AdjustmentT.Blank;
        }

        /// <summary>
        /// Gets or sets the type definition for the adjustment.
        /// </summary>
        /// <value>The type of the adjustment.</value>
        [JsonProperty]
        public E_AdjustmentT AdjustmentType
        {
            get
            {
                return this.ProtectedAdjustmentType;
            }

            set
            {
                this.ProtectedAdjustmentType = value;
            }
        }

        /// <summary>
        /// Gets or sets the party responsible for making the payment.
        /// </summary>
        /// <value>The party responsible for making the payment.</value>
        [JsonProperty]
        public E_PartyT PaidFromParty { get; set; }

        /// <summary>
        /// Gets or sets the party that will receive the payment.
        /// </summary>
        /// <value>The party that will receive the payment.</value>
        [JsonProperty]
        public E_PartyT PaidToParty { get; set; }

        /// <summary>
        /// Gets or sets where the adjustment info should populate to.
        /// </summary>
        /// <value>A value indicating if populating to anywhere.</value>
        [JsonProperty]
        public E_PopulateToT PopulateTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the adjustment populates to line L on the 1003.
        /// </summary>
        /// <value>True iff PopulateTo is LineLOn1003.</value>
        [JsonProperty("IsPopulateToLineLOn1003")]
        public bool IsPopulateToLineLOn1003
        {
            get
            {
                if (this.POC == true)
                {
                    return false;
                }

                if (this.IsPopulateToLineLOn1003Disabled)
                {
                    return false;
                }

                return this.PopulateTo == E_PopulateToT.LineLOn1003;
            }            
        }

        /// <summary>
        /// Gets or sets a value indicating whether the adjustment is paid outside of closing.
        /// </summary>
        /// <value>A value indicating whether the adjustment is paid outside of closing.</value>
        [JsonProperty("POC")]
        public bool POC
        {
            get;
            protected set;
        }        

        /// <summary>
        /// Gets or sets a value indicating whether the adjustment is deducted from loan proceeds.
        /// </summary>
        /// <value>A value indicating whether the adjustment is deducted from loan proceeds.</value>
        [JsonProperty]
        public bool DFLP
        {
            get
            {
                if (this.PaidToParty != E_PartyT.Lender)
                {
                    return false;
                }

                return this.dflp;
            }
             
            protected set
            {
                this.dflp = value;
            }
        }

        /// <summary>
        /// Gets or sets the description of the adjustment.
        /// </summary>
        /// <value>The description of the adjustment.</value>
        [JsonProperty]
        public string Description { get; set; }

        /// <summary>
        /// Gets the string that represents the adjustment type.
        /// </summary>
        /// <value>String that represents the adjustment type.</value>
        [JsonProperty]
        public string AdjustmentTypeString
        {
            get
            {
                return this.AdjustmentType.ToString();
            }
        }

        /// <summary>
        /// Gets a value indicating whether populating to 1003 line L is disabled.  Still need to check against the loans sLoads1003LineLFromAdjustments.
        /// </summary>
        /// <value>Still need to check against the loans sLoads1003LineLFromAdjustments.</value>
        public bool IsPopulateToLineLOn1003Disabled
        {
            get
            {
                return this.PaidFromParty != E_PartyT.Borrower && this.PaidToParty != E_PartyT.Borrower;
            }
        }

        /// <summary>
        /// Gets or sets the loan version that the loan is currently in.
        /// </summary>
        /// <value>Loan version.</value>
        protected LoanVersionT LoanVersionT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the adjustment type.
        /// </summary>
        /// <value>Adjustment type.</value>
        protected E_AdjustmentT ProtectedAdjustmentType { get; set; }

        /// <summary>
        /// Sets the loan version that will be used by this adjustment.
        /// </summary>
        /// <param name="loanVersion">The loan version to use.</param>
        public void SetLoanVersion(LoanVersionT loanVersion)
        {
            this.LoanVersionT = loanVersion;
        }

        /// <summary>
        /// Provides a way of setting the POC or DFLP settings.
        /// </summary>
        /// <param name="setting">Field setting.</param>
        public void SetPocDflp(PocDflp setting)
        {
            switch (setting)
            {
                case PocDflp.Neither:
                    this.POC = false;
                    this.DFLP = false;
                    break;
                case PocDflp.Poc:
                    this.POC = true;
                    this.DFLP = false;
                    break;
                case PocDflp.Dflp:
                    this.POC = false;
                    this.DFLP = true;
                    break;
                default:
                    throw new UnhandledEnumException(setting);
            }
        }

        /// <summary>
        /// Sets POC to false.
        /// </summary>
        public void ClearPoc()
        {
            this.POC = false;
        }

        /// <summary>
        /// Sets DFLP to false.
        /// </summary>
        public void ClearDflp()
        {
            this.DFLP = false;
        }

        /// <summary>
        /// Sets the PopulateTo enum.
        /// </summary>
        /// <param name="populate">True will populate this adjustment data to Line L on 1003, false otherwise.</param>
        /// <remarks>Check the actual UI to make sure the circular dependency does not exist.</remarks>
        public void SetIsPopulateToLineLOn1003(bool populate)
        {
            this.PopulateTo = populate ? E_PopulateToT.LineLOn1003 : E_PopulateToT.DoNotPopulate;
        }
    }
}
