﻿// <copyright file="AdjustmentsAndOtherCreditsViewModel.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/25/15
// </summary>

namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup;
    using LendersOffice.ObjLib.QualifyingBorrower;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// The model used for the Adjustments and Other Credits page.
    /// </summary>
    public class AdjustmentsAndOtherCreditsViewModel
    {
        /// <summary>
        /// The path for virtual root.
        /// </summary>
        public string VRoot { get; set; }

        /// <summary>
        /// Whether or not the migration is disabled.
        /// </summary>
        public bool DisableUserMigrationToAdjustmentsMode { get; set; }

        /// <summary>
        /// The loan value of sPropertyTransferD.
        /// </summary>
        public string sPropertyTransferD { get; set; }

        /// <summary>
        /// The loan value of sPropertyTransferDLckd.
        /// </summary>
        public bool sPropertyTransferDLckd { get; set; }


        /// <summary>
        /// The loan value of sAdjustmentList.
        /// </summary>
        public AdjustmentList AdjustmentList { get; set; }

        public IEnumerable<Adjustment> AssetAdjustments { get; set; }
        public IEnumerable<Adjustment> AdjustmentListWithAssets { get; set; }

        /// <summary>
        /// The loan value of sProrationList.
        /// </summary>
        public ProrationList ProrationList { get; set; }

        /// <summary>
        /// The loan value of sPurchasePrice1003.
        /// </summary>
        public string sPurchasePrice1003 { get; set; }

        /// <summary>
        /// The loan value of sGrossDueFromBorrPersonalProperty.
        /// </summary>
        public string sGrossDueFromBorrPersonalProperty { get; set; }

        /// <summary>
        /// The loan value of sTRIDClosingDisclosureBorrowerCostsPaidAtClosing.
        /// </summary>
        public string sTRIDClosingDisclosureBorrowerCostsPaidAtClosing { get; set; }

        /// <summary>
        /// The loan value of sTRIDCashDeposit.
        /// </summary>
        public string sTRIDCashDeposit { get; set; }

        /// <summary>
        /// The loan value of sFinalLAmt.
        /// </summary>
        public string sFinalLAmt { get; set; }

        /// <summary>
        /// The loan value of sLoads1003LineLFromAdjustments.
        /// </summary>
        public bool sLoads1003LineLFromAdjustments { get; set; }

        /// <summary>
        /// The loan value of sReductionsDueToSellerExcessDeposit.
        /// </summary>
        public string sReductionsDueToSellerExcessDeposit { get; set; }

        /// <summary>
        /// The loan value of sTotalSellerPaidClosingCostsPaidAtClosing.
        /// </summary>
        public string sTotalSellerPaidClosingCostsPaidAtClosing { get; set; }

        /// <summary>
        /// The loan value of sReductionsDueToSellerPayoffOf1stMtgLoan.
        /// </summary>
        public string sReductionsDueToSellerPayoffOf1stMtgLoan { get; set; }

        /// <summary>
        /// The loan value of sReductionsDueToSellerPayoffOf2ndMtgLoan.
        /// </summary>
        public string sReductionsDueToSellerPayoffOf2ndMtgLoan { get; set; }

        /// <summary>
        /// The loan value of sRefPdOffAmt1003.
        /// </summary>
        public string sRefPdOffAmt1003 { get; set; }

        /// <summary>
        /// The loan value of sRefPdOffAmt1003Lckd.
        /// </summary>
        public bool sRefPdOffAmt1003Lckd { get; set; }

        public string sONewFinBal { get; set; }
        public bool sIsRenovationLoan { get; set; }
        public bool sAltCostLckd { get; set; }
        public string sAltCost { get; set; }
        public bool sTRIDClosingDisclosureAltCostLckd { get; set; }
        public string sTRIDClosingDisclosureAltCost { get; set; }
        public string sRenoFeesInClosingCosts { get; set; }
        public bool IncludeAlterationsImprovementsRepairsBreakdown { get; set; }
        public bool sTotCcPbsLocked { get; set; }
        public string sTotCcPbs { get; set; }
        public string sLandIfAcquiredSeparately1003 { get; set; }
        public string sOCredit1Desc { get; set; }
        public bool sOCredit1Lckd { get; set; }
        public string sOCredit1Amt { get; set; }
        public string sOCredit2Desc { get; set; }
        public string sOCredit2Amt { get; set; }
        public bool sTotEstPp1003Lckd { get; set; }
        public string sTotEstPp1003 { get; set; }
        public string sOCredit3Desc { get; set; }
        public string sOCredit3Amt { get; set; }
        public string sOCredit4Desc { get; set; }
        public string sOCredit4Amt { get; set; }
        public string sOCredit5Amt { get; set; }
        public string sONewFinCc { get; set; }
        public bool sTotEstCc1003Lckd { get; set; }
        public string sTotEstCcNoDiscnt1003 { get; set; }
        public bool sLAmtLckd { get; set; }
        public string sLAmtCalc { get; set; }
        public bool sFfUfmip1003Lckd { get; set; }
        public string sFfUfmip1003 { get; set; }
        public string sFfUfmipFinanced { get; set; }
        public bool sLDiscnt1003Lckd { get; set; }
        public string sLDiscnt1003 { get; set; }
        public string sTotTransC { get; set; }
        public bool sTransNetCashLckd { get; set; }
        public string sTransNetCash { get; set; }
        public string sONewFinNetProceeds { get; set; }
        public bool sONewFinNetProceedsLckd { get; set; }
        public string sTotEstPp { get; set; }
        public string sTotalBorrowerPaidProrations { get; set; }
        public string sTotEstCcNoDiscnt { get; set; }
        public bool sIsIncludeProrationsInTotPp { get; set; }
        public bool sIsIncludeONewFinCcInTotEstCc { get; set; }
        public List<PredefinedAdjustment> predefinedAdjustmentsList { get; set; }
        public bool sIsRestrictAdjustmentsAndOtherCreditsDescriptions { get; set; }
        public ReadOnlyDictionary<E_AdjustmentT, PredefinedAdjustment> PredefinedAdjustmentTypeMap { get; set; }
        public bool ExcludeSalesPriceAndFirstLienLoanAmountCredit { get; set; }
        public bool ExcludeDebtsToBePaidOff { get; set; }
        public bool IsTrid2 { get; set; }
        public bool IncludeInLeCdForThisLien { get; set; }

        public QualifyingBorrowerModel qualifyingBorrowerModel { get; set; }
    }
}
