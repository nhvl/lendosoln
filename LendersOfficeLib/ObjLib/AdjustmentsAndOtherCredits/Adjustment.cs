﻿// <copyright file="Adjustment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/23/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup;
    using Newtonsoft.Json;

    /// <summary>
    /// Enum for providing mutually exclusive POC/DFLP settings.
    /// </summary>
    public enum PocDflp
    {
        /// <summary>
        /// Set both to false.
        /// </summary>
        Neither = 0,

        /// <summary>
        /// Set POC to true, DFLP to false.
        /// </summary>
        Poc = 1,

        /// <summary>
        /// Set DFLP to true, POC to false.
        /// </summary>
        Dflp = 2
    }

    /// <summary>
    /// Represents an non-proration adjustment.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Adjustment : AdjustmentBase, IEquatable<Adjustment>
    {
        /// <summary>
        /// The preset description for employer assisted housing.
        /// </summary>
        public const string EmployerAssistedHousingDesc = "employer assisted housing";

        /// <summary>
        /// The preset description for a lease purchase fund.
        /// </summary>
        public const string LeasePurchaseFundDesc = "lease purchase fund";

        /// <summary>
        /// The preset description for relocation funds.
        /// </summary>
        public const string ReloactionFundsDesc = "relocation funds";

        /// <summary>
        /// The preset description for the seller credit.
        /// </summary>
        public const string SellerCreditDesc = "seller credit";

        /// <summary>
        /// The preset description for the TRID 2017 principal reduction adjustment to
        /// reduce the cash to the borrower.
        /// </summary>
        public const string PrincipalReductionToReduceCashToBorrowerDesc = "principal reduction to reduce cash to borrower";

        /// <summary>
        /// The preset description for the TRID 2017 principal reduction adjustment to
        /// cure a tolerance violation.
        /// </summary>
        public const string PrincipalReductionToCureToleranceViolationDesc = "principal reduction to cure tolerance violation";

        /// <summary>
        /// The converter for translating between string/non-string property 
        /// values.
        /// </summary>
        private LosConvert losConvert;

        /// <summary>
        /// The amount paid.
        /// </summary>
        private decimal amount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Adjustment" /> class.
        /// </summary>
        public Adjustment()
        {
            this.losConvert = new LosConvert();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Adjustment" /> class. 
        /// Infers the From / To party types from the description and amount.
        /// Makes the amount positive.
        /// </summary>
        /// <param name="description">The adjustment description.</param>
        /// <param name="amount">The adjustment amount.</param>
        public Adjustment(string description, string amount)
            : this()
        {
            this.Description = description;
            this.Amount_rep = amount;

            this.PaidFromParty = Adjustment.DefaultPaidFromPartyTForAdjustmentT(this.AdjustmentType) ?? E_PartyT.Other;
            this.PaidToParty = Adjustment.DefaultPaidToPartyTForAdjustmentT(this.AdjustmentType) ?? E_PartyT.Borrower;

            if (this.Amount < 0)
            {
                this.Amount = -this.Amount;

                var oldPaidFrom = this.PaidFromParty;
                this.PaidFromParty = this.PaidToParty;
                this.PaidToParty = oldPaidFrom;
            }

            this.IncludeAdjustmentInLeCdForThisLien = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Adjustment" /> class. 
        /// Sets the loan version to 24 in order to allow predefined adjustments.
        /// Infers the From / To party types from the description and amount.
        /// Makes the amount positive.
        /// </summary>
        /// <param name="description">The adjustment description.</param>
        /// <param name="amount">The adjustment amount.</param>
        /// <param name="adjustmentType">The adjustment type.</param>
        /// <param name="loanVersion">The loan version.</param>
        public Adjustment(string description, string amount, E_AdjustmentT adjustmentType, LoanVersionT loanVersion)
            : this()
        {
            this.LoanVersionT = loanVersion;
            this.AdjustmentType = adjustmentType;

            this.Description = description;
            this.Amount_rep = amount;

            this.PaidFromParty = Adjustment.DefaultPaidFromPartyTForAdjustmentT(this.AdjustmentType) ?? E_PartyT.Other;
            this.PaidToParty = Adjustment.DefaultPaidToPartyTForAdjustmentT(this.AdjustmentType) ?? E_PartyT.Borrower;

            if (this.Amount < 0)
            {
                this.Amount = -this.Amount;

                var oldPaidFrom = this.PaidFromParty;
                this.PaidFromParty = this.PaidToParty;
                this.PaidToParty = oldPaidFrom;
            }

            this.IncludeAdjustmentInLeCdForThisLien = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Adjustment" /> class.
        /// </summary>
        /// <param name="description">The adjustment description.</param>
        /// <param name="paidFrom">The party that will make the payment.</param>
        /// <param name="paidTo">The party that will receive the payment.</param>
        /// <param name="populateTo">Where the adjustment should flow.</param>
        /// <param name="adjustmentType">The Adjustment type.</param>
        public Adjustment(string description, E_PartyT paidFrom, E_PartyT paidTo, E_PopulateToT populateTo, E_AdjustmentT? adjustmentType)
            : this()
        {
            this.Description = description;
            this.PaidFromParty = paidFrom;
            this.PaidToParty = paidTo;
            this.PopulateTo = populateTo;

            if (adjustmentType.HasValue)
            {
                this.AdjustmentType = adjustmentType.Value;
            }

            this.IncludeAdjustmentInLeCdForThisLien = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Adjustment" /> class.
        /// </summary>
        /// <param name="other">The adjustment to copy.</param>
        /// <param name="idAssignment">The method of assigning the id.</param>
        public Adjustment(Adjustment other, IdAssignment idAssignment)
            : this()
        {
            this.OverrideFrom(other, idAssignment);
        }

        /// <summary>
        /// Gets or sets the ID of the servicing payment linked to this adjustment.
        /// </summary>
        /// <value>The ID of the linked servicing payment or null if no payment exists.</value>
        [JsonProperty]
        public Guid? LinkedServicingPaymentId { get; set; }

        /// <summary>
        /// Gets or sets the type of the adjustment.  Calculated from the description if loan version is below 24.
        /// </summary>
        /// <value>The type of the adjustment.</value>
        [JsonProperty]
        public new E_AdjustmentT AdjustmentType
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.LoanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments))
                {
                    return this.ProtectedAdjustmentType;
                }
                else
                {
                    if (string.IsNullOrEmpty(this.Description))
                    {
                        return E_AdjustmentT.Other;
                    }

                    var lowerDesc = this.Description.ToLower();

                    if (lowerDesc == EmployerAssistedHousingDesc)
                    {
                        return E_AdjustmentT.EmployerAssistedHousing;
                    }
                    else if (lowerDesc == LeasePurchaseFundDesc)
                    {
                        return E_AdjustmentT.LeasePurchaseFund;
                    }
                    else if (lowerDesc == ReloactionFundsDesc)
                    {
                        return E_AdjustmentT.RelocationFunds;
                    }
                    else if (lowerDesc == SellerCreditDesc)
                    {
                        return E_AdjustmentT.SellerCredit;
                    }
                    else if (lowerDesc == PrincipalReductionToReduceCashToBorrowerDesc)
                    {
                        return E_AdjustmentT.PrincipalReductionToReduceCashToBorrower;
                    }
                    else if (lowerDesc == PrincipalReductionToCureToleranceViolationDesc)
                    {
                        return E_AdjustmentT.PrincipalReductionToCureToleranceViolation;
                    }
                    else
                    {
                        return E_AdjustmentT.Other;
                    }
                }                    
            }

            set
            {          
                this.ProtectedAdjustmentType = value;
            }
        }

        /// <summary>
        /// Gets or sets the id of the adjustment.
        /// </summary>
        /// <value>The id of the adjustment.</value>
        [JsonProperty]
        public Guid? Id
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we should generate ids on the next serialization. <para/>
        /// Set this every time just prior to Save so we can set the id if it's empty. <para/>
        /// Alternatively we could just have a save method that also bumps the loan version and sets the id if it's not there.
        /// </summary>
        /// <value>If true we will set ids on the next serialization and then reset it to false.</value>
        public bool GenerateIdOnSerialization { get; set; }

        /// <summary>
        /// Gets the string representation of the Paid From Party property.
        /// </summary>
        /// <value>The string representation of the Paid from Party property.</value>
        [JsonProperty]
        public string PaidFromParty_rep
        {
            get
            {
                return Tools.GetPartyDescription(this.PaidFromParty);
            }

            private set
            { 
            }
        }

        /// <summary>
        /// Gets the string representation of the Paid To Party property.
        /// </summary>
        [JsonProperty]
        public string PaidToParty_rep
        {
            get
            {
                return Tools.GetPartyDescription(this.PaidToParty);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether the borrower is responsible for 
        /// making the payment.
        /// </summary>
        /// <value>
        /// A value indicating whether the borrower is responsible for making
        /// the payment.
        /// </value>
        [JsonProperty]
        public bool IsPaidFromBorrower
        {
            get
            {
                return this.PaidFromParty == E_PartyT.Borrower;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the seller is responsible for making
        /// the payment.
        /// </summary>
        /// <value>
        /// A value indicating whether the seller is responsible for making the
        /// payment.
        /// </value>
        [JsonProperty]
        public bool IsPaidFromSeller
        {
            get
            {
                return this.PaidFromParty == E_PartyT.Seller;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the payment is made to the borrower.
        /// </summary>
        /// <value>
        /// A value indicating whether the payment is made to the borrower.
        /// </value>
        [JsonProperty]
        public bool IsPaidToBorrower
        {
            get
            {
                return this.PaidToParty == E_PartyT.Borrower;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the payment is made to the seller.
        /// </summary>
        /// <value>
        /// A value indicating whether the payment is made to the seller.
        /// </value>
        [JsonProperty]
        public bool IsPaidToSeller
        {
            get
            {
                return this.PaidToParty == E_PartyT.Seller;
            }
        }

        /// <summary>
        /// Gets or sets the amount of the adjustment.
        /// </summary>
        /// <value>The amount of the adjustment.</value>
        public decimal Amount
        {
            get
            {
                return Math.Max(0, this.amount);
            }

            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the adjustment amount.
        /// </summary>
        /// <value>The string representation of the adjustment amount.</value>
        [JsonProperty("Amount")]
        public string Amount_rep
        {
            get 
            { 
                return this.losConvert.ToMoneyString(
                    this.Amount,
                    FormatDirection.ToRep); 
            }

            set 
            { 
                this.Amount = this.losConvert.ToMoney(value); 
            }
        }

        /// <summary>
        /// Gets the amount the borrower will either be paying or receiving.
        /// </summary>
        /// <value>
        /// The amount the borrower will either be paying or receiving. Will be
        /// the same as Amount when the borrower is the payment source or 
        /// destination.
        /// </value>
        public decimal BorrowerAmount
        {
            get
            {
                if (this.PaidFromParty == E_PartyT.Borrower ||
                    this.PaidToParty == E_PartyT.Borrower)
                {
                    return this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets how much is paid to the borrower.  <para></para>
        /// Nothing if the amount is both from and to the borrower. <para></para>
        /// Can be negative if it is marked as from the borrower.
        /// </summary>
        /// <value>The amount paid to the borrower.</value>
        public decimal AmountToBorrower
        {
            get
            {
                if (this.PaidToParty == E_PartyT.Borrower && 
                    this.PaidFromParty == E_PartyT.Borrower)
                {
                    return 0;
                }
                else if (this.PaidToParty == E_PartyT.Borrower)
                {
                    return this.Amount;
                }
                else if (this.PaidFromParty == E_PartyT.Borrower)
                {
                    return -this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string representation of the amount paid to borrower.
        /// </summary>
        /// <value>The string representation of the amount paid to borrower.</value>
        public string AmountToBorrower_rep
        {
            get
            {
                return this.losConvert.ToMoneyString(this.AmountToBorrower, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the string representation of the borrower amount.
        /// </summary>
        /// <value>The string representation of the borrower amount.</value>
        [JsonProperty("BorrowerAmount")]
        public string BorrowerAmount_rep
        {
            get 
            {
                return this.losConvert.ToMoneyString(
                    this.BorrowerAmount,
                    FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the amount the seller will either be paying or receiving.
        /// </summary>
        /// <value>
        /// The amount the seller will either be paying or receiving. Will be
        /// the same as Amount when the seller is the payment source or 
        /// destination.
        /// </value>
        public decimal SellerAmount
        {
            get
            {
                if (this.PaidFromParty == E_PartyT.Seller ||
                    this.PaidToParty == E_PartyT.Seller)
                {
                    return this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string representation of the seller amount.
        /// </summary>
        /// <value>The string representation of the seller amount.</value>
        [JsonProperty("SellerAmount")]
        public string SellerAmount_rep
        {
            get
            {
                return this.losConvert.ToMoneyString(
                    this.SellerAmount,
                    FormatDirection.ToRep);
            }
        }    
        
        /// <summary>
        /// Gets or sets a value indicating whether this adjustment should be included in the LE and CD for this lien.
        /// </summary>
        /// <value>The boolean representation of whether to include current adjustment in LE and CD for this lien.</value>
        [JsonProperty]
        public bool IncludeAdjustmentInLeCdForThisLien { get; set; }

        /// <summary>
        /// Default paid from party type for a particular type of adjustment.  Can be null if no default.
        /// </summary>
        /// <param name="adjustmentT">The adjustment type under consideration.</param>
        /// <returns>The default from party type for that adjustment type, or null if there is no default.</returns>
        public static E_PartyT? DefaultPaidFromPartyTForAdjustmentT(E_AdjustmentT adjustmentT)
        {
            switch (adjustmentT)
            {
                case E_AdjustmentT.SellerCredit:
                case E_AdjustmentT.LeasePurchaseFund:
                case E_AdjustmentT.SellersEscrowAssumption:
                case E_AdjustmentT.SellersMortgageInsuranceAssumption:
                case E_AdjustmentT.SellersReserveAccountAssumption:
                    return E_PartyT.Seller;
                case E_AdjustmentT.FuelCosts:
                case E_AdjustmentT.PrincipalReductionToReduceCashToBorrower:
                case E_AdjustmentT.PrincipalReductionToCureToleranceViolation:
                    return E_PartyT.Borrower;
                case E_AdjustmentT.RelocationFunds:
                case E_AdjustmentT.EmployerAssistedHousing:
                    return E_PartyT.BorrowersEmployer;
                case E_AdjustmentT.Other:
                case E_AdjustmentT.Gift:
                case E_AdjustmentT.Grant:
                case E_AdjustmentT.ProceedsOfSubordinateLiens:
                case E_AdjustmentT.RebateCredit:
                case E_AdjustmentT.RentFromSubjectProperty:
                case E_AdjustmentT.RepairCompletionEscrowHoldback:
                case E_AdjustmentT.Repairs:
                case E_AdjustmentT.SatisfactionOfSubordinateLien:
                case E_AdjustmentT.Services:
                case E_AdjustmentT.SubordinateFinancingProceeds:
                case E_AdjustmentT.TenantSecurityDeposit:
                case E_AdjustmentT.TitlePremiumAdjustment:
                case E_AdjustmentT.TradeEquity:
                case E_AdjustmentT.UnpaidUtilityEscrowHoldback:
                case E_AdjustmentT.BuydownFund:
                case E_AdjustmentT.CommitmentOriginationFee:
                case E_AdjustmentT.GiftOfEquity:
                case E_AdjustmentT.MIPremiumRefund:
                case E_AdjustmentT.SweatEquity:
                case E_AdjustmentT.TradeEquityCredit:
                case E_AdjustmentT.TradeEquityFromPropertySwap:
                case E_AdjustmentT.BorrowerPaidFees:
                    return null;

                default:
                    throw new UnhandledEnumException(adjustmentT);
            }
        }

        /// <summary>
        /// Default paid to party type for a particular type of adjustment.  Can be null if no default.
        /// </summary>
        /// <param name="adjustmentT">The adjustment type under consideration.</param>
        /// <returns>The default to party type for that adjustment type, or null if there is no default.</returns>/
        public static E_PartyT? DefaultPaidToPartyTForAdjustmentT(E_AdjustmentT adjustmentT)
        {
            switch (adjustmentT)
            {
                case E_AdjustmentT.SellerCredit:
                case E_AdjustmentT.LeasePurchaseFund:
                case E_AdjustmentT.RelocationFunds:
                case E_AdjustmentT.EmployerAssistedHousing:
                case E_AdjustmentT.Gift:
                case E_AdjustmentT.Grant:
                case E_AdjustmentT.SellersEscrowAssumption:
                case E_AdjustmentT.SellersMortgageInsuranceAssumption:
                case E_AdjustmentT.SellersReserveAccountAssumption:
                case E_AdjustmentT.FederalAgencyFundingFeeRefund:
                case E_AdjustmentT.GiftOfEquity:
                case E_AdjustmentT.MIPremiumRefund:
                case E_AdjustmentT.Other:
                case E_AdjustmentT.SweatEquity:
                case E_AdjustmentT.TradeEquityCredit:
                case E_AdjustmentT.TradeEquityFromPropertySwap:
                case E_AdjustmentT.BorrowerPaidFees:
                    return E_PartyT.Borrower;
                case E_AdjustmentT.FuelCosts:
                    return E_PartyT.Seller;
                case E_AdjustmentT.PrincipalReductionToReduceCashToBorrower:
                case E_AdjustmentT.PrincipalReductionToCureToleranceViolation:
                    return E_PartyT.Lender;
                case E_AdjustmentT.ProceedsOfSubordinateLiens:
                case E_AdjustmentT.RebateCredit:
                case E_AdjustmentT.RentFromSubjectProperty:
                case E_AdjustmentT.RepairCompletionEscrowHoldback:
                case E_AdjustmentT.Repairs:
                case E_AdjustmentT.SatisfactionOfSubordinateLien:
                case E_AdjustmentT.Services:
                case E_AdjustmentT.SubordinateFinancingProceeds:
                case E_AdjustmentT.TenantSecurityDeposit:
                case E_AdjustmentT.TitlePremiumAdjustment:
                case E_AdjustmentT.TradeEquity:
                case E_AdjustmentT.UnpaidUtilityEscrowHoldback:
                case E_AdjustmentT.BuydownFund:
                case E_AdjustmentT.CommitmentOriginationFee:
                    return null;
                default:
                    throw new UnhandledEnumException(adjustmentT);
            }
        }                    

        /// <summary>
        /// Determines whether the specified Adjustment is equal to the current
        /// Adjustment.
        /// </summary>
        /// <param name="other">The other Adjustment.</param>
        /// <returns>
        /// True if the specified Adjustment is equal to the current Adjustment.
        /// Otherwise, false.
        /// </returns>
        public bool Equals(Adjustment other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Description == other.Description &&
                this.PaidFromParty == other.PaidFromParty &&
                this.PaidToParty == other.PaidToParty &&
                this.Amount == other.Amount &&
                this.PopulateTo == other.PopulateTo;
        }

        /// <summary>
        /// Overrides from other adjustment.
        /// </summary>
        /// <param name="other">The other adjustment to copy info from.</param>
        /// <param name="idAssignment">The method of assigning the id.</param>
        public void OverrideFrom(Adjustment other, IdAssignment idAssignment)
        {
            this.Description = other.Description;
            this.Amount = other.Amount;
            this.PaidFromParty = other.PaidFromParty;
            this.PaidToParty = other.PaidToParty;
            this.PopulateTo = other.PopulateTo;
            this.POC = other.POC;
            this.DFLP = other.DFLP;
            this.PopulateTo = other.IsPopulateToLineLOn1003 ? E_PopulateToT.LineLOn1003 : E_PopulateToT.DoNotPopulate;
            switch (idAssignment)
            {
                case IdAssignment.GenerateNew:
                    this.Id = Guid.NewGuid();
                    break;
                case IdAssignment.Copy:
                    this.Id = other.Id;
                    break;
                case IdAssignment.LeaveIdUnchanged:
                    break;
                default:
                    throw new UnhandledEnumException(idAssignment);
            }

            this.IncludeAdjustmentInLeCdForThisLien = other.IncludeAdjustmentInLeCdForThisLien;
        }

        /// <summary>
        /// Gets a value that represents a hash of this instance.
        /// </summary>
        /// <returns>Returns 1 because this is a mutable object.</returns>
        public override int GetHashCode()
        {
            return 1;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current
        /// Adjustment.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>
        /// True if the specified object is equal to the current Adjustment.
        /// Otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Adjustment);
        }

        /// <summary>
        /// Sets the los convert object used to generate _rep fields.
        /// </summary>
        /// <param name="convert">The converter to use.</param>
        public void SetLosConvert(LosConvert convert)
        {
            this.losConvert = convert;
        }

        /// <summary>
        /// Validate the adjustment to see if they exist in the predefined adjustments list.
        /// </summary>
        /// <param name="allowCustomDescriptions">Employee permission that allows custom descriptions.</param>
        /// <param name="isRestrictAdjustmentsAndOtherCreditsDescriptions">The bool that restricts adjustment description changes.</param>
        /// <param name="allowedAdjustments">List of allowed predefined adjustments.</param>
        public void ValidateDescriptions(bool allowCustomDescriptions, bool isRestrictAdjustmentsAndOtherCreditsDescriptions, List<PredefinedAdjustment> allowedAdjustments)
        {
            if (!allowCustomDescriptions && isRestrictAdjustmentsAndOtherCreditsDescriptions)
            {
                foreach (PredefinedAdjustment allowedAdj in allowedAdjustments)
                {
                    if (this.AdjustmentType == allowedAdj.AdjustmentType && this.Description == allowedAdj.Description)
                    {
                        return;
                    }

                    throw new CBaseException("You do not have permission to modify adjustment descriptions", "You do not have permission to modify adjustment descriptions");
                }
            }            
        }

        /// <summary>
        /// Resets the los convert of this object back to the default before
        /// serializing.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void PrepareForSerialization(StreamingContext context)
        {
            this.Validate();
            this.ResetLosConvert();
            if (this.GenerateIdOnSerialization && !this.Id.HasValue)
            {
                this.Id = Guid.NewGuid();
            }
        }

        /// <summary>
        /// After being serialized.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        private void OnSerialized(StreamingContext context)
        {
            this.GenerateIdOnSerialization = false;
        }

        /// <summary>
        /// Verifies the object is in a valid state. Throws if not.
        /// </summary>
        private void Validate()
        {
            if (this.POC && this.DFLP)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Should never be in a state where both DFLP and POC are true.");
            }

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.LoanVersionT, LoanVersionT.V13_LimitAdjustmentLength) &&
                this.Description.Length > 60)
            {
                throw new CBaseException("Descriptions of adjustments on the Adjustments and Other Credits page cannot exceed 60 characters.", this.Description + " is greater than 60 characters.");
            }
        }

        /// <summary>
        /// Restores the LosConvert to its default value.
        /// </summary>
        private void ResetLosConvert()
        {
            this.losConvert = new LosConvert();
        }
    }
}
