﻿// <copyright file="E_PartyT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   5/1/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// The parties that can be involved in adjustments and proration items.
    /// </summary>
    /// <remarks>
    /// Only Blank, Borrower, and Seller are valid for proration items.
    /// When adding another option to this list, make sure to check for places in the UI that uses this.
    /// </remarks>
    public enum E_PartyT
    {
        /// <summary>
        /// A blank entry.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// The borrower.
        /// </summary>
        Borrower = 1,

        /// <summary>
        /// The seller.
        /// </summary>
        Seller = 2,

        /// <summary>
        /// The lender.
        /// </summary>
        Lender = 3,

        /// <summary>
        /// The borrower's employer.
        /// </summary>
        BorrowersEmployer = 4,

        /// <summary>
        /// The borrower's parent.
        /// </summary>
        BorrowersParent = 5,

        /// <summary>
        /// The borrower's relative.
        /// </summary>
        BorrowersRelative = 6,

        /// <summary>
        /// A federal agency.
        /// </summary>
        FederalAgency = 7,

        /// <summary>
        /// A builder or developer.
        /// </summary>
        BuilderOrDeveloper = 8,

        /// <summary>
        /// A real estate agent.
        /// </summary>
        RealEstateAgent = 9,

        /// <summary>
        /// Some other option.
        /// </summary>
        Other = 10,

        /// <summary>
        /// The borrower's friend.
        /// </summary>
        BorrowersFriend = 11
    }
}