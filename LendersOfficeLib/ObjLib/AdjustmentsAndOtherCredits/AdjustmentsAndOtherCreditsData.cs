﻿namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup;

    /// <summary>
    /// Class that holds data for adjustments and other credits.
    /// </summary>
    public class AdjustmentsAndOtherCreditsData
    {
        /// <summary>
        /// A dictionary that maps adjustment type into predefined adjustment default values.
        /// </summary>
        private static Dictionary<E_AdjustmentT, PredefinedAdjustment> predefinedAdjustmentTypeMap = new Dictionary<E_AdjustmentT, PredefinedAdjustment>()
        {
            { E_AdjustmentT.Blank, new PredefinedAdjustment(-1, -1, string.Empty, 0, 0, false, 0, false) },
            { E_AdjustmentT.EmployerAssistedHousing, new PredefinedAdjustment(-1, 0, "Employer Assisted Housing", 4, 1, false, 1, false) },
            { E_AdjustmentT.LeasePurchaseFund, new PredefinedAdjustment(-1, 1, "Lease Purchase Fund", 2, 1, false, 1, false) },
            { E_AdjustmentT.RelocationFunds, new PredefinedAdjustment(-1, 2, "Relocation Funds", 4, 1, false, 1, false) },
            { E_AdjustmentT.SellerCredit, new PredefinedAdjustment(-1, 3, "Seller Credit", 4, 0, false, 0, false) },
            { E_AdjustmentT.Other, new PredefinedAdjustment(-1, 4, "Other", 0, 0, false, 0, false) },
            { E_AdjustmentT.FuelCosts, new PredefinedAdjustment(-1, 5, "Fuel Costs", 1, 2, false, 0, false) },
            { E_AdjustmentT.Grant, new PredefinedAdjustment(-1, 6, "Grant", 0, 1, false, 0, false) },
            { E_AdjustmentT.ProceedsOfSubordinateLiens, new PredefinedAdjustment(-1, 7, "Proceeds Of Subordinate Liens", 0, 0, false, 0, false) },
            { E_AdjustmentT.RebateCredit, new PredefinedAdjustment(-1, 8, "Rebate Credit", 0, 0, false, 0, false) },
            { E_AdjustmentT.RentFromSubjectProperty, new PredefinedAdjustment(-1, 9, "Rent From Subject Property", 0, 0, false, 0, false) },
            { E_AdjustmentT.RepairCompletionEscrowHoldback, new PredefinedAdjustment(-1, 10, "Repair Completion Escrow Holdback", 0, 0, false, 0, false) },
            { E_AdjustmentT.Repairs, new PredefinedAdjustment(-1, 11, "Repairs", 0, 0, false, 0, false) },
            { E_AdjustmentT.SatisfactionOfSubordinateLien, new PredefinedAdjustment(-1, 12, "Satisfaction Of Subordinate Liens", 0, 0, false, 0, false) },
            { E_AdjustmentT.SellersEscrowAssumption, new PredefinedAdjustment(-1, 13, "Sellers Escrow Assumption", 2, 1, false, 0, false) },
            { E_AdjustmentT.SellersMortgageInsuranceAssumption, new PredefinedAdjustment(-1, 14, "Sellers Mortgage Insurance Assumption", 2, 1, false, 0, false) },
            { E_AdjustmentT.SellersReserveAccountAssumption, new PredefinedAdjustment(-1, 15, "Sellers Reserve Account Assumption", 2, 1, false, 0, false) },
            { E_AdjustmentT.Services, new PredefinedAdjustment(-1, 16, "Services", 0, 0, false, 0, false) },
            { E_AdjustmentT.SubordinateFinancingProceeds, new PredefinedAdjustment(-1, 17, "Subordinate Financing Proceeds", 0, 0, false, 0, false) },
            { E_AdjustmentT.TenantSecurityDeposit, new PredefinedAdjustment(-1, 18, "Tenant Security Deposit", 0, 0, false, 0, false) },
            { E_AdjustmentT.TitlePremiumAdjustment, new PredefinedAdjustment(-1, 19, "Title Premium Adjustment", 0, 0, false, 0, false) },
            { E_AdjustmentT.TradeEquity, new PredefinedAdjustment(-1, 20, "Trade Equity", 0, 0, false, 0, false) },
            { E_AdjustmentT.UnpaidUtilityEscrowHoldback, new PredefinedAdjustment(-1, 21, "Unpaid Utility Escrow Holdback", 0, 0, false, 0, false) },
            { E_AdjustmentT.BuydownFund, new PredefinedAdjustment(-1, 22, "Buydown Fund", 0, 1, true, 0, false) },
            { E_AdjustmentT.CommitmentOriginationFee, new PredefinedAdjustment(-1, 23, "Commitment Origination Fee", 0, 1, false, 1, false) },
            { E_AdjustmentT.FederalAgencyFundingFeeRefund, new PredefinedAdjustment(-1, 24, "Federal Agency Funding Fee Refund", 4, 1, false, 1, false) },
            { E_AdjustmentT.GiftOfEquity, new PredefinedAdjustment(-1, 25, "Gift Of Equity", 0, 1, false, 1, false) },
            { E_AdjustmentT.MIPremiumRefund, new PredefinedAdjustment(-1, 26, "MI Premium Refund", 0, 1, false, 1, false) },
            { E_AdjustmentT.SweatEquity, new PredefinedAdjustment(-1, 27, "Sweat Equity", 0, 1, false, 1, false) },
            { E_AdjustmentT.TradeEquityFromPropertySwap, new PredefinedAdjustment(-1, 28, "Trade Equity From Property Swap", 0, 1, false, 1, false) },
            { E_AdjustmentT.BorrowerPaidFees, new PredefinedAdjustment(-1, 29, "Borrower Paid Fees", 0, 1, false, 0, false) },
            { E_AdjustmentT.TradeEquityCredit, new PredefinedAdjustment(-1, 30, "Purchase Trade Equity (Credit)", 0, 1, false, 1, false) },
            { E_AdjustmentT.Gift, new PredefinedAdjustment(-1, 31, "Gift", 0, 1, false, 0, false) },
            { E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, new PredefinedAdjustment(-1, 32, "Principal Reduction to Reduce Cash to Borrower", 1, 3, false, 0, true) },
            { E_AdjustmentT.PrincipalReductionToCureToleranceViolation, new PredefinedAdjustment(-1, 33, "Principal Reduction to Cure Tolerance Violation", 1, 3, false, 0, true) },
        };

        /// <summary>
        /// A dictionary that maps a string into adjustment type.
        /// </summary>
        private static Dictionary<string, Tuple<E_AdjustmentT, string>> predefinedAdjustmentsStringMap = new Dictionary<string, Tuple<E_AdjustmentT, string>>(StringComparer.CurrentCultureIgnoreCase)
        {
            { string.Empty, new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Other, "blank") },
            { "borrower paid fees", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.BorrowerPaidFees, "Borrower Paid Fees") },
            { "borrowerpaidfees", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.BorrowerPaidFees, "Borrower Paid Fees") },
            { "buydownfund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.BuydownFund, "Buydown Fund") },
            { "buydown fund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.BuydownFund, "Buydown Fund") },
            { "commitment origination fee", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.CommitmentOriginationFee, "Commitment Origination Fee") },
            { "commitmentoriginationfee", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.CommitmentOriginationFee, "Commitment Origination Fee") },
            { "employer assisted housing", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.EmployerAssistedHousing, "Employer Assisted Housing") },
            { "employerassistedhousing", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.EmployerAssistedHousing, "Employer Assisted Housing") },
            { "federal agency funding fee refund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.FederalAgencyFundingFeeRefund, "Federal Agency Funding Fee Refund") },
            { "federalagencyfundingfeerefund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.FederalAgencyFundingFeeRefund, "Federal Agency Funding Fee Refund") },
            { "fuelcosts", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.FuelCosts, "Fuel Costs") },
            { "fuel costs", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.FuelCosts, "Fuel Costs") },
            { "gift", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Gift, "Gift") },
            { "gift of equity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.GiftOfEquity, "Gift Of Equity") },
            { "giftofequity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.GiftOfEquity, "Gift Of Equity") },
            { "grant", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Grant, "Grant") },
            { "lease purchase fund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.LeasePurchaseFund, "Lease Purchase Fund") },
            { "leasepurchasefund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.LeasePurchaseFund, "Lease Purchase Fund") },
            { "mi premium refund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.MIPremiumRefund, "MI Premium Refund") },
            { "mipremiumrefund", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.MIPremiumRefund, "MI Premium Refund") },
            { "other", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Other, "Other") },
            { "proceeds of subordinate liens", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.ProceedsOfSubordinateLiens, "Proceeds Of Subordinate Liens") },
            { "proceedsofsubordinateliens", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.ProceedsOfSubordinateLiens, "Proceeds Of Subordinate Liens") },
            { "rebate credit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RebateCredit, "Rebate Credit") },
            { "rebatecredit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RebateCredit, "Rebate Credit") },
            { "relocation funds", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RelocationFunds, "Relocation Funds") },
            { "relocationfunds", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RelocationFunds, "Relocation Funds") },
            { "rent from subject property", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RentFromSubjectProperty, "Rent From Subject Property") },
            { "rentfromsubjectproperty", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RentFromSubjectProperty, "Rent From Subject Property") },
            { "repair completion escrow holdback", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RepairCompletionEscrowHoldback, "Repair Completion Escrow Holdback") },
            { "repaircompletionescrowholdback", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.RepairCompletionEscrowHoldback, "Repair Completion Escrow Holdback") },
            { "repairs", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Repairs, "Repairs") },
            { "satisfaction of subordinate lien", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SatisfactionOfSubordinateLien, "Satisfaction Of Subordinate Lien") },
            { "satisfactionofsubordinatelien", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SatisfactionOfSubordinateLien, "Satisfaction Of Subordinate Lien") },
            { "sellers escrow assumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersEscrowAssumption, "Sellers Escrow Assumption") },
            { "sellersescrowassumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersEscrowAssumption, "Sellers Escrow Assumption") },
            { "sellers mortgage insurance assumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersMortgageInsuranceAssumption, "Sellers Mortgage Insurance Assumption") },
            { "sellersmortgageinsuranceassumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersMortgageInsuranceAssumption, "Sellers Mortgage Insurance Assumption") },
            { "sellers reserve account assumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersReserveAccountAssumption, "Sellers Reserve Account Assumption") },
            { "sellersreserveaccountassumption", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SellersReserveAccountAssumption, "Sellers Reserve Account Assumption") },
            { "services", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.Services, "Services") },
            { "subordinate financing proceeds", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SubordinateFinancingProceeds, "Subordinate Financing Proceeds") },
            { "subordinatefinancingproceeds", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SubordinateFinancingProceeds, "Subordinate Financing Proceeds") },
            { "sweat equity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SweatEquity, "Sweat Equity") },
            { "sweatequity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.SweatEquity, "Sweat Equity") },
            { "tenant security deposit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TenantSecurityDeposit, "Tenant Security Deposit") },
            { "tenantsecuritydeposit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TenantSecurityDeposit, "Tenant Security Deposit") },
            { "title premium adjustment", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TitlePremiumAdjustment, "Title Premium Adjustment") },
            { "titlepremiumadjustment", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TitlePremiumAdjustment, "Title Premium Adjustment") },
            { "trade equity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquity, "Trade Equity") },
            { "tradeequity", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquity, "Trade Equity") },
            { "trade equity credit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquityCredit, "Trade Equity Credit") },
            { "tradeequitycredit", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquityCredit, "Trade Equity Credit") },
            { "trade equity from property swap", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquityFromPropertySwap, "Trade Equity From Property Swap") },
            { "tradeequityfrompropertyswap", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.TradeEquityFromPropertySwap, "Trade Equity From Property Swap") },
            { "unpaid utility escrow holdback", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.UnpaidUtilityEscrowHoldback, "Unpaid Utility Escrow Holdback") },
            { "unpaidutilityescrowholdback", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.UnpaidUtilityEscrowHoldback, "Unpaid Utility Escrow Holdback") },
            { "principal reduction to reduce cash to borrower", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, "Principal Reduction to Reduce Cash to Borrower") },
            { "principalreductiontoreducecashtoborrower", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, "Principal Reduction to Reduce Cash to Borrower") },
            { "principal reduction to cure tolerance violation", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, "Principal Reduction to Cure Tolerance Violation") },
            { "principalreductiontocuretoleranceviolation", new Tuple<E_AdjustmentT, string>(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, "Principal Reduction to Cure Tolerance Violation") }
        };

        /// <summary>
        /// A dictionary that maps adjustment type into their respective int values.
        /// </summary>
        private static Dictionary<E_AdjustmentT, int> adjustmentTypeEnums = new Dictionary<E_AdjustmentT, int>()
        {
            { E_AdjustmentT.Blank, (int)E_AdjustmentT.Blank },
            { E_AdjustmentT.EmployerAssistedHousing, (int)E_AdjustmentT.EmployerAssistedHousing },
            { E_AdjustmentT.LeasePurchaseFund, (int)E_AdjustmentT.LeasePurchaseFund },
            { E_AdjustmentT.RelocationFunds, (int)E_AdjustmentT.RelocationFunds },
            { E_AdjustmentT.SellerCredit, (int)E_AdjustmentT.SellerCredit },
            { E_AdjustmentT.Other, (int)E_AdjustmentT.Other },
            { E_AdjustmentT.FuelCosts, (int)E_AdjustmentT.FuelCosts },
            { E_AdjustmentT.Grant, (int)E_AdjustmentT.Grant },
            { E_AdjustmentT.ProceedsOfSubordinateLiens, (int)E_AdjustmentT.ProceedsOfSubordinateLiens },
            { E_AdjustmentT.RebateCredit, (int)E_AdjustmentT.RebateCredit },
            { E_AdjustmentT.RentFromSubjectProperty, (int)E_AdjustmentT.RentFromSubjectProperty },
            { E_AdjustmentT.RepairCompletionEscrowHoldback, (int)E_AdjustmentT.RepairCompletionEscrowHoldback },
            { E_AdjustmentT.Repairs, (int)E_AdjustmentT.Repairs },
            { E_AdjustmentT.SatisfactionOfSubordinateLien, (int)E_AdjustmentT.SatisfactionOfSubordinateLien },
            { E_AdjustmentT.SellersEscrowAssumption, (int)E_AdjustmentT.SellersEscrowAssumption },
            { E_AdjustmentT.SellersMortgageInsuranceAssumption, (int)E_AdjustmentT.SellersMortgageInsuranceAssumption },
            { E_AdjustmentT.SellersReserveAccountAssumption, (int)E_AdjustmentT.SellersReserveAccountAssumption },
            { E_AdjustmentT.Services, (int)E_AdjustmentT.Services },
            { E_AdjustmentT.SubordinateFinancingProceeds, (int)E_AdjustmentT.SubordinateFinancingProceeds },
            { E_AdjustmentT.TenantSecurityDeposit, (int)E_AdjustmentT.TenantSecurityDeposit },
            { E_AdjustmentT.TitlePremiumAdjustment, (int)E_AdjustmentT.TitlePremiumAdjustment },
            { E_AdjustmentT.TradeEquity, (int)E_AdjustmentT.TradeEquity },
            { E_AdjustmentT.UnpaidUtilityEscrowHoldback, (int)E_AdjustmentT.UnpaidUtilityEscrowHoldback },
            { E_AdjustmentT.BuydownFund, (int)E_AdjustmentT.BuydownFund },
            { E_AdjustmentT.CommitmentOriginationFee, (int)E_AdjustmentT.CommitmentOriginationFee },
            { E_AdjustmentT.FederalAgencyFundingFeeRefund, (int)E_AdjustmentT.FederalAgencyFundingFeeRefund },
            { E_AdjustmentT.GiftOfEquity, (int)E_AdjustmentT.GiftOfEquity },
            { E_AdjustmentT.MIPremiumRefund, (int)E_AdjustmentT.MIPremiumRefund },
            { E_AdjustmentT.SweatEquity, (int)E_AdjustmentT.SweatEquity },
            { E_AdjustmentT.TradeEquityFromPropertySwap, (int)E_AdjustmentT.TradeEquityFromPropertySwap },
            { E_AdjustmentT.BorrowerPaidFees, (int)E_AdjustmentT.BorrowerPaidFees },
            { E_AdjustmentT.TradeEquityCredit, (int)E_AdjustmentT.TradeEquityCredit },
            { E_AdjustmentT.Gift, (int)E_AdjustmentT.Gift },
            { E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, (int)E_AdjustmentT.PrincipalReductionToReduceCashToBorrower },
            { E_AdjustmentT.PrincipalReductionToCureToleranceViolation, (int)E_AdjustmentT.PrincipalReductionToCureToleranceViolation }
        };

        /// <summary>
        /// Provides the set of adjustment types that have linked servicing payments.
        /// </summary>
        private static Dictionary<E_AdjustmentT, Tuple<string, string>> adjustmentTypesWithLinkedServicingPayments = new Dictionary<E_AdjustmentT, Tuple<string, string>>
        {
            [E_AdjustmentT.PrincipalReductionToReduceCashToBorrower] = Tuple.Create("Principal Reduction", "Principal Reduction for Reducing Cash to Borrower"),
            [E_AdjustmentT.PrincipalReductionToCureToleranceViolation] = Tuple.Create("Principal Reduction", "Principal Reduction to Cure Tolerance Violation")
        };

        /// <summary>
        /// Gets the predefined adjustments that matches input string to adjustment type and description.
        /// </summary>
        /// <value>The dictionary that maps string to adjustment type and its description.</value>
        public static ReadOnlyDictionary<string, Tuple<E_AdjustmentT, string>> PredefinedAdjustmentsStringMap
        {
            get
            {
                return new ReadOnlyDictionary<string, Tuple<E_AdjustmentT, string>>(predefinedAdjustmentsStringMap);
            }
        }

        /// <summary>
        /// Gets the predefined adjustments type map.
        /// </summary>
        /// <value>The dictionary that maps adjustment type into predefined adjustments.</value>
        public static ReadOnlyDictionary<E_AdjustmentT, PredefinedAdjustment> PredefinedAdjustmentTypeMap
        {
            get
            {
                return new ReadOnlyDictionary<E_AdjustmentT, PredefinedAdjustment>(predefinedAdjustmentTypeMap);
            }
        }

        /// <summary>
        /// Gets the adjustment to enum dictionary.
        /// </summary>
        /// <value>The dictionary that maps E_AdjustmentT enum to its respective int value.</value>
        public static ReadOnlyDictionary<E_AdjustmentT, int> AdjustmentTypeEnums
        {
            get
            {
                return new ReadOnlyDictionary<E_AdjustmentT, int>(adjustmentTypeEnums);
            }
        }

        /// <summary>
        /// Gets the list of adjustment types that have linked servicing transaction payments
        /// with associated servicing type and notes.
        /// </summary>
        public static ReadOnlyDictionary<E_AdjustmentT, Tuple<string, string>> AdjustmentsTypesWithLinkedServicingPayments
        {
            get
            {
                return new ReadOnlyDictionary<E_AdjustmentT, Tuple<string, string>>(adjustmentTypesWithLinkedServicingPayments);
            }
        }

        /// <summary>
        /// Creates a list of string tuples that is used for the dropdownlist in the adjustments and other credits setup page.
        /// </summary>
        /// <returns>List of string tuples.</returns>
        public static List<Tuple<string, string>> CreateAdjustmentSetupDrowndownList()
        {
            List<Tuple<string, string>> selectableAdjustments = new List<Tuple<string, string>>();

            List<E_AdjustmentT> adjustmentTypeList = Enum.GetValues(typeof(E_AdjustmentT)).Cast<E_AdjustmentT>().ToList();
            adjustmentTypeList.Remove(E_AdjustmentT.SellerCredit);

            foreach (E_AdjustmentT adjType in adjustmentTypeList)
            {
                selectableAdjustments.Add(new Tuple<string, string>(adjType.ToString(), predefinedAdjustmentTypeMap[adjType].Description));
            }

            selectableAdjustments.Sort();

            return selectableAdjustments;
        }
    }
}
