﻿// <copyright file="AdjustmentList.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/28/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using Migration.LoanDataMigrations;
    using Newtonsoft.Json;

    /// <summary>
    /// Represents a list of adjustments that will always have a seller credit.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class AdjustmentList : IEnumerable<Adjustment>, IEquatable<AdjustmentList>
    {
        /// <summary>
        /// The converter that should be used for adjustments added to this list.
        /// </summary>
        private LosConvert losConvert;

        /// <summary>
        /// The loan version.
        /// </summary>
        private LoanVersionT loanVersionT;

        /// <summary>
        /// We probably only want to set this when we are about to save.
        /// </summary>
        private bool generateIdsOnSerialization;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdjustmentList"/> class.
        /// </summary>
        public AdjustmentList()
        {
            this.CustomItems = new List<Adjustment>();
            E_AdjustmentT? adjustmentType = null;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.loanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments)) 
            {
                adjustmentType = E_AdjustmentT.SellerCredit;
            }

            this.SellerCredit = new Adjustment(
                "Seller Credit",
                E_PartyT.Seller,
                E_PartyT.Borrower,
                E_PopulateToT.DoNotPopulate,
                adjustmentType);
            this.losConvert = new LosConvert();
        }

        /// <summary>
        /// Gets the seller credit adjustment.
        /// </summary>
        /// <value>The seller credit adjustment.</value>
        [JsonProperty]
        public Adjustment SellerCredit { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we should generate ids on the next serialization. <para/>
        /// Set this every time just prior to Save so we can set the id if it's empty. <para/>
        /// Alternatively we could just have a save method that also bumps the loan version and sets the id if it's not there.
        /// </summary>
        /// <value>If true we will set ids on the next serialization and then reset it to false.</value>
        public bool GenerateIdsOnSerialization
        {
            get
            {
                return this.generateIdsOnSerialization;
            }

            set
            {
                this.generateIdsOnSerialization = value;
                if (this.SellerCredit != null)
                {
                    this.SellerCredit.GenerateIdOnSerialization = value;
                }

                if (this.CustomItems != null)
                {
                    foreach (var adjustment in this.CustomItems)
                    {
                        if (adjustment != null)
                        {
                            adjustment.GenerateIdOnSerialization = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the list of items created from assets.
        /// </summary>
        public List<Adjustment> AssetAdjustments { get; set; }

        /// <summary>
        /// Gets or sets the list of custom items, excluding the default seller
        /// credit.
        /// </summary>
        /// <value>
        /// The list of custom items, excluding the default seller credit.
        /// </value>
        [JsonProperty]
        private List<Adjustment> CustomItems { get; set; }

        /// <summary>
        /// Adds an Adjustment to the enumerable.
        /// </summary>
        /// <param name="a">The adjustment to add.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when a is null.
        /// </exception>
        public void Add(Adjustment a)
        {
            if (a == null)
            {
                throw new ArgumentNullException();
            }

            a.SetLosConvert(this.losConvert);

            this.CustomItems.Add(a);
        }

        /// <summary>
        /// Add an adjustment that has the same loan version as the adjustment list during creation.
        /// </summary>
        /// <param name="desc">Description for the adjustment.</param>
        /// <param name="amt">Amount for the adjustment.</param>
        public void AddWithLoanVersion(string desc, string amt)
        {
            Adjustment adj;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.loanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments))
            {
                Tuple<E_AdjustmentT, string> value;

                if (AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap.TryGetValue(desc, out value))
                {
                    adj = new Adjustment(value.Item2, amt, value.Item1, this.loanVersionT);
                }
                else
                {
                    adj = new Adjustment(desc, amt, E_AdjustmentT.Other, this.loanVersionT);
                }
            }
            else
            {
                adj = new Adjustment(desc, amt);
            }

            adj.PopulateTo = E_PopulateToT.LineLOn1003;
            adj.SetLosConvert(this.losConvert);
            this.CustomItems.Add(adj);
        }

        /// <summary>
        /// This method duplicates the current
        /// adjustment list but returns a copy. This is handy in situations such as
        /// lead-to-loan conversion where sAdjustmentList for a loan should be set
        /// so that field will be marked as dirty and saved.
        /// </summary>
        /// <param name="idAssignment">The method of assigning the id.</param>
        /// <returns>A copy of of the adjustment list this method is called on.</returns>
        public AdjustmentList GetUnlinkedCopy(IdAssignment idAssignment)
        {
            var adjList = new AdjustmentList();

            adjList.SellerCredit.OverrideFrom(this.SellerCredit, idAssignment);

            foreach (var adjustment in this.CustomItems)
            {
                adjList.Add(new Adjustment(adjustment, idAssignment));
            }

            adjList.SetLosConvert(this.losConvert);
            adjList.SetLoanVersionNumber(this.loanVersionT);

            return adjList;
        }

        /// <summary>
        /// Removes an Adjustment from the enumerable.
        /// </summary>
        /// <param name="a">The adjustment to remove.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when a is null.
        /// </exception>
        public void Remove(Adjustment a)
        {
            if (a == null)
            {
                throw new ArgumentNullException();
            }

            this.CustomItems.Remove(a);
        }

        /// <summary>
        /// Clears (removes) any adjustments that are marked as populating to the 1003. <para></para>
        /// If the Seller Credit is marked as populating to the 1003, it sets the value to 0.
        /// </summary>
        public void ClearAdjustmentsThatPopulateTo1003()
        {
            if (this.SellerCredit.IsPopulateToLineLOn1003)
            {
               this.SellerCredit.SetIsPopulateToLineLOn1003(false);
               this.SellerCredit.Amount = 0;
            }

            var adjustments = this.CustomItems.ToArray();
            foreach (var adjustment in adjustments)
            {
                if (adjustment.IsPopulateToLineLOn1003)
                {
                    this.Remove(adjustment);
                }
            }
        }

        /// <summary>
        /// Gets an enumerable without the seller credit.
        /// </summary>
        /// <returns>An enumerable without the seller credit.</returns>
        public IEnumerable<Adjustment> GetEnumeratorWithoutSellerCredit()
        {
            return this.CustomItems.Concat(this.AssetAdjustments ?? new List<Adjustment>());
        }

        /// <summary>
        /// Gets an enumerable without the asset adjustments.
        /// </summary>
        /// <returns>An enumerable without the asset adjustments.</returns>
        public IEnumerable<Adjustment> GetEnumeratorWithoutAssets()
        {
            return this.GetEnumerable(includeZeroAmountSellerCredit: true, includeAssetsAsAdjustments: false);
        }

        /// <summary>
        /// Gets an enumerable without the seller credit and asset adjustments.
        /// </summary>
        /// <returns>An enumerable without the seller credit and asset adjustments.</returns>
        public IEnumerable<Adjustment> GetEnumeratorWithoutSellerCreditAndAssets()
        {
            return this.CustomItems;
        }

        /// <summary>
        /// Get an enumerable of adjustments.
        /// </summary>
        /// <param name="includeZeroAmountSellerCredit">Boolean indicating whether to include seller credit despite zero dollar amount.</param>
        /// <param name="includeAssetsAsAdjustments">Boolean indicating whether to include the asset adjustments.</param>
        /// <returns>An enumerable of adjustments.</returns>
        public IEnumerable<Adjustment> GetEnumerable(bool includeZeroAmountSellerCredit, bool includeAssetsAsAdjustments)
        {
            IEnumerable<Adjustment> items = this.CustomItems;
            if (includeZeroAmountSellerCredit || this.SellerCredit.Amount != 0.0M)
            {
                items = new[] { this.SellerCredit }.Concat(items);
            }

            if (includeAssetsAsAdjustments)
            {
                items = items.Concat(this.AssetAdjustments ?? new List<Adjustment>());
            }

            return items;
        }

        /// <summary>
        /// Gets an enumerator for all items in the list, including the seller
        /// credit and custom items.
        /// </summary>
        /// <returns>
        /// An enumerator for all items in the list, including the seller credit
        /// and custom items.
        /// </returns>
        public IEnumerator<Adjustment> GetEnumerator()
        {
            return this.GetEnumerable(true, true).GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator for all items in the list, including the seller
        /// credit and custom items.
        /// </summary>
        /// <returns>
        /// An enumerator for all items in the list, including the seller credit
        /// and custom items.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Gets a value indicating whether the provided AdjustmentList is equal
        /// to the current AdjustmentList.
        /// </summary>
        /// <param name="other">The other AdjustmentList.</param>
        /// <returns>
        /// True if the adjustment lists have the same length and all contained
        /// items are equal.
        /// </returns>
        public bool Equals(AdjustmentList other)
        {
            if (other == null)
            {
                return false;
            }

            return this.SequenceEqual(other);
        }

        /// <summary>
        /// Gets a value indicating whether the provided object is equal
        /// to the current AdjustmentList.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>
        /// True if the object is an adjustment list that has the same length
        /// as the current adjustment list and all contained items are equal.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as AdjustmentList);
        }

        /// <summary>
        /// Gets a hash value for the current AdjustmentList.
        /// </summary>
        /// <returns>Returns 1 because this object is mutable.</returns>
        public override int GetHashCode()
        {
            return 1;
        }

        /// <summary>
        /// Sets the los convert to be used by this instance as well as all
        /// contained adjustments.
        /// </summary>
        /// <param name="convert">The converter to use.</param>
        public void SetLosConvert(LosConvert convert)
        {
            this.losConvert = convert;

            foreach (var adjustment in this)
            {
                adjustment.SetLosConvert(convert);
            }
        }

        /// <summary>
        /// Sets the loan version for the adjustments in this adjustment list.
        /// </summary>
        /// <param name="loanVersion">The loan version.</param>
        public void SetLoanVersionNumber(LoanVersionT loanVersion)
        {
            this.loanVersionT = loanVersion;
            foreach (var adjustment in this)
            {
                adjustment.SetLoanVersion(loanVersion);
            }
        }

        /// <summary>
        /// Updates the adjustment type and description for the adjustments if loan version is greater than 24.
        /// </summary>
        public void UsePredefinedAdjustmentsIfApplicable()
        {
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.loanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments))
            {
                this.SellerCredit.AdjustmentType = E_AdjustmentT.SellerCredit;

                Tuple<E_AdjustmentT, string> value;

                foreach (Adjustment adj in this.GetEnumeratorWithoutSellerCreditAndAssets())
                {
                    // Set the adjustment type if it is blank. 
                    // This will happen in webservices if they do not specify the adjustment type.
                    if (adj.AdjustmentType == E_AdjustmentT.Blank)
                    {
                        if (AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap.TryGetValue(adj.Description, out value))
                        {
                            adj.AdjustmentType = value.Item1;
                            adj.Description = value.Item2;
                        }
                        else
                        {
                            adj.AdjustmentType = E_AdjustmentT.Other;
                        }
                    }                    
                }
            }            
        }

        /// <summary>
        /// This checks this adjustment list to the passed in one, checking if the changes made from the passed in list are valid.
        /// Make sure that the adjustments in the two lists have the same ids.
        /// </summary>
        /// <param name="principal">The principal of the user who caused the changes.</param>
        /// <param name="oldAdjustmentList">The list to check against.</param>
        /// <param name="loads1003LineLFromAdjustments">The field sLoads1003LineLFromAdjustments.</param>
        /// <param name="error">Any errors found.</param>
        /// <returns>True if valid. False otherwise.</returns>
        public bool ValidateToOldAdjustmentList(LendersOffice.Security.AbstractUserPrincipal principal, AdjustmentList oldAdjustmentList, bool loads1003LineLFromAdjustments, out string error)
        {
            if (oldAdjustmentList == null)
            {
                throw new ArgumentNullException(nameof(oldAdjustmentList));
            }

            oldAdjustmentList.SetLosConvert(this.losConvert);
            oldAdjustmentList.SetLoanVersionNumber(this.loanVersionT);

            Adjustment newSellerCredit = this.SellerCredit;
            Adjustment oldSellerCredit = oldAdjustmentList.SellerCredit;
            string sellerCreditError = "The {0} field of the default Seller Credit Adjustment cannot be modified";
            if (!string.Equals(Adjustment.SellerCreditDesc, newSellerCredit.Description, StringComparison.OrdinalIgnoreCase))
            {
                error = string.Format(sellerCreditError, "Description");
                return false;
            }

            if (newSellerCredit.IsPopulateToLineLOn1003 != oldSellerCredit.IsPopulateToLineLOn1003 &&
                !loads1003LineLFromAdjustments)
            {
                error = string.Format(sellerCreditError, "IsPopulateToLineLOn1003");
                return false;
            }

            if (newSellerCredit.PaidFromParty != oldSellerCredit.PaidFromParty)
            {
                error = string.Format(sellerCreditError, "PaidFromParty");
                return false;
            }

            if (newSellerCredit.PaidToParty != oldSellerCredit.PaidToParty)
            {
                error = string.Format(sellerCreditError, "PaidToParty");
                return false;
            }

            if (newSellerCredit.DFLP != oldSellerCredit.DFLP)
            {
                error = string.Format(sellerCreditError, "DFLP");
                return false;
            }

            bool hasFundingFolderWritePermission = principal.HasPermission(LendersOffice.Security.Permission.AllowCloserWrite);

            IEnumerable<Adjustment> newAdjustments = this.GetEnumeratorWithoutSellerCreditAndAssets().Where((a) => !a.Id.HasValue);
            foreach (var newAdjustment in newAdjustments)
            {
                if (newAdjustment.AdjustmentType == E_AdjustmentT.SellerCredit)
                {
                    error = "Adjustments of type Seller Credit can only be modified and not added";
                    return false;
                }

                // New adjustment. Default value for DFLP and IsPopulateToLineLOn1003 is false so we have to check if they've been set to true.
                if (newAdjustment.DFLP)
                {
                    if (!hasFundingFolderWritePermission)
                    {
                        error = "Funding Folder permission required to edit DFLP fields";
                        return false;
                    }
                }

                if (newAdjustment.IsPopulateToLineLOn1003)
                {
                    if (!loads1003LineLFromAdjustments)
                    {
                        error = "IsPopulateToLineLOn1003 Field cannot be modified.";
                        return false;
                    }
                }

                if (!this.ValidatePartiesForAdjustment(newAdjustment, out error))
                {
                    return false;
                }
            }

            Dictionary<Guid, Adjustment> newAdjustmentMap = this.GetEnumeratorWithoutSellerCreditAndAssets().Where((a) => a.Id.HasValue).ToDictionary((adj) => adj.Id.Value);
            Dictionary<Guid, Adjustment> oldAdjustmentMap = oldAdjustmentList.GetEnumeratorWithoutSellerCreditAndAssets().ToDictionary((adj) => adj.Id.Value);
            foreach (var newAdjustment in newAdjustmentMap)
            {
                Adjustment oldAdjustment;
                if (oldAdjustmentMap.TryGetValue(newAdjustment.Key, out oldAdjustment))
                {
                    // DFLP checkbox is hidden if they don't have the Funding Folder permission
                    // It is disabled if the adjustment being modified has Paid To Party set to something other than Lender. However, the code semi-calculates DFLP based on this requirement so no need to check this.
                    if (newAdjustment.Value.DFLP != oldAdjustment.DFLP)
                    {
                        if (!hasFundingFolderWritePermission)
                        {
                            error = "Funding Folder permission required to edit DFLP fields";
                            return false;
                        }
                    }

                    // Can't be modified if
                    // - POC is true => Calculated by code. No need to check.
                    // - PaidFromParty != Borrower && PaidToParty != Borrower => Calculated by code. No need to check.
                    // - sLoads1003LineLFromAdjustments is false 
                    if (newAdjustment.Value.IsPopulateToLineLOn1003 != oldAdjustment.IsPopulateToLineLOn1003)
                    {
                        if (!loads1003LineLFromAdjustments)
                        {
                            error = "IsPopulateToLineLOn1003 Field cannot be modified.";
                            return false;
                        }
                    }
                }
                else
                {
                    if (newAdjustment.Value.AdjustmentType == E_AdjustmentT.SellerCredit)
                    {
                        error = "Adjustments of type Seller Credit can only be modified and not added";
                        return false;
                    }

                    // Not in the old adjustment map so this must be an addition with an ID.
                    if (newAdjustment.Value.DFLP)
                    {
                        if (!hasFundingFolderWritePermission)
                        {
                            error = "Funding Folder permission required to edit DFLP fields";
                            return false;
                        }
                    }

                    if (newAdjustment.Value.IsPopulateToLineLOn1003)
                    {
                        if (!loads1003LineLFromAdjustments)
                        {
                            error = "IsPopulateToLineLOn1003 Field cannot be modified.";
                            return false;
                        }
                    }
                }

                if (!this.ValidatePartiesForAdjustment(newAdjustment.Value, out error))
                {
                    return false;
                }
            }

            error = string.Empty;
            return true;
        }

        /// <summary>
        /// Validates the parties for an adjustment.
        /// </summary>
        /// <param name="adjustment">
        /// The adjustment to validate.
        /// </param>
        /// <param name="error">
        /// The error message, if any.
        /// </param>
        /// <returns>
        /// True if the validation succeeded, false otherwise.
        /// </returns>
        private bool ValidatePartiesForAdjustment(Adjustment adjustment, out string error)
        {
            error = null;

            var isPrincipalCurtailmentAdjustment = adjustment.AdjustmentType.EqualsOneOf(
                E_AdjustmentT.PrincipalReductionToCureToleranceViolation,
                E_AdjustmentT.PrincipalReductionToReduceCashToBorrower);

            if (isPrincipalCurtailmentAdjustment && 
                (adjustment.PaidFromParty != E_PartyT.Borrower || adjustment.PaidToParty != E_PartyT.Lender))
            {
                error = ErrorMessages.PrincipalCurtailmentPartiesCannotBeModified;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Resets the los convert of this object back to the default before
        /// serializing.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void OnSerializing(StreamingContext context)
        {
            this.losConvert = new LosConvert();
        }

        /// <summary>
        /// After being serialized.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            this.GenerateIdsOnSerialization = false;
        }

        /// <summary>
        /// Verifies the SellerCredit for this list has the expected description
        /// and paid to / from values.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void ValidateSellerCredit(StreamingContext context)
        {
            this.SellerCredit.Description = "Seller Credit";
            this.SellerCredit.PaidFromParty = E_PartyT.Seller;
            this.SellerCredit.PaidToParty = E_PartyT.Borrower;
        }
    }
}
