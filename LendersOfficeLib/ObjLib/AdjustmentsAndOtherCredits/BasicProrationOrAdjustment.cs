﻿// <copyright file="BasicProrationOrAdjustment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   5/28/2015
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// A simple class for storing info common to both prorations and adjustments.
    /// </summary>
    public class BasicProrationOrAdjustment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasicProrationOrAdjustment" /> class.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="desc">The description.</param>
        /// <param name="paidFromParty">The party where it is paid from.</param>
        /// <param name="populatesTo1003">Whether or not it populates to the 1003.</param>
        /// <param name="paidToParty">The party where it is paid to.</param>
        /// <param name="adjustmentType">The type of the adjustment.</param>
        public BasicProrationOrAdjustment(decimal amount, string desc, E_PartyT paidFromParty, bool populatesTo1003, E_PartyT paidToParty, E_AdjustmentT? adjustmentType)
        {
            this.Amount = amount;
            this.Desc = desc;
            this.PaidFromParty = paidFromParty;
            this.PaidToParty = paidToParty;
            this.IsPopulateTo1003 = populatesTo1003;

            if (this.PaidFromParty == E_PartyT.Borrower && this.Amount > 0)
            {
                this.Amount = -this.Amount;
                this.PaidFromParty = this.PaidToParty;
                this.PaidToParty = E_PartyT.Borrower;
            }

            this.AdjustmentType = adjustmentType;
        }

        /// <summary>
        /// Gets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public decimal Amount { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Desc { get; private set; }

        /// <summary>
        /// Gets the value of which party it is paid from.
        /// </summary>
        /// <value>The party where it is paid from.</value>
        public E_PartyT PaidFromParty { get; private set; }

        /// <summary>
        /// Gets the value of which party it is paid to.
        /// </summary>
        /// <value>The party where it is paid to.</value>
        public E_PartyT PaidToParty { get; private set; }

        /// <summary>
        /// Gets a value indicating whether or not it populates to the 1003.
        /// </summary>
        /// <value>Whether or not it populates to the 1003.</value>
        public bool IsPopulateTo1003 { get; private set; }

        /// <summary>
        /// Gets the adjustment type for the adjustment.
        /// Will be null if it's a proration.
        /// </summary>
        /// <value>Value of the adjustment type, or null.</value>
        public E_AdjustmentT? AdjustmentType { get; private set; }
    }
}
