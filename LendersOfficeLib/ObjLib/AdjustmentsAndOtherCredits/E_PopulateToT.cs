﻿// <copyright file="E_PopulateToT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   5/1/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// Where to populate adjustment data.
    /// </summary>
    public enum E_PopulateToT
    {
        /// <summary>
        /// Do not populate the data to other areas of the system.
        /// </summary>
        DoNotPopulate = 0,

        /// <summary>
        /// Populate the data to line L of the 1003.
        /// </summary>
        LineLOn1003 = 1
    }
}