﻿// <copyright file="ProrationList.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/23/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;
    using Newtonsoft.Json;

    /// <summary>
    /// Container for list of proration items, as well as data points needed for
    /// proration calculations.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ProrationList : IEnumerable<Proration>, IEquatable<ProrationList>
    {
        /// <summary>
        /// The closing date.
        /// </summary>
        private CDateTime closingDate;

        /// <summary>
        /// The converter used to generate the reps of child prorations.
        /// </summary>
        private LosConvert losConvert;

        /// <summary>
        /// We probably only want to set this when we are about to save.
        /// </summary>
        private bool generateIdsOnSerialization;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProrationList" /> class.
        /// Does NOT add the default proration items..
        /// </summary>
        public ProrationList()
        {
            this.losConvert = new LosConvert();
            this.CityTownTaxes = new Proration(E_ProrationT.CityTownTaxes);
            this.CountyTaxes = new Proration(E_ProrationT.CountyTaxes);
            this.Assessments = new Proration(E_ProrationT.Assessments);
            this.CustomItems = new List<Proration>();
            this.ClosingDate = CDateTime.InvalidWrapValue;
            this.IncludeProrationsInLeCdForThisLien = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProrationList" /> class.
        /// Adds default items.
        /// </summary>
        /// <param name="closingDate">The closing date.</param>
        public ProrationList(CDateTime closingDate)
            : this()
        {
            this.ClosingDate = closingDate ?? CDateTime.InvalidWrapValue;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we should generate ids on the next serialization. <para/>
        /// Set this everytime just prior to Save so we can set the id if it's empty. <para/>
        /// Alternatively we could just have a save method that also bumps the loan version and sets the id if it's not there.
        /// </summary>
        /// <value>If true we will set ids on the next serialization and then reset it to false.</value>
        public bool GenerateIdsOnSerialization
        {
            get
            {
                return this.generateIdsOnSerialization;
            }

            set
            {
                this.generateIdsOnSerialization = value;

                var prorations = this.GetEnumerable(includeZeroAmountSystemAdjustments: true);
                if (prorations != null)
                {
                    foreach (var proration in prorations)
                    {
                        if (proration != null)
                        {
                            proration.GenerateIdOnSerialization = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the closing date. Setting this value will propagate it 
        /// to all proration elements.
        /// </summary>
        /// <value>The closing date.</value>
        public CDateTime ClosingDate 
        { 
            get
            {
                return this.closingDate;
            }

            set
            {
                this.closingDate = value;
                this.UpdateClosingDateOnAllItems();
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the closing date. Setting 
        /// this value will propagate it to all proration elements.
        /// </summary>
        /// <value>The string representation of the closing date.</value>
        [JsonProperty]
        public string ClosingDate_rep
        {
            get
            {
                return this.ClosingDate.ToString(this.losConvert);
            }

            set
            {
                this.ClosingDate = CDateTime.Create(value, this.losConvert);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating where the proration data should 
        /// populate.
        /// </summary>
        /// <value>Where the data should populate.</value>
        [JsonProperty]
        public E_PopulateToT PopulateTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the proration list populates to line L on the 1003.
        /// </summary>
        /// <value>True iff PopulateTo is LineLOn1003.</value>
        [JsonProperty("IsPopulateToLineLOn1003")]
        public bool IsPopulateToLineLOn1003
        {
            get
            {
                return this.PopulateTo == E_PopulateToT.LineLOn1003;
            }

            set
            {
                if (value == true)
                {
                    this.PopulateTo = E_PopulateToT.LineLOn1003;
                }
                else
                {
                    this.PopulateTo = E_PopulateToT.DoNotPopulate;
                }
            }
        }

        /// <summary>
        /// Gets the city/town taxes proration.
        /// </summary>
        /// <value>The city/town taxes proration.</value>
        [JsonProperty]
        public Proration CityTownTaxes { get; private set; }

        /// <summary>
        /// Gets the county taxes proration.
        /// </summary>
        /// <value>The county taxes proration.</value>
        [JsonProperty]
        public Proration CountyTaxes { get; private set; }

        /// <summary>
        /// Gets the assessments proration.
        /// </summary>
        /// <value>The assessments proration.</value>
        [JsonProperty]
        public Proration Assessments { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether proration list should be included in the LE and CD for this lien.
        /// </summary>
        /// <value>The boolean representation of whether to include proration list in LE and CD for this lien.</value>
        [JsonProperty]
        public bool IncludeProrationsInLeCdForThisLien { get; set; }

        /// <summary>
        /// Gets or sets the manually entered proration items.
        /// </summary>
        /// <value>The manually entered proration items.</value>
        [JsonProperty]
        private List<Proration> CustomItems { get; set; }        

        /// <summary>
        /// Adds a proration to the list.
        /// </summary>
        /// <param name="p">The proration to add.</param>
        public void Add(Proration p)
        {
            if (p == null)
            {
                throw new ArgumentNullException();
            }
            else if (p.ProrationType != E_ProrationT.Custom)
            {
                throw new ArgumentException(
                    "Only support adding Custom prorations.");
            }

            p.ClosingDate = this.ClosingDate;
            p.SetLosConvert(this.losConvert);
            this.CustomItems.Add(p);
        }

        /// <summary>
        /// Removes a Proration from the enumerable.
        /// </summary>
        /// <param name="p">The Proration to remove.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when p is null.</exception>
        public void Remove(Proration p)
        {
            if (p == null)
            {
                throw new ArgumentNullException();
            }

            this.CustomItems.Remove(p);
        }

        /// <summary>
        /// Does nothing if the proration list is not marked as populating to the 1003. <para></para>
        /// If it is marked as populating to the 1003, clears (sets to zero) any standard prorations, and removes any custom prorations.
        /// </summary>
        public void ClearProrationsIfPopulatingTo1003()
        {
            if (!this.IsPopulateToLineLOn1003)
            {
                return;
            }

            this.CustomItems.Clear();

            foreach (var proration in this)
            {
                proration.Amount = 0;
            }

            this.IsPopulateToLineLOn1003 = false;
        }

        /// <summary>
        /// Sets the converter to be used by this instance and all child
        /// prorations it contains.
        /// </summary>
        /// <param name="convert">The converter to use.</param>
        public void SetLosConvert(LosConvert convert)
        {
            this.losConvert = convert;

            foreach (var proration in this)
            {
                proration.SetLosConvert(convert);
            }
        }

        /// <summary>
        /// Gets an enumerator.
        /// </summary>
        /// <returns>An enumerator.</returns>
        public IEnumerator<Proration> GetEnumerator()
        {
            return this.GetEnumerable(true).GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator.
        /// </summary>
        /// <returns>An enumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerable of prorations.
        /// </summary>
        /// <param name="includeZeroAmountSystemAdjustments">Boolean indicating whether to include system-generated prorations despite zero dollar amount.</param>
        /// <returns>An enumerable of the prorations.</returns>
        public IEnumerable<Proration> GetEnumerable(bool includeZeroAmountSystemAdjustments)
        {
            IEnumerable<Proration> systemItems = new Proration[]
            {
                this.CityTownTaxes,
                this.CountyTaxes,
                this.Assessments
            };

            if (!includeZeroAmountSystemAdjustments)
            {
                systemItems = systemItems.Where(p => p.Amount != 0.0M);
            }

            return systemItems.Concat(this.CustomItems);
        }

        /// <summary>
        /// Gets an enumerable of the custom prorations.
        /// </summary>
        /// <returns>An enumerable of the custom prorations.</returns>
        public IEnumerable<Proration> GetCustomItemsEnumerable()
        {
            return this.CustomItems;
        }

        /// <summary>
        /// Creates and returns a copy of this ProrationList.
        /// </summary>
        /// <returns>A complete copy of this proration list.</returns>
        /// <param name="idAssignment">The method of assigning the id.</param>
        public ProrationList GetUnlinkedCopy(IdAssignment idAssignment)
        {
            var proList = new ProrationList();

            proList.PopulateTo = this.PopulateTo;

            proList.IsPopulateToLineLOn1003 = this.IsPopulateToLineLOn1003;

            proList.CityTownTaxes.OverrideFrom(this.CityTownTaxes, idAssignment);

            proList.CountyTaxes.OverrideFrom(this.CountyTaxes, idAssignment);

            proList.Assessments.OverrideFrom(this.Assessments, idAssignment);

            foreach (var proration in this.CustomItems)
            {
                proList.Add(new Proration(proration, idAssignment));
            }

            proList.ClosingDate = CDateTime.Create(this.ClosingDate_rep, null);

            proList.SetLosConvert(this.losConvert);

            proList.IncludeProrationsInLeCdForThisLien = this.IncludeProrationsInLeCdForThisLien;

            return proList;
        }

        /// <summary>
        /// Compares this proration list to a passed in proration list to validate the changes.
        /// </summary>
        /// <param name="oldProrationList">The old proratino list to compare against.</param>
        /// <param name="loads1003LineLFromAdjustments">The field sLoads1003LineLFromAdjustments.</param>
        /// <param name="error">Any validation errors.</param>
        /// <returns>True if validated, false otherwise.</returns>
        public bool ValidateToOldProrationList(ProrationList oldProrationList, bool loads1003LineLFromAdjustments, out string error)
        {
            if (oldProrationList == null)
            {
                throw new ArgumentNullException(nameof(oldProrationList));
            }

            oldProrationList.SetLosConvert(this.losConvert);

            if (this.CityTownTaxes.ProrationType != E_ProrationT.CityTownTaxes ||
                this.CountyTaxes.ProrationType != E_ProrationT.CountyTaxes ||
                this.Assessments.ProrationType != E_ProrationT.Assessments)
            {
                // The Description property automatically calculates the description if the Proration is set to one of these types,
                // so it should be sufficient to check that the types haven't changed since the descriptions will be correct as long as the types are correct.
                error = "Descriptions/Types for Prorations of type City/Town Taxes, County Taxes, or Assessments cannot be modified";
                return false;
            }

            // Can't modify this if sLoads1003LineLFromAdjustments is false.
            bool newIsPopulateToLineLOn1003 = this.IsPopulateToLineLOn1003;
            bool oldIsPopulateToLineLOn1003 = oldProrationList.IsPopulateToLineLOn1003;
            if (!loads1003LineLFromAdjustments && newIsPopulateToLineLOn1003 != oldIsPopulateToLineLOn1003)
            {
                error = "IsPopulateToLineLOn1003 Field cannot be modified.";
                return false;
            }

            // Prorations without an ID are going to be treated as new prorations.
            IEnumerable<Proration> newAddedProrations = this.GetCustomItemsEnumerable().Where((p) => !p.Id.HasValue);
            foreach (var newProration in newAddedProrations)
            {
                if (newProration.IsSystemProration)
                {
                    error = "Prorations of type City/Town Taxes, County Taxes, Assessments can only be modified and not added";
                    return false;
                }
            }

            Dictionary<Guid, Proration> changedProrationMap = this.GetCustomItemsEnumerable().Where((p) => p.Id.HasValue).ToDictionary((proration) => proration.Id.Value);
            Dictionary<Guid, Proration> oldProrationMap = oldProrationList.GetCustomItemsEnumerable().ToDictionary((proration) => proration.Id.Value);
            foreach (var changedProration in changedProrationMap)
            {
                Proration oldProration;
                if (!oldProrationMap.TryGetValue(changedProration.Key, out oldProration))
                {
                    // If it's not in the old, it must be a new proration that was already assigned an ID. 
                    if (changedProration.Value.IsSystemProration)
                    {
                        error = "Prorations of type City/Town Taxes, County Taxes, Assessments can only be modified and not added";
                        return false;
                    }
                }
            }

            error = string.Empty;
            return true;
        }

        /// <summary>
        /// Gets a value indicating whether the provided proration list is equal
        /// to the current one.
        /// </summary>
        /// <param name="other">The other proration list.</param>
        /// <returns>
        /// True if the closing date, population info, and sequences are equal.
        /// Otherwise, false.
        /// </returns>
        public bool Equals(ProrationList other)
        {
            if (other == null)
            {
                return false;
            }

            return this.ClosingDate.SameAs(other.ClosingDate) &&
                this.PopulateTo == other.PopulateTo &&
                this.SequenceEqual(other);
        }

        /// <summary>
        /// Gets a value indicating whether the provided object is equal
        /// to the current proration list.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>
        /// True if the object is a ProrationList and the closing date, population
        /// info, and sequences are equal. Otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as ProrationList);
        }

        /// <summary>
        /// Gets a hash value for the proration list.
        /// </summary>
        /// <returns>Returns 1 because the object is mutable.</returns>
        public override int GetHashCode()
        {
            return 1;
        }

        /// <summary>
        /// Sets the closing date for all of the proration instances in Items.
        /// </summary>
        private void UpdateClosingDateOnAllItems()
        {
            foreach (var item in this)
            {
                item.ClosingDate = this.ClosingDate;
            }
        }

        /// <summary>
        /// Resets the converter prior to serialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void OnSerializing(StreamingContext context)
        {
            this.SetLosConvert(new LosConvert());
        }

        /// <summary>
        /// After being serialized.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            this.GenerateIdsOnSerialization = false;
        }

        /// <summary>
        /// Ensures object is in proper state after deserialization. Transfers
        /// closing date to all child items.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            this.CityTownTaxes.ProrationType = E_ProrationT.CityTownTaxes;
            this.CountyTaxes.ProrationType = E_ProrationT.CountyTaxes;
            this.Assessments.ProrationType = E_ProrationT.Assessments;
            this.UpdateClosingDateOnAllItems();
        }
    }
}
