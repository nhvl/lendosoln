﻿// <copyright file="E_ProrationT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   5/1/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// The type of a proration.
    /// </summary>
    public enum E_ProrationT
    {
        /// <summary>
        /// A custom proration that was not included by default by the system.
        /// </summary>
        Custom = 0,

        /// <summary>
        /// Default city/town taxes.
        /// </summary>
        CityTownTaxes = 1,

        /// <summary>
        /// Default county taxes.
        /// </summary>
        CountyTaxes = 2,

        /// <summary>
        /// Default assessments.
        /// </summary>
        Assessments = 3
    }
}