﻿// <copyright file="ProrationListWorkflowEqualityComparer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/30/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Equality comparer for proration lists that ignores the closing date.
    /// </summary>
    public class ProrationListWorkflowEqualityComparer : EqualityComparer<ProrationList>
    {
        /// <summary>
        /// Gets a value indicating if the proration lists are considered equal.
        /// </summary>
        /// <param name="x">A proration list.</param>
        /// <param name="y">Another proration list.</param>
        /// <returns>
        /// True if the PopulateTo and sequences are equal. Ignores the closing
        /// date of the list and contained items. Otherwise, false.
        /// </returns>
        public override bool Equals(ProrationList x, ProrationList y)
        {
            if (object.ReferenceEquals(x, y))
            {
                return true;
            }
            else if (x != null && y != null)
            {
                var prorationComparer = new ProrationWorkflowEqualityComparer();

                return x.PopulateTo == y.PopulateTo &&
                    x.SequenceEqual(y, prorationComparer);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a hash value for the proration list.
        /// </summary>
        /// <param name="obj">The proration list.</param>
        /// <returns>Return 1 because the proration lists are mutable.</returns>
        public override int GetHashCode(ProrationList obj)
        {
            return 1;
        }
    }
}
