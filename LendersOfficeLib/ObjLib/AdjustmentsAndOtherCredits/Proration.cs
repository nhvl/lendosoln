﻿// <copyright file="Proration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/23/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using DataAccess;
    using Newtonsoft.Json;

    /// <summary>
    /// Represents a proration.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Proration : IEquatable<Proration>
    {
        /// <summary>
        /// Maps system proration types to their respective description values.
        /// </summary>
        public static readonly IReadOnlyDictionary<E_ProrationT, string> SystemProrationDescriptions = new Dictionary<E_ProrationT, string>
        {
            { E_ProrationT.Assessments, "Assessments" },
            { E_ProrationT.CityTownTaxes, "City/Town Taxes" },
            { E_ProrationT.CountyTaxes, "County Taxes" }
        };

        /// <summary>
        /// The description.
        /// </summary>
        private string description;

        /// <summary>
        /// The converter that will be used to transform to/from string 
        /// representations.
        /// </summary>
        private LosConvert losConvert;

        /// <summary>
        /// The closing date.
        /// </summary>
        private CDateTime closingDate;

        /// <summary>
        /// The date the item is paid through.
        /// </summary>
        private CDateTime paidThroughDate;

        /// <summary>
        /// The amount paid.
        /// </summary>
        private decimal amount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Proration" /> class.
        /// </summary>
        public Proration()
        {
            this.description = string.Empty;
            this.losConvert = new LosConvert();
            this.closingDate = CDateTime.InvalidWrapValue;
            this.paidThroughDate = CDateTime.InvalidWrapValue;
            this.PaidFromDate = CDateTime.InvalidWrapValue;
            this.PaidToDate = CDateTime.InvalidWrapValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Proration" /> class.
        /// </summary>
        /// <param name="type">The type of the proration.</param>
        public Proration(E_ProrationT type)
            : this()
        {
            this.ProrationType = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Proration" /> class.
        /// </summary>
        /// <param name="other">The proration to initialize the instance with.</param>
        /// <param name="idAssignment">The method of assigning the id.</param>
        public Proration(Proration other, IdAssignment idAssignment)
            : this()
        {
            this.OverrideFrom(other, idAssignment);
        }

        /// <summary>
        /// Gets the id of the adjustment.
        /// </summary>
        /// <value>The id of the adjustment.</value>
        [JsonProperty]
        public Guid? Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we should generate ids on the next serialization. <para/>
        /// Set this everytime just prior to Save so we can set the id if it's empty. <para/>
        /// Alternatively we could just have a save method that also bumps the loan version and sets the id if it's not there.
        /// </summary>
        /// <value>If true we will set ids on the next serialization and then reset it to false.</value>
        public bool GenerateIdOnSerialization { get; set; }

        /// <summary>
        /// Gets or sets the type of the proration.
        /// </summary>
        /// <value>
        /// The type of the proration. If not a default proration, it will be
        /// Custom.
        /// </value>
        [JsonProperty]
        public E_ProrationT ProrationType { get; set; }

        /// <summary>
        /// Gets a value indicating whether the proration is automatically
        /// included by the system.
        /// </summary>
        /// <value>True if the proration is included by default. Otherwise, false.</value>
        [JsonProperty]
        public bool IsSystemProration 
        {
            get
            {
                return this.ProrationType != E_ProrationT.Custom;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the proration is paid from the
        /// borrower to the seller.
        /// </summary>
        /// <value>
        /// True if the proration is paid from the borrower to the seller.
        /// Otherwise, false.
        /// </value>
        [JsonProperty]
        public bool IsPaidFromBorrowerToSeller
        {
            get 
            { 
                return this.PaidFromParty == E_PartyT.Borrower &&
                    this.PaidToParty == E_PartyT.Seller; 
            }
        }

        /// <summary>
        /// Gets a value indicating whether the proration is paid from the
        /// seller to the borrower.
        /// </summary>
        /// <value>
        /// True if the proration is paid from the seller to the borrower.
        /// Otherwise, false.
        /// </value>
        [JsonProperty]
        public bool IsPaidFromSellerToBorrower
        {
            get
            {
                return this.PaidFromParty == E_PartyT.Seller &&
                    this.PaidToParty == E_PartyT.Borrower;
            }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty]
        public string Description 
        {
            get
            {
                if (this.ProrationType == E_ProrationT.Custom)
                {
                    return this.description;
                }
                else
                {
                    return SystemProrationDescriptions[this.ProrationType];
                }
            }

            set
            {
                this.description = value;
            }
        }

        /// <summary>
        /// Gets or sets the closing date. Setting this value may cause several
        /// other values to be updated, including the paid to / from dates and 
        /// paid to / from parties.
        /// </summary>
        /// <value>The closing date.</value>
        public CDateTime ClosingDate
        {
            get
            {
                return this.closingDate;
            }

            set
            {
                this.closingDate = value;
                this.CalculateTransactionDetails();
            }
        }

        /// <summary>
        /// Gets a string representation of the closing date.
        /// </summary>
        /// <value>The closing date in string form.</value>
        public string ClosingDate_rep
        {
            get
            {
                return this.ClosingDate.ToString(this.losConvert);
            }
        }

        /// <summary>
        /// Gets or sets the date the proration is paid through.
        /// </summary>
        /// <value>The date the proration is paid through.</value>
        public CDateTime PaidThroughDate 
        {
            get
            {
                return this.paidThroughDate;
            }

            set
            {
                this.paidThroughDate = value;
                this.CalculateTransactionDetails();
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the paid through date.
        /// </summary>
        /// <value>The string representation of the paid through date.</value>
        [JsonProperty("PaidThroughDate")]
        public string PaidThroughDate_rep
        {
            get
            {
                return this.PaidThroughDate.ToString(this.losConvert);
            }

            set
            {
                this.PaidThroughDate = CDateTime.Create(value, this.losConvert);
            }
        }

        /// <summary>
        /// Gets the initial date the proration is paid from.
        /// </summary>
        /// <value>The initial date the proration is paid from.</value>
        public CDateTime PaidFromDate { get; private set; }

        /// <summary>
        /// Gets the string representation that the proration is paid from.
        /// </summary>
        /// <value>The string representation that the proration is paid from.</value>
        [JsonProperty("PaidFromDate")]
        public string PaidFromDate_rep
        {
            get { return this.PaidFromDate.ToString(this.losConvert); }
        }

        /// <summary>
        /// Gets the string representation of the date the proration is paid to.
        /// </summary>
        /// <value>The date the proration is paid to.</value>
        public CDateTime PaidToDate { get; private set; }

        /// <summary>
        /// Gets the string representation of the date the proration is paid to.
        /// </summary>
        /// <value>
        /// The string representation of the date the proration is paid to.
        /// </value>
        [JsonProperty("PaidToDate")]
        public string PaidToDate_rep
        {
            get { return this.PaidToDate.ToString(this.losConvert); }
        }

        /// <summary>
        /// Gets or sets the amount of the payment.
        /// </summary>
        /// <value>The amount of the payment.</value>
        public decimal Amount
        {
            get
            {
                return Math.Max(0, this.amount);
            }

            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the amount of the payment.
        /// </summary>
        /// <value>The string representation of the amount of the payment.</value>
        [JsonProperty("Amount")]
        public string Amount_rep
        {
            get 
            { 
                return this.losConvert.ToMoneyString(this.Amount, FormatDirection.ToRep); 
            }

            set 
            { 
                this.Amount = this.losConvert.ToMoney(value); 
            }
        }

        /// <summary>
        /// Gets the amount paid from the borrower to the seller.
        /// </summary>
        /// <value>
        /// The amount paid from the borrower to the seller. Returns 0 if the
        /// proration is not paid from the borrower to the seller. 
        /// </value>
        public decimal AmountPaidFromBorrowerToSeller
        {
            get 
            {
                if (this.PaidFromParty == E_PartyT.Borrower &&
                    this.PaidToParty == E_PartyT.Seller)
                {
                    return this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string representation of the amount paid from the borrower
        /// to the seller.
        /// </summary>
        /// <value>
        /// The string representation of the amount paid from the borrower to
        /// the seller.
        /// </value>
        [JsonProperty("AmountPaidFromBorrowerToSeller")]
        public string AmountPaidFromBorrowerToSeller_rep
        {
            get
            {
                return this.losConvert.ToMoneyString(
                    this.AmountPaidFromBorrowerToSeller,
                    FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the amount paid from the seller to the borrower.
        /// </summary>
        /// <value>
        /// The amount paid from the seller to the borrower. Returns 0 if the
        /// proration is not paid from the seller to the borrower. 
        /// </value>
        public decimal AmountPaidFromSellerToBorrower
        {
            get
            {
                if (this.PaidFromParty == E_PartyT.Seller &&
                    this.PaidToParty == E_PartyT.Borrower)
                {
                    return this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string representation of the amount paid from the seller
        /// to the borrower.
        /// </summary>
        /// <value>
        /// The string representation of the amount paid from the seller to
        /// the borrower.
        /// </value>
        [JsonProperty("AmountPaidFromSellerToBorrower")]
        public string AmountPaidFromSellerToBorrower_rep
        {
            get
            {
                return this.losConvert.ToMoneyString(
                    this.AmountPaidFromSellerToBorrower,
                    FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the party the payment is coming from.
        /// </summary>
        /// <value>The party providing the payment.</value>
        [JsonProperty]
        public E_PartyT PaidFromParty { get; private set; }

        /// <summary>
        /// Gets the party the payment is going to.
        /// </summary>
        /// <value>The party receiving the payment.</value>
        [JsonProperty]
        public E_PartyT PaidToParty { get; private set; }

        /// <summary>
        /// Gets how much is paid to the borrower.  <para></para>
        /// Nothing if the amount is both from and to the borrower. <para></para>
        /// Can be negative if it is marked as from the borrower.
        /// </summary>
        /// <value>The amount paid to the borrower.  Can be negative positive or zero.</value>
        public decimal AmountToBorrower
        {
            get
            {
                if (this.PaidToParty == E_PartyT.Borrower &&
                    this.PaidFromParty == E_PartyT.Borrower)
                {
                    return 0;
                }
                else if (this.PaidToParty == E_PartyT.Borrower)
                {
                    return this.Amount;
                }
                else if (this.PaidFromParty == E_PartyT.Borrower)
                {
                    return -this.Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string representation of the amount paid to borrower.
        /// </summary>
        /// <value>The string representation of the amount paid to borrower.</value>
        public string AmountToBorrower_rep
        {
            get
            {
                return this.losConvert.ToMoneyString(this.AmountToBorrower, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the provided Proration is equal to
        /// the current Proration.
        /// </summary>
        /// <param name="other">The other Proration.</param>
        /// <returns>True if the proration items are equal. Otherwise, false.</returns>
        public bool Equals(Proration other)
        {
            if (other == null)
            {
                return false;
            }

            return this.ProrationType == other.ProrationType &&
                this.Description == other.Description &&
                ((this.ClosingDate == null && other.ClosingDate == null) || 
                    (this.ClosingDate != null && this.ClosingDate.SameAs(other.ClosingDate))) &&
                ((this.PaidThroughDate == null && other.PaidThroughDate == null) || 
                    (this.PaidThroughDate != null && this.PaidThroughDate.SameAs(other.PaidThroughDate))) &&
                this.Amount == other.Amount;
        }

        /// <summary>
        /// Gets a value indicating whether the provided object is equal to
        /// the current Proration.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>
        /// True if the object is a proration and the proration items are equal.
        /// Otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Proration);
        }

        /// <summary>
        /// Gets a hash value for the current proration.
        /// </summary>
        /// <returns>Returns 1 because the proration is mutable.</returns>
        public override int GetHashCode()
        {
            return 1;
        }

        /// <summary>
        /// Overrides settings from the other proration.
        /// </summary>
        /// <param name="other">The other proration we are copying over data from.</param>
        /// <param name="idAssignment">The method of assignmning the id.</param>
        public void OverrideFrom(Proration other, IdAssignment idAssignment)
        {
            this.description = other.Description;
            this.closingDate = CDateTime.Create(other.ClosingDate_rep, null);
            this.paidThroughDate = CDateTime.Create(other.PaidThroughDate_rep, null);
            this.PaidFromDate = CDateTime.Create(other.PaidFromDate_rep, null);
            this.PaidToDate = CDateTime.Create(other.PaidToDate_rep, null);
            this.PaidFromParty = other.PaidFromParty;
            this.PaidToParty = other.PaidToParty;
            this.ProrationType = other.ProrationType;
            this.Amount = other.Amount;
            switch (idAssignment)
            {
                case IdAssignment.GenerateNew:
                    this.Id = Guid.NewGuid();
                    break;
                case IdAssignment.Copy:
                    this.Id = other.Id;
                    break;
                case IdAssignment.LeaveIdUnchanged:
                    break;
                default:
                    throw new UnhandledEnumException(idAssignment);
            }
        }

        /// <summary>
        /// Sets the converter to be used by this instance when generating _rep fields.
        /// </summary>
        /// <param name="convert">The converter to use.</param>
        public void SetLosConvert(LosConvert convert)
        {
            this.losConvert = convert;
        }

        /// <summary>
        /// Calculates the paid to / from dates and the paid to / from parties
        /// based on the closing date and the paid through date.
        /// </summary>
        private void CalculateTransactionDetails()
        {
            if (this.ClosingDate == null || !this.ClosingDate.IsValid || 
                this.PaidThroughDate == null || !this.PaidThroughDate.IsValid)
            {
                this.PaidToParty = E_PartyT.Blank;
                this.PaidFromParty = E_PartyT.Blank;
                return;
            }

            var closingDate = this.ClosingDate.DateTimeForComputation;
            var paidThrough = this.paidThroughDate.DateTimeForComputation;
            var comparisonResult = paidThrough.AddDays(1).CompareTo(closingDate);

            if (comparisonResult == 0)
            {
                this.PaidFromDate = CDateTime.InvalidWrapValue;
                this.PaidToDate = CDateTime.InvalidWrapValue;
                this.PaidFromParty = E_PartyT.Blank;
                this.PaidToParty = E_PartyT.Blank;
            }
            else if (comparisonResult > 0)
            {
                this.PaidFromDate = CDateTime.Create(closingDate);
                this.PaidToDate = CDateTime.Create(paidThrough);
                this.PaidFromParty = E_PartyT.Borrower;
                this.PaidToParty = E_PartyT.Seller;
            }
            else
            {
                this.PaidFromDate = CDateTime.Create(paidThrough.AddDays(1));
                this.PaidToDate = CDateTime.Create(closingDate.AddDays(-1));
                this.PaidFromParty = E_PartyT.Seller;
                this.PaidToParty = E_PartyT.Borrower;
            }
        }

        /// <summary>
        /// Resets the converter prior to serialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void PrepareForSerialization(StreamingContext context)
        {
            this.SetLosConvert(new LosConvert());
            if (this.GenerateIdOnSerialization && !this.Id.HasValue)
            {
                this.Id = Guid.NewGuid();
            }
        }

        /// <summary>
        /// After being serialized.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        private void OnSerialized(StreamingContext context)
        {
            this.GenerateIdOnSerialization = false;
        }

        /// <summary>
        /// Makes sure the object is up to date after deserialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            this.CalculateTransactionDetails();
        }
    }
}
