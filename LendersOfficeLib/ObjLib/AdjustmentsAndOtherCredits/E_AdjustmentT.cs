﻿// <copyright file="E_AdjustmentT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   5/1/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    /// <summary>
    /// The type of an adjustment.
    /// </summary>
    /// <remarks>
    /// When adding another option to this list, make sure to check for places in the UI that uses this.
    /// </remarks>
    public enum E_AdjustmentT
    {
        /// <summary>
        /// No options selected. Only enabled if sIsRestrictAdjustmentsAndOtherCreditsTypes is false.
        /// </summary>
        Blank = -1,

        /// <summary>
        /// Employer assisted housing.
        /// </summary>
        EmployerAssistedHousing = 0,

        /// <summary>
        /// Lease purchase fund.
        /// </summary>
        LeasePurchaseFund = 1,

        /// <summary>
        /// Relocation funds.
        /// </summary>
        RelocationFunds = 2,

        /// <summary>
        /// Seller credit.
        /// </summary>
        SellerCredit = 3,

        /// <summary>
        /// Not one of the predefined types.
        /// </summary>
        Other = 4,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Fuel Costs.
        /// </summary>
        FuelCosts = 5,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Grant.
        /// </summary>
        Grant = 6,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Proceeds Of Subordinate Liens.
        /// </summary>
        ProceedsOfSubordinateLiens = 7,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Rebate Credit.
        /// </summary>
        RebateCredit = 8,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Rent From Subject Property.
        /// </summary>
        RentFromSubjectProperty = 9,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Repair Completion Escrow Holdback.
        /// </summary>
        RepairCompletionEscrowHoldback = 10,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Repairs.
        /// </summary>
        Repairs = 11,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Satisfaction Of Subordinate Liens.
        /// </summary>
        SatisfactionOfSubordinateLien = 12,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Sellers Escrow Assumption.
        /// </summary>
        SellersEscrowAssumption = 13,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Sellers Mortgage Insurance Assumption.
        /// </summary>
        SellersMortgageInsuranceAssumption = 14,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Sellers Reserve Account Assumption.
        /// </summary>
        SellersReserveAccountAssumption = 15,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Services.
        /// </summary>
        Services = 16,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Subordinate Financing Proceeds.
        /// </summary>
        SubordinateFinancingProceeds = 17,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Tenant Security Deposit.
        /// </summary>
        TenantSecurityDeposit = 18,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Title Premium Adjustment.
        /// </summary>
        TitlePremiumAdjustment = 19,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Trade Equity.
        /// </summary>
        TradeEquity = 20,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Unpaid Utility Escrow Holdback.
        /// </summary>
        UnpaidUtilityEscrowHoldback = 21,

        /// <summary>
        /// MISMO Base: Purchase Credit - Buydown Fund.
        /// </summary>
        BuydownFund = 22,

        /// <summary>
        /// MISMO Base: Purchase Credit - Commitment Origination Fee.
        /// </summary>
        CommitmentOriginationFee = 23,

        /// <summary>
        /// MISMO Base: Purchase Credit - Federal Agency Funding Fee Refund.
        /// </summary>
        FederalAgencyFundingFeeRefund = 24,

        /// <summary>
        /// MISMO Base: Purchase Credit - Gift Of Equity.
        /// </summary>
        GiftOfEquity = 25,

        /// <summary>
        /// MISMO Base: Purchase Credit - MI Premium Refund.
        /// </summary>
        MIPremiumRefund = 26,

        /// <summary>
        /// MISMO Base: Purchase Credit - Sweat Equity.
        /// </summary>
        SweatEquity = 27,

        /// <summary>
        /// MISMO Base: Purchase Credit - Trade Equity From Property Swap.
        /// </summary>
        TradeEquityFromPropertySwap = 28,

        /// <summary>
        /// Fannie Import: Other - Borrower Paid Fees.
        /// </summary>
        BorrowerPaidFees = 29,

        /// <summary>
        /// MISMO Base: Purchase Credit - Purchase Trade Equity (Credit).
        /// </summary>
        TradeEquityCredit = 30,

        /// <summary>
        /// MISMO Base: Closing Adjustment - Gift.
        /// </summary>
        Gift = 31,

        /// <summary>
        /// TRID 2017: Principal Reduction to Reduce Cash to Borrower.
        /// </summary>
        PrincipalReductionToReduceCashToBorrower = 32,

        /// <summary>
        /// TRID 2017: Principal Reduction to Cure Tolerance Violation.
        /// </summary>
        PrincipalReductionToCureToleranceViolation = 33
    }
}