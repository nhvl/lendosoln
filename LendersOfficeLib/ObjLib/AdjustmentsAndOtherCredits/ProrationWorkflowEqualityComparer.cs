﻿// <copyright file="ProrationWorkflowEqualityComparer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/30/15
// </summary>
namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System.Collections.Generic;

    /// <summary>
    /// Equality comparer for proration items that ignores closing dates.
    /// </summary>
    public class ProrationWorkflowEqualityComparer : EqualityComparer<Proration>
    {
        /// <summary>
        /// Gets a value indicating whether the proration items are equal.
        /// </summary>
        /// <param name="x">A proration.</param>
        /// <param name="y">Another proration.</param>
        /// <returns>True if the items are considered equal. Ignores the closing date.</returns>
        public override bool Equals(Proration x, Proration y)
        {
            if (object.ReferenceEquals(x, y))
            {
                return true;
            }
            else if (x != null && y != null)
            {
                return x.ProrationType == y.ProrationType &&
                    x.Description == y.Description &&
                    x.Amount == y.Amount &&
                    ((x.PaidThroughDate == null && y.PaidThroughDate == null) || 
                        (x.PaidThroughDate != null && x.PaidThroughDate.SameAs(y.PaidThroughDate)));
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a hash value for the proration.
        /// </summary>
        /// <param name="obj">The proration.</param>
        /// <returns>Returns 1 because the proration is mutable.</returns>
        public override int GetHashCode(Proration obj)
        {
            return 1;
        }
    }
}
