﻿namespace LendersOffice.ObjLib.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup;

    /// <summary>
    /// View model for adjustments and other credits setup page.
    /// </summary>
    public class AdjustmentsAndOtherCreditsSetupViewModel
    {
        /// <summary>
        /// Gets or sets a list of predefined adjustments that contains the data from the DB.
        /// </summary>
        /// <value>List of predefined adjustments from db.</value>
        public List<PredefinedAdjustment> SerializedAdjustments { get; set; }

        /// <summary>
        /// Gets or sets a list of string tuple that contains the value and the selectable string of the Adjustment Type dropdownlist.
        /// </summary>
        /// <value>List of tuples that contain dropdownlist values and text.</value>
        public List<Tuple<string, string>> SelectableAdjustments { get; set; }
    }
}