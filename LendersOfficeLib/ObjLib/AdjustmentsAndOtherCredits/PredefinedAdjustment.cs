﻿#region Auto-generated code
namespace LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Adapter;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// View model for adjustments and other credits setup page.
    /// </summary>
    public class PredefinedAdjustment : AdjustmentBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PredefinedAdjustment"/> class.
        /// </summary>
        /// <param name="id">Id of the predefined adjustment.</param>
        /// <param name="type">Type of the predefined adjustment.</param>
        /// <param name="description">Description for the predefined adjustment.</param>
        /// <param name="from">Who the adjustment will be paid from.</param>
        /// <param name="to">Who the adjustment will be paid to.</param>
        /// <param name="poc">The POC bit for the predefined adjustment.</param>
        /// <param name="includeIn1003Details">The bit that tells you whether to include in section L of 1003 page.</param>
        /// <param name="dflp">The DFLP bit for the predefined adjustment.</param>
        public PredefinedAdjustment(int id, int type, string description, int from, int to, bool poc, int includeIn1003Details, bool dflp)
        {
            this.ID = id;
            this.AdjustmentType = (E_AdjustmentT)type;
            this.Description = description;
            this.PaidFromParty = (E_PartyT)from;
            this.PaidToParty = (E_PartyT)to;
            this.POC = poc;
            this.DFLP = dflp;
            this.PopulateTo = (E_PopulateToT)includeIn1003Details;
        }

        /// <summary>
        /// Gets or sets the id that identifies the adjustment.
        /// </summary>
        /// <value>Int that represents the id.</value>        
        public int ID { get; set; }

        /// <summary>
        /// Retrieve adjustments and other credits set up data from the DB.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>List of adjustment setups that for a broker.</returns>
        public static List<PredefinedAdjustment> GetAllAdjustmentsForBroker(Guid brokerId)
        {
            List<PredefinedAdjustment> adjList = new List<PredefinedAdjustment>();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "Predefined_Adjustment_Retrieve_All_For_Broker", parameters))
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    int type = (int)reader["Type"];
                    string description = (string)reader["Description"];
                    int from = (int)reader["From"];
                    int to = (int)reader["To"];
                    bool poc = (bool)reader["POC"];
                    int includeIn1003Details = (int)reader["IncludeIn1003Details"];
                    bool dflp = (bool)reader["DFLP"];

                    PredefinedAdjustment data = new PredefinedAdjustment(id, type, description, from, to, poc, includeIn1003Details, dflp);
                    adjList.Add(data);
                }
            }

            return adjList;
        }

        /// <summary>
        /// Add a new predefined adjustment settings to the DB.
        /// </summary>
        /// <param name="conn">SqlConnection object.</param>
        /// <param name="trans">SqlTransaction object.</param>
        /// <param name="adj">Predefined Adjustment object.</param>
        /// <param name="brokerId">Broker identifier.</param>
        /// <param name="isDflpEnabled">Bool that tells you whether the user has Dflp enabled.</param>
        public static void AddPredefinedAdjustment(DbConnection conn, DbTransaction trans, PredefinedAdjustment adj, Guid brokerId, bool isDflpEnabled)
        {
            ValidatePredefinedAdjustment(adj, isDflpEnabled);

            StoredProcedureName spName = StoredProcedureName.Create("Predefined_Adjustment_Add").Value;

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Type", adj.AdjustmentType),
                new SqlParameter("@Description", adj.Description),
                new SqlParameter("@From", adj.PaidFromParty),
                new SqlParameter("@To", adj.PaidToParty),
                new SqlParameter("@POC", adj.POC),
                new SqlParameter("@IncludeIn1003Details", adj.PopulateTo),
                new SqlParameter("@DFLP", adj.DFLP)
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, spName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Update a predefined adjustment setting already existing in DB.
        /// </summary>
        /// <param name="conn">SqlConnection object.</param>
        /// <param name="trans">SqlTransaction object.</param>
        /// <param name="adj">Predefined Adjustment object.</param>
        /// <param name="brokerId">Broker identifier.</param>
        /// <param name="isDflpEnabled">Bool that tells you whether the user has Dflp enabled.</param>
        public static void UpdatePredefinedAdjustment(DbConnection conn, DbTransaction trans, PredefinedAdjustment adj, Guid brokerId, bool isDflpEnabled)
        {
            ValidatePredefinedAdjustment(adj, isDflpEnabled);

            StoredProcedureName spName = StoredProcedureName.Create("Predefined_Adjustment_Update").Value;

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", adj.ID),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Type", adj.AdjustmentType),
                new SqlParameter("@Description", adj.Description),
                new SqlParameter("@From", adj.PaidFromParty),
                new SqlParameter("@To", adj.PaidToParty),
                new SqlParameter("@POC", adj.POC),
                new SqlParameter("@IncludeIn1003Details", adj.PopulateTo),
                new SqlParameter("@DFLP", adj.DFLP)
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, spName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Delete a predefined adjustment setting in DB.
        /// </summary>
        /// <param name="conn">SqlConnection object.</param>
        /// <param name="trans">SqlTransaction object.</param>        
        /// <param name="id">Int id of the predefined adjustment to delete in the DB.</param>
        /// <param name="brokerId">The broker id.</param>
        public static void DeletePredefinedAdjustment(DbConnection conn, DbTransaction trans, int id, Guid brokerId)
        {
            StoredProcedureName spName = StoredProcedureName.Create("Predefined_Adjustment_Delete").Value;

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Id", id)
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, spName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Saves predefined adjustments to the DB.
        /// </summary>    
        /// <param name="addedAdjustments">List of added predefined adjustments.</param>
        /// <param name="updatedAdjustments">List of updated predefined adjustments.</param>
        /// <param name="deletedAdjustments">List of int ids for deleted predefined adjustments.</param>
        /// <param name="brokerId">Broker identifier.</param>
        /// <param name="isDflpEnabled">Bool that tells you whether the user has Dflp enabled.</param>
        public static void SavePredefinedAdjustments(List<PredefinedAdjustment> addedAdjustments, List<PredefinedAdjustment> updatedAdjustments, List<int> deletedAdjustments, Guid brokerId, bool isDflpEnabled)
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                conn.OpenWithRetry();

                using (DbTransaction tx = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (PredefinedAdjustment adj in addedAdjustments)
                        {
                            AddPredefinedAdjustment(conn, tx, adj, brokerId, isDflpEnabled);
                        }

                        foreach (PredefinedAdjustment adj in updatedAdjustments)
                        {
                            UpdatePredefinedAdjustment(conn, tx, adj, brokerId, isDflpEnabled);
                        }

                        foreach (int id in deletedAdjustments)
                        {
                            DeletePredefinedAdjustment(conn, tx, id, brokerId);
                        }

                        tx.Commit();
                    }
                    catch
                    {
                        tx.Rollback();
                        throw;
                    }                
                }
            }
        }

        /// <summary>
        /// Validate a predefined adjustment.
        /// </summary>
        /// <param name="adj">Adjustment to validate.</param>
        /// <param name="isDflpEnabled">Bool that tells you whether the user has Dflp enabled.</param>
        private static void ValidatePredefinedAdjustment(PredefinedAdjustment adj, bool isDflpEnabled)
        {
            string error;

            if (adj.AdjustmentType == E_AdjustmentT.Blank || adj.Description == string.Empty)
            {
                error = "Type of a predefined adjustment cannot be blank upon save.";
                throw new CBaseException(error, error);
            }

            if (adj.POC && adj.DFLP)
            {
                error = "POC and DFLP cannot be both checked.";
                throw new CBaseException(error, error);
            }

            if (adj.Description.Length > 60)
            {
                throw new CBaseException("Descriptions of adjustments on the Adjustments and Other Credits page cannot exceed 60 characters.", adj.Description + " is greater than 60 characters.");
            }

            if (adj.DFLP && !isDflpEnabled)
            {
                error = "Funding Folder permission required to edit DFLP fields";
                throw new CBaseException(error, error);
            }
        }
    }           
}