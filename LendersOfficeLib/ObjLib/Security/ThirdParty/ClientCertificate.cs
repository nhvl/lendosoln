﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a third party client certificate.
    /// </summary>
    public sealed class ClientCertificate
    {
        /// <summary>
        /// Gets or sets the unique identifier for the certificate.
        /// </summary>
        /// <value>Unique identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the password for the private key of the certificate.
        /// </summary>
        /// <value>The certificate password.</value>
        public Sensitive<string> Password { get; set; }

        /// <summary>
        /// Gets or sets the user provided description for the certificate.
        /// </summary>
        /// <value>The description of the certificate.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the local file path to the certificate.
        /// </summary>
        /// <value>The path to the certificate file.</value>
        public LocalFilePath Path { get; set; }

        /// <summary>
        /// Gets or sets the certificate's subject as specified by the vendor.
        /// </summary>
        /// <value>The vendor specified subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the certificate's issuer as specified by the vendor.
        /// </summary>
        /// <value>The vendor specified issuer.</value>
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets the certificate's expiration date as specified by the vendor.
        /// </summary>
        /// <value>The vendor specified expiration date.</value>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Gets or sets the certificate's start date as specified by the vendor.
        /// </summary>
        /// <value>The vendor specified start date.</value>
        public DateTime ValidOn { get; set; }

        /// <summary>
        /// Gets or sets the date in which the certificate was uploaded to LQB.
        /// </summary>
        /// <value>The creation date.</value>
        public DateTime CreationDate { get; set; }
    }
}
