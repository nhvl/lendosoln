﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    /// <summary>
    /// Represents a link between a client certificate and the URL it applies to.
    /// </summary>
    public sealed class ClientCertificateUrlAssociation
    {
        /// <summary>
        /// Gets or sets the unique identifier for the association.
        /// </summary>
        /// <value>The id of the association.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the URL for the given association.
        /// </summary>
        /// <value>The URL for the association.</value>
        public string Url { get; set; }
    }
}
