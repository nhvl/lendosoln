﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    using System.Collections.Generic;

    /// <summary>
    /// An object holding all the data needed to display a registered client certificate record.
    /// </summary>
    public sealed class ClientCertificateView
    {
        /// <summary>
        /// Gets or sets the certificate details.
        /// </summary>
        /// <value>The details for the given certificate.</value>
        public ClientCertificate Certificate { get; set; }

        /// <summary>
        /// Gets or sets a set of URL's that are associated with the certificate.
        /// </summary>
        /// <value>A set of URLs for the given certificate.</value>
        public IEnumerable<ClientCertificateUrlAssociation> Url { get; set; }
    }
}
