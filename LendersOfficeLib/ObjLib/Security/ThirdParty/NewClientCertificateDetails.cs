﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The details needed to register a new client certificate with the system.
    /// </summary>
    public sealed class NewClientCertificateDetails
    {
        /// <summary>
        /// Gets or sets the path where the certificate file is located.
        /// </summary>
        /// <value>The path where the file is located.</value>
        public LocalFilePath Path { get; set; }

        /// <summary>
        /// Gets or sets the password for the certificate.
        /// </summary>
        /// <value>The password for the given certificate.</value>
        public Sensitive<string> Password { get; set; }

        /// <summary>
        /// Gets or sets the user specified description for the certificate.
        /// </summary>
        /// <value>The user friendly description for the certificate.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the date when the certificate was registered with the system.
        /// </summary>
        /// <value>The certificate upload date.</value>
        public DateTime CreationDate { get; set; }
    }
}
