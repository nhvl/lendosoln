﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    using System;

    /// <summary>
    /// A POCO containing client certificate details.
    /// </summary>
    public sealed class ClientCertificateInspectionResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the certificate has a private key.
        /// </summary>
        /// <value>True if the certificate has a private key.</value>
        public bool HasPrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the date the certificate expires on.
        /// </summary>
        /// <value>The date where the certificate will no longer be valid.</value>
        public DateTime NotAfter { get; set; }
        
        /// <summary>
        /// Gets or sets the date the certificate is valid on.
        /// </summary>
        /// <value>The certificate is valid on this date.</value>
        public DateTime NotBefore { get; set; }

        /// <summary>
        /// Gets or sets the issuer of the certificate as specified in the certificate.
        /// </summary>
        /// <value>The name of th entity who issues the certificate.</value>
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets the subject of the certificate as specified by the issuing entity.
        /// </summary>
        /// <value>The subject as specified on the client certificate.</value>
        public string Subject { get; set; }
    }
}
