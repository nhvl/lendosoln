﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Net;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The data layer for third party client certificates.
    /// </summary>
    public class ThirdPartyClientCertificateManager
    {
        /// <summary>
        /// Inspects a web request, checks to see if there is a client certificate registered and appends it to the web request.
        /// </summary>
        /// <param name="request">The request to inspect.</param>
        public static void AppendCertificate(HttpWebRequest request)
        {
            var manager = new ThirdPartyClientCertificateManager();
            var cert = manager.GetCertificate(request.RequestUri.GetLeftPart(UriPartial.Path));

            if (cert == null)
            {
                return;
            }

            X509Certificate2Collection certificates = new X509Certificate2Collection();
            certificates.Import(cert.Path.Value, cert.Password.Value, X509KeyStorageFlags.PersistKeySet);
            request.ClientCertificates.Clear();
            request.ClientCertificates = certificates;
        }

        /// <summary>
        /// Retrieves the proper certificate(s) needed for connection to the given URI.
        /// </summary>
        /// <param name="uri">The URI to map to a third-party certificate.</param>
        /// <returns>If a certificate was found, a certificate collection containing that cert. If no certs found, returns null.</returns>
        public static X509Certificate2Collection RetrieveCertificate(LqbAbsoluteUri uri)
        {
            var manager = new ThirdPartyClientCertificateManager();
            var cert = manager.GetCertificate(uri.GetUri().GetLeftPart(UriPartial.Path));

            if (cert == null)
            {
                return null;
            }

            X509Certificate2Collection certificates = new X509Certificate2Collection();
            certificates.Import(cert.Path.Value, cert.Password.Value, X509KeyStorageFlags.PersistKeySet);
            return certificates;
        }

        /// <summary>
        /// Gets a certificate listing view.
        /// </summary>
        /// <returns>A list of certificate view containing all the needed data.</returns>
        public IEnumerable<ClientCertificateView> GetCertificateView()
        {
            List<ClientCertificateView> entries = new List<ClientCertificateView>();
            var urls = new Dictionary<int, List<ClientCertificateUrlAssociation>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "THIRD_PARTY_CERTIFICATE_Retrieve"))
            {
                while (reader.Read())
                {
                    int certId = (int)reader["ThirdPartyCertificateId"];
                    List<ClientCertificateUrlAssociation> associations;  

                    if (!urls.TryGetValue(certId, out associations))
                    {
                        associations = new List<ClientCertificateUrlAssociation>();
                        urls.Add(certId, associations);
                    }

                    var urlAssociation = new ClientCertificateUrlAssociation();
                    urlAssociation.Url = reader["ApplicableUrl"].ToString();
                    urlAssociation.Id = (int)reader["Id"];
                    associations.Add(urlAssociation);
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        ClientCertificateView view = new ClientCertificateView();
                        view.Certificate = new ClientCertificate();
                        view.Certificate.Id = (int)reader["Id"];

                        List<ClientCertificateUrlAssociation> associations;

                        if (!urls.TryGetValue(view.Certificate.Id, out associations))
                        {
                            associations = new List<ClientCertificateUrlAssociation>();
                        }
                        
                        view.Url = associations;
                        view.Certificate.CreationDate = (DateTime)reader["CreatedDate"];
                        view.Certificate.Description = (string)reader["Description"];
                        view.Certificate.Issuer = (string)reader["Issuer"];
                        view.Certificate.Subject = (string)reader["Subject"];
                        view.Certificate.ValidOn = (DateTime)reader["ValidOn"];
                        view.Certificate.ExpiresOn = (DateTime)reader["ExpiresOn"];

                        entries.Add(view);
                    }
                }
            }

            return entries;
        }

        /// <summary>
        /// Attempts to add a new certificate to the system, returns a status code with the result.
        /// </summary>
        /// <param name="newCertificate">The certificate uploaded by the user.</param>
        /// <returns>OK if the addition succeeding otherwise an error code.</returns>
        public CertificateAdditionResult AddNewCertificate(NewClientCertificateDetails newCertificate)
        {
            ClientCertificateInspectionResult details;

            CertificateAdditionResult result = this.InspectCertificate(newCertificate, out details);
            if (result != CertificateAdditionResult.OK)
            {
                return result;
            }

            if (newCertificate.Description == null)
            {
                return CertificateAdditionResult.DescriptionCannotBeEmpty;
            }

            if (newCertificate.Description.Length > 300)
            {
                return CertificateAdditionResult.DescriptionTooLong;
            }

            if (!details.HasPrivateKey)
            {
                return CertificateAdditionResult.DoesNotContainPrivateKey;
            }

            string savedKey = null;

            try
            {
                Guid typedKey = Guid.NewGuid();
                string key = typedKey.ToString();
                FileDBTools.WriteFile(E_FileDB.Normal, key, newCertificate.Path.Value);
                savedKey = key;

                byte[] password = new byte[0];
                var encryptionId = EncryptionKeyIdentifier.ClientCertificatePassword;
                if (newCertificate.Password.Value.Length > 0)
                {
                    password = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(encryptionId, newCertificate.Password.Value);
                }

                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@Description", newCertificate.Description),
                    new SqlParameter("@FileDBKey", typedKey),
                    new SqlParameter("@EncryptionKeyId", EncryptionKeyIdentifier.ClientCertificatePassword.ToString()),
                    new SqlParameter("@CreatedDate", DateTime.Now),
                    new SqlParameter("@EncryptedPassword", password),
                    new SqlParameter("@ValidOn", details.NotBefore),
                    new SqlParameter("@ExpiresOn", details.NotAfter),
                    new SqlParameter("@Issuer", details.Issuer),
                    new SqlParameter("@Subject", details.Subject),
                };

                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "THIRD_PARTY_CERTIFICATE_Insert", 1, parameters);

                return CertificateAdditionResult.OK;
            }
            catch (Exception e)
            {
                if (!string.IsNullOrEmpty(savedKey))
                {
                    FileDBTools.Delete(E_FileDB.Normal, savedKey);
                }

                DataAccess.Tools.LogError(e);
                throw;
            }
        }

        /// <summary>
        /// Associates the given URL with the specified registered certificate.
        /// </summary>
        /// <param name="url">The URL to associate.</param>
        /// <param name="thirdPartyCertificateId">The certificate to associate it with.</param>
        public void AssociateUrl(string url, int thirdPartyCertificateId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@ThirdPartyCertificateId", thirdPartyCertificateId),
                new SqlParameter("@ApplicableUrl", url),
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "THIRD_PARTY_CERTIFICATE_URL_Add", 1, parameters);
        }

        /// <summary>
        /// Deletes the association with the given id.
        /// </summary>
        /// <param name="id">The id of the association to delete.</param>
        public void DeleteAssociatedUrl(int id)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "THIRD_PARTY_CERTIFICATE_URL_Delete", 1, new SqlParameter("@Id", id));
        }

        /// <summary>
        /// Looks for a certificate with the given URL.
        /// </summary>
        /// <param name="url">The URL to search for.</param>
        /// <returns>A certificate that has been associated with the given URL or null.</returns>
        private ClientCertificate GetCertificate(string url)
        {
            ClientCertificate cert = null;
            Guid encryptionKeyIdData = Guid.Empty;
            byte[] encryptedBytes = null;
            Guid fileDbKey = Guid.Empty;

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "THIRD_PARTY_CERTIFICATE_URL_Retrieve", new SqlParameter("@Url", url)))
            {
                if (reader.Read())
                {
                    cert = new ClientCertificate();
                    cert.Id = (int)reader["Id"];
                    cert.CreationDate = (DateTime)reader["CreatedDate"];
                    cert.Description = (string)reader["Description"];
                    cert.Issuer = (string)reader["Issuer"];
                    cert.Subject = (string)reader["Subject"];
                    cert.ValidOn = (DateTime)reader["ValidOn"];
                    cert.ExpiresOn = (DateTime)reader["ExpiresOn"];

                    encryptionKeyIdData = (Guid)reader["EncryptionKeyId"];
                    fileDbKey = (Guid)reader["FileDBKey"];
                    encryptedBytes = (byte[])reader["EncryptedPassword"];
                }
            }

            if (cert != null)
            {
                string file = FileDBTools.CreateCopy(E_FileDB.Normal, fileDbKey.ToString());
                var encryptionKeyId = EncryptionKeyIdentifier.Create(encryptionKeyIdData.ToString());

                cert.Path = LocalFilePath.Create(file).Value;

                cert.Password = Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId.Value, encryptedBytes);
            }

            return cert;
        }

        /// <summary>
        /// Inspects the certificate specified by the certificate details passed in. Return a status code if the certificate cannot be opened. The details
        /// parameter is also set with information from the open certificate.
        /// </summary>
        /// <param name="cert">The user uploaded certificate and the password.</param>
        /// <param name="details">The details as read from the certificate.</param>
        /// <returns>OK if the certificate was inspected otherwise one of many status code specifying what went wrong.</returns>
        private CertificateAdditionResult InspectCertificate(NewClientCertificateDetails cert, out ClientCertificateInspectionResult details)
        {
            details = new ClientCertificateInspectionResult();
            try
            {
                var certificate = new X509Certificate2(cert.Path.Value, cert.Password.Value);
                details.HasPrivateKey = certificate.HasPrivateKey;
                details.NotAfter = certificate.NotAfter;
                details.NotBefore = certificate.NotBefore;
                details.Issuer = certificate.Issuer;
                details.Subject = certificate.Subject;
            }
            catch (CryptographicException e)
            {
                if (e.HResult == -2146885623)
                {
                    return CertificateAdditionResult.InvalidCertificate;
                }

                if (e.HResult == -2147024810)
                {
                    return CertificateAdditionResult.InvalidPassword;
                }

                Tools.LogError(e);

                return CertificateAdditionResult.Error;
            }

            return CertificateAdditionResult.OK;
        }
    }
}
