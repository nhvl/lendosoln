﻿namespace LendersOffice.ObjLib.Security.ThirdParty
{
    /// <summary>
    /// Enumeration for insertion attempts for new certificates.
    /// </summary>
    public enum CertificateAdditionResult
    {
        /// <summary>
        /// Success response.
        /// </summary>
        OK = 0,

        /// <summary>
        /// Failed to open the certificate with the given password.
        /// </summary>
        InvalidPassword = 1,

        /// <summary>
        /// There was no description specified for the certificate.
        /// </summary>
        DescriptionCannotBeEmpty = 2,

        /// <summary>
        /// The provided certificate is not a x509 certificate.
        /// </summary>
        InvalidCertificate = 3,

        /// <summary>
        /// The description is over the alloted size.
        /// </summary>
        DescriptionTooLong = 4,

        /// <summary>
        /// The X509 certificate does not contain a private key.
        /// </summary>
        DoesNotContainPrivateKey = 5,

        /// <summary>
        /// Unexpected error.
        /// </summary>
        Error = 6
    }
}
