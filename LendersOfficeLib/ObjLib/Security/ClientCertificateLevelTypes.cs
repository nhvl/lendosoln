﻿namespace LendersOffice.ObjLib.Security
{
    /// <summary>
    /// Enum for the client certificate levels.
    /// </summary>
    public enum ClientCertificateLevelTypes
    {
        /// <summary>
        /// Only the specified user can use this certificate.
        /// </summary>
        Individual = 0,

        /// <summary>
        /// Users belong to specified Employee groups can use this certificate.
        /// </summary>
        EmployeeGroup = 1,

        /// <summary>
        /// Users belong to specified branch groups can use this certificate.
        /// </summary>
        BranchGroup = 2,

        /// <summary>
        /// Users belong to specified OC groups can use this certificate.
        /// </summary>
        OcGroup = 3,

        /// <summary>
        /// Corporate level.
        /// </summary>
        Corporate = 4
    }
}
