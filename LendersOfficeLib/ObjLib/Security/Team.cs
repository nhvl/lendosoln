﻿// <copyright file="Team.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Matthew Flynn
//    Date:   06/02/2014
// </summary>

namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Class represents a Team.
    /// </summary>
    public class Team
    {
        /// <summary>
        /// True if the team has not been committed to the database.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// BrokerId of this team.
        /// </summary>
        private Guid brokerId = Guid.Empty;

        /// <summary>
        /// The name of the team.
        /// </summary>
        private string name;
        
        /// <summary>
        /// If the team name was set.
        /// </summary>
        private bool nameSet;

        /// <summary>
        /// Team notes.
        /// </summary>
        private string notes;
        
        /// <summary>
        /// If the team notes where set.
        /// </summary>
        private bool notesSet;

        /// <summary>
        /// RoleId of the team.
        /// </summary>
        private Guid roleId;

        /// <summary>
        /// If the team role was set.
        /// </summary>
        private bool roleIdSet;

        /// <summary>
        /// Prevents a default instance of the <see cref="Team" /> class from being created.
        /// </summary>
        private Team()
        {
            this.brokerId = GetBrokerId();
        }

        /// <summary>
        /// Gets the Id of the team.
        /// </summary>
        /// <value> The Id of the team.</value>
        public Guid Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the Name of the team.
        /// </summary>
        /// <value> The Name of the team.</value>
        public string Name
        {
            get 
            { 
                return this.name; 
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.nameSet = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the RoleId of the team.
        /// </summary>
        /// <value> The RoleId of the team.</value>
        public Guid RoleId
        {
            get 
            { 
                return this.roleId; 
            }

            set
            {
                if (this.roleId != value)
                {
                    this.roleId = value;
                    this.roleIdSet = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Notes of the team.
        /// </summary>
        /// <value> The Notes of the team.</value>
        public string Notes
        {
            get 
            {
                return this.notes; 
            }

            set
            {
                if (this.notes != value)
                {
                    this.notes = value;
                    this.notesSet = true;
                }
            }
        }

        /// <summary>
        /// Create a new Team.
        /// </summary>
        /// <param name="name">Name of team to be created.</param>
        /// <param name="roleId">Team role id.</param>
        /// <param name="notes">Team notes.</param>
        /// <returns>Instance of new team.</returns>
        public static Team CreateTeam(string name, Guid roleId, string notes)
        {
            Team newTeam = new Team();
            newTeam.isNew = true;
            newTeam.name = name;
            newTeam.roleId = roleId;
            newTeam.notes = notes;
            return newTeam;
        }

        /// <summary>
        /// Load an existing team from the Database.
        /// </summary>
        /// <param name="id"> The id of the team to be loaded.</param>
        /// <returns> Team instance from the database.</returns>
        public static Team LoadTeam(Guid id)
        {
            Team team = new Team();
            team.Id = id;

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", team.brokerId),
                new SqlParameter("@TeamId", id)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(team.brokerId, "TEAM_RetrieveTeam", parameters))
            {
                if (reader.Read())
                {
                    team.name = reader["Name"].ToString();
                    team.roleId = (Guid)reader["RoleId"];
                    team.notes = reader["Notes"].ToString();
                }
            }

            team.isNew = false;
            return team;
        }

        /// <summary>
        /// Delete an existing team from the Database.
        /// </summary>
        /// <param name="id">The id of the team to be deleted.</param>
        public static void DeleteTeam(Guid id)
        {
            Guid brokerId = GetBrokerId();
            if (brokerId != Guid.Empty)
            {
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@TeamId", id)
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_DeleteTeam", 2, parameters);
            }
        }

        /// <summary>
        /// Gets a list of all Teams at a lender that match the search criteria.
        /// If criteria is blank or null, returns all Teams at the lender.
        /// </summary>
        /// <param name="searchTerm">Term to look for in the name of the Team.</param>
        /// <returns>List of teams matching the specified criteria.</returns>
        public static IEnumerable<Team> ListTeamsAtLender(string searchTerm)
        {
            return ListTeamsAtLender(GetBrokerId(), searchTerm);
        }

        /// <summary>
        /// Gets a list of all teams at a lender that match the search criteria.
        /// If criteria is blank or null, returns all Teams at the lender.
        /// </summary>
        /// <param name="loanId">The loan id of the loan.</param>
        /// <returns>List of teams matching the specified criteria.</returns>
        public static IEnumerable<Team> ListTeamsOnLoan(Guid loanId)
        {
            return ListTeamsOnLoan(GetBrokerId(), loanId);
        }

        /// <summary>
        /// List the teams associated with a user.
        /// </summary>
        /// <param name="employeeId">Employee id of the user.</param>
        /// <returns>List of teams the user is associated with.</returns>
        public static IEnumerable<Team> ListTeamsByUser(Guid employeeId)
        {
            return ListTeamsByUser(GetBrokerId(), employeeId, null);
        }

        /// <summary>
        /// List the teams associated with a user.
        /// </summary>
        /// <param name="brokerId">The broker id of user.</param>
        /// <param name="employeeId">The employee id of the user.</param>
        /// <returns>List of teams the user is associated with.</returns>
        public static IEnumerable<Team> ListTeamsByUser(Guid brokerId, Guid employeeId)
        {
            return ListTeamsByUser(brokerId, employeeId, null);
        }

        /// <summary>
        /// List the primary teams associated with a user.
        /// </summary>
        /// <param name="employeeId">Employee id of the user.</param>
        /// <param name="roleId">Role Id of the user and team.</param>
        /// <returns>List of teams the user is associated with.</returns>
        public static IEnumerable<Team> ListPrimaryTeamByUserInRole(Guid employeeId, Guid roleId)
        {
            return ListPrimaryTeamByUserInRole(GetBrokerId(), employeeId, roleId);
        }

        /// <summary>
        /// List the primary teams associated with a user.
        /// </summary>
        /// <param name="brokerId">Associated broker id.</param>
        /// <param name="employeeId">Employee Id of the employee.</param>
        /// <param name="roleId">Role Id of the role.</param>
        /// <returns>List of teams associated with the user.</returns>
        public static IEnumerable<Team> ListPrimaryTeamByUserInRole(Guid brokerId, Guid employeeId, Guid? roleId)
        {
            // Make this method public if we ever need to load teams without user context or across lenders.
            // 8/28/2014 dd - Make this method so that broker id can be pass in from the LOAdmin employee editor.
            List<Team> teamList = new List<Team>();

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@EmployeeId", employeeId));

            if (roleId.HasValue)
            {
                parameters.Add(new SqlParameter("@RoleId", roleId.Value));
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(
                brokerId,
                "TEAM_ListTeamsByUser",
                parameters))
            {
                while (reader.Read())
                {
                    if ((bool)reader["IsPrimary"])
                    {
                        teamList.Add(new Team()
                        {
                            Id = (Guid)reader["Id"],
                            Name = reader["Name"].ToString(),
                            Notes = reader["Notes"].ToString(),
                            RoleId = (Guid)reader["RoleId"],
                        });
                    }
                }
            }

            return teamList;
        }

        /// <summary>
        /// List the teams of a particular role associated with the user.
        /// </summary>
        /// <param name="employeeId">Employee Id of the user.</param>
        /// <param name="roleId">RoleId of the role.</param>
        /// <returns>List of teams in a the role that the employee is associated with.</returns>
        public static IEnumerable<Team> ListTeamsByUserInRole(Guid employeeId, Guid roleId)
        {
            return ListTeamsByUser(GetBrokerId(), employeeId, roleId);
        }

        /// <summary>
        /// List the teams of a particular role associated with the user.
        /// </summary>
        /// <param name="brokerId">The broker id of the user.</param>
        /// <param name="employeeId">Employee Id of the user.</param>
        /// <param name="roleId">RoleId of the role.</param>
        /// <returns>List of teams in a the role that the employee is associated with.</returns>
        public static IEnumerable<Team> ListTeamsByUserInRole(Guid brokerId, Guid employeeId, Guid roleId)
        {
            return ListTeamsByUser(brokerId, employeeId, roleId);
        }

        /// <summary>
        /// List the users in a team.
        /// </summary>
        /// <param name="teamId">Team id of the team.</param>
        /// <returns>List of users in the team.</returns>
        public static IEnumerable<TeamUser> ListUsersInTeam(Guid teamId)
        {
            return ListUsersInTeam(GetBrokerId(), teamId);
        }

        /// <summary>
        /// Add a user assignment to a team.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="employeeId">Employee id of the user.</param>
        /// <param name="teamId">Team to assign.</param>
        /// <param name="primary">If this will be the primary for the user.</param>
        public static void SetUserAssignment(Guid brokerId, Guid employeeId, Guid teamId, bool primary)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@TeamId", teamId),
                new SqlParameter("@EmployeeId", employeeId),
                new SqlParameter("@IsPrimary", primary)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_SetUserAssignment", 2, parameters);
        }

        /// <summary>
        /// Update a user assignment to a team.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="employeeId">Employee id of the user.</param>
        /// <param name="teamId">Team to assign.</param>
        /// <param name="primary">If this will be the primary for the user.</param>
        public static void UpdateUserAssignment(Guid brokerId, Guid employeeId, Guid teamId, bool primary)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@TeamId", teamId),
                new SqlParameter("@EmployeeId", employeeId),
                new SqlParameter("@IsPrimary", primary)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_UpdateUserAssignment", 2, parameters);
        }

        /// <summary>
        /// Remove a user assignment from a team.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="employeeId">Employee id of the user.</param>
        /// <param name="teamId">Team to remove assignment from.</param>
        public static void RemoveUserAssignment(Guid brokerId, Guid employeeId, Guid teamId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@TeamId", teamId),
                new SqlParameter("@EmployeeId", employeeId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_RemoveUserAssignment", 2, parameters);
        }

        /// <summary>
        /// Add or update a team assignment to a loan.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="teamId">Team Id of the team.</param>
        /// <param name="loanId">Loan Id of the loan.</param>
        public static void SetLoanAssignment(Guid brokerId, Guid teamId, Guid loanId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@TeamId", teamId),
                new SqlParameter("@LoanId", loanId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_SetLoanAssignment", 2, parameters);
        }

        /// <summary>
        /// Remove a team assignment from a loan.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="teamId">Team Id of the team.</param>
        /// <param name="loanId">Loan Id of the loan.</param>
        public static void RemoveLoanAssignment(Guid brokerId, Guid teamId, Guid loanId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@TeamId", teamId),
                new SqlParameter("@LoanId", loanId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TEAM_RemoveLoanAssignment", 2, parameters);
        }

        /// <summary>
        /// Save the team data to the database.
        /// </summary>
        public void Save()
        {
            if (this.brokerId == Guid.Empty)
            {
                throw new CBaseException(ErrorMessages.Generic, "No broker specified.");
            }

            if (this.isNew)
            {
                SqlParameter idparam = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                idparam.Direction = ParameterDirection.Output;

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(idparam);
                parameters.Add(new SqlParameter("@BrokerId", this.brokerId));
                parameters.Add(new SqlParameter("@RoleId", this.roleId));
                parameters.Add(new SqlParameter("@Name", this.name));
                parameters.Add(new SqlParameter("@Notes", this.notes));

                StoredProcedureHelper.ExecuteNonQuery(
                    this.brokerId, 
                    "TEAM_CreateTeam",
                     2,
                     parameters);

                this.isNew = false;

                this.Id = (Guid)idparam.Value;
            }
            else
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (this.roleIdSet)
                {
                    parameters.Add(new SqlParameter("@RoleId", this.roleId));
                }

                if (this.nameSet)
                {
                    parameters.Add(new SqlParameter("@Name", this.name));
                }

                if (this.notesSet)
                {
                    parameters.Add(new SqlParameter("@Notes", this.notes));
                }

                if (parameters.Count == 0)
                {
                    return; // Nothing to update.
                }

                parameters.Add(new SqlParameter("@Id", this.Id));
                parameters.Add(new SqlParameter("@BrokerId", this.brokerId));

                StoredProcedureHelper.ExecuteNonQuery(
                    this.brokerId, 
                    "TEAM_UpdateTeam",
                    2,
                    parameters);
            }
        }

        /// <summary>
        /// Gets a list of all Teams at a lender that match the search criteria.
        /// If criteria is blank or null, returns all Teams at the lender.
        /// </summary>
        /// <param name="brokerId">BrokerId of the lender.</param>
        /// <param name="searchTerm">Term to look for in the name of the Team.</param>
        /// <returns>List of teams matching the specified criteria.</returns>
        private static IEnumerable<Team> ListTeamsAtLender(Guid brokerId, string searchTerm)
        {
            // Make this method public if we ever need to load teams without user context or across lenders.
            List<Team> teamList = new List<Team>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@NameFilter", string.IsNullOrEmpty(searchTerm) ? string.Empty : searchTerm)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TEAM_ListTeams", parameters))
            {
                while (reader.Read())
                {
                    teamList.Add(new Team()
                    {
                        Id = (Guid)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Notes = reader["Notes"].ToString(),
                        RoleId = (Guid)reader["RoleId"],
                    });
                }
            }

            return teamList;
        }

        /// <summary>
        /// Gets a list of all Teams at a lender that on on the loan.
        /// </summary>
        /// <param name="brokerId">BrokerId of the lender.</param>
        /// <param name="loanId">Id of the loan.</param>
        /// <returns>List of teams matching the specified criteria.</returns>
        private static IEnumerable<Team> ListTeamsOnLoan(Guid brokerId, Guid loanId)
        {
            // Make this method public if we ever need to load teams without user context or across lenders.
            List<Team> teamList = new List<Team>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TEAM_ListTeamsByLoan", parameters))
            {
                while (reader.Read())
                {
                    teamList.Add(new Team()
                    {
                        Id = (Guid)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Notes = reader["Notes"].ToString(),
                        RoleId = (Guid)reader["RoleId"],
                    });
                }
            }

            return teamList;
        }

        /// <summary>
        /// List the teams associated with a user.
        /// </summary>
        /// <param name="brokerId">Associated broker id.</param>
        /// <param name="employeeId">Employee Id of the employee.</param>
        /// <param name="roleId">Role Id of the role.</param>
        /// <returns>List of teams associated with the user.</returns>
        private static IEnumerable<Team> ListTeamsByUser(Guid brokerId, Guid employeeId, Guid? roleId)
        {
            // Make this method public if we ever need to load teams without user context or across lenders.
            List<Team> teamList = new List<Team>();

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@EmployeeId", employeeId));

            if (roleId.HasValue)
            {
                parameters.Add(new SqlParameter("@RoleId", roleId.Value));
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(
                brokerId,
                "TEAM_ListTeamsByUser",
                parameters))
            {
                while (reader.Read())
                {
                    teamList.Add(new Team()
                    {
                        Id = (Guid)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Notes = reader["Notes"].ToString(),
                        RoleId = (Guid)reader["RoleId"],
                    });
                }
            }

            return teamList;
        }

        /// <summary>
        /// Lists users in a team.
        /// </summary>
        /// <param name="brokerId">Broker id of team.</param>
        /// <param name="teamId">Team Id of the team.</param>
        /// <returns>List of users on the team.</returns>
        private static IEnumerable<TeamUser> ListUsersInTeam(Guid brokerId, Guid teamId)
        {
            // Make this method public if we ever need to load teams without user context or across lenders.
            List<TeamUser> userList = new List<TeamUser>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@TeamId", teamId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TEAM_ListUsersByTeam", parameters))
            {
                while (reader.Read())
                {
                    userList.Add(new TeamUser()
                    {
                        EmployeeId = (Guid)reader["EmployeeId"],
                        FirstName = reader["UserFirstNm"].ToString(),
                        LastName = reader["UserLastNm"].ToString(),
                        IsInRole = true,
                        IsPrimary = true
                    });
                }
            }

            return userList;            
        }

        /// <summary>
        /// Get the broker id from the principal.  Default way to get it if we assume the
        /// a user context exists and it is at the same broker as the team.
        /// </summary>
        /// <returns>The broker id of the principal or empty id if no principal.</returns>
        private static Guid GetBrokerId()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            return principal != null ? principal.BrokerId : Guid.Empty;
        }

        /// <summary>
        /// POD class representing a user assigned to a team.
        /// </summary>
        public class TeamUser
        {
            /// <summary>
            /// Gets or sets Employee Id.
            /// </summary>
            /// <value>The employee id of the user.</value>
            public Guid EmployeeId { get; set; }

            /// <summary>
            /// Gets or sets the first name of the user.
            /// </summary>
            /// <value>The first name of the user.</value>
            public string FirstName { get; set; }

            /// <summary>
            /// Gets or sets the last name of the user.
            /// </summary>
            /// <value>The last name of the user.</value>
            public string LastName { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the employee has the role of the team.
            /// </summary>
            /// <value>True if the user currently holds the role of the team.  Otherwise false.</value>
            public bool IsInRole { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the team assignment is the primary one for this employee in this role.
            /// </summary>
            /// <value>True if the the team is the user's primary for this role.</value>
            public bool IsPrimary { get; set; }
        }
    }
}
