﻿namespace LendersOffice.ObjLib.Security.Authorization
{
    /// <summary>
    /// A general operation type on a resource tend to be CRUD. <para></para>
    /// However, somethings don't fit that model easily.  <para></para>
    /// For instance <see cref="ConversationLog.ConvoLogOperationType"/>, where the Hide operation type would not easily fit CRUD afaik.
    /// </summary>
    public enum ResourceOperationType
    {
        /// <summary>
        /// You want to create something.
        /// </summary>
        Create = 0,

        /// <summary>
        /// You want to read something.
        /// </summary>
        Read = 1,

        /// <summary>
        /// You want to update something.
        /// </summary>
        Update = 2,

        /// <summary>
        /// You want to delete something.
        /// </summary>
        Delete = 3
    }
}
