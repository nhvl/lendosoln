﻿namespace LendersOffice.ObjLib.Security.Authorization.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using LqbGrammar.DataTypes;    

    /// <summary>
    /// Decides if a user should or should not have access to a particular operation on a permission level.
    /// </summary>
    public class PermissionLevelAuthorizer : IAuthorizer<long?, ConvoLogOperationType>
    {
        /// <summary>
        /// The only instance of the PermissionLevelAuthorizer, for easy reference.
        /// </summary>
        public static readonly PermissionLevelAuthorizer Instance = new PermissionLevelAuthorizer();

        /// <summary>
        /// Prevents a default instance of the <see cref="PermissionLevelAuthorizer"/> class from being created.<para></para>        
        /// The above is required text from stylecop.  This constructor is called once to create the only instance.
        /// </summary>
        private PermissionLevelAuthorizer()
        {
        }
        
        /// <summary>
        /// This may hit the db more than just passing in the loaded permissionLevel. <para></para>
        /// Returns true if the feature is not enabled, preserving existing behavior.
        /// </summary>
        /// <param name="token">The security token associated with the person requesting access.</param>
        /// <param name="permissionLevelId">The id of the permission level we are checking permission for.  If it is null, it will be assumed to be the broker Default.</param>
        /// <param name="permissionType">The type of operation they are requesting access to for the specified permission level.</param>
        /// <returns>True iff the user has access to the operation for the specified permission level.</returns>
        public bool CanRun(SecurityToken token, long? permissionLevelId, ConvoLogOperationType permissionType)
        {
            if (!ConstStage.EnableConversationLogPermissions)
            {
                return true;
            }

            if (token == null)
            {
                return false;
            }

            if (token.IsValid() == false)
            {
                return false;
            }

            // the roleids and employee groupids should only be null for old security token versions, grant access during the transitional period.
            if (token.RoleIds == null || token.EmployeeGroupIds == null)
            {
                return true;
            }

            ConversationLogPermissionLevel permissionLevel;

            // todo: consider putting all this in a ConverationLogPermissionLevel.GetPermissionLevelByNullableId(.., ...);
            if (permissionLevelId.HasValue)
            {
                permissionLevel = ConversationLogPermissionLevel.GetPermissionLevelById(token.BrokerId, permissionLevelId.Value);
            }
            else
            {
                // if(!permissionLevelId.HasValue)
                permissionLevel = ConversationLogPermissionLevel.CreateUnsavedDefaultForBroker(token.BrokerId.Value);                
            }

            return this.CanRun(token, permissionLevel, permissionType);
        }

        /// <summary>
        /// This is more efficient than the version that takes only an id, as it hits the db less.  It will throw if the supplied permission level is null.
        /// Returns true if the feature is not enabled, preserving existing behavior.
        /// </summary>
        /// <param name="token">The security token associated with the person requesting access.</param>
        /// <param name="permissionLevel">The permission level we are checking permission for.  If this is null, the method will throw.</param>
        /// <param name="permissionType">The type of operation they are requesting access to for the specified permission level.</param>
        /// <returns>True iff the user has access to the operation for the specified permission level.</returns>
        public bool CanRun(SecurityToken token, ConversationLogPermissionLevel permissionLevel, ConvoLogOperationType permissionType)
        {
            if (!ConstStage.EnableConversationLogPermissions)
            {
                return true;
            }

            if (token == null)
            {
                return false;
            }

            if (token.IsValid() == false)
            {
                return false;
            }

            // the roleids and employee groupids should only be null for old security token versions, grant access during the transitional period.
            if (token.RoleIds == null || token.EmployeeGroupIds == null)
            {
                return true;
            }

            if (permissionLevel == null)
            {
                throw new ArgumentNullException(nameof(permissionLevel));
            }

            if (permissionType == ConvoLogOperationType.Post)
            {
                if (!permissionLevel.IsActive)
                {
                    return false;
                }
            }

            var roleIdsAllowedToRunOperation = new HashSet<Guid>(permissionLevel.GetRoleIdsForOperationType(permissionType));

            foreach (var roleId in token.RoleIds)
            {
                if (roleIdsAllowedToRunOperation.Contains(roleId.Value))
                {
                    return true;
                }
            }

            var employeeGroupIdsAllowedToRunOperation = new HashSet<Guid>(permissionLevel.GetGroupIdsForOperationType(permissionType));
            foreach (var employeeGroupId in token.EmployeeGroupIds)
            {
                if (employeeGroupIdsAllowedToRunOperation.Contains(employeeGroupId.GroupId))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
