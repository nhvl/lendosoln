﻿namespace LendersOffice.ObjLib.Security.Authorization.ConversationLog
{
    /// <summary>
    /// Gives the operation types associated with a conversation log permission level, which is set for a conversation.
    /// </summary>
    public enum ConvoLogOperationType
    {
        /// <summary>
        /// Allows posting a new comment, that is, starting a new conversation.
        /// </summary>
        Post = 0,
        
        /// <summary>
        /// Allows viewing comments within the conversation.
        /// </summary>
        View = 1,

        /// <summary>
        /// Allows replying to comments within a conversation.
        /// </summary>
        Reply = 2,
        
        /// <summary>
        /// Not yet implemented, but will eventually allow users to hide individual comments and also view comments that have been hidden.
        /// </summary>
        Hide = 3
    }
}
