﻿namespace LendersOffice.ObjLib.Security.Authorization.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using Admin;
    using DataAccess;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    
    /// <summary>
    /// This partial contains the private class that manages the associated roles and groups for each operation.
    /// </summary>
    public partial class ConversationLogPermissionLevel
    {
        /// <summary>
        /// This class helps the permission level class make sense of all the enabled ids for roles/groups and each operation type.
        /// </summary>
        private class ConversationLogPermissionLevelAssociationsRepository
        {
            /// <summary>
            /// The name of the stored procedure that sets the enabled role ids for a permission level and operation type.
            /// </summary>
            private static readonly StoredProcedureName SpSetRoles = StoredProcedureName.Create("SetRolesPermittedForPermissionLevel").Value;

            /// <summary>
            /// The name of the stored procedure that sets the enabled group ids for a permission level and operation type.
            /// </summary>>
            private static readonly StoredProcedureName SpSetGroups = StoredProcedureName.Create("SetGroupsPermittedForPermissionLevel").Value;

            /// <summary>
            /// The name of the stored procedure that gets the enabled role ids for a permission level and operation type.
            /// </summary>
            private static readonly StoredProcedureName SpGetRoles = StoredProcedureName.Create("GetRolesPermittedForPermissionLevelAndType").Value;

            /// <summary>
            /// The name of the stored procedure that gets the enabled group ids for a permission level and operation type.
            /// </summary>
            private static readonly StoredProcedureName SpGetGroups = StoredProcedureName.Create("GetGroupsPermittedForPermissionLevelAndType").Value;

            /// <summary>
            /// The types of operations that require having at least view access to be enabled.
            /// </summary>
            private static readonly HashSet<ConvoLogOperationType> OperationTypesThatRequireView = new HashSet<ConvoLogOperationType>(new ConvoLogOperationType[]
            {
                ConvoLogOperationType.Hide,
                ConvoLogOperationType.Post,
                ConvoLogOperationType.Reply
            });

            /// <summary>
            /// Gets the enabled role ids for the permission level for the specified operation.
            /// </summary>
            /// <param name="brokerId">The id of the broker that the permission level belongs to..</param>
            /// <param name="permissionLevelId">The id of the permission level itself.</param>
            /// <param name="permissionType">The type of operation of interest.</param>
            /// <returns>The ids of the enabled roles for the operation for the permission level.</returns>
            public static IEnumerable<Guid> GetRoleIdsForPermissionLevel(BrokerIdentifier brokerId, long permissionLevelId, ConvoLogOperationType permissionType)
            {
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", brokerId.Value),
                    new SqlParameter("@PermissionLevelId", permissionLevelId),
                    new SqlParameter("@PermissionType", (int)permissionType)
                };

                var roleIds = new List<Guid>();
                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId.Value, SpGetRoles.ToString(), parameters))
                {
                    while (reader.Read())
                    {
                        roleIds.Add((Guid)reader["roleId"]);
                    }
                }

                return roleIds;
            }

            /// <summary>
            /// Gets the enabled group ids for the permission level for the specified operation.
            /// </summary>
            /// <param name="brokerId">The id of the broker that the permission level belongs to..</param>
            /// <param name="permissionLevelId">The id of the permission level itself.</param>
            /// <param name="permissionType">The type of operation of interest.</param>
            /// <returns>The ids of the enabled grpups for the operation for the permission level.</returns>
            public static IEnumerable<Guid> GetGroupIdsForPermissionLevel(BrokerIdentifier brokerId, long permissionLevelId, ConvoLogOperationType permissionType)
            {
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", brokerId.Value),
                    new SqlParameter("@PermissionLevelId", permissionLevelId),
                    new SqlParameter("@PermissionType", (int)permissionType)
                };

                var groupIds = new List<Guid>();
                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId.Value, SpGetGroups.ToString(), parameters))
                {
                    while (reader.Read())
                    {
                        groupIds.Add((Guid)reader["groupId"]);
                    }
                }

                return groupIds;
            }

            /// <summary>
            /// Sets the roles and groups for each operation for the given permission level.  Can throw for poorly formed inputs, or if no access.
            /// </summary>
            /// <param name="conn">The connection this is being saved on.</param>
            /// <param name="tran">The transaction these are being saved in.</param>
            /// <param name="brokerId">The id of the broker this is being saved for.</param>
            /// <param name="permissionLevelId">The id of the permission level these are associated with.</param>
            /// <param name="newRoleIdsByOperation">This should have a non-null hashset of enabled role ids for all operation types, or it will throw.</param>
            /// <param name="newGroupIdsByOperation">This should have a non-null hashset of enabled group ids for all operation types, or it will throw.</param>
            /// <returns>A list of reasons the save could not take place.</returns>
            public static IEnumerable<string> SetForPermissionLevel(
                DbConnection conn,
                DbTransaction tran,
                BrokerIdentifier brokerId,
                long permissionLevelId,
                Dictionary<ConvoLogOperationType, HashSet<Guid>> newRoleIdsByOperation,
                Dictionary<ConvoLogOperationType, HashSet<Guid>> newGroupIdsByOperation)
            {
                if (newRoleIdsByOperation == null)
                {
                    throw new ArgumentNullException(nameof(newRoleIdsByOperation));
                }

                if (newGroupIdsByOperation == null)
                {
                    throw new ArgumentNullException(nameof(newGroupIdsByOperation));
                }

                ThrowIfCantUpdateDatabase();

                var reasonsInvalid = GetReasonsInvalid(brokerId, newRoleIdsByOperation, newGroupIdsByOperation);
                if (reasonsInvalid.Any())
                {
                    return reasonsInvalid;
                }

                foreach (var kvp in newRoleIdsByOperation)
                {
                    var operation = kvp.Key;
                    var roleIds = kvp.Value;
                    SetRoleIdsForPermissionLevel(conn, tran, brokerId, permissionLevelId, operation, roleIds);
                }

                foreach (var kvp in newGroupIdsByOperation)
                {
                    var operation = kvp.Key;
                    var groupIds = kvp.Value;
                    SetGroupIdsForPermissionLevel(conn, tran, brokerId, permissionLevelId, operation, groupIds);
                }

                return reasonsInvalid;
            }

            /// <summary>
            /// Gets the reasons the set of all enabled roles and groups for all operation types should be considered invalid, or empty.
            /// </summary>
            /// <param name="brokerId">The id of the broker of the permission level.</param>
            /// <param name="newRoleIdsByOperation">This should have a non-null hashset of role ids for all operation types, or it will throw.</param>
            /// <param name="newGroupIdsByOperation">This should have a non-null hashset of group ids for all operation types, or it will throw.</param>
            /// <returns>A list of reasons invalid, or empty.</returns>
            public static IEnumerable<string> GetReasonsInvalid(
                BrokerIdentifier brokerId,
                Dictionary<ConvoLogOperationType, HashSet<Guid>> newRoleIdsByOperation,
                Dictionary<ConvoLogOperationType, HashSet<Guid>> newGroupIdsByOperation)
            {
                if (newRoleIdsByOperation == null)
                {
                    throw new ArgumentNullException(nameof(newRoleIdsByOperation));
                }

                if (newGroupIdsByOperation == null)
                {
                    throw new ArgumentNullException(nameof(newGroupIdsByOperation));
                }

                foreach (ConvoLogOperationType operation in Enum.GetValues(typeof(ConvoLogOperationType)))
                {
                    if (!newRoleIdsByOperation.ContainsKey(operation))
                    {
                        throw new KeyNotFoundException("operation: " + operation.ToString("G") + " was missing from the roles dictionary.");
                    }
                    else
                    {
                        if (newRoleIdsByOperation[operation] == null)
                        {
                            throw new ArgumentNullException(nameof(newRoleIdsByOperation) + "[" + operation.ToString("G") + "]");
                        }
                    }

                    if (!newGroupIdsByOperation.ContainsKey(operation))
                    {
                        throw new KeyNotFoundException("operation: " + operation.ToString("G") + " was missing from the groups dictionary.");
                    }
                    else
                    {
                        if (newGroupIdsByOperation[operation] == null)
                        {
                            throw new ArgumentNullException(nameof(newGroupIdsByOperation) + "[" + operation.ToString("G") + "]");
                        }
                    }
                }

                var reasonsInvalid = new List<string>();
                if (!newRoleIdsByOperation[ConvoLogOperationType.View].Any() && !newGroupIdsByOperation[ConvoLogOperationType.View].Any())
                {
                    reasonsInvalid.Add("no role or employee group would have access to view.");
                }

                if (!newRoleIdsByOperation[ConvoLogOperationType.Post].Any() && !newGroupIdsByOperation[ConvoLogOperationType.Post].Any())
                {
                    reasonsInvalid.Add("no role or employee group would have post access.");
                }

                UpdateReasonsInvalid(brokerId, newRoleIdsByOperation, reasonsInvalid, isForRole: true);
                UpdateReasonsInvalid(brokerId, newGroupIdsByOperation, reasonsInvalid, isForRole: false);

                return reasonsInvalid;
            }

            /// <summary>
            /// Sets the enabled role ids for the permission level for the specified operation. Takes effect after the transaction commits.
            /// </summary>
            /// <param name="conn">The open connection to the database where the save should take place.</param>
            /// <param name="tran">The transaction that the save belongs to.</param>
            /// <param name="brokerId">The id of the broker that the permission level belongs to..</param>
            /// <param name="permissionLevelId">The id of the permission level itself.</param>
            /// <param name="permissionType">The type of operation of interest.</param>
            /// <param name="newRoleIds">The ids of the roles that will be enabled for this permission level and operation type.</param>
            private static void SetRoleIdsForPermissionLevel(DbConnection conn, DbTransaction tran, BrokerIdentifier brokerId, long permissionLevelId, ConvoLogOperationType permissionType, HashSet<Guid> newRoleIds)
            {
                if (newRoleIds == null)
                {
                    throw new ArgumentNullException(nameof(newRoleIds));
                }

                ThrowIfCantUpdateDatabase();

                var roleXmlElements = from r in newRoleIds
                                      select new XElement("r", new XAttribute("id", r.ToString()));
                var xmlOfRoles = string.Concat(roleXmlElements.Select(el => el.ToString()));
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", brokerId.Value),
                    new SqlParameter("@PermissionLevelId", permissionLevelId),
                    new SqlParameter("@PermissionType", (int)permissionType),
                    new SqlParameter("@XmlOfRoleIdsPermittedForPermissionLevelAndType", xmlOfRoles)
                };

                var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
                var driver = factory.Create(TimeoutInSeconds.Default);
                driver.ExecuteNonQuery(conn, tran, SpSetRoles, parameters);
            }

            /// <summary>
            /// Sets the enabled group ids for the permission level for the specified operation. Takes effect after the transaction commits.
            /// </summary>
            /// <param name="conn">The open connection to the database where the save should take place.</param>
            /// <param name="tran">The transaction that the save belongs to.</param>
            /// <param name="brokerId">The id of the broker that the permission level belongs to..</param>
            /// <param name="permissionLevelId">The id of the permission level itself.</param>
            /// <param name="permissionType">The type of operation of interest.</param>
            /// <param name="newGroupIds">The ids of the groups that will be enabled for this permission level and operation type.</param>
            private static void SetGroupIdsForPermissionLevel(DbConnection conn, DbTransaction tran, BrokerIdentifier brokerId, long permissionLevelId, ConvoLogOperationType permissionType, HashSet<Guid> newGroupIds)
            {
                if (newGroupIds == null)
                {
                    throw new ArgumentNullException(nameof(newGroupIds));
                }

                ThrowIfCantUpdateDatabase();

                var groupXmlElements = from g in newGroupIds
                                       select new XElement("g", new XAttribute("id", g.ToString()));
                var xmlOfGroups = string.Concat(groupXmlElements.Select(el => el.ToString()));
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", brokerId.Value),
                    new SqlParameter("@PermissionLevelId", permissionLevelId),
                    new SqlParameter("@PermissionType", (int)permissionType),
                    new SqlParameter("@XmlOfGroupIdsPermittedForPermissionLevelAndType", xmlOfGroups)
                };

                var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
                var driver = factory.Create(TimeoutInSeconds.Default);
                driver.ExecuteNonQuery(conn, tran, SpSetGroups, parameters);
            }

            /// <summary>
            /// Adds more reasons the save could be invalid considering the enabled permissions for each operation that are passed in.
            /// </summary>
            /// <param name="brokerId">The id of the broker of the permission level.</param>
            /// <param name="idsByOperation">The mapping of operation to the role/group ids enabled for that operation.</param>
            /// <param name="reasonsInvalid">The existing reasons the save would be invalid.</param>
            /// <param name="isForRole">True iff the ids are roleids, otherwise they are interpreted as group ids.</param>
            private static void UpdateReasonsInvalid(BrokerIdentifier brokerId, Dictionary<ConvoLogOperationType, HashSet<Guid>> idsByOperation, List<string> reasonsInvalid, bool isForRole)
            {
                if (idsByOperation == null)
                {
                    throw new ArgumentException(nameof(idsByOperation));
                }

                var needsView = new List<Tuple<ConvoLogOperationType, Guid>>();
                foreach (var operationType in OperationTypesThatRequireView)
                {
                    foreach (var id in idsByOperation[operationType])
                    {
                        if (!idsByOperation[ConvoLogOperationType.View].Contains(id))
                        {
                            needsView.Add(new Tuple<ConvoLogOperationType, Guid>(operationType, id));                            
                        }
                    }
                }

                if (needsView.Any())
                {
                    var resourceType = isForRole ? "role" : "group";
                    Dictionary<Guid, string> nameById;
                                        
                    if (isForRole)
                    {
                        nameById = Role.AllRoles.ToDictionary(r => r.Id, r => r.ModifiableDesc);
                    }
                    else
                    {
                        nameById = GroupDB.GetAllGroupIdsAndNames(brokerId.Value, GroupType.Employee).ToDictionary(t => t.Item1, t => t.Item2);
                    }

                    foreach (var needView in needsView)
                    {
                        var operationType = needView.Item1;
                        var id = needView.Item2;
                        var name = nameById[id];
                        reasonsInvalid.Add($"The {resourceType} '{name}' would be allowed to {operationType.ToString("G")} but not View.");
                    }
                }
            }

            /// <summary>
            /// Throws if the user doesn't have permission to save the permission level to the database.
            /// </summary>
            private static void ThrowIfCantUpdateDatabase()
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
                {
                    throw new PermissionException("You must be an internal user to set groups for a permission level.");
                }
            }
        }
    }
}
