﻿namespace LendersOffice.ObjLib.Security.Authorization.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using Db;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;    

    /// <summary>
    /// Represents the permission level to be associated with a conversation log conversation.
    /// </summary>
    public partial class ConversationLogPermissionLevel
    {
        /// <summary>
        /// The name of the default permission level for a broker.
        /// </summary>
        public const string DefaultLevelName = "All Roles Full Access";

        /// <summary>
        /// The name of the stored procedure that gets a permission level by id.
        /// </summary>
        private static readonly StoredProcedureName SpGet = StoredProcedureName.Create("GetConvoLogPermissionLevelById").Value;

        /// <summary>
        /// The name of the stored procedure that gets a permission level by name.
        /// </summary>
        private static readonly StoredProcedureName SpGetByNameForBroker = StoredProcedureName.Create("GetConvoLogPermissionLevelByName").Value;

        /// <summary>
        /// The name of the stored procedure that gets all permission levels for the broker.
        /// </summary>
        private static readonly StoredProcedureName SpGetAllForBroker = StoredProcedureName.Create("GetConvoLogPermissionLevelsForBroker").Value;

        /// <summary>
        /// The name of the stored procedure that updates a permission level.
        /// </summary>
        private static readonly StoredProcedureName SpUpdate = StoredProcedureName.Create("UpdateConvoLogPermissionLevel").Value;

        /// <summary>
        /// The name of the stored procedure that creates a permission level.
        /// </summary>
        private static readonly StoredProcedureName SpCreate = StoredProcedureName.Create("CreateConvoLogPermissionLevel").Value;

        /// <summary>
        /// For each operation type, specifies the enabled role ids.
        /// </summary>
        private Dictionary<ConvoLogOperationType, HashSet<Guid>> roleIdsByOperationType = new Dictionary<ConvoLogOperationType, HashSet<Guid>>();
        
        /// <summary>
        /// For each operartion type, specifies the enabled group ids.
        /// </summary>
        private Dictionary<ConvoLogOperationType, HashSet<Guid>> groupIdsByOperationType = new Dictionary<ConvoLogOperationType, HashSet<Guid>>();

        /// <summary>
        /// The description of the permission level.
        /// </summary>
        private string description;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationLogPermissionLevel"/> class. <para></para>
        /// This is for initializing new permission levels that haven't yet been created in the database. <para></para>
        /// If they have been created before, use GetPermissionLevelById.
        /// </summary>
        /// <param name="brokerId">The id of the broker this permission level will belong to.</param>
        public ConversationLogPermissionLevel(BrokerIdentifier brokerId)
        {
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Gets the id associated with the permission level, or null if not saved.<para></para>
        /// It will only get set on successful first save, or load. <para></para>
        /// If it is null, it should be considered not yet saved.
        /// </summary>
        /// <value>The id of the permission level, or null if not saved.</value>
        public long? Id { get; private set; }

        /// <summary>
        /// Gets the identifier of the broker that this permission level belongs to.
        /// </summary>
        /// <value>The identifier of the broker.</value>
        public BrokerIdentifier BrokerId { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level should be available for POST.
        /// </summary>
        /// <value>A value indicating whether or not the permission level should be available for POSTing.</value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the name of the permission level.
        /// </summary>
        /// <value>The name of the permission level.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a non-null description of the permission level.
        /// </summary>
        /// <value>A non-null description for the permission level.</value>
        public string Description
        {
            get
            {
                return (this.description == null) ? string.Empty : this.description;
            }

            set
            {
                this.description = (value == null) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets the permission level from the id specified, or returns null if not found.
        /// </summary>
        /// <param name="brokerId">The id of the broker this permission level belongs to.</param>
        /// <param name="id">The id of the permission level of interest.</param>
        /// <returns>The permission level with the specified id, or null if not found.</returns>
        public static ConversationLogPermissionLevel GetPermissionLevelById(BrokerIdentifier brokerId, long id)
        {
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@Id", id)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId.Value, SpGet.ToString(), parameters))
            {
                if (reader.Read())
                {
                    var level = new ConversationLogPermissionLevel(brokerId)
                    {
                        BrokerId = brokerId,
                        Id = id,
                        IsActive = (bool)reader["IsActive"],
                        Name = (string)reader["Name"],
                        Description = (string)reader["Description"]
                    };
                    return level;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the ConversationLogPermissionLevels in the order they belong for the broker.
        /// </summary>
        /// <param name="brokerId">The id of the broker that the permission level belongs to.</param>
        /// <returns>The permission levels as ordered since the last permission-level editor save.</returns>
        public static List<ConversationLogPermissionLevel> GetOrderedPermissionLevelsForBroker(BrokerIdentifier brokerId)
        {
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId.Value)
            };

            var levels = new List<ConversationLogPermissionLevel>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId.Value, SpGetAllForBroker.ToString(), parameters))
            {
                while (reader.Read())
                {
                    var level = new ConversationLogPermissionLevel(brokerId)
                    {
                        Id = (int)reader["ID"],
                        IsActive = (bool)reader["IsActive"],
                        Name = (string)reader["Name"],
                        Description = (string)reader["Description"]
                    };
                    levels.Add(level);
                }
            }

            var ordinalByPermissionLevelId = ConversationLogPermissionLevelOrderingManager.GetOrdinalByPermissionLevelIdForBroker(brokerId);
            return levels.OrderBy(l => ordinalByPermissionLevelId[l.Id.Value]).ToList();
        }

        /// <summary>
        /// If the default is absent (say the migration failed), this includes a default permission level.
        /// </summary>
        /// <param name="brokerId">The id of the broker for this permission level.</param>
        /// <returns>The ordered list of permisison levels associated with the broker, including a default permission level (migrated or in-memory).</returns>
        public static List<ConversationLogPermissionLevel> GetOrderedPermissionLevelsForBrokerIncludingDefault(BrokerIdentifier brokerId)
        {
            var permissionLevels = GetOrderedPermissionLevelsForBroker(brokerId);

            permissionLevels.Insert(0, CreateUnsavedDefaultForBroker(brokerId.Value));
            
            return permissionLevels;
        }

        /// <summary>
        /// Creates an in-memory default permission level for a broker as determined for opm 450195, that is all roles for view, post, and reply. <para></para>
        /// It does not save it to the database. <para></para>
        /// It does not set any employee groups as enabled. <para></para>
        /// It does not set any roles or employee groups for the Hide operation.
        /// </summary>
        /// <param name="brokerId">The id of the broker the permission level would be for.</param>
        /// <returns>The in-memory default permission level for the broker.</returns>
        public static ConversationLogPermissionLevel CreateUnsavedDefaultForBroker(Guid brokerId)
        {
            var brokerIdentifer = BrokerIdentifier.Create(brokerId.ToString()).Value;
            var p = new ConversationLogPermissionLevel(brokerIdentifer);
            p.Description = "All roles for view, post, reply.";
            p.Name = DefaultLevelName;
            p.IsActive = true;
            var allRoles = Role.AllRoles.Select(r => r.Id);
            p.SetRoleIdsForOperationType(ConvoLogOperationType.View, allRoles);
            p.SetRoleIdsForOperationType(ConvoLogOperationType.Reply, allRoles);
            p.SetRoleIdsForOperationType(ConvoLogOperationType.Post, allRoles);

            return p;
        }
        
        /// <summary>
        /// Saves the permission level, or returns the reason it could not.
        /// </summary>
        /// <returns>The reasons it could not save it, or empty on successful save.</returns>
        public IEnumerable<string> Save()
        {
            /*
                validate first.
                if id is null: save, grab id, save roles, groups all in one transaction, add it to the ordering as the last one, and update the instance's id.
                if id is not null: update, and update all roles/groups 
            */

            // ensure we've got all the roles/groups before saving.
            this.LoadAllIdsThatArentCurrentlyLoaded();

            var reasonsInvalid = this.GetReasonsInvalid();
            if (reasonsInvalid.Any())
            {
                return reasonsInvalid;
            }

            var principal = PrincipalFactory.CurrentPrincipal;
            if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new PermissionException("You do not have permission to create/update conversation log permission levels.");
            }

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            if (this.Id == null)
            {
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", this.BrokerId.Value),
                    new SqlParameter("@IsActive", this.IsActive),
                    new SqlParameter("@Name", this.Name),
                    new SqlParameter("@Description", this.Description)
                };

                long id;

                TransactionHelper.DoTransactionally(
                connectionGetter: () => DbConnectionInfo.GetConnection(this.BrokerId.Value),
                actionToCommitOrNot: (conn, tran) =>
                {
                    using (var reader = driver.ExecuteReader(conn, tran, SpCreate, parameters))
                    {
                        if (reader.Read())
                        {
                            id = (int)reader["Id"];
                        }
                        else
                        {
                            throw new CBaseException("Did not get the id of the permission level from the database.", "Did not get the id of the permission level from the database.");
                        }
                    }

                    reasonsInvalid = ConversationLogPermissionLevelAssociationsRepository.SetForPermissionLevel(conn, tran, this.BrokerId, id, this.roleIdsByOperationType, this.groupIdsByOperationType);

                    if (reasonsInvalid.Any())
                    {
                        return false;
                    }
                    else
                    {
                        this.Id = id;
                        return true;
                    }
                });
            }
            else
            {
                // if this.Id != null, update instead of create.
                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@Id", this.Id.Value),
                    new SqlParameter("@IsActive", this.IsActive),
                    new SqlParameter("@Name", this.Name),
                    new SqlParameter("@Description", this.Description)
                };

                TransactionHelper.DoTransactionally(
                connectionGetter: () => DbConnectionInfo.GetConnection(this.BrokerId.Value),
                actionToCommitOrNot: (conn, tran) =>
                {
                    reasonsInvalid = ConversationLogPermissionLevelAssociationsRepository.SetForPermissionLevel(conn, tran, this.BrokerId, this.Id.Value, this.roleIdsByOperationType, this.groupIdsByOperationType);

                    if (reasonsInvalid.Any())
                    {
                        return false;
                    }

                    driver.ExecuteNonQuery(conn, tran, SpUpdate, parameters);
                    return true;
                });
            }

            return reasonsInvalid;
        }

        /// <summary>
        /// Gets the role ids for the operation.  If it had been set or gotten before, it does not hit the database again.
        /// </summary>
        /// <param name="operationType">The operation type for the roles we are interested in.</param>
        /// <returns>A hashset of the ids of the roles that are allowed to run this operation for this permission level.</returns>
        public HashSet<Guid> GetRoleIdsForOperationType(ConvoLogOperationType operationType)
        {
            if (!this.roleIdsByOperationType.ContainsKey(operationType))
            {
                if (this.Id.HasValue)
                {
                    this.roleIdsByOperationType[operationType] = new HashSet<Guid>(ConversationLogPermissionLevelAssociationsRepository.GetRoleIdsForPermissionLevel(this.BrokerId, this.Id.Value, operationType));
                }
                else
                {
                    this.roleIdsByOperationType[operationType] = new HashSet<Guid>();
                }
            }

            return new HashSet<Guid>(this.roleIdsByOperationType[operationType]);
        }

        /// <summary>
        /// Gets the group ids for the operation.  If it had been set or gotten before, it does not hit the database again.
        /// </summary>
        /// <param name="operationType">The operation type for the groups we are interested in.</param>
        /// <returns>A hashset of the ids of the groups that are allowed to run this operation for this permission level.</returns>
        public HashSet<Guid> GetGroupIdsForOperationType(ConvoLogOperationType operationType)
        {
            if (!this.groupIdsByOperationType.ContainsKey(operationType))
            {
                if (this.Id.HasValue)
                {
                    this.groupIdsByOperationType[operationType] = new HashSet<Guid>(ConversationLogPermissionLevelAssociationsRepository.GetGroupIdsForPermissionLevel(this.BrokerId, this.Id.Value, operationType));
                }
                else
                {
                    this.groupIdsByOperationType[operationType] = new HashSet<Guid>();
                }
            }

            return new HashSet<Guid>(this.groupIdsByOperationType[operationType]);
        }

        /// <summary>
        /// Sets the ids of the enabled roles for the given operation type to the ids specified.<para></para>
        /// Doesn't take effect until saved.  Clears existing and sets only the active ones.<para></para>
        /// </summary>
        /// <param name="operationType">The type of operation to set the enabled role ids for.</param>
        /// <param name="roleIds">The ids of the roles to be enabled for the specified operation type.</param>
        public void SetRoleIdsForOperationType(ConvoLogOperationType operationType, IEnumerable<Guid> roleIds)
        {
            if (roleIds == null)
            {
                throw new ArgumentNullException(nameof(roleIds));
            }

            var newRoleIds = new HashSet<Guid>(roleIds);
            this.roleIdsByOperationType[operationType] = newRoleIds;
        }

        /// <summary>
        /// Sets the ids of the enabled groups for the given operation type to the ids specified.<para></para>
        /// Doesn't take effect until saved.  Clears existing and sets only the active ones.<para></para>
        /// </summary>
        /// <param name="operationType">The type of operation to set the enabled group ids for.</param>
        /// <param name="employeeGroupIds">The ids of the groups to be enabled for the specified operation type.</param>
        public void SetGroupIdsForOperationType(ConvoLogOperationType operationType, IEnumerable<Guid> employeeGroupIds)
        {
            if (employeeGroupIds == null)
            {
                throw new ArgumentNullException(nameof(employeeGroupIds));
            }

            var employeeGroupIdsSet = new HashSet<Guid>(employeeGroupIds);
            this.groupIdsByOperationType[operationType] = employeeGroupIdsSet;
        }

        /// <summary>
        /// Gets the reasons that the permission level is not valid for save.
        /// </summary>
        /// <returns>The reasons that it's not valid, or an empty list.</returns>
        private IEnumerable<string> GetReasonsInvalid()
        {
            // Invalid if active and no corresponding role for view in post / hide / reply, or if no view roles/ EGs.
            // Invalid if inactive and there would be no active permission levels for this broker after save.
            var reasonsInvalid = new List<string>();
            if (this.IsActive)
            {
                if (string.Equals(this.Name, DefaultLevelName, StringComparison.OrdinalIgnoreCase))
                {
                    reasonsInvalid.Add("One does not save the default permission level.");
                }

                reasonsInvalid.AddRange(ConversationLogPermissionLevelAssociationsRepository.GetReasonsInvalid(this.BrokerId, this.roleIdsByOperationType, this.groupIdsByOperationType));
            }
            else
            {
                // if !this.IsActive, todo: add constraint to db that prevents this (after migration).
            }

            return reasonsInvalid;
        }

        /// <summary>
        /// Ensures that the permission level has all the roles and groups that haven't been set.
        /// </summary>
        private void LoadAllIdsThatArentCurrentlyLoaded()
        {
            foreach (ConvoLogOperationType type in Enum.GetValues(typeof(ConvoLogOperationType)))
            {
                this.GetRoleIdsForOperationType(type);
                this.GetGroupIdsForOperationType(type);
            }
        }
    }
}
