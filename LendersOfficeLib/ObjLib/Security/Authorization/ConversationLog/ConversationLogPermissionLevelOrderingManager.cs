﻿namespace LendersOffice.ObjLib.Security.Authorization.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using Db;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// There could multiple orderings, so perhaps an ordering should have a name associated with it like Loadmin.
    /// </summary>
    public class ConversationLogPermissionLevelOrderingManager
    {
        /// <summary>
        /// The name of the stored procedure that gets the order of the permission levels for the broker.
        /// </summary>
        private static readonly StoredProcedureName SpGetOrder = StoredProcedureName.Create("GetConvoLogPermissionLevelsOrder").Value;

        /// <summary>
        /// The name of the stored procedure that sets the order of the permission levels for the broker.
        /// </summary>
        private static readonly StoredProcedureName SpSetOrder = StoredProcedureName.Create("SetConvoLogPermissionLevelsOrder").Value;

        /// <summary>
        /// Gets the ordering of the permission level ids, by their ids.
        /// </summary>
        /// <param name="brokerId">The id of the broker that the permission levels belong to.</param>
        /// <returns>The mapping of id to it's ordering index.</returns>
        public static Dictionary<long, int> GetOrdinalByPermissionLevelIdForBroker(BrokerIdentifier brokerId)
        {
            var brokerGuid = new Guid(brokerId.ToString());
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerGuid)
            };

            var ordinalIds = new Dictionary<long, int>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerGuid, SpGetOrder.ToString(), parameters))
            {
                while (reader.Read())
                {
                    ordinalIds.Add(
                        (int)reader["PermissionLevelId"],
                        (int)reader["Ordinal"]);
                }
            }

            return ordinalIds;
        }

        /// <summary>
        /// Sets the order of the permission levels to that passed in.
        /// </summary>
        /// <param name="brokerId">The id of the broker that the permission levels belong to.</param>
        /// <param name="orderedPermissionLevelIds">The ids of the permission levels in the order they should be.</param>
        public static void SetOrderingForPermissionLevelsForBrokerId(BrokerIdentifier brokerId, long[] orderedPermissionLevelIds)
        {
            if (orderedPermissionLevelIds == null)
            {
                throw new ArgumentNullException(nameof(orderedPermissionLevelIds));
            }

            if (orderedPermissionLevelIds.Length != new HashSet<long>(orderedPermissionLevelIds).Count)
            {
                throw new ArgumentException("The values of the permission level ids are not distinct.");
            }

            ThrowIfCantUpdateDatabase();

            var brokerGuid = new Guid(brokerId.ToString());

            var permissionLevelOrderXmlElements = new List<XElement>();
            for (var i = 0; i < orderedPermissionLevelIds.Length; i++)
            {
                permissionLevelOrderXmlElements.Add(new XElement("p", new XAttribute("id", orderedPermissionLevelIds[i]), new XAttribute("o", i + 1)));
            }

            var permissionLevelOrderXml = string.Concat(permissionLevelOrderXmlElements.Select(e => e.ToString()));

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerGuid),
                new SqlParameter("@PermissionLevelOrderXml", permissionLevelOrderXml)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            TransactionHelper.DoTransactionally(
                connectionGetter: () => DbConnectionInfo.GetConnection(brokerId.Value),
                actionToCommitOrNot: (conn, tran) =>
                 {
                     driver.ExecuteNonQuery(conn, tran, SpSetOrder, parameters);
                     return true;
                 });
        }
        
        /// <summary>
        /// Throws if the user doesn't have permission to set the order of the permission levels.
        /// </summary>
        private static void ThrowIfCantUpdateDatabase()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new PermissionException("You must be an internal user to set permission level ordering.");
            }
        }
    }
}
