﻿namespace LendersOffice.ObjLib.Security.Authorization
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Ideally all derived classes are singletons. <para></para>
    /// See the <see cref="ConversationLog.PermissionLevelAuthorizer"/> class.
    /// </summary>
    /// <typeparam name="TResource">The type for the resource of interest.</typeparam>
    /// <typeparam name="TOperation">The operation type to grant or deny on the resource.</typeparam>
    public interface IAuthorizer<TResource, TOperation>
    {
        /// <summary>
        /// Returns true iff the user is authorized to perform the desired operation on the specified resource.
        /// </summary>
        /// <param name="token">The security token associated with the person who wants access.</param>
        /// <param name="resource">The resource they are requesting access to.</param>
        /// <param name="operation">The operation they'd like to perform on the requested resource.</param>
        /// <returns>True iff the user is authorized to perform the desired operation on the specified resource.</returns>
        bool CanRun(SecurityToken token, TResource resource, TOperation operation);
    }    
}
