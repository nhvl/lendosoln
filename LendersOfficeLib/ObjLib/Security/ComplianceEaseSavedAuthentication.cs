﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    public class ComplianceEaseAuthentication
    {
        protected Guid m_brokerId;
        protected Guid m_userId;
        protected EncryptionKeyIdentifier encryptionKeyId;
        protected Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);
        protected string m_encryptedPassword = "";

        protected string m_originalUserName = "";
        protected Lazy<string> lazyOriginalPassword = new Lazy<string>(() => string.Empty);
        protected string m_originalEncryptedPassword = "";

        public string UserName { get; set; }
        public string Password
        {
            get { return this.lazyPassword.Value ?? string.Empty; }
            set { this.lazyPassword = new Lazy<string>(() => value); }
        }
        public void Save()
        {
            if (m_originalUserName == UserName && this.lazyOriginalPassword.Value == this.lazyPassword.Value)
            {
                return;
            }

            if (Password == string.Empty)
            {
                if (UserName != string.Empty)
                {
                    throw new ArgumentException("ComplianceEase login info must have either both username and password or neither.");
                }
            }
            else if (Password != string.Empty)
            {
                if (UserName == string.Empty)
                {
                    throw new ArgumentException("ComplianceEase login info must have either both username and password or neither.");
                }
            }

            byte[] passwordBytes = null;
            if (this.encryptionKeyId != default(EncryptionKeyIdentifier))
            {
                // EncryptionTODO: force retrieval and save to actually force an encryption key id to exist.
                passwordBytes = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value);
            }

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@UserId", m_userId),
                new SqlParameter("@ComplianceEaseUserName", UserName),
                new SqlParameter("@ComplianceEasePassword", EncryptionHelper.Encrypt(this.lazyPassword.Value)),
                new SqlParameter("@EncryptedComplianceEasePassword", passwordBytes ?? new byte[0]),
            };
            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "ComplianceEase_UpdateSavedAuthentication", 3, parameters);
        }
    }

    public class ComplianceEaseSavedAuthentication : ComplianceEaseAuthentication
    {
        private ComplianceEaseSavedAuthentication(Guid brokerId, Guid userId)
        {
            m_userId = userId;
            m_brokerId = brokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "ComplianceEase_RetrieveSavedAuthentication", parameters))
            {
                if (reader.Read())
                {
                    UserName = (string)reader["ComplianceEaseUserName"];
                    var localEncryptKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]); // EncryptionTODO: Also need to force a value here
                    if (localEncryptKeyId.HasValue)
                    {
                        this.encryptionKeyId = localEncryptKeyId.Value;
                        byte[] encryptedPasswordBytes = (byte[])reader["EncryptedComplianceEasePassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(localEncryptKeyId.Value, encryptedPasswordBytes));
                    }
                    else
                    {
                        string encryptedPasswordString = (string)reader["ComplianceEasePassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPasswordString));
                    }

                    m_originalUserName = UserName;
                    this.lazyOriginalPassword = this.lazyPassword;
                }
                else
                {
                    throw new NotFoundException("Could not locate user", "Unable to find userid=" + userId);
                }
            }
        }

        public static ComplianceEaseSavedAuthentication Retrieve(AbstractUserPrincipal principal)
        {
            return new ComplianceEaseSavedAuthentication(principal.BrokerId, principal.UserId);
        }
    }
}
