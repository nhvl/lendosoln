﻿namespace LendersOffice.Security
{
    using System;
    using System.Security.Principal;

    /// <summary>
    /// Provides functionality to temporarily use
    /// a user principal within a constrained region
    /// of code.
    /// </summary>
    /// <remarks>
    /// This class is intended for situations where
    /// passing the principal directly to a method
    /// would require extensive refactoring and should
    /// not be used in situations where supplying the
    /// principal to a method would require simple
    /// changes to the call stack.
    /// </remarks>
    public sealed class TemporaryPrincipalUseHelper : IDisposable
    {
        /// <summary>
        /// The original user principal to restore.
        /// </summary>
        private readonly IPrincipal originalPrincipal;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemporaryPrincipalUseHelper"/> class.
        /// </summary>
        /// <param name="originalPrincipal">
        /// The original principal to restore.
        /// </param>
        /// <param name="temporaryPrincipal">
        /// The temporary principal to use.
        /// </param>
        public TemporaryPrincipalUseHelper(IPrincipal originalPrincipal, IPrincipal temporaryPrincipal)
        {
            this.originalPrincipal = originalPrincipal;

            if (temporaryPrincipal != null)
            {
                this.SetPrincipal(temporaryPrincipal);
            }
        }

        /// <summary>
        /// Disposes the helper, restoring the original principal.
        /// </summary>
        public void Dispose()
        {
            this.SetPrincipal(this.originalPrincipal);
        }

        /// <summary>
        /// Sets the principal.
        /// </summary>
        /// <param name="principal">
        /// The principal to set.
        /// </param>
        private void SetPrincipal(IPrincipal principal)
        {
            System.Threading.Thread.CurrentPrincipal = principal;
        }
    }
}
