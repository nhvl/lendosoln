﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Provides access to all client certificates for a lender's users.
    /// </summary>
    public class ClientCertificates
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private readonly Guid brokerId;

        /// <summary>
        /// Gets a value indicating whether the data has been loaded from the data source.
        /// </summary>
        private bool loadedData;

        /// <summary>
        /// A map from user id to the user's certificates.
        /// </summary>
        private Dictionary<Guid, List<ClientCertificate>> certificatesByUserId;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientCertificates"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        public ClientCertificates(Guid brokerId)
        {
            this.brokerId = brokerId;
        }

        /// <summary>
        /// Gets the certificates for the given user id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>
        /// The certificates for the given user id. Returns an empty enumerable
        /// if there are none.
        /// </returns>
        public IEnumerable<ClientCertificate> GetCertificatesForUser(Guid userId)
        {
            if (!this.loadedData)
            {
                this.Retrieve();
            }

            if (this.certificatesByUserId.ContainsKey(userId))
            {
                return this.certificatesByUserId[userId];
            }
            else
            {
                return new List<ClientCertificate>();
            }
        }

        /// <summary>
        /// Loads all of the certificates for the lender.
        /// </summary>
        public void Retrieve()
        {
            this.certificatesByUserId = new Dictionary<Guid, List<ClientCertificate>>();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(this.brokerId, "ALL_USER_CLIENT_CERTIFICATE_ListAllForBroker", parameters))
            {
                while (reader.Read())
                {
                    var userId = (Guid)reader["UserId"];
                    var cert = ClientCertificate.LoadFromReader(reader);

                    if (this.certificatesByUserId.ContainsKey(userId))
                    {
                        this.certificatesByUserId[userId].Add(cert);
                    }
                    else
                    {
                        this.certificatesByUserId.Add(
                            userId,
                            new List<ClientCertificate>() { cert });
                    }
                }
            }

            this.loadedData = true;
        }
    }
}
