﻿// <copyright file="ComplianceEagleUserCredentials.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Timothy Jewell
//    Date:   7/30/2014 5:57:53 PM 
// </summary>
namespace LendersOffice.Security
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Holds user-level credentials for Compliance Eagle. Saved and loaded on the BROKER_USER table.
    /// </summary>
    public class ComplianceEagleUserCredentials
    {
        /// <summary>
        /// The user ID of the user who owns this set of credentials.
        /// </summary>
        private Guid userID;

        /// <summary>
        /// The UserName of the credentials.
        /// </summary>
        private string userName;

        /// <summary>
        /// The identifier of the encryption key used to encrypt these credentials.
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// Provides lazy access to the password value.
        /// </summary>
        private Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleUserCredentials" /> class.
        /// </summary>
        /// <param name="userName">The UserName of the credentials.</param>
        /// <param name="lazyPassword">The lazy password of the credentials.</param>
        /// <param name="encryptionKeyId">The encryption key to use to encrypt the password.</param>
        /// <param name="companyID">The Company ID of the credentials.</param>
        /// <param name="userID">The user ID for the user owning these credentials.</param>
        /// <param name="brokerID">The broker ID of the  credentials.</param>
        private ComplianceEagleUserCredentials(string userName, Lazy<string> lazyPassword, EncryptionKeyIdentifier encryptionKeyId, string companyID, Guid userID, Guid brokerID)
        {
            this.userName = userName;
            this.lazyPassword = lazyPassword;
            this.encryptionKeyId = encryptionKeyId;
            this.CompanyID = companyID;
            this.userID = userID;
            this.BrokerID = brokerID;
        }

        /// <summary>
        /// Gets or sets the UserName of the credentials.
        /// </summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get
            {
                return this.userName;
            }

            set
            {
                if (this.UserName != value)
                {
                    this.userName = value;
                    this.IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Password of the credentials.
        /// </summary>
        /// <value>The password (unencrypted).</value>
        public string Password
        {
            get
            {
                return this.lazyPassword.Value ?? string.Empty;
            }

            set
            {
                if (this.lazyPassword.Value != value)
                {
                    this.lazyPassword = new Lazy<string>(() => value);
                    this.IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Gets the Broker ID of the credentials.
        /// </summary>
        /// <value>The broker ID of the credentials.</value>
        public Guid BrokerID { get; private set; }

        /// <summary>
        /// Gets the Company ID of the credentials.  This field is specified at the broker level.
        /// </summary>
        /// <value>The Company ID.</value>
        public string CompanyID { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the credentials have been modified since loading.
        /// </summary>
        /// <value><see langword="true"/> if the credentials have been modified; false otherwise.</value>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// Retrieves the Compliance Eagle credentials for a user from the BROKER_USER table.
        /// </summary>
        /// <param name="principal">The principal owning the credentials.</param>
        /// <returns>The Compliance Eagle credentials for the user.</returns>
        public static ComplianceEagleUserCredentials Retrieve(AbstractUserPrincipal principal)
        {
            return Retrieve(principal.UserId, principal.BrokerId);
        }

        /// <summary>
        /// Retrieves the Compliance Eagle credentials for a user from the BROKER_USER table.
        /// </summary>
        /// <param name="userId">The user ID of the user.</param>
        /// <param name="brokerId">The broker ID of the user.</param>
        /// <returns>The Compliance Eagle credentials for the user.</returns>
        public static ComplianceEagleUserCredentials Retrieve(Guid userId, Guid brokerId)
        {
            if (userId == Guid.Empty)
            {
                // 8/19/2014 dd - Return empty object.
                return new ComplianceEagleUserCredentials(string.Empty, new Lazy<string>(() => string.Empty), default(EncryptionKeyIdentifier), string.Empty, Guid.Empty, brokerId);
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserId", userId)
            };

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ComplianceEagle_RetrieveUserCredentials", parameters))
            {
                if (reader.Read())
                {
                    byte[] encryptedPassword = (byte[])reader["ComplianceEaglePassword"];
                    var encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]) ?? EncryptionKeyIdentifier.ComplianceEagle;
                    return new ComplianceEagleUserCredentials(
                        (string)reader["ComplianceEagleUserName"],
                        new Lazy<string>(() => EncryptionHelper.DecryptString(encryptionKeyId, encryptedPassword)),
                        encryptionKeyId,
                        (string)reader["ComplianceEagleCompanyID"],
                        userId,
                        brokerId);
                }
                else
                {
                    throw new NotFoundException("Could not locate user", "Unable to find userid=" + userId);
                }
            }
        }

        /// <summary>
        /// Saves the user's credentials to the BROKER_USER table.
        /// </summary>
        public void Save()
        {
            if (this.userID == Guid.Empty)
            {
                // NO-OP.
                return;
            }

            if (this.IsDirty)
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@UserID", this.userID),
                    new SqlParameter("@ComplianceEagleUserName", this.UserName ?? string.Empty),
                    new SqlParameter("@ComplianceEaglePassword", EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value) ?? new byte[0])
                };
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerID, "ComplianceEagle_SaveUserCredentials", 3, parameters);
            }
        }
    }
}
