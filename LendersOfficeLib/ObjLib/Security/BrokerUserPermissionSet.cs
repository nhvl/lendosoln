﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Security
{
    /// <summary>
    /// We need to fetch all the employee's of a broker in one
    /// shot for quicker analysis of a list of employees.
    /// </summary>
    
    public class BrokerUserPermissionsSet
    {
        /// <summary>
        /// Keep track of all the employees we find.
        /// </summary>

        private Hashtable m_Table = new Hashtable();

        public BrokerUserPermissions this[Guid employeeId]
        {
            // Access employee's permissions.

            get
            {
                BrokerUserPermissions buP = m_Table[employeeId] as BrokerUserPermissions;

                if (buP != null)
                {
                    return buP;
                }

                return null;
            }
        }

        /// <summary>
        /// Fetch latest set for this broker.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            // Load all employees' permissions and fill out the table
            // with individual permissions objects.
            Dictionary<Guid, PmlBrokerPermission> pmlBrokerPermissionCache = new Dictionary<Guid, PmlBrokerPermission>();

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId)
                                            };
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeePermissionWithPopulationTypeByBrokerId", parameters))
                {
                    while (sR.Read() == true)
                    {
                        try
                        {
                            Guid employeeId = (Guid)sR["EmployeeId"];

                            if (sR["Permissions"] is DBNull == true)
                            {
                                continue;
                            }

                            if (employeeId != Guid.Empty)
                            {
                                EmployeePopulationMethodT? permissionPopulationMethod = null;

                                if (!Convert.IsDBNull(sR["PopulatePmlPermissionsT"]))
                                {
                                    permissionPopulationMethod = (EmployeePopulationMethodT)sR["PopulatePmlPermissionsT"];
                                }

                                var permissions = new BrokerUserPermissions(
                                    brokerId,
                                    employeeId,
                                    sR["Permissions"].ToString(),
                                    sR["Type"].ToString(),
                                    sR["PmlBrokerId"] as Guid? ?? Guid.Empty,
                                    permissionPopulationMethod,
                                    pmlBrokerPermissionCache );

                                m_Table.Add(employeeId, permissions);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(e);

                throw e;
            }
        }

        /// <summary>
        /// Search for employee's record in our set.  It is
        /// good to test before accessing to avoid null.
        /// </summary>

        public Boolean Contains(Guid employeeId)
        {
            // Check our hashtable.

            if (m_Table.Contains(employeeId) == true)
            {
                return true;
            }

            return false;
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>
        /// <param name="brokerId">
        /// Broker to fetch.
        /// </param>

        public BrokerUserPermissionsSet(Guid brokerId)
        {
            // Initialize set.

            Retrieve(brokerId);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public BrokerUserPermissionsSet()
        {
        }

        #endregion

    }
}
