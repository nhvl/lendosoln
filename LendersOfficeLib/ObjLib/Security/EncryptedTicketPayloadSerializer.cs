﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// A encryption decorator that encrypts/decrypts the output of another IAuthTicketPayloadSerializer.
    /// </summary>
    public class EncryptedTicketPayloadSerializer : IAuthTicketPayloadSerializer
    {
        /// <summary>
        /// The number of characters to add.
        /// </summary>
        private const int PADDINGLENGTH = 20;

        /// <summary>
        /// The inner serializer whose output will be encrypted.
        /// </summary>
        private IAuthTicketPayloadSerializer serializer;

        /// <summary>
        /// The encryption driver for encrypting the payload.
        /// </summary>
        private IEncryptionDriver encryptionDriver;

        /// <summary>
        /// The encryption key id to use for the encryption.
        /// </summary>
        private EncryptionKeyIdentifier keyId;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptedTicketPayloadSerializer" /> class.
        /// </summary>
        /// <param name="encryptionDriver">The driver used to encrypt the payload.</param>
        /// <param name="keyId">The key identifier to use for encryption/decryption.</param>
        /// <param name="serializer">The serializer whose output will be encrypted.</param>
        public EncryptedTicketPayloadSerializer(IEncryptionDriver encryptionDriver, EncryptionKeyIdentifier keyId, IAuthTicketPayloadSerializer serializer)
        {
            this.serializer = serializer;
            this.encryptionDriver = encryptionDriver;
            this.keyId = keyId;
        }

        /// <summary>
        /// Decrypts and deserializes the given payload.
        /// </summary>
        /// <param name="data">The data to decrypt.</param>
        /// <returns>The AuthTicketPayload representation.</returns>
        public AuthTicketPayload Deserialize(string data)
        {
            try
            { 
                data = this.encryptionDriver.DecodeAndDecrypt(this.keyId, data);
                data = this.RemovePadding(data);
                return this.serializer.Deserialize(data);
            }
            catch (FormatException)
            {
                Tools.LogWarning($"Received invalid encrypted auth ticket. --{data}--");
                return null;
            }
        }

        /// <summary>
        /// Serializes the given payload into an encrypted string representation.
        /// </summary>
        /// <param name="payload">The payload to serialize.</param>
        /// <returns>The string representation of the payload.</returns>
        public string Serialize(AuthTicketPayload payload)
        {
            string data = this.serializer.Serialize(payload);
            data = this.AddPadding(data);
            return this.encryptionDriver.EncryptAndEncode(this.keyId, data);
        }

        /// <summary>
        /// Removes the added padding from the given data.
        /// </summary>
        /// <param name="data">The serialized padded data.</param>
        /// <returns>Unpadded data.</returns>
        private string RemovePadding(string data)
        {
            int endMinusPadding = data.Length - (PADDINGLENGTH * 2);
            return data.Substring(PADDINGLENGTH + 1, endMinusPadding);
        }

        /// <summary>
        /// Generates appends random data to the front and end to increase the entropy.
        /// </summary>
        /// <param name="input">The unpadded payload.</param>
        /// <returns>The string with added padding.</returns>
        private string AddPadding(string input)
        {
            int length = PADDINGLENGTH * 2;
            char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.-".ToCharArray();
            byte[] buffer = new byte[length];

            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(buffer);
            }

            StringBuilder result = new StringBuilder(length + input.Length);

            int i = 0;
            byte b;

            for (; i < length; i++)
            {
                b = buffer[i];
                result.Append(chars[b % chars.Length]);

                if (PADDINGLENGTH == i)
                {
                    result.Append(input);
                }
            }

            return result.ToString();
        }
    }
}
