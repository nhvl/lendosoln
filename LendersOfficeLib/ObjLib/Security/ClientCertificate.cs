﻿// <copyright file="ClientCertificate.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/9/2014 9:37:35 AM 
// </summary>
namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// This class contains method for Create / Retrieve client certificate from database.
    /// </summary>
    public class ClientCertificate
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="ClientCertificate" /> class from being created.
        /// </summary>
        private ClientCertificate()
        {
        }

        /// <summary>
        /// Gets unique Certificate Id.
        /// </summary>
        /// <value>Unique Certificate Id.</value>
        public Guid CertificateId { get; private set; }

        /// <summary>
        /// Gets certificate description.
        /// </summary>
        /// <value>Certificate description.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets certificate Created Date.
        /// </summary>
        /// <value>Certificate created date.</value>
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Gets user id that created certificate.
        /// </summary>
        /// <value>User id that created certificate.</value>
        public Guid CreatedBy { get; private set; }

        /// <summary>
        /// Gets login name of user that created certificate.
        /// </summary>
        /// <value>Login name of user that created certificate.</value>
        public string CreatedByUserName { get; private set; }

        /// <summary>
        /// Verify if there is a certificate id for the user.
        /// </summary>
        /// <param name="certificateId">Certificate id to verify.</param>
        /// <param name="brokerId">Broker id that user belong to.</param>
        /// <param name="userId">User id that certificate must belong to.</param>
        /// <returns>Whether there is a certificate for this user.</returns>
        public static bool Verify(Guid certificateId, Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@CertificateId", certificateId),
                new SqlParameter("@UserId", userId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ALL_USER_CLIENT_CERTIFICATE_Verify", parameters))
            {
                if (reader.Read())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Check if this user has any valid certificate.
        /// </summary>
        /// <param name="brokerId">Broker id to check.</param>
        /// <param name="userId">User id to check.</param>
        /// <returns>Whether this user has certificate.</returns>
        public static bool HasClientCertificate(Guid brokerId, Guid userId)
        {
            var list = ListByUser(brokerId, userId);

            if (list == null || list.Count() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// List all available client certificates of the user.
        /// </summary>
        /// <param name="brokerId">Broker id to check.</param>
        /// <param name="userId">User id to check.</param>
        /// <returns>List of available client certificates.</returns>
        public static IEnumerable<ClientCertificate> ListByUser(Guid brokerId, Guid userId)
        {
            List<ClientCertificate> list = new List<ClientCertificate>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@UserId", userId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ALL_USER_CLIENT_CERTIFICATE_List", parameters))
            {
                while (reader.Read())
                {
                    ClientCertificate item = LoadFromReader(reader);
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Create a client certificate record for user.
        /// </summary>
        /// <param name="principal">A principal of user making the request. This is for audit purpose.</param>
        /// <param name="certificateId">Certificate id.</param>
        /// <param name="userId">User id for the certificate.</param>
        /// <param name="description">Description of the certificate.</param>
        public static void Create(AbstractUserPrincipal principal, Guid certificateId, Guid userId, string description)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@CertificateId", certificateId),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@Description", description),
                new SqlParameter("@CreatedBy", principal.UserId)
            };

            StoredProcedureHelper.ExecuteNonQuery(principal.ConnectionInfo, "ALL_USER_CLIENT_CERTIFICATE_Insert", 3, parameters);
        }

        /// <summary>
        /// Create a PKCS #12 client certificate and record the entry in database.
        /// </summary>
        /// <param name="principal">A principal to create client certificate for.</param>
        /// <param name="description">Description of where client certificate will install.</param>
        /// <param name="password">Private password of certificate.</param>
        /// <returns>Client certificate.</returns>
        public static byte[] CreateClientCertificate(AbstractUserPrincipal principal, string description, string password)
        {
            EmployeeDB employee = EmployeeDB.RetrieveByUserId(principal.BrokerId, principal.UserId);

            Guid certificateId = Guid.NewGuid();

            byte[] bytes = ClientCertificateUtilities.GenerateClientCertificate(certificateId, password, employee.FullName, ClientCertificateType.User);

            Create(principal, certificateId, principal.UserId, description);

            return bytes;
        }

        /// <summary>
        /// Delete a client certificate record from the database.
        /// </summary>
        /// <param name="brokerId">Broker id of certificate.</param>
        /// <param name="userId">User id of certificate.</param>
        /// <param name="certificateId">Certificate id.</param>
        public static void Delete(Guid brokerId, Guid userId, Guid certificateId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@CertificateId", certificateId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ALL_USER_CLIENT_CERTIFICATE_Delete", 3, parameters);
        }

        /// <summary>
        /// Loads a certificate from a data reader.
        /// </summary>
        /// <param name="reader">The reader to load the data from.</param>
        /// <returns>A client certificate.</returns>
        /// <remarks>
        /// If you update this method, make sure that the callers are passing
        /// SqlDataReaders that have the required information. As of 12/29/15
        /// the only stored procedures that are used to create ClientCertificates
        /// are:
        ///    * ALL_USER_CLIENT_CERTIFICATE_List.
        ///    * ALL_USER_CLIENT_CERTIFICATE_ListAllForBroker.
        /// </remarks>
        internal static ClientCertificate LoadFromReader(DbDataReader reader)
        {
            ClientCertificate item = new ClientCertificate();
            item.CertificateId = (Guid)reader["CertificateId"];
            item.CreatedBy = (Guid)reader["CreatedBy"];
            item.CreatedByUserName = (string)reader["CreatedByUserName"];
            item.CreatedDate = (DateTime)reader["CreatedDate"];
            item.Description = (string)reader["Description"];
            return item;
        }
    }
}