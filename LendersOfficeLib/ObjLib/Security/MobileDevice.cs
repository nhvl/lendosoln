﻿namespace LendersOffice.Security
{
    /// <summary>
    /// A Mobile device is used to authenticate users using the mobile LendingQB App.  This is just a container for the data.
    /// </summary>
    public class MobileDevice
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MobileDevice"/> class.
        /// </summary>
        /// <param name="id">The id of the device.</param>
        /// <param name="deviceName">The name of the device.</param>
        /// <param name="createdDate">The date the device was created.</param>
        /// <param name="lastLoginDate">The date the device was last used to log into the system.</param>
        public MobileDevice(long id, string deviceName, string createdDate, string lastLoginDate)
        {
            this.Id = id;
            this.Name = deviceName;
            this.CreatedDate = createdDate;
            this.LastLoginDate = lastLoginDate;
        }

        /// <summary>
        /// Gets the id of the mobile device.
        /// </summary>
        /// <value>The id of the mobile device.</value>
        public long Id { get; private set; }

        /// <summary>
        /// Gets the name of the mobile device.
        /// </summary>
        /// <value>The name of the mobile device.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the date the device was created.
        /// </summary>
        /// <value>The date the device was created.</value>
        public string CreatedDate { get; private set; }

        /// <summary>
        /// Gets the date the user last logged into the system using the mobile device.
        /// </summary>
        /// <value>The date the user last logged into the system using the mobile device.</value>
        public string LastLoginDate { get; private set; }
    }
}
