﻿// <copyright file="BrokerGlobalIpWhiteList.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is a BrokerGlobalIpWhiteList class.</summary>
namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// OPM 125242 - Implement the Global IP Whitelist as part of Multi-Factor Authentication.
    /// </summary>
    [DataContract]
    public class BrokerGlobalIpWhiteList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerGlobalIpWhiteList"/> class.
        /// </summary>
        public BrokerGlobalIpWhiteList()
        {
            this.Id = -1;
        }

        /// <summary>
        /// Gets the identifier of an entry.
        /// </summary>
        /// <value>Identifier of an entry.</value>
        [DataMember]
        public int Id { get; private set; }

        /// <summary>
        /// Gets or sets the IP Address Range. Use * as wildcard. Example 11.12.13.*.
        /// </summary>
        /// <value>IP Address Range.</value>
        [DataMember]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the friendly description.
        /// </summary>
        /// <value>Friendly Description.</value>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets the last modified date.
        /// </summary>
        /// <value>Last modified date.</value>
        public DateTime LastModifiedDate { get; private set; }

        /// <summary>
        /// Gets the last modified date in preformatted string.
        /// </summary>
        /// <value>Last modified date in preformatted string.</value>
        [DataMember]
        public string LastModifiedDateDisplay
        {
            get
            {
                // Return the date as string.
                return Tools.GetDateTimeDescription(this.LastModifiedDate);
            }

            internal set 
            { 
            }
        }

        /// <summary>
        /// Gets the last modified login name.
        /// </summary>
        /// <value>Last modified login name.</value>
        [DataMember]
        public string LastModifiedLoginName { get; private set; }

        /// <summary>
        /// Gets the broker id that this entry belong to.
        /// </summary>
        /// <value>The broker id that this entry belong to.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Retrieve all IP whitelist for this broker.
        /// </summary>
        /// <param name="brokerId">BrokerId to return list of whitelist.</param>
        /// <returns>List of whitelist - Empty list if no entry.</returns>
        public static IEnumerable<BrokerGlobalIpWhiteList> ListByBrokerId(Guid brokerId)
        {
            SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", brokerId)
                };

            List<BrokerGlobalIpWhiteList> list = new List<BrokerGlobalIpWhiteList>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_GLOBAL_IP_WHITELIST_ListByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    BrokerGlobalIpWhiteList item = CreateFromReader(reader);
                    item.BrokerId = brokerId;

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieve individual record.
        /// </summary>
        /// <param name="brokerId">BrokerId to return entry of whitelist.</param>
        /// <param name="id">Id of the record.</param>
        /// <returns>Individual whitelist entry.</returns>
        public static BrokerGlobalIpWhiteList RetrieveById(Guid brokerId, int id)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Id", id)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_GLOBAL_IP_WHITELIST_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    BrokerGlobalIpWhiteList item = CreateFromReader(reader);
                    item.BrokerId = brokerId;

                    return item;
                }
            }

            throw new NotFoundException("Not Found", "Global Ip Whitelist is not found for brokerid=" + brokerId + ", id=" + id);
        }

        /// <summary>
        /// Remove record from database.
        /// </summary>
        /// <param name="brokerId">BrokerId to verify before remove record.</param>
        /// <param name="id">Id of the record.</param>
        public static void Delete(Guid brokerId, int id)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@Id", id),
                new SqlParameter("@BrokerId", brokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BROKER_GLOBAL_IP_WHITELIST_Delete", 2, parameters);
        }

        /// <summary>
        /// Create or Update entry to database.
        /// </summary>
        /// <param name="principal">Principal of current user.</param>
        public void Save(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                throw CBaseException.GenericException("Principal is required.");
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter parameterId = null;

            string storedProcedureName = string.Empty;
            if (this.Id == -1)
            {
                parameterId = new SqlParameter("@Id", SqlDbType.Int);

                parameterId.Direction = System.Data.ParameterDirection.Output;
                storedProcedureName = "BROKER_GLOBAL_IP_WHITELIST_Create";
            }
            else
            {
                if (principal.BrokerId != this.BrokerId)
                {
                    // 4/19/2014 dd - Should never reach here.
                    throw CBaseException.GenericException("Cross Broker Save detect");
                }

                parameterId = new SqlParameter("@Id", this.Id);
                storedProcedureName = "BROKER_GLOBAL_IP_WHITELIST_Update";
            }

            parameters.Add(parameterId);

            parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
            parameters.Add(new SqlParameter("@IpAddress", this.IpAddress));
            parameters.Add(new SqlParameter("@Description", this.Description));
            parameters.Add(new SqlParameter("@UserId", principal.UserId));

            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, storedProcedureName, 2, parameters);

            if (this.Id == -1)
            {
                this.Id = (int)parameterId.Value;
                this.BrokerId = principal.BrokerId;
            }

            this.LastModifiedDate = DateTime.Now;
            this.LastModifiedLoginName = principal.LoginNm;
        }

        /// <summary>
        /// Internal method for create class from reader.
        /// </summary>
        /// <param name="reader">SQL data reader contains necessary information.</param>
        /// <returns>Individual record.</returns>
        private static BrokerGlobalIpWhiteList CreateFromReader(DbDataReader reader)
        {
            BrokerGlobalIpWhiteList item = new BrokerGlobalIpWhiteList();
            item.Id = (int)reader["Id"];
            item.IpAddress = (string)reader["IpAddress"];
            item.Description = (string)reader["Description"];
            item.LastModifiedDate = (DateTime)reader["LastModifiedDate"];
            item.LastModifiedLoginName = (string)reader["LoginNm"];

            return item;
        }
    }
}