﻿namespace LendersOffice.Security
{
    using System;
    using LendersOffice.Common;
    using System.Data.SqlClient;
    using DataAccess;

    public class DriveSavedAuthentication
    {
        private AbstractUserPrincipal m_principal;

        private Guid m_userId;
        private Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);
        private LqbGrammar.DataTypes.EncryptionKeyIdentifier encryptionKeyId;

        private string m_originalUserName = "";
        private Lazy<string> lazyOriginalPassword = new Lazy<string>(() => string.Empty);

        public string UserName { get; set; }
        public string Password
        {
            get { return this.lazyPassword.Value ?? string.Empty; }
            set { this.lazyPassword = new Lazy<string>(() => value); }
        }

        public void Save()
        {
            if (this.m_originalUserName == this.UserName && this.lazyOriginalPassword.Value == this.lazyPassword.Value)
            {
                return; // 11/3/2010 dd - Do not invoke save if data does not change.
            }

            byte[] passwordBytes = null;
            if (this.encryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
            {
                passwordBytes = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", this.m_userId),
                                            new SqlParameter("@DriveUserName", this.UserName),
                                            new SqlParameter("@DrivePassword", EncryptionHelper.Encrypt(this.lazyPassword.Value)),
                                            new SqlParameter("@EncryptedDrivePassword", passwordBytes ?? new byte[0]),
                                        };

            StoredProcedureHelper.ExecuteNonQuery(m_principal.ConnectionInfo, "Drive_UpdateSavedAuthentication", 3, parameters);
        }

        private DriveSavedAuthentication(AbstractUserPrincipal principal)
        {
            m_principal = principal;
            m_userId = principal.UserId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(principal.ConnectionInfo, "Drive_RetrieveSavedAuthentication", parameters))
            {
                if (reader.Read())
                {
                    this.UserName = (string)reader["DriveUserName"];
                    var maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        this.encryptionKeyId = maybeEncryptionKeyId.Value;
                        byte[] encryptedPasswordBytes = (byte[])reader["EncryptedDrivePassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedPasswordBytes));
                    }
                    else
                    {
                        string encryptedPasswordString = (string)reader["DrivePassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPasswordString));
                    }

                    this.m_originalUserName = this.UserName;
                    this.lazyOriginalPassword = this.lazyPassword;
                }
                else
                {
                    throw new NotFoundException("Could not locate user", "Unable to find userid=" + m_userId);
                }
            }
        }

        public static DriveSavedAuthentication Retrieve(AbstractUserPrincipal principal)
        {
            return new DriveSavedAuthentication(principal);
        }
    }
}
