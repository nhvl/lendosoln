///
/// Author: David Dao
/// 
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Security.Principal;
using DataAccess;
using LendersOffice.Common;
using System.Web.Security;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    public sealed class InternalUserPrincipal : AbstractUserPrincipal 
    {

        AbstractUserPrincipal m_becomeUserPrincipal = null;

        public Guid GetBecomeUser()
        {
            return BecomeUserId;
        }


        public bool SetBecomeUser( Guid becomeUserId )        
        {
            if( becomeUserId == Guid.Empty )
            {
                BecomeUserId           = Guid.Empty;
                m_becomeUserPrincipal = null;
                return true;
            }

            //TODO : David will decide where the best location to check permission for DualPrinciple.
            // Thien temporary put code for checking permission at here
            InternalUserPermissions internalUserPermissions = new InternalUserPermissions(Permissions); ;
            bool allowDualPrinciple = internalUserPermissions.Can(E_InternalUserPermissions.BecomeUser);


            if (allowDualPrinciple)
            {
                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByUserId(becomeUserId, out brokerId);

                AbstractUserPrincipal becomeUserPrincipal = PrincipalFactory.Create(brokerId, becomeUserId, "B", true /* allowDuplicateLogin */, false /* isStoreToCookie */, initialPrincipalType: InitialPrincipalTypeT.Internal, initialUserId: UserId);
                if (becomeUserPrincipal != null)
                {
                    BecomeUserId = becomeUserId;
                    m_becomeUserPrincipal = becomeUserPrincipal;
                    return true;
                }
            }

            BecomeUserId          = Guid.Empty;
            m_becomeUserPrincipal = null;
            return false;
        }

        public AbstractUserPrincipal BecomeUserPrincipal
        {
            get 
            { 
                if( m_becomeUserPrincipal == null && BecomeUserId != Guid.Empty)
                {
                    // Thinking : do we need the thread-safe ?
                    SetBecomeUser( BecomeUserId );

                }

                return m_becomeUserPrincipal; 
            }
        }

        private InternalUserPrincipal(IIdentity identity, Guid userID, string firstName, string lastName, string permissions, Guid becomeUserId, Guid initialUserId, PostLoginTask postLoginTask)
            : base ( identity , new string[] {"I"}, userID, Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty, firstName, lastName, permissions,
            false, /* isOthersAllowedToEditUnderwriterAssignedFile */
            false /* isLOAllowedToEditProcessorAssignedFile */,
            Guid.Empty /* LpePriceGroupId */,
            false /* isRateLockedAtSubmission */,
            false /* hasLenderDefaultFeatures */,
            becomeUserId /* becomeUserId */,
            false /* isQuickPricerEnable */
            , false /* isPricingMultipleAppsSupported */
            , "" /* cookieSessionId */,
            Guid.Empty, /*pml broker id*/
            E_PmlLoanLevelAccess.Individual
            , "I"
            , E_ApplicationT.LendersOffice
            , null /* ipWhiteList */
            , null /* Teams */
            , false /* IsNewPmlUIEnabled */
            , false /* IsUsePml2AsQuickPricer */
            , E_PortalMode.Blank /*User's last portal mode*/
            , Guid.Empty /* Mini-Correspondent Branch Id */
            , Guid.Empty /* Correspondent Branch Id */
            , InitialPrincipalTypeT.Internal // initial principal type, from LoAdmin
            , initialUserId // initial user id, from LoAdmin
            , postLoginTask
            )
        {
            SetBecomeUser( becomeUserId );
        }

        public override bool IsLQBStaff()
        {
            return true;
        }

        public override bool IsInRole(string role)
        {
            if (role == "IB" && m_becomeUserPrincipal != null)
                return true;

            // los/admin requires Administrator 
            if (role == "Administrator" && m_becomeUserPrincipal != null)
            {
                if (m_becomeUserPrincipal.IsInRole(role))
                    return true;
            }

            return base.IsInRole(role);
        }

        public static AbstractUserPrincipal CreatePrincipal(string storedProcedureName, IEnumerable<SqlParameter> parameters, Guid becomeUserId, Guid initialUserId, PostLoginTask postLoginTask) 
        {
            // 7/21/2005 dd - Adding new columns need to make sure all following store procedures have same result set.
            // RetrieveInternalUserCredentialByUserId
            // RetrieveInternalUserCredentialByUserNamePassword

            // 7/28/2014 dd - Internal user accounts are always store in default loan database.
            DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;

            using (var reader = StoredProcedureHelper.ExecuteReader(connectionInfo, storedProcedureName, parameters)) 
            {
                if (reader.Read()) 
                {
                    Guid userId = (Guid) reader["UserId"];
                    string loginNm = (string) reader["LoginNm"];
                    string permissions = (string) reader["Permissions"];
                    string firstName = (string) reader["FirstNm"];
                    string lastName = (string) reader["LastNm"];


                    AbstractUserPrincipal principal = new InternalUserPrincipal(new GenericIdentity(loginNm), userId, firstName, lastName, permissions, becomeUserId, initialUserId, postLoginTask);
                    RequestHelper.StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                    System.Threading.Thread.CurrentPrincipal = principal ;

                    return principal;
                }
            }
            return null;
        }

		public static AbstractUserPrincipal CreatePrincipalWithFailureType(string loginNm, string password, out PrincipalFactory.E_LoginProblem loginProblem, LoginSource loginSource) 
		{
			// 7/21/2005 dd - Adding new columns need to make sure all following store procedures have same result set.
			// RetrieveInternalUserCredentialByUserId
			// RetrieveInternalUserCredentialByUserNamePassword

			loginProblem = PrincipalFactory.E_LoginProblem.None;

			//Check first if they're allowed to log in
			DateTime nextAvailableLoginTime = GetNextAvailableLoginTime(loginNm);
			if(nextAvailableLoginTime != SmallDateTime.MinValue)
			{
				if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))				
				{
					loginProblem = PrincipalFactory.E_LoginProblem.IsLocked;
					return null;
				}

				if(nextAvailableLoginTime > DateTime.Now)
				{
					loginProblem = PrincipalFactory.E_LoginProblem.NeedsToWait;
					return null;
				}
			}

            // 7/28/2014 dd - Internal user accounts are always store in default loan database.
            DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", loginNm),
                                        };

            // OPM 194431 - Find user by LoginNm and check if password correct.
			using (var reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "RetrieveInternalUserCredentialByUserName", parameters)) 
			{
				if (reader.Read()) 
				{
					Guid userId = (Guid) reader["UserId"];
					loginNm = (string) reader["LoginNm"];
					string permissions = (string) reader["Permissions"];
					string firstName = (string) reader["FirstNm"];
					string lastName = (string) reader["LastNm"];
                    string passwordHash = (string)reader["PasswordHash"];
                    string passwordSalt = (string)reader["PasswordSalt"];
                    bool isUpdatedPassword = (bool)reader["IsUpdatedPassword"];

                    bool isPasswordCorrect = false;
                    if (isUpdatedPassword)
                    {
                        isPasswordCorrect = EncryptionHelper.ValidatePBKDF2Hash(password, passwordHash, passwordSalt);
                    }
                    else
                    {
                        isPasswordCorrect = passwordHash == Tools.HashOfPassword(password);
                    }

                    if (isPasswordCorrect)
                    {
                        AbstractUserPrincipal principal = new InternalUserPrincipal(new GenericIdentity(loginNm), userId, firstName, lastName, permissions, Guid.Empty /* becomeUserId */, userId, PostLoginTask.Login);

                        RequestHelper.StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                        System.Threading.Thread.CurrentPrincipal = principal;
                        return principal;
                    }
				}
			}

			loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;

			int loginFailureCount = IncrementLoginFailureCount(loginNm);

            var logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(ConstAppDavid.SystemBrokerGuid, loginNm, firstNm: string.Empty, lastNm: string.Empty, initialPrincipalType: InitialPrincipalTypeT.Internal, type: "I");

            if (loginFailureCount >= 50)
            {
                LockAccount(loginNm, logPrincipal);
                loginProblem = PrincipalFactory.E_LoginProblem.IsLocked;
            }
            else if (loginFailureCount < 10)
            {
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
            }
            else
            {
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                AddTimeToLoginBlock(loginNm, Math.Floor((double)(loginFailureCount / 10)));
                loginProblem = PrincipalFactory.E_LoginProblem.InvalidAndWait;
            }

			return null;
		}

		private static void LockAccount(string loginNm, AbstractUserPrincipal logPrincipal)
		{
            // 7/28/2014 dd - Internal user accounts are always store in default loan database.
            DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;

			SqlParameter[] parameters = {
											new SqlParameter("@LoginNm", loginNm),
											new SqlParameter("@Type", "I"),
											new SqlParameter("@BrokerPmlSiteId", Guid.Empty),
											new SqlParameter("@NextAvailableLoginTime", SmallDateTime.MaxValue)
										};
			StoredProcedureHelper.ExecuteNonQuery(connectionInfo, "SetLoginBlockExpirationDate",1, parameters);

            SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
        }

		private static void AddTimeToLoginBlock(string loginNm, double minutesToAdd)
		{
            if (minutesToAdd < 0)
            {
                return;
            }

            // 7/28/2014 dd - Internal user accounts are always store in default loan database.
            DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;

			DateTime nextAvailableTime = DateTime.Now.AddMinutes(minutesToAdd);
			SqlParameter[] parameters = {
											new SqlParameter("@LoginNm", loginNm),
											new SqlParameter("@Type", "I"),
											new SqlParameter("@BrokerPmlSiteId", Guid.Empty),
											new SqlParameter("@NextAvailableLoginTime", nextAvailableTime)
										};
			StoredProcedureHelper.ExecuteNonQuery(connectionInfo, "SetLoginBlockExpirationDate", 1,parameters);
		}

		private static int IncrementLoginFailureCount(string loginNm)
		{
			int loginFailureCount = 0;

            SqlParameter[] parameters = { 
                                                new SqlParameter("@LoginNm", loginNm),
                                                new SqlParameter("@Type", "I"), 
                                                new SqlParameter("@BrokerPmlSiteId", Guid.Empty)};

            // 7/28/2014 dd - Internal user accounts are always store in default loan database.
            DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;

            using (var reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "UpdateLoginFailureCount", parameters))
            {
                if (reader.Read())
                {
                    loginFailureCount = (int)reader["LoginFailureCount"];
                }
            }

			return loginFailureCount;
		}

		private static DateTime GetNextAvailableLoginTime(string loginNm)
		{
			DateTime nextAvailableLoginTime = SmallDateTime.MinValue;
			
			try
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@LoginNm", loginNm)
                                            };

                // 7/28/2014 dd - Internal user accounts are always store in default loan database.
                DbConnectionInfo connectionInfo = DbConnectionInfo.DefaultConnectionInfo;

				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "GetLoginBlockExpirationDateByLoginNm", parameters )) 
				{
                    if (reader.Read())
                    {
                        nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                    }
				}
			}
			catch{}
			return nextAvailableLoginTime;
		}

    }

}
