﻿namespace LendersOffice.Security
{
    /// <summary>
    /// Represents the type of client certificate being created.
    /// </summary>
    public enum ClientCertificateType
    {
        /// <summary>
        /// The client certificate is for a user.
        /// </summary>
        User = 0,

        /// <summary>
        /// The client certificate is for a lender.
        /// </summary>
        Lender = 1,

        /// <summary>
        /// The client certificate is for a vendor.
        /// </summary>
        Vendor = 2
    }
}
