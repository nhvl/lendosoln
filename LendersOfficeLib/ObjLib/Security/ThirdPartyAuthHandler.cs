﻿// <copyright file="ThirdPartyAuthHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   10/31/2014 3:26:22 PM 
// </summary>
namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Linq;
    using System.Web;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Responds to Third party site authentication request. If the user is logged in it will send the user 
    /// to the requesting third party otherwise it will redirect to the LQB login page.
    /// </summary>
    public class ThirdPartyAuthHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is usable in multiple threads at the same time.
        /// </summary>
        /// <value>Whether the http handler can be used in multiple threads.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Gets an authentication ticket for the third party site.
        /// </summary>
        /// <param name="encryptionId">The encryption key id to encrypt the content of authentication ticket.</param>
        /// <returns>A base 64 authentication ticket for the third party site.</returns>
        public static string GetSafeBase64AuthTicket(string encryptionId)
        {
            string firstName = PrincipalFactory.CurrentPrincipal.FirstName.Replace("|", string.Empty);
            string lastName = PrincipalFactory.CurrentPrincipal.LastName.Replace("|", string.Empty);
            string loginName = PrincipalFactory.CurrentPrincipal.LoginNm.ToString();

            string userId = PrincipalFactory.CurrentPrincipal.UserId.ToString();
            var hashedIdBase64 = EncryptionHelper.ComputeSHA256Hash(userId);

            string unencryptedData = string.Format("{0}|{1}|{2}|{3}|{4}", DateTime.Now.ToString(), firstName, lastName, hashedIdBase64, loginName);

            byte[] encryptedData = null;

            if (string.IsNullOrEmpty(encryptionId))
            {
                // dd - 1/19/2019 - if encryptionId is not pass in then use default encryption key for 3rd party.
                // Ideally we should have each 3rd party invoke this method to pass in unique encryption id.
                encryptedData = EncryptionHelper.EncryptThirdParty(unencryptedData);
            }
            else
            {
                var keyId = EncryptionKeyIdentifier.Create(encryptionId);
                if (!keyId.HasValue)
                {
                    throw DataAccess.CBaseException.GenericException("Invalid encryptionId");
                }

                encryptedData = EncryptionHelper.EncryptString(keyId.Value, unencryptedData);
            }

            return HttpServerUtility.UrlTokenEncode(encryptedData);
        }

        /// <summary>
        /// Processes an http request for the handler.
        /// </summary>
        /// <param name="context">The http context for the request.</param>
        public void ProcessRequest(HttpContext context)
        {
            string passedInId = RequestHelper.GetSafeQueryString("id");

            if (string.IsNullOrEmpty(passedInId))
            {
                this.Send404Response(context.Response);
                return;
            }

            var sites = ConstStage.ThirdPartyReferrers;
            var selectedReferrer = sites.FirstOrDefault(p => p.Item2.Equals(passedInId));

            if (selectedReferrer == null)
            {
                this.Send404Response(context.Response);
                return;
            }

            string url; 
            if (!(context.User is BrokerUserPrincipal))
            {
                url = ConstSite.LoginUrl + "?tp=" + selectedReferrer.Item2;
            }
            else
            {
                string base64AuthTicket = ThirdPartyAuthHandler.GetSafeBase64AuthTicket(selectedReferrer.Item3);
                url = selectedReferrer.Item1 + base64AuthTicket; 
            }

            context.Response.Redirect(url);
        }

        /// <summary>
        /// Returns a 404 Response.
        /// </summary>
        /// <param name="response">The http response to return the 404 to.</param>
        private void Send404Response(HttpResponse response)
        {
            response.StatusCode = 404;
            response.End();
        }
    }
}