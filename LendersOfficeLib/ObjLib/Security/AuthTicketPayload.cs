﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the internal ticket data of our auth tickets.
    /// </summary>
    [DataContract]
    public class AuthTicketPayload
    {
        /// <summary>
        /// Gets or sets the broker identifier.
        /// </summary>
        [DataMember(Name = "b", IsRequired = true)]
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the type of principal this auth ticket represents.
        /// </summary>
        [DataMember(Name = "p", IsRequired = true)]
        public string PrincipalType { get; set; }

        /// <summary>
        /// Gets or sets the user id the ticket belongs to.
        /// </summary>
        [DataMember(Name = "u", IsRequired = true)]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the creation time.
        /// </summary>
        [DataMember(Name = "t", IsRequired = false)]
        public long Ticks { get; set; }

        /// <summary>
        /// Gets or sets the billing provider id if needed.
        /// </summary>
        [DataMember(Name = "bp", IsRequired = false)]
        public string BillingProviderId { get; set; }

        /// <summary>
        /// Gets or sets the billing service type if needed.
        /// </summary>
        [DataMember(Name = "bs", IsRequired = false)]
        public string BillingServiceType { get; set; }

        /// <summary>
        /// Gets or sets the loan reference number if needed.
        /// </summary>
        [DataMember(Name = "l", IsRequired = false)]
        public string LoanReferenceNumber { get; set; }
    }
}
