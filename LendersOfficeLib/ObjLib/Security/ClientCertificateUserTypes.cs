﻿namespace LendersOffice.ObjLib.Security
{
    /// <summary>
    /// The user types available for client certificates.
    /// </summary>
    public enum ClientCertificateUserTypes
    {
        /// <summary>
        /// Certificate is meant for LQB users only.
        /// </summary>
        Lqb = 0,

        /// <summary>
        /// Certificate is meant for TPO users only.
        /// </summary>
        Tpo = 1,

        /// <summary>
        /// Certificate is meant for any type of user.
        /// </summary>
        Any = 2
    }
}
