﻿using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.DocumentVendor;
using System.Data;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    public abstract class DocMagicAuthentication : LendersOffice.Integration.DocumentVendor.IDocumentVendorCredentials
    {
        protected Guid m_userId;
        protected Guid m_brokerId;
        protected Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);
        private string m_CustomerID = "";
        private string m_UserName = "";
        protected Lazy<string> lazyOriginalPassword = new Lazy<string>(() => string.Empty);
        protected string m_origCustomerID;
        protected string m_origUserName;

        public string CustomerId
        {
            get
            {
                return m_CustomerID;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    m_CustomerID = value;
                }
                else
                {
                    m_CustomerID = value.TrimWhitespaceAndBOM();
                }
            }
        }
        public string UserName
        {
            get
            {
                return m_UserName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    m_UserName = value;
                }
                else
                {
                    m_UserName = value.TrimWhitespaceAndBOM();
                }
            }
        }
        public string Password
        {
            get
            {
                try
                {
                    return this.lazyPassword.Value ?? string.Empty;
                }
                catch (FormatException)
                {
                    Tools.LogBug("User had a bad encrypted password, probably from setting too long a password");
                    return "";
                }
            }
            set
            {
                if (value == ConstAppDavid.FakePasswordDisplay)
                {
                    throw new ArgumentException("can't assign a password of " + ConstAppDavid.FakePasswordDisplay);
                }
                else if (value?.Length > 50)
                {
                    throw new ArgumentException("can't assign a password such that it's encrypted form is longer than 50 characters.");
                }

                this.lazyPassword = new Lazy<string>(() => value);
            }
        }
        public bool UserEnabled
        {
            get
            {
                return VendorCredentials.GetUserCredentialsOnly(m_userId, m_brokerId, Guid.Empty).UserEnabled;
            }
        }

        public void SaveUserCredentials()
        {
            Save();
        }

        public abstract void Save();

        public static DocMagicAuthentication Current
        {
            get
            {
                var bp = BrokerUserPrincipal.CurrentPrincipal;

                //This happens when we're being created by the LOAdmin employee editor
                if (bp == default(BrokerUserPrincipal))
                {
                    return DocMagicSavedAuthentication.Current;
                }
                BrokerDB broker = BrokerDB.RetrieveById(bp.BrokerId);
                if (bp.BillingVersion == E_BrokerBillingVersion.PerTransaction && broker.IsEnablePTMDocMagicSeamlessInterface)
                {
                    return DocMagicSavedAuthenticationForPTM.Current;
                }
                else
                {
                    return DocMagicSavedAuthentication.Current;
                }
            }

        }
    }
    
    public class DocMagicSavedAuthentication : DocMagicAuthentication
    {
        private EncryptionKeyIdentifier encryptionKeyId;

        public DocMagicSavedAuthentication(Guid userId, Guid brokerId)
        {
            m_userId = userId;
            m_brokerId = brokerId;
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId)
                                        };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "RetrieveDocMagicSavedAuthentication", parameters)) 
            {
                if (reader.Read())
                {
                    CustomerId = (string)reader["DocMagicCustomerId"];
                    UserName = (string)reader["DocMagicUserName"];
                    var maybeEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        this.encryptionKeyId = maybeEncryptionKeyId.Value;
                        byte[] passwordBytes = (byte[])reader["EncryptedDocMagicPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, passwordBytes));
                    }
                    else
                    {
                        string encryptedPasswordString = (string)reader["DocMagicPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPasswordString));
                    }

                    m_origCustomerID = CustomerId;
                    this.lazyOriginalPassword = this.lazyPassword;
                    m_origUserName = UserName;
                }
            }
        }
        public static new DocMagicSavedAuthentication Current
        {
            get
            {
                var currPrincipal = PrincipalFactory.CurrentPrincipal;
                return new DocMagicSavedAuthentication(currPrincipal.UserId, currPrincipal.BrokerId);
            }
        }

        public override void Save()
        {
            if (string.Empty == UserName)
            {
                if (string.Empty != Password)
                {
                    throw new ArgumentException("DocMagic login info may not have a password but no username.");
                }
            }
            else // if string.Empty != UserName
            {
                if (string.Empty == Password)
                {
                    throw new ArgumentException("DocMagic login info may not have a username but no password.");
                }
            }

            if (m_origUserName == UserName && m_origCustomerID == CustomerId && this.lazyOriginalPassword.Value == this.lazyPassword.Value)
            {
                return;
            }

            byte[] encryptedPasswordBytes = null;
            if (this.encryptionKeyId != default(EncryptionKeyIdentifier))
            {
                encryptedPasswordBytes = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId),
                                            new SqlParameter("@DocMagicCustomerId", CustomerId),
                                            new SqlParameter("@DocMagicUserName", UserName),
                                            new SqlParameter("@DocMagicPassword", EncryptionHelper.Encrypt(this.lazyPassword.Value)),
                                            new SqlParameter("@EncryptedDocMagicPassword", encryptedPasswordBytes ?? new byte[0]),
                                        };

            StoredProcedureHelper.ExecuteNonQuery(this.m_brokerId, "SaveDocMagicAuthenticationForNonPTM", 3, parameters);
        }
    }

    // SK - DO NOT Attempt to downcast this to a regular DocMagicSavedAuthentication, as they load from different tables, and won't cast anyway.
    public sealed class DocMagicSavedAuthenticationForPTM: DocMagicAuthentication
    {
        public DocMagicSavedAuthenticationForPTM(Guid UserId, Guid BranchId, Guid BrokerId)
        {
            m_userId = UserId;
            m_brokerId = BrokerId;
            BrokerDB broker = BrokerDB.RetrieveById(BrokerId);
            
            if (broker.BillingVersion != E_BrokerBillingVersion.PerTransaction)
            {
                throw new PermissionException("This feature or one of its subfeatures is not enabled.", 
                    "Don't have permission to use per transaction billing credentials when broker isn't using per transaction billing.");
            }

            SqlParameter[] parameters = {
                                new SqlParameter("@UserId", UserId),
                                new SqlParameter("@BranchId", BranchId),
                                new SqlParameter("@BrokerId", BrokerId)
                            };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "GetDocMagicLoginInfoForPerTransactionBilling", parameters))
            {
                if (reader.Read())
                {
                    CustomerId = (string)reader["DocMagicCustomerId"];
                    UserName = (string)reader["DocMagicUserName"];
                    string encryptedPassword = (string)reader["DocMagicPassword"];
                    this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPassword));

                    m_origCustomerID = CustomerId;
                    this.lazyOriginalPassword = this.lazyPassword;
                    m_origUserName = UserName;
                }
            }
        }
        private DocMagicSavedAuthenticationForPTM(Guid UserId, Guid BrokerId) //For loading from User level only
        {
            m_userId = UserId;
            m_brokerId = BrokerId;
            BrokerDB broker = BrokerDB.RetrieveById(BrokerId);

            if (broker.BillingVersion != E_BrokerBillingVersion.PerTransaction)
            {
                throw new PermissionException("don't have permission to create this object in this way. when not using per transaction billing.");
            }

            var ds = StoredProcedureHelper.ExecuteDataSet(BrokerId, "DOCUMENT_VENDOR_GetCredentialLevelsNew", new[] {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@BranchId", Guid.Empty),
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@VendorId", Guid.Empty)
            });
            var dt = ds.Tables[0]; //Only load from user level

            if (dt.Rows.Count > 0)
            {
                var row = dt.Rows[0];
                UserName = Convert.ToString(row["DocVendorUserName"]);
                string encryptedPassword = Convert.ToString(row["DocVendorPassword"]);
                this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPassword));
                CustomerId = Convert.ToString(row["DocVendorCustomerId"]);

                m_origCustomerID = CustomerId;
                this.lazyOriginalPassword = this.lazyPassword;
                m_origUserName = UserName;
            }
        }
        public static DocMagicSavedAuthenticationForPTM GetUserCredentialsOnly(Guid UserId, Guid BrokerId)
        {
            return new DocMagicSavedAuthenticationForPTM(UserId, BrokerId);
        }

        public static new DocMagicSavedAuthenticationForPTM Current
        {
            get
            {
                var bp = PrincipalFactory.CurrentPrincipal;
                return new DocMagicSavedAuthenticationForPTM(bp.UserId, bp.BranchId, bp.BrokerId);
            }
        }
        public override void Save()
        {
            if (string.Empty == UserName)
            {
                if (string.Empty != Password)
                {
                    throw new ArgumentException("DocMagic login info may not have a password but no username.");
                }
            }
            else // if String.Empty != UserName
            {
                if (string.Empty == Password)
                {
                    throw new ArgumentException("DocMagic login info may not have a username but no password.");
                }
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId),
                                            new SqlParameter("@DocMagicCustomerId", CustomerId),
                                            new SqlParameter("@DocMagicUserName", UserName),
                                            new SqlParameter("@DocMagicPassword", EncryptionHelper.Encrypt(this.lazyPassword.Value))
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.m_brokerId, "UpdateDocMagicSavedAuthentication", 3, parameters);
        }
    }
}
