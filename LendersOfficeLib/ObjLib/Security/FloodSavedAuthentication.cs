﻿using System;
using System.Data;
using System.Data.SqlClient;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Security
{
    public class FloodSavedAuthentication
    {
        private Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);
        private LqbGrammar.DataTypes.EncryptionKeyIdentifier encryptionKeyId;

        private string m_sOriginalUserName = string.Empty;
        private Lazy<string> lazyOriginalPassword = new Lazy<string>(() => string.Empty);

        public string UserName { get; set; }
        public string Password
        {
            get { return this.lazyPassword.Value ?? string.Empty; }
            set { this.lazyPassword = new Lazy<string>(() => value); }
        }

        public Guid BrokerId { get; private set; }
        public Guid UserId { get; private set; }

        public void Save()
        {
            if (this.m_sOriginalUserName == this.UserName && this.lazyOriginalPassword.Value == this.lazyPassword.Value)
            {
                return; // Do not invoke save unless the data has changed.
            }

            byte[] passwordBytes = null;
            if (this.encryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
            {
                passwordBytes = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", this.UserId),
                                            new SqlParameter("@FloodLoginNm", this.UserName),
                                            new SqlParameter("@FloodPassword", EncryptionHelper.Encrypt(this.lazyPassword.Value)),
                                            new SqlParameter("@EncryptedFloodPassword", passwordBytes ?? new byte[0]),
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "Flood_UpdateSavedAuthentication", 3, parameters);
        }

        public FloodSavedAuthentication(Guid brokerId, Guid userId)
        {
            this.UserId = userId;
            this.BrokerId = brokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", this.UserId)
                                        };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "Flood_RetrieveSavedAuthentication", parameters))
            {
                if (reader.Read())
                {
                    this.UserName = (string)reader["FloodLoginNm"];
                    var maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        this.encryptionKeyId = maybeEncryptionKeyId.Value;
                        byte[] encryptedPasswordBytes = (byte[])reader["EncryptedFloodPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedPasswordBytes));
                    }
                    else
                    {
                        string encryptedPasswordString = (string)reader["FloodPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPasswordString));
                    }

                    this.m_sOriginalUserName = this.UserName;
                    this.lazyOriginalPassword = this.lazyPassword;
                }
                else
                {
                    throw new NotFoundException("Could not locate user", "Unable to find userid=" + userId);
                }
            }
        }
    }
}
