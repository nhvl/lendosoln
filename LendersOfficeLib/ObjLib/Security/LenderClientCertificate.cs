﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a client certificate that is made via the broker editor.
    /// </summary>
    public class LenderClientCertificate
    {
        /// <summary>
        /// The suffix for the notes file db key.
        /// </summary>
        private const string NotesFileDbKeySuffix = "_lccNotes";

        /// <summary>
        /// Lazy loader for the notes property. Since it is in FileDB, we don't want to load unless needed.
        /// </summary>
        private Lazy<string> lazyNotes;

        /// <summary>
        /// Lazy loader for the user login since we have to load up EmployeeDB for it.
        /// </summary>
        private Lazy<string> lazyUserLogin;

        /// <summary>
        /// Initializes a new instance of the <see cref="LenderClientCertificate"/> class.
        /// This constructor assumes that this is a completely new certificiate since no certificate id was passed in.
        /// This will make a new LenderClientCertificate with a random guid for the certificate id, notes file db key, and created date.
        /// Use the static functions if you want to load an existing LenderClientCertificate.
        /// </summary>
        /// <param name="brokerId">The id of the broker this certificate belongs to.</param>
        /// <param name="description">The description for the certificate.</param>
        /// <param name="notes">The notes for this certificate.</param>
        /// <param name="level">The level for this certificate.</param>
        /// <param name="userType">The user type for this certificate.</param>
        /// <param name="userId">The user id if the cert is on the Individual level.</param>
        /// <param name="groupIds">The list of group ids.</param>
        private LenderClientCertificate(Guid brokerId, string description, string notes, ClientCertificateLevelTypes level, ClientCertificateUserTypes userType, Guid? userId, IEnumerable<Guid> groupIds)
        {
            this.CertificateId = Guid.NewGuid();
            this.BrokerId = brokerId;
            this.IsRevoked = false;
            this.Description = description;
            this.Notes = notes;
            this.NotesFileDbKey = Guid.NewGuid();
            this.Level = level;
            this.UserType = userType;
            this.CreatedDate = DateTime.Now;
            this.RevokedDate = null;
            this.AssociatedUserId = userId;
            this.GroupIds = groupIds;

            if (this.AssociatedUserId.HasValue)
            {
                this.lazyUserLogin = new Lazy<string>(() => EmployeeDB.RetrieveUserLoginByUserId(brokerId, userId.Value));
            }
            else
            {
                this.lazyUserLogin = new Lazy<string>(() => string.Empty);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LenderClientCertificate"/> class.
        /// Meant for loading up a certificate that has already been made.
        /// </summary>
        /// <param name="certId">The certificate id.</param>
        /// <param name="brokerId">The broker id of the broker this certificate belongs to.</param>
        /// <param name="isRevoked">Whether this certificate is revoked or not.</param>
        /// <param name="description">The description for the certificate.</param>
        /// <param name="notesFileDbKey">The file db key for the notes.</param>
        /// <param name="level">The level of the certificate.</param>
        /// <param name="userType">The user type for the certificate.</param>
        /// <param name="createdDate">The created date for the certificate.</param>
        /// <param name="revokedDate">The revoked date for the certificate.</param>
        /// <param name="associatedUserId">The user id if the certificate is at the Individual level.</param>
        private LenderClientCertificate(
            Guid certId,
            Guid brokerId,
            bool isRevoked,
            string description,
            Guid? notesFileDbKey,
            ClientCertificateLevelTypes level,
            ClientCertificateUserTypes userType,
            DateTime createdDate,
            DateTime? revokedDate,
            Guid? associatedUserId)
        {
            this.CertificateId = certId;
            this.BrokerId = brokerId;
            this.IsRevoked = isRevoked;
            this.Description = description;
            this.Level = level;
            this.UserType = userType;
            this.CreatedDate = createdDate;
            this.RevokedDate = revokedDate;
            this.AssociatedUserId = associatedUserId;

            if (!notesFileDbKey.HasValue || notesFileDbKey.Value == Guid.Empty)
            {
                this.lazyNotes = new Lazy<string>(() => string.Empty);
            }
            else
            {
                this.lazyNotes = new Lazy<string>(() => FileDBTools.ReadDataText(E_FileDB.Normal, GetFullNotesFileDbKey(certId, notesFileDbKey.Value)));
            }

            if (!associatedUserId.HasValue)
            {
                this.lazyUserLogin = new Lazy<string>(() => string.Empty);
            }
            else
            {
                this.lazyUserLogin = new Lazy<string>(() => EmployeeDB.RetrieveUserLoginByUserId(brokerId, associatedUserId.Value));
            }
        }

        /// <summary>
        /// Gets the certificate id.
        /// </summary>
        /// <value>The certificate id.</value>
        public Guid CertificateId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the broker id of the broker this client certificate belongs to.
        /// </summary>
        /// <value>The broker id of the broker this client certificate belongs to.</value>
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the description for the lender client certificate.
        /// </summary>
        /// <value>The description for lender client certificate.</value>
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the level for the lender client certificate.
        /// </summary>
        /// <value>The level for the lender client certificate.</value>
        public ClientCertificateLevelTypes Level
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user type for the lender client certificate.
        /// </summary>
        /// <value>The user type for the lender client certificate.</value>
        public ClientCertificateUserTypes UserType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the string representation of the user type.
        /// </summary>
        /// <value>The string representation of the user type.</value>
        public string UserTypeAsString
        {
            get
            {
                switch (this.UserType)
                {
                    case ClientCertificateUserTypes.Any:
                        return "Any";
                    case ClientCertificateUserTypes.Lqb:
                        return "LQB";
                    case ClientCertificateUserTypes.Tpo:
                        return "TPO";
                    default:
                        throw new UnhandledEnumException(this.UserType);
                }
            }
        }

        /// <summary>
        /// Gets the user login name. Lazy loaded.
        /// </summary>
        /// <value>The user login name.</value>
        public string AssociatedUserLogin
        {
            get
            {
                return this.lazyUserLogin.Value;
            }
        }

        /// <summary>
        /// Gets or sets the user id of the associated user if the certificate is set to the Individual level.
        /// </summary>
        /// <value>The user id of the associated user if the certificate is set to the Individual level.</value>
        public Guid? AssociatedUserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the notes set for this certificate.
        /// </summary>
        /// <value>The notes set for this certificate.</value>
        public string Notes
        {
            get
            {
                return this.lazyNotes.Value;
            }

            set
            {
                this.lazyNotes = new Lazy<string>(() => value);
            }
        }

        /// <summary>
        /// Gets the file db key for the notes.
        /// </summary>
        /// <value>The file db key for the notes.</value>
        public Guid? NotesFileDbKey
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this certificate is revoked.
        /// </summary>
        /// <value>A value indicating whether or not this certificate is revoked.</value>
        public bool IsRevoked
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the date when this certificate was created.
        /// </summary>
        /// <value>When this certificate was created.</value>
        public DateTime CreatedDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the date that this certificate was last revoked.
        /// </summary>
        /// <value>The date that this certificate was last revoked.</value>
        public DateTime? RevokedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a list of the associated group ids.
        /// </summary>
        /// <value>A list of the associated group ids.</value>
        public IEnumerable<Guid> GroupIds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a dictionary of group ids to Group objects.
        /// </summary>
        /// <value>A dictionary of group ids to Group objects.</value>
        public Dictionary<Guid, Group> Groups
        {
            get;
            private set;
        }

        /// <summary>
        /// Converts the level enum to a string.
        /// </summary>
        /// <param name="level">The level enum to convert.</param>
        /// <returns>A string representation of the level enum.</returns>
        public static string GetLevelAsString(ClientCertificateLevelTypes level)
        {
            switch (level)
            {
                case ClientCertificateLevelTypes.BranchGroup:
                    return "Branch Group";
                case ClientCertificateLevelTypes.Corporate:
                    return "Corporate";
                case ClientCertificateLevelTypes.EmployeeGroup:
                    return "Employee Group";
                case ClientCertificateLevelTypes.Individual:
                    return "Individual";
                case ClientCertificateLevelTypes.OcGroup:
                    return "OC Group";
                default:
                    throw new UnhandledEnumException(level);
            }
        }

        /// <summary>
        /// Constructs the auto expire cache key for the certificate fields.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="randomGuidUsed">A random guid used as part of the key.</param>
        /// <returns>The full key used for storing cert info in the auto expire cache.</returns>
        public static string ConstructFieldsCacheKey(Guid userId, Guid randomGuidUsed)
        {
            return $"{randomGuidUsed.ToString()}_{userId.ToString()}_lenderCert";
        }

        /// <summary>
        /// Verifies if the broker client certificate is valid for the given principal.
        /// </summary>
        /// <param name="certificateId">The certificate to check.</param>
        /// <param name="principal">The principal to check against.</param>
        /// <returns>True if valid, false otherwise.</returns>
        public static bool VerifyBrokerClientCertificate(Guid certificateId, AbstractUserPrincipal principal)
        {
            Guid brokerId = principal.BrokerId;

            LenderClientCertificate certificate = GetLenderClientCertificate(brokerId, certificateId);
            if (certificate == null)
            {
                return false;
            }

            if (certificate.IsRevoked)
            {
                return false;
            }

            if (certificate.Level == ClientCertificateLevelTypes.Corporate)
            {
                /* 
                   Return true if: 
                   User belongs to the lender => Already verified 
                   The certificate user type matches the user's. Or the certificate user type is ANY.
                */

                return CheckUserType(certificate.UserType, principal.Type);
            }
            else if (certificate.Level == ClientCertificateLevelTypes.Individual)
            {
                /* Conditions for true:
                    User belongs to broker => Already verified
                    Cert user type must match the user's
                    The cert's associated user must match the user
                */

                return CheckUserType(certificate.UserType, principal.Type) && certificate.AssociatedUserId == principal.UserId;
            }
            else if (certificate.Level == ClientCertificateLevelTypes.BranchGroup || certificate.Level == ClientCertificateLevelTypes.EmployeeGroup ||
                     certificate.Level == ClientCertificateLevelTypes.OcGroup)
            {
                /* Conditions for true:
                    User belongs to the broker => already verified
                    User type matches cert user type
                    User belongs to one of the associated groups
                */

                if (!CheckUserType(certificate.UserType, principal.Type))
                {
                    return false;
                }

                if (certificate.Level == ClientCertificateLevelTypes.EmployeeGroup)
                {
                    if (!string.Equals(principal.Type, "b", StringComparison.OrdinalIgnoreCase))
                    {
                        return false;
                    }

                    if (CheckGroupsAgainstCertificateGroups(certificate, GroupDB.ListInclusiveGroupForEmployee(brokerId, principal.EmployeeId)))
                    {
                        return true;
                    }
                }
                else if (certificate.Level == ClientCertificateLevelTypes.OcGroup)
                {
                    if (!string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase))
                    {
                        return false;
                    }

                    if (CheckGroupsAgainstCertificateGroups(certificate, GroupDB.ListInclusiveGroupForPmlBroker(brokerId, principal.PmlBrokerId)))
                    {
                        return true;
                    }
                }
                else if (certificate.Level == ClientCertificateLevelTypes.BranchGroup)
                {
                    if (string.Equals(principal.Type, "b", StringComparison.OrdinalIgnoreCase))
                    {
                        if (CheckGroupsAgainstCertificateGroups(certificate, GroupDB.ListInclusiveGroupForBranch(brokerId, principal.BranchId)))
                        {
                            return true;
                        }
                    }
                    else if (string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase))
                    {
                        // So we can get the correct branch ids for all 3 OC roles
                        EmployeeDB employeeDb = EmployeeDB.RetrieveByUserId(brokerId, principal.UserId);

                        HashSet<Guid> idsToCheck = new HashSet<Guid>();
                        idsToCheck.Add(employeeDb.BranchID);
                        idsToCheck.Add(employeeDb.CorrespondentBranchID);
                        idsToCheck.Add(employeeDb.MiniCorrespondentBranchID);

                        foreach (Guid branchId in idsToCheck)
                        {
                            if (CheckGroupsAgainstCertificateGroups(certificate, GroupDB.ListInclusiveGroupForBranch(brokerId, branchId)))
                            {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }

            return false;
        }

        /// <summary>
        /// Creates a lender client certificate and stores its info into the database.
        /// </summary>
        /// <param name="password">The password for the certificate.</param>
        /// <param name="cacheKey">The guid portion of the auto expire cache key that contains the XML document with the certificate info.</param>
        /// <param name="creatorUserId">The user id of the user who wants to make this certificate.</param>
        /// <returns>The created certificate.</returns>
        public static byte[] CreateLenderClientCertificate(string password, Guid cacheKey, Guid creatorUserId)
        {
            string cachedInfo = AutoExpiredTextCache.GetFromCache(ConstructFieldsCacheKey(creatorUserId, cacheKey));
            XDocument certFields = XDocument.Parse(cachedInfo);

            XElement fieldsElement = certFields.Element("fields");
            Guid brokerId = new Guid(fieldsElement.Element("brokerId").Value);
            string description = fieldsElement.Element("desc").Value;
            string notes = fieldsElement.Element("notes").Value;
            ClientCertificateLevelTypes level = (ClientCertificateLevelTypes)Enum.Parse(typeof(ClientCertificateLevelTypes), fieldsElement.Element("level").Value);
            ClientCertificateUserTypes userType = (ClientCertificateUserTypes)Enum.Parse(typeof(ClientCertificateUserTypes), fieldsElement.Element("userType").Value);
            string loginName = fieldsElement.Element("loginNm").Value;

            var groupIds = fieldsElement.Element("groups").Elements("group");
            List<Guid> groups = new List<Guid>();
            foreach (XElement groupIdElement in groupIds)
            {
                groups.Add(new Guid(groupIdElement.Value));
            }

            string errors;
            if (!VerifyParameters(brokerId, description, level, userType, groups, loginName, out errors))
            {
                throw new CBaseException(errors, errors);
            }

            string nameToUse = string.Empty;
            Guid? userId = null;
            if (level == ClientCertificateLevelTypes.Individual)
            {
                string type = userType == ClientCertificateUserTypes.Lqb ? "B" : "P";
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@LoginName", loginName),
                    new SqlParameter("@Type", type),
                    new SqlParameter("@BrokerId", brokerId)
                };

                userId = Guid.Empty;
                string firstName = string.Empty;
                string lastName = string.Empty;
                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetUserByLoginName", parameters))
                {
                    if (reader.Read())
                    {
                        userId = (Guid)reader["UserId"];
                        firstName = (string)reader["FirstNm"];
                        lastName = (string)reader["LastNm"];
                    }
                }

                if (userId == Guid.Empty)
                {
                    throw new CBaseException(ErrorMessages.Generic, $"User {loginName} does not exist");
                }

                nameToUse = CEmployeeFields.ComposeFullName(firstName, lastName);
            }
            else
            {
                nameToUse = BrokerDB.RetrieveNameOnlyFromDb(brokerId);
            }

            LenderClientCertificate lenderCert = new LenderClientCertificate(brokerId, description, notes, level, userType, userId, groups);
            RegisterBrokerCertificate(lenderCert);

            return ClientCertificateUtilities.GenerateClientCertificate(lenderCert.CertificateId, password, nameToUse, ClientCertificateType.Lender);
        }

        /// <summary>
        /// Gets the lender client certificates for the specified broker.
        /// </summary>
        /// <param name="brokerId">The broker id to search for.</param>
        /// <param name="isRevoked">True for revoked certs only, false for active certs only, null for everything.</param>
        /// <returns>A list of certificates belonging to that broker.</returns>
        public static IEnumerable<LenderClientCertificate> GetLenderClientCertificatesByBrokerId(Guid brokerId, bool? isRevoked)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@IsRevoked", isRevoked)
            };

            List<LenderClientCertificate> certificates = new List<LenderClientCertificate>();
            Dictionary<Guid, List<Guid>> certIdToGroupIds = new Dictionary<Guid, List<Guid>>();
            Dictionary<Guid, Dictionary<Guid, Group>> certIdToGroups = new Dictionary<Guid, Dictionary<Guid, Group>>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLenderClientCertificatesByBrokerId", parameters))
            {
                // First result are certificate properties from the Broker_Client_Certificate table.
                while (reader.Read())
                {
                    var cert = LoadCertificateFromReader(reader);
                    certificates.Add(cert);

                    List<Guid> preppedGroupIdList = new List<Guid>();
                    Dictionary<Guid, Group> preppedGroups = new Dictionary<Guid, Group>();
                    cert.GroupIds = preppedGroupIdList;
                    cert.Groups = preppedGroups;
                    certIdToGroupIds.Add(cert.CertificateId, preppedGroupIdList);
                    certIdToGroups.Add(cert.CertificateId, preppedGroups);
                }

                // Next result is the group ids.
                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        Guid certId = (Guid)reader["CertificateId"];
                        Guid groupId = (Guid)reader["GroupId"];
                        if (certIdToGroupIds.ContainsKey(certId))
                        {
                            certIdToGroupIds[certId].Add(groupId);
                        }

                        if (certIdToGroups.ContainsKey(certId))
                        {
                            Group group = new Group();
                            group.Id = groupId;
                            group.Name = (string)reader["GroupName"];
                            group.Description = (string)reader["Description"];
                            group.GroupType = (GroupType)reader["GroupType"];

                            certIdToGroups[certId].Add(groupId, group);
                        }
                    }
                }
            }

            return certificates;
        }

        /// <summary>
        /// Gets a specific lender certificate.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="certificateId">The certificate id.</param>
        /// <returns>The certificate if found. Null if not found.</returns>
        public static LenderClientCertificate GetLenderClientCertificate(Guid brokerId, Guid certificateId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CertificateId", certificateId)
            };

            LenderClientCertificate cert = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLenderClientCertificate", parameters))
            {
                if (reader.Read())
                {
                    // First result is the certificate properties from Broker_Client_Certificate.
                    cert = LoadCertificateFromReader(reader);

                    // Next result should be the GroupIds.
                    if (reader.NextResult())
                    {
                        Dictionary<Guid, Group> groups = new Dictionary<Guid, Group>();
                        List<Guid> groupIds = new List<Guid>();
                        while (reader.Read())
                        {
                            Guid groupId = (Guid)reader["GroupId"];
                            groupIds.Add(groupId);

                            Group group = new Group();
                            group.Id = groupId;
                            group.Name = (string)reader["GroupName"];
                            group.Description = (string)reader["Description"];
                            group.GroupType = (GroupType)reader["GroupType"];

                            groups.Add(groupId, group);
                        }

                        cert.Groups = groups;
                        cert.GroupIds = groupIds;
                    }
                }
            }

            return cert;
        }

        /// <summary>
        /// Revokes the client certificate.
        /// </summary>
        /// <param name="brokerId">The broker the certificate belongs to.</param>
        /// <param name="certificateId">The certificate id.</param>
        public static void RevokeClientCertificate(Guid brokerId, Guid certificateId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CertificateId", certificateId),
                new SqlParameter("@RevokedDate", DateTime.Now)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "RevokeBrokerClientCertificate", 3, parameters);
        }

        /// <summary>
        /// Restores a revoked broker client certificate.
        /// </summary>
        /// <param name="brokerId">The broker id for the certificate.</param>
        /// <param name="certificateId">The certificate id.</param>
        public static void RestoreClientCertificate(Guid brokerId, Guid certificateId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CertificateId", certificateId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "RestoreBrokerClientCertificate", 3, parameters);
        }

        /// <summary>
        /// Updates this certificate with the new description, notes, and group ids.
        /// </summary>
        /// <param name="description">The new description.</param>
        /// <param name="notes">The new notes.</param>
        /// <param name="groupIds">The new group ids.</param>
        /// <param name="errors">Any errors encountered during the update process.</param>
        /// <returns>True if updated, false otherwise.</returns>
        public bool UpdateCertificate(string description, string notes, IEnumerable<Guid> groupIds, out string errors)
        {
            Guid newNotesKey = Guid.NewGuid();
            Guid? oldNotesKey = this.NotesFileDbKey;

            this.Description = description;
            this.Notes = notes;
            this.NotesFileDbKey = newNotesKey;
            this.GroupIds = groupIds;

            if (!VerifyParameters(this.BrokerId, this.Description, this.Level, this.UserType, this.GroupIds, this.AssociatedUserLogin, out errors))
            {
                return false;
            }

            GroupType? groupType = null;
            string groupIdsXml = null;
            switch (this.Level)
            {
                case ClientCertificateLevelTypes.BranchGroup:
                    groupType = GroupType.Branch;
                    groupIdsXml = ConvertGroupListToXml(this.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.EmployeeGroup:
                    groupType = GroupType.Employee;
                    groupIdsXml = ConvertGroupListToXml(this.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.OcGroup:
                    groupType = GroupType.PmlBroker;
                    groupIdsXml = ConvertGroupListToXml(this.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.Individual:
                case ClientCertificateLevelTypes.Corporate:
                    break;
                default:
                    throw new UnhandledEnumException(this.Level);
            }

            // Write the new notes to a different FileDb file, then save to DB, then remove old FileDb file.
            string newNotesFileDbKey = GetFullNotesFileDbKey(this.CertificateId, newNotesKey);
            FileDBTools.WriteData(E_FileDB.Normal, newNotesFileDbKey, this.Notes);

            SqlParameter[] parameters =
            {
                new SqlParameter("@CertificateId", this.CertificateId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@Description", this.Description),
                new SqlParameter("@NotesFileDbKey", this.NotesFileDbKey),
                new SqlParameter("@GroupIdsXml", groupIdsXml),
                new SqlParameter("@GroupType", groupType)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "EditBrokerClientCertificate", 3, parameters);

            if (oldNotesKey.HasValue)
            {
                FileDBTools.Delete(E_FileDB.Normal, GetFullNotesFileDbKey(this.CertificateId, oldNotesKey.Value));
            }

            errors = "No errors";
            return true;
        }

        /// <summary>
        /// Constructs the full Notes file db key.
        /// </summary>
        /// <param name="certId">The certificate id.</param>
        /// <param name="notesFileDbKey">The notes file db partial key.</param>
        /// <returns>The full file db key.</returns>
        private static string GetFullNotesFileDbKey(Guid certId, Guid notesFileDbKey)
        {
            return $"{notesFileDbKey}_{certId}_{NotesFileDbKeySuffix}";
        }

        /// <summary>
        /// Loads the certificate from the reader.
        /// </summary>
        /// <param name="reader">The reader to load the parameters from.</param>
        /// <returns>The certificate constructed from the reader.</returns>
        private static LenderClientCertificate LoadCertificateFromReader(DbDataReader reader)
        {
            Guid certId = (Guid)reader["CertificateId"];
            Guid brokerId = (Guid)reader["BrokerId"];
            bool isRevoked = (bool)reader["IsRevoked"];
            string description = (string)reader["Description"];
            Guid? notesFileDbKey = null;
            if (reader["NotesFileDbKey"] != DBNull.Value)
            {
                notesFileDbKey = (Guid)reader["NotesFileDbKey"];
            }

            ClientCertificateLevelTypes level = (ClientCertificateLevelTypes)reader["Level"];
            ClientCertificateUserTypes userType = (ClientCertificateUserTypes)reader["UserType"];
            DateTime createdDate = (DateTime)reader["CreatedDate"];
            DateTime? revokedDate = null;
            if (reader["RevokedDate"] != DBNull.Value)
            {
                revokedDate = (DateTime)reader["RevokedDate"];
            }

            Guid? associatedUserId = null;
            if (reader["AssociatedUserId"] != DBNull.Value)
            {
                associatedUserId = (Guid)reader["AssociatedUserId"];
            }

            LenderClientCertificate cert = new LenderClientCertificate(
                certId,
                brokerId,
                isRevoked,
                description,
                notesFileDbKey,
                level,
                userType,
                createdDate,
                revokedDate,
                associatedUserId);

            return cert;
        }

        /// <summary>
        /// Adds a new broker client certificate to the database. Also saves the Notes to FileDb using the NotesFileDbKey property.
        /// </summary>
        /// <param name="certificate">The certificate to add.</param>
        private static void RegisterBrokerCertificate(LenderClientCertificate certificate)
        {
            Guid brokerId = certificate.BrokerId;
            Guid? userId = null;
            GroupType? groupType = null;
            string groupIds = null;
            switch (certificate.Level)
            {
                case ClientCertificateLevelTypes.BranchGroup:
                    groupType = GroupType.Branch;
                    groupIds = ConvertGroupListToXml(certificate.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.EmployeeGroup:
                    groupType = GroupType.Employee;
                    groupIds = ConvertGroupListToXml(certificate.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.OcGroup:
                    groupType = GroupType.PmlBroker;
                    groupIds = ConvertGroupListToXml(certificate.GroupIds).ToString(SaveOptions.DisableFormatting);
                    break;
                case ClientCertificateLevelTypes.Individual:
                    userId = certificate.AssociatedUserId;
                    break;
                case ClientCertificateLevelTypes.Corporate:
                    break;
                default:
                    throw new UnhandledEnumException(certificate.Level);
            }

            SqlParameter[] parameters = new SqlParameter[]
                            {
                                new SqlParameter("@CertificateId", certificate.CertificateId),
                                new SqlParameter("@BrokerId", brokerId),
                                new SqlParameter("@Description", certificate.Description),
                                new SqlParameter("@Level", certificate.Level),
                                new SqlParameter("@UserType", certificate.UserType),
                                new SqlParameter("@CreatedDate", certificate.CreatedDate),
                                new SqlParameter("@NotesFileDbKey", certificate.NotesFileDbKey),
                                new SqlParameter("@AssociatedUserId", userId),
                                new SqlParameter("@GroupType", groupType),
                                new SqlParameter("@Groups", groupIds)
                            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "AddNewBrokerClientCertificate", 3, parameters);

            if (certificate.NotesFileDbKey.HasValue && certificate.NotesFileDbKey.Value != Guid.Empty)
            {
                string fullFileDbKey = GetFullNotesFileDbKey(certificate.CertificateId, certificate.NotesFileDbKey.Value);
                FileDBTools.WriteData(E_FileDB.Normal, fullFileDbKey, certificate.Notes);
            }
        }

        /// <summary>
        /// Converts a list of group ids to xml to pass into a stored procedure.
        /// </summary>
        /// <param name="groupIds">The group ids to convert.</param>
        /// <returns>
        /// An XElement. Example below (just replace the square brackets with angle brackets).
        /// [root][group id="GuidHere"][/group][/root].
        /// </returns>
        private static XElement ConvertGroupListToXml(IEnumerable<Guid> groupIds)
        {
            XElement rootElement = new XElement("root");

            foreach (Guid id in groupIds)
            {
                XElement groupElement = new XElement("group", new XAttribute("id", id.ToString()));
                rootElement.Add(groupElement);
            }

            return rootElement;
        }

        /// <summary>
        /// Verifies the certificate parameters are valid.
        /// </summary>
        /// <param name="brokerId">The broker to save the certificate to.</param>
        /// <param name="desc">The certificate description.</param>
        /// <param name="level">The level specified for the certificate.</param>
        /// <param name="userType">The user type specified for the certificate.</param>
        /// <param name="ids">The group ids associated with the certificate.</param>
        /// <param name="loginName">The login name if the certificate has the individual level specified.</param>
        /// <param name="error">The error message.</param>
        /// <returns>True if the parameters are valid. False otherwise.</returns>
        private static bool VerifyParameters(Guid brokerId, string desc, ClientCertificateLevelTypes level, ClientCertificateUserTypes userType, IEnumerable<Guid> ids, string loginName, out string error)
        {
            error = string.Empty;
            if (string.IsNullOrEmpty(desc))
            {
                error = "Description must not be blank.";
                return false;
            }

            if (level == ClientCertificateLevelTypes.BranchGroup)
            {
                if (!ids.Any())
                {
                    error = "No groups have been associated.";
                    return false;
                }

                List<Tuple<Guid, string>> temp = new List<Tuple<Guid, string>>(GroupDB.GetAllGroupIdsAndNames(brokerId, GroupType.Branch));
                Dictionary<Guid, string> idToName = temp.ToDictionary(tuple1 => tuple1.Item1, tuple2 => tuple2.Item2);

                foreach (Guid id in ids)
                {
                    if (!idToName.ContainsKey(id))
                    {
                        error = "Associated group is not a Branch group.";
                        return false;
                    }
                }
            }
            else if (level == ClientCertificateLevelTypes.EmployeeGroup)
            {
                if (!ids.Any())
                {
                    error = "No groups have been associated. Please associate at least one group.";
                    return false;
                }

                if (userType == ClientCertificateUserTypes.Tpo || userType == ClientCertificateUserTypes.Any)
                {
                    error = "Invalid user type for Employee group.";
                    return false;
                }

                List<Tuple<Guid, string>> temp = new List<Tuple<Guid, string>>(GroupDB.GetAllGroupIdsAndNames(brokerId, GroupType.Employee));
                Dictionary<Guid, string> idToName = temp.ToDictionary(tuple1 => tuple1.Item1, tuple2 => tuple2.Item2);

                foreach (Guid id in ids)
                {
                    if (!idToName.ContainsKey(id))
                    {
                        error = "Associated group is not an Employee group.";
                        return false;
                    }
                }
            }
            else if (level == ClientCertificateLevelTypes.OcGroup)
            {
                if (!ids.Any())
                {
                    error = "No groups have been associated.";
                    return false;
                }

                if (userType == ClientCertificateUserTypes.Lqb || userType == ClientCertificateUserTypes.Any)
                {
                    error = "Invalid user type for OC group.";
                    return false;
                }

                List<Tuple<Guid, string>> temp = new List<Tuple<Guid, string>>(GroupDB.GetAllGroupIdsAndNames(brokerId, GroupType.PmlBroker));
                Dictionary<Guid, string> idToName = temp.ToDictionary(tuple1 => tuple1.Item1, tuple2 => tuple2.Item2);

                foreach (Guid id in ids)
                {
                    if (!idToName.ContainsKey(id))
                    {
                        error = "Associated group is not an OC group.";
                        return false;
                    }
                }
            }
            else if (level == ClientCertificateLevelTypes.Individual)
            {
                if (string.IsNullOrEmpty(loginName))
                {
                    error = "No login name provided.";
                    return false;
                }

                if (userType == ClientCertificateUserTypes.Any)
                {
                    error = "Invalid user type for the Individual level.";
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the user type of the certificate matches the passed in string user type.
        /// </summary>
        /// <param name="certUserType">The certificate user type.</param>
        /// <param name="userType">The string user type.</param>
        /// <returns>True if the cert user type is Any or if the user types match. False otherwise.</returns>
        private static bool CheckUserType(ClientCertificateUserTypes certUserType, string userType)
        {
            return certUserType == ClientCertificateUserTypes.Any ||
                   (certUserType == ClientCertificateUserTypes.Lqb && string.Equals(userType, "b", StringComparison.OrdinalIgnoreCase)) ||
                   (certUserType == ClientCertificateUserTypes.Tpo && string.Equals(userType, "p", StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Checks the Group enumerable to see if it contains any allowed groups in the certificate.
        /// </summary>
        /// <param name="cert">The certificate to check.</param>
        /// <param name="groupsToCheck">The groups to check against.</param>
        /// <returns>True if found, false otherwise.</returns>
        private static bool CheckGroupsAgainstCertificateGroups(LenderClientCertificate cert, IEnumerable<Group> groupsToCheck)
        {
            foreach (Group branchGroup in groupsToCheck)
            {
                if (cert.Groups.ContainsKey(branchGroup.Id))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
