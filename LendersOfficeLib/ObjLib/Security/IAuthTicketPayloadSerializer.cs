﻿namespace LendersOffice.ObjLib.Security
{
    using System.Security.Cryptography;
    using System.Text;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Classes that serialize the auth ticket payload must implement this interface.
    /// </summary>
    public interface IAuthTicketPayloadSerializer
    {
        /// <summary>
        /// Serializes the given payload to a string.
        /// </summary>
        /// <param name="payload">The auth ticket to serialize.</param>
        /// <returns>A string representing the payload.</returns>
        string Serialize(AuthTicketPayload payload);

        /// <summary>
        /// Deserializes the given string into a AuthTicketPayload.
        /// </summary>
        /// <param name="data">The data to deserialize.</param>
        /// <returns>A AuthTicketPayload representing the data.</returns>
        AuthTicketPayload Deserialize(string data);
    }
}
