﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.Admin;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Security
{
    public class ExecutingEnginePrincipal
    {
        public string UserType { get; private set; }
        public Guid BrokerId { get; private set; }
        public Guid BranchId { get; private set; }
        public Guid PmlBrokerId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public Guid UserId { get; private set; }
        public E_PmlLoanLevelAccess PmlLevelAccess { get; private set; }
        public E_ApplicationT ApplicationType { get; private set; }

        private DbConnectionInfo x_connectionInfo = null;

        public DbConnectionInfo ConnectionInfo
        {
            get
            {
                if (x_connectionInfo == null)
                {
                    x_connectionInfo = DbConnectionInfo.GetConnectionInfo(this.BrokerId);
                }

                return x_connectionInfo;
            }
        }

        #region Property belong to Broker
        private BrokerDB x_brokerDB = null;

        private BrokerDB CurrentBroker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(BrokerId);
                }
                return x_brokerDB;
            }
        }

        public bool IsRateLockedAtSubmission
        {
            get { return CurrentBroker.IsRateLockedAtSubmission; }
        }
        public bool IsLOAllowedToEditProcessorAssignedFile
        {
            get { return CurrentBroker.IsLOAllowedToEditProcessorAssignedFile; }
        }
        public bool IsOthersAllowedToEditUnderwriterAssignedFile
        {
            get { return CurrentBroker.IsOthersAllowedToEditUnderwriterAssignedFile; }
        }
        public bool HasFeatures(E_BrokerFeatureT feature)
        {
            return CurrentBroker.HasFeatures(feature);
        }
        #endregion

        private List<E_RoleT> m_roles = null;

        public bool HasRole(E_RoleT role)
        {
            return m_roles.Contains(role);
        }
        public IEnumerable<E_RoleT> GetRoles()
        {
            return m_roles;
        }


        private HashSet<Guid> m_teams = null;

        public bool HasTeam(Guid teamId)
        {
            return m_teams.Contains(teamId);
        }
        public HashSet<Guid> GetTeams()
        {
            return m_teams;
        }

        private BrokerUserPermissions m_permissionBits = null;
        public bool HasPermission(Permission p)
        {
            return m_permissionBits.HasPermission(p);
        }

        #region Constructors
        private ExecutingEnginePrincipal(Guid brokerId, Guid branchId, Guid pmlBrokerId, Guid employeeId, Guid userId, E_PmlLoanLevelAccess pmlLevelAccess, E_ApplicationT applicationType, IEnumerable<E_RoleT> roleList, HashSet<Guid> teamList, string permissions, string userType)
        {
            this.UserType = userType;
            this.BrokerId = brokerId;
            this.BranchId = branchId;
            this.PmlBrokerId = pmlBrokerId;
            this.EmployeeId = employeeId;
            this.UserId = userId;
            this.PmlLevelAccess = pmlLevelAccess;
            this.ApplicationType = applicationType;

            if (roleList != null)
            {
                m_roles = roleList.ToList();
            }
            else
            {
                m_roles = new List<E_RoleT>();
            }

            if (teamList != null)
            {
                m_teams = teamList;
            }
            else
            {
                m_teams = new HashSet<Guid>();
            }


            m_permissionBits = new BrokerUserPermissions(brokerId, employeeId, permissions, userType);
        }
        #endregion

        #region Static Constructors 
        public static ExecutingEnginePrincipal CreateFrom(Guid brokerId, SystemUserPrincipal principal)
        {
            return new ExecutingEnginePrincipal(brokerId, principal.BranchId, principal.PmlBrokerId, principal.EmployeeId, principal.UserId, principal.PmlLevelAccess, principal.ApplicationType, principal.GetRoles(),principal.GetTeams(), principal.Permissions, principal.Type);
        }
        public static ExecutingEnginePrincipal CreateFrom(AbstractUserPrincipal principal)
        {
            AbstractUserPrincipal p = principal;

            if (principal is ConsumerPortalUserPrincipal)
            {
                p = ((ConsumerPortalUserPrincipal)principal).GetImpersonatePrincipal();
            }
            ExecutingEnginePrincipal ret = new ExecutingEnginePrincipal(p.BrokerId, p.BranchId, p.PmlBrokerId, p.EmployeeId, p.UserId, p.PmlLevelAccess, p.ApplicationType, p.GetRoles(), p.GetTeams(), p.Permissions, p.Type);
            ret.x_brokerDB = p.BrokerDB;
            ret.x_connectionInfo = p.ConnectionInfo;

            return ret;
        }

        public static ExecutingEnginePrincipal CreateFrom(Guid brokerId, Guid branchId, Guid pmlBrokerId, Guid employeeId, Guid userId, E_PmlLoanLevelAccess pmlLevelAccess, E_ApplicationT applicationType, string permissions, string userType)
        {
            #region Retrieve Role separately
            List<E_RoleT> roleList = new List<E_RoleT>();

            EmployeeRoles employeeRoles = new EmployeeRoles(brokerId, employeeId);

            foreach (var role in employeeRoles.Items)
            {

                switch (applicationType)
                {
                    case E_ApplicationT.LendersOffice:

                        roleList.Add(role.RoleT);

                        break;
                    case E_ApplicationT.PriceMyLoan:
                        if (role.RoleT == E_RoleT.Administrator)
                        {
                            roleList.Add(E_RoleT.Pml_Administrator);
                        }
                        else if (role.RoleT == E_RoleT.Pml_BrokerProcessor)
                        {
                            roleList.Add(E_RoleT.Pml_BrokerProcessor);
                        }
                        else if (role.RoleT == E_RoleT.LoanOfficer)
                        {
                            roleList.Add(E_RoleT.Pml_LoanOfficer);
                        }
                        else if (role.RoleT == E_RoleT.Pml_Secondary)
                        {
                            roleList.Add(E_RoleT.Pml_Secondary);
                        }
                        else if (role.RoleT == E_RoleT.Pml_PostCloser)
                        {
                            roleList.Add(E_RoleT.Pml_PostCloser);
                        }
                        break;
                    case E_ApplicationT.ConsumerPortal:
                        if (role.RoleT == E_RoleT.Consumer)
                        {
                            roleList.Add(E_RoleT.Consumer);
                        }
                        else
                        {
                            throw CBaseException.GenericException("Unhandle role=" + role.RoleT + " for ApplicationType=ConsumerPortal");
                        }
                        break;
                    default:
                        throw new UnhandledEnumException(applicationType);
                }

            }
            #endregion

            #region Retrieve team separately
            HashSet<Guid> teamList = new HashSet<Guid>();
            foreach (var team in Team.ListTeamsByUser(employeeId))
            {
                teamList.Add(team.Id);
            }

            #endregion

            return new ExecutingEnginePrincipal(brokerId, branchId, pmlBrokerId, employeeId, userId, pmlLevelAccess, applicationType, roleList, teamList, permissions, userType);
        }
        #endregion

    }
}
