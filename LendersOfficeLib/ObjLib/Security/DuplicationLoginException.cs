using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Security.Principal;
using DataAccess;
using LendersOffice.Common;
using System.Web.Security;
using LendersOffice.Constants;

namespace LendersOffice.Security
{
    public class DuplicationLoginException : CBaseException
    {
        public DuplicationLoginException(string loginName) : this(loginName, true)
        {
        }
        public DuplicationLoginException(string loginName, bool displayLoginUrl) : base("", "DuplicateLoginException: " + loginName) 
        {
            string formatStr = null;
            string loginUrl = ConstSite.LoginUrl.Replace("~/", Tools.VRoot + "/");
            if (displayLoginUrl)
                formatStr = "You have been logged out because you logged in on a different computer.<br><br><a href={0}>Click here to login again</a>";
            else
                formatStr = "You have been logged out because you logged in on a different computer.<br><br>";

            this.UserMessage = string.Format(formatStr, loginUrl);

        }
    }
}
