﻿// <copyright file="MultiFactorAuthCodeUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>Contains utilities method for generate and verify multi-factor authentication code.</summary>
namespace LendersOffice.Security
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Email;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains utilities method for generate and verify multi-factor authentication code.
    /// </summary>
    public static class MultiFactorAuthCodeUtilities
    {
        /// <summary>
        /// Constants for max allowable number of failed attempts.
        /// </summary>
        public const int MaxAllowNumberOfFailedAttempts = 5;

        /// <summary>
        /// Constants for number of minutes to expire for authentication code. 
        /// Default is 2 hours.
        /// </summary>
        private const int ExpiresInMinutes = 120; // 2 hours.

        /// <summary>
        /// Array contains list of numeric digits. Authentication code will not contains 0.
        /// </summary>
        private static readonly char[] Digits = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        /// <summary>
        /// Generate a new multi-factor authentication code.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="userId">User to generate the new authentication for.</param>
        /// <returns>Authentication code.</returns>
        public static string GenerateNewCode(Guid brokerId, Guid userId)
        {
            string authCode = GenerateRandom(4);

            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@AuthCode", authCode),
                new SqlParameter("@MaxAllowNumberOfFailedAttempts", MaxAllowNumberOfFailedAttempts),
                new SqlParameter("@ExpiresInMinutes", ExpiresInMinutes)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ALL_USER_MULTI_FACTOR_AUTH_CODE_Insert", 3, parameters);

            return authCode;
        }

        /// <summary>
        /// Determines whether the branch or the company logo is used.
        /// </summary>
        /// <param name="employee">The employee to retrieve the data from.</param>
        /// <param name="branchId">The ID of the branch that may be used as the source.</param>
        /// <param name="displayName">The name of the Branch if it's supposed to use it's Display Name.</param>
        /// <returns>True if the Branch source is to be used, and displayName populates with the branch's display name.</returns>
        public static bool DetermineBranchAsSource(EmployeeDB employee, Guid branchId, out string displayName)
        {
            BranchDB branch = BranchDB.RetrieveById(employee.BrokerID, branchId);
            branch.Retrieve();

            if (branch.IsDisplayNmModified)
            {
                displayName = branch.DisplayNm;
                return true;
            }
            else
            {
                displayName = string.Empty;
                return false;
            }
        }

        /// <summary>
        /// Determines whether the Broker or Branch will be used as the source for Authentication Code messages.
        /// </summary>
        /// <param name="employee">The employee to determine what source to use for.</param>
        /// <param name="displayName">The name that the branch is displayed as.  Only set if the display name is modified.</param>
        /// <returns>A bool that is true if the branch is the source, and false if it's the broker.</returns>
        public static bool UseBranchAsSource(EmployeeDB employee, out string displayName)
        {
            var portalMode = employee.PortalMode == E_PortalMode.Blank ? Tools.DeterminePortalModeFirstLogin(employee) : employee.PortalMode;

            switch (portalMode)
            {
                case E_PortalMode.Blank:
                    displayName = string.Empty;
                    return false;
                case E_PortalMode.Broker:
                case E_PortalMode.Retail:
                    return DetermineBranchAsSource(employee, employee.BranchID, out displayName);

                case E_PortalMode.Correspondent:
                    return DetermineBranchAsSource(employee, employee.CorrespondentBranchID, out displayName);

                case E_PortalMode.MiniCorrespondent:
                    return DetermineBranchAsSource(employee, employee.MiniCorrespondentBranchID, out displayName);

                default:
                    throw new UnhandledEnumException(portalMode);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the generated token matches the seed.
        /// </summary>
        /// <param name="seed">The seed used to generate the tokens.</param>
        /// <param name="token">The tokens to validate.</param>
        /// <returns>Null if an error was encoutnered otherwise returns a value indicating whether token validates.</returns>
        public static bool? ValidateAuthenticatorToken(string seed, string token)
        {
            return BananaApiService.ValidateAuthenticatorToken(seed, token);
        }

        /// <summary>
        /// Generates a seed, manual code and barcode url that users can use to register on authenticator type apps.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Null if there was an error otherwise an object with the seed, barcode url and manual code.</returns>
        public static AuthenticatorRegistrationInfo GenerateAuthenticatorRegistrationInfo(Guid brokerId, Guid userId)
        {
            EmployeeDB employee = EmployeeDB.RetrieveByUserId(brokerId, userId);
            employee.Retrieve();

            string source = "LendingQB";
            string displayName = string.Empty;

            if (char.Equals(employee.UserType, 'P'))
            {
                if (!UseBranchAsSource(employee, out displayName))
                {
                    source = BrokerDB.RetrieveNameOnlyFromDb(employee.BrokerID);
                }
                else
                {
                    source = displayName;
                }
            }

            string seed = EncryptionHelper.GeneratePassword(10);
            return BananaApiService.GetAuthenticatorRegistration(seed, source, employee.FullName);
        }

        /// <summary>
        /// Generate a new multi-factor authentication code for user. Either send SMS or email the code to user.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="userId">User to send an authentication code.</param>
        /// <param name="isManuallyRequested">A value indicating whether the user manually requested a new token.</param>
        public static void GenerateAndSendNewCode(Guid brokerId, Guid userId, bool isManuallyRequested)
        {
            EmployeeDB employee = EmployeeDB.RetrieveByUserId(brokerId, userId);
            employee.Retrieve();

            // If the user has OTP enabled, do not automatically send the sms /email unless he/she manually presses submit.
            if (employee.EnableAuthCodeViaAuthenticator && !isManuallyRequested)
            {
                return;
            }

            if (!employee.EnableAuthCodeViaSms)
            {
                ErrorUtilities.DisplayErrorPage(new AccessDenied(ErrorMessages.SmsAuthenticationNotEnabled, $"User <{userId}> does not have EnableAuthCodeViaSms == true"), false, brokerId, userId);
            }

            string authCode = GenerateNewCode(brokerId, userId);

            string source = "LendingQB";
            string displayName = string.Empty;

            if (char.Equals(employee.UserType, 'P'))
            {
                if (!UseBranchAsSource(employee, out displayName))
                {
                    source = BrokerDB.RetrieveNameOnlyFromDb(employee.BrokerID);
                }
                else
                {
                    source = displayName;
                }
            }

            if (string.IsNullOrEmpty(employee.PrivateCellPhone) == false)
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                BrokerDB broker;
                if (principal == null || principal.BrokerId != brokerId)
                {
                    broker = BrokerDB.RetrieveById(brokerId);
                }
                else
                {
                    broker = principal.BrokerDB;
                }

                var cacheKey = GetBananaAuthCodeCacheKey(userId);
                var cacheValue = AutoExpiredTextCache.GetFromCacheThenRemove(cacheKey);
                int attemptCount = cacheValue.ToNullable<int>(int.TryParse) ?? 0;
                string message = $"{source} Authentication Code: {authCode}";
                BananaApiService.SendSmsMessage(employee.PrivateCellPhone, message, attemptCount++);

                AutoExpiredTextCache.AddToCache(attemptCount.ToString(), TimeSpan.FromMinutes(10), cacheKey);
            }
            else
            {
                // 5/9/2014 dd - SMS is not support right now.
                CBaseEmail email = new CBaseEmail(brokerId);
                email.From = ConstStage.DefaultDoNotReplyAddress;
                email.To = employee.Email;
                email.Subject = "Authentication Code";

                if (char.Equals(employee.UserType, 'P'))
                {
                    email.Message = string.Format(
@"Dear {0} {1},

This message was transmitted on behalf of {2}.

Below is the Site Security Authorization Code that you will need to log into the system. If you did not request a Site Security Authorization Code then please contact your user administrator.

Authentication code: {3}

This authentication code is only valid for 2 hours.

Thank you.",
                                       employee.FirstName,
                                       employee.LastName,
                                       source,
                                       authCode);
                }
                else
                {
                    email.Message = string.Format(
@"THIS IS AN AUTOMATED MESSAGE. PLEASE DO NOT REPLY TO THIS E-MAIL.

Dear {0} {1},

Below is the Site Security Authentication Code that you will need to login to the system.  If you did not request a Site Security Authentication Code, please report this incident to support@lendingqb.

Authentication Code: {2}

This authentication code is only valid for 2 hours.

Thank you for using LendingQB.",
                                       employee.FirstName,
                                       employee.LastName,
                                       authCode);
                }

                email.Send();
            }
        }

        /// <summary>
        /// Perform an multi-factor authentication code check for user.
        /// </summary>
        /// <param name="brokerId">Broker id of the user.</param>
        /// <param name="userId">User id to check.</param>
        /// <param name="authCode">Authentication code to check.</param>
        /// <returns>Whether the authentication is valid.</returns>
        public static bool Verify(Guid brokerId, Guid userId, string authCode)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@AuthCode", authCode)
            };

            bool ret = false;

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ALL_USER_MULTI_FACTOR_AUTH_CODE_Verify", parameters))
            {
                if (reader.Read())
                {
                    if ((string)reader["Status"] == "OK")
                    {
                        ret = true;
                    }
                }
            }

            if (ret)
            {
                // Authenticated, we can now remove the auto expire text cache entry for retries.
                AutoExpiredTextCache.RemoveFromCacheImmediately(GetBananaAuthCodeCacheKey(userId));
            }
            else 
            {
                EmployeeDB db = EmployeeDB.RetrieveByUserId(brokerId, userId);
                string seed = db.MfaOtpSeed.Value;
                var result = BananaApiService.ValidateAuthenticatorToken(seed, authCode);

                if (result.HasValue)
                {
                    ret = result.Value;
                }
            }

            return ret;
        }

        /// <summary>
        /// Generate a randomize n-number of digits.
        /// </summary>
        /// <param name="numberOfDigits">Number of digits of the randomize string.</param>
        /// <returns>A randomize string.</returns>
        public static string GenerateRandom(int numberOfDigits)
        {
            if (numberOfDigits <= 0 || numberOfDigits >= 100)
            {
                throw new ArgumentOutOfRangeException("Number of digits must be between 1-100.");
            }

            char[] list = new char[numberOfDigits];

            Random random = new Random();
            for (int i = 0; i < numberOfDigits; i++)
            {
                list[i] = Digits[random.Next(Digits.Length)];
            }

            return new string(list);
        }

        /// <summary>
        /// The auto expire text cache key for keeping track of auth code retries.
        /// </summary>
        /// <param name="userId">The user's user id.</param>
        /// <returns>The AutoExpireTextCache key.</returns>
        private static string GetBananaAuthCodeCacheKey(Guid userId)
        {
            return $"{userId.ToString("N")}_BananaSmsAuthRetryCount";
        }
    }
}
