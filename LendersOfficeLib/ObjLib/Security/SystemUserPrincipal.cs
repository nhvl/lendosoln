﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    public sealed class SystemUserPrincipal : AbstractUserPrincipal
    {
        public static AbstractUserPrincipal TaskSystemUser = new SystemUserPrincipal(
            new Guid("11111111-1111-1111-1111-111111111111"), "tasksystem", "Task", "System", new string[0],
            false, false, false, Guid.Empty
            );
        public static AbstractUserPrincipal CESystemUser = new SystemUserPrincipal(
            new Guid("22222222-2222-2222-2222-222222222222"), "cesystem", "Compliance", "Ease", new string[] { "Administrator" },
            false, false, false, Guid.Empty
            );
        /// <summary>
        /// Gets a principal for use by the process that submits requests to ComplianceEagle from a DB message queue.
        /// </summary>
        public static AbstractUserPrincipal ComplianceEagleSystemUser = new SystemUserPrincipal(
            new Guid("F93525C4-9E0F-428E-B1AC-5E09684F3EA7"), "ceaglesystem", "Compliance", "Eagle", new string[] { "Administrator" },
            false, false, false, Guid.Empty
            );
        /// <summary>
        /// Gets a principal for use by the quickpricer2 deletion process at least.
        /// </summary>
        public static AbstractUserPrincipal QP2SystemUser = new SystemUserPrincipal(
            new Guid("44444444-4444-4444-4444-444444444444"), "qp2system", "QuickPricer", "Two", new string[] { "Administrator" },
            true, true, true, Guid.Empty
            );

        /// <summary>
        /// Gets a principal for use by the overnight pricer process.
        /// </summary>
        public static AbstractUserPrincipal OvernightPricingUser(Guid userId)
        {
            return new SystemUserPrincipal(
                userId,
                "overnightProcess",
                "Overnight",
                "Process",
                new string[] { "Administrator" },
                false,
                false,
                false,
                Guid.Empty
                );
        }

        /// <summary>
        /// Gets a principal for use by the automated disclosure process.
        /// </summary>
        public static AbstractUserPrincipal AutomatedDisclosuresSystemUser(Guid userId)
        {
            return new SystemUserPrincipal(
                userId,
                "autodisclosuresystem",
                "Automated",
                "Disclosures",
                new string[] { "Administrator" },
                false,
                false,
                false,
                Guid.Empty);
        }

        public static AbstractUserPrincipal SecurityLogPrincipalInternal(Guid brokerId)
        {
            return new SystemUserPrincipal(
                Guid.Empty,
                "LQB User",
                "LQB",
                "User",
                new string[0], /* roles */
                false, /* isQuickPricerEnable*/
                false, /* IsNewPmlUIEnabled */
                false, /* IsUsePml2AsQuickPricer */
                brokerId,
                InitialPrincipalTypeT.Internal,
                "I");
        }

        /// <summary>
        /// One time use principals that are created with minimal data to create security event logs.
        /// </summary>        
        public static AbstractUserPrincipal SecurityLogPrincipal(Guid brokerId, string loginNm, string firstNm, string lastNm, InitialPrincipalTypeT initialPrincipalType, string type, Guid userId = new Guid())
        {
            return new SystemUserPrincipal(
                userId,
                loginNm,
                firstNm,
                lastNm,
                new string[0], /* roles */
                false, /* isQuickPricerEnable*/
                false, /* IsNewPmlUIEnabled */
                false, /* IsUsePml2AsQuickPricer */
                brokerId,
                initialPrincipalType,
                type);
        }

        public static AbstractUserPrincipal DisclosureQueueProcessorSystemUser(Guid brokerId)
        {
            return new SystemUserPrincipal(
                new Guid("11111111-1111-1111-1111-111111111111"), // arbitrary user id.
                "disclsouresystem", //login nm
                "Disclosure", // first nm
                "System", // last nm
                new string[0], /* roles */
                false, /* isQuickPricerEnable*/
                false, /* IsNewPmlUIEnabled */
                false, /* IsUsePml2AsQuickPricer */
                brokerId);
        }

        private SystemUserPrincipal(Guid userid, string loginName, string firstName, string lastName, string[] roles,
            bool isQuickPricerEnable, bool IsNewPmlUIEnabled, bool IsUsePml2AsQuickPricer, Guid brokerId, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.System, string type = "B")
            : base(new GenericIdentity(loginName), roles /* roles */, userid,
            brokerId, Guid.Empty /* branchId */, Guid.Empty /* employeeid */, Guid.Empty /* loginSessionId */,
            firstName, lastName, string.Empty /* permissions */, false /* isOthersAllowedToEditUnderwriterAssignedFile */,
            false /* isLoAllowedToEditProcessorAssignedFile */, Guid.Empty /* lpePriceGroupId */, false /*isRateLockedAtSubmission */,
            false /* hasLenderDefaultFeatures */, Guid.Empty /*becomeUserId */,
            isQuickPricerEnable /*isQuickPricerEnable*/, false /*isPricingMultipleAppsSupported*/, string.Empty /*cookieSessionId*/,
            Guid.Empty /*pmlbrokerid */, E_PmlLoanLevelAccess.Individual, type, E_ApplicationT.LendersOffice,
            null /* ipWhiteList */, null /* teams */, IsNewPmlUIEnabled /* IsNewPmlUIEnabled */, IsUsePml2AsQuickPricer /* IsUsePml2AsQuickPricer */,
            E_PortalMode.Blank /*User's last portal mode*/, Guid.Empty /* Mini-Correspondent Branch Id */, Guid.Empty /* Correspondent Branch Id */,
            initialPrincipalType: initialPrincipalType, initialUserId: new Guid(), postLoginTask: PostLoginTask.Redirect)
        { }
           
    }
}
