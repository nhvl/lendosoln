///
/// Author: David Dao
/// 
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LqbGrammar;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    public enum E_LpeRsExpirationByPassPermissionT 
    {
        NoByPass,
        ByPassAllExceptInvestorCutOff,
        ByPassAll,

    }

    public abstract class AbstractUserPrincipal : GenericPrincipal, IUserPrincipal
    {
        #region Simple Read-Only Properties
        public string Type { get; private set; }

        public E_ApplicationT ApplicationType { get; private set; }

        public bool IsQuickPricerEnable { get; private set; }

        public bool IsNewPmlUIEnabled { get; private set; }

        /// <summary>
        /// This is just the check state in the DB, and likely not what you want. <para></para>
        /// Instead use IsActuallyUsePml2AsQuickPricer, or the BrokerUserPrincipal property IsActuallyUsePml2AsQuickPricer.
        /// </summary>
        protected bool IsUsePml2AsQuickPricer { get; private set; } // opm 185732

        public E_PortalMode PortalMode { get; set; }

        /// <summary>
        /// This is the calculated version of IsUsePml2AsQuickPricer, which is just what's in the DB. <para></para>
        /// It depends on if the user has PML2 and QP, which may also depend on broker state. <para></para>
        /// It also depends on if the ConstStage.AllowQuickPricer2InTPOPortal is on and the user type.
        /// </summary>
        public bool IsActuallyUsePml2AsQuickPricer // opm 185732
        {
            get
            {
                return Tools.CanUserUseQP2(BrokerDB, IsQuickPricerEnable, IsNewPmlUIEnabled, IsUsePml2AsQuickPricer, Type);
            }
        }

        /// <summary>
        /// Gets the PML template id based on the portal mode.
        /// </summary>
        /// <param name="portalMode"></param>
        /// <returns></returns>
        public Guid GetPmlTemplateId(E_PortalMode portalMode)
        {
            switch (portalMode)
            {
                case E_PortalMode.Broker:
                    return this.BrokerDB.PmlLoanTemplateID;
                case E_PortalMode.MiniCorrespondent:
                    return this.BrokerDB.MiniCorrLoanTemplateID;
                case E_PortalMode.Correspondent:
                    return this.BrokerDB.CorrLoanTemplateID;
                case E_PortalMode.Retail:
                    return this.BrokerDB.RetailTPOLoanTemplateID ?? Guid.Empty;
                default:
                    throw new UnhandledEnumException(portalMode);
            }
        }

        public bool CanAccessTpoPortal()
        {
            if (string.Equals("B", this.Type, StringComparison.OrdinalIgnoreCase))
            {
                // Retail portal users are always allowed to access the portal.
                // B-user loan creation permissions will control whether the 
                // retail user can create a loan within the portal.
                return true;
            }

            return (this.HasPermission(Permission.AllowViewingWholesaleChannelLoans)
                    || this.HasPermission(Permission.AllowViewingMiniCorrChannelLoans)
                    || this.HasPermission(Permission.AllowViewingCorrChannelLoans));
        }

        /// <summary>
        /// This will be enabled for broker users and tpo users depending on their broker's settings.
        /// </summary>
        public bool HasConversationLogFeatureEnabled
        {
            get
            {
                var featureIsEnabledForUser = false;
                featureIsEnabledForUser = (this.Type == "B" && this.BrokerDB.ActuallyEnableConversationLogForLoans);
                featureIsEnabledForUser |= (this.Type == "P" && this.BrokerDB.ActuallyEnableConversationLogForLoansInTpo);
                return featureIsEnabledForUser;
            }
        }

        public bool HasConversationLogQuickPostingEnabled
        {
            get
            {
                return ConstStage.EnableConversationLogQuickPosting && this.HasConversationLogFeatureEnabled;
            }
        }

        public Guid UserId { get; private set; }

        public Guid BrokerId { get; private set; }

        private Guid branchId;
        private Guid miniCorrespondentBranchId;
        private Guid correspondentBranchId;

        public Guid BranchId 
        { 
            get
            {
                if (this.Type != "P" ||
                    this.PortalMode == E_PortalMode.Blank ||
                    this.PortalMode == E_PortalMode.Broker)
                {
                    return this.branchId;
                }
                else if (this.PortalMode == E_PortalMode.MiniCorrespondent)
                {
                    return this.miniCorrespondentBranchId;
                }
                else if (this.PortalMode == E_PortalMode.Correspondent)
                {
                    return this.correspondentBranchId;
                }
                else
                {
                    throw new UnhandledEnumException(this.PortalMode);
                }
            }
        }

        public Guid EmployeeId { get; private set; }

        public Guid LoginSessionId { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public bool IsOthersAllowedToEditUnderwriterAssignedFile { get; private set; }

        public bool IsLOAllowedToEditProcessorAssignedFile { get; private set; }

        public bool IsOnlyAccountantCanModifyTrustAccount { get; private set; }

        public bool HasLenderDefaultFeatures { get; private set; }

        /// <summary>
        /// This list contains a list of allowable ip for user.
        /// Empty list mean there is no IP restriction
        /// </summary>
        public IEnumerable<string> IpWhiteList { get; private set; }

        /// <summary>
        /// Gets the date that this principal object is create.
        /// </summary>
        public DateTime CreatedDate { get; private set; }
        #endregion

        /// <summary>
        /// Gets a value indicating whether <see cref="Type"/> is "P".
        /// </summary>
        public bool IsPUser => this.Type == "P";

        private BrokerUserPermissions m_permissionBits;
        private string[] m_roles;
        private string m_permissions;
        private Guid m_lpePriceGroupId;
        private bool m_isPricingMultipleAppsSupported = false;
        
        // 9/4/2013 dd - Only use to determine if the original principal is ConsumerPortalPrincipal.
        // Need for case 137164
        public bool IsOriginalPrincipalAConsumer
        {
            get;
            set;
        }

        public ConsumerPortalUserPrincipal OriginalPrincipalConsumer { get; set; }

        private Guid m_becomeUserId = Guid.Empty; // Only valid for InternalUserPrincipal only

        protected Guid BecomeUserId  // Only valid for InternalUserPrincipal only.
        {
            get { return m_becomeUserId; }
            set { m_becomeUserId = value; }
        }

        public InitialPrincipalTypeT InitialPrincipalType { get; private set; }

        public Guid InitialUserId { get; private set; }

        public PostLoginTask PostLoginTask { get; set; }

        /// <summary>
        /// This method will create a temporary id that could be pass in the GET request with authid=xxxx and it will log user in without login/password.
        /// Please be aware that this temporary id is only good for one time use and will expire after 2 minutes of creation.
        /// </summary>
        /// <returns></returns>
        public Guid GenerateByPassTicket()
        {
            Guid ticket = Guid.NewGuid();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BypassTicket", ticket),
                                            new SqlParameter("@UserId", UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "SetBypassTicketByUserId", 3, parameters);
            AutoExpiredTextCache.AddToCache(DateTime.Now.AddMinutes(2).ToString(), TimeSpan.FromMinutes(2), ticket);

            return ticket;
        }

        #region Auto Login information for DO and DU are loaded on demand.
        private bool x_isAutoLoginLoaded = false;
        private string x_doAutoLoginNm = "";
        private string x_doAutoPassword = "";
        private string x_duAutoLoginNm = "";
        private string x_duAutoPassword = "";

        public string DoAutoLoginNm
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_doAutoLoginNm;
            }
        }

        public E_PmlLoanLevelAccess PmlLevelAccess { get; private set; }
        public Guid PmlBrokerId { get; private set; }

        public string DoAutoPassword
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_doAutoPassword;
            }
        }

        public string DuAutoLoginNm
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_duAutoLoginNm;
            }
        }

        public string DuAutoPassword
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_duAutoPassword;
            }
        }

        private string x_duAutoCraId = string.Empty;
        /// <summary>
        /// 3 digits FNMA credit vendor code.
        /// </summary>
        public string DuAutoCraId
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_duAutoCraId;
            }
        }

        private string x_duAutoCraLogin = string.Empty;
        public string DuAutoCraLogin
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_duAutoCraLogin;
            }
        }

        private string x_duAutoCraPassword = string.Empty;
        public string DuAutoCraPassword
        {
            get
            {
                if (!x_isAutoLoginLoaded)
                {
                    InitializeAutoLogin();
                }

                return x_duAutoCraPassword;
            }
        }

        private void InitializeAutoLogin()
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", this.UserId) };
            Lazy<string> lazyDoAutoPassword = null;
            Lazy<string> lazyDuAutoPassword = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerDB.BrokerID, "RetrieveAutoDoDuLoginByUserId", parameters))
            {
                if (reader.Read())
                {
                    x_doAutoLoginNm = (string)reader["DOAutoLoginNm"];
                    EncryptionKeyIdentifier? maybeEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        byte[] b = (byte[])reader["EncryptedDOAutoPassword"];
                        lazyDoAutoPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, b) ?? string.Empty);
                    }
                    else
                    {
                        string s = (string)reader["DOAutoPassword"];
                        lazyDoAutoPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(s));
                    }

                    x_duAutoLoginNm = (string)reader["DUAutoLoginNm"];
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        byte[] b = (byte[])reader["EncryptedDUAutoPassword"];
                        lazyDuAutoPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, b) ?? string.Empty);
                    }
                    else
                    {
                        string s = (string)reader["DUAutoPassword"];
                        lazyDuAutoPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(s));
                    }
                }
            }

            x_doAutoPassword = lazyDoAutoPassword?.Value ?? x_doAutoPassword;
            x_duAutoPassword = lazyDuAutoPassword?.Value ?? x_duAutoPassword;

            if (this.BrokerDB.CustomerCode == "PML0223")
            {
                // 11/6/2013 dd - OPM 143673
                // 11/6/2013 dd - For FirstTech Lender we just use the cra specify in Lender CRA map.
                // 11/6/2013 dd - We should create separate fields and store in BrokerUser for other broker.

                // 11/6/2013 dd - Use the first lender map CRA. Assume they using the same FNMA cra.
                Guid craId = Guid.Empty;
                var proxy = CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(this.BrokerId).FirstOrDefault();
                if (proxy != null)
                {
                    x_duAutoCraLogin = proxy.CraUserName;
                    x_duAutoCraPassword = proxy.CraPassword;
                    craId = proxy.CraId;
                }

                if (craId != Guid.Empty)
                {
                    CRA cra = MasterCRAList.FindById(craId, false);
                    if (cra != null && cra.Protocol == CreditReportProtocol.FannieMae)
                    {
                        x_duAutoCraId = cra.Url;
                    }
                }
            }
            else
            {
                // 11/6/2013 dd - Not support for other broker.
                x_duAutoCraId = string.Empty;
                x_duAutoCraLogin = string.Empty;
                x_duAutoCraPassword = string.Empty;
            }

            x_isAutoLoginLoaded = true;
        }

        public bool HasDoAutoLogin
        {
            get
            {
                if (this.BrokerDB.PmlUserAutoLoginOption == "Off")
                {
                    return false; // 9/18/2008 dd - OPM 19618
                }

                return this.DoAutoLoginNm != "" && this.DoAutoPassword != "";
            }
        }

        public bool HasDuAutoLogin
        {
            get
            {
                if (this.BrokerDB.PmlUserAutoLoginOption == "Off")
                {
                    return false; // 9/18/2008 dd - OPM 19618
                }

                return this.DuAutoLoginNm != "" && this.DuAutoPassword != "";
            }
        }
        #endregion

        #region DO, DU and LP last login name are only load when need. Since these three fields are not in the index view, and they access infrequently make them a good candidate for load on demand.
        private bool x_isLastLoginDoDuLoaded = false;
        private string x_doLastLoginNm = "";
        private string x_duLastLoginNm = "";
        private string x_lpLastLoginNm = "";

        public string DoLastLoginNm 
        {
            get 
            { 
                if (!x_isLastLoginDoDuLoaded)
                {
                    InitializeLastLoginDoDu();
                }

                return x_doLastLoginNm;
            }
        }

        public string DuLastLoginNm 
        {
            get 
            {
                if (!x_isLastLoginDoDuLoaded)
                {
                    InitializeLastLoginDoDu();
                }

                return x_duLastLoginNm;
            }
        }

        public string LpLastLoginNm 
        {
            get 
            {
                if (!x_isLastLoginDoDuLoaded)
                {
                    InitializeLastLoginDoDu();
                }

                return x_lpLastLoginNm;
            }
        }

        private void InitializeLastLoginDoDu() 
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", this.UserId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "RetrieveLastDoDuLpLoginNmByUserId", parameters)) 
            {
                if (reader.Read()) 
                {
                    x_doLastLoginNm = (string) reader["DOLastLoginNm"];
                    x_duLastLoginNm = (string) reader["DULastLoginNm"];
                    x_lpLastLoginNm = (string) reader["LPLastLoginNm"];
                }
            }

            x_isLastLoginDoDuLoaded = true;
        }
        #endregion 

        #region Loanding LpeRsExpirationByPassPermission
        private bool x_isLpeRsExpirationByPassPermissionLoaded = false;
        private E_LpeRsExpirationByPassPermissionT x_lpeRsExpirationByPassPermission = E_LpeRsExpirationByPassPermissionT.NoByPass;
        public E_LpeRsExpirationByPassPermissionT LpeRsExpirationByPassPermission 
        {
            get 
            {
                if (!x_isLpeRsExpirationByPassPermissionLoaded)
                {
                    InitializedLpeRsExpirationByPassPermission();
                }

                return x_lpeRsExpirationByPassPermission;
            }
        }

        private void InitializedLpeRsExpirationByPassPermission() 
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", this.UserId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "RetrieveLpeRsExpirationByPassPermissionByUserId", parameters)) 
            {
                if (reader.Read()) 
                {
                    string str = (string) reader["LpeRsExpirationByPassPermission"];
                    switch (str) 
                    {
                        case "NoByPass":
                            x_lpeRsExpirationByPassPermission = E_LpeRsExpirationByPassPermissionT.NoByPass;
                            break;
                        case "ByPassAllExceptInvestorCutOff":
                            x_lpeRsExpirationByPassPermission = E_LpeRsExpirationByPassPermissionT.ByPassAllExceptInvestorCutOff;
                            break;
                        case "ByPassAll":
                            x_lpeRsExpirationByPassPermission = E_LpeRsExpirationByPassPermissionT.ByPassAll;
                            break;
                        default:
                            Tools.LogBug("Unhandle LpeRsExpirationByPassPermission=" + str + ". Default to NoByPass");
                            x_lpeRsExpirationByPassPermission = E_LpeRsExpirationByPassPermissionT.NoByPass;
                            break;
                    }
                }
            }

            x_isLpeRsExpirationByPassPermissionLoaded = true;
        }
        #endregion

        public bool IsPricingMultipleAppsSupported
        {
            get 
            { 
                // 7/30/2009 dd - OPM 33530 - We are now introduce broker-level switch for unmarried coborrower.
                // The logic is if either the user level or broker level is turn on then we will enable the feature.
                if (m_isPricingMultipleAppsSupported)
                {
                    return true; // 7/30/2009 dd - No need to check the broker level.
                }
                else
                {
                    return this.BrokerDB.IsPricingMultipleAppsSupported;
                }
            }
        }

        public bool IsAlwaysUseBestPrice
        {
            get
            {
                return this.BrokerDB.IsAlwaysUseBestPrice && (HasPermission(Permission.AllowBypassAlwaysUseBestPrice) == false);
            }
        }

        public bool IsEncompassIntegrationEnabled //opm 45012 fs 01/21/10
        {
            get { return this.BrokerDB.IsEncompassIntegrationEnabled; }
        }

        public bool IsPmlQuickPricerAnonymousLogin
        {
            get { return string.Equals(LoginNm, ConstAppDavid.PmlQuickPricerAnonymousLogin, StringComparison.CurrentCultureIgnoreCase) || GetAnonymousQuickPricerId() != Guid.Empty; }
        }

        private bool x_isAnonymousQuickPricerIdSet = false;
        private Guid x_anonymousQuickPricerId = Guid.Empty;

        public Guid GetAnonymousQuickPricerId()
        {
            if (!x_isAnonymousQuickPricerIdSet)
            {
                SqlParameter[] parameters = { new SqlParameter("@UserId", this.UserId) };
                x_anonymousQuickPricerId = Guid.Empty;
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.ConnectionInfo, "RetrieveAnonymousQuickPricerUserId", parameters))
                {
                    if (reader.Read())
                    {
                        x_anonymousQuickPricerId = (Guid)reader["AnonymousQuickPricerUserId"];
                    }
                }

                x_isAnonymousQuickPricerIdSet = true;
            }

            return x_anonymousQuickPricerId;
        }

        public bool IsPmlMortgageLeagueLogin 
        {
            get { return LoginNm.ToUpper() == ConstAppDavid.PmlMortgageLeagueLogin.ToUpper(); }
        }

        public bool IsRecordPmlActivity
        {
            // 12/29/2008 dd - Turn this on will record detail information about user interaction with PML screen.
            get { return IsPmlMortgageLeagueLogin; }
        }

        public virtual bool IsLQBStaff()
        {
            return false;
        }

        public string LoginNm 
        {
            get { return Identity.Name; }
        }

        public string DisplayNameForAuditRecentModification
        {
            get
            {
                string name = FirstName + " " + LastName;
                if (Type == "P" && PmlBrokerId != Guid.Empty)
                {
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(PmlBrokerId, BrokerId);
                    name += " of " + pmlBroker.Name;
                }

                return name;
            }
        }

        public string DisplayName 
        {
            get { return FirstName + " " + LastName; }
        }

        
        
        /// <summary>
        /// Returns true iff the user can view the test page.
        /// <param name="reasonCant">Null unless there's a reason they can't view the loan editor test page.</param>
        /// </summary>
        /// <returns>True iff the user should be allowed to view the loan editor test page.</returns>
        public bool CanUserViewLoanEditorTestPage(out string reasonCant)
        {
            bool isBUser = this.Type == "B";

            if (!isBUser)
            {
                reasonCant = $"User is not of type 'B' but rather of type {this.Type}.";
                return false;
            }

            bool canModifyLoanPrograms = this.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms);
            if (canModifyLoanPrograms)
            {
                reasonCant = null;
                return true;
            }

            reasonCant = "User does not have CanModifyLoanPrograms permission and ";

            bool doesRequestHavePrivilegedIp = ConstStage.RequiredUserIpToViewLoanTestPageWithoutPermission.Contains(RequestHelper.ClientIP);
            if (!doesRequestHavePrivilegedIp)
            {
                reasonCant += $"user has RequestHelper.ClientIP of {RequestHelper.ClientIP} which is not one of the allowed ips.";
                return false;
            }

            bool doesUserHavePrivilegedUserName = ConstStage.RequiredLoginNmToViewLoanTestPageWithoutPermission.Contains(this.LoginNm);
            if (!doesUserHavePrivilegedUserName)
            {
                reasonCant += $"{this.LoginNm} is not one of the login names allowed to see the test page.";
                return false;
            }

            bool isOnServerWhereBUsersCanGainAccess = 
                       ConstAppDavid.CurrentServerLocation == ServerLocation.Alpha
                    || ConstAppDavid.CurrentServerLocation == ServerLocation.Beta
                    || ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost
                    || ConstAppDavid.CurrentServerLocation == ServerLocation.Development
                    || ConstAppDavid.CurrentServerLocation == ServerLocation.DevCopy;

            if (!isOnServerWhereBUsersCanGainAccess)
            {
                reasonCant += $"{ConstAppDavid.CurrentServerLocation} is not on ALPHA/BETA/DEV/DEVCOPY stage.";
                return false;
            }

            if (isBUser && (canModifyLoanPrograms || (doesRequestHavePrivilegedIp && doesUserHavePrivilegedUserName && isOnServerWhereBUsersCanGainAccess)))
            {
                reasonCant = null;
                return true;
            }
            
            reasonCant = "You are not authorized to view the page";
            return false;
        }

        public virtual bool HasPermission(Permission p) 
        {
            // OPM 245766
            if (p == Permission.CanViewEDocs && (this.ApplicationType == E_ApplicationT.ConsumerPortal || this.ApplicationType == E_ApplicationT.PriceMyLoan))
            {
                return true;
            }
            else if (p == Permission.AllowOrderingGlobalDMSAppraisals && this.Type == "P")
            {
                //case 468258
                return true;
            }

            return m_permissionBits.HasPermission(p);
        }

        public string Permissions
        {
            get { return m_permissions; }
        }

        public bool IsRateLockedAtSubmission { get; private set; }

        public string[] Roles 
        {
            get { return m_roles; }
        }

        /// <summary>
        /// Returns the price group id for the user. For PML users, this is the 
        /// Broker Price Group id.
        /// </summary>
        public Guid LpePriceGroupId 
        {
            get { return m_lpePriceGroupId; }
        }


        /// <summary>
        /// This property will determine if current user has LpePriceGroupId assign, if not then it will use the branch default.
        /// If being accessed for p-users, this will use their broker branch relationship.
        /// </summary>
        public Guid DefaultLpePriceGroupId 
        {
            get 
            {
                Guid lpePriceGroupId = m_lpePriceGroupId;

                // If user doesn't assign to LpePriceGroupId then use the branch price group.
                if (lpePriceGroupId == Guid.Empty) 
                {
                    BranchDB branch = new BranchDB(this.branchId, BrokerId);
                    branch.Retrieve();
                    lpePriceGroupId = branch.BranchLpePriceGroupIdDefault;
                }

                // TODO: Not the best performance code.
                if (lpePriceGroupId == Guid.Empty) 
                {
                    lpePriceGroupId = this.BrokerDB.LpePriceGroupIdDefault;
                }

                return lpePriceGroupId;
            }
        }

        private BrokerFeatures m_brokerFeatures = null;

        /// <summary>
        /// Check to see if user has a feature subscribe. Use LendersOffice.Features.FeatureConstants
        /// for list of features.
        /// </summary>
        
        public bool HasFeatures( E_BrokerFeatureT feature )
        {
            if (m_brokerFeatures == null)
            {
                m_brokerFeatures = new BrokerFeatures(this.ConnectionInfo, this.BrokerId);
            }

            return m_brokerFeatures.HasFeature(feature);
        }

        private List<E_RoleT> m_enumRoles = null;

        public bool HasRole(E_RoleT role)
        {
            return m_enumRoles.Contains(role);
        }

        /// <summary>
        /// Will return true if user has at least one of the role in the list. Return false if and only if user does not have any of the role.
        /// </summary>
        /// <param name="roleList"></param>
        /// <returns></returns>
        public bool HasAtLeastOneRole(IEnumerable<E_RoleT> roleList)
        {
            if (null == roleList)
            {
                return false;
            }

            foreach (var role in roleList)
            {
                if (HasRole(role) == true)
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerable<E_RoleT> GetRoles()
        {
            return m_enumRoles;
        }

        private HashSet<Guid> m_teams = null;
        public HashSet<Guid> GetTeams()
        {
            return m_teams;
        }

        public bool HasTeam(Guid teamId)
        {
            return m_teams.Contains(teamId);
        }

        /// <summary>
        /// Determines whether this user has the settings and permissions necessary for creating envelopes to ESign with DocuSign.
        /// </summary>
        /// <returns>Whether or not the user can create signing envelopes.</returns>
        public bool CanCreateDocuSignEnvelopes()
        {
            var settings = Admin.DocuSign.LenderDocuSignSettings.Retrieve(this.BrokerId);

            return settings != null && settings.DocuSignEnabled && settings.IsSetupComplete() && this.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes);
        }

        /// <summary>
        /// Test whether the user belong to an employee group name
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public bool IsInEmployeeGroup(string groupName)  // Is maybe something similar for teams.
        {
            IEnumerable<Group> groupList = GroupDB.ListInclusiveGroupForEmployee(BrokerId, EmployeeId);
            if (groupList != null)
            {
                foreach (var o in groupList)
                {
                    if (o.Name.Equals(groupName, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Test whether the user belongs to an employee group with the given ID.
        /// </summary>
        /// <param name="groupId">The ID of the group to check for.</param>
        /// <returns>Whether the user is in the given employee group.</returns>
        public bool IsInEmployeeGroup(Guid groupId)
        {
            IEnumerable<Group> groupList = GroupDB.ListInclusiveGroupForEmployee(BrokerId, EmployeeId);
            return groupList?.Any(group => group.GroupId == groupId) ?? false;
        }

        public bool IsInPmlBrokerGroup(Guid groupId)
        {
            var groupList = GroupDB.ListInclusiveGroupForPmlBroker(this.BrokerId, this.PmlBrokerId);
            return groupList?.Any(group => group.GroupId == groupId) ?? false;
        }

        public bool IsInPmlBrokerGroup(string groupName)
        {
            var groupList = GroupDB.ListInclusiveGroupForPmlBroker(this.BrokerId, this.PmlBrokerId);
            return groupList?.Any(group => group.Name == groupName) ?? false;
        }

        private void ProcessRoles(string[] roles)
        {
            m_enumRoles = new List<E_RoleT>();
            foreach (string role in roles)
            {
                if ( (Type == "C" || Type == "D") && ApplicationType == E_ApplicationT.ConsumerPortal)
                {
                    if (role == "B")
                    {
                        continue; // 8/25/2010 dd - Not sure why this role even existed.
                    }
                    else if (role == ConstApp.ROLE_CONSUMER)
                    {
                        m_enumRoles.Add(E_RoleT.Consumer);
                    }
                }
                else if (Type == "B")
                {
                    if (role == "B")
                    {
                        continue; // 8/25/2010 dd - Not sure why this role even existed.
                    }

                    m_enumRoles.Add(Role.Get(role).RoleT);
                }
                else if (Type == "P" && ApplicationType == E_ApplicationT.PriceMyLoan)
                {
                    if (role == ConstApp.ROLE_ADMINISTRATOR)
                    {
                        m_enumRoles.Add(E_RoleT.Pml_Administrator);
                    }
                    else if (role == ConstApp.ROLE_BROKERPROCESSOR)
                    {
                        m_enumRoles.Add(E_RoleT.Pml_BrokerProcessor);
                    }
                    else if (role == ConstApp.ROLE_LOAN_OFFICER)
                    {
                        m_enumRoles.Add(E_RoleT.Pml_LoanOfficer);
                    }
                    else if (role == ConstApp.ROLE_EXTERNAL_SECONDARY)
                    {
                        m_enumRoles.Add(E_RoleT.Pml_Secondary);
                    }
                    else if (role == ConstApp.ROLE_EXTERNAL_POST_CLOSER)
                    {
                        m_enumRoles.Add(E_RoleT.Pml_PostCloser);
                    }
                    else if (role == "B")
                    {
                        continue; // Ignore.
                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Unhandle role=" + role + " for Type=P and ApplicationType=PML");
                    }
                }
                else if (Type == "I")
                {
                    // 8/25/2010 dd - Internal user does not have role.
                    break;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Unhandle situation for AbstractUserPrincipal.ProcessRoles. Type=" + Type + ", ApplicationType=" + ApplicationType);
                }
            }
        }

        protected AbstractUserPrincipal(IIdentity identity, string[] roles, Guid userId, Guid brokerId, Guid branchId,
            Guid employeeId, Guid loginSessionId, string firstName, string lastName, string permissions, 
            bool isOthersAllowedToEditUnderwriterAssignedFile, bool isLOAllowedToEditProcessorAssignedFile, 
            Guid lpePriceGroupId, bool isRateLockedAtSubmission,
            bool hasLenderDefaultFeatures, Guid becomeUserId, bool isQuickPricerEnable,
            bool isPricingMultipleAppsSupported, string cookieSessionId, Guid pmlBrokerId, E_PmlLoanLevelAccess pmlLoanLevelAccess,
            string type, E_ApplicationT applicationT,
            IEnumerable<string> ipWhiteList, // 4/18/2014 dd - Add to support OPM 125242
            HashSet<Guid> teams,
            bool isNewPmlUIEnabled,
            bool isUsePml2AsQuickPricer,
            E_PortalMode portalMode,
            Guid miniCorrespondentBranchId,
            Guid correspondentBranchId,
            InitialPrincipalTypeT initialPrincipalType,
            Guid initialUserId,
            PostLoginTask postLoginTask
            )
            : base(identity, roles)
        {
            Type = type;
            ApplicationType = applicationT;

            if (this is SystemUserPrincipal)
            {
                m_permissionBits = new BrokerUserPermissions();
            }
            else
            {
                m_permissionBits = new BrokerUserPermissions(brokerId, employeeId, permissions, type);
            }

            UserId = userId;
            BrokerId = brokerId;
            this.branchId = branchId;
            this.miniCorrespondentBranchId = miniCorrespondentBranchId;
            this.correspondentBranchId = correspondentBranchId;
            EmployeeId = employeeId;
            LoginSessionId = loginSessionId;
            FirstName = firstName;
            LastName = lastName;
            m_roles = roles;
            IsOthersAllowedToEditUnderwriterAssignedFile = isOthersAllowedToEditUnderwriterAssignedFile;
            IsLOAllowedToEditProcessorAssignedFile = isLOAllowedToEditProcessorAssignedFile;
            m_permissions = permissions;
            m_lpePriceGroupId = lpePriceGroupId;
            IsRateLockedAtSubmission = isRateLockedAtSubmission;
            HasLenderDefaultFeatures              = hasLenderDefaultFeatures;

            m_becomeUserId = becomeUserId; // 3/30/2006 dd - Only InternalUserPrincipal should set this value.
            IsQuickPricerEnable = isQuickPricerEnable;
            m_isPricingMultipleAppsSupported = isPricingMultipleAppsSupported; // 4/30/2009 dd - OPM 30082

            m_cookieSessionId = cookieSessionId;

            PmlBrokerId = pmlBrokerId;
            PmlLevelAccess = pmlLoanLevelAccess;

            IpWhiteList = ipWhiteList;
            ProcessRoles(roles);
            m_teams = teams;
            IsNewPmlUIEnabled = isNewPmlUIEnabled;
            IsUsePml2AsQuickPricer = isUsePml2AsQuickPricer;
            this.PortalMode = portalMode;
            this.CreatedDate = DateTime.Now;
            InitialPrincipalType = initialPrincipalType;
            InitialUserId = initialUserId;
            PostLoginTask = postLoginTask;
        }

        private string m_cookieSessionId = null;
        public string CookieSessionId
        {
            get
            {
                // 10/28/2009 dd - The purpose cookie session id is base from Citi security finding.
                // Problem: User log in to our site. User then log out. The problem is that if some how
                //          hacker able to obtain previous valid cookie (before it expire), and send that to
                //          our server we will reauthenticate user.
                // Solution: We will generate a unique id that does not change during user session and embedded in the cookie.
                //           After user is log out, we will black list this id. As the result when hacker try to reuse valid
                //           cookie it will fail because the id is no longer valid.
                // Problem with solution:
                //           - The black list is store in server memory. Therefore if server is restart then the black list will
                //           be wipe.
                //           - The check is only on PML right now.
                if (string.IsNullOrEmpty(m_cookieSessionId))
                {
                    CPmlFIdGenerator generator = new CPmlFIdGenerator();
                    m_cookieSessionId = generator.GenerateNewFriendlyId();
                }

                return m_cookieSessionId;
            }
        }

        /// <summary>
        /// Return UserType|UserID|LoginSessionId|ApplicationType|BecomeUserId|CookieSessionId|BrokerId|InitialPrincipalType|InitialUserId|PostLoginTask.
        /// 9/1/2014 - dd - Add BrokerId to user data.
        /// 6/25/2018 - jk - Add InitialPrincipalType and InitialUserId to user data.
        /// 9/21/2018 - jk - Add PostLoginTask to user data.
        /// </summary>
        /// <returns></returns>
        public virtual string ToUserDataString(bool isAnonymousQP = false) 
        {
            string format = "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}";
            if (isAnonymousQP)
            {
                format = "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|aqp";
            }

            return string.Format(format, this.Type, UserId, LoginSessionId, this.ApplicationType.ToString("D"), this.BecomeUserId, this.CookieSessionId, this.BrokerId, this.InitialPrincipalType.ToString("D"), this.InitialUserId, this.PostLoginTask.ToString("D"));
        }

        public override string ToString() 
        {
            return string.Format("UserPrincipal: UserType = {0}, LoginName = [{1}], UserId={2}, EmployeeId={3}, BrokerId={4}, BranchId={5}", 
                this.Type, this.LoginNm, this.UserId, this.EmployeeId, this.BrokerId, this.BranchId);
        }

        public bool IsAccountExecutiveOnly
        {
            get { return m_enumRoles.Count == 1 && m_enumRoles[0] == E_RoleT.LenderAccountExecutive; }
        }

        public bool IsSpecialRemnEncompassTotalAccount
        {
            get
            {
                // 6/14/2012 dd - This is a special account that REMN (PML0200) use to run TOTAL Score
                // through Encompass.
                string[] remnUsers = new string[] { "REMN_ENCOMPASS_TOTAL", "david_acc", "david_pml" };
                return remnUsers.Contains(LoginNm, StringComparer.OrdinalIgnoreCase);
            }
        }

        private DbConnectionInfo x_dbConnectionInfo = null;

        public DbConnectionInfo ConnectionInfo
        {
            get
            {
                if (x_dbConnectionInfo == null && this.BrokerId != Guid.Empty)
                {
                    x_dbConnectionInfo = DbConnectionInfo.GetConnectionInfo(this.BrokerId);
                }

                return x_dbConnectionInfo;
            }
        }

        protected void SetConnectionInfoInternalUse(DbConnectionInfo connInfo)
        {
            this.x_dbConnectionInfo = connInfo;
        }

        private BrokerDB x_brokerDB = null;

        public BrokerDB BrokerDB
        {
            get
            {
                if (x_brokerDB == null && this.BrokerId != Guid.Empty)
                {
                    x_brokerDB = BrokerDB.RetrieveById(this.BrokerId);
                }
                return x_brokerDB;
            }
        }

        public bool UseInternalPricerV2
        {
            get
            {
                return this.Type == "B" && this.BrokerDB.IsEnableInternalPricerV2Ui(this);
            }
        }

        public bool UseHistoricalPricingAndUiEnhancements
        {
            get
            {
                return this.Type == "B" && this.BrokerDB.IsEnableHistoricalPricingAndUiEnhancements(this);
            }
        }

        public bool CanViewDualCertificate
        {
            get
            {
                return (BrokerDB?.UseCustomXsltCertificate == true) && 
                       (ConstSite.DisplayPolicyAndRuleIdOnLpeResult || this.HasPermission(Permission.CanModifyLoanPrograms));
            }
        }

    }
}
