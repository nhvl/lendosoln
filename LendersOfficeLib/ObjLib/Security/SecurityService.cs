﻿namespace LendersOffice.Security
{
    using System.Linq;
    using Constants;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// For now, the Security Service will be represented by this simple class.
    /// In the future it may become its own full blown micro-service.  For sure 
    /// this will get fleshed as the SecurityToken gets enhanced.  Until then I'm
    /// not going to be too strict with the interface as very little code is using it.
    /// </summary>
    public static class SecurityService
    {
        /// <summary>
        /// Create a security token from the logged in user's information.  This will throw in threads without a logged in user.
        /// </summary>
        /// <returns>A token representing the logged in user's security information.</returns>
        public static SecurityToken CreateToken()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (principal == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            string brokerIdString;
            if (principal is InternalUserPrincipal)
            {
                brokerIdString = Common.RequestHelper.GetSafeQueryString("BrokerId");
            }
            else
            {
                brokerIdString = principal.BrokerId.ToString();
            }

            return CreateToken(principal, brokerIdString);
        }

        /// <summary>
        /// This method is specificially for creating a token on a request that doesn't have a broker id for an internal user, e.g. broker creation.
        /// </summary>
        /// <param name="brokerId">The id of the broker to be associated with the request.</param>
        /// <returns>A security token for the request.</returns>
        public static SecurityToken CreateTokenOfInternalUser(string brokerId)
        {
            var principal = PrincipalFactory.CurrentPrincipal;

            if (principal == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
            
            if (!(principal is InternalUserPrincipal))
            {
                throw new DeveloperException(ErrorMessage.SystemError); 
            }

            return CreateToken(principal, brokerId);
        }

        /// <summary>
        /// Creates the token given a principal and brokerIdString. <para></para>
        /// Note that this should not be made public, this is only meant as a common method to the public ones.
        /// </summary>
        /// <param name="principal">The principal for the request.</param>
        /// <param name="brokerIdString">The brokerid string to use for this request.</param>
        /// <returns>A security token for the request.</returns>
        private static SecurityToken CreateToken(AbstractUserPrincipal principal, string brokerIdString)
        {
            var name = PersonName.CreateWithReplace(principal.FirstName, principal.LastName);
            if (name == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            SecurityToken token = new SecurityToken();
            token.Fullname = name.Value;
            token.UserId = UserAccountIdentifier.Create(principal.UserId.ToString()).Value;
            token.BrokerId = BrokerIdentifier.Create(brokerIdString).Value;

            if (ConstStage.EnableConversationLogPermissions)
            {
                // Temporary solution until Scott K & Alan can discuss how to correctly handle the roles that are not in the Role table
                token.RoleIds = principal.GetRoles().Where(r => !r.EqualsOneOf(E_RoleT.Pml_Administrator, E_RoleT.Pml_LoanOfficer)).Select(r => PrimaryKeyGuid.Create(Role.Get(r).Id).Value).ToArray();

                if (principal.EmployeeId == System.Guid.Empty)
                {
                    token.EmployeeGroupIds = new GroupIdentifier[0];
                }
                else
                {
                    var brokerIdGuid = new System.Guid(token.BrokerId.ToString());
                    token.EmployeeGroupIds = Admin.GroupDB.ListInclusiveGroupForEmployee(brokerIdGuid, principal.EmployeeId).Select(g => GroupIdentifier.Create(token.BrokerId, g.GroupId).Value).ToArray();
                }
            }

            return token;
        }
    }   
}
