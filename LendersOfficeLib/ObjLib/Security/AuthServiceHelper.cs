﻿namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;
    using LendersOffice.Integration.GenericFramework;
    using ConfigSystem.Operations;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using ObjLib.Security;
    using ObjLib.Webservices;
    using System.Text;
    using LqbGrammar.Drivers;
    using LqbGrammar;

    public class AuthServiceHelper
    {
        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanNumber">The loan number (<see cref="CPageData.sLNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanNumber"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber)
        {
            return GetPrincipalFromTicket(ticket, loanNumber, false);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanNumber">The loan number (<see cref="CPageData.sLNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="ignoreTicketLoanRestriction">A value indicating whether the restriction of the ticket to the specified loan can safely be ignored.</param>
        /// <param name="genericFrameworkVendorInfo">The vendor specified within the ticket.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanNumber"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber, bool ignoreTicketLoanRestriction)
        {
            string ticketLoanReferenceNumber;
            Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo;
            return GetPrincipalFromTicket(ticket, loanNumber, false, ignoreTicketLoanRestriction, out ticketLoanReferenceNumber, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanNumber">The loan number (<see cref="CPageData.sLNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="genericFrameworkVendorInfo">The vendor specified within the ticket.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanNumber"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber, out Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo)
        {
            string ticketLoanReferenceNumber;
            return GetPrincipalFromTicket(ticket, loanNumber, false, false, out ticketLoanReferenceNumber, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanNumber">The loan number (<see cref="CPageData.sLNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="genericFrameworkVendorInfo">The vendor specified within the ticket.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanNumber"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicketByRefNum(string ticket, string loanRefNum, out Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo)
        {
            string ticketLoanReferenceNumber;
            return GetPrincipalFromTicket(ticket, loanRefNum, true, false, out ticketLoanReferenceNumber, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket. If Generic Framework ticket, the loan id to which the ticket has access will also be returned.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="ticketLoanReferenceNumber">The loan reference number to which the Generic Framework ticket has access, empty string otherwise.</param>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalAndsLIdFromTicket(string ticket, out string ticketLoanReferenceNumber)
        {
            Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo;
            return GetPrincipalFromTicket(ticket, null, false, true, out ticketLoanReferenceNumber, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanRefNm">The loan reference number (<see cref="CPageData.sLRefNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="ignoreTicketLoanRestriction">A value indicating whether the restriction of the ticket to the specified loan can safely be ignored.</param>
        /// <param name="genericFrameworkVendorInfo">The vendor specified within the ticket.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanRefNm"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicketAndValidateByRefNum(string ticket, string loanRefNm, bool ignoreTicketLoanRestriction, out BillingInfo genericFrameworkBillingInfo)
        {
            string ticketLoanReferenceNumber;
            return GetPrincipalFromTicket(ticket, loanRefNm, true, ignoreTicketLoanRestriction, out ticketLoanReferenceNumber, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanRefNm">The loan reference number (<see cref="CPageData.sLRefNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="ignoreTicketLoanRestriction">A value indicating whether the restriction of the ticket to the specified loan can safely be ignored.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanRefNm"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        public static AbstractUserPrincipal GetPrincipalFromTicketAndValidateByRefNum(string ticket, string loanRefNm, bool ignoreTicketLoanRestriction)
        {
            BillingInfo genericFrameworkBillingInfo;
            return GetPrincipalFromTicketAndValidateByRefNum(ticket, loanRefNm, ignoreTicketLoanRestriction, out genericFrameworkBillingInfo);
        }

        /// <summary>
        /// Gets the <see cref="AbstractUserPrincipal"/> corresponding to the specified ticket.
        /// </summary>
        /// <param name="ticket">The ticket to read.</param>
        /// <param name="loanIdentifier">Either the loan number (<see cref="CPageData.sLNm" />) or the loan reference number (<see cref="CPageData.sLRefNm" />) being accessed in the call;
        /// used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="isIdentifierRefNm">A value indicating whether <paramref name="loanIdentifier"/> is a loan reference number. If false <paramref name="loanIdentifier"/> is assumed to be a loan number.</param>
        /// <param name="ignoreTicketLoanRestriction">A value indicating whether the restriction of the ticket to the specified loan can safely be ignored.</param>
        /// <param name="ticketLoanReferenceNumber">The loan reference number embedded in the generic framework authenticated ticket.</param>
        /// <param name="genericFrameworkVendorInfo">The vendor specified within the ticket.</param>
        /// <exception cref="LendersOffice.Security.AccessDenied">The ticket has expired or is restricted to a loan not matching <paramref name="loanIdentifier"/>.</exception>
        /// <returns>The principal created from the ticket.</returns>
        private static AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanIdentifier, bool isIdentifierRefNm, bool ignoreTicketLoanRestriction, out string ticketLoanReferenceNumber, out Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo)
        {
            ticketLoanReferenceNumber = string.Empty;
            genericFrameworkBillingInfo = null;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.LoadXml(ticket);
            }
            catch (XmlException)
            {
                Tools.LogInfo("AuthService.GetPrincipalFromTicket sTicket is not a valid xml: sTicket=[" + ticket + "]");
                throw new AccessDenied(ErrorMessages.WebServices.InvalidAuthTicket); ;
            }

            string encryptedTicket = xd.DocumentElement.GetAttribute("EncryptedTicket");

            if (string.IsNullOrEmpty(encryptedTicket))
            {
                Tools.LogInfo("AuthService.GetPrincipalFromTicket sTicket is not the valid format. Missing EncryptedTicket.");
                throw new AccessDenied(ErrorMessages.WebServices.InvalidAuthTicket); ;
            }

            AbstractUserPrincipal principal = null;

            if (xd.DocumentElement.Name == "CONSUMER_USER_TICKET")
            {
                principal = ConsumerPortalUserTicket.Decrypt(encryptedTicket).Principal;
            }
            else
            { 
                var authTicket = DecryptTicketData(encryptedTicket);

                if (authTicket == null)
                {
                    throw new AccessDenied("Ticket is invalid.");
                }

                DateTime createdDate = new DateTime(authTicket.Ticks);

                if (createdDate.AddMinutes(ConstApp.LOCookieExpiresInMinutes) < DateTime.Now)
                {
                    throw new AccessDenied("Ticket is expired", "Ticket is expired");
                }

                principal = PrincipalFactory.Create(authTicket.BrokerId, authTicket.UserId, authTicket.PrincipalType, true/* allowDuplicateLogin */, false /* isStoreToCookie */) as AbstractUserPrincipal;

                if (principal != null)
                { 
                    if (!string.IsNullOrEmpty(authTicket.LoanReferenceNumber))
                    {
                        VerifyLoanAccess(authTicket.LoanReferenceNumber, loanIdentifier, isIdentifierRefNm, ignoreTicketLoanRestriction, principal);
                    }
                    if (!string.IsNullOrEmpty(authTicket.BillingProviderId) && !string.IsNullOrEmpty(authTicket.BillingServiceType))
                    {
                        genericFrameworkBillingInfo = LoadGenericFrameworkVendorInfo(authTicket.BillingProviderId, authTicket.BillingServiceType);
                    }
                }
            }

            if (principal != null && HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }

            return principal;
        }

        /// <summary>
        /// Authenticates a document capture partner.
        /// </summary>
        /// <param name="partnerKey">
        /// The partner key to authenticate.
        /// </param>
        public static void AuthDocumentCapture(string partnerKey)
        {
            if (string.IsNullOrEmpty(partnerKey))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Partner key was null or empty.");
            }

            // ML 8/30 - For now, we are only authenticating requests from KTA.
            var hash = ConstStage.KtaPartnerKey_Hash;
            var salt = ConstStage.KtaPartnerKey_Salt;
            if (!EncryptionHelper.ValidatePBKDF2Hash(partnerKey, hash, salt))
            {
                var warning = "Unable to authenticate partner key= " + partnerKey;
                throw new CBaseException(ErrorMessages.GenericAccessDenied, warning);
            }

            RequestHelper.EnforceForDocumentCapture();
        }

        private static void VerifyLoanAccess(string ticketLoanReferenceNumber, string requestedLoanIdentifier, bool isIdentifierRefNm, bool ignoreTicketLoanRestriction, AbstractUserPrincipal principal)
        {
            if (ignoreTicketLoanRestriction || ticketLoanReferenceNumber == null)
            {
                return;
            }
            else if (string.IsNullOrEmpty(requestedLoanIdentifier))
            {
                throw new RestrictedLoanAccessDenied("Ticket is restricted to methods for the specified loan file.", "Ticket is restricted to methods for the specified loan file.");
            }
            else if (principal == null)
            {
                throw new AccessDenied("Principal was null");
            }

            string requestedLoanReferenceNumber = isIdentifierRefNm ? requestedLoanIdentifier : Tools.GetLoanRefNmByLoanId(principal.BrokerId, Tools.GetLoanIdByLoanName(principal.BrokerId, requestedLoanIdentifier));
            if (requestedLoanReferenceNumber != ticketLoanReferenceNumber)
            {
                throw new RestrictedLoanAccessDenied("Ticket does not have permission to access this loan file.", "Ticket does not have permission to access this loan file.");
            }
        }

        private static Integration.GenericFramework.BillingInfo LoadGenericFrameworkVendorInfo(string vendorProviderId, string serviceTypeString)
        {
            int serviceTypeInt = int.Parse(serviceTypeString);
            if (Enum.IsDefined(typeof(TypeOfService), serviceTypeInt))
            {
                return new Integration.GenericFramework.BillingInfo(vendorProviderId, (TypeOfService)serviceTypeInt);
            }
            else
            {
                throw new AccessDenied(ErrorMessages.GenericAccessDenied, "Expected valid TypeOfService.  ProviderID = " + vendorProviderId + ", ServiceType = " + serviceTypeString + ".");
            }
        }

        /// <summary>
        /// Generates the encrypted ticket data. 
        /// </summary>
        /// <returns>The unencrypted ticket.</returns>
        private static string GenerateEncryptedTicketData(AbstractUserPrincipal principal, DateTime createdDateTime, string loanReferenceNumber = null, Integration.GenericFramework.BillingInfo billingInfo = null)
        {
            AuthTicketPayload payload = new AuthTicketPayload()
            {
                BillingProviderId = billingInfo?.ProviderID,
                BillingServiceType = billingInfo?.ServiceType.ToString("D"),
                BrokerId = principal.BrokerId,
                UserId = principal.UserId, 
                PrincipalType = principal.Type,
                LoanReferenceNumber = loanReferenceNumber,
                Ticks = createdDateTime.Ticks
            };

            var s = GetSerializer();
            return s.Serialize(payload);
        }

        /// <summary>
        /// Decrypts the auth ticket.
        /// </summary>
        /// <param name="ticketData">Ticket data.</param>
        /// <returns></returns>
        private static AuthTicketPayload DecryptTicketData(string ticketData)
        {
            var serializer = AuthServiceHelper.GetSerializer();
            return serializer.Deserialize(ticketData);
        }

        private static IAuthTicketPayloadSerializer GetSerializer()
        {
            var factory = GenericLocator<IEncryptionDriverFactory>.Factory;
            IAuthTicketPayloadSerializer serializer = new JsonAuthTicketPayloadSerializer();
            return new EncryptedTicketPayloadSerializer(factory.Create(), ConstStage.WebserviceAuthTicketEncryptionkeyId, serializer);
        }

        /// <summary>
        /// The name of the Generic Framework Ticket.  Do not use this to specify which type of ticket is being parsed, as then a
        /// vendor could make their ticket have normal ticket restrictions, which could allow additional access.
        /// </summary>
        internal const string GenericFrameworkTicketName = "GENERIC_FRAMEWORK_USER_TICKET";

        /// <summary>
        /// Creates a ticket for the Generic Framework using the specified principal. This ticket expires in 30 minutes.
        /// </summary>
        /// <param name="principal">The principal who the Generic Framework will use to access the values.</param>
        /// <param name="loanReferenceNumber">The loan reference number (<seealso cref="CPageData.sLRefNm"/>) to restrict the ticket to. Pass null to not have any restriction.</param>
        /// <returns>A user ticket good for 30 minutes.</returns>
        public static XElement GetGenericFrameworkUserAuthTicket(AbstractUserPrincipal principal, string loanReferenceNumber, Integration.GenericFramework.BillingInfo vendorInfo)
        {
            // We want this ticket to expire after 30 minutes (GenericFrameworkTicketExpiresInMinutes), so
            // we pretend that the ticket was created the expiration time - 30 minutes ago.
            int timeToRemove = ConstApp.LOCookieExpiresInMinutes - ConstApp.GenericFrameworkTicketExpiresInMinutes;
            string data = GenerateEncryptedTicketData(principal, DateTime.Now.AddMinutes(-timeToRemove), loanReferenceNumber, vendorInfo);
            return new XElement(
                GenericFrameworkTicketName,
                new XAttribute("EncryptedTicket", data),
                new XAttribute("Site", ConstStage.SiteCode));
        }

        public static AbstractUserPrincipal AuthenticateUser(string userName, string passWord)
        {
            PrincipalFactory.E_LoginProblem loginProblem;
            AbstractUserPrincipal principal = PrincipalFactory.CreateWithFailureType(userName, passWord, out loginProblem, true /* allowDuplicateLogin */, false /* isStoreToCookie */, LoginSource.Webservice);
            if (null == principal)
            {
                if (loginProblem != PrincipalFactory.E_LoginProblem.None)
                {
                    Tools.LogInfo("AuthenticationUser Failed. UserName=[" + userName + "]");
                }
                
                ThrowAuthFailureExceptionForBUser(loginProblem, userName);
            }

            ProcessFactoryAuthenticatedPrincipalForBUser(principal, userName, true);
            
            return principal;
        }

        public static AbstractUserPrincipal AuthenticateActiveDirectoryUser(string sADUsername, string sADPassword, string sCustomerCode)
        {
            PrincipalFactory.E_LoginProblem loginProblem;

            AbstractUserPrincipal principal = PrincipalFactory.CreateWithFailureTypeForActiveDirectoryUser(sADUsername, sADPassword, sCustomerCode, out loginProblem, true, LoginSource.Webservice);
            if (null == principal)
            {
                if (loginProblem != PrincipalFactory.E_LoginProblem.None)
                {
                    Tools.LogInfo(String.Format("AuthenticateActiveDirectoryUser Failed. UserName: {0}; CustomerCode: {1}", sADUsername, sCustomerCode));
                }
                
                ThrowAuthFailureExceptionForBUser(loginProblem, sADUsername);
            }

            ProcessFactoryAuthenticatedPrincipalForBUser(principal, sADUsername, false);


            return principal;
        }

        private static void ThrowAuthFailureExceptionForBUser(PrincipalFactory.E_LoginProblem loginProblem, string sUsername)
        {
            ThrowAuthFailureException(loginProblem, sUsername, true, String.Empty);
        }
        private static void ThrowAuthFailureException(PrincipalFactory.E_LoginProblem loginProblem, string sUsername, bool bIsBUserType, string sCustomerCode)
        {
            switch (loginProblem)
            {
                case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                    double minutesToWait = (bIsBUserType) ? PrincipalFactory.GetMinutesToWait(sUsername, "B", Guid.Empty) : PrincipalFactory.GetMinutesToWait(sUsername, "P", sCustomerCode);
                    throw new AccessDenied(ErrorMessages.WaitTimeDueToMaxFailedLogins(minutesToWait));
                case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                    throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
                case PrincipalFactory.E_LoginProblem.TempPasswordExpired:
                    throw new AccessDenied(ErrorMessages.PasswordExpired);
                case PrincipalFactory.E_LoginProblem.IsDisabled:
                    throw new AccessDenied(ErrorMessages.AccountDisabled + ' ' + ErrorMessages.PleaseContactYourAdministrator);
                case PrincipalFactory.E_LoginProblem.IsLocked:
                    throw new AccessDenied(ErrorMessages.AccountLocked + ' ' + ErrorMessages.PleaseContactYourAdministrator);
                case PrincipalFactory.E_LoginProblem.NeedsToWait:
                    throw new AccessDenied(ErrorMessages.NeedsToWaitForLogin);
                case PrincipalFactory.E_LoginProblem.ActiveDirectorySetupIncomplete:
                    throw new AccessDenied(ErrorMessages.WebServices.ActiveDirectoryNotEnabled);
                case PrincipalFactory.E_LoginProblem.InvalidCustomerCode:
                    throw new AccessDenied(ErrorMessages.WebServices.InvalidCredentials);
                case PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType:
                    throw new AccessDenied(ErrorMessages.WebServices.MustLoginAsADUser);
                case PrincipalFactory.E_LoginProblem.None:
                default:
                    string loginProblemNotFound = "The IPrincipal is null, but no login problem error code was returned - AuthServiceHelper";
                    Tools.LogBug(loginProblemNotFound);
                    throw new CBaseException(ErrorMessages.GenericLoginFailed, loginProblemNotFound);
            }
        }

        private static void ProcessFactoryAuthenticatedPrincipalForBUser(AbstractUserPrincipal principal, string sUsername, bool bIsNormalUser)
        {
            ProcessFactoryAuthenticatedPrincipal(principal, sUsername, bIsNormalUser, true, String.Empty);
        }
        private static void ProcessFactoryAuthenticatedPrincipal(AbstractUserPrincipal principal, string sUsername, bool checkPasswordExpiration, bool bIsBUserType, string sCustomerCode)
        {
            if (bIsBUserType)
            {
                CheckLicenseExpiration(principal);
            }

            if (checkPasswordExpiration) // Not needed for Active Directory users
            {
                CheckExpiredPassword((BrokerUserPrincipal)principal);
            }

            SecurityEventLogHelper.CreateWebserviceAuthenticationLog(principal);            

            if (bIsBUserType)
            {
                ResetLOLoginFailureCount(principal.BrokerId, sUsername);
            }
            else
            {
                ResetPMLLoginFailureCount(principal.BrokerId, sUsername, PrincipalFactory.GetBrokerPmlSiteIdFromCustomerCode(sCustomerCode));
            }
            
            if (null != HttpContext.Current)
            {
                HttpContext.Current.User = principal;
            }
            System.Threading.Thread.CurrentPrincipal = principal;
        }

        private static void AuthPartner(XmlDocument xd)
        {
            string sSecretKey = xd.DocumentElement.GetAttribute("secret_key");
            Guid brokerId = new Guid(xd.DocumentElement.GetAttribute("brokerid"));
            string userName = xd.DocumentElement.GetAttribute("username");

            AuthPartner(sSecretKey, brokerId, userName);
        }

        public static void AuthPartner(string sSecretKey, Guid brokerId, string userName)
        {
            string partner = CheckKey(sSecretKey);

            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@user", userName), new SqlParameter("@brokerid", brokerId) };

            string sSQL = @"SELECT a.UserId, a.Type AS UserType, c.CustomerCode, c.BrokerNm
				FROM all_user a, employee b, broker c, broker_user d, branch e
				WHERE a.userid = d.userid AND d.employeeid = b.employeeid AND b.branchid = e.branchid AND e.brokerid = c.brokerid AND a.loginnm = @user AND c.brokerid = @brokerid";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                // throw exception if login failed
                if (!reader.Read())
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "AuthService.asmx:AuthPartner Login Failed");
                }

                Guid userID = (Guid)reader["userID"];
                string userType = reader["UserType"].ToString();
                string brokerNm = (string)reader["BrokerNm"];
                string customerCode = (string)reader["CustomerCode"];
                // 10/4/2011 dd - For audit purpose.

                string ip = string.Empty;
                if (HttpContext.Current != null)
                {
                    try
                    {
                        if (HttpContext.Current.Request != null)
                        {
                            ip = HttpContext.Current.Request.UserHostAddress;
                        }
                    }
                    catch (HttpException) { }
                }

                InitializePrincipal(brokerId, userID, userType);
                Tools.LogInfo("AuthService.asmx:AuthPartner: " + partner + ".IP=" + ip + ". Broker=" + brokerNm + "(" + customerCode + "). User:" + userName);
            };

            DBSelectUtility.ProcessDBData(brokerId, sSQL, null, listParams, readHandler);
        }

        public static void AuthPmlUser(string userName, string password, string customerCode, bool checkPasswordExpiration = true)
        {

            PrincipalFactory.E_LoginProblem loginProblem;
            Guid brokerId = Tools.GetBrokerIdByCustomerCode(customerCode);

            AbstractUserPrincipal principal = PrincipalFactory.CreatePmlUserWithFailureType(userName, password, customerCode, out loginProblem, false, brokerId, LoginSource.Webservice);
            if (null == principal)
            {
                if (loginProblem != PrincipalFactory.E_LoginProblem.None)
                {
                    Tools.LogInfo("AuthPmlUser FAIL. LoginProblem=" + loginProblem + ". UserName=[" + userName + "]. CustomerCode=[" + customerCode + "]");
                }

                ThrowAuthFailureException(loginProblem, userName, false, customerCode);
            }

            ProcessFactoryAuthenticatedPrincipal(principal, userName, checkPasswordExpiration, false, customerCode);
        }

        public static void CheckOrderCreditAuthorization(Guid loanId, CreditReport.CreditReportProtocol protocol, AbstractUserPrincipal principal)
        {
            // OPM 450477 requires special way to access this check due to
            // its need to have non-loan data parameters.

            if (principal is ConsumerPortalUserPrincipal)
            {
                ((ConsumerPortalUserPrincipal)principal).SetLoanId(loanId);
            }

            string reasons;
            var canOrderCredit = Tools.CanOrderCredit(principal, loanId, protocol, out reasons);

            if (!canOrderCredit)
            {
                throw new AccessDenied(reasons);
            }
        }

        public static void CheckOrderCreditAuthorization(Guid loanId, Guid craId)
        {
            // OPM 450477 requires special way to access this check due to
            // its need to have non-loan data parameters.

            AbstractUserPrincipal principal = (AbstractUserPrincipal)HttpContext.Current.User;
            if (principal is ConsumerPortalUserPrincipal)
            {
                ((ConsumerPortalUserPrincipal)principal).SetLoanId(loanId);
            }

            string reasons;
            var canOrderCredit = Tools.CanOrderCredit(principal, loanId, craId, out reasons);

            if (!canOrderCredit)
            {
                throw new AccessDenied(reasons);
            }
        }

        public static void CheckLoanAuthorization(Guid loanId, WorkflowOperation operation)
        {
            CheckLoanAuthorization(loanId, new[] { operation });
        }

        public static void CheckLoanAuthorization(Guid loanId, IEnumerable<WorkflowOperation> operations)
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)HttpContext.Current.User;
            // 10/22/2013 dd - If principal is consumer portal user then use the current loan officer.
            if (principal is ConsumerPortalUserPrincipal)
            {
                ((ConsumerPortalUserPrincipal)principal).SetLoanId(loanId);
            }

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, loanId, operations.ToArray());
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
            foreach (var operation in operations)
            {
                string sUserFriendlyMessage = LendingQBExecutingEngine.GetUserFriendlyMessage(operation, valueEvaluator);
                if (!string.IsNullOrEmpty(sUserFriendlyMessage))
                {
                    throw new AccessDenied(sUserFriendlyMessage);
                }
            }
        }

        public static bool IsOperationAuthorized(Guid loanId, WorkflowOperation operation)
        {
            var dictionary = AreOperationsAuthorized(loanId, new[] { operation });
            return dictionary.Values.First();
        }

        public static Dictionary<WorkflowOperation, bool> AreOperationsAuthorized(Guid loanId, IEnumerable<WorkflowOperation> operations)
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)HttpContext.Current.User;
            if (principal is ConsumerPortalUserPrincipal)
            {
                ((ConsumerPortalUserPrincipal)principal).SetLoanId(loanId);
            }

            return Tools.AreWorkflowOperationsAuthorized(principal, loanId, operations).ToDictionary((value) => value.Key, (value) => value.Value.Item1);
        }

        //OPM 22733 - jM
        //Checks for valid license
        private static void CheckLicenseExpiration(AbstractUserPrincipal p)
        {
            AbstractUserPrincipal principal = p;

            if (null != principal)
            {
                var broker = LendersOffice.Admin.BrokerDB.RetrieveById(principal.BrokerId);
                if (broker.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.Classic)
                {
                    LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUserLic = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(principal.UserId, principal.BrokerId, principal.EmployeeId, principal.DisplayName, principal.Permissions, principal.Type);
                    if (null != brokerUserLic && brokerUserLic.EnforceLicensing())
                    {
                        if (!brokerUserLic.HasValidLicense())
                        {
                            throw new CBaseException(ErrorMessages.LicenseExpired, ErrorMessages.LicenseExpired);
                        }
                    }
                }
                else if (p.Type == "B" && broker.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.New)
                {
                    throw new NotSupportedException();
                }
                //billing version 0 = no license check AND  pml users will not have a license
            }

        }
        //OPM 22934 10-9-08 jk - Checks if the user password is expired
        //If so, it throws a PasswordExpiredException so that it can easily be caught if needed.
        private static void CheckExpiredPassword(BrokerUserPrincipal principal)
        {
            if (principal != null)
            {
                if (principal.IsPasswordExpired)
                {
                    throw new PasswordExpiredException("Password Expired", principal);
                }
            }
        }

        // OPM 20453
        private static void InsertAuditRecord(AbstractUserPrincipal principal)
        {
            SecurityEventLogHelper.CreateLoginLog(principal);
        }

        private static void ResetLOLoginFailureCount(Guid brokerId, string userName)
        {
            try
            {
                SqlParameter[] parameters = {

                    new SqlParameter("@LoginNm", userName),
                    new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue),
                    new SqlParameter("@Type", "B")
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "ResetLoginFailureCount", 1, parameters);
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to reset login failure count from AuthService", e);
            }
        }

        private static void ResetPMLLoginFailureCount(Guid brokerId, string userName, Guid brokerPmlSiteId)
        {
            SqlParameter[] parameters = {
											 new SqlParameter("@LoginNm", userName),
											 new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue),
											 new SqlParameter("@Type", "P"),
											 new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId),
										 };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ResetLoginFailureCount", 1, parameters);
        }

        private static void InitializePrincipal(Guid brokerId, Guid userID, string userType)
        {
            // 9/2/2004 dd - Allow user to have duplicate login, if one of the authenticate is from webservice.
            var principal = PrincipalFactory.Create(brokerId, userID, userType, true/* allowDuplicateLogin */, true /* isStoreToCookie */);

            if (null == principal)
                throw new CBaseException(ErrorMessages.InvalidAuthorization, "Cannot load principal from db");
            if (!principal.Identity.IsAuthenticated)
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "User has not been authenticated");

            HttpContext.Current.User = principal;
            System.Threading.Thread.CurrentPrincipal = principal;
        }
        public static string CheckKey(string sSecretKey)
        {
            // opm 248903 ejm - Changed to use a salted password
            // ad - changed to make thread-safe
            var tuple = ConstStage.GetPartnerKeysData();
            var salts = tuple.Item1;
            var hashes = tuple.Item2;

            foreach (var partnerKeyHash in hashes)
            {
                string partnerName = partnerKeyHash.Key;
                string keyHash = partnerKeyHash.Value;
                string partnerKeySalt;
                if (!salts.TryGetValue(partnerName, out partnerKeySalt))
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "Partner key <" + partnerName + "> has no salt.");
                }

                if (EncryptionHelper.ValidatePBKDF2Hash(sSecretKey, keyHash, partnerKeySalt))
                {
                    return partnerName;
                }
            }

            string warning = "Unable to authenticate partner key=" + sSecretKey;
            throw new CBaseException(ErrorMessages.GenericAccessDenied, warning);
        }

        /// <summary>
        /// Creates an auth ticket given the specified principal.
        /// The ticket type is not used at all, but in the refactor im leaving it in place.
        /// </summary>
        /// <param name="ticketType">The type of the ticket.</param>
        /// <param name="principal">The principal that this ticket will belong to.</param>
        /// <returns>A xml representation of the ticket.</returns>
        public static string CreateAuthTicket(string ticketType, AbstractUserPrincipal principal)
        {
            string data = AuthServiceHelper.GenerateEncryptedTicketData(principal, DateTime.Now);
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement(ticketType);
                writer.WriteAttributeString("EncryptedTicket", data);
                writer.WriteAttributeString("Site", ConstStage.SiteCode);
                writer.WriteEndElement();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Creates an auth ticket given the specified principal.
        /// The ticket type is not used at all, but in the refactor im leaving it in place.
        /// </summary>
        /// <param name="ticketType">The type of the ticket.</param>
        /// <param name="principal">The principal that this ticket will belong to.</param>
        /// <returns>A xml representation of the ticket.</returns>
        public static string CreateAuthTicketServiceResult(string ticketType, AbstractUserPrincipal principal)
        {
            string data = AuthServiceHelper.GenerateEncryptedTicketData(principal, DateTime.Now);
            AuthenticationServiceResult serviceResult = new AuthenticationServiceResult(ticketType, data, ConstStage.SiteCode);
            serviceResult.Status = ServiceResultStatus.OK;
            return serviceResult.ToResponse();
        }


        public static void VerifyMobileAccess(AbstractUserPrincipal principal, string devicePassword, string deviceUniqueId)
        {
            if (!principal.BrokerDB.EnableLqbMobileApp)
            {
                throw new AccessDenied(
                    ErrorMessages.LqbMobileAppNotEnabled,
                    $"LQB mobile app disabled for user {principal.LoginNm} ({principal.UserId})");
            }

            var brokerId = principal.BrokerId;
            var userId = principal.UserId;
            var dbConn = principal.ConnectionInfo;

            //check device id
            //load password and hash
            SqlParameter[] parameters = {
                            new SqlParameter("@BrokerID", brokerId),
                            new SqlParameter("@UserID", userId),
                            new SqlParameter("@DeviceUniqueId", deviceUniqueId)
                          };

            using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(dbConn, "dbo.ALL_USER_REGISTERED_MOBILE_DEVICE_PasswordList", parameters))
            {   //for each record
                while (sdr.Read())
                {
                    string id = sdr["Id"].ToString();
                    string passHash = sdr["PasswordHash"].ToString();
                    string passSalt = sdr["PasswordSalt"].ToString();
                    bool passwordMatched = EncryptionHelper.ValidatePBKDF2Hash(devicePassword, passHash, passSalt);
                    //if password matched
                    if (passwordMatched)
                    {
                        //if user don't have device id, add device id
                        if (sdr["DeviceUniqueId"].ToString().Length == 0)
                        {
                            SqlParameter[] addDevicePara = {
                                        new SqlParameter("@Id", id),
                                        new SqlParameter("@BrokerID", brokerId),
                                        new SqlParameter("@UserID", userId),
                                        new SqlParameter("@DeviceUniqueId", deviceUniqueId)
                                      };
                            StoredProcedureHelper.ExecuteNonQuery(dbConn, "ALL_USER_REGISTERED_MOBILE_DEVICE_AddDevice", 1, addDevicePara);
                        }
                        //increate login successful 
                        SqlParameter[] updateLastUsePara = {
                                        new SqlParameter("@Id", id),
                                        new SqlParameter("@BrokerID", brokerId),
                                        new SqlParameter("@UserID", userId),
                                      };
                        StoredProcedureHelper.ExecuteNonQuery(dbConn, "ALL_USER_REGISTERED_MOBILE_DEVICE_UpdateLastUse", 1, updateLastUsePara);
                        return;
                    }
                    //else
                    else
                    {
                        //if other info matched, increase fail attemp and break
                        if (sdr["DeviceUniqueId"] != null && sdr["DeviceUniqueId"].ToString() == deviceUniqueId)
                        {
                            SqlParameter[] failedAttempPara = {
                                        new SqlParameter("@Id", id),
                                        new SqlParameter("@BrokerID", brokerId),
                                        new SqlParameter("@UserID", userId),
                                      };
                            StoredProcedureHelper.ExecuteNonQuery(dbConn, "ALL_USER_REGISTERED_MOBILE_DEVICE_IncreaseFailedAttemp", 1, failedAttempPara);
                        }
                    }
                }

            }
            //if there are no matched record throw exception   
            throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
        }
    }

    // OPM 22934 10-9-08 jk - If the password is expired when trying to authorize, the PasswordExpiredException is thrown. It doesn't
    //just throw another ApplicationException, so that this can be easily caught because it needs to be ignored in function PmlUser.ChangePassword()
    public class PasswordExpiredException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordExpiredException"/> class.
        /// </summary>
        /// <param name="developerMessage">The developer message for the underlying <see cref="CBaseException"/>.</param>
        /// <param name="principal">The principal with an expired password.</param>
        public PasswordExpiredException(string developerMessage, AbstractUserPrincipal principal) : base(ErrorMessages.PasswordExpired, developerMessage)
        {
            this.Principal = principal;
        }

        /// <summary>
        /// Gets the principal with an expired password.
        /// </summary>
        public AbstractUserPrincipal Principal { get; }
    }
}