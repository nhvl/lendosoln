﻿namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text.RegularExpressions;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// This class methods for Registering and Removing mobile devices so they can be used with the LendingQB App.
    /// </summary>
    public class MobileDeviceUtilities
    {
        /// <summary>
        /// The list of characters allowed in password generation.
        /// </summary>
        private static char[] alphaArray = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'T', 'W', 'X', 'Y' };

        /// <summary>
        /// The list of numbers allowed in password generation.
        /// </summary>
        private static char[] numericArray = new char[] { '2', '3', '4', '6', '7', '8', '9' };

        /// <summary>
        /// Deletes a device from the user's list of registered devices.
        /// </summary>
        /// <param name="id">The Id of the device to be removed.</param>
        /// <returns>A boolean that is true if the remove was successful, and false otherwise.</returns>
        public static bool RemoveDevice(long id)
        {
            // Send delete query to server
            // Load login info
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var brokerID = principal.BrokerId;
            var userID = principal.UserId;
            SqlParameter[] parameters =
                {
                    new SqlParameter("@Id", id),
                    new SqlParameter("@BrokerId", brokerID),
                    new SqlParameter("@UserId", userID)
                  };

            // Call stored procedure
            return StoredProcedureHelper.ExecuteNonQuery(principal.ConnectionInfo, "ALL_USER_REGISTERED_MOBILE_DEVICE_Remove", 1, parameters) > 0;
        }

        /// <summary>
        /// Registers a new device for the user's account.
        /// </summary>
        /// <param name="deviceName">The name of the device to be registered.</param>
        /// <param name="mobilePassword">A string that will contain the mobile Password if the registration was successful.</param>
        /// <returns>Returns a bool that is true if the registration was successsful, and false otherwise.</returns>
        public static bool RegisterNewDevice(string deviceName, out string mobilePassword)
        {
            // If mobile name is null, show error message
            // Get phone name
            // Get authentication 
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var brokerID = principal.BrokerId;
            var userID = principal.UserId;

            // Create new password
            string newPassword = MobileDeviceUtilities.CreateNewPassword();

            // Creat random salt and hash pashword
            string salt = string.Empty;
            string passwordHash = EncryptionHelper.GeneratePBKDF2Hash(newPassword, out salt);

            // Store database
            string mobileName = HttpUtility.UrlDecode(deviceName);

            SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerID),
                    new SqlParameter("@UserId", userID),
                    new SqlParameter("@DeviceName", mobileName),
                    new SqlParameter("@PasswordHash", passwordHash),
                    new SqlParameter("@PasswordSalt", salt)
                 };

            bool inserted = StoredProcedureHelper.ExecuteNonQuery(principal.ConnectionInfo, "ALL_USER_REGISTERED_MOBILE_DEVICE_Create", 1, parameters) > 0;

            if (inserted)
            {
                // Show message to user
                mobilePassword = Regex.Replace(newPassword, ".{3}", "$0 ");
            }
            else
            {
                mobilePassword = string.Empty;
            }

            return inserted;
        }

        /// <summary>
        /// Creates a new password for the Mobile Device.
        /// </summary>
        /// <returns>A password for the mobile device.</returns>
        public static string CreateNewPassword()
        {
            string newPassword = string.Empty;

            // Prepare random method
            byte[] randomData = new byte[9];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomData);
            int randomIndex = 0;

            // For each round
            for (int i = 0; i < 3; i++)
            {
                // Choose 1 alphabelt
                int alphaIndex = randomData[randomIndex++] % alphaArray.Count();

                // Choose 1 digit
                int digitIndex1 = randomData[randomIndex++] % numericArray.Count();

                // Choose another 1 digit
                int digitIndex2 = randomData[randomIndex++] % numericArray.Count();

                newPassword += alphaArray[alphaIndex].ToString() + numericArray[digitIndex1].ToString() + numericArray[digitIndex2].ToString();
            }

            return newPassword;
        }

        /// <summary>
        /// Retrieve all devices registered to a user's profile.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <param name="brokerId">The user's broker id.</param>
        /// <param name="connectionInfo">The DBConnectionInfo retrieved from the user's principal.</param>
        /// <returns>A list of every mobile device registered to the user.</returns>
        public static List<MobileDevice> RetrieveUsersMobileDevices(Guid userId, Guid brokerId, DbConnectionInfo connectionInfo)
        {
            List<MobileDevice> mobileDevices = new List<MobileDevice>();
            SqlParameter[] parameters =
                {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@UserId", userId)
                    };

            List<MobileDevice> registerList = new List<MobileDevice>();

            using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(connectionInfo, "ALL_USER_REGISTERED_MOBILE_DEVICE_ListByUser", parameters))
            {
                while (sdr.Read())
                {
                    string createDate = Convert.ToDateTime(sdr["CreateD"]).ToString("G");
                    string lastLogin = sdr["LastUsedD"] == DBNull.Value ? string.Empty : Convert.ToDateTime(sdr["LastUsedD"]).ToString("G");
                    mobileDevices.Add(new MobileDevice((long)sdr["Id"], (string)sdr["DeviceName"], createDate, lastLogin));
                }
            }

            return mobileDevices;
        }
    }
}
