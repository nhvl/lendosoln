﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Security
{
    /// <summary>
    /// 8/27/2013 dd - 
    /// When adding a new role, ALWAYS specify numeric value.
    /// Since the numeric value is store in Workflow Configuration Xml. DO NOT MODIFY existing value.
    /// </summary>
    public enum E_RoleT
    {
        Accountant = 0,
        Administrator = 1,
        CallCenterAgent = 2,
        Closer = 3,
        Consumer = 4,
        Funder = 5,
        LenderAccountExecutive = 6,
        LoanOfficer = 7,
        LoanOpener = 8,
        LockDesk = 9,
        Manager = 10,
        Processor = 11,
        RealEstateAgent = 12,
        Shipper = 13,
        Underwriter = 14,
        PostCloser = 15,
        Insuring = 16,
        CollateralAgent = 17,
        DocDrawer = 18,
        CreditAuditor = 19,
        DisclosureDesk = 20,
        JuniorProcessor = 21,
        JuniorUnderwriter = 22,
        LegalAuditor = 23,
        LoanOfficerAssistant = 24,
        Purchaser = 25,
        QCCompliance = 26,
        Secondary = 27,
        Servicing = 28,

        Pml_LoanOfficer = 50, // This role is same as loan officer. Except it is only available for "P" user.
        Pml_BrokerProcessor = 51,
        Pml_Administrator = 52, // This role is same as admin. Except it is only available for "P" user.
        Pml_Secondary = 70,
        Pml_PostCloser = 71
    }
}
