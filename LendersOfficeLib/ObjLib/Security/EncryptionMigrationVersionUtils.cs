﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Linq;

    /// <summary>
    /// Provides a set of utilities for the encryption migration version
    /// on a loan file.
    /// </summary>
    public static class EncryptionMigrationVersionUtils
    {
        /// <summary>
        /// Gets the latest encryption migration version.
        /// </summary>
        /// <returns>
        /// The latest encryption migration version.
        /// </returns>
        public static EncryptionMigrationVersion GetLatestVersion()
        {
            return Enum.GetValues(typeof(EncryptionMigrationVersion)).Cast<EncryptionMigrationVersion>().Max();
        }

        /// <summary>
        /// Checks if the specified encryption version is greater than or equal to the target version.
        /// </summary>
        /// <param name="loanVersion">
        /// The loan's encryption version enum.
        /// </param>
        /// <param name="targetVersion">
        /// The absolute minimum loan encryption version for this to be true.
        /// </param>
        /// <returns>
        /// True if the loan encryption version is greater than or equal to the target encryption version, false otherwise.
        /// </returns>
        public static bool IsOnOrBeyondEncryptionVersion(EncryptionMigrationVersion loanVersion, EncryptionMigrationVersion targetVersion)
        {
            return (int)loanVersion >= (int)targetVersion;
        }
    }
}
