﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Security
{
    /// <summary>
    /// 8/27/2013 dd - 
    /// This class represent data in the ROLE database table.
    /// 
    /// The class is immutable. Only way to modify data is through database.
    /// </summary>
    public class Role
    {
        public Guid Id { get; private set; }

        /// <summary>
        /// Represent short description of role. Should not be change once role is create.
        /// </summary>
        public string Desc { get; private set; }

        public E_RoleT RoleT { get; private set; }

        /// <summary>
        /// Friendly display name.
        /// </summary>
        public string ModifiableDesc { get; private set; }

        public int ImportanceRank { get; private set; }

        public Guid DefaultPipelineReportId { get; private set; }

        private Role(DbDataReader reader)
        {
            this.Id = (Guid)reader["RoleId"];
            this.Desc = (string)reader["RoleDesc"];
            this.ModifiableDesc = (string)reader["RoleModifiableDesc"];
            this.ImportanceRank = (int)reader["RoleImportanceRank"];
            this.DefaultPipelineReportId = (Guid)reader["RoleDefaultPipelineReportId"];

            // 8/27/2013 dd - When add new role, need to map to the enum type.

            switch (Desc) {
                case "Administrator": this.RoleT = E_RoleT.Administrator; break;
                case "Manager": this.RoleT = E_RoleT.Manager; break;
                case "Accountant": this.RoleT = E_RoleT.Accountant; break;
                case "Underwriter": this.RoleT = E_RoleT.Underwriter; break;
                case "LockDesk": this.RoleT = E_RoleT.LockDesk; break;
                case "Processor": this.RoleT = E_RoleT.Processor; break;
                case "LoanOpener": this.RoleT = E_RoleT.LoanOpener; break;
                case "BrokerProcessor": this.RoleT = E_RoleT.Pml_BrokerProcessor; break;
                case "Agent": this.RoleT = E_RoleT.LoanOfficer; break;
                case "LenderAccountExec": this.RoleT = E_RoleT.LenderAccountExecutive; break;
                case "RealEstateAgent": this.RoleT = E_RoleT.RealEstateAgent; break;
                case "Telemarketer": this.RoleT = E_RoleT.CallCenterAgent; break;
                case "DocDrawer": this.RoleT = E_RoleT.DocDrawer; break;
                case "Shipper": this.RoleT = E_RoleT.Shipper; break;
                case "Closer": this.RoleT = E_RoleT.Closer; break;
                case "Consumer": this.RoleT = E_RoleT.Consumer; break;
                case "Funder": this.RoleT = E_RoleT.Funder; break;
                case "PostCloser": this.RoleT = E_RoleT.PostCloser; break;
                case "Insuring": this.RoleT = E_RoleT.Insuring; break;
                case "CollateralAgent": this.RoleT = E_RoleT.CollateralAgent; break;
                case "CreditAuditor": this.RoleT = E_RoleT.CreditAuditor; break;
                case "DisclosureDesk": this.RoleT = E_RoleT.DisclosureDesk; break;
                case "JuniorProcessor": this.RoleT = E_RoleT.JuniorProcessor; break;
                case "JuniorUnderwriter": this.RoleT = E_RoleT.JuniorUnderwriter; break;
                case "LegalAuditor": this.RoleT = E_RoleT.LegalAuditor; break;
                case "LoanOfficerAssistant": this.RoleT = E_RoleT.LoanOfficerAssistant; break;
                case "Purchaser": this.RoleT = E_RoleT.Purchaser; break;
                case "QCCompliance": this.RoleT = E_RoleT.QCCompliance; break;
                case "Secondary": this.RoleT = E_RoleT.Secondary; break;
                case "Servicing": this.RoleT = E_RoleT.Servicing; break;
                case "ExternalSecondary": this.RoleT = E_RoleT.Pml_Secondary; break;
                case "ExternalPostCloser": this.RoleT = E_RoleT.Pml_PostCloser; break;
                default:
                    throw CBaseException.GenericException("Unhandle Role Desc=[" + Desc + "]");
            }
        }

        private static void Initialize()
        {
            s_allRoleList = new List<Role>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetAllRoles"))
            {
                while (reader.Read())
                {
                    s_allRoleList.Add(new Role(reader));
                }
            }
        }

        static Role()
        {
            // 8/27/2013 dd - Since role information is static and can only be change through database.
            // Therefore I only initialize during static constructor.
            Initialize();
        }

        private static List<Role> s_allRoleList = null;

        public static IEnumerable<Role> AllRoles
        {
            get { return s_allRoleList; }
        }

        /// <summary>
        /// Return only roles relate to "B" user.
        /// </summary>
        public static IEnumerable<Role> LendingQBRoles
        {
            get
            {
                foreach (var o in AllRoles)
                {
                    if (o.RoleT == E_RoleT.Consumer ||
                        o.RoleT == E_RoleT.Pml_BrokerProcessor ||
                        o.RoleT == E_RoleT.Pml_Secondary ||
                        o.RoleT == E_RoleT.Pml_PostCloser)
                    {
                        // 8/28/2013 dd - Skip PML roles and Consumer.
                        continue;
                    }
                    yield return o;
                }
            }
        }

        public static Role Get(E_RoleT roleType)
        {
            foreach (var role in AllRoles)
            {
                if (role.RoleT == roleType)
                {
                    return role;
                }

            }
            throw new UnhandledEnumException(roleType);
        }

        public static string GetRoleDescription(E_RoleT roleType)
        {
            if (roleType == E_RoleT.Pml_LoanOfficer) 
            {
                return "Loan Officer (PML)";
            } 
            else if (roleType == E_RoleT.Pml_Administrator) 
            {
                return "Administrator (PML)";
            }
            else 
            {
                // 8/27/2013 dd - Pull from RoleModifiableDesc field.
                Role role = Get(roleType);
                return role.ModifiableDesc;
            }
        }
        /// <summary>
        /// Get role by short description. Must MATCH exactly.
        /// </summary>
        /// <param name="roleDesc"></param>
        /// <returns></returns>
        public static Role Get(string roleDesc)
        {
            if (string.IsNullOrEmpty(roleDesc)) 
            {
                throw new NotFoundException("Role description cannot be empty.");
            }

            foreach (var role in AllRoles)
            {
                if (role.Desc == roleDesc)
                {
                    return role;
                }
            }
            throw new NotFoundException("Role description [" + roleDesc + "] is not a valid role.");
        }

        public static Role Get(Guid roleId)
        {
            foreach (var role in AllRoles)
            {
                if (role.Id == roleId)
                {
                    return role;
                }
            }
            throw new NotFoundException("Role description [" + roleId + "] is not a valid role.");

        }

        public static bool IsValidRoleId(Guid roleId)
        {
            foreach (var role in AllRoles)
            {
                if (role.Id == roleId)
                {
                    return true;
                }
            }
            return false;
        }
        public static E_AgentRoleT GetAgentRoleT(E_RoleT roleType)
        {
            switch (roleType)
            {
                case E_RoleT.CallCenterAgent:
                    return E_AgentRoleT.CallCenterAgent;
                case E_RoleT.Funder:
                    return E_AgentRoleT.Funder;
                case E_RoleT.LenderAccountExecutive:
                    return E_AgentRoleT.Lender; // OPM 1312
                case E_RoleT.LoanOfficer:
                    return E_AgentRoleT.LoanOfficer;
                case E_RoleT.LoanOpener:
                    return E_AgentRoleT.LoanOpener;
                case E_RoleT.Manager:
                    return E_AgentRoleT.Manager;
                case E_RoleT.Processor:
                    return E_AgentRoleT.Processor;
                case E_RoleT.RealEstateAgent:
                    return E_AgentRoleT.ListingAgent;
                case E_RoleT.Shipper:
                    return E_AgentRoleT.Shipper;
                case E_RoleT.Underwriter:
                    return E_AgentRoleT.Underwriter;
                case E_RoleT.PostCloser:
                    return E_AgentRoleT.PostCloser;
                case E_RoleT.Insuring:
                    return E_AgentRoleT.Insuring;
                case E_RoleT.CollateralAgent:
                    return E_AgentRoleT.CollateralAgent;
                case E_RoleT.DocDrawer:
                    return E_AgentRoleT.DocDrawer;
                case E_RoleT.Pml_BrokerProcessor:
                    return E_AgentRoleT.BrokerProcessor;
                case E_RoleT.CreditAuditor:
                    return E_AgentRoleT.CreditAuditor;
                case E_RoleT.DisclosureDesk:
                    return E_AgentRoleT.DisclosureDesk;
                case E_RoleT.JuniorProcessor:
                    return E_AgentRoleT.JuniorProcessor;
                case E_RoleT.JuniorUnderwriter:
                    return E_AgentRoleT.JuniorUnderwriter;
                case E_RoleT.LegalAuditor:
                    return E_AgentRoleT.LegalAuditor;
                case E_RoleT.LoanOfficerAssistant:
                    return E_AgentRoleT.LoanOfficerAssistant;
                case E_RoleT.Purchaser:
                    return E_AgentRoleT.Purchaser;
                case E_RoleT.QCCompliance:
                    return E_AgentRoleT.QCCompliance;
                case E_RoleT.Secondary:
                    return E_AgentRoleT.Secondary;
                case E_RoleT.Servicing:
                    return E_AgentRoleT.Servicing;
                case E_RoleT.Pml_Secondary:
                    return E_AgentRoleT.ExternalSecondary;
                case E_RoleT.Pml_PostCloser:
                    return E_AgentRoleT.ExternalPostCloser;

                case E_RoleT.Pml_LoanOfficer:
                case E_RoleT.LockDesk:
                case E_RoleT.Closer:
                case E_RoleT.Consumer:
                case E_RoleT.Accountant:
                case E_RoleT.Administrator:
                case E_RoleT.Pml_Administrator:
                    // 8/27/2013 dd - These have no equivalent AgentRoleT. Return other.
                    return E_AgentRoleT.Other;
                default:
                    throw new UnhandledEnumException(roleType);
            }
        }

        public static string GetLoanFileCacheFieldName(E_RoleT roleType)
        {
            switch (roleType)
            {
                case E_RoleT.CallCenterAgent: return "sEmployeeCallCenterAgentId";
                case E_RoleT.Closer: return "sEmployeeCloserId";
                case E_RoleT.Funder: return "sEmployeeFunderId";
                case E_RoleT.LenderAccountExecutive: return "sEmployeeLenderAccExecId";
                case E_RoleT.LoanOfficer: return "sEmployeeLoanRepId";
                case E_RoleT.LoanOpener: return "sEmployeeLoanOpenerId";
                case E_RoleT.LockDesk: return "sEmployeeLockDeskId";
                case E_RoleT.Manager: return "sEmployeeManagerId";
                case E_RoleT.Processor: return "sEmployeeProcessorId";
                case E_RoleT.RealEstateAgent: return "sEmployeeRealEstateAgentId";
                case E_RoleT.Shipper: return "sEmployeeShipperId";
                case E_RoleT.Underwriter: return "sEmployeeUnderwriterId";
                case E_RoleT.PostCloser: return "sEmployeePostCloserId";
                case E_RoleT.Insuring: return "sEmployeeInsuringId";
                case E_RoleT.CollateralAgent: return "sEmployeeCollateralAgentId";
                case E_RoleT.DocDrawer: return "sEmployeeDocDrawerId";
                case E_RoleT.Pml_BrokerProcessor: return "sEmployeeBrokerProcessorId";
                case E_RoleT.CreditAuditor: return "sEmployeeCreditAuditorId";
                case E_RoleT.DisclosureDesk: return "sEmployeeDisclosureDeskId";
                case E_RoleT.JuniorProcessor: return "sEmployeeJuniorProcessorId";
                case E_RoleT.JuniorUnderwriter: return "sEmployeeJuniorUnderwriterId";
                case E_RoleT.LegalAuditor: return "sEmployeeLegalAuditorId";
                case E_RoleT.LoanOfficerAssistant: return "sEmployeeLoanOfficerAssistantId";
                case E_RoleT.Purchaser: return "sEmployeePurchaserId";
                case E_RoleT.QCCompliance: return "sEmployeeQCComplianceId";
                case E_RoleT.Secondary: return "sEmployeeSecondaryId";
                case E_RoleT.Servicing: return "sEmployeeServicingId";
                case E_RoleT.Pml_Secondary: return "sEmployeeExternalSecondaryId";
                case E_RoleT.Pml_PostCloser: return "sEmployeeExternalPostCloserId";

                // There are no associate columns in LOAN_FILE_CACHE for these roles
                case E_RoleT.Pml_LoanOfficer:
                case E_RoleT.Pml_Administrator:
                case E_RoleT.Accountant:
                case E_RoleT.Administrator:
                case E_RoleT.Consumer:
                default:
                    throw new UnhandledEnumException(roleType);
            }
        }
    }
}
