using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.ActiveDirectory;
using LendersOffice.ObjLib.ConsumerPortal;
using System.Web;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using LendersOffice.Admin;

namespace LendersOffice.Security
{
	public class PrincipalFactory
	{
		public enum E_LoginProblem
		{
			None = 0,					            // successful login
			InvalidLoginPassword = 1,	            // invalid login or password
			IsDisabled = 2,				            // account is disabled
			IsLocked = 3,				            // account is locked
			NeedsToWait = 4,			            // user must wait before attempting to log in
			InvalidAndWait = 5,			            // invalid password and due to the current # of failures, now must wait before trying to log in again
            TempPasswordExpired = 6,	            // the temporary password has expired
            InvalidCustomerCode = 7,                // invalid customer code
            ActiveDirectorySetupIncomplete = 8,     // AD setup incomplete
            MustLoginAsActiveDirectoryUserType = 9  // normal user authentication path was chosed for AD user; must choose AD path
		}
        
        /// <summary>
        /// Enter a record to ALL_USER_SHARED_DB table. If this method fail then there is a duplicate login.
        /// This method should get call everytime "B" and "I" user account is update.
        /// Do not call this method when "P" user is update.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="userId"></param>
        /// <param name="loginNm"></param>
        public static void AllUserSharedDbSync(Guid brokerId, Guid userId, string loginNm)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginNm),
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "ALL_USER_SHARED_DB_Sync", 0, parameters);
        }

        /// <summary>
        /// Removes a record from the ALL_USER_SHARED_DB table, intended for use
        /// in the event a user's record fails creation and is rolled back.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the lender for the user.
        /// </param>
        /// <param name="loginName">
        /// The login name for the user.
        /// </param>
        public static void AllUserSharedDbDelete(Guid brokerId, string loginName)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoginName", loginName)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "ALL_USER_SHARED_DB_Delete", 0, parameters);
        }

		/// <summary>
		/// Gets the BrokerPmlSiteId associated with the given customer code.
		/// We might need to think about forcing the customer codes to be unique, because there is currently
		/// the potential for 2 of the same login name to share the same customer code, even though they 
		/// exist in separate brokers.  This could potentially mess up the login procedure for webservices.
		/// For now, we pair with the login name for all checks, as this has always been the behavior,
		/// and we are the ones who set up the customer codes, not the users (and we choose to make them unique
		/// even though it is not forced)
		/// </summary>
		/// <param name="customerCode"></param>
		/// <returns></returns>
		public static Guid GetBrokerPmlSiteIdFromCustomerCode(string customerCode)
		{

			Guid brokerPmlSiteId = Guid.Empty;

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@CustomerCode", customerCode)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveBrokerPmlSiteIdByCustomerCode", parameters))
                {
                    if (reader.Read())
                    {

                        brokerPmlSiteId = (Guid)reader["BrokerPmlSiteID"];
                        break;
                    }
                }
            }

			return brokerPmlSiteId;
		}

        // OPM 28343 db - auth ID will now expire after 2 minutes, so we will check here if it's expired, and if so, return a null principal
        // The entry in the cache takes the auth ID as the cache ID and a DateTime representing the expiration time.
        // We do not clear the auth ID from the all_users table explicitly if the ID is expired because that would
        // require an extra hit to the large all_users table, and it gets cleared automatically anyways when the user
        // successfully logs in.  Therefore, it will still hang around (permanently unused) until the user successfully 
        // logs in, but that is worth the trade off in not having another db hit.
        private static bool IsAuthenticationIdExpired(Guid authenticationID)
        {
            string authIdExpiresAt = AutoExpiredTextCache.GetFromCache(authenticationID);
            
            if (!string.IsNullOrEmpty(authIdExpiresAt))
            {
                try
                {
                    DateTime expiresAtDT = DateTime.Parse(authIdExpiresAt);
                    if (DateTime.Now >= expiresAtDT)
                    {
                        return true;
                    }
                }
                catch { }
            }

            return false;
        }
        
        /// <summary>
        /// Return user principal based on third party unique authenticate id.
        /// </summary>
        /// <param name="authenticateID"></param>
        /// <returns></returns>
        public static AbstractUserPrincipal Create(Guid authenticationID) 
        {
            if (IsAuthenticationIdExpired(authenticationID))
            {
                return null;
            }

            DbConnectionInfo connInfo = null;

            // 4/26/2015 dd - Locate which database the authentication id locate.
            foreach (DbConnectionInfo o in DbConnectionInfo.ListAll())
            {
                SqlParameter[] ps = {
                                        new SqlParameter("@ByPassTicket", authenticationID)
                                    };

                using (var reader = StoredProcedureHelper.ExecuteReader(o, "ALL_USER_GetUserIdByByPassTicket", ps))
                {
                    if (reader.Read())
                    {
                        connInfo = o;
                        break;
                    }
                }
            }

            if (connInfo == null)
            {
                return null;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@AuthenticationId", authenticationID)
                                        };

            return BrokerUserPrincipal.CreatePrincipal(connInfo, E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByAuthenticationId, parameters, 
                Guid.Empty, true, E_ApplicationT.LendersOffice, true, true /*updateCurrentPrincipal*/);

        }

		public static double GetMinutesToWait(string loginNm, string type, Guid BrokerPmlSiteId)
		{
			return Math.Floor((double) (GetLoginFailureCount(loginNm, type, BrokerPmlSiteId)/10));
		}

		public static double GetMinutesToWait(string loginNm, string type, string customerCode)
		{
            Guid brokerPmlSiteId = GetBrokerPmlSiteIdFromCustomerCode(customerCode);

            return GetMinutesToWait(loginNm, type, brokerPmlSiteId);
		}

		public static int GetLoginFailureCount(string loginNm, string type, Guid BrokerPmlSiteId)
		{
			int loginFailureCount = 0;

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = { 
                                                new SqlParameter("@LoginNm", loginNm),
                                                new SqlParameter("@Type", type), 
                                                new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetLoginFailureCount", parameters))
                {
                    if (reader.Read())
                    {
                        loginFailureCount = (int)reader["LoginFailureCount"];
                        break; // Break out of the foreach loop
                    }
                }
            }

			return loginFailureCount;
		}

        private static AbstractUserPrincipal Create( Guid brokerId, string userType, Guid userID, Guid loginSessionID, Guid becomeUserID, string cookieSessionId, InitialPrincipalTypeT initialPrincipalType, Guid initialUserId, PostLoginTask postLoginTask)
        {

            AbstractUserPrincipal principal = null;
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@UserID", userID));

            switch (userType) 
            {
                case "B":
                    parameters.Add(new SqlParameter("@BrokerId", brokerId));

                    int secondsToCache = ConstStage.BrokerUserPrincipalCacheInSeconds;

                    if (secondsToCache > 0)
                    {
                        principal = GetPrincipalFromCache(brokerId, userID, loginSessionID, secondsToCache);
                    }

                    if (principal == null)
                    {
                        DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

                        principal = BrokerUserPrincipal.CreatePrincipal(connInfo, E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByUserId, parameters,
                            loginSessionID, false, E_ApplicationT.LendersOffice, true, true /*updateCurrentPrincipal */, cookieSessionId, initialPrincipalType, initialUserId, postLoginTask);
                    }
                    else
                    {
                        // 1/21/2015 dd - If we retrieve principal from cache then we need to set it to cookie and put it to principal thread.
                        if (null != HttpContext.Current && null != HttpContext.Current.Response)
                        {
                            RequestHelper.StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                        }
                        System.Threading.Thread.CurrentPrincipal = principal;
                    }

                    if (secondsToCache > 0 && principal != null)
                    {
                        PutPrincipalToCache(userID, principal);
                    }
                    break;
                case "I":
                    principal = InternalUserPrincipal.CreatePrincipal("RetrieveInternalUserCredentialByUserId", parameters, becomeUserID, initialUserId, postLoginTask);
                    break;
                default:
                    Tools.LogBug("Invalid user type in PrincipalFactory: userType = " + userType + ", UserId=" + userID);
                    break;
            }
            return principal;
        }

        private static Dictionary<Guid, AbstractUserPrincipal> x_principalDictionary = new Dictionary<Guid, AbstractUserPrincipal>();
        private static object x_principalDictionaryLock = new object();

        private static AbstractUserPrincipal GetPrincipalFromCache(Guid brokerId, Guid userId, Guid loginSessionId, int secondsToCache)
        {
            // 1/21/2015 dd - This caching method is create to ONLY handle RetrieveUserCredentialByUserId. Don't use this method
            //                for anything else yet.


            if (secondsToCache == 0)
            {
                return null;
            }
            lock (x_principalDictionaryLock)
            {
                AbstractUserPrincipal principal = null;

                if (x_principalDictionary.TryGetValue(userId, out principal))
                {
                    if (principal == null)
                    {
                        return null;
                    }

                    // 1/21/2015 dd - If principal is older than seconds to cache then remove from dictionary and return null.
                    if (principal.CreatedDate.AddSeconds(secondsToCache) < DateTime.Now)
                    {
                        x_principalDictionary.Remove(userId);
                        principal = null;
                    }
                    else if (principal.LoginSessionId != loginSessionId)
                    {
                        // 1/21/2015 dd - Login session id is not same as cache. Remove from dictionary and return null.
                        x_principalDictionary.Remove(userId);
                        principal = null;
                    }
                    else if ((principal is BrokerUserPrincipal && ((BrokerUserPrincipal)principal).IsPasswordExpired))
                    {
                        x_principalDictionary.Remove(userId);
                        principal = null;
                    }
                }

                return principal;
            }

        }

        private static void PutPrincipalToCache(Guid userId, AbstractUserPrincipal principal)
        {
            lock (x_principalDictionaryLock)
            {
                if (x_principalDictionary.Count == 5000)
                {
                    // 1/21/2015 dd - To prevent store too much in memory, I only keep 5000 active principals.
                    x_principalDictionary = new Dictionary<Guid, AbstractUserPrincipal>();
                }
                x_principalDictionary[userId] = principal;
            }
        }

        public static AbstractUserPrincipal Create(FormsIdentity identity)
        {
            var principal = CreateImpl(identity.Ticket);
            if (principal == null)
            {
                RequestHelper.Signout();
            }
            return principal;
        }

        /// <summary>
        /// Takes an authentication ticket and creates a principal if the ticket is valid. It will also update
        /// the cookies in the response with a new usable cookie for the next request.  
        /// </summary>
        /// <param name="ticket">The authentication ticket to validate.</param>
        /// <returns>Null if the ticket could not be validated or the principal.</returns>
        public static AbstractUserPrincipal CreateAndUpdateResponseCookie(FormsAuthenticationTicket ticket)
        {
            var principal = CreateImpl(ticket);
            return principal;
        }

        public static AbstractUserPrincipal CreateImpl(FormsAuthenticationTicket ticket)
        {
            // NOTE: UserData CANNOT exceed 1000 characters or it will not stored in cookies.
            // UserData = UserType|UserID|LoginSessionId|ApplicationType|BecomeUserId|CookieSessionId
            // 9/1/2014 dd - Add Broker Id to cookies.
            // UserType | UserID | LoginSessionId | ApplicationType | BecomeUserId | CookieSessionId | BrokerId
            // 6/25/2018 jk - Add Initial Principal Type & Initial User Id to cookies.
            // UserType | UserID | LoginSessionId | ApplicationType | BecomeUserId | CookieSessionId | BrokerId | InitialPrincipalType | InitialUserId
            // 9/21/2018 jk - Add PostLoginTask to cookies
            // UserType | UserID | LoginSessionId | ApplicationType | BecomeUserId | CookieSessionId | BrokerId | InitialPrincipalType | InitialUserId | PostLoginTask
            string[] data = ticket.UserData.Split('|');

            // OPM 19222 -jM. now because of IP field  |IP Address <- populated in RequestHelper and length is now 7
            // OPM 471099 - jk. IP address was removed in case 24152, & now we're at length 9 with InitialPrincipalType & InitialUserId

            PostLoginTask postLoginTask = PostLoginTask.Login;

            if (data.Length == 10)
            {
                Enum.TryParse(data[9], out postLoginTask);
            }
            else if (data.Length != 9)
            {
                return null;
            }

            string userType = data[0];
            Guid userID = new Guid(data[1]);
            Guid loginSessionID = new Guid(data[2]);
            Guid becomeUserID = Guid.Empty;

            try
            {
                becomeUserID = new Guid(data[4]);
            }
            catch (FormatException) { }

            string cookieSessionId = data[5];
            Guid brokerId = Guid.Empty;
            try
            {
                brokerId = new Guid(data[6]);
            }
            catch (FormatException) { }

            InitialPrincipalTypeT initialPrincipalType;
            Enum.TryParse(data[7], out initialPrincipalType);

            Guid initialUserId = Guid.Empty;
            Guid.TryParse(data[8], out initialUserId);

            AbstractUserPrincipal principal = Create(brokerId, userType, userID, loginSessionID, becomeUserID, cookieSessionId, initialPrincipalType, initialUserId, postLoginTask);
            return principal;
        }

        public static ConsumerPortalUserPrincipal CreateConsumerPrincipalWithFailureType(Guid brokerId, Guid consumerPortalId, string email, string password, out E_LoginProblem loginProblem)
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(brokerId, email);
            bool usedAltPassword = false;
            if (user == null)
            {
                loginProblem = E_LoginProblem.InvalidAndWait;
                var logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, email, firstNm: string.Empty, lastNm: string.Empty, initialPrincipalType: InitialPrincipalTypeT.NonInternal, type: "C");
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                return null;
            }
            else
            {
                var logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, email, user.FirstName, user.LastName, InitialPrincipalTypeT.NonInternal, "C");

                if (user.PasswordResetCodeRequestD.HasValue && user.PasswordResetCode.Length > 0 && user.PasswordResetCode == password)
                {
                    if (user.PasswordResetCodeRequestD.Value.AddHours(3) < DateTime.Now)
                    {
                        loginProblem = E_LoginProblem.TempPasswordExpired;
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                        return null;
                    }
                    usedAltPassword = true;
                }
                else if (user.IsLockedOut)
                {
                    loginProblem = E_LoginProblem.IsLocked;
                    SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
                    return null;
                }
                else if (!user.IsPasswordValid(password))
                {
                    // 2/23/2015 dd - Attempts to find out why user could enter incorrect temporary password.
                    // NEVER LOG actual password even if it incorrect.
                    if (user.IsTemporaryPassword)
                    {
                        // 2/23/2015 dd - Only log when user has temporary password.
                        int originalLength = password.Length;
                        int trimLength = password.TrimWhitespaceAndBOM().Length;

                        int upper = 0;
                        int lower = 0;
                        int digit = 0;
                        int other = 0;
                        for (int j = 0; j < password.Length; j++)
                        {
                            if (char.IsDigit(password, j))
                            {
                                digit++;
                            }
                            else if (char.IsLower(password, j))
                            {
                                lower++;
                            }
                            else if (char.IsUpper(password, j))
                            {
                                upper++;
                            }
                            else
                            {
                                other++;
                            }
                        }
                        Tools.LogInfo("[" + email + "] - Failed Login . Pre-Trim Password Length=" + originalLength + ". Trim Password Length=" + trimLength + ". Upper=" + upper + ", Lower=" + lower + ", Digit=" + digit + ", Other=" + other);
                    }

                    user.LoginAttempts += 1;
                    user.Save();
                    loginProblem = E_LoginProblem.InvalidLoginPassword;
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                    return null;
                }
                else if (user.IsTemporaryPassword && user.TempPasswordSetD.HasValue && user.TempPasswordSetD.Value.AddDays(ConstApp.CPTempPasswordValidLengthInDays) < DateTime.Now)
                {
                    loginProblem = E_LoginProblem.TempPasswordExpired;
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                    return null;
                }
            }
            
            user.LoginAttempts = 0; 
            user.LastLoginD = DateTime.Now;
            user.PasswordResetCode = "";
            user.PasswordResetCodeRequestD = null;
            user.Save();
            loginProblem = E_LoginProblem.None;
            var principal =  new ConsumerPortalUserPrincipal(new GenericIdentity(email), user, consumerPortalId, Guid.Empty);
            if (usedAltPassword)
            {
                principal.IsTemporaryPassword = true;
            }
            SecurityEventLogHelper.CreateLoginLog(principal);
            return principal;
        }

        //3/26/07 db - OPM 10294 Notify users if their account has been disabled
        //8/08/07 db - OPM 1863 Enhance this to notify if blocked or needs to wait as well
        public static AbstractUserPrincipal CreateWithFailureType(string userName, string password, out PrincipalFactory.E_LoginProblem loginProblem, bool allowDuplicateLogin, bool isStoreToCookie, LoginSource loginSource) 
        {
            // 8/19/2005 dd - We want user password to be case insensitive
            try
            {
                Guid brokerId = Guid.Empty;
                DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoginName(userName, out brokerId);

                SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", userName)
                                        };
                return BrokerUserPrincipal.CreatePrincipalWithFailureType(brokerId, connInfo, E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByUserName, parameters, password,
                    Guid.Empty /* expectedLoginSessionId */,
                    allowDuplicateLogin, E_ApplicationT.LendersOffice,
                    isStoreToCookie, out loginProblem,
                    loginSource);
            }
            catch (NotFoundException)
            {
                loginProblem = E_LoginProblem.InvalidLoginPassword;
            }
            return null;
        }

        public static AbstractUserPrincipal CreateWithFailureTypeForActiveDirectoryUser(string sADUserName, string sPassword, string sCustomerCode, out PrincipalFactory.E_LoginProblem loginProblem, bool bAllowDuplicateLogin, LoginSource loginSource)
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = null;

            try
            {
                connInfo = DbConnectionInfo.GetConnectionInfoByCustomerCode(sCustomerCode, out brokerId);
            }
            catch (NotFoundException)
            {
                loginProblem = E_LoginProblem.InvalidCustomerCode;
                return null;
            }

            if (brokerId == Guid.Empty)
            {
                loginProblem = E_LoginProblem.InvalidCustomerCode;
                return null;
            }

            ActiveDirectoryHelper adHelper = new ActiveDirectoryHelper(brokerId);
            if (!adHelper.IsLenderADAccountSetupComplete())
            {
                loginProblem = E_LoginProblem.ActiveDirectorySetupIncomplete;
                return null;
            }

            Guid userID = GetLQBUserId(connInfo, brokerId, sADUserName);

            var employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerAndUserId(brokerId, userID);
            AbstractUserPrincipal logPrincipal = null;

            if (employeeDB != null)
            {
                logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, sADUserName, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "B", userID);
            }
            
            if (userID == Guid.Empty)
            {
                loginProblem = (IsLQBUserDisabled(connInfo, sADUserName)) ? E_LoginProblem.IsDisabled : E_LoginProblem.InvalidLoginPassword;
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                return null;
            }

            if (!adHelper.IsLQBUserAnActiveDirectoryUser(userID))
            {
                loginProblem = E_LoginProblem.ActiveDirectorySetupIncomplete;
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                return null;
            }

            if (!adHelper.Authenticate(sADUserName, sPassword))
            {
                loginProblem = E_LoginProblem.InvalidLoginPassword;
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                return null;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserID", userID),
                                            new SqlParameter("@BrokerID", brokerId)
                                        };

            return BrokerUserPrincipal.CreatePrincipalWithFailureTypeForActiveDirectoryUser(connInfo, E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByUserId, parameters, 
                sADUserName, bAllowDuplicateLogin, out loginProblem, logPrincipal, loginSource);
        }

        private static Guid GetBrokerIdByCustomerCode(string sCustomerCode)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@CustomerCode", sCustomerCode)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveBrokerIdByCustomerCode", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["BrokerID"];
                }
            }

            return Guid.Empty;
        }

        private static Guid GetLQBUserId(DbConnectionInfo connInfo, Guid brokerId, string loginName)
        {
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId),
                                                new SqlParameter("@UserType", "B"),
                                                new SqlParameter("@LoginName", loginName)
                                            };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveUserIdByLoginNameAndBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        return (Guid)reader["UserId"];
                    }
                }
            }
            catch (SqlException sqlExc)
            {
                Tools.LogError(String.Format("Failed to retrieve UserId for LQB user [{0}]. BrokerId:{1}", loginName, brokerId), sqlExc);
            }

            return Guid.Empty;
        }

        private static bool IsLQBUserDisabled(DbConnectionInfo connInfo, string sLoginName)
        {
            SqlParameter[] parameters = {
                                                new SqlParameter("@LoginName", sLoginName),
                                                new SqlParameter("@Type", "B")
                                            };

            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveDisabledLendersOfficeUserByLoginNm", parameters))
            {
                return reader.Read();
            }
        }

        public static AbstractUserPrincipal CreatePmlUserWithFailureType(string userName, string password, string customerCode, out PrincipalFactory.E_LoginProblem loginProblem, bool isStoreToCookie, Guid brokerId, LoginSource loginSource) 
		{
            try
            {
                DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

                SqlParameter[] parameters = {
											new SqlParameter("@LoginName", userName),
											new SqlParameter("@CustomerCode", customerCode)
										};

                return BrokerUserPrincipal.CreatePrincipalWithFailureType(brokerId, connInfo, E_RetrieveBrokerUserStoreProcedureT.RetrievePmlUserCredentialByUserName, parameters, password,
                    Guid.Empty, true, E_ApplicationT.PriceMyLoan, isStoreToCookie, out loginProblem, loginSource);
            }
            catch (NotFoundException)
            {
                // NO-OP    
            }

            loginProblem = E_LoginProblem.InvalidLoginPassword;
            return null;
		}

        public static AbstractUserPrincipal Create(Guid brokerId, Guid userId, string userType, bool allowDuplicateLogin, bool isStoreToCookie, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid())
        {
            return Create(brokerId, userId, userType, allowDuplicateLogin, isStoreToCookie, true, storedProcedure: E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByUserId, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }

        public static AbstractUserPrincipal CreateInactive(Guid brokerId, Guid userId, string userType)
        {
            return Create(brokerId, userId, userType, allowDuplicateLogin: true, isStoreToCookie: false, isUpdatePrincipal: false, storedProcedure: E_RetrieveBrokerUserStoreProcedureT.RetrieveInactiveUserCredentialByUserId);
        }

        private static AbstractUserPrincipal Create(Guid brokerId, Guid userID, string userType, bool allowDuplicateLogin, bool isStoreToCookie, bool isUpdatePrincipal, E_RetrieveBrokerUserStoreProcedureT storedProcedure = E_RetrieveBrokerUserStoreProcedureT.RetrieveUserCredentialByUserId, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid()) 
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@UserId", userID));

            if (brokerId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@BrokerId", brokerId));
            }

            E_ApplicationT applicationT = E_ApplicationT.ToBeDefineFromUserType;
            switch (userType) 
            {
                case "B":
                    applicationT = E_ApplicationT.LendersOffice;
                    break;
                case "P":
                    applicationT = E_ApplicationT.PriceMyLoan;
                    break;
            }

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            return BrokerUserPrincipal.CreatePrincipal(connInfo, storedProcedure, parameters, 
                        Guid.Empty, allowDuplicateLogin, applicationT, isStoreToCookie, isUpdatePrincipal, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);

        }

        /// <summary>
        ///  Only retrieve principal while not setting the cookie or replacing the current principal.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="userId"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        public static AbstractUserPrincipal RetrievePrincipalForUser(Guid brokerId, Guid userId, string userType, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid())
        {
            return Create(brokerId, userId, userType, true, false, false, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }

        public static AbstractUserPrincipal CurrentPrincipal 
        {
            get 
            {
                return System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            }
        }

        public static string GetSimpleSerializedString()
        {
            var principal = CurrentPrincipal;
            if (principal == null)
            {
                return null;
            }

            Guid brokerId = principal.BrokerId;
            Guid userId = principal.UserId;
            string userType = principal.Type;

            string serialized = string.Join("|", userType, userId.ToString("N"), brokerId.ToString("N"));
            return EncryptionHelper.Encrypt(serialized);
        }

        public static AbstractUserPrincipal RetrieveFromSimpleSerialized(string serialized)
        {
            if (string.IsNullOrEmpty(serialized))
            {
                return null;
            }

            string decrypted = EncryptionHelper.Decrypt(serialized);
            string[] pieces = decrypted.Split('|');
            if (pieces.Length != 3)
            {
                return null;
            }

            if (string.IsNullOrEmpty(pieces[0]) || pieces[0].Length != 1)
            {
                return null;
            }

            string userType = pieces[0];

            Guid userId;
            bool good = Guid.TryParse(pieces[1], out userId);
            if (!good)
            {
                return null;
            }

            Guid brokerId;
            good = Guid.TryParse(pieces[2], out brokerId);
            if (!good)
            {
                return null;
            }

            return RetrievePrincipalForUser(brokerId, userId, userType);
        }
	}
}
