﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Provides access to all registered IP addresses for a lender's users.
    /// </summary>
    public class UserRegisteredIpAddresses
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private readonly Guid brokerId;

        /// <summary>
        /// A value indicating whether the data has been retrieved from the data source.
        /// </summary>
        private bool loadedData;

        /// <summary>
        /// A map from user id to the registered IP addresses for that user.
        /// </summary>
        private Dictionary<Guid, List<UserRegisteredIp>> registeredIpsByUserId;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRegisteredIpAddresses"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        public UserRegisteredIpAddresses(Guid brokerId)
        {
            this.brokerId = brokerId;
        }

        /// <summary>
        /// Gets the registered ips for the given user id.
        /// </summary>
        /// <param name="userId">The user id to look up.</param>
        /// <returns>
        /// The registered IP addresses for the given user. If there aren't any,
        /// returns an empty enumerable.
        /// </returns>
        public IEnumerable<UserRegisteredIp> GetRegisteredIpsForUser(Guid userId)
        {
            if (!this.loadedData)
            {
                this.Retrieve();
            }

            if (this.registeredIpsByUserId.ContainsKey(userId))
            {
                return this.registeredIpsByUserId[userId];
            }
            else
            {
                return new List<UserRegisteredIp>();
            }
        }

        /// <summary>
        /// Retrieves all of the registered IP addresses for the lender.
        /// </summary>
        public void Retrieve()
        {
            this.registeredIpsByUserId = new Dictionary<Guid, List<UserRegisteredIp>>();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(
                this.brokerId,
                "ALL_USER_REGISTERED_IP_ListByBrokerId",
                parameters))
            {
                while (reader.Read())
                {
                    var ip = UserRegisteredIp.CreateFromReader(reader);

                    if (this.registeredIpsByUserId.ContainsKey(ip.UserId))
                    {
                        this.registeredIpsByUserId[ip.UserId].Add(ip);
                    }
                    else
                    {
                        this.registeredIpsByUserId.Add(
                            ip.UserId,
                            new List<UserRegisteredIp>() { ip });
                    }
                }
            }

            this.loadedData = true;
        }
    }
}
