﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Security
{
    public class DocuTechSavedAuthentication
    {
        private AbstractUserPrincipal m_principal = null;

        private Guid m_userId;
        private Lazy<string> lazyPassword = new Lazy<string>(() => string.Empty);
        private LqbGrammar.DataTypes.EncryptionKeyIdentifier encryptionKeyId;

        private string m_originalUserName = "";
        private Lazy<string> lazyOriginalPassword = new Lazy<string>(() => string.Empty);

        public string UserName { get; set; }
        public string Password 
        {
            get { return this.lazyPassword.Value ?? string.Empty; }
            set { this.lazyPassword = new Lazy<string>(() => value); }
        }

        public void Save()
        {
            if (this.m_originalUserName == this.UserName && this.lazyOriginalPassword.Value == this.lazyPassword.Value)
            {
                return; // 11/3/2010 dd - Do not invoke save if data does not change.
            }

            byte[] encryptedPasswordBytes = null;
            if (this.encryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
            {
                encryptedPasswordBytes = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazyPassword.Value);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", this.m_userId),
                                            new SqlParameter("@DocuTechUserName", this.UserName),
                                            new SqlParameter("@DocuTechPassword", EncryptionHelper.Encrypt(this.lazyPassword.Value)),
                                            new SqlParameter("@EncryptedDocuTechPassword", encryptedPasswordBytes ?? new byte[0])
                                        };
            StoredProcedureHelper.ExecuteNonQuery(m_principal.ConnectionInfo, "DocuTech_UpdateSavedAuthentication", 3, parameters);
        }
        private DocuTechSavedAuthentication(AbstractUserPrincipal principal)
        {
            m_principal = principal;
            m_userId = principal.UserId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.ConnectionInfo, "DocuTech_RetrieveSavedAuthentication", parameters))
            {
                if (reader.Read())
                {
                    this.UserName = (string)reader["DocuTechUserName"];
                    var maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                    if (maybeEncryptionKeyId.HasValue)
                    {
                        this.encryptionKeyId = maybeEncryptionKeyId.Value;
                        byte[] passwordBytes = (byte[])reader["EncryptedDocuTechPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, passwordBytes));
                    }
                    else
                    {
                        string encryptedPasswordString = (string)reader["DocuTechPassword"];
                        this.lazyPassword = new Lazy<string>(() => EncryptionHelper.Decrypt(encryptedPasswordString));
                    }

                    this.m_originalUserName = (string)reader["DocuTechUserName"];
                    this.lazyOriginalPassword = this.lazyPassword;
                }
                else
                {
                    throw new NotFoundException("Could not locate user", "Unable to find userid=" + m_userId);
                }
            }
        }

        public static DocuTechSavedAuthentication Retrieve(AbstractUserPrincipal principal)
        {
            return new DocuTechSavedAuthentication(principal);
        }
    }
}
