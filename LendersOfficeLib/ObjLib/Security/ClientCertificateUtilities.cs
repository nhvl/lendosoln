﻿// <copyright file="ClientCertificateUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/4/2014 2:47:01 AM 
// </summary>
namespace LendersOffice.Security
{
    // It is extremely annoying to have to use the full global namespace for Org.BouncyCastle.
    // The reason is iTextSharp.dll contains the exact same namespace with Org.BouncyCastle. Therefore
    // I must create a separate crypto namespace.
    extern alias crypto;

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Security;
    using AsymmetricKeyParameter = crypto::Org.BouncyCastle.Crypto.AsymmetricKeyParameter;
    using BigInteger = crypto::Org.BouncyCastle.Math.BigInteger;
    using DerObjectIdentifier = crypto::Org.BouncyCastle.Asn1.DerObjectIdentifier;
    using MS509 = System.Security.Cryptography.X509Certificates;
    using X509Name = crypto::Org.BouncyCastle.Asn1.X509.X509Name;

    /// <summary>
    /// This class contains utilities methods that create Client Certificate to be use 
    /// in Multi Factor Authentication per OPM 125245.
    /// This class based on the similar code that use by MCL project.
    /// </summary>
    public static class ClientCertificateUtilities
    {
        /// <summary>
        /// Host name constant.
        /// </summary>
        private const string HostName = "LendingQB";

        /// <summary>
        /// String for T subject property if user type certificate.
        /// </summary>
        private const string UserTypeString = "User";

        /// <summary>
        /// String for T subject property if lender type certificate.
        /// </summary>
        private const string LenderTypeString = "Lender";

        /// <summary>
        /// String for T subject property if vendor type certificate.
        /// </summary>
        private const string VendorTypeString = "Vendor";

        /// <summary>
        /// Public Key Size.
        /// </summary>
        private const int PublicKeySize = 1024;

        /// <summary>
        /// Signature Algorithm.
        /// </summary>
        private const string SignatureAlgorithm = "SHA1WithRSAEncryption";

        /// <summary>
        /// Convert the hexadecimal string to unique identifier that used to generate the hexadecimal string.
        /// </summary>
        /// <param name="str">The hexadecimal string. It has following format 1F 2E F3 .. ..</param>
        /// <returns>A unique identifier that generate the hexadecimal string.</returns>
        public static Guid ConvertFromX509SerialString(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentException("str cannot be empty");
            }

            string[] parts = str.Split(new char[] { ' ', '-' });

            if (parts.Length != 17)
            {
                // 5/3/2014 dd - Expected the string contains 17 hexadecimal numbers.
                throw new ArgumentException("str is not a valid format.");
            }

            byte[] bytes = new byte[16];

            for (int i = 0; i < bytes.Length; i++)
            {
                // 5/3/2014 dd - 
                // I skipped the first hexadecimal in the string. The
                // first hexadecimal is only a filler that enforce BigInteger to be positive number.
                // The last 16 hexadecimal number is the one I need to convert to Guid.

                // I only expect hexadecimal in the string.
                // Therefore I want this method to fail if it is unable to Parse to byte
                // correctly.
                bytes[i] = byte.Parse(parts[i + 1], NumberStyles.AllowHexSpecifier);
            }

            return new Guid(bytes);
        }

        /// <summary>
        /// Generate a hexadecimal string of the big integer value. This method is only use
        /// in unit test.
        /// </summary>
        /// <param name="bigInt">A big integer value to get the hexadecimal string.</param>
        /// <returns>The hexadecimal string. Sample string E1-B2-02......</returns>
        public static string GenerateHexadecimalString(BigInteger bigInt)
        {
            byte[] bytes = bigInt.ToByteArray();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                if (i != 0)
                {
                    sb.Append("-");
                }

                sb.Append(bytes[i].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Generate a PKCS #12 client certificate that will be use in Multi Factor authentication.
        /// </summary>
        /// <param name="certificateId">A unique id that can be use to look up in our database for matching with user information.</param>
        /// <param name="password">Password requires to install the certificate on client computer.</param>
        /// <param name="userFullName">User full name.</param>
        /// <param name="certificateType">The type of certificate being created.</param>
        /// <returns>A PKCS #12 client certificate.</returns>
        public static byte[] GenerateClientCertificate(Guid certificateId, string password, string userFullName, ClientCertificateType certificateType)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Missing password");
            }

            if (string.IsNullOrEmpty(userFullName))
            {
                throw new ArgumentException("Missing user full name");
            }

            AsymmetricKeyParameter intermKeyParam;

            crypto::Org.BouncyCastle.X509.X509Certificate intermCert = GetIntermediateCertificate(ConstStage.IntermediateCertificateAuhorityThumbPrint, out intermKeyParam);
            
            string certType;
            switch (certificateType)
            {
                case ClientCertificateType.User:
                    certType = UserTypeString;
                    break;
                case ClientCertificateType.Lender:
                    certType = LenderTypeString;
                    break;
                case ClientCertificateType.Vendor:
                    certType = VendorTypeString;
                    break;
                default:
                    throw new UnhandledEnumException(certificateType);
            }

            Dictionary<DerObjectIdentifier, string> dicSubjectAttributes = new Dictionary<DerObjectIdentifier, string>();
            dicSubjectAttributes.Add(X509Name.CN, userFullName);
            dicSubjectAttributes.Add(X509Name.T, certType);

            // Import the issuer information from the subject information on the intermediate certificate,
            // but replace the issuer common name with the host name.  This will allow the generated certificate
            // to be more identifiable at a glance.
            X509Name issuer = crypto::Org.BouncyCastle.X509.PrincipalUtilities.GetSubjectX509Principal(intermCert);
            IList lstIssuerOIDs, lstIssuerValues;
            lstIssuerOIDs = issuer.GetOidList();
            lstIssuerValues = issuer.GetValueList();
            if (lstIssuerOIDs.Count != lstIssuerValues.Count)
            {
                throw new Exception("Mismatch between intermediate certificate object identifiers and values.");
            }

            bool addIssuerCN = true;
            for (int i = 0; i < lstIssuerOIDs.Count; i++)
            {
                if (X509Name.CN.Equals(lstIssuerOIDs[i]))
                {
                    lstIssuerValues[i] = HostName;
                    addIssuerCN = false;
                }
            }

            if (addIssuerCN)
            {
                lstIssuerOIDs.Add(X509Name.CN);
                lstIssuerValues.Add(HostName);
            }

            DateTime now = DateTime.UtcNow;
            DateTime expirationDate = intermCert.NotAfter;
            if (expirationDate < now)
            {
                throw new Exception("Intermediate certificate has expired.");
            }

            crypto::Org.BouncyCastle.X509.X509V3CertificateGenerator certGen = new crypto::Org.BouncyCastle.X509.X509V3CertificateGenerator();
            certGen.SetSerialNumber(GenerateX509SerialNumber(certificateId));
            certGen.SetIssuerDN(new X509Name(lstIssuerOIDs, lstIssuerValues));
            certGen.SetSubjectDN(new X509Name(dicSubjectAttributes.Keys.ToList<DerObjectIdentifier>(), dicSubjectAttributes));
            certGen.SetNotBefore(now);
            certGen.SetNotAfter(expirationDate);

            crypto::Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair keyPair = GenerateRandomKeyPair(PublicKeySize);
            certGen.SetPublicKey(keyPair.Public);
            certGen.SetSignatureAlgorithm(SignatureAlgorithm);

            certGen.AddExtension(crypto::Org.BouncyCastle.Asn1.X509.X509Extensions.SubjectKeyIdentifier, false, new crypto::Org.BouncyCastle.X509.Extension.SubjectKeyIdentifierStructure(keyPair.Public));
            certGen.AddExtension(crypto::Org.BouncyCastle.Asn1.X509.X509Extensions.AuthorityKeyIdentifier, false, new crypto::Org.BouncyCastle.X509.Extension.AuthorityKeyIdentifierStructure(intermCert));
            certGen.AddExtension(crypto::Org.BouncyCastle.Asn1.X509.X509Extensions.ExtendedKeyUsage, false, new crypto::Org.BouncyCastle.Asn1.X509.ExtendedKeyUsage(crypto::Org.BouncyCastle.Asn1.X509.KeyPurposeID.IdKPClientAuth));

            crypto::Org.BouncyCastle.X509.X509Certificate cert = certGen.Generate(intermKeyParam);
            cert.CheckValidity(now);
            cert.Verify(intermCert.GetPublicKey());

            // If Internet Explorer prompts the user to select a certificate, it will show both
            // the friendly name and the issuer.  The friendly name is often truncated for display.
            string friendlyName = userFullName;

            crypto::Org.BouncyCastle.Pkcs.X509CertificateEntry[] certificateChain = new crypto::Org.BouncyCastle.Pkcs.X509CertificateEntry[2];
            certificateChain[0] = new crypto::Org.BouncyCastle.Pkcs.X509CertificateEntry(cert);
            certificateChain[1] = new crypto::Org.BouncyCastle.Pkcs.X509CertificateEntry(intermCert);

            crypto::Org.BouncyCastle.Pkcs.Pkcs12Store store = new crypto::Org.BouncyCastle.Pkcs.Pkcs12StoreBuilder().Build();
            store.SetKeyEntry(friendlyName, new crypto::Org.BouncyCastle.Pkcs.AsymmetricKeyEntry(keyPair.Private), certificateChain);

            using (MemoryStream mem = new MemoryStream())
            {
                store.Save(mem, password.ToCharArray(), new crypto::Org.BouncyCastle.Security.SecureRandom());
                mem.Flush();
                return mem.GetBuffer();
            }
        }

        /// <summary>
        /// Generate a positive big integer value that will be use in X509 serial number.
        /// </summary>
        /// <param name="guid">A unique identifier.</param>
        /// <returns>Positive big integer.</returns>
        public static BigInteger GenerateX509SerialNumber(Guid guid)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentException("guid cannot be Guid.Empty");
            }

            // Convert to a byte array that will result in positive number for BigInteger.
            byte[] bytes = new byte[17];

            byte[] guidBytes = guid.ToByteArray();

            // Make the first byte to be ALWAYS positive number when convert to sbyte.
            bytes[0] = (byte)((guidBytes[0] % 17) + 1); // 17 is just a random choice. First byte cannot be leading zero.

            Array.Copy(guidBytes, 0, bytes, 1, guidBytes.Length);

            BigInteger bigInt = new BigInteger(bytes);

            // 5/3/2014 dd - Enforce that BigInteger cannot be negative.
            if (bigInt.SignValue == -1)
            {
                throw new Exception("BigInteger cannot be negative for X509SerialNumber. Guid=[" + guid + "]");
            }

            return bigInt;
        }

        /// <summary>
        /// Check to see if the client certificate issues by us and belong to the user.
        /// </summary>
        /// <param name="certificate">Client certificate to verify.</param>
        /// <param name="principal">The principal to check the certificates against.</param>
        /// <returns>True if the certificate is valid and belong to user.</returns>
        public static bool IsValidClientCertificate(HttpClientCertificate certificate, AbstractUserPrincipal principal)
        {
            var certificateId = RetrieveCertificateId(certificate);
            if (certificateId == null)
            {
                return false;
            }

            string certType = certificate["SUBJECTT"];

            /* ejm opm 248905
               I'm going to err on the side of caution and check if the passed in certificate is both types if I can't find it in
               the certificate. Really though, only the old user-level certs shouldn't have this property.
               If I do find it, then I'll only check the one that matches it.
            */
            if (string.IsNullOrEmpty(certType) ||
                (!string.IsNullOrEmpty(certType) && certType.Equals(UserTypeString, StringComparison.OrdinalIgnoreCase)))
            {
                // Verifies the old user-level certificate, if it is one.
                if (ClientCertificate.Verify(certificateId.Value, principal.BrokerId, principal.UserId))
                {
                    Tools.LogInfo("VerifyClientCertificateHang", $"Cert converted and cert id retrieved.");
                    return true;
                }
            }

            if (string.IsNullOrEmpty(certType) ||
                (!string.IsNullOrEmpty(certType) && certType.Equals(LenderTypeString, StringComparison.OrdinalIgnoreCase)))
            {
                // Verifies the new lender-level certificate, it it is one.
                if (LenderClientCertificate.VerifyBrokerClientCertificate(certificateId.Value, principal))
                {
                    Tools.LogInfo("VerifyClientCertificateHang", $"Cert converted and cert id retrieved.");
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified certificate is valid for the vendor.
        /// </summary>
        /// <param name="certificate">
        /// The certificate to validate.
        /// </param>
        /// <param name="vendorId">
        /// The ID of the vendor.
        /// </param>
        /// <returns>
        /// True if the certificate is valid, false otherwise.
        /// </returns>
        public static bool IsValidVendorClientCertificate(HttpClientCertificate certificate, Guid vendorId)
        {
            var certificateId = RetrieveCertificateId(certificate);
            if (certificateId == null)
            {
                return false;
            }

            return VendorClientCertificate.VerifyCertificate(vendorId, certificateId.Value);
        }

        /// <summary>
        /// Retrieves the certificate ID from the certificate.
        /// </summary>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <returns>
        /// The extracted ID or null if the certificate is invalid or the ID cannot be parsed.
        /// </returns>
        private static Guid? RetrieveCertificateId(HttpClientCertificate certificate)
        {
            if (certificate == null)
            {
                return null;
            }

            if (certificate.IsPresent == false)
            {
                return null;
            }

            if (certificate.IsValid == false)
            {
                return null;
            }

            try
            {
                return ConvertFromX509SerialString(certificate.SerialNumber);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieves client intermediate certificate data from the server's certificate store.
        /// </summary>
        /// <param name="thumbPrint">Thumbprint for the self-signed intermediate certificate.</param>
        /// <param name="keyParam">Key parameters for signing.</param>
        /// <returns>Intermediate Certificate to use with creating client certificates.</returns>
        /// <remarks>
        /// The X.509 certificate format doesn't include the private key, either through
        /// the BouncyCastle <see cref="Org.BouncyCastle.X509.X509Certificate"/> object or
        /// the Microsoft <see cref="System.Security.Cryptography.X509Certificates.X509Certificate"/> object.
        /// However, this information is present in PKCS #12 data and the Windows Certificate Store,
        /// and can be accessed with a different Microsoft object with an API modeled after common usage:
        /// <see cref="System.Security.Cryptography.X509Certificates.X509Certificate2"/>.
        /// The private key from the intermediate certificate is exported into an
        /// <see cref="Org.BouncyCastle.Crypto.AsymmetricKeyParameter"/> key parameter for further use.
        /// </remarks>
        private static crypto::Org.BouncyCastle.X509.X509Certificate GetIntermediateCertificate(string thumbPrint, out AsymmetricKeyParameter keyParam)
        {
            if (string.IsNullOrEmpty(thumbPrint))
            {
                throw new ArgumentException("No thumbprint provided for intermediate certificate.");
            }

            // Windows Certificate Store is perform case sensitive search on thumbprint.
            thumbPrint = thumbPrint.Replace(" ", string.Empty).ToUpper();

            // Open the Intermediate Certification Authorities store.
            // The certificate should be installed for the local computer, which will cascade to the current user [service] account.
            MS509.X509Store store = null;
            MS509.X509Certificate2 microsoftCert = null;
            try
            {
                store = new MS509.X509Store(MS509.StoreName.CertificateAuthority, MS509.StoreLocation.CurrentUser);
                store.Open(MS509.OpenFlags.OpenExistingOnly);

                MS509.X509Certificate2Collection certs = store.Certificates.Find(MS509.X509FindType.FindByThumbprint, thumbPrint, true);
                int numOfCerts = certs.Count;
                if (numOfCerts == 0)
                {
                    throw new Exception("Intermediate certificate not found.");
                }

                if (numOfCerts > 1)
                {
                    throw new Exception("Duplicate intermediate certificates found.");
                }

                microsoftCert = certs[0];
                System.Security.Cryptography.RSACryptoServiceProvider rsaCSP = microsoftCert.PrivateKey as System.Security.Cryptography.RSACryptoServiceProvider;
                System.Security.Cryptography.RSAParameters rsaParams = rsaCSP.ExportParameters(true);

                keyParam = new crypto::Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters(
                    new BigInteger(1, rsaParams.Modulus),
                    new BigInteger(1, rsaParams.Exponent),
                    new BigInteger(1, rsaParams.D),
                    new BigInteger(1, rsaParams.P),
                    new BigInteger(1, rsaParams.Q),
                    new BigInteger(1, rsaParams.DP),
                    new BigInteger(1, rsaParams.DQ),
                    new BigInteger(1, rsaParams.InverseQ));
            }
            finally
            {
                if (store != null)
                {
                    store.Close();
                }
            }

            return crypto::Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(microsoftCert);
        }

        /// <summary>
        /// Generate random asymmetric key.
        /// </summary>
        /// <param name="keySize">Size of the key.</param>
        /// <returns>Asymmetric key.</returns>
        private static crypto::Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair GenerateRandomKeyPair(int keySize)
        {
            crypto::Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator keyGen = new crypto::Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator();
            crypto::Org.BouncyCastle.Crypto.KeyGenerationParameters keyGenParams = new crypto::Org.BouncyCastle.Crypto.KeyGenerationParameters(new crypto::Org.BouncyCastle.Security.SecureRandom(), keySize);
            keyGen.Init(keyGenParams);

            return keyGen.GenerateKeyPair();
        }
    }
}