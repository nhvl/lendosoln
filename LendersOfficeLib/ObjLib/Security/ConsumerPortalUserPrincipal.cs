﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using DataAccess;
using LendersOffice.ObjLib.ConsumerPortal;
using LendersOffice.Constants;
using System.Web;
using LendersOffice.Common;
using System.Web.Security;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.Security
{
    /// <summary>
    /// This principal is for the Consumer Portal v2. OPM 105554
    /// </summary>
    public sealed class ConsumerPortalUserPrincipal : AbstractUserPrincipal
    {
        private static new readonly string Permissions = "0000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000";

        // Temporary assign to my david_acc2 account on localhost.
        private Guid m_impersonateUserId = Guid.Empty; 
        
        public ConsumerPortalUserPrincipal(IIdentity identity,
             ConsumerPortalUser portalUser, Guid consumerPortalId, Guid allowViewSubmittedLoanId)
            : base(identity, new string[] { ConstApp.ROLE_CONSUMER } /*roles */,
                Guid.Empty /*userId*/, portalUser.BrokerId /*brokerId*/, Guid.Empty /* branchId */,
                Guid.Empty /* employeeId */, Guid.Empty /* loginSessionId */,
                portalUser.FirstName /* firstName */, portalUser.LastName /* lastName */, Permissions /* permissions */,
                false /* isOthersAllowedToEditUnderwriterAssignedFile */,
                false /* isLOAllowedToEditProcessorAssignedFile */,
                Guid.Empty /* lpePriceGroupId */, false /* isRateLockedAtSubmission */,
                false /* hasLenderDefaultFeatures */, Guid.Empty /* becomeUserId */,
                false /* isQuickPricerEnable */,
                false /* isPricingMultipleAppsSupported */,
                string.Empty /* cookieSessionId*/, Guid.Empty /* pmlBrokerId */,
                E_PmlLoanLevelAccess.Individual /* pmlLoanLevelAccess */,
                "D" /* type */, E_ApplicationT.ConsumerPortal, 
                null, /* ipWhiteList */
                null, /* teams */
                false, /* IsNewPmlUIEnabled */
                false, /* IsUsePml2AsQuickPricer */
                E_PortalMode.Blank, /*User's last portal mode*/
                Guid.Empty, /* Mini-Correspondent Branch Id */
                Guid.Empty, /* Correspondent Branch Id */
                initialPrincipalType: InitialPrincipalTypeT.NonInternal,
                initialUserId: Guid.Empty,
                postLoginTask: PostLoginTask.Redirect
            )
        {
            ConsumerPortalId = consumerPortalId;
            IsLockedOut = portalUser.IsLockedOut;
            Id = portalUser.Id;
            DisclosureAcceptedD = portalUser.DisclosureAcceptedD;
            IsTemporaryPassword = portalUser.IsTemporaryPassword;
            Email = portalUser.Email;
            m_impersonateUserId = ConsumerPortalConfig.LoanOfficerUserId;
            SignatureName = portalUser.SignatureName;
            AllowViewSubmittedLoanId = allowViewSubmittedLoanId;
        }
        public Guid AllowViewSubmittedLoanId { get; private set; }

        public Guid ConsumerPortalId { get; private set; }
        public bool IsLockedOut { get; private set; }
        public long Id { get; private set; }
        public DateTime? DisclosureAcceptedD { get; private set; }
        public bool IsTemporaryPassword { get;  set; }
        public string SignatureName { get; set; }
        public string Email { get; private set; }

        private ConsumerPortalConfig x_consumerPortalConfig = null;
        public ConsumerPortalConfig ConsumerPortalConfig
        {
            get
            {
                if (x_consumerPortalConfig == null)
                {
                    x_consumerPortalConfig = ConsumerPortalConfig.Retrieve(this.BrokerId, ConsumerPortalId);
                }
                return x_consumerPortalConfig;
            }
        }

        /// <summary>
        /// Set a loan id to consumer user principal to impersonate the loan officer current assign to loan.
        /// </summary>
        /// <param name="sLId"></param>
        public void SetLoanId(Guid sLId)
        {
            // 9/30/2013 dd - Assign loan officer as an impersonate user id
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortalUserPrincipal));
            dataLoan.InitLoad();
            
            if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
            {
                Guid employeeId = dataLoan.sEmployeeLoanRepId;

                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeId", employeeId)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataLoan.sBrokerId, "GetUserByEmployeeId", parameters)) 
                {
                    if (reader.Read()) 
                    {
                        m_impersonateUserId = (Guid)reader["UserId"];
                    }
                }
                
            }
        }

        public override bool HasPermission(Permission p)
        {
            // OPM 245766
            if (p == Permission.CanViewEDocs)
            {
                return true;
            }

            return base.HasPermission(p);
        }

        public AbstractUserPrincipal GetImpersonatePrincipal()
        {
            return GetImpersonatePrincipal(null);
        }
        public AbstractUserPrincipal GetImpersonatePrincipal(string loginNm)
        {
            Guid userId = m_impersonateUserId;
            if (!string.IsNullOrEmpty(loginNm))
            {
                //we really should move this code somewhere else
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", BrokerId), 
                                                new SqlParameter("@LoginName", loginNm)
                                            };
                using (DbDataReader r = StoredProcedureHelper.ExecuteReader(BrokerId, "GetEmployeeIdByLoginName", parameters))
                {
                    if (r.Read())
                    {
                        m_impersonateUserId = (Guid)r["UserId"];
                    }
                }
            }

            if (userId == Guid.Empty)
            {
                return null;
            }

            AbstractUserPrincipal principal = PrincipalFactory.Create(this.BrokerId, m_impersonateUserId, "B", true, false) as AbstractUserPrincipal;

            if (principal == null)
            {
                throw new CBaseException("Could not impersonate the given user.", "Could not get impersonated principal because create returned null UserId=" + m_impersonateUserId + " LoginNm=" + loginNm);
            }

            principal.IsOriginalPrincipalAConsumer = true; // 9/4/2013 dd - OPM 137164.
            principal.OriginalPrincipalConsumer = this; // 9/30/2013 dd - Save the original consumer Id for access checking.
            return principal;
        }


        public void StoreToCookie(int cookieExpirationMinutes)
        {
            string userData = string.Format("{0}|{1}|{2}|{3}|{4}", Id, RequestHelper.ClientIP, BrokerId.ToString("N"), ConsumerPortalId.ToString("N"), AllowViewSubmittedLoanId.ToString("N"));

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                ConstAppDavid.CookieVersion,
                Identity.Name,
                DateTime.Now,
                DateTime.Now.AddMinutes(cookieExpirationMinutes),
                false,
                userData);

            string str = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, str);
            if (ConstSite.IsSecureCookieRequired)
            {
                cookie.Secure = true;
            }
            cookie.Path = FormsAuthentication.FormsCookiePath;
            cookie.HttpOnly = true;
            cookie.Expires = DateTime.MaxValue;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static IPrincipal Create(FormsIdentity identity)
        {
            string[] data = identity.Ticket.UserData.Split('|');
            if (data.Length != 5)
            {
                CBaseException e = new CBaseException(ErrorMessages.Generic, "Invalid format in user cookie [" + identity.Ticket.UserData + "]");
                FormsAuthentication.SignOut();
                ErrorUtilities.DisplayErrorPage(e, true, Guid.Empty, Guid.Empty);
            }

            Guid brokerId = new Guid(data[2]);
            int ConsumerID = Int32.Parse(data[0]);
            Guid portalId = new Guid(data[3]);
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(brokerId, ConsumerID);
            Guid allowViewSubmittedLoanId = new Guid(data[4]);
            AbstractUserPrincipal p = new ConsumerPortalUserPrincipal(new GenericIdentity(user.Email), user, portalId, allowViewSubmittedLoanId);
            System.Threading.Thread.CurrentPrincipal = p;
            return p;
        }

    }
}
