using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Security
{
	/// <summary>
	/// Each permission afforded to a broker user is codified as an
	/// entry in this enum set.  The enum value corresponds to an
	/// offset within a permission string (with about 100 slots --
	/// see the max constant).  Add new permissions sequentially
	/// and in increasing order.
	/// 
	/// 3/15/2005 kb ***NOTE: If anyone adds to this list, go find
	/// BrokerUserPermissionTable and update the corresponding
	/// specializations of that class.  We use these tables to
	/// dynamically manage permission lists throughout the app.
	/// If you add an entry to the base table's constructor, it
	/// will show up in all the other tables' sets.
	/// </summary>

	public enum Permission 
    {
        CanModifyAdministrativeItem             = 0,
        BrokerLevelAccess                       = 1,
        BranchLevelAccess                       = 2,
        IndividualLevelAccess                   = 3,
        CanExportLoan                           = 4,
		AllowLoanAssignmentsToAnyBranch         = 5,
		AllowAccessToTemplatesOfAnyBranch       = 6,
		AllowReadingFromRolodex                 = 7,
		CanModifyLoanPrograms                   = 8,
		CanViewLoanInEditor                     = 9,
		CanApplyForIneligibleLoanPrograms       = 10,
		CanRunPricingEngineWithoutCreditReport  = 11,
		AllowWritingToRolodex                   = 12,
		CanRunCustomReports                     = 13,
		CanDeleteLoan                           = 14,
		CanCreateLoanTemplate                   = 15,
		CanEditOthersCustomReports              = 16,
		CanEditPrintGroups                      = 17,
		CanModifyLoanName                       = 18,
		CanPublishCustomReports                 = 19,
		CanAccessClosedLoans                    = 20,
        CanWriteNonAssignedLoan                 = 21,
        CanSubmitWithoutCreditReport            = 22,
		CanAdministrateExternalUsers			= 23, // OPM 9721
		CanAccessCCTemplates					= 24, // 04/06/07 db - OPM 3453
		CanCreateCustomForms					= 25, // 05/31/07 db - OPM 2259
		CanEditOthersCustomForms				= 26, // 05/31/07 db - OPM 2259
		CanViewHiddenInformation				= 27,  // 10/03/07 db - OPM 18269
		CanEditDataTracLPNames					= 28, //09-17-08 av opm 24638
		CanViewEDocs                            = 29, // db - OPM 44494
        CanEditEDocs                            = 30, // db - OPM 44494        
        AllowLockDeskRead                       = 31,
        AllowLockDeskWrite                      = 32,
        AllowCloserRead                         = 33,
        AllowCloserWrite                        = 34,
        AllowAccountantRead                     = 35,
        AllowAccountantWrite                    = 36,
        AllowBypassAlwaysUseBestPrice           = 37, // 4/7/2010 dd - OPM 47238
        CanAccessLendingStaffNotes              = 38,  // 07/30/10 fs - opm 50434
        CanAccessTotalScorecard                 = 39, // 8/9/10 db - OPM 46742
        CanViewEDocsInternalNotes               = 40, // 11/12/2010 dd - OPM 51942
        CanEditEDocsInternalNotes               = 41, // 11/12/2010 dd - OPM 51942
        AllowCapitalMarketsAccess               = 42, // 08/29/2010 mp - OPM 70510
        AllowScreeningEDocs                     = 43,
        AllowApprovingRejectingEDocs            = 44,
        AllowEditingApprovedEDocs               = 45,
        AllowDocMagicDocumentGeneration_OBSOLETE= 46, // av opm 76156    3/19/2012 // sk opm 183803, 126308 **try to avoid this bit.**
        CanDuplicateLoans                       = 47,
        AllowOrderingTitleServices              = 48,
        AllowUnderwritingAccess                 = 49,
        AllowQualityControlAccess               = 50,
        AllowOrderingGlobalDMSAppraisals        = 51,
        AllowViewingInvestorInformation         = 52,
        AllowEditingInvestorInformation         = 53,
        ExportDocRequestsToTestingPath          = 54,
        ResponsibleForInitialDiscRetail         = 55,
        ResponsibleForInitialDiscWholesale      = 56,
        ResponsibleForRedisclosures             = 57,
        AllowDisclosureDocumentGeneration       = 58,
        AllowEditingLoanProgramName             = 59,  // opm 105273
        AllowManagingContactList                = 60,   // opm 109122
        AllowOrder4506T                         = 61, // 8/9/2013 dd - OPM 88442 - 4506-T Integration
        AllowHideEDocsFromPML                   = 62,   // opm 64817
        AllowTpoPortalConfiguring               = 63,    // opm 150384
        AllowEnablingCustomFeeDescriptions      = 64,    // opm 169207
        TeamLevelAccess                         = 65,   // opm 178119
        AllowCreatingNewLoanFiles               = 66, // 6/26/2014 gf - opm 179075
        CanEditLoanTemplates                    = 67,
        AllowManuallyArchivingGFE               = 68, // 8/4/2014  sk - opm 172077
        AllowOrderingPMIPolicies                = 69, // 9/26/2014 ir - opm 187049
        AllowEditingOriginatingCompanyTiers     = 70, // 10/27/2014 ir - opm 190542
        AllowEnableGfePaidToManualDesc          = 71, // 10/1/2014  je - opm 149774
        AllowViewingAndEditingGeneralSettings = 72, // start 11/5/2014 sk - opm 180788 section general settings
        AllowViewingAndEditingBranches          = 73,
        AllowViewingAndEditingConsumerPortalConfigs = 74,
        AllowViewingAndEditingEventNotifications = 75,
        AllowViewingAndEditingCustomFieldChoices = 76,
        AllowViewingAndEditingConditionChoices   = 77,
        AllowViewingAndEditingLeadSources        = 78,
        AllowViewingAndEditingEdocsConfiguration = 79,
        AllowViewingAndEditingArmIndexEntries    = 80,
        AllowViewingAndEditingManualLoanPrograms = 81, // opm 180788 section loan programs
        AllowViewingAndEditingFeeTypeSetup       = 82,
        AllowViewingAndEditingGFEFeeService      = 83,
        AllowViewingAndEditingDisablePricing     = 84,
        AllowViewingAndEditingPriceGroups        = 85,
        AllowViewingAndEditingLockDeskQuestions  = 86,
        AllowViewingPrintGroups                  = 87,           
        AllowViewingAndEditingTeams              = 88,
        AllowViewingAndEditingGlobalIPSettings   = 89, // end 11/5/2014 sk - opm 180788
        AllowCreatingWholesaleChannelLoans      = 90, // 11/19/2014 em - opm 196794
        AllowCreatingMiniCorrChannelLoans       = 91, // 11/19/2014 em - opm 196794
        AllowCreatingCorrChannelLoans           = 92, // 11/19/2014 em - opm 196794
        AllowLoadingDeletedLoans                = 93,
        AllowManageNewFeatures                  = 94,  // 3/25/2015 ejm - opm 198510

        /// <summary>
        /// Enables a new section in Dropbox to look at docs that were uploaded but had no barcodes.
        /// </summary>
        AllowManagingFailedDocs                 = 95,
        AllowCreatingTestLoans                  = 96,    // opm 217783
        AllowViewingWholesaleChannelLoans       = 97,
        AllowViewingMiniCorrChannelLoans        = 98,
        AllowViewingCorrChannelLoans            = 99,
        AllowOverridingContactLicenses          = 100, // OPM 222483
        AllowViewingNewLoanEditorUI             = 101,  // sk - note it is not enough to have this permission.
        AllowAccessingOCRReviewPortal           = 102,
        AllowCreatingNewLeadFiles               = 103,
        AllowViewingAndEditingAdjustmentsAndOtherCreditsSetup = 104, // OPM 456439
        AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox = 105, // OPM 456439
        AllowExportingFullSsnViaCustomReports = 106,
        AllowCreatingDocumentSigningEnvelopes = 107,
        AllowAppraisalDelivery = 108, // opm 468712
        AllowAccessingCorporateAdminDocumentCaptureAuditPage = 109,
        AllowAccessingLoanEditorDocumentCaptureAuditPage = 110,
        AllowAccessingCorporateAdminDocumentCaptureReviewPage = 111,
        AllowAccessingLoanEditorDocumentCaptureReviewPage = 112,
        AllowAccessingSecurityEventLogsPage = 113,
        AllowViewingAndEditingCustomNavigation = 114,
    }


    public class BrokerUserPermissions
	{
		/// <summary>
		/// We need a cap count for allocation so that we can
		/// work with a fixed array of permission characters.
		/// </summary>

		private const int MAX_PERMISSIONS = 255;

		/// <summary>
		/// Keep track of the affirmative state of each bit.
		/// The affirmative state of a permission is the value
		/// that depicts an "on" or "active" state.
		/// </summary>

		private static char[] m_affirmativeBits;

		static BrokerUserPermissions()
		{
            m_affirmativeBits = new char[ MAX_PERMISSIONS ];

			m_affirmativeBits[ ( int ) Permission.CanModifyAdministrativeItem            ] = '1';
			m_affirmativeBits[ ( int ) Permission.BrokerLevelAccess                      ] = '1';
			m_affirmativeBits[ ( int ) Permission.BranchLevelAccess                      ] = '1';
			m_affirmativeBits[ ( int ) Permission.IndividualLevelAccess                  ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanExportLoan                          ] = '1';
			m_affirmativeBits[ ( int ) Permission.AllowLoanAssignmentsToAnyBranch        ] = '0';
			m_affirmativeBits[ ( int ) Permission.AllowAccessToTemplatesOfAnyBranch      ] = '0';
			m_affirmativeBits[ ( int ) Permission.AllowReadingFromRolodex                ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanModifyLoanPrograms                  ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanViewLoanInEditor                    ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanApplyForIneligibleLoanPrograms      ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanRunPricingEngineWithoutCreditReport ] = '1';
			m_affirmativeBits[ ( int ) Permission.AllowWritingToRolodex                  ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanRunCustomReports                    ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanDeleteLoan                          ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanCreateLoanTemplate                  ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanEditOthersCustomReports             ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanEditPrintGroups                     ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanModifyLoanName                      ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanPublishCustomReports                ] = '1';
			m_affirmativeBits[ ( int ) Permission.CanAccessClosedLoans                   ] = '0';
            m_affirmativeBits[ ( int ) Permission.CanWriteNonAssignedLoan                ] = '0';
            m_affirmativeBits[ ( int ) Permission.CanSubmitWithoutCreditReport           ] = '0';
			m_affirmativeBits[ ( int ) Permission.CanAdministrateExternalUsers			 ] = '1'; // OPM 9721
			m_affirmativeBits[ ( int ) Permission.CanAccessCCTemplates					 ] = '1'; // 04/06/07 db - OPM 3453
			m_affirmativeBits[ ( int ) Permission.CanCreateCustomForms					 ] = '0'; // 05/31/07 db - OPM 2259
			m_affirmativeBits[ ( int ) Permission.CanEditOthersCustomForms				 ] = '0'; // 05/31/07 db - OPM 2259
			m_affirmativeBits[ ( int ) Permission.CanViewHiddenInformation				 ] = '1'; //10/04/07 db - OPM 18269
			m_affirmativeBits[ ( int ) Permission.CanEditDataTracLPNames				 ] = '1'; // av
            m_affirmativeBits[ ( int ) Permission.CanViewEDocs                           ] = '1'; // db - OPM 44494
            m_affirmativeBits[ ( int ) Permission.CanEditEDocs                           ] = '1'; // db - OPM 44494
            m_affirmativeBits[ ( int ) Permission.AllowLockDeskRead                      ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowLockDeskWrite                     ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowCloserRead                        ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowCloserWrite                       ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowAccountantRead                    ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowAccountantWrite                   ] = '1';            
            m_affirmativeBits[ ( int ) Permission.AllowBypassAlwaysUseBestPrice          ] = '1';
            m_affirmativeBits[ ( int ) Permission.CanAccessLendingStaffNotes             ] = '1'; // 07/30/10 fs - opm 50434
            m_affirmativeBits[ ( int ) Permission.CanAccessTotalScorecard                ] = '1'; // 8/9/10 db - opm 46742
            m_affirmativeBits[ ( int ) Permission.CanEditEDocsInternalNotes              ] = '1'; // 11/12/2010 dd - OPM 51942
            m_affirmativeBits[ ( int ) Permission.CanViewEDocsInternalNotes              ] = '1'; // 11/12/2010 dd - OPM 51942
            m_affirmativeBits[ ( int ) Permission.AllowCapitalMarketsAccess              ] = '1'; // 08/29/2011 mp - OPM 70386
            m_affirmativeBits[ ( int ) Permission.AllowScreeningEDocs                    ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowApprovingRejectingEDocs           ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowEditingApprovedEDocs              ] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowDocMagicDocumentGeneration_OBSOLETE] = '1';
            m_affirmativeBits[ ( int ) Permission.CanDuplicateLoans] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowOrderingTitleServices] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowUnderwritingAccess] = '0';
            m_affirmativeBits[ ( int ) Permission.AllowQualityControlAccess] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowOrderingGlobalDMSAppraisals] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowViewingInvestorInformation] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowEditingInvestorInformation] = '1';
            m_affirmativeBits[ ( int ) Permission.ExportDocRequestsToTestingPath] = '1';
            m_affirmativeBits[ ( int ) Permission.ResponsibleForInitialDiscRetail] = '1';
            m_affirmativeBits[ ( int ) Permission.ResponsibleForInitialDiscWholesale] = '1';
            m_affirmativeBits[ ( int ) Permission.ResponsibleForRedisclosures] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowDisclosureDocumentGeneration] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowEditingLoanProgramName] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowManagingContactList] = '1';  // opm 109122
            m_affirmativeBits[ ( int ) Permission.AllowOrder4506T] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowHideEDocsFromPML                  ] = '0'; // opm 64817
            m_affirmativeBits[ ( int ) Permission.AllowTpoPortalConfiguring              ] = '1'; // opm 150384
            m_affirmativeBits[ ( int ) Permission.AllowEnablingCustomFeeDescriptions     ] = '0'; // opm 169207
            m_affirmativeBits[ ( int ) Permission.TeamLevelAccess] = '1'; // opm 169207
            m_affirmativeBits[ ( int ) Permission.AllowCreatingNewLoanFiles] = '1'; // 6/26/2014 gf - opm 179075
            m_affirmativeBits[ ( int ) Permission.CanEditLoanTemplates] = '1';
            m_affirmativeBits[ ( int ) Permission.AllowManuallyArchivingGFE] = '1';
            m_affirmativeBits[(int)Permission.AllowOrderingPMIPolicies] = '1';
            m_affirmativeBits[(int)Permission.AllowEditingOriginatingCompanyTiers] = '1';
            m_affirmativeBits[(int)Permission.AllowEnableGfePaidToManualDesc] = '1'; // opm 149774
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingGeneralSettings] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingGlobalIPSettings] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingBranches] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingConsumerPortalConfigs] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingEventNotifications] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingCustomFieldChoices] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingConditionChoices] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingLeadSources] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingEdocsConfiguration] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingArmIndexEntries] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingManualLoanPrograms] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingFeeTypeSetup] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingGFEFeeService] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingDisablePricing] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingPriceGroups] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingLockDeskQuestions] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingPrintGroups] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingTeams] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingWholesaleChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingMiniCorrChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingCorrChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowLoadingDeletedLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowManageNewFeatures] = '1';
            m_affirmativeBits[(int)Permission.AllowManagingFailedDocs] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingTestLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingWholesaleChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingMiniCorrChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingCorrChannelLoans] = '1';
            m_affirmativeBits[(int)Permission.AllowOverridingContactLicenses] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingNewLoanEditorUI] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingOCRReviewPortal] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingNewLeadFiles] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingAdjustmentsAndOtherCreditsSetup] = '1';
            m_affirmativeBits[(int)Permission.AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox] = '1';
            m_affirmativeBits[(int)Permission.AllowExportingFullSsnViaCustomReports] = '1';
            m_affirmativeBits[(int)Permission.AllowCreatingDocumentSigningEnvelopes] = '1';
            m_affirmativeBits[(int)Permission.AllowAppraisalDelivery] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingCorporateAdminDocumentCaptureAuditPage] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingCorporateAdminDocumentCaptureReviewPage] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage] = '1';
            m_affirmativeBits[(int)Permission.AllowAccessingSecurityEventLogsPage] = '1';
            m_affirmativeBits[(int)Permission.AllowViewingAndEditingCustomNavigation] = '1';
        }

		/// <summary>
		/// Track an individual user's broker permissions.
		/// </summary>

		private char[]		m_permissionBits;
        private Guid       m_brokerID;
        private Guid       m_employeeID;
		private Guid       m_roleID = Guid.Empty;

		private Boolean m_isDefault;

		public Boolean IsDefault
		{
			get { return m_isDefault; }
		}

		/// <summary>
		/// Initialize this permissions container with the given employee.
		/// We punt if the employee is "new" and lacking in a valid login
		/// account (with permissions).
		/// </summary>

		public void Retrieve( Guid brokerId , Guid employeeId )
		{
			// Initialize this permissions container with the given employee.

			m_employeeID = employeeId;
			m_brokerID   = brokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter( "@EmployeeID" , employeeId )
                                        };
			using( IDataReader sR = StoredProcedureHelper.ExecuteReader( brokerId, "ListEmployeePermissionByEmployeeId" , parameters ) )
			{
				if( sR.Read() == true )
				{
                    SetInternalPermission(sR);
				} 
				else 
				{
					throw new GenericUserErrorMessageException( "No login account for this employee" );
				}
			}
		}

        public void Retrieve(Guid brokerId, Guid employeeId, IDataReader reader, Dictionary<Guid, PmlBrokerPermission> cache)
        {
            m_employeeID = employeeId;
            m_brokerID = brokerId;
            SetInternalPermission(reader, cache);
        }

		static public char GetDefaultOnValueFor( Permission permission ) 
		{
			return m_affirmativeBits[ (int)permission ] ; 
		}
		
		public string PermissionString 
		{
			get { return new String( this.m_permissionBits  ); } 
		}

        private void SetInternalPermission(IDataReader reader, Dictionary<Guid, PmlBrokerPermission> cache = null) 
        {
            var permissions = reader["Permissions"] as string ?? string.Empty;

            if (permissions.TrimWhitespaceAndBOM() == string.Empty) 
            {
                SetDefaultPermissions();
                InitBrokerDefaults();
                m_isDefault = true;
            } 
            else 
            {
                m_permissionBits = permissions.ToCharArray();
                m_isDefault = false;
            }

            var populationMethod = (EmployeePopulationMethodT)reader["PopulatePMLPermissionsT"];

            if (populationMethod == EmployeePopulationMethodT.OriginatingCompany)
            {
                SetInternalPermissionFromOC((Guid)reader["PmlBrokerId"], cache);
            }
        }

        private void SetInternalPermissionFromOC(Guid pmlBrokerId, Dictionary<Guid, PmlBrokerPermission> pmlBrokerPermissionCache = null)
        {
            if (pmlBrokerId == Guid.Empty || this.m_brokerID == Guid.Empty)
            {
                return;
            }

            PmlBrokerPermission pmlBrokerPermissions;

            if (pmlBrokerPermissionCache == null)
            {
                pmlBrokerPermissions = PmlBrokerPermission.Retrieve(this.m_brokerID, pmlBrokerId);
            }
            else if (!pmlBrokerPermissionCache.TryGetValue(pmlBrokerId, out pmlBrokerPermissions))
            {
                pmlBrokerPermissions = PmlBrokerPermission.Retrieve(this.m_brokerID, pmlBrokerId);
                pmlBrokerPermissionCache.Add(pmlBrokerId, pmlBrokerPermissions);
            }

            this.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, pmlBrokerPermissions.CanApplyForIneligibleLoanPrograms);
            this.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, pmlBrokerPermissions.CanRunPricingEngineWithoutCreditReport);
            this.SetPermission(Permission.CanSubmitWithoutCreditReport, pmlBrokerPermissions.CanSubmitWithoutCreditReport);
            this.SetPermission(Permission.CanAccessTotalScorecard, pmlBrokerPermissions.CanAccessTotalScorecard);
            this.SetPermission(Permission.AllowOrder4506T, pmlBrokerPermissions.AllowOrder4506T);
        }

        /// <summary>
        /// Initialize this permissions container with the given broker.  We
        /// collect the broker's defaults and return.
        /// </summary>

        public void Retrieve( Guid brokerId )
		{
			// Initialize this permissions container with the given broker.

			m_employeeID = Guid.Empty;

			m_brokerID = brokerId;

			SetDefaultPermissions();
			InitBrokerDefaults();

			m_isDefault = true;
		}

		public void RetrieveRolePermissions( Guid brokerId, Guid roleId)
		{
			// Initialize this permissions container with the given role.

			m_employeeID = Guid.Empty;
			m_brokerID   = brokerId;
			m_roleID	 = roleId;
		
			SqlParameter[] parameters = { 
											new SqlParameter( "@BrokerID" , brokerId ),
											new SqlParameter( "@RoleID" , roleId )
										};

			using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( brokerId, "ListRolePermissionsByBrokerId" , parameters ) )
			{

                if (reader.Read() == true)
				{
                    if (reader["DefaultPermissions"].ToString().TrimWhitespaceAndBOM() == "")
					{
						SetDefaultPermissions();
						InitBrokerDefaults();
						m_isDefault = true;
					} 
					else 
					{
                        m_permissionBits = reader["DefaultPermissions"].ToString().ToCharArray();
						m_isDefault = false;
					}
				} 
				else 
				{
                    SetDefaultPermissions();
                    InitBrokerDefaults();
                    m_isDefault = true;
                    AddRoleToRoleDefaultPermissions();

				}
			}	
		}

		private void AddRoleToRoleDefaultPermissions()
		{
			string pSet = new String( m_permissionBits );

            if (m_roleID != Guid.Empty && pSet.Length > 0)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", m_brokerID),
                                                new SqlParameter("@RoleID", m_roleID),
                                                new SqlParameter("@DefaultPermissions", pSet)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(m_brokerID, "CreateDefaultRolePermission", 3, parameters);
            }

		}

		/// <summary>
		/// Construct and load permission from database.
		/// </summary>

		public BrokerUserPermissions( 
            Guid brokerID, 
            Guid employeeID, 
            string chPermissions,
            string userType = null,
            Guid? pmlBrokerId = null,
            EmployeePopulationMethodT? permissionPopulationMethod = null,
            Dictionary<Guid, PmlBrokerPermission> pmlBrokerPermissionCache = null )
		{
			m_brokerID   = brokerID;
			m_employeeID = employeeID;

			SetDefaultPermissions();

			if( chPermissions.Length > 0 )
			{
				int i = 0;

				foreach( char c in chPermissions )
				{
					if( i < m_permissionBits.Length )
					{
						m_permissionBits[ i ] = c;
					}
					else
					{
						break;
					}

					++i;
				}

				m_isDefault = false;
			} 
			else 
			{
				m_isDefault = true;
			}

            if (!string.IsNullOrWhiteSpace(userType) && userType != "P")
            {
                return;
            }

            if (!permissionPopulationMethod.HasValue || !pmlBrokerId.HasValue)
            {
                SqlParameter[] parameter = { new SqlParameter("@EmployeeId", employeeID) };

                using (var reader = StoredProcedureHelper.ExecuteReader(this.m_brokerID, "ListEmployeePermissionByEmployeeId", parameter))
                {
                    if (reader.Read())
                    {
                        permissionPopulationMethod = (EmployeePopulationMethodT)reader.SafeInt("PopulatePMLPermissionsT");

                        pmlBrokerId = reader.SafeGuid("PmlBrokerId");
                    }
                    else
                    {
                        var msg = string.Format(
                            "Failed to load PopulatePMLPermissionsT and PmlBrokerId for employee {0} with broker {1}.",
                            employeeID,
                            this.m_brokerID);

                        throw new GenericUserErrorMessageException(msg);
                    }
                }
            }

            if (permissionPopulationMethod.Value == EmployeePopulationMethodT.OriginatingCompany)
            {
                this.SetInternalPermissionFromOC(pmlBrokerId.Value, pmlBrokerPermissionCache);
            }
        }

		/// <summary>
		/// Construct and load permission from database.
		/// </summary>

		public BrokerUserPermissions( Guid brokerID , Guid employeeID , CStoredProcedureExec spExec )
		{
			m_employeeID = employeeID;
			m_brokerID   = brokerID;

			using( IDataReader sR = spExec.ExecuteReader( "ListEmployeePermissionByEmployeeId" , new SqlParameter( "@EmployeeId" , employeeID ) )	)
			{

				if( sR.Read() == true )
				{
                    SetInternalPermission(sR);
				} 
				else 
				{
					throw new GenericUserErrorMessageException("No login account for this employee" );
				}
			}
		}

		/// <summary>
		/// Construct and load permission from database.
		/// </summary>

		public BrokerUserPermissions( Guid brokerId , Guid employeeId )
		{
			Retrieve( brokerId , employeeId );
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public BrokerUserPermissions( Guid brokerId )
		{
			Retrieve( brokerId );
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public BrokerUserPermissions()
		{
			// Initialize empty permissions.

			m_employeeID = Guid.Empty;
			m_brokerID   = Guid.Empty;

			SetDefaultPermissions();

			m_isDefault = true;
		}

		/// <summary>
		/// Initialize permission set for typical broker user.
		/// </summary>

		private void SetDefaultPermissions() 
        {
			// 12/20/2004 kb - We now just blank out the character
			// set.  Defaults are now set by the containing employee
			// accessor.

            m_permissionBits = new char[ MAX_PERMISSIONS ];

			for( int i = 0 ; i < MAX_PERMISSIONS ; i++ )
            {
				m_permissionBits[ i ] = '0';
            }

            SetPermission(Permission.CanCreateLoanTemplate, false);
            // OPM 112605 - These permissions should default to false. GF
            SetPermission(Permission.AllowUnderwritingAccess, false);
            SetPermission(Permission.CanAccessClosedLoans, false);
            SetPermission(Permission.CanWriteNonAssignedLoan, false);
            SetPermission(Permission.CanModifyLoanName, false);
            SetPermission(Permission.AllowLoanAssignmentsToAnyBranch, false);
            SetPermission(Permission.AllowAccessToTemplatesOfAnyBranch, false);
        }

		/// <summary>
		/// Initialize permission set for typical broker user.
		/// </summary>

		public void InitBrokerDefaults() 
		{
			// 12/20/2004 kb - We now just blank out the character
			// set.  Defaults are now set by the containing employee
			// accessor.
			//
			// 6/1/2005 kb - To make sure broker defaults are getting
			// set, we hook in here and initialize when the broker id
			// is valid.

			if( m_brokerID != Guid.Empty )
			{
				BrokerDB brokerDB = BrokerDB.RetrieveById(m_brokerID);

                if (brokerDB.IsAllowViewLoanInEditorByDefault == false)
					SetPermission( Permission.CanViewLoanInEditor , false );
				else
					SetPermission( Permission.CanViewLoanInEditor , true );

                if (brokerDB.RestrictWritingRolodexByDefault == true)
					SetPermission( Permission.AllowWritingToRolodex , false );
				else
					SetPermission( Permission.AllowWritingToRolodex , true );

                if (brokerDB.RestrictReadingRolodexByDefault == true)
					SetPermission( Permission.AllowReadingFromRolodex , false );
				else
					SetPermission( Permission.AllowReadingFromRolodex , true );

                if (brokerDB.CannotDeleteLoanByDefault == true)
					SetPermission( Permission.CanDeleteLoan , false );
				else
					SetPermission( Permission.CanDeleteLoan , true );

                if (brokerDB.CannotCreateLoanTemplateByDefault == true)
					SetPermission( Permission.CanCreateLoanTemplate , false );
				else
					SetPermission( Permission.CanCreateLoanTemplate , true );

                if (brokerDB.CanRunPricingEngineWithoutCreditReportByDefault == false)
				{
					SetPermission( Permission.CanRunPricingEngineWithoutCreditReport , false );
					SetPermission( Permission.CanSubmitWithoutCreditReport, false);
				}
				else
				{
					SetPermission( Permission.CanRunPricingEngineWithoutCreditReport , true );
					SetPermission( Permission.CanSubmitWithoutCreditReport, true);
				}

				SetPermission( Permission.IndividualLevelAccess , true );
				SetPermission( Permission.CanExportLoan , false );
                SetPermission( Permission.CanCreateLoanTemplate, false);
				SetPermission( Permission.CanAdministrateExternalUsers, false); //OPM 9721
				SetPermission( Permission.CanAccessCCTemplates, false); // 04/06/07 db - OPM 3453
				SetPermission( Permission.CanCreateCustomForms, true); // 05/31/07 db - OPM 2259
				SetPermission( Permission.CanEditOthersCustomForms, true); // 05/31/07 db - OPM 2259				
				SetPermission( Permission.CanEditDataTracLPNames, false);  // 08 17 08 opm 24638 
                SetPermission( Permission.CanEditEDocs, false);  // db - OPM 44494 
                SetPermission( Permission.CanViewEDocs, false);  // db - OPM 44494
                SetPermission( Permission.CanAccessTotalScorecard, false); // db - OPM 46742
                SetPermission(Permission.CanViewEDocsInternalNotes, false);
                SetPermission(Permission.CanEditEDocsInternalNotes, false);
                SetPermission(Permission.AllowCapitalMarketsAccess, false);
                SetPermission(Permission.AllowScreeningEDocs, false);
                SetPermission(Permission.AllowApprovingRejectingEDocs, false);
                SetPermission(Permission.AllowEditingApprovedEDocs, false);
                SetPermission(Permission.CanDuplicateLoans, false);
                SetPermission(Permission.AllowDocMagicDocumentGeneration_OBSOLETE, false);
                SetPermission(Permission.AllowDisclosureDocumentGeneration, false);
                SetPermission(Permission.AllowOrderingTitleServices, false);
                SetPermission(Permission.AllowEditingLoanProgramName, false); // opm 105273
                SetPermission(Permission.AllowManagingContactList, false);  // opm 109122
                SetPermission(Permission.AllowEnableGfePaidToManualDesc, false);  // opm 149774
			}
		}

		/// <summary>
		/// Test for certain permissions that denote this user
		/// is a privelaged, internal user that we add to brokers
		/// to have special access to a broker's data (for now,
		/// loan programs).
		/// </summary>
		/// <returns>
		/// True if internal.
		/// </returns>

		public bool IsInternalBrokerUser()
		{
			if( HasPermission( Permission.CanModifyLoanPrograms ) == true )
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Set permission for this object.
		/// </summary>
		/// <param name="p">
		/// Permission to set.
		/// </param>

		public void SetPermission( Permission p )
        {
            SetPermission( p , true );
        }

		/// <summary>
		/// Set permission for this object.
		/// </summary>
		/// <param name="p">
		/// Permission to set.
		/// </param>

		public void SetPermission( int p )
		{
			SetPermission( p , true );
		}

		/// <summary>
		/// Set permission for this object.
		/// </summary>
		/// <param name="p">
		/// Permission to set.
		/// </param>

		public void SetPermission( Permission p , bool isSet )
        {
			SetPermission( ( int ) p , isSet );
        }

		/// <summary>
		/// Set permission for this object.
		/// </summary>
		/// <param name="p">
		/// Permission to set.
		/// </param>

		public void SetPermission( int p , bool isSet )
		{
			// 5/25/2005 kb - Now, per case 1963, we pass all permission
			// setting to this implementation, which maps the active
			// state of a permission through its affirmative value.

			if( p >= 0 && p < m_permissionBits.Length )
			{
				if( m_affirmativeBits [ p ] == '1' )
				{
					m_permissionBits[ p ] = isSet ? '1' : '0';
				}
				else
				{
					m_permissionBits[ p ] = isSet ? '0' : '1';
				}
			}
		}

		/// <summary>
		/// Remove permission from this object.
		/// </summary>
		/// <param name="p">
		/// Permission to clear.
		/// </param>

		public void ClearPermission( Permission p ) 
        {
            SetPermission( p , false );
        }

		/// <summary>
		/// Test for permission / privelage.
		/// </summary>
		/// <param name="p">
		/// Permission to test.
		/// </param>
		
		public bool HasPermission( Permission p )
        {
            return HasPermission( ( int ) p );
        }

		/// <summary>
		/// Test for permission / privelage.
		/// </summary>
		/// <param name="p">
		/// Permission to test.
		/// </param>
		
		public bool HasPermission( int p )
		{
			if( p >= 0 && p < m_permissionBits.Length )
			{
				return m_permissionBits[ p ] == m_affirmativeBits[ p ];
			}

			return false;
		}

		/// <summary>
		/// OPM 9022 - use this function to determine if a given set of BrokerUserPermissions objects contains
		/// at least one with this permission set to true.
		/// </summary>
		public bool HasPermission(int p, ICollection bUpColl)
		{
			IEnumerator traverser = bUpColl.GetEnumerator();
			while(traverser.MoveNext())
			{
				if(traverser.Current.GetType().Equals(typeof(BrokerUserPermissions)))
				{
					BrokerUserPermissions bUp = traverser.Current as BrokerUserPermissions;
					if(bUp.HasPermission(p))
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Save role permissions to database.
		/// </summary>
		public void UpdateRolePermissions( CStoredProcedureExec spExec , Guid brokerId, Guid roleIdToUpdate )
		{
			string pSet = new String( m_permissionBits );

			if( pSet.Length > 0 )
			{
				int res = spExec.ExecuteNonQuery
					( "UpdateDefaultRolePermissions"
					, new SqlParameter( "@RoleID" , roleIdToUpdate		   )
					, new SqlParameter( "@BrokerID"   , brokerId           )
					, new SqlParameter( "@DefaultPermissions" , pSet       )
					);

				if( res < 0 )
				{
					throw new GenericUserErrorMessageException("Unable to save default role permissions." );
				}
			}
			else
			{
				throw new GenericUserErrorMessageException("Unable to save default role permissions." );
			}
		}

		/// <summary>
		/// Save permission to database.
		/// </summary>

		public void Update( CStoredProcedureExec spExec , Guid brokerId, Guid employeeIdToUpdate )
		{
			string pSet = new String( m_permissionBits );

			if( pSet.Length > 0 )
			{
				int res = spExec.ExecuteNonQuery
					( "UpdateEmployeePermission"
					, 1
					, new SqlParameter( "@EmployeeID" , employeeIdToUpdate )
					, new SqlParameter( "@BrokerID"   , brokerId           )
					, new SqlParameter( "@Permission" , pSet               )
					);

				if( res < 0 )
				{
					throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
				}
			}
			else
			{
				throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
			}
		}

		/// <summary>
		/// Save permission to database.
		/// </summary>

		public void Update( CStoredProcedureExec spExec , Guid employeeIdToUpdate )
		{
			string pSet = new String( m_permissionBits );

			if( pSet.Length > 0 )
			{
				int res = spExec.ExecuteNonQuery
					( "UpdateEmployeePermission"
					, 1
					, new SqlParameter( "@EmployeeID" , employeeIdToUpdate )
					, new SqlParameter( "@BrokerID"   , m_brokerID         )
					, new SqlParameter( "@Permission" , pSet               )
					);

				if( res < 0 )
				{
					throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
				}
			}
			else
			{
				throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
			}
		}

		/// <summary>
		/// Save permission to database.
		/// </summary>

		public void Update( CStoredProcedureExec spExec )
		{
			string pSet = new String( m_permissionBits );

			if( pSet.Length > 0 )
			{
				int res = spExec.ExecuteNonQuery
					( "UpdateEmployeePermission"
					, 1
					, new SqlParameter( "@EmployeeID" , m_employeeID )
					, new SqlParameter( "@BrokerID"   , m_brokerID   )
					, new SqlParameter( "@Permission" , pSet         )
					);

				if( res < 0 )
				{
					throw new GenericUserErrorMessageException( "Unable to save broker user permissions." );
				}
			}
			else
			{
				throw new GenericUserErrorMessageException( "Unable to save broker user permissions." );
			}
		}

		/// <summary>
		/// Save permission to database.
		/// </summary>

		public void Update()
		{
			string pSet = new String( m_permissionBits );

			if( pSet.Length > 0 )
			{
                SqlParameter[] parameters = {
                                                new SqlParameter( "@EmployeeID" , m_employeeID )
					                            , new SqlParameter( "@BrokerID"   , m_brokerID   )
					                            , new SqlParameter( "@Permission" , pSet         )
                                            };

				int res = StoredProcedureHelper.ExecuteNonQuery( m_brokerID, "UpdateEmployeePermission", 1, parameters);

				if( res < 0 )
				{
					throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
				}
			}
			else
			{
				throw new GenericUserErrorMessageException("Unable to save broker user permissions." );
			}
		}

	}
}
