using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Security
{
    /// <summary>
    /// Each permission corresponds to a position within the
    /// internal user's permission option array.  We offset
    /// into the array to get the current setting.  Add new
    /// permissions for internal users here.
    /// </summary>

    public enum E_InternalUserPermissions
    {
        IsSystemAdmin    =  0,
        IsDevelopment    =  1,
        ViewBrokers      =  2,
        EditBroker       =  3,
        CreateBroker     =  4,
        ViewEmployees    =  5,
        EditEmployee     =  6,
        BecomeUser       =  7,
        ViewActivity     =  8,
        ViewInvoices     =  9,
        GenerateInvoices = 10,
        ViewLenders      = 11,
		IsSAE			 = 12,
		UpdateStageConstants = 13,
        HasBillingAccess = 14,
        EditIntegrationSetup = 15,
        FeatureAdoption = 16,
        DownloadReplicationWhitelist = 17,
        UploadReplicationWhitelist = 18,
        ExportBroker = 19,
        ImportBroker = 20,
        Max
    }

    /// <summary>
    /// We store the permissions in our array.  We use
    /// the enum to offset into a particular setting.
    /// </summary>

    public class InternalUserPermissions
    {
        /// <summary>
        /// We store the permissions in our array.  We use
        /// the enum to offset into a particular setting.
        /// </summary>

        private char[] m_Opts = new char[ 100 ];

        public string Opts
        {
            // Access member.

            set
            {
                m_Opts = value.ToCharArray();
            }
            get
            {
                return new string( m_Opts );
            }
        }

        /// <summary>
        /// Set new permission option.  The boolean flag is
        /// translated to the corresponding character.
        /// </summary>
        /// <param name="eOpt">
        /// Permission option to set.
        /// </param>
        /// <param name="bOn">
        /// Option setting on?
        /// </param>

        public void Set( E_InternalUserPermissions eOpt , Boolean bOn )
        {
            // Set new option setting.

            if( m_Opts != null )
            {
                m_Opts[ ( int ) eOpt ] = ( bOn ? '1' : '0' );
            }
        }

        /// <summary>
        /// Check this internal user's permission set to see
        /// if the specified action is allowed.
        /// </summary>
        /// <param name="eOpt">
        /// Permission option to check.
        /// </param>
        /// <returns>
        /// True if allowed.
        /// </returns>

        public bool Can( E_InternalUserPermissions eOpt )
        {
            // Read option setting.

            if( m_Opts != null )
            {
                if( m_Opts[ ( int ) eOpt ] == '1' )
                {
                    return true;
                }
            }

            return false;
        }

		/// <summary>
		/// Construct default.
		/// </summary>

		public InternalUserPermissions( String sOpts )
		: this()
		{
			// Initialize members.

			int i = 0;

			foreach( char c in sOpts )
			{
				if( i < m_Opts.Length )
				{
					m_Opts[ i ] = c;
				}
				else
				{
					break;
				}

				++i;
			}
		}

        /// <summary>
        /// Construct default.
        /// </summary>

        public InternalUserPermissions()
        {
            // Initialize members.

            for( int i = 0 ; i < m_Opts.Length ; ++i )
            {
                m_Opts[ i ] = '0';
            }
        }

    }

}