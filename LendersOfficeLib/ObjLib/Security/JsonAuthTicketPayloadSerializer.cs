﻿namespace LendersOffice.ObjLib.Security
{
    using LendersOffice.Common;

    /// <summary>
    /// A json serializer for AuthTicketPayload.
    /// </summary>
    public class JsonAuthTicketPayloadSerializer : IAuthTicketPayloadSerializer
    {
        /// <summary>
        /// Deserializes the json into a AuthTicketPayload.
        /// </summary>
        /// <param name="data">The json data to deserialize.</param>
        /// <returns>The AuthTicketPayload representing the payload.</returns>
        public AuthTicketPayload Deserialize(string data)
        {
            return SerializationHelper.JsonNetDeserialize<AuthTicketPayload>(data);
        }

        /// <summary>
        /// Serializes the given AuthTicketPayload into JSON.
        /// </summary>
        /// <param name="payload">The payload to serialize.</param>
        /// <returns>The JSON string representation of the payload.</returns>
        public string Serialize(AuthTicketPayload payload)
        {
            return SerializationHelper.JsonNetSerialize(payload);
        }
    }
}
