﻿// <copyright file="UserRegisteredIp.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is a UserRegisteredIp class.</summary>
namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// Registered IP of a user. See OPM 125242.
    /// </summary>
    public class UserRegisteredIp
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="UserRegisteredIp" /> class from being created.
        /// </summary>
        private UserRegisteredIp()
        {
        }

        /// <summary>
        /// Gets the id of the entry.
        /// </summary>
        /// <value>Id of the entry.</value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>Broker id.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>User id of the entry.</value>
        public Guid UserId { get; private set; }

        /// <summary>
        /// Gets or sets the IP Address.
        /// </summary>
        /// <value>IP Address.</value>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>Description of the entry.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets the created date of the entry.
        /// </summary>
        /// <value>Created date of the entry.</value>
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IP marked by user as come from trusted network. When it is true, we only perform 
        /// additional security check every 30 days.
        /// </summary>
        /// <value>Whether the IP marked by user as come from trusted network.</value>
        public bool IsRegistered { get; set; }

        /// <summary>
        /// Retrieve a list of the known IP addresses for the user.
        /// </summary>
        /// <param name="brokerId">BrokerId of the user. This is for additional security.</param>
        /// <param name="userId">UserId of the user.</param>
        /// <returns>A list of known IP addresses for the user.</returns>
        public static IEnumerable<UserRegisteredIp> ListByUserId(Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserId", userId)
            };
            List<UserRegisteredIp> list = new List<UserRegisteredIp>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ALL_USER_REGISTERED_IP_ListByUserId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(CreateFromReader(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Add known IP address to user account.
        /// </summary>
        /// <param name="brokerId">The broker user belong to.</param>
        /// <param name="userId">User id for the entry..</param>
        /// <param name="knownIpAddress">Known IP Address to add.</param>
        /// <param name="isRegistered">Whether this IP address from trusted network.</param>
        public static void AddKnownIpAddress(Guid brokerId, Guid userId, string knownIpAddress, bool isRegistered)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@IpAddress", knownIpAddress),
                new SqlParameter("@IsRegistered", isRegistered),
                new SqlParameter("@Description", isRegistered ? "REGISTERED" : string.Empty)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ALL_USER_REGISTERED_IP_Add", 3, parameters);
        }

        /// <summary>
        /// Delete known IP address entry from user account.
        /// </summary>
        /// <param name="brokerId">The broker user belong to.</param>
        /// <param name="userId">User id for the entry.</param>
        /// <param name="id">Entry to delete.</param>
        public static void Delete(Guid brokerId, Guid userId, int id)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@Id", id)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ALL_USER_REGISTERED_IP_Delete", 3, parameters);
        }

        /// <summary>
        /// Internal method for create class from reader.
        /// </summary>
        /// <param name="reader">SQL data reader contains necessary information.</param>
        /// <returns>Individual record.</returns>
        internal static UserRegisteredIp CreateFromReader(DbDataReader reader)
        {
            UserRegisteredIp item = new UserRegisteredIp();

            item.Id = (int)reader["Id"];
            item.BrokerId = (Guid)reader["BrokerId"];
            item.UserId = (Guid)reader["UserId"];
            item.IpAddress = (string)reader["IpAddress"];
            item.Description = (string)reader["Description"];
            item.CreatedDate = (DateTime)reader["CreatedDate"];
            item.IsRegistered = (bool)reader["IsRegistered"];

            return item;
        }
    }
}
