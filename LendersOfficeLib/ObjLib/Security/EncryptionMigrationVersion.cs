﻿namespace LendersOffice.ObjLib.Security
{
    /// <summary>
    /// Provides a marker for the encryption migration version of a loan file.
    /// </summary>
    public enum EncryptionMigrationVersion
    {
        /// <summary>
        /// The loan file has not been migrated to encrypt sensitive data.
        /// </summary>
        Unmigrated = 0,

        /// <summary>
        /// The social security numbers on the loan file have been encrypted.
        /// </summary>
        SocialSecurityNumbers = 1,

        /// <summary>
        /// The FHA sponsored originator EIN, agent XML, and preparer XML 
        /// have been migrated.
        /// </summary>
        SponsoredOriginatorEinAgentXml = 2
    }
}