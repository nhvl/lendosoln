﻿namespace LendersOffice.ObjLib.Security
{
    using System;
    using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Encapsulates data for a vendor client certificate.
    /// </summary>
    public class VendorClientCertificate
    {
        /// <summary>
        /// Verifies that the ID of the certificate supplied belongs
        /// to the vendor with the specified ID.
        /// </summary>
        /// <param name="vendorId">
        /// The ID of the vendor.
        /// </param>
        /// <param name="certificateId">
        /// The ID of the certificate.
        /// </param>
        /// <returns>
        /// True if the certificate belongs to the vendor, false otherwise.
        /// </returns>
        public static bool VerifyCertificate(Guid vendorId, Guid certificateId)
        {
            if (vendorId == Guid.Empty || certificateId == Guid.Empty)
            {
                return false;
            }

            var procedureName = StoredProcedureName.Create("VENDOR_CLIENT_CERTIFICATE_Verify").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@VendorId", vendorId),
                new SqlParameter("@CertificateId", certificateId)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                return reader.Read();
            }
        }
    }
}
