﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using LendersOffice.ObjLib.ConsumerPortal;
using System.Security.Principal;
using LendersOffice.Constants;
using DataAccess;

namespace LendersOffice.Security
{
    public class ConsumerPortalUserTicket
    {
        public long ConsumerPortalUserId { get; private set; }
        public Guid BrokerId { get; private set; }
        public Guid ConsumerPortalId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string RandomEntropy { get; private set; }
        public DateTime ExpiredDate { get; private set; }

        public string AllowLoanNumber { get; private set; }
        public Guid AllowLoanId { get; private set; }
        public bool AllowViewSubmittedLoan { get; private set; }
        public string ConsumerPortalEmail { get; private set; }

        private ConsumerPortalUserPrincipal x_principal = null;
        public ConsumerPortalUserPrincipal Principal
        {
            get
            {
                if (x_principal == null)
                {
                    ConsumerPortalUser consumerPortalUser = ConsumerPortalUser.Retrieve(this.BrokerId, this.ConsumerPortalUserId);

                    x_principal = new ConsumerPortalUserPrincipal(new GenericIdentity(consumerPortalUser.Email), consumerPortalUser, this.ConsumerPortalId, this.AllowLoanId);
                }
                return x_principal;
            }
        }

        private ConsumerPortalUserTicket()
        {
            AllowLoanNumber = string.Empty;
            ConsumerPortalEmail = string.Empty;
        }

        public void ValidateLoanAndPartialSsn(string sLNm, string partialSsn)
        {
            // 10/28/2013 dd - For added security, we going to required last 4 digits of SSN to match with the applicant email address.
            Guid sLId = Tools.GetLoanIdByLoanName(this.BrokerId, sLNm);
            if (sLId == Guid.Empty)
            {
                throw new CBaseException("Invalid loan number " + sLNm, "Invalid loan number " + sLNm);
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortalUserTicket));
            dataLoan.InitLoad();

            bool isValidate = false;
            bool isEmailMatched = false;
            bool isSsnMatched = false;
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                if (dataApp.aBSsn.EndsWith(partialSsn))
                {
                    isSsnMatched = true;
                    if (dataApp.aBEmail.Equals(this.ConsumerPortalEmail, StringComparison.OrdinalIgnoreCase))
                    {
                        isEmailMatched = true;
                        isValidate = true;
                        break;
                    }
                }
                else if(dataApp.aCSsn.EndsWith(partialSsn))
                {
                    isSsnMatched = true;
                    if (dataApp.aCEmail.Equals(this.ConsumerPortalEmail, StringComparison.OrdinalIgnoreCase))
                    {
                        isEmailMatched = true;
                        isValidate = true;
                        break;
                    }
                }
            }

            if (!isValidate)
            {
                foreach (TitleBorrower tb in dataLoan.sTitleBorrowers)
                {
                    if (tb.SSN.EndsWith(partialSsn))
                    {
                        isSsnMatched = true;
                        if (tb.Email.Equals(this.ConsumerPortalEmail, StringComparison.OrdinalIgnoreCase))
                        {
                            isEmailMatched = true;
                            isValidate = true;
                            break;
                        }
                    }
                }
            }

            ConsumerPortalUser portalUser = ConsumerPortalUser.Retrieve(this.BrokerId, this.ConsumerPortalUserId);

            if (portalUser.IsLockedOut)
            {
                // 4/27/2017 BS - (450275) When user account gets locked, display message to user and send a notification to the email address
                var agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                string loPhone = null;
                string fromAddress = null;
                if (agent.IsValid)
                {
                    loPhone = agent.Phone;
                    fromAddress = agent.EmailAddr;
                }
                else if (this.Principal.ConsumerPortalConfig.LoanOfficerEmployeeDB != null)
                {
                    loPhone = this.Principal.ConsumerPortalConfig.LoanOfficerEmployeeDB.Phone;
                    fromAddress = this.Principal.ConsumerPortalConfig.LoanOfficerEmployeeDB.Email;
                }
                else
                {
                    loPhone = this.Principal.ConsumerPortalConfig.LeadManagerEmployeeDB.Phone;
                    fromAddress = this.Principal.ConsumerPortalConfig.LeadManagerEmployeeDB.Email;
                }    

                Email.CBaseEmail email = new Email.CBaseEmail(this.BrokerId);
                email.From = fromAddress;
                email.To = this.ConsumerPortalEmail;
                email.Subject = "Account has been locked";
                email.Message = "Your account has been locked due to too many incorrect login attempts. "
                        + "Please call us at " + loPhone + " to unlock your account.";
                email.SendImmediately();

                throw new CBaseException("Account has been locked out. Please call us at " 
                    + loPhone + " to unlock your account", "Account is locked out.");
            }
            if (isValidate == false)
            {
                // 10/28/2013 dd - Increase the log in failure count.
                // 10/28/2013 dd - The correct way is to create a new SSN counter.
                portalUser.LoginAttempts += 4; // 10/28/2013 dd - A quick attemmpt to only allow 3 tries with SSN
                portalUser.Save();

                string errorMessage = string.Empty;
                if (!isSsnMatched)
                {
                    errorMessage = "Partial SSN does not match.";
                }
                else if (!isEmailMatched)
                {
                    errorMessage = "Access Denied. Could not find a matching borrower on the application.";
                }

                throw new CBaseException(errorMessage, errorMessage);
            }
            else
            {
                ExpiredDate = DateTime.Now.AddMinutes(this.Principal.ConsumerPortalConfig.SessionTimeoutInMinutes);

                // 10/28/2013 dd - Validation complete.
                AllowLoanNumber = sLNm;
                AllowViewSubmittedLoan = true;
                AllowLoanId = sLId;
            }
        }
        public void ExtendExpiration()
        {
            ExpiredDate = DateTime.Now.AddMinutes(this.Principal.ConsumerPortalConfig.SessionTimeoutInMinutes);
        }
        public string Encrypt()
        {
            string sTicket = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}",
                RandomEntropy, // 0
                BrokerId, // 1
                ConsumerPortalId, // 2
                ConsumerPortalUserId, // 3
                CreatedDate.Ticks, // 4
                ExpiredDate.Ticks, // 5
                ConsumerPortalEmail, // 6
                AllowViewSubmittedLoan.ToString(), // 7
                AllowLoanNumber, // 8
                AllowLoanId // 9
                );

            return EncryptionHelper.Encrypt(sTicket);
        }

        public static ConsumerPortalUserTicket Decrypt(string encryptString)
        {
            string[] parts = EncryptionHelper.Decrypt(encryptString).Split('|');

            Guid brokerId = new Guid(parts[1]);
            Guid consumerPortalId = new Guid(parts[2]);
            long consumerPortalUserId = long.Parse(parts[3]);
            DateTime createdDate = new DateTime(long.Parse(parts[4]));
            DateTime expiredDate = new DateTime(long.Parse(parts[5]));

            string consumerPortalEmail = parts[6];
            bool allowViewSubmittedLoan = bool.Parse(parts[7]);
            string allowLoanNumber = parts[8];
            Guid allowLoanId = new Guid(parts[9]);
            if (expiredDate < DateTime.Now)
            {
                throw new AccessDenied("Ticket is expired");
            }

            ConsumerPortalUserTicket consumerPortalUserTicket = new ConsumerPortalUserTicket();
            consumerPortalUserTicket.BrokerId = brokerId;
            consumerPortalUserTicket.ConsumerPortalId = consumerPortalId;
            consumerPortalUserTicket.ConsumerPortalUserId = consumerPortalUserId;
            consumerPortalUserTicket.CreatedDate = createdDate;
            consumerPortalUserTicket.ExpiredDate = expiredDate;
            consumerPortalUserTicket.ConsumerPortalEmail = consumerPortalEmail;
            consumerPortalUserTicket.AllowViewSubmittedLoan = allowViewSubmittedLoan;
            consumerPortalUserTicket.AllowLoanNumber = allowLoanNumber;
            consumerPortalUserTicket.AllowLoanId = allowLoanId;
            return consumerPortalUserTicket;
        }

        public static ConsumerPortalUserTicket Create(Guid consumerPortalId, ConsumerPortalUserPrincipal principal)
        {
            ConsumerPortalUserTicket consumerPortalUserTicket = new ConsumerPortalUserTicket();
            consumerPortalUserTicket.CreatedDate = DateTime.Now;
            consumerPortalUserTicket.ExpiredDate = DateTime.Now.AddMinutes(principal.ConsumerPortalConfig.SessionTimeoutInMinutes);
            consumerPortalUserTicket.RandomEntropy = Guid.NewGuid().ToString();
            consumerPortalUserTicket.x_principal = principal;
            consumerPortalUserTicket.BrokerId = principal.BrokerId;
            consumerPortalUserTicket.ConsumerPortalId = consumerPortalId;
            consumerPortalUserTicket.ConsumerPortalUserId = principal.Id;
            consumerPortalUserTicket.ConsumerPortalEmail = principal.Email;
            consumerPortalUserTicket.AllowLoanId = principal.AllowViewSubmittedLoanId;

            return consumerPortalUserTicket;
        }
        public static ConsumerPortalUserTicket Create(Guid consumerPortalId, ConsumerPortalUser user)
        {
            ConsumerPortalConfig config = ConsumerPortalConfig.Retrieve(user.BrokerId, consumerPortalId);

            ConsumerPortalUserTicket consumerPortalUserTicket = new ConsumerPortalUserTicket();
            consumerPortalUserTicket.CreatedDate = DateTime.Now;
            consumerPortalUserTicket.ExpiredDate = DateTime.Now.AddMinutes(config.SessionTimeoutInMinutes);
            consumerPortalUserTicket.RandomEntropy = Guid.NewGuid().ToString();
            consumerPortalUserTicket.BrokerId = user.BrokerId;
            consumerPortalUserTicket.ConsumerPortalId = consumerPortalId;
            consumerPortalUserTicket.ConsumerPortalUserId = user.Id;
            consumerPortalUserTicket.ConsumerPortalEmail = user.Email;
            consumerPortalUserTicket.AllowLoanId = Guid.Empty;
            return consumerPortalUserTicket;
        }
    }
}
