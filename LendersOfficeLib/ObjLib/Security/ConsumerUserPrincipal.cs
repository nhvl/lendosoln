﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using System.Security.Principal;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Collections;
using System.Web;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Web.Security;
using System.Text.RegularExpressions;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.Security
{
    public enum E_ConsumerSSnMatch
    {
        NoMatch = 0,
        PartialMatch,
        FullMatch
    }

    public sealed class ConsumerUserPrincipal : AbstractUserPrincipal 
    {
        private static readonly string[] ConsumerRoles = { "B", ConstApp.ROLE_CONSUMER };
        private static new readonly string Permissions = "0100100011110100110100011001111111111000000000000000000000000000000000000000000000000000000000000000";

        private Int64 m_consumerId;
        private string m_email;
        private bool m_isLocked;
        private DateTime m_LastLoggedInD;
        private DateTime m_PasswordRequestD;
        private int m_LoginFailureNumber;
        private string m_PasswordResetCode;
        private bool m_IsTemporaryPassword;

        private string m_LastSsnDigits;
        private int m_SsnFailureNumber;
        private DateTime m_LastSsnAttemptD;
        private DateTime? m_DisclosureAcceptedD;
            

        private Dictionary<Guid, bool> m_LoanAccess = null; //Dictionary that contains accessible loan ids by this consumer
        private Dictionary<Guid, Guid> m_AppAccess = null; //Dictionary that contains accessible app ids by this consumer

        public Int64 ConsumerId
        {
            get { return m_consumerId; }
        }

        public string LoginName
        {
            get { return m_email; }
        }

        public string LastSsnDigits
        {
            get { return m_LastSsnDigits; }
        }

        public void SetSsn(string ssn)
        {
            if (!string.IsNullOrEmpty(ssn) && ssn.Length == 4 && Regex.IsMatch(ssn, @"\d\d\d\d"))
            {
                m_LastSsnDigits = ssn;
            }
        }

        public int SsnFailureNumber
        {
            get { return m_SsnFailureNumber; }
        }

        public DateTime LastSsnAttemptD
        {
            get { return m_LastSsnAttemptD; }
        }

        public string Email
        {
            get { return m_email; }
        }

        public bool IsTempPassword
        {
            get { return m_IsTemporaryPassword; }
            set { m_IsTemporaryPassword = value; }
        }

        public DateTime LastLoginD
        {
            get { return m_LastLoggedInD; }
        }

        public string ToConsumerDataString()
        {
            return string.Format("{0}|{1}|{2}|{3}", this.Type, ConsumerId, this.ApplicationType.ToString("D"), this.BrokerId);
        }

        public override string ToUserDataString(bool isAnonymousQP = false)
        {
            throw new Exception();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ConsumerId: " + ConsumerId);
            sb.AppendLine("Consumer: " + Email);
            sb.AppendLine("Broker: " + BrokerId);
            sb.AppendLine("Last Login: " + m_LastLoggedInD);
            return sb.ToString();
        }

        private string m_signingAs = null;

        public string SigningAs
        {
            get
            {
                return m_signingAs;
            }
            set
            {
                m_signingAs = value;
            }
        }

        public bool CanAccessLoanFile(Guid loanId)
        {
            if (m_LoanAccess == null || m_AppAccess == null)
                RetrieveConsumerAccessibleItems();

            return m_LoanAccess.ContainsKey(loanId);
        }

        public bool CanAccessApp(Guid appId)
        {
            if (m_AppAccess == null || m_LoanAccess == null)
                RetrieveConsumerAccessibleItems();

            return m_AppAccess.ContainsKey(appId);
        }

        public Guid GetLoanFromAppId(Guid appId)
        {
            if (m_AppAccess == null || m_LoanAccess == null)
                RetrieveConsumerAccessibleItems();

            if (m_AppAccess.ContainsKey(appId))
            {
                return m_AppAccess[appId];
            }
            return Guid.Empty;
        }

        Dictionary<Guid, bool> m_LoanSsnList = null;
        string m_SsnDigitsUsed = "";

        public bool NeedsSsnVerification(Guid loanId)
        {
            if (loanId == Guid.Empty)
                return false;

            if (string.IsNullOrEmpty(LastSsnDigits) || !Regex.IsMatch(LastSsnDigits, @"\d\d\d\d"))
                return true;

            if (m_LoanSsnList == null || m_SsnDigitsUsed != LastSsnDigits)
                RetrieveConsumerAccessibleSsn(LastSsnDigits);

            if (!m_LoanSsnList.ContainsKey(loanId))
                return true;

            return !m_LoanSsnList[loanId];
            
        }

        public bool AcceptedDisclosure
        {
            get {
                return m_DisclosureAcceptedD.HasValue; 
            }
        }

        public E_ConsumerSSnMatch VerifySsn(string ssn)
        {
            if (string.IsNullOrEmpty(ssn) || !Regex.IsMatch(ssn, @"\d\d\d\d"))
                return E_ConsumerSSnMatch.NoMatch;

            if (m_LoanSsnList == null || m_SsnDigitsUsed != ssn)
                RetrieveConsumerAccessibleSsn(ssn);

            int total = m_LoanSsnList.Count;
            int match = 0;

            foreach (Guid loanId in m_LoanSsnList.Keys)
            {
                if (m_LoanSsnList[loanId])
                    match++;
            }

            if (total <= 0 || match == 0)
            {
                return E_ConsumerSSnMatch.NoMatch;
            }

            if (match < total)
            {
                return E_ConsumerSSnMatch.PartialMatch;
            }

            if (match == total)
            {
                return E_ConsumerSSnMatch.FullMatch;
            }

            throw new Exception();
        }

        public static bool? RetrievePasswordIsTemporaryByEmail(string Email, Guid BrokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Email", Email),
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            
            bool? ret = null;

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "CP_RetrieveConsumerPasswordTemporaryByEmailBroker", parameters))
            {
                if (reader.Read())
                {
                    if (reader["IsTemporaryPassword"] != DBNull.Value)
                    {
                        ret = Convert.ToBoolean(reader["IsTemporaryPassword"]);
                    }
                }
            }

            return ret;
        }

        private void RetrieveConsumerAccessibleSsn(string ssn)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("ConsumerId: {0}\n", this.ConsumerId);
            sb.AppendFormat("Email: {0}\n", this.Email);
            sb.AppendFormat("SSN Attempt: {0}\n", ssn);
            sb.AppendFormat("SsnFailureNumber: {0}\n", this.SsnFailureNumber);
            sb.AppendFormat("m_signingAs: {0}\n", this.m_signingAs);

            if (string.IsNullOrEmpty(ssn) || !Regex.IsMatch(ssn, @"\d\d\d\d"))
            {
                sb.AppendLine("Invalid SSN Detected");
                Tools.LogInfo("CPDEBUG-" + EncryptionHelper.Encrypt(sb.ToString()));
                throw new Exception("Invalid ssn detected.");
            }

            if (m_AppAccess == null || m_LoanAccess == null)
            {
                RetrieveConsumerAccessibleItems();
            }

            m_LoanSsnList = new Dictionary<Guid, bool>();
            m_SsnDigitsUsed = ssn;
            sb.AppendLine();
            foreach (Guid appId in m_AppAccess.Keys)
            {
                Guid loanId = m_AppAccess[appId];
                if (m_LoanSsnList.ContainsKey(loanId))
                {
                    continue;
                }

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ConsumerUserPrincipal));
                try
                {
                    dataLoan.InitLoad();
                }
                catch (PageDataLoadDenied)
                {
                    sb.AppendFormat("\nCould not load loan {0}\n", loanId);
                    continue;
                }
                CAppData app = dataLoan.GetAppData(appId);

                bool ContainsSsn = false;
                if (IsValidSsn(app.aBSsn) && Email.Equals(app.aBEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase))
                {
                    ContainsSsn = (app.aBSsn.Substring(app.aBSsn.Length - 4) == ssn);
                    sb.AppendFormat("\nIF1 ContainsSSN: {0}\n", ContainsSsn);
                }
                if (IsValidSsn(app.aCSsn) && Email.Equals(app.aCEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase))
                {
                    ContainsSsn |= (app.aCSsn.Substring(app.aCSsn.Length - 4) == ssn);
                    sb.AppendFormat("\nIF2 ContainsSSN: {0}\n", ContainsSsn);
                }
                if (IsValidSsn(app.aBSsn) && IsValidSsn(app.aCSsn) && app.aBSsn == app.aCSsn)
                {
                    ContainsSsn = false;
                    sb.AppendFormat("\nIF3 ContainsSSN: {0}\n", ContainsSsn);
                }

                sb.AppendFormat("app.aBSsn = {0};\nIsValidSsn(app.aBSsn) = {1};\nEmail = {2};\napp.aBEmail.Trim = {3};\n Email.Equals(app.aBEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase) = {4};\nF={5}\n",
                    app.aBSsn,     //0
                    IsValidSsn(app.aBSsn), //1 
                    Email,   //2
                    app.aBEmail.TrimWhitespaceAndBOM(), //3
                    Email.Equals(app.aBEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase),  //4
                    IsValidSsn(app.aBSsn) && Email.Equals(app.aBEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase)); //5 

                sb.AppendFormat("app.aCSsn = {0};\nIsValidSsn(app.aCSsn) = {1};\nEmail = {2};\napp.aCEmail.Trim = {3};\nEmail.Equals(app.aCEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase) = {4};\nF={5}\n",
                    app.aCSsn,    //0
                    IsValidSsn(app.aCSsn), //1 
                    Email,  //2
                    app.aCEmail.TrimWhitespaceAndBOM(), //3
                    Email.Equals(app.aCEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase),  //4 
                    IsValidSsn(app.aCSsn) && Email.Equals(app.aCEmail.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase)); //5

                sb.AppendFormat("IsValidSsn(app.aBSsn) && IsValidSsn(app.aCSsn) && app.aBSsn == app.aCSsn : {0}\n", IsValidSsn(app.aBSsn) && IsValidSsn(app.aCSsn) && app.aBSsn == app.aCSsn);
                sb.AppendFormat("sLId {0} AppId {1} has match ssn : {2} \n\n\n", loanId, appId, ContainsSsn);

                m_LoanSsnList.Add(loanId, ContainsSsn);
            }
            foreach (KeyValuePair<Guid, bool> item in m_LoanSsnList)
            {
                sb.AppendFormat("LoanId: {0} ContainsSSN: {1}\n", item.Key, item.Value);
            }
            Tools.LogInfo("CPDEBUG-" + EncryptionHelper.Encrypt(sb.ToString()));
        }


        private bool IsValidSsn(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }
            return Regex.IsMatch(str, @"\d\d\d-\d\d-\d\d\d\d");
        }

        private void RetrieveConsumerAccessibleItems()
        {
            m_LoanAccess = new Dictionary<Guid, bool>();
            m_AppAccess = new Dictionary<Guid, Guid>();
            
             SqlParameter[] parameters = { new SqlParameter("@ConsumerId", ConsumerId) };

             using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "CP_RetrieveAccessibleLoansByConsumerId", parameters))
             {
                 while (reader.Read())
                 {
                     Guid loanId = (Guid)reader["sLId"];
                     if (!m_LoanAccess.ContainsKey(loanId))
                     {
                         m_LoanAccess.Add(loanId, true);
                     }
                     Guid appId = (Guid)reader["aAppId"];
                     if (!m_AppAccess.ContainsKey(appId))
                     {
                         m_AppAccess.Add(appId, loanId);
                     }
                 }
             }
        }

        public ConsumerUserPrincipal(IIdentity identity, Int64 consumerId, Guid brokerId, string email, bool islocked, DateTime lastlogin, DateTime pwRequestD, 
            int failNumber, string pwReqCode, bool IsTempPassword, string SsnDigits, int SsnFailureNumber, DateTime LastSsnAttemptD, DateTime? DisclosureAcceptedD)
        : base(identity, ConsumerUserPrincipal.ConsumerRoles, Guid.Empty /* userId */, brokerId, Guid.Empty /* branchId */, Guid.Empty /* employeeId */, Guid.Empty,
        "", "", ConsumerUserPrincipal.Permissions, false /*isOthersAllowedToEditUnderwriterAssignedFile*/, false /*isLOAllowedToEditProcessorAssignedFile */,
            Guid.Empty /*lpePriceGroupId*/, false /* isRateLockedAtSubmission */, true /*hasLenderDefaultFeatures*/, Guid.Empty /* becomeUserId*/,
            false, false, "" /* cookieSessionId */, Guid.Empty, E_PmlLoanLevelAccess.Individual
            , "C" //B=broker user(LO), P=PML User, C=Consumer Portal User
            , E_ApplicationT.ConsumerPortal, null /*ipWhiteList */, null /* teams */, false /* IsNewPmlUIEnabled */
            , false /* IsUsePml2AsQuickPricer */, E_PortalMode.Blank /*User's last portal mode*/
            , Guid.Empty /* Mini-Correspondent Branch Id */
            , Guid.Empty /* Correspondent Branch Id */
            , initialPrincipalType: InitialPrincipalTypeT.NonInternal
            , initialUserId: Guid.Empty
            , postLoginTask: PostLoginTask.Redirect)
        {
            m_consumerId = consumerId;
            m_email = email;
            m_isLocked = islocked;
            m_LastLoggedInD = lastlogin;
            m_PasswordRequestD = pwRequestD;
            m_LoginFailureNumber = failNumber;
            m_PasswordResetCode = pwReqCode;
            m_IsTemporaryPassword = IsTempPassword;
            
            m_LastSsnAttemptD = LastSsnAttemptD;
            m_LastSsnDigits = SsnDigits;
            m_SsnFailureNumber = SsnFailureNumber;
            m_DisclosureAcceptedD = DisclosureAcceptedD;
        }

    }
}
