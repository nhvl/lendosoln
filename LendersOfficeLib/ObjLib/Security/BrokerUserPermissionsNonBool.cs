﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using LendersOffice.Admin;

namespace LendersOffice.Security
{
    public enum NonBoolPermission
    {
        RateLockRequestForExpiredRate = 0
    }

    public class BrokerUserPermissionsNonBool
    {
        /// <summary>
        /// We need a cap count for allocation so that we can
        /// work with a fixed array of permission characters.
        /// </summary>

        private const int MAX_PERMISSIONS = 100;

        /// <summary>
        /// Track an individual user's broker permissions.
        /// </summary>

        private char[] m_permissionBits;
        private Guid m_brokerID;
        private Guid m_employeeID;
        private Guid m_roleID = Guid.Empty;

        private Boolean m_isDefault;

        public Boolean IsDefault
        {
            get { return m_isDefault; }
        }

        public string PermissionString
        {
            get { return new String(this.m_permissionBits); }
        }

        /// <summary>
		/// Construct and load permission from database.
		/// </summary>

		public BrokerUserPermissionsNonBool( Guid brokerId , Guid employeeId )
		{
			Retrieve( brokerId , employeeId );
		}

        public BrokerUserPermissionsNonBool()
        {
        }

        /// <summary>
        /// Initialize permission set for typical broker user.
        /// </summary>

        private void SetDefaultPermissions()
        {
            // 12/20/2004 kb - We now just blank out the character
            // set.  Defaults are now set by the containing employee
            // accessor.

            m_permissionBits = new char[MAX_PERMISSIONS];

            for (int i = 0; i < MAX_PERMISSIONS; i++)
            {
                m_permissionBits[i] = '0';
            }
        }

        public void Retrieve(Guid brokerId, Guid employeeId)
        {
            // Initialize this permissions container with the given employee.

            m_employeeID = employeeId;
            m_brokerID = brokerId;
            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeID", employeeId)
                                        };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeePermissionByEmployeeId", parameters))
            {
                if (sR.Read() == true)
                {
                    SetInternalPermission(sR["Permissions"].ToString());
                }
                else
                {
                    throw new GenericUserErrorMessageException("No login account for this employee");
                }
            }
        }

        private void InitBrokerDefaults()
        {
            string pSet = new String(m_permissionBits);
            SetPermission(NonBoolPermission.RateLockRequestForExpiredRate, char.Parse(((int)E_RatesheetExpirationBypassType.BypassAll).ToString()));
            string pSet1 = new String(m_permissionBits);
        }

        protected void AddRoleToRoleDefaultPermissions()
        {
            string pSet = new String(m_permissionBits);
            CStoredProcedureExec spExec = new CStoredProcedureExec(m_brokerID);
            try
            {
                spExec.BeginTransactionForWrite();

                if ((pSet.Length > 0) && (m_roleID != Guid.Empty))
                {
                    int res = spExec.ExecuteNonQuery
                        ("CreateDefaultNonBoolRolePermission"
                        , new SqlParameter("@BrokerID", m_brokerID)
                        , new SqlParameter("@RoleID", m_roleID)
                        , new SqlParameter("@DefaultNonBoolPermissions", pSet)
                        );
                }

                spExec.CommitTransaction();
            }
            catch (Exception e)
            {
                spExec.RollbackTransaction();
                throw new GenericUserErrorMessageException(e);
            }
        }

        private bool BoolPermissionsWereSet(Guid brokerId, Guid roleId)
        {
            SqlParameter[] parameters = { 
											new SqlParameter( "@BrokerID" , brokerId ),
											new SqlParameter( "@RoleID" , roleId )
										};

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListRolePermissionsByBrokerId", parameters))
            {
                return sR.Read();
            }
        }

        public void RetrieveRolePermissions(Guid brokerId, Guid roleId)
        {
            // Initialize this permissions container with the given role.

            m_employeeID = Guid.Empty;
            m_brokerID = brokerId;
            m_roleID = roleId;

            SqlParameter[] parameters = { 
											new SqlParameter( "@BrokerID" , brokerId ),
											new SqlParameter( "@RoleID" , roleId )
										};

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListNonBoolRolePermissionsByBrokerId", parameters))
            {
                if (sR.Read() == true)
                {
                    if (sR["DefaultNonBoolPermissions"].ToString().TrimWhitespaceAndBOM() == "")
                    {
                        SetDefaultPermissions();
                        InitBrokerDefaults();
                        m_isDefault = true;
                    }
                    else
                    {
                        m_permissionBits = sR["DefaultNonBoolPermissions"].ToString().ToCharArray();
                        m_isDefault = false;
                    }
                }
                else if (BoolPermissionsWereSet(brokerId, roleId) == false)
                {
                    // check for an existing real role in the ROLE table and add it to the default role permissions
                    // table since not there yet
                    if (Role.IsValidRoleId(roleId))
                    {
                        // the role is valid but is not yet included in the ROLE_DEFAULT_PERMISSIONS table
                        // so we will include it now with general defaults
                        SetDefaultPermissions();
                        InitBrokerDefaults();
                        m_isDefault = true;
                        try
                        {
                            AddRoleToRoleDefaultPermissions();
                        }
                        catch (GenericUserErrorMessageException g)
                        {
                            throw new CBaseException(ErrorMessages.FailedCreatingDefaultNonBoolRolePermissions, ErrorMessages.FailedCreatingDefaultNonBoolRolePermissions + ": " + g);
                        }
                    }
                    else
                    {
                         throw new CBaseException(ErrorMessages.FailedCreatingDefaultNonBoolRolePermissions, ErrorMessages.FailedCreatingDefaultNonBoolRolePermissions + "- Invalid RoleID");
                    }

                }
                else
                {
                    // At this point, bool permissions were set already, but no non-bool permissions were set, so we will set the default values for our UI
                    SetDefaultPermissions();
                    InitBrokerDefaults();
                }
            }
        }

        private void SetInternalPermission(string permission)
        {
            if (null == permission || permission.TrimWhitespaceAndBOM() == "")
            {
                SetDefaultPermissions();
                InitBrokerDefaults();
                m_isDefault = true;
            }
            else
            {
                m_permissionBits = permission.ToCharArray();
                m_isDefault = false;
            }
        }

        public void SetPermission(NonBoolPermission p, char value)
        {
            SetPermission((int)p, value);
        }

        public void SetPermission(NonBoolPermission p, int value)
        {
            char val = char.Parse(value.ToString());
            SetPermission((int)p, val);
        }

        public void SetPermission(int p, char value)
        {
            if (p >= 0 && p < m_permissionBits.Length)
            {
                m_permissionBits[p] = value;
            }
        }

        #region Get Permission
        public char GetPermission(NonBoolPermission p)
        {
            return GetPermission((int)p);
        }

        public char GetPermission(NonBoolPermission p, ICollection coll)
        {
            return GetPermission((int)p, coll);
        }

        /// <summary>
        /// Return permission
        /// </summary>
        /// <param name="p">
        /// Permission to test.
        /// </param>

        public char GetPermission(int p)
        {
            if (p >= 0 && p < m_permissionBits.Length)
            {
                return m_permissionBits[p];
            }
            else
            {
                throw new CBaseException("Unable to load permissions.", "Unable to get non-bool permissions - permission index " + p + " is not valid.  Non-bool permission indices must be greater than 0 and less than " + MAX_PERMISSIONS);
            }
        }

        // Retrieve the lowest value in the list - we will consider this to be the "highest" level of the permission
        // This must be enforced by devs adding new permissions to this class.  Each new permission added must
        // be ordered such that the lowest value indicates the most permission.  This gives us room to continue
        // to add more restrictive permission later.
        public char GetPermission(int p, ICollection bUpColl)
        {
            IEnumerator traverser = bUpColl.GetEnumerator();
            int highestVal = -1;
            while (traverser.MoveNext())
            {
                if (traverser.Current.GetType().Equals(typeof(BrokerUserPermissionsNonBool)))
                {
                    BrokerUserPermissionsNonBool bUp = traverser.Current as BrokerUserPermissionsNonBool;
                    int currentPVal = int.Parse(bUp.GetPermission(p).ToString());
                    if (highestVal < 0)
                    {
                        highestVal = currentPVal;
                    }
                    else
                    {
                        if (currentPVal > highestVal)
                        {
                            highestVal = currentPVal;
                        }
                    }
                }
            }

            if (highestVal < 0)
            {
                throw new CBaseException("Unable to load permissions.", "Unable to get non-bool permissions - permission index " + p + " is not valid.  Non-bool permission indices must be greater than 0 and less than " + MAX_PERMISSIONS);
            }
            else
            {
                return char.Parse(highestVal.ToString());
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Save role permissions to database.
        /// </summary>
        public void UpdateRolePermissions(CStoredProcedureExec spExec, Guid brokerId, Guid roleIdToUpdate)
        {
            string pSet = new String(m_permissionBits);

            if (pSet.Length > 0)
            {
                int res = spExec.ExecuteNonQuery
                    ("UpdateDefaultNonBoolRolePermissions"
                    , new SqlParameter("@RoleID", roleIdToUpdate)
                    , new SqlParameter("@BrokerID", brokerId)
                    , new SqlParameter("@DefaultNonBoolPermissions", pSet)
                    );

                if (res < 0)
                {
                    throw new GenericUserErrorMessageException("Unable to save default role permissions non-bool.");
                }
            }
            else
            {
                throw new GenericUserErrorMessageException("Unable to save default role permissions non-bool.");
            }
        }

        /// <summary>
        /// Save permission to database.
        /// </summary>

        public void Update(CStoredProcedureExec spExec, Guid brokerId, Guid employeeIdToUpdate)
        {
            string pSet = new String(m_permissionBits);

            if (pSet.Length > 0)
            {
                int res = spExec.ExecuteNonQuery
                    ("UpdateEmployeeNonBoolPermission"
                    , 1
                    , new SqlParameter("@EmployeeID", employeeIdToUpdate)
                    , new SqlParameter("@BrokerID", brokerId)
                    , new SqlParameter("@NonBoolPermission", pSet)
                    );

                if (res < 0)
                {
                    throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
                }
            }
            else
            {
                throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
            }
        }

        /// <summary>
        /// Save permission to database.
        /// </summary>

        public void Update(CStoredProcedureExec spExec, Guid employeeIdToUpdate)
        {
            string pSet = new String(m_permissionBits);

            if (pSet.Length > 0)
            {
                int res = spExec.ExecuteNonQuery
                    ("UpdateEmployeeNonBoolPermission"
                    , 1
                    , new SqlParameter("@EmployeeID", employeeIdToUpdate)
                    , new SqlParameter("@BrokerID", m_brokerID)
                    , new SqlParameter("@NonBoolPermission", pSet)
                    );

                if (res < 0)
                {
                    throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions .");
                }
            }
            else
            {
                throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
            }
        }

        /// <summary>
        /// Save permission to database.
        /// </summary>

        public void Update(CStoredProcedureExec spExec)
        {
            string pSet = new String(m_permissionBits);

            if (pSet.Length > 0)
            {
                int res = spExec.ExecuteNonQuery
                    ("UpdateEmployeeNonBoolPermission"
                    , 1
                    , new SqlParameter("@EmployeeID", m_employeeID)
                    , new SqlParameter("@BrokerID", m_brokerID)
                    , new SqlParameter("@NonBoolPermission", pSet)
                    );

                if (res < 0)
                {
                    throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
                }
            }
            else
            {
                throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
            }
        }

        /// <summary>
        /// Save permission to database.
        /// </summary>

        public void Update()
        {
            string pSet = new String(m_permissionBits);

            if (pSet.Length > 0)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeID", m_employeeID)
                                                , new SqlParameter("@BrokerID", m_brokerID)
                                                , new SqlParameter("@NonBoolPermission", pSet)
                                            };

                int res = StoredProcedureHelper.ExecuteNonQuery(m_brokerID, "UpdateEmployeeNonBoolPermission", 1, parameters);

                if (res < 0)
                {
                    throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
                }
            }
            else
            {
                throw new GenericUserErrorMessageException("Unable to save broker user non-bool permissions.");
            }
        }

        #endregion
    }
}
