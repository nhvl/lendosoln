using System;

namespace LendersOffice.Security
{
	/// <summary>
	/// Summary description for IAuthentication.
	/// </summary>
	public interface IAuthentication
	{
        /// <summary>
        /// After validate username/password, need to set System.Thread.CurrentPrincipal to correct principal.
        /// Throw SecurityException if failed.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        void Authenticate(string username, string password);
        void Authenticate(string secretKey);
        string GenerateSecretKey(string username, string password);
	}
}
