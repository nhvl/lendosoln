///
/// Author: David Dao
///
namespace LendersOffice.Security
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Security.Principal;
    using System.Threading;
    using System.Web;
    using DataAccess;
    using Admin;
    using Common;
    using Constants;
    using Events;
    using global::CommonProjectLib.Common.Lib;
    using System.Data;
    using ObjLib.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar;
    using LqbGrammar.Drivers;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// // 4/18/2014 dd -
    /// Use enum for stored procedure that need to retrieve user authentication information.
    /// The name of enum property MUST MATCH with actual stored procedure name.
    /// </summary>
    public enum E_RetrieveBrokerUserStoreProcedureT
    {
        RetrieveUserCredentialByAuthenticationId,
        RetrieveUserCredentialByUserId,
        RetrieveUserCredentialByUserName,
        RetrievePmlUserCredentialByUserName,
        RetrieveInactiveUserCredentialByUserId
    }
    public sealed class BrokerUserPrincipal : AbstractUserPrincipal
    {
        public bool GiveNewLoanEditorUIPreferenceOption
        {
            get
            {
                switch (this.BrokerDB.ActualNewLoanEditorUIPermissionType)
                {
                    case E_PermissionType.HardEnable:
                    case E_PermissionType.HardDisable:
                        return false;
                    case E_PermissionType.DeferToMoreGranularLevel:
                        return this.HasPermission(Permission.AllowViewingNewLoanEditorUI);
                    default:
                        throw new UnhandledEnumException(this.BrokerDB.ActualNewLoanEditorUIPermissionType);
                }
            }
        }

        public bool OptedToUseNewLoanEditorUI { get; set; }

        public bool ActuallyShowNewLoanEditorUI
        {
            get
            {
                switch (this.BrokerDB.ActualNewLoanEditorUIPermissionType)
                {
                    case E_PermissionType.HardEnable:
                        return true;
                    case E_PermissionType.HardDisable:
                        return false;
                    case E_PermissionType.DeferToMoreGranularLevel:
                        return this.OptedToUseNewLoanEditorUI && this.HasPermission(Permission.AllowViewingNewLoanEditorUI);
                    default:
                        throw new UnhandledEnumException(this.BrokerDB.ActualNewLoanEditorUIPermissionType);
                }
            }
        }

        public E_BrokerBillingVersion BillingVersion { get; private set; }

        public string BrokerName { get; private set; }

        public bool IsPasswordExpired { get; private set; }

        public bool HasLONIntegration { get; private set; }

        public Guid SelectedPipelineCustomReportId { get; private set; }

        public Guid LastUsedCreditProtocolId { get; private set; }

        public string LastUsedCreditLoginNm { get; private set; }
        public string LastUsedCreditAccountId { get; private set; }
        public bool ByPassBgCalcForGfeAsDefault { get; private set; }

        private bool m_needToAcceptLatestAgreement;

        public bool NeedToAcceptLatestAgreement
        {
            get
            {
                // 1/5/2004 dd - Allow LOAdmin to become broker user without the need to accept TOS.
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("tos");
                if (null != cookie && cookie.Value == "f")
                    return false;
                else
                    return m_needToAcceptLatestAgreement;
            }
        }

        /// <summary>
        /// Does the user have access to the given feature at a broker level
        /// and access to the privileged action at a user level?
        /// </summary>
        /// <param name="feature">
        /// Broker level subscription feature.
        /// </param>
        /// <param name="accessControl">
        /// Individual access control right.
        /// </param>
        /// <returns>
        /// True if user has feature and permission.
        /// </returns>

        public bool HasAccessTo(E_BrokerFeatureT feature, Permission accessControl)
        {
            return HasFeatures(feature) && HasPermission(accessControl);
        }

        private BrokerUserPrincipal(DbConnectionInfo connInfo, IIdentity identity, string firstName, string lastName, Guid brokerId,
            Guid userId, Guid employeeId, string permissions,
            string[] roles, bool needToAcceptLatestAgreement, DateTime passwordExpirationD,
            Guid branchId, Guid loginSessionId, string brokerName,
            bool isOthersAllowedToEditUnderwriterAssignedFile,
            bool isLOAllowedToEditProcessorAssignedFile, bool hasLONIntegration,
            Guid selectedPipelineCustomReportId, E_ApplicationT applicationT,
            Guid lastUsedCreditProtocolId, string lastUsedCreditLoginNm, string lastUsedCreditAccountId,
            bool byPassBgCalcForGfeAsDefault, string type, Guid lpePriceGroupId,
            bool isRateLockedAtSubmission,
            bool hasLenderDefaultFeatures, bool isQuickPricerEnable, bool isPricingMultipleAppsSupported, E_BrokerBillingVersion billingVersion,
            Guid pmlBrokerId, E_PmlLoanLevelAccess pmlAccess, IEnumerable<string> ipWhiteList, HashSet<Guid> teamList,
            bool isNewPmlUIEnabled,
            bool isUsePml2AsQuickPricer, E_PortalMode portalMode, Guid miniCorrespondentBranchId, Guid correspondentBranchId,
            bool optsToUseNewLoanEditorUI, string cookieSessionId, InitialPrincipalTypeT initialPrincipalType, Guid initialUserId, PostLoginTask postLoginTask)
            : base(identity, roles, userId, brokerId, branchId, employeeId, loginSessionId,
            firstName, lastName, permissions, isOthersAllowedToEditUnderwriterAssignedFile,
            isLOAllowedToEditProcessorAssignedFile, lpePriceGroupId, isRateLockedAtSubmission,
            hasLenderDefaultFeatures, Guid.Empty /* becomeUserId*/, isQuickPricerEnable,
            isPricingMultipleAppsSupported, cookieSessionId, pmlBrokerId, pmlAccess
            , type, applicationT,
            ipWhiteList, teamList, isNewPmlUIEnabled, isUsePml2AsQuickPricer, portalMode/*User's last portal mode*/,
            miniCorrespondentBranchId, correspondentBranchId, initialPrincipalType, initialUserId, postLoginTask)
        {
            SetConnectionInfoInternalUse(connInfo);

            m_needToAcceptLatestAgreement = needToAcceptLatestAgreement;
            BrokerName = brokerName;
            BillingVersion = billingVersion;
            IsPasswordExpired = DateTime.Now.CompareTo(passwordExpirationD) >= 0;
            HasLONIntegration = hasLONIntegration;
            SelectedPipelineCustomReportId = selectedPipelineCustomReportId;
            LastUsedCreditProtocolId = lastUsedCreditProtocolId;
            LastUsedCreditLoginNm = lastUsedCreditLoginNm;
            LastUsedCreditAccountId = lastUsedCreditAccountId;
            ByPassBgCalcForGfeAsDefault = byPassBgCalcForGfeAsDefault;
            this.OptedToUseNewLoanEditorUI = optsToUseNewLoanEditorUI;
        }

        //3/26/07 db - OPM 10294 Notify users if their account has been disabled
        //8/08/07 db - OPM 1863 Enhance this to notify if blocked or needs to wait as well
        public static AbstractUserPrincipal CreatePrincipalWithFailureType(Guid brokerId, DbConnectionInfo connInfo, E_RetrieveBrokerUserStoreProcedureT storedProcedureName, SqlParameter[] parameters,
            string password, Guid expectedLoginSessionId, bool allowDuplicateLogin, E_ApplicationT applicationT, bool isStoreToCookie, out PrincipalFactory.E_LoginProblem loginProblem, LoginSource loginSource)
        {
            string loginNm = "";
            Guid brokerPmlSiteId = Guid.Empty;
            AbstractUserPrincipal logPrincipal = null;
            EmployeeDB employeeDB = null;
            foreach (SqlParameter parm in parameters)
            {
                if (parm.ParameterName.Equals("@LoginName"))
                {
                    loginNm = parm.Value.ToString();
                }
                if (parm.ParameterName.Equals("@CustomerCode")) //will only exist for PML users
                {
                    try
                    {
                        brokerPmlSiteId = PrincipalFactory.GetBrokerPmlSiteIdFromCustomerCode(parm.Value.ToString());
                    }
                    catch { } //ignore and leave at Guid.Empty
                }
            }

            // Only LQB users are AD users; if AD user then force them to use the AD authentication path
            if (applicationT == E_ApplicationT.LendersOffice)
            {
                if (IsLQBUserActiveDirectoryUser(connInfo, loginNm))
                {
                    loginProblem = PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType;
                    employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerIdAndLoginName(brokerId, loginNm);

                    if (employeeDB != null)
                    {
                        logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, loginNm, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "B", employeeDB.UserID);
                    }

                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                    return null;
                }
            }

            string type = (applicationT == E_ApplicationT.LendersOffice) ? "B" : "P";

            //Check first if they're allowed to log in
            Tuple<bool, PrincipalFactory.E_LoginProblem> isAvailableLoginTime_LoginProblem = IsNowAnAvailableLoginTime(connInfo, loginNm, type, brokerPmlSiteId);
            if (!isAvailableLoginTime_LoginProblem.Item1)
            {
                loginProblem = isAvailableLoginTime_LoginProblem.Item2;
                employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerIdAndLoginName(brokerId, loginNm);

                if (employeeDB != null)
                {
                    logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, loginNm, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "B", employeeDB.UserID);
                }
                
                SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
                return null;
            }

            AbstractUserPrincipal principal = null;

            // OPM 194431 - Check Password before creating principal.
            if (Tools.IsPasswordValid(connInfo, loginNm, brokerPmlSiteId, password))
            {
                principal = CreatePrincipal(connInfo, storedProcedureName, parameters, expectedLoginSessionId, allowDuplicateLogin, applicationT, isStoreToCookie, true /*updateCurrentPrincipal */);
            }

            loginProblem = PrincipalFactory.E_LoginProblem.None;
            if (principal == null)
            {
                employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerIdAndLoginName(brokerId, loginNm);

                if (employeeDB != null)
                {
                    logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, loginNm, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, type.ToUpper(), employeeDB.UserID);
                }                

                if (IsDisabled(connInfo, loginNm, type, brokerPmlSiteId))
                {
                    loginProblem = PrincipalFactory.E_LoginProblem.IsDisabled;
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                }
                else
                {
                    int loginFailureCount = IncrementLoginFailureCount(connInfo, loginNm, type, brokerPmlSiteId);

                    if (loginFailureCount >= 50)
                    {
                        LockAccount(brokerId, connInfo, loginNm, type, brokerPmlSiteId, logPrincipal);
                        loginProblem = PrincipalFactory.E_LoginProblem.IsLocked;
                    }
                    else if (loginFailureCount < 10)
                    {
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                        loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
                    }
                    else
                    {
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                        AddTimeToLoginBlock(connInfo, loginNm, type, brokerPmlSiteId, Math.Floor((double)(loginFailureCount / 10)));
                        loginProblem = PrincipalFactory.E_LoginProblem.InvalidAndWait;
                    }
                }
            }
            else
            {
                // OPM 26569 - reject accounts with CanModifyLoanPrograms permission from logging in directly from LO (need to use "become" from LoAdmin instead)
                if (IsInternalUser(applicationT, principal))
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(principal, loginSource);
                    principal = null;
                    loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
                    RequestHelper.ClearAuthenticationCookies();
                }
            }

            return principal;
        }

        public static AbstractUserPrincipal CreatePrincipalWithFailureTypeForActiveDirectoryUser(DbConnectionInfo connInfo, E_RetrieveBrokerUserStoreProcedureT sStoredProcedureName, SqlParameter[] parameters, string sLoginName, bool bAllowDuplicateLogin, out PrincipalFactory.E_LoginProblem loginProblem, IUserPrincipal logPrincipal, LoginSource loginSource)
        {
            string sUserType = "B"; // Only LQB users are AD users

            // LQB lock-out and wait time should be non-existent for AD users but any such existing security measure will still be honored
            Tuple<bool, PrincipalFactory.E_LoginProblem> isAvailableLoginTime_LoginProblem = IsNowAnAvailableLoginTime(connInfo, sLoginName, sUserType, Guid.Empty);
            if (!isAvailableLoginTime_LoginProblem.Item1)
            {
                loginProblem = isAvailableLoginTime_LoginProblem.Item2;
                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                return null;
            }

            AbstractUserPrincipal principal = CreatePrincipal(connInfo, sStoredProcedureName, parameters, Guid.Empty, bAllowDuplicateLogin, E_ApplicationT.LendersOffice, true, true /*updateCurrentPrincipal */);
            loginProblem = PrincipalFactory.E_LoginProblem.None;

            // Auth management such as lockout on failed attempts will be handled by the lender within Active Directory
            // The LQB user account must still be valid & not disabled
            if (principal == null)
            {
                if (IsDisabled(connInfo, sLoginName, sUserType, Guid.Empty))
                {
                    loginProblem = PrincipalFactory.E_LoginProblem.IsDisabled;
                }
                else
                {
                    loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
                }

                SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
            }
            else
            {
                // OPM 26569 - reject accounts with CanModifyLoanPrograms permission from logging in directly from LO (need to use "become" from LoAdmin instead)
                if (IsInternalUser(E_ApplicationT.LendersOffice, principal))
                {
                    principal = null;
                    loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, loginSource);
                    RequestHelper.ClearAuthenticationCookies();
                }
            }

            return principal;
        }

        private static Tuple<bool, PrincipalFactory.E_LoginProblem> IsNowAnAvailableLoginTime(DbConnectionInfo connInfo, string sLoginName, string sUserType, Guid brokerPMLSiteId)
        {
            Tuple<bool, PrincipalFactory.E_LoginProblem> result = Tuple.Create(true, PrincipalFactory.E_LoginProblem.None);

            DateTime nextAvailableLoginTime = GetNextAvailableLoginTime(connInfo, sLoginName, sUserType, brokerPMLSiteId);
            if (nextAvailableLoginTime != SmallDateTime.MinValue)
            {
                if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))
                {
                    result = Tuple.Create(false, PrincipalFactory.E_LoginProblem.IsLocked);
                }
                else if (nextAvailableLoginTime > DateTime.Now)
                {
                    result = Tuple.Create(false, PrincipalFactory.E_LoginProblem.NeedsToWait);
                }
            }

            return result;
        }

        private static bool IsLQBUserActiveDirectoryUser(DbConnectionInfo connInfo, string sLoginName)
        {

            Object oIsADUser = null;

            try
            {

                SqlParameter[] parameters = {
                                                new SqlParameter("LoginName", sLoginName),
                                                new SqlParameter("Type", "B")
                                            };

                oIsADUser = StoredProcedureHelper.ExecuteScalar(connInfo, "IsActiveDirectoryUser", parameters);
            }
            catch (NotFoundException)
            {
                return false;
            }
            catch (SqlException sqlExc)
            {
                Tools.LogError("Failed to retrieve Active Directory status for login " + sLoginName + ".", sqlExc);
                return false;
            }

            return Convert.ToBoolean(oIsADUser);
        }

        private static bool IsInternalUser(E_ApplicationT environment, AbstractUserPrincipal principal)
        {
            return ((environment == E_ApplicationT.LendersOffice) && (principal.HasPermission(Permission.CanModifyLoanPrograms))
                && (principal.LoginNm.ToLower() != "nhingo") // OPM 111905 - "nhingo" is allowed to log in with "allow modifying loan programs"
                );
        }

        private static int IncrementLoginFailureCount(DbConnectionInfo connInfo, string loginNm, string type, Guid BrokerPmlSiteId)
        {
            int loginFailureCount = 0;

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@LoginNm", loginNm), new SqlParameter("@Type", type), new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "UpdateLoginFailureCount", parameters))
                {
                    if (reader.Read())
                        loginFailureCount = Int32.Parse(reader["LoginFailureCount"].ToString());
                }
            }
            catch { }

            return loginFailureCount;
        }

        private static DateTime GetNextAvailableLoginTime(DbConnectionInfo connInfo, string loginNm, string type, Guid BrokerPmlSiteId)
        {
            DateTime nextAvailableLoginTime = SmallDateTime.MinValue;


            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginNm),
                                            new SqlParameter("@Type", type),
                                            new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId)
                                        };

            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetLoginBlockExpirationDate", parameters))
                {
                    if (reader.Read())
                    {
                        nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                    }
                }
            }
            catch { }

            return nextAvailableLoginTime;
        }

        private static void LockAccount(Guid brokerId, DbConnectionInfo connInfo, string loginNm, string type, Guid BrokerPmlSiteId, AbstractUserPrincipal logPrincipal)
        {

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginNm),
                                            new SqlParameter("@Type", type),
                                            new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId),
                                            new SqlParameter("@NextAvailableLoginTime", SmallDateTime.MaxValue)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(connInfo, "SetLoginBlockExpirationDate", 1, parameters);

            if (type.ToUpper().Equals("B"))
            {
                // notify user that their account has been locked
                Guid employeeID = Guid.Empty;
                parameters = new SqlParameter[] {
                    new SqlParameter("@LoginName", loginNm)
                };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetEmployeeIdByLoginName", parameters))
                {
                    if (reader.Read())
                        employeeID = (Guid)reader["EmployeeId"];
                    }

                if (Guid.Empty != employeeID)
                {
                    try
                    {
                        UserAccountLockedEvent userAccLocked = new UserAccountLockedEvent();
                        userAccLocked.Initialize(employeeID, loginNm, E_AccountLockedReason.UnsuccessfulLoginAttempts);
                        userAccLocked.Send();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(String.Format("Failed to send User Account Locked Event for login {0}", loginNm) + e);
                    }
                }
            }

            SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
        }

        private static bool IsDisabled(DbConnectionInfo connInfo, string loginNm, string type, Guid BrokerPmlSiteId)
        {
            try
            {


                if (type.ToUpper().Equals("B"))
                {

                    SqlParameter[] parameters = {
                                                   new SqlParameter("@LoginName", loginNm),
                                                   new SqlParameter("@Type", type)
                                               };

                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveDisabledLendersOfficeUserByLoginNm", parameters))
                    {
                        return reader.Read();
                    }
                }
                else if (type.ToUpper().Equals("P"))
                {
                    SqlParameter[] parameters = { new SqlParameter("@LoginName", loginNm), new SqlParameter("@BrokerPmlSiteID", BrokerPmlSiteId) };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveDisabledPmlUserByLoginNm", parameters))
                    {
                        return reader.Read();
                    }
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private static void AddTimeToLoginBlock(DbConnectionInfo connInfo, string loginNm, string type, Guid BrokerPmlSiteId, double minutesToAdd)
        {
            if (minutesToAdd < 0)
            {
                return;
            }

            DateTime nextAvailableTime = DateTime.Now.AddMinutes(minutesToAdd);

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginNm),
                                            new SqlParameter("@Type", type),
                                            new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId),
                                            new SqlParameter("@NextAvailableLoginTime", nextAvailableTime)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(connInfo, "SetLoginBlockExpirationDate", 1, parameters);
        }

        public static AbstractUserPrincipal CreatePrincipal(DbConnectionInfo connInfo, E_RetrieveBrokerUserStoreProcedureT storedProcedureName, IEnumerable<SqlParameter> parameters, Guid expectedLoginSessionId, bool allowDuplicateLogin, E_ApplicationT applicationT, bool isStoreToCookie, bool updateCurrentPrincipal, string cookieSessionId = "", InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid(), PostLoginTask postLoginTask = PostLoginTask.Login)
        {
            // 7/20/2005 dd - If adding new info return by stored procedures, make sure all following procedures have same result set.
            // RetrieveUserCredentialByAuthenticationId
            // RetrieveUserCredentialByUserId
            // RetrieveUserCredentialByUserName
            // RetrievePmlUserCredentialByUserName

            if (connInfo == null)
            {
                throw CBaseException.GenericException("DbConnectionInfo object is null.");
            }

            IReadOnlyDictionary<string, object> record = null;
            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, storedProcedureName.ToString(), parameters))
            {
                if (reader.Read())
                {
                    record = reader.ToDictionary();
                }
            }

            return record == null ? null : CreatePrincipal(record, expectedLoginSessionId, allowDuplicateLogin, applicationT, isStoreToCookie, updateCurrentPrincipal, cookieSessionId, initialPrincipalType, initialUserId, postLoginTask);
        }


        /// <summary>
        /// Creates a BrokerUserPrincipal from the given database values and other options.
        /// </summary>
        /// <param name="dataRecord">A dictionary containing the required data points to create a BrokerUserPrincipal. Usually this is read straight from the database already, as we don't want to leave the SQL connection open for the lifetime of this function.</param>
        /// <param name="expectedLoginSessionId">The expected login session id, for </param>
        /// <param name="allowDuplicateLogin">Whether to allow duplicate login for this creation. If true, indicates that the load is for data purposes, rather than for logging in to the UI and doesn't check logic related to multiple concurrent logins.</param>
        /// <param name="applicationT">Whether the user is being loaded for Lender's Office, PML, etc.</param>
        /// <param name="isStoreToCookie">Whether to store the login session to a cookie for the user's browser.</param>
        /// <param name="updateCurrentPrincipal">Whether to update the static <see cref="Thread.CurrentPrincipal"/> for the current thread in the <see cref="BrokerUserPrincipal"/> class.</param>
        /// <returns></returns>
        public static AbstractUserPrincipal CreatePrincipal(IReadOnlyDictionary<string, object> dataRecord, Guid expectedLoginSessionId, bool allowDuplicateLogin, E_ApplicationT applicationT, bool isStoreToCookie, bool updateCurrentPrincipal, string cookieSessionId = "", InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid(), PostLoginTask postLoginTask = PostLoginTask.Login)
        {
            // 7/20/2005 dd - If adding new info return by stored procedures, make sure all following procedures have same result set.
            // RetrieveUserCredentialByAuthenticationId
            // RetrieveUserCredentialByUserId
            // RetrieveUserCredentialByUserName
            // RetrievePmlUserCredentialByUserName
            // RetrieveInactiveUserCredentialByUserId

            BrokerUserPrincipal principal = null;

            Guid newLoginSessionID = Guid.NewGuid();
            Guid brokerId = (Guid)dataRecord["BrokerId"];
            Guid userId = (Guid)dataRecord["UserId"];
            Guid employeeId = (Guid)dataRecord["EmployeeId"];
            string permissions = (string)dataRecord["Permissions"];
            string brokerName = (string)dataRecord["BrokerNm"];
            string firstName = (string)dataRecord["UserFirstNm"];
            string lastName = (string)dataRecord["UserLastNm"];
            bool needToAcceptLatestAgreement = (bool)dataRecord["NeedToAcceptLatestAgreement"];
            string loginName = (string)dataRecord["LoginNm"];
            bool isSharable = (bool)dataRecord["IsSharable"];
            Guid branchId = (Guid)dataRecord["BranchId"];
            bool isOthersAllowedToEditUnderwriterAssignedFile = (bool)dataRecord["IsOthersAllowedToEditUnderwriterAssignedFile"];
            bool isLOAllowedToEditProcessorAssignedFile = (bool)dataRecord["IsLOAllowedToEditProcessorAssignedFile"];
            Guid selectedPipelineCustomReportId = (Guid)dataRecord["SelectedPipelineCustomReportId"];
            string lastUsedCreditLoginNm = (string)dataRecord["LastUsedCreditLoginNm"];
            string lastUsedCreditAccountId = (string)dataRecord["LastUsedCreditAccountId"];
            bool byPassBgCalcForGfeAsDefault = (bool)dataRecord["ByPassBgCalcForGfeAsDefault"];
            bool isRateLockedAtSubmission = (bool)dataRecord["IsRateLockedAtSubmission"];
            bool hasLenderDefaultFeatures = (bool)dataRecord["HasLenderDefaultFeatures"];
            bool isQuickPricerEnable = Convert.IsDBNull(dataRecord["IsQuickPricerEnable"]) ? false : (bool)dataRecord["IsQuickPricerEnable"];
            bool isUserQuickPricerEnable = (bool)dataRecord["IsUserQuickPricerEnabled"];
            bool isPricingMultipleAppsSupported = (bool)dataRecord["IsPricingMultipleAppsSupported"];
            E_BrokerBillingVersion billingVersion = (E_BrokerBillingVersion)(short)dataRecord["BillingVersion"];
            E_PmlLoanLevelAccess pmlLevelAccess = (E_PmlLoanLevelAccess)dataRecord["PmlLevelAccess"];
            bool isNewPmlUIEnabled = (bool)dataRecord["IsNewPmlUIEnabled"];
            bool isUsePml2AsQuickPricer = (bool)dataRecord["IsUsePml2AsQuickPricer"];
            E_PortalMode portalMode = (E_PortalMode)((int)dataRecord["PortalMode"]);

            if (portalMode == E_PortalMode.Retail)
            {
                portalMode = E_PortalMode.Blank;
            }

            DateTime passwordExpirationD = DateTime.MaxValue;
            Guid currentLoginSessionId = Guid.Empty;
            bool hasLONIntegration = false;
            Guid lastUsedCreditProtocolId = Guid.Empty;
            Guid lpePriceGroupId = Guid.Empty;
            Guid pmlBrokerId = Guid.Empty;
            Guid miniCorrespondentBranchId = Guid.Empty;
            Guid correspondentBranchId = Guid.Empty;

            string type = (string)dataRecord["Type"];
            if (applicationT == E_ApplicationT.ToBeDefineFromUserType)
            {
                if (type == "B")
                {
                    applicationT = E_ApplicationT.LendersOffice;
                }
                else if (type == "P")
                {
                    applicationT = E_ApplicationT.PriceMyLoan;
                }
            }

            if (dataRecord["PasswordExpirationD"] != DBNull.Value)
            {
                passwordExpirationD = (DateTime)dataRecord["PasswordExpirationD"];
            }
            if (dataRecord["LoginSessionID"] != DBNull.Value)
            {
                currentLoginSessionId = (Guid)dataRecord["LoginSessionID"];
            }
            if (dataRecord["HasLONIntegration"] != DBNull.Value)
            {
                hasLONIntegration = (bool)dataRecord["HasLONIntegration"];
            }
            if (dataRecord["LastUsedCreditProtocolId"] != DBNull.Value)
            {
                lastUsedCreditProtocolId = (Guid)dataRecord["LastUsedCreditProtocolId"];
            }
            if (dataRecord["LpePriceGroupId"] != DBNull.Value)
            {
                lpePriceGroupId = (Guid)dataRecord["LpePriceGroupId"];
            }
            if (dataRecord["PmlBrokerId"] != DBNull.Value)
            {
                pmlBrokerId = (Guid)dataRecord["PmlBrokerId"];
            }

            if (dataRecord["MiniCorrespondentBranchId"] != DBNull.Value)
            {
                miniCorrespondentBranchId = (Guid)dataRecord["MiniCorrespondentBranchId"];
            }

            if (dataRecord["CorrespondentBranchId"] != DBNull.Value)
            {
                correspondentBranchId = (Guid)dataRecord["CorrespondentBranchId"];
            }

            bool optsToUseNewLoanEditorUI = (bool)dataRecord["OptsToUseNewLoanEditorUI"];
            List<string> roles;
            List<string> ipWhiteList;
            HashSet<Guid> teamList;
            bool isLoggedOut;

            Tools.GetAdditionalCredentialForBrokerUserPrincipal(DbConnectionInfo.GetConnectionInfo(brokerId), userId, employeeId, brokerId, cookieSessionId, out roles, out ipWhiteList, out teamList, out isLoggedOut);
            roles.Insert(0, "B");

            if (ConstStage.UseEnhancedSessionManagement && isLoggedOut)
            {
                // OPM 247103. The corresponding sessionId was previously signed out.
                return null;
            }

            bool isAppError = HttpContext.Current != null && HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("apperror.aspx");

            if (!allowDuplicateLogin && expectedLoginSessionId == Guid.Empty)
            {
                currentLoginSessionId = newLoginSessionID;
            }
            else if (!allowDuplicateLogin && !isSharable)
            {
                if (expectedLoginSessionId != currentLoginSessionId && isAppError == false)
                {
                    DuplicationLoginException ex = new DuplicationLoginException(loginName);

                    string host = HttpContext.Current != null ? HttpContext.Current.Request.Url.Host : "";
                    if (ConstStage.LogDuplicateLoginDebugInfo &&
                        host.Equals("edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase))
                    {
                        var msg = "Encountered duplicate login error on EDocs server.";
                        Tools.LogError(msg, ex);
                    }

                    throw ex;
                }
            }

            principal = new BrokerUserPrincipal(DbConnectionInfo.GetConnectionInfo(brokerId), new GenericIdentity(loginName), firstName, lastName,
                brokerId, userId, employeeId, permissions,
                roles.ToArray(), needToAcceptLatestAgreement,
                passwordExpirationD, branchId, currentLoginSessionId,
                brokerName, isOthersAllowedToEditUnderwriterAssignedFile,
                isLOAllowedToEditProcessorAssignedFile, hasLONIntegration,
                selectedPipelineCustomReportId, applicationT, lastUsedCreditProtocolId,
                lastUsedCreditLoginNm, lastUsedCreditAccountId, byPassBgCalcForGfeAsDefault, type, lpePriceGroupId,
                isRateLockedAtSubmission, hasLenderDefaultFeatures,
                isQuickPricerEnable || isUserQuickPricerEnable, isPricingMultipleAppsSupported, billingVersion,
                pmlBrokerId, pmlLevelAccess, ipWhiteList, teamList, isNewPmlUIEnabled, isUsePml2AsQuickPricer, portalMode,
                miniCorrespondentBranchId, correspondentBranchId,
                optsToUseNewLoanEditorUI, cookieSessionId, initialPrincipalType, initialUserId, postLoginTask);

            if (isStoreToCookie)
            {
                if (null != HttpContext.Current && null != HttpContext.Current.Response)
                {
                    RequestHelper.StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                }
            }
            if (updateCurrentPrincipal)
            {
                System.Threading.Thread.CurrentPrincipal = principal;
            }

            if (userId != Guid.Empty && !allowDuplicateLogin && expectedLoginSessionId == Guid.Empty)
            {
                SqlParameter[] param = {
                                           new SqlParameter("@UserID", userId),
                                           new SqlParameter("@LoginSessionID", newLoginSessionID)
                                       };
                StoredProcedureHelper.ExecuteNonQuery(principal.ConnectionInfo, "UpdateLoginSessionID", 2, param);

                if (ConstStage.LogDuplicateLoginDebugInfo)
                {
                    var msg = string.Format("Generating new session id for login {0}. New Id: {1}.", loginName, newLoginSessionID);
                    Tools.LogInfo(msg);
                }
            }

            return principal;
        }

        public static BrokerUserPrincipal CurrentPrincipal
        {
            get
            {
                BrokerUserPrincipal brokerUserPrincipal = Thread.CurrentPrincipal as BrokerUserPrincipal;
                if (brokerUserPrincipal != null)
                {
                    return brokerUserPrincipal;
                }

                InternalUserPrincipal internalUserPrincipal = Thread.CurrentPrincipal as InternalUserPrincipal;
                if (internalUserPrincipal != null)
                {
                    return internalUserPrincipal.BecomeUserPrincipal as BrokerUserPrincipal;
                }

                return null;
            }
        }
    }
}
