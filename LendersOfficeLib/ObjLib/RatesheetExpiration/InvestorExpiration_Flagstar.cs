using System;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.ObjLib.RatesheetExpiration;

namespace LendersOfficeApp.ObjLib.RatesheetExpiration
{
	public class InvestorExpiration_Flagstar : InvestorExpiration
	{
        public InvestorExpiration_Flagstar(string lLpInvestorNm, string productCode, LockPolicy lockPolicy, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
            : base(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data)
		{
			// db - Even if we for some reason remove the normal cutoff time from the database for Flagstar,
			// they will still be expiring their products at 7AM, 9AM, 11AM, and 1PM PST, so they will effectively
			// always have a "cutoff time"
			m_hasInvestorCutoffTime = true;  
		}

		protected override void SetCutoffStatus()
		{
			if(DateTime.Now.CompareTo(m_investorCutoffTime) >= 0)
			{
				m_investorCutoffType = E_InvestorCutoffType.IsOutsideInvestorCutoff;
				return;
			}

			if(m_IsExpirationIssuedByInvestor == true)
			{
				m_investorCutoffType = E_InvestorCutoffType.RatesheetIsNotCurrent;
				return;
			}

			// 6/3/2008 dd - OPM 20142 - Hard code mid day reprice for Flagstar Investor
			// Flagstar reprice at 7AM, 9AM, 11AM, 1PM PST

			DateTime repriceTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0);
			
			for (int i = 0; i < 4; i++)
			{
				if (DateTime.Now.CompareTo(repriceTime) >= 0)
				{
					if (m_latestEffectiveDateTime.CompareTo(repriceTime) < 0)
					{
						m_userMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage; // 7/7/08 - db This field is not used yet
						m_devMessage = "Ratesheet latest effective date is not current. Latest Effective Date=" + m_latestEffectiveDateTime; // 7/7/08 - db This field is not used yet
						m_investorCutoffType = E_InvestorCutoffType.RatesheetIsNotCurrent;
						return;
					} 
					break;
				} 
				else 
				{
					repriceTime = repriceTime.AddHours(-2);
				}
			}

			m_investorCutoffType = E_InvestorCutoffType.IsWithinInvestorCutoff;
		}
	}
}