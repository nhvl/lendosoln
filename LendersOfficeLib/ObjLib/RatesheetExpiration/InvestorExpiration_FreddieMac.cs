﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Admin;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.ObjLib.RatesheetExpiration;

namespace LendersOfficeApp.ObjLib.RatesheetExpiration
{
    /// <summary>
    /// // 9/13/2011 dd - OPM 70543 Need ability to turn off some elements of expiration for Freddie Mac pricing.
    /// </summary>
    public class InvestorExpiration_FreddieMac : InvestorExpiration
    {
        public InvestorExpiration_FreddieMac(string lLpInvestorNm, string productCode, BrokerDB brokerDB, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
            : base(lLpInvestorNm, productCode, brokerDB, lpeAcceptableRsFileId, data)
        {
            m_expireUponNewRatesheetDownload = false; // OPM 70543
        }

        public InvestorExpiration_FreddieMac(string lLpInvestorNm, string productCode, LockPolicy lockPolicy, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
            : base(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data)
		{
            m_expireUponNewRatesheetDownload = false; // OPM 70543
		}

        public InvestorExpiration_FreddieMac(string investorXlsFileName)
            : base(investorXlsFileName)
        {
            m_expireUponNewRatesheetDownload = false; // OPM 70543
        }
    }
}
