﻿namespace LendersOffice.ObjLib.RatesheetExpiration
{
    using System;

    /// <summary>
    /// Holds investor rate lock cut off info.
    /// </summary>
    public class InvestorRateLockCutOffInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvestorRateLockCutOffInfo"/> class.
        /// </summary>
        /// <param name="investorName">The investor name.</param>
        /// <param name="rateLockCutOffTime">The rate lock cut off time if it exists.</param>
        /// <param name="useLenderTimezoneForRsExpiration">Whether the lender timezone should be used.</param>
        public InvestorRateLockCutOffInfo(string investorName, DateTime? rateLockCutOffTime, bool useLenderTimezoneForRsExpiration)
        {
            this.InvestorName = investorName;
            this.RateLockCutOffTime = rateLockCutOffTime;
            this.UseLenderTimezoneForRsExpiration = useLenderTimezoneForRsExpiration;
        }

        /// <summary>
        /// Gets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        public string InvestorName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the rate lock cut off time.
        /// </summary>
        /// <value>The rate lock cut off time.</value>
        public DateTime? RateLockCutOffTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the lender timezone should be used.
        /// </summary>
        /// <value>Whether the lender timezone should be used.</value>
        public bool UseLenderTimezoneForRsExpiration
        {
            get;
            private set;
        }
    }
}
