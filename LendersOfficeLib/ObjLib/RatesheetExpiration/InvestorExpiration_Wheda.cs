﻿using System;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.ObjLib.RatesheetExpiration;

namespace LendersOfficeApp.ObjLib.RatesheetExpiration
{
    // This class is being added for OPM 29001 - it can be expanded in the future to override the other
    // virtual functions in the base class, but for now, all we need to do is indicate that Fannie Mae ratesheets
    // should not be expired immediately upon downloading the new ratesheet.
    public class InvestorExpiration_Wheda : InvestorExpiration
    {
        public InvestorExpiration_Wheda(string lLpInvestorNm, string productCode, BrokerDB brokerDB, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
            : base(lLpInvestorNm, productCode, brokerDB, lpeAcceptableRsFileId, data)
        {
            m_expireUponNewRatesheetDownload = false; // OPM 29001
        }

        public InvestorExpiration_Wheda(string lLpInvestorNm, string productCode, LockPolicy lockPolicy, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
            : base(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data)
        {
            m_expireUponNewRatesheetDownload = false; // OPM 29001
        }

        public InvestorExpiration_Wheda(string investorXlsFileName)
            : base(investorXlsFileName)
        {
            m_expireUponNewRatesheetDownload = false; // OPM 29001
        }
    }
}
