﻿namespace LendersOffice.ObjLib.RatesheetExpiration
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using Admin;
    using Constants;
    using DataAccess;
    using RatePrice.FileBasedPricing;

    /// <summary>
    /// Class to hold data needed to calculate investor expiration data.
    /// </summary>
    public class InvestorExpirationDataLoader
    {
        /// <summary>
        /// Holds a mapping from investor name to rate lock cutoff info.
        /// </summary>
        private Lazy<Dictionary<string, InvestorRateLockCutOffInfo>> lazyInvestorToCutoffInfo;

        /// <summary>
        /// Holds a mapping from RS file id to file info.
        /// </summary>
        private Lazy<Dictionary<string, RateSheetFileInfo>> lazyRsFileIdToFileVersionInfo;

        /// <summary>
        /// Whether we should retrieve the data in one bulk load or go to the DB every time.
        /// </summary>
        private bool shouldBulkLoad = true;

        /// <summary>
        /// The lender that the investor expiration information belong to.
        /// </summary>
        private BrokerDB broker = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvestorExpirationDataLoader"/> class.
        /// </summary>
        /// <param name="broker">The roker.</param>
        /// <param name="investorNamesForRateLockCutOff">The investor names to be used for loading the rate lock cut off info.</param>
        /// <param name="rateSheetFileIdsForFileInfo">The rate sheet file ids to pull file info.</param>
        /// <param name="shouldBulkLoad">Whether we should retrieve the data in one bulk load or go to the DB every time.</param>
        public InvestorExpirationDataLoader(BrokerDB broker, IEnumerable<string> investorNamesForRateLockCutOff, IEnumerable<string> rateSheetFileIdsForFileInfo, bool shouldBulkLoad = true)
        {
            this.broker = broker;
            this.shouldBulkLoad = shouldBulkLoad;
            this.lazyInvestorToCutoffInfo = new Lazy<Dictionary<string, InvestorRateLockCutOffInfo>>(() => this.InitializeRateLockCutOffInfo(investorNamesForRateLockCutOff));
            this.lazyRsFileIdToFileVersionInfo = new Lazy<Dictionary<string, RateSheetFileInfo>>(() => this.InitializeRsFileVersionInfo(rateSheetFileIdsForFileInfo));
        }

        /// <summary>
        /// Gets the investor rate lock cut off info for the given investor.
        /// </summary>
        /// <param name="investorName">The investor name.</param>
        /// <param name="shouldLog">Whether missing entries should be logged.</param>
        /// <returns>The rate lock cut off info if it exists. Null otherwise.</returns>
        public InvestorRateLockCutOffInfo GetRateLockCutOffInfo(string investorName, bool shouldLog)
        {
            InvestorRateLockCutOffInfo info = null;
            if (this.shouldBulkLoad)
            {
                if (!this.lazyInvestorToCutoffInfo.Value.TryGetValue(investorName, out info))
                {
                    info = null;
                }
            }
            else
            {
                info = this.GetRateLockCutOffInfoSingleUse(investorName);
            }

            if (shouldLog && info == null)
            {
                Tools.LogWarning($"GetRateLockCutOffInfo. Info not found. investorName: {investorName}");
            }

            return info;
        }

        /// <summary>
        /// Gets the RS file info.
        /// </summary>
        /// <param name="acceptableRsFileId">The fil id.</param>
        /// <param name="shouldLog">Whether missing entries should be logged.</param>
        /// <returns>The file info if found. Null otherwise.</returns>
        public RateSheetFileInfo GetRsFileVersionInfo(string acceptableRsFileId, bool shouldLog)
        {
            RateSheetFileInfo info = null;
            if (this.shouldBulkLoad && ConstStage.ShouldBulkLoadRateSheetInfoForROExpiration)
            {
                if (!this.lazyRsFileIdToFileVersionInfo.Value.TryGetValue(acceptableRsFileId, out info))
                {
                    info = null;
                }
            }
            else
            {
                info = this.GetRsFileVersionInfoSingleUse(acceptableRsFileId);
            }

            if (shouldLog && info == null)
            {
                Tools.LogWarning($"GetRsFileVersionInfo. Info not found. acceptableRsFileId: {acceptableRsFileId}");
            }

            return info;
        }

        /// <summary>
        /// Gets the rate sheet file info with the given file id.
        /// For single use only. Don't do this in a loop.
        /// </summary>
        /// <param name="acceptableRsFileId">The rate sheet field id.</param>
        /// <returns>The rate sheet file info.</returns>
        private RateSheetFileInfo GetRsFileVersionInfoSingleUse(string acceptableRsFileId)
        {
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "GetLatestRsFileVersion", new SqlParameter("@LpeAcceptableRsFileId", acceptableRsFileId)))
            {
                if (reader.Read())
                {
                    long versionNumber = (long)reader["VersionNumber"];
                    DateTime firstEffectiveDateTime = (DateTime)reader["FirstEffectiveDateTime"];
                    DateTime latestEffectiveDateTime = (DateTime)reader["LatestEffectiveDateTime"];
                    bool isExpriationIssuedByInvestor = (bool)reader["IsExpirationIssuedByInvestor"];

                    return new RateSheetFileInfo(acceptableRsFileId, versionNumber, firstEffectiveDateTime, latestEffectiveDateTime, isExpriationIssuedByInvestor);
                }
            }

            return null;
        }

        /// <summary>
        /// Retunrs the rate lock cutt off info for a single investor. 
        /// Use this only if you're sure you won't need to call this for other investors.
        /// </summary>
        /// <param name="investorName">The investor name.</param>
        /// <returns>The rate lock cut off info.</returns>
        private InvestorRateLockCutOffInfo GetRateLockCutOffInfoSingleUse(string investorName)
        {
            if (this.broker.ActualPricingBrokerId == Guid.Empty)
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveRateLockCutOffTimeByInvestorName", new SqlParameter("@InvestorName", investorName)))
                {
                    if (reader.Read())
                    {
                        bool useLenderTimeZone = (bool)reader["UseLenderTimezoneForRsExpiration"];
                        DateTime? rateLockCutOffTime = null;
                        if (reader["RateLockCutOffTime"] != DBNull.Value)
                        {
                            rateLockCutOffTime = (DateTime)reader["RateLockCutOffTime"];
                        }

                        return new InvestorRateLockCutOffInfo(investorName, rateLockCutOffTime, useLenderTimeZone);
                    }
                }
            }
            else
            {
                var snapshot = this.broker.RetrieveLatestManualImport();

                foreach (var item in snapshot.GetInvestorNameList())
                {
                    if (item.InvestorName.Equals(investorName, StringComparison.OrdinalIgnoreCase))
                    {
                        DateTime? rateLockCutOffTime = null;
                        if (item.RateLockCutOffTime != DateTime.MinValue)
                        {
                            rateLockCutOffTime = item.RateLockCutOffTime;
                        }

                        return new InvestorRateLockCutOffInfo(item.InvestorName, rateLockCutOffTime, item.UseLenderTimezoneForRsExpiration);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the investor rate lock cut off info from the DB.
        /// </summary>
        /// <param name="investorNames">The investor names to use for loading the info.</param>
        /// <returns>A mapping between investor name and the rate lock cut off info.</returns>
        private Dictionary<string, InvestorRateLockCutOffInfo> InitializeRateLockCutOffInfo(IEnumerable<string> investorNames)
        {
            if (this.broker.ActualPricingBrokerId == Guid.Empty)
            {
                return this.InitializeRateLockCutOffInfoFromLpeDatabase(investorNames);
            }
            else
            {
                return this.InitializeRateLockCutOffInfoFromActualPricingBroker(this.broker, investorNames);
            }
        }

        /// <summary>
        /// Gets the investor rate lock cut off info from the DB.
        /// </summary>
        /// <param name="investorNames">The investor names to use for loading the info.</param>
        /// <returns>A mapping between investor name and the rate lock cut off info.</returns>
        private Dictionary<string, InvestorRateLockCutOffInfo> InitializeRateLockCutOffInfoFromLpeDatabase(IEnumerable<string> investorNames)
        {
            string investorNameXmlString = null;
            if (investorNames != null && investorNames.Any())
            {
                XElement rootElement = new XElement("root");
                foreach (string name in investorNames.Distinct())
                {
                    XElement investorElement = new XElement("investor", new XAttribute("name", name));
                    rootElement.Add(investorElement);
                }

                investorNameXmlString = rootElement.ToString(SaveOptions.DisableFormatting);
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@InvestorNames", investorNameXmlString)
            };

            Dictionary<string, InvestorRateLockCutOffInfo> mapping = new Dictionary<string, InvestorRateLockCutOffInfo>(StringComparer.OrdinalIgnoreCase);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveRateLockCutOffTimeForAllInvestors", parameters))
            {
                while (reader.Read())
                {
                    string investorName = (string)reader["InvestorName"];
                    if (!mapping.ContainsKey(investorName))
                    {
                        bool useLendertimeZone = (bool)reader["UseLenderTimezoneForRsExpiration"];
                        DateTime? rateLockCutOffTime = null;
                        if (reader["RateLockCutOffTime"] != DBNull.Value)
                        {
                            rateLockCutOffTime = (DateTime)reader["RateLockCutOffTime"];
                        }

                        mapping.Add(investorName, new InvestorRateLockCutOffInfo(investorName, rateLockCutOffTime, useLendertimeZone));
                    }
                }
            }

            return mapping;
        }

        /// <summary>
        /// Gets the investor rate lock cut off info from the DB.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <param name="investorNames">The investor names to use for loading the info.</param>
        /// <returns>A mapping between investor name and the rate lock cut off info.</returns>
        private Dictionary<string, InvestorRateLockCutOffInfo> InitializeRateLockCutOffInfoFromActualPricingBroker(BrokerDB broker, IEnumerable<string> investorNames)
        {
            HashSet<string> investorNameSet = null;
            if (investorNames != null && investorNames.Any())
            {
                investorNameSet = new HashSet<string>(investorNames, StringComparer.OrdinalIgnoreCase);
            }

            Dictionary<string, InvestorRateLockCutOffInfo> mapping = new Dictionary<string, InvestorRateLockCutOffInfo>(StringComparer.OrdinalIgnoreCase);

            var snapshot = broker.RetrieveLatestManualImport();
            foreach (var item in snapshot.GetInvestorNameList())
            {
                if (investorNameSet != null)
                {
                    if (!investorNameSet.Contains(item.InvestorName))
                    {
                        continue; // Skip to next investor name.
                    }
                }

                if (!mapping.ContainsKey(item.InvestorName))
                {
                    DateTime? rateLockCutOffTime = null;
                    if (item.RateLockCutOffTime != DateTime.MinValue)
                    {
                        rateLockCutOffTime = item.RateLockCutOffTime;
                    }

                    mapping.Add(item.InvestorName, new InvestorRateLockCutOffInfo(item.InvestorName, rateLockCutOffTime, item.UseLenderTimezoneForRsExpiration));
                }
            }

            return mapping;
        }

        /// <summary>
        /// Gets rate sheet file info and version number from the DB.
        /// </summary>
        /// <param name="acceptableRsFileIds">The file ids to search for.</param>
        /// <returns>A mapping from rate sheet file id to file info.</returns>
        private Dictionary<string, RateSheetFileInfo> InitializeRsFileVersionInfo(IEnumerable<string> acceptableRsFileIds)
        {
            string acceptableRsFileIdsXml = null;
            if (acceptableRsFileIds != null && acceptableRsFileIds.Any())
            {
                XElement rootElement = new XElement("root");
                foreach (string id in acceptableRsFileIds.Distinct())
                {
                    XElement investorElement = new XElement("file", new XAttribute("fileId", id));
                    rootElement.Add(investorElement);
                }

                acceptableRsFileIdsXml = rootElement.ToString(SaveOptions.DisableFormatting);
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@RsFileIdsXml", acceptableRsFileIdsXml)
            };

            Dictionary<string, RateSheetFileInfo> mapping = new Dictionary<string, RateSheetFileInfo>(StringComparer.OrdinalIgnoreCase);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "GetLatestRsFileVersionsById", parameters))
            {
                while (reader.Read())
                {
                    string acceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                    long versionNumber = (long)reader["VersionNumber"];
                    DateTime firstEffectiveDateTime = (DateTime)reader["FirstEffectiveDateTime"];
                    DateTime latestEffectiveDateTime = (DateTime)reader["LatestEffectiveDateTime"];
                    bool isExpirationIssuedByInvestor = (bool)reader["IsExpirationIssuedByInvestor"];

                    mapping.Add(acceptableRsFileId, new RateSheetFileInfo(acceptableRsFileId, versionNumber, firstEffectiveDateTime, latestEffectiveDateTime, isExpirationIssuedByInvestor));
                }
            }

            return mapping;
        }
    }
}
