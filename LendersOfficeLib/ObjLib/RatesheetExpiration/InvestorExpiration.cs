using System;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.ObjLib.LockPolicies;
using System.Collections.Generic;
using LendersOffice.ObjLib.RatesheetExpiration;

namespace LendersOfficeApp.ObjLib.RatesheetExpiration
{
	public enum E_InvestorCutoffType
	{
		IsOutsideInvestorCutoff = 0,
		IsWithinInvestorCutoff = 1,
		RatesheetIsNotCurrent = 2,
		IsWithinInvestorOvernightProtection = 3
	}
	
	public class InvestorExpirationFactory
	{
        public static InvestorExpiration GetInvestorExpirationObj(string lLpInvestorNm, string productCode, LockPolicy lockPolicy, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
        {
            InvestorExpiration invExp;

            string lLpInvestorNmLower = lLpInvestorNm.ToLower();
            if (lLpInvestorNmLower.Equals("fannie mae"))
            {
                invExp = new InvestorExpiration_FannieMae(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data);
            }
            else if (lLpInvestorNmLower.Equals("freddie mac"))
            {
                // 9/13/2011 dd - OPM 70543
                invExp = new InvestorExpiration_FreddieMac(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data);
            }
            else if (lLpInvestorNmLower.Equals("wheda backend"))
            {
                invExp = new InvestorExpiration_Wheda(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data);
            }
            else
            {
                invExp = new InvestorExpiration(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, data);
            }

            return invExp;
        }

        public static bool ExpireUponNewRatesheetDownload(string investorXlsFileName)
        {
            InvestorExpiration invExp;

            string investorXlsFileNameLower = investorXlsFileName.ToLower();

            if (investorXlsFileNameLower.StartsWith("fnma_")) // OPM 29001 - all fannie mae ratesheets will start with "fnma_"
            {
                invExp = new InvestorExpiration_FannieMae(investorXlsFileName);
            }
            else if (investorXlsFileNameLower.StartsWith("fhlmc_be_1") ||
                investorXlsFileNameLower.StartsWith("fhlmc_be_2") ||
                investorXlsFileNameLower.StartsWith("fhlmc_be_3") ||
                investorXlsFileNameLower.StartsWith("fhlmc_m_1") ||
                investorXlsFileNameLower.StartsWith("fhlmc_m_2") ||
                investorXlsFileNameLower.StartsWith("fhlmc_m_3")
                )
            {
                // 9/13/2011 dd - OPM 70543
                invExp = new InvestorExpiration_FreddieMac(investorXlsFileName);
            }
            else if (investorXlsFileNameLower.StartsWith("pml0214_custom_be") ||
                      investorXlsFileNameLower.StartsWith("pml0214_custom_md"))
            {
                invExp = new InvestorExpiration_Wheda(investorXlsFileName);
            }
            else
            {
                invExp = new InvestorExpiration(investorXlsFileName);
            }

            return invExp.ExpireUponNewRatesheetDownload;
        }
	}
	
	public class InvestorExpiration
	{
		#region Variables
		protected string				m_lLpInvestorNm = "";
		protected string				m_productCode = "";
        protected BrokerDB m_brokerDB { get { return BrokerDB.RetrieveById(m_lockPolicy.BrokerId); } }
		protected bool					m_hasInvestorCutoffTime = false;
		protected DateTime				m_investorCutoffTime;
		protected string				m_lpeAcceptableRsFileId = "";
		protected bool					m_latestEffectiveInfoSet = false;
		protected long					m_latestVersionNumber = -1;
		protected DateTime				m_latestEffectiveDateTime = SmallDateTime.MinValue;
		protected string				m_userMessage = "";
		protected string				m_devMessage = "";
		protected E_InvestorCutoffType	m_investorCutoffType = E_InvestorCutoffType.IsWithinInvestorCutoff;
		protected bool					m_IsExpirationIssuedByInvestor = false;
        protected bool                  m_expireUponNewRatesheetDownload = true;
        protected LockPolicy            m_lockPolicy = null;
        protected InvestorExpirationDataLoader expirationData = null;

        public string InvestorName
		{
			get { return m_lLpInvestorNm; }
		}

        public bool ExpireUponNewRatesheetDownload
        {
            get { return m_expireUponNewRatesheetDownload; }
        }

		// Will implement these later for overnight protection
		/*public string UserMessage
		{
			get { return m_userMessage; }
		}

		public string DevMessage
		{
			get { return m_devMessage; }
		}*/
		
		public string ProductCode
		{
			get { return m_productCode; }
		}

		public string AcceptableRsFileId
		{
			get { return m_lpeAcceptableRsFileId; }
		}

		public DateTime InvestorCutoffTime
		{
			get { return m_investorCutoffTime; }
		}

		public bool HasInvestorCutoffTime
		{
			get { return m_hasInvestorCutoffTime; }
		}

		public DateTime LatestEffectiveDateTime
		{
			get 
			{
				if(m_latestEffectiveInfoSet == false)
					SetEffectiveInfo();
			
				return m_latestEffectiveDateTime;
			}
		}
	
		public long LatestVersionNumber
		{
			get
			{
				if(m_latestEffectiveInfoSet == false)
					SetEffectiveInfo();
			
				return m_latestVersionNumber;
			}
		}

		public E_InvestorCutoffType InvestorCutoffType
		{
			get { return m_investorCutoffType; }
		}

		#endregion

        public InvestorExpiration(string lLpInvestorNm, string productCode, BrokerDB brokerDB, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
        {
            m_lLpInvestorNm = lLpInvestorNm;
            m_productCode = productCode;
            m_lpeAcceptableRsFileId = lpeAcceptableRsFileId;
            m_lockPolicy = LockPolicy.Retrieve(brokerDB.BrokerID, brokerDB.DefaultLockPolicyID.Value);
            this.expirationData = data;
            Initialize();
        }

        public InvestorExpiration(string lLpInvestorNm, string productCode, LockPolicy lockPolicy, string lpeAcceptableRsFileId, InvestorExpirationDataLoader data)
        {
            m_lLpInvestorNm = lLpInvestorNm;
            m_productCode = productCode;
            m_lockPolicy = lockPolicy;
            m_lpeAcceptableRsFileId = lpeAcceptableRsFileId;
            this.expirationData = data;
            Initialize();
        }

        public InvestorExpiration(string investorXlsFileName)
        {
            // NO-OP as of 4/15/09
        }

		protected void SetEffectiveInfo()
		{
            RateSheetFileInfo fileInfo = this.expirationData.GetRsFileVersionInfo(this.m_lpeAcceptableRsFileId, shouldLog: true);
            if (fileInfo != null)
            {
                m_latestEffectiveDateTime = fileInfo.LatestEffectiveDateTime;
                m_latestVersionNumber = fileInfo.VersionNumber;
                m_IsExpirationIssuedByInvestor = fileInfo.IsExpirationIssuedByInvestor;
            }
            
			m_latestEffectiveInfoSet = true;
		}

		protected virtual void SetCutoffStatus()
		{
			if (DateTime.Now.CompareTo(m_investorCutoffTime) >= 0) // if we've passed the cutoff time
			{
				m_investorCutoffType = E_InvestorCutoffType.IsOutsideInvestorCutoff;
			}
			else if(m_IsExpirationIssuedByInvestor == true)
			{
				m_investorCutoffType = E_InvestorCutoffType.RatesheetIsNotCurrent;
			}
			else
			{
				m_investorCutoffType = E_InvestorCutoffType.IsWithinInvestorCutoff;
			}
		}

        protected void Initialize()
        {
            bool useLenderTimeZoneForRsExpiration = false;

            if (m_lockPolicy.BrokerId == new Guid("06A4B9D8-263E-4E04-AA88-D7E1BD25852B")) // OPM 110031: hard code PML0034 to ignore investor cutoff time
            {
                // Pretend that the investor cutoff time is at the end of the day
                m_investorCutoffTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
                m_hasInvestorCutoffTime = true;
            }
            else
            {
                InvestorRateLockCutOffInfo cutOffInfo = this.expirationData.GetRateLockCutOffInfo(this.m_lLpInvestorNm, shouldLog: true);
                if (cutOffInfo != null && cutOffInfo.RateLockCutOffTime.HasValue)
                {
                    useLenderTimeZoneForRsExpiration = cutOffInfo.UseLenderTimezoneForRsExpiration;
                    m_investorCutoffTime = cutOffInfo.RateLockCutOffTime.Value;
                    m_investorCutoffTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, m_investorCutoffTime.Hour, m_investorCutoffTime.Minute, m_investorCutoffTime.Second);

                    // Apply lender timezone to cutoff time
                    if (useLenderTimeZoneForRsExpiration)
                    {
                        switch (m_lockPolicy.TimezoneForRsExpiration)
                        {
                            case "PST":
                                // NO-OP
                                break;
                            case "MST":
                                if (m_brokerDB.Address.State == "AZ")
                                {
                                    // 5/5/2008 dd - Arizona does not observe daylight saving time.
                                    if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now))
                                    {
                                        // 5/5/2008 dd - When we observe daylight saving time, California and Arizona are on the same hour.
                                        break;
                                    }
                                }
                                m_investorCutoffTime = m_investorCutoffTime.AddHours(-1); // Convert to PST
                                break;
                            case "CST":
                                m_investorCutoffTime = m_investorCutoffTime.AddHours(-2); // Convert to PST
                                break;
                            case "EST":
                                m_investorCutoffTime = m_investorCutoffTime.AddHours(-3); // Convert to PST
                                break;
                            default:
                                Tools.LogErrorWithCriticalTracking("Unhandled TimeZoneForRsExpiration=" + m_lockPolicy.TimezoneForRsExpiration + ". Default to PST");
                                break;
                        }
                    }

                    // "Hasten cutoff time" option
                    m_investorCutoffTime = m_investorCutoffTime.AddMinutes(-1 * m_lockPolicy.LpeMinutesNeededToLockLoan);

                    m_hasInvestorCutoffTime = true;
                }
            }
            SetEffectiveInfo();
            SetCutoffStatus();
        }
	}
}