﻿namespace LendersOffice.ObjLib.RatesheetExpiration
{
    using System;

    /// <summary>
    /// The rate sheet file info.
    /// </summary>
    public class RateSheetFileInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateSheetFileInfo"/> class.
        /// </summary>
        /// <param name="rateSheetFileId">The rate sheet file id.</param>
        /// <param name="versionNum">The file version number.</param>
        /// <param name="firstEffectiveDateTime">The first effective date.</param>
        /// <param name="latestEffectiveDateTime">The latest effective date.</param>
        /// <param name="isExpirationIssuedByInvestor">Whether the expiration was issued by the investor.</param>
        public RateSheetFileInfo(string rateSheetFileId, long versionNum, DateTime firstEffectiveDateTime, DateTime latestEffectiveDateTime, bool isExpirationIssuedByInvestor)
        {
            this.AcceptableRsFileId = rateSheetFileId;
            this.VersionNumber = versionNum;
            this.FirstEffectiveDateTime = firstEffectiveDateTime;
            this.LatestEffectiveDateTime = latestEffectiveDateTime;
            this.IsExpirationIssuedByInvestor = isExpirationIssuedByInvestor;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RateSheetFileInfo"/> class.
        /// </summary>
        /// <param name="rateSheetFileId">The rate sheet file id.</param>
        /// <param name="deploymentType">The deployement type.</param>
        /// <param name="mapAndBotWorking">Whether the rs map and bot are both working.</param>
        /// <param name="investorXlsFileId">The investor xls file id.</param>
        public RateSheetFileInfo(string rateSheetFileId, string deploymentType, bool mapAndBotWorking, long investorXlsFileId)
        {
            this.AcceptableRsFileId = rateSheetFileId;
            this.DeploymentType = deploymentType;
            this.IsBothRsMapAndBotWorking = mapAndBotWorking;
            this.InvestorXlsFileId = investorXlsFileId;
        }

        /// <summary>
        /// Gets the acceptable rate sheet file id.
        /// </summary>
        /// <value>The acceptable rate sheet file id.</value>
        public string AcceptableRsFileId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the first effective date.
        /// </summary>
        /// <value>The first effective date.</value>
        public DateTime FirstEffectiveDateTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the latest effective date.
        /// </summary>
        /// <value>The latest effective date.</value>
        public DateTime LatestEffectiveDateTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the file version number.
        /// </summary>
        /// <value>The version number.</value>
        public long VersionNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the expiration was issued by the investor.
        /// </summary>
        /// <value>Whether the expiration was issued by the investor.</value>
        public bool IsExpirationIssuedByInvestor
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the deployment type.
        /// </summary>
        /// <value>The deployment type.</value>
        public string DeploymentType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the rs map and bot are working.
        /// </summary>
        /// <value>Whether the rs map and bot are working.</value>
        public bool IsBothRsMapAndBotWorking
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the investor xls file id.
        /// </summary>
        /// <value>The investor xls file id.</value>
        public long InvestorXlsFileId
        {
            get;
            private set;
        }
    }
}
