﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Custom favorite folder filter.
    /// </summary>
    public class CustomFavoriteFolderFilter
    {
        /// <summary>
        /// Gets or sets employee id.
        /// </summary>
        public Guid? EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets lead loan status.
        /// </summary>
        public LeadLoanStatusT LeadLoanStatus { get; set; }

        /// <summary>
        /// Gets or sets role ids.
        /// </summary>
        public IEnumerable<Guid> RoleIds { get; set; }

        /// <summary>
        /// Gets or sets group ids.
        /// </summary>
        public IEnumerable<Guid> GroupIds { get; set; }

        /// <summary>
        /// Gets or sets loan purposes.
        /// </summary>
        public IEnumerable<E_sLPurposeT> LoanPurposes { get; set; }

        /// <summary>
        /// Gets or sets loan types.
        /// </summary>
        public IEnumerable<E_sLT> LoanTypes { get; set; }
    }
}
