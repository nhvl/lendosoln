﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System.Collections.Generic;

    /// <summary>
    /// Custom favorite folder data object.
    /// </summary>
    public class CustomFavoriteFolderData
    {
        /// <summary>
        /// Gets or sets custom favorite folder nodes.
        /// </summary>
        public IEnumerable<CustomFavoriteFolderNode> Nodes { get; set; }

        /// <summary>
        /// Gets or sets parent folder ids.
        /// </summary>
        public HashSet<int> ParentFolderIds { get; set; }
    }
}
