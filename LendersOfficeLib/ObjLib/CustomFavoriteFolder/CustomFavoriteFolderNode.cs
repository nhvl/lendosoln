﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Common;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// Custom favorite folder node.
    /// </summary>
    public class CustomFavoriteFolderNode : CustomFavoriteNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoriteFolderNode" /> class.
        /// </summary>
        public CustomFavoriteFolderNode()
        {
            this.IsVisible = true; // folder is visible unless explicitly set to false
            this.Children = new List<CustomFavoriteNode>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoriteFolderNode" /> class.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="reader">IDataReader object.</param>
        public CustomFavoriteFolderNode(Guid brokerId, IDataReader reader)
        {
            this.IsVisible = true; // folder is visible unless explicitly set to false
            this.Children = new List<CustomFavoriteNode>();
            this.Folder = new CustomFavoriteFolder(brokerId, reader);
            this.SortOrder = Convert.ToInt32(reader["SortOrder"]);
        }

        /// <summary>
        /// Gets or sets custom favorite folder.
        /// </summary>
        public CustomFavoriteFolder Folder { get; set; }

        /// <summary>
        /// Gets or sets the list of children nodes.
        /// </summary>
        public List<CustomFavoriteNode> Children { get; set; }
    }
}
