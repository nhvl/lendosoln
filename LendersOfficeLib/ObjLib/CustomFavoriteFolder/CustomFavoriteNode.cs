﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    /// <summary>
    /// Custom favorite node object.
    /// </summary>
    public class CustomFavoriteNode
    {        
        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the folder node is visible.
        /// </summary>
        public bool IsVisible { get; set; }
    }
}
