﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Custom favorite folder.
    /// </summary>
    public class CustomFavoriteFolder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoriteFolder" /> class.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="reader">IDataReader object.</param>
        public CustomFavoriteFolder(Guid brokerId, IDataReader reader)
        {
            this.FolderId = (int)reader["FolderId"];
            this.EmployeeId = reader["EmployeeId"] as Guid?;
            this.InternalName = (string)reader["InternalName"];
            this.ExternalName = (string)reader["ExternalName"];
            this.LeadLoanStatus = (LeadLoanStatusT)Convert.ToInt32(reader["LeadLoanStatus"]);
            this.ParentFolderId = reader["ParentFolderId"] as int?;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoriteFolder" /> class.  This serves as the default Folder data for the root elements.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="employeeId">Employee id.</param>
        public CustomFavoriteFolder(Guid brokerId, Guid? employeeId)
        {
            this.InternalName = "Custom Folder";
            this.ExternalName = "Custom Folder";
            this.LeadLoanStatus = LeadLoanStatusT.DC;
            this.ParentFolderId = null;
            this.FolderId = -1;
            this.BrokerId = brokerId;
            this.EmployeeId = employeeId;
            this.RoleIds = new List<Guid>();
            this.GroupIds = new List<Guid>();
            this.LoanPurposes = new List<E_sLPurposeT>();
            this.LoanTypes = new List<E_sLT>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoriteFolder" /> class.  This required for serialization.
        /// </summary>
        public CustomFavoriteFolder()
        {
            this.InternalName = "Custom Folder";
            this.ExternalName = "Custom Folder";
            this.LeadLoanStatus = LeadLoanStatusT.DC;
            this.ParentFolderId = null;
            this.FolderId = -1;
            this.RoleIds = new List<Guid>();
            this.GroupIds = new List<Guid>();
            this.LoanPurposes = new List<E_sLPurposeT>();
            this.LoanTypes = new List<E_sLT>();
        }

        /// <summary>
        /// Gets or sets the folder id.
        /// </summary>
        public int FolderId { get; set; }

        /// <summary>
        /// Gets or sets the broker id.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        public Guid? EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the internal name.
        /// </summary>
        public string InternalName { get; set; }

        /// <summary>
        /// Gets or sets the external name.
        /// </summary>
        public string ExternalName { get; set; }

        /// <summary>
        /// Gets or sets the lead loan status of the folder.
        /// </summary>
        public LeadLoanStatusT LeadLoanStatus { get; set; }

        /// <summary>
        /// Gets or sets the parent fodler id.
        /// </summary>
        public int? ParentFolderId { get; set; }

        /// <summary>
        /// Gets or sets the list of role ids.
        /// </summary>
        public List<Guid> RoleIds { get; set; }

        /// <summary>
        /// Gets or sets the list of group ids.
        /// </summary>
        public List<Guid> GroupIds { get; set; }

        /// <summary>
        /// Gets or sets the list of loan purposes.
        /// </summary>
        public List<E_sLPurposeT> LoanPurposes { get; set; }

        /// <summary>
        /// Gets or sets the list of loan types.
        /// </summary>
        public List<E_sLT> LoanTypes { get; set; }
    }
}
