﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using DynaTree;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Custom Favorite Folder Helper class.
    /// </summary>
    public class CustomFavoriteFolderHelper
    {
        /// <summary>
        /// Gets the list of pages that should be treated like the other.  I.E.  If one page gets favorited, the other should be favorited as well, and vice-versa.
        /// </summary>
        public static Dictionary<string, List<string>> AssociatedPages
        {
            get
            {
                return new Dictionary<string, List<string>>()
                {
                    { "Page_StatusConditions", new List<string>() { "Page_NewStatusConditions", "Page_UnderwritingConditions", "Page_UnderwritingConditionsForTask" } },
                    { "Page_SellerClosingCosts", new List<string>() { "Page_SellerClosingDisclosure" } },
                };
            }
        }

        /// <summary>
        /// Retrieves all custom folders for a broker. Can specifiy to get ones specific to employee as well.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="employeeId">Employee id.</param>
        /// <param name="isView">Boolean that determines whether you're viewing in the loan editor or potentially updating in the editor page.</param>
        /// <returns>Custom favorite folder data object.</returns>
        public static CustomFavoriteFolderData RetrieveAllCustomFavoriteFoldersByBrokerId(Guid brokerId, Guid? employeeId, bool isView)
        {
            HashSet<int> parentFolderIds = new HashSet<int>();
            var nodes = new List<CustomFavoriteFolderNode>();

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            if (employeeId.HasValue)
            {
                parameters.Add(new SqlParameter("@EmployeeId", employeeId.Value));
            }

            StoredProcedureName spName;

            if (isView)
            {
                spName = StoredProcedureName.Create("CUSTOM_FOLDER_RETRIEVE_BY_BROKERID_AND_EMPLOYEEID").Value;
            }
            else if (employeeId.HasValue)
            {
                spName = StoredProcedureName.Create("CUSTOM_FOLDER_RETRIEVE_BY_EMPLOYEEID").Value;
            }
            else
            {
                spName = StoredProcedureName.Create("CUSTOM_FOLDER_RETRIEVE_BY_BROKERID").Value;
            }

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                if (conn != null && conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, spName, parameters, TimeoutInSeconds.Default))
                        {
                            while (reader.Read())
                            {
                                nodes.Add(new CustomFavoriteFolderNode(brokerId, reader));
                            }
                        }

                        foreach (CustomFavoriteFolderNode node in nodes)
                        {
                            var roleIds = new List<Guid>();
                            var groupIds = new List<Guid>();
                            var loanTypes = new List<E_sLT>();
                            var loanPurposes = new List<E_sLPurposeT>();
                            var customFavoritePages = new List<CustomFavoriteNode>();

                            SqlParameter[] parameter =
                            {
                                new SqlParameter("@FolderId", node.Folder.FolderId)
                            };

                            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_ROLE_RETRIEVE_BY_FOLDERID").Value, parameter, TimeoutInSeconds.Default))
                            {
                                while (reader.Read())
                                {
                                    roleIds.Add((Guid)reader["RoleId"]);
                                }
                            }

                            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_EMPLOYEE_GROUP_RETRIEVE_BY_FOLDERID").Value, parameter, TimeoutInSeconds.Default))
                            {
                                while (reader.Read())
                                {
                                    groupIds.Add((Guid)reader["GroupId"]);
                                }
                            }

                            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_LOAN_PURPOSE_RETRIEVE_BY_FOLDERID").Value, parameter, TimeoutInSeconds.Default))
                            {
                                while (reader.Read())
                                {
                                    loanPurposes.Add((E_sLPurposeT)Convert.ToInt32(reader["LoanPurpose"]));
                                }
                            }

                            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_LOAN_TYPE_RETRIEVE_BY_FOLDERID").Value, parameter, TimeoutInSeconds.Default))
                            {
                                while (reader.Read())
                                {
                                    loanTypes.Add((E_sLT)Convert.ToInt32(reader["LoanType"]));
                                }
                            }

                            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_PAGE_RETRIEVE_BY_FOLDERID").Value, parameter, TimeoutInSeconds.Default))
                            {
                                while (reader.Read())
                                {
                                    customFavoritePages.Add(new CustomFavoritePageNode((string)reader["PageId"], Convert.ToInt32(reader["SortOrder"])));
                                }
                            }

                            node.Folder.RoleIds = roleIds;
                            node.Folder.GroupIds = groupIds;
                            node.Folder.LoanPurposes = loanPurposes;
                            node.Folder.LoanTypes = loanTypes;
                            node.Children = customFavoritePages.OrderBy(childNode => childNode.SortOrder).ToList();

                            if (node.Folder.ParentFolderId.HasValue && !parentFolderIds.Contains(node.Folder.ParentFolderId.Value))
                            {
                                parentFolderIds.Add(node.Folder.ParentFolderId.Value);
                            }
                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }

            var customFavoriteFolderData = new CustomFavoriteFolderData()
            {
                Nodes = nodes,
                ParentFolderIds = parentFolderIds
            };

            return customFavoriteFolderData;
        }

        /// <summary>
        /// Retrieves the entire custom favorite folder tree for a specific broker and employee id setting.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="employeeId">Employee id.</param>
        /// <param name="isView">Boolean that determines whether you're viewing in the loan editor or potentially updating in the editor page.</param>
        /// <returns>Root node containing the tree.</returns>
        public static List<CustomFavoriteNode> RetrieveCustomFavoriteFolderTreeByBrokerId(Guid brokerId, Guid? employeeId, bool isView)
        {
            var folderData = RetrieveAllCustomFavoriteFoldersByBrokerId(brokerId, employeeId, isView);
            var children = new List<CustomFavoriteNode>();

            foreach (int parentFolderId in folderData.ParentFolderIds)
            {
                var parentFolder = folderData.Nodes.FirstOrDefault(n => n.Folder.FolderId == parentFolderId);

                if (parentFolder != null)
                {
                    var childFolders = folderData.Nodes.Where(n => n.Folder.ParentFolderId == parentFolderId).OrderBy(childNode => childNode.SortOrder);
                    foreach (var node in childFolders)
                    {
                        parentFolder.Children.Add(node);
                    }
                }
            }

            foreach (var node in folderData.Nodes.Where(n => !n.Folder.ParentFolderId.HasValue))
            {
                children.Add(node);
            }

            return children;
        }

        /// <summary>
        /// Saves the custom favorite folder tree by traversing it from the root node.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="employeeId">Employee Id.</param>
        /// <param name="rootNode">Root node.</param>
        public static void SaveCustomFavoriteFolderTree(Guid brokerId, Guid? employeeId, CustomFavoriteFolderNode rootNode)
        {
            CustomFolderTableTypes folderTables = new CustomFolderTableTypes();

            var folderNodes = GetAllFolderNodes(rootNode, new List<CustomFavoriteFolderNode>());

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                if (conn != null && conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (CustomFavoriteFolderNode node in folderNodes)
                        {
                            if (node.Folder == null)
                            {
                                continue;
                            }

                            // -1, or any other value to denote that the folder is new.
                            if (node.Folder.FolderId == -1)
                            {
                                SqlParameter[] customFolderParameters =
                                {
                                    new SqlParameter("@BrokerId", brokerId),
                                    new SqlParameter("@EmployeeId", employeeId),
                                    new SqlParameter("@InternalName", node.Folder.InternalName),
                                    new SqlParameter("@ExternalName", node.Folder.ExternalName),
                                    new SqlParameter("@LeadLoanStatus", node.Folder.LeadLoanStatus),
                                    new SqlParameter("@SortOrder", node.SortOrder),
                                    new SqlParameter("@ParentFolderId", node.Folder.ParentFolderId),
                                };

                                node.Folder.FolderId = (int)StoredProcedureDriverHelper.ExecuteScalar(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_CREATE").Value, customFolderParameters, TimeoutInSeconds.Default);
                                foreach (var childNode in node.Children)
                                {
                                    if (childNode is CustomFavoriteFolderNode)
                                    {
                                        var folderNode = childNode as CustomFavoriteFolderNode;
                                        folderNode.Folder.ParentFolderId = node.Folder.FolderId;
                                    }
                                }
                            }
                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }

            foreach (var node in folderNodes)
            {
                if (node.Folder == null)
                {
                    continue;
                }

                BuildCustomFavoriteFolderParameters(node, folderTables);

                foreach (CustomFavoriteNode childNode in node.Children)
                {
                    if (childNode is CustomFavoritePageNode)
                    {
                        BuildCustomFavoritePageParameters(node.Folder.FolderId, (CustomFavoritePageNode)childNode, folderTables);
                    }
                }
            }

            var readSpName = StoredProcedureName.Create(employeeId.HasValue ? "CUSTOM_FOLDER_RETRIEVE_BY_EMPLOYEEID" : "CUSTOM_FOLDER_RETRIEVE_BY_BROKERID").Value;
            var readParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            if (employeeId.HasValue)
            {
                readParameters.Add(new SqlParameter("@EmployeeId", employeeId));
            }

            var folderSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolder", folderTables.FolderTable)
            };

            var roleSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolderXRole", folderTables.RoleTable)
            };

            var employeeGroupSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolderXEmployeeGroup", folderTables.EmployeeGroupTable)
            };

            var loanPurposeSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolderXLoanPurpose", folderTables.LoanPurposeTable)
            };

            var loanTypeSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolderXLoanType", folderTables.LoanTypeTable)
            };

            var pageSaveParameters = new List<SqlParameter>()
            {
                new SqlParameter("@CustomFolderXPage", folderTables.PageTable)
            };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                if (conn != null && conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        var folderIds = new List<int>();

                        using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, trans, readSpName, readParameters, TimeoutInSeconds.Default))
                        {
                            while (reader.Read())
                            {
                                folderIds.Add((int)reader["FolderId"]);
                            }
                        }

                        var intTableDictionary = new Dictionary<string, Type>()
                        {
                            { "IntValue", typeof(int) },
                        };

                        var folderIdTable = DbAccessUtils.InitializeTableValueDataTable("IntTableType", intTableDictionary);
                        foreach (var folderId in folderIds)
                        {
                            folderIdTable.Rows.Add(folderId);
                        }

                        roleSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));
                        employeeGroupSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));
                        loanTypeSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));
                        loanPurposeSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));
                        pageSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));
                        folderSaveParameters.Add(new SqlParameter("@FolderIds", folderIdTable));

                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_ROLE_SAVE").Value, roleSaveParameters, TimeoutInSeconds.Default);
                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_EMPLOYEE_GROUP_SAVE").Value, employeeGroupSaveParameters, TimeoutInSeconds.Default);
                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_LOAN_TYPE_SAVE").Value, loanTypeSaveParameters, TimeoutInSeconds.Default);
                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_LOAN_PURPOSE_SAVE").Value, loanPurposeSaveParameters, TimeoutInSeconds.Default);
                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_X_PAGE_SAVE").Value, pageSaveParameters, TimeoutInSeconds.Default);
                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("CUSTOM_FOLDER_UPDATE_OR_DELETE").Value, folderSaveParameters, TimeoutInSeconds.Default);

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Updates a single folder's page.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="folderId">Folder id.</param>
        /// <param name="pages">The entire list of pages for that folder containing the changes.</param>
        public static void UpdatePagesInAFolder(Guid brokerId, int folderId, List<CustomFavoritePageNode> pages)
        {
            var pageTable = new CustomFolderTableTypes().PageTable;

            foreach (var page in pages)
            {
                pageTable.Rows.Add(folderId, page.PageId, page.SortOrder);
            }

            var intTableDictionary = new Dictionary<string, Type>()
            {
                { "IntValue", typeof(int) },
            };

            var folderIdTable = DbAccessUtils.InitializeTableValueDataTable("IntTableType", intTableDictionary);

            folderIdTable.Rows.Add(folderId);

            SqlParameter[] parameters =
            {
                new SqlParameter("@CustomFolderXPage", pageTable),
                new SqlParameter("@FolderIds", folderIdTable)
            };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("CUSTOM_FOLDER_X_PAGE_SAVE").Value, parameters, TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Retrieves the user's favorite folder, and updates it with the given pages.  If the favorite folder does not exist, it will create it.
        /// </summary>
        /// <param name="folderId">The id of the folder that you're attempting to find.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="employeeId">The user's employee id.</param>
        /// <returns>The favorite folder.</returns>
        public static CustomFavoriteFolderNode RetrieveUserFavoriteFolder(int folderId, Guid brokerId, Guid employeeId)
        {
            var userFolder = CustomFavoriteFolderHelper.RetrieveCustomFavoriteFolderTreeByBrokerId(brokerId, employeeId, false);
            CustomFavoriteFolderNode treeRoot;

            if (userFolder.Count > 0)
            {
                treeRoot = userFolder[0] as CustomFavoriteFolderNode;
            }
            else
            {
                treeRoot = new CustomFavoriteFolderNode()
                {
                    Folder = new CustomFavoriteFolder()
                    {
                        InternalName = "Favorites",
                        ExternalName = "Favorites",
                    }
                };
            }

            if (folderId != -1)
            {
                var folders = GetAllFolderNodes(treeRoot, new List<CustomFavoriteFolderNode>());
                treeRoot = folders.FirstOrDefault(node => node.Folder.FolderId == folderId);
            }

            return treeRoot;
        }

        /// <summary>
        /// Recursively gets all folders in a tree and returns a list.
        /// </summary>
        /// <param name="node">Root node to start traversing from.</param>
        /// <param name="folderNodes">List of modified nodes to build as recursion continues.</param>
        /// <returns>List of all modified folders.</returns>
        public static List<CustomFavoriteFolderNode> GetAllFolderNodes(CustomFavoriteFolderNode node, List<CustomFavoriteFolderNode> folderNodes)
        {
            folderNodes.Add(node);

            foreach (CustomFavoriteNode childNode in node.Children)
            {
                if (childNode is CustomFavoriteFolderNode)
                {
                    folderNodes = GetAllFolderNodes((CustomFavoriteFolderNode)childNode, folderNodes);
                }
            }

            return folderNodes;
        }

        /// <summary>
        /// Builds custom favorite folder parameters.
        /// </summary>
        /// <param name="node">Node to build the parameter from.</param>
        /// <param name="customFolderTableType">Custom folder table type data holder.</param>
        public static void BuildCustomFavoriteFolderParameters(CustomFavoriteFolderNode node, CustomFolderTableTypes customFolderTableType)
        {
            customFolderTableType.FolderTable.Rows.Add(node.Folder.FolderId, node.Folder.BrokerId, node.Folder.EmployeeId, node.Folder.InternalName, node.Folder.ExternalName, (int)node.Folder.LeadLoanStatus, node.SortOrder + 1, node.Folder.ParentFolderId);

            foreach (Guid roleId in node.Folder.RoleIds)
            {
                customFolderTableType.RoleTable.Rows.Add(node.Folder.FolderId, roleId);
            }

            foreach (Guid groupId in node.Folder.GroupIds)
            {
                customFolderTableType.EmployeeGroupTable.Rows.Add(node.Folder.FolderId, groupId);
            }

            foreach (E_sLPurposeT loanPurpose in node.Folder.LoanPurposes)
            {
                customFolderTableType.LoanPurposeTable.Rows.Add(node.Folder.FolderId, (int)loanPurpose);
            }

            foreach (E_sLT loanType in node.Folder.LoanTypes)
            {
                customFolderTableType.LoanTypeTable.Rows.Add(node.Folder.FolderId, (int)loanType);
            }
        }

        /// <summary>
        /// Builds custom favorite page parameters.
        /// </summary>
        /// <param name="folderId">Folder id.</param>
        /// <param name="node">Node to build the parameter from.</param>
        /// <param name="customFolderTableTypes">Custom folder table type data holder.</param>
        public static void BuildCustomFavoritePageParameters(int folderId, CustomFavoritePageNode node, CustomFolderTableTypes customFolderTableTypes)
        {
            customFolderTableTypes.PageTable.Rows.Add(folderId, node.PageId, node.SortOrder);
        }

        /// <summary>
        /// Checks a folder node's permission and all its children's through recursion.
        /// </summary>
        /// <param name="node">Node to check permission.</param>
        /// <param name="filter">Filter to determine whether the folder is visible.</param>
        public static void CheckCustomFavoriteFolderNodePermissionRecursive(CustomFavoriteNode node, CustomFavoriteFolderFilter filter)
        {
            if (node is CustomFavoriteFolderNode)
            {
                var folder = node as CustomFavoriteFolderNode;
                if (CheckFolderPermission(folder.Folder, filter))
                {
                    foreach (CustomFavoriteNode childNode in folder.Children)
                    {
                        CheckCustomFavoriteFolderNodePermissionRecursive(childNode, filter);
                    }
                }
                else
                {
                    node.IsVisible = false;
                }
            }
        }

        /// <summary>
        /// Constructs a dictionary in an attempt to flatten the tree structure.  The key is the page or folder's id, 
        /// and the value is a dictionary of dynaNodes that share that same key.
        /// </summary>
        /// <param name="node">The node that will be flattened.</param>
        /// <param name="dynaTreeDictionary">The dictionary that will be set to the new value.</param>
        /// <returns>A dictionary where the key is the node's id, and the value is a list of matching dynaNodes.</returns>
        public static Dictionary<string, List<DynaTreeNode>> FlattenDynaNodePages(DynaTreeNode node, Dictionary<string, List<DynaTreeNode>> dynaTreeDictionary)
        {
            foreach (DynaTreeNode child in node.children)
            {
                if (!dynaTreeDictionary.ContainsKey(child.key))
                {
                    dynaTreeDictionary.Add(child.key, new List<DynaTreeNode>() { child });
                }
                else
                {
                    List<DynaTreeNode> list;
                    if (dynaTreeDictionary.TryGetValue(child.key, out list))
                    {
                        list.Add(child);
                    }
                }

                FlattenDynaNodePages(child, dynaTreeDictionary);
            }

            return dynaTreeDictionary;
        }

        /// <summary>
        /// Checks specific folder's permission.
        /// </summary>
        /// <param name="folder">Folder to check permission.</param>
        /// <param name="filter">Filter to compare the permissions to.</param>
        /// <returns>Whether the folder is visible or not depending on the filter.</returns>
        private static bool CheckFolderPermission(CustomFavoriteFolder folder, CustomFavoriteFolderFilter filter)
        {
            if (filter.EmployeeId.HasValue && folder.EmployeeId.HasValue)
            {
                if (folder.EmployeeId != filter.EmployeeId)
                {
                    return false;
                }
            }

            if (filter.LeadLoanStatus != LeadLoanStatusT.DC && folder.LeadLoanStatus != LeadLoanStatusT.DC)
            {
                if (folder.LeadLoanStatus != filter.LeadLoanStatus)
                {
                    return false;
                }
            }

            if (folder.RoleIds.Any())
            {
                bool hasRole = false;
                foreach (Guid roleId in filter.RoleIds)
                {
                    if (folder.RoleIds.Any(folderRoleId => folderRoleId == roleId))
                    {
                        hasRole = true;
                        break;
                    }
                }

                if (!hasRole)
                {
                    return false;
                }
            }

            if (folder.GroupIds.Any())
            {
                bool hasGroup = false;
                foreach (Guid groupid in filter.GroupIds)
                {
                    if (folder.GroupIds.Any(folderGroupId => folderGroupId == groupid))
                    {
                        hasGroup = true;
                        break;
                    }
                }

                if (!hasGroup)
                {
                    return false;
                }
            }

            if (folder.LoanPurposes.Any())
            {
                bool hasLoanPurpose = false;
                foreach (E_sLPurposeT loanPurpose in filter.LoanPurposes)
                {
                    if (folder.LoanPurposes.Any(folderLoanPurpose => folderLoanPurpose == loanPurpose))
                    {
                        hasLoanPurpose = true;
                        break;
                    }
                }

                if (!hasLoanPurpose)
                {
                    return false;
                }
            }

            if (folder.LoanTypes.Any())
            {
                bool hasLoanType = false;
                foreach (E_sLT loanType in filter.LoanTypes)
                {
                    if (folder.LoanTypes.Any(folderLoanType => folderLoanType == loanType))
                    {
                        hasLoanType = true;
                        break;
                    }
                }

                if (!hasLoanType)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
