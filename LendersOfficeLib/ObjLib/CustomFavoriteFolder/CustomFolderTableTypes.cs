﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Custom folder table types.
    /// </summary>
    public class CustomFolderTableTypes
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFolderTableTypes" /> class.
        /// </summary>
        public CustomFolderTableTypes()
        {
            var folderTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "BrokerId", typeof(Guid) },
                { "EmployeeId", typeof(Guid) },
                { "InternalName", typeof(string) },
                { "ExternalName", typeof(string) },
                { "LeadLoanStatus", typeof(int) },
                { "SortOrder", typeof(int) },
                { "ParentFolderId", typeof(int) }
            };

            var roleTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "RoleId", typeof(Guid) }
            };

            var employeeGroupTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "GroupId", typeof(Guid) }
            };

            var loanPurposeTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "LoanPurpose", typeof(int) }
            };

            var loanTypeTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "LoanType", typeof(int) }
            };

            var pageTableDictionary = new Dictionary<string, Type>()
            {
                { "FolderId", typeof(int) },
                { "PageId", typeof(string) },
                { "SortOrder", typeof(int) }
            };

            this.FolderTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderTableType", folderTableDictionary);
            this.RoleTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderXRoleTableType", roleTableDictionary);
            this.EmployeeGroupTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderXEmployeeGroupTableType", employeeGroupTableDictionary);
            this.LoanPurposeTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderXLoanPurposeTableType", loanPurposeTableDictionary);
            this.LoanTypeTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderXLoanTypeTableType", loanTypeTableDictionary);
            this.PageTable = DbAccessUtils.InitializeTableValueDataTable("CustomFolderXPageTableType", pageTableDictionary);
        }

        /// <summary>
        /// Gets or sets the folder table.
        /// </summary>
        public DataTable FolderTable { get; set; }

        /// <summary>
        /// Gets or sets the role table.
        /// </summary>
        public DataTable RoleTable { get; set; }

        /// <summary>
        /// Gets or sets the employee group table.
        /// </summary>
        public DataTable EmployeeGroupTable { get; set; }

        /// <summary>
        /// Gets or sets the loan purpose table.
        /// </summary>
        public DataTable LoanPurposeTable { get; set; }

        /// <summary>
        /// Gets or sets the loan type table.
        /// </summary>
        public DataTable LoanTypeTable { get; set; }

        /// <summary>
        /// Gets or sets the page table.
        /// </summary>
        public DataTable PageTable { get; set; }                   
    }
}
