﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    /// <summary>
    /// Custom favorite page node.
    /// </summary>
    public class CustomFavoritePageNode : CustomFavoriteNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoritePageNode" /> class.
        /// </summary>
        /// <param name="pageId">Page id of the node.</param>
        /// <param name="sortOrder">Sort order.</param>
        public CustomFavoritePageNode(string pageId, int sortOrder)
        {
            this.PageId = pageId;
            this.SortOrder = sortOrder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFavoritePageNode" /> class.  It is necessary for JsonNet.
        /// </summary>
        public CustomFavoritePageNode()
        {
        }

        /// <summary>
        /// Gets or sets the page id.
        /// </summary>
        public string LinkText { get; set; }

        /// <summary>
        /// Gets or sets the page id.
        /// </summary>
        public string PageId { get; set; }
    }
}
