﻿namespace LendersOffice.ObjLib.CustomFavoriteFolder
{
    /// <summary>
    /// Lead loan status enum.
    /// </summary>
    public enum LeadLoanStatusT
    {
        /// <summary>
        /// Don't care.
        /// </summary>
        DC = 0,

        /// <summary>
        /// This is a loan.
        /// </summary>
        Loan = 1,

        /// <summary>
        /// This is a lead.
        /// </summary>
        Lead = 2,
    }
}
