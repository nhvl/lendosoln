﻿namespace LendersOffice.ObjLib.CreditReportAuthorization
{
    using System;
    using DataAccess;

    /// <summary>
    /// A simple class for associating documents to a given borrower's credit report authorization.
    /// </summary>
    /// <remarks>
    /// The static methods provide the interface to load or alter associations.
    /// </remarks>
    public class DocumentAssociation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentAssociation"/> class.
        /// </summary>
        /// <param name="appId">The <see cref="CAppData.aAppId"/> of the borrower.</param>
        /// <param name="isCoborrrower">A value indicating whether the borrower is a co-borrower on the application.</param>
        /// <param name="documentId">The <see cref="EDocs.EDocument.DocumentId"/> of the document associated with the borrower.</param>
        /// <param name="documentDescription">The description of the document.</param>
        private DocumentAssociation(Guid appId, bool isCoborrrower, Guid documentId, string documentDescription)
        {
            this.AppId = appId;
            this.IsCoborrower = isCoborrrower;
            this.DocumentId = documentId;
            this.DocumentDescription = documentDescription;
        }

        /// <summary>
        /// Gets the <see cref="CAppData.aAppId"/> of the borrower.
        /// </summary>
        /// <value>The <see cref="CAppData.aAppId"/> of the borrower.</value>
        public Guid AppId { get; }

        /// <summary>
        /// Gets a value indicating whether the borrower is a co-borrower on the application.
        /// </summary>
        /// <value>A value indicating whether the borrower is a co-borrower on the application.</value>
        public bool IsCoborrower { get; }

        /// <summary>
        /// Gets the <see cref="EDocs.EDocument.DocumentId"/> of the document associated with the borrower.
        /// </summary>
        /// <value>The <see cref="EDocs.EDocument.DocumentId"/> of the document associated with the borrower.</value>
        public Guid DocumentId { get; }

        /// <summary>
        /// Gets the description of the document.  For loading efficiency only.
        /// </summary>
        /// <value>The description of the document.  For loading efficiency only.</value>
        public string DocumentDescription { get; }

        /// <summary>
        /// Saves the specified document association to the database.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        /// <param name="appId">The application id of the borrower.</param>
        /// <param name="isCoborrower">A value indicating whether the borrower is a co-borrower on the application.</param>
        /// <param name="documentId">The document id of the document.</param>
        public static void Save(Guid brokerId, Guid appId, bool isCoborrower, Guid documentId)
        {
            var parameters = new[]
            {
                new System.Data.SqlClient.SqlParameter("@AppId", appId),
                new System.Data.SqlClient.SqlParameter("@IsCoborrower", isCoborrower),
                new System.Data.SqlClient.SqlParameter("@DocumentId", documentId)
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BorrowerCreditAuthorizationDocumentAssociation_Save", 2, parameters);
        }

        /// <summary>
        /// Clears the specified document association from the database.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        /// <param name="appId">The application id of the borrower.</param>
        /// <param name="isCoborrower">A boolean value indicating whether the borrower is a co-borrower on the application.</param>
        public static void Clear(Guid brokerId, Guid appId, bool isCoborrower)
        {
            var parameters = new[]
            {
                new System.Data.SqlClient.SqlParameter("@AppId", appId),
                new System.Data.SqlClient.SqlParameter("@IsCoborrower", isCoborrower)
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BorrowerCreditAuthorizationDocumentAssociation_Clear", 2, parameters);
        }

        /// <summary>
        /// Retrieves a pair of association for an application, where each may be null if no association exists.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        /// <param name="appId">The application id to look up.</param>
        /// <returns>A tuple of document associations.  If one or both do not exist, their entry will be null.</returns>
        public static Tuple<DocumentAssociation, DocumentAssociation> RetrieveForApplication(Guid brokerId, Guid appId)
        {
            DocumentAssociation borrowerAssociation = null;
            DocumentAssociation coborrowerAssociation = null;
            var parameters = new[] { new System.Data.SqlClient.SqlParameter("@AppId", appId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "BorrowerCreditAuthorizationDocumentAssociation_RetrieveForApp", parameters))
            {
                while (reader.Read())
                {
                    var documentAssociation = new DocumentAssociation(
                        (Guid)reader["AppId"],
                        (bool)reader["IsCoborrower"],
                        (Guid)reader["DocumentId"],
                        (string)reader["DocumentDescription"]);

                    if (documentAssociation.IsCoborrower)
                    {
                        coborrowerAssociation = documentAssociation;
                    }
                    else
                    {
                        borrowerAssociation = documentAssociation;
                    }
                }
            }

            return Tuple.Create(borrowerAssociation, coborrowerAssociation);
        }

        /// <summary>
        /// Swaps the document associations of the borrower and the co-borrower on the specified application.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        /// <param name="appId">The application id.</param>
        public static void SwapBorrowerAndCoborrower(Guid brokerId, Guid appId)
        {
            var parameters = new[] { new System.Data.SqlClient.SqlParameter("@AppId", appId) };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BorrowerCreditAuthorizationDocumentAssociation_SwapInApp", 2, parameters);
        }
    }
}
