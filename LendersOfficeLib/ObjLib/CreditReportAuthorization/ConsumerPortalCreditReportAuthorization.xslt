﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="root"/>
  </xsl:template>

  <xsl:template match="root">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
</xsl:text>
    <html>
      <head>
        <title>
          Authorization of Credit Check
        </title>
        <style type="text/css"><xsl:text disable-output-escaping="yes">
      * {
        font-family: "Helvetica", sans-serif;
      }
      table {
        margin-top: 1.1em;
      }
      td {
        padding-top: .2em;
        padding-bottom: .2em;
      }
      tr td:first-child {
        font-weight: bold;
        padding-right: 10px;
      }
      #authorization-notice {
        font-weight: bold;
      }
      .date-time {
        font-weight: normal;
      }
      h1 {
        text-align: center;
      }
    </xsl:text></style>
      </head>
      <body>
        <h1>Authorization of Credit Check</h1>
        <p id="authorization-notice">
          The following authorization was provided on <span class="date-time"><xsl:value-of select="@date"/></span> at <span class="date-time"><xsl:value-of select="@time"/></span>
        </p>

        <blockquote>
          <p>
            <xsl:call-template name="InsertLineBreaks">
              <xsl:with-param name="paragraphText" select="@authorizationVerbiage"/>
            </xsl:call-template>
          </p>
        </blockquote>

        <p>
          <input type="checkbox" checked="checked"/>
          <xsl:choose>
            <xsl:when test="count(borrower) = 1">
              I authorize my credit to be checked.
            </xsl:when>
            <xsl:otherwise>
              We authorize our credit to be checked.
            </xsl:otherwise>
          </xsl:choose>
        </p>

        <xsl:apply-templates select="borrower"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="borrower">
    <table>
      <xsl:call-template name="CreateBorrowerDataRow">
        <xsl:with-param name="DataName" select="'Name'"/>
        <xsl:with-param name="DataValue" select="@name"/>
      </xsl:call-template>
      <xsl:call-template name="CreateBorrowerDataRow">
        <xsl:with-param name="DataName" select="'DOB'"/>
        <xsl:with-param name="DataValue" select="@dob"/>
      </xsl:call-template>
      <xsl:call-template name="CreateBorrowerDataRow">
        <xsl:with-param name="DataName" select="'SSN'"/>
        <xsl:with-param name="DataValue" select="@maskedSsn"/>
      </xsl:call-template>
      <xsl:call-template name="CreateBorrowerDataRow">
        <xsl:with-param name="DataName" select="'Email'"/>
        <xsl:with-param name="DataValue" select="@email"/>
      </xsl:call-template>
      <xsl:call-template name="CreateBorrowerDataRow">
        <xsl:with-param name="DataName" select="'Phone'"/>
        <xsl:with-param name="DataValue" select="@phone"/>
      </xsl:call-template>
    </table>
  </xsl:template>

  <xsl:template name="CreateBorrowerDataRow">
    <xsl:param name="DataName"/>
    <xsl:param name="DataValue"/>
    <tr>
      <td>
        <xsl:if test="@type = 'C'">Co-</xsl:if>Borrower <xsl:value-of select="$DataName"/>:
      </td>
      <td>
        <xsl:value-of select="$DataValue"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="InsertLineBreaks">
    <!-- Copied from http://stackoverflow.com/a/3310019/2946652 -->
    <xsl:param name="paragraphText"/>
    <xsl:choose>
      <xsl:when test="not(contains($paragraphText, '&#xA;'))">
        <xsl:value-of select="$paragraphText"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="substring-before($paragraphText, '&#xA;')"/>
        <br />
        <xsl:call-template name="InsertLineBreaks">
          <xsl:with-param name="paragraphText" select="substring-after($paragraphText, '&#xA;')"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
