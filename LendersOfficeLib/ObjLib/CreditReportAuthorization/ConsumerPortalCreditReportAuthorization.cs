﻿namespace LendersOffice.ObjLib.CreditReportAuthorization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a simple interface for the consumer portal's action to authorize credit check for all borrowers on a loan.
    /// </summary>
    public class ConsumerPortalCreditReportAuthorization
    {
        /// <summary>
        /// The date format used in the generated PDF document.
        /// </summary>
        private const string DateFormat = "MM/dd/yyyy";

        /// <summary>
        /// Records the authorization of a credit check on every regular application borrower on the loan.
        /// </summary>
        /// <param name="loan">The loan to pull borrowers from.</param>
        /// <param name="authorizationVerbiage">The authorization verbiage the borrower agreed to.</param>
        /// <param name="principal">The user recording this event.</param>
        public static void AuthorizeCreditCheckForAllBorrowers(CPageData loan, string authorizationVerbiage, AbstractUserPrincipal principal)
        {
            DateTime now = DateTime.Now;
            SetCreditAuthorizationDates(loan, now.ToString());
            var documentId = GenerateCreditReportAuthorizationDocument(loan, now, authorizationVerbiage);
            loan.RecordAuditOnSave(CreateAuditEvent(loan, principal));
            if (documentId.HasValue)
            {
                foreach (var app in GetBorrowers(loan))
                {
                    DocumentAssociation.Save(loan.sBrokerId, app.aAppId, app.BorrowerModeT == E_BorrowerModeT.Coborrower, documentId.Value);
                }
            }
        }

        /// <summary>
        /// Sets the borrower or co-borrower credit report authorization dates.
        /// </summary>
        /// <param name="loan">The loan object to set these dates on.</param>
        /// <param name="dateString">The date value to set each date field to.</param>
        private static void SetCreditAuthorizationDates(CPageData loan, string dateString)
        {
            foreach (CAppData app in GetBorrowers(loan))
            {
                if (app.BorrowerModeT == E_BorrowerModeT.Borrower)
                {
                    app.aBCreditAuthorizationD_rep = dateString;
                }
                else
                {
                    app.aCCreditAuthorizationD_rep = dateString;
                }
            }
        }

        /// <summary>
        /// Generates a PDF recording the borrowers' credit report authorizations and saves it to EDocs.
        /// </summary>
        /// <param name="loan">The loan to save the document to (as well as load the borrower data from).</param>
        /// <param name="dateTime">The date of the credit report authorization.</param>
        /// <param name="authorizationVerbiage">The verbiage of the authorization the borrower's agreed to.</param>
        /// <returns>The <seealso cref="EDocs.EDocument.DocumentId"/>, or null if the document could not be created.</returns>
        private static Guid? GenerateCreditReportAuthorizationDocument(CPageData loan, DateTime dateTime, string authorizationVerbiage)
        {
            string docDescription = "Authorization of Credit Check for:" + Environment.NewLine + string.Join(Environment.NewLine, GetBorrowers(loan).Select(app => app.aNm));

            const string Xslt = "LendersOffice.ObjLib.CreditReportAuthorization.ConsumerPortalCreditReportAuthorization.xslt";
            return LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SaveHtmlDocAsSystem(
                E_AutoSavePage.ConsumerPortalCreditAuthorization,
                () => XslTransformHelper.TransformFromEmbeddedResource(Xslt, CreateCreditReportAuthorizationXml(loan, dateTime, authorizationVerbiage).ToString(SaveOptions.DisableFormatting), null),
                loan.sBrokerId,
                loan.sLId,
                loan.GetAppData(0).aAppId,
                docDescription,
                E_EnforceFolderPermissions.False);
        }

        /// <summary>
        /// Creates the XML representation of the authorization event, for XSL transformation into the HTML of the PDF.
        /// </summary>
        /// <param name="loan">The loan to load the borrower data from.</param>
        /// <param name="dateTime">The date and time of the authorization.</param>
        /// <param name="authorizationVerbiage">The text of the authorization.</param>
        /// <returns>An XML element representing the authorization event.</returns>
        private static XElement CreateCreditReportAuthorizationXml(CPageData loan, DateTime dateTime, string authorizationVerbiage)
        {
            return new XElement(
                "root",
                new XAttribute("date", dateTime.ToString(DateFormat)),
                new XAttribute("time", Tools.GetDateTimeDescription(dateTime, "hh:mm tt")),
                new XAttribute("authorizationVerbiage", Tools.SantizeXmlString(authorizationVerbiage)),
                GetBorrowers(loan).Select(
                    app => new XElement(
                        "borrower",
                        new XAttribute("type", app.BorrowerModeT == E_BorrowerModeT.Borrower ? "B" : "C"),
                        new XAttribute("name", app.aNm),
                        new XAttribute("dob", ConvertDateToString(app.aDob)),
                        new XAttribute("maskedSsn", CommonLib.SSNUtil.MaskSSN(app.aSsn)),
                        new XAttribute("email", app.aEmail),
                        new XAttribute("phone", app.aHPhone))));
        }

        /// <summary>
        /// Creates an audit event for the borrower's authorization of a credit report.
        /// </summary>
        /// <param name="loan">The loan to pull the borrower data from.</param>
        /// <param name="principal">The user recording the audit event.</param>
        /// <returns>An audit event of the borrowers' authorization.</returns>
        private static AbstractAuditItem CreateAuditEvent(CPageData loan, AbstractUserPrincipal principal)
        {
            var auditEvent = new CreditReportAuthorizationAuditItem(principal);
            foreach (var app in GetBorrowers(loan))
            {
                auditEvent.AddBorrower(app.aNm, app.aSsn, app.BorrowerModeT == E_BorrowerModeT.Coborrower);
            }

            return auditEvent;
        }

        /// <summary>
        /// Lazily returns an application with the borrower mode set for each borrower that is included.
        /// </summary>
        /// <param name="loan">The loan containing the borrowers.</param>
        /// <returns>A lazy set of applications, with the appropriate borrower mode set.</returns>
        private static IEnumerable<CAppData> GetBorrowers(CPageData loan)
        {
            foreach (var app in loan.Apps)
            {
                app.BorrowerModeT = E_BorrowerModeT.Borrower;
                yield return app;
                if (app.aCIsDefined)
                {
                    app.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    yield return app;
                }
            }
        }

        /// <summary>
        /// Converts a <seealso cref="CDateTime"/> object into a string representation, using our formatting.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>The string representation of the date.</returns>
        /// <remarks>
        /// While this could be done inline, doing so would cause a double evaluation of the value containing
        /// the <seealso cref="CDateTime"/>.  For example, <c>app.aBDob.IsValid ? app.aBDob : null</c> will
        /// evaluate the property twice, and CDateTime initialization is surprisingly expensive.
        /// </remarks>
        private static string ConvertDateToString(CDateTime date)
        {
            return date.IsValid ? date.DateTimeForComputation.ToString(DateFormat) : string.Empty;
        }
    }
}
