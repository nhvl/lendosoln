﻿namespace LendersOfficeLib.ObjLib.Lon
{
    using System;
    using DataAccess;

    /// <summary>
    /// Bypass security checks for this accessor.  We don't
    /// have a valid user principal, so we just assume that
    /// all access by this robot is safe.
    /// </summary>
    internal class CLonIntegrationAllData : CLoanLONXmlWriterData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLonIntegrationAllData"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        internal CLonIntegrationAllData(Guid loanId)
            : base(loanId)
        {
        }

        /// <summary>
        /// Bypass security checks for this accessor.  We
        /// don't have a valid user principal, so we just
        /// assume that all access by this robot is safe.
        /// </summary>
        #region Generated Code
        protected override bool m_enforceAccessControl
        #endregion
        {
            // By default, all page data accessors must submit
            // to access control checking.  Override on a limited
            // case-by-case basis.
            get
            {
                return false;
            }
        }
    }
}
