﻿namespace LendersOffice.ObjLib.Appraisal
{
    /// <summary>
    /// Appraisal Delivery Method.
    /// </summary>
    public enum AppraisalDeliveryMethod
    {
        /// <summary>
        /// Blank. Empty. Nothing. None.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Delivered via fax.
        /// </summary>
        Fax = 1,

        /// <summary>
        /// Delivered in person.
        /// </summary>
        InPerson = 2,

        /// <summary>
        /// Delivered via snail mail.
        /// </summary>
        Mail = 3,

        /// <summary>
        /// Delivered via overnight delivery.
        /// </summary>
        Overnight = 4,

        /// <summary>
        /// Delivered via E-Mail.
        /// </summary>
        Email = 5
    }
}
