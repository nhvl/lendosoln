﻿namespace LendersOffice.ObjLib.Appraisal
{
    /// <summary>
    /// Appraisal order types.
    /// </summary>
    public enum AppraisalOrderType
    {
        /// <summary>
        /// Manually enterd appraisal order.
        /// </summary>
        Manual = 0,

        /// <summary>
        /// GDMS appraisal order.
        /// </summary>
        Gdms = 1,

        /// <summary>
        /// LQB generic appraisal framework order.
        /// </summary>
        Lqb = 2
    }
}
