﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Adapter;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class for representing manually entered appraisal orders.
    /// </summary>
    public class ManualAppraisalOrder : AppraisalOrderBase
    {
        /// <summary>
        /// A value indicating whether this is a new appraisal order.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// The edocs associated with this manual appraisal order.
        /// </summary>
        private HashSet<Guid> associatedEdocs = new HashSet<Guid>();

        /// <summary>
        /// The edocs to save.
        /// </summary>
        private HashSet<Guid> edocsToSave = new HashSet<Guid>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualAppraisalOrder"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        public ManualAppraisalOrder(Guid loanId, Guid brokerId)
            : base(loanId, brokerId)
        {
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualAppraisalOrder"/> class.
        /// </summary>
        /// <param name="appraisalProperties">The appraisal order properties.</param>
        /// <param name="edocRows">The edoc rows.</param>
        private ManualAppraisalOrder(IReadOnlyDictionary<string, object> appraisalProperties, List<IReadOnlyDictionary<string, object>> edocRows)
            : base(appraisalProperties)
        {
            foreach (var edocRow in edocRows.CoalesceWithEmpty())
            {
                var edocId = (Guid)edocRow["EDocId"];

                this.associatedEdocs.Add(edocId);
            }

            this.isNew = false;
        }

        /// <summary>
        /// Gets or sets the type of appraisal order this object is.
        /// </summary>
        /// <value>Appraisal order type.</value>
        public override AppraisalOrderType AppraisalOrderType
        {
            get
            {
                return AppraisalOrderType.Manual;
            }

            protected set
            {
            }
        }

        /// <summary>
        /// The ids of the edocs associated with this manual appraisal order.
        /// </summary>
        protected override IEnumerable<Guid> AssociatedEdocIds
        {
            get
            {
                return this.associatedEdocs;
            }
        }

        /// <summary>
        /// Retrieves a manual appraisal order by appraisal order ID.
        /// </summary>
        /// <param name="appraisalOrderId">Appraisal order ID.</param>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>A ManualAppraisalOrder object, or null if order is not found.</returns>
        public static ManualAppraisalOrder GetOrderById(Guid appraisalOrderId, Guid brokerId)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@AppraisalOrderId", appraisalOrderId)
                };

            IReadOnlyDictionary<string, object> orderProperties = null;
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "APPRAISAL_ORDER_GetById", parameters))
            {
                if (reader.Read())
                {
                    orderProperties = reader.ToDictionary();

                    List<IReadOnlyDictionary<string, object>> edocs = new List<IReadOnlyDictionary<string, object>>();
                    if (orderProperties != null && reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            edocs.Add(reader.ToDictionary());
                        }
                    }

                    return new ManualAppraisalOrder(orderProperties, edocs);
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves all manual appraisal orders by appraisal loan ID.
        /// </summary>
        /// <param name="loanId">Appraisal order ID.</param>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="isValid">Filter on order validity.</param>
        /// <returns>An enumerable of ManualAppraisalOrder objects.</returns>
        public static IEnumerable<ManualAppraisalOrder> GetAllOrdersByLoanId(Guid loanId, Guid brokerId, bool? isValid = null)
        {
            List<ManualAppraisalOrder> orders = new List<ManualAppraisalOrder>();

            SqlParameter[] parameters =
                {
                    new SqlParameter("@LoanId", loanId),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@isValid", isValid)
                };

            List<IReadOnlyDictionary<string, object>> retrievedAppraisalRows = new List<IReadOnlyDictionary<string, object>>();
            Dictionary<Guid, List<IReadOnlyDictionary<string, object>>> retrievedEdocsByAppraisalId = new Dictionary<Guid, List<IReadOnlyDictionary<string, object>>>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ManualAppraisalOrder_GetAllByLoanId", parameters))
            {
                while (reader.Read())
                {
                    retrievedAppraisalRows.Add(reader.ToDictionary());
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        Guid appraisalOrderId = (Guid)reader["AppraisalOrderId"];
                        List<IReadOnlyDictionary<string, object>> edocsRows;
                        if (!retrievedEdocsByAppraisalId.TryGetValue(appraisalOrderId, out edocsRows))
                        {
                            edocsRows = new List<IReadOnlyDictionary<string, object>>();
                            retrievedEdocsByAppraisalId.Add(appraisalOrderId, edocsRows);
                        }

                        edocsRows.Add(reader.ToDictionary());
                    }
                }
            }

            foreach (var orderRow in retrievedAppraisalRows)
            {
                var appraisalOrderId = (Guid)orderRow["AppraisalOrderId"];
                var edocs = retrievedEdocsByAppraisalId.GetValueOrNull(appraisalOrderId);

                orders.Add(new ManualAppraisalOrder(orderRow, edocs));
            }

            return orders;
        }

        /// <summary>
        /// Marks order as invalid.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="loanId">The loan Id.</param>
        /// <param name="appraisalOrderId">Appraisal order ID.</param>
        /// <param name="deletedByUserId">ID of user who deleted the order.</param>
        public static void InvalidateOrder(Guid brokerId, Guid loanId, Guid appraisalOrderId, Guid deletedByUserId)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@AppraisalOrderId", appraisalOrderId),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@LoanId", loanId),
                    new SqlParameter("@DeletedByUserId", deletedByUserId),
                    new SqlParameter("@DeletedDate", DateTime.Now)
                };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ManualAppraisalOrder_Invalidate", 3, parameters);
        }

        /// <summary>
        /// Creates view for manual appraisal orders.
        /// </summary>
        /// <param name="principal">The principal of the user generating the view.</param>
        /// <returns>The appraisal order view.</returns>
        public override AppraisalOrderView ToView(AbstractUserPrincipal principal)
        {
            var view = base.ToView(principal);
            view.LoadingId = this.AppraisalOrderId.ToString();
            view.UploadedDocuments = this.GetEdocDocumentViews(principal);

            return view;
        }

        /// <summary>
        /// Associates an edoc with this manual appraisal order.
        /// </summary>
        /// <param name="edocId">The edoc id.</param>
        /// <returns>True if edoc added. False otherwise.</returns>
        public bool AddEdocRecord(Guid edocId)
        {
            if (this.associatedEdocs.Contains(edocId))
            {
                return false;
            }

            this.associatedEdocs.Add(edocId);
            this.edocsToSave.Add(edocId);

            return true;
        }

        /// <summary>
        /// Removes the edoc assocations.
        /// </summary>
        public void RemoveEdocAssocations()
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@AppraisalOrderId", this.AppraisalOrderId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            StoredProcedureName storedProcedure = StoredProcedureName.Create("APPRAISAL_ORDER_EDOCS_DeleteAllByAppraisalOrderId").Value;
            using (DbConnection conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, storedProcedure, parameters, TimeoutInSeconds.Thirty);
            }

            this.associatedEdocs.Clear();
        }

        /// <summary>
        /// Save to DB.
        /// </summary>
        public void Save()
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                conn.OpenWithRetry();

                using (DbTransaction tx = conn.BeginTransaction())
                {
                    try
                    {
                        List<SqlParameter> orderParameters = this.GetBaseParameters();
                        StoredProcedureName orderSp = StoredProcedureName.Create(this.isNew ? "APPRAISAL_ORDER_Create" : "APPRAISAL_ORDER_Update").Value;

                        StoredProcedureDriverHelper.ExecuteNonQuery(conn, tx, orderSp, orderParameters, TimeoutInSeconds.Thirty);

                        StoredProcedureName edocCreateSp = StoredProcedureName.Create("APPRAISAL_ORDER_EDOCS_Create").Value;
                        foreach (var edoc in this.edocsToSave)
                        {
                            SqlParameter[] parameters = new SqlParameter[]
                            {
                                new SqlParameter("@AppraisalOrderId", this.AppraisalOrderId),
                                new SqlParameter("@LoanId", this.LoanId),
                                new SqlParameter("@BrokerId", this.BrokerId),
                                new SqlParameter("@EDocId", edoc),
                            };

                            StoredProcedureDriverHelper.ExecuteNonQuery(conn, tx, edocCreateSp, parameters, TimeoutInSeconds.Thirty);
                        }

                        tx.Commit();
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError(exc);
                        tx.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}
