﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using Conversions.GlobalDMS;
    using Conversions.LQBAppraisal;
    using GDMS.LookupMethods;

    /// <summary>
    /// Appraisal order container.
    /// </summary>
    public class OrderContainer
    {
        /// <summary>
        /// Gets or sets the GDMS client order.
        /// </summary>
        public ClientOrder ClientOrder { get; set; }

        /// <summary>
        /// Gets or sets the LQB appraisal order.
        /// </summary>
        public LQBAppraisalOrder LQBAppraisalOrder { get; set; }

        /// <summary>
        /// Gets or sets the GDMS appraisal order.
        /// </summary>
        public GDMSAppraisalOrderInfo GdmsAppraisalOrder { get; set; }

        /// <summary>
        /// Gets or sets the order's uploaded files.
        /// </summary>
        public OrderFileObject[] UploadedFiles { get; set; }

        /// <summary>
        /// Gets or sets the vendor id for the order.
        /// </summary>
        public Guid VendorID { get; set; }

        /// <summary>
        /// Gets or sets the vendor name for the order.
        /// </summary>
        public string VendorName { get; set; } = null;
    }
}
