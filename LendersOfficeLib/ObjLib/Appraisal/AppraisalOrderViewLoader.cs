﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Conversions.GlobalDMS;
    using Conversions.LQBAppraisal;
    using DataAccess;
    using Edocs.AutoSaveDocType;
    using EDocs;
    using LendersOffice.Security;

    /// <summary>
    /// A loader that will load up the various appraisal orders we have and turn them all into an <see cref="AppraisalOrderView"/>.
    /// </summary>
    public class AppraisalOrderViewLoader
    {
        /// <summary>
        /// The appraisal order views.
        /// </summary>
        private Lazy<List<AppraisalOrderView>> appraisalOrderViews;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderViewLoader"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="principal">The principal of the user loading the orders.</param>
        public AppraisalOrderViewLoader(Guid loanId, Guid brokerId, AbstractUserPrincipal principal)
        {
            this.LoanId = loanId;
            this.BrokerId = brokerId;
            this.Principal = principal;

            this.appraisalOrderViews = new Lazy<List<AppraisalOrderView>>(this.GetAppraisalOrdersForLoan);
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the principal.
        /// </summary>
        public AbstractUserPrincipal Principal { get; }

        /// <summary>
        /// Retrieves the appraisal order views.
        /// </summary>
        /// <returns>The list of appraisal order views.</returns>
        public List<AppraisalOrderView> Retrieve()
        {
            return this.appraisalOrderViews.Value;
        }

        /// <summary>
        /// Gets existing appraisal orders for loan and turns them all into views.
        /// </summary>
        /// <returns>The list of appraisal order views.</returns>
        private List<AppraisalOrderView> GetAppraisalOrdersForLoan()
        {
            List<AppraisalOrderView> orderViews = new List<AppraisalOrderView>();

            // Manual orders.
            orderViews.AddRange(ManualAppraisalOrder.GetAllOrdersByLoanId(this.LoanId, this.BrokerId).Select(order => order.ToView(this.Principal)));
            
            // LQB Appraisal orders.
            orderViews.AddRange(LQBAppraisalOrder.GetOrdersForLoan(this.LoanId).Select(o => o.ToView(this.Principal)));

            // GDMS Appraisal orders.
            orderViews.AddRange(this.GetGdmsViews());

            return orderViews;
        }

        /// <summary>
        /// Gets appraisal order views for GDMS Appraisal orders.
        /// </summary>
        /// <returns>List of AppraisalOrderView objects.</returns>
        private List<AppraisalOrderView> GetGdmsViews()
        {
            List<AppraisalOrderView> views = new List<AppraisalOrderView>();

            // Load orders for vendors using GlobalDMS / Etrac - hits their webservices to load newest data
            var globalDmsOrderLoader = new GlobalDMSOrderLoader(this.LoanId, this.Principal, loadDocAttachmentTypes: false, shouldQueueNewFiles: true);
            var orderContainers = globalDmsOrderLoader.Retrieve();

            foreach (var orderInfo in orderContainers)
            {
                AppraisalOrderView view = orderInfo.GdmsAppraisalOrder.ToView(this.Principal);

                if (orderInfo.ClientOrder.StatusName == "Error Accessing Order")
                {
                    view.ViewOrderUrl = "ERROR";
                    view.LoadingId = string.Empty;
                }
                else
                {
                    var pdf = new LendersOffice.Pdf.CAppraisalOrderPDF();
                    view.ViewOrderUrl = $"{Tools.VRoot}/pdf/{pdf.Name}.aspx?loanid={this.LoanId}&filenumber={orderInfo.ClientOrder.FileNumber}&vendorID={orderInfo.VendorID}";
                }

                view.VendorName = orderInfo.VendorName;
                view.ProductName = orderInfo.ClientOrder.ProductName;
                view.OrderedDate_rep = orderInfo.ClientOrder.CreationDate_String?.Split(' ')[0]; // Get rid of the time
                view.NeededDate_rep = orderInfo.ClientOrder.DateNeeded_String?.Split(' ')[0];
                view.Status = orderInfo.ClientOrder.StatusName;
                view.StatusDate_rep = orderInfo.ClientOrder.AppraiserLastModifiedDate_String?.Split(' ')[0];

                List<AppraisalDocumentView> docs = new List<AppraisalDocumentView>();
                var docsInEdocs = orderInfo.GdmsAppraisalOrder?.UploadedDocsSavedToEdocs;
                foreach (var docItem in orderInfo.UploadedFiles.CoalesceWithEmpty())
                {
                    AppraisalDocumentView docView = new AppraisalDocumentView();
                    docView.ViewUrl = docItem.url;
                    docView.FileName = docItem.fileName;

                    Tuple<Guid, string> edocRecord;
                    if (docsInEdocs != null && docsInEdocs.TryGetValue(docItem.id, out edocRecord))
                    {
                        docView.EDocId = edocRecord.Item1;
                    }

                    view.UploadedDocuments.Add(docView);
                }

                views.Add(view);
            }

            return views;
        }
    }
}
