﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;
    using System.Web;

    using DataAccess;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Used to represent Appraisal Orders in appraisal tracking UI.
    /// </summary>
    [DataContract]
    public class AppraisalOrderView : AppraisalOrderBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderView"/> class.
        /// Used for serialization.
        /// </summary>
        public AppraisalOrderView()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderView"/> class.
        /// Copies data from passed in AppraisalOrderBase derived object.
        /// </summary>
        /// <param name="order">Object derived from AppraisalOrderBase.</param>
        public AppraisalOrderView(AppraisalOrderBase order)
            : base()
        {
            this.AppraisalOrderId = order.AppraisalOrderId;
            this.LoanId = order.LoanId;
            this.BrokerId = order.BrokerId;
            this.IsValid = order.IsValid;
            this.AppraisalOrderType = order.AppraisalOrderType;
            this.ValuationMethod = order.ValuationMethod;
            this.DeliveryMethod = order.DeliveryMethod;
            this.AppraisalFormType = order.AppraisalFormType;
            this.ProductName = order.ProductName;
            this.OrderNumber = order.OrderNumber;
            this.FhaDocFileId = order.FhaDocFileId;
            this.UcdpAppraisalId = order.UcdpAppraisalId;
            this.Notes = order.Notes;
            this.OrderedDate = order.OrderedDate;
            this.NeededDate = order.NeededDate;
            this.ExpirationDate = order.ExpirationDate;
            this.ReceivedDate = order.ReceivedDate;
            this.SentToBorrowerDate = order.SentToBorrowerDate;
            this.BorrowerReceivedDate = order.BorrowerReceivedDate;
            this.RevisionDate = order.RevisionDate;
            this.ValuationEffectiveDate = order.ValuationEffectiveDate;
            this.SubmittedToFhaDate = order.SubmittedToFhaDate;
            this.SubmittedToUcdpDate = order.SubmittedToUcdpDate;
            this.CuRiskScore = order.CuRiskScore;
            this.OvervaluationRiskT = order.OvervaluationRiskT;
            this.PropertyEligibilityRiskT = order.PropertyEligibilityRiskT;
            this.AppraisalQualityRiskT = order.AppraisalQualityRiskT;
            this.DeletedByUserId = order.DeletedByUserId;
            this.DeletedDate = order.DeletedDate;
            this.AppIdAssociatedWithProperty = order.AppIdAssociatedWithProperty;
            this.LinkedReoId = order.LinkedReoId;
            this.PropertyAddress = order.PropertyAddress;
            this.PropertyCity = order.PropertyCity;
            this.PropertyState = order.PropertyState;
            this.PropertyZip = order.PropertyZip;
        }

        /// <summary>
        /// Gets or sets the type of appraisal order this object is.
        /// </summary>
        /// <value>Appraisal order type.</value>
        public override AppraisalOrderType AppraisalOrderType { get; protected set; }

        /// <summary>
        /// Gets or sets the id used to load the appraisal order.
        /// </summary>
        [DataMember(Name = "LoadingId")]
        public string LoadingId { get; set; }

        /// <summary>
        /// Gets or sets the UI index value of this object.
        /// </summary>
        /// <value>Index value used by UI.</value>
        [DataMember(Name = "Index")]
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the Vendor ID.
        /// </summary>
        /// <value>Vendor ID.</value>
        [DataMember(Name = "VendorId")]
        public Guid VendorId { get; set; }

        /// <summary>
        /// Gets or sets the Vendor Name.
        /// </summary>
        /// <value>Vendor Name.</value>
        [DataMember(Name = "VendorName")]
        public string VendorName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the order status.
        /// </summary>
        /// <value>Order status.</value>
        [DataMember(Name = "Status")]
        public string Status { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the status date.
        /// </summary>
        /// <value>Status date as a CDataTime.</value>
        public CDateTime StatusDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the status date.
        /// </summary>
        /// <value>Status date as a string.</value>
        [DataMember(Name = "StatusDate")]
        public string StatusDate_rep
        {
            get
            {
                return this.StatusDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.StatusDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the view order url.
        /// </summary>
        /// <value>The view order URL.</value>
        [DataMember(Name = "ViewOrderUrl")]
        public string ViewOrderUrl { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the list of uploaded document views.
        /// </summary>
        /// <value>A List of AppraisalDocumentViews.</value>
        [DataMember(Name = "UploadedDocuments")]
        public List<AppraisalDocumentView> UploadedDocuments { get; set; } = new List<AppraisalDocumentView>();

        /// <summary>
        /// Gets the associated edoc ids.
        /// </summary>
        protected override IEnumerable<Guid> AssociatedEdocIds
        {
            get
            {
                throw new NotImplementedException("Please use the actual appraisal object to pull this.");
            }
        }

        /// <summary>
        /// Saves a collection or appraisal order views.
        /// </summary>
        /// <param name="ordersToSave">The orders to save.</param>
        /// <param name="principal">The principal of the user saving.</param>
        /// <returns>Result object. Will have error if not saved.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Any error in transactions should be caught.")]
        public static Result<DBNull> Save(IEnumerable<AppraisalOrderView> ordersToSave, AbstractUserPrincipal principal)
        {
            using (var exec = new CStoredProcedureExec(principal.BrokerId))
            {
                try
                {
                    exec.BeginTransactionForWrite();
                    foreach (AppraisalOrderView order in ordersToSave)
                    {
                        order.Save(exec, principal.UserId);
                    }

                    exec.CommitTransaction();
                }
                catch (Exception e)
                {
                    Tools.LogError(e);
                    exec.RollbackTransaction();
                    return Result<DBNull>.Failure(e);
                }
            }

            return Result<DBNull>.Success(null);
        }

        /// <summary>
        /// Links this appraisal to an REO.
        /// </summary>
        /// <param name="reo">The reo to link to.</param>
        /// <param name="appId">The app id.</param>
        public void SetReo(IRealEstateOwned reo, Guid appId)
        {
            if (this.AppraisalOrderType != AppraisalOrderType.Manual)
            {
                return;
            }

            this.LinkedReoId = reo.RecordId;
            this.AppIdAssociatedWithProperty = appId;
            this.PropertyAddress = reo.Addr;
            this.PropertyCity = reo.City;
            this.PropertyState = reo.State;
            this.PropertyZip = reo.Zip;
        }

        /// <summary>
        /// Sets the appraisal order for subject property.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The city of the proeprty.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zipcode.</param>
        public void SetSubjectProperty(Guid appId, string streetAddress, string city, string state, string zip)
        {
            if (this.AppraisalOrderType != AppraisalOrderType.Manual)
            {
                return;
            }

            this.LinkedReoId = null;
            this.AppIdAssociatedWithProperty = appId;
            this.PropertyAddress = streetAddress;
            this.PropertyCity = city;
            this.PropertyState = state;
            this.PropertyZip = zip;
        }

        /// <summary>
        /// Save to DB.
        /// </summary>
        /// <param name="exec">Used to execute stored procedures within a transaction scope.</param>
        /// <param name="userId">ID of user who initiated save.</param>
        public void Save(CStoredProcedureExec exec, Guid userId)
        {
            switch (this.AppraisalOrderType)
            {
                case AppraisalOrderType.Manual:
                    this.SaveManualAppraisalOrder(exec, userId);
                    break;
                case AppraisalOrderType.Gdms:
                    this.SaveGdmsAppraisalOrder(exec);
                    break;
                case AppraisalOrderType.Lqb:
                    this.SaveLqbAppraisalOrder(exec);
                    break;
                default:
                    throw new UnhandledEnumException(this.AppraisalOrderType);
            }   
        }

        /// <summary>
        /// Updates DB with view data. Has some logic for manual appraisal order specific scenarios.
        /// </summary>
        /// <param name="exec">Used to execute stored procedures within a transaction scope.</param>
        /// <param name="userId">ID of user who initiated save.</param>
        private void SaveManualAppraisalOrder(CStoredProcedureExec exec, Guid userId)
        {
            bool isNew = this.AppraisalOrderId == Guid.Empty;
            if (isNew)
            {
                this.AppraisalOrderId = Guid.NewGuid();
                this.LoadingId = this.AppraisalOrderId.ToString();
            }

            if (!this.IsValid)
            {
                this.DeletedByUserId = userId;
                this.DeletedDate = CDateTime.Create(DateTime.Now);
            }

            List<SqlParameter> parameters = this.GetBaseParameters();
            string procedureName = isNew ? "APPRAISAL_ORDER_Create" : "APPRAISAL_ORDER_Update";

            exec.ExecuteNonQuery(procedureName, 3, parameters.ToArray());
        }

        /// <summary>
        /// Updates DB with view data.
        /// </summary>
        /// <param name="exec">Used to execute stored procedures within a transaction scope.</param>
        private void SaveGdmsAppraisalOrder(CStoredProcedureExec exec)
        {
            List<SqlParameter> parameters = this.GetBaseParameters();

            string procedureName = "APPRAISAL_ORDER_Update";

            exec.ExecuteNonQuery(procedureName, 3, parameters.ToArray());
        }

        /// <summary>
        /// Updates DB with view data.
        /// </summary>
        /// <param name="exec">Used to execute stored procedures within a transaction scope.</param>
        /// <remarks>
        /// Saves in a roundabout manner in order to sync view data with LQB appraisal data stored in XML fields.
        /// </remarks>
        private void SaveLqbAppraisalOrder(CStoredProcedureExec exec)
        {
            LQBAppraisalOrder order = LQBAppraisalOrder.Load(this.LoanId.ToString(), this.OrderNumber, exec);
            order.ValuationMethod = this.ValuationMethod;
            order.DeliveryMethod = this.DeliveryMethod;
            order.AppraisalFormType = this.AppraisalFormType;
            order.FhaDocFileId = this.FhaDocFileId;
            order.UcdpAppraisalId = this.UcdpAppraisalId;
            order.Notes = this.Notes;
            order.ExpirationDate = this.ExpirationDate;
            order.ReceivedDate = this.ReceivedDate;
            order.SentToBorrowerDate = this.SentToBorrowerDate;
            order.BorrowerReceivedDate = this.BorrowerReceivedDate;
            order.RevisionDate = this.RevisionDate;
            order.ValuationEffectiveDate = this.ValuationEffectiveDate;
            order.SubmittedToFhaDate = this.SubmittedToFhaDate;
            order.SubmittedToUcdpDate = this.SubmittedToUcdpDate;
            order.CuRiskScore = this.CuRiskScore;
            order.OvervaluationRiskT = this.OvervaluationRiskT;
            order.PropertyEligibilityRiskT = this.PropertyEligibilityRiskT;
            order.AppraisalQualityRiskT = this.AppraisalQualityRiskT;
            order.Save(exec);
        }
    }
}
