﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Runtime.Serialization;

    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.Security;

    /// <summary>
    /// Appraisal order interface that includes fields used for generic appraisal order tracking.
    /// </summary>
    [DataContract]
    [KnownType(typeof(ManualAppraisalOrder))]
    [KnownType(typeof(GDMSAppraisalOrderInfo))]
    [KnownType(typeof(LQBAppraisalOrder))]
    [KnownType(typeof(AppraisalOrderView))]
    public abstract class AppraisalOrderBase
    {
        /// <summary>
        /// The date this order was deleted.
        /// </summary>
        private CDateTime deletedDate = CDateTime.InvalidWrapValue;

        /// <summary>
        /// The ID of the user that deleted this order.
        /// </summary>
        private Guid deletedByUserId;

        /// <summary>
        /// The name of the user that deleted this order.
        /// </summary>
        private string deletedByUserName;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderBase"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        public AppraisalOrderBase(Guid loanId, Guid brokerId)
        {
            this.IsValid = true;
            this.AppraisalOrderId = Guid.NewGuid();

            this.LoanId = loanId;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderBase"/> class.
        /// Used for serialization.
        /// </summary>
        protected AppraisalOrderBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderBase"/> class.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        protected AppraisalOrderBase(IDataRecord reader)
            : this(reader.ToDictionary())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalOrderBase"/> class.
        /// </summary>
        /// <param name="reader">Data reader turned dictioanry.</param>
        protected AppraisalOrderBase(IReadOnlyDictionary<string, object> reader)
        {
            this.AppraisalOrderId = (Guid)reader["AppraisalOrderId"];
            this.LoanId = (Guid)reader["LoanId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.IsValid = (bool)reader["IsValid"];
            this.AppraisalOrderType = (AppraisalOrderType)reader["AppraisalOrderType"];
            this.ValuationMethod = (AppraisalValuationMethod)reader["ValuationMethod"];
            this.DeliveryMethod = (AppraisalDeliveryMethod)reader["DeliveryMethod"];
            this.AppraisalFormType = (E_sSpAppraisalFormT)reader["AppraisalFormType"];
            this.ProductName = (string)reader["ProductName"];
            this.OrderNumber = (string)reader["OrderNumber"];
            this.FhaDocFileId = (string)reader["FhaDocFileId"];
            this.UcdpAppraisalId = (string)reader["UcdpAppraisalId"];
            this.Notes = (string)reader["Notes"];
            this.OrderedDate = this.GetCDateTime(reader["OrderedDate"]);
            this.NeededDate = this.GetCDateTime(reader["NeededDate"]);
            this.ExpirationDate = this.GetCDateTime(reader["ExpirationDate"]);
            this.ReceivedDate = this.GetCDateTime(reader["ReceivedDate"]);
            this.SentToBorrowerDate = this.GetCDateTime(reader["SentToBorrowerDate"]);
            this.BorrowerReceivedDate = this.GetCDateTime(reader["BorrowerReceivedDate"]);
            this.RevisionDate = this.GetCDateTime(reader["RevisionDate"]);
            this.ValuationEffectiveDate = this.GetCDateTime(reader["ValuationEffectiveDate"]);
            this.SubmittedToFhaDate = this.GetCDateTime(reader["SubmittedToFha"]);
            this.SubmittedToUcdpDate = this.GetCDateTime(reader["SubmittedToUcdp"]);
            this.CuRiskScore = (decimal)reader["CuRiskScore"];
            this.OvervaluationRiskT = (E_CuRiskT)reader["OvervaluationRiskT"];
            this.PropertyEligibilityRiskT = (E_CuRiskT)reader["PropertyEligibilityRiskT"];
            this.AppraisalQualityRiskT = (E_CuRiskT)reader["AppraisalQualityRiskT"];
            this.DeletedDate = this.GetCDateTime(reader["DeletedDate"]);
            this.DeletedByUserId = reader["DeletedByUserId"] == DBNull.Value ? Guid.Empty : (Guid)reader["DeletedByUserId"];

            this.AppIdAssociatedWithProperty = reader.GetValueOrNull("AppIdAssociatedWithProperty") as Guid?;
            this.LinkedReoId = reader.GetValueOrNull("LinkedReoId") as Guid?;
            this.PropertyAddress = reader.GetValueOrNull("PropertyAddress") as string;
            this.PropertyCity = reader.GetValueOrNull("PropertyCity") as string;
            this.PropertyState = reader.GetValueOrNull("PropertyState") as string;
            this.PropertyZip = reader.GetValueOrNull("PropertyZip") as string;
        }

        /// <summary>
        /// Gets or sets the appraisal order id.
        /// </summary>
        /// <value>Appraisal order id.</value>
        [DataMember(Name = "AppraisalOrderId")]
        public virtual Guid AppraisalOrderId { get; protected set; }

        /// <summary>
        /// Gets or sets the loan ID.
        /// </summary>
        /// <value>A loan ID.</value>
        [DataMember(Name = "LoanId")]
        public virtual Guid LoanId { get; protected set; }

        /// <summary>
        /// Gets or sets the broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        [DataMember(Name = "BrokerId")]
        public virtual Guid BrokerId { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether this order is valid.
        /// </summary>
        /// <value>Whether this order is valid.</value>
        [DataMember(Name = "IsValid")]
        public virtual bool IsValid { get; protected set; }

        /// <summary>
        /// Gets or sets the type of appraisal order this object is.
        /// </summary>
        /// <value>Appraisal order type.</value>
        [DataMember(Name = "AppraisalOrderType")]
        public abstract AppraisalOrderType AppraisalOrderType { get; protected set; }

        /// <summary>
        /// Gets or sets the appraisal valuation method.
        /// </summary>
        /// <value>The appraisal valuation method.</value>
        [DataMember(Name = "ValuationMethod")]
        public virtual AppraisalValuationMethod ValuationMethod { get; set; }

        /// <summary>
        /// Gets or sets the method of delivery.
        /// </summary>
        /// <value>The mothod of delivery.</value>
        [DataMember(Name = "DeliveryMethod")]
        public virtual AppraisalDeliveryMethod DeliveryMethod { get; set; }

        /// <summary>
        /// Gets or sets the appraisal form type.
        /// </summary>
        /// <value>The Appraisal for type.</value>
        [DataMember(Name = "AppraisalFormType")]
        public virtual E_sSpAppraisalFormT AppraisalFormType { get; set; }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        /// <value>The product name.</value>
        [DataMember(Name = "ProductName")]
        public virtual string ProductName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the file number.
        /// </summary>
        /// <value>The file number.</value>
        [DataMember(Name = "OrderNumber")]
        public virtual string OrderNumber { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the FHA doc file ID.
        /// </summary>
        /// <value>The FHA doc file ID.</value>
        [DataMember(Name = "FhaDocFileId")]
        public virtual string FhaDocFileId { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the UCDP appraisal ID.
        /// </summary>
        /// <value>The UCDP appraisal ID.</value>
        [DataMember(Name = "UcdpAppraisalId")]
        public virtual string UcdpAppraisalId { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets user notes.
        /// </summary>
        /// <value>User notes.</value>
        [DataMember(Name = "Notes")]
        public virtual string Notes { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the order date.
        /// </summary>
        /// <value>The order date.</value>
        public virtual CDateTime OrderedDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the order date.
        /// </summary>
        /// <value>The order date in string form.</value>
        [DataMember(Name = "OrderedDate")]
        public virtual string OrderedDate_rep
        {
            get
            {
                return this.OrderedDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.OrderedDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the needed date.
        /// </summary>
        /// <value>The needed date.</value>
        public virtual CDateTime NeededDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the needed date.
        /// </summary>
        /// <value>The nedded date in string form.</value>
        [DataMember(Name = "NeededDate")]
        public virtual string NeededDate_rep
        {
            get
            {
                return this.NeededDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.NeededDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public virtual CDateTime ExpirationDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date in string form.</value>
        [DataMember(Name = "ExpirationDate")]
        public virtual string ExpirationDate_rep
        {
            get
            {
                return this.ExpirationDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.ExpirationDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the received date.
        /// </summary>
        /// <value>The received date.</value>
        public CDateTime ReceivedDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the received date.
        /// </summary>
        /// <value>The received date in string form.</value>
        [DataMember(Name = "ReceivedDate")]
        public virtual string ReceivedDate_rep
        {
            get
            {
                return this.ReceivedDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.ReceivedDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the sent to borrower date.
        /// </summary>
        /// <value>The sent to borrower date.</value>
        public virtual CDateTime SentToBorrowerDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the sent to borrower date.
        /// </summary>
        /// <value>The sent to borrower date in string form.</value>
        [DataMember(Name = "SentToBorrowerDate")]
        public virtual string SentToBorrowerDate_rep
        {
            get
            {
                return this.SentToBorrowerDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.SentToBorrowerDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the borrower received date.
        /// </summary>
        /// <value>The borrower received date.</value>
        public CDateTime BorrowerReceivedDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the borrower received date.
        /// </summary>
        /// <value>The borrower received date in string form.</value>
        [DataMember(Name = "BorrowerReceivedDate")]
        public virtual string BorrowerReceivedDate_rep
        {
            get
            {
                return this.BorrowerReceivedDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.BorrowerReceivedDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the revision date.
        /// </summary>
        /// <value>The revesion date.</value>
        public virtual CDateTime RevisionDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the revision date.
        /// </summary>
        /// <value>The revision date in string form.</value>
        [DataMember(Name = "RevisionDate")]
        public virtual string RevisionDate_rep
        {
            get
            {
                return this.RevisionDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.RevisionDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the valuation effective date.
        /// </summary>
        /// <value>The valuation effective date.</value>
        public virtual CDateTime ValuationEffectiveDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the valuation effective date.
        /// </summary>
        /// <value>The valuation effective date in string form.</value>
        [DataMember(Name = "ValuationEffectiveDate")]
        public virtual string ValuationEffectiveDate_rep
        {
            get
            {
                return this.ValuationEffectiveDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.ValuationEffectiveDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the submitted to FHA date.
        /// </summary>
        /// <value>The submitted to FHA date.</value>
        public virtual CDateTime SubmittedToFhaDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the submitted to FHA date.
        /// </summary>
        /// <value>The submitted to FHA date in string form.</value>
        [DataMember(Name = "SubmittedToFhaDate")]
        public virtual string SubmittedToFhaDate_rep
        {
            get
            {
                return this.SubmittedToFhaDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.SubmittedToFhaDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the submitted to UCDP date.
        /// </summary>
        /// <value>The submitted to UCDP date.</value>
        public virtual CDateTime SubmittedToUcdpDate { get; set; } = CDateTime.InvalidWrapValue;

        /// <summary>
        /// Gets or sets the submitted to UCDP date.
        /// </summary>
        /// <value>The submitted to UCDP date in string form.</value>
        [DataMember(Name = "SubmittedToUcdpDate")]
        public virtual string SubmittedToUcdpDate_rep
        {
            get
            {
                return this.SubmittedToUcdpDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.SubmittedToUcdpDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the Collateral Underwriter risk score, aka <seealso cref="CPageData.sSpCuScore"/>.
        /// </summary>
        /// <value>The Collateral Underwriter risk score.</value>
        public virtual decimal CuRiskScore { get; set; }

        /// <summary>
        /// Gets or sets the string form of the Collateral Underwriter risk score, aka <seealso cref="CPageData.sSpCuScore"/>.
        /// </summary>
        /// <value>The Collateral Underwriter risk score.</value>
        [DataMember(Name = "CuRiskScore")]
        public string CuRiskScore_rep
        {
            get { return this.Converter.ToDecimalString_sSpCuScore(this.CuRiskScore); }
            set { this.CuRiskScore = this.Converter.ToDecimal(value); }
        }

        /// <summary>
        /// Gets or sets the Collateral Underwriter overvaluation risk, aka <seealso cref="CPageData.sSpOvervaluationRiskT"/>.
        /// </summary>
        /// <value>The Collateral Underwriter overvaluation risk.</value>
        [DataMember(Name = "OvervaluationRiskT")]
        public virtual E_CuRiskT OvervaluationRiskT { get; set; }

        /// <summary>
        /// Gets or sets the Collateral Underwriter property eligibility risk, aka <seealso cref="CPageData.sSpPropertyEligibilityRiskT"/>.
        /// </summary>
        /// <value>The Collateral Underwriter property eligibility risk.</value>
        [DataMember(Name = "PropertyEligibilityRiskT")]
        public virtual E_CuRiskT PropertyEligibilityRiskT { get; set; }

        /// <summary>
        /// Gets or sets the Collateral Underwriter appraisal quality risk, aka <seealso cref="CPageData.sSpAppraisalQualityRiskT"/>.
        /// </summary>
        /// <value>The Collateral Underwriter appraisal quality risk.</value>
        [DataMember(Name = "AppraisalQualityRiskT")]
        public virtual E_CuRiskT AppraisalQualityRiskT { get; set; }

        /// <summary>
        /// Gets or sets the date this order was deleted.
        /// </summary>
        /// <value>The date this order was deleted.</value>
        public virtual CDateTime DeletedDate
        {
            get
            {
                if (this.IsValid)
                {
                    return CDateTime.InvalidWrapValue;
                }

                return this.deletedDate;
            }

            set
            {
                this.deletedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the date this order was deleted.
        /// </summary>
        /// <value>The date this order was deleted, in string form.</value>
        [DataMember(Name = "DeletedDate")]
        public virtual string DeletedDate_rep
        {
            get
            {
                return this.DeletedDate.ToStringWithDefaultFormatting();
            }

            set
            {
                this.DeletedDate = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets the ID of the user that deleted this order.
        /// </summary>
        /// <value>The ID of the user that deleted this order.</value>
        public virtual Guid DeletedByUserId
        {
            get
            {
                if (this.IsValid)
                {
                    return Guid.Empty;
                }

                return this.deletedByUserId;
            }

            set
            {
                this.deletedByUserId = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the user that deleted this order.
        /// </summary>
        /// <value>The name of the user that deleted this order.</value>
        [DataMember(Name = "DeletedByUserName")]
        public string DeletedByUserName
        {
            get
            {
                if (this.IsValid || this.DeletedByUserId == Guid.Empty)
                {
                    return string.Empty;
                }

                if (string.IsNullOrEmpty(this.deletedByUserName))
                {
                    EmployeeDB employee = EmployeeDB.RetrieveByUserId(this.BrokerId, this.deletedByUserId);
                    this.deletedByUserName = employee.FullName;
                }

                return this.deletedByUserName;
            }

            protected set
            {
                // Do nothing. Needed for serialization.
            }
        }

        /// <summary>
        /// Gets or sets the app associated with the order, as determined by the chosen property.
        /// </summary>
        /// <remarks>
        /// Stamped in case the REO gets deleted.
        /// </remarks>
        [DataMember(Name = "AppIdAssociatedWithProperty")]
        public Guid? AppIdAssociatedWithProperty
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the linked REO Guid id.
        /// </summary>
        [DataMember(Name = "LinkedReoId")]
        public Guid? LinkedReoId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets a value indicating whether this appraisal has a linked REO.
        /// </summary>
        [DataMember(Name = "HasLinkedReo")]
        public bool HasLinkedReo
        {
            get
            {
                return this.LinkedReoId.HasValue && this.LinkedReoId.Value != Guid.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the property address specified when the appraisal was ordered.
        /// </summary>
        [DataMember(Name = "PropertyAddress")]
        public string PropertyAddress
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the city specified when the appraisal was ordered.
        /// </summary>
        [DataMember(Name = "PropertyCity")]
        public string PropertyCity
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the state specified when the appraisal was ordered.
        /// </summary>
        [DataMember(Name = "PropertyState")]
        public string PropertyState
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the zip code specified when the appraisal was ordered.
        /// </summary>
        [DataMember(Name = "PropertyZip")]
        public string PropertyZip
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets a converter used to format data.
        /// </summary>
        /// <value>A converter used to format data.</value>
        protected LosConvert Converter { get; } = new LosConvert(FormatTarget.Webform);

        /// <summary>
        /// Gets the edoc ids of all the edocs associated with this appraisal order.
        /// </summary>
        protected abstract IEnumerable<Guid> AssociatedEdocIds { get; }

        /// <summary>
        /// Gets the linked REO from the loan. Or null if not linked to one.
        /// </summary>
        /// <param name="dataLoan">The data loan to pull from.</param>
        /// <returns>The REO or null if not linked to one.</returns>
        public IRealEstateOwned GetLinkedReo(CPageData dataLoan)
        {
            if (!this.HasLinkedReo || !this.AppIdAssociatedWithProperty.HasValue)
            {
                return null;
            }

            CAppData associatedApp = dataLoan.GetAppData(this.AppIdAssociatedWithProperty.Value);
            if (associatedApp == null)
            {
                return null;
            }

            return associatedApp.aReCollection.GetRecordOf(this.LinkedReoId.Value) as IRealEstateOwned;
        }

        /// <summary>
        /// Converts this appraisal order into an appraisal order view.
        /// </summary>
        /// <param name="principal">The principal of the user generating the views.</param>
        /// <returns>Appraisal order view representation of the order.</returns>
        /// <remarks>Just calls AppraisalOrderView constructor, which takes care of the rest.</remarks>
        public virtual AppraisalOrderView ToView(AbstractUserPrincipal principal)
        {
            return new AppraisalOrderView(this);
        }

        /// <summary>
        /// Gets the document views for all edocs associated with this appraisal order.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The views for all edocs.</returns>
        public List<AppraisalDocumentView> GetEdocDocumentViews(AbstractUserPrincipal principal)
        {
            return this.GetDocumentViewsForEdocs(principal, this.AssociatedEdocIds);
        }

        /// <summary>
        /// Sets the property info for the appraisal order.
        /// </summary>
        /// <param name="requestDetails">The request details to populate property order from.</param>
        public void SetPropertyInfo(Integration.Appraisals.AbstractOrderRequestDetails requestDetails)
        {
            this.AppIdAssociatedWithProperty = requestDetails.AppIdAssociatedWithProperty;
            this.LinkedReoId = requestDetails.LinkedReoId;
            this.PropertyAddress = requestDetails.Property?.Address;
            this.PropertyCity = requestDetails.Property?.City;
            this.PropertyState = requestDetails.Property?.State;
            this.PropertyZip = requestDetails.Property?.Zip;
        }

        /// <summary>
        /// Used to convert smalldatetime info on DB to a CDateTime.
        /// Handles NULL columns.
        /// </summary>
        /// <param name="dateTime">A DateTime or DBNull object retreived from a data reader.</param>
        /// <returns>A CDateTime Object.</returns>
        protected CDateTime GetCDateTime(object dateTime)
        {
            if (dateTime == null || dateTime == DBNull.Value)
            {
                return CDateTime.InvalidWrapValue;
            }

            return CDateTime.Create((DateTime)dateTime);
        }

        /// <summary>
        /// Constructs a list of document views given the list of edoc ids.
        /// </summary>
        /// <param name="principal">The principal of the user generating the views.</param>
        /// <param name="edocIds">The edoc ids.</param>
        /// <returns>The list of document views.</returns>
        protected List<AppraisalDocumentView> GetDocumentViewsForEdocs(AbstractUserPrincipal principal, IEnumerable<Guid> edocIds)
        {
            List<AppraisalDocumentView> docViews = new List<AppraisalDocumentView>();
            foreach (Guid edocId in edocIds)
            {
                AppraisalDocumentView docView = new AppraisalDocumentView();

                EDocumentRepository repo = EDocumentRepository.GetUserRepository(principal);
                EDocument edoc;
                try
                {
                    edoc = repo.GetDocumentById(edocId);
                    docView.FileName = edoc.FolderAndDocTypeName + ".pdf";
                    docView.ViewUrl = string.Format("javascript:openDoc('{0}');", edocId);
                    docView.EDocId = edocId;
                    docView.Folder = edoc.Folder.FolderNm;
                    docView.DocType = edoc.DocType.DocTypeName;
                    docView.Description = edoc.PublicDescription;
                    docView.Comments = edoc.Comments;
                    docView.LastModifiedDate = edoc.LastModifiedDate.ToString();
                    docView.EdocMetaData = Tools.CreateDocMetaDataTooltipDataAttributeValue(edoc);
                }
                catch (NotFoundException)
                {
                    edoc = repo.GetDeletedDocumentById(edocId);
                    docView.IsDeleted = true;
                    docView.FileName = "[Deleted] " + edoc.FolderAndDocTypeName + ".pdf";
                    docView.Title = "To restore, go to EDocs\\Document List and click \"Restore deleted docs...\"";
                }
                catch (AccessDenied)
                {
                    continue; // OPM 457416 - Don't include Docs the user does not have permission to view.
                }

                docViews.Add(docView);
            }

            return docViews;
        }

        /// <summary>
        /// Gets a list of parameters for saving order to DB.
        /// </summary>
        /// <returns>List of parameter.</returns>
        protected List<SqlParameter> GetBaseParameters()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@AppraisalOrderId", this.AppraisalOrderId));
            parameters.Add(new SqlParameter("@LoanId", this.LoanId));
            parameters.Add(new SqlParameter("@BrokerId", this.BrokerId));
            parameters.Add(new SqlParameter("@IsValid", this.IsValid));
            parameters.Add(new SqlParameter("@AppraisalOrderType", this.AppraisalOrderType));
            parameters.Add(new SqlParameter("@ValuationMethod", this.ValuationMethod));
            parameters.Add(new SqlParameter("@DeliveryMethod", this.DeliveryMethod));
            parameters.Add(new SqlParameter("@AppraisalFormType", this.AppraisalFormType));
            parameters.Add(new SqlParameter("@ProductName", Tools.TruncateString(this.ProductName, 100)));
            parameters.Add(new SqlParameter("@OrderNumber", Tools.TruncateString(this.OrderNumber, 50)));
            parameters.Add(new SqlParameter("@FhaDocFileId", Tools.TruncateString(this.FhaDocFileId, 50)));
            parameters.Add(new SqlParameter("@UcdpAppraisalId", Tools.TruncateString(this.UcdpAppraisalId, 50)));
            parameters.Add(new SqlParameter("@Notes", this.Notes));
            parameters.Add(new SqlParameter("@OrderedDate", this.OrderedDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@NeededDate", this.NeededDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@ExpirationDate", this.ExpirationDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@ReceivedDate", this.ReceivedDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@SentToBorrowerDate", this.SentToBorrowerDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@BorrowerReceivedDate", this.BorrowerReceivedDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@RevisionDate", this.RevisionDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@ValuationEffectiveDate", this.ValuationEffectiveDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@SubmittedToFha", this.SubmittedToFhaDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@SubmittedToUcdp", this.SubmittedToUcdpDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@CuRiskScore", this.CuRiskScore));
            parameters.Add(new SqlParameter("@OvervaluationRiskT", this.OvervaluationRiskT));
            parameters.Add(new SqlParameter("@PropertyEligibilityRiskT", this.PropertyEligibilityRiskT));
            parameters.Add(new SqlParameter("@AppraisalQualityRiskT", this.AppraisalQualityRiskT));
            parameters.Add(new SqlParameter("@DeletedDate", this.DeletedDate.DateTimeForDBWriting));
            parameters.Add(new SqlParameter("@AppIdAssociatedWithProperty", this.AppIdAssociatedWithProperty));
            parameters.Add(new SqlParameter("@LinkedReoId", this.LinkedReoId));
            parameters.Add(new SqlParameter("@PropertyAddress", this.PropertyAddress));
            parameters.Add(new SqlParameter("@PropertyCity", this.PropertyCity));
            parameters.Add(new SqlParameter("@PropertyState", this.PropertyState));
            parameters.Add(new SqlParameter("@PropertyZip", this.PropertyZip));

            if (this.DeletedByUserId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@DeletedByUserId", this.DeletedByUserId));
            }
            
            return parameters;
        }
    }
}
