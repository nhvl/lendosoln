﻿namespace LendersOffice.ObjLib.Appraisal
{
    /// <summary>
    /// Appraisal Valuation Methods.
    /// </summary>
    public enum AppraisalValuationMethod
    {
        /// <summary>
        /// Blank. Empty. Nothing. None.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// AUS findings.
        /// </summary>
        AusFindings = 1,

        /// <summary>
        /// An Appraisal.
        /// </summary>
        Appraisal = 2,

        /// <summary>
        /// Desk review.
        /// </summary>
        DeskReview = 3,

        /// <summary>
        /// AVM (padding).
        /// </summary>
        Avm = 4
    }
}
