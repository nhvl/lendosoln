﻿namespace LendersOffice.ObjLib.Appraisal
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Used to represent an appraisal document for appraisal tracking UI.
    /// </summary>
    [DataContract]
    public class AppraisalDocumentView
    {
        /// <summary>
        /// Gets or sets the document filename.
        /// </summary>
        /// <value>The document filename.</value>
        [DataMember(Name = "FileName")]
        public string FileName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the URL the documant can be viewed at.
        /// </summary>
        /// <value>The URL the document can be viewed at.</value>
        [DataMember(Name = "ViewUrl")]
        public string ViewUrl { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets title (tool tip) text for doc view link.
        /// </summary>
        /// <value>Tool tip text for doc view link.</value>
        [DataMember(Name = "Title")]
        public string Title { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the edoc id, if this document is an edoc.
        /// </summary>
        public Guid? EDocId { get; set; } = null;

        /// <summary>
        /// Gets or sets the folder.
        /// </summary>
        public string Folder { get; set; }

        /// <summary>
        /// Gets or sets the doc type.
        /// </summary>
        public string DocType { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the last modified date.
        /// </summary>
        public string LastModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the document was deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the edoc meta data. For the popup.
        /// </summary>
        [DataMember(Name = "EdocMetaData")]
        public string EdocMetaData { get; set; } = null;
    }
}
