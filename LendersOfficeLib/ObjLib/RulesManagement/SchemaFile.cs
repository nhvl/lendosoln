using System;
using System.Reflection;
using System.IO;
using ExcelLibraryClient;

namespace LendersOfficeApp.ObjLib.RulesManagement
{
	public class SchemaFile
	{
        private string m_compactedSchemaFilePath;
		public string ValidateFailureReason ;

        public string Version
        {
            get
            {
                if (string.IsNullOrEmpty(m_compactedSchemaFilePath) == false)
                {
                    FileInfo info = new FileInfo(m_compactedSchemaFilePath);
                    if (info.Exists)
                    {
                        return File.GetLastWriteTime(m_compactedSchemaFilePath).ToString();
                    }
                }
                return string.Empty;
            }
        }
		public SchemaFile(string sCompactedSchemaFilePath)
		{
            m_compactedSchemaFilePath = sCompactedSchemaFilePath;
		}
		public bool Validate(string inputXlsFile)
		{
            return ExcelLibrary.ValidateCompactSchemaFile(m_compactedSchemaFilePath, inputXlsFile, out ValidateFailureReason);
		}
	}
}
