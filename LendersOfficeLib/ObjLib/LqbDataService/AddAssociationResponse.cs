﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The response data for an AddAssociation command.
    /// </summary>
    public sealed class AddAssociationResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddAssociationResponse"/> class.
        /// </summary>
        /// <param name="command">The add association command.</param>
        /// <param name="associationId">The identifier for the added association.</param>
        public AddAssociationResponse(AddAssociation command, Guid associationId)
        {
            this.Command = command;
            this.AssociationId = associationId.ToString("D");
        }

        /// <summary>
        /// Gets the identifier for the added association.
        /// </summary>
        public string AssociationId { get; }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public string Path
        {
            get { return this.Command.Path.ToString(); }
        }

        /// <summary>
        /// Gets the collection to which a new entity will get added.
        /// </summary>
        public string AssociationSet
        {
            get { return this.Command.AssociationSet; }
        }

        /// <summary>
        /// Gets the identifier for the first entity.
        /// </summary>
        public string FirstIdentifier
        {
            get { return this.Command.FirstIdentifier.ToString("D"); }
        }

        /// <summary>
        /// Gets the identifier for the second entity.
        /// </summary>
        public string SecondIdentifier
        {
            get { return this.Command.SecondIdentifier.ToString("D"); }
        }

        /// <summary>
        /// Gets the command.
        /// </summary>
        private AddAssociation Command { get; }
    }
}
