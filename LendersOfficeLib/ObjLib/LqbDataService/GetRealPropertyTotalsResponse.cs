﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The data needed to generate a response for the real property totals.
    /// </summary>
    public class GetRealPropertyTotalsResponse : GetTotalsResponse<DataObjectKind.RealProperty>
    {
        /// <summary>
        /// The totals result from the command.
        /// </summary>
        private readonly RealPropertyTotals totals;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetRealPropertyTotalsResponse"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="totals">The totals result from the command.</param>
        /// <param name="formatter">The formatter.</param>
        internal GetRealPropertyTotalsResponse(GetTotals<DataObjectKind.RealProperty> command, RealPropertyTotals totals, IFormatAsString formatter)
            : base(command, formatter)
        {
            this.totals = totals;
        }

        /// <summary>
        /// Gets the total market value where the real property status is not sold.
        /// </summary>
        public string MarketValue => this.Formatter.Format(this.totals.MarketValue);

        /// <summary>
        /// Gets the total mortgage amount where the real property status is not sold.
        /// </summary>
        public string MortgageAmount => this.Formatter.Format(this.totals.MortgageAmount);

        /// <summary>
        /// Gets the total net rental income for real properties in the Rental status.
        /// </summary>
        public string RentalNetRentalIncome => this.Formatter.Format(this.totals.RentalNetRentalIncome);

        /// <summary>
        /// Gets the total net rental income for the real properties where the status is Retained.
        /// </summary>
        public string RetainedNetRentalIncome => this.Formatter.Format(this.totals.RetainedNetRentalIncome);
    }
}
