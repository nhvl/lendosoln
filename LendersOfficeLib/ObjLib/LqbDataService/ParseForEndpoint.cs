﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Parser for the endpoint, where we want to ensure that enum values are translated from integers.
    /// </summary>
    internal class ParseForEndpoint : ParseEntityFromString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseForEndpoint"/> class.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        public ParseForEndpoint(FormatTarget target, FormatDirection direction)
            : base(target, direction)
        {
        }

        /// <summary>
        /// Parse the input string into an enumeration value.
        /// </summary>
        /// <typeparam name="E">The expected type, which must be an enumeration.</typeparam>
        /// <param name="value">The input string to be parsed, which should be an integer.</param>
        /// <returns>The enumeration value, or null.</returns>
        public override E? TryParseEnum<E>(string value)
        {
            if (!typeof(E).IsEnum)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            long longValue;
            bool isNumeric = long.TryParse(value, out longValue);
            if (isNumeric)
            {
                foreach (var possibleValue in Enum.GetValues(typeof(E)))
                {
                    long test = Convert.ToInt64(possibleValue);
                    if (test == longValue)
                    {
                        return (E)Enum.ToObject(typeof(E), possibleValue);
                    }
                }
            }

            return null;
        }
    }
}
