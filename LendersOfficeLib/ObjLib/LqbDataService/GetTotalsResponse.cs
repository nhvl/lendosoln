﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Base class for totals responses.
    /// </summary>
    /// <typeparam name="T">The data object kind for which totals were retrieved.</typeparam>
    public abstract class GetTotalsResponse<T> where T : DataObjectKind
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetTotalsResponse{T}"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="formatter">The formatter.</param>
        internal GetTotalsResponse(GetTotals<T> command, IFormatAsString formatter)
        {
            this.Command = command;
            this.Formatter = formatter;
        }

        /// <summary>
        /// Gets the path from the command.
        /// </summary>
        public string Path => this.Command.Path.ToString();

        /// <summary>
        /// Gets the totals type id from the command.
        /// </summary>
        public string TotalsType => this.Command.TotalsType.ToString();

        /// <summary>
        /// Gets the identifier from the command.
        /// </summary>
        public string Id => this.Command.Id?.ToString() ?? null;

        /// <summary>
        /// Gets the command.
        /// </summary>
        protected GetTotals<T> Command { get; private set; }

        /// <summary>
        /// Gets the formatter.
        /// </summary>
        protected IFormatAsString Formatter { get; private set; }
    }
}
