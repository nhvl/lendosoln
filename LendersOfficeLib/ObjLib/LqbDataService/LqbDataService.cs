﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Provides the functionality needed to process an endpoint request.
    /// </summary>
    public class LqbDataService
    {
        /// <summary>
        /// Processes a data request and returns the response.
        /// </summary>
        /// <param name="loanId">The loan id for the request.</param>
        /// <param name="operation">The operation to process.</param>
        /// <param name="jsonRequest">The request json.</param>
        /// <returns>The response as a string.</returns>
        public static string ProcessRequest(DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId, LqbDataServiceOperation operation, string jsonRequest)
        {
            var formatTarget = FormatTarget.LqbDataService;
            var converter = new LosConvert(formatTarget);

            var parsedJson = JObject.Parse(jsonRequest);
            var request = new LqbDataRequest(parsedJson, converter);

            // Borrower management commands get their own block before we load any loan object, since the legacy
            // operations require all sorts of funk fiddling with the loan's data state and the data rows loaded
            var borrowerManagementAddResponses = new List<ServiceMessage<BorrowerManagementAddResponse>>();
            if (request.BorrowerManagementCommands != null)
            {
                if (operation != LqbDataServiceOperation.Save)
                {
                    throw CBaseException.GenericException("Running a borrower management command is only supported as a save operation and is not available for recalculation.");
                }

                foreach (var command in request.BorrowerManagementCommands)
                {
                    Guid? addedId = command.Message.Handle(loanId);
                    if (addedId.HasValue)
                    {
                        borrowerManagementAddResponses.Add(ServiceMessage.Create(command.OperationId, command.CommandName, new BorrowerManagementAddResponse(addedId.Value)));
                    }
                }
            }

            var loan = new CPageData(loanId.Value, request.LoanDependencies);
            loan.SetFormatTarget(formatTarget);
            if (operation == LqbDataServiceOperation.Save)
            {
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            else
            {
                loan.InitLoad();
            }

            var aggregate = new LqbDataServiceAggregate(loan);
            var formatter = new FormatForEndpoint(formatTarget, FormatDirection.ToRep);
            var parser = new ParseForEndpoint(formatTarget, FormatDirection.ToDb);

            if (request.RemoveAssociationCommands != null)
            {
                foreach (var removeAssociation in request.RemoveAssociationCommands)
                {
                    HandleRemove(removeAssociation.Message, aggregate);
                }
            }

            if (request.RemoveEntityCommands != null)
            {
                foreach (var removeEntity in request.RemoveEntityCommands)
                {
                    HandleRemove(removeEntity.Message, aggregate);
                }
            }

            List<ServiceMessage<AddEntityResponse>> addEntityResponses = null;
            if (request.AddEntityCommands != null)
            {
                addEntityResponses = new List<ServiceMessage<AddEntityResponse>>();
                foreach (var addEntity in request.AddEntityCommands)
                {
                    Guid addedIdentifier = HandleAdd(addEntity.Message, aggregate, parser);
                    addEntityResponses.Add(ServiceMessage.Create(addEntity.OperationId, addEntity.CommandName, new AddEntityResponse(addEntity.Message, addedIdentifier)));
                }
            }

            List<ServiceMessage<AddAssociationResponse>> addAssocResponses = null;
            if (request.AddAssociationCommands != null)
            {
                addAssocResponses = new List<ServiceMessage<AddAssociationResponse>>();
                foreach (var addAssociation in request.AddAssociationCommands)
                {
                    Guid addedIdentifier = HandleAdd(addAssociation.Message, aggregate);
                    addAssocResponses.Add(ServiceMessage.Create(addAssociation.OperationId, addAssociation.CommandName, new AddAssociationResponse(addAssociation.Message, addedIdentifier)));
                }
            }

            if (request.SetOwnershipCommand != null)
            {
                HandleSetOwnership(request.SetOwnershipCommand.Message, aggregate);
            }

            if (request.SetOrderCommand != null)
            {
                HandleSetOrder(request.SetOrderCommand.Message, aggregate);
            }

            if (request.SetRequestNodeTree != null)
            {
                LqbSetRequestResolver.ResolveSetRequest(request.SetRequestNodeTree.Message, aggregate, parser);
            }

            foreach (var command in request.AddLiabilityBalanceAndPaymentToRealPropertyCommands)
            {
                HandleAddLiabilityBalanceAndPaymentToRealProperty(command.Message, aggregate);
            }

            // begin data query
            var assetTotalsResponses = new List<ServiceMessage<GetAssetTotalsResponse>>();
            foreach (var getTotals in request.GetAssetTotalsCommands)
            {
                var totals = HandleGetAssetTotals(getTotals.Message, aggregate);
                assetTotalsResponses.Add(ServiceMessage.Create(getTotals.OperationId, getTotals.CommandName, new GetAssetTotalsResponse(getTotals.Message, totals, formatter)));
            }

            var liabilityTotalsResponses = new List<ServiceMessage<GetLiabilityTotalsResponse>>();
            foreach (var getTotals in request.GetLiabilityTotalsCommands)
            {
                var totals = HandleGetLiabilityTotals(getTotals.Message, aggregate);
                liabilityTotalsResponses.Add(ServiceMessage.Create(getTotals.OperationId, getTotals.CommandName, new GetLiabilityTotalsResponse(getTotals.Message, totals, formatter)));
            }

            var realPropertyTotalsResponses = new List<ServiceMessage<GetRealPropertyTotalsResponse>>();
            foreach (var getTotals in request.GetRealPropertyTotalsCommands)
            {
                var totals = HandleGetRealPropertyTotals(getTotals.Message, aggregate);
                realPropertyTotalsResponses.Add(ServiceMessage.Create(getTotals.OperationId, getTotals.CommandName, new GetRealPropertyTotalsResponse(getTotals.Message, totals, formatter)));
            }

            var incomeSourceTotalsResponse = new List<ServiceMessage<GetIncomeSourceTotalsResponse>>(request.GetIncomeSourceTotalsCommands.Count);
            foreach (var getTotals in request.GetIncomeSourceTotalsCommands)
            {
                var totals = HandleGetIncomeSourceTotals(getTotals.Message, aggregate);
                incomeSourceTotalsResponse.Add(ServiceMessage.Create(getTotals.OperationId, getTotals.CommandName, new GetIncomeSourceTotalsResponse(getTotals.Message, totals, formatter)));
            }

            ServiceMessage<GetResponseNode> getResponseNodeTree = null;
            if (request.GetRequestNodeTree != null)
            {
                var getRequest = request.GetRequestNodeTree;
                getResponseNodeTree = new ServiceMessage<GetResponseNode>(getRequest.OperationId, getRequest.CommandName, LqbGetRequestResolver.ResolveGetRequest(getRequest.Message, aggregate, formatter));
            }

            if (operation == LqbDataServiceOperation.Save)
            {
                loan.Save();
            }

            var response = new LqbDataResponse
            {
                AddEntityResponses = addEntityResponses,
                AddAssociationResponses = addAssocResponses,
                GetResponse = getResponseNodeTree,
                GetAssetTotalsResponses = assetTotalsResponses,
                GetLiabilityTotalsResponses = liabilityTotalsResponses,
                GetRealPropertyTotalsResponses = realPropertyTotalsResponses,
                GetIncomeSourceTotalsResponses = incomeSourceTotalsResponse,
                BorrowerManagementAddResponses = borrowerManagementAddResponses,
            };
            var jsonResponse = response.ToJObject();
            return jsonResponse.ToString();
        }

        /// <summary>
        /// Generates an error response with the given error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The error response json.</returns>
        public static string ErrorResponse(string message)
        {
            JObject response = LqbDataResponse.ErrorResponse(message);
            return response.ToString();
        }

        /// <summary>
        /// Handles a remove entity command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        private static void HandleRemove(RemoveEntity command, LqbDataServiceAggregate aggregate)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var removeHandler = target as IRemoveEntityHandler;
            if (removeHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for RemoveEntity did not implement IRemoveEntityHandler. Path: " + command.Path);
            }

            removeHandler.HandleRemove(command);
        }

        /// <summary>
        /// Handles a remove association command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        private static void HandleRemove(RemoveAssociation command, LqbDataServiceAggregate aggregate)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var removeHandler = target as ILoanCommandHandler;
            if (removeHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for RemoveAssociation did not implement ILoanCommandHandler. Path: " + command.Path);
            }

            removeHandler.HandleRemove(command);
        }

        /// <summary>
        /// Handles an add entity command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <param name="parser">The parser to conver string values to semantic data types.</param>
        /// <returns>The identifier for the added entity.</returns>
        private static Guid HandleAdd(AddEntity command, LqbDataServiceAggregate aggregate, IParseFromString parser)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var addHandler = target as IAddEntityHandler;
            if (addHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for AddEntity did not implement IAddEntityHandler. Path: " + command.Path);
            }

            return addHandler.HandleAdd(command, parser);
        }

        /// <summary>
        /// Handles an add association command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <returns>The identifier for the added association.</returns>
        private static Guid HandleAdd(AddAssociation command, LqbDataServiceAggregate aggregate)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var addHandler = target as ILoanCommandHandler;
            if (addHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for AddEntity did not implement ILoanCommandHandler. Path: " + command.Path);
            }

            return addHandler.HandleAdd(command);
        }

        /// <summary>
        /// Handles a set ownership command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        private static void HandleSetOwnership(SetOwnership command, LqbDataServiceAggregate aggregate)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var setOwnershipHandler = target as ILoanCommandHandler;
            if (setOwnershipHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for SetOwnership did not implement ILoanCommandHandler. Path: " + command.Path);
            }

            setOwnershipHandler.HandleSetOwnership(command);
        }

        /// <summary>
        /// Handles a set order command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        private static void HandleSetOrder(SetOrder command, LqbDataServiceAggregate aggregate)
        {
            var target = PathResolver.Get(aggregate, command.Path);

            var setOrderHandler = target as ILoanCommandHandler;
            if (setOrderHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target for SetOrder did not implement ILoanCommandHandler. Path: " + command.Path);
            }

            setOrderHandler.HandleSetOrder(command);
        }

        /// <summary>
        /// Handles a set of commands to add liability balance and payment to a real property.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        private static void HandleAddLiabilityBalanceAndPaymentToRealProperty(AddLiabilityBalanceAndPaymentToRealProperty command, LqbDataServiceAggregate aggregate)
        {
            GetLoanCommandHandler(aggregate, command.Path).HandleAddLiabilityBalanceAndPaymentToRealProperty(command);
        }

        /// <summary>
        /// Handles a get asset totals command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <returns>The asset totals returned by the command.</returns>
        private static AssetTotals HandleGetAssetTotals(GetTotals<DataObjectKind.Asset> command, LqbDataServiceAggregate aggregate)
        {
            return GetLoanCommandHandler(aggregate, command.Path).HandleGetAssetTotals(command);
        }

        /// <summary>
        /// Handles a get liability totals command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <returns>The liability totals returned by the command.</returns>
        private static LiabilityTotals HandleGetLiabilityTotals(GetTotals<DataObjectKind.Liability> command, LqbDataServiceAggregate aggregate)
        {
            return GetLoanCommandHandler(aggregate, command.Path).HandleGetLiabilityTotals(command);
        }

        /// <summary>
        /// Handles a get real property totals command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <returns>The real property totals returned by the command.</returns>
        private static RealPropertyTotals HandleGetRealPropertyTotals(GetTotals<DataObjectKind.RealProperty> command, LqbDataServiceAggregate aggregate)
        {
            return GetLoanCommandHandler(aggregate, command.Path).HandleGetRealPropertyTotals(command);
        }

        /// <summary>
        /// Handles a get income source totals command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="aggregate">The aggregate.</param>
        /// <returns>The income source totals returned by the command.</returns>
        private static IncomeSourceTotals HandleGetIncomeSourceTotals(GetTotals<DataObjectKind.IncomeSource> command, LqbDataServiceAggregate aggregate)
        {
            return GetLoanCommandHandler(aggregate, command.Path).HandleGetIncomeSourceTotals(command);
        }

        /// <summary>
        /// Gets the loan command handler for the specified path.
        /// </summary>
        /// <param name="aggregate">The aggregate.</param>
        /// <param name="path">The path to be handled.</param>
        /// <returns>The loan command handler.</returns>
        /// <exception cref="CBaseException">The <paramref name="path"/> on <paramref name="aggregate"/> was not available.</exception>
        private static ILoanCommandHandler GetLoanCommandHandler(LqbDataServiceAggregate aggregate, LqbGrammar.DataTypes.PathDispatch.DataPath path)
        {
            var target = PathResolver.Get(aggregate, path);

            var loanCommandHandler = target as ILoanCommandHandler;
            if (loanCommandHandler == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Target did not implement ILoanCommandHandler. Path: " + path);
            }

            return loanCommandHandler;
        }
    }
}
