﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A response node for a get request.
    /// </summary>
    public class GetResponseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetResponseNode"/> class.
        /// </summary>
        /// <param name="pathComponent">The path component.</param>
        /// <param name="children">The child nodes.</param>
        /// <param name="value">The value for this node.</param>
        public GetResponseNode(IDataPathElement pathComponent, IReadOnlyList<GetResponseNode> children, object value)
        {
            this.PathComponent = pathComponent;
            this.Children = children;
            this.Value = value;
        }

        /// <summary>
        /// Gets the path component associated with this node.
        /// </summary>
        public IDataPathElement PathComponent { get; }

        /// <summary>
        /// Gets the child nodes.
        /// </summary>
        public IReadOnlyList<GetResponseNode> Children { get; }

        /// <summary>
        /// Gets the value associated with this node.
        /// </summary>
        public object Value { get; }
    }
}
