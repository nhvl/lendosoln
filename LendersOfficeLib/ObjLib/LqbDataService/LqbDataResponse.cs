﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Provides the functionality needed to generated LqbDataService responses.
    /// </summary>
    public class LqbDataResponse
    {
        /// <summary>
        /// Gets or sets the response for the "Get" request.
        /// </summary>
        public ServiceMessage<GetResponseNode> GetResponse { get; set; }

        /// <summary>
        /// Gets or sets the response for the "addEntity" request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<AddEntityResponse>> AddEntityResponses { get; set; }

        /// <summary>
        /// Gets or sets the response for the "addAssociation" request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<AddAssociationResponse>> AddAssociationResponses { get; set; }

        /// <summary>
        /// Gets or sets the get asset totals response.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetAssetTotalsResponse>> GetAssetTotalsResponses { get; set; }

        /// <summary>
        /// Gets or sets the get liability totals response.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetLiabilityTotalsResponse>> GetLiabilityTotalsResponses { get; set; }

        /// <summary>
        /// Gets or sets the get real property totals response.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetRealPropertyTotalsResponse>> GetRealPropertyTotalsResponses { get; set; }

        /// <summary>
        /// Gets or sets the get income source totals response.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetIncomeSourceTotalsResponse>> GetIncomeSourceTotalsResponses { get; set; }

        /// <summary>
        /// Gets or sets the borrower management add command response.
        /// </summary>
        public IReadOnlyList<ServiceMessage<BorrowerManagementAddResponse>> BorrowerManagementAddResponses { get; set; }

        /// <summary>
        /// Generates an error response with the given error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The error response json.</returns>
        public static JObject ErrorResponse(string message)
        {
            return JObject.FromObject(new
            {
                data = new
                {
                    error = message
                }
            });
        }

        /// <summary>
        /// Converts the response to json.
        /// </summary>
        /// <returns>The response in json format.</returns>
        public JObject ToJObject()
        {
            var actionResponses = new JArray();

            foreach (var addResponse in this.BorrowerManagementAddResponses.CoalesceWithEmpty())
            {
                actionResponses.Add(WrapCommandResponse(addResponse, BorrowerManagementAddResponseToJObject));
            }

            if (this.AddEntityResponses != null)
            {
                foreach (var addResponse in this.AddEntityResponses)
                {
                    actionResponses.Add(WrapCommandResponse(addResponse, AddEntityResponseToJObject));
                }
            }

            if (this.AddAssociationResponses != null)
            {
                foreach (var addResponse in this.AddAssociationResponses)
                {
                    actionResponses.Add(WrapCommandResponse(addResponse, AddAssociationResponseToJObject));
                }
            }

            if (this.GetResponse != null)
            {
                actionResponses.Add(WrapCommandResponse(this.GetResponse, this.GetChildProperties));
            }

            foreach (var totalsResponse in this.GetAssetTotalsResponses.CoalesceWithEmpty())
            {
                actionResponses.Add(WrapCommandResponse(totalsResponse, GetAssetTotalsResponseToJObject));
            }

            foreach (var totalsResponse in this.GetLiabilityTotalsResponses.CoalesceWithEmpty())
            {
                actionResponses.Add(WrapCommandResponse(totalsResponse, GetLiabilityTotalsResponseToJObject));
            }

            foreach (var totalsResponse in this.GetRealPropertyTotalsResponses.CoalesceWithEmpty())
            {
                actionResponses.Add(WrapCommandResponse(totalsResponse, GetRealPropertyTotalsResponseToJObject));
            }

            foreach (var totalsResponse in this.GetIncomeSourceTotalsResponses.CoalesceWithEmpty())
            {
                actionResponses.Add(WrapCommandResponse(totalsResponse, GetIncomeSourceTotalsResponseToJObject));
            }

            var response = new JObject(
                new JProperty(
                    "data", 
                    new JObject(
                        new JProperty(
                            "responses",
                            actionResponses))));

            return response;
        }

        /// <summary>
        /// Builds the response JSON from a path notde for a borrower management add result.
        /// </summary>
        /// <param name="response">The response to convert to JSON.</param>
        /// <returns>The JObject representing the add command.</returns>
        private static JObject BorrowerManagementAddResponseToJObject(BorrowerManagementAddResponse response)
        {
            return new JObject(new JProperty("id", response.Id));
        }

        /// <summary>
        /// Builds the response json from a path node for an addEntity request.
        /// </summary>
        /// <param name="addEntityResponse">The add entity response.</param>
        /// <returns>The addEntity JObject.</returns>
        private static JObject AddEntityResponseToJObject(AddEntityResponse addEntityResponse)
        {
            var response = new JObject(
                new JProperty("path", addEntityResponse.Path),
                new JProperty("collectionName", addEntityResponse.CollectionName),
                new JProperty("id", addEntityResponse.EntityId));

            if (!string.IsNullOrEmpty(addEntityResponse.OwnerId))
            {
                response.Add(new JProperty("ownerId", addEntityResponse.OwnerId));
            }

            return response;
        }

        /// <summary>
        /// Builds the response json from a path node for an addAssociation request.
        /// </summary>
        /// <param name="addAssociationResponse">The add association response.</param>
        /// <returns>The addAssociation JObject.</returns>
        private static JObject AddAssociationResponseToJObject(AddAssociationResponse addAssociationResponse)
        {
            return new JObject(
                new JProperty("path", addAssociationResponse.Path),
                new JProperty("associationSet", addAssociationResponse.AssociationSet),
                new JProperty("firstId", addAssociationResponse.FirstIdentifier),
                new JProperty("secondId", addAssociationResponse.SecondIdentifier),
                new JProperty("id", addAssociationResponse.AssociationId));
        }

        /// <summary>
        /// Converts the asset totals response to a JObject.
        /// </summary>
        /// <param name="getTotalsResponse">The get totals response.</param>
        /// <returns>The totals response JObject.</returns>
        private static JObject GetAssetTotalsResponseToJObject(GetAssetTotalsResponse getTotalsResponse)
        {
            var responseProperties = new List<JProperty>();
            responseProperties.AddRange(GetBaseTotalsProperties(getTotalsResponse));

            responseProperties.Add(new JProperty("subtotalReo", getTotalsResponse.SubtotalReo));
            responseProperties.Add(new JProperty("subtotalLiquid", getTotalsResponse.SubtotalLiquid));
            responseProperties.Add(new JProperty("subtotalIlliquid", getTotalsResponse.SubtotalIlliquid));
            responseProperties.Add(new JProperty("total", getTotalsResponse.Total));

            return new JObject(responseProperties);
        }

        /// <summary>
        /// Converts the liability totals response to a JObject.
        /// </summary>
        /// <param name="getTotalsResponse">The get totals response.</param>
        /// <returns>The totals response JObject.</returns>
        private static JObject GetLiabilityTotalsResponseToJObject(GetLiabilityTotalsResponse getTotalsResponse)
        {
            var responseProperties = new List<JProperty>();

            responseProperties.AddRange(GetBaseTotalsProperties(getTotalsResponse));

            responseProperties.Add(new JProperty("balance", getTotalsResponse.Balance));
            responseProperties.Add(new JProperty("payment", getTotalsResponse.Payment));
            responseProperties.Add(new JProperty("paidOff", getTotalsResponse.PaidOff));

            return new JObject(responseProperties);
        }

        /// <summary>
        /// Converts the real property totals response to a JObject.
        /// </summary>
        /// <param name="getTotalsResponse">The get totals response.</param>
        /// <returns>The totals response JObject.</returns>
        private static JObject GetRealPropertyTotalsResponseToJObject(GetRealPropertyTotalsResponse getTotalsResponse)
        {
            var responseProperties = new List<JProperty>();

            responseProperties.AddRange(GetBaseTotalsProperties(getTotalsResponse));

            responseProperties.Add(new JProperty("marketValue", getTotalsResponse.MarketValue));
            responseProperties.Add(new JProperty("mortgageAmount", getTotalsResponse.MortgageAmount));
            responseProperties.Add(new JProperty("rentalNetRentalIncome", getTotalsResponse.RentalNetRentalIncome));
            responseProperties.Add(new JProperty("retainedNetRentalIncome", getTotalsResponse.RetainedNetRentalIncome));

            return new JObject(responseProperties);
        }

        /// <summary>
        /// Converts the income source totals response to a JObject.
        /// </summary>
        /// <param name="getTotalsResponse">The get totals response.</param>
        /// <returns>The totals response JObject.</returns>
        private static JObject GetIncomeSourceTotalsResponseToJObject(GetIncomeSourceTotalsResponse getTotalsResponse)
        {
            var responseProperties = new List<JProperty>();
            responseProperties.AddRange(GetBaseTotalsProperties(getTotalsResponse));
            responseProperties.Add(new JProperty("monthlyAmount", getTotalsResponse.MonthlyAmount));
            return new JObject(responseProperties);
        }

        /// <summary>
        /// Gets the base properties for totals responses.
        /// </summary>
        /// <typeparam name="T">The data object kind of the type for which totals were retrieved.</typeparam>
        /// <param name="getTotalsResponse">The response.</param>
        /// <returns>The properties to include in the response json.</returns>
        private static IEnumerable<JProperty> GetBaseTotalsProperties<T>(GetTotalsResponse<T> getTotalsResponse) where T : DataObjectKind
        {
            var baseResponseProperties = new List<JProperty>();
            baseResponseProperties.Add(new JProperty("path", getTotalsResponse.Path));
            baseResponseProperties.Add(new JProperty("type", getTotalsResponse.TotalsType));
            if (!string.IsNullOrEmpty(getTotalsResponse.Id))
            {
                baseResponseProperties.Add(new JProperty("id", getTotalsResponse.Id));
            }

            return baseResponseProperties;
        }

        /// <summary>
        /// Wraps a command response for the responses array.
        /// </summary>
        /// <typeparam name="T">The type of the response message.</typeparam>
        /// <param name="serviceMessage">The service message containing the response.</param>
        /// <param name="serializeReturnValue">The function to serialize the response.</param>
        /// <returns>The wrapped command response.</returns>
        private static JObject WrapCommandResponse<T>(ServiceMessage<T> serviceMessage, Func<T, JToken> serializeReturnValue)
        {
            return new JObject(
                new JProperty("name", serviceMessage.CommandName),
                new JProperty("operationId", serviceMessage.OperationId),
                new JProperty("returns", serializeReturnValue(serviceMessage.Message)));
        }

        /// <summary>
        /// Builds the response json from a path node.
        /// </summary>
        /// <param name="node">The path node.</param>
        /// <returns>The json reflecting the path node structure.</returns>
        private JToken GetChildProperties(GetResponseNode node)
        {
            if (node.Children == null || !node.Children.Any())
            {
                if (node.Value is IPathResolvableCollection)
                {
                    // Handle case of empty collection.
                    return new JProperty(node.PathComponent.Name, new JObject());
                }
                else if (node.Value is IPathResolvableExpandable)
                {
                    return new JProperty(node.PathComponent.Name, JValue.CreateNull());
                }
                else
                {
                    return new JProperty(node.PathComponent.Name, node.Value?.ToString() ?? string.Empty);
                }
            }

            var childProperties = new List<JToken>();
            foreach (var child in node.Children)
            {
                childProperties.Add(this.GetChildProperties(child));
            }

            JObject childObject = new JObject(childProperties);

            if (node.PathComponent == null)
            {
                return childObject;
            }
            else
            {
                return new JProperty(node.PathComponent.Name, childObject);
            }
        }
    }
}
