﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using DataAccess.PathDispatch;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides functionality needed to set fields from a request.
    /// </summary>
    public class LqbSetRequestResolver
    {
        /// <summary>
        /// Sets the values provided from the request.
        /// </summary>
        /// <param name="requestRoot">The request root.</param>
        /// <param name="responseRootValue">The value for the root node.</param>
        /// <param name="parser">The string formatter to use when an entity that supports formatting is encountered.</param>
        public static void ResolveSetRequest(RequestNode requestRoot, IPathResolvable responseRootValue, IParseFromString parser)
        {
            if (responseRootValue is IPathableStringFormatterProvider)
            {
                responseRootValue = ((IPathableStringFormatterProvider)responseRootValue).GetStringFormatter(null, parser);
            }

            foreach (var requestChild in requestRoot.Children)
            {
                ResolveSetRequestRecursively(requestChild, responseRootValue, parser);
            }
        }

        /// <summary>
        /// Builds the response tree from the given request node.
        /// </summary>
        /// <param name="requestNode">The request node.</param>
        /// <param name="parent">The path resolvable parent value.</param>
        /// <param name="parser">The string parser to use when an entity that supports formatting is encountered.</param>
        private static void ResolveSetRequestRecursively(RequestNode requestNode, IPathResolvable parent, IParseFromString parser)
        {
            if (requestNode.IsLeafNode)
            {
                if (parent is IPathSettable)
                {
                    ((IPathSettable)parent).SetElement(requestNode.PathComponent, requestNode.Value);
                }
                else
                {
                    throw new DataAccess.CBaseException(Common.ErrorMessages.Generic, "Parent node must support IPathSettable to set value.");
                }
            }
            else
            {
                var value = parent.GetElement(requestNode.PathComponent);
                if (value is IPathableStringFormatterProvider)
                {
                    value = ((IPathableStringFormatterProvider)value).GetStringFormatter(null, parser);
                }

                if (value is IPathResolvable)
                {
                    foreach (var child in requestNode.Children)
                    {
                        ResolveSetRequestRecursively(child, (IPathResolvable)value, parser);
                    }
                }
                else
                {
                    throw new DataAccess.CBaseException(Common.ErrorMessages.Generic, "Node with children must support IPathResolvable.");
                }

                if (parent is IPathSettable && value is IPathableNestedStringFormatter)
                {
                    ((IPathSettable)parent).SetElement(requestNode.PathComponent, value);
                }
            }
        }
    }
}
