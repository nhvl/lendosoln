﻿namespace LendersOffice.ObjLib.LqbDataService
{
    /// <summary>
    /// Provides a type-inferred way to create instances of the generic type <see cref="ServiceMessage{T}"/>.
    /// </summary>
    public static class ServiceMessage
    {
        /// <summary>
        /// Creates an instance of the <see cref="ServiceMessage{T}"/> class.
        /// </summary>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <param name="operationId">The operation id.</param>
        /// <param name="commandName">The name of the command.</param>
        /// <param name="message">The message to wrap.</param>
        /// <returns>The instance of <see cref="ServiceMessage{T}"/>.</returns>
        public static ServiceMessage<T> Create<T>(string operationId, string commandName, T message)
        {
            return new ServiceMessage<T>(operationId, commandName, message);
        }
    }

    /// <summary>
    /// Represents a wrapper of common data to a service message.
    /// </summary>
    /// <typeparam name="T">The type of the message (either request or response).</typeparam>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "The static helper triggering this violation has the same name and only exists to serve this type.")]
    public class ServiceMessage<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceMessage{T}"/> class.
        /// </summary>
        /// <param name="operationId">The operation id.</param>
        /// <param name="commandName">The name of the command.</param>
        /// <param name="message">The message to wrap.</param>
        public ServiceMessage(string operationId, string commandName, T message)
        {
            this.OperationId = operationId;
            this.CommandName = commandName;
            this.Message = message;
        }

        /// <summary>
        /// Gets the operation id.
        /// </summary>
        public string OperationId { get; }

        /// <summary>
        /// Gets the name of the command.
        /// </summary>
        public string CommandName { get; }

        /// <summary>
        /// Gets the message to wrap.
        /// </summary>
        public T Message { get; }
    }
}
