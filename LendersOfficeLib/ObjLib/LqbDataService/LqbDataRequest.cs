﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Provides a way to retrieve path data out of a request.
    /// </summary>
    public class LqbDataRequest
    {
        /// <summary>
        /// The top-level loan path.
        /// </summary>
        private static readonly DataPath LoanPath = DataPath.Create("loan");

        /// <summary>
        /// The hardcoded loan fields with nested dependency loading, as these are large sets of data.
        /// </summary>
        private static readonly IReadOnlyCollection<string> DeepLoanNodes = new HashSet<string>(new[] { nameof(CPageData.Consumers), nameof(CPageData.LegacyApplications), }, StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The conversion object.
        /// </summary>
        private readonly LosConvert converter;

        /// <summary>
        /// The lazy-initialized get request node tree.
        /// </summary>
        private readonly Lazy<ServiceMessage<RequestNode>> getRequestNodeTree;

        /// <summary>
        /// The lazy-initialized set request node tree.
        /// </summary>
        private readonly Lazy<ServiceMessage<RequestNode>> setRequestNodeTree;

        /// <summary>
        /// The lazy-initialized borrower management commands.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<BorrowerManagementCommand>>> borrowerManagementCommands;

        /// <summary>
        /// The lazy-initialized commands to remove entities.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<RemoveEntity>>> removeEntityCommands;

        /// <summary>
        /// The lazy-initialized commands to add entities.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<AddEntity>>> addEntityCommands;

        /// <summary>
        /// The lazy-initialized commands to remove associations.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<RemoveAssociation>>> removeAssociationCommands;

        /// <summary>
        /// The lazy-initialized commands to add associations.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<AddAssociation>>> addAssociationCommands;

        /// <summary>
        /// The lazy-initialized command to set ownership for an entity.
        /// </summary>
        private readonly Lazy<ServiceMessage<SetOwnership>> setOwnershipCommand;

        /// <summary>
        /// The lazy-initialized command to set the order for a collection.
        /// </summary>
        private readonly Lazy<ServiceMessage<SetOrder>> setOrderCommand;

        /// <summary>
        /// The lazy-initialized commands to add liability balance and payment to a real property.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<AddLiabilityBalanceAndPaymentToRealProperty>>> addLiabilityBalanceAndPaymentToRealPropertyCommands;

        /// <summary>
        /// The lazy-initialized commands to get asset totals.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Asset>>>> getAssetTotalsCommands;

        /// <summary>
        /// The lazy-initialized commands to get liability totals.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Liability>>>> getLiabilityTotalsCommands;

        /// <summary>
        /// The lazy-initialized commands to get real property totals.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.RealProperty>>>> getRealPropertyTotalsCommands;

        /// <summary>
        /// The lazy-initialized commands to get income source totals.
        /// </summary>
        private readonly Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.IncomeSource>>>> getIncomeSourceTotalsCommands;

        /// <summary>
        /// The raw commands that were passed via the "requests" array.
        /// </summary>
        private readonly Lazy<IReadOnlyList<RawCommand>> rawCommands;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbDataRequest"/> class.
        /// </summary>
        /// <param name="requestJson">The request json.</param>
        /// <param name="converter">The conversion object.</param>
        public LqbDataRequest(JObject requestJson, LosConvert converter)
        {
            this.converter = converter;
            this.getRequestNodeTree = new Lazy<ServiceMessage<RequestNode>>(this.BuildGetRequestNodeTree);
            this.setRequestNodeTree = new Lazy<ServiceMessage<RequestNode>>(this.BuildSetRequestNodeTree);
            this.borrowerManagementCommands = new Lazy<IReadOnlyList<ServiceMessage<BorrowerManagementCommand>>>(this.ParseBorrowerManagementCommands);
            this.removeEntityCommands = new Lazy<IReadOnlyList<ServiceMessage<RemoveEntity>>>(() => this.ParseCommands("removeEntity", ParseRemoveEntityCommand));
            this.addEntityCommands = new Lazy<IReadOnlyList<ServiceMessage<AddEntity>>>(() => this.ParseCommands("addEntity", this.ParseAddEntityCommand));
            this.removeAssociationCommands = new Lazy<IReadOnlyList<ServiceMessage<RemoveAssociation>>>(() => this.ParseCommands("removeAssociation", ParseRemoveAssociationCommand));
            this.addAssociationCommands = new Lazy<IReadOnlyList<ServiceMessage<AddAssociation>>>(() => this.ParseCommands("addAssociation", ParseAddAssociationCommand));
            this.setOwnershipCommand = new Lazy<ServiceMessage<SetOwnership>>(this.GetSetOwnershipCommand);
            this.setOrderCommand = new Lazy<ServiceMessage<SetOrder>>(this.GetSetOrderCommand);
            this.addLiabilityBalanceAndPaymentToRealPropertyCommands = new Lazy<IReadOnlyList<ServiceMessage<AddLiabilityBalanceAndPaymentToRealProperty>>>(() => this.ParseCommands("addLiabilityBalanceAndPaymentToRealProperty", ParseAddLiabilityBalanceAndPaymentToRealPropertyCommand));
            this.getAssetTotalsCommands = new Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Asset>>>>(() => this.ParseCommands("getAssetTotals", ParseGetAssetTotalsCommand));
            this.getLiabilityTotalsCommands = new Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Liability>>>>(() => this.ParseCommands("getLiabilityTotals", ParseGetLiabilityTotalsCommand));
            this.getRealPropertyTotalsCommands = new Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.RealProperty>>>>(() => this.ParseCommands("getRealPropertyTotals", ParseGetRealPropertyTotalsCommand));
            this.getIncomeSourceTotalsCommands = new Lazy<IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.IncomeSource>>>>(() => this.ParseCommands("getIncomeSourceTotals", ParseGetIncomeSourceTotalsCommand));
            this.rawCommands = new Lazy<IReadOnlyList<RawCommand>>(() => this.ParseRawCommands(requestJson));
        }

        /// <summary>
        /// Gets the root node of the get request.
        /// </summary>
        public ServiceMessage<RequestNode> GetRequestNodeTree => this.getRequestNodeTree.Value;

        /// <summary>
        /// Gets the root node of the set request.
        /// </summary>
        public ServiceMessage<RequestNode> SetRequestNodeTree => this.setRequestNodeTree.Value;

        /// <summary>
        /// Gets the borrower management commands to apply.
        /// </summary>
        public IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> BorrowerManagementCommands => this.borrowerManagementCommands.Value;

        /// <summary>
        /// Gets the remove entity commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<RemoveEntity>> RemoveEntityCommands => this.removeEntityCommands.Value;

        /// <summary>
        /// Gets the remove entity commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<AddEntity>> AddEntityCommands => this.addEntityCommands.Value;

        /// <summary>
        /// Gets the remove association commmands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<RemoveAssociation>> RemoveAssociationCommands => this.removeAssociationCommands.Value;

        /// <summary>
        /// Gets the add association commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<AddAssociation>> AddAssociationCommands => this.addAssociationCommands.Value;

        /// <summary>
        /// Gets the set ownership command for the request.
        /// </summary>
        public ServiceMessage<SetOwnership> SetOwnershipCommand => this.setOwnershipCommand.Value;

        /// <summary>
        /// Gets the set order command for the request.
        /// </summary>
        public ServiceMessage<SetOrder> SetOrderCommand => this.setOrderCommand.Value;

        /// <summary>
        /// Gets the commands for the request to add liability balance and payment to a real property.
        /// </summary>
        public IReadOnlyList<ServiceMessage<AddLiabilityBalanceAndPaymentToRealProperty>> AddLiabilityBalanceAndPaymentToRealPropertyCommands => this.addLiabilityBalanceAndPaymentToRealPropertyCommands.Value;

        /// <summary>
        /// Gets the get asset totals commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Asset>>> GetAssetTotalsCommands => this.getAssetTotalsCommands.Value;

        /// <summary>
        /// Gets the get liability totals commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Liability>>> GetLiabilityTotalsCommands => this.getLiabilityTotalsCommands.Value;

        /// <summary>
        /// Gets the get real property totals commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.RealProperty>>> GetRealPropertyTotalsCommands => this.getRealPropertyTotalsCommands.Value;

        /// <summary>
        /// Gets the get income source totals commands for the request.
        /// </summary>
        public IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.IncomeSource>>> GetIncomeSourceTotalsCommands => this.getIncomeSourceTotalsCommands.Value;

        /// <summary>
        /// Gets the loan field dependencies that will be needed to fulfill this request.
        /// </summary>
        public IEnumerable<string> LoanDependencies
        {
            get
            {
                var dependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                // For now we will assume all dependencies are top-level loan fields or values from a hardcoded set.
                var getLoanNode = this.GetRequestNodeTree?.Message?.Children
                    .SingleOrDefault(p => p.PathComponent.Name.Equals("loan", StringComparison.OrdinalIgnoreCase));
                if (getLoanNode != null)
                {
                    foreach (RequestNode node in getLoanNode.Children)
                    {
                        dependencies.Add(node.PathComponent.Name);
                        if (DeepLoanNodes.Contains(node.PathComponent.Name)
                            && node.PathComponent is DataPathCollectionElement
                            && node.Children.All(c => c.PathComponent is DataPathSelectionElement))
                        {
                            dependencies.UnionWith(node.Children.SelectMany(n => n.Children).Select(n => n.PathComponent.Name));
                        }
                    }
                }

                var setLoanNode = this.SetRequestNodeTree?.Message?.Children
                    .SingleOrDefault(p => p.PathComponent.Name.Equals("loan", StringComparison.OrdinalIgnoreCase));
                if (setLoanNode != null)
                {
                    foreach (RequestNode node in setLoanNode.Children)
                    {
                        dependencies.Add(node.PathComponent.Name);
                        if (DeepLoanNodes.Contains(node.PathComponent.Name)
                            && node.PathComponent is DataPathCollectionElement
                            && node.Children.All(c => c.PathComponent is DataPathSelectionElement))
                        {
                            dependencies.UnionWith(node.Children.SelectMany(n => n.Children).Select(n => n.PathComponent.Name));
                        }
                    }
                }

                foreach (var removeEntity in this.RemoveEntityCommands)
                {
                    if (removeEntity.Message.Path.Equals(LoanPath))
                    {
                        dependencies.Add(removeEntity.Message.CollectionName);
                    }
                    else if (removeEntity.Message.Path.Head.Name == LoanPath.Head.Name && removeEntity.Message.Path.Length > 1)
                    {
                        dependencies.Add(removeEntity.Message.Path.PathList.Skip(1).First().Name);
                    }
                }

                foreach (var addEntity in this.AddEntityCommands)
                {
                    if (addEntity.Message.Path.Equals(LoanPath))
                    {
                        dependencies.Add(nameof(CPageBase.sfHandleAdd));
                        dependencies.Add(addEntity.Message.CollectionName);
                    }
                    else if (addEntity.Message.Path.Head.Name == LoanPath.Head.Name && addEntity.Message.Path.Length > 1)
                    {
                        dependencies.Add(addEntity.Message.Path.PathList.Skip(1).First().Name);
                    }
                }

                dependencies.UnionWith(this.RemoveAssociationCommands.Select(c => c.Message.AssociationSet));

                dependencies.UnionWith(this.AddAssociationCommands.Select(c => c.Message.AssociationSet));

                if (this.SetOwnershipCommand != null)
                {
                    dependencies.Add(this.SetOwnershipCommand.Message.AssociationName);
                }

                if (this.SetOrderCommand != null)
                {
                    dependencies.Add(this.SetOrderCommand.Message.CollectionName);
                }

                if (this.AddLiabilityBalanceAndPaymentToRealPropertyCommands.Any())
                {
                    dependencies.Add(nameof(CPageData.HandleAddLiabilityBalanceAndPaymentToRealProperty));
                }

                if (this.GetAssetTotalsCommands.Any())
                {
                    dependencies.Add(nameof(CPageBase.HandleGetAssetTotals));
                }

                if (this.GetLiabilityTotalsCommands.Any())
                {
                    dependencies.Add(nameof(CPageBase.HandleGetLiabilityTotals));
                }

                if (this.GetRealPropertyTotalsCommands.Any())
                {
                    dependencies.Add(nameof(CPageBase.HandleGetRealPropertyTotals));
                }

                if (this.GetIncomeSourceTotalsCommands.Any())
                {
                    dependencies.Add(nameof(CPageBase.HandleGetIncomeSourceTotals));
                }

                return dependencies;
            }
        }

        /// <summary>
        /// Parses a remove entity command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed remove entity command.</returns>
        private static RemoveEntity ParseRemoveEntityCommand(RawCommand command)
        {
            var token = command.Args.Property("path")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveEntity command must have 'path' property specified.");
            }

            string pathString = (string)token;

            token = command.Args.Property("collectionName")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveEntity command must have 'collectionName' property specified.");
            }

            string collectionName = (string)token;

            token = command.Args.Property("id")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveEntity command must have 'id' property specified.");
            }

            var idString = (string)token;
            Guid id;
            if (!Guid.TryParse(idString, out id))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveEntity command must have 'id' property specified as a valid guid.");
            }

            return new RemoveEntity(DataPath.Create(pathString), collectionName, id);
        }

        /// <summary>
        /// Parses a get asset totals command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The get asset totals command.</returns>
        private static GetTotals<DataObjectKind.Asset> ParseGetAssetTotalsCommand(RawCommand command)
        {
            return ParseTotalsCommand<DataObjectKind.Asset>("GetAssetTotals", command);
        }

        /// <summary>
        /// Parses a get liability totals command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The get liability totals command.</returns>
        private static GetTotals<DataObjectKind.Liability> ParseGetLiabilityTotalsCommand(RawCommand command)
        {
            return ParseTotalsCommand<DataObjectKind.Liability>("GetLiabilityTotals", command);
        }

        /// <summary>
        /// Parses a get real property totals command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The get real property totals command.</returns>
        private static GetTotals<DataObjectKind.RealProperty> ParseGetRealPropertyTotalsCommand(RawCommand command)
        {
            return ParseTotalsCommand<DataObjectKind.RealProperty>("GetRealPropertyTotals", command);
        }

        /// <summary>
        /// Parses a get income source totals command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The get income source totals command.</returns>
        private static GetTotals<DataObjectKind.IncomeSource> ParseGetIncomeSourceTotalsCommand(RawCommand command)
        {
            return ParseTotalsCommand<DataObjectKind.IncomeSource>("GetIncomeSourceTotals", command);
        }

        /// <summary>
        /// Parses a totals command.
        /// </summary>
        /// <typeparam name="T">The type of the totals command.</typeparam>
        /// <param name="commandName">The command name.</param>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed totals command.</returns>
        private static GetTotals<T> ParseTotalsCommand<T>(string commandName, RawCommand command) where T : DataObjectKind
        {
            JToken pathToken = command.Args.Property("path")?.Value;
            if (pathToken?.Type != JTokenType.String || string.IsNullOrEmpty((string)pathToken))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, $"{commandName} command must have 'path' property specified.");
            }

            var path = DataPath.Create((string)pathToken);

            TotalsType type;
            JToken typeToken = command.Args.Property("type")?.Value;
            if (typeToken?.Type != JTokenType.String || string.IsNullOrEmpty((string)typeToken))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, $"{commandName} command must have 'type' property specified.");
            }
            else if (!((string)typeToken).TryParseDefine(out type, ignoreCase: true))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, $"{commandName} has invalid 'type' property specified: {(string)typeToken}.");
            }

            if (type == TotalsType.Loan)
            {
                return new GetTotals<T>(path, type, id: null);
            }

            Guid id = Guid.Empty;
            JToken idToken = command.Args.Property("id")?.Value;
            if (idToken == null || idToken.Type != JTokenType.String || !Guid.TryParse((string)idToken, out id))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, $"{commandName} command must have 'id' property specified as a valid guid.");
            }

            return new GetTotals<T>(path, type, id);
        }

        /// <summary>
        /// Parses a remove association command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed remove association command.</returns>
        private static RemoveAssociation ParseRemoveAssociationCommand(RawCommand command)
        {
            var token = command.Args.Property("path")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveAssociation command must have 'path' property specified.");
            }

            var pathString = (string)token;

            token = command.Args.Property("associationSet")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveAssociation command must have 'associationSet' property specified.");
            }

            var associationSet = (string)token;

            token = command.Args.Property("id")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveAssociation command must have 'id' property specified.");
            }

            var idString = (string)token;
            Guid id;
            if (!Guid.TryParse(idString, out id))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "RemoveAssociation command must have 'id' property specified as a valid guid.");
            }

            return new RemoveAssociation(DataPath.Create(pathString), associationSet, id);
        }

        /// <summary>
        /// Parses an add association command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed add association command.</returns>
        private static AddAssociation ParseAddAssociationCommand(RawCommand command)
        {
            var token = command.Args.Property("path")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'path' property specified.");
            }

            string pathString = (string)token;

            token = command.Args.Property("associationSet")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'associationSet' property specified.");
            }

            string associationSet = (string)token;

            token = command.Args.Property("firstId")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'firstId' property specified.");
            }

            var idString = (string)token;
            Guid firstId;
            if (!Guid.TryParse(idString, out firstId))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'firstId' property specified as a valid guid.");
            }

            token = command.Args.Property("secondId")?.Value;
            if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'secondId' property specified.");
            }

            idString = (string)token;
            Guid secondId;
            if (!Guid.TryParse(idString, out secondId))
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "AddAssociation command must have 'secondId' property specified as a valid guid.");
            }

            return new AddAssociation(DataPath.Create(pathString), associationSet, firstId, secondId);
        }

        /// <summary>
        /// Parses a command to add liability balance and payment to a real property from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed command.</returns>
        private static AddLiabilityBalanceAndPaymentToRealProperty ParseAddLiabilityBalanceAndPaymentToRealPropertyCommand(RawCommand command)
        {
            string path = ReadString(command.Args.Property("path")?.Value);
            if (path == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "AddLiabilityBalanceAndPaymentToRealProperty command must have 'path' property specified.");
            }

            Guid? liabilityId = ReadString(command.Args.Property("liabilityId")?.Value)?.ToNullable<Guid>(Guid.TryParse);
            if (!liabilityId.HasValue)
            {
                throw new CBaseException(ErrorMessages.Generic, "AddLiabilityBalanceAndPaymentToRealProperty command must have 'liabilityId' property specified as a valid guid.");
            }

            Guid? realPropertyId = ReadString(command.Args.Property("realPropertyId")?.Value)?.ToNullable<Guid>(Guid.TryParse);
            if (!realPropertyId.HasValue)
            {
                throw new CBaseException(ErrorMessages.Generic, "AddLiabilityBalanceAndPaymentToRealProperty command must have 'realPropertyId' property specified as a valid guid.");
            }

            return new AddLiabilityBalanceAndPaymentToRealProperty(
                DataPath.Create(path),
                liabilityId.Value.ToIdentifier<DataObjectKind.Liability>(),
                realPropertyId.Value.ToIdentifier<DataObjectKind.RealProperty>());
        }

        /// <summary>
        /// Throws an exception if <paramref name="throwCondition"/> is true.
        /// </summary>
        /// <param name="throwCondition">The condition under which to throw.</param>
        /// <param name="description">The developer error message to throw.</param>
        private static void ThrowIfTrue(bool throwCondition, string description)
        {
            if (throwCondition)
            {
                throw new CBaseException(ErrorMessages.Generic, description);
            }
        }

        /// <summary>
        /// Reads a string from the specified JSON token, which is usually a property value.
        /// </summary>
        /// <param name="propertyValue">The value to read the string value from.</param>
        /// <param name="normalizeEmptyStringToNull">A value indicating whether the empty string should be converted to null for return purposes.</param>
        /// <returns>The value of the property if it's a string, or null if it was not found or blank.</returns>
        private static string ReadString(JToken propertyValue, bool normalizeEmptyStringToNull = true)
        {
            if (propertyValue?.Type != JTokenType.String || (normalizeEmptyStringToNull && string.IsNullOrEmpty((string)propertyValue)))
            {
                return null;
            }

            return (string)propertyValue;
        }

        /// <summary>
        /// Reads an array from the specified JSON property.
        /// </summary>
        /// <typeparam name="T">The type of the items to read from the array.</typeparam>
        /// <param name="property">The property to read the array value from.</param>
        /// <param name="selector">The selection function converting each token within the array to the type.</param>
        /// <returns>The value of the array as selected by <paramref name="selector"/>, or null if it is not an array type.</returns>
        private static IReadOnlyList<T> ReadArray<T>(JProperty property, Func<JToken, T> selector)
        {
            if (property?.Value?.Type != JTokenType.Array)
            {
                return null;
            }

            JArray array = (JArray)property.Value;
            var result = new List<T>(array.Count);
            foreach (JToken item in array)
            {
                result.Add(selector(item));
            }

            return result;
        }

        /// <summary>
        /// Parses an add entity command from a raw command.
        /// </summary>
        /// <param name="command">The raw command.</param>
        /// <returns>The parsed add entity command.</returns>
        private AddEntity ParseAddEntityCommand(RawCommand command)
        {
            string pathString = ReadString(command.Args.Property("path")?.Value);
            if (pathString == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "AddEntity command must have 'path' property specified.");
            }

            var path = DataPath.Create(pathString);

            string collectionName = ReadString(command.Args.Property("collectionName")?.Value);
            if (collectionName == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "AddEntity command must have 'collectionName' property specified.");
            }

            RequestNode fields = null;
            JToken fieldsToken = command.Args.Property("fields")?.Value;
            if (fieldsToken != null)
            {
                fields = new RequestNode(null, this.GetRequestNodesFrom(fieldsToken));
            }

            if (!path.Equals(LoanPath))
            {
                return new AddEntity(path, collectionName, fields);
            }

            Guid primaryOwnerId;
            List<Guid> additionalOwnerIds;
            if (collectionName == "CounselingEvents")
            {
                IReadOnlyList<Guid?> attendeeIds = ReadArray(command.Args.Property("attendeeIds"), item => ReadString(item)?.ToNullable<Guid>(Guid.TryParse));
                if (attendeeIds == null || attendeeIds.Count == 0 || attendeeIds.Any(id => id == null))
                {
                    throw CBaseException.GenericException("AddEntity command must have 'attendeeIds' property specified as an array of valid guids.");
                }

                primaryOwnerId = attendeeIds[0].Value;
                additionalOwnerIds = attendeeIds.Skip(1).Select(g => g.Value).ToList();
            }
            else
            {
                Guid? maybeOwnerId = ReadString(command.Args.Property("ownerId")?.Value)?.ToNullable<Guid>(Guid.TryParse);
                if (maybeOwnerId == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "AddEntity command must have 'ownerId' property specified as a valid guid.");
                }

                primaryOwnerId = maybeOwnerId.Value;
                additionalOwnerIds = new List<Guid>();
                var jarray = command.Args.Property("additionalOwnerIds")?.Value;
                if (jarray != null)
                {
                    if (jarray.Type != JTokenType.Array)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "AddEntity command 'additionalOwnerIds' must be an array.");
                    }

                    var additionalOwnerIdsJson = (JArray)jarray;
                    foreach (var additionalOwnerIdString in additionalOwnerIdsJson.Values<string>())
                    {
                        Guid additionalOwnerId;
                        if (!Guid.TryParse(additionalOwnerIdString, out additionalOwnerId))
                        {
                            throw new CBaseException(ErrorMessages.Generic, "AddEntity command must have Guid id strings for 'additionalOwnerIds'.");
                        }

                        additionalOwnerIds.Add(additionalOwnerId);
                    }
                }
            }

            JToken indexToken = command.Args.Property("index")?.Value;
            int? index = null;
            if (indexToken != null)
            {
                if (indexToken.Type != JTokenType.Integer)
                {
                    throw new CBaseException(ErrorMessages.Generic, "AddEntity command expects 'index' property to be an integer.");
                }

                index = indexToken.Value<int>();
            }

            return new AddEntity(path, collectionName, primaryOwnerId, additionalOwnerIds, fields, index);
        }

        /// <summary>
        /// Builds the get request node tree from the request json.
        /// </summary>
        /// <returns>The root of the get request tree.</returns>
        private ServiceMessage<RequestNode> BuildGetRequestNodeTree()
        {
            var command = this.rawCommands.Value.SingleOrDefault(c => c.Name.Equals("get", StringComparison.OrdinalIgnoreCase));
            if (command == null)
            {
                return null;
            }

            var children = this.GetRequestNodesFrom(command.Args);
            return ServiceMessage.Create(command.OperationId, command.Name, new RequestNode(null, children));
        }

        /// <summary>
        /// Builds the set request node tree from the request json.
        /// </summary>
        /// <returns>The root of the set request node tree.</returns>
        private ServiceMessage<RequestNode> BuildSetRequestNodeTree()
        {
            var command = this.rawCommands.Value.SingleOrDefault(c => c.Name.Equals("set", StringComparison.OrdinalIgnoreCase));
            if (command == null)
            {
                return null;
            }

            var children = this.GetRequestNodesFrom(command.Args);
            return ServiceMessage.Create(command.OperationId, command.Name, new RequestNode(null, children));
        }

        /// <summary>
        /// Extracts path nodes from the given token.
        /// </summary>
        /// <param name="token">The token to extract the path nodes from.</param>
        /// <returns>The path nodes from the token, with all child path nodes initialized too.</returns>
        private IReadOnlyList<RequestNode> GetRequestNodesFrom(JToken token)
        {
            if (token.Type == JTokenType.Property)
            {
                var property = (JProperty)token;

                if (this.IsLeafToken(property.Value))
                {
                    string value = this.GetValueFromProperty(property);
                    return new[]
                    {
                        new RequestNode(new DataPathBasicElement(property.Name), children: null, value: value)
                    };
                }
                else
                {
                    IDataPathElement pathHead;
                    Guid id;

                    var children = this.GetRequestNodesFrom(property.Value);

                    if (children.Any(n => n.PathComponent is DataPathSelectionElement))
                    {
                        pathHead = new DataPathCollectionElement(property.Name);
                    }
                    else if (Guid.TryParse(property.Name, out id)
                        || property.Name.Equals(DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase)
                        || property.Name.ToNullable<int>(int.TryParse).HasValue)
                    {
                        pathHead = new DataPathSelectionElement(new SelectIdExpression(property.Name));
                    }
                    else
                    {
                        pathHead = new DataPathBasicElement(property.Name);
                    }

                    return new[]
                    {
                        new RequestNode(pathHead, children)
                    };
                }
            }
            else if (token.Type == JTokenType.Object)
            {
                var obj = (JObject)token;
                var children = new List<RequestNode>();

                foreach (var property in obj.Properties())
                {
                    children.AddRange(this.GetRequestNodesFrom(property));
                }

                return children;
            }
            else
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "Encountered unexpected token type: " + token.Type.ToString());
            }
        }

        /// <summary>
        /// Parses a list of borrower management commands.
        /// </summary>
        /// <returns>The borrower management commands loaded.</returns>
        private IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> ParseBorrowerManagementCommands()
        {
            var list = new List<ServiceMessage<BorrowerManagementCommand>>();
            foreach (var command in this.rawCommands.Value)
            {
                var parsedCommand = this.ParseBorrowerManagementCommand(command);
                if (parsedCommand != null)
                {
                    parsedCommand.ValidateCommand();
                    list.Add(ServiceMessage.Create(command.OperationId, command.Name, parsedCommand));
                }
            }

            return list.Count > 0 ? list : null;
        }

        /// <summary>
        /// Parses an individual borrower management command from a raw command instance.
        /// </summary>
        /// <param name="command">The command to parse.</param>
        /// <returns>The command parsed, or null if the command was not a borrower management command.</returns>
        private BorrowerManagementCommand ParseBorrowerManagementCommand(RawCommand command)
        {
            const string SetPrimaryApplication = "setPrimaryApplication";
            const string AddBorrowerToApplication = "addBorrowerToApplication";
            const string AddApplication = "addApplication";
            const string DeleteApplication = "deleteApplication";
            const string DeleteBorrower = "deleteBorrower";
            const string SetPrimaryBorrower = "setPrimaryBorrower";
            const string SwapBorrowers = "swapBorrowers";
            if (!command.Name.EqualsOneOf(SetPrimaryApplication, AddBorrowerToApplication, AddApplication, DeleteApplication, DeleteBorrower, SetPrimaryBorrower, SwapBorrowers))
            {
                return null;
            }

            string pathString = ReadString(command.Args.Property("path")?.Value);
            if (pathString == null)
            {
                throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'path' property specified.");
            }

            const string LegacyApplicationType = "LegacyApplication";
            const string UladApplicationType = "UladApplication";
            string applicationType = ReadString(command.Args.Property("applicationType")?.Value);
            if (applicationType != null && !applicationType.EqualsOneOf(LegacyApplicationType, UladApplicationType))
            {
                throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have a valid 'applicationType' specified.");
            }

            DataPath path = DataPath.Create(pathString);
            Guid? applicationId = ReadString(command.Args.Property("applicationId")?.Value).ToNullable<Guid>(Guid.TryParse);
            Guid? legacyApplicationId = applicationType == LegacyApplicationType ? applicationId : null;
            Guid? uladApplicationId = applicationType == UladApplicationType ? applicationId : null;
            Guid? consumerId = ReadString(command.Args.Property("consumerId")?.Value).ToNullable<Guid>(Guid.TryParse);
            switch (command.Name)
            {
                case SetPrimaryApplication:
                    return BorrowerManagementCommand.CreateSetPrimaryApplication(path, legacyApplicationId, uladApplicationId);
                case AddBorrowerToApplication:
                    return BorrowerManagementCommand.CreateAddBorrowerToApplication(path, legacyApplicationId, uladApplicationId);
                case AddApplication:
                    ThrowIfTrue(applicationType == null, command.Name + " command must specify the type of application to add.");
                    return BorrowerManagementCommand.CreateAddApplication(path, isLegacyApplication: applicationType == LegacyApplicationType);
                case DeleteApplication:
                    return BorrowerManagementCommand.CreateDeleteApplication(path, legacyApplicationId, uladApplicationId);
                case DeleteBorrower:
                    ThrowIfTrue(!consumerId.HasValue && !legacyApplicationId.HasValue, command.Name + " command must specify either a valid 'consumerId' or a legacy 'applicationId'.");
                    return consumerId.HasValue
                        ? BorrowerManagementCommand.CreateDeleteBorrower(path, consumerId.Value)
                        : BorrowerManagementCommand.CreateDeleteCoBorrower(path, legacyApplicationId.Value);
                case SetPrimaryBorrower:
                    ThrowIfTrue(!consumerId.HasValue, command.Name + " command must specify a valid 'consumerId'.");
                    return BorrowerManagementCommand.CreateSetPrimaryBorrower(path, consumerId.Value);
                case SwapBorrowers:
                    ThrowIfTrue(!legacyApplicationId.HasValue, command.Name + " command must specify a valid legacy 'applicationId'.");
                    return BorrowerManagementCommand.CreateSwapBorrowers(path, legacyApplicationId.Value);
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid borrower management command.");
            }
        }

        /// <summary>
        /// Gets the set ownership command from the request, if it exists.
        /// </summary>
        /// <returns>The set ownership command or null if it doesn't exist.</returns>
        /// <exception cref="CBaseException">Thrown when the command is not correctly specified in the request.</exception>
        private ServiceMessage<SetOwnership> GetSetOwnershipCommand()
        {
            RawCommand command = this.rawCommands.Value.SingleOrDefault(c => c.Name.Equals("setOwnership", StringComparison.OrdinalIgnoreCase) || c.Name.Equals("setAttendance", StringComparison.OrdinalIgnoreCase));
            if (command == null)
            {
                return null;
            }

            string pathString = ReadString(command.Args.Property("path")?.Value);
            if (pathString == null)
            {
                throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'path' property specified.");
            }

            string associationName = ReadString(command.Args.Property("associationName")?.Value);
            if (associationName == null)
            {
                throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'associationName' property specified.");
            }

            Guid? recordId = ReadString(command.Args.Property("id")?.Value)?.ToNullable<Guid>(Guid.TryParse);
            if (recordId == null)
            {
                throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'id' property specified as a valid guid.");
            }

            Guid primaryOwnerId;
            List<Guid> additionalOwnerIds = null;
            if (command.Name.Equals("setAttendance", StringComparison.OrdinalIgnoreCase))
            {
                IReadOnlyList<Guid?> attendeeIds = ReadArray(command.Args.Property("attendeeIds"), item => ReadString(item)?.ToNullable<Guid>(Guid.TryParse));
                if (attendeeIds == null || attendeeIds.Count == 0 || attendeeIds.Any(id => id == null))
                {
                    throw CBaseException.GenericException(command.Name + " command must have 'attendeeIds' property specified as an array of valid guids.");
                }

                primaryOwnerId = attendeeIds[0].Value;
                additionalOwnerIds = attendeeIds.Skip(1).Select(g => g.Value).ToList();
            }
            else
            {
                JToken token = command.Args.Property("primaryOwnerId")?.Value;
                if (token == null || token.Type != JTokenType.String || string.IsNullOrEmpty((string)token))
                {
                    throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'primaryOwnerId' property specified.");
                }

                var primaryOwnerIdString = (string)token;
                if (!Guid.TryParse(primaryOwnerIdString, out primaryOwnerId))
                {
                    throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have 'primaryOwnerId' property specified as a valid guid.");
                }

                var jarray = command.Args.Property("additionalOwnerIds")?.Value;
                if (jarray != null)
                {
                    if (jarray.Type != JTokenType.Array)
                    {
                        throw new CBaseException(ErrorMessages.Generic, command.Name + " command 'additionalOwnerIds' must be an array.");
                    }

                    var additionalOwnerIdsJson = (JArray)jarray;
                    additionalOwnerIds = new List<Guid>();
                    foreach (var additionalOwnerIdString in additionalOwnerIdsJson.Values<string>())
                    {
                        Guid additionalOwnerId;
                        if (!Guid.TryParse(additionalOwnerIdString, out additionalOwnerId))
                        {
                            throw new CBaseException(ErrorMessages.Generic, command.Name + " command must have Guid id strings for 'additionalOwnerIds'.");
                        }

                        additionalOwnerIds.Add(additionalOwnerId);
                    }
                }
            }

            var setOwnership = new SetOwnership(
                DataPath.Create(pathString),
                recordId.Value,
                associationName,
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryOwnerId),
                additionalOwnerIds?.Select(additionalId => DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(additionalId)));

            return new ServiceMessage<SetOwnership>(command.OperationId, command.Name, setOwnership);
        }

        /// <summary>
        /// Gets the set order command from the request, if it exists.
        /// </summary>
        /// <returns>The set order command or null if it doesn't exist.</returns>
        /// <exception cref="CBaseException">Thrown when the command is not correctly specified in the request.</exception>
        private ServiceMessage<SetOrder> GetSetOrderCommand()
        {
            var command = this.rawCommands.Value.SingleOrDefault(c => c.Name.Equals("setOrder", StringComparison.OrdinalIgnoreCase));
            if (command == null)
            {
                return null;
            }

            JToken pathToken = command.Args.Property("path")?.Value;
            if (pathToken == null || pathToken.Type != JTokenType.String || string.IsNullOrEmpty((string)pathToken))
            {
                throw new CBaseException(ErrorMessages.Generic, "SetOrder command must have 'path' property specified.");
            }

            string pathString = (string)pathToken;

            JToken collectionNameToken = command.Args.Property("collectionName")?.Value;
            string collectionName = null;
            if (collectionNameToken == null || collectionNameToken.Type != JTokenType.String || string.IsNullOrEmpty((string)collectionNameToken))
            {
                throw new CBaseException(ErrorMessages.Generic, "SetOrder command must have 'collectionName' property specified.");
            }

            collectionName = (string)collectionNameToken;

            JToken uladAppIdToken = command.Args.Property("uladApplicationId")?.Value;
            Guid? uladAppId = null;
            if (uladAppIdToken != null)
            {
                if (uladAppIdToken.Type != JTokenType.String || string.IsNullOrEmpty((string)uladAppIdToken))
                {
                    throw new CBaseException(ErrorMessages.Generic, "The SetOrder command, when populating the 'uladApplicationId', must specify it as a non-empty string value.");
                }

                Guid testVal;
                if (!Guid.TryParse((string)uladAppIdToken, out testVal))
                {
                    throw new CBaseException(ErrorMessages.Generic, "The SetOrder command, when populating the 'uladApplicationId', must specify it as a valid guid.");
                }

                uladAppId = testVal;
            }

            JToken idsJson = command.Args.Property("order")?.Value;
            if (idsJson == null || idsJson.Type != JTokenType.Array || idsJson.Values<JToken>().Any(t => t.Type != JTokenType.String))
            {
                throw new CBaseException(ErrorMessages.Generic, "SetOrder command must have 'order' property specified as array with identifier strings.");
            }

            var ids = new List<Guid>();
            foreach (var recordIdString in idsJson.Values<string>())
            {
                Guid recordId;
                if (!Guid.TryParse(recordIdString, out recordId))
                {
                    throw new CBaseException(ErrorMessages.Generic, "SetOrder command must have Guid id strings for 'order'.");
                }

                ids.Add(recordId);
            }

            var setOrder = new SetOrder(
                DataPath.Create(pathString),
                collectionName,
                uladAppId,
                ids);

            return new ServiceMessage<SetOrder>(command.OperationId, command.Name, setOrder);
        }

        /// <summary>
        /// Gets a value indicating whether this json property corresponds to a leaf node in the request tree.
        /// </summary>
        /// <param name="token">The property to check.</param>
        /// <returns>True if the property is a leaf node.</returns>
        private bool IsLeafToken(JToken token)
        {
            switch (token.Type)
            {
                case JTokenType.String:
                case JTokenType.Null:
                case JTokenType.Integer:
                case JTokenType.Float:
                case JTokenType.Boolean:
                case JTokenType.Date:
                    return true;
                case JTokenType.Array:
                case JTokenType.Object:
                case JTokenType.Property:
                    return false;
                case JTokenType.Comment:
                case JTokenType.None:
                case JTokenType.Constructor:
                case JTokenType.Undefined:
                case JTokenType.Raw:
                case JTokenType.Bytes:
                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                default:
                    throw new UnhandledEnumException(token.Type);
            }
        }

        /// <summary>
        /// Gets the value of a property.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <returns>
        /// The extracted value.
        /// </returns>
        private string GetValueFromProperty(JProperty property)
        {
            switch (property.Value.Type)
            {
                case JTokenType.Null:
                    return null;
                case JTokenType.String:
                    return (string)property.Value;
                case JTokenType.Boolean:
                    return ((bool)property.Value).ToString();
                case JTokenType.Integer:
                    return this.converter.ToCountString((int)property.Value);
                case JTokenType.Float:
                    return this.converter.ToFloatString((float)property.Value);
                case JTokenType.Date:
                    return this.converter.ToDateTimeString((DateTime)property.Value, displayTime: true);
                case JTokenType.Guid:
                    return this.converter.ToGuidString((Guid)property.Value);
                case JTokenType.None:
                case JTokenType.Object:
                case JTokenType.Array:
                case JTokenType.Constructor:
                case JTokenType.Property:
                case JTokenType.Comment:
                case JTokenType.Undefined:
                case JTokenType.Raw:
                case JTokenType.Bytes:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                default:
                    throw new UnhandledEnumException(property.Value.Type);
            }
        }

        /// <summary>
        /// Parses the requests into raw commands.
        /// </summary>
        /// <param name="requestJson">The request json.</param>
        /// <returns>The raw commands parsed from the request.</returns>
        private IReadOnlyList<RawCommand> ParseRawCommands(JObject requestJson)
        {
            var rawCommands = new List<RawCommand>();

            JArray requests = requestJson.SelectToken("data.requests") as JArray;
            if (requests == null)
            {
                throw new CBaseException(
                    Common.ErrorMessages.Generic,
                    "Request must have 'requests' specified as an array.");
            }

            foreach (var child in requests.Children<JObject>())
            {
                var nameToken = child.SelectToken("name");
                var name = (string)nameToken;

                var operationIdToken = child.SelectToken("operationId");
                var operationId = (string)operationIdToken;

                var argsToken = child.SelectToken("args");
                var args = (JObject)argsToken;

                rawCommands.Add(new RawCommand(name, operationId, args));
            }

            return rawCommands;
        }

        /// <summary>
        /// Parses the raw commands into a list of type T.
        /// </summary>
        /// <typeparam name="T">The type of the command that is being parsed from the raw command.</typeparam>
        /// <param name="commandName">The name of the command.</param>
        /// <param name="parseCommand">A factory method that will return a command of type T from a raw command.</param>
        /// <returns>The list of commands or null if there were not any.</returns>
        private IReadOnlyList<ServiceMessage<T>> ParseCommands<T>(string commandName, Func<RawCommand, T> parseCommand)
        {
            return this.rawCommands.Value
                .Where(c => c.Name.Equals(commandName, StringComparison.OrdinalIgnoreCase))
                .Select(raw => new ServiceMessage<T>(raw.OperationId, raw.Name, parseCommand(raw)))
                .ToList();
        }

        /// <summary>
        /// A raw command from the request. Intermediate representation between JSON and executable Command.
        /// </summary>
        private class RawCommand
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RawCommand"/> class.
            /// </summary>
            /// <param name="name">The command name.</param>
            /// <param name="operationId">The operation id.</param>
            /// <param name="args">The arguments for the command.</param>
            public RawCommand(string name, string operationId, JObject args)
            {
                this.Name = name;
                this.OperationId = operationId;
                this.Args = args;
            }

            /// <summary>
            /// Gets the transaction id.
            /// </summary>
            public string OperationId { get; }

            /// <summary>
            /// Gets the command name.
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Gets the arguments object for the command.
            /// </summary>
            public JObject Args { get; }
        }
    }
}
