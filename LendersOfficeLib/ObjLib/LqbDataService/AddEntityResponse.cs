﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The response data for an AddEntity command.
    /// </summary>
    public sealed class AddEntityResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddEntityResponse"/> class.
        /// </summary>
        /// <param name="command">The add entity command.</param>
        /// <param name="entityId">The identifier for the added entity.</param>
        public AddEntityResponse(AddEntity command, Guid entityId)
        {
            this.Command = command;
            this.EntityId = entityId.ToString("D");
        }

        /// <summary>
        /// Gets the identifier for the added entity.
        /// </summary>
        public string EntityId { get; }

        /// <summary>
        /// Gets the path to the command target.
        /// </summary>
        public string Path
        {
            get { return this.Command.Path.ToString(); }
        }

        /// <summary>
        /// Gets the collection to which a new entity will get added.
        /// </summary>
        public string CollectionName
        {
            get { return this.Command.CollectionName; }
        }

        /// <summary>
        /// Gets the identifier for the owner of the new entity.
        /// </summary>
        public string OwnerId
        {
            get { return this.Command.OwnerId?.ToString("D"); }
        }

        /// <summary>
        /// Gets the command.
        /// </summary>
        private AddEntity Command { get; }
    }
}
