﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using DataAccess;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Formatter for the endpoint, where we want to ensure that enum values are transmitted as integers.
    /// </summary>
    internal class FormatForEndpoint : FormatEntityAsString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatForEndpoint"/> class.
        /// </summary>
        /// <param name="target">The target format.</param>
        /// <param name="direction">The target direction.</param>
        public FormatForEndpoint(FormatTarget target, FormatDirection direction)
            : base(target, direction)
        {
        }

        /// <summary>
        /// Format an enumeration value into a string.
        /// </summary>
        /// <typeparam name="E">The enumeration type.</typeparam>
        /// <param name="value">The enumeration value.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string represenation of the enumeration value.</returns>
        public override string FormatEnum<E>(E? value, string defaultValue)
        {
            if (!typeof(E).IsEnum)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (value == null)
            {
                return defaultValue;
            }
            else
            {
                var numeric = Convert.ToInt64(value.Value);
                return numeric.ToString();
            }
        }
    }
}