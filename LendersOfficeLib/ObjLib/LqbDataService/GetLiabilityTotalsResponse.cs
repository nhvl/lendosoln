﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The data needed to generate a response for the GetLiabilityTotals command.
    /// </summary>
    public class GetLiabilityTotalsResponse : GetTotalsResponse<DataObjectKind.Liability>
    {
        /// <summary>
        /// The asset totals result from the command.
        /// </summary>
        private LiabilityTotals totals;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetLiabilityTotalsResponse"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="totals">The totals resulting from the command.</param>
        /// <param name="formatter">The formatter.</param>
        internal GetLiabilityTotalsResponse(GetTotals<DataObjectKind.Liability> command, LiabilityTotals totals, FormatEntityAsString formatter)
            : base(command, formatter)
        {
            this.totals = totals;
        }

        /// <summary>
        /// The total balance.
        /// </summary>
        public string Balance => this.Formatter.Format(this.totals.Balance);

        /// <summary>
        /// The total payment.
        /// </summary>
        public string Payment => this.Formatter.Format(this.totals.Payment);

        /// <summary>
        /// The payoff amount total for liabilities marked as will be paid off.
        /// </summary>
        public string PaidOff => this.Formatter.Format(this.totals.PaidOff);
    }
}
