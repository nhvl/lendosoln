﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides the functionality needed to traverse, expand, and retrieve values for a path tree.
    /// </summary>
    public class LqbGetRequestResolver
    {
        /// <summary>
        /// Builds the response tree from the given request.
        /// </summary>
        /// <param name="requestRoot">The request root.</param>
        /// <param name="responseRootValue">The value for the root response node.</param>
        /// <param name="formatter">The string formatter to use when an entity that supports formatting is encountered.</param>
        /// <returns>The response tree with values populated.</returns>
        public static GetResponseNode ResolveGetRequest(RequestNode requestRoot, IPathResolvable responseRootValue, IFormatAsString formatter)
        {
            var children = new List<GetResponseNode>();

            foreach (var requestChild in requestRoot.Children)
            {
                var responseChild = ResolveGetRequestRecursively(requestChild, responseRootValue, formatter);
                children.Add(responseChild);
            }

            return new GetResponseNode(null, children, responseRootValue);
        }

        /// <summary>
        /// Builds the response tree from the given request node.
        /// </summary>
        /// <param name="requestNode">The request node.</param>
        /// <param name="parent">The path resolvable parent value.</param>
        /// <param name="formatter">The string formatter to use when an entity that supports formatting is encountered.</param>
        /// <returns>The response tree with values populated.</returns>
        private static GetResponseNode ResolveGetRequestRecursively(RequestNode requestNode, IPathResolvable parent, IFormatAsString formatter)
        {
            object responseNodeValue = parent.GetElement(requestNode.PathComponent);
            if (responseNodeValue is IPathableStringFormatterProvider)
            {
                responseNodeValue = ((IPathableStringFormatterProvider)responseNodeValue).GetStringFormatter(formatter, null);
            }

            if (responseNodeValue is IPathResolvableExpandable && requestNode.IsLeafNode)
            {
                var responseChildren = new List<GetResponseNode>();

                foreach (var child in ((IPathResolvableExpandable)responseNodeValue).GetReadableChildren())
                {
                    var responseNode = ResolveGetRequestRecursively(new RequestNode(child, null), (IPathResolvable)responseNodeValue, formatter);
                    responseChildren.Add(responseNode);
                }

                return new GetResponseNode(requestNode.PathComponent, responseChildren, responseNodeValue);
            }
            else if (responseNodeValue is IPathResolvable && requestNode.IsLeafNode)
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "IPathResolvable node must have child nodes.");
            }
            else if (responseNodeValue is IPathResolvableCollection)
            {
                if (requestNode.IsLeafNode)
                {
                    throw new CBaseException(Common.ErrorMessages.Generic, "IPathResolvableCollection nodes must have child nodes.");
                }
                else if (requestNode.Children.Any(node => IsAnyNode(node)))
                {
                    var anyNodes = requestNode.Children.Where(node => IsAnyNode(node));
                    if (anyNodes.Count() > 1)
                    {
                        throw new CBaseException(
                            Common.ErrorMessages.Generic,
                            "Encountered more than 1 ANY node specified for collection: " + requestNode.PathComponent.Name);
                    }

                    var anyNode = anyNodes.Single();
                    var expandedRequestNodes = ((IPathResolvableCollection)responseNodeValue).GetAllKeys()
                        .Select(path => new RequestNode(path, anyNode.Children));

                    var responseChildren = new List<GetResponseNode>();
                    foreach (var requestChild in expandedRequestNodes)
                    {
                        var responseChild = ResolveGetRequestRecursively(requestChild, (IPathResolvable)responseNodeValue, formatter);
                        responseChildren.Add(responseChild);
                    }

                    return new GetResponseNode(requestNode.PathComponent, responseChildren, responseNodeValue);
                }
            }

            if (requestNode.IsLeafNode)
            {
                return new GetResponseNode(requestNode.PathComponent, children: null, value: responseNodeValue);
            }
            else
            {
                if (responseNodeValue is IPathResolvable)
                {
                    var responseChildren = new List<GetResponseNode>();

                    foreach (var child in requestNode.Children)
                    {
                        var responseNode = ResolveGetRequestRecursively(child, (IPathResolvable)responseNodeValue, formatter);
                        responseChildren.Add(responseNode);
                    }

                    return new GetResponseNode(requestNode.PathComponent, responseChildren, responseNodeValue);
                }
                else
                {
                    throw new CBaseException(Common.ErrorMessages.Generic, "Node with children must support IPathResolvable.");
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given node is an "Any" node which should be expanded.
        /// </summary>
        /// <param name="node">The request node.</param>
        /// <returns>True if the node is an "Any" node. False otherwise.</returns>
        private static bool IsAnyNode(RequestNode node)
        {
            if (node.PathComponent == null)
            {
                return false;
            }

            return node.PathComponent.Name.Equals(DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase);
        }
    }
}
