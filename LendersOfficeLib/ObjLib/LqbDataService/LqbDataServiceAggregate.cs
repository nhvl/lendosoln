﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using DataAccess;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides access to the top-level entities that are accessible to the LqbDataService.
    /// </summary>
    public class LqbDataServiceAggregate : IPathResolvable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbDataServiceAggregate"/> class.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        public LqbDataServiceAggregate(CPageData loan)
        {
            this.Loan = loan;
        }

        /// <summary>
        /// Gets the loan file.
        /// </summary>
        private CPageData Loan { get; }

        /// <summary>
        /// Retrieves the object at the given path.
        /// </summary>
        /// <param name="element">The path to the element.</param>
        /// <returns>The object at the given path.</returns>
        /// <exception cref="CBaseException">Thrown when an unsupported path or unsupported path type is provided.</exception>
        public object GetElement(IDataPathElement element)
        {
            if (element is DataPathBasicElement)
            {
                switch (element.Name.ToLower())
                {
                    case "loan": return this.Loan;
                    default: throw new CBaseException(Common.ErrorMessages.Generic, $"{nameof(LqbDataServiceAggregate)} does not support {element.Name}.");
                }
            }

            throw new CBaseException(Common.ErrorMessages.Generic, $"{nameof(LqbDataServiceAggregate)} only supports {nameof(DataPathBasicElement)}.");
        }
    }
}
