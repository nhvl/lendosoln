﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The data needed to generate a response for the income source totals.
    /// </summary>
    public class GetIncomeSourceTotalsResponse : GetTotalsResponse<DataObjectKind.IncomeSource>
    {
        /// <summary>
        /// The totals result from the command.
        /// </summary>
        private readonly IncomeSourceTotals totals;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetIncomeSourceTotalsResponse"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="totals">The totals result from the command.</param>
        /// <param name="formatter">The formatter.</param>
        internal GetIncomeSourceTotalsResponse(GetTotals<DataObjectKind.IncomeSource> command, IncomeSourceTotals totals, IFormatAsString formatter)
            : base(command, formatter)
        {
            this.totals = totals;
        }

        /// <summary>
        /// Gets the total monthly income amount.
        /// </summary>
        public string MonthlyAmount => this.Formatter.Format(this.totals.MonthlyAmount);
    }
}
