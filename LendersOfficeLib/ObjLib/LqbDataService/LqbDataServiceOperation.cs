﻿namespace LendersOffice.ObjLib.LqbDataService
{
    /// <summary>
    /// Operations for data service.
    /// </summary>
    public enum LqbDataServiceOperation
    {
        /// <summary>
        /// Perform the requested actions but do not save.
        /// </summary>
        Load = 0,

        /// <summary>
        /// Perform the requested actions, saving upon completion.
        /// </summary>
        Save = 1
    }
}
