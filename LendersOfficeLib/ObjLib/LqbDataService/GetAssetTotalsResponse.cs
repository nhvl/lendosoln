﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using LendingQB.Core.Commands;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The data needed to generate a response for the GetAssetTotals command.
    /// </summary>
    public class GetAssetTotalsResponse : GetTotalsResponse<DataObjectKind.Asset>
    {
        /// <summary>
        /// The asset totals result from the command.
        /// </summary>
        private AssetTotals totals;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetAssetTotalsResponse"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="totals">The totals resulting from the command.</param>
        /// <param name="formatter">The formatter.</param>
        internal GetAssetTotalsResponse(GetTotals<DataObjectKind.Asset> command, AssetTotals totals, IFormatAsString formatter)
            : base(command, formatter)
        {
            this.totals = totals;
        }

        /// <summary>
        /// The subtotal for REO market value.
        /// </summary>
        public string SubtotalReo => this.Formatter.Format(this.totals.SubtotalReo);

        /// <summary>
        /// The subtotal for liquid assets.
        /// </summary>
        public string SubtotalLiquid => this.Formatter.Format(this.totals.SubtotalLiquid);

        /// <summary>
        /// The subtotal for illiquid assets.
        /// </summary>
        public string SubtotalIlliquid => this.Formatter.Format(this.totals.SubtotalIlliquid);

        /// <summary>
        /// The sum of the subtotal values.
        /// </summary>
        public string Total => this.Formatter.Format(this.totals.Total);
    }
}
