﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Web;
    using Common;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Handler for LqbDataService, which is used to load and save data in the system.
    /// </summary>
    public class LqbDataServiceHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reusable.
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes an incoming request and writes the response.
        /// </summary>
        /// <param name="context">The http context.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "We want to provide an error message to the client in the case of all errors.")]
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                var loanIdGuid = Guid.Parse(RequestHelper.GetSafeQueryString("loanid"));
                var loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(loanIdGuid);
                var operationQueryParam = RequestHelper.GetSafeQueryString("op");
                var operation = operationQueryParam.ToNullableEnum<LqbDataServiceOperation>(ignoreCase: true) ?? LqbDataServiceOperation.Load;

                string request = this.ReadRequest(context);

                string response = LqbDataService.ProcessRequest(loanId, operation, request);

                context.Response.Write(response);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                var errorMessage = this.GetErrorMessage(e);
                var errorResponse = LqbDataService.ErrorResponse(errorMessage);
                context.Response.Write(errorResponse);
            }
        }

        /// <summary>
        /// Reads the request from the http context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The request body.</returns>
        private string ReadRequest(HttpContext context)
        {
            using (var reader = new StreamReader(context.Request.InputStream))
            {
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Gets the error message to send to the user based on an Exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>The error message to return to the client.</returns>
        private string GetErrorMessage(Exception e)
        {
            string errorMessage = null;
            if (e is CBaseException)
            {
                var userMessage = ((CBaseException)e).UserMessage;
                errorMessage = string.IsNullOrEmpty(userMessage)
                    ? ErrorMessages.Generic
                    : userMessage;
            }
            else
            {
                errorMessage = ErrorMessages.Generic;
            }

            return errorMessage;
        }
    }
}
