﻿﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Represents a node in the request tree.
    /// </summary>
    public class RequestNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestNode"/> class.
        /// </summary>
        /// <param name="pathComponent">The path component.</param>
        /// <param name="children">The child nodes.</param>
        public RequestNode(IDataPathElement pathComponent, IReadOnlyList<RequestNode> children)
        {
            this.PathComponent = pathComponent;
            this.Children = children;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestNode"/> class.
        /// </summary>
        /// <param name="pathComponent">The path component.</param>
        /// <param name="children">The child nodes.</param>
        /// <param name="value">The value to set for this node.</param>
        public RequestNode(IDataPathElement pathComponent, IReadOnlyList<RequestNode> children, string value)
        {
            this.PathComponent = pathComponent;
            this.Children = children;
            this.Value = value;
        }

        /// <summary>
        /// Gets the path component associated with this node.
        /// </summary>
        public IDataPathElement PathComponent { get; }

        /// <summary>
        /// Gets the child nodes.
        /// </summary>
        public IReadOnlyList<RequestNode> Children { get; }

        /// <summary>
        /// Gets the value associated with the node.
        /// </summary>
        public string Value { get; }

        /// <summary>
        /// Gets a value indicating whether this is a leaf node.
        /// </summary>
        public bool IsLeafNode => this.Children == null || !this.Children.Any();
    }
}
