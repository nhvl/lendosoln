﻿namespace LendersOffice.ObjLib.LqbDataService
{
    using System;

    /// <summary>
    /// The response data from a <see cref="LendingQB.Core.Commands.BorrowerManagementCommand"/> that adds a record.
    /// </summary>
    public class BorrowerManagementAddResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerManagementAddResponse"/> class.
        /// </summary>
        /// <param name="id">The id of the added record.</param>
        public BorrowerManagementAddResponse(Guid id)
        {
            this.Id = id.ToString("D");
        }

        /// <summary>
        /// Gets the id of the added record.
        /// </summary>
        public string Id { get; }
    }
}
