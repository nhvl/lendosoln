﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;
using System.IO;
namespace LendersOffice.Accounting
{
    // 5/23/2012 dd - See OPM 83628 for specs
    public class AccountReport
    {
        class BranchFeeItem
        {
            public decimal VAFee { get; set; }
            public decimal FHAFee { get; set; }
            public decimal ConvFee { get; set; }
        }
        class AccountReportItem
        {
            public Guid BrokerId { get; private set; }
            public string sLNm { get; private set; }
            public E_sLT sLT { get; private set; }
            public decimal sFinalLAmt { get; private set; }
            public decimal sOriginatorCompensationAmount { get; private set; }
            public Guid sEmployeeLoanRepId { get; private set; }
            public string sEmployeeLoanRepName { get; private set; }
            public decimal s800U1F { get; private set; } // Per specs - Will always assume 813 to be funding fee.
            public string sBranchNm { get; private set; }
            public string sInvestorLockAdjustXmlContent { get; private set; }
            public string aBFirstNm { get; private set; }
            public string aBLastNm { get; private set; }
            public string sFundD { get; private set; }
            public string sLPurchaseD { get; private set; }
            private BranchFeeItem m_branchFeeItem = null;
            public AccountReportItem(DbDataReader reader, Guid brokerId, Dictionary<Guid, decimal> employeeCommissionTable, Dictionary<string, BranchFeeItem> branchFeeTable)
            {
                BrokerId = brokerId;

                sLNm = reader.GetSafeValue("sLNm", string.Empty);
                sLT = (E_sLT)reader.GetSafeValue("sLT", (int)E_sLT.Conventional);
                sFinalLAmt = reader.GetSafeValue("sFinalLAmt", 0.0M);
                sOriginatorCompensationAmount = reader.GetSafeValue("sOriginatorCompensationAmount", 0.0M);
                sEmployeeLoanRepId = reader.GetSafeValue("sEmployeeLoanRepId", Guid.Empty);
                sEmployeeLoanRepName = reader.GetSafeValue("sEmployeeLoanRepName", string.Empty);
                if (BrokerId == new Guid("CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB"))
                {
                    // PML0170 - iServe Residential Lending. Always use s800U1F as funding fee.
                    s800U1F = reader.GetSafeValue("s800U1F", 0.0M);
                }
                sBranchNm = reader.GetSafeValue("sBranchNm", string.Empty);

                // 7/17/2012 dd - Use Front-End Rate Lock 
                sInvestorLockAdjustXmlContent = reader.GetSafeValue("sBrokerLockAdjustXmlContent", string.Empty);

                //if (BrokerId == new Guid("B94B7017-9E70-4DE7-A316-B5CD7D1E34BB"))
                //{
                //    sInvestorLockAdjustXmlContent = reader.GetSafeValue("sBrokerLockAdjustXmlContent", string.Empty);
                //}
                //else
                //{
                //    sInvestorLockAdjustXmlContent = reader.GetSafeValue("sInvestorLockAdjustXmlContent", string.Empty);
                //}
                
                employeeCommissionTable.TryGetValue(sEmployeeLoanRepId, out m_loanRepSplitPercent);
                branchFeeTable.TryGetValue(sBranchNm, out m_branchFeeItem);
                aBFirstNm = reader.GetSafeValue("aBFirstNm", string.Empty);
                aBLastNm = reader.GetSafeValue("aBLastNm", string.Empty);

                DateTime dt = (DateTime)reader.GetSafeValue("sFundD", DateTime.MinValue);
                if (dt != DateTime.MinValue)
                {
                    sFundD = dt.ToString("MM-dd-yyyy");
                }
                else
                {
                    sFundD = string.Empty;
                }

                dt = (DateTime)reader.GetSafeValue("sLPurchaseD", DateTime.MinValue);
                if (dt != DateTime.MinValue)
                {
                    sLPurchaseD = dt.ToString("MM-dd-yyyy");
                }
                else
                {
                    sLPurchaseD = string.Empty;
                }
            }
            private decimal m_loanRepSplitPercent = 0;
            public decimal LoanRepSplitPercent
            {
                get
                {
                    return m_loanRepSplitPercent;
                }

            }
            public decimal LoanRepSplitAmount
            {
                get
                {
                    return Math.Round(LoanRepSplitPercent * sOriginatorCompensationAmount / 100, 2);
                }
            }
            public decimal BranchSplitPercent
            {
                get { return 100 - LoanRepSplitPercent; }
            }
            public decimal BranchSplitAmount
            {
                get
                {
                    return sOriginatorCompensationAmount - LoanRepSplitAmount;
                }
            }

            private List<PricingAdjustment> x_pricingAddjustmentList = null;

            private List<PricingAdjustment> PricingAdjustmentList
            {
                get
                {
                    if (x_pricingAddjustmentList == null)
                    {
                        if (string.IsNullOrEmpty(sInvestorLockAdjustXmlContent))
                        {
                            x_pricingAddjustmentList = new List<PricingAdjustment>();
                        }
                        else
                        {
                            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<PricingAdjustment>));
                            using (TextReader reader = new StringReader(sInvestorLockAdjustXmlContent))
                            {
                                x_pricingAddjustmentList = (List<PricingAdjustment>)serializer.Deserialize(reader);
                            }
                            
                        }
                    }
                    return x_pricingAddjustmentList;
                }
            }
            private decimal? m_branchMargin = null;
            public decimal BranchMargin
            {
                get
                {
                    if (m_branchMargin.HasValue == false)
                    {
                        CalculateBranchMargin();
                    }
                    return m_branchMargin.Value;
                }
            }
            private decimal? m_branchMarginPercent = null;
            public decimal BranchMarginPercent
            {
                get
                {
                    if (m_branchMarginPercent.HasValue == false)
                    {
                        CalculateBranchMargin();
                    }
                    return m_branchMarginPercent.Value;
                }
            }

            private string BranchMarginKeyword
            {
                get
                {
                    if (BrokerId == new Guid("CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB"))
                    {
                        return "Branch Margin"; // PML0170 - iServe Residential Lending.
                    }
                    else if (BrokerId == new Guid("B94B7017-9E70-4DE7-A316-B5CD7D1E34BB"))
                    {
                        return "Price Group"; // PML0177 - THe Lending Company
                    }
                    else
                    {
                        return "Branch Margin";
                    }
                }
            }
            private void CalculateBranchMargin()
            {
                decimal v = 0;
                foreach (PricingAdjustment pricingAdjustment in PricingAdjustmentList)
                {
                    if (pricingAdjustment.Description.IndexOf(BranchMarginKeyword, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        decimal fee = 0;
                        if (decimal.TryParse(pricingAdjustment.Fee.Replace("%", ""), out fee))
                        {
                            v += fee;
                        }
                    }
                }
                m_branchMarginPercent = Math.Abs(v);
                m_branchMargin = Math.Round(sFinalLAmt * Math.Abs(v) / 100, 2);

            }

            private string BranchOverrideKeyword
            {
                get
                {
                    if (BrokerId == new Guid("CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB"))
                    {
                        return "Branch Override"; // PML0170 - iServe Residential Lending.
                    }
                    else if (BrokerId == new Guid("B94B7017-9E70-4DE7-A316-B5CD7D1E34BB"))
                    {
                        return "Corporate Margin"; // PML0177 - THe Lending Company
                    }
                    else
                    {
                        return "Branch Override";
                    }
                }

            }
            private decimal? m_branchOverride = null;
            public decimal BranchOverride
            {
                get
                {
                    if (m_branchOverride.HasValue == false)
                    {
                        CalculateBranchOverride();

                    }
                    return m_branchOverride.Value;
                }
            }

            private decimal? m_branchOverridePercent = null;
            public decimal BranchOverridePercent
            {
                get
                {
                    if (m_branchOverridePercent.HasValue == false)
                    {
                        CalculateBranchOverride();
                    }
                    return m_branchOverridePercent.Value;
                }
            }
            private void CalculateBranchOverride()
            {
                decimal v = 0;
                foreach (PricingAdjustment pricingAdjustment in PricingAdjustmentList)
                {
                    if (pricingAdjustment.Description.IndexOf(BranchOverrideKeyword, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        decimal fee = 0;
                        if (decimal.TryParse(pricingAdjustment.Fee.Replace("%", ""), out fee))
                        {
                            v += fee;
                        }
                    }
                }
                m_branchOverridePercent = Math.Abs(v);
                m_branchOverride = Math.Round(sFinalLAmt * Math.Abs(v) / 100, 2);
            }
            public decimal BranchProductFee
            {
                get
                {
                    decimal v = 0;
                    if (m_branchFeeItem != null)
                    {
                        if (sLT == E_sLT.Conventional)
                        {
                            v = m_branchFeeItem.ConvFee;
                        }
                        else if (sLT == E_sLT.FHA)
                        {
                            v = m_branchFeeItem.FHAFee;
                        }
                        else if (sLT == E_sLT.VA)
                        {
                            v = m_branchFeeItem.VAFee;
                        }
                    }
                    return v;
                }
            }

            public decimal TotalFee
            {
                get { return s800U1F + BranchProductFee; }
            }
            public decimal BranchNetFee
            {
                get
                {
                    return BranchSplitAmount + BranchMargin + BranchOverride + TotalFee;
                }
            }
        }
        public static string Generate(Guid brokerId, DateTime startDate, DateTime endDate, out string message)
        {
            StringBuilder warningMessages = new StringBuilder();

            // Build Employee Commission Table
            Dictionary<Guid, decimal> employeeCommissionTable = BuildEmployeeCommissionTable(brokerId);

            // Build Branch Product Fee Table
            Dictionary<string, BranchFeeItem> branchFeeTable = BuildBranchFeeTable(brokerId);

            // List Loan.

            HashSet<string> employeeMissingCommissionSplit = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            Dictionary<string, List<AccountReportItem>> groupByBranch = new Dictionary<string,List<AccountReportItem>>(StringComparer.OrdinalIgnoreCase);
            //List<AccountReportItem> list = new List<AccountReportItem>();

            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59); // Include report up to midnight.
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@StartD", startDate),
                                            new SqlParameter("@EndD", endDate)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "Accounting_ListFundedLoansByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    AccountReportItem item = new AccountReportItem(reader, brokerId, employeeCommissionTable, branchFeeTable);

                    if (item.LoanRepSplitPercent <= 0)
                    {
                        employeeMissingCommissionSplit.Add(item.sEmployeeLoanRepName);
                    }
                    List<AccountReportItem> list = null;

                    if (groupByBranch.TryGetValue(item.sBranchNm, out list) == false) 
                    {
                        list = new List<AccountReportItem>();
                        groupByBranch.Add(item.sBranchNm, list);
                    }
                    list.Add(item);
                }
            }

            if (employeeMissingCommissionSplit.Count > 0)
            {
                warningMessages.AppendLine("These loan officer missing commission split.");
                foreach (var loanOfficer in employeeMissingCommissionSplit)
                {
                    warningMessages.AppendLine("    " + loanOfficer);
                }
            }
            message = warningMessages.ToString();

            // Build CSV

            StringBuilder csv = new StringBuilder();
            csv.AppendFormat("Accounting Report: Funded Date {0} - {1}", startDate.ToShortDateString(), endDate.ToShortDateString());
            csv.AppendLine();
            csv.AppendLine();

            // Build Header
            string[] headerList = new string[] 
            { 
                                                        "Loan Officer Name", 
                                        "Loan Number", 
                                        "Funded Date",
                                        "Purchased Date",
                                        "Last Name",
                                        "First Name",
                                        "Loan Type", 
                                        "Loan Amount", 
                                        "Total Compensation", 
                                        "LO Split %", 
                                        "LO Net $", 
                                        "Branch Split %", 
                                        "Branch Split $",
                                        "Branch Margin %",
                                        "Branch Margin $",
                                        "Branch Override %",
                                        "Branch Override $", 
                                        "Funding Fee", 
                                        "Branch Product Fee", 
                                        "Total Fee", 
                                        "Branch Net $" 
            };


            foreach (var branch in groupByBranch.OrderBy(o => o.Key))
            {
                csv.AppendLine("Branch: " + branch.Key);
                csv.AppendLine();
                csv.AppendLine(BuildRow(headerList));
                csv.AppendLine();

                foreach (var item in branch.Value.OrderBy(o =>o.sEmployeeLoanRepName))
                {
                    csv.AppendLine(BuildRow(new object[] {
                        item.sEmployeeLoanRepName,

                        item.sLNm,
                        item.sFundD,
                        item.sLPurchaseD,
                        item.aBLastNm,
                        item.aBFirstNm,
                        item.sLT.ToString(),
                        item.sFinalLAmt,
                        item.sOriginatorCompensationAmount,
                        item.LoanRepSplitPercent,
                        item.LoanRepSplitAmount,
                        item.BranchSplitPercent,
                        item.BranchSplitAmount,
                        item.BranchMarginPercent,
                        item.BranchMargin,
                        item.BranchOverridePercent,
                        item.BranchOverride,
                        item.s800U1F,
                        item.BranchProductFee,
                        item.TotalFee,
                        item.BranchNetFee

                    }));
                }
                csv.AppendLine();
                csv.AppendLine();
            }
            return csv.ToString();
        }
        private static string BuildRow(object[] data)
        {
            StringBuilder line = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                if (i != 0)
                {
                    line.Append(",");
                }
                if (data[i].GetType() == typeof(string))
                {
                    line.Append("\"" + data[i] + "\"");
                }
                else
                {
                    line.Append(data[i].ToString());
                }
            }
            return line.ToString();
        }
        private static Dictionary<string, BranchFeeItem> BuildBranchFeeTable(Guid brokerId)
        {
            // 5/23/2012 dd - TODO. Parse from CSV.
            // Hard code thing for now.
            Dictionary<string, BranchFeeItem> dictionary = new Dictionary<string, BranchFeeItem>(StringComparer.OrdinalIgnoreCase);

            if (brokerId == new Guid("CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB"))
            {
                // Hard code for PML 0170 - iServe Residential Lending
                dictionary.Add("San Leandro", new BranchFeeItem() { VAFee = 200, FHAFee = 200, ConvFee = 250 });
                dictionary.Add("Napa", new BranchFeeItem() { VAFee = 200, FHAFee = 200, ConvFee = 250 });
                dictionary.Add("Pleasanton", new BranchFeeItem() { VAFee = 200, FHAFee = 200, ConvFee = 250 });
                dictionary.Add("San Diego", new BranchFeeItem() { VAFee = 200, FHAFee = 200, ConvFee = 250 });
                dictionary.Add("Cleveland", new BranchFeeItem() { VAFee = 150, FHAFee = 150, ConvFee = 150 });
            }
            return dictionary;
        }
        private static Dictionary<Guid, decimal> BuildEmployeeCommissionTable(Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            Dictionary<Guid, decimal> dictionary = new Dictionary<Guid, decimal>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "Accounting_RetrieveEmployeeLOSplitByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    dictionary.Add((Guid)reader["EmployeeId"], (decimal)reader["CommissionPointOfGrossProfit"]);
                }
            }
            return dictionary;
        }
    }
}
