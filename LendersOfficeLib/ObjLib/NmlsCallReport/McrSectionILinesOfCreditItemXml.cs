﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public class McrSectionILinesOfCreditItemXml : AbstractMcrXml
    {
        public ulong? ItemId { get; set; }
        public string Name { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal AvailableAtPeriodEnd { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("SectionILinesOfCreditItem");

            Write(writer, "ItemId", ItemId);
            Write(writer, "ACLOC", Name);
            WriteDollar(writer, "ACLOC_1", CreditLimit);
            WriteDollar(writer, "ACLOC_2", AvailableAtPeriodEnd);

            writer.WriteEndElement();
        }
    }
}
