﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrReportSectionIColumnT
    {
        Brokered_Column1 = 0,
        Retail_Column2,
        Wholesale_Column3
    }
}
