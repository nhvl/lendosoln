﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrPeriodT
    {
        Undefined,
        Q1,
        Q2,
        Q3,
        Q4,
        FiscalQ1,
        FiscalQ2,
        FiscalQ3,
        FiscalAnnual,
        FiscalYearToDate


    }
}
