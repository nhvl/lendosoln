﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrReportSection3ItemT
    {
        S100_LoanModProcessAtBeginning = 0,
        S110_LoanModCompleted,
        S120_LoanModTerminatedByBorrower,
        S130_LoanModDeniedByLender,
        S140_LoanModTerminatedByOther,
        S150_LoanModReceivedDuringPeriod,
        S160_LoanModInProcesAtEndOfPeriod,

        S200_LoanTobeModifiedAtBeginning,
        S210_LoanModCompleted,
        S220_LoanModAttemtsTerminated,
        S230_NewLoanReceived,
        S240_LoanModifiedAtEndOfPeriod,

        S300_30DaysDelinqent,
        S305_30_60DaysDelinquent,
        S310_61_90DaysDelinquent,
        S315_90DaysDelinquent,
        
        S320_30DaysDelinquent,
        S325_30_60DaysDelinquent,
        S330_61_90DaysDelinquent,
        S335_90DaysDelinquent,

        S340_30DaysDelinquent,
        S345_30_60DaysDelinquent,
        S350_61_90DaysDelinquent,
        S355_90DaysDelinquent,

        S400_InForeclosureAtLastPeriodEndDate,
        S410_MovedIntoForeclosure,
        S420_ForeclosureResolvedOther,
        S430_ForeclosureResultingSheriffSale,
        S440_ForeclosureAsOfEndDate,
        S450_Reo
        
    }
}
