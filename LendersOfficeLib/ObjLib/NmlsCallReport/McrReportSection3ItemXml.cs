﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public class McrReportSection3ItemXml : AbstractMcrXml
    {
        public E_McrReportSection3ItemT ItemT { get; set; }

        public int? Count { get; set; }
        public decimal? Amount { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            string baseId = GetSectionId(ItemT);

            string amountId = baseId + "_1";
            string countId = baseId + "_2";

            WriteDollar(writer, amountId, Amount);
            Write(writer, countId, Count);
        }
        private string GetSectionId(E_McrReportSection3ItemT item)
        {
            return item.ToString().Split('_')[0];
        }
    }
}
