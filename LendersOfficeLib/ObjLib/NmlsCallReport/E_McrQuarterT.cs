﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrQuarterT
    {
        [DataAccess.OrderedDescriptionAttribute("Q1 (January-March)")]
        Q1,
        
        [DataAccess.OrderedDescriptionAttribute("Q2 (April-June)")]
        Q2,
        
        [DataAccess.OrderedDescriptionAttribute("Q3 (July-September)")]
        Q3,
        
        [DataAccess.OrderedDescriptionAttribute("Q4 (October-December)")]
        Q4
    }
}
