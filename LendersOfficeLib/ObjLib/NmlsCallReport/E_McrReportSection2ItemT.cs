﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrReportSection2ItemT
    {
        I010_GovernmentFixed = 0,
        I020_GovernmentArm,
        I030_PrimeConfFixed,
        I040_PrimeConfArm,
        I050_PrimeJumboFixed,
        I060_PrimeJumboArm,
        I070_OtherFixed,
        I080_OtherArm,
        I100_TotalResidential,

        I110_ClosedEndSecond,
        I120_FundedHelocs,
        I130_ReverseMortgages,
        I140_Construction,
        I150_Multifamily,
        I160_Commercial,
        I170_OtherMortgage,
        I180_TotalOther,

        I200_TotalMortgagesOriginated,

        I210_Retail,
        I220_Correspondent,
        I230_Broker,
        I240_TotalResidential,

        I250_FixedRate,
        I251_ARM,
        I259_TotalResidential,

        I260_Jumbo,
        I261_NonJumbo,
        I269_TotalResidential,

        I270_AltDoc,
        I271_FullDoc,
        I279_TotalResidential,

        I280_Interest_Only,
        I281_Not_Interest_Only,

        I290_OptionArm,
        I291_NotOptionArm,
        I299_TotalResidential,

        I300_PrepaymentPenalties,
        I301_NoPrepaymentPenalties,
        I309_TotalResidential,

        I310_Purchase,
        I311_RefiRateTerm,
        I312_RefiCashout,
        I313_RefiRestructure,
        I314_RefiOther,
        I319_TotalResidential,

        I320_OwnerOccupied,
        I321_NonOwnerOccupied,
        I329_TotalResidential,

        I330_Pmi,
        I331_NoPmi,
        I339_TotalResidential,

        I340_LoanWithPiggyback,
        I341_LoanWithoutPiggyback,
        I349_TotalResidential,

        I350_FicoLess600,
        I351_Fico600_650,
        I352_Fico650_700,
        I353_Fico700_750,
        I354_Fico750,
        I359_TotalResidential,

        I360_AvgFicoForFirstMortgage,
        I365_AvgFicoForSecondMortgage,

        I370_Ltv60,
        I371_Ltv60_70,
        I372_Ltv70_80,
        I373_Ltv80_90,
        I374_Ltv90_100,
        I375_Ltv100,
        I379_TotalResidential,

        I380_AvgLtvFirstMortgage,
        I385_AvgCltv,
        I390_AvgCoupon,

        I400_SoldToSecondaryMarket,
        I401_SoldToOtherNonAffliate,
        I402_SoldToOtherAffliate,
        I403_KeptInPortfolio,
        I404_SoldWithSaleTreatment,
        I405_SoldWithoutSaleTreatment,
        I409_TotalResidential,

        I410_SoldServicingReleased,
        I420_BrokeredOut,

        I430_FalloutRatio,

        I440_TotalMultiFamilySold,

        I450_AvgDaysSfrMortgages,
        I455_AvgDaysMultifamilyMortgages,
        I456_AvgDaysCommercial,

        I460_WarehousedExcess90Days






    }

    public class McrReportSection2Item
    {

        private static List<E_McrReportSection2ItemT> DependsOnMortgageLoanT = new List<E_McrReportSection2ItemT>() {
            E_McrReportSection2ItemT.I010_GovernmentFixed,
            E_McrReportSection2ItemT.I020_GovernmentArm,
            E_McrReportSection2ItemT.I030_PrimeConfFixed,
            E_McrReportSection2ItemT.I040_PrimeConfArm,
            E_McrReportSection2ItemT.I050_PrimeJumboFixed,
            E_McrReportSection2ItemT.I060_PrimeJumboArm,
            E_McrReportSection2ItemT.I070_OtherFixed,
            E_McrReportSection2ItemT.I080_OtherArm,
            E_McrReportSection2ItemT.I210_Retail,
            E_McrReportSection2ItemT.I220_Correspondent,
            E_McrReportSection2ItemT.I230_Broker,
            E_McrReportSection2ItemT.I250_FixedRate,
            E_McrReportSection2ItemT.I251_ARM,
            E_McrReportSection2ItemT.I260_Jumbo,
            E_McrReportSection2ItemT.I261_NonJumbo,
            E_McrReportSection2ItemT.I270_AltDoc,
            E_McrReportSection2ItemT.I271_FullDoc,
            E_McrReportSection2ItemT.I280_Interest_Only,
            E_McrReportSection2ItemT.I281_Not_Interest_Only,
            E_McrReportSection2ItemT.I290_OptionArm,
            E_McrReportSection2ItemT.I291_NotOptionArm,
            E_McrReportSection2ItemT.I300_PrepaymentPenalties,
            E_McrReportSection2ItemT.I301_NoPrepaymentPenalties,
            E_McrReportSection2ItemT.I310_Purchase,
            E_McrReportSection2ItemT.I311_RefiRateTerm,
            E_McrReportSection2ItemT.I312_RefiCashout,
            E_McrReportSection2ItemT.I313_RefiRestructure,
            E_McrReportSection2ItemT.I314_RefiOther,
            E_McrReportSection2ItemT.I320_OwnerOccupied,
            E_McrReportSection2ItemT.I321_NonOwnerOccupied,
            E_McrReportSection2ItemT.I330_Pmi,
            E_McrReportSection2ItemT.I331_NoPmi,
            E_McrReportSection2ItemT.I340_LoanWithPiggyback,
            E_McrReportSection2ItemT.I341_LoanWithoutPiggyback,
            E_McrReportSection2ItemT.I350_FicoLess600,
            E_McrReportSection2ItemT.I351_Fico600_650,
            E_McrReportSection2ItemT.I352_Fico650_700,
            E_McrReportSection2ItemT.I353_Fico700_750,
            E_McrReportSection2ItemT.I354_Fico750,
            E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage,
            E_McrReportSection2ItemT.I370_Ltv60,
            E_McrReportSection2ItemT.I371_Ltv60_70,
            E_McrReportSection2ItemT.I372_Ltv70_80,
            E_McrReportSection2ItemT.I373_Ltv80_90,
            E_McrReportSection2ItemT.I374_Ltv90_100,
            E_McrReportSection2ItemT.I375_Ltv100,
            E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage,
            E_McrReportSection2ItemT.I390_AvgCoupon
        };

        /// <summary>
        /// Given the item Type, determine if that item should include a loan with the given mortgage type.
        /// </summary>
        /// <param name="itemType">The Section II Item that is being updated.</param>
        /// <param name="mortageType">The mortgage type of the loan.</param>
        /// <returns>Whether the loan can be included in the Section II amounts for the given item.</returns>
        public static bool IsValidMortgageTForItem(E_McrReportSection2ItemT itemType, E_MortgageLoanT mortageType)
        {
            return DependsOnMortgageLoanT.Contains(itemType) ?
                !(mortageType == E_MortgageLoanT.ClosedEndSecond || mortageType == E_MortgageLoanT.FundedHELOC || mortageType == E_MortgageLoanT.Construction)
                : true;

        }
    }
}
