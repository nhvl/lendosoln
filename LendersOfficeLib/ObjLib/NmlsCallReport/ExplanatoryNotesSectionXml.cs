﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public class ExplanatoryNotesSectionXml : AbstractMcrXml
    {
        public string ExplanatoryNotes { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ExplanatoryNotesSection");

            Write(writer, "ACNOTE_1", ExplanatoryNotes);

            writer.WriteEndElement();
        }
    }
}
