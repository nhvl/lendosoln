﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public class McrReportSection1ItemXml : AbstractMcrXml
    {
        public E_McrReportSection1ItemT ItemT { get; set; }
        public E_McrReportSectionIColumnT ColumnT { get; set; }

        public int? Count { get; set; }
        public decimal? Amount { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            // 7/21/2011 dd - The element name for amount and count are in this format:
            // Column 1:
            //          {xxxxx}_1 - For Amount
            //          {xxxxx}_2 - For Count
            // Column 2:
            //          {xxxxx}_3 - For Amount
            //          {xxxxx}_4 - For Count
            // Column 3:
            //          {xxxxx}_5 - For Amount
            //          {xxxxx}_6 - For Count
            // xxxxx will be the id is E_McrReportSectionIItemT name.
            //    Example: AC010_ApplicationsInProcessAtBeginingPeriod - Id -> AC010.

            string baseId = GetSectionId(ItemT);

            string amountId = baseId + "_" + (1 + 2 * (int)ColumnT);
            string countId = baseId + "_" + (2 + 2 * (int)ColumnT);

            WriteDollar(writer, amountId, Amount);
            Write(writer, countId, Count);

        }
        private string GetSectionId(E_McrReportSection1ItemT item)
        {
            return item.ToString().Split('_')[0];
        }
    }
}
