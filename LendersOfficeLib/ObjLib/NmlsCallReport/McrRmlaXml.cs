﻿namespace LendersOffice.NmlsCallReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using CommonProjectLib.Common;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;

    public class McrRmlaXml : AbstractMcrXml
    {
        private static readonly string[] ValidStateCodes = {
                                                     "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL",
                                                     "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA",
                                                     "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV",
                                                     "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA",
                                                     "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VI", "VA",
                                                     "WA", "WI", "WV", "WY"
                                                 };
        private List<McrSectionILinesOfCreditItemXml> m_sectionILinesOfCreditItemList = new List<McrSectionILinesOfCreditItemXml>();
        private List<McrSectionIMlosItemXml> m_sectionIMlosItemList = new List<McrSectionIMlosItemXml>();

        private SortedList<int, McrReportSection1ItemXml> m_section1ItemList = new SortedList<int, McrReportSection1ItemXml>();
        private SortedList<E_McrReportSection2ItemT, McrReportSection2ItemXml> m_section2ItemList = new SortedList<E_McrReportSection2ItemT, McrReportSection2ItemXml>();
        private SortedList<E_McrReportSection3ItemT, McrReportSection3ItemXml> m_section3ItemList = new SortedList<E_McrReportSection3ItemT, McrReportSection3ItemXml>();
        public SortedList<E_McrReportSection4ItemT, McrReportSection4ItemXml> m_section4ItemList = new SortedList<E_McrReportSection4ItemT, McrReportSection4ItemXml>();

        private ExplanatoryNotesSectionXml m_explanatoryNotesSectionXml = null;
        public string StateCode { get; set; }

        public string ExplanatoryNotes
        {
            get
            {
                if (m_explanatoryNotesSectionXml == null)
                {
                    return string.Empty;
                }
                else
                {
                    return m_explanatoryNotesSectionXml.ExplanatoryNotes;
                }
            }
            set
            {
                if (m_explanatoryNotesSectionXml == null)
                {
                    m_explanatoryNotesSectionXml = new ExplanatoryNotesSectionXml();
                }
                m_explanatoryNotesSectionXml.ExplanatoryNotes = value;
            }
        }

        public void Add(McrSectionILinesOfCreditItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_sectionILinesOfCreditItemList.Add(item);
        }
        public void Add(McrSectionIMlosItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_sectionIMlosItemList.Add(item);
        }
        public void Add(McrReportSection1ItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_section1ItemList.Add((int) item.ItemT * 1000 + (int)item.ColumnT, item);
        }
        public void Add(McrReportSection2ItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_section2ItemList.Add(item.ItemT, item);
        }
        public void Add(McrReportSection3ItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_section3ItemList.Add(item.ItemT, item);
        }
        public void Add(McrReportSection4ItemXml item)
        {
            if (null == item)
            {
                return;
            }
            m_section4ItemList.Add(item.ItemT, item);
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Rmla");

            if (ValidStateCodes.Contains(StateCode) == false)
            {
                throw DataAccess.CBaseException.GenericException("Unhandled statecode=[" + StateCode + "]");
            }
            writer.WriteAttributeString("stateCode", StateCode);

            Write(writer, m_section1ItemList);
            Write(writer, m_sectionILinesOfCreditItemList);
            Write(writer, m_sectionIMlosItemList);
            Write(writer, m_section2ItemList);
            Write(writer, m_section3ItemList);
            Write(writer, m_explanatoryNotesSectionXml);

            writer.WriteEndElement();

        }
        public static E_McrQuarterT ConvertPeriodToQuarter(E_McrPeriodT periodT)
        {
           switch (periodT)
            {
                case E_McrPeriodT.Q1:
                case E_McrPeriodT.FiscalQ1:
                    return E_McrQuarterT.Q1;                     
                case E_McrPeriodT.Q2:
                case E_McrPeriodT.FiscalQ2:
                     return E_McrQuarterT.Q2;
                case E_McrPeriodT.Q3:
                case E_McrPeriodT.FiscalQ3:
                    return E_McrQuarterT.Q3;
                case E_McrPeriodT.Q4:
                    return E_McrQuarterT.Q4;
                case E_McrPeriodT.Undefined:
                case E_McrPeriodT.FiscalAnnual:
                case E_McrPeriodT.FiscalYearToDate:
                default:
                    throw new UnhandledEnumException(periodT);
            }
        }
        private static E_McrPeriodT ConvertQuarterToPeriod(E_McrQuarterT quarter)
        {
            switch (quarter)
            {
                case E_McrQuarterT.Q1:
                    return E_McrPeriodT.Q1;
                case E_McrQuarterT.Q2:
                    return E_McrPeriodT.Q2;
                case E_McrQuarterT.Q3:
                    return E_McrPeriodT.Q3;
                case E_McrQuarterT.Q4:
                    return E_McrPeriodT.Q4;
                default:
                    throw new UnhandledEnumException(quarter);
            }
        }

        // OPM 90357 - Initally want all of the values to be 0, so the user does not have 
        // to manually enter 0s on the site.        
        private static void InitializeSection1Items(Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]> section1ItemDict, E_McrVersionT version)
        {
            foreach (E_McrReportSection1ItemT item in Enum.GetValues(typeof(E_McrReportSection1ItemT)).Cast<E_McrReportSection1ItemT>())
                    {
                        switch (item)
                        {
                            case E_McrReportSection1ItemT.AC010_ApplicationsInProcessAtBeginingPeriod:
                            case E_McrReportSection1ItemT.AC020_ApplicationsReceived:
                            case E_McrReportSection1ItemT.AC030_ApplicationsApprovedButNotAccepted:
                            case E_McrReportSection1ItemT.AC040_ApplicationDenied:
                            case E_McrReportSection1ItemT.AC050_ApplicationWithdrawn:
                            case E_McrReportSection1ItemT.AC060_FileClosedForIncompleteness:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                break;
                            case E_McrReportSection1ItemT.AC065_NetChangesInApplicationAmount:
                                // AC065 is only available in version 4 and does not keep a 
                                // count.
                                if (version >= E_McrVersionT.Version4)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, null);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, null);
                                }
                                break;
                            case E_McrReportSection1ItemT.AC920_QualifiedMortgage:
                            case E_McrReportSection1ItemT.AC930_NonQualifiedMortgage:
                            case E_McrReportSection1ItemT.AC940_NotSubjectToQm:
                                if (version >= E_McrVersionT.Version4)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                }
                                break;
                            case E_McrReportSection1ItemT.AC062_PreApprovalRequestsDenied:
                            case E_McrReportSection1ItemT.AC064_PreApprovalRequestsApprovedButNotAccepted:
                                // These fields are absent in version 1
                                if (version != E_McrVersionT.Version1)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                }
                                break;
                            case E_McrReportSection1ItemT.AC070_LoansOriginated:
                            case E_McrReportSection1ItemT.AC080_ApplicationsInProcessAtEndOfPeriod:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                break;
                            case E_McrReportSection1ItemT.AC100_LoanType_Conventional:
                            case E_McrReportSection1ItemT.AC110_LoanType_FHA:
                            case E_McrReportSection1ItemT.AC120_LoanType_VA:
                            case E_McrReportSection1ItemT.AC130_LoanType_RHS:
                            case E_McrReportSection1ItemT.AC200_PropertyType_OneToFour:
                            case E_McrReportSection1ItemT.AC210_PropertyType_Manufactured:
                            case E_McrReportSection1ItemT.AC220_PropertyType_Multifamily:
                            case E_McrReportSection1ItemT.AC300_LoanPurpose_Purchase:
                            case E_McrReportSection1ItemT.AC310_LoanPurpose_HomeImprovement:
                            case E_McrReportSection1ItemT.AC320_LoanPurpose_Refinancing:
                            case E_McrReportSection1ItemT.AC400_HOEPA:
                            case E_McrReportSection1ItemT.AC500_Lien_First:
                            case E_McrReportSection1ItemT.AC510_Lien_Second:
                            case E_McrReportSection1ItemT.AC520_Lien_NotSecured:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                break;
                            case E_McrReportSection1ItemT.AC600_BrokerFees:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, null);
                                break;
                            case E_McrReportSection1ItemT.AC610_LenderFees:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, null);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, null);
                                break;
                            case E_McrReportSection1ItemT.AC700_ReverseMortgages_Standard:
                            case E_McrReportSection1ItemT.AC710_ReverseMortgages_Saver:
                            case E_McrReportSection1ItemT.AC720_ReverseMortgages_Proprietary:
                            case E_McrReportSection1ItemT.AC800_ReverseMortages_HomePurchase:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                break;
                            case E_McrReportSection1ItemT.AC810_Other:
                                // AC810 not in version 1
                                if (version != E_McrVersionT.Version1)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, 0);
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, 0);
                                }
                                break;
                            case E_McrReportSection1ItemT.AC620_BrokerFees_ReverseMortgages:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, null);
                                break;
                            case E_McrReportSection1ItemT.AC630_LenderFees_ReverseMortgages:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, 0, null);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, 0, null);
                                break;
                            case E_McrReportSection1ItemT.AC900_TotalLoansBrokered:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, null, 0);
                                break;
                            case E_McrReportSection1ItemT.AC910_TotalLoansFunded:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Retail, null, 0);
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Wholesale, null, 0);
                                break;
                            case E_McrReportSection1ItemT.AC1000_LoansMadeAndAssigned:
                                UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                break;
                            case E_McrReportSection1ItemT.AC1100_GrossRevenueFromOperations:
                                // AC1100 not in version 1
                                if (version != E_McrVersionT.Version1)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, null);
                                }
                                break;
                            case E_McrReportSection1ItemT.AC1200_ClosedLoansWithServicingRetainedDuringQuarter:
                            case E_McrReportSection1ItemT.AC1210_ClosedLoansWithServicingReleasedDuringQuarter:
                                if (version >= E_McrVersionT.Version4)
                                {
                                    UpdateItem(section1ItemDict, item, E_BranchChannelT.Broker, 0, 0);
                                }
                                break;
                            default:
                                throw new UnhandledEnumException(item);
                        }
                    }
        }
        private static void InitializeSection2Items(Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml> section2ItemDict, E_McrVersionT version, E_McrFormatT reportType)
        {
            if (version == E_McrVersionT.Version1 || reportType != E_McrFormatT.Expanded)
            {
                return;
            }
            foreach (E_McrReportSection2ItemT item in Enum.GetValues(typeof(E_McrReportSection2ItemT)).Cast<E_McrReportSection2ItemT>())
            {
                switch (item)
                {
                    case E_McrReportSection2ItemT.I100_TotalResidential:
                    case E_McrReportSection2ItemT.I180_TotalOther:
                    case E_McrReportSection2ItemT.I200_TotalMortgagesOriginated:
                    case E_McrReportSection2ItemT.I240_TotalResidential:
                    case E_McrReportSection2ItemT.I259_TotalResidential:
                    case E_McrReportSection2ItemT.I269_TotalResidential:
                    case E_McrReportSection2ItemT.I279_TotalResidential:
                    case E_McrReportSection2ItemT.I299_TotalResidential:
                    case E_McrReportSection2ItemT.I309_TotalResidential:
                    case E_McrReportSection2ItemT.I319_TotalResidential:
                    case E_McrReportSection2ItemT.I329_TotalResidential:
                    case E_McrReportSection2ItemT.I339_TotalResidential:
                    case E_McrReportSection2ItemT.I349_TotalResidential:
                    case E_McrReportSection2ItemT.I359_TotalResidential:
                    case E_McrReportSection2ItemT.I379_TotalResidential:
                    case E_McrReportSection2ItemT.I409_TotalResidential:
                        // Do nothing, these won't be included for now.
                        break;
                    case E_McrReportSection2ItemT.I010_GovernmentFixed:
                    case E_McrReportSection2ItemT.I020_GovernmentArm:
                    case E_McrReportSection2ItemT.I030_PrimeConfFixed:
                    case E_McrReportSection2ItemT.I040_PrimeConfArm:
                    case E_McrReportSection2ItemT.I050_PrimeJumboFixed:
                    case E_McrReportSection2ItemT.I060_PrimeJumboArm:
                    case E_McrReportSection2ItemT.I070_OtherFixed:
                    case E_McrReportSection2ItemT.I080_OtherArm:
                    case E_McrReportSection2ItemT.I110_ClosedEndSecond:
                    case E_McrReportSection2ItemT.I140_Construction:
                    case E_McrReportSection2ItemT.I250_FixedRate:
                    case E_McrReportSection2ItemT.I251_ARM:
                    case E_McrReportSection2ItemT.I260_Jumbo:
                    case E_McrReportSection2ItemT.I261_NonJumbo:
                    case E_McrReportSection2ItemT.I270_AltDoc:
                    case E_McrReportSection2ItemT.I271_FullDoc:
                    case E_McrReportSection2ItemT.I280_Interest_Only:
                    case E_McrReportSection2ItemT.I281_Not_Interest_Only:
                    case E_McrReportSection2ItemT.I290_OptionArm:
                    case E_McrReportSection2ItemT.I291_NotOptionArm:
                    case E_McrReportSection2ItemT.I300_PrepaymentPenalties:
                    case E_McrReportSection2ItemT.I301_NoPrepaymentPenalties:
                    case E_McrReportSection2ItemT.I310_Purchase:
                    case E_McrReportSection2ItemT.I311_RefiRateTerm:
                    case E_McrReportSection2ItemT.I312_RefiCashout:
                    case E_McrReportSection2ItemT.I313_RefiRestructure:
                    case E_McrReportSection2ItemT.I314_RefiOther:
                    case E_McrReportSection2ItemT.I320_OwnerOccupied:
                    case E_McrReportSection2ItemT.I321_NonOwnerOccupied:
                    case E_McrReportSection2ItemT.I330_Pmi:
                    case E_McrReportSection2ItemT.I331_NoPmi:
                    case E_McrReportSection2ItemT.I340_LoanWithPiggyback:
                    case E_McrReportSection2ItemT.I341_LoanWithoutPiggyback:
                    case E_McrReportSection2ItemT.I350_FicoLess600:
                    case E_McrReportSection2ItemT.I351_Fico600_650:
                    case E_McrReportSection2ItemT.I352_Fico650_700:
                    case E_McrReportSection2ItemT.I353_Fico700_750:
                    case E_McrReportSection2ItemT.I354_Fico750:
                    case E_McrReportSection2ItemT.I370_Ltv60:
                    case E_McrReportSection2ItemT.I371_Ltv60_70:
                    case E_McrReportSection2ItemT.I372_Ltv70_80:
                    case E_McrReportSection2ItemT.I373_Ltv80_90:
                    case E_McrReportSection2ItemT.I374_Ltv90_100:
                    case E_McrReportSection2ItemT.I375_Ltv100:
                    case E_McrReportSection2ItemT.I400_SoldToSecondaryMarket:
                    case E_McrReportSection2ItemT.I401_SoldToOtherNonAffliate:
                    case E_McrReportSection2ItemT.I402_SoldToOtherAffliate:
                    case E_McrReportSection2ItemT.I403_KeptInPortfolio:
                    case E_McrReportSection2ItemT.I404_SoldWithSaleTreatment:
                    case E_McrReportSection2ItemT.I405_SoldWithoutSaleTreatment:
                    case E_McrReportSection2ItemT.I410_SoldServicingReleased:
                    case E_McrReportSection2ItemT.I420_BrokeredOut:
                    case E_McrReportSection2ItemT.I460_WarehousedExcess90Days:
                        UpdateItem(section2ItemDict, item, 0, 0, null);
                        break;
                    case E_McrReportSection2ItemT.I210_Retail:
                        UpdateItem(section2ItemDict, item, 0, 0, null);
                        break;
                    case E_McrReportSection2ItemT.I220_Correspondent:
                        UpdateItem(section2ItemDict, item, 0, 0, null);
                        break;
                    case E_McrReportSection2ItemT.I230_Broker:
                        UpdateItem(section2ItemDict, item, 0, 0, null);
                        break;
                    case E_McrReportSection2ItemT.I120_FundedHelocs:
                    case E_McrReportSection2ItemT.I130_ReverseMortgages:
                    case E_McrReportSection2ItemT.I150_Multifamily:
                    case E_McrReportSection2ItemT.I160_Commercial:
                    case E_McrReportSection2ItemT.I170_OtherMortgage:
                    case E_McrReportSection2ItemT.I440_TotalMultiFamilySold:
                    case E_McrReportSection2ItemT.I455_AvgDaysMultifamilyMortgages:
                    case E_McrReportSection2ItemT.I456_AvgDaysCommercial:
                        // N/A
                        break;
                    case E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage:
                    case E_McrReportSection2ItemT.I365_AvgFicoForSecondMortgage:
                    case E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage:
                    case E_McrReportSection2ItemT.I385_AvgCltv:
                    case E_McrReportSection2ItemT.I390_AvgCoupon:
                    case E_McrReportSection2ItemT.I430_FalloutRatio:
                    case E_McrReportSection2ItemT.I450_AvgDaysSfrMortgages:
                        // don't include ratios if the ratio is divide by zero.
                        UpdateItem(section2ItemDict, item, null, 0, null);
                        break;
                    default:
                        throw new UnhandledEnumException(item);
                }
            }
        }

        private static void InitializeSection4Items(Dictionary<E_McrReportSection4ItemT, McrReportSection4ItemXml> section4ItemDict, E_McrVersionT version, E_McrFormatT reportType)
        {
            if (version < E_McrVersionT.Version4)
            {
                return;
            }
            foreach (E_McrReportSection4ItemT item in Enum.GetValues(typeof(E_McrReportSection4ItemT)).Cast<E_McrReportSection4ItemT>())
            {
                UpdateItem(section4ItemDict, item, 0, 0);
            }
        }

        public static void GenerateQuarterDates(E_McrPeriodT periodT, int year, out DateTime startDate, out DateTime endDate)
        {
            switch (periodT)
            {
                case E_McrPeriodT.Q1:
                case E_McrPeriodT.FiscalQ1:
                    startDate = new DateTime(year, 1, 1);
                    endDate = new DateTime(year, 3, 31);
                    break;
                case E_McrPeriodT.Q2:
                case E_McrPeriodT.FiscalQ2:
                    startDate = new DateTime(year, 4, 1);
                    endDate = new DateTime(year, 6, 30);

                    break;
                case E_McrPeriodT.Q3:
                case E_McrPeriodT.FiscalQ3:
                    startDate = new DateTime(year, 7, 1);
                    endDate = new DateTime(year, 9, 30);

                    break;
                case E_McrPeriodT.Q4:
                    startDate = new DateTime(year, 10, 1);
                    endDate = new DateTime(year, 12, 31);

                    break;
                case E_McrPeriodT.Undefined:
                case E_McrPeriodT.FiscalAnnual:
                case E_McrPeriodT.FiscalYearToDate:
                default:
                    throw new UnhandledEnumException(periodT);
            }
        }

        public static List<McrRmlaXml> Generate(Report customReport, E_McrPeriodT periodT, int year, E_McrVersionT version, E_McrFormatT reportType, Guid brokerID)
        {

            if (null == customReport)
            {
                return new List<McrRmlaXml>();
            }

            #region Setup start date and end date period.
            DateTime startDate;
            DateTime endDate;

            // 7/22/2011 dd - Quarter Period is define on http://mortgage.nationwidelicensingsystem.org/slr/common/mcr/Pages/default.aspx
            switch (periodT)
            {
                case E_McrPeriodT.Q1:
                case E_McrPeriodT.FiscalQ1:
                    startDate = new DateTime(year, 1, 1);
                    endDate = new DateTime(year, 3, 31);
                    break;
                case E_McrPeriodT.Q2:
                case E_McrPeriodT.FiscalQ2:
                    startDate = new DateTime(year, 4, 1);
                    endDate = new DateTime(year, 6, 30);

                    break;
                case E_McrPeriodT.Q3:
                case E_McrPeriodT.FiscalQ3:
                    startDate = new DateTime(year, 7, 1);
                    endDate = new DateTime(year, 9, 30);

                    break;
                case E_McrPeriodT.Q4:
                    startDate = new DateTime(year, 10, 1);
                    endDate = new DateTime(year, 12, 31);

                    break;
                case E_McrPeriodT.Undefined:
                case E_McrPeriodT.FiscalAnnual:
                case E_McrPeriodT.FiscalYearToDate:
                default:
                    throw new UnhandledEnumException(periodT);
            }
            #endregion
            var quarter = ConvertPeriodToQuarter(periodT);

            ReportHelper reportHelper = new ReportHelper(customReport, version, reportType);
            List<Value[]> results = new List<Value[]>();
            foreach (Rows rows in customReport.Tables)
            {
                GatherRows(rows, results);
            }

            #region Section 1 Logic
            // 7/28/2011 dd - This dictionary will contains 2 pieces of information per state (key)
            //    1) a list of Section I item section per section 1 item.
            //    2) a list of loan originator item per loan originator id.
            var rmlaSection1DictionaryByState = new Dictionary<string,
                                                               Tuple<Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]>,
                                                                      Dictionary<string, McrSectionIMlosItemXml>>>();

            #region Revenue by state code from DB
            var revenueByStateCode = new Dictionary<string, string>();
            if (E_McrVersionT.Version1 != version)
            {
                var broker = LendersOffice.Admin.BrokerDB.RetrieveById(brokerID);
                var revenueByStateByQuarterByYear = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<int, Dictionary<E_McrQuarterT, Dictionary<string, string>>>>(broker.NmlsCallReportOriginationRelatedRevenueJSONContent);
                if (null != revenueByStateByQuarterByYear)
                {
                    if (revenueByStateByQuarterByYear.ContainsKey(year))
                    {
                        if (revenueByStateByQuarterByYear[year].ContainsKey(quarter))
                        {
                            revenueByStateCode = revenueByStateByQuarterByYear[year][quarter];
                        }
                    }
                }
            }
            #endregion

            #region Section I logic per loan in result set
            foreach (Value[] row in results)
            {
                string sSpState = reportHelper.GetString("sSpState", row);
                E_BranchChannelT? sBranchChannelT = reportHelper.GetEnum<E_BranchChannelT>("sBranchChannelT", row);

                DateTime? sOpenedD = reportHelper.GetDate("sOpenedD", row);
                DateTime? applicationDate;

                if (version.Equals(E_McrVersionT.Version5))
                {
                    DateTime? sNmlsApplicationDate = reportHelper.GetDate("sNmlsApplicationDate", row);
                    if (sNmlsApplicationDate.HasValue)
                    {
                        applicationDate = sNmlsApplicationDate;
                    }
                    else
                    {
                        // In Version 5, only include loans that have a valid sNmlsApplicationDate.
                        continue;
                    }
                }
                else
                {
                    applicationDate = sOpenedD;
                }


                if (ValidStateCodes.Contains(sSpState) == false)
                {
                    // Skip unknown state.
                    continue;
                }

                //   skip empty branch channels per opm 69332 av 8/1/11
                if (sBranchChannelT.HasValue == false || sBranchChannelT.Value == E_BranchChannelT.Blank || sBranchChannelT.Value == E_BranchChannelT.Correspondent)
                {
                    continue;
                }

                #region Main Calculation

                Tuple<Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]>,
                                      Dictionary<string, McrSectionIMlosItemXml>> tuple = null;

                if (rmlaSection1DictionaryByState.TryGetValue(sSpState, out tuple) == false)
                {

                    tuple = Tuple.Create(new Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]>(), new Dictionary<string, McrSectionIMlosItemXml>());
                    rmlaSection1DictionaryByState.Add(sSpState, tuple);

                    // OPM 90357 -- Want blank values to be entered in report as 0s.
                    InitializeSection1Items(tuple.Item1, version);

                    if (E_McrVersionT.Version1 != version)
                    {
                        if (revenueByStateCode.ContainsKey(sSpState))
                        {
                            if (0 != reportHelper.GetDecimal(revenueByStateCode[sSpState]))
                            {
                                UpdateItem(rmlaSection1DictionaryByState[sSpState].Item1, E_McrReportSection1ItemT.AC1100_GrossRevenueFromOperations, E_BranchChannelT.Broker,
                                    reportHelper.GetDecimal(revenueByStateCode[sSpState]), null);
                            }
                        }
                    }
                }
                var dictionary = tuple.Item1;
                decimal sFinalLAmt = reportHelper.GetDecimal("sFinalLAmt", row);

                var sNMLSApplicationAmount = reportHelper.GetDecimal("sNMLSApplicationAmount", row);

                if (sNMLSApplicationAmount == 0)
                {
                    sNMLSApplicationAmount = sFinalLAmt;
                }

                DateTime? sHmdaActionD = reportHelper.GetDate("sHmdaActionD", row);
                bool isActionDWithinPeriod = false;
                var mlo_dictionary = tuple.Item2;
                E_sLT? sLT = reportHelper.GetEnum<E_sLT>("sLT", row);;
                E_sHmdaPropT? sHmdaPropT = reportHelper.GetEnum<E_sHmdaPropT>("sHmdaPropT", row);
                E_sLPurposeT? sLPurposeT = reportHelper.GetEnum<E_sLPurposeT>("sLPurposeT", row);
                bool sHmdaReportAsHomeImprov = reportHelper.GetBool("sHmdaReportAsHomeImprov", row);
                bool sHmdaReportAsHoepaLoan = reportHelper.GetBool("sHmdaReportAsHoepaLoan", row);
                E_sLienPosT? sLienPosT = reportHelper.GetEnum<E_sLienPosT>("sLienPosT", row);
                E_sHmdaLienT? sHmdaLienT = reportHelper.GetEnum<E_sHmdaLienT>("sHmdaLienT", row);
                decimal sBrokerFeesCollected = reportHelper.GetDecimal("sBrokerFeesCollected", row);
                decimal sLenderFeesCollected = reportHelper.GetDecimal("sLenderFeesCollected", row);
                string sApp1003InterviewerLoanOriginatorIdentifier = reportHelper.GetString("sApp1003InterviewerLoanOriginatorIdentifier", row);

                string sHmdaActionTaken = reportHelper.GetString("sHmdaActionTaken", row);
                if (sHmdaActionD.HasValue)
                {
                    isActionDWithinPeriod = sHmdaActionD.Value >= startDate && sHmdaActionD.Value <= endDate;
                }
                if (applicationDate.HasValue && applicationDate.Value < startDate &&
                    ((sHmdaActionD.HasValue && sHmdaActionD.Value >= startDate) || sHmdaActionD.HasValue == false))
                {
                    // 7/22/2011 dd - AC010 - Applications in process at the beginning of the period.
                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC010_ApplicationsInProcessAtBeginingPeriod, sBranchChannelT, sNMLSApplicationAmount, 1);
                }
                if (applicationDate.HasValue && applicationDate.Value >= startDate && applicationDate.Value <= endDate)
                {
                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC020_ApplicationsReceived, sBranchChannelT, sNMLSApplicationAmount, 1);
                }
                if (applicationDate.HasValue && applicationDate.Value <= endDate &&
                    ((sHmdaActionD.HasValue && sHmdaActionD.Value > endDate) || sHmdaActionD.HasValue == false))
                {
                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC080_ApplicationsInProcessAtEndOfPeriod, sBranchChannelT, sNMLSApplicationAmount, 1);
                }
                if (isActionDWithinPeriod)
                {
                    if (sHmdaActionTaken.Equals("App approved but not accepted by applicant", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationApprovedButNotAccepted), StringComparison.OrdinalIgnoreCase))
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC030_ApplicationsApprovedButNotAccepted, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }

                    else if (sHmdaActionTaken.Equals("Application denied", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationDenied), StringComparison.OrdinalIgnoreCase))
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC040_ApplicationDenied, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }
                    else if (sHmdaActionTaken.Equals("Application withdrawn", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationWithdrawnByApplicant), StringComparison.OrdinalIgnoreCase))
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC050_ApplicationWithdrawn, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }
                    else if (sHmdaActionTaken.Equals("File closed for incompleteness", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.FileClosedForIncompleteness), StringComparison.OrdinalIgnoreCase))
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC060_FileClosedForIncompleteness, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }
                    else if (sHmdaActionTaken.Equals("Loan originated", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.LoanOriginated), StringComparison.OrdinalIgnoreCase))
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC070_LoansOriginated, sBranchChannelT, sFinalLAmt, 1);

                        if (version >= E_McrVersionT.Version4)
                        {

                            E_sQMStatusT? sQMStatusT = reportHelper.GetEnum<E_sQMStatusT>("sQMStatusT", row);
                            E_ServicingStatus? sNMLSServicingIntentT = reportHelper.GetEnum<E_ServicingStatus>("sNMLSServicingIntentT", row);

                            if (sQMStatusT.HasValue)
                            {
                                // sIsExemptFromAtr is only available in Version 5.  Retrieving the value for other versions will throw an error.
                                bool sIsExemptFromAtr = version == E_McrVersionT.Version5 ? reportHelper.GetBool("sIsExemptFromAtr", row) : false;

                                if (sIsExemptFromAtr)
                                {
                                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC940_NotSubjectToQm, sBranchChannelT, sFinalLAmt, 1);
                                }
                                else if (sQMStatusT == E_sQMStatusT.Eligible)
                                {
                                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC920_QualifiedMortgage, sBranchChannelT, sFinalLAmt, 1);
                                }
                                else
                                {
                                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC930_NonQualifiedMortgage, sBranchChannelT, sFinalLAmt, 1);
                                }
                            }

                            if (sNMLSServicingIntentT.HasValue)
                            {
                                if (sNMLSServicingIntentT == E_ServicingStatus.Retained)
                                {
                                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC1200_ClosedLoansWithServicingRetainedDuringQuarter, sBranchChannelT, sFinalLAmt, 1);
                                }
                                else if (sNMLSServicingIntentT == E_ServicingStatus.Released)
                                {
                                    UpdateItem(dictionary, E_McrReportSection1ItemT.AC1210_ClosedLoansWithServicingReleasedDuringQuarter, sBranchChannelT, sFinalLAmt, 1);
                                }
                            }

                            decimal sNMLSNetChangeApplicationAmount = reportHelper.GetDecimal("sNMLSNetChangeApplicationAmount", row);

                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC065_NetChangesInApplicationAmount, sBranchChannelT, sNMLSNetChangeApplicationAmount, null);
                        }

                        // Closed Loan Data
                        if (sLT.HasValue)
                        {
                            if (sLT.Value == E_sLT.Conventional)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC100_LoanType_Conventional, sBranchChannelT, sFinalLAmt, 1);
                            }
                            else if (sLT.Value == E_sLT.FHA)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC110_LoanType_FHA, sBranchChannelT, sFinalLAmt, 1);
                            }
                            else if (sLT.Value == E_sLT.VA)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC120_LoanType_VA, sBranchChannelT, sFinalLAmt, 1);

                            }
                            else if (sLT.Value == E_sLT.UsdaRural)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC130_LoanType_RHS, sBranchChannelT, sFinalLAmt, 1);
                            }
                        }

                        if (sHmdaPropT.HasValue)
                        {
                            if (sHmdaPropT.Value == E_sHmdaPropT.OneTo4Family)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC200_PropertyType_OneToFour, sBranchChannelT, sFinalLAmt, 1);

                            }
                            else if (sHmdaPropT.Value == E_sHmdaPropT.ManufacturedHousing)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC210_PropertyType_Manufactured, sBranchChannelT, sFinalLAmt, 1);

                            }
                            else if (sHmdaPropT.Value == E_sHmdaPropT.MultiFamiliy)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC220_PropertyType_Multifamily, sBranchChannelT, sFinalLAmt, 1);

                            }

                        }

                        // Purpose of loan
                        if (sLPurposeT.HasValue)
                        {
                            if (sLPurposeT.Value == E_sLPurposeT.Purchase || sLPurposeT.Value == E_sLPurposeT.Construct ||
                                sLPurposeT.Value == E_sLPurposeT.ConstructPerm)
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC300_LoanPurpose_Purchase, sBranchChannelT, sFinalLAmt, 1);

                            }
                            if (sHmdaReportAsHomeImprov && (sLPurposeT.Value == E_sLPurposeT.Refin || sLPurposeT.Value == E_sLPurposeT.RefinCashout
                                || sLPurposeT.Value == E_sLPurposeT.HomeEquity
                                || sLPurposeT.Value == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT.Value == E_sLPurposeT.VaIrrrl))
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC310_LoanPurpose_HomeImprovement, sBranchChannelT, sFinalLAmt, 1);

                            }
                            if (sHmdaReportAsHomeImprov == false && (sLPurposeT.Value == E_sLPurposeT.Refin || sLPurposeT.Value == E_sLPurposeT.RefinCashout
                                || sLPurposeT.Value == E_sLPurposeT.HomeEquity
    || sLPurposeT.Value == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT.Value == E_sLPurposeT.VaIrrrl))
                            {
                                UpdateItem(dictionary, E_McrReportSection1ItemT.AC320_LoanPurpose_Refinancing, sBranchChannelT, sFinalLAmt, 1);

                            }
                        }

                        if (sHmdaReportAsHoepaLoan)
                        {
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC400_HOEPA, sBranchChannelT, sFinalLAmt, 1);

                        }

                        // Lien Status
                        if ((sHmdaLienT.HasValue && sHmdaLienT.Value == E_sHmdaLienT.FirstLien) ||
                            ((sHmdaLienT.HasValue == false || sHmdaLienT.Value == E_sHmdaLienT.LeaveBlank) && sLienPosT.HasValue && sLienPosT.Value == E_sLienPosT.First))
                        {

                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC500_Lien_First, sBranchChannelT, sFinalLAmt, 1);
                        }
                        if ((sHmdaLienT.HasValue && sHmdaLienT.Value == E_sHmdaLienT.SubordinateLien) ||
                            ((sHmdaLienT.HasValue == false || sHmdaLienT.Value == E_sHmdaLienT.LeaveBlank) && sLienPosT.HasValue && sLienPosT.Value == E_sLienPosT.Second))
                        {
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC510_Lien_Second, sBranchChannelT, sFinalLAmt, 1);

                        }
                        if (sHmdaLienT.HasValue && sHmdaLienT.Value == E_sHmdaLienT.NotSecuredByLien)
                        {
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC520_Lien_NotSecured, sBranchChannelT, sFinalLAmt, 1);

                        }

                        // 8/15/2012 SK opm 90420 - only update these if action taken is loan originated.
                        if (sBranchChannelT.HasValue == false ||
                        (sBranchChannelT.Value == E_BranchChannelT.Blank || sBranchChannelT.Value == E_BranchChannelT.Broker ||
                        sBranchChannelT.Value == E_BranchChannelT.Correspondent))
                        {
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC600_BrokerFees, sBranchChannelT, sBrokerFeesCollected, null);
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC900_TotalLoansBrokered, sBranchChannelT, null, 1);
                        }
                        else if (sBranchChannelT.HasValue == true && (sBranchChannelT.Value == E_BranchChannelT.Retail || sBranchChannelT.Value == E_BranchChannelT.Wholesale))
                        {
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC610_LenderFees, sBranchChannelT, sLenderFeesCollected, null);
                            UpdateItem(dictionary, E_McrReportSection1ItemT.AC910_TotalLoansFunded, sBranchChannelT, null, 1);
                        }

                        if (sBranchChannelT.HasValue == true && (sBranchChannelT.Value == E_BranchChannelT.Broker || sBranchChannelT.Value == E_BranchChannelT.Retail))
                        {
                            if (string.IsNullOrEmpty(sApp1003InterviewerLoanOriginatorIdentifier) == false)
                            {
                                McrSectionIMlosItemXml item;
                                if (mlo_dictionary.TryGetValue(sApp1003InterviewerLoanOriginatorIdentifier, out item) == false)
                                {
                                    item = new McrSectionIMlosItemXml();
                                    item.NmlsId = sApp1003InterviewerLoanOriginatorIdentifier;
                                    mlo_dictionary.Add(sApp1003InterviewerLoanOriginatorIdentifier, item);
                                }
                                item.Amount += sFinalLAmt;
                                item.Count++;
                            }
                        }
                    }// End: action taken = loan originated
                    else if ((sHmdaActionTaken.Equals("Preapproval request denied", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestDenied), StringComparison.OrdinalIgnoreCase))
                        && version != E_McrVersionT.Version1)
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC062_PreApprovalRequestsDenied, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }
                    else if ((sHmdaActionTaken.Equals("Preapproval request approved but not accepted", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted), StringComparison.OrdinalIgnoreCase))
                        && version != E_McrVersionT.Version1)
                    {
                        UpdateItem(dictionary, E_McrReportSection1ItemT.AC064_PreApprovalRequestsApprovedButNotAccepted, sBranchChannelT, sNMLSApplicationAmount, 1);
                    }
                } // if (isActionDWithinPeriod)

                #endregion
            }
            #endregion
            #endregion

            #region Section II Logic

            var rmlaSection2DictionaryByState = new Dictionary<string,
                                                                Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml>>();
            
            if (version != E_McrVersionT.Version1 && reportType == E_McrFormatT.Expanded)
            {
                #region Setup some additional info to aggregate for Section II
                var extraAggregateInfo = new Dictionary<string, Dictionary<string, decimal>>(); // use this to track additional amounts that won't go into the xml directly.
                var extraAggregates = new string[]{
                    "I360_SumOfQualifyingScore","I360_ClosedLoanCount",
                    "I361_SumOfQualifyingScore", "I361_ClosedLoanCount",
                    "I380_Numerator", "I380_SumOfClosedLoanUPB",
                    "I385_Numerator", "I385_SumOfClosedLoanUPB",
                    "I390_Numerator",   // 390 denominator is same as 385 denominator.
                    "I450_SumDaysInWarehouse", "I450_LoanCount"
                };
                foreach (var extraAggregate in extraAggregates)
                {
                    extraAggregateInfo.Add(extraAggregate, new Dictionary<string, decimal>());
                }
                #endregion

                #region Section II Per Loan Logic
                foreach (var row in results)
                {
                    string sSpState = reportHelper.GetString("sSpState", row);
                    E_BranchChannelT? sBranchChannelT = reportHelper.GetEnum<E_BranchChannelT>("sBranchChannelT", row);


                    if (ValidStateCodes.Contains(sSpState) == false)
                    {
                        // Skip unknown state.
                        continue;
                    }

                    //   skip empty branch channels per opm 69332 av 8/1/11
                    if (sBranchChannelT.HasValue == false || sBranchChannelT.Value == E_BranchChannelT.Blank)
                    {
                        continue;
                    }

                    #region Main Calculation

                    Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml> dictionary = null;

                    if (rmlaSection2DictionaryByState.TryGetValue(sSpState, out dictionary) == false)
                    {

                        dictionary = new Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml>();
                        rmlaSection2DictionaryByState.Add(sSpState, dictionary);

                        // OPM 90357 -- Want blank values to be entered in report as 0s.
                        InitializeSection2Items(dictionary, version, reportType);
                    }
                   
                    decimal sFinalLAmt = reportHelper.GetDecimal("sFinalLAmt", row);
                    DateTime? sHmdaActionD = reportHelper.GetDate("sHmdaActionD", row);
                    bool isActionDWithinPeriod = false;

                    string sHmdaActionTaken = reportHelper.GetString("sHmdaActionTaken", row);
                    if (sHmdaActionD.HasValue)
                    {
                        isActionDWithinPeriod = sHmdaActionD.Value >= startDate && sHmdaActionD.Value <= endDate;
                    }
                    if (isActionDWithinPeriod && (sHmdaActionTaken.Equals("Loan originated", StringComparison.OrdinalIgnoreCase)
                        || sHmdaActionTaken.Equals(DataAccess.Tools.MapHmdaActionTakenToString(HmdaActionTaken.LoanOriginated), StringComparison.OrdinalIgnoreCase)))
                    {
                        E_MortgageLoanT? sMortgageLoanT = reportHelper.GetEnum<E_MortgageLoanT>("sMortgageLoanT", row);
                        E_sFinMethT? sFinMethT = reportHelper.GetEnum<E_sFinMethT>("sFinMethT", row);
                        E_JumboT? sJumboT = reportHelper.GetEnum<E_JumboT>("sJumboT", row);
                        E_DocumentationT? sDocumentationT = reportHelper.GetEnum<E_DocumentationT>("sDocumentationT", row);
                        int sIOnlyMon = reportHelper.GetInt("sIOnlyMon", row);
                        bool sIsOptionArm = reportHelper.GetBool("sIsOptionArm", row);
                        bool sHasPrepaymentPenalty = reportHelper.GetBool("sHasPrepaymentPenalty", row);
                        E_NMLSLoanPurposeT? sNMLSLoanPurposeT = reportHelper.GetEnum<E_NMLSLoanPurposeT>("sNMLSLoanPurposeT", row);
                        E_sOccT? sOccT = reportHelper.GetEnum<E_sOccT>("sOccT", row);
                        bool sHasPrivateMortgageInsurance = reportHelper.GetBool("sHasPrivateMortgageInsurance", row);
                        bool sHasPiggybackFinancing = reportHelper.GetBool("sHasPiggybackFinancing", row);
                        int sCreditScoreLpeQual = reportHelper.GetInt("sCreditScoreLpeQual", row);
                        decimal sNMLSLtvR = reportHelper.GetDecimal("sNMLSLtvR", row);
                        decimal sNMLSCLtvR = reportHelper.GetDecimal("sNMLSCLtvR", row);
                        E_LoanSaleDispositionT? sLoanSaleDispositionT = reportHelper.GetEnum<E_LoanSaleDispositionT>("sLoanSaleDispositionT", row);
                        var sPurchaseAdviceSummaryServicingStatus = reportHelper.GetString("sPurchaseAdviceSummaryServicingStatus", row);
                        bool isProductionServicingReleased = string.IsNullOrEmpty(sPurchaseAdviceSummaryServicingStatus) || sPurchaseAdviceSummaryServicingStatus == ((int)E_ServicingStatus.Released).ToString();
                        int sDaysInWarehouse = reportHelper.GetInt("sDaysInWarehouse", row);

                        #region I010-I170
                        if (sFinMethT.HasValue && E_sFinMethT.Fixed == sFinMethT.Value)
                        {
                            if (sMortgageLoanT.HasValue)
                            {
                                switch (sMortgageLoanT.Value)
                                {
                                    case E_MortgageLoanT.Government_FHA_VA_RHS:
                                        UpdateItem(dictionary, E_McrReportSection2ItemT.I010_GovernmentFixed, sFinalLAmt, 1, sMortgageLoanT);
                                        break;
                                    case E_MortgageLoanT.PrimeConforming:
                                        UpdateItem(dictionary, E_McrReportSection2ItemT.I030_PrimeConfFixed, sFinalLAmt, 1, sMortgageLoanT);
                                        break;
                                    case E_MortgageLoanT.PrimeNonConforming:
                                        UpdateItem(dictionary, E_McrReportSection2ItemT.I050_PrimeJumboFixed, sFinalLAmt, 1, sMortgageLoanT);
                                        break;
                                    case E_MortgageLoanT.Other1To4UnitResidential:
                                        UpdateItem(dictionary, E_McrReportSection2ItemT.I070_OtherFixed, sFinalLAmt, 1, sMortgageLoanT);
                                        break;
                                }
                            }
                        }
                        else if (sFinMethT.HasValue && E_sFinMethT.ARM == sFinMethT.Value)
                        {
                            switch (sMortgageLoanT.Value)
                            {
                                case E_MortgageLoanT.Government_FHA_VA_RHS:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I020_GovernmentArm, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_MortgageLoanT.PrimeConforming:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I040_PrimeConfArm, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_MortgageLoanT.PrimeNonConforming:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I060_PrimeJumboArm, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_MortgageLoanT.Other1To4UnitResidential:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I080_OtherArm, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }
                        if (sMortgageLoanT.HasValue)
                        {
                            switch (sMortgageLoanT.Value)
                            {
                                case E_MortgageLoanT.ClosedEndSecond:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I110_ClosedEndSecond, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_MortgageLoanT.FundedHELOC:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I120_FundedHelocs, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_MortgageLoanT.Construction:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I140_Construction, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }
                        #endregion

                        #region I200s
                        if (sBranchChannelT.HasValue)
                        {
                            switch (sBranchChannelT.Value)
                            {
                                case E_BranchChannelT.Retail:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I210_Retail, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_BranchChannelT.Correspondent:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I220_Correspondent, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_BranchChannelT.Wholesale:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I230_Broker, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sFinMethT.HasValue)
                        {
                            switch (sFinMethT.Value)
                            {
                                case E_sFinMethT.Fixed:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I250_FixedRate, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_sFinMethT.ARM:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I251_ARM, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sJumboT.HasValue)
                        {
                            switch (sJumboT.Value)
                            {
                                case E_JumboT.Jumbo:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I260_Jumbo, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_JumboT.NonJumbo:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I261_NonJumbo, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sDocumentationT.HasValue)
                        {
                            switch (sDocumentationT.Value)
                            {
                                case E_DocumentationT.AltDoc:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I270_AltDoc, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_DocumentationT.FullDoc:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I271_FullDoc, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sIOnlyMon > 0)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I280_Interest_Only, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sIOnlyMon == 0)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I281_Not_Interest_Only, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else
                        {
                            // for now, doing nothing, but a separate program should enforce that this never be negative.
                        }
                        if (sIsOptionArm)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I290_OptionArm, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I291_NotOptionArm, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        #endregion

                        #region 300s
                        if (sHasPrepaymentPenalty)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I300_PrepaymentPenalties, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I301_NoPrepaymentPenalties, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        if (sNMLSLoanPurposeT.HasValue)
                        {
                            switch (sNMLSLoanPurposeT.Value)
                            {
                                case E_NMLSLoanPurposeT.Purchase:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I310_Purchase, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_NMLSLoanPurposeT.RefinanceRateTerm:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I311_RefiRateTerm, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_NMLSLoanPurposeT.RefinanceCashOutRefinance:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I312_RefiCashout, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_NMLSLoanPurposeT.RefinanceRestructure:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I313_RefiRestructure, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_NMLSLoanPurposeT.RefinanceOtherUnknown:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I314_RefiOther, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sOccT.HasValue)
                        {
                            switch (sOccT.Value)
                            {
                                case E_sOccT.PrimaryResidence:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I320_OwnerOccupied, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_sOccT.SecondaryResidence:
                                case E_sOccT.Investment:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I321_NonOwnerOccupied, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        if (sHasPrivateMortgageInsurance)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I330_Pmi, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I331_NoPmi, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        if (sHasPiggybackFinancing)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I340_LoanWithPiggyback, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I341_LoanWithoutPiggyback, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        if (sCreditScoreLpeQual <= 600)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I350_FicoLess600, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sCreditScoreLpeQual <= 650)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I351_Fico600_650, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sCreditScoreLpeQual <= 700)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I352_Fico650_700, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sCreditScoreLpeQual <= 750)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I353_Fico700_750, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else // > 750
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I354_Fico750, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        // 360s
                        if (sMortgageLoanT.HasValue)
                        {
                            if (sMortgageLoanT.Value != E_MortgageLoanT.ClosedEndSecond && sMortgageLoanT.Value != E_MortgageLoanT.FundedHELOC)
                            {
                                UpdateExtraInfo(extraAggregateInfo, "I360_SumOfQualifyingScore", sSpState, sCreditScoreLpeQual, sMortgageLoanT, E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage);
                                UpdateExtraInfo(extraAggregateInfo, "I360_ClosedLoanCount", sSpState, 1, sMortgageLoanT, E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage);
                                UpdateItem(dictionary, E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage, null, 1, sMortgageLoanT);
                            }
                            else
                            {
                                UpdateExtraInfo(extraAggregateInfo, "I361_SumOfQualifyingScore", sSpState, sCreditScoreLpeQual);
                                UpdateExtraInfo(extraAggregateInfo, "I361_ClosedLoanCount", sSpState, 1);
                                UpdateItem(dictionary, E_McrReportSection2ItemT.I365_AvgFicoForSecondMortgage, null, 1, sMortgageLoanT);
                            }
                        }

                        // 370s
                        if (sNMLSLtvR <= 60)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I370_Ltv60, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sNMLSLtvR <= 70)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I371_Ltv60_70, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sNMLSLtvR <= 80)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I372_Ltv70_80, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sNMLSLtvR <= 90)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I373_Ltv80_90, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else if (sNMLSLtvR <= 100)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I374_Ltv90_100, sFinalLAmt, 1, sMortgageLoanT);
                        }
                        else // if >100
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I375_Ltv100, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        // 380s.
                        UpdateExtraInfo(extraAggregateInfo, "I385_Numerator", sSpState, sNMLSCLtvR * sFinalLAmt);
                        UpdateExtraInfo(extraAggregateInfo, "I385_SumOfClosedLoanUPB", sSpState, sFinalLAmt);
                        UpdateItem(dictionary, E_McrReportSection2ItemT.I385_AvgCltv, null, 1, sMortgageLoanT);

                        E_sLienPosT? sLienPosT = reportHelper.GetEnum<E_sLienPosT>("sLienPosT", row);

                        // OPM 237867, 2/10/2016, ML
                        if (sLienPosT.HasValue && sLienPosT.Value == E_sLienPosT.First)
                        {
                            UpdateExtraInfo(extraAggregateInfo, "I380_Numerator", sSpState, sNMLSLtvR * sFinalLAmt, sMortgageLoanT, E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage);
                            UpdateExtraInfo(extraAggregateInfo, "I380_SumOfClosedLoanUPB", sSpState, sFinalLAmt, sMortgageLoanT, E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage);
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage, null, 1, sMortgageLoanT);

                            var sNoteIR = reportHelper.GetDecimal("sNoteIR", row);

                            UpdateExtraInfo(extraAggregateInfo, "I390_Numerator", sSpState, sNoteIR * sFinalLAmt, sMortgageLoanT, E_McrReportSection2ItemT.I390_AvgCoupon);
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I390_AvgCoupon, null, 1, sMortgageLoanT);
                        }
                        #endregion

                        #region 400s
                        if (sLoanSaleDispositionT.HasValue)
                        {
                            switch (sLoanSaleDispositionT.Value)
                            {
                                case E_LoanSaleDispositionT.SoldToSecondaryMarketAgencies_FNMA_FHLMC_GNMA:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I400_SoldToSecondaryMarket, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_LoanSaleDispositionT.SoldToOther_NonAffiliate:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I401_SoldToOtherNonAffliate, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_LoanSaleDispositionT.SoldToOther_Affiliate:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I402_SoldToOtherAffliate, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_LoanSaleDispositionT.KeptInPortfolio_HeldForInvestment:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I403_KeptInPortfolio, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithSaleTreatment:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I404_SoldWithSaleTreatment, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                                case E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithOutSaleTreatment:
                                    UpdateItem(dictionary, E_McrReportSection2ItemT.I405_SoldWithoutSaleTreatment, sFinalLAmt, 1, sMortgageLoanT);
                                    break;
                            }
                        }

                        // 410
                        if (isProductionServicingReleased)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I410_SoldServicingReleased, sFinalLAmt, 1, sMortgageLoanT);
                        }

                        if (sBranchChannelT.HasValue && sBranchChannelT.Value == E_BranchChannelT.Broker)
                        {
                            UpdateItem(dictionary, E_McrReportSection2ItemT.I420_BrokeredOut, sFinalLAmt, 1, sMortgageLoanT);
                        }



                        // 430 is handled elsewhere.
                        // 450
                        UpdateExtraInfo(extraAggregateInfo, "I450_SumDaysInWarehouse", sSpState, sDaysInWarehouse);
                        UpdateExtraInfo(extraAggregateInfo, "I450_LoanCount", sSpState, 1);
                        UpdateItem(dictionary, E_McrReportSection2ItemT.I450_AvgDaysSfrMortgages, null, 1, sMortgageLoanT);
                        #endregion
                    }

                    #endregion
                }
                #endregion

                UpdateColumnValuesWithExtraInfo(extraAggregateInfo, rmlaSection2DictionaryByState, rmlaSection1DictionaryByState);                
            }
            #endregion

            #region Section IV Logic
            var rmlaSection4DictionaryByState = new Dictionary<string,
                                                                Dictionary<E_McrReportSection4ItemT, McrReportSection4ItemXml>>();

            if (version >= E_McrVersionT.Version4)
            {

                foreach (var row in results)
                {
                    string sSpState = reportHelper.GetString("sSpState", row);
                    E_BranchChannelT? sBranchChannelT = reportHelper.GetEnum<E_BranchChannelT>("sBranchChannelT", row);


                    if (ValidStateCodes.Contains(sSpState) == false)
                    {
                        // Skip unknown state.
                        continue;
                    }

                    //   skip empty branch channels per opm 69332 av 8/1/11
                    if (sBranchChannelT.HasValue == false || sBranchChannelT.Value == E_BranchChannelT.Blank)
                    {
                        continue;
                    }

                    #region Main Calculation

                    Dictionary<E_McrReportSection4ItemT, McrReportSection4ItemXml> dictionary = null;

                    if (rmlaSection4DictionaryByState.TryGetValue(sSpState, out dictionary) == false)
                    {

                        dictionary = new Dictionary<E_McrReportSection4ItemT, McrReportSection4ItemXml>();
                        rmlaSection4DictionaryByState.Add(sSpState, dictionary);

                        // OPM 90357 -- Want blank values to be entered in report as 0s.
                        InitializeSection4Items(dictionary, version, reportType);
                    }
                    #endregion

                    //Load the necessary data
                    E_LoanSaleDispositionT? loanSaleDisposition = reportHelper.GetEnum<E_LoanSaleDispositionT>("sLoanSaleDispositionT", row);
                    DateTime? sServicingByUsEndD = reportHelper.GetDate("sServicingByUsEndD", row);
                    DateTime? sServicingByUsStartD = reportHelper.GetDate("sServicingByUsStartD", row);
                    DateTime? sNMLSServicingTransferInD = reportHelper.GetDate("sNMLSServicingTransferInD", row);
                    DateTime? sNMLSServicingTransferOutD = reportHelper.GetDate("sNMLSServicingTransferOutD", row);
                    DateTime? sGLServTransEffD = reportHelper.GetDate("sGLServTransEffD", row);
                    int sNMLSDaysDelinquentAsOfPeriodEnd = reportHelper.GetInt("sNMLSDaysDelinquentAsOfPeriodEnd", row);
                    E_McrQuarterT? sNMLSPeriodForDaysDelinquent = reportHelper.GetEnum<E_McrQuarterT>("sNMLSPeriodForDaysDelinquentT", row);
                    decimal sFinalLAmt = reportHelper.GetDecimal("sFinalLAmt", row);

                    DateTime quarterEndDate = new DateTime();
                    DateTime quarterStartDate = new DateTime();
                    GenerateQuarterDates(periodT, year, out quarterStartDate, out quarterEndDate);

                    if (sServicingByUsStartD.HasValue && 
                        (( sServicingByUsStartD.Value.CompareTo(quarterStartDate) <= 0 && (!sServicingByUsEndD.HasValue || sServicingByUsEndD.Value.CompareTo(quarterStartDate) >= 0))
                            ||
                        (sServicingByUsStartD.Value.CompareTo(quarterStartDate) >= 0 && sServicingByUsStartD.Value.CompareTo(quarterEndDate) <= 0)
                        ))
                    {

                        if (loanSaleDisposition.HasValue)
                        {
                            if (loanSaleDisposition.Value.Equals(E_LoanSaleDispositionT.KeptInPortfolio_HeldForInvestment))
                            {
                                if (sServicingByUsEndD.HasValue)
                                {
                                    if (sServicingByUsEndD.Value.CompareTo(quarterEndDate) >= 1)
                                    {
                                        UpdateItem(dictionary, E_McrReportSection4ItemT.LS010_LoansLenderIsServicingRetainsAllPropertyRights, sFinalLAmt, 1);

                                    }
                                }
                                else
                                {
                                    UpdateItem(dictionary, E_McrReportSection4ItemT.LS010_LoansLenderIsServicingRetainsAllPropertyRights, sFinalLAmt, 1);
                                }

                                if (sGLServTransEffD.HasValue && sGLServTransEffD.Value.CompareTo(quarterEndDate) <= 0)
                                {
                                    UpdateItem(dictionary, E_McrReportSection4ItemT.LS040_LoansLenderOwnsServicingRightsWithThirdPartyServicing, sFinalLAmt, 1);
                                }
                            }
                            else if (loanSaleDisposition.Value != E_LoanSaleDispositionT.Blank && loanSaleDisposition.Value != E_LoanSaleDispositionT.KeptInPortfolio_HeldForInvestment
                                || (!sServicingByUsEndD.HasValue))
                            {
                                UpdateItem(dictionary, E_McrReportSection4ItemT.LS020_LoansLenderIsServicingRetainsOnlyServicingRights, sFinalLAmt, 1);
                            }
                        }

                        if (sNMLSServicingTransferInD.HasValue && sNMLSServicingTransferInD.Value.CompareTo(quarterStartDate) >= 0 && sNMLSServicingTransferInD.Value.CompareTo(quarterEndDate) <= 0)
                        {
                            UpdateItem(dictionary, E_McrReportSection4ItemT.LS100_LoansServicingTransferredInDuringPeriod, sFinalLAmt, 1);
                        }

                        if (sNMLSServicingTransferOutD.HasValue && sNMLSServicingTransferOutD.Value.CompareTo(quarterStartDate) >= 0 && sNMLSServicingTransferOutD.Value.CompareTo(quarterEndDate) <= 0)
                        {
                            UpdateItem(dictionary, E_McrReportSection4ItemT.LS110_LoansServicingTransferredOutDuringPeriod, sFinalLAmt, 1);
                        }

                        if ((sNMLSPeriodForDaysDelinquent.HasValue && sNMLSPeriodForDaysDelinquent.Value == quarter))
                        {
                            if (sNMLSDaysDelinquentAsOfPeriodEnd < 30)
                            {
                                UpdateItem(dictionary, E_McrReportSection4ItemT.LS200_LoansLessThan30DaysDelinquent, sFinalLAmt, 1);
                            }
                            else if (sNMLSDaysDelinquentAsOfPeriodEnd >= 30 && sNMLSDaysDelinquentAsOfPeriodEnd <= 60)
                            {
                                UpdateItem(dictionary, E_McrReportSection4ItemT.LS210_Loans30To60DaysDelinquent, sFinalLAmt, 1);
                            }
                            else if (sNMLSDaysDelinquentAsOfPeriodEnd > 60 && sNMLSDaysDelinquentAsOfPeriodEnd <= 90)
                            {
                                UpdateItem(dictionary, E_McrReportSection4ItemT.LS220_Loans61To90DaysDelinquent, sFinalLAmt, 1);
                            }
                            else if (sNMLSDaysDelinquentAsOfPeriodEnd > 90)
                            {
                                UpdateItem(dictionary, E_McrReportSection4ItemT.LS230_91OrMoreDaysDelinquent, sFinalLAmt, 1);
                            }
                        }
                    }
                }
            }
            #endregion

            #region  Loop through the states from both sections and generate a list of McrRmlaXml.
            List<McrRmlaXml> list = new List<McrRmlaXml>();
            var section1States = rmlaSection1DictionaryByState.Keys;
            var section2States = rmlaSection2DictionaryByState.Keys;
            var statesForBothSections = section1States.Union(section2States);
            foreach (var state in statesForBothSections)
            {
                McrRmlaXml rmlaXml = new McrRmlaXml();
                rmlaXml.StateCode = state;

                if (rmlaSection1DictionaryByState.ContainsKey(state))
                {
                    var section1ItemList = rmlaSection1DictionaryByState[state].Item1;
                    foreach (var values in section1ItemList.Values)
                    {
                        foreach (var item in values)
                        {
                            if (item != null)
                            {
                                rmlaXml.Add(item);
                            }
                        }
                    }
                    var mloItemList = rmlaSection1DictionaryByState[state].Item2;
                    foreach (var value in mloItemList.Values)
                    {
                        rmlaXml.Add(value);
                    }
                }
                if (reportType == E_McrFormatT.Expanded && version != E_McrVersionT.Version1 
                    && rmlaSection2DictionaryByState.ContainsKey(state))
                {
                    var section2ItemList = rmlaSection2DictionaryByState[state];
                    foreach (var value in section2ItemList.Values)
                    {
                        rmlaXml.Add(value);
                    }
                }
                if (version >= E_McrVersionT.Version4
                    && rmlaSection4DictionaryByState.ContainsKey(state))
                {
                    var section4ItemList = rmlaSection4DictionaryByState[state];
                    foreach (var value in section4ItemList.Values)
                    {
                        rmlaXml.Add(value);
                    }
                }
                list.Add(rmlaXml);
            }
            #endregion
            return list;
        }

        private static void UpdateColumnValuesWithExtraInfo(Dictionary<string, Dictionary<string, decimal>> extraInfo,
            Dictionary<string, Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml>> rmlaSection2DictionaryByState,
            Dictionary<string, 
                       Tuple<Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]>,
                              Dictionary<string, McrSectionIMlosItemXml>>> rmlaSection1DictionaryByState
            )
        {
            // I360 Average FICO Score for First Mortgage Borrowers
            var dict = extraInfo["I360_SumOfQualifyingScore"];
            foreach (var state in dict.Keys)
            {
                var denom = extraInfo["I360_ClosedLoanCount"][state];
                if(denom != 0)
                {
                    var valToPopulate = Math.Round(dict[state] / denom, 2, MidpointRounding.AwayFromZero);
                    UpdateItem(rmlaSection2DictionaryByState[state], E_McrReportSection2ItemT.I360_AvgFicoForFirstMortgage, valToPopulate, 0, null);
                }
            }

            // I361 Average FICO Score for Second and HELOC Mortgage Borrowers
            dict = extraInfo["I361_SumOfQualifyingScore"];
            foreach (var key in dict.Keys)
            {
                var denom = extraInfo["I361_ClosedLoanCount"][key];
                if (denom != 0)
                {
                    var valToPopulate = Math.Round(dict[key] / denom, 2, MidpointRounding.AwayFromZero);
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I365_AvgFicoForSecondMortgage, valToPopulate, 0, null);
                }
            }

            // I380 Weighted Average LTV on First Mortgages Only
            dict = extraInfo["I380_Numerator"];
            foreach (var key in dict.Keys)
            {
                 var denom = extraInfo["I380_SumOfClosedLoanUPB"][key];
                 if (denom != 0)
                 {
                     var valToPopulate = Math.Round(dict[key] / denom, 2, MidpointRounding.AwayFromZero);
                     UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage, valToPopulate, 0, null);
                 }
            }

            // I385 Weighted Average CLTV 
            dict = extraInfo["I385_Numerator"];
            foreach (var key in dict.Keys)
            {
                var denom = extraInfo["I385_SumOfClosedLoanUPB"][key];
                if (denom != 0)
                {
                    var valToPopulate = Math.Round(dict[key] / denom, 2, MidpointRounding.AwayFromZero);
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I385_AvgCltv, valToPopulate, 0, null);
                }
            }
            // I390 Weighted Average Coupon
            dict = extraInfo["I390_Numerator"];
            foreach (var key in dict.Keys)
            {
                var denom = extraInfo["I385_SumOfClosedLoanUPB"][key]; // not a typo, 385 and 390 denominators are the same.
                if (denom != 0)
                {
                    var valToPopulate = Math.Round(dict[key] / denom, 2, MidpointRounding.AwayFromZero); 
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I390_AvgCoupon, valToPopulate, 0, null);
                }
            }

            // I430 Pull-Through Ratio
            foreach (var key in rmlaSection2DictionaryByState.Keys)
            {
                // sum(70) over all channels / sum(30,40,50,60) round that up to 2 decimal places.
                // excludes correpondent channel.
                if(!rmlaSection1DictionaryByState.ContainsKey(key))
                {
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I430_FalloutRatio, null, 0, null);
                    continue;
                }
                var item = rmlaSection1DictionaryByState[key].Item1;
                var valsToPopulate70 = item[E_McrReportSection1ItemT.AC070_LoansOriginated];
                var valsToPopulate30 = item[E_McrReportSection1ItemT.AC030_ApplicationsApprovedButNotAccepted];
                var valsToPopulate40 = item[E_McrReportSection1ItemT.AC040_ApplicationDenied];
                var valsToPopulate50 = item[E_McrReportSection1ItemT.AC050_ApplicationWithdrawn];
                var valsToPopulate60 = item[E_McrReportSection1ItemT.AC060_FileClosedForIncompleteness];
                decimal denom = 0;
                decimal numerator = 0;
                for (int i = 0; i < valsToPopulate70.Length; i++)
                {
                    if (valsToPopulate70[i] == null)
                    {
                        continue;
                    }
                    numerator += (decimal)(valsToPopulate70[i].Count.HasValue ? valsToPopulate70[i].Count.Value : 0);
                    var v30 = (decimal)(valsToPopulate30[i].Count.HasValue ? valsToPopulate30[i].Count.Value: 0);
                    var v40 = (decimal)(valsToPopulate40[i].Count.HasValue ? valsToPopulate40[i].Count.Value: 0);
                    var v50 = (decimal)(valsToPopulate50[i].Count.HasValue ? valsToPopulate50[i].Count.Value: 0);
                    var v60 = (decimal)(valsToPopulate60[i].Count.HasValue ? valsToPopulate60[i].Count.Value: 0);
                    denom += v30 + v40 + v50 + v60 + (decimal)(valsToPopulate70[i].Count.HasValue ? valsToPopulate70[i].Count.Value : 0);
                }

                if (denom != 0)
                {
                    var valToPopulate = Math.Round(numerator / denom * 100, 2, MidpointRounding.AwayFromZero);
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I430_FalloutRatio,
                        valToPopulate, (int)denom, null);
                }
                else
                {
                    // omit from xml.
                }
            }

            // I450 Average Days In Warehouse 1-4 Unit Residtenial Loans Only
            dict = extraInfo["I450_SumDaysInWarehouse"];
            foreach (var key in dict.Keys)
            {
                var denom = extraInfo["I450_LoanCount"][key];
                if (denom != 0)
                {
                    var valToPopulate = Math.Round(dict[key] / denom, 2, MidpointRounding.AwayFromZero);
                    UpdateItem(rmlaSection2DictionaryByState[key], E_McrReportSection2ItemT.I450_AvgDaysSfrMortgages, valToPopulate, 0, null);
                }
            }
        }
        // OPM 89068
        ///////////////////////       
        public static Dictionary<int, Dictionary<E_McrQuarterT, HashSet<String>>> GetRevenueByStateByYearAndQuarter(string id, AbstractUserPrincipal principal)
        {

            Report rA = null;
            string result;
            var statesDictionary = new Dictionary<int, Dictionary<E_McrQuarterT, HashSet<String>>>();
            if ((result = AutoExpiredTextCache.GetFromCacheByUser(principal, id)) != null)
                rA = (Report)result;

            // 7/21/2011 dd - The year must be between the current year minus 2 and current year plus 1 (From NMLS 
            // Call Report validation rule. Year must be greater than equal 2011.
            for (int year = DateTime.Now.Year - 2; year <= DateTime.Now.Year + 1; year++)
            {
                if (year >= 2011)
                {
                    Dictionary<E_McrQuarterT, HashSet<String>> quarterStates = new Dictionary<E_McrQuarterT, HashSet<String>>();
                    foreach (E_McrQuarterT quarter in Enum.GetValues(typeof(E_McrQuarterT)))
                    {
                        var states = new HashSet<string>();
                        var xmls = Generate(rA, ConvertQuarterToPeriod(quarter), year, E_McrVersionT.Version1, E_McrFormatT.Undefined, Guid.Empty);
                        foreach (var statexml in xmls)
                        {
                            states.Add(statexml.StateCode);
                        }
                        quarterStates.Add(quarter, states);
                    }
                    statesDictionary.Add(year, quarterStates);                    
                }
            }
            return statesDictionary;
        }


        //////////////////////////

        private static void UpdateItem(Dictionary<E_McrReportSection4ItemT, McrReportSection4ItemXml> dictionary,
            E_McrReportSection4ItemT itemT, decimal? amount, int? count)
        {
            McrReportSection4ItemXml itemXml = null;
            if (dictionary.TryGetValue(itemT, out itemXml) == false)
            {
                itemXml = new McrReportSection4ItemXml();
                dictionary.Add(itemT, itemXml);
            }
            itemXml.ItemT = itemT;
            if (count.HasValue)
            {
                if (itemXml.Count.HasValue == false)
                {
                    itemXml.Count = count.Value;
                }
                else
                {
                    itemXml.Count = itemXml.Count.Value + count.Value;
                }
            }
            if (amount.HasValue)
            {
                if (itemXml.Amount.HasValue == false)
                {
                    itemXml.Amount = amount.Value;
                }
                else
                {
                    itemXml.Amount = itemXml.Amount.Value + amount.Value;
                }
            }
        }

        ///
        private static void UpdateItem(Dictionary<E_McrReportSection2ItemT, McrReportSection2ItemXml> dictionary,
            E_McrReportSection2ItemT itemT, decimal? amount, int? count, E_MortgageLoanT? mortgageType )
        {

            if(mortgageType.HasValue && !McrReportSection2Item.IsValidMortgageTForItem(itemT, mortgageType.Value))
            {
                return;
            }

            McrReportSection2ItemXml itemXml = null;
            if (dictionary.TryGetValue(itemT, out itemXml) == false)
            {
                itemXml = new McrReportSection2ItemXml();
                dictionary.Add(itemT, itemXml);
            }
            itemXml.ItemT = itemT;
            if (count.HasValue)
            {
                if (itemXml.Count.HasValue == false)
                {
                    itemXml.Count = count.Value;
                }
                else
                {
                    itemXml.Count = itemXml.Count.Value + count.Value;
                }
            }
            if (amount.HasValue)
            {
                if (itemXml.Amount.HasValue == false)
                {
                    itemXml.Amount = amount.Value;
                }
                else
                {
                    itemXml.Amount = itemXml.Amount.Value + amount.Value;
                }
            }
        }
        private static void UpdateItem(Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]> dictionary,
            E_McrReportSection1ItemT itemT, E_BranchChannelT? sBranchChannelT, decimal? amount, int? count)
        {
            E_McrReportSectionIColumnT column = GetColumnFor(itemT, sBranchChannelT);
            UpdateItem(dictionary, itemT, column, amount, count);
        }

        private static void UpdateItem(Dictionary<E_McrReportSection1ItemT, McrReportSection1ItemXml[]> dictionary,
            E_McrReportSection1ItemT itemT, E_McrReportSectionIColumnT column, decimal? amount, int? count)
        {
            McrReportSection1ItemXml[] items = null;
            if (dictionary.TryGetValue(itemT, out items) == false)
            {
                items = new McrReportSection1ItemXml[3];
                dictionary.Add(itemT, items);
            }


            McrReportSection1ItemXml itemXml = items[(int)column];
            if (itemXml == null)
            {
                itemXml = new McrReportSection1ItemXml();
                itemXml.ItemT = itemT;
                itemXml.ColumnT = column;
                items[(int)column] = itemXml;
            }
            if (count.HasValue)
            {
                if (itemXml.Count.HasValue == false)
                {
                    itemXml.Count = count.Value;
                }
                else
                {
                    itemXml.Count = itemXml.Count.Value + count.Value;
                }
            }
            if (amount.HasValue)
            {
                if (itemXml.Amount.HasValue == false)
                {
                    itemXml.Amount = amount.Value;
                }
                else
                {
                    itemXml.Amount = itemXml.Amount.Value + amount.Value;
                }

            }
        }

        private static void UpdateExtraInfo(Dictionary<string, Dictionary<string, decimal>> dict, string amountType, string state, decimal value, E_MortgageLoanT? sMortgageLoanT = null, E_McrReportSection2ItemT? itemT = null)
        {
            if (!dict.ContainsKey(amountType))
            {
                throw new KeyNotFoundException("missing " + amountType);
            }

            if(sMortgageLoanT.HasValue && itemT.HasValue && !McrReportSection2Item.IsValidMortgageTForItem(itemT.Value, sMortgageLoanT.Value))
            {
                return;
            }

            var dictionary = dict[amountType];
            if (dictionary.ContainsKey(state))
            {
                dictionary[state] = dictionary[state] + value;
            }
            else
            {
                dictionary.Add(state, value);
            }
        }
        
        private static void GatherRows(Rows rows, List<Value[]> results)
        {
            foreach (var o in rows)
            {
                if (o == null)
                {
                    continue;
                }
                if (o is Row)
                {
                    Row ro = o as Row;
                    //DataAccess.Tools.LogBug(ro.Key.ToString());
                    results.Add(ro.Items);
                }
                else if (o is Rows)
                {
                    GatherRows((Rows)o, results);
                }
                else
                {
                    throw DataAccess.CBaseException.GenericException("Unhandle type=" + o.GetType());
                }
            }
        }

        private static E_McrReportSectionIColumnT GetColumnFor(E_McrReportSection1ItemT itemT, E_BranchChannelT? sBranchChannelT)
        {
            switch (itemT)
            {
                case E_McrReportSection1ItemT.AC010_ApplicationsInProcessAtBeginingPeriod:
                case E_McrReportSection1ItemT.AC020_ApplicationsReceived:
                case E_McrReportSection1ItemT.AC030_ApplicationsApprovedButNotAccepted:
                case E_McrReportSection1ItemT.AC040_ApplicationDenied:
                case E_McrReportSection1ItemT.AC050_ApplicationWithdrawn:
                case E_McrReportSection1ItemT.AC060_FileClosedForIncompleteness:
                case E_McrReportSection1ItemT.AC062_PreApprovalRequestsDenied:
                case E_McrReportSection1ItemT.AC064_PreApprovalRequestsApprovedButNotAccepted:
                case E_McrReportSection1ItemT.AC065_NetChangesInApplicationAmount:
                case E_McrReportSection1ItemT.AC070_LoansOriginated:
                case E_McrReportSection1ItemT.AC080_ApplicationsInProcessAtEndOfPeriod:
                    return GetColumnForApplicationData(sBranchChannelT);
                case E_McrReportSection1ItemT.AC100_LoanType_Conventional:
                case E_McrReportSection1ItemT.AC110_LoanType_FHA:
                case E_McrReportSection1ItemT.AC120_LoanType_VA:
                case E_McrReportSection1ItemT.AC130_LoanType_RHS:
                case E_McrReportSection1ItemT.AC200_PropertyType_OneToFour:
                case E_McrReportSection1ItemT.AC210_PropertyType_Manufactured:
                case E_McrReportSection1ItemT.AC220_PropertyType_Multifamily:
                case E_McrReportSection1ItemT.AC300_LoanPurpose_Purchase:
                case E_McrReportSection1ItemT.AC310_LoanPurpose_HomeImprovement:
                case E_McrReportSection1ItemT.AC320_LoanPurpose_Refinancing:
                case E_McrReportSection1ItemT.AC400_HOEPA:
                case E_McrReportSection1ItemT.AC500_Lien_First:
                case E_McrReportSection1ItemT.AC510_Lien_Second:
                case E_McrReportSection1ItemT.AC520_Lien_NotSecured:
                case E_McrReportSection1ItemT.AC600_BrokerFees:
                case E_McrReportSection1ItemT.AC610_LenderFees:
                case E_McrReportSection1ItemT.AC700_ReverseMortgages_Standard:
                case E_McrReportSection1ItemT.AC710_ReverseMortgages_Saver:
                case E_McrReportSection1ItemT.AC720_ReverseMortgages_Proprietary:
                case E_McrReportSection1ItemT.AC800_ReverseMortages_HomePurchase:
                case E_McrReportSection1ItemT.AC810_Other:
                case E_McrReportSection1ItemT.AC620_BrokerFees_ReverseMortgages:
                case E_McrReportSection1ItemT.AC630_LenderFees_ReverseMortgages:
                case E_McrReportSection1ItemT.AC900_TotalLoansBrokered:
                case E_McrReportSection1ItemT.AC910_TotalLoansFunded:
                case E_McrReportSection1ItemT.AC920_QualifiedMortgage:
                case E_McrReportSection1ItemT.AC930_NonQualifiedMortgage:
                case E_McrReportSection1ItemT.AC940_NotSubjectToQm:
                    return GetColumnForClosedData(sBranchChannelT);
                case E_McrReportSection1ItemT.AC1000_LoansMadeAndAssigned:
                    return E_McrReportSectionIColumnT.Brokered_Column1;
                case E_McrReportSection1ItemT.AC1100_GrossRevenueFromOperations:
                    return E_McrReportSectionIColumnT.Brokered_Column1;
                case E_McrReportSection1ItemT.AC1200_ClosedLoansWithServicingRetainedDuringQuarter:
                case E_McrReportSection1ItemT.AC1210_ClosedLoansWithServicingReleasedDuringQuarter:
                    return E_McrReportSectionIColumnT.Brokered_Column1;
                default:
                    throw new UnhandledEnumException(itemT);
            }
        }
        private static E_McrReportSectionIColumnT GetColumnForApplicationData(E_BranchChannelT? sBranchChannelT)
        {
            if (sBranchChannelT.HasValue)
            {
                switch (sBranchChannelT.Value)
                {
                    case E_BranchChannelT.Blank:
                    case E_BranchChannelT.Retail:
                    case E_BranchChannelT.Broker:
                    case E_BranchChannelT.Correspondent:
                        return E_McrReportSectionIColumnT.Brokered_Column1;
                    case E_BranchChannelT.Wholesale:
                        return E_McrReportSectionIColumnT.Retail_Column2;
                    default:
                        throw new UnhandledEnumException(sBranchChannelT.Value);
                }
            }

            return E_McrReportSectionIColumnT.Brokered_Column1;
        }
        private static E_McrReportSectionIColumnT GetColumnForClosedData(E_BranchChannelT? sBranchChannelT)
        {
            if (sBranchChannelT.HasValue)
            {
                switch (sBranchChannelT.Value)
                {
                    case E_BranchChannelT.Blank:
                    case E_BranchChannelT.Broker:
                    case E_BranchChannelT.Correspondent:
                        return E_McrReportSectionIColumnT.Brokered_Column1;
                    case E_BranchChannelT.Retail:
                        return E_McrReportSectionIColumnT.Retail_Column2;
                    case E_BranchChannelT.Wholesale:
                        return E_McrReportSectionIColumnT.Wholesale_Column3;
                    default:
                        throw new UnhandledEnumException(sBranchChannelT.Value);
                }
            }
            return E_McrReportSectionIColumnT.Brokered_Column1;
        }
    }

    class ReportHelper
    {
        private readonly LosConvert convert = new LosConvert();

        private Dictionary<string, Tuple<string, int, Field>> m_dictionary = null;

        public ReportHelper(Report report, E_McrVersionT version, E_McrFormatT reportType)
        {
            m_dictionary = new Dictionary<string, Tuple<string, int, Field>>(StringComparer.OrdinalIgnoreCase);

            foreach (var name in ConstAppDavid.NmlsCallReportRequiredColumns)
            {
                int index = report.Columns.FindIndex(name);
                Field field = LoanReporting.Schema.LookupById(name);
                m_dictionary.Add(name, Tuple.Create(name, index, field));
            }
            if (version != E_McrVersionT.Version1 && reportType == E_McrFormatT.Expanded)
            {
                var missingFields = new List<string>();
                foreach (var name in ConstAppDavid.NMLSCallReportExpandedColumns)
                {
                    int index = report.Columns.FindIndex(name);
                    if (index == -1)
                    {
                        missingFields.Add(name);
                        continue;
                    }
                    Field field = LoanReporting.Schema.LookupById(name);
                    m_dictionary.Add(name, Tuple.Create(name, index, field));
                }
                if (missingFields.Count > 0)
                {
                    string message = "expanded report requires these additional fields: " + string.Join(", ", missingFields.ToArray());
                    throw new DataAccess.CBaseException(message, message);
                }
            }
            if (version >= E_McrVersionT.Version4)
            {
                string versionName = "Version 4";
                var missingFields = new List<string>();
                foreach (var name in ConstAppDavid.NMLSCallReportVersion4StandardColumns)
                {
                    int index = report.Columns.FindIndex(name);
                    if (index == -1)
                    {
                        missingFields.Add(name);
                        continue;
                    }
                    Field field = LoanReporting.Schema.LookupById(name);
                    m_dictionary.Add(name, Tuple.Create(name, index, field));
                }

                if(version == E_McrVersionT.Version5)
                {
                    versionName = "Version 5";
                    foreach (var name in ConstAppDavid.NMLSCallReportVersion5StandardColumns)
                    {
                        int index = report.Columns.FindIndex(name);
                        if (index == -1)
                        {
                            missingFields.Add(name);
                            continue;
                        }
                        Field field = LoanReporting.Schema.LookupById(name);
                        m_dictionary.Add(name, Tuple.Create(name, index, field));
                    }
                }

                if (missingFields.Count > 0)
                {
                    string message = versionName + " requires these additional fields: " + string.Join(", ", missingFields.ToArray());
                    throw new DataAccess.CBaseException(message, message);
                }
            }
        }

        public string GetString(string name, Value[] row)
        {
            return row[m_dictionary[name].Item2].Text;
        }
        public DateTime? GetDate(string name, Value[] row)
        {
            object data = row[m_dictionary[name].Item2].Data;
            if (null == data || data == DBNull.Value)
            {
                return null;
            }
            if (data is DateTime)
            {
                return (DateTime)data;
            }
            DateTime dt;
            if (DateTime.TryParse(data.ToString(), out dt) == false)
            {
                return null;
            }
            return dt;
            
        }
        
        public Guid GetGuid(string name, Value[] row)
        {
            string s = row[m_dictionary[name].Item2].Data.ToString();
            return new Guid(s);
        }
        public int GetInt(string val)
        {
            string s = val.Replace("$", "").Replace(",", "");
            int v = 0;
            Int32.TryParse(s, out v);
            return v;
        }
        public int GetInt(string name, Value[] row)
        {
            string s = row[m_dictionary[name].Item2].Data.ToString().Replace("$", "").Replace(",", "");
            int v = 0;
            Int32.TryParse(s, out v);
            return v;
        }
        public decimal GetDecimal(string val)
        {
            return this.convert.ToMoney(val);
        }
        public decimal GetDecimal(string name, Value[] row)
        {
            return this.convert.ToMoney(row[m_dictionary[name].Item2].Data.ToString());
        }
        public bool GetBool(string name, Value[] row)
        {
            object data = row[m_dictionary[name].Item2].Data;
            if (null == data || data == DBNull.Value)
            {
                return false;
            }
            return data.ToString() == "Yes";
        }
        public T? GetEnum<T>(string name, Value[] row) where T : struct
        {
            try
            {
                string desc = GetString(name, row);
                Field field = m_dictionary[name].Item3;

                string value = field.Mapping.ByVal[desc];

                return GetEnum<T>(value);
            }
            catch
            {
                throw new Exception("Failed to retrieve " + name);
            }

        }

        private T? GetEnum<T>(string v) where T : struct
        {
            if (string.IsNullOrEmpty(v))
            {
                return null;
            }
            try
            {
                return (T)Enum.Parse(typeof(T), v);
            }
            catch (ArgumentException)
            {
                return null;
            } 
            catch (OverflowException) 
            {
                return null;
            }
        }
    }
}
