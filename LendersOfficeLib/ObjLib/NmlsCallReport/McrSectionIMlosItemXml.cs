﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.NmlsCallReport
{
    public class McrSectionIMlosItemXml : AbstractMcrXml
    {
        public ulong? ItemId { get; set; }
        public string NmlsId { get; set; }
        public decimal Amount { get; set; }
        public int Count { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("SectionIMlosItem");

            Write(writer, "ItemId", ItemId);
            Write(writer, "ACMLO", NmlsId);
            WriteDollar(writer, "ACMLO_2", Amount);
            Write(writer, "ACMLO_3", Count);

            writer.WriteEndElement();
        }
    }
}
