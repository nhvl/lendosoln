﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.NmlsCallReport
{
    public enum E_McrReportSection1ItemT
    {
        // 7/20/2011 dd - Do not rearrange the order of enum.
        // Convention is SectionNumber_Name.  It is used by McrReportSection1ItemXml.cs::GetSectionId
        AC010_ApplicationsInProcessAtBeginingPeriod = 0,
        AC020_ApplicationsReceived,
        AC030_ApplicationsApprovedButNotAccepted,
        AC040_ApplicationDenied,
        AC050_ApplicationWithdrawn,
        AC060_FileClosedForIncompleteness,
        AC062_PreApprovalRequestsDenied,
        AC064_PreApprovalRequestsApprovedButNotAccepted,
        AC065_NetChangesInApplicationAmount,
        AC070_LoansOriginated,
        AC080_ApplicationsInProcessAtEndOfPeriod,

        AC100_LoanType_Conventional,
        AC110_LoanType_FHA,
        AC120_LoanType_VA,
        AC130_LoanType_RHS,

        AC200_PropertyType_OneToFour,
        AC210_PropertyType_Manufactured,
        AC220_PropertyType_Multifamily,

        AC300_LoanPurpose_Purchase,
        AC310_LoanPurpose_HomeImprovement,
        AC320_LoanPurpose_Refinancing,

        AC400_HOEPA,

        AC500_Lien_First,
        AC510_Lien_Second,
        AC520_Lien_NotSecured,

        AC600_BrokerFees,
        AC610_LenderFees,

        AC700_ReverseMortgages_Standard,
        AC710_ReverseMortgages_Saver,
        AC720_ReverseMortgages_Proprietary,

        AC800_ReverseMortages_HomePurchase,
        AC810_Other,

        AC620_BrokerFees_ReverseMortgages,
        AC630_LenderFees_ReverseMortgages,

        AC900_TotalLoansBrokered,
        AC910_TotalLoansFunded,
        AC920_QualifiedMortgage,
        AC930_NonQualifiedMortgage,
        AC940_NotSubjectToQm,

        AC1000_LoansMadeAndAssigned,

        AC1100_GrossRevenueFromOperations,

        AC1200_ClosedLoansWithServicingRetainedDuringQuarter,
        AC1210_ClosedLoansWithServicingReleasedDuringQuarter


    }
}