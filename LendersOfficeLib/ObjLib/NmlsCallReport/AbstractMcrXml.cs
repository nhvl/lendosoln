﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace LendersOffice.NmlsCallReport
{
    public abstract class AbstractMcrXml : IXmlSerializable
    {
        protected void WriteRmlag(XmlWriter writer, List<McrRmlaXml> rmlaList)
        {
            //  Add all of the Section4 Items into one ItemXML since it's using the nationwide totals
            SortedList<E_McrReportSection4ItemT, McrReportSection4ItemXml> nationwideRmlagList = new SortedList<E_McrReportSection4ItemT, McrReportSection4ItemXml>();
            foreach (McrRmlaXml rmla in rmlaList)
            {
                foreach (var item in rmla.m_section4ItemList)
                {
                    McrReportSection4ItemXml section4Item;
                    if(!nationwideRmlagList.TryGetValue(item.Key, out section4Item))
                    {
                        section4Item = new McrReportSection4ItemXml();
                        section4Item.Amount = 0;
                        section4Item.Count = 0;
                        nationwideRmlagList.Add(item.Key, section4Item);
                    }

                    section4Item.Amount += item.Value.Amount;
                    section4Item.Count += item.Value.Count;
                    section4Item.ItemT = item.Value.ItemT;

                }
            }


            writer.WriteStartElement("Rmlag");
            writer.WriteStartElement("LoansServicedNationwideTotalsSection");
            //Go through the totals and write their xml
            foreach (McrReportSection4ItemXml item in nationwideRmlagList.Values)
            {
                item.WriteXml(writer);
            }

            writer.WriteEndElement();
            writer.WriteEndElement();

        }
        protected void Write(XmlWriter writer, AbstractMcrXml el)
        {
            if (el == null)
            {
                return;
            }
            el.WriteXml(writer);
        }
        protected void Write(XmlWriter writer, string name, ulong? value)
        {
            if (value.HasValue == false)
            {
                return;
            }
            writer.WriteElementString(name, value.Value.ToString());
        }

        protected void Write(XmlWriter writer, SortedList<int, McrReportSection1ItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }
            writer.WriteStartElement("SectionISection");

            foreach (var o in list.Values)
            {
                o.WriteXml(writer);
            }
            writer.WriteEndElement();
        }
        protected void Write(XmlWriter writer, SortedList<E_McrReportSection2ItemT, McrReportSection2ItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }
            writer.WriteStartElement("SectionIISection");

            foreach (var o in list.Values)
            {
                o.WriteXml(writer);
            }
            writer.WriteEndElement();
        }
        protected void Write(XmlWriter writer, SortedList<E_McrReportSection3ItemT, McrReportSection3ItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }
            writer.WriteStartElement("SectionIIISection");

            foreach (var o in list.Values)
            {
                o.WriteXml(writer);
            }
            writer.WriteEndElement();
        }
        protected void Write(XmlWriter writer, SortedList<E_McrReportSection4ItemT, McrReportSection4ItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }

            foreach (var o in list.Values)
            {
                o.WriteXml(writer);
            }
        }
        protected void Write(XmlWriter writer, List<McrSectionILinesOfCreditItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }
            writer.WriteStartElement("ListSectionOfSectionILinesOfCreditItem");

            writer.WriteStartElement("DetailItemList");
            foreach (var o in list)
            {
                if (null != o)
                {
                    o.WriteXml(writer);
                }
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        protected void Write(XmlWriter writer, List<McrSectionIMlosItemXml> list)
        {
            if (null == list || list.Count == 0)
            {
                return;
            }
            writer.WriteStartElement("ListSectionOfSectionIMlosItem");
            writer.WriteStartElement("DetailItemList");
            foreach (var o in list)
            {
                if (null != o)
                {
                    o.WriteXml(writer);
                }
            }
            writer.WriteEndElement();
            writer.WriteEndElement();

        }

        protected void Write(XmlWriter writer, string name, int? value)
        {
            if (value.HasValue == false)
            {
                return;
            }
            Write(writer, name, value.Value);
        }
        protected void Write(XmlWriter writer, string name, int value)
        {
            writer.WriteElementString(name, value.ToString());
        }

        protected void WriteDollar(XmlWriter writer, string name, decimal? value)
        {
            if (value.HasValue == false)
            {
                return;
            }
            WriteDollar(writer, name, value.Value);
        }
        protected void WriteDollar(XmlWriter writer, string name, decimal value)
        {
            // 7/20/2011 dd - Round decimal to nearest dollar.
            writer.WriteElementString(name, Math.Round(value, 0).ToString());
        }
        protected void WriteHundredth(XmlWriter writer, string name, decimal value)
        {
            writer.WriteElementString(name, Math.Round(value, 2).ToString());
        }
        protected void Write(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }
            writer.WriteElementString(name, value);
        }
        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public abstract void WriteXml(XmlWriter writer);

        #endregion
    }
}
