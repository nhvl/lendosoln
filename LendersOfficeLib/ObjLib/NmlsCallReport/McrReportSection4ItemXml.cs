﻿// <copyright file="McrReportSection4ItemXML.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   3/18/2015
// </summary>

namespace LendersOffice.NmlsCallReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// This class determines the xml for Section 4 of the NMLS call report.
    /// </summary>
    public class McrReportSection4ItemXml : AbstractMcrXml
    {
        /// <summary>
        /// Gets or sets the item type for the section.
        /// </summary>
        /// <value>A Section 4 Item enum.</value>
        public E_McrReportSection4ItemT ItemT { get; set; }

        /// <summary>
        /// Gets or sets the number of loans that meet the conditions for the item.
        /// </summary>
        /// <value>The number of loans that meet the conditions for the item.</value>
        public int? Count { get; set; }

        /// <summary>
        /// Gets or sets the total sum of final loan amount for every loan that meets the item's condition.
        /// </summary>
        /// <value>The total sum of final loan amount for every loan that meets the item's condition.</value>
        public decimal? Amount { get; set; }

        /// <summary>
        /// This method generates the xml for the Section 4 items.  The branch channel does not affect where WriteXML writes the data.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        public override void WriteXml(XmlWriter writer)
        {
            string baseId = this.GetSectionId(this.ItemT);

            string amountId = baseId + "_1";
            string countId = baseId + "_2";

            this.WriteDollar(writer, amountId, this.Amount);
            this.Write(writer, countId, this.Count);
        }

        /// <summary>
        /// This method returns the items Section ID name.
        /// </summary>
        /// <param name="item">The enum whose section ID is needed.</param>
        /// <returns>A string containing the item's ID.</returns>
        private string GetSectionId(E_McrReportSection4ItemT item)
        {
            return item.ToString().Split('_')[0];
        }
    }
}
