﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.NmlsCallReport
{
    public class McrReportSection2ItemXml : AbstractMcrXml
    {
        public E_McrReportSection2ItemT ItemT { get; set; }
        public decimal? AvgValue { get; set; }
        public int? Count { get; set; }
        public decimal? Amount { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            string baseId = GetSectionId(ItemT);

            string[] specialSections = { "I365", "I360", "I380", "I385", "I390" };
            E_McrReportSection2ItemT[] decimalSections =
                {
                E_McrReportSection2ItemT.I380_AvgLtvFirstMortgage,
                E_McrReportSection2ItemT.I385_AvgCltv,
                E_McrReportSection2ItemT.I390_AvgCoupon };
            string[] thirdKeyFields = { "I430", "I450" };

            // 7/21/2014 AV - 187602 Fix Expanded NMLS Call Report XML to conform with the schema 
            // Include the amount section in count element. Do not include the amountId element.
            bool includeAmount = !specialSections.Contains(baseId, StringComparer.OrdinalIgnoreCase);

            string amountId = String.Concat(baseId, "_1");
            string countId = String.Concat(baseId, "_2");

            if (thirdKeyFields.Contains(baseId, StringComparer.OrdinalIgnoreCase))
            {
                includeAmount = false;
                countId = String.Concat(baseId, "_3");  
            }

            if (includeAmount)
            {

                WriteDollar(writer, amountId, Amount);
                if (null != AvgValue && AvgValue.HasValue)
                {
                    WriteHundredth(writer, countId, AvgValue.Value);
                }
                if (null != Count && Count.HasValue)
                {
                    Write(writer, countId, Count.Value);
                }
            }
            else
            {
                if(decimalSections.Contains(this.ItemT) && this.Amount.HasValue)
                {
                    WriteHundredth(writer, countId, this.Amount.Value);
                }
                else if (this.Amount.HasValue)
                {
                    WriteDollar(writer, countId, this.Amount.Value);
                }
            }
        }

        private string GetSectionId(E_McrReportSection2ItemT item)
        {
            return item.ToString().Split('_')[0];
        }
    }
}
