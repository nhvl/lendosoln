﻿// <copyright file="E_McrReportSection4ItemT.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   3/10/2015
// </summary>

namespace LendersOffice.NmlsCallReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This contains an enum for version 4 of the NMLS Call Report.  The new section is servicing.
    /// </summary>
    public enum E_McrReportSection4ItemT
    {
        // 7/20/2011 dd - Do not rearrange the order of enum.
        // Convention is SectionNumber_Name.  It is used by McrReportSection1ItemXml.cs::GetSectionId

        /// <summary>
        /// Loans the lender is servicing and for which the lender retains  all property ownership rights.
        /// </summary>
        LS010_LoansLenderIsServicingRetainsAllPropertyRights,

        /// <summary>
        /// Loans the lender is servicing and for which the lender owns only the servicing rights.
        /// </summary>
        LS020_LoansLenderIsServicingRetainsOnlyServicingRights,

        /// <summary>
        /// Loans that are wholly owned for which the lender owns the servicing rights with a third party to service on the lender's behalf.
        /// </summary>
        LS040_LoansLenderOwnsServicingRightsWithThirdPartyServicing,

        /// <summary>
        /// Loan servicing transferred in during the period.
        /// </summary>
        LS100_LoansServicingTransferredInDuringPeriod,

        /// <summary>
        /// Loan servicing transferred out during the period.
        /// </summary>
        LS110_LoansServicingTransferredOutDuringPeriod,

        /// <summary>
        /// Less than 30 Days Delinquent (includes loans that are not delinquent).
        /// </summary>
        LS200_LoansLessThan30DaysDelinquent,

        /// <summary>
        /// 30 to 60 Days Delinquent.
        /// </summary>
        LS210_Loans30To60DaysDelinquent,

        /// <summary>
        /// 61 to 90 Days Delinquent .
        /// </summary>
        LS220_Loans61To90DaysDelinquent,

        /// <summary>
        /// 91 or more Days Delinquent.
        /// </summary>
        LS230_91OrMoreDaysDelinquent
    }
}
