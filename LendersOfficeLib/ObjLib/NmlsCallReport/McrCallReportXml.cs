﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.NmlsCallReport
{
    public class McrCallReportXml : AbstractMcrXml
    {
        public E_McrFormatT McrFormatT { get; set; }
        public int Year { get; set; }
        public E_McrPeriodT McrPeriodT { get; set; }
        public E_McrVersionT McrVersionT { get; set; }
        public DateTime ReportingDate { get; set; }

        // 7/20/2011 dd - Will not support Fc element right now.

        private List<McrRmlaXml> m_rmlaList = null;

        public List<McrRmlaXml> RmlaList
        {
            get
            {
                if (m_rmlaList == null)
                {
                    m_rmlaList = new List<McrRmlaXml>();
                }
                return m_rmlaList;
            }
        }

        public void Add(McrRmlaXml rmla)
        {
            if (rmla == null)
            {
                return;
            }
            RmlaList.Add(rmla);
        }


        private string ConvertEnum(E_McrFormatT format)
        {
            switch (format)
            {
                case E_McrFormatT.Expanded:
                    return "E";
                case E_McrFormatT.Standard:
                    return "S";
                default:
                    throw new UnhandledEnumException(format);
            }
        }
        private string ConvertEnum(E_McrVersionT version)
        {
            switch (version)
            {
                case E_McrVersionT.Version1:
                    return "v1";
                case E_McrVersionT.Version2:
                    return "v2";
                case E_McrVersionT.Version3:
                    return "v3";
                case E_McrVersionT.Version4:
                    return "v4";
                case E_McrVersionT.Version5:
                    return "v5";
                default:
                    throw new UnhandledEnumException(version);
            }
        }
        private string ConvertEnum(E_McrPeriodT period)
        {
            switch (period)
            {
                case E_McrPeriodT.Q1:
                    return "MCRQ1";
                case E_McrPeriodT.Q2:
                    return "MCRQ2";
                case E_McrPeriodT.Q3:
                    return "MCRQ3";
                case E_McrPeriodT.Q4:
                    return "MCRQ4";
                case E_McrPeriodT.FiscalQ1:
                    return "MCRFQ1";
                case E_McrPeriodT.FiscalQ2:
                    return "MCRFQ2";
                case E_McrPeriodT.FiscalQ3:
                    return "MCRFQ3";
                case E_McrPeriodT.FiscalAnnual:
                    return "MCRANNUAL";
                case E_McrPeriodT.FiscalYearToDate:
                    return "MCRYTD";
                default:
                    throw new UnhandledEnumException(period);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Mcr");

            writer.WriteAttributeString("type", ConvertEnum(McrFormatT));
            writer.WriteAttributeString("year", Year.ToString());
            writer.WriteAttributeString("periodType", ConvertEnum(McrPeriodT));
            if (McrVersionT != E_McrVersionT.Version1)
            {
                writer.WriteAttributeString("formVersion", ConvertEnum(McrVersionT));
            }
            if (ReportingDate != DateTime.MinValue && ReportingDate != DateTime.MaxValue)
            {
                writer.WriteAttributeString("reportingDate", ReportingDate.ToString("YYYY-MM-DD"));
            }

            if (m_rmlaList != null)
            {
                foreach (var o in m_rmlaList)
                {
                    Write(writer, o);
                }
                if (McrVersionT >= E_McrVersionT.Version4)
                {
                    WriteRmlag(writer, m_rmlaList);
                }
            }

            writer.WriteEndElement(); // </Mcr>
        }

        public string ToHtml()
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.ASCII;

            XmlWriter writer = XmlWriter.Create(sb, settings);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            WriteXml(writer);
            writer.Flush();
            return XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.NmlsCallReport.MCRCallReport.xslt.config", sb.ToString(), null);
        }
    }
}
