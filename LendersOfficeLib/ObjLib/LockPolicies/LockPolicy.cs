﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using System.Linq;
using System.Xml.Linq;
using CommonProjectLib.Common.Lib;
using LendersOffice.Constants;
using System.Reflection;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class LockPolicy
    {
        public Guid LockPolicyId { get; private set; }
        public string PolicyNm { get; set; }
        public Guid BrokerId { get; set; }
        private bool m_isNew { get; set; }
        private Dictionary<DayOfWeek, DaySetting> m_daySettings;
        public bool IsCodeBased { get; private set; }
        public string CodeBasedFullTypeName { get; private set; }
        public Guid DefaultLockDeskID { get; set; }

        /// <summary>
        /// Gets or sets a bool indicating whether the Rate Lock Confirmation defaults the 'From' Address to the lock desk's email.
        /// </summary>
        /// <value>True if the lock desk's email is used in the lock confirmation window.</value>
        public bool IsLockDeskEmailDefaultFromForRateLockConfirmation { get; set; }

        public Guid DefaultLockDeskIDOrInheritedID
        {
            get
            {
                if (DefaultLockDeskID == Guid.Empty) // opm 126355, per DW - default to broker level if policy level isn't
                {
                    var broker = BrokerDB.RetrieveById(BrokerId);
                    if (broker != null)
                    {
                        return broker.DefaultLockDeskID;
                    }
                }

                return DefaultLockDeskID;
            }
        }

        #region Lock Desk Hours Fields

        public IEnumerable<DaySetting> LpeLockDeskDaySettings
        {
            get
            {
                return m_daySettings.Values.OrderBy(p => p.Day == DayOfWeek.Sunday ? 7 : (int)p.Day);
            }
        }

        /// <summary>
        /// Updates to DaySettings returned by this object will be persisted when save is called.
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public DaySetting GetDaySettingsFor(DayOfWeek day)
        {
            return m_daySettings[day];
        }

        private DateTime GetPacificTimeFromLenderTimezone(DateTime dt)
        {
            try
            {
                switch (m_TimezoneForRsExpiration)
                {
                    case "EST":
                        if (dt.TimeOfDay < TimeSpan.FromHours(3))
                            return dt.Date;
                        else
                            return dt.AddHours(-3);
                    case "CST":
                        if (dt.TimeOfDay < TimeSpan.FromHours(2))
                            return dt.Date;
                        else
                            return dt.AddHours(-2);
                    case "MST":
                        if (BrokerDB.RetrieveById(BrokerId).Address.State.ToUpper() == "AZ" && TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now))
                        {
                            return dt;
                        }
                        if (dt.TimeOfDay < TimeSpan.FromHours(1))
                            return dt.Date;
                        else
                            return dt.AddHours(-1);
                    default:
                        return dt; //assume PST
                }
            }
            catch
            {
                return dt;
            }
        }

        public bool IsLockDeskOpened(DayOfWeek day)
        {
            DaySetting ds = GetDaySettingsFor(day);
            return ds.IsOpenForLocks;
        }

        /// <summary>
        /// Returns bad date with correct time.  (1/1/1) unless not enabled returns date min
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public TimeSpan GetLpeLockDeskWorkHourInPstStart(DayOfWeek day)
        {
            DaySetting ds = GetDaySettingsFor(day);
            if (ds.IsOpenForLocks)
            {
                return GetPacificTimeFromLenderTimezone(ds.StartTimeD).TimeOfDay;
            }
            else
            {
                return DateTime.MinValue.TimeOfDay;
            }
        }

        /// <summary>
        /// Returns bad date with correct time.  (1/1/1) unless its not enabled returnes bad range
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public TimeSpan GetLpeLockDeskWorkHourInPstEnd(DayOfWeek day)
        {
            DaySetting ds = GetDaySettingsFor(day);
            if (ds.IsOpenForLocks)
            {
                return GetPacificTimeFromLenderTimezone(ds.EndTimeD).TimeOfDay;
            }
            else
            {
                return DateTime.MinValue.TimeOfDay;
            }
        }

        public DateTime GetLpeLockDeskWorkHoursInLenderTimezoneStart(DayOfWeek day)
        {
            DaySetting ds = GetDaySettingsFor(day);
            if (ds.IsOpenForLocks)
            {
                return ds.StartTimeD;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        public DateTime GetLpeLockDeskWorkHoursInLenderTimezoneEnd(DayOfWeek day)
        {
            DaySetting ds = GetDaySettingsFor(day);
            if (ds.IsOpenForLocks)
            {
                return ds.EndTimeD;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// A backing field to contain the holiday closures.
        /// </summary>
        private LockDeskClosures holidayClosures;

        /// <summary>
        /// Gets the holiday closure data for this lock policy.
        /// </summary>
        /// <remarks>
        /// Ideally, this would be just a field initialized in the constructor,
        /// but neither constructor provides access to the lock policy id and we
        /// don't have time to refactor further.
        /// </remarks>
        private LockDeskClosures HolidayClosures
        {
            get
            {
                if (this.holidayClosures == null)
                {
                    this.holidayClosures = new LockDeskClosures(this.BrokerId, this.LockPolicyId);
                }

                return this.holidayClosures;
            }
        }

        /// <summary>
        /// Checks if time given is within lock desk working hours.
        /// </summary>
        private bool IsWithinLockDeskWorkingHour(DateTime now)
        {
            var start = GetLpeLockDeskWorkHourInPstStart(now.DayOfWeek);
            var end = GetLpeLockDeskWorkHourInPstEnd(now.DayOfWeek);

            DateTime _start = new DateTime(now.Year, now.Month, now.Day, start.Hours, start.Minutes, start.Seconds);
            DateTime _end = new DateTime(now.Year, now.Month, now.Day, end.Hours, end.Minutes, end.Seconds);

            return now.CompareTo(_start) >= 0 && now.CompareTo(_end) < 0;
        }

        ///// <summary>
        ///// Determine if it's closed (it's the weekend OR
        /////  they enforce lock desk hours and (it's outside the hours OR
        /////                                    it's a lock desk holiday)
        /////  )
        ///// </summary>
        public bool IsClosedForLockDesk(DateTime nowInPst)
        {
            // arranged by least DB-hit expensive to most.
            return
                       LpeIsEnforceLockDeskHourForNormalUser
                    && (
                             !IsWithinLockDeskWorkingHour(nowInPst)
                          || IsOneOfLockDeskHolidayClosures(nowInPst)
                       );
        }

        public bool IsOneOfBusinessHolidayClosures(DateTime now)
        {
            return this.HolidayClosures.FindClosureOrDefault(now)?.IsClosedForBusiness ?? false;
        }

        /// <summary>
        /// Determines if the lock policy is open for business on the given date.
        /// Does not check if the given DateTime is within the lock desk hours.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns>
        /// True if the lock policy is open for business on the given date and
        /// the given date is not a business holiday closure. Otherwise, false.
        /// </returns>
        public bool IsOpenForBusiness(DateTime date)
        {
            var daySettings = this.GetDaySettingsFor(date.DayOfWeek);

            return daySettings.IsOpenForBusiness && !this.IsOneOfBusinessHolidayClosures(date);
        }

        /// <summary>
        /// Determines if the time is during one of the defined holiday closures for the lender.
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public bool IsOneOfLockDeskHolidayClosures(DateTime now)
        {
            LockDeskClosureItem item = this.HolidayClosures.FindClosureOrDefault(now);
            if (item == null)
            {
                return false;
            }

            if (item.IsClosedForLocks)
            {
                return true;
            }
            else
            {
                DateTime openTime = GetPacificTimeFromLenderTimezone(item.LockDeskOpenTimeInLenderTimezone);
                DateTime closeTime = GetPacificTimeFromLenderTimezone(item.LockDeskCloseTimeInLenderTimezone);

                DateTime _open = new DateTime(now.Year, now.Month, now.Day, openTime.Hour, openTime.Minute, openTime.Second); // Strip out the date of openTime.
                DateTime _close = new DateTime(now.Year, now.Month, now.Day, closeTime.Hour, closeTime.Minute, closeTime.Minute); // Strip out the date of closeTime.

                return now.CompareTo(_open) < 0 || now.CompareTo(_close) > 0;
            }
        }

        public bool HasLpeLockDeskWorkHourEndTime_Obsolete
        {
            get { return m_LpeLockDeskWorkHourEndTime != DateTime.MinValue && m_LpeLockDeskWorkHourEndTime.CompareTo(new DateTime(1901, 1, 2)) > 0; }
        }

        private bool m_IsUsingRateSheetExpirationFeature = false;
        public bool IsUsingRateSheetExpirationFeature
        {
            get { return m_IsUsingRateSheetExpirationFeature; }
            set { m_IsUsingRateSheetExpirationFeature = value; }
        }

        private bool m_LpeIsEnforceLockDeskHourForNormalUser = false;
        public bool LpeIsEnforceLockDeskHourForNormalUser
        {
            get { return m_LpeIsEnforceLockDeskHourForNormalUser; }
            set { m_LpeIsEnforceLockDeskHourForNormalUser = value; }
        }

        private string m_TimezoneForRsExpiration = null;
        public string TimezoneForRsExpiration
        {
            get { return m_TimezoneForRsExpiration; }
            set { m_TimezoneForRsExpiration = value; }
        }

        private DateTime m_LpeLockDeskWorkHourStartTime = new DateTime(1901, 1, 1);
        public DateTime LpeLockDeskWorkHourStartTime_Obsolete
        {
            get { return m_LpeLockDeskWorkHourStartTime; }
            set { m_LpeLockDeskWorkHourStartTime = value; }
        }


        private DateTime m_LpeLockDeskWorkHourEndTime = new DateTime(1901, 1, 1);
        public DateTime LpeLockDeskWorkHourEndTime_Obsolete
        {
            get { return m_LpeLockDeskWorkHourEndTime; }
            set { m_LpeLockDeskWorkHourEndTime = value; }
        }

        private int m_LpeMinutesNeededToLockLoan = 30;
        public int LpeMinutesNeededToLockLoan
        {
            get { return m_LpeMinutesNeededToLockLoan; }
            set { m_LpeMinutesNeededToLockLoan = value; }
        }
        public string LpeMinutesNeededToLockLoan_rep
        {
            get { return m_LpeMinutesNeededToLockLoan.ToString(); }
            set
            {
                int mins;
                if (int.TryParse(value, out mins))
                    m_LpeMinutesNeededToLockLoan = mins;
            }
        }

        public string LockDeskHoursDetails
        {
            get
            {
                if (!IsUsingRateSheetExpirationFeature)
                    return "Disabled";


                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Enabled");
                sb.AppendLine("Hasten investor cutoff by: " + LpeMinutesNeededToLockLoan + " min");
                if (LpeIsEnforceLockDeskHourForNormalUser)
                {

                    List<Tuple<string, string, string>> hours = new List<Tuple<string, string, string>>();

                    foreach (DaySetting day in LpeLockDeskDaySettings)
                    {
                        if (!day.IsOpenForLocks)
                        {
                            hours.Add(Tuple.Create("", "", ""));
                            continue;
                        }
                        string key = day.StartTimeD.ToShortTimeString() + " " + TimezoneForRsExpiration + "-" + day.EndTimeD.ToShortTimeString() + " " + TimezoneForRsExpiration;
                        string dayShorthand = day.DayShorthand;

                        if (hours.Count == 0 || key != hours.Last().Item3)
                        {
                            hours.Add(Tuple.Create(dayShorthand, dayShorthand, key));
                        }
                        else
                        {
                            var current = hours.Last();
                            hours[hours.Count - 1] = Tuple.Create(current.Item1, dayShorthand, key);
                        }
                    }

                    sb.AppendLine("Hours Open:");
                    foreach (var entry in hours)
                    {
                        if (entry.Item1.Length == 0)
                        {
                            continue;
                        }
                        string days = entry.Item1;

                        if (entry.Item1 != entry.Item2)
                        {
                            days += "-" + entry.Item2;
                        }

                        sb.AppendLine(String.Format("{0,5} {1}", days, entry.Item3));
                    }
                }
                return sb.ToString();
            }
        }

        #endregion

        #region Lock Extension Fields

        private bool m_EnableAutoLockExtensions = false;
        public bool EnableAutoLockExtensions
        {
            get
            {
                if (IsCodeBased)
                {
                    return false; // 1/14/2014 dd - Will handle in code based.
                }
                return m_EnableAutoLockExtensions;
            }
            set { m_EnableAutoLockExtensions = value; }
        }

        private int m_MaxLockExtensions = 0;
        public int MaxLockExtensions
        {
            get { return m_MaxLockExtensions; }
            set { m_MaxLockExtensions = value; }
        }
        public string MaxLockExtensions_rep
        {
            get { return m_MaxLockExtensions.ToString(); }
            set
            {
                int num;
                if (int.TryParse(value, out num))
                    m_MaxLockExtensions = num;
            }
        }

        private int m_MaxTotalLockExtensionDays = 0;
        public int MaxTotalLockExtensionDays
        {
            get { return m_MaxTotalLockExtensionDays; }
            set { m_MaxTotalLockExtensionDays = value; }
        }
        public string MaxTotalLockExtensionDays_rep
        {
            get { return m_MaxTotalLockExtensionDays.ToString(); }
            set
            {
                int days;
                if (int.TryParse(value, out days))
                    m_MaxTotalLockExtensionDays = days;
            }
        }

        private List<LockExtensionOption> m_lockExtensionOptionTable = new List<LockExtensionOption>();
        public List<LockExtensionOption> LockExtensionOptionTable
        {
            get { return m_lockExtensionOptionTable; }
            set { m_lockExtensionOptionTable = value; }
        }
        public string LockExtensionOptionTableXmlContent
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(List<LockExtensionOption>));
                    ser.Serialize(writer, m_lockExtensionOptionTable);
                }
                return sb.ToString();
            }
            set
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<LockExtensionOption>));
                using (StringReader reader = new StringReader(value))
                {
                    m_lockExtensionOptionTable = (List<LockExtensionOption>)ser.Deserialize(reader);
                }
            }
        }

        private decimal m_LockExtensionMarketWorsenPoints = 0;
        public decimal LockExtensionMarketWorsenPoints
        {
            get { return m_LockExtensionMarketWorsenPoints; }
            set { m_LockExtensionMarketWorsenPoints = value; }
        }
        public string LockExtensionMarketWorsenPoints_rep
        {
            get { return m_LockExtensionMarketWorsenPoints.ToString(); }
            set
            {
                decimal points;
                if (decimal.TryParse(value, out points))
                    m_LockExtensionMarketWorsenPoints = points;
            }
        }

        public string LockExtensionsDetails
        {
            get
            {
                if (IsCodeBased)
                {
                    return "Contact Support for update to policy.";
                }
                if (!EnableAutoLockExtensions)
                    return "Manual";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Max # of Extensions: " + MaxLockExtensions);
                sb.AppendLine("Max Total Extension: " + MaxTotalLockExtensionDays + " days");
                foreach (LockExtensionOption option in LockExtensionOptionTable)
                {
                    decimal minPoints = Math.Min(option.FeeImproving, Math.Min(option.FeeStatic, option.FeeWorsening)),
                        maxPoints = Math.Max(option.FeeImproving, Math.Max(option.FeeStatic, option.FeeWorsening));
                    sb.AppendLine(option.ExtensionLength + " days: " + minPoints.ToString("F3") + "-" + maxPoints.ToString("F3") + " points");
                }
                sb.AppendLine("Extend at Market Points Trigger: " + LockExtensionMarketWorsenPoints.ToString("F3") + " points");
                return sb.ToString();
            }
        }

        #endregion

        #region Float Down Fields

        private bool m_EnableAutoFloatDowns = false;
        public bool EnableAutoFloatDowns
        {
            get
            {
                if (IsCodeBased)
                {
                    return false; // 1/14/2014 dd - Will handle in code based.
                }

                return m_EnableAutoFloatDowns;
            }
            set { m_EnableAutoFloatDowns = value; }
        }

        private bool m_FloatDownAllowedAfterLockExtension = false;
        public bool FloatDownAllowedAfterLockExtension
        {
            get { return m_FloatDownAllowedAfterLockExtension; }
            set { m_FloatDownAllowedAfterLockExtension = value; }
        }

        private bool m_FloatDownAllowedAfterReLock = false;
        public bool FloatDownAllowedAfterReLock
        {
            get { return m_FloatDownAllowedAfterReLock; }
            set { m_FloatDownAllowedAfterReLock = value; }
        }

        private int m_MaxFloatDowns = 0;
        public int MaxFloatDowns
        {
            get { return m_MaxFloatDowns; }
            set { m_MaxFloatDowns = value; }
        }
        public string MaxFloatDowns_rep
        {
            get { return m_MaxFloatDowns.ToString(); }
            set
            {
                int num;
                if (int.TryParse(value, out num))
                    m_MaxFloatDowns = num;
            }
        }

        private decimal m_FloatDownPointFee = 0;
        public decimal FloatDownPointFee
        {
            get { return m_FloatDownPointFee; }
            set { m_FloatDownPointFee = value; }
        }
        public string FloatDownPointFee_rep
        {
            get { return m_FloatDownPointFee.ToString(); }
            set
            {
                decimal points;
                if (decimal.TryParse(value, out points))
                    m_FloatDownPointFee = points;
            }
        }

        private decimal m_FloatDownPercentFee = 0;
        public decimal FloatDownPercentFee
        {
            get { return m_FloatDownPercentFee; }
            set { m_FloatDownPercentFee = value; }
        }
        public string FloatDownPercentFee_rep
        {
            get { return m_FloatDownPercentFee.ToString(); }
            set
            {
                decimal percent;
                if (decimal.TryParse(value, out percent))
                    m_FloatDownPercentFee = percent;
            }
        }

        private bool m_FloatDownFeeIsPercent = false;
        public bool FloatDownFeeIsPercent
        {
            get { return m_FloatDownFeeIsPercent; }
            set { m_FloatDownFeeIsPercent = value; }
        }

        private decimal m_FloatDownMinRateImprovement = 0;
        public decimal FloatDownMinRateImprovement
        {
            get { return m_FloatDownMinRateImprovement; }
            set { m_FloatDownMinRateImprovement = value; }
        }
        public string FloatDownMinRateImprovement_rep
        {
            get { return m_FloatDownMinRateImprovement.ToString(); }
            set
            {
                decimal rateImprovement;
                if (decimal.TryParse(value, out rateImprovement))
                    m_FloatDownMinRateImprovement = rateImprovement;
            }
        }

        private decimal m_FloatDownMinPointImprovement = 0;
        public decimal FloatDownMinPointImprovement
        {
            get { return m_FloatDownMinPointImprovement; }
            set { m_FloatDownMinPointImprovement = value; }
        }
        public string FloatDownMinPointImprovement_rep
        {
            get { return m_FloatDownMinPointImprovement.ToString(); }
            set
            {
                decimal pointImprovement;
                if (decimal.TryParse(value, out pointImprovement))
                    m_FloatDownMinPointImprovement = pointImprovement;
            }
        }

        private bool m_FloatDownAllowRateOptionsRequiringAdditionalPoints = false;
        public bool FloatDownAllowRateOptionsRequiringAdditionalPoints
        {
            get { return m_FloatDownAllowRateOptionsRequiringAdditionalPoints; }
            set { m_FloatDownAllowRateOptionsRequiringAdditionalPoints = value; }
        }

        public string FloatDownsDetails
        {
            get
            {
                if (IsCodeBased)
                {
                    return "Contact Support for update to policy.";
                }
                if (!EnableAutoFloatDowns)
                    return "Manual";
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Allow after Lock Extension: " + (FloatDownAllowedAfterLockExtension ? "Yes" : "No"));
                sb.AppendLine("Allow after Rate Re-Lock: " + (FloatDownAllowedAfterReLock ? "Yes" : "No"));
                sb.AppendLine("Max # of Float Downs: " + MaxFloatDowns);
                sb.AppendLine("Float Down Fee: " +
                    (FloatDownFeeIsPercent ? FloatDownPercentFee.ToString("F1") + "% of market improvement"
                    : FloatDownPointFee.ToString("F3") + " points"));
                sb.AppendLine("Min Rate Improvement: " + FloatDownMinRateImprovement.ToString("F3") + "%");
                sb.AppendLine("Min Market Points Improvement: " + FloatDownMinPointImprovement.ToString("F3") + " points");
                if (FloatDownAllowRateOptionsRequiringAdditionalPoints)
                {
                    sb.AppendLine("Allow Float Down to higher discount points");
                }
                else
                {
                    sb.AppendLine("Don't allow Float Down to higher discount");
                }
                return sb.ToString();
            }
        }

        #endregion

        #region Re-Lock Fields

        private bool m_EnableAutoReLocks = false;
        public bool EnableAutoReLocks
        {
            get
            {
                if (IsCodeBased)
                {
                    return false; // 1/14/2014 dd - Will handle in code based.
                }

                return m_EnableAutoReLocks;
            }
            set { m_EnableAutoReLocks = value; }
        }

        private int m_MaxReLocks = 0;
        public int MaxReLocks
        {
            get { return m_MaxReLocks; }
            set { m_MaxReLocks = value; }
        }
        public string MaxReLocks_rep
        {
            get { return m_MaxReLocks.ToString(); }
            set
            {
                int num;
                if (int.TryParse(value, out num))
                    m_MaxReLocks = num;
            }
        }

        private int m_ReLockWorstCasePricingMaxDays = 0;
        public int ReLockWorstCasePricingMaxDays
        {
            get { return m_ReLockWorstCasePricingMaxDays; }
            set { m_ReLockWorstCasePricingMaxDays = value; }
        }
        public string ReLockWorstCasePricingMaxDays_rep
        {
            get { return m_ReLockWorstCasePricingMaxDays.ToString(); }
            set
            {
                int days;
                if (int.TryParse(value, out days))
                    m_ReLockWorstCasePricingMaxDays = days;
            }
        }

        private List<ReLockFee> m_reLockFeeTable = new List<ReLockFee>();
        public List<ReLockFee> ReLockFeeTable
        {
            get { return m_reLockFeeTable; }
            set { m_reLockFeeTable = value; }
        }

        public string ReLockFeeTableXmlContent
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(List<ReLockFee>));
                    ser.Serialize(writer, m_reLockFeeTable);
                }
                return sb.ToString();
            }
            set
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<ReLockFee>));
                using (StringReader reader = new StringReader(value))
                {
                    m_reLockFeeTable = (List<ReLockFee>)ser.Deserialize(reader);
                }
            }
        }

        private decimal m_ReLockMarketPriceReLockFee = 0;
        public decimal ReLockMarketPriceReLockFee
        {
            get { return m_ReLockMarketPriceReLockFee; }
            set { m_ReLockMarketPriceReLockFee = value; }
        }
        public string ReLockMarketPriceReLockFee_rep
        {
            get { return m_ReLockMarketPriceReLockFee.ToString(); }
            set
            {
                decimal fee;
                if (decimal.TryParse(value, out fee))
                    m_ReLockMarketPriceReLockFee = fee;
            }
        }

        private bool m_ReLockRequireSameInvestor = false;
        public bool ReLockRequireSameInvestor
        {
            get { return m_ReLockRequireSameInvestor; }
            set { m_ReLockRequireSameInvestor = value; }
        }

        public string ReLocksDetails
        {
            get
            {
                if (IsCodeBased)
                {
                    return "Contact Support for update to policy.";
                }

                if (!EnableAutoReLocks)
                    return "Manual";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Max # of Re-Locks: " + MaxReLocks);
                sb.AppendLine("Worst-Case Pricing if <= " + ReLockWorstCasePricingMaxDays + " days expired");
                foreach (ReLockFee fee in ReLockFeeTable)
                {
                    sb.AppendLine(fee.PeriodEndDay + " day lock: " + fee.Fee.ToString("F3") + " points");
                }
                sb.AppendLine("Current-Pricing if > " + ReLockWorstCasePricingMaxDays + " days expired");
                sb.AppendLine("Current-Pricing Re-Lock Fee: " + ReLockMarketPriceReLockFee.ToString("F3") + " points");
                return sb.ToString();
            }
        }

        #endregion

        #region Code Based Lock Policies
        public AbstractCodeBasedLockPolicy GetCustomLockPolicy()
        {
            if (IsCodeBased == false)
            {
                throw CBaseException.GenericException("GetCustomLockPolicy is only valid for code based policy");
            }
            Type t = Type.GetType(CodeBasedFullTypeName);

            return (AbstractCodeBasedLockPolicy)Activator.CreateInstance(t);
        }
        #endregion
        /// <summary>
        /// for use with retrieve since that sets a brokerid
        /// </summary>
        private LockPolicy()
        {
        }

        public LockPolicy(Guid broker)
        {
            LockPolicyId = Guid.Empty;
            PolicyNm = "";
            TimezoneForRsExpiration = "PST";
            BrokerId = broker;
            m_isNew = true;
            this.IsLockDeskEmailDefaultFromForRateLockConfirmation = true;
            m_daySettings = new Dictionary<DayOfWeek, DaySetting>()
            {
                { DayOfWeek.Friday, new DaySetting(DayOfWeek.Friday) },
                { DayOfWeek.Monday, new DaySetting(DayOfWeek.Monday) },
                { DayOfWeek.Saturday, new DaySetting(DayOfWeek.Saturday) },
                { DayOfWeek.Sunday, new DaySetting(DayOfWeek.Sunday) },
                { DayOfWeek.Thursday, new DaySetting(DayOfWeek.Thursday) },
                { DayOfWeek.Tuesday, new DaySetting(DayOfWeek.Tuesday) },
                { DayOfWeek.Wednesday, new DaySetting(DayOfWeek.Wednesday) },
            };
        }

        private void PopulateDaySettings(string xml)
        {
            m_daySettings = new Dictionary<DayOfWeek, DaySetting>();

            if (string.IsNullOrEmpty(xml))
            {

                foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
                {
                    m_daySettings.Add(day, new DaySetting(day));
                }

                return;
            }

            XDocument doc = XDocument.Parse(xml);

            foreach (XElement daySetting in doc.Root.Elements("DaySetting"))
            {
                int day = int.Parse(daySetting.Attribute("day").Value);
                DaySetting ds = new DaySetting((DayOfWeek)day);

                ds.IsOpenForBusiness = daySetting.Attribute("isOpenForBusiness").Value == "True";
                ds.IsOpenForLocks = daySetting.Attribute("isOpenForLocks").Value == "True";
                ds.StartHour = int.Parse(daySetting.Attribute("startHour").Value);
                ds.EndHour = int.Parse(daySetting.Attribute("endHour").Value);
                ds.StartMinute = int.Parse(daySetting.Attribute("startMinute").Value);
                ds.EndMinute = int.Parse(daySetting.Attribute("endMinute").Value);

                m_daySettings.Add(ds.Day, ds);
            }


            if (m_daySettings.Count != 7)
            {
                throw new CBaseException("Invalid Lock Policy State", xml);
            }
        }

        private string SerializeDaySettings()
        {
            XDocument doc = new XDocument(
                new XElement("DaySettings"));

            foreach (DaySetting ds in LpeLockDeskDaySettings)
            {
                if (!ds.IsValid())
                {
                    throw new CBaseException("Invalid lock desk hours for " + ds.Day, "Open Lock Desk Hour range is not valid.");
                }
                if (ds.StartHour < 3 && ds.IsOpenForLocks)
                {
                    throw new CBaseException("Please enter a start time on or after 3am for " + ds.Day, "Timezone 3am Limitation");
                }
                doc.Root.Add(new XElement("DaySetting",
                 new XAttribute("day", ds.Day.ToString("D")),
                 new XAttribute("isOpenForBusiness", ds.IsOpenForBusiness.ToString()),
                 new XAttribute("isOpenForLocks", ds.IsOpenForLocks.ToString()),
                 new XAttribute("startHour", ds.StartHour.ToString()),
                 new XAttribute("endHour", ds.EndHour.ToString()),
                 new XAttribute("startMinute", ds.StartMinute.ToString()),
                 new XAttribute("endMinute", ds.EndMinute.ToString())));
            }


            return doc.ToString(SaveOptions.DisableFormatting);
        }

        private LockPolicy(DataRow data)
        {
            #region Load DataRow fields
            LockPolicyId = (Guid)data["LockPolicyId"];
            BrokerId = (Guid)data["BrokerId"];
            DefaultLockDeskID = (Guid)data["DefaultLockDeskID"];
            PolicyNm = data["PolicyNm"].ToString();
            IsLockDeskEmailDefaultFromForRateLockConfirmation = (bool)data["IsLockDeskEmailDefaultFromForRateLockConfirmation"];

            IsUsingRateSheetExpirationFeature = (bool)data["IsUsingRateSheetExpirationFeature"];
            LpeIsEnforceLockDeskHourForNormalUser = (bool)data["LpeIsEnforceLockDeskHourForNormalUser"];
            TimezoneForRsExpiration = data["TimezoneForRsExpiration"].ToString();
            LpeLockDeskWorkHourStartTime_Obsolete = (DateTime)data["LpeLockDeskWorkHourStartTime"];
            LpeLockDeskWorkHourEndTime_Obsolete = (DateTime)data["LpeLockDeskWorkHourEndTime"];
            LpeMinutesNeededToLockLoan = (int)data["LpeMinutesNeededToLockLoan"];

            EnableAutoLockExtensions = (bool)data["EnableAutoLockExtensions"];
            MaxLockExtensions = (int)data["MaxLockExtensions"];
            MaxTotalLockExtensionDays = (int)data["MaxTotalLockExtensionDays"];
            LockExtensionOptionTableXmlContent = data["LockExtensionOptionTableXmlContent"].ToString();
            LockExtensionMarketWorsenPoints = (decimal)data["LockExtensionMarketWorsenPoints"];

            EnableAutoFloatDowns = (bool)data["EnableAutoFloatDowns"];
            MaxFloatDowns = (int)data["MaxFloatDowns"];
            FloatDownAllowedAfterLockExtension = (bool)data["FloatDownAllowedAfterLockExtension"];
            FloatDownAllowedAfterReLock = (bool)data["FloatDownAllowedAfterReLock"];
            FloatDownPointFee = (decimal)data["FloatDownPointFee"];
            FloatDownPercentFee = (decimal)data["FloatDownPercentFee"];
            FloatDownFeeIsPercent = (bool)data["FloatDownFeeIsPercent"];
            FloatDownMinRateImprovement = (decimal)data["FloatDownMinRateImprovement"];
            FloatDownMinPointImprovement = (decimal)data["FloatDownMinPointImprovement"];
            FloatDownAllowRateOptionsRequiringAdditionalPoints = (bool)data["FloatDownAllowRateOptionsRequiringAdditionalPoints"];

            EnableAutoReLocks = (bool)data["EnableAutoReLocks"];
            MaxReLocks = (int)data["MaxReLocks"];
            ReLockWorstCasePricingMaxDays = (int)data["ReLockWorstCasePricingMaxDays"];
            ReLockFeeTableXmlContent = data["ReLockFeeTableXmlContent"].ToString();
            ReLockMarketPriceReLockFee = (decimal)data["ReLockMarketPriceReLockFee"];
            ReLockRequireSameInvestor = (bool)data["ReLockRequireSameInvestor"];
            IsCodeBased = (bool)data["IsCodeBased"];
            CodeBasedFullTypeName = (string)data["CodeBasedFullTypeName"];
            #endregion

            PopulateDaySettings(data["DaySettingsXml"].ToString());
            m_isNew = false;
        }

        public bool PolicyNameIsAvailable()
        {
            SqlParameter[] parameters = new SqlParameter[4];
            parameters[0] = new SqlParameter("LockPolicyId", LockPolicyId);
            parameters[1] = new SqlParameter("BrokerId", BrokerId);
            parameters[2] = new SqlParameter("PolicyNm", PolicyNm);
            parameters[3] = new SqlParameter("IsDuplicateId", SqlDbType.Bit);
            parameters[3].Direction = ParameterDirection.Output;
            StoredProcedureHelper.ExecuteScalar(this.BrokerId, "LockPolicy_VerifyName", parameters);
            return (bool)parameters[3].Value;
        }

        public void Save()
        {
            string procedureName = m_isNew ? "LockPolicy_Create" : "LockPolicy_Update";

            if (m_isNew)
                LockPolicyId = Guid.NewGuid();

            List<SqlParameter> parameters = new List<SqlParameter>();
            #region SQL Parameters
            parameters.Add(new SqlParameter("LockPolicyId", LockPolicyId));
            parameters.Add(new SqlParameter("PolicyNm", PolicyNm));
            parameters.Add(new SqlParameter("BrokerId", BrokerId));
            parameters.Add(new SqlParameter("DefaultLockDeskID", DefaultLockDeskID));
            parameters.Add(new SqlParameter("IsLockDeskEmailDefaultFromForRateLockConfirmation", IsLockDeskEmailDefaultFromForRateLockConfirmation));

            parameters.Add(new SqlParameter("IsUsingRateSheetExpirationFeature", IsUsingRateSheetExpirationFeature));
            parameters.Add(new SqlParameter("LpeIsEnforceLockDeskHourForNormalUser", LpeIsEnforceLockDeskHourForNormalUser));
            parameters.Add(new SqlParameter("TimezoneForRsExpiration", TimezoneForRsExpiration));
            //parameters.Add(new SqlParameter("LpeLockDeskWorkHourStartTime", LpeLockDeskWorkHourStartTime));
            //parameters.Add(new SqlParameter("LpeLockDeskWorkHourEndTime", LpeLockDeskWorkHourEndTime));
            parameters.Add(new SqlParameter("LpeMinutesNeededToLockLoan", LpeMinutesNeededToLockLoan));

            parameters.Add(new SqlParameter("EnableAutoLockExtensions", EnableAutoLockExtensions));
            parameters.Add(new SqlParameter("MaxLockExtensions", MaxLockExtensions));
            parameters.Add(new SqlParameter("MaxTotalLockExtensionDays", MaxTotalLockExtensionDays));
            parameters.Add(new SqlParameter("LockExtensionOptionTableXmlContent", LockExtensionOptionTableXmlContent));
            parameters.Add(new SqlParameter("LockExtensionMarketWorsenPoints", LockExtensionMarketWorsenPoints));

            parameters.Add(new SqlParameter("EnableAutoFloatDowns", EnableAutoFloatDowns));
            parameters.Add(new SqlParameter("MaxFloatDowns", MaxFloatDowns));
            parameters.Add(new SqlParameter("FloatDownAllowedAfterLockExtension", FloatDownAllowedAfterLockExtension));
            parameters.Add(new SqlParameter("FloatDownAllowedAfterReLock", FloatDownAllowedAfterReLock));
            parameters.Add(new SqlParameter("FloatDownPointFee", FloatDownPointFee));
            parameters.Add(new SqlParameter("FloatDownPercentFee", FloatDownPercentFee));
            parameters.Add(new SqlParameter("FloatDownFeeIsPercent", FloatDownFeeIsPercent));
            parameters.Add(new SqlParameter("FloatDownMinRateImprovement", FloatDownMinRateImprovement));
            parameters.Add(new SqlParameter("FloatDownMinPointImprovement", FloatDownMinPointImprovement));
            parameters.Add(new SqlParameter("FloatDownAllowRateOptionsRequiringAdditionalPoints", FloatDownAllowRateOptionsRequiringAdditionalPoints));

            parameters.Add(new SqlParameter("EnableAutoReLocks", EnableAutoReLocks));
            parameters.Add(new SqlParameter("MaxReLocks", MaxReLocks));
            parameters.Add(new SqlParameter("ReLockWorstCasePricingMaxDays", ReLockWorstCasePricingMaxDays));
            parameters.Add(new SqlParameter("ReLockFeeTableXmlContent", ReLockFeeTableXmlContent));
            parameters.Add(new SqlParameter("ReLockMarketPriceReLockFee", ReLockMarketPriceReLockFee));
            parameters.Add(new SqlParameter("ReLockRequireSameInvestor", ReLockRequireSameInvestor));
            parameters.Add(new SqlParameter("DaySettingsXml", SerializeDaySettings()));
            #endregion

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, procedureName, 3, parameters);
        }

        public void DuplicateHolidaysFrom(Guid brokerId, Guid LockPolicyId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("ToPolicy", this.LockPolicyId), 
                                            new SqlParameter("FromPolicy", LockPolicyId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "LockPolicy_DuplicateHolidays", 3, parameters);
        }
        public void DuplicateDisabledPricingFrom(Guid LockPolicyId)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "LockPolicy_DuplicateDisabledPricing", 3, new SqlParameter("ToPolicy", this.LockPolicyId), new SqlParameter("FromPolicy", LockPolicyId));
        }

        public static LockPolicy Retrieve(Guid brokerId, Guid LockPolicyId)
        {
            LockPolicy policy = null;
            using (PerformanceStopwatch.Start("Retrieve LockPolicy"))
            {
                policy = (LockPolicy)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.LockPolicyKey(LockPolicyId));
                if (policy == null)
                {
                    using (PerformanceStopwatch.Start("Load LockPolicy From DB"))
                    {
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@LockPolicyId", LockPolicyId)
                                                    };
                        policy = new LockPolicy();
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "LockPolicy_Retrieve", parameters))
                        {
                            if (reader.Read())
                            {
                                #region Load Fields From Database
                                policy.LockPolicyId = (Guid)reader["LockPolicyId"];
                                policy.BrokerId = (Guid)reader["BrokerId"];
                                policy.DefaultLockDeskID = (Guid)reader["DefaultLockDeskID"];
                                policy.PolicyNm = reader["PolicyNm"].ToString();
                                policy.IsLockDeskEmailDefaultFromForRateLockConfirmation = (bool)reader["IsLockDeskEmailDefaultFromForRateLockConfirmation"];

                                policy.IsUsingRateSheetExpirationFeature = (bool)reader["IsUsingRateSheetExpirationFeature"];
                                policy.LpeIsEnforceLockDeskHourForNormalUser = (bool)reader["LpeIsEnforceLockDeskHourForNormalUser"];
                                policy.TimezoneForRsExpiration = reader["TimezoneForRsExpiration"].ToString();
                                //policy.LpeLockDeskWorkHourStartTime = (DateTime)reader["LpeLockDeskWorkHourStartTime"];
                                //policy.LpeLockDeskWorkHourEndTime = (DateTime)reader["LpeLockDeskWorkHourEndTime"];
                                policy.LpeMinutesNeededToLockLoan = (int)reader["LpeMinutesNeededToLockLoan"];

                                policy.EnableAutoLockExtensions = (bool)reader["EnableAutoLockExtensions"];
                                policy.MaxLockExtensions = (int)reader["MaxLockExtensions"];
                                policy.MaxTotalLockExtensionDays = (int)reader["MaxTotalLockExtensionDays"];
                                policy.LockExtensionOptionTableXmlContent = reader["LockExtensionOptionTableXmlContent"].ToString();
                                policy.LockExtensionMarketWorsenPoints = (decimal)reader["LockExtensionMarketWorsenPoints"];

                                policy.EnableAutoFloatDowns = (bool)reader["EnableAutoFloatDowns"];
                                policy.MaxFloatDowns = (int)reader["MaxFloatDowns"];
                                policy.FloatDownAllowedAfterLockExtension = (bool)reader["FloatDownAllowedAfterLockExtension"];
                                policy.FloatDownAllowedAfterReLock = (bool)reader["FloatDownAllowedAfterReLock"];
                                policy.FloatDownPointFee = (decimal)reader["FloatDownPointFee"];
                                policy.FloatDownPercentFee = (decimal)reader["FloatDownPercentFee"];
                                policy.FloatDownFeeIsPercent = (bool)reader["FloatDownFeeIsPercent"];
                                policy.FloatDownMinRateImprovement = (decimal)reader["FloatDownMinRateImprovement"];
                                policy.FloatDownMinPointImprovement = (decimal)reader["FloatDownMinPointImprovement"];
                                policy.FloatDownAllowRateOptionsRequiringAdditionalPoints = (bool)reader["FloatDownAllowRateOptionsRequiringAdditionalPoints"];

                                policy.EnableAutoReLocks = (bool)reader["EnableAutoReLocks"];
                                policy.MaxReLocks = (int)reader["MaxReLocks"];
                                policy.ReLockWorstCasePricingMaxDays = (int)reader["ReLockWorstCasePricingMaxDays"];
                                policy.ReLockFeeTableXmlContent = reader["ReLockFeeTableXmlContent"].ToString();
                                policy.ReLockMarketPriceReLockFee = (decimal)reader["ReLockMarketPriceReLockFee"];
                                policy.ReLockRequireSameInvestor = (bool)reader["ReLockRequireSameInvestor"];
                                policy.IsCodeBased = (bool)reader["IsCodeBased"];
                                policy.CodeBasedFullTypeName = (string)reader["CodeBasedFullTypeName"];
                                #endregion

                                policy.PopulateDaySettings(reader["DaySettingsXml"].ToString());

                                policy.m_isNew = false;
                            }
                            else
                            {
                                throw new NotFoundException("Lock Policy not found. Its id was: " + LockPolicyId);
                            }
                        }
                    }
                    CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.LockPolicyKey(LockPolicyId), policy);
                }
            }
            return policy;
        }

        public static List<LockPolicy> RetrieveAllForBroker(Guid BrokerId)
        {
            List<LockPolicy> policies = new List<LockPolicy>();

            SqlParameter[] parameters = {
                                            new SqlParameter("BrokerId", BrokerId)
                                        };
            DataTable dt = StoredProcedureHelper.ExecuteDataTable(BrokerId, "LockPolicy_RetrieveAllForBroker", parameters);
            LockPolicy policy;
            foreach (DataRow dr in dt.Rows)
            {
                policy = new LockPolicy(dr);
                if (policy.PolicyNm == "Default") //Default policy should be first in the list
                    policies.Insert(0, policy);
                else
                    policies.Add(policy);
            }

            return policies;
        }

        public static void DeleteLockPolicy(Guid brokerId, Guid policyId)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DeleteManualDisabledInvestorProductsByPolicyId", 3, new SqlParameter("LockPolicyId", policyId));

            SqlParameter[] parameters = {
                                            new SqlParameter("LockPolicyId", policyId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "LockPolicy_Delete", 3, parameters);
        }
        public static List<string> GetSafePriceGroupNames(Guid brokerId, Guid policyId)
        {
            List<string> list = new List<string>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@PolicyId", policyId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "LockPolicy_ListPriceGroupNamesForPolicy", parameters))
            {
                while (reader.Read())
                {
                    list.Add(AspxTools.HtmlString(reader["LpePriceGroupName"].ToString()));
                }
            }
            return list;
        }
    }
    public class ReLockFee : IComparable
    {
        public int PeriodEndDay { get; set; }
        public decimal Fee { get; set; }

        int IComparable.CompareTo(object obj)
        {
            ReLockFee other = (ReLockFee)obj;
            return this.PeriodEndDay - other.PeriodEndDay;
        }
    }
    public class LockExtensionOption : IComparable
    {
        public int ExtensionLength { get; set; }
        public decimal FeeImproving { get; set; }
        public decimal FeeStatic { get; set; }
        public decimal FeeWorsening { get; set; }

        int IComparable.CompareTo(object obj)
        {
            LockExtensionOption other = (LockExtensionOption)obj;
            return this.ExtensionLength - other.ExtensionLength;
        }
    }
}
