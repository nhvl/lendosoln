﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class CodeBasedRelockResultItem : AbstractCodeBasedPolicyResultItem
    {
        public Guid lLpTemplateId { get; private set; }
        public decimal Rate { get; private set; }
        public decimal OriginalBasePoint { get; private set; }
        public decimal NewBasePoint { get; private set; }
        public decimal TotalAdjustmentFee { get; private set; }
        public decimal FinalBasePoint { get; private set; }
        public decimal RelockFee { get; private set; }
        public int LockPeriod { get { return 30; } }
        public decimal OriginalPoint
        {
            get
            {
                return OriginalBasePoint + TotalAdjustmentFee;
            }
        }
        public decimal NewPoint
        {
            get
            {
                return NewBasePoint + TotalAdjustmentFee;
            }
        }
        public decimal FinalPointWithRelockFee
        {
            get { return FinalBasePoint + TotalAdjustmentFee + RelockFee; }
        }
        public CodeBasedRelockResultItem(string encryptedData)
            : base(encryptedData)
        {
        }

        public CodeBasedRelockResultItem(Guid loanId, Guid userId, 
            Guid _lLpTemplateId, decimal rate, decimal originalBasePoint, decimal newBasePoint, 
            decimal totalAdjustmentFee, decimal finalBasePoint, decimal relockFee)
            : base(loanId, userId)
        {
            this.lLpTemplateId = _lLpTemplateId;
            this.Rate = rate;
            this.OriginalBasePoint = originalBasePoint;
            this.NewBasePoint = newBasePoint;
            this.TotalAdjustmentFee = totalAdjustmentFee;
            this.FinalBasePoint = finalBasePoint;
            this.RelockFee = relockFee;
        }
        protected override IEnumerable<string> BuildDataList()
        {
            yield return lLpTemplateId.ToString(); // 0;
            yield return Rate.ToString(); // 1
            yield return OriginalBasePoint.ToString(); // 2
            yield return NewBasePoint.ToString(); //3
            yield return TotalAdjustmentFee.ToString(); // 4
            yield return FinalBasePoint.ToString(); // 5
            yield return RelockFee.ToString(); // 6
        }

        protected override void ParseData(string[] parts)
        {
            lLpTemplateId = new Guid(parts[0]);
            Rate = decimal.Parse(parts[1]);
            OriginalBasePoint = decimal.Parse(parts[2]);
            NewBasePoint = decimal.Parse(parts[3]);
            TotalAdjustmentFee = decimal.Parse(parts[4]);
            FinalBasePoint = decimal.Parse(parts[5]);
            RelockFee = decimal.Parse(parts[6]);
        }
    }
}
