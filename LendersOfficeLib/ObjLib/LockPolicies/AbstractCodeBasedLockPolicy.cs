﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.ObjLib.PriceGroups;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.RatePrice;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.LockPolicies
{
    public abstract class AbstractCodeBasedLockPolicy
    {

        private CPageData CreatePageData(Guid sLId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(AbstractCodeBasedLockPolicy));

            return dataLoan;
        }

        private bool HasWorkflowPermission(Guid sLId, E_AutoRateLockAction action, out string errorMessage)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            WorkflowOperation workflowOperation = null;

            switch (action) 
            {
                case E_AutoRateLockAction.Extend:
                    workflowOperation = WorkflowOperations.ExtendLock;
                    break;
                case E_AutoRateLockAction.FloatDown:
                    workflowOperation = WorkflowOperations.FloatDownLock;
                    break;
                case E_AutoRateLockAction.ReLock:
                    workflowOperation = WorkflowOperations.RateReLock;
                    break;
                default:
                    throw new UnhandledEnumException(action);
            }

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, sLId, workflowOperation);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            bool isAllow = LendingQBExecutingEngine.CanPerform(workflowOperation, valueEvaluator);
            errorMessage = string.Empty;
            if (isAllow == false)
            {
                IReadOnlyCollection<string> msgList = LendingQBExecutingEngine.GetUserFriendlyMessageList(workflowOperation, valueEvaluator);

                if (msgList.Any())
                {
                    errorMessage = Environment.NewLine;
                    errorMessage += string.Join(Environment.NewLine, msgList);
                }
                else
                {
                    errorMessage = "Access Denied.  You do not have permission to take this action.";
                }
            }

            return isAllow;

        }

        public bool IsAllowLockExtension(Guid sLId)
        {
            string errMsg = string.Empty;
            if (HasWorkflowPermission(sLId, E_AutoRateLockAction.Extend, out errMsg))
            {
                return IsAllowLockExtensionImpl(sLId);
            }
            return false;
        }

        public bool IsAllowFloatDown(Guid sLId)
        {
            string errMsg = string.Empty;
            if (HasWorkflowPermission(sLId, E_AutoRateLockAction.FloatDown, out errMsg))
            {
                return IsAllowFloatDownImpl(sLId);
            }
            return false;
        }

        public bool IsAllowRelock(Guid sLId)
        {
            string errMsg = string.Empty;
            if (HasWorkflowPermission(sLId, E_AutoRateLockAction.ReLock, out errMsg))
            {
                return IsAllowRelockImpl(sLId);
            }
            return false;
        }

        protected abstract bool IsAllowLockExtensionImpl(Guid sLId);
        protected abstract IEnumerable<CodeBasedLockExtensionResultItem> GetLockExtensionResult(Guid sLId
            , UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult);

        protected abstract bool IsAllowFloatDownImpl(Guid sLId);
        protected abstract IEnumerable<CodeBasedFloatDownResultItem> GetFloatDownResult(Guid sLId
            , UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult);

        protected abstract bool IsAllowRelockImpl(Guid sLId);
        protected abstract IEnumerable<CodeBasedRelockResultItem> GetRelockResult(Guid sLId
            , UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult);

        public abstract List<ReLockFee> GetReLockFeeTable(Guid sLId);

        /// <summary>
        /// Returns settings for running historical pricing.
        /// </summary>
        /// <returns>Settings for running historical pricing.</returns>
        public virtual HybridLoanProgramSetOption GetHistoricalPricingSettings()
        {
            HybridLoanProgramSetOption options = new HybridLoanProgramSetOption();
            options.ApplicablePolicies = E_LoanProgramSetPricingT.Historical;
            options.RateOptions = E_LoanProgramSetPricingT.Historical;

            return options;
        }

        public AutoRateLock.AutoRateLockResult ExecuteCommand(Guid sLId, E_AutoRateLockAction action, 
            string encryptedCommand
            , UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            string[] parts = encryptedCommand.Split(':');

            string reason = string.Empty;
            if (parts.Length > 1)
            {
                reason = parts[1];
            }
            AbstractCodeBasedPolicyResultItem actionRequest = AbstractCodeBasedPolicyResultItem.Create(action, parts[0],
                sLId, principal.UserId);

            switch (action)
            {
                case E_AutoRateLockAction.Extend:
                    return Extend(sLId, principal, (CodeBasedLockExtensionResultItem) actionRequest, reason);
                case E_AutoRateLockAction.FloatDown:
                    return FloatDown(sLId, principal, (CodeBasedFloatDownResultItem) actionRequest);
                case E_AutoRateLockAction.ReLock:
                    return Relock(sLId, principal, (CodeBasedRelockResultItem) actionRequest, reason);
                default:
                    throw new UnhandledEnumException(action);
            }
        }

        private AutoRateLock.AutoRateLockResult Extend(Guid sLId, AbstractUserPrincipal principal, CodeBasedLockExtensionResultItem actionRequest, string reason)
        {
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            PricingAdjustment newAdjustment = new PricingAdjustment()
            {
                Description = "LOCK EXTENSION - " + actionRequest.Days + " DAYS",
                Fee = actionRequest.Fee.ToString(),
                IsHidden = false,
                IsLenderAdjustment = true,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = "0.000%",
                TeaserRate = "0.000%",
                PricingAdjustmentT = E_PricingAdjustmentT.LockExtension,
                ExtensionDays = actionRequest.Days,
                IsPersist = true
            };

            dataLoan.ExtendRateLock(principal.DisplayName, reason, actionRequest.Days, newAdjustment);

            dataLoan.Save();

            PriceGroup priceGroup = dataLoan.sPriceGroup;

            bool displayPmlFeeIn100Format = priceGroup.DisplayPmlFeeIn100Format;

            AutoRateLock.SendNotificationEmail(E_AutoRateLockAction.Extend, dataLoan);

            AutoRateLock.AutoRateLockResult result = new AutoRateLock.AutoRateLockResult()
            {
                ExpirationDate = dataLoan.sRLckdExpiredD_rep,
                Price = CApplicantRateOption.PointFeeInResult(displayPmlFeeIn100Format, dataLoan.sBrokComp1Pc_rep),
                TotalEvents = dataLoan.sTotalLockExtensions_rep
            };
            return result;
        }

        private AutoRateLock.AutoRateLockResult FloatDown(Guid sLId, AbstractUserPrincipal principal, CodeBasedFloatDownResultItem actionRequest)
        {
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            PriceGroup priceGroup = dataLoan.sPriceGroup;
            bool displayPmlFeeIn100Format = priceGroup.DisplayPmlFeeIn100Format;

            List<PricingAdjustment> adjustmentList = new List<PricingAdjustment>();

            // 1/14/2014 dd - Get all current adjustments on loan file
            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.PricingAdjustmentT == E_PricingAdjustmentT.RolldownFee)
                {
                    continue;
                }
                adjustmentList.Add(o);
            }
            adjustmentList.Add(new PricingAdjustment()
            {
                Description = "FLOAT DOWN FEE",
                Fee = actionRequest.RolldownFee.ToString(),
                IsHidden = false,
                IsLenderAdjustment = true,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = "0.000%",
                TeaserRate = "0.000%",
                PricingAdjustmentT = E_PricingAdjustmentT.RolldownFee,
                IsPersist = true
            });

            dataLoan.FloatDownRate(principal.LoginNm, actionRequest.NewRate, actionRequest.FinalPoint, adjustmentList);

            dataLoan.Save();

            AutoRateLock.SendNotificationEmail(E_AutoRateLockAction.FloatDown, dataLoan);

            AutoRateLock.AutoRateLockResult result = new AutoRateLock.AutoRateLockResult()
            {
                ExpirationDate = dataLoan.sRLckdExpiredD_rep,
                Price = CApplicantRateOption.PointFeeInResult(displayPmlFeeIn100Format, dataLoan.sBrokComp1Pc_rep),
                TotalEvents = dataLoan.sTotalRateLockFloatDowns_rep
            };
            return result;
        }

        private AutoRateLock.AutoRateLockResult Relock(Guid sLId, AbstractUserPrincipal principal, CodeBasedRelockResultItem actionRequest, string reason)
        {
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            List<PricingAdjustment> adjustmentList = new List<PricingAdjustment>();
            // 1/14/2014 dd - Get all current adjustments on loan file
            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.PricingAdjustmentT == E_PricingAdjustmentT.RelockFee)
                {
                    continue;
                }
                adjustmentList.Add(o);
            }

            if (actionRequest.RelockFee != 0)
            {
                adjustmentList.Add(new PricingAdjustment()
                {
                    Description = "RE-LOCK FEE",
                    Fee = actionRequest.RelockFee.ToString(),
                    IsHidden = false,
                    IsLenderAdjustment = true,
                    IsSRPAdjustment = false,
                    Margin = "0.000%",
                    Rate = "0.000%",
                    TeaserRate = "0.000%",
                    PricingAdjustmentT = E_PricingAdjustmentT.RelockFee,
                    IsPersist = true
                });
            }
            
            dataLoan.ReLockRate(principal.DisplayName, reason, actionRequest.LockPeriod, actionRequest.Rate, actionRequest.FinalPointWithRelockFee, adjustmentList);

            dataLoan.Save();

            AutoRateLock.SendNotificationEmail(E_AutoRateLockAction.ReLock, dataLoan);

            AutoRateLock.AutoRateLockResult result = new AutoRateLock.AutoRateLockResult()
            {
                ExpirationDate = dataLoan.sRLckdExpiredD_rep,
                Price = CApplicantRateOption.PointFeeInResult(dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, dataLoan.sBrokComp1Pc_rep),
                TotalEvents = dataLoan.sTotalRateReLocks_rep,
                Rate = dataLoan.sNoteIR_rep
            };

            return result;
        }

        public List<string[]> BuildResultTable(Guid sLId, E_AutoRateLockAction action,
            UnderwritingResultItem currentPricingResult,
            UnderwritingResultItem historicalPricingResult)
        {
            switch (action)
            {
                case E_AutoRateLockAction.Extend:
                    return BuildResultTable_Extend(sLId, currentPricingResult, historicalPricingResult);
                case E_AutoRateLockAction.FloatDown:
                    return BuildResultTable_FloatDown(sLId, currentPricingResult, historicalPricingResult);
                case E_AutoRateLockAction.ReLock:
                    return BuildResultTable_Relock(sLId, currentPricingResult, historicalPricingResult);


                default:
                    throw new UnhandledEnumException(action);
            }
        }

        protected virtual string[] ResultHeaderRow_Extend(Guid sLId)
        {
            return new string[] { "Available Extension", "Extension Fee", "New Final Points", "New Expiration Date", "" };
        }

        private List<string[]> BuildResultTable_Extend(Guid sLId, UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult)
        {
            List<string[]> table = new List<string[]>();

            string[] headerRow = ResultHeaderRow_Extend(sLId);

            table.Add(headerRow);


            var result = GetLockExtensionResult(sLId, currentPricingResult, historicalPricingResult);

            foreach (var item in result)
            {
                table.Add(new string[] { item.Days.ToString(), ToRate(item.Fee), ToPointRate(item.FinalPoint), item.NewExpirationDate, "extend:" + item.EncryptedData });
            }
            return table;

        }

        protected virtual string[] ResultHeaderRow_FloatDown(Guid sLId)
        {
            return new string[] { 
                "New Rate"
                ,"Previous Points"
                , "New Market Points"
                , "Market Points Improvement"
                , "Float Down Fee"
                , "New Final Points"
                , "" }; 
        }
        private List<string[]> BuildResultTable_FloatDown(Guid sLId, UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult)
        {
            List<string[]> table = new List<string[]>();

            string[] headerRow = ResultHeaderRow_FloatDown(sLId);

            table.Add(headerRow);


            var result = GetFloatDownResult(sLId, currentPricingResult, historicalPricingResult);

            foreach (var item in result)
            {
                string rolldownFee = "N/A";
                string finalPoint = "N/A";

                if (item.RolldownFee != decimal.MinValue)
                {
                    rolldownFee = ToRate(item.RolldownFee);
                }
                if (item.FinalPoint != decimal.MinValue)
                {
                    finalPoint = ToPointRate(item.FinalPoint);
                }

                table.Add(new string[] { ToRate(item.NewRate), ToPointRate(item.OriginalPoint), 
                    ToPointRate(item.NewPoint), 
                    ToRate(item.MarketImprovementPoint), 
                    rolldownFee,
                    finalPoint, item.FinalPoint == decimal.MinValue ? "" : "float down:" + item.EncryptedData});
            }
            return table;

        }


        protected virtual string[] ResultHeaderRow_Relock(Guid sLId)
        {
            return new string[] { 
                "Rate"
                ,"Previous Points"
                , "Market Points"
                , "Re-Lock Fee"
                , "Re-Lock Points"
                , "" };
        }
        private List<string[]> BuildResultTable_Relock(Guid sLId, UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult)
        {


            List<string[]> table = new List<string[]>();

            string[] headerRow = ResultHeaderRow_Relock(sLId);

            table.Add(headerRow);


            var result = GetRelockResult(sLId, currentPricingResult, historicalPricingResult);

            foreach (var item in result)
            {
                table.Add(new string[] { ToRate(item.Rate), ToPointRate(item.OriginalPoint), ToPointRate(item.NewPoint), ToRate(item.RelockFee), ToPointRate(item.FinalPointWithRelockFee), 
                "relock:" + item.EncryptedData});
            }
            return table;
        }

        public LoanProgramByInvestorSet GetProgramsToRun(E_AutoRateLockAction action, Guid sLId, out Guid sProdLpePriceGroupId)
        {
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            sProdLpePriceGroupId = dataLoan.sProdLpePriceGroupId;

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            lpSet.Add(dataLoan.sLpTemplateId, "N/A", null);

            return lpSet;
            
        }

        protected string ToRate(decimal input)
        {
            return input.ToString();
            //return m_dataLoan.m_convertLos.ToRateString(input);
        }

        protected string ToPointRate(decimal input)
        {
            return input.ToString();
            //return CApplicantRateOption.PointFeeInResult(
            //    sPriceGroup.DisplayPmlFeeIn100Format
            //    , m_dataLoan.m_convertLos.ToRateString(input));
        }

        #region Helper Methods
        protected CApplicantPriceXml FindById(UnderwritingResultItem result, Guid lLpTemplateId)
        {
            if (result == null || result.GetApplicantPriceList() == null)
            {
                throw CBaseException.GenericException(lLpTemplateId + " is not found in result list.");
            }

            foreach (var o in result.GetApplicantPriceList())
            {
                if (o.lLpTemplateId == lLpTemplateId)
                {
                    return o;
                }
            }

            throw CBaseException.GenericException(lLpTemplateId + " is not found in result list.");
        }

        protected CApplicantRateOption FindRateOptionByRate(CApplicantPriceXml applicantPrice, decimal sNoteIR)
        {
            foreach (var o in applicantPrice.ApplicantRateOptions)
            {
                if (o.Rate == sNoteIR)
                {
                    return o;
                }
            }
            return null;
        }

        protected decimal SafeDecimal(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }
            str = str.Replace("%", "");
            decimal v = 0;

            decimal.TryParse(str, out v);

            return v;
        }

        protected bool IsDateExceed(CDateTime dt, int numberOfDays)
        {
            if (dt == null)
            {
                return false;
            }

            if (dt.IsValid == false)
            {
                return false;
            }

            try
            {
                return dt.DateTimeForComputation.Date.AddDays(numberOfDays) < DateTime.Today;
                
            }
            catch (CBaseException)
            {
                return false;
            }
        }
        #endregion
    }
}
