﻿namespace LendersOffice.ObjLib.LockPolicies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;
    using PriceGroups;

    /// <summary>
    /// Data class that holds a row of auto rate lock extend data.
    /// </summary>
    public class AutoRateLockExtendData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutoRateLockExtendData"/> class.
        /// </summary>
        /// <param name="extensionLength">Extension length.</param>
        /// <param name="extensionFee">Extension fee (price).</param>
        /// <param name="newFinalPoints">New final price.</param>
        /// <param name="newExpirationDate">New rate lock expiration date.</param>
        /// <param name="calendarDays">Actual calendar days the rate lock is being extended.</param>
        /// <param name="showCalendarDays">Boolean that determines whether to show or hdie calendar days.</param>
        public AutoRateLockExtendData(int extensionLength, decimal extensionFee, decimal newFinalPoints, CDateTime newExpirationDate, int calendarDays, bool showCalendarDays)
        {
            this.ExtensionLength = extensionLength;
            this.ExtensionFee = extensionFee;
            this.NewFinalPoints = newFinalPoints;
            this.NewExpirationDate = newExpirationDate;
            this.CalendarDays = calendarDays;
            this.ShowCalendarDays = showCalendarDays;
            this.IsActionEnabled = true;
        }

        /// <summary>
        /// Gets or sets the amount of days the rate lock will be extended.
        /// </summary>
        public int ExtensionLength { get; set; }

        /// <summary>
        /// Gets or sets the extension fee (price).
        /// </summary>
        public decimal ExtensionFee { get; set; }

        /// <summary>
        /// Gets or sets the new final price.
        /// </summary>
        public decimal NewFinalPoints { get; set; }

        /// <summary>
        /// Gets or sets the new expiration date.
        /// </summary>
        public CDateTime NewExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the number of actual calendar days that the rate lock will be extended.
        /// </summary>
        public int CalendarDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether calendar days will be visible.
        /// </summary>
        public bool ShowCalendarDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the action (extend) is enabled.
        /// </summary>
        public bool IsActionEnabled { get; set; }

        /// <summary>
        /// Gets or sets the extend action string.
        /// </summary>
        public string ExtendActionStr { get; set; }

        /// <summary>
        /// Gets the absolute value number of days between extension days and calendar days.
        /// </summary>
        public int DaysBetweenExtensionAndCalendarDays
        {
            get
            {
                return Math.Abs(this.ExtensionLength - this.CalendarDays);
            }
        }

        /// <summary>
        /// Gets the auto rate lock extend data in the string form that can be outputted.
        /// </summary>
        /// <param name="convertLos">LosConvert object.</param>
        /// <param name="priceGroup">Current price group.</param>
        /// <returns>String representation of the row that can be used to build a row in the table.</returns>
        public string[] GetStringTableRow(LosConvert convertLos, PriceGroup priceGroup)
        {
            string[] tableRow = null;            

            if (this.ShowCalendarDays)
            {
                tableRow = new string[6];

                tableRow.SetValue(this.CalendarDays + " days", 4);
                tableRow.SetValue(this.IsActionEnabled ? "extend:" + this.ExtensionLength + ":" + convertLos.ToRateString(this.ExtensionFee) + ":" + convertLos.ToRateString(this.NewFinalPoints) + ":" + this.NewExpirationDate + ":" + this.CalendarDays + " days" : "disabled", 5);
            }
            else
            {
                tableRow = new string[5];

                tableRow.SetValue("extend:" + this.ExtensionLength + ":" + convertLos.ToRateString(this.ExtensionFee) + ":" + convertLos.ToRateString(this.NewFinalPoints) + ":" + this.NewExpirationDate, 4);
            }

            tableRow.SetValue(this.ExtensionLength.ToString() + " days", 0);
            tableRow.SetValue(convertLos.ToRateString(this.ExtensionFee), 1);
            tableRow.SetValue(CApplicantRateOption.PointFeeInResult(priceGroup.DisplayPmlFeeIn100Format, convertLos.ToRateString(this.NewFinalPoints)), 2);
            tableRow.SetValue(this.NewExpirationDate.ToString(convertLos), 3);

            return tableRow;
        }
    }
}
