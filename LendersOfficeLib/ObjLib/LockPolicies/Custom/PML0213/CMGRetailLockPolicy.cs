﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.RatePrice;
using LendersOffice.DistributeUnderwriting;

namespace LendersOffice.ObjLib.LockPolicies.Custom.PML0213
{
    public class CMGRetailLockPolicy : AbstractCodeBasedLockPolicy
    {
        private decimal RelockFee = 0.125M;

        private CPageData GetPageData(Guid sLId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(CMGRetailLockPolicy));
            dataLoan.InitLoad();

            return dataLoan;
        }

        private bool IsIncludeOriginatorComp(CPageData dataLoan)
        {

            return (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                     && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                     && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);

        }

        #region Lock Extension
        protected override bool IsAllowLockExtensionImpl(Guid sLId)
        {
            CPageData dataLoan = GetPageData(sLId);
            if (dataLoan.sStatusT == E_sStatusT.Loan_Canceled)
            {
                return false;
            }
            if (IsDateExceed(dataLoan.sRLckdExpiredD, 0))
            {
                // 6/1/2014 dd - Lock Expired.
                return false;
            }
            if (dataLoan.sTotalDaysLockExtended >= 30)
            {
                return false;
            }

            return true;
        }

        protected override IEnumerable<CodeBasedLockExtensionResultItem> GetLockExtensionResult(Guid sLId
            , UnderwritingResultItem currentPricingResult
            , UnderwritingResultItem historicalPricingResult)
        {
            List<CodeBasedLockExtensionResultItem> resultList = new List<CodeBasedLockExtensionResultItem>();

            CPageData dataLoan = GetPageData(sLId);

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            // 2/27/2014 - dd - OPM 170981 - New Retail Lock Policy.
            // Extension is 5 days @ .125 fee.
            // Max is 30 days.
            int totalDaysOfExtension = dataLoan.sTotalDaysLockExtended;

            if (totalDaysOfExtension <= 25)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 5, .125M));
            }
            if (totalDaysOfExtension <= 20)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 10, .250M));
            }
            if (totalDaysOfExtension <= 15)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 15, .375M));
            }
            if (totalDaysOfExtension <= 10)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 20, .500M));
            }
            if (totalDaysOfExtension <= 5)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 25, .625M));
            }
            if (totalDaysOfExtension == 0)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(sLId, userId, 30, .750M));
            }

            decimal lenderComp = 0;

            if (IsIncludeOriginatorComp(dataLoan))
            {
                lenderComp = dataLoan.sOriginatorCompPoint;
            }
            foreach (var o in resultList)
            {
                o.SetFinalData(dataLoan.CalculateRateLockExtensionExpirationDate(dataLoan.sRLckdExpiredD, o.Days),
                     dataLoan.sBrokComp1Pc + o.Fee + lenderComp, dataLoan.m_convertLos);
            }
            return resultList;
        }

        #endregion

        #region Float Down
        protected override bool IsAllowFloatDownImpl(Guid sLId)
        {
            CPageData dataLoan = GetPageData(sLId);

            if (dataLoan.sStatusT == E_sStatusT.Loan_Suspended || dataLoan.sStatusT == E_sStatusT.Loan_Canceled)
            {
                return false;
            }

            if (IsDateExceed(dataLoan.sRLckdExpiredD, 0))
            {
                // 6/1/2014 dd - Lock Expired.
                return false;
            }

            if (dataLoan.sFundD.IsValid)
            {
                // 6/1/2014 dd - Has Funded Date.
                return false;
            }

            if (dataLoan.sLpCustomCode1.Equals("True Jumbo", StringComparison.OrdinalIgnoreCase))
            {
                // 6/1/2014 dd - True Jumbo is not allow to roll down.
                return false;
            }

            if (dataLoan.sTotalRateLockFloatDowns > 0)
            {
                return false;
            }

            return true;
        }

        protected override IEnumerable<CodeBasedFloatDownResultItem> GetFloatDownResult(Guid sLId
    , UnderwritingResultItem currentPricingResult
    , UnderwritingResultItem historicalPricingResult)
        {
            // 2/27/2014 dd - OPM 170981 - Updated CMG Retail Policy
            //     Renegotiated price will be .625 worse than current day price.

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            List<CodeBasedFloatDownResultItem> resultList = new List<CodeBasedFloatDownResultItem>();

            decimal floatDownMinPointImprovement = .625M; // TODO: Pull From Lock Policy
            
            CPageData dataLoan = GetPageData(sLId);

            decimal currentRate = dataLoan.sNoteIR;

            CApplicantPriceXml currentPrice = FindById(currentPricingResult, dataLoan.sLpTemplateId);
            CApplicantPriceXml historicalPrice = FindById(historicalPricingResult, dataLoan.sLpTemplateId);

            decimal totalAdjustmentFee = 0; // Need to use Per-Rate Option + Current Adjustment.

            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.IsHidden)
                {
                    continue; // Skip hidden adjustment.
                }
                if (o.PricingAdjustmentT == E_PricingAdjustmentT.RolldownFee)
                {
                    continue; // Skip Previous Rolldown Fee
                }
                totalAdjustmentFee += SafeDecimal(o.Fee);
            }
            Dictionary<decimal, CApplicantRateOption> historicalPriceDictionary = new Dictionary<decimal, CApplicantRateOption>();
            foreach (var o in historicalPrice.ApplicantRateOptions)
            {
                if (o.IsDisqualified == false)
                {
                    historicalPriceDictionary.Add(o.Rate, o);
                }
            }
            // foreach var rateOption in Result.
            // 
            foreach (var rateOption in currentPrice.ApplicantRateOptions)
            {
                if (rateOption.IsDisqualified)
                {
                    continue;
                }
                if (currentRate < rateOption.Rate)
                {
                    // 5/30/2014 dd - Filter out higher rate
                    continue;
                }

                CApplicantRateOption originalRateOption = null;

                if (historicalPriceDictionary.TryGetValue(rateOption.Rate, out originalRateOption) == false)
                {
                    // 1/14/2014 dd - Could not find this rate option from historical pricing. Skip
                    continue;
                }
                decimal originalBasePoint = originalRateOption.OriginatorBasePoint;
                decimal currentBasePoint = rateOption.OriginatorBasePoint; // To Test: 1 for market worst, - 1 for market improvement.


                decimal marketChange = originalBasePoint - currentBasePoint;
                decimal finalBasePoint = decimal.MinValue;

                if (marketChange >= floatDownMinPointImprovement)
                {
                    finalBasePoint = currentBasePoint + floatDownMinPointImprovement;
                }

                resultList.Add(new CodeBasedFloatDownResultItem(sLId, userId, rateOption.Rate, originalBasePoint, currentBasePoint, totalAdjustmentFee, finalBasePoint));
            }

            return resultList;
        }
        #endregion

        #region Re-Lock
        protected override bool IsAllowRelockImpl(Guid sLId)
        {
            CPageData dataLoan = GetPageData(sLId);

            if (IsDateExceed(dataLoan.sRLckdExpiredD, 30))
            {
                return false;
            }

            if (dataLoan.sFundD.IsValid)
            {
                return false;
            }

            return true;
        }

        protected override IEnumerable<CodeBasedRelockResultItem> GetRelockResult(Guid sLId, UnderwritingResultItem currentPricingResult, UnderwritingResultItem historicalPricingResult)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            CPageData dataLoan = GetPageData(sLId);
            decimal currentRate = dataLoan.sNoteIR;

            CApplicantPriceXml currentPrice = FindById(currentPricingResult, dataLoan.sLpTemplateId);
            CApplicantPriceXml historicalPrice = FindById(historicalPricingResult, dataLoan.sLpTemplateId);

            List<CodeBasedRelockResultItem> resultList = new List<CodeBasedRelockResultItem>();

            Dictionary<decimal, CApplicantRateOption> historicalPriceDictionary = new Dictionary<decimal, CApplicantRateOption>();
            foreach (var o in historicalPrice.ApplicantRateOptions)
            {
                if (o.IsDisqualified == false)
                {
                    historicalPriceDictionary.Add(o.Rate, o);
                }
            }
            decimal totalAdjustmentFee = 0;
            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.IsHidden)
                {
                    continue; // Skip hidden adjustment.
                }
                if (o.PricingAdjustmentT == E_PricingAdjustmentT.RolldownFee)
                {
                    continue; // For Retail Relock Rolldown goes away
                }
                totalAdjustmentFee += SafeDecimal(o.Fee);
            }

            decimal lenderComp = 0;

            if (IsIncludeOriginatorComp(dataLoan))
            {
                lenderComp = dataLoan.sOriginatorCompPoint;
            }

            foreach (var rateOption in currentPrice.ApplicantRateOptions)
            {
                CApplicantRateOption originalRateOption = null;

                if (historicalPriceDictionary.TryGetValue(rateOption.Rate, out originalRateOption) == false)
                {
                    // 1/14/2014 dd - Could not find this rate option from historical pricing. Skip
                    continue;
                }

                decimal originalBasePoint = originalRateOption.OriginatorBasePoint;
                decimal currentBasePoint = rateOption.OriginatorBasePoint; // To Test: 1 for market worst, - 1 for market improvement.

                decimal finalBasePoint = originalBasePoint > currentBasePoint ? originalBasePoint : currentBasePoint; // Use worse price

                resultList.Add(new CodeBasedRelockResultItem(sLId, userId, currentPrice.lLpTemplateId, rateOption.Rate, originalBasePoint,
                    currentBasePoint, totalAdjustmentFee, finalBasePoint + lenderComp, RelockFee));


            }
            return resultList;
        }
        public override List<ReLockFee> GetReLockFeeTable(Guid sLId)
        {
            List<ReLockFee> list = new List<ReLockFee>();

            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 21 });
            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 30 });
            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 45 });

            return list;
        }
        #endregion

    }

}
