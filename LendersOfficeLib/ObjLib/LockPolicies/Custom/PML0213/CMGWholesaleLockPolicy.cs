﻿// <copyright file="CMGWholesaleLockPolicy.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   6/1/2014 10:49:26 AM 
// </summary>
namespace LendersOffice.ObjLib.LockPolicies.Custom.PML0213
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// CMG Wholesale's Custom Lock Policy.
    /// </summary>
    public class CMGWholesaleLockPolicy : AbstractCodeBasedLockPolicy
    {
        /// <summary>
        /// Relock fee for wholesale.
        /// </summary>
        private const decimal RelockFee = 0.125M;

        /// <summary>
        /// List of relock fee per lock period.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <returns>Relock fee per lock period.</returns>
        public override List<ReLockFee> GetReLockFeeTable(Guid loanId)
        {
            List<ReLockFee> list = new List<ReLockFee>();

            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 21 });
            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 30 });
            list.Add(new ReLockFee() { Fee = RelockFee, PeriodEndDay = 45 });

            return list;
        }

        /// <summary>
        /// Check whether loan is eligible for lock extension.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <returns>Whether loan is eligible for lock extension.</returns>
        protected override bool IsAllowLockExtensionImpl(Guid loanId)
        {
            CPageData dataLoan = this.GetPageData(loanId);

            if (dataLoan.sStatusT == E_sStatusT.Loan_Canceled)
            {
                return false;
            }

            if (this.IsDateExceed(dataLoan.sRLckdExpiredD, 0))
            {
                // 6/1/2014 dd - Lock Expired.
                return false;
            }

            if (dataLoan.sTotalDaysLockExtended >= 30)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calculate the fee for possible extension.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <param name="currentPricingResult">Today pricing.</param>
        /// <param name="historicalPricingResult">Historical pricing.</param>
        /// <returns>List of available extensions.</returns>
        protected override IEnumerable<CodeBasedLockExtensionResultItem> GetLockExtensionResult(Guid loanId, UnderwritingResultItem currentPricingResult, UnderwritingResultItem historicalPricingResult)
        {
            List<CodeBasedLockExtensionResultItem> resultList = new List<CodeBasedLockExtensionResultItem>();

            CPageData dataLoan = this.GetPageData(loanId);

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            // Allow 1 free 5 days extension when loan in DOCS-ORDERED, DOCS-DRAWN, DOCS-OUT, DOCS-BACK.
            // Extension is 5 days @ .125 fee.
            // Max is 30 days.
            int totalDaysOfExtension = dataLoan.sTotalDaysLockExtended;

            if (totalDaysOfExtension <= 25)
            {
                E_sStatusT[] docStatusList = 
                {
                     E_sStatusT.Loan_DocsOrdered,
                     E_sStatusT.Loan_DocsDrawn,
                     E_sStatusT.Loan_DocsBack,
                     E_sStatusT.Loan_Docs
                };

                if (docStatusList.Contains(dataLoan.sStatusT))
                {
                    // Check to see if user already use the free extension.
                    bool allowFeeExtension = true;

                    foreach (var o in dataLoan.sBrokerLockAdjustments)
                    {
                        if (o.PricingAdjustmentT == E_PricingAdjustmentT.LockExtension)
                        {
                            if (this.SafeDecimal(o.Fee) == 0)
                            {
                                allowFeeExtension = false;
                                break;
                            }
                        }
                    }

                    if (allowFeeExtension)
                    {
                        resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 5, 0));
                    }
                }

                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 5, .125M));
            }

            if (totalDaysOfExtension <= 20)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 10, .250M));
            }

            if (totalDaysOfExtension <= 15)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 15, .375M));
            }

            if (totalDaysOfExtension <= 10)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 20, .500M));
            }

            if (totalDaysOfExtension <= 5)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 25, .625M));
            }

            if (totalDaysOfExtension == 0)
            {
                resultList.Add(new CodeBasedLockExtensionResultItem(loanId, userId, 30, .750M));
            }

            decimal lenderComp = 0;

            if (this.IsIncludeOriginatorComp(dataLoan))
            {
                lenderComp = dataLoan.sOriginatorCompPoint;
            }

            foreach (var o in resultList)
            {
                o.SetFinalData(dataLoan.CalculateRateLockExtensionExpirationDate(dataLoan.sRLckdExpiredD, o.Days), dataLoan.sBrokComp1Pc + o.Fee + lenderComp, dataLoan.m_convertLos);
            }

            return resultList;
        }

        /// <summary>
        /// Check whether loan is eligible for float down.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <returns>Whether loan is eligible for float down.</returns>
        protected override bool IsAllowFloatDownImpl(Guid loanId)
        {
            CPageData dataLoan = this.GetPageData(loanId);

            if (dataLoan.sStatusT == E_sStatusT.Loan_Suspended || dataLoan.sStatusT == E_sStatusT.Loan_Canceled)
            {
                return false;
            }

            if (this.IsDateExceed(dataLoan.sRLckdExpiredD, 0))
            {
                // 6/1/2014 dd - Lock Expired.
                return false;
            }

            if (dataLoan.sFundD.IsValid)
            {
                // 6/1/2014 dd - Has Funded Date.
                return false;
            }

            if (dataLoan.sClearToCloseD.IsValid)
            {
                // 6/1/2014 dd - Has Clear To Close Date
                return false;
            }

            if (dataLoan.sLpCustomCode1.Equals("True Jumbo", StringComparison.OrdinalIgnoreCase))
            {
                // 6/1/2014 dd - True Jumbo is not allow to roll down.
                return false;
            }

            if (dataLoan.sTotalRateLockFloatDowns > 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// List available options for float down.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <param name="currentPricingResult">Today pricing.</param>
        /// <param name="historicalPricingResult">Historical pricing.</param>
        /// <returns>Options for float down.</returns>
        protected override IEnumerable<CodeBasedFloatDownResultItem> GetFloatDownResult(Guid loanId, UnderwritingResultItem currentPricingResult, UnderwritingResultItem historicalPricingResult)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            List<CodeBasedFloatDownResultItem> resultList = new List<CodeBasedFloatDownResultItem>();

            decimal floatDownMinPointImprovement = .625M;

            CPageData dataLoan = this.GetPageData(loanId);

            decimal currentRate = dataLoan.sNoteIR;

            CApplicantPriceXml currentPrice = FindById(currentPricingResult, dataLoan.sLpTemplateId);
            CApplicantPriceXml historicalPrice = FindById(historicalPricingResult, dataLoan.sLpTemplateId);

            decimal totalAdjustmentFee = 0; // Need to use Per-Rate Option + Current Adjustment.

            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.IsHidden)
                {
                    continue; // Skip hidden adjustment.
                }

                if (o.PricingAdjustmentT == E_PricingAdjustmentT.RolldownFee)
                {
                    continue; // Skip Previous Rolldown Fee
                }

                totalAdjustmentFee += this.SafeDecimal(o.Fee);
            }

            Dictionary<decimal, CApplicantRateOption> historicalPriceDictionary = new Dictionary<decimal, CApplicantRateOption>();
            foreach (var o in historicalPrice.ApplicantRateOptions)
            {
                if (o.IsDisqualified == false)
                {
                    historicalPriceDictionary.Add(o.Rate, o);
                }
            }

            foreach (var rateOption in currentPrice.ApplicantRateOptions)
            {
                if (rateOption.IsDisqualified)
                {
                    continue;
                }

                if (currentRate < rateOption.Rate)
                {
                    // 5/30/2014 dd - Filter out higher rate
                    continue;
                }

                CApplicantRateOption originalRateOption = null;

                if (historicalPriceDictionary.TryGetValue(rateOption.Rate, out originalRateOption) == false)
                {
                    // 1/14/2014 dd - Could not find this rate option from historical pricing. Skip
                    continue;
                }

                decimal originalBasePoint = originalRateOption.OriginatorBasePoint;
                decimal currentBasePoint = rateOption.OriginatorBasePoint; // To Test: 1 for market worst, - 1 for market improvement.

                decimal marketChange = originalBasePoint - currentBasePoint;
                decimal finalBasePoint = decimal.MinValue;

                if (marketChange >= floatDownMinPointImprovement)
                {
                    finalBasePoint = currentBasePoint + floatDownMinPointImprovement;
                }

                resultList.Add(new CodeBasedFloatDownResultItem(loanId, userId, rateOption.Rate, originalBasePoint, currentBasePoint, totalAdjustmentFee, finalBasePoint));
            }

            return resultList;
        }

        /// <summary>
        /// Check whether loan is available for relock.
        /// </summary>
        /// <param name="loanId">Loan id to check.</param>
        /// <returns>Whether loan is available for relock.</returns>
        protected override bool IsAllowRelockImpl(Guid loanId)
        {
            CPageData dataLoan = this.GetPageData(loanId);

            if (this.IsDateExceed(dataLoan.sRLckdExpiredD, 30))
            {
                return false;
            }

            if (dataLoan.sFundD.IsValid)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// List of available price for relock.
        /// </summary>
        /// <param name="loanId">Loan to relock.</param>
        /// <param name="currentPricingResult">Today pricing.</param>
        /// <param name="historicalPricingResult">Historical pricing.</param>
        /// <returns>Available price for relock.</returns>
        protected override IEnumerable<CodeBasedRelockResultItem> GetRelockResult(Guid loanId, UnderwritingResultItem currentPricingResult, UnderwritingResultItem historicalPricingResult)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid userId = principal.UserId;

            CPageData dataLoan = this.GetPageData(loanId);
            decimal currentRate = dataLoan.sNoteIR;

            CApplicantPriceXml currentPrice = FindById(currentPricingResult, dataLoan.sLpTemplateId);
            CApplicantPriceXml historicalPrice = FindById(historicalPricingResult, dataLoan.sLpTemplateId);

            List<CodeBasedRelockResultItem> resultList = new List<CodeBasedRelockResultItem>();

            Dictionary<decimal, CApplicantRateOption> historicalPriceDictionary = new Dictionary<decimal, CApplicantRateOption>();
            foreach (var o in historicalPrice.ApplicantRateOptions)
            {
                if (o.IsDisqualified == false)
                {
                    historicalPriceDictionary.Add(o.Rate, o);
                }
            }

            decimal totalAdjustmentFee = 0;
            foreach (var o in dataLoan.sBrokerLockAdjustments)
            {
                if (o.IsHidden)
                {
                    continue; // Skip hidden adjustment.
                }

                totalAdjustmentFee += this.SafeDecimal(o.Fee);
            }

            decimal lenderComp = 0;

            if (this.IsIncludeOriginatorComp(dataLoan))
            {
                lenderComp = dataLoan.sOriginatorCompPoint;
            }

            foreach (var rateOption in currentPrice.ApplicantRateOptions)
            {
                CApplicantRateOption originalRateOption = null;

                if (historicalPriceDictionary.TryGetValue(rateOption.Rate, out originalRateOption) == false)
                {
                    // 1/14/2014 dd - Could not find this rate option from historical pricing. Skip
                    continue;
                }

                decimal originalBasePoint = originalRateOption.OriginatorBasePoint;
                decimal currentBasePoint = rateOption.OriginatorBasePoint; // To Test: 1 for market worst, - 1 for market improvement.

                decimal finalBasePoint = originalBasePoint > currentBasePoint ? originalBasePoint : currentBasePoint; // Use worse price

                resultList.Add(new CodeBasedRelockResultItem(
                    loanId, 
                    userId, 
                    currentPrice.lLpTemplateId, 
                    rateOption.Rate, 
                    originalBasePoint,
                    currentBasePoint, 
                    totalAdjustmentFee, 
                    finalBasePoint + lenderComp, 
                    RelockFee));
            }

            return resultList;
        }

        /// <summary>
        /// Construct a data loan object.
        /// </summary>
        /// <param name="loanId">Loan id to construct.</param>
        /// <returns>A data loan object.</returns>
        private CPageData GetPageData(Guid loanId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CMGWholesaleLockPolicy));
            dataLoan.InitLoad();

            return dataLoan;
        }

        /// <summary>
        /// Check whether the loan is set to include originator comp in pricing result.
        /// </summary>
        /// <param name="dataLoan">A data loan object to check.</param>
        /// <returns>Whether the loan is set to include originator comp in pricing result.</returns>
        private bool IsIncludeOriginatorComp(CPageData dataLoan)
        {
            return dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                     && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                     && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
        }
    }
}