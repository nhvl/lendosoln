﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.ObjLib.LockPolicies
{
    public sealed class DaySetting
    {
        public bool IsOpenForBusiness { get; set; }
        public bool IsOpenForLocks { get; set; }

        public DayOfWeek Day { get; private  set; }

        public int StartHour { get; set; }
        public int EndHour { get; set; }

        public int StartMinute { get; set; }
        public int EndMinute { get; set; }

        public int StartHourIn12Format
        {
            get { return GetHourIn12Format(StartHour); }
        }

        public int EndHourIn12Format
        {
            get { return GetHourIn12Format(EndHour); }
        }

        public string DayShorthand
        {
            get
            {
                switch (Day)
                {
                    case DayOfWeek.Friday:
                        return "F";
                    case DayOfWeek.Monday:
                        return "M";
                    case DayOfWeek.Saturday:
                        return "Sa";
                    case DayOfWeek.Sunday:
                        return "Su";
                    case DayOfWeek.Thursday:
                        return "Th";
                    case DayOfWeek.Tuesday:
                        return "Tu";
                    case DayOfWeek.Wednesday:
                        return "W";
                    default:
                        throw new UnhandledEnumException(Day);
                }
            }
        }


        public DaySetting(DayOfWeek day)
        {
            Day = day;
        }

        public bool IsValid()
        {
            if (!IsOpenForLocks)
            {
                return true; 
            }


            DateTime startTime = new DateTime(2000, 1, 1, StartHour, StartMinute,0);
            DateTime endTime = new DateTime(2000, 1, 1, EndHour, EndMinute,0);

            return startTime  < endTime;
        }

        public DateTime StartTimeD
        {
            get { return new DateTime(1, 1, 1, StartHour, StartMinute, 0); }
        }

        public DateTime EndTimeD
        {
            get { return new DateTime(1, 1, 1, EndHour, EndMinute, 0); }
        }


        private int GetHourIn12Format(int hour)
        {
            if ( hour > 12 )
            {
                hour -= 12;
            }
            else if (hour == 0){
                hour = 12;
            }

            return hour;
        }
    }
}
