﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class AutoRateLockFloatDown : AutoRateLock
    {
        protected override WorkflowOperation WorkflowOperation
        {
            get { return WorkflowOperations.FloatDownLock; }
        }
        protected override Func<bool> ActionAllowed
        {
            get { return () => m_dataLoan.sIsRateLockFloatDownAllowed; }
        }

        protected override string ActionFriendlyName
        {
            get { return "Float Down"; }
        }

        public AutoRateLockFloatDown(Guid loanId)
            : base(loanId)
        {
        }

        protected override List<string[]> BuildResultTable()
        {
            if (ProgramResult == null || ProgramResult.Status != E_EvalStatus.Eval_Eligible)
            {
                throw new NonCriticalAutoRateLockException("No Pricing Results for program.");
            }

            decimal currentRate = m_dataLoan.sNoteIR;

            // If the cert we are looking at was rendered in Price mode
            // we want to do the calcuations in point mode regardless,
            // then convert it (if needed) for display
            bool convertPreviousPoint = m_dataLoan.sSubmittedRatePriceIsInPriceFormat;

            IDictionary<decimal, decimal> originalPrices = m_dataLoan.sSubmittedRatePrices;
            SortedDictionary<decimal, decimal> currentPrices = new SortedDictionary<decimal, decimal>();

            var headerRow = new string[] { 
                "New Rate"
                ,"Previous " + PointPriceStr
                , "New Market " + PointPriceStr
                , "Market " + PointPriceStr + "<br/>Improvement"
                , "Float Down<br/>" + PointPriceStr + " Fee"
                , "New Final " + PointPriceStr
                , "Action" };

            List<string[]> table = new List<string[]>();
            table.Add(headerRow);

            foreach (var newOption in ProgramResult.ApplicantRateOptions)
            {
                if ( newOption.IsDisqualified == false)
                    currentPrices.Add(newOption.Rate
                        , IncludeOriginatorComp ? newOption.PointIncludingOriginatorComp : newOption.Point_);
            }

            decimal totalLockFees = m_dataLoan.sTotalNonFloatLockFees;

            foreach (var ratePrice in originalPrices)
            {
                if (ratePrice.Key <= currentRate
                    && currentPrices.ContainsKey(ratePrice.Key))
                {
                    // Show rates that are <= the current one and are contained on both the original and new result
                    decimal newRate = ratePrice.Key;
                    decimal previousPoints = ( convertPreviousPoint ?  ConvertPointBase( originalPrices[newRate]) : originalPrices[newRate]) + totalLockFees;

                    decimal newMarketPoints = currentPrices[newRate] + totalLockFees;
                    decimal marketImprovement = previousPoints - newMarketPoints;
                    decimal floatDownPointFee = sLockPolicy.FloatDownFeeIsPercent ?
                        marketImprovement * sLockPolicy.FloatDownPercentFee / 100
                        : sLockPolicy.FloatDownPointFee;
                    decimal newFinalPoints = newMarketPoints + floatDownPointFee;

                    List<string> reasonsCannotFloat = GetReasonsCannotFloatDown(
                        marketImprovement, currentRate - newRate);

                    string actionString;
                    if (reasonsCannotFloat.Count != 0)
                    {
                        // Render cannot do it link.
                        actionString = "unavailable";
                        reasonsCannotFloat.ForEach(s => actionString += ":" + s);
                    }
                    else
                    {
                        actionString = "float down"
                            + ":" + ToRate(newRate)
                            + ":" + ToRate(newMarketPoints) // new point
                            + ":" + ToRate(newFinalPoints)
                            + ":" + ToRate(floatDownPointFee);
                    }

                    table.Add(new string[] {
                        ToRate(newRate)
                        , ToPointRate(previousPoints)
                        , ToPointRate(newMarketPoints)
                        , ToRate(marketImprovement)
                        , ToRate(floatDownPointFee)
                        , ToPointRate(newFinalPoints)
                        , actionString
                    });

                }
            }

            if (table.Count == 1) 
            {
                throw new NonCriticalAutoRateLockException("There are no float-down options currently available for this file.");
            }

            return table;
        }

        protected override AutoRateLockResult ExecuteActionImpl(string action, string arguments)
        {
            // Do not trust the request coming in from UI.
            // Make sure the option they chose can still be done with the same result.
            // If they sat in the UI too long, something (lock policy, loan data, pricing) could have changed.
            // Or if they attempt to hack the UI to improve the terms of their lock...

            string[] args = arguments.Split(':');
            if (args.Length < 4)
                throw new CBaseException(ErrorMessages.Generic, "Missing arguments");

            bool foundMatch = false;
            foreach (var option in ResultTable)
            {
                string[] optionArgs = option[6].Split(':');
                if (optionArgs.Length >= 5)
                {
                    if (optionArgs[1] == args[0]    // Rate
                        && optionArgs[2] == args[1] // New Point
                        && optionArgs[3] == args[2] // Final Point
                        && optionArgs[4] == args[3] // Fee
                        )
                    {
                        foundMatch = true;
                        break;
                    }
                }
            }

            if (foundMatch == false)
            {
                // The action being asked for is no longer allowed.
                throw new NonCriticalAutoRateLockException("Float down option requested is not available.");
            }
            decimal currentRate = m_dataLoan.sNoteIR;
            decimal currentPoint = IncludeOriginatorComp ? m_dataLoan.sBrokComp1Pc + m_dataLoan.sOriginatorCompPoint : m_dataLoan.sBrokComp1Pc;

            decimal rate = decimal.Parse(args[0].Replace("%","")); // new final rate
            decimal rateImprovement = rate - currentRate;
            decimal pointImprovement = decimal.Parse(args[1].Replace("%", "")) - currentPoint; // Difference between current point
            decimal point = decimal.Parse(args[2].Replace("%","")); // new final point (after fee)
            decimal fee = decimal.Parse(args[3].Replace("%", "")); // fee being paid to get there


            List<PricingAdjustment> adjustments = BuildAdjustmentList();

            adjustments.Add(new PricingAdjustment()
            {
                Description = "FLOAT DOWN TO MARKET",
                Fee = ToRate(pointImprovement),
                IsHidden = false,
                IsLenderAdjustment = false,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = ToRate(rateImprovement),
                TeaserRate = "0.000%",
                IsPersist = true
            });

            adjustments.Add( new PricingAdjustment()
            {
                Description = "FLOAT DOWN FEE",
                Fee = ToRate(fee),
                IsHidden = false,
                IsLenderAdjustment = true,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = "0.000%",
                TeaserRate = "0.000%",
                IsPersist = true
            });

            CPageData data = new CAutoFloatDownData(m_loanId);
            data.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
            data.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.

            if (IncludeOriginatorComp) point -= m_dataLoan.sOriginatorCompPoint;

            data.FloatDownRate(m_principal.DisplayName, rate, point, adjustments);

            data.Save();

            SendNotificationEmail(E_AutoRateLockAction.FloatDown, data);

            AutoRateLockResult result = new AutoRateLockResult()
            {
                ExpirationDate = data.sRLckdExpiredD_rep,
                Price = CApplicantRateOption.PointFeeInResult(m_dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, IncludeOriginatorComp ? data.m_convertLos.ToRateString(data.sOriginatorCompPoint + data.sBrokComp1Pc) : data.sBrokComp1Pc_rep),
                TotalEvents = data.sTotalRateLockFloatDowns_rep,
                Rate = data.sNoteIR_rep
            };

            return result;
        }

        private List<string> GetReasonsCannotFloatDown(decimal pointImprovement, decimal rateImprovement)
        {
            List<string> reasonsCannotFloat = new List<string>();

            if (pointImprovement < sLockPolicy.FloatDownMinPointImprovement)
            {
                reasonsCannotFloat.Add("The minimum required point improvement is " + sLockPolicy.FloatDownMinPointImprovement_rep + ".");
            }

            if (rateImprovement < sLockPolicy.FloatDownMinRateImprovement)
            {
                reasonsCannotFloat.Add("The minimum required rate improvement is " + sLockPolicy.FloatDownMinRateImprovement_rep + ".");
            }

            return reasonsCannotFloat;
        }

        protected override void VerifyActionImpl()
        {
            if (sLockPolicy.EnableAutoFloatDowns == false)
            {
                throw new NonCriticalAutoRateLockException("Float downs are not allowed.");
            }

            if (m_dataLoan.sTotalRateLockFloatDowns >= sLockPolicy.MaxFloatDowns)
            {
                string msg = "This loan file has already had " + m_dataLoan.sTotalRateLockFloatDowns_rep + " rate lock float downs. Additional float downs are not allowed.";
                throw new NonCriticalAutoRateLockException(msg);
            }

            if (sLockPolicy.FloatDownAllowedAfterReLock == false
                && m_dataLoan.sTotalRateReLocks != 0)
            {
                throw new NonCriticalAutoRateLockException("Float Downs cannot be requested after a Rate Re-Lock.");
            }

            if (sLockPolicy.FloatDownAllowedAfterLockExtension == false
                && m_dataLoan.sTotalLockExtensions != 0)
            {
                throw new NonCriticalAutoRateLockException("Float Downs cannot be requested after a lock extension.");
            }

        }

        protected override LoanProgramByInvestorSet GetProgramsToRun()
        {
            VerifySubmittedCert();
            return base.GetProgramsToRun();
        }

        // This is private for a reason--this data object bypasses the write permission checking.
        // But the containing class checks for the appropriate lock permission.  If making this
        // public or moving it outside, please be sure you are checking permissions appropriately.
        private class CAutoFloatDownData : CPageData
        {
            private static CSelectStatementProvider s_selectProvider;
            static CAutoFloatDownData()
            {
                StringList list = new StringList();

                list.Add("sfFloatDownRate");

                s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

            }

            public CAutoFloatDownData(Guid fileId)
                : base(fileId, "CAutoFloatDownData", s_selectProvider)
            {
            }

            protected override bool m_enforceAccessControl
            {
                get
                {
                    // Have to do this bypass.  It is often the case that the user 
                    // doing locking action does not have rights to edit because of the lock.
                    // Having the lock action permission overrides it.
                    return false;
                }
            }

        }

    }

}
