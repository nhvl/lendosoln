﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Email;
using LendersOffice.RatePrice;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Admin;

namespace LendersOffice.ObjLib.LockPolicies
{
    public abstract class AutoRateLock
    {
        protected abstract WorkflowOperation WorkflowOperation { get;}
        protected abstract void VerifyActionImpl();
        protected abstract AutoRateLockResult ExecuteActionImpl(string action, string args);
        protected abstract List<string[]> BuildResultTable();
        protected UnderwritingResultItem m_resultItem = null;
        protected E_RenderResultModeT m_priceMode = E_RenderResultModeT.Regular;
        protected int? m_rateLockDays = null;
        protected abstract Func<bool> ActionAllowed { get; }
        protected abstract string ActionFriendlyName { get; }
        

        protected CApplicantPriceXml ProgramResult
        {
            get
            {
                if (m_resultItem == null 
                    || m_resultItem.Results == null 
                    || m_resultItem.Results.Count == 0) return null;

                return m_resultItem.Results[0] as CApplicantPriceXml;
            }
        }

        private CPageData x_dataLoan = null;
        protected CPageData m_dataLoan
        {
            get
            {
                if (x_dataLoan == null)
                {
                    List<string> fields = CPageData.GetCPageBaseAndCAppDataDependencyList(this.GetType()).ToList();
                    fields.AddRange(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(AutoRateLock)));
                    
                    x_dataLoan = new CPageData(m_loanId, "AutoRateLock", fields);

                    x_dataLoan.InitLoad();
                    x_dataLoan.ByPassFieldSecurityCheck = true; // CAREFUL.  Have to do this because people without lock desk permission will need to use this feature.
                }
                return x_dataLoan;
            }
        }
        protected LockPolicy sLockPolicy
        {
            get
            {
                return m_dataLoan.sLockPolicy;
            }
        }
        protected LendersOffice.ObjLib.PriceGroups.PriceGroup sPriceGroup
        {
            get
            {
                return m_dataLoan.sPriceGroup;
            }
        }

        protected string PointPriceStr
        {
            get
            {
                return sPriceGroup.DisplayPmlFeeIn100Format ? "Price" : "Points";
            }
        }

        private AbstractUserPrincipal x_principal = null;
        protected AbstractUserPrincipal m_principal
        {
            get
            {
                if (x_principal == null)
                {
                    x_principal = (AbstractUserPrincipal)PrincipalFactory.CurrentPrincipal;
                    if (x_principal == null)
                        throw new CBaseException(ErrorMessages.Generic, "Null user principal.");

                }
                return x_principal;
            }
        }
        
        protected Guid m_loanId;

        public static AutoRateLock Create(Guid loanId, E_AutoRateLockAction action, params string[] args)
        {
            
            int? lockDays = null;
            if (args != null && args.Count() > 0)
            {
                if ( action != E_AutoRateLockAction.ReLock )
                    throw new CBaseException(ErrorMessages.Generic, "Invalid lock action.");

                int parsedInt;
                if (int.TryParse(args[0], out parsedInt))
                {
                    lockDays = parsedInt;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid argument.");
                }
            }
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(AutoRateLock));
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true;

            LockPolicy lockPolicy = dataLoan.sLockPolicy;

            bool isUsingCustomPolicy = lockPolicy.IsCodeBased;

            if (isUsingCustomPolicy)
            {
                switch (action)
                {
                    case E_AutoRateLockAction.Extend:
                    case E_AutoRateLockAction.FloatDown:
                    case E_AutoRateLockAction.ReLock:
                        return new CodeBasedAutoRateLock(action, loanId, lockPolicy.GetCustomLockPolicy());
                    default:
                        throw new UnhandledEnumException(action);
                }
            }
            else
            {
                switch (action)
                {
                    case E_AutoRateLockAction.Extend: return new AutoRateLockExtend(loanId);
                    case E_AutoRateLockAction.FloatDown: return new AutoRateLockFloatDown(loanId);
                    case E_AutoRateLockAction.ReLock: return new AutoRateLockReLock(loanId, lockDays.Value);
                    default:
                        throw new UnhandledEnumException(action);
                }
            }
        }

        public static void SendNotificationEmail(E_AutoRateLockAction actionType, CPageData dataLoan)
        {
            string loanOfficerEmail = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject).EmailAddr;
            string underwriterEmail = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject).EmailAddr;
            string lockDeskEmail = dataLoan.sEmployeeLockDeskEmail;

            string[] emails = null;

            if (loanOfficerEmail == string.Empty && underwriterEmail == string.Empty && lockDeskEmail == string.Empty)
            {
                EmployeeDB empDB = new EmployeeDB(dataLoan.sLockPolicy.DefaultLockDeskIDOrInheritedID, dataLoan.sBrokerId);

                if (empDB.Retrieve())
                {
                    emails = new string[] { empDB.Email };
                }
            }
            else
            {
                emails = new string[] { loanOfficerEmail, underwriterEmail, lockDeskEmail };
            }

            if (emails != null)
            {
                SendNotificationEmailImpl(
                    actionType,
                    dataLoan.sBrokerId,
                    dataLoan.sLNm,
                    dataLoan.GetAppData(0).aBNm,
                    dataLoan.sOpenedD_rep,
                    dataLoan.sBrokerLockOriginatorPriceNoteIR_rep,
                    dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep,
                    dataLoan.sRLckdExpiredD_rep,
                    emails);
            }
            else
            {
                Tools.LogWarning("Auto-lock operation performed but the notification email recipient does not exist.");
            }
        }

        /// <summary>
        /// Sends a notification email to the specified <paramref name="recipients"/>
        /// that the auto rate lock event was successful.
        /// </summary>
        /// <param name="autoRateLockType">
        /// The <see cref="E_AutoRateLockAction"/> type for the event.
        /// </param>
        /// <param name="loanName">
        /// The name of the loan where the event occurred.
        /// </param>
        /// <param name="borrowerName">
        /// The borrower's name on the loan.
        /// </param>
        /// <param name="loanOpenDate">
        /// The date the loan was opened.
        /// </param>
        /// <param name="rate">
        /// The rate for the lock.
        /// </param>
        /// <param name="points">
        /// The points for the lock.
        /// </param>
        /// <param name="expirationDate">
        /// The date when the lock expires.
        /// </param>
        /// <param name="recipients">
        /// The recipients for the email.
        /// </param>
        /// <remarks>
        /// Empty values present in <paramref name="recipients"/>
        /// will be pruned before the email is sent.
        /// </remarks>
        private static void SendNotificationEmailImpl(
            E_AutoRateLockAction autoRateLockType,
            Guid brokerId,
            string loanName,
            string borrowerName,
            string loanOpenDate,
            string rate,
            string points,
            string expirationDate,
            params string[] recipients)
        {
            var acceptanceHeader = string.Empty;
            var request = string.Empty;

            switch (autoRateLockType)
            {
                case E_AutoRateLockAction.Extend:
                    acceptanceHeader = "Lock Extension Accepted";
                    request = "lock-extension";
                    break;
                case E_AutoRateLockAction.FloatDown:
                    acceptanceHeader = "Rate Renegotiation Accepted";
                    request = "rate-renegotiation";
                    break;
                case E_AutoRateLockAction.ReLock:
                    acceptanceHeader = "Re-Lock Accepted";
                    request = "re-lock";
                    break;
                default:
                    throw new UnhandledEnumException(autoRateLockType);
            }

            var bodyFormat =
@"The request for a {0} for loan {1}, which was opened on {2} for {3}, has been automatically accepted at {4} rate and {5} points on {6}. The rate lock expires on {7}.

This notification was automatically generated for you by LendingQB (www.lendingqb.com).  Please do not directly reply to this email.";

            var email = new CBaseEmail(brokerId)
            {
                From = ConstStage.DefaultDoNotReplyAddress,
                Subject = loanName + " - " + borrowerName + " - " + acceptanceHeader,
                DisclaimerType = E_DisclaimerType.NORMAL,
            };

            email.To = string.Join(",", recipients.Where(address => !string.IsNullOrWhiteSpace(address)));

            email.Message = string.Format(
                bodyFormat,
                request,
                loanName,
                loanOpenDate,
                borrowerName,
                rate,
                points,
                DateTime.Now,
                expirationDate);

            email.Send();
        }

        protected AutoRateLock(Guid loanId)
        {
            m_loanId = loanId;
        }

        public virtual string CreatePricingRequest()
        {
            // Create a single pricing request for this single program
            // return the request Id.

            // 1/10/2014 dd - Changed the signature from return Guid to string.
            //                The reason is in the custom lock policy I need to be able to return 2 
            //                separate Guid request id.
            VerifyAction();

            LoanProgramByInvestorSet lpSet = GetProgramsToRun();
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(m_principal.BrokerDB);
            Guid requestID = DistributeUnderwritingEngine.SubmitToEngineForAutoLock(
                m_principal
                , m_loanId
                , lpSet
                , E_sLienQualifyModeT.ThisLoan
                , m_dataLoan.sProdLpePriceGroupId
                , m_priceMode
                , m_rateLockDays
                , options
                );

            return requestID.ToString();
        }

        protected virtual LoanProgramByInvestorSet GetProgramsToRun()
        {
            // Build a pricing request for this loan with the only applicable program being the registered

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            lpSet.Add(m_dataLoan.sLpTemplateId, "N/A", null);

            return lpSet;
        }

        public AutoRateLockResult ExecuteAction(string action, string args)
        {
            VerifyAction();

            return ExecuteActionImpl(action, args);

        }

        public virtual bool IsResultReady(string requestId)
        {
            // 1/10/2014 dd - Change the signature of the method. RequestId is a string instead of Guid.
            //                The reason is in custom lock policy the requestId will contains 2 separate
            //                Guid

            Guid id = new Guid(requestId);
            if (LpeDistributeResults.IsResultAvailable(id) == false)
                return false;

            m_resultItem = LpeDistributeResults.RetrieveResultItem(id);

            return m_resultItem != null;
        }

        public virtual bool HasResult
        {
            get { return m_resultItem != null; }
        }
        public List<string[]> ResultTable
        {
            get
            {
                if (HasResult == false)
                    throw new CBaseException(ErrorMessages.Generic,
                        "Cannot build a result table without first getting a result");

                return BuildResultTable();

            }
        }

        protected void VerifySubmittedCert()
        {
            // OPM 477076. The file can get into a state where it is setup for a relock or floatdown
            // but the file does not have a submitted cert, which we need for this operation.

            if (string.IsNullOrEmpty(m_dataLoan.sPmlCertXmlContent.Value))
            {
                throw new NonCriticalAutoRateLockException("The file does not have any pricing record, please contact your lock desk to perform this action.");
            }
        }

        private void VerifyAction()
        {
            if (ActionAllowed.Invoke() == false)
            {
                // "Safety" check.  UI should not present them with this
                // action based on the file and policy, but in case of stale UI or SDE mistake.
                throw new NonCriticalAutoRateLockException("Cannot perform this action.");
            }

            // Workflow permission
            VerifyPermission();

            // RSE checks 
            if ( sLockPolicy.IsClosedForLockDesk(DateTime.Now) )                  
            {
                throw new NonCriticalAutoRateLockException("The Lock Desk is currently closed.  Lock " + ActionFriendlyName.ToLower() + "s must be made while the lock desk is open.");
            }

            VerifyActionImpl();

            if (m_dataLoan.sLpTemplateId == Guid.Empty)
                throw new NonCriticalAutoRateLockException("Loan does not have a registered program.");  // Might also have been stale UI. Give gentle msg.

        }

        private void VerifyPermission()
        {
            // Make sure the user has workflow permission to do what they are trying to do.
            // Throw exception if not.

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(m_principal.ConnectionInfo, m_principal.BrokerId, m_loanId, WorkflowOperation);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(m_principal));

            if (LendingQBExecutingEngine.CanPerform(WorkflowOperation, valueEvaluator) == false)
            {
                //string msg = LendingQBExecutingEngine.GetUserFriendlyMessage(operation, valueEvaluator);
                IReadOnlyCollection<string> msgList = LendingQBExecutingEngine.GetUserFriendlyMessageList(WorkflowOperation, valueEvaluator);

                string userMsg;
                if (msgList.Any())
                {
                    userMsg = Environment.NewLine;
                    userMsg += string.Join(Environment.NewLine, msgList);
                }
                else
                {
                    userMsg = "Access Denied.  You do not have permission to take this action.";
                }

                throw new NonCriticalAutoRateLockException(userMsg);
            }
        }

        protected string ToRate(decimal input)
        {
            return m_dataLoan.m_convertLos.ToRateString(input);
        }

        protected string ToPointRate(decimal input)
        {
            return CApplicantRateOption.PointFeeInResult(
                sPriceGroup.DisplayPmlFeeIn100Format
                , m_dataLoan.m_convertLos.ToRateString(input));
        }

        protected decimal ConvertPointBase(decimal originalDecimal)
        {
            return 100 - originalDecimal;
        }

        protected bool IncludeOriginatorComp
        {
            get
            {
                return (m_dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                         && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                         && m_dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);
            }
        }

        protected List<PricingAdjustment> BuildAdjustmentList()
        {
            List<PricingAdjustment> adjustments = new List<PricingAdjustment>();
            foreach (CAdjustItem adjustment in ProgramResult.AdjustDescs)
            {
                if (adjustment.HasAdjustment == false)
                    continue;

                PricingAdjustment adjForList = new PricingAdjustment()
                {
                    IsHidden = false,
                    Description = adjustment.Description,
                    Rate = adjustment.Rate,
                    Fee = adjustment.Fee,
                    Margin = adjustment.Margin,
                    QualifyingRate = adjustment.QRate,
                    TeaserRate = adjustment.TeaserRate,
                    IsLenderAdjustment = adjustment.IsLenderAdjustment,
                    IsSRPAdjustment = false,
                    IsPersist = adjustment.IsPersist
                };

                adjustments.Add(adjForList);
            }
            foreach (CAdjustItem adjustment in ProgramResult.HiddenAdjustDescs)
            {
                if (adjustment.HasAdjustment == false)
                    continue;

                PricingAdjustment adjForList = new PricingAdjustment()
                {
                    IsHidden = true,
                    Description = adjustment.Description,
                    Rate = adjustment.Rate,
                    Fee = adjustment.Fee,
                    Margin = adjustment.Margin,
                    QualifyingRate = adjustment.QRate,
                    TeaserRate = adjustment.TeaserRate,
                    IsLenderAdjustment = adjustment.IsLenderAdjustment,
                    IsSRPAdjustment = false,
                    IsPersist = adjustment.IsPersist
                };
                adjustments.Add(adjForList);
            }

            return adjustments;
        }

        // POD for result of auto lock action
        public class AutoRateLockResult
        {
            public string ExpirationDate { get; set; }
            public string Price { get; set; }
            public string Rate { get; set; }
            public string TotalEvents { get; set; }
        }

        public class NonCriticalAutoRateLockException : CBaseException
        {
            public NonCriticalAutoRateLockException(string userMessage, string developerMessage) :
                base(userMessage, developerMessage)
            {
                this.IsEmailDeveloper = false;
            }

            public NonCriticalAutoRateLockException(string msg)
                : base(msg, msg)
            {
                this.IsEmailDeveloper = false;   
            }
        }



    }



}