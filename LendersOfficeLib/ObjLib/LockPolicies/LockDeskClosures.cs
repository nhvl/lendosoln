﻿namespace LendersOffice.ObjLib.LockPolicies
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;

    /// <summary>
    /// The holiday closures of a particular lock policy.
    /// </summary>
    public class LockDeskClosures
    {
        /// <summary>
        /// The lender for which the lock policy and these closures apply.
        /// </summary>
        private readonly Guid brokerId;

        /// <summary>
        /// The lock policy to which these closures apply.
        /// </summary>
        private readonly Guid lockPolicyId;

        /// <summary>
        /// The manually entered and past holiday closures, i.e., all the non-calculated holiday closures.
        /// </summary>
        private Lazy<IDictionary<DateTime, LockDeskClosureItem>> manualAndPastClosures;

        /// <summary>
        /// The holidays that the lender will observe for all future occurrences.
        /// </summary>
        private Lazy<IList<Holiday>> observedHolidays;

        /// <summary>
        /// Initializes a new instance of the <see cref="LockDeskClosures"/> class.
        /// </summary>
        /// <param name="brokerId">The lender for which the lock policy and these closures apply.</param>
        /// <param name="lockPolicyId">The lock policy to which these closures apply.</param>
        public LockDeskClosures(Guid brokerId, Guid lockPolicyId)
        {
            this.lockPolicyId = lockPolicyId;
            this.brokerId = brokerId;
            this.manualAndPastClosures = new Lazy<IDictionary<DateTime, LockDeskClosureItem>>(this.LoadManualAndPastClosures);
            this.observedHolidays = new Lazy<IList<Holiday>>(this.LoadObservedHolidays);
        }

        /// <summary>
        /// Gets the observed holidays for the lock policy.
        /// </summary>
        /// <value>The observed holidays for the lock policy.</value>
        public IReadOnlyList<Holiday> ObservedHolidays
        {
            get { return new System.Collections.ObjectModel.ReadOnlyCollection<Holiday>(this.observedHolidays.Value); }
        }

        /// <summary>
        /// Migrates the closures for the lock policy to the new holiday closure storage.
        /// </summary>
        /// <param name="brokerId">The id of the lender who owns the lock policy.</param>
        /// <param name="lockPolicyId">The id of the lock policy of the closures.</param>
        /// <remarks>This is a one-time migration and should be deleted after it is run on all lenders.</remarks>
        public static void MigrateOldNonCalculatedHolidayDates(Guid brokerId, Guid lockPolicyId)
        {
            var closures = new LockDeskClosures(brokerId, lockPolicyId);
            foreach (var holiday in Holiday.AvailableLockPolicyHolidays)
            {
                if (closures.observedHolidays.Value.Any(h => h.EnumName == holiday.EnumName))
                {
                    continue; // This holiday is already migrated; safe to skip.
                }

                // This calculation is ripped from code deleted from HolidayListNew.aspx.cs
                // It is ugly, but very effective at determining the next and last dates of a given holiday.
                DateTime lastDate, nextDate;
                lastDate = nextDate = holiday.CalculateDate(DateTime.Today.Year);
                if (nextDate < DateTime.Today)
                {
                    nextDate = holiday.CalculateDate(DateTime.Today.Year + 1);
                }
                else
                {
                    lastDate = holiday.CalculateDate(DateTime.Today.Year - 1);
                }

                if (closures.manualAndPastClosures.Value.ContainsKey(lastDate))
                {
                    // If the last date is in the list, we consider the holiday valid.
                    closures.AddObservedHoliday(holiday);
                }
                else if (closures.manualAndPastClosures.Value.ContainsKey(nextDate))
                {
                    // Else if only the next date is in the list, the holiday is also valid, and it probably means
                    // that someone visited the page, triggering the replacement of the last date with the next date.
                    // Per the spec, this means we should add a closure for last year's occurrence. (This is the second migration specified.)
                    closures.AddObservedHoliday(holiday);
                    closures.SaveClosure(LockDeskClosureItem.CreateHolidayAutoClosure(holiday, lastDate.Year));
                }
            }
        }

        /// <summary>
        /// Converts any calculated holiday closures on the specified dates into manual/past entries.
        /// This is intended to be called in the nightly process to update each item.
        /// </summary>
        /// <param name="brokerId">The id of the lender who owns the lock policy.</param>
        /// <param name="lockPolicyId">The id of the lock policy of the closures.</param>
        /// <param name="dates">The dates to convert into a past entry.</param>
        public static void ConvertObservedHolidayToPastEntry(Guid brokerId, Guid lockPolicyId, params DateTime[] dates)
        {
            var closures = new LockDeskClosures(brokerId, lockPolicyId);
            foreach (var date in dates)
            {
                var closure = closures.FindClosureOrDefault(date.Date, DateTime.MinValue); // allow calculating holidays for past dates
                if (closure?.SourceHoliday != null)
                {
                    closures.SaveClosure(closure);
                }
            }
        }

        /// <summary>
        /// Creates a data table from the list of closures.  This method is primarily for legacy data-binding
        /// and should be removed once the UI has been updated to no longer use a <see cref="DataTable"/>.
        /// </summary>
        /// <param name="closures">The holiday closures to convert to a table.</param>
        /// <returns>A data table containing the field data.</returns>
        public static DataTable GetDataTableOfClosures(IEnumerable<LockDeskClosureItem> closures)
        {
            var closureDateColumn = new DataColumn("ClosureDate");
            var isClosedForBusinessColumn = new DataColumn("IsClosedForBusiness");
            var isClosedAllDayColumn = new DataColumn("IsClosedAllDay");
            var lockDeskOpenTimeColumn = new DataColumn("LockDeskOpenTime");
            var lockDeskCloseTimeColumn = new DataColumn("LockDeskCloseTime");

            DataTable table = new DataTable();
            table.Columns.Add(closureDateColumn);
            table.Columns.Add(isClosedForBusinessColumn);
            table.Columns.Add(isClosedAllDayColumn);
            table.Columns.Add(lockDeskOpenTimeColumn);
            table.Columns.Add(lockDeskCloseTimeColumn);

            foreach (LockDeskClosureItem closureItem in closures)
            {
                var row = table.NewRow();
                row[closureDateColumn] = closureItem.ClosureDate;
                row[isClosedForBusinessColumn] = closureItem.IsClosedForBusiness;
                row[isClosedAllDayColumn] = closureItem.IsClosedForLocks;
                row[lockDeskOpenTimeColumn] = closureItem.LockDeskOpenTimeInLenderTimezone;
                row[lockDeskCloseTimeColumn] = closureItem.LockDeskCloseTimeInLenderTimezone;
                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// Gets the upcoming closures for the lock policy.  For manual entries, this means all future dates;
        /// for calculated holidays this means the next occurrence of each holiday.
        /// A boolean parameter allows inclusion of past entries as well.
        /// </summary>
        /// <param name="includePastClosures">A value indicating whether past closures should be included in the result.</param>
        /// <returns>The upcoming closures for the lock policy.</returns>
        public IEnumerable<LockDeskClosureItem> GetUpcoming(bool includePastClosures = false)
        {
            var nonCalculatedClosures = includePastClosures ? this.manualAndPastClosures.Value.Values : this.manualAndPastClosures.Value.Values.Where(closure => DateTime.Today <= closure.ClosureDate);
            var calculatedClosures = new List<LockDeskClosureItem>(this.ObservedHolidays.Count);
            foreach (var holiday in this.ObservedHolidays)
            {
                var nextOccurrence = holiday.NextOccurrenceOnOrAfter(DateTime.Today);
                if (!this.manualAndPastClosures.Value.ContainsKey(nextOccurrence))
                {
                    calculatedClosures.Add(LockDeskClosureItem.CreateHolidayAutoClosure(holiday, nextOccurrence.Year));
                }
            }

            return nonCalculatedClosures.Concat(calculatedClosures).OrderBy(closure => closure.ClosureDate);
        }

        /// <summary>
        /// Gets the closure applying to the specified date, if it exists.
        /// </summary>
        /// <param name="date">The date to look for in the closure list. Only the date component will be considered.</param>
        /// <param name="holidayCalculationStartDate">
        /// The start date for holiday calculations.  This parameter defaults to <seealso cref="DateTime.Today"/>
        /// and should be omitted in most cases.
        /// </param>
        /// <returns>A closure for the specified date or null if none exists.</returns>
        public LockDeskClosureItem FindClosureOrDefault(DateTime date, DateTime? holidayCalculationStartDate = null)
        {
            date = date.Date; // truncate immediately; we don't want time to factor in
            LockDeskClosureItem closure;
            if (this.manualAndPastClosures.Value.TryGetValue(date, out closure))
            {
                return closure;
            }
            else if (date >= (holidayCalculationStartDate ?? DateTime.Today))
            {
                Holiday holidayOccurrence = this.observedHolidays.Value.FirstOrDefault(holiday => holiday.CalculateDate(date.Year) == date);
                if (holidayOccurrence != null)
                {
                    return LockDeskClosureItem.CreateHolidayAutoClosure(holidayOccurrence, date.Year);
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a holiday to the list of observed holidays both in the database and this instance.
        /// </summary>
        /// <param name="holiday">The holiday to add.</param>
        /// <returns>A value indicating whether the holiday was successfully added.</returns>
        public bool AddObservedHoliday(Holiday holiday)
        {
            if (this.ObservedHolidays.Any(h => h.EnumName == holiday.EnumName))
            {
                return false;
            }

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@LockPolicyId", this.lockPolicyId),
                new SqlParameter("@HolidayName", holiday.EnumName),
            };
            int rowCount = DataAccess.StoredProcedureHelper.ExecuteNonQuery(this.brokerId, "LockPolicyObservedHoliday_Create", 0, parameters);
            if (rowCount <= 0)
            {
                this.observedHolidays = new Lazy<IList<Holiday>>(this.LoadObservedHolidays);
            }
            else
            {
                this.observedHolidays.Value.Add(holiday);
            }

            return rowCount > 0;
        }

        /// <summary>
        /// Removes all future occurrences of the specified holiday from the lender's closure list, as well as the observance itself.
        /// </summary>
        /// <param name="holiday">The holiday to remove from the set.</param>
        public void RemoveFutureHolidayOccurrences(Holiday holiday)
        {
            this.RemoveObservedHoliday(holiday);

            var futureManualClosureDates = this.manualAndPastClosures.Value.Keys.Where(closureDate => DateTime.Today <= closureDate).ToList(); // Need to evaluate eagerly since we intend to modify the root collection.
            foreach (var date in futureManualClosureDates)
            {
                if (holiday.CalculateDate(date.Year) == date)
                {
                    this.RemoveClosure(date);
                }
            }
        }

        /// <summary>
        /// Saves a manual or past closure in the database and this instance.
        /// </summary>
        /// <param name="holidayClosure">The closure to save.</param>
        /// <returns>A value indicating whether the save was successful.</returns>
        /// <remarks>
        /// Initially this was an add and an update function, but the transition from calculated
        /// to non-calculated left the page thinking the record was only being edited, but this
        /// object knew it didn't exist yet.  Thus, it is simpler to only provide one interface.
        /// </remarks>
        public bool SaveClosure(LockDeskClosureItem holidayClosure)
        {
            string storedProcedure = this.manualAndPastClosures.Value.ContainsKey(holidayClosure.ClosureDate) ? "UpdateLockDeskClosureForPolicyId" : "CreateLockDeskClosureForPolicyId";
            bool saveSuccessful = this.SaveClosureToDatabase(holidayClosure.ClosureDate, storedProcedure, holidayClosure);
            if (saveSuccessful)
            {
                this.manualAndPastClosures.Value[holidayClosure.ClosureDate] = holidayClosure;
            }

            return saveSuccessful;
        }

        /// <summary>
        /// Removes a manual or past closure for the specified date from the database and this instance.
        /// </summary>
        /// <param name="date">The date of the closure.</param>
        /// <returns>A value indicating whether the save was successful.</returns>
        public bool RemoveClosure(DateTime date)
        {
            date = date.Date;
            if (!this.manualAndPastClosures.Value.ContainsKey(date))
            {
                return false;
            }

            bool saveSuccessful = this.SaveClosureToDatabase(date, "DeleteLockDeskClosureForPolicyId");
            if (saveSuccessful)
            {
                this.manualAndPastClosures.Value.Remove(date);
            }

            return saveSuccessful;
        }

        /// <summary>
        /// Saves a holiday closure to the database, including possible removal.
        /// </summary>
        /// <param name="closureDate">The date of the closure.</param>
        /// <param name="storedProcedureName">The stored procedure name.</param>
        /// <param name="holidayClosure">The holiday closure to save to the DB. May be null for removal.</param>
        /// <returns>A value indicating whether rows were updated in the save action.</returns>
        private bool SaveClosureToDatabase(DateTime closureDate, string storedProcedureName, LockDeskClosureItem holidayClosure = null)
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@PolicyId", this.lockPolicyId),
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@ClosureDate", closureDate)
            };

            if (holidayClosure != null)
            {
                parameters.Add(new SqlParameter("@LockDeskOpenTime", holidayClosure.LockDeskOpenTimeInLenderTimezone));
                parameters.Add(new SqlParameter("@LockDeskCloseTime", holidayClosure.LockDeskCloseTimeInLenderTimezone));
                parameters.Add(new SqlParameter("@IsClosedAllDay", holidayClosure.IsClosedForLocks));
                parameters.Add(new SqlParameter("@IsClosedForBusiness", holidayClosure.IsClosedForBusiness));
            }

            int rowCount = DataAccess.StoredProcedureHelper.ExecuteNonQuery(this.brokerId, storedProcedureName, 1, parameters);
            if (rowCount <= 0)
            {
                this.manualAndPastClosures = new Lazy<IDictionary<DateTime, LockDeskClosureItem>>(this.LoadManualAndPastClosures);
            }

            return rowCount > 0;
        }

        /// <summary>
        /// Removes a holiday from the list of observed holidays both in the database and this instance.
        /// </summary>
        /// <param name="holiday">The holiday to remove.</param>
        /// <returns>A value indicating whether the holiday was successfully removed.</returns>
        private bool RemoveObservedHoliday(Holiday holiday)
        {
            var holidayIndices = this.observedHolidays.Value.SelectIndicesWhere(h => h.EnumName == holiday.EnumName).ToList();
            if (!holidayIndices.Any())
            {
                return false;
            }

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@LockPolicyId", this.lockPolicyId),
                new SqlParameter("@HolidayName", holiday.EnumName),
            };
            int rowCount = DataAccess.StoredProcedureHelper.ExecuteNonQuery(this.brokerId, "LockPolicyObservedHoliday_Delete", 0, parameters);
            if (rowCount <= 0)
            {
                this.observedHolidays = new Lazy<IList<Holiday>>(this.LoadObservedHolidays);
            }
            else
            {
                holidayIndices.ForEach(i => this.observedHolidays.Value.RemoveAt(i));
            }

            return rowCount > 0;
        }

        /// <summary>
        /// Loads the manual and past closures from the DB.
        /// </summary>
        /// <returns>A dictionary of closure items by the date of the closure.</returns>
        private IDictionary<DateTime, LockDeskClosureItem> LoadManualAndPastClosures()
        {
            var closures = new Dictionary<DateTime, LockDeskClosureItem>();
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@PolicyId", this.lockPolicyId),
            };

            using (IDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(this.brokerId, "ListLockDeskClosureByPolicyId", parameters))
            {
                while (reader.Read())
                {
                    LockDeskClosureItem item = new LockDeskClosureItem(
                        (DateTime)reader["ClosureDate"],
                        (bool)reader["IsClosedForBusiness"],
                        (bool)reader["IsClosedAllDay"],
                        (DateTime)reader["LockDeskOpenTime"],
                        (DateTime)reader["LockDeskCloseTime"]);
                    closures.Add(item.ClosureDate.Date, item);
                }
            }

            return closures;
        }

        /// <summary>
        /// Loads the observed holidays of the lock policy from the DB.
        /// </summary>
        /// <returns>A list of holidays the lock policy observes.</returns>
        private IList<Holiday> LoadObservedHolidays()
        {
            var holidayNames = new List<string>();
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@LockPolicyId", this.lockPolicyId),
            };

            using (IDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(this.brokerId, "LockPolicyObservedHoliday_List", parameters))
            {
                while (reader.Read())
                {
                    holidayNames.Add((string)reader["HolidayName"]);
                }
            }

            return holidayNames.Select(name => Holiday.AvailableLockPolicyHolidays.Single(holiday => holiday.EnumName == name)).ToList();
        }
    }
}
