﻿namespace LendersOffice.ObjLib.LockPolicies
{
    using System;
    using Common;

    /// <summary>
    /// Represents a single holiday closure for the lender's lock policy.
    /// </summary>
    public class LockDeskClosureItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LockDeskClosureItem"/> class.
        /// </summary>
        /// <param name="closureDate">The date of the closure.</param>
        /// <param name="isClosedForBusiness">A value indicating whether the lock desk is closed for business.</param>
        /// <param name="isClosedForLocks">A value indicating whether the lock desk is closed for locks.</param>
        /// <param name="lockDeskOpenTimeInLenderTimezone">The time the lock desk opens.  This should be as stored in the DB, which is currently in the lender's time zone.</param>
        /// <param name="lockDeskCloseTimeInLenderTimezone">The time the lock desk closes.  This should be as stored in the DB, which is currently in the lender's time zone.</param>
        public LockDeskClosureItem(DateTime closureDate, bool isClosedForBusiness, bool isClosedForLocks, DateTime lockDeskOpenTimeInLenderTimezone, DateTime lockDeskCloseTimeInLenderTimezone)
        {
            this.ClosureDate = closureDate.Date;
            this.IsClosedForBusiness = isClosedForBusiness;
            this.IsClosedForLocks = isClosedForLocks;
            this.LockDeskOpenTimeInLenderTimezone = lockDeskOpenTimeInLenderTimezone;
            this.LockDeskCloseTimeInLenderTimezone = lockDeskCloseTimeInLenderTimezone;
        }

        /// <summary>
        /// Gets the date of the closure. This should only be compared against a date value.
        /// </summary>
        /// <value>The date of the closure.</value>
        public DateTime ClosureDate { get; }

        /// <summary>
        /// Gets a value indicating whether the lock desk is closed for business.
        /// </summary>
        /// <value>A value indicating whether the lock desk is closed for business.</value>
        public bool IsClosedForBusiness { get; }

        /// <summary>
        /// Gets a value indicating whether the lock desk is closed for locks.
        /// In the database, this value goes by <c>IsClosedAllDay</c>.
        /// </summary>
        /// <value>A value indicating whether the lock desk is closed for locks.</value>
        public bool IsClosedForLocks { get; }

        /// <summary>
        /// Gets the time the lock desk opens in the lender's time zone. This should only be compared against a time value.
        /// </summary>
        /// <value>The time the lock desk opens.</value>
        /// <remarks>The raw time the lock desk opens, as stored in the database.  This value is (conceptually) in the lender's time zone.</remarks>
        public DateTime LockDeskOpenTimeInLenderTimezone { get; }

        /// <summary>
        /// Gets the time the lock desk closes in the lender's time zone. This should only be compared against a time value.
        /// </summary>
        /// <value>The time the lock desk closes.</value>
        /// <remarks>The raw time the lock desk closes, as stored in the database.  This value is (conceptually) in the lender's time zone.</remarks>
        public DateTime LockDeskCloseTimeInLenderTimezone { get; }

        /// <summary>
        /// Gets the holiday from which this closure item was generated, or null if this item was not generated.
        /// </summary>
        /// <value>The holiday from which this closure item was generated, or null if this item was not generated.</value>
        public Holiday SourceHoliday { get; private set; }

        /// <summary>
        /// Creates an instance representing a calculated holiday closure.
        /// </summary>
        /// <param name="holiday">The holiday of the closure.</param>
        /// <param name="year">The year of the closure.</param>
        /// <returns>The instance representing the closure.</returns>
        public static LockDeskClosureItem CreateHolidayAutoClosure(Holiday holiday, int year)
        {
            if (holiday == null)
            {
                throw new ArgumentNullException(nameof(holiday));
            }

            // Start and end times are set because they are automatically closed all day
            DateTime startTime = DateTime.Parse("12:00AM");
            DateTime endTime = DateTime.Parse("11:59PM");

            return new LockDeskClosureItem(
                holiday.CalculateDate(year),
                true,
                true,
                startTime,
                endTime)
            {
                SourceHoliday = holiday
            };
        }
    }
}
