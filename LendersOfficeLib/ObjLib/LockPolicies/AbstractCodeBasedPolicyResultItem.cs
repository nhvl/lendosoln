﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.ObjLib.LockPolicies
{
    public abstract class AbstractCodeBasedPolicyResultItem
    {
        public Guid EntropyData { get; private set; }
        public Guid LoanId { get; private set; }
        public Guid UserId { get; private set; }

        protected AbstractCodeBasedPolicyResultItem(Guid loanId, Guid userId)
        {
            this.EntropyData = Guid.NewGuid();
            this.LoanId = loanId;
            this.UserId = userId;
        }
        protected AbstractCodeBasedPolicyResultItem(string encryptedData)
        {
            string[] parts = EncryptionHelper.Decrypt(encryptedData).Split(':');

            this.EntropyData = new Guid(parts[0]);
            this.LoanId = new Guid(parts[1]);
            this.UserId = new Guid(parts[2]);

            string[] newParts = new string[parts.Length - 3];
            for (int i = 0; i < newParts.Length; i++)
            {
                newParts[i] = parts[i + 3];
            }
            ParseData(newParts);
        }

        protected abstract IEnumerable<string> BuildDataList();
        protected abstract void ParseData(string[] parts);

        public string EncryptedData
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}:{1}:{2}", EntropyData, LoanId, UserId);

                foreach (var o in BuildDataList())
                {
                    sb.AppendFormat(":{0}", o);
                }
                return EncryptionHelper.Encrypt(sb.ToString());
            }
        }


        private void Verify(Guid sLId, Guid userId)
        {
            if (sLId != LoanId || userId != UserId)
            {
                // 1/13/2014 dd - Only get here if someone copy the encryptedCommand from other session.
                throw CBaseException.GenericException("Expect sLId=[" + sLId + "], Actual=[" + LoanId + "]. Expect UserId=["
                    + userId + "], Actual=[" + UserId + "]");
            }
        }

        public static AbstractCodeBasedPolicyResultItem Create(E_AutoRateLockAction action,
            string encryptedData, Guid expected_sLId, Guid expected_userId)
        {
            AbstractCodeBasedPolicyResultItem result = null;

            switch (action)
            {
                case E_AutoRateLockAction.Extend:
                    result = new CodeBasedLockExtensionResultItem(encryptedData);
                    break;
                case E_AutoRateLockAction.FloatDown:
                    result = new CodeBasedFloatDownResultItem(encryptedData);
                    break;
                case E_AutoRateLockAction.ReLock:
                    result = new CodeBasedRelockResultItem(encryptedData);
                    break;
                default:
                    throw new UnhandledEnumException(action);
            }
            result.Verify(expected_sLId, expected_userId);

            return result;
        }

    }
}
