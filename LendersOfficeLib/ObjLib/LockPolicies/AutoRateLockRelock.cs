﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using DataAccess.LoanComparison;
using System.Data.Common;
using System.Data.SqlClient;
using CommonProjectLib.Common.Lib;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class AutoRateLockReLock : AutoRateLock
    {
        protected override WorkflowOperation WorkflowOperation
        {
            get { return WorkflowOperations.RateReLock; }
        }
        protected override Func<bool> ActionAllowed
        {
            get { return () => m_dataLoan.sIsRateReLockAllowed; }
        }

        private bool UseMarketPricing = false;

        protected override string ActionFriendlyName
        {
            get { return "Re-Lock"; }
        }

        public AutoRateLockReLock(Guid loanId, int lockDays)
            : base(loanId)
        {
            m_rateLockDays = lockDays;
            m_priceMode = sLockPolicy.ReLockRequireSameInvestor ? E_RenderResultModeT.Regular : E_RenderResultModeT.BestPricesPerProgram;
        }

        protected override List<string[]> BuildResultTable()
        {

            decimal? fee = null;

            int daysExpired = (DateTime.Now - m_dataLoan.sRLckdExpiredD.DateTimeForComputation).Days;
            UseMarketPricing = daysExpired > sLockPolicy.ReLockWorstCasePricingMaxDays;

            if (UseMarketPricing == false)
            {
                // Fee for this period
                foreach (var relockFee in sLockPolicy.ReLockFeeTable)
                {
                    if (relockFee.PeriodEndDay == m_rateLockDays)
                    {
                        fee = relockFee.Fee;
                        break;
                    }
                }

                if (fee.HasValue == false)
                {
                    string msg = "Unsupported length: " + m_rateLockDays + " days.";
                    throw new CBaseException(msg, msg);
                }
            }
            else
            {
                // Market price fee;
                fee = sLockPolicy.ReLockMarketPriceReLockFee;
            }

            decimal extraFees = m_dataLoan.sTotalNonFloatRelockFees;

            IDictionary<decimal, decimal> originalPrices = m_dataLoan.sSubmittedRatePrices;

            var mergedResult = GetMergedResult();

            // If the cert we are looking at was rendered in Price mode
            // we want to do the calcuations in point mode regardless,
            // then convert it (if needed) for display
            bool convertPreviousPoint = m_dataLoan.sSubmittedRatePriceIsInPriceFormat;

            decimal lowestOriginalRate = originalPrices.Count > 0 ? originalPrices.Last().Key : decimal.MaxValue;

            // Build the table of choices.
            var headerRow = new string[] { 
                "Rate"
                ,"Previous " + PointPriceStr
                , "Market " + PointPriceStr
                , "Re-Lock " + PointPriceStr + " Fee"
                , "Re-Lock "+ PointPriceStr + ( UseMarketPricing ? "" : "<br/>(Worst-Case + Fee)" )
                , "Action" };

            List<string[]> table = new List<string[]>();
            table.Add(headerRow);

            decimal lastSeenPoints = originalPrices.Count > 0 ? originalPrices.First().Value : 0;
            if (convertPreviousPoint) lastSeenPoints = ConvertPointBase(lastSeenPoints);
            
            foreach (var currentPrice in mergedResult.Reverse())
            {
                if (currentPrice.Key < lowestOriginalRate) continue;

                decimal newRate = currentPrice.Key;
                decimal previousPoints;
                if (!originalPrices.ContainsKey(currentPrice.Key))
                {
                    previousPoints = lastSeenPoints;
                }
                else
                {
                    previousPoints =
                         ( convertPreviousPoint ?
                        ConvertPointBase(originalPrices[newRate])
                        : originalPrices[newRate] ) + extraFees;

                }

                lastSeenPoints = previousPoints;
                
                decimal marketPoints = mergedResult[newRate].Item1 + extraFees;
                

                decimal relockFee = fee.Value;

                decimal relockPoint = UseMarketPricing ?
                    relockFee + marketPoints
                    : relockFee + Math.Max(previousPoints, marketPoints);

                string actionString = "re-lock"
                + ":" + ToRate(newRate)
                + ":" + ToRate(previousPoints)
                + ":" + ToRate(marketPoints)
                + ":" + ToRate(relockFee)
                + ":" + ToRate(relockPoint)
                + ":" + m_dataLoan.sLpTemplateId.ToString();

                table.Add(new string[] {
                    ToRate(newRate)
                    , ToPointRate(previousPoints)
                    , ToPointRate(marketPoints)
                    , ToRate(relockFee)
                    , ToPointRate(relockPoint)
                    , actionString
                });

            }

            if (table.Count == 1)
            {
                throw new NonCriticalAutoRateLockException("There are no re-lock options currently available for this file.");
            }

            return table;
        }

        protected override AutoRateLockResult ExecuteActionImpl(string action, string arguments)
        {
            // Do not trust the request coming in from UI.
            // Make sure the option they chose can still be done with the same result.
            // If they sat in the UI too long, something (lock policy, loan data, pricing) could have changed.
            // Or if they attempt to hack the UI to improve the terms of their lock...

            string[] args = arguments.Split(':');
            if (args.Length < 4)
                throw new CBaseException(ErrorMessages.Generic, "Missing arguments");

            bool foundMatch = false;
            foreach (var option in ResultTable)
            {
                string[] optionArgs = option[5].Split(':');
                if (optionArgs.Length >= 4)
                {
                    if (optionArgs[1] == args[0]    // Rate
                        && optionArgs[2] == args[1] // Prev. Point
                        && optionArgs[3] == args[2] // Markt. Point
                        && optionArgs[4] == args[3] // Fee
                        && optionArgs[5] == args[4] // Relock Point
                        && optionArgs[6] == args[5] // ProgramId
                        )
                    {
                        foundMatch = true;
                        break;
                    }
                }
            }

            if (foundMatch == false)
            {
                // The action being asked for is no longer allowed.
                throw new NonCriticalAutoRateLockException("Float down option requested is not available.");
            }

            decimal rate = decimal.Parse(args[0].Replace("%",""));
            decimal point = decimal.Parse(args[4].Replace("%",""));
            decimal fee = decimal.Parse(args[3].Replace("%", ""));
            decimal prevPoint = decimal.Parse(args[1].Replace("%", ""));
            decimal marketPoint = decimal.Parse(args[2].Replace("%", ""));

            Guid programId = new Guid(args[5]);
            string reason = Utilities.SafeHtmlString(args[6]);

            List<PricingAdjustment> adjustments = BuildAdjustmentList();

            adjustments.Add(new PricingAdjustment()
            {
                Description = "RE-LOCK FEE",
                Fee = ToRate(fee),
                IsHidden = false,
                IsLenderAdjustment = true,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = "0.000%",
                TeaserRate = "0.000%",
                IsPersist = true
            });

            if (UseMarketPricing == false && prevPoint > marketPoint)
            {
                adjustments.Add(new PricingAdjustment()
                {
                    Description = "WORST-CASE MARKET ADJUSTMENT",
                    Fee = ToRate(prevPoint - marketPoint),
                    IsHidden = false,
                    IsLenderAdjustment = true,
                    IsSRPAdjustment = false,
                    Margin = "0.000%",
                    Rate = "0.000%",
                    TeaserRate = "0.000%",
                    IsPersist = true
                });
            }

            adjustments.AddRange(m_dataLoan.sBrokerLockAdjustments.Where(a => a.Description.StartsWith("LOCK EXTENSION") || a.Description.StartsWith("FLOAT DOWN") || a.Description.StartsWith("RE-LOCK FEE")));

            CPageData data = new CAutoRelockData(m_loanId);

            data.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
            data.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.
            
            if (IncludeOriginatorComp) point -= m_dataLoan.sOriginatorCompPoint;

            data.ReLockRate(m_principal.DisplayName, reason, m_rateLockDays.Value, rate, point, adjustments);

            data.Save();

            SendNotificationEmail(E_AutoRateLockAction.ReLock, data);

            AutoRateLockResult result = new AutoRateLockResult()
            {
                ExpirationDate = data.sRLckdExpiredD_rep,
                Price = CApplicantRateOption.PointFeeInResult(m_dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, IncludeOriginatorComp ? data.m_convertLos.ToRateString(data.sOriginatorCompPoint + data.sBrokComp1Pc) : data.sBrokComp1Pc_rep),
                TotalEvents = data.sTotalRateReLocks_rep,
                Rate = data.sNoteIR_rep
            };

            return result;
        }

        protected override void VerifyActionImpl()
        {
            if (sLockPolicy.EnableAutoReLocks == false)
            {
                throw new NonCriticalAutoRateLockException("Re-Locks are not allowed.");
            }

            if (m_dataLoan.sTotalRateReLocks >= sLockPolicy.MaxReLocks)
            {
                string msg = "This loan file has already had " + m_dataLoan.sTotalRateReLocks_rep + " Re-Locks. Additional Re-Locks are not allowed.";
                throw new NonCriticalAutoRateLockException(msg);
            }
        }

        protected override LoanProgramByInvestorSet GetProgramsToRun()
        {
            VerifySubmittedCert();

            if (sLockPolicy.ReLockRequireSameInvestor) return base.GetProgramsToRun();

            LoanProgramByInvestorSet set = new LoanProgramByInvestorSet();
            List<TemplateRowData> programs = new List<TemplateRowData>();

            foreach (TemplateRowData row in TemplateRowData.List(DataSrc.LpeSrc, m_dataLoan.BrokerDB, sPriceGroup.ID))
            {
                programs.Add(row);
            }

            RateMergeGroupKey mergeGroupKey = null;
            foreach (var program in programs)
            {
                if (program.lLpTemplateId == m_dataLoan.sLpTemplateId)
                {
                    mergeGroupKey = program.Key;
                }
            }

            if (mergeGroupKey == null)
            {
                // This could happen if the program was removed from the pricegroup after it was registered.
                throw new CBaseException(ErrorMessages.Generic,
                    "Unable to find registered program");
            }

            string registeredProgramGroupName = new RateMergeGroup(mergeGroupKey).Name;

            foreach (var program in programs)
            {
               string programGroup = new RateMergeGroup(program.Key).Name;

               if (programGroup == registeredProgramGroupName)
                {
                    set.Add(program.lLpTemplateId, program.lLpTemplateNm, program.productCode);
                }
            }
            return set;
            
        }

        // 
        private SortedDictionary<decimal, Tuple<decimal, Guid>> GetMergedResult()
        {
            SortedDictionary<decimal, Tuple<decimal, Guid>> result
                = new SortedDictionary<decimal, Tuple<decimal, Guid>>();

            if (sLockPolicy.ReLockRequireSameInvestor)
            {

                if (ProgramResult == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "No Pricing Results for program.");
                }

                // Means we only priced for the currently registered program.
                // Set up a list that is only the current program.
                foreach (CApplicantRateOption option in ProgramResult.ApplicantRateOptions)
                {
                    if ( option.IsDisqualified == false)
                        result.Add(option.Rate, Tuple.Create(
                            IncludeOriginatorComp ? option.PointIncludingOriginatorComp : option.Point_
                            , option.LpTemplateId));
                }
            }
            else
            {

                // We have to merge the result to know which wins at which rates
                // By merging the rates.

                RateMergeBucket rateMergeBucket = new RateMergeBucket(m_dataLoan.BrokerDB.IsShowRateOptionsWorseThanLowerRate);

                foreach (var priceResult in m_resultItem.Results)
                {
                    if (priceResult is CApplicantPriceXml)
                    {
                        var programResult = priceResult as CApplicantPriceXml;
                        if ( programResult.Status == E_EvalStatus.Eval_Eligible )
                            rateMergeBucket.Add(priceResult as CApplicantPriceXml);
                    }
                }

                RateMergeGroup keyGroup = null;

                if (rateMergeBucket.RateMergeGroups.Count == 1)
                {
                    // Expected case.
                    keyGroup = rateMergeBucket.RateMergeGroups.OfType<RateMergeGroup>().FirstOrDefault();
                }
                else
                {
                    foreach (RateMergeGroup group in rateMergeBucket.RateMergeGroups)
                    {
                        if (keyGroup == null || group.RateOptions.Count > keyGroup.RateOptions.Count)
                            keyGroup = group;
                    }
                }

                if (keyGroup != null)
                {
                    foreach (CApplicantRateOption option in keyGroup.RateOptions)
                    {
                        if ( option.IsDisqualified == false)
                            result.Add(option.Rate, Tuple.Create(
                                IncludeOriginatorComp ? option.PointIncludingOriginatorComp : option.Point_
                                , option.LpTemplateId));
                    }
                }
            }
            return result;
        }


        // This is private for a reason--this data object bypasses the write permission checking.
        // But the containing class checks for the appropriate lock permission.  If making this
        // public or moving it outside, please be sure you are checking permissions appropriately.
        private class CAutoRelockData : CPageData
        {
            private static CSelectStatementProvider s_selectProvider;
            static CAutoRelockData()
            {
                StringList list = new StringList();

                list.Add("sfReLockRate");

                s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

            }

            public CAutoRelockData(Guid fileId)
                : base(fileId, "CAutoRelockData", s_selectProvider)
            {
            }

            protected override bool m_enforceAccessControl
            {
                get
                {
                    // Have to do this bypass.  It is often the case that the user 
                    // doing locking action does not have rights to edit because of the lock.
                    // Having the lock action permission overrides it.
                    return false;
                }
            }

        }
    }
}