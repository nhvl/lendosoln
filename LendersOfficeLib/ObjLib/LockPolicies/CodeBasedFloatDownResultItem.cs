﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class CodeBasedFloatDownResultItem : AbstractCodeBasedPolicyResultItem
    {
        public CodeBasedFloatDownResultItem(string encryptedData) : base(encryptedData)
        {
        }
        public CodeBasedFloatDownResultItem(Guid loanId, Guid userId, decimal newRate, decimal originalBasePoint, decimal newBasePoint, decimal totalAdjustmentFee, decimal finalBasePoint)
            : base(loanId, userId)
        {
            this.NewRate = newRate;
            this.OriginalBasePoint = originalBasePoint;
            this.NewBasePoint = newBasePoint;
            this.TotalAdjustmentFee = totalAdjustmentFee;
            this.FinalBasePoint = finalBasePoint;
        }
        public decimal NewRate { get; private set; }
        public decimal OriginalBasePoint { get; private set; }
        public decimal NewBasePoint { get; private set; }
        public decimal FinalBasePoint { get; private set; }

        public decimal TotalAdjustmentFee { get; private set; }

        public decimal OriginalPoint
        {
            get { return OriginalBasePoint + TotalAdjustmentFee; }
        }
        public decimal NewPoint
        {
            get { return NewBasePoint + TotalAdjustmentFee; }
        }

        public decimal MarketImprovementPoint
        {
            get 
            {
                return OriginalPoint - NewPoint; 
            }
        }
        public decimal FinalPoint
        {
            get 
            {
                if (FinalBasePoint != decimal.MinValue)
                {
                    return FinalBasePoint + TotalAdjustmentFee;
                }
                return decimal.MinValue;
            }
        }

        public decimal RolldownFee
        {
            get 
            {
                if (FinalBasePoint != decimal.MinValue)
                {
                    return OriginalBasePoint - FinalBasePoint;
                }
                return decimal.MinValue;
            }
        }


        protected override IEnumerable<string> BuildDataList()
        {
            yield return NewRate.ToString(); // 0
            yield return OriginalBasePoint.ToString(); // 1
            yield return NewBasePoint.ToString(); // 2
            yield return TotalAdjustmentFee.ToString(); // 3
            yield return FinalBasePoint.ToString(); // 4
        }
        protected override void ParseData(string[] parts)
        {
            NewRate = decimal.Parse(parts[0]);
            OriginalBasePoint = decimal.Parse(parts[1]);
            NewBasePoint = decimal.Parse(parts[2]);
            TotalAdjustmentFee = decimal.Parse(parts[3]);
            FinalBasePoint = decimal.Parse(parts[4]);
        }

    }
}
