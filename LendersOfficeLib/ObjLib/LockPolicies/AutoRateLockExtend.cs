﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class AutoRateLockExtend : AutoRateLock
    {

        protected override WorkflowOperation WorkflowOperation
        {
            get { return WorkflowOperations.ExtendLock; }
        }
        protected override Func<bool> ActionAllowed
        {
            get { return () => m_dataLoan.sIsRateLockExtentionAllowed; }
        }

        protected override string ActionFriendlyName
        {
            get { return "Extension"; }
        }

        public AutoRateLockExtend(Guid loanId)
            : base(loanId)
        {
        }

        protected override List<string[]> BuildResultTable()
        {
            if (ProgramResult == null || ProgramResult.Status != E_EvalStatus.Eval_Eligible)
            {
                throw new NonCriticalAutoRateLockException("No Pricing Results for program.");
            }

            decimal currentRate = m_dataLoan.sNoteIR;
            decimal currentPointWithoutLockFees = m_dataLoan.sPointLessLockFees;
            decimal currentPoint = m_dataLoan.sBrokComp1Pc;
            if (IncludeOriginatorComp)
            {
                currentPoint += m_dataLoan.sOriginatorCompPoint;
                currentPointWithoutLockFees += m_dataLoan.sOriginatorCompPoint;
            }

            DateTime currentExpirationDate = m_dataLoan.sRLckdExpiredD.DateTimeForComputation;
            CApplicantRateOption newOptionAtCurrentRate = null;

            if (ProgramResult.Status == E_EvalStatus.Eval_Eligible)
            {
                foreach (var option in ProgramResult.ApplicantRateOptions)
                {
                    if (option.Rate == currentRate && option.IsDisqualified == false)
                    {
                        newOptionAtCurrentRate = option;
                        break;
                    }
                }
            }

            string marketType;
            AutoRateLockExtendData worstRowData = null;
            bool showCalendarDays = m_dataLoan.BrokerDB.RateLockExpirationWeekendHolidayBehavior != E_RateLockExpirationWeekendHolidayBehavior.AllowWeekendHoliday;
            bool autoExtensionForceWorstOptionForLockExtensionDateTies = m_dataLoan.BrokerDB.AutoExtensionForceWorstOptionForLockExtensionDateTies;

            if (newOptionAtCurrentRate != null)
            {

                decimal newPoint;
                if (IncludeOriginatorComp)
                    newPoint = newOptionAtCurrentRate.PointIncludingOriginatorComp;
                else
                    newPoint = newOptionAtCurrentRate.Point_;

                if (newPoint < currentPointWithoutLockFees)
                {
                    marketType = "improving";
                }
                else if (newPoint == currentPointWithoutLockFees)
                {
                    marketType = "static";
                }
                else
                {
                    marketType = "worsening";
                    decimal worseningAmt = newPoint - currentPointWithoutLockFees;
                    if (worseningAmt > sLockPolicy.LockExtensionMarketWorsenPoints)
                    {
                        // "The lender can also set a threshold, so that if the market worsens by at least that amount 
                        // then the user will only have one option for the extension period 
                        // (the amount of days remaining in their extension cap), 
                        // and the extension fee is equal to the market worsening."

                        int worstDays = sLockPolicy.MaxTotalLockExtensionDays - m_dataLoan.sTotalDaysLockExtended;

                        decimal worstNewFinalPoints = worseningAmt + currentPoint;
                        CDateTime worstNewExpirationDate = m_dataLoan.CalculateRateLockExtensionExpirationDate(m_dataLoan.sRLckdExpiredD, worstDays);

                        int calendarDays = worstNewExpirationDate.DateTimeForComputation.Subtract(m_dataLoan.sRLckdExpiredD.DateTimeForComputation).Days;

                        if (showCalendarDays)
                        {
                            worstRowData = new AutoRateLockExtendData(worstDays, worseningAmt, worstNewFinalPoints, worstNewExpirationDate, calendarDays, showCalendarDays: true);
                        }
                        else
                        {
                            worstRowData = new AutoRateLockExtendData(worstDays, worseningAmt, worstNewFinalPoints, worstNewExpirationDate, -1, showCalendarDays: false);
                        }
                    }
                }
            }
            else
            {
                marketType = "worsening";
            }

            string[] headerRow = null; 
            if (showCalendarDays)
            {
                headerRow = new string[] { "Available Extension", "Extension Fee (" + PointPriceStr + ")", "New Final " + PointPriceStr, "New Expiration Date", "Calendar Days", "Action" };
            }
            else
            {
                headerRow = new string[] { "Available Extension", "Extension Fee (" + PointPriceStr + ")", "New Final " + PointPriceStr, "New Expiration Date", "Action" };
            }

            List<AutoRateLockExtendData> rateLockExtendRows = new List<AutoRateLockExtendData>();

            if (worstRowData == null)
            {
                bool cappedExtension = false;
                int daysCanBeExtended = sLockPolicy.MaxTotalLockExtensionDays - m_dataLoan.sTotalDaysLockExtended;
                int? previousDays = null;
                if (daysCanBeExtended > 0)
                {
                    foreach (var policyOption in sLockPolicy.LockExtensionOptionTable)
                    {
                        int extensionLength = policyOption.ExtensionLength;

                        // If the full extension of this option will be beyond the cap,
                        // show it as the maximum days allowed.
                        if (extensionLength > daysCanBeExtended)
                        {
                            if (cappedExtension) continue;

                            extensionLength = daysCanBeExtended;
                            cappedExtension = true;

                            if (previousDays.HasValue && extensionLength <= previousDays.Value)
                                continue;
                        }
                        previousDays = extensionLength;
                        string extensionLengthStr = extensionLength.ToString() + " days";

                        decimal extensionFee;
                        switch (marketType)
                        {
                            case "improving": extensionFee = policyOption.FeeImproving; break;
                            case "static": extensionFee = policyOption.FeeStatic; break;
                            case "worsening": extensionFee = policyOption.FeeWorsening; break;
                            default:
                                throw new CBaseException(ErrorMessages.Generic, "Unknown market type");
                        }

                        decimal newFinalPoints = currentPoint + extensionFee;

                        CDateTime newExpirationDate = null;

                        if (cappedExtension) // if extension date is capped, use the capped date to calculate the new expiration date. 
                        {
                            newExpirationDate = m_dataLoan.CalculateRateLockExtensionExpirationDate(m_dataLoan.sRLckdExpiredD, extensionLength);
                        }
                        else 
                        {
                            newExpirationDate = m_dataLoan.CalculateRateLockExtensionExpirationDate(m_dataLoan.sRLckdExpiredD, policyOption.ExtensionLength);
                        }

                        int calendarDays = newExpirationDate.DateTimeForComputation.Subtract(m_dataLoan.sRLckdExpiredD.DateTimeForComputation).Days;

                        AutoRateLockExtendData currentRow = null;

                        if (showCalendarDays)
                        {
                            currentRow = new AutoRateLockExtendData(extensionLength, extensionFee, newFinalPoints, newExpirationDate, calendarDays, showCalendarDays: true);
                        }

                        else
                        {
                            currentRow = new AutoRateLockExtendData(extensionLength, extensionFee, newFinalPoints, newExpirationDate, -1, showCalendarDays: false);
                        }                        

                        rateLockExtendRows.Add(currentRow);
                    }
                }
            }
            else
            {
                rateLockExtendRows.Add(worstRowData);
            }            

            if (rateLockExtendRows.Count == 0) 
            {
                throw new NonCriticalAutoRateLockException("There are no extend options currently available for this file.");
            }

            List<string[]> table = new List<string[]>();
            table.Add(headerRow);

            if (worstRowData != null)
            {
                table.Add(worstRowData.GetStringTableRow(m_dataLoan.m_convertLos, m_dataLoan.sPriceGroup));
            }
            else
            {
                if (showCalendarDays)
                {
                    foreach (var sameExpirationDates in rateLockExtendRows.GroupBy(x => x.NewExpirationDate).Where(x => x.Count() > 1))
                    {
                        decimal newFinalPoints;

                        if (autoExtensionForceWorstOptionForLockExtensionDateTies)
                        {
                            newFinalPoints = sameExpirationDates.Max(x => x.NewFinalPoints);
                        }
                        else
                        {
                            newFinalPoints = sameExpirationDates.Min(x => x.NewFinalPoints);
                        }

                        var sameExpirationDatesWithMatchingPoints = sameExpirationDates.Where(x => x.NewFinalPoints == newFinalPoints);

                        if (sameExpirationDatesWithMatchingPoints.Count() != 1)
                        {
                            int minDaysBetweenExtensionAndCalendarDays = sameExpirationDatesWithMatchingPoints.Min(x => x.DaysBetweenExtensionAndCalendarDays);

                            foreach (var disabledRow in sameExpirationDatesWithMatchingPoints.Where(x => x.DaysBetweenExtensionAndCalendarDays != minDaysBetweenExtensionAndCalendarDays))
                            {
                                disabledRow.IsActionEnabled = false;
                            }
                        }

                        foreach (var disabledRow in sameExpirationDates.Where(x => x.NewFinalPoints != newFinalPoints))
                        {
                            disabledRow.IsActionEnabled = false;
                        }
                    }
                }

                foreach (var row in rateLockExtendRows)
                {
                    table.Add(row.GetStringTableRow(m_dataLoan.m_convertLos, m_dataLoan.sPriceGroup));
                }
            }            

            return table;
        }
        
        protected override AutoRateLockResult ExecuteActionImpl(string action, string arguments)
        {

            // Do not trust the request coming in from UI.
            // Make sure the option they chose can still be done with the same result.
            // If they sat in the UI too long, something (lock policy, loan data, pricing) could have changed.
            // Or if they attempt to hack the UI to improve the terms of their lock...

            string[] args = arguments.Split(':');
            if (args.Length < 5)
            {
                throw new CBaseException(ErrorMessages.Generic, "Missing arguments");
            }

            decimal selectedFee = decimal.Parse(args[1].Replace("%", string.Empty));
            bool showCalendarDays = m_dataLoan.BrokerDB.RateLockExpirationWeekendHolidayBehavior != E_RateLockExpirationWeekendHolidayBehavior.AllowWeekendHoliday;
            bool autoExtensionForceWorstOptionForLockExtensionDateTies = m_dataLoan.BrokerDB.AutoExtensionForceWorstOptionForLockExtensionDateTies;
            bool foundMatch = false;
            string selectedDay = showCalendarDays ? args[4] : string.Empty;
            foreach (var option in ResultTable)
            {
                string[] optionArgs = null;
                if (showCalendarDays)
                {
                    optionArgs = option[5].Split(':');
                }
                else
                {
                    optionArgs = option[4].Split(':');
                }
                if (optionArgs.Length >= 5)
                {
                    if (showCalendarDays)
                    {
                        if (args[4] == "0 days") // Calendar Days
                        {
                            foundMatch = false;
                            break;
                        }

                        if (optionArgs[1] != args[0]    // Days
                           && optionArgs[2] != args[1]     // Fee
                           && optionArgs[3] != args[2]     // New Point
                           && optionArgs[4] == args[3]     // New Expiration
                           && optionArgs[5] == selectedDay) // Calendar Days 
                        {
                            decimal validFee = decimal.Parse(optionArgs[2].Replace("%", string.Empty));

                            if (!autoExtensionForceWorstOptionForLockExtensionDateTies)
                            {
                                // user somehow selected a more expensive option even though their calendar days are the same.
                                if (validFee < selectedFee)
                                {
                                    foundMatch = false;
                                    break;
                                }
                            }
                            else
                            {
                                // user somehow selected a cheaper option even though their calendar days are the same.
                                if (validFee > selectedFee)
                                {
                                    foundMatch = false;
                                    break;
                                }
                            }
                        }

                        if (optionArgs[1] == args[0] // Days
                        && optionArgs[2] == args[1]  // Fee
                        && optionArgs[3] == args[2]  // New Point
                        && optionArgs[4] == args[3]) // New Expiration
                        {
                            foundMatch = true; // the option the user selected is a valid option, however, 
                                               // keep looping to see if there's a cheaper option with the same extension date
                        }
                    }
                    else
                    {
                        if (optionArgs[1] == args[0] // Days
                        && optionArgs[2] == args[1]  // Fee
                        && optionArgs[3] == args[2]  // New Point
                        && optionArgs[4] == args[3]) // New Expiration
                        {
                            foundMatch = true;
                            break;
                        }
                    }                    
                }                
            }

            if (foundMatch == false)
            {
                // The action being asked for is no longer allowed.  Should be rare--
                // probably from leaving UI open long enough for pricing to change.
                throw new NonCriticalAutoRateLockException("Action requested is not allowed");
            }

            int days = int.Parse(args[0]);
            string fee = args[1].Replace("%", "");
            string reason = showCalendarDays ? Utilities.SafeHtmlString(args[5]) : Utilities.SafeHtmlString(args[4]);

            CPageData data = new CAutoExtendData(m_loanId);
            data.ByPassFieldSecurityCheck = true;  // CAREFUL.  Have to do this because users without permission to see the lock desk folder will need to use this feature.
            data.InitSave(Constants.ConstAppDavid.SkipVersionCheck);


            PricingAdjustment newAdjustment = new PricingAdjustment()
            {
                Description = "LOCK EXTENSION - " + days + " DAYS",
                Fee = fee.ToString(),
                IsHidden = false,
                IsLenderAdjustment = true,
                IsSRPAdjustment = false,
                Margin = "0.000%",
                Rate = "0.000%",
                TeaserRate = "0.000%",
                IsPersist = true
            };

            data.ExtendRateLock(m_principal.DisplayName, reason, days, newAdjustment);

            data.Save();

            SendNotificationEmail(E_AutoRateLockAction.Extend, data);

            AutoRateLockResult result = new AutoRateLockResult()
            {
                ExpirationDate = data.sRLckdExpiredD_rep,
                Price = ToPointRate( data.sBrokComp1Pc),
                TotalEvents = data.sTotalLockExtensions_rep
            };
            return result;
        }

        protected override void VerifyActionImpl()
        {
            if (sLockPolicy.EnableAutoLockExtensions == false)
            {
                throw new NonCriticalAutoRateLockException("Lock extensions are not allowed.");
            }


            if (m_dataLoan.sTotalLockExtensions >= sLockPolicy.MaxLockExtensions)
            {
                string msg = "This loan file has already been extended " + m_dataLoan.sTotalLockExtensions_rep + " times. Additional extensions are not allowed.";
                throw new NonCriticalAutoRateLockException(msg);
            }

            if (m_dataLoan.sTotalDaysLockExtended >= sLockPolicy.MaxTotalLockExtensionDays)
            {
                string msg = "This loan file has already been extended by " + m_dataLoan.sTotalDaysLockExtended_rep + " days. "
                    + "Additional extension of the lock expiration date is not allowed. For additional information, please contact your lock desk directly.";
                throw new NonCriticalAutoRateLockException(msg);
            }
        }

        // This is private for a reason--this data object bypasses the write permission checking.
        // But the containing class checks for the appropriate lock permission.  If making this
        // public or moving it outside, please be sure you are checking permissions appropriately.
        private class CAutoExtendData : CPageData
        {
            private static CSelectStatementProvider s_selectProvider;
            static CAutoExtendData()
            {
                StringList list = new StringList();

                list.Add("sfExtendRateLock");

                s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

            }

            public CAutoExtendData(Guid fileId)
                : base(fileId, "CAutoExtendData", s_selectProvider)
            {
            }

            protected override bool m_enforceAccessControl
            {
                get
                {
                    // Have to do this bypass.  It is often the case that the user 
                    // doing locking action does not have rights to edit because of the lock.
                    // Having the lock action permission overrides it.
                    return false;
                }
            }

        }

    }
}
