﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class CodeBasedLockExtensionResultItem : AbstractCodeBasedPolicyResultItem
    {
        public int Days { get; private set; }
        public decimal Fee { get; private set; }

        public string NewExpirationDate { get; private set; }
        public decimal FinalPoint { get; private set; }



        public CodeBasedLockExtensionResultItem(string encryptedData) : base(encryptedData)
        {
        }
        public CodeBasedLockExtensionResultItem(Guid sLId, Guid userId, int days, decimal fee) : base (sLId, userId)
        {
            this.Days = days;
            this.Fee = fee;

        }

        public void SetFinalData(CDateTime newExpirationDate, decimal finalPoint, LosConvert converter)
        {
            this.NewExpirationDate = newExpirationDate.ToString(converter);
            this.FinalPoint = finalPoint;
        }

        protected override IEnumerable<string> BuildDataList()
        {
            yield return Days.ToString(); // 0
            yield return Fee.ToString(); // 1
        }

        protected override void ParseData(string[] parts)
        {
            Days = int.Parse(parts[0]);
            Fee = decimal.Parse(parts[1]);
        }
    }
}
