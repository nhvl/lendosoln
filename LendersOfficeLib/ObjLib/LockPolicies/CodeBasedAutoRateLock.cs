﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.RatePrice;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.LockPolicies
{
    public class CodeBasedAutoRateLock : AutoRateLock
    {


        public E_AutoRateLockAction LockAction { get; private set; }
        public AbstractCodeBasedLockPolicy LockPolicy { get; private set;}

        private UnderwritingResultItem m_historicalPricingResult = null;
        private UnderwritingResultItem m_currentPricingResult = null;

        public CodeBasedAutoRateLock(E_AutoRateLockAction lockAction, Guid sLId, AbstractCodeBasedLockPolicy codeBasedLockPolicy)
            : base(sLId)
        {
            LockAction = lockAction;
            LockPolicy = codeBasedLockPolicy;
        }
        protected override WorkflowOperation WorkflowOperation
        {
            get 
            {
                switch (LockAction)
                {
                    case E_AutoRateLockAction.Extend:
                        return WorkflowOperations.ExtendLock; 
                    case E_AutoRateLockAction.FloatDown:
                        return WorkflowOperations.FloatDownLock;
                    case E_AutoRateLockAction.ReLock:
                        return WorkflowOperations.RateReLock;
                    default:
                        throw new UnhandledEnumException(LockAction);
                }
            }
        }

        protected override void VerifyActionImpl()
        {
            // NO-OP
        }

        public override string CreatePricingRequest()
        {

            // Need to call VerifyAction();
            Guid sProdLpePriceGroupId = Guid.Empty;
            LoanProgramByInvestorSet lpSet = LockPolicy.GetProgramsToRun(LockAction, m_loanId, out sProdLpePriceGroupId);
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(m_principal.BrokerDB);
            Guid currentSnapshotRequestId = DistributeUnderwritingEngine.SubmitToEngineForAutoLock(
    m_principal
    , m_loanId
    , lpSet
    , E_sLienQualifyModeT.ThisLoan
    , sProdLpePriceGroupId
    , m_priceMode
    , m_rateLockDays
    , options
    );

            Guid historicalRequestId = DistributeUnderwritingEngine.SubmitToEngineForAutoLock(
    m_principal
    , m_loanId
    , lpSet
    , E_sLienQualifyModeT.ThisLoan
    , sProdLpePriceGroupId
    , m_priceMode
    , m_rateLockDays
    , LockPolicy.GetHistoricalPricingSettings() 
    );

            return currentSnapshotRequestId.ToString() + ":" + historicalRequestId;
        }

        public override bool IsResultReady(string requestId)
        {
            // 1/10/2014 dd - RequestId has this format:  {currentSnapshotRequestId}:{historicalRequestId}
            string[] parts = requestId.Split(':');

            Guid currentSnapshotRequestId = new Guid(parts[0]);
            Guid historicalRequestId = new Guid(parts[1]);

            if (LpeDistributeResults.IsResultAvailable(currentSnapshotRequestId) == false)
            {
                return false;
            }

            if (LpeDistributeResults.IsResultAvailable(historicalRequestId) == false)
            {
                return false;
            }

            m_currentPricingResult = LpeDistributeResults.RetrieveResultItem(currentSnapshotRequestId);

            if (m_currentPricingResult == null)
            {
                return false;
            }

            m_historicalPricingResult = LpeDistributeResults.RetrieveResultItem(historicalRequestId);
            if (m_historicalPricingResult == null)
            {
                return false;
            }

            return true;
        }

        public override bool HasResult
        {
            get
            {
                return m_currentPricingResult != null && m_historicalPricingResult != null;
            }
        }
        protected override AutoRateLock.AutoRateLockResult ExecuteActionImpl(string action, string args)
        {
            return LockPolicy.ExecuteCommand(m_loanId, LockAction, args, m_currentPricingResult, m_historicalPricingResult);
        }

        protected override List<string[]> BuildResultTable()
        {
            return LockPolicy.BuildResultTable(m_loanId, LockAction, m_currentPricingResult, m_historicalPricingResult);
        }




        protected override Func<bool> ActionAllowed
        {
            get 
            {
                switch (LockAction)
                {
                    case E_AutoRateLockAction.Extend:
                        return () => LockPolicy.IsAllowLockExtension(m_loanId); 
                    case E_AutoRateLockAction.FloatDown:
                        return () => LockPolicy.IsAllowFloatDown(m_loanId); 
                    case E_AutoRateLockAction.ReLock:
                        return () => LockPolicy.IsAllowRelock(m_loanId); 
                    default:
                        throw new UnhandledEnumException(LockAction);
                }
                
            }
        }

        protected override string ActionFriendlyName
        {
            get 
            { 
                switch (LockAction) 
                {
                    case E_AutoRateLockAction.Extend:
                        return "Extension"; 
                    case E_AutoRateLockAction.FloatDown:
                        return "Float Down";
                    case E_AutoRateLockAction.ReLock:
                        return "Re-Lock";
                    default:
                        throw new UnhandledEnumException(LockAction);
                }
                
            }
        }
    }
}
