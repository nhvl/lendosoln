﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    /// <summary>
    /// Loan updater actions as an enum.
    /// </summary>
    public enum LoanUpdaterAction
    {
        /// <summary>
        /// Action for updating the loan officer license info.
        /// </summary>
        UpdateLicenseInfoForLoanOfficer = 0
    }
}
