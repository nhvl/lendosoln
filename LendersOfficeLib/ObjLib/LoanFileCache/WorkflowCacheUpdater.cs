﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    using System;
    using System.Collections.Generic;
    using System.Messaging;
    using System.Xml.Linq;
    using CommonProjectLib.Common;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Updates loans' cache in order to keep workflow up to date.
    /// </summary>
    public class WorkflowCacheUpdater : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Maps an enum value to the field name.
        /// </summary>
        public static readonly Dictionary<NonLoanInfoDependentLoanFields, string> EnumValueToStringField = new Dictionary<NonLoanInfoDependentLoanFields, string>()
        {
            { NonLoanInfoDependentLoanFields.sPmlBrokerStatusT, nameof(CPageData.sPmlBrokerStatusT) },
            { NonLoanInfoDependentLoanFields.sLinkedLoanName, nameof(CPageData.sLinkedLoanName) }
        };

        /// <summary>
        /// Gets the description for this IRunnable.
        /// </summary>
        /// <value>The description for this IRunnable.</value>
        public string Description
        {
            get
            {
                return "Updates the cache for loans in order to keep workflow up to date. This will only pull from the PriorityLoanUpdater MSMQ.";
            }
        }

        /// <summary>
        /// Adds a message to the loan cache updater queue. 
        /// </summary>
        /// <param name="loanId">The loan id in the cache to update.</param>
        /// <param name="fieldsToUpdate">The fields to update. Can be any combination of the enums.</param>
        public static void SubmitToUpdateCacheFields(Guid loanId, NonLoanInfoDependentLoanFields fieldsToUpdate)
        {
            string connectionString = ConstStage.PriorityLoanUpdaterQueueConnectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Skipping.");
                return;
            }

            XDocument message = new XDocument();
            XElement root = new XElement("root");
            root.SetAttributeValue("s", loanId.ToString());
            root.SetAttributeValue("f", fieldsToUpdate.ToString("d"));
            message.Add(root);

            Drivers.Gateways.MessageQueueHelper.SendXML(connectionString, null, message);
        }

        /// <summary>
        /// Pulls the fields to update from the root element of the message.
        /// </summary>
        /// <param name="messageRoot">The root element of the message.</param>
        /// <returns>The field ids if they are specified. Null if not.</returns>
        public static string[] GetFieldsToUpdateFromMessage(XElement messageRoot)
        {
            NonLoanInfoDependentLoanFields fieldsToUpdate;
            if (Enum.TryParse<NonLoanInfoDependentLoanFields>(messageRoot.Attribute("f").Value, out fieldsToUpdate))
            {
                List<string> fieldList = new List<string>();

                foreach (NonLoanInfoDependentLoanFields enumVal in Enum.GetValues(typeof(NonLoanInfoDependentLoanFields)))
                {
                    if (fieldsToUpdate.HasFlag(enumVal))
                    {
                        fieldList.Add(EnumValueToStringField[enumVal]);
                    }
                }

                return fieldList.ToArray();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Checks the PriorityUpdater queue and updates the cache fields specified in the message.
        /// </summary>
        public void Run()
        {
            string connectionString = ConstStage.PriorityLoanUpdaterQueueConnectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Please check if PriorityLoanUpdaterQueueConnectionString is specified.");
                return;
            }

            using (var queue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(connectionString, false))
            {
                long processedLoans = 0;
                while (true)
                {
                    try
                    {
                        DateTime arrivalTime;
                        XDocument xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);
                        if (xdoc == null)
                        {
                            break; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                        }

                        XElement root = xdoc.Root;

                        Guid loanId = new Guid(root.Attribute("s").Value);
                        string[] fields = GetFieldsToUpdateFromMessage(root);

                        if (fields == null || fields.Length == 0)
                        {
                            continue;
                        }

                        try
                        {
                            DataAccess.Tools.UpdateCacheTable(loanId, fields);

                            if (processedLoans % 1000 == 0)
                            {
                                DataAccess.Tools.LogInfo("[PriorityUpdater] Updated " + processedLoans + " loans.");
                            }

                            processedLoans++;
                        }
                        catch (Exception e)
                        {
                            DataAccess.Tools.LogError("[PriorityUpdater] slid=" + loanId, e);
                            throw;
                        }
                    }
                    catch (MessageQueueException e)
                    {
                        if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                        {
                            break;
                        }

                        throw;
                    }
                }
            }
        }
    }
}
