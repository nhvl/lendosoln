﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    using System;

    /// <summary>
    /// These fields are those that require non-loan info (like broker data, employee data) in order to calculate.
    /// </summary>
    [Flags]
    public enum NonLoanInfoDependentLoanFields
    {
        /// <summary>
        /// Will change if PmlBroker.BrokerRoleStatusT, MiniCorrRoleStatusT, or CorrRoleStatusT changes.
        /// </summary>
        sPmlBrokerStatusT = 1 << 0,

        /// <summary>
        /// Used to update linked loan name on linked loan after SetsLNmWithPermissionBypass is called.
        /// </summary>
        sLinkedLoanName = 1 << 1,
    }
}
