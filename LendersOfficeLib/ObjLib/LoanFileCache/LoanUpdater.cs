﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Messaging;
    using System.Threading;
    using System.Xml.Linq;
    using CommonProjectLib.Common;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using StatusEvents;

    /// <summary>
    /// 1/26/2018 - dd - This class will be remove once LoanUpdaterCancellableRunnable is verify on production.
    /// DO NOT UPDATE this class.
    /// </summary>
    public class LoanUpdater : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Queues up the loans to update the Loan Officer's licenses.
        /// Note, the updater will not update a loan if <see cref="DataAccess.Tools.IsInactiveStatus(E_sStatusT)"/> returns false for its sStatusT.
        /// </summary>
        /// <param name="loanIds">The loans to queue up.</param>
        /// <param name="principal">The user who triggered the license change.</param>
        public static void QueueLoansForLOLicenseUpdate(IEnumerable<Guid> loanIds, AbstractUserPrincipal principal)
        {
            if (loanIds == null || !loanIds.Any())
            {
                return;
            }

            XDocument message = new XDocument();
            XElement root = new XElement("root");
            root.SetAttributeValue("a", LoanUpdaterAction.UpdateLicenseInfoForLoanOfficer.ToString("D"));
            message.Add(root);
            if (principal != null && 
                (string.Equals(principal.Type, "b", StringComparison.OrdinalIgnoreCase) || string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase)))
            {
                root.Add(new XElement("p",
                                      new XAttribute("u", principal.UserId.ToString()),
                                      new XAttribute("b", principal.BrokerId.ToString()),
                                      new XAttribute("t", principal.Type)));
            }

            string connectionString = ConstStage.LoanFileCacheQueueConnectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Skipping.");
                return;
            }

            MessageQueue queue = new MessageQueue(connectionString);
            queue.Formatter = new XDocumentMessageFormater();

            foreach (Guid loanId in loanIds)
            {
                root.SetAttributeValue("s", loanId.ToString());
                queue.Send(message);
            }
        }

        #region IRunnable Members

        public string Description
        {
            get
            {
                return "Updates loan file cache for loans whose id is in LoanFileCacheQueueConnectionString (MSMQ). The entry contains the files that need updating.";
            }
        }

        public void Run()
        {
            string cacheUpdaterQueueConnectionString = ConstStage.LoanFileCacheQueueConnectionString;
            if (string.IsNullOrEmpty(cacheUpdaterQueueConnectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Please check if LoanFileCacheQueueConnectionString is set.");
                return;
            }

            string workflowQueueConnectionString = ConstStage.PriorityLoanUpdaterQueueConnectionString;
            if (string.IsNullOrEmpty(workflowQueueConnectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Please check if PriorityLoanUpdaterQueueConnectionString is set.");
                return;
            }

            using (var cacheUpdaterQueue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(cacheUpdaterQueueConnectionString, false))
            {
                using (var workflowQueue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(workflowQueueConnectionString, false))
                {
                    long processedLoans = 0;
                    while (true)
                    {
                        try
                        {
                            // We want to prioritize the normal loan updater queue in this IRunnable. 
                            // If we don't find any messages there, we go ahead and check the workflow updater queue.
                            XDocument xdoc = null;
                            DateTime arrivalTime;
                            try
                            {
                                xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(cacheUpdaterQueue, null, out arrivalTime);
                                if (xdoc == null)
                                {
                                    xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(workflowQueue, null, out arrivalTime); // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                                    if (xdoc == null)
                                    {
                                        break; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                                    }
                                }
                            }
                            catch (MessageQueueException exc)
                            {
                                if (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                                {
                                    xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(workflowQueue, null, out arrivalTime);
                                }
                                else
                                {
                                    throw;
                                }
                            }

                            XElement root = xdoc.Root;

                            string[] fields;
                            Guid sLId = new Guid(root.Attribute("s").Value);
                            Guid brokerId = Guid.Empty;

                            // The 'f' attribute will indicate if we want to migrate the fields specified in the message rather than the ones in the stage constants.
                            if (root.Attribute("f") != null)
                            {
                                fields = WorkflowCacheUpdater.GetFieldsToUpdateFromMessage(root);
                                if (fields == null || fields.Length == 0)
                                {
                                    continue;
                                }
                            }
                            else if (root.Attribute("a") != null)
                            {
                                LoanUpdaterAction action;
                                if (!Enum.TryParse<LoanUpdaterAction>(root.Attribute("a").Value, out action) ||
                                    !Enum.IsDefined(typeof(LoanUpdaterAction), action))
                                {
                                    continue;
                                }

                                switch (action)
                                {
                                    case LoanUpdaterAction.UpdateLicenseInfoForLoanOfficer:
                                        {
                                            // We'll just set the task one as default. The audit history will go ahead and use "System User" anyways if the principal is a SystemUserPrincipal.
                                            AbstractUserPrincipal principal = SystemUserPrincipal.TaskSystemUser;
                                            var principalElement = root.Element("p");
                                            if (principalElement != null)
                                            {
                                                Guid userId;
                                                Guid bId;
                                                string type = principalElement.Attribute("t")?.Value;
                                                if (Guid.TryParse(principalElement.Attribute("u")?.Value, out userId) &&
                                                    Guid.TryParse(principalElement.Attribute("b")?.Value, out bId) &&
                                                    !string.IsNullOrEmpty(type))
                                                {
                                                    principal = PrincipalFactory.RetrievePrincipalForUser(bId, userId, type);
                                                }
                                            }

                                            this.ProcessLOLicenseInfoUpdate(sLId, principal);
                                            break;
                                        }
                                    default:
                                        break;
                                }

                                continue;
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(ConstStage.LoanUpdaterFieldsForMigration))
                                {
                                    return;
                                }

                                fields = ConstStage.LoanUpdaterFieldsForMigration.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198Valid"))
                                {
                                    CPageData dt = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(LoanUpdater));
                                    dt.InitSave(ConstAppDavid.SkipVersionCheck);
                                    brokerId = dt.sBrokerId;

                                    if (dt.sProdCurrPIPmtLckdHasValue)
                                    {
                                        continue;
                                    }

                                    if (!dt.IsTemplate && !dt.sIsQuickPricerLoan)
                                    {
                                        var item = dt.sProdCurrPIPmtLckd;
                                        dt.Save();
                                    }
                                    else
                                    {
                                        string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";

                                        SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };
                                        DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    }

                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198NotValid"))
                                {
                                    string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";
                                    SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };

                                    DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198Undo"))
                                {
                                    string updateQuery = "UPDATE TOP (1) LOAN_FILE_B SET sProdCurrPIPmtLckd = NULL WHERE SLID = @slid";
                                    SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };

                                    DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("MigratesFfUfmip1003_Case144828"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_MigratesFfUfmip1003_Case144828" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.MigratesFfUfmip1003_Case144828())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("FlushLiabilities_235420"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "aLiaCollection" });
                                    pageData.DisableFieldEnforcement();
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    foreach (var app in pageData.Apps)
                                    {
                                        app.aLiaCollection.Flush();
                                    }
                                    pageData.Save();
                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("UpdateLicenseInfoForLoanOfficer"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_UpdateLicenseInfoForLoanOfficer" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.UpdateLicenseInfoForLoanOfficer())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("MigratesProMInsRounding_Case190889"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_MigratesProMInsRounding_Case190889" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.MigratesProMInsRounding_Case190889())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }
                                
                                if (fields.Length > 0 && fields[0].Equals("OPM462698_MigrateToTouchesFeature"))
                                {
                                    try
                                    {
                                        StatusEvent.MigrateStatusEvents(sLId);
                                    }
                                    catch (DataAccess.CBaseException e)
                                    {
                                        DataAccess.Tools.LogError("Error migrating slid <" + sLId.ToString() + "> for OPM462698_MigrateToTouchesFeature.", e);
                                    }

                                    continue;
                                }
                            }

                            try
                            {
                                if (fields.FirstOrDefault() == "AllFieldUpdate")
                                {
                                    fields = null;
                                }

                                DataAccess.Tools.UpdateCacheTable(sLId, fields);

                                if (processedLoans % 1000 == 0)
                                {
                                    DataAccess.Tools.LogInfo("[LoanUpdater] Updated " + processedLoans + " loans.");
                                }
                                processedLoans++;
                            }
                            catch (Exception e)
                            {
                                DataAccess.Tools.LogError("[LoanUpdater] slid=" + sLId, e);
                                throw;
                            }
                        }
                        catch (MessageQueueException e)
                        {
                            if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                break;
                            }
                            throw;
                        }
                    }
                }
            }
        }

        public static void UpdateFirstProdCurrPIPmtLckd(Guid brokerId, Guid loanId, bool useNull)
        {
            if (useNull)
            {
                string updateQuery = "UPDATE TOP (1) LOAN_FILE_B SET sProdCurrPIPmtLckd = NULL WHERE SLID = @slid";
                SqlParameter[] parameters = { new SqlParameter("@slid", loanId) };

                DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
            }
            else
            {
                string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";
                SqlParameter[] parameters = { new SqlParameter("@slid", loanId) };

                DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
            }
        }

        private void ProcessLOLicenseInfoUpdate(Guid loanId, AbstractUserPrincipal principal)
        {
            CPageData pageData = new NotEnforceAccessControlPageData(loanId, new string[] { "sf_UpdateLicenseInfoForLoanOfficer", "sStatusT" });
            pageData.DisableLoanModificationTracking = true;
            pageData.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!DataAccess.Tools.IsInactiveStatus(pageData.sStatusT) && pageData.UpdateLicenseInfoForLoanOfficer(principal))
            {
                pageData.Save();
            }
        }
        #endregion
    }

    public class LoanUpdaterCancellableRunnable : CommonProjectLib.Runnable.ICancellableRunnable
    {
        /// <summary>
        /// Queues up the loans to update the Loan Officer's licenses.
        /// Note, the updater will not update a loan if <see cref="DataAccess.Tools.IsInactiveStatus(E_sStatusT)"/> returns false for its sStatusT.
        /// </summary>
        /// <param name="loanIds">The loans to queue up.</param>
        /// <param name="principal">The user who triggered the license change.</param>
        public static void QueueLoansForLOLicenseUpdate(IEnumerable<Guid> loanIds, AbstractUserPrincipal principal)
        {
            if (loanIds == null || !loanIds.Any())
            {
                return;
            }

            XDocument message = new XDocument();
            XElement root = new XElement("root");
            root.SetAttributeValue("a", LoanUpdaterAction.UpdateLicenseInfoForLoanOfficer.ToString("D"));
            message.Add(root);
            if (principal != null &&
                (string.Equals(principal.Type, "b", StringComparison.OrdinalIgnoreCase) || string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase)))
            {
                root.Add(new XElement("p",
                                      new XAttribute("u", principal.UserId.ToString()),
                                      new XAttribute("b", principal.BrokerId.ToString()),
                                      new XAttribute("t", principal.Type)));
            }

            string connectionString = ConstStage.LoanFileCacheQueueConnectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Skipping.");
                return;
            }

            MessageQueue queue = new MessageQueue(connectionString);
            queue.Formatter = new XDocumentMessageFormater();

            foreach (Guid loanId in loanIds)
            {
                root.SetAttributeValue("s", loanId.ToString());
                queue.Send(message);
            }
        }

        #region IRunnable Members

        public string Description
        {
            get
            {
                return "Updates loan file cache for loans whose id is in LoanFileCacheQueueConnectionString (MSMQ). The entry contains the files that need updating.";
            }
        }

        public void Run(CancellationToken cancellationToken)
        {
            string cacheUpdaterQueueConnectionString = ConstStage.LoanFileCacheQueueConnectionString;
            if (string.IsNullOrEmpty(cacheUpdaterQueueConnectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Please check if LoanFileCacheQueueConnectionString is set.");
                return;
            }

            string workflowQueueConnectionString = ConstStage.PriorityLoanUpdaterQueueConnectionString;
            if (string.IsNullOrEmpty(workflowQueueConnectionString))
            {
                DataAccess.Tools.LogError("No connection string for the queue was found. Please check if PriorityLoanUpdaterQueueConnectionString is set.");
                return;
            }

            using (var cacheUpdaterQueue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(cacheUpdaterQueueConnectionString, false))
            {
                using (var workflowQueue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(workflowQueueConnectionString, false))
                {
                    long processedLoans = 0;
                    while (!cancellationToken.IsCancellationRequested)
                    {
                        try
                        {
                            // We want to prioritize the normal loan updater queue in this IRunnable. 
                            // If we don't find any messages there, we go ahead and check the workflow updater queue.
                            XDocument xdoc = null;
                            DateTime arrivalTime;
                            try
                            {
                                xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(cacheUpdaterQueue, null, out arrivalTime);
                                if (xdoc == null)
                                {
                                    xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(workflowQueue, null, out arrivalTime); // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                                    if (xdoc == null)
                                    {
                                        break; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                                    }
                                }
                            }
                            catch (MessageQueueException exc)
                            {
                                if (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                                {
                                    xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(workflowQueue, null, out arrivalTime);
                                }
                                else
                                {
                                    throw;
                                }
                            }

                            XElement root = xdoc.Root;

                            string[] fields;
                            Guid sLId = new Guid(root.Attribute("s").Value);
                            Guid brokerId = Guid.Empty;

                            // The 'f' attribute will indicate if we want to migrate the fields specified in the message rather than the ones in the stage constants.
                            if (root.Attribute("f") != null)
                            {
                                fields = WorkflowCacheUpdater.GetFieldsToUpdateFromMessage(root);
                                if (fields == null || fields.Length == 0)
                                {
                                    continue;
                                }
                            }
                            else if (root.Attribute("a") != null)
                            {
                                LoanUpdaterAction action;
                                if (!Enum.TryParse<LoanUpdaterAction>(root.Attribute("a").Value, out action) ||
                                    !Enum.IsDefined(typeof(LoanUpdaterAction), action))
                                {
                                    continue;
                                }

                                switch (action)
                                {
                                    case LoanUpdaterAction.UpdateLicenseInfoForLoanOfficer:
                                        {
                                            // We'll just set the task one as default. The audit history will go ahead and use "System User" anyways if the principal is a SystemUserPrincipal.
                                            AbstractUserPrincipal principal = SystemUserPrincipal.TaskSystemUser;
                                            var principalElement = root.Element("p");
                                            if (principalElement != null)
                                            {
                                                Guid userId;
                                                Guid bId;
                                                string type = principalElement.Attribute("t")?.Value;
                                                if (Guid.TryParse(principalElement.Attribute("u")?.Value, out userId) &&
                                                    Guid.TryParse(principalElement.Attribute("b")?.Value, out bId) &&
                                                    !string.IsNullOrEmpty(type))
                                                {
                                                    principal = PrincipalFactory.RetrievePrincipalForUser(bId, userId, type);
                                                }
                                            }

                                            this.ProcessLOLicenseInfoUpdate(sLId, principal);
                                            break;
                                        }
                                    default:
                                        break;
                                }

                                continue;
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(ConstStage.LoanUpdaterFieldsForMigration))
                                {
                                    return;
                                }

                                fields = ConstStage.LoanUpdaterFieldsForMigration.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198Valid"))
                                {
                                    CPageData dt = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(LoanUpdater));
                                    dt.InitSave(ConstAppDavid.SkipVersionCheck);
                                    brokerId = dt.sBrokerId;

                                    if (dt.sProdCurrPIPmtLckdHasValue)
                                    {
                                        continue;
                                    }

                                    if (!dt.IsTemplate && !dt.sIsQuickPricerLoan)
                                    {
                                        var item = dt.sProdCurrPIPmtLckd;
                                        dt.Save();
                                    }
                                    else
                                    {
                                        string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";

                                        SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };
                                        DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    }

                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198NotValid"))
                                {
                                    string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";
                                    SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };

                                    DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("Migration_182198Undo"))
                                {
                                    string updateQuery = "UPDATE TOP (1) LOAN_FILE_B SET sProdCurrPIPmtLckd = NULL WHERE SLID = @slid";
                                    SqlParameter[] parameters = {
                                                            new SqlParameter("@slid", sLId)
                                                        };

                                    DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("MigratesFfUfmip1003_Case144828"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_MigratesFfUfmip1003_Case144828" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.MigratesFfUfmip1003_Case144828())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("FlushLiabilities_235420"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "aLiaCollection" });
                                    pageData.DisableFieldEnforcement();
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    foreach (var app in pageData.Apps)
                                    {
                                        app.aLiaCollection.Flush();
                                    }
                                    pageData.Save();
                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("UpdateLicenseInfoForLoanOfficer"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_UpdateLicenseInfoForLoanOfficer" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.UpdateLicenseInfoForLoanOfficer())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }


                                if (fields.Length > 0 && fields[0].Equals("MigratesProMInsRounding_Case190889"))
                                {
                                    CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_MigratesProMInsRounding_Case190889" });
                                    pageData.DisableLoanModificationTracking = true;
                                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                                    if (pageData.MigratesProMInsRounding_Case190889())
                                    {
                                        pageData.Save();
                                    }

                                    continue;
                                }

                                if (fields.Length > 0 && fields[0].Equals("OPM462698_MigrateToTouchesFeature"))
                                {
                                    try
                                    {
                                        StatusEvent.MigrateStatusEvents(sLId);
                                    }
                                    catch (DataAccess.CBaseException e)
                                    {
                                        DataAccess.Tools.LogError("Error migrating slid <" + sLId.ToString() + "> for OPM462698_MigrateToTouchesFeature.", e);
                                    }

                                    continue;
                                }
                            }

                            try
                            {
                                if (fields.FirstOrDefault() == "AllFieldUpdate")
                                {
                                    fields = null;
                                }

                                DataAccess.Tools.UpdateCacheTable(sLId, fields);

                                if (processedLoans % 1000 == 0)
                                {
                                    DataAccess.Tools.LogInfo("[LoanUpdater] Updated " + processedLoans + " loans.");
                                }
                                processedLoans++;
                            }
                            catch (Exception e)
                            {
                                DataAccess.Tools.LogError("[LoanUpdater] slid=" + sLId, e);
                                throw;
                            }
                        }
                        catch (MessageQueueException e)
                        {
                            if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                break;
                            }
                            throw;
                        }
                    }
                }
            }
        }

        public static void UpdateFirstProdCurrPIPmtLckd(Guid brokerId, Guid loanId, bool useNull)
        {
            if (useNull)
            {
                string updateQuery = "UPDATE TOP (1) LOAN_FILE_B SET sProdCurrPIPmtLckd = NULL WHERE SLID = @slid";
                SqlParameter[] parameters = { new SqlParameter("@slid", loanId) };

                DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
            }
            else
            {
                string updateQuery = "UPDATE TOP(1) LOAN_FILE_B SET sProdCurrPIPmtLckd = 0 WHERE SLID = @slid";
                SqlParameter[] parameters = { new SqlParameter("@slid", loanId) };

                DBUpdateUtility.Update(brokerId, updateQuery, null, parameters);
            }
        }

        private void ProcessLOLicenseInfoUpdate(Guid loanId, AbstractUserPrincipal principal)
        {
            CPageData pageData = new NotEnforceAccessControlPageData(loanId, new string[] { "sf_UpdateLicenseInfoForLoanOfficer", "sStatusT" });
            pageData.DisableLoanModificationTracking = true;
            pageData.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!DataAccess.Tools.IsInactiveStatus(pageData.sStatusT) && pageData.UpdateLicenseInfoForLoanOfficer(principal))
            {
                pageData.Save();
            }
        }
        #endregion
    }
}
