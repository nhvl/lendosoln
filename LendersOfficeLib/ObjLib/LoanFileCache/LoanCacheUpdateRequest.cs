﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Common;
    using Constants;
    using DataAccess;
    using LendersOffice.Admin;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using Task;

    /// <summary>
    /// Class to represent an entry from the LOAN_CACHE_UPDATE_REQUEST table.
    /// Contains a few extra data points for ease of use.
    /// </summary>
    public class LoanCacheUpdateRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCacheUpdateRequest"/> class.
        /// Intended only for use in serialization.
        /// </summary>
        public LoanCacheUpdateRequest()
        {
            // for Serialization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCacheUpdateRequest"/> class.
        /// </summary>
        /// <param name="reader">The data source.</param>
        public LoanCacheUpdateRequest(IDataRecord reader)
        {
            // Fields from the LOAN_CACHE_UPDATE_REQUEST table.
            this.RequestId = (Guid)reader["RequestId"];
            this.sLId = (Guid)reader["sLId"];
            this.UserId = reader["UserId"] as string;
            this.UserLoginNm = reader["UserLoginNm"] as string;
            this.PageName = reader["PageName"] as string;
            this.AffectedCachedFields = reader["AffectedCachedFields"] as string;
            this.FirstOccurD = (DateTime)reader["FirstOccurD"];
            this.WhoRetriedLastLoginNm = reader["WhoRetriedLastLoginNm"] as string;
            this.ErrorInfo = reader["ErrorInfo"] as string;
            this.LastRetryD = reader.SafeDateTimeNullable("LastRetryD");
            this.NumRetries = (int)reader["NumRetries"];

            // Fields from other tables.
            this.sLNm = reader["sLNm"] as string;
            this.BrokerId = (Guid)reader["BrokerId"];
            this.CustomerCode = reader["CustomerCode"] as string; // opm 19711 av added CustomerCode to GetCacheRequest
        }

        /// <summary>
        /// Gets or sets the id of the request.
        /// </summary>
        /// <value>The id of the request.</value>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Gets or sets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "StyleCopped an existing class. Not going to change the property name.")]
        public Guid sLId { get; set; }

        /// <summary>
        /// Gets or sets the id of the user when the record was initially inserted.
        /// </summary>
        /// <value>This may be a system user id.</value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the login name of the user when the record was initially inserted.
        /// </summary>
        /// <value>The login name of the user when the record was initially inserted.</value>
        public string UserLoginNm { get; set; }

        /// <summary>
        /// Gets or sets the page / area from which the record was inserted.
        /// </summary>
        /// <value>This can either be a page or an area. For example, "TaskUtilities".</value>
        public string PageName { get; set; }

        /// <summary>
        /// Gets or sets the database fields associated with this record.
        /// </summary>
        /// <value>
        /// May be blank. If the record was inserted as part of the loan process, it will initially
        /// contain just the dirty loan database fields. If a cache update was attempted and failed,
        /// this value may have the expanded cache fields appended to it. The format of the value will
        /// be: sDirtyLoanField1,sDirtyLoanField2,...:sExpandedCacheField1,sExpandedCacheField2,...
        /// If the record did not come from the loan save process, this is not necessarily the case.
        /// </value>
        public string AffectedCachedFields { get; set; }

        /// <summary>
        /// Gets or sets the error message for the record. This may be truncated from the database.
        /// </summary>
        /// <remarks>
        /// If this is loaded for the background process it will only load the first 50 characters
        /// of the error message.
        /// </remarks>
        /// <value>The error message.</value>
        public string ErrorInfo { get; set; }

        /// <summary>
        /// Gets or sets the date this record was first inserted into the LOAN_CACHE_UPDATE_REQUEST table.
        /// </summary>
        /// <value>The date this record was first inserted into the LOAN_CACHE_UPDATE_REQUEST table.</value>
        public DateTime FirstOccurD { get; set; }

        /// <summary>
        /// Gets or sets the date of the most recent retry attempt for the cache update.
        /// TODO: The UI will need to be updated to print a better value.
        /// The type switch from string to DateTime? resulted in an overly verbose description.
        /// </summary>
        /// <value>The date of the most recent retry attempt for the cache update.</value>
        public DateTime? LastRetryD { get; set; }

        /// <summary>
        /// Gets or sets the login name of user during the most recent retry attempt.
        /// </summary>
        /// <value>The login name of user during the most recent retry attempt.</value>
        public string WhoRetriedLastLoginNm { get; set; }

        /// <summary>
        /// Gets or sets the number of times this request has been run.
        /// </summary>
        /// <value>The number of times this request has been run.</value>
        public int NumRetries { get; set; }

        /// <summary>
        /// Gets or sets the loan number of the request.
        /// </summary>
        /// <value>The loan number.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "StyleCopped an existing class. Not going to change the property name.")]
        public string sLNm { get; set; }

        /// <summary>
        /// Gets or sets the broker id of the request.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the customer code of the request.
        /// </summary>
        /// <value>The customer code.</value>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Gets the dirty, not-expanded fields from the original failed save.
        /// If the record did not come from the loan save process, this may be
        /// blank or not correspond directly to a loan save.
        /// </summary>
        /// <value>The dirty fields.</value>
        public IEnumerable<string> DirtyFieldsFromOriginalLoanSave
        {
            get
            {
                return this.AffectedCachedFields.Split(':')[0]
                    .Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(fieldId => fieldId.Trim());
            }
        }

        /// <summary>
        /// Retrieves the 100 most recent records from each database.
        /// </summary>
        /// <returns>The 100 most recent records from each database.</returns>
        public static IEnumerable<LoanCacheUpdateRequest> GetTop100FromEachDatabase()
        {
            return Retrieve("GetCacheRequest");
        }

        /// <summary>
        /// Retrieves the 1000 most recent records from each database.
        /// </summary>
        /// <returns>The 1000 most recent records from each database.</returns>
        public static IEnumerable<LoanCacheUpdateRequest> GetTop1000FromEachDatabase()
        {
            return Retrieve("GetCacheRequestAll");
        }

        /// <summary>
        /// Gets the failed records that should be automatically retried.
        /// </summary>
        /// <returns>The records for retry.</returns>
        public static IEnumerable<LoanCacheUpdateRequest> GetFailedRequestsForRetry()
        {
            // Using a LinkedList<T> to prevent a List<T> from ending up on the large
            // object heap in the case that there are a lot of items to retry.
            var requests = new LinkedList<LoanCacheUpdateRequest>();

            var procedureName = StoredProcedureName.Create("GetCacheRequestsForRetry");
            if (procedureName == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                var parameters = new[] 
                {
                    new SqlParameter("@MaxRetries", ConstStage.MaxAutomaticCacheUpdateRetries),
                    new SqlParameter("@TenMinutesAgo", DateTime.Now.AddMinutes(-10))
                };

                using (var connection = connectionInfo.GetConnection())
                using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
                {
                    while (reader.Read())
                    {
                        requests.AddLast(new LoanCacheUpdateRequest(reader));
                    }
                }
            }

            return requests;
        }

        /// <summary>
        /// Inserts a record into LOAN_CACHE_UPDATE_REQUEST table.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="requestId">The id of the request.</param>
        /// <param name="loanId">The id of the loan.</param>
        /// <param name="userId">The id of the user.</param>
        /// <param name="loginName">The login name of the user.</param>
        /// <param name="pageName">The name of the page.</param>
        /// <param name="dirtyFields">The fields that were dirty at save.</param>
        public static void InsertForLoanSave(DbConnectionInfo connection, Guid requestId, Guid loanId, Guid userId, string loginName, string pageName, string dirtyFields)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@ReqId", requestId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@LoginName", loginName),
                new SqlParameter("@PageName", pageName),
                new SqlParameter("@DirtyFields", dirtyFields)
            };

            StoredProcedureHelper.ExecuteNonQuery(connection, "InsertCacheRequest", 0, parameters);
        }

        /// <summary>
        /// Inserts a record into the LOAN_CACHE_UPDATE_REQUEST table for a failure in UpdateTasksDueDateByLoan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="loginName">The login name to use for the record.</param>
        /// <param name="exc">The exception that was handled.</param>
        public static void InsertForTaskFailure(Guid loanId, Guid brokerId, Guid userId, string loginName, Exception exc)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@ReqId", Guid.NewGuid()),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@LoginName", loginName),
                new SqlParameter("@PageName", "TaskUtilities"),
                new SqlParameter("@DirtyFields", string.Empty),
                new SqlParameter("@Error", GetTaskFailureMessage(exc))
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "InsertCacheRequest", 0, parameters);
        }

        /// <summary>
        /// Update the affected cached fields for a record.
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="affectedCachedFields">The affected cached fields.</param>
        public static void UpdateAffectedCachedFields(Guid requestId, Guid brokerId, string affectedCachedFields)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@ReqId", requestId),
                new SqlParameter("@Error", DBNull.Value),
                new SqlParameter("@sqlFields", affectedCachedFields)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateCacheRequest", 0, parameters);
        }

        /// <summary>
        /// Updates a record for a task failure. Intended to provide an updated error message.
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="exc">The exception that was handled.</param>
        /// <param name="loginName">The login name to use.</param>
        public static void UpdateForTaskFailure(Guid requestId, Guid brokerId, Exception exc, string loginName)
        {
            var connection = DbConnectionInfo.GetConnectionInfo(brokerId);
            Update(requestId, connection, GetTaskFailureMessage(exc), loginName);
        }

        /// <summary>
        /// Updates a record with the given data.
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <param name="connection">The database connection where the request can be found.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="loginName">The login name to use.</param>
        public static void Update(Guid requestId, DbConnectionInfo connection, string errorMessage, string loginName)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@ReqId", requestId),
                new SqlParameter("@Error", errorMessage),
                new SqlParameter("@sqlFields", DBNull.Value),
                new SqlParameter("@LastRetryD", DateTime.Now),
                new SqlParameter("@WhoRetriedLastLogin", loginName)
            };

            StoredProcedureHelper.ExecuteNonQuery(connection, "UpdateCacheRequest", 0, parameters);
        }

        /// <summary>
        /// Deletes the request from the database.
        /// </summary>
        /// <param name="request">The request to delete.</param>
        public static void Delete(LoanCacheUpdateRequest request)
        {
            Delete(request.RequestId, request.BrokerId);
        }

        /// <summary>
        /// Deletes the request from the database.
        /// </summary>
        /// <param name="requestId">The id of the request.</param>
        /// <param name="brokerId">The id of the broker.</param>
        public static void Delete(Guid requestId, Guid brokerId)
        {
            var connection = DbConnectionInfo.GetConnectionInfo(brokerId);
            Delete(requestId, connection);
        }

        /// <summary>
        /// Deletes the request from the database.
        /// </summary>
        /// <param name="requestId">The id of the request.</param>
        /// <param name="connection">The database connection to use.</param>
        public static void Delete(Guid requestId, DbConnectionInfo connection)
        {
            var parameters = new[] { new SqlParameter("@ReqId", requestId) };
            StoredProcedureHelper.ExecuteNonQuery(connection, "DeleteCacheRequest", 0, parameters);
        }

        /// <summary>
        /// Gets the number of records in the LOAN_CACHE_UPDATE_REQUEST table across all databases.
        /// </summary>
        /// <returns>The number of records in the table across all databases.</returns>
        public static int GetCacheUpdateRequestCount()
        {
            // find how many errors now exist in the table
            // 11/16/2007 dd - Review. This is safe sql query string.
            string sql = "SELECT Count('') AS errors FROM LOAN_CACHE_UPDATE_REQUEST ";
            int numErrors = 0;

            Action<IDataReader> readHandler = reader =>
            {
                if (reader.Read())
                {
                    numErrors += Convert.ToInt32(reader["errors"]);
                }
            };

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return numErrors;
        }

        /// <summary>
        /// Retries the cache update. On success, the record is deleted from the database.
        /// </summary>
        /// <param name="request">The request to rerun.</param>
        /// <param name="brokerEnabledById">A map from broker id to a value indicating whether a lender is enabled.</param>
        /// <param name="loanValidById">A map from loan id to a value indicating whether a loan is valid.</param>
        public static void Rerun(LoanCacheUpdateRequest request, Dictionary<Guid, bool> brokerEnabledById, Dictionary<Guid, bool> loanValidById)
        {
            if (!LenderEnabled(request.BrokerId, brokerEnabledById))
            {
                var message = "Dropping cache request for disabled lender. "
                    + $"RequestId: {request.RequestId}, BrokerId: {request.BrokerId}, LoanId: {request.sLId}.";
                Tools.LogInfo(message);
                Delete(request);
            }
            else if (!LoanValid(request, loanValidById))
            {
                var message = "Dropping cache request for invalid loan file."
                    + $"RequestId: {request.RequestId}, BrokerId: {request.BrokerId}, LoanId: {request.sLId}.";
                Tools.LogInfo(message);
                Delete(request);
            }
            else if (request.PageName == "TaskUtilities"
                && string.IsNullOrEmpty(request.AffectedCachedFields)
                && request.ErrorInfo.StartsWith("TaskUtilities.UpdateTasksDueDateByLoan"))
            {
                // This will attempt to update the due dates for tasks with calculated due dates
                // and sync the borrower name and loan number stored in the tasks with that from
                // the loan file.
                // These types of records should only be inserted if the lender has the new task
                // system enabled, so we will not perform the feature check here.
                // This behavior comes from commit 4f71ebf2a9a for case 67951 to specifically 
                // handle failed calls to TaskUtilities.UpdateTasksDueDateByLoan() for retrying. 
                // It doesn't look like this was ever tied to loan cache updates. The page name, 
                // affected fields, and error message that we are filtering on were not modified 
                // from when this behavior was initially added, so we will perform this retry 
                // separate from the cache update to reduce unnecessary work.
                // This method will handle deleting the request if it succeeds or updating it
                // if it fails.
                TaskUtilities.UpdateTasksDueDateByLoan(request.BrokerId, request.sLId, request.RequestId);
            }
            else
            {
                var connInfo = DbConnectionInfo.GetConnectionInfo(request.BrokerId);

                // If the record does not have the affected fields filled out, we will assume
                // the file needs a full cache update.
                ICollection dirtyFields = null;

                if (!string.IsNullOrEmpty(request.AffectedCachedFields))
                {
                    dirtyFields = request.DirtyFieldsFromOriginalLoanSave.ToArray();
                }

                // This will attempt the cache update and remove the record from the table if it succeeds.
                Tools.UpdateCacheTable(connInfo, request.sLId, dirtyFields, request.RequestId, rerunningCacheUpdateRequest: true);
            }
        }

        /// <summary>
        /// Deletes requests that are not associated with a loan. This should only
        /// happen in the case that the loan id no longer exists in the database.
        /// This does NOT delete requests associated with invalid loan files.
        /// </summary>
        /// <returns>The number of requests that were deleted.</returns>
        public static int DeleteInvalidRequests()
        {
            int total = 0;

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                total += (int)StoredProcedureHelper.ExecuteScalar(connection, "DeleteInvalidLoanCacheUpdateRequests", null);
            }

            return total;
        }

        /// <summary>
        /// Retrieves records using the given stored procedure.
        /// </summary>
        /// <param name="storedProcedure">The name of the stored procedure.</param>
        /// <returns>An enumerable of cache update requests.</returns>
        private static IEnumerable<LoanCacheUpdateRequest> Retrieve(string storedProcedure)
        {
            var requests = new LinkedList<LoanCacheUpdateRequest>();

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, storedProcedure, null))
                {
                    while (reader.Read())
                    {
                        requests.AddLast(new LoanCacheUpdateRequest(reader));
                    }
                }
            }

            return requests;
        }

        /// <summary>
        /// Generates an error message for an exception in TaskUtilities.UpdateTasksDueDateByLoan.
        /// </summary>
        /// <param name="exc">The exception.</param>
        /// <returns>The error message.</returns>
        private static string GetTaskFailureMessage(Exception exc)
        {
            return $"TaskUtilities.UpdateTasksDueDateByLoan{Environment.NewLine}{exc.ToString()}{Environment.NewLine}{Environment.StackTrace}";
        }

        /// <summary>
        /// Gets a value indicating whether the lender account is enabled.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="brokerEnabledById">A dictionary cache from broker id to whether they are enabled.</param>
        /// <returns>True if the lender account is enabled. Otherwise, false.</returns>
        private static bool LenderEnabled(Guid brokerId, Dictionary<Guid, bool> brokerEnabledById)
        {
            bool enabled = false;

            if (!brokerEnabledById.TryGetValue(brokerId, out enabled))
            {
                enabled = BrokerDB.IsEnabled(brokerId);
                brokerEnabledById.Add(brokerId, enabled);
            }

            return enabled;
        }

        /// <summary>
        /// Gets a value indicating if the file is marked valid in loan_file_a.
        /// </summary>
        /// <param name="request">The cache update request whose loan will be checked.</param>
        /// <param name="loanValidById">A map from loan id to a value indicating whether a loan is valid.</param>
        /// <returns>True if the file is valid or was not found in loan_file_a.</returns>
        private static bool LoanValid(LoanCacheUpdateRequest request, Dictionary<Guid, bool> loanValidById)
        {
            // We're going to select from loan_file_a in case the file
            // is missing a record from the cache. If the file is so 
            // messed up that it is missing a record from loan_file_a,
            // then we will just drop the cache update request.
            bool valid;
            if (!loanValidById.TryGetValue(request.sLId, out valid))
            {
                var sql = "select isvalid from loan_file_a where slid = @slid";
                Action<IDataReader> readerCode = reader =>
                {
                    if (reader.Read())
                    {
                        valid = (bool)reader["isvalid"];
                    }
                };

                DBSelectUtility.ProcessDBData(request.BrokerId, sql, null, new[] { new SqlParameter("@slid", request.sLId) }, readerCode);

                loanValidById.Add(request.sLId, valid);
            }

            return valid;
        }
    }
}
