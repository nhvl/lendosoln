﻿namespace LendersOffice.ObjLib.LoanFileCache
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CommonProjectLib.Runnable;
    using DataAccess;

    /// <summary>
    /// Reruns cache update requests.
    /// </summary>
    public class FailedCacheUpdateRunner : IRunnable
    {
        /// <summary>
        /// Gets the description of what this runnable does.
        /// </summary>
        /// <value>The description of what this runnable does.</value>
        public string Description => "Reruns failed cache updates.";

        /// <summary>
        /// Retrieves cache update requests from the database and attempts to rerun them.
        /// </summary>
        public void Run()
        {
            this.Log("Started.");

            var requests = LoanCacheUpdateRequest.GetFailedRequestsForRetry();

            var totalRequests = requests.Count();
            this.Log($"Retrieved {totalRequests} items.");

            var startedProcessing = DateTime.Now;
            int i = 0;

            var brokerEnabledById = new Dictionary<Guid, bool>();
            var loanValidById = new Dictionary<Guid, bool>();

            foreach (var request in requests)
            {
                LoanCacheUpdateRequest.Rerun(request, brokerEnabledById, loanValidById);
                ++i;

                if (i % 50 == 0)
                {
                    var now = DateTime.Now;
                    var rate = i / (now - startedProcessing).TotalSeconds;
                    var projectedFinish = now.AddSeconds(1 / rate * totalRequests);
                    this.Log($"Processed {i} items. {rate.ToString("F3")} items/sec, projected completion: {projectedFinish}");
                }
            }

            this.Log($"Complete. Processed {i} requests.");
        }

        /// <summary>
        /// Logs the given message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        private void Log(string message)
        {
            Tools.LogInfo("FailedCacheUpdateRunner", message);
        }
    }
}
