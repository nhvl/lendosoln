﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using DataAccess;
using System.Security.Principal;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.ConsumerPortal
{
    public enum E_TicketType
    {
        ESignRequest, 
        FaxRequest, 
        DownloadEDoc,
        DownloadRequest,
    }

    public class EDocPortalTicket
    {
        public Guid BrokerId { get;  set; }
        public long ConsumerId { get;  set; }
        public string Data { get;  set; }
        public DateTime ExpirationDate { get;  set; }
        public Guid ConsumerPortalId { get;  set; }
        public E_TicketType TicketType { get; set; }
        public Guid AllowViewSubmittedLoanId { get; set; }

        public EDocPortalTicket() {}

        public static string CreateSignatureTicket(ConsumerPortalUserPrincipal info, long requestId)
        {
            EDocPortalTicket ticket = new EDocPortalTicket();
            ticket.BrokerId = info.BrokerId;
            ticket.ConsumerId = info.Id;
            ticket.ExpirationDate = DateTime.Now.AddMinutes(1.5);
            ticket.ConsumerPortalId = info.ConsumerPortalId;
            ticket.Data = requestId.ToString();
            ticket.TicketType = E_TicketType.ESignRequest;
            ticket.AllowViewSubmittedLoanId = info.AllowViewSubmittedLoanId;
            return Cache(ticket);
        }

        public static string CreateDownlodRequestTicket(ConsumerPortalUserPrincipal info, long requestId)
        {
            EDocPortalTicket ticket = new EDocPortalTicket();
            ticket.BrokerId = info.BrokerId;
            ticket.ConsumerId = info.Id;
            ticket.ExpirationDate = DateTime.Now.AddMinutes(5);
            ticket.ConsumerPortalId = info.ConsumerPortalId;
            ticket.Data = requestId.ToString();
            ticket.TicketType = E_TicketType.DownloadRequest;
            ticket.AllowViewSubmittedLoanId = info.AllowViewSubmittedLoanId;
            return Cache(ticket);
        }


        public static string CreateDownlodShareRequestTicket(ConsumerPortalUserPrincipal info, long requestId)
        {
            EDocPortalTicket ticket = new EDocPortalTicket();
            ticket.BrokerId = info.BrokerId;
            ticket.ConsumerId = info.Id;
            ticket.ExpirationDate = DateTime.Now.AddMinutes(5);
            ticket.ConsumerPortalId = info.ConsumerPortalId;
            ticket.Data = "s"+ requestId.ToString();
            ticket.TicketType = E_TicketType.DownloadRequest;
            ticket.AllowViewSubmittedLoanId = info.AllowViewSubmittedLoanId;
            return Cache(ticket);
        }


        public static string CreateFaxPackageTicket(ConsumerPortalUserPrincipal info, long requestId)
        {
            EDocPortalTicket ticket = new EDocPortalTicket();
            ticket.BrokerId = info.BrokerId;
            ticket.ConsumerId = info.Id;
            ticket.ExpirationDate = DateTime.Now.AddMinutes(5);
            ticket.ConsumerPortalId = info.ConsumerPortalId;
            ticket.Data = requestId.ToString();
            ticket.TicketType = E_TicketType.FaxRequest;
            ticket.AllowViewSubmittedLoanId = info.AllowViewSubmittedLoanId;
            return Cache(ticket);
        }

        public static string CreateDownloadEDocTicket(ConsumerPortalUserPrincipal info, Guid documentId)
        {
            EDocPortalTicket ticket = new EDocPortalTicket();
            ticket.BrokerId = info.BrokerId;
            ticket.ConsumerId = info.Id;
            ticket.ExpirationDate = DateTime.Now.AddMinutes(5);
            ticket.ConsumerPortalId = info.ConsumerPortalId;
            ticket.Data = documentId.ToString();
            ticket.TicketType = E_TicketType.DownloadEDoc;
            ticket.AllowViewSubmittedLoanId = info.AllowViewSubmittedLoanId;
            return Cache(ticket);
        }

        private static string Cache(EDocPortalTicket ticket)
        {
            string data = ObsoleteSerializationHelper.JavascriptJsonSerialize(ticket);
            Guid id = Guid.NewGuid();
            string key = Tools.ShortenGuid(id);
            AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(5), id);
            return key;
        }

        public static EDocPortalTicket GetPortalTicket(string key)
        {
            Guid g = Tools.GetGuidFrom(key);
            string data = AutoExpiredTextCache.GetFromCache(g);
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }
            EDocPortalTicket ticket = ObsoleteSerializationHelper.JavascriptJsonDeserializer<EDocPortalTicket>(data);
            //if it esign lets allow IE to do multiple request.
            if (ticket.TicketType != E_TicketType.ESignRequest)
            {
                AutoExpiredTextCache.RemoveFromCacheImmediately(key);
            }
            if (ticket.ExpirationDate < DateTime.Now)
            {
                return null;
            }

            

            return ticket;
        }

        public ConsumerPortalUserPrincipal GetPrincipal()
        {
            ConsumerPortalUser consumerPortalUser = ConsumerPortalUser.Retrieve(BrokerId, ConsumerId);
            ConsumerPortalUserPrincipal p = new ConsumerPortalUserPrincipal(new GenericIdentity(consumerPortalUser.Email), consumerPortalUser, ConsumerPortalId, AllowViewSubmittedLoanId);
            return p;
        }
    }
}
