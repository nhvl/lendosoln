﻿// <copyright file="PendingDocumentRequestHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/5/2014 2:11:36 PM 
// </summary>
namespace LendersOffice.ObjLib.ConsumerPortal
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Handles adding and removing pending document requests from the scheduling pool.
    /// It has capabilities to load, save, delete, and send pending requests.
    /// </summary>
    public static class PendingDocumentRequestHandler
    {
        /// <summary>
        /// Gets the pending document requests for the given loan. This is 
        /// intended to be used when the requests are to be displayed in the UI.
        /// </summary>
        /// <param name="loanId">The id of the loan.</param>
        /// <param name="brokerId">The id of the broker.</param>
        /// <returns>An enumerable of the pending document requests for the given loan.</returns>
        public static IEnumerable<ReadOnlyPendingDocumentRequest> GetRequestsForLoanId(Guid loanId, Guid brokerId)
        {
            var pendingRequests = new List<ReadOnlyPendingDocumentRequest>();
            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "PENDING_DOCUMENT_REQUEST_RetrieveByLoanId", parameters))
            {
                while (reader.Read())
                {
                    pendingRequests.Add(new ReadOnlyPendingDocumentRequest(reader));
                }
            }

            return pendingRequests;
        }

        /// <summary>
        /// Gets the pending document requests that are ready to be sent.
        /// A request is ready to be send when all of the requests associated
        /// with the same id are scheduled to be sent.
        /// </summary>
        /// <remarks>
        /// For the old consumer portal, the item is ready to be sent if 
        /// it is scheduled and either a borrower or co-borrower email 
        /// is entered.
        /// For the new consumer portal, the item is ready to be sent if
        /// it is scheduled and either the borrower or co-borrower has been
        /// granted access to the portal for the given application.
        /// </remarks>
        /// <returns>
        /// The pending document requests where all requests associated with the 
        /// same loan have reached their scheduled exit time.
        /// </returns>
        public static IEnumerable<PendingDocumentRequest> GetScheduledRequests()
        {
            var scheduledRequests = new List<PendingDocumentRequest>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "PENDING_DOCUMENT_REQUEST_RetrieveScheduledRequests", null))
                {
                    while (reader.Read())
                    {
                        scheduledRequests.Add(new PendingDocumentRequest(reader));
                    }
                }
            }

            return scheduledRequests;
        }

        /// <summary>
        /// Removes the pending document requests from the scheduling pool.
        /// </summary>
        /// <param name="deleteFileFromFileDB">
        /// Indicates whether the associated files should be deleted from FileDB.
        /// </param>
        /// <param name="ids">The ids of the requests to remove.</param>
        /// <param name="brokerId">The id of the broker.</param>
        public static void DeleteRequestsById(bool deleteFileFromFileDB, IEnumerable<int> ids, Guid brokerId)
        {
            var pendingRequests = RetrieveRequestsById(ids, brokerId);

            if (PrincipalFactory.CurrentPrincipal != null)
            {
                var applications = string.Join(", ", pendingRequests.GroupBy(r => r.ApplicationId).Select(g => g.Key.ToString()).ToArray());

                var log = string.Format(
                    "[PendingDocumentRequestHandler]User {0} (id: {1}, broker id {2}) " +
                    "manually deleted pending document requests for applications: {3}",
                    PrincipalFactory.CurrentPrincipal.DisplayName,
                    PrincipalFactory.CurrentPrincipal.UserId,
                    PrincipalFactory.CurrentPrincipal.BrokerId,
                    applications);

                Tools.LogInfo(log);
            }

            foreach (var pendingRequest in pendingRequests)
            {
                pendingRequest.Delete(deleteFileFromFileDB);
            }
        }

        /// <summary>
        /// Sends the pending document requests with the given ids immediately
        /// and removes them from the scheduling pool.
        /// </summary>
        /// <param name="ids">The ids of the requests to send.</param>
        /// <param name="brokerId">The id of the broker.</param>
        public static void SendRequestsById(IEnumerable<int> ids, Guid brokerId)
        {
            var pendingRequests = RetrieveRequestsById(ids, brokerId);

            if (PrincipalFactory.CurrentPrincipal != null)
            {
                var applications = string.Join(", ", pendingRequests.GroupBy(r => r.ApplicationId).Select(g => g.Key.ToString()).ToArray());

                var log = string.Format(
                    "[PendingDocumentRequestHandler]User {0} (id: {1}, broker id {2}) " +
                    "manually sent pending document requests for applications: {3}",
                    PrincipalFactory.CurrentPrincipal.DisplayName,
                    PrincipalFactory.CurrentPrincipal.UserId,
                    PrincipalFactory.CurrentPrincipal.BrokerId,
                    applications);

                Tools.LogInfo(log);
            }

            foreach (var requestsForLoanId in pendingRequests.GroupBy(r => r.LoanId))
            {
                SendRequestsForLoanId(requestsForLoanId.Key, requestsForLoanId.ToArray());
            }
        }

        /// <summary>
        /// Sends the given document requests for the loan. Document notifications 
        /// will be sent per request type per application. Removes the requests from
        /// the scheduling pool.
        /// </summary>
        /// <param name="loanId">The id of the loan with which the requests are associated.</param>
        /// <param name="pendingRequests">The pending document requests to send.</param>
        public static void SendRequestsForLoanId(Guid loanId, params PendingDocumentRequest[] pendingRequests)
        {
            if (pendingRequests.Length == 0)
            {
                return;
            }
            else if (pendingRequests.Any(r => r.LoanId != loanId))
            {
                throw CBaseException.GenericException("Implementation error. " +
                    "Trying to send document requests that are not associated with this loan.");
            }

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(PendingDocumentRequestHandler));
            dataLoan.InitLoad();

            foreach (var requestsByApplicationId in pendingRequests.GroupBy(r => r.ApplicationId))
            {
                var dataApp = dataLoan.GetAppData(requestsByApplicationId.Key);

                string errorMessage = null;

                if (!ValidateBorrowerInformation(dataApp, out errorMessage))
                {
                    foreach (var request in requestsByApplicationId)
                    {
                        request.SetErrorMessage(errorMessage);
                    }

                    Tools.LogInfo(
                        string.Format(
                            "[PendingDocumentRequestHandler]Tried to process " +
                            "application with invalid borrower information. " +
                            "Error: {0}, Application Id: {1}, Loan Id: {2}.",
                            errorMessage,
                            requestsByApplicationId.Key,
                            loanId));

                    continue;
                }

                bool areAllBorrowerEmailsUnique = dataLoan.AreAllBorrowerEmailsUnique;

                foreach (var requestGroup in requestsByApplicationId.GroupBy(r => r.RequestType))
                {
                    switch (requestGroup.Key)
                    {
                        case DocumentRequestType.ReceiveDocumentFromBorrower:
                            SendReceiveDocumentRequests(dataLoan, dataApp, requestGroup);
                            break;
                        case DocumentRequestType.SendDocumentToBorrower:
                            SendSharedDocumentRequest(dataLoan, dataApp, requestGroup);
                            break;
                        case DocumentRequestType.SendDocumentToBorrowerForSigning:
                            SendSignDocumentRequests(dataLoan, dataApp, requestGroup, areAllBorrowerEmailsUnique);
                            break;
                        default:
                            throw new UnhandledEnumException(requestGroup.Key);
                    }
                }
            }
        }

        /// <summary>
        /// Checks to see if the data on the application is valid for sending document requests.
        /// </summary>
        /// <param name="dataApp">The application to validate.</param>
        /// <param name="errorMessage">The reason why the application is invalid. Empty string if valid.</param>
        /// <returns>True if the application is valid. Otherwise, false.</returns>
        private static bool ValidateBorrowerInformation(CAppData dataApp, out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!UserDataValidation.IsValidEmailAddress(dataApp.aBEmail) &&
                !UserDataValidation.IsValidEmailAddress(dataApp.aCEmail))
            {
                errorMessage = "Please enter a valid email address for a borrower on this application.";
                return false;
            }
            else if (dataApp.aBSsn == dataApp.aCSsn && UserDataValidation.IsValidSSN(dataApp.aCSsn))
            {
                errorMessage = "Please enter unique SSNs for the borrowers on this application.";
                return false;
            }
            else if (UserDataValidation.IsValidEmailAddress(dataApp.aBEmail) &&
                !UserDataValidation.IsValidSSN(dataApp.aBSsn))
            {
                errorMessage = "Please enter a valid SSN for the borrower on this application.";
                return false;
            }
            else if (UserDataValidation.IsValidEmailAddress(dataApp.aCEmail) &&
                !UserDataValidation.IsValidSSN(dataApp.aCSsn))
            {
                errorMessage = "Please enter a valid SSN for the co-borrower on this application.";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load a pending document request by id.
        /// </summary>
        /// <param name="id">The id of the request.</param>
        /// <param name="brokerId">The id of the broker.</param>
        /// <returns>A new instance of the PendingDocumentRequestClass for the given id.</returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the request with the given id cannot be found in the database.
        /// </exception>
        private static PendingDocumentRequest RetrieveRequestById(int id, Guid brokerId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@Id", id),
                new SqlParameter("@BrokerId", brokerId)
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "PENDING_DOCUMENT_REQUEST_RetrieveById", parameters))
            {
                if (reader.Read())
                {
                    return new PendingDocumentRequest(reader);
                }
                else
                {
                    throw new NotFoundException("Unable to find pending " +
                         "document request for the given id. Id: " + id);
                }
            }
        }

        /// <summary>
        /// Gets an enumerable of PendingDocumentRequest instances for the given ids.
        /// </summary>
        /// <param name="ids">The ids of the requests.</param>
        /// <param name="brokerId">The id of the broker.</param>
        /// <returns>An enumerable of the PendingDocumentRequests with the given ids.</returns>
        private static IEnumerable<PendingDocumentRequest> RetrieveRequestsById(IEnumerable<int> ids, Guid brokerId)
        {
            var pendingRequests = new List<PendingDocumentRequest>();

            foreach (var id in ids)
            {
                var pendingDocumentRequest = RetrieveRequestById(id, brokerId);
                pendingRequests.Add(pendingDocumentRequest);
            }

            return pendingRequests;
        }

        /// <summary>
        /// Creates an instance of the ConsumerActionItem class from the 
        /// PendingDocumentRequest. Sets the title only co-borrower if needed.
        /// </summary>
        /// <param name="dataLoan">The loan associated with the request.</param>
        /// <param name="dataApp">The application associated with the request.</param>
        /// <param name="pendingRequest">The pending document request.</param>
        /// <param name="titleOnlyCoborrower">
        /// The title only co-borrower associated with the request. Will be null 
        /// if not set or not found.
        /// </param>
        /// <returns>A new ConsumerActionItem instance.</returns>
        private static ConsumerActionItem CreateConsumerActionItemFromPendingRequest(CPageData dataLoan, CAppData dataApp, PendingDocumentRequest pendingRequest, out TitleBorrower titleOnlyCoborrower)
        {
            titleOnlyCoborrower = null;
            if (pendingRequest.CoborrowerTitleBorrowerId.HasValue)
            {
                // 6/12/2014 gf - If they created a request with a title only 
                // co-borrower and then deleted that co-borrower from the file
                // we just want to send the request out to the borrower on file.
                titleOnlyCoborrower = dataLoan.sTitleBorrowers
                    .FirstOrDefault(b => b.Id == pendingRequest.CoborrowerTitleBorrowerId.Value);
            }

            Func<string, string> getInitial = (s) => (s != null && s.Length > 0) ? s.Substring(0, 1) : string.Empty;

            string coborrowerName = titleOnlyCoborrower != null ? titleOnlyCoborrower.FullName : dataApp.aCNm;
            string coborrowerFirstInitial = titleOnlyCoborrower != null ? getInitial(titleOnlyCoborrower.FirstNm) : getInitial(dataApp.aCFirstNm);
            string coborrowerLastInitial = titleOnlyCoborrower != null ? getInitial(titleOnlyCoborrower.LastNm) : getInitial(dataApp.aCLastNm);

            var consumerActionItem = new ConsumerActionItem(
                pendingRequest.LoanId,
                pendingRequest.ApplicationId,
                dataApp.aBNm,
                coborrowerName,
                getInitial(dataApp.aBFirstNm) + getInitial(dataApp.aBLastNm),
                coborrowerFirstInitial + coborrowerLastInitial,
                pendingRequest.CreatorEmployeeId);

            if (titleOnlyCoborrower != null)
            {
                consumerActionItem.SetTitleCoborrower(titleOnlyCoborrower);
            }

            return consumerActionItem;
        }

        /// <summary>
        /// Verifies that the document requests are of the given type and associated 
        /// with the given loan and application.
        /// </summary>
        /// <param name="loanId">The id of the loan with which the requests should be associated.</param>
        /// <param name="applicationId">The id of the application with which the requests should be associated.</param>
        /// <param name="requestType">The expected type of all requests.</param>
        /// <param name="pendingRequests">The requests.</param>
        private static void ValidateDocumentRequests(
            Guid loanId,
            Guid applicationId,
            DocumentRequestType requestType,
            IGrouping<DocumentRequestType, PendingDocumentRequest> pendingRequests)
        {
            if (pendingRequests.Any(r => r.RequestType != requestType))
            {
                throw CBaseException.GenericException("Implementation error. " +
                    "Trying to create shared document requests for the wrong type of request.");
            }
            else if (pendingRequests.Any(r => r.LoanId != loanId))
            {
                throw CBaseException.GenericException("Implementation error. " +
                    "Attempting to send receive document requests for wrong loans.");
            }
            else if (pendingRequests.Any(r => r.ApplicationId != applicationId))
            {
                throw CBaseException.GenericException("Implementation error. " +
                    "Attempting to send receive document requests for wrong applications.");
            }
        }

        /// <summary>
        /// Sends receive document requests for the given loan and application.
        /// Removes the requests from the scheduling pool.
        /// </summary>
        /// <param name="dataLoan">The loan associated with the requests.</param>
        /// <param name="dataApp">The application associated with the requests.</param>
        /// <param name="pendingRequests">The pending document requests to send.</param>
        private static void SendReceiveDocumentRequests(CPageData dataLoan, CAppData dataApp, IGrouping<DocumentRequestType, PendingDocumentRequest> pendingRequests)
        {
            ValidateDocumentRequests(
                dataLoan.sLId,
                dataApp.aAppId,
                DocumentRequestType.ReceiveDocumentFromBorrower,
                pendingRequests);

            ConsumerActionItem consumerActionItem = null;
            int numRequests = 0;

            foreach (var pendingRequest in pendingRequests)
            {
                TitleBorrower titleOnlyCoborrower = null;
                try
                {
                    consumerActionItem = CreateConsumerActionItemFromPendingRequest(dataLoan, dataApp, pendingRequest, out titleOnlyCoborrower);
                }
                catch (CBaseException exc)
                {
                    var errorMessage = string.Format(
                        "[PendingDocumentRequestHandler]There was an error processing a pending document request. " +
                        "Unable to create ConsumerActionItem. {0}. Request Id: {1}, " +
                        "Loan Id: {2}, App Id: {3}, Broker Id: {4}",
                        exc.UserMessage,
                        pendingRequest.Id.Value,
                        pendingRequest.LoanId,
                        pendingRequest.ApplicationId,
                        pendingRequest.BrokerId);

                    Tools.LogInfo(errorMessage);

                    pendingRequest.SetErrorMessage(exc.UserMessage);
                    continue;
                }

                consumerActionItem.RequestDocument(
                    pendingRequest.DocTypeId.Value,
                    pendingRequest.Description);

                ////consumerActionItem.Save(false);

                // If the request was successfully sent, we want to delete the 
                // it and leave the file in FileDB. The new request will be using 
                // the same file.
                ////pendingRequest.Delete(false);

                Action<CStoredProcedureExec> transaction = (spExec) =>
                {
                    consumerActionItem.Save(spExec);
                    pendingRequest.Delete(spExec);
                };

                var transactionError = "[PendingDocumentRequestHandler]" +
                    "There was an error in creating a consumer action item or " +
                    "deleting a pending document request, rolling back.";

                // We want to make sure that the creation of the new request AND
                // the deletion of the pending request succeed. If the deletion
                // of the old request fails but we've created a new request, it 
                // will generate duplicates.
                PerformTransaction(dataLoan.sBrokerId, transaction, transactionError);

                numRequests++;
            }

            if (numRequests > 0)
            {
                EDocumentNotifier.SetupAccountsAndSendEmails(
                    consumerActionItem,
                    dataLoan.sBrokerId,
                    pendingRequests.First().CreatorEmployeeId,
                    null,
                    numRequests > 1);
            }
        }

        /// <summary>
        /// Sends shared document requests for the given loan and application.
        /// Removes the requests from the scheduling pool.
        /// </summary>
        /// <param name="dataLoan">The loan associated with the requests.</param>
        /// <param name="dataApp">The application associated with the requests.</param>
        /// <param name="pendingRequests">The pending document requests to send.</param>
        private static void SendSharedDocumentRequest(CPageData dataLoan, CAppData dataApp, IGrouping<DocumentRequestType, PendingDocumentRequest> pendingRequests)
        {
            ValidateDocumentRequests(
                dataLoan.sLId,
                dataApp.aAppId,
                DocumentRequestType.SendDocumentToBorrower,
                pendingRequests);

            var sharedDocumentRequests = new List<SharedDocumentRequest>();

            foreach (var pendingRequest in pendingRequests)
            {
                var sharedDocumentRequest = SharedDocumentRequest.InstantiateRequest(pendingRequest);

                if (pendingRequest.EDocId.HasValue)
                {
                    sharedDocumentRequest.SetEDocument(
                        pendingRequest.FileDbKey,
                        pendingRequest.EDocId.Value,
                        pendingRequest.EDocVersion.Value);
                }
                else
                {
                    sharedDocumentRequest.SetForm(pendingRequest.FileDbKey);
                }

                ////sharedDocumentRequest.FinalizeCreation();

                // If the request was successfully sent, we want to delete the 
                // it and leave the file in FileDB. The new request will be using 
                // the same file.
                ////pendingRequest.Delete(false);

                Action<CStoredProcedureExec> transaction = (spExec) => 
                {
                    sharedDocumentRequest.FinalizeCreation(spExec);
                    pendingRequest.Delete(spExec);
                };

                var errorMessage = "[PendingDocumentRequestHandler]" +
                    "There was an error in creating a shared document request or " +
                    "deleting a pending document request, rolling back.";

                // We want to make sure that the creation of the new request and
                // the deletion of the pending request BOTH succeed. If the we
                // are unable to delete the old request, we need to revert the
                // creation of the new one in order to avoid creating duplicates.
                PerformTransaction(dataLoan.sBrokerId, transaction, errorMessage);

                sharedDocumentRequests.Add(sharedDocumentRequest);
            }

            if (sharedDocumentRequests.Any())
            {
                EDocumentNotifier.NotifyBorrowersAboutSharedDoc(sharedDocumentRequests.First());

                foreach (var docRequest in sharedDocumentRequests.Skip(1))
                {
                    docRequest.MarkAsNotified();
                }
            }
        }

        /// <summary>
        /// Sends sign document requests for the given loan and application.
        /// Removes the requests from the scheduling pool.
        /// </summary>
        /// <param name="dataLoan">The loan associated with the requests.</param>
        /// <param name="dataApp">The application associated with the requests.</param>
        /// <param name="pendingRequests">The pending document requests to send.</param>
        /// <param name="areAllBorrowerEmailsUnique">Indicates whether all the borrowers on the loan have unique emails.</param>
        private static void SendSignDocumentRequests(CPageData dataLoan, CAppData dataApp, IGrouping<DocumentRequestType, PendingDocumentRequest> pendingRequests, bool areAllBorrowerEmailsUnique)
        {
            ValidateDocumentRequests(
                dataLoan.sLId,
                dataApp.aAppId,
                DocumentRequestType.SendDocumentToBorrowerForSigning,
                pendingRequests);

            ConsumerActionItem consumerActionItem = null;
            int numRequests = 0;

            foreach (var pendingRequest in pendingRequests)
            {
                if (pendingRequest.IsESignAllowed.Value && !areAllBorrowerEmailsUnique)
                {
                    pendingRequest.SetErrorMessage(
                        "Each borrower must have a unique e-mail address for " +
                        "online signing.");

                    Tools.LogInfo(
                        string.Format(
                            "[PendingDocumentRequestHandler]Prevented e-sign request " +
                            "with non-unique borrower emails for Request Id: {0}, " +
                            "Loan Id: {1}, App Id: {2}, Broker Id: {3}.",
                            pendingRequest.Id,
                            pendingRequest.LoanId,
                            pendingRequest.ApplicationId,
                            pendingRequest.BrokerId));

                    continue;
                }

                TitleBorrower titleOnlyCoborrower = null;
                try
                {
                    consumerActionItem = CreateConsumerActionItemFromPendingRequest(dataLoan, dataApp, pendingRequest, out titleOnlyCoborrower);
                }
                catch (CBaseException exc)
                {
                    var errorMessage = string.Format(
                        "[PendingDocumentRequestHandler]There was an error processing a pending document request. " +
                        "Unable to create ConsumerActionItem. {0}. Request Id: {1}, " +
                        "Loan Id: {2}, App Id: {3}, Broker Id: {4}",
                        exc.UserMessage,
                        pendingRequest.Id.Value,
                        pendingRequest.LoanId,
                        pendingRequest.ApplicationId,
                        pendingRequest.BrokerId);

                    Tools.LogInfo(errorMessage);

                    pendingRequest.SetErrorMessage(exc.UserMessage);
                    continue;
                }

                bool isFormApplicableToBorrower = pendingRequest.IsRequestBorrowerSignature.Value && 
                    !string.IsNullOrEmpty(dataApp.aBNm);

                string coborrowerName = titleOnlyCoborrower == null ? dataApp.aCNm : titleOnlyCoborrower.FullName;

                bool isFormApplicableToCoborrower = pendingRequest.IsRequestCoborrowerSignature.Value &&
                    !string.IsNullOrEmpty(coborrowerName);

                if (pendingRequest.IsESignAllowed.Value && !isFormApplicableToBorrower && !isFormApplicableToCoborrower)
                {
                    pendingRequest.SetErrorMessage("Please enter valid a borrower name in order to send this request.");
                    continue;
                }

                consumerActionItem.RequestSignature(
                    new Guid(pendingRequest.FileDbKey),
                    pendingRequest.DocumentLayoutXml,
                    pendingRequest.DocTypeId.Value,
                    pendingRequest.Description,
                    pendingRequest.IsESignAllowed.Value,
                    isFormApplicableToBorrower,
                    isFormApplicableToCoborrower);

                ////consumerActionItem.Save(false);

                // If the request was successfully sent, we want to delete it
                // and leave the file in FileDB. The new request will be using 
                // the same file.
                ////pendingRequest.Delete(false);

                Action<CStoredProcedureExec> transaction = (spExec) =>
                {
                    consumerActionItem.Save(spExec);
                    pendingRequest.Delete(spExec);
                };

                var transactionError = "[PendingDocumentRequestHandler]" +
                    "There was an error in creating a consumer action item or " +
                    "deleting a pending document request, rolling back.";

                // We want to make sure that the creation of the new request AND
                // the deletion of the pending request succeed. If the deletion
                // of the old request fails but we've created a new request, it 
                // will generate duplicates.
                PerformTransaction(dataLoan.sBrokerId, transaction, transactionError);

                numRequests++;
            }

            if (numRequests > 0)
            {
                EDocumentNotifier.SetupAccountsAndSendEmails(
                    consumerActionItem,
                    dataLoan.sBrokerId,
                    pendingRequests.First().CreatorEmployeeId,
                    null,
                    numRequests > 1);
            }
        }

        /// <summary>
        /// Performs the given action within a transaction and commits the changes 
        /// to the database. If there is an exception thrown during the execution
        /// of the action, it will roll back the changes and re-throw.
        /// </summary>
        /// <param name="brokerId">The broker id of this document request handler.</param>
        /// <param name="action">The action to take with the CStoredProcedureExec.</param>
        /// <param name="errorMessage">The error message to log in case the operation fails.</param>
        private static void PerformTransaction(Guid brokerId, Action<CStoredProcedureExec> action, string errorMessage)
        {
            using (var storedProcedureExec = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    storedProcedureExec.BeginTransactionForWrite();

                    action(storedProcedureExec);

                    storedProcedureExec.CommitTransaction();
                }
                catch (Exception exc)
                {
                    Tools.LogError(errorMessage, exc);

                    try
                    {
                        storedProcedureExec.RollbackTransaction();
                    }
                    catch (InvalidOperationException) 
                    { 
                    }

                    throw;
                }
            }
        }
    }
}