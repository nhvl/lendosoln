﻿namespace LendersOffice.ObjLib.ConsumerPortal
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using EDocs;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;

    public enum  E_ShareStatusT : byte 
    {
        New = 0,
        BorrowerInformed = 1,
        BorrowerDownloaded = 2
    }
    /// <summary>
    /// TODO: Add code for app Deletion
    /// </summary>
    public sealed class SharedDocumentRequest
    {
        public long Id { get; private set; }
        public Guid sLId { get; private set; }
        public Guid aAppId { get; private set; }
        public string aBFullName { get; private set; }
        public string aCFullName { get; private set; }
        public E_ShareStatusT ShareStatusT { get; private set; }
        public string Description { get; set; }
        public Guid CreatorEmployeeId { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public Guid BrokerId { get; private set; }


        /// <summary>
        /// Theres NO FK it may not exist - used to let users know which documents have been shared
        /// </summary>
        public Guid? LinkedEDocId { get; private set; }
        public int? LinkedEDocVersion { get; private set; }
        public DateTime? DownloadedOn { get; private set; }


        private SharedDocumentRequest(DbDataReader reader)
        {
            IsNew = false;
            Id = (long)reader["Id"];
            sLId = (Guid)reader["sLId"];
            aAppId = (Guid)reader["aAppId"];
            aBFullName = (string)reader["aBFullName"];
            aCFullName = (string)reader["aCFullName"];
            ShareStatusT = (E_ShareStatusT)reader["ShareStatusT"];
            Description = (string)reader["Description"];
            CreatorEmployeeId = (Guid)reader["CreatorEmployeeId"];
            BrokerId = (Guid)reader["BrokerId"];
            var d = reader["CreateOnD"];
            PdfFileDbKey = (string)reader["PdfFileDbKey"];
            CreatedOn = (DateTime)d;
            LinkedEDocId = reader.AsNullableGuid("EDocId");
            LinkedEDocVersion = reader.AsNullableInt("EDocVersion");
            DownloadedOn = reader.AsNullableDateTime("DownloadedOn");
        }

        private SharedDocumentRequest()
        {
            IsNew = true;
        }

        private byte[] m_pdfBytes = null;
        private string PdfFileDbKey { get;  set; }
        private bool IsNew { get;  set; }

        private void SetDocPath(byte[] pdfBytes)
        {
            if (!this.IsNew || !string.IsNullOrEmpty(this.PdfFileDbKey))
            {
                throw CBaseException.GenericException("Cannot set a new form to a existing shared document or to a document that already has a FileDB key.");
            }
            m_pdfBytes = pdfBytes;
        }

        /// <summary>
        /// Fetches the shared document from file db into a file and returns it path.
        /// </summary>
        /// <returns></returns>
        public string GetDocumentPath()
        {
            string path = FileDBTools.CreateCopy(E_FileDB.EDMS, PdfFileDbKey);
            return path;
        }

        public void MarkAsNotified()
        {
            if (IsNew)
            {
                throw CBaseException.GenericException("Cannot mark document as notified when its new.");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId), 
                                            new SqlParameter("@id", Id)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_ShareDocument_MarkAsNotified", 3, parameters);
        }

        public void MarkDocumentAsDownloaded()
        {
            if (IsNew)
            {
                throw CBaseException.GenericException("Cannot mark document as downloaded when its new.");
            }
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId), 
                                            new SqlParameter("@id", Id)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_ShareDocument_MarkAsDownloaded", 3, parameters );
        }


        public void SetEDocument(byte[] pdfBytes, Guid edocId, int version)
        {
            LinkedEDocId = edocId;
            LinkedEDocVersion = version;
            SetDocPath(pdfBytes);
        }

        /// <summary>
        /// Sets the EDoc associated with the request.
        /// </summary>
        /// <param name="fileDbKey">The key to access this file in EDMS FileDB.</param>
        /// <param name="edocId">The id of the EDoc.</param>
        /// <param name="version">The version of the EDoc.</param>
        public void SetEDocument(string fileDbKey, Guid edocId, int version)
        {
            this.LinkedEDocId = edocId;
            this.LinkedEDocVersion = version;

            if (!this.IsNew || this.m_pdfBytes != null)
            {
                throw CBaseException.GenericException("Should not try to set the EDoc after it has already been set.");
            }

            this.PdfFileDbKey = fileDbKey;
        }

        public void SetForm(byte[] pdfBytes)
        {
            LinkedEDocVersion = null;
            LinkedEDocId = null;
            SetDocPath(pdfBytes);
        }

        /// <summary>
        /// Set the form to a file that already has been copied to EDMS FileDB.
        /// </summary>
        /// <param name="fileDbKey">The key to access the document in EDMS FileDB.</param>
        public void SetForm(string fileDbKey)
        {
            LinkedEDocVersion = null;
            LinkedEDocId = null;

            if (!this.IsNew || this.m_pdfBytes != null)
            {
                throw CBaseException.GenericException("Should not try to set the form after it has already been set.");
            }
            this.PdfFileDbKey = fileDbKey;
        }

        public void FinalizeCreation(CStoredProcedureExec spExec)
        {
            if (!IsNew)
            {
                throw CBaseException.GenericException("Cannot finalize a saved share document.");
            }

            if (m_pdfBytes == null && string.IsNullOrEmpty(this.PdfFileDbKey))
            {
                throw CBaseException.GenericException("Need to set a pdf file first.");
            }
            else if (m_pdfBytes != null)
            {
                //make sure its a valid pdf
                EDocumentViewer.GetPDFPageCount(m_pdfBytes);

                PdfFileDbKey = GenerateNewFileDBKey();
                string tempFile = TempFileUtils.NewTempFilePath();
                BinaryFileHelper.WriteAllBytes(tempFile, m_pdfBytes);
                FileDBTools.WriteFile(E_FileDB.EDMS, PdfFileDbKey, tempFile);
            }


            SqlParameter[] parameters = new SqlParameter[] {
                    new SqlParameter("@sLId", sLId),
                    new SqlParameter("@aAppId", aAppId),
                    new SqlParameter("@Description", Description),
                    new SqlParameter("@CreatorEmployeeId", CreatorEmployeeId),
                    new SqlParameter("@LinkedEDocId", LinkedEDocId),
                    new SqlParameter("@LinkedEDocVersion", LinkedEDocVersion),
                    new SqlParameter("@PdfFileDbKey", PdfFileDbKey),
                    new SqlParameter("@BrokerId", BrokerId)
                };

            using (DbDataReader reader = spExec.ExecuteReader("CP_ShareDocument_Add", parameters))
            {
                if (!reader.Read())
                {
                    throw CBaseException.GenericException("No results when adding a new shared document.  The parameters were: "
                        + SqlParamSerializer.ToString(parameters));
                }

                aBFullName = (string)reader["aBFullName"];
                aCFullName = (string)reader["aCFullName"];
                Id = (long)reader["Id"];
                IsNew = false;
            }
        }

        public void FinalizeCreation()
        {
            using (var spExec = new CStoredProcedureExec(this.BrokerId))
            {
                try
                {
                    spExec.BeginTransactionForWrite();
                    this.FinalizeCreation(spExec);
                    spExec.CommitTransaction();
                }
                catch (Exception exc)
                {
                    Tools.LogError("Failed to finalize creation of shared document request.", exc);

                    try
                    {
                        spExec.RollbackTransaction();
                    }
                    catch (InvalidOperationException) { }

                    throw;
                }
            }
        }

        public static SharedDocumentRequest InstantiateRequest(AbstractUserPrincipal principal, Guid sLid, Guid aAppId, string description)
        {
            SharedDocumentRequest documentRequest = new SharedDocumentRequest();
            documentRequest.aAppId = aAppId;
            documentRequest.sLId = sLid;
            documentRequest.BrokerId = principal.BrokerId;
            documentRequest.ShareStatusT = E_ShareStatusT.New;
            documentRequest.CreatedOn = DateTime.Now;
            documentRequest.Description = description;
            documentRequest.CreatorEmployeeId = principal.EmployeeId;
            return documentRequest;
        }

        /// <summary>
        /// Creates a SharedDocumentRequest instance from a pending document request.
        /// </summary>
        /// <param name="pendingRequest">The pending document request.</param>
        /// <returns>The new instance of the SharedDocumentRequest class.</returns>
        public static SharedDocumentRequest InstantiateRequest(PendingDocumentRequest pendingRequest)
        {
            var documentRequest = new SharedDocumentRequest();
            documentRequest.aAppId = pendingRequest.ApplicationId;
            documentRequest.sLId = pendingRequest.LoanId;
            documentRequest.BrokerId = pendingRequest.BrokerId;
            documentRequest.ShareStatusT = E_ShareStatusT.New;
            documentRequest.CreatedOn = DateTime.Now;
            documentRequest.Description = pendingRequest.Description;
            documentRequest.CreatorEmployeeId = pendingRequest.CreatorEmployeeId;
            return documentRequest;
        }

        public static IEnumerable<SharedDocumentRequest> GetRequestByLoanId(Guid brokerId, Guid sLId, Guid? aAppId)
        {
            List<SharedDocumentRequest> items = new List<SharedDocumentRequest>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@sLId", sLId),
                                            new SqlParameter("@aAppId", aAppId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_ShareDocument_GetByLoan", parameters))
            {
                while (reader.Read())
                {
                    items.Add(new SharedDocumentRequest(reader));
                }
            }

            return items;
        }

        public static void Delete(Guid brokerId, long id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "CP_ShareDocument_Delete",3,parameters);
        }

        public static SharedDocumentRequest GetRequestById(Guid brokerId, long id)
        {
            List<SharedDocumentRequest> items = new List<SharedDocumentRequest>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_ShareDocument_GetById",parameters))
            {
                if (reader.Read())
                {
                    return new SharedDocumentRequest(reader);
                }
            }

            return null;
        }

        public static string GenerateNewFileDBKey()
        {
            return string.Format("SharedDocumentRequest_{0}.pdf", Guid.NewGuid());
        }
    }
}
