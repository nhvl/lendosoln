﻿// <copyright file="PendingDocumentRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/4/2014 3:09:33 PM 
// </summary>
namespace LendersOffice.ObjLib.ConsumerPortal
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a document request to one or more borrowers.
    /// </summary>
    public class PendingDocumentRequest
    {
        /// <summary>
        /// Initializes a new instance of the PendingDocumentRequest class.
        /// </summary>
        /// <param name="requestType">The type of the document request.</param>
        /// <param name="loanId">The id of the loan.</param>
        /// <param name="applicationId">The id of the application.</param>
        /// <param name="principal">The principal of the requesting user.</param>
        /// <param name="description">The description of the request.</param>
        public PendingDocumentRequest(DocumentRequestType requestType, Guid loanId, Guid applicationId, AbstractUserPrincipal principal, string description)
            : this(requestType, loanId, applicationId, principal.BrokerId, principal.EmployeeId, description)
        {
        }

        /// <summary>
        /// Initializes a new instance of the PendingDocumentRequest class.
        /// </summary>
        /// <param name="requestType">The type of the document request.</param>
        /// <param name="loanId">The id of the loan.</param>
        /// <param name="applicationId">The id of the application.</param>
        /// <param name="brokerId">The id of the broker.</param>
        /// <param name="employeeId">The id of the creating employee.</param>
        /// <param name="description">The description of the request.</param>
        public PendingDocumentRequest(DocumentRequestType requestType, Guid loanId, Guid applicationId, Guid brokerId, Guid employeeId, string description)
        {
            this.RequestType = requestType;
            this.LoanId = loanId;
            this.ApplicationId = applicationId;
            this.BrokerId = brokerId;
            this.CreatorEmployeeId = employeeId;
            this.Description = description;
            this.ScheduledSendTime = DateTime.Now.AddMinutes(ConstStage.DocumentRequestQueueWaitTime);
        }

        /// <summary>
        /// Initializes a new instance of the PendingDocumentRequest class from the database.
        /// </summary>
        /// <param name="reader">The data provider.</param>
        public PendingDocumentRequest(DbDataReader reader)
        {
            this.Id = (int)reader["Id"];
            this.RequestType = (DocumentRequestType)reader["RequestType"];
            this.LoanId = (Guid)reader["LoanId"];
            this.ApplicationId = (Guid)reader["ApplicationId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.CreatorEmployeeId = reader["CreatorEmployeeId"] == DBNull.Value ? Guid.Empty : (Guid)reader["CreatorEmployeeId"];
            this.Description = (string)reader["Description"];
            this.ScheduledSendTime = (DateTime)reader["ScheduledSendTime"];

            this.ErrorMessage = reader.AsNullableString("ErrorMessage");

            this.DocTypeId = reader.AsNullableInt("DocTypeId");
            this.FileDbKey = reader.AsNullableString("FileDbKey");
            this.DocumentLayoutXml = reader.AsNullableString("DocumentLayoutXml");
            this.EDocId = reader.AsNullableGuid("EDocId");
            this.EDocVersion = reader.AsNullableInt("EDocVersion");

            this.IsESignAllowed = reader.AsNullableBool("IsESignAllowed");
            this.IsRequestBorrowerSignature = reader.AsNullableBool("IsRequestBorrowerSignature");
            this.IsRequestCoborrowerSignature = reader.AsNullableBool("IsRequestCoborrowerSignature");

            this.CoborrowerTitleBorrowerId = reader.AsNullableGuid("CoborrowerTitleBorrowerId");
        }

        /// <summary>
        /// Gets the identifier of the request.
        /// </summary>
        /// <value>
        /// The id of the request. This will be assigned after the instance is saved.
        /// If the request has not been saved, the Id will be null.
        /// </value>
        public int? Id { get; private set; }

        /// <summary>
        /// Gets the loan id associated with the request.
        /// </summary>
        /// <value>
        /// The id of the associated loan.
        /// </value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the application id associated with the request.
        /// </summary>
        /// <value>
        /// The id of the associated application.
        /// </value>
        public Guid ApplicationId { get; private set; }

        /// <summary>
        /// Gets the id of the broker associated with the request.
        /// </summary>
        /// <value>
        /// The id of the broker associated with the request.
        /// </value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the employee id of the user who created the request.
        /// </summary>
        /// <value>
        /// The employee id of the user who created the request.
        /// </value>
        public Guid CreatorEmployeeId { get; private set; }

        /// <summary>
        /// Gets the type of the request.
        /// </summary>
        /// <value>
        /// The type of the request.
        /// </value>
        public DocumentRequestType RequestType { get; private set; }

        /// <summary>
        /// Gets the description of the request.
        /// </summary>
        /// <value>
        /// The description of the request.
        /// </value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the time when the request should be sent to the borrower.
        /// </summary>
        /// <value>
        /// The earliest time at which the request should be processed.
        /// </value>
        public DateTime ScheduledSendTime { get; private set; }

        /// <summary>
        /// Gets the document type id for the request.
        /// </summary>
        /// <value>
        /// The id of the document type for the request.
        /// </value>
        public int? DocTypeId { get; private set; }

        /// <summary>
        /// Gets the title id of the borrower when applicable.
        /// </summary>
        /// <value>
        /// If the co-borrower is a title only borrower, returns the title id. Otherwise, returns null.
        /// </value>
        public Guid? CoborrowerTitleBorrowerId { get; private set; }

        /// <summary>
        /// Gets the key to access the document in EDMS FileDB.
        /// </summary>
        /// <value>
        /// The key to access the document in EDMS FileDB.
        /// </value>
        public string FileDbKey { get; private set; }

        /// <summary>
        /// Gets the id of the EDoc associated with the request.
        /// </summary>
        /// <value>
        /// The id of the EDoc associated with the request.
        /// If there is no EDoc associated with the request, null.
        /// </value>
        public Guid? EDocId { get; private set; }

        /// <summary>
        /// Gets the version of the EDoc associated with the request.
        /// </summary>
        /// <value>
        /// The version of the EDoc associated with the request.
        /// If there is no EDoc associated with the request, null.
        /// </value>
        public int? EDocVersion { get; private set; }

        /// <summary>
        /// Gets the form layout information of the document.
        /// </summary>
        /// <value>
        /// The layout xml of the document.
        /// </value>
        public string DocumentLayoutXml { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this request can be e-signed by the recipient.
        /// </summary>
        /// <value>
        /// True if the document can be e-signed. Otherwise, false.
        /// If this setting is not applicable to the request type, this will be null.
        /// </value>
        public bool? IsESignAllowed { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the document requests the borrower's signature.
        /// </summary>
        /// <value>
        /// True if the document requests the borrower's signature. Otherwise, false.
        /// If this setting is not applicable to the request type, this will be null.
        /// </value>
        public bool? IsRequestBorrowerSignature { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the document requests the co-borrower's signature.
        /// </summary>
        /// <value>
        /// True if the document requests the co-borrower's signature. Otherwise, false.
        /// If this setting is not applicable to the request type, this will be null.
        /// </value>
        public bool? IsRequestCoborrowerSignature { get; private set; }

        /// <summary>
        /// Gets the error message for this request.
        /// </summary>
        /// <value>
        /// The error message. If there has not been an error while processing
        /// this request, the value will be null.
        /// </value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Gets or sets the content of the file that will be sent to the borrower.
        /// </summary>
        /// <value>
        /// The content of the file that will be sent to the borrower.
        /// If requesting to receive a document, this will be null.
        /// </value>
        private byte[] PdfFileContent { get; set; }

        /// <summary>
        /// Moves pending document request from the source loan to the destination loan.
        /// </summary>
        /// <param name="brokerId">The broker id that owns both loan files.</param>
        /// <param name="src_sLId">The loan that contains the pending document request.</param>
        /// <param name="dst_sLId">The loan that will receive the pending document request.</param>
        /// <param name="src_aAppId">The application the document request belongs to.</param>
        /// <param name="dst_aAppId">The application the document request is being moved to.</param>
        public static void MoveExistingToNewLoan(Guid brokerId, Guid src_sLId, Guid dst_sLId, Guid src_aAppId, Guid dst_aAppId)
        {
            SqlParameter[] parameters = 
            { 
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@src_sLId", src_sLId),
                new SqlParameter("@dst_sLId", dst_sLId),
                new SqlParameter("@src_aAppId", src_aAppId),
                new SqlParameter("@dst_aAppId", dst_aAppId)
            };

            // The stored procedure does a cross broker check.
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "PENDING_DOCUMENT_REQUEST_MoveExistingToNewLoan", 3, parameters);
        }

        /// <summary>
        /// Sets the content of the document that will be sent to the borrower.
        /// </summary>
        /// <param name="pdfBytes">The content of the file.</param>
        public void SetDocument(byte[] pdfBytes)
        {
            this.PdfFileContent = pdfBytes;
        }

        /// <summary>
        /// Sets the content of the document that will be sent to the borrower.
        /// </summary>
        /// <param name="pdfBytes">The content of the file.</param>
        /// <param name="edocId">The id of the EDocument that was used for the request.</param>
        /// <param name="edocVersion">The version of the EDocument that will be used for the request.</param>
        public void SetDocument(byte[] pdfBytes, Guid edocId, int edocVersion)
        {
            // Verify that the file is valid.
            EDocs.EDocumentViewer.GetPDFPageCount(pdfBytes);

            this.PdfFileContent = pdfBytes;
            this.EDocId = edocId;
            this.EDocVersion = edocVersion;
        }

        /// <summary>
        /// Sets the document for the request.
        /// </summary>
        /// <param name="pdfBytes">The content of the document.</param>
        /// <param name="layoutXml">The layout information of the document.</param>
        /// <param name="docTypeId">The document type id of the document.</param>
        /// <param name="containsBorrowerSignature">Whether or not the document contains a field for the borrower signature.</param>
        /// <param name="containsCoborrowerSignature">Whether or not the document contains a field for the co-borrower signature.</param>
        public void SetDocument(byte[] pdfBytes, string layoutXml, int docTypeId, bool containsBorrowerSignature, bool containsCoborrowerSignature)
        {
            // Verify that the file is valid.
            EDocs.EDocumentViewer.GetPDFPageCount(pdfBytes);

            this.PdfFileContent = pdfBytes;
            this.DocumentLayoutXml = layoutXml;
            this.DocTypeId = docTypeId;
            this.IsESignAllowed = false;
            this.IsRequestBorrowerSignature = containsBorrowerSignature;
            this.IsRequestCoborrowerSignature = containsCoborrowerSignature;
        }

        /// <summary>
        /// Sets the document type for the request.
        /// </summary>
        /// <param name="docTypeId">The id of the document type.</param>
        public void SetDocType(int docTypeId)
        {
            this.DocTypeId = docTypeId;
        }

        /// <summary>
        /// Sets the title only borrower id of the co-borrower. Verifies that the 
        /// borrower has a valid email and SSN.
        /// </summary>
        /// <param name="borrower">The title only co-borrower.</param>
        /// <exception cref="CBaseException">
        /// Throws CBaseException if the borrower does not have a valid email 
        /// and a valid SSN.
        /// </exception>
        public void SetTitleOnlyCoborrower(TitleBorrower borrower)
        {
            if (!UserDataValidation.IsValidEmailAddress(borrower.Email) ||
                !UserDataValidation.IsValidSSN(borrower.SSN))
            {
                throw new CBaseException(
                    "Title Borrower must have a valid email and ssn to sign.",
                    "Title borrower doesn't have valid email or ssn.");
            }

            this.CoborrowerTitleBorrowerId = borrower.Id;
        }

        /// <summary>
        /// Sets the error message and immediately saves the error status to 
        /// the database. Will not hit the database if the error message is the
        /// same as the current error message.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        public void SetErrorMessage(string errorMessage)
        {
            if (errorMessage == this.ErrorMessage)
            {
                return;
            }

            this.ErrorMessage = errorMessage;
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@ErrorMessage", this.ErrorMessage),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "PENDING_DOCUMENT_REQUEST_UpdateErrorMessageById", 3, parameters);
        }

        /// <summary>
        /// Save the request to the scheduling pool. Assigns the id after
        /// successful save. If there is an associated document, it is copied
        /// to FileDB. This is only meant to handle newly created requests.
        /// </summary>
        public void Save()
        {
            this.ValidateRequest();
            
            if (this.PdfFileContent != null)
            {
                this.FileDbKey = this.GetFileDbKeyForRequestType(this.RequestType);
                FileDBTools.WriteData(E_FileDB.EDMS, this.FileDbKey, this.PdfFileContent);
            }

            var outputId = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            outputId.Direction = System.Data.ParameterDirection.Output;

            var parameters = new SqlParameter[]
            {
                outputId, 
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@ApplicationId", this.ApplicationId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@CreatorEmployeeId", this.CreatorEmployeeId),
                new SqlParameter("@RequestType", this.RequestType),
                new SqlParameter("@Description", this.Description),
                new SqlParameter("@ScheduledSendTime", this.ScheduledSendTime),
                new SqlParameter("@DocTypeId", (object)this.DocTypeId ?? DBNull.Value),
                new SqlParameter("@FileDbKey", (object)this.FileDbKey ?? DBNull.Value),
                new SqlParameter("@EDocId", (object)this.EDocId ?? DBNull.Value),
                new SqlParameter("@EDocVersion", (object)this.EDocVersion ?? DBNull.Value),
                new SqlParameter("@DocumentLayoutXml", (object)this.DocumentLayoutXml ?? DBNull.Value),
                new SqlParameter("@IsESignAllowed", (object)this.IsESignAllowed ?? DBNull.Value),
                new SqlParameter("@IsRequestBorrowerSignature", (object)this.IsRequestBorrowerSignature ?? DBNull.Value),
                new SqlParameter("@IsRequestCoborrowerSignature", (object)this.IsRequestCoborrowerSignature ?? DBNull.Value),
                new SqlParameter("@CoborrowerTitleBorrowerId", (object)this.CoborrowerTitleBorrowerId ?? DBNull.Value),
                new SqlParameter("@ErrorMessage", (object)this.ErrorMessage ?? DBNull.Value),
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "PENDING_DOCUMENT_REQUEST_Create", 3, parameters);

            this.Id = (int)outputId.Value;
        }

        /// <summary>
        /// Deletes the file from the scheduling pool. Optionally deletes the 
        /// associated file from FileDB.
        /// </summary>
        /// <param name="deleteFileFromFileDB">
        /// Indicates whether or not the associated file should be deleted 
        /// from FileDB. If the request is being deleted before it has been
        /// sent, the file should be deleted.
        /// </param>
        public void Delete(bool deleteFileFromFileDB)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "PENDING_DOCUMENT_REQUEST_Delete", 3, parameters);

            if (deleteFileFromFileDB && !string.IsNullOrEmpty(this.FileDbKey))
            {
                FileDBTools.Delete(E_FileDB.EDMS, this.FileDbKey);
            }
        }

        /// <summary>
        /// Delete the file in the given transaction. Does not delete the file from
        /// FileDB.
        /// </summary>
        /// <param name="storedProcedureExec">The transaction in which to delete.</param>
        public void Delete(CStoredProcedureExec storedProcedureExec)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            storedProcedureExec.ExecuteNonQuery("PENDING_DOCUMENT_REQUEST_Delete", 3, parameters);
        }

        /// <summary>
        /// Gets the FileDB key depending on the request type.
        /// </summary>
        /// <param name="requestType">The type of the request.</param>
        /// <returns>The FileDB key to access the file.</returns>
        private string GetFileDbKeyForRequestType(DocumentRequestType requestType)
        {
            switch (requestType)
            {
                case DocumentRequestType.ReceiveDocumentFromBorrower:
                    throw new CBaseException(ErrorMessages.Generic, "This request type should never have an associated pdf");
                case DocumentRequestType.SendDocumentToBorrower:
                    return SharedDocumentRequest.GenerateNewFileDBKey();
                case DocumentRequestType.SendDocumentToBorrowerForSigning:
                    return ConsumerActionItem.GenerateNewFileDbKey().ToString();
                default:
                    throw new UnhandledEnumException(requestType);
            }
        }

        /// <summary>
        /// Validates that the request has the necessary information to create
        /// a request of the given type. Enforces that this instance was not
        /// already saved to the database.
        /// </summary>
        private void ValidateRequest()
        {
            if (this.Id.HasValue)
            {
                throw CBaseException.GenericException("Programming error: " +
                    "this instance was already saved to the database.");
            }

            string errorMessage = null;
            switch (this.RequestType)
            {
                case DocumentRequestType.ReceiveDocumentFromBorrower:
                    if (this.DocTypeId == null)
                    {
                        errorMessage = "A DocTypeId is required when creating a " +
                            "receive document from borrower request.";
                    }

                    break;
                case DocumentRequestType.SendDocumentToBorrower:
                    if (this.PdfFileContent == null)
                    {
                        errorMessage = "A pdf file must be set when creating a send document to borrower request.";
                    }

                    break;
                case DocumentRequestType.SendDocumentToBorrowerForSigning:
                    var missingFields = new List<string>();
                    if (this.PdfFileContent == null)
                    {
                        missingFields.Add("PdfFileContent");
                    }
                    else if (this.DocTypeId == null)
                    {
                        missingFields.Add("DocTypeId");
                    }
                    else if (this.IsESignAllowed == null)
                    {
                        missingFields.Add("IsESignAllowed");
                    }
                    else if (this.IsRequestBorrowerSignature == null)
                    {
                        missingFields.Add("IsRequestBorrowerSignature");
                    }
                    else if (this.IsRequestCoborrowerSignature == null)
                    {
                        missingFields.Add("IsRequestCoborrowerSignature");
                    }

                    if (missingFields.Any())
                    {
                        string requiredFieldsMessage = "A sign document request must have " +
                            "settings for PdfFileContent, DocTypeId, IsESignAllowed, " +
                            "IsRequestBorrowerSignature, and IsRequestCoborrowerSignature.";

                        string missingFieldsMessage = "The following fields were missing: " +
                            string.Join(", ", missingFields.ToArray());

                        errorMessage = string.Format("{0} {1}", requiredFieldsMessage, missingFieldsMessage);
                    }

                    break;
                default:
                    throw new UnhandledEnumException(this.RequestType);
            }

            if (errorMessage != null)
            {
                throw CBaseException.GenericException(errorMessage);
            }
        }
    }
}
