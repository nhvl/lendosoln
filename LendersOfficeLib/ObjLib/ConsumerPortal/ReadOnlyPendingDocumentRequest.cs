﻿// <copyright file="ReadOnlyPendingDocumentRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/9/2014 11:15:05 AM 
// </summary>
namespace LendersOffice.ObjLib.ConsumerPortal
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides the information needed to display pending document requests in the UI.
    /// </summary>
    public class ReadOnlyPendingDocumentRequest
    {
        /// <summary>
        /// The type of the request.
        /// </summary>
        private DocumentRequestType requestType;

        /// <summary>
        /// The full name of the co-borrower from the co-borrower database fields.
        /// We don't use this as a property directly because we will need to 
        /// decide if we need to use the title only co-borrower information from
        /// the loan.
        /// </summary>
        private string coborrowerFullName;

        /// <summary>
        /// Gets the co-borrower title only borrower id. If there isn't one, 
        /// this will be null.
        /// </summary>
        private Guid? coborrowerTitleBorrowerId = null;

        /// <summary>
        /// Indicates whether the instance has attempted to load the title only 
        /// co-borrower.
        /// </summary>
        private bool loadedTitleOnlyCoborrower = false;

        /// <summary>
        /// A cached copy of the title-only co-borrower. Used to avoid hitting
        /// the loan multiple times.
        /// </summary>
        private TitleBorrower cachedTitleOnlyCoborrower = null;

        /// <summary>
        /// Initializes a new instance of the ReadOnlyPendingDocumentRequest class from the database.
        /// </summary>
        /// <param name="reader">The data source.</param>
        public ReadOnlyPendingDocumentRequest(DbDataReader reader)
        {
            this.Id = (int)reader["Id"];
            this.requestType = (DocumentRequestType)reader["RequestType"];
            this.LoanId = (Guid)reader["LoanId"];
            this.ApplicationId = (Guid)reader["ApplicationId"];
            this.FolderDescription = reader.AsNullableString("FolderDescription");
            this.DocTypeDescription = reader.AsNullableString("DocTypeDescription");
            this.BorrowerFullName = (string)reader["BorrowerFullName"];
            this.coborrowerFullName = (string)reader["CoborrowerFullName"];
            this.Description = (string)reader["Description"];
            this.ErrorMessage = reader.AsNullableString("ErrorMessage");
            this.ScheduledSendTime = (DateTime)reader["ScheduledSendTime"];
            this.coborrowerTitleBorrowerId = reader.AsNullableGuid("CoborrowerTitleBorrowerId");
            this.BorrowerEmail = (string)reader["BorrowerEmail"];
            this.CoborrowerEmail = (string)reader["CoborrowerEmail"];
        }

        /// <summary>
        /// Gets the id of the request.
        /// </summary>
        /// <value>
        /// The id of the request.
        /// </value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the id of the associated loan.
        /// </summary>
        /// <value>
        /// The id of the associated loan.
        /// </value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the id of the associated application.
        /// </summary>
        /// <value>
        /// The id of the associated application.
        /// </value>
        public Guid ApplicationId { get; private set; }

        /// <summary>
        /// Gets the friendly description of the request type.
        /// </summary>
        /// <value>
        /// The description of the request type.
        /// </value>
        public string RequestTypeDescription 
        {
            get
            {
                switch (this.requestType)
                {
                    case DocumentRequestType.ReceiveDocumentFromBorrower:
                        return "Send in a document";
                    case DocumentRequestType.SendDocumentToBorrower:
                        return "Receive a document";
                    case DocumentRequestType.SendDocumentToBorrowerForSigning:
                        return "Sign a form";
                    default:
                        throw new UnhandledEnumException(this.requestType);
                }
            }
        }

        /// <summary>
        /// Gets the associated folder name.
        /// </summary>
        /// <value>
        /// The EDocs folder name.
        /// </value>
        public string FolderDescription { get; private set; }

        /// <summary>
        /// Gets the associated doc type name.
        /// </summary>
        /// <value>
        /// The EDocs doc type name.
        /// </value>
        public string DocTypeDescription { get; private set; }

        /// <summary>
        /// Gets the full name of the borrower.
        /// </summary>
        /// <value>
        /// The full name of the borrower.
        /// </value>
        public string BorrowerFullName { get; private set; }

        /// <summary>
        /// Gets the email of the borrower associated with the request.
        /// </summary>
        /// <value>
        /// Returns empty string if no borrower on file.
        /// </value>
        public string BorrowerEmail { get; private set; }

        /// <summary>
        /// Gets the full name of the co-borrower.
        /// </summary>
        /// <value>
        /// The full name of the co-borrower.
        /// </value>
        public string CoborrowerFullName 
        {
            get
            {
                if (this.TitleOnlyCoborrower != null)
                {
                    return string.Format("{0} {1}", this.TitleOnlyCoborrower.FirstNm, this.TitleOnlyCoborrower.LastNm).TrimWhitespaceAndBOM();
                }
                else
                {
                    return this.coborrowerFullName.TrimWhitespaceAndBOM();
                }
            }
        }

        /// <summary>
        /// Gets the description of the request.
        /// </summary>
        /// <value>
        /// The description of the request.
        /// </value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the time this message is scheduled to be sent.
        /// </summary>
        /// <value>
        /// The time this message is scheduled to be sent.
        /// </value>
        public DateTime ScheduledSendTime { get; private set; }

        /// <summary>
        /// Gets the string representation of the scheduled send time.
        /// </summary>
        /// <value>
        /// A string representation of the date and time.
        /// </value>
        public string ScheduledSendTime_rep
        {
            get
            {
                return string.Format(
                    "{0} on {1}",
                    Tools.GetTimeDescription(this.ScheduledSendTime),
                    this.ScheduledSendTime.ToString("d"));
            }
        }

        /// <summary>
        /// Gets the error message for this pending request.
        /// </summary>
        /// <value>
        /// The error message if one has been set. Otherwise, null.
        /// </value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Gets the email of the co-borrower on the file. DOES NOT return 
        /// email of title only co-borrower.
        /// </summary>
        /// <value>
        /// Returns empty string if no co-borrower on file.
        /// </value>
        public string CoborrowerEmail { get; private set; }

        /// <summary>
        /// Gets the title only co-borrower associated with the request. 
        /// </summary>
        /// <value>
        /// The title only co-borrower associated with the request. Returns
        /// null if is not associated with a title only co-borrower or if the 
        /// associated borrower was deleted.
        /// </value>
        private TitleBorrower TitleOnlyCoborrower
        {
            get
            {
                if (this.cachedTitleOnlyCoborrower == null && !this.loadedTitleOnlyCoborrower)
                {
                    if (this.coborrowerTitleBorrowerId.HasValue)
                    {
                        var dataLoan = CPageData.CreateUsingSmartDependency(
                            this.LoanId,
                            typeof(ReadOnlyPendingDocumentRequest));
                        dataLoan.InitLoad();

                        this.cachedTitleOnlyCoborrower = dataLoan.sTitleBorrowers
                            .FirstOrDefault(b => b.Id == this.coborrowerTitleBorrowerId.Value);
                    }

                    this.loadedTitleOnlyCoborrower = true;
                }

                return this.cachedTitleOnlyCoborrower;
            }
        }
    }
}
