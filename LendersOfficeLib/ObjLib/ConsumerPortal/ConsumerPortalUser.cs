﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using EDocs;

namespace LendersOffice.ObjLib.ConsumerPortal
{
    public enum E_SignatureFontType 
    {
        LucidaHandwriting = 0
    }
    /// <summary>
    /// Represents a document the consumer has access to. 
    /// It can represent a edoc or it can be a document request. 
    /// </summary>
    public sealed class DocumentItem
    {
        public string Description { get; set; }
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }

        public bool CanSignOnline { get; set; }
        public bool CanDownload { get; set; }
        public bool CanUpload { get; set; }
        public bool CanFaxIn { get; set; }
        public string Status { get; set; }
        public string RequiredSignatureMessage { get; set; }
        public bool IsNew { get; set; }
        public DocumentItem()
        {
            IsNew = false;
            Description = "";
            DocumentType = "";
            Status = "";
            RequiredSignatureMessage = "";
        }

    }

    public sealed class LinkedLoanApp
    {
        public Guid sLId { get; set; }
        public string sLNm { get; set; }
        public E_sStatusT sStatusT { get; set; }
        public string sSpAddr { get; set; }
        public string sSpCity { get; set; }
        public string sSpZip { get; set; }
        public string sSpCounty { get; set; }
        public string sSpState { get; set; }
        public decimal sFinalLAmt { get; set; }
        public DateTime? sOpenedD { get; set; }
        public DateTime? sLeadD { get; set; }
        public DateTime? sConsumerPortalSubmittedD { get; set; }
        public DateTime? sConsumerPortalVerbalSubmissionD { get; set; }
        public E_sLPurposeT sLPurposeT { get; set; }
        public E_sLienPosT sLienPosT { get; set; }
        public E_sSubFinT sSubFinT { get; set; }

        public E_aOccT aOccT { get; set; }
        public E_sProdSpT sProdSpT { get; set;}

        public string aBFirstNm { get; set; }
        public string aBMidNm { get; set; }
        public string aBLastNm { get; set; }
        public string aBSuffix { get; set; }
        public string aBNm
        {
            get { return Tools.ComposeFullName(aBFirstNm, aBMidNm, aBLastNm, aBSuffix); }
        }
        public string sSpFullAddr
        {
            get
            {
                StringBuilder sBuild = new StringBuilder(60);
                sBuild.Append(sSpAddr);

                if (sBuild.Length > 0 && sSpCity.Length > 0)
                    sBuild.Append(", ");
                sBuild.Append(sSpCity);

                if (sBuild.Length > 0 && sSpState.Length > 0)
                    sBuild.Append(", ");
                sBuild.Append(sSpState);

                if (sBuild.Length > 0 && sSpZip.Length > 0)
                    sBuild.Append((sSpState.Length > 0) ? " " : ", ");
                sBuild.Append(sSpZip);

                if (sBuild.Length > 0 && sSpCounty.Length > 0)
                    sBuild.Append(", ");
                sBuild.Append(sSpCounty);

                return sBuild.ToString();
            }
        }
        public string LoanStatus
        {
            get { return CPageBase.sStatusT_map_rep(sStatusT); }
        }

        // 7/6/2016 BS - Add new fields in order to be able to mark the application as submitted or not based on David's logic in case 242509
        public DateTime? sInitialLoanEstimateCreatedD { get; set; }
        
        public DateTime? sGfeInitialDisclosureD { get; set; }

        public DateTime? sConsumerPortalCreationD { get; set; }

        public DateTime? sAppSubmittedD { get; set; }
    }

    public sealed class ConsumerPortalUser
    {
        private LqbGrammar.DataTypes.EncryptionKeyIdentifier encryptionKeyId;

        private Lazy<string> lazySecurityPin = new Lazy<string>(() => string.Empty);

        public long Id { get; private set; }
        public Guid BrokerId { get; private set; }
        public DateTime? DisclosureAcceptedD { get; set; }
        public DateTime? CreateD { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get;  set; }
        public int LoginAttempts { get;  set; }
        public bool IsTemporaryPassword { get; private set; }
        public DateTime? LastLoginD { get;  set; }
        public string LastName { get;  set; }
        public string PasswordHash { get; private set; }
        public string PasswordResetCode { get;  set; }
        public DateTime? PasswordResetCodeRequestD { get;  set; }
        public string PasswordSalt { get; private set; }
        public string Phone { get;  set; }
        public DateTime? TempPasswordSetD { get; private set; }
        public bool IsNotifyOnNewDocRequests { get; set; }
        public bool IsNotifyOnStatusChanges { get; set; }
        public string NotificationEmail { get; set; }
        public E_SignatureFontType SignatureFontType { get; set; }
        public string SignatureName { get; set; }
        public string ReferralSource { get; set; }
        public string SecurityPin
        {
            get { return this.lazySecurityPin.Value; }
            set { this.lazySecurityPin = new Lazy<string>(() => value); }
        }

        public bool IsSecurityPinSet => !string.IsNullOrEmpty(this.SecurityPin);
        public string SignatureInitials
        {
            get
            {
                string initials = "";

                if (FirstName.Length > 0)
                {
                    initials = FirstName.Substring(0, 1);
                }
                if (LastName.Length > 0)
                {
                    initials += LastName.Substring(0, 1);
                }

                return initials;
            }
        }

        public bool IsLockedOut
        {
            get
            {
                return LoginAttempts > 10;
            }
        }

        private bool IsNew
        {
            get { return Id == -1; }
        }

        private ConsumerPortalUser()
        {
            Id = -1;
        }

        private ConsumerPortalUser(IDataRecord reader)
        {
            Id = (long)reader["Id"];
            BrokerId = (Guid)reader["BrokerId"];
            DisclosureAcceptedD = reader.AsNullableDateTime("DisclosureAcceptedD");
            CreateD = reader.AsNullableDateTime("CreateD");
            Email = (string)reader["Email"];
            FirstName = (string)reader["FirstName"];
            LoginAttempts = (int)reader["LoginAttempts"];
            IsTemporaryPassword = (bool)reader["IsTemporaryPassword"];
            LastLoginD = reader.AsNullableDateTime("LastLoginD");
            LastName = (string)reader["LastName"];
            PasswordHash = (string)reader["PasswordHash"];
            PasswordResetCode = (string)reader["PasswordResetCode"];
            PasswordResetCodeRequestD = reader.AsNullableDateTime("PasswordResetCodeRequestD");
            PasswordSalt = (string)reader["PasswordSalt"];
            Phone = (string)reader["Phone"];
            TempPasswordSetD = reader.AsNullableDateTime("TempPasswordSetD");
           
            IsNotifyOnNewDocRequests = (bool)reader["IsNotifyOnNewDocRequests"];
            IsNotifyOnStatusChanges = (bool)reader["IsNotifyOnStatusChanges"];
            SignatureName = (string)reader["SignatureName"];
            SignatureFontType = (E_SignatureFontType)reader["SignatureFontType"];
            NotificationEmail = (string)reader["NotificationEmail"];
            ReferralSource = (string)reader["ReferralSource"];
            var maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
            if (maybeEncryptionKeyId.HasValue)
            {
                this.encryptionKeyId = maybeEncryptionKeyId.Value;
                byte[] encryptedSecurityPin = (byte[])reader["EncryptedSecurityPin"];
                this.lazySecurityPin = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedSecurityPin));
            }
            else
            {
                string securityPin = (string)reader["SecurityPin"];
                this.lazySecurityPin = new Lazy<string>(() => securityPin);
            }
        }

        private List<SqlParameter> GenerateParameters()
        {
            byte[] encryptedSecurityPin = null;
            if (this.encryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
            {
                encryptedSecurityPin = EncryptionHelper.EncryptString(this.encryptionKeyId, this.lazySecurityPin.Value);
            }

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@Id", Id),
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@DisclosureAcceptedD", DisclosureAcceptedD ),
                new SqlParameter("@CreateD", CreateD ),
                new SqlParameter("@Email", Email ),
                new SqlParameter("@FirstName", FirstName ),
                new SqlParameter("@LoginAttempts", LoginAttempts ),
                new SqlParameter("@IsTemporaryPassword", IsTemporaryPassword ),
                new SqlParameter("@LastLoginD", LastLoginD ),
                new SqlParameter("@LastName", LastName ),
                new SqlParameter("@PasswordHash", PasswordHash ),
                new SqlParameter("@PasswordResetCode", PasswordResetCode ),
                new SqlParameter("@PasswordResetCodeRequestD", PasswordResetCodeRequestD ),
                new SqlParameter("@PasswordSalt", PasswordSalt ),
                new SqlParameter("@Phone", Phone ),
                new SqlParameter("@TempPasswordSetD", TempPasswordSetD),
                new SqlParameter("@IsNotifyOnNewDocRequests", IsNotifyOnNewDocRequests),
                new SqlParameter("@IsNotifyOnStatusChanges", IsNotifyOnStatusChanges),
                new SqlParameter("@SignatureName", SignatureName),
                new SqlParameter("@SignatureFontType", SignatureFontType),
                new SqlParameter("@NotificationEmail", NotificationEmail),
                new SqlParameter("@ReferralSource", ReferralSource),
                new SqlParameter("@SecurityPin", this.lazySecurityPin.Value),
                new SqlParameter("@EncryptedSecurityPin", encryptedSecurityPin ?? new byte[0]),
            };
            return parameters;
        }
        private void Create()
        {
            this.encryptionKeyId = EncryptionHelper.GenerateNewKey();
            List<SqlParameter> parameters = GenerateParameters();
            parameters.Add(new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value));
            try
            {

                Id = (long)StoredProcedureHelper.ExecuteScalar(BrokerId, "CONSUMER_PORTAL_User_Create", parameters);
            }
            catch (SqlException exc)
            {
                if (exc.Message.Contains("Cannot insert duplicate key in object"))
                {
                    CBaseException exception = new CBaseException("Account already exists.", "Account already exists.");
                    exception.IsEmailDeveloper = false;

                    throw exception;
                }
                throw;
            }
        }

        private void Update()
        {
            List<SqlParameter> parameters = GenerateParameters();
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CONSUMER_PORTAL_User_Update", 1, parameters);
        }

        public void Save()
        {
            if (Email.Length > 80)
            {
                throw CBaseException.GenericException("Email is invalid length.");
            }
            if (IsNew)
            {
                Create();
            }
            else
            {
                Update();
            }
        }

        public void InitializePasswordReset()
        {
            PasswordResetCode = EncryptionHelper.GeneratePassword(10);

            PasswordResetCodeRequestD = DateTime.Now;
            LoginAttempts = 0;
        }

        public string GenerateTempPassword()
        {
            string password, passwordSalt, passwordHash;
            password = EncryptionHelper.GetPasswordAndPBKDF2HashSalt(out passwordHash, out passwordSalt);

            PasswordSalt = passwordSalt;
            PasswordHash = passwordHash;
            IsTemporaryPassword = true;
            TempPasswordSetD = DateTime.Now;
            LoginAttempts = 0;
            PasswordResetCode = "";
            PasswordResetCodeRequestD = null;
            return password;
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("password cannot be empty.");
            }

            password = password.TrimWhitespaceAndBOM();

            string salt;
            PasswordHash = EncryptionHelper.GeneratePBKDF2Hash(password, out salt);
            PasswordSalt = salt;
            IsTemporaryPassword = false;
            TempPasswordSetD = null;
            LoginAttempts = 0;
            PasswordResetCode = "";
            PasswordResetCodeRequestD = null;
        }

        public bool IsPasswordValid(string password)
        {
            // 2/21/2015 dd - OPM 203448 - Trim whitespace just in case user copy-n-paste password contains extra whitespace.
            password = password.TrimWhitespaceAndBOM();

            return EncryptionHelper.ValidatePBKDF2Hash(password, PasswordHash, PasswordSalt);
        }

        public IEnumerable<DocumentItem> GetAccessibleDocuments(Guid sLId, out bool requestDisclosure)
        {
            requestDisclosure = !this.DisclosureAcceptedD.HasValue;
            //if (requestDisclosure) //
            //{
            //    requestDisclosure = true;
            //    return new DocumentItem[0]; 
            //}
            CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(ConsumerPortalUser));
            data.InitLoad();

            Guid aAppId = Guid.Empty;

            for (int i = 0; i < data.nApps; i++)
            {
                CAppData app = data.GetAppData(i);

                if (app.aCEmail.TrimWhitespaceAndBOM().Equals(Email, StringComparison.OrdinalIgnoreCase) ||
                    app.aBEmail.TrimWhitespaceAndBOM().Equals(Email, StringComparison.OrdinalIgnoreCase))
                {
                    aAppId = app.aAppId;
                    break;
                }
            }

            // The user can be a title borrower.  OPM 111877. Theres multiple matches because the email can show up twice.
            Dictionary<Guid, TitleBorrower> titleIds = data.sTitleBorrowers.Where(x => x.Email.Equals(Email, StringComparison.OrdinalIgnoreCase)).ToDictionary(p => p.Id);


            List<DocumentItem> documents = new List<DocumentItem>();
            documents.Add(new DocumentItem()
            {
                DocumentId = new Guid("99999999-9999-9999-9999-999999999999").ToString(),
                Description = "Federal E-Sign Disclosure",
                CanDownload = true,
                DocumentType = "EDocument"
            });
            var folders = EDocumentFolder.GetFoldersInBroker(BrokerId);
            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(BrokerId);
            foreach (EDocument doc in repo.GetDocumentsByLoanId(sLId))
            {
                if (!doc.AppId.HasValue || doc.AppId.Value != aAppId) //they can only get documents that are accepted by the lender
                {
                    continue;
                }

                if (doc.Folder.AssociatedRoleIds.Any(p => p.RoleId == CEmployeeFields.s_ConsumerId ))
                {
                    documents.Add(new DocumentItem()
                    {
                        DocumentId = doc.DocumentId.ToString(),
                        Description = doc.PublicDescription,
                        DocumentType = "EDocument",
                        CanDownload = true
                    });
                }
            }


            //shared documents
            foreach (SharedDocumentRequest doc in SharedDocumentRequest.GetRequestByLoanId(BrokerId, sLId, aAppId))
            {
                documents.Add(new DocumentItem()
                {
                    DocumentId = "s" + doc.Id,
                    Description = doc.Description,
                    DocumentType = "EDocument",
                    IsNew = doc.CreatedOn.AddDays(2) > LastLoginD,
                    CanDownload = true,
                    CanFaxIn = false,
                    CanSignOnline = false,
                    CanUpload = false
                });
            }

            //Need Sign
            List<ReadOnlyConsumerActionItem> consumerActionItems = ReadOnlyConsumerActionItem.GetConsumerActionItems(BrokerId, sLId);

            foreach (ReadOnlyConsumerActionItem item in consumerActionItems)
            {
                if (aAppId != Guid.Empty && aAppId != item.AppId)
                {
                    continue;
                }

                if (aAppId == Guid.Empty && (!item.CoborrowerTitleId.HasValue || !titleIds.ContainsKey(item.CoborrowerTitleId.Value)))
                {
                    continue;
                }


                DocumentItem doc = new DocumentItem();
                documents.Add(doc);
                doc.DocumentId = item.Id.ToString();
                doc.Description = item.RequestDescription;
                doc.IsNew = !LastLoginD.HasValue || (item.CreatedOn.HasValue && item.CreatedOn.Value.AddDays(1) > LastLoginD);

                if (item.ResponseStatus == E_ConsumerResponseStatusT.WaitingForConsumer)
                {
                    if (item.IsSignatureRequired)
                    {
                        doc.DocumentType = "SignatureRequest";
                        doc.CanDownload = true;
                        doc.CanUpload = true;
                        doc.CanFaxIn = true;
                    }
                    else
                    {
                        doc.DocumentType = "DocumentRequest";
                        doc.CanFaxIn = true;
                        doc.CanUpload = true;
                    }
                }
                else if (item.ResponseStatus == E_ConsumerResponseStatusT.PendingResponse)
                {
                    doc.DocumentType = "CompletedRequestPendingApproval";
                    doc.Status = "Awaiting acceptance";
                    doc.CanDownload = true;

                }
                else if (item.ResponseStatus == E_ConsumerResponseStatusT.Accepted)
                {
                    doc.DocumentType = "CompletedRequestAccepted";
                    doc.Status = "Document was accepted on " + item.ConsumerCompletedDate.Value.ToShortDateString();
                    doc.CanDownload = true;
                }
            }

            return documents;
        }

        private static ConsumerPortalUser RetrieveImpl(Guid brokerId, long? id, string email)
        {
            ConsumerPortalUser user = null;
            if (!id.HasValue && string.IsNullOrEmpty(email))
            {
                throw new ArgumentException("Not all parameters can be null.");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", id),
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Email", email)
                                        };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CONSUMER_PORTAL_User_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    user = new ConsumerPortalUser(reader);
                }
            }

            return user;
        }


        public static ConsumerPortalUser Retrieve(Guid brokerId, long id)
        {
            return RetrieveImpl(brokerId, id, null);
        }

        public static ConsumerPortalUser Retrieve(Guid brokerId,  string email)
        {
            return RetrieveImpl(brokerId, null, email);
        }

        public static ConsumerPortalUser Create(Guid brokerId, string email)
        {
            if (email.Length > 80)
            {
                throw new ArgumentException("Email is too long. Has to be 80 characters at most.", "email");
            }
            ConsumerPortalUser newUser = new ConsumerPortalUser()
            {
                BrokerId = brokerId, 
                Email = email,
                CreateD = DateTime.Now,
                DisclosureAcceptedD = null,
                LoginAttempts = 0, 
                IsTemporaryPassword = true, 
                LastLoginD = null,
                PasswordResetCode = null,
                PasswordResetCodeRequestD = null,
                FirstName = "",
                LastName = "",
                Phone = "",
                SignatureName = "",
                SignatureFontType = E_SignatureFontType.LucidaHandwriting,
                NotificationEmail = "",
                IsNotifyOnNewDocRequests = true,
                ReferralSource = "",
                SecurityPin = ""
            };

            return newUser;
        }

        public static IEnumerable<LinkedLoanApp> GetLinkedLoanApps(Guid brokerId, long consumerId)
        {
            List<LinkedLoanApp> loanApps = new List<LinkedLoanApp>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerUserId", consumerId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CONSUMER_PORTAL_User_GetLinkedLoanApps", parameters))
            {
                while (reader.Read())
                {
                    LinkedLoanApp app = new LinkedLoanApp();
                    app.sLId = (Guid)reader["sLId"];
                    app.sLNm = (string)reader["sLNm"];
                    app.sStatusT = (E_sStatusT)reader["sStatusT"];
                    app.sSpAddr = (string)reader["sSpAddr"];
                    app.sSpCity = (string)reader["sSpCity"];
                    app.sSpCounty = (string)reader["sSpCounty"];
                    app.sSpState = (string)reader["sSpState"];
                    app.sSpZip = (string)reader["sSpZip"];
                    app.sFinalLAmt = (decimal)reader["sFinalLAmt"];

                    if(reader["sOpenedD"] != DBNull.Value)
                    {
                        app.sOpenedD = (DateTime)reader["sOpenedD"];
                    }
                    if (reader["sLeadD"] != DBNull.Value)
                    {
                        app.sLeadD = (DateTime)reader["sLeadD"];
                    }
                    if(reader["sConsumerPortalSubmittedD"]!= DBNull.Value)
                    {
                        app.sConsumerPortalSubmittedD = (DateTime)reader["sConsumerPortalSubmittedD"];
                    }
                    if (reader["sConsumerPortalVerbalSubmissionD"] != DBNull.Value)
                    {
                        app.sConsumerPortalVerbalSubmissionD = (DateTime)reader["sConsumerPortalVerbalSubmissionD"];
                    }
                    if (reader["sInitialLoanEstimateCreatedD"] != DBNull.Value)
                    {
                        app.sInitialLoanEstimateCreatedD = (DateTime)reader["sInitialLoanEstimateCreatedD"];
                    }
                    if (reader["sGfeInitialDisclosureD"] != DBNull.Value)
                    {
                        app.sGfeInitialDisclosureD = (DateTime)reader["sGfeInitialDisclosureD"];
                    }
                    if (reader["sConsumerPortalCreationD"] != DBNull.Value)
                    {
                        app.sConsumerPortalCreationD = (DateTime)reader["sConsumerPortalCreationD"];
                    }
                    if (reader["sAppSubmittedD"] != DBNull.Value)
                    {
                        app.sAppSubmittedD = (DateTime)reader["sAppSubmittedD"];
                    }
                    app.sLPurposeT = (E_sLPurposeT)reader["sLPurposeT"];
                    app.sLienPosT = (E_sLienPosT)reader["sLienPosT"];
                    app.sSubFinT = (E_sSubFinT)reader["sSubFinT"];
                    app.aOccT = (E_aOccT)reader["aOccT"];
                    app.sProdSpT = (E_sProdSpT)reader["sProdSpT"];
                    app.aBFirstNm = (string)reader["aBFirstNm"];
                    app.aBMidNm = (string)reader["aBMidNm"];
                    app.aBLastNm = (string)reader["aBLastNm"];
                    app.aBSuffix = (string)reader["aBSuffix"];
                    loanApps.Add(app);

                }
            }

            return loanApps;
        }

        public static bool IsLinkedToLoan(Guid brokerId, long consumerId, Guid sLId)
        {
            return GetLinkedLoanApps(brokerId, consumerId).Where(p => p.sLId == sLId).Any();
        }

        /// <summary>
        /// Will link the consumer to the given loan. If the consumer shows up for that loan  in another app or as coborrower 
        /// they will be purged first. So calling this will result in consumer onyl being linked once in loan.
        /// </summary>
        /// <param name="consumerId"></param>
        /// <param name="sLid"></param>
        /// <param name="aAppId"></param>
        /// <param name="asBorrower"></param>
        public static void LinkLoan(Guid brokerId, long consumerId, Guid sLId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerUserId", consumerId),
                                            new SqlParameter("@sLid", sLId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "CONSUMER_PORTAL_User_LinkLoan", 1, parameters);
        }

        /// <summary>
        /// Retrieves the users who have access to the given loan. If time permits will return a different object
        /// with less details. 
        /// </summary>
        /// <param name="brokerId">Broker id of the loan.</param>
        /// <param name="sLId">Loan Id to check for</param>
        /// <returns>A collection of users who have access to the specified loan.</returns>
        public static IEnumerable<Tuple<ConsumerPortalUser,LinkedLoanApp>> GetConsumerPortalUsersWithAccessToLoan(Guid brokerId, Guid sLId)
        {
            List<Tuple<ConsumerPortalUser,LinkedLoanApp>> users = new List<Tuple<ConsumerPortalUser,LinkedLoanApp>>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CONSUMER_PORTAL_USER_GetByLoan", parameters))
            {
                while (reader.Read())
                {
                    ConsumerPortalUser cp = new ConsumerPortalUser(reader);
                    LinkedLoanApp app = new LinkedLoanApp();
                    app.sLId = (Guid)reader["sLId"];
                    users.Add(Tuple.Create(cp, app));
                }
            }

            return users;
        }
    }
}
