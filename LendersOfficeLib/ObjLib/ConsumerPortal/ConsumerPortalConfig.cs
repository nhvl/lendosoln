using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using LendersOffice.Constants;
using System.Xml.Linq;

namespace LendersOffice.ObjLib.ConsumerPortal
{
    public class ConsumerPortalConfig
    {
        static List<KeyValuePair<string, string>> FIRST_TECH_MASTER_STATE_LIST = new List<KeyValuePair<string, string>>()
        {
            // 6/25/2013 dd - OPM 439274 - Temporary hard code to only show available state for FirstTech
            // AZ, CA, CO, CT, DC, DE, FL, GA, ID, MA, MD, ME, MN, NC, NH, NJ, NM, NV, NY, OH, OR, PA, SC, TX, UT, VA, WA
//            new KeyValuePair<string,string>("AK","ALASKA"),
//new KeyValuePair<string,string>("AL","ALABAMA"),
////new KeyValuePair<string,string>("AP","AP"),
//new KeyValuePair<string,string>("AR","ARKANSAS"),
//new KeyValuePair<string,string>("AS","AMERICAN SAMOA"),
new KeyValuePair<string,string>("AZ","ARIZONA"),
new KeyValuePair<string,string>("CA","CALIFORNIA"),
new KeyValuePair<string,string>("CO","COLORADO"),
new KeyValuePair<string,string>("CT","CONNECTICUT"),
new KeyValuePair<string,string>("DC","DISTRICT OF COLUMBIA"),
new KeyValuePair<string,string>("DE","DELAWARE"),
new KeyValuePair<string,string>("FL","FLORIDA"),
new KeyValuePair<string,string>("GA","GEORGIA"),
////new KeyValuePair<string,string>("GU","GUAM"),
//new KeyValuePair<string,string>("HI","HAWAII"),
//new KeyValuePair<string,string>("IA","IOWA"),
new KeyValuePair<string,string>("ID","IDAHO"),
//new KeyValuePair<string,string>("IL","ILLINOIS"),
//new KeyValuePair<string,string>("IN","INDIANA"),
//new KeyValuePair<string,string>("KS","KANSAS"),
//new KeyValuePair<string,string>("KY","KENTUCKY"),
//new KeyValuePair<string,string>("LA","LOUISIANA"),
new KeyValuePair<string,string>("MA","MASSACHUSETTS"),
new KeyValuePair<string,string>("MD","MARYLAND"),
new KeyValuePair<string,string>("ME","MAINE"),
//new KeyValuePair<string,string>("MI","MICHIGAN"),
new KeyValuePair<string,string>("MN","MINNESOTA"),
//new KeyValuePair<string,string>("MO","MISSOURI"),
////new KeyValuePair<string,string>("MP","NORTHERN MARIANA ISLANDS"),
//new KeyValuePair<string,string>("MS","MISSISSIPPI"),
//new KeyValuePair<string,string>("MT","MONTANA"),
new KeyValuePair<string,string>("NC","NORTH CAROLINA"),
//new KeyValuePair<string,string>("ND","NORTH DAKOTA"),
//new KeyValuePair<string,string>("NE","NEBRASKA"),
new KeyValuePair<string,string>("NH","NEW HAMPSHIRE"),
new KeyValuePair<string,string>("NJ","NEW JERSEY"),
new KeyValuePair<string,string>("NM","NEW MEXICO"),
new KeyValuePair<string,string>("NV","NEVADA"),
new KeyValuePair<string,string>("NY","NEW YORK"),
new KeyValuePair<string,string>("OH","OHIO"),
//new KeyValuePair<string,string>("OK","OKLAHOMA"),
new KeyValuePair<string,string>("OR","OREGON"),
new KeyValuePair<string,string>("PA","PENNSYLVANIA"),
////new KeyValuePair<string,string>("PR","PUERTO RICO"),
//new KeyValuePair<string,string>("RI","RHODE ISLAND"),
new KeyValuePair<string,string>("SC","SOUTH CAROLINA"),
//new KeyValuePair<string,string>("SD","SOUTH DAKOTA"),
//new KeyValuePair<string,string>("TN","TENNESSEE"),
new KeyValuePair<string,string>("TX","TEXAS"),
new KeyValuePair<string,string>("UT","UTAH"),
new KeyValuePair<string,string>("VA","VIRGINIA"),
////new KeyValuePair<string,string>("VI","VIRGIN ISLANDS"),
//new KeyValuePair<string,string>("VT","VERMONT"),
new KeyValuePair<string,string>("WA","WASHINGTON"),
//new KeyValuePair<string,string>("WI","WISCONSIN"),
//new KeyValuePair<string,string>("WV","WEST VIRGINIA"),
//new KeyValuePair<string,string>("WY","WYOMING")

        };

        static List<KeyValuePair<string, string>> MASTER_STATE_LIST = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string,string>("AK","ALASKA"),
new KeyValuePair<string,string>("AL","ALABAMA"),
////new KeyValuePair<string,string>("AP","AP"),
new KeyValuePair<string,string>("AR","ARKANSAS"),
//new KeyValuePair<string,string>("AS","AMERICAN SAMOA"),
new KeyValuePair<string,string>("AZ","ARIZONA"),
new KeyValuePair<string,string>("CA","CALIFORNIA"),
new KeyValuePair<string,string>("CO","COLORADO"),
new KeyValuePair<string,string>("CT","CONNECTICUT"),
new KeyValuePair<string,string>("DC","DISTRICT OF COLUMBIA"),
new KeyValuePair<string,string>("DE","DELAWARE"),
new KeyValuePair<string,string>("FL","FLORIDA"),
new KeyValuePair<string,string>("GA","GEORGIA"),
////new KeyValuePair<string,string>("GU","GUAM"),
new KeyValuePair<string,string>("HI","HAWAII"),
new KeyValuePair<string,string>("IA","IOWA"),
new KeyValuePair<string,string>("ID","IDAHO"),
new KeyValuePair<string,string>("IL","ILLINOIS"),
new KeyValuePair<string,string>("IN","INDIANA"),
new KeyValuePair<string,string>("KS","KANSAS"),
new KeyValuePair<string,string>("KY","KENTUCKY"),
new KeyValuePair<string,string>("LA","LOUISIANA"),
new KeyValuePair<string,string>("MA","MASSACHUSETTS"),
new KeyValuePair<string,string>("MD","MARYLAND"),
new KeyValuePair<string,string>("ME","MAINE"),
new KeyValuePair<string,string>("MI","MICHIGAN"),
new KeyValuePair<string,string>("MN","MINNESOTA"),
new KeyValuePair<string,string>("MO","MISSOURI"),
////new KeyValuePair<string,string>("MP","NORTHERN MARIANA ISLANDS"),
new KeyValuePair<string,string>("MS","MISSISSIPPI"),
new KeyValuePair<string,string>("MT","MONTANA"),
new KeyValuePair<string,string>("NC","NORTH CAROLINA"),
new KeyValuePair<string,string>("ND","NORTH DAKOTA"),
new KeyValuePair<string,string>("NE","NEBRASKA"),
new KeyValuePair<string,string>("NH","NEW HAMPSHIRE"),
new KeyValuePair<string,string>("NJ","NEW JERSEY"),
new KeyValuePair<string,string>("NM","NEW MEXICO"),
new KeyValuePair<string,string>("NV","NEVADA"),
new KeyValuePair<string,string>("NY","NEW YORK"),
new KeyValuePair<string,string>("OH","OHIO"),
new KeyValuePair<string,string>("OK","OKLAHOMA"),
new KeyValuePair<string,string>("OR","OREGON"),
new KeyValuePair<string,string>("PA","PENNSYLVANIA"),
////new KeyValuePair<string,string>("PR","PUERTO RICO"),
new KeyValuePair<string,string>("RI","RHODE ISLAND"),
new KeyValuePair<string,string>("SC","SOUTH CAROLINA"),
new KeyValuePair<string,string>("SD","SOUTH DAKOTA"),
new KeyValuePair<string,string>("TN","TENNESSEE"),
new KeyValuePair<string,string>("TX","TEXAS"),
new KeyValuePair<string,string>("UT","UTAH"),
new KeyValuePair<string,string>("VA","VIRGINIA"),
////new KeyValuePair<string,string>("VI","VIRGIN ISLANDS"),
new KeyValuePair<string,string>("VT","VERMONT"),
new KeyValuePair<string,string>("WA","WASHINGTON"),
new KeyValuePair<string,string>("WI","WISCONSIN"),
new KeyValuePair<string,string>("WV","WEST VIRGINIA"),
new KeyValuePair<string,string>("WY","WYOMING")

        };

        #region Private Members
        private bool m_isNew;
        private Guid m_Id;
        private Guid m_BrokerId;
        private string m_Name;
        private bool m_IsEnabled;
        private string m_ReferralUrl;
        private string m_StyleSheetUrl;
        private bool m_IsAskForReferralSource;
        private List<ReferralSource> m_DisplayedReferralSources;
        private bool m_IsAllowLockPeriodSelection;
        private List<int> m_AvailableLockPeriods;
        private Guid m_LeadManagerUserId;
        private Guid m_LoanOfficerUserId;
        private bool m_IsHideLoanOfficer;
        private string m_GetRatesPageDisclaimer;
        private string m_OnlineApplicationTerms;
        private string m_OnlineApplicationCreditCheck;
        private bool m_IsAutomaticPullCredit;
        private Guid m_AutomaticCRAId;
        private string m_AutomaticCRALogin;
        private string m_AutomaticCRAPassword;
        private string m_AutomaticCRAAccountId;
        private string m_AutomaticCRADULogin;
        private string m_AutomaticCRADUPassword;
        private string m_EligibleProgramDisclaimer;
        private string m_NoEligibleProgramMessage;
        private bool m_IsAutomaticRunDU;
        private string m_AutomaticDULogin;
        private string m_AutomaticDUPassword;
        private E_F1003SubmissionReceiptT m_F1003SubmissionReceiptT;
        private Guid m_PreApprovalLetterId;
        private Guid m_PreQualificationLetterdId;
        private string m_ShortApplicationConfirmation;
        private string m_FullApplicationConfirmation;
        private string m_ContactInfoMessage;
        private string m_HelpLinkPagesHostURL;
        private bool m_IsHelpForGetRatesEnabled;
        private bool m_IsHelpForApplicantLoginEnabled;
        private bool m_IsHelpForApplicantLoanPipelineEnabled;
        private bool m_IsHelpForPersonalInformationEnabled;
        private bool m_IsHelpForLoanInformationEnabled;
        private bool m_IsHelpForPropertyInformationEnabled;
        private bool m_IsHelpForLoanOfficerEnabled;
        private bool m_IsHelpForAssetInformationEnabled;
        private bool m_IsHelpForLiabilityInformationEnabled;
        private bool m_IsHelpForExpenseInformationEnabled;
        private bool m_IsHelpForIncomeInformationEnabled;
        private bool m_IsHelpForDeclarationsEnabled;
        private bool m_IsHelpForSelectLoanEnabled;
        private bool m_IsHelpForSubmitApplicationEnabled;
        private bool m_IsHelpForLoanStatusAndDocExchangeEnabled;
        private bool m_IsHelpForEmploymentEnabled;
        private bool m_IsAskForMemberId;
        private HashSet<int> m_enabledCustomPmlFields_FullApp;
        private HashSet<int> m_enabledCustomPmlFields_QuickPricer;
        private string m_ARMProgramDisclosuresURL;
        private E_SelectsLoanProgramInFullSubmission m_SelectsLoanProgramInFullSubmission;
        private Guid m_defaultLoanTemplateId;
        private bool m_SubmissionsShouldBecomeLoans;
        private bool isGenerateLeadOnShortAppSubmission;
        private E_AskConsumerForSubjPropertyT askConsumerForSubjProperty;
        private E_AskConsumerForSubjValue askConsumerForSubjValue;  // Here
        private string loanStatusConfigJson;
        private bool isAllowRespaOnly_Purchase;
        private bool isAllowRespaOnly_Refinance;
        private bool isAllowRespaOnly_HomeEquity;
        private bool enableShortApplications;
        private bool enableFullApplications;
        private bool allowRegisterNewAccounts;
        #endregion

        public bool IsAllowRespaOnly_Purchase
        {
            get { return isAllowRespaOnly_Purchase; }
            set { isAllowRespaOnly_Purchase = value; }
        }

        public bool IsAllowRespaOnly_Refinance
        {
            get { return isAllowRespaOnly_Refinance; }
            set { isAllowRespaOnly_Refinance = value; }
        }

        public bool IsAllowRespaOnly_HomeEquity
        {
            get { return isAllowRespaOnly_HomeEquity; }
            set { isAllowRespaOnly_HomeEquity = value; }
        }

        public bool EnableShortApplications
        {
            get { return enableShortApplications; }
            set { enableShortApplications = value; }
        }

        public bool EnableFullApplications
        {
            get { return enableFullApplications; }
            set { enableFullApplications = value; }
        }

        public bool AllowRegisterNewAccounts
        {
            get { return allowRegisterNewAccounts; }
            set { allowRegisterNewAccounts = value; }
        }

        public E_AskConsumerForSubjPropertyT AskConsumerForSubjProperty
        {
            get { return askConsumerForSubjProperty; }
            set { askConsumerForSubjProperty = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public E_AskConsumerForSubjValue AskConsumerForSubjValue
        {
            get { return askConsumerForSubjValue; }
            set { askConsumerForSubjValue = value; }
        }

        /// <summary>
        /// TODO: Need to create a customizable settings. All emails from consumer portal will use this From Email address.
        /// </summary>
        public string NotificationFromEmailAddress
        {
            get
            {
                return ConstStage.DefaultDoNotReplyAddress;
            }
        }

        public int SessionTimeoutInMinutes
        {
            // 6/19/2013 dd - Temporary set to longer timeout for testing period. 
            // Default to 10 minutes.
            get
            {
                if (this.Id == new Guid("1A723170-F33B-4A1B-902D-211448791CD8"))
                {
                    return 30;
                }
                else
                {
                    return 10;
                }
            }
        }
        #region Public Access
        public Guid Id { get { return m_Id; } }
        public Guid BrokerId { get { return m_BrokerId; } }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public bool IsEnabled
        {
            get { return m_IsEnabled; }
            set { m_IsEnabled = value; }
        }
        public string ReferralUrl
        {
            get { return m_ReferralUrl; }
            set { m_ReferralUrl = value; }
        }
        public string StyleSheetUrl
        {
            get { return m_StyleSheetUrl; }
            set { m_StyleSheetUrl = value; }
        }

        public bool IsAskForReferralSource
        {
            get { return m_IsAskForReferralSource; }
            set { m_IsAskForReferralSource = value; }
        }
        public List<ReferralSource> DisplayedReferralSources
        {
            get
            {
                if (m_DisplayedReferralSources == null)
                    m_DisplayedReferralSources = new List<ReferralSource>();
                return m_DisplayedReferralSources;
            }
            set { m_DisplayedReferralSources = value; }
        }
        public string DisplayedReferralSourcesXmlContent
        {
            get
            {
                XElement referralSources = new XElement("Sources");
                foreach (ReferralSource item in m_DisplayedReferralSources)
                {
                    referralSources.Add(new XElement("Source", new XAttribute("name", item.Name), new XAttribute("id", item.Id)));
                }
                return referralSources.ToString();
            }
            set
            {
                m_DisplayedReferralSources = new List<ReferralSource>();
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                XElement el = XElement.Parse(value);

                foreach (XElement e in el.Elements("Source"))
                {
                    ReferralSource leadSrc = new ReferralSource();
                    leadSrc.Name = e.Attribute("name").Value;
                    int val;
                    var idAttr = e.Attribute("id");
                    if (idAttr != null && int.TryParse(idAttr.Value, out val))
                        leadSrc.Id = val;
                    m_DisplayedReferralSources.Add(leadSrc);
                }
            }
        }
        public bool IsAllowLockPeriodSelection
        {
            get { return m_IsAllowLockPeriodSelection; }
            set { m_IsAllowLockPeriodSelection = value; }
        }
        public List<int> AvailableLockPeriods
        {
            get
            {
                if (m_AvailableLockPeriods == null)
                    m_AvailableLockPeriods = new List<int>();
                return m_AvailableLockPeriods;
            }
            set { m_AvailableLockPeriods = value; }
        }
        private BrokerDB x_brokerDB = null;
        public BrokerDB BrokerDB
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(this.BrokerId);
                }
                return x_brokerDB;
            }
        }
        public IEnumerable<KeyValuePair<string, string>> AvailableStates
        {
            get
            {
                // 6/12/2013 dd - Eventually we may want to provide customize settings that allow certain
                // states to display in consumer portal drop down.
                // Right now just return everything.

                if (BrokerDB.CustomerCode.Equals("PML0223", StringComparison.OrdinalIgnoreCase))
                {
                    return FIRST_TECH_MASTER_STATE_LIST;
                }
                else
                {
                    return MASTER_STATE_LIST;
                }
            }
        }

        public string LoanStatusConfigJson
        {
            get { return this.loanStatusConfigJson; }
            set { this.loanStatusConfigJson = value; }
        }

        public IEnumerable<DateConfig> StatusList
        {
            get
            {
                if (string.IsNullOrEmpty(this.LoanStatusConfigJson))
                {
                    // 6/24/2013 dd - Return a list of status to display in the Loan Status Page.
                    // In future we can provide the UI for this settings. Right now we just hardcode
                    // the settings for FirstTech.
                    var statusList = new List<DateConfig>()
                    {
                        new DateConfig() { FieldId = "sOpenedD", SystemDescription = "Opened" },
                        new DateConfig() { FieldId = "sApprovD", SystemDescription = "Approved" },
                        new DateConfig() { FieldId = "sRLckdD", SystemDescription = "Rate Locked" },
                        new DateConfig() { FieldId = "sPrelimRprtRd", SystemDescription = "Title Report Received" },
                        new DateConfig() { FieldId = "sApprRprtRd", SystemDescription = "Appraisal Received" },
                        new DateConfig() { FieldId = "sClearToCloseD", SystemDescription = "Clear To Close" },
                        new DateConfig() { FieldId = "sClosedD", SystemDescription = "Closed" }
                    };

                    return statusList;
                }
                else
                {
                    return SerializationHelper.JsonNetDeserialize<List<DateConfig>>(this.LoanStatusConfigJson);
                }
            }
        }
        public string GetStatusDescription(E_sStatusT sStatusT)
        {
            // 6/13/2013 dd - Each lender may want to relabel our internal status description to be more consumer friendly.

            switch (sStatusT)
            {
                case E_sStatusT.Loan_Open:
                    return "Submitted"; // 6/13/2013 dd - David W mentioned that consider as submitted when lead convert to open.
                case E_sStatusT.Loan_WebConsumer:
                case E_sStatusT.Lead_New:
                    return "Opened";

                case E_sStatusT.Loan_Prequal:
                    return "Pre-Qual";
                case E_sStatusT.Loan_Preapproval:
                    return "Pre-Approved";
                case E_sStatusT.Loan_Registered:
                    return "Registered";
                case E_sStatusT.Loan_Approved:
                    return "Approved";
                case E_sStatusT.Loan_Docs:
                    return "Loan Docs";
                case E_sStatusT.Loan_Funded:
                    return "Loan Funded";
                case E_sStatusT.Loan_OnHold:
                    return "On Hold";
                case E_sStatusT.Loan_Suspended:
                    return "Suspended";
                case E_sStatusT.Loan_Canceled:
                    return "Canceled";
                case E_sStatusT.Loan_Rejected:
                    return "Rejected";
                case E_sStatusT.Loan_Closed:
                    return "Closed";
                case E_sStatusT.Loan_Underwriting:
                    return "Underwriting";
                case E_sStatusT.Lead_Canceled:
                    return "Lead Canceled";
                case E_sStatusT.Lead_Declined:
                    return "Lead Declined";
                case E_sStatusT.Lead_Other:
                    return "Lead Other";
                case E_sStatusT.Loan_Other:
                    return "Loan Other";
                case E_sStatusT.Loan_Recorded:
                    return "Loan Recorded";
                case E_sStatusT.Loan_Shipped:
                    return "Loan Shipped";
                case E_sStatusT.Loan_ClearToClose:
                    return "Clear To Close";
                case E_sStatusT.Loan_Processing:
                    return "Processing";
                case E_sStatusT.Loan_FinalUnderwriting:
                    return "Final Underwriting";
                case E_sStatusT.Loan_DocsBack:
                    return "Docs Back";
                case E_sStatusT.Loan_FundingConditions:
                    return "Funding Conditions";
                case E_sStatusT.Loan_FinalDocs:
                    return "Final Docs";
                case E_sStatusT.Loan_LoanPurchased:
                    return "Loan Sold";
                case E_sStatusT.Loan_LoanSubmitted:
                    return "Loan Submitted";
                case E_sStatusT.Loan_PreProcessing:
                    return "Pre-Processing";    // start sk 1/6/2014 opm 145251
                case E_sStatusT.Loan_DocumentCheck:
                    return "Document Check";
                case E_sStatusT.Loan_DocumentCheckFailed:
                    return "Document Check Failed";
                case E_sStatusT.Loan_PreUnderwriting:
                    return "Pre-Underwriting";
                case E_sStatusT.Loan_ConditionReview:
                    return "Condition Review";
                case E_sStatusT.Loan_PreDocQC:
                    return "Pre-Doc QC";
                case E_sStatusT.Loan_DocsOrdered:
                    return "Docs Ordered";
                case E_sStatusT.Loan_DocsDrawn:
                    return "Docs Drawn";
                case E_sStatusT.Loan_InvestorConditions:
                    return "Investor Conditions";       //  tied to sSuspendedByInvestorD
                case E_sStatusT.Loan_InvestorConditionsSent:
                    return "Investor Conditions Sent";   //  tied to sCondSentToInvestorD
                case E_sStatusT.Loan_ReadyForSale:
                    return "Ready For Sale";
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                    return "Submitted For Purchase Review";
                case E_sStatusT.Loan_InPurchaseReview:
                    return "In Purchase Review";
                case E_sStatusT.Loan_PrePurchaseConditions:
                    return "Pre-Purchase Conditions";
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                    return "Submitted For Final Purchase Review";
                case E_sStatusT.Loan_InFinalPurchaseReview:
                    return "In Final Purchase Review";
                case E_sStatusT.Loan_ClearToPurchase:
                    return "Clear To Purchase";
                case E_sStatusT.Loan_Purchased:
                    return "Loan Purchased";        // don't confuse with the old case E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                case E_sStatusT.Loan_CounterOffer:
                    return "Counter Offer Approved";
                case E_sStatusT.Loan_Withdrawn:
                    return "Loan Withdrawn";
                case E_sStatusT.Loan_Archived:
                    return "Loan Archived";
                default:
                    throw new UnhandledEnumException(sStatusT);
            }
        }
        public string AvailableLockPeriodsXmlContent
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(List<int>));
                    ser.Serialize(writer, AvailableLockPeriods);
                }
                return sb.ToString();
            }
            set
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<int>));
                using (StringReader reader = new StringReader(value))
                {
                    try
                    {
                        m_AvailableLockPeriods = (List<int>)ser.Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        //empty xml in database
                        m_AvailableLockPeriods = null;
                    }
                }
            }
        }
        public Guid LeadManagerUserId
        {
            get { return m_LeadManagerUserId; }
            set
            {
                m_LeadManagerUserId = value;
                x_leadManagerEmployeeDB = null; // Reset EmployeeDB object.
            }
        }

        private EmployeeDB x_leadManagerEmployeeDB = null;
        public EmployeeDB LeadManagerEmployeeDB
        {
            get
            {
                if (x_leadManagerEmployeeDB == null)
                {
                    x_leadManagerEmployeeDB = EmployeeDB.RetrieveByUserId(this.BrokerId, LeadManagerUserId);
                }
                return x_leadManagerEmployeeDB;
            }
        }
        public Guid LoanOfficerUserId
        {
            get { return m_LoanOfficerUserId; }
            set { m_LoanOfficerUserId = value; }
        }
        private EmployeeDB x_loanOfficerEmployeeDB = null;
        public EmployeeDB LoanOfficerEmployeeDB
        {
            get
            {
                if (x_loanOfficerEmployeeDB == null)
                {
                    x_loanOfficerEmployeeDB = EmployeeDB.RetrieveByUserId(this.BrokerId, LoanOfficerUserId);
                }
                return x_loanOfficerEmployeeDB;
            }
        }
        public bool IsHideLoanOfficer
        {
            get { return m_IsHideLoanOfficer; }
            set { m_IsHideLoanOfficer = value; }
        }
        public string GetRatesPageDisclaimer
        {
            get { return m_GetRatesPageDisclaimer; }
            set { m_GetRatesPageDisclaimer = value; }
        }

        public string OnlineApplicationTerms
        {
            get { return m_OnlineApplicationTerms; }
            set { m_OnlineApplicationTerms = value; }
        }
        public string OnlineApplicationCreditCheck
        {
            get { return m_OnlineApplicationCreditCheck; }
            set { m_OnlineApplicationCreditCheck = value; }
        }
        public bool IsAutomaticPullCredit
        {
            get { return m_IsAutomaticPullCredit; }
            set { m_IsAutomaticPullCredit = value; }
        }
        public Guid AutomaticCRAId
        {
            get { return m_AutomaticCRAId; }
            set { m_AutomaticCRAId = value; }
        }
        public string AutomaticCRALogin
        {
            get { return m_AutomaticCRALogin; }
            set { m_AutomaticCRALogin = value; }
        }
        public string AutomaticCRAPassword
        {
            get { return m_AutomaticCRAPassword; }
            set { m_AutomaticCRAPassword = value; }
        }
        public string AutomaticCRAAccountId
        {
            get { return m_AutomaticCRAAccountId; }
            set { m_AutomaticCRAAccountId = value; }
        }
        public string AutomaticCRADULogin
        {
            get
            {
                // 9/23/2013 dd - Always try to look up in BrokerDB settings first.
                if (string.IsNullOrEmpty(BrokerDB.CreditMornetPlusUserID) == false)
                {
                    return BrokerDB.CreditMornetPlusUserID;
                }
                else
                {
                    return m_AutomaticCRADULogin;
                }
            }
            set { m_AutomaticCRADULogin = value; }
        }
        public string AutomaticCRADUPassword
        {
            get
            {
                // 9/23/2013 dd - Always try to look up in BrokerDB settings first.
                // This is what consumer portal UI setting does
                if (string.IsNullOrEmpty(BrokerDB.CreditMornetPlusPassword.Value) == false)
                {
                    return BrokerDB.CreditMornetPlusPassword.Value;
                }
                else
                {
                    return m_AutomaticCRADUPassword;
                }
            }
            set { m_AutomaticCRADUPassword = value; }
        }
        public bool IsEnableHomeEquityInLoanPurpose
        {
            get
            {
                // 10/14/2013 dd - Per eOPM 463946 We don't want to display
                // home equity loan purpose for all lender.
                return BrokerDB.IsEnableHomeEquityInLoanPurpose;
            }
        }
        public bool IsEnableHELOC
        {
            get
            {
                // 6/5/2015 for case 215527 must send IsEnableHELOC to cpotal to check
                return BrokerDB.IsEnableHELOC;
            }
        }

        public string EligibleProgramDisclaimer
        {
            get { return m_EligibleProgramDisclaimer; }
            set { m_EligibleProgramDisclaimer = value; }
        }
        public string NoEligibleProgramMessage
        {
            get { return m_NoEligibleProgramMessage; }
            set { m_NoEligibleProgramMessage = value; }
        }
        public bool IsAutomaticRunDU
        {
            get { return m_IsAutomaticRunDU; }
            set { m_IsAutomaticRunDU = value; }
        }
        public string AutomaticDULogin
        {
            get { return m_AutomaticDULogin; }
            set { m_AutomaticDULogin = value; }
        }
        public string AutomaticDUPassword
        {
            get { return m_AutomaticDUPassword; }
            set { m_AutomaticDUPassword = value; }
        }

        public string AutomaticDUCraId { get; set; }

        /// <summary>
        /// This is the cra login to use when running DU.
        /// </summary>
        public string AutomaticDUCraLogin { get; set; }

        /// <summary>
        /// This is the cra password to use when running DU.
        /// </summary>
        public string AutomaticDUCraPassword { get; set; }

        public E_F1003SubmissionReceiptT F1003SubmissionReceiptT
        {
            get { return m_F1003SubmissionReceiptT; }
            set { m_F1003SubmissionReceiptT = value; }
        }
        public Guid PreApprovalLetterId
        {
            get { return m_PreApprovalLetterId; }
            set { m_PreApprovalLetterId = value; }
        }
        public Guid PreQualificationLetterdId
        {
            get { return m_PreQualificationLetterdId; }
            set { m_PreQualificationLetterdId = value; }
        }
        public string ShortApplicationConfirmation
        {
            get { return m_ShortApplicationConfirmation; }
            set { m_ShortApplicationConfirmation = value; }
        }
        public string FullApplicationConfirmation
        {
            get { return m_FullApplicationConfirmation; }
            set { m_FullApplicationConfirmation = value; }
        }
        public string ContactInfoMessage
        {
            get { return m_ContactInfoMessage; }
            set { m_ContactInfoMessage = value; }
        }
        public string HelpLinkPagesHostURL
        {
            get { return m_HelpLinkPagesHostURL; }
            set { m_HelpLinkPagesHostURL = value; }
        }
        public bool IsHelpForGetRatesEnabled
        {
            get { return m_IsHelpForGetRatesEnabled; }
            set { m_IsHelpForGetRatesEnabled = value; }
        }
        public bool IsHelpForApplicantLoginEnabled
        {
            get { return m_IsHelpForApplicantLoginEnabled; }
            set { m_IsHelpForApplicantLoginEnabled = value; }
        }
        public bool IsHelpForApplicantLoanPipelineEnabled
        {
            get { return m_IsHelpForApplicantLoanPipelineEnabled; }
            set { m_IsHelpForApplicantLoanPipelineEnabled = value; }
        }
        public bool IsHelpForPersonalInformationEnabled
        {
            get { return m_IsHelpForPersonalInformationEnabled; }
            set { m_IsHelpForPersonalInformationEnabled = value; }
        }
        public bool IsHelpForLoanInformationEnabled
        {
            get { return m_IsHelpForLoanInformationEnabled; }
            set { m_IsHelpForLoanInformationEnabled = value; }
        }
        public bool IsHelpForPropertyInformationEnabled
        {
            get { return m_IsHelpForPropertyInformationEnabled; }
            set { m_IsHelpForPropertyInformationEnabled = value; }
        }
        public bool IsHelpForLoanOfficerEnabled
        {
            get { return m_IsHelpForLoanOfficerEnabled; }
            set { m_IsHelpForLoanOfficerEnabled = value; }
        }
        public bool IsHelpForAssetInformationEnabled
        {
            get { return m_IsHelpForAssetInformationEnabled; }
            set { m_IsHelpForAssetInformationEnabled = value; }
        }
        public bool IsHelpForLiabilityInformationEnabled
        {
            get { return m_IsHelpForLiabilityInformationEnabled; }
            set { m_IsHelpForLiabilityInformationEnabled = value; }
        }
        public bool IsHelpForExpenseInformationEnabled
        {
            get { return m_IsHelpForExpenseInformationEnabled; }
            set { m_IsHelpForExpenseInformationEnabled = value; }
        }
        public bool IsHelpForIncomeInformationEnabled
        {
            get { return m_IsHelpForIncomeInformationEnabled; }
            set { m_IsHelpForIncomeInformationEnabled = value; }
        }
        public bool IsHelpForDeclarationsEnabled
        {
            get { return m_IsHelpForDeclarationsEnabled; }
            set { m_IsHelpForDeclarationsEnabled = value; }
        }
        public bool IsHelpForSelectLoanEnabled
        {
            get { return m_IsHelpForSelectLoanEnabled; }
            set { m_IsHelpForSelectLoanEnabled = value; }
        }
        public bool IsHelpForSubmitApplicationEnabled
        {
            get { return m_IsHelpForSubmitApplicationEnabled; }
            set { m_IsHelpForSubmitApplicationEnabled = value; }
        }
        public bool IsHelpForLoanStatusAndDocExchangeEnabled
        {
            get { return m_IsHelpForLoanStatusAndDocExchangeEnabled; }
            set { m_IsHelpForLoanStatusAndDocExchangeEnabled = value; }
        }
        public bool IsHelpForEmploymentEnabled
        {
            get { return m_IsHelpForEmploymentEnabled; }
            set { m_IsHelpForEmploymentEnabled = value; }
        }
        public bool IsAskForMemberId
        {
            get { return m_IsAskForMemberId; }
            set { m_IsAskForMemberId = value; }
        }
        public string EnabledCustomPmlFieldXmlContent
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(HashSet<int>));
                    ser.Serialize(writer, EnabledCustomPmlFields_FullApp);
                }
                return sb.ToString();
            }
            set
            {
                XmlSerializer ser = new XmlSerializer(typeof(HashSet<int>));
                using (StringReader reader = new StringReader(value))
                {
                    try
                    {
                        m_enabledCustomPmlFields_FullApp = (HashSet<int>)ser.Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        //empty xml in database
                        m_enabledCustomPmlFields_FullApp = null;
                    }
                }
            }
        }
        public string EnabledCustomPmlFieldXmlContent_QuickPricer
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(HashSet<int>));
                    ser.Serialize(writer, EnabledCustomPmlFields_QuickPricer);
                }
                return sb.ToString();
            }
            set
            {
                XmlSerializer ser = new XmlSerializer(typeof(HashSet<int>));
                using (StringReader reader = new StringReader(value))
                {
                    try
                    {
                        m_enabledCustomPmlFields_QuickPricer = (HashSet<int>)ser.Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        //empty xml in database
                        m_enabledCustomPmlFields_QuickPricer = null;
                    }
                }
            }
        }
        /// <summary>
        /// A set of the custom pml field keyword numbers.
        /// </summary>
        public HashSet<int> EnabledCustomPmlFields_FullApp
        {
            get
            {
                if (m_enabledCustomPmlFields_FullApp == null)
                    m_enabledCustomPmlFields_FullApp = new HashSet<int>();
                return m_enabledCustomPmlFields_FullApp;
            }
            set { m_enabledCustomPmlFields_FullApp = value; }
        }

        /// <summary>
        /// A set of the custom pml field keyword numbers.
        /// </summary>
        public HashSet<int> EnabledCustomPmlFields_QuickPricer
        {
            get
            {
                if (m_enabledCustomPmlFields_QuickPricer == null)
                    m_enabledCustomPmlFields_QuickPricer = new HashSet<int>();
                return m_enabledCustomPmlFields_QuickPricer;
            }
            set { m_enabledCustomPmlFields_QuickPricer = value; }
        }

        public string ARMProgramDisclosuresURL
        {
            get { return m_ARMProgramDisclosuresURL; }
            set { m_ARMProgramDisclosuresURL = value; }
        }

        public E_SelectsLoanProgramInFullSubmission SelectsLoanProgramInFullSubmission
        {
            get
            {
                if (!BrokerDB.IsNewPmlUIEnabled || AskConsumerForSubjValue == E_AskConsumerForSubjValue.NoNever)
                {
                    m_SelectsLoanProgramInFullSubmission = E_SelectsLoanProgramInFullSubmission.NoNever;
                }

                return m_SelectsLoanProgramInFullSubmission;
            }
            set { m_SelectsLoanProgramInFullSubmission = value; }
        }

        public Guid DefaultLoanTemplateId
        {
            get
            {
                if (m_defaultLoanTemplateId == Guid.Empty)
                {
                    return BrokerDB.PmlLoanTemplateID;
                }
                return m_defaultLoanTemplateId;
            }
            set { m_defaultLoanTemplateId = value; }
        }

        public string DefaultLoanTemplateNm
        {
            get
            {
                if (DefaultLoanTemplateId == Guid.Empty)
                {
                    return "";
                }

                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(DefaultLoanTemplateId,
                    typeof(ConsumerPortalConfig));
                dataLoan.InitLoad();
                if (!dataLoan.IsTemplate)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Default loan template for portal is not a template.");
                }
                return dataLoan.sLNm;
            }
        }

        public bool SubmissionsShouldBecomeLoans
        {
            get { return m_SubmissionsShouldBecomeLoans; }
            set { m_SubmissionsShouldBecomeLoans = value; }
        }

        public bool IsGenerateLeadOnShortAppSubmission
        {
            get { return this.isGenerateLeadOnShortAppSubmission; }
            set { this.isGenerateLeadOnShortAppSubmission = value; }
        }
        #endregion

        /// <summary>
        /// Creates a brand new ConsumerPortalConfig
        /// </summary>
        public ConsumerPortalConfig(Guid BrokerId, string Name, bool IsEnabled, string ReferralUrl,
            string StyleSheetUrl, bool IsAskForReferralSource,
            List<ReferralSource> ReferralSources, bool IsAllowLockPeriodSelection,
            List<int> AvailableLockPeriods, Guid LeadManagerUserId, Guid LoanOfficerUserId,
            bool IsHideLoanOfficer, string GetRatesPageDisclaimer,
            string OnlineApplicationTerms, string OnlineApplicationCreditCheck,
            bool IsAutomaticPullCredit, Guid AutomaticCRAId, string AutomaticCRALogin,
            string AutomaticCRAPassword, string AutomaticCRAAccountId, string AutomaticCRADULogin,
            string AutomaticCRADUPassword, string EligibleProgramDisclaimer, string NoEligibleProgramMessage,
            bool IsAutomaticRunDU, string AutomaticDULogin, string AutomaticDUPassword,
            E_F1003SubmissionReceiptT F1003SubmissionReceiptT, Guid PreApprovalLetterId,
            Guid PreQualificationLetterdId, string ShortApplicationConfirmation,
            string FullApplicationConfirmation, string HelpLinkPagesHostURL,
            bool IsHelpForGetRatesEnabled, bool IsHelpForApplicantLoginEnabled,
            bool IsHelpForApplicantLoanPipelineEnabled, bool IsHelpForPersonalInformationEnabled,
            bool IsHelpForLoanInformationEnabled, bool IsHelpForPropertyInformationEnabled,
            bool IsHelpForLoanOfficerEnabled, bool IsHelpForAssetInformationEnabled,
            bool IsHelpForLiabilityInformationEnabled, bool IsHelpForExpenseInformationEnabled,
            bool IsHelpForIncomeInformationEnabled, bool IsHelpForDeclarationsEnabled,
            bool IsHelpForSelectLoanEnabled, bool IsHelpForSubmitApplicationEnabled,
            bool IsHelpForLoanStatusAndDocExchangeEnabled, bool IsHelpForEmploymentEnabled,
            string automaticDUCraId, string automaticDUCraLogin, string automaticDUCraPassword, bool isAskForMemberId,
            HashSet<int> enabledCustomPmlFields_FullApp, HashSet<int> enabledCustomPmlFields_QuickPricer, string armProgramDisclosuresURL, E_SelectsLoanProgramInFullSubmission selectsLoanProgramInFullSubmission,
            Guid defaultLoanTemplateId, bool submissionsShouldBecomeLoans, bool isGenerateLeadOnShortAppSubmission,
            E_AskConsumerForSubjPropertyT askConsumerForSubjProperty, E_AskConsumerForSubjValue askConsumerForSubjValue, string loanStatusConfigJson,
            bool isAllowRespaOnly_Purchase, bool isAllowRespaOnly_Refinance, bool isAllowRespaOnly_HomeEquity,
            bool enableShortApplications, bool enableFullApplications, bool allowRegisterNewAccounts, string ContactInfoMessage)
        {
            m_isNew = true;
            m_Id = Guid.NewGuid();

            Initialize(BrokerId, Name, IsEnabled, ReferralUrl,
                StyleSheetUrl, IsAskForReferralSource,
                ReferralSources, IsAllowLockPeriodSelection,
                AvailableLockPeriods, LeadManagerUserId, LoanOfficerUserId,
                IsHideLoanOfficer, GetRatesPageDisclaimer,
                OnlineApplicationTerms, OnlineApplicationCreditCheck,
                IsAutomaticPullCredit, AutomaticCRAId, AutomaticCRALogin,
                AutomaticCRAPassword, AutomaticCRAAccountId, AutomaticCRADULogin,
                AutomaticCRADUPassword, EligibleProgramDisclaimer, NoEligibleProgramMessage,
                IsAutomaticRunDU, AutomaticDULogin, AutomaticDUPassword,
                F1003SubmissionReceiptT, PreApprovalLetterId,
                PreQualificationLetterdId, ShortApplicationConfirmation,
                FullApplicationConfirmation, HelpLinkPagesHostURL,
                IsHelpForGetRatesEnabled, IsHelpForApplicantLoginEnabled,
                IsHelpForApplicantLoanPipelineEnabled, IsHelpForPersonalInformationEnabled,
                IsHelpForLoanInformationEnabled, IsHelpForPropertyInformationEnabled,
                IsHelpForLoanOfficerEnabled, IsHelpForAssetInformationEnabled,
                IsHelpForLiabilityInformationEnabled, IsHelpForExpenseInformationEnabled,
                IsHelpForIncomeInformationEnabled, IsHelpForDeclarationsEnabled,
                IsHelpForSelectLoanEnabled, IsHelpForSubmitApplicationEnabled,
                IsHelpForLoanStatusAndDocExchangeEnabled, IsHelpForEmploymentEnabled,
                automaticDUCraId, automaticDUCraLogin, automaticDUCraPassword, isAskForMemberId,
                enabledCustomPmlFields_FullApp, enabledCustomPmlFields_QuickPricer, armProgramDisclosuresURL, selectsLoanProgramInFullSubmission,
                defaultLoanTemplateId, submissionsShouldBecomeLoans,
                isGenerateLeadOnShortAppSubmission, askConsumerForSubjProperty, askConsumerForSubjValue, loanStatusConfigJson,
                isAllowRespaOnly_Purchase, isAllowRespaOnly_Refinance, isAllowRespaOnly_HomeEquity,
                enableShortApplications, enableFullApplications, allowRegisterNewAccounts, ContactInfoMessage
                );

        }

        /// <summary>
        /// Used for saving without having to reload the object
        /// </summary>
        public ConsumerPortalConfig(Guid Id, Guid BrokerId, string Name, bool IsEnabled, string ReferralUrl,
            string StyleSheetUrl, bool IsAskForReferralSource,
            List<ReferralSource> ReferralSources, bool IsAllowLockPeriodSelection,
            List<int> AvailableLockPeriods, Guid LeadManagerUserId, Guid LoanOfficerUserId,
            bool IsHideLoanOfficer, string GetRatesPageDisclaimer,
            string OnlineApplicationTerms, string OnlineApplicationCreditCheck, bool IsAutomaticPullCredit,
            Guid AutomaticCRAId, string AutomaticCRALogin, string AutomaticCRAPassword,
            string AutomaticCRAAccountId, string AutomaticCRADULogin, string AutomaticCRADUPassword,
            string EligibleProgramDisclaimer, string NoEligibleProgramMessage, bool IsAutomaticRunDU,
            string AutomaticDULogin, string AutomaticDUPassword, E_F1003SubmissionReceiptT F1003SubmissionReceiptT,
            Guid PreApprovalLetterId, Guid PreQualificationLetterdId, string ShortApplicationConfirmation,
            string FullApplicationConfirmation, string HelpLinkPagesHostURL, bool IsHelpForGetRatesEnabled,
            bool IsHelpForApplicantLoginEnabled, bool IsHelpForApplicantLoanPipelineEnabled,
            bool IsHelpForPersonalInformationEnabled, bool IsHelpForLoanInformationEnabled,
            bool IsHelpForPropertyInformationEnabled, bool IsHelpForLoanOfficerEnabled,
            bool IsHelpForAssetInformationEnabled, bool IsHelpForLiabilityInformationEnabled,
            bool IsHelpForExpenseInformationEnabled, bool IsHelpForIncomeInformationEnabled,
            bool IsHelpForDeclarationsEnabled, bool IsHelpForSelectLoanEnabled,
            bool IsHelpForSubmitApplicationEnabled, bool IsHelpForLoanStatusAndDocExchangeEnabled,
            bool IsHelpForEmploymentEnabled, string automaticDUCraId, string automaticDUCraLogin,
            string automaticDUCraPassword, bool isAskForMemberId, HashSet<int> enabledCustomPmlFields_FullApp,
            HashSet<int> enabledCustomPmlFields_QuickPricer, string armProgramDisclosuresURL, E_SelectsLoanProgramInFullSubmission selectsLoanProgramInFullSubmission,
            Guid defaultLoanTemplateId, bool submissionsShouldBecomeLoans, bool isGenerateLeadOnShortAppSubmission,
            E_AskConsumerForSubjPropertyT askConsumerForSubjProperty, E_AskConsumerForSubjValue askConsumerForSubjValue, string loanStatusConfigJson,
            bool isAllowRespaOnly_Purchase, bool isAllowRespaOnly_Refinance, bool isAllowRespaOnly_HomeEquity,
            bool enableShortApplications, bool enableFullApplications, bool allowRegisterNewAccounts, string ContactInfoMessage)
        {
            m_isNew = false;
            m_Id = Id;
            Initialize(BrokerId, Name, IsEnabled, ReferralUrl,
                StyleSheetUrl, IsAskForReferralSource,
                ReferralSources, IsAllowLockPeriodSelection,
                AvailableLockPeriods, LeadManagerUserId, LoanOfficerUserId,
                IsHideLoanOfficer, GetRatesPageDisclaimer,
                OnlineApplicationTerms, OnlineApplicationCreditCheck,
                IsAutomaticPullCredit, AutomaticCRAId, AutomaticCRALogin,
                AutomaticCRAPassword, AutomaticCRAAccountId, AutomaticCRADULogin,
                AutomaticCRADUPassword, EligibleProgramDisclaimer, NoEligibleProgramMessage,
                IsAutomaticRunDU, AutomaticDULogin, AutomaticDUPassword,
                F1003SubmissionReceiptT, PreApprovalLetterId,
                PreQualificationLetterdId, ShortApplicationConfirmation,
                FullApplicationConfirmation, HelpLinkPagesHostURL,
                IsHelpForGetRatesEnabled, IsHelpForApplicantLoginEnabled,
                IsHelpForApplicantLoanPipelineEnabled, IsHelpForPersonalInformationEnabled,
                IsHelpForLoanInformationEnabled, IsHelpForPropertyInformationEnabled,
                IsHelpForLoanOfficerEnabled, IsHelpForAssetInformationEnabled,
                IsHelpForLiabilityInformationEnabled, IsHelpForExpenseInformationEnabled,
                IsHelpForIncomeInformationEnabled, IsHelpForDeclarationsEnabled,
                IsHelpForSelectLoanEnabled, IsHelpForSubmitApplicationEnabled,
                IsHelpForLoanStatusAndDocExchangeEnabled, IsHelpForEmploymentEnabled,
                automaticDUCraId, automaticDUCraLogin, automaticDUCraPassword, isAskForMemberId,
                enabledCustomPmlFields_FullApp, enabledCustomPmlFields_QuickPricer, armProgramDisclosuresURL, selectsLoanProgramInFullSubmission,
                defaultLoanTemplateId, submissionsShouldBecomeLoans, isGenerateLeadOnShortAppSubmission,
                askConsumerForSubjProperty, askConsumerForSubjValue, loanStatusConfigJson,
                isAllowRespaOnly_Purchase, isAllowRespaOnly_Refinance, isAllowRespaOnly_HomeEquity,
                enableShortApplications, enableFullApplications, allowRegisterNewAccounts, ContactInfoMessage);
        }

        private void Initialize(Guid BrokerId, string Name, bool IsEnabled, string ReferralUrl,
            string StyleSheetUrl, bool IsAskForReferralSource,
            List<ReferralSource> ReferralSources, bool IsAllowLockPeriodSelection,
            List<int> AvailableLockPeriods, Guid LeadManagerUserId, Guid LoanOfficerUserId,
            bool IsHideLoanOfficer, string GetRatesPageDisclaimer,
            string OnlineApplicationTerms, string OnlineApplicationCreditCheck,
            bool IsAutomaticPullCredit, Guid AutomaticCRAId, string AutomaticCRALogin,
            string AutomaticCRAPassword, string AutomaticCRAAccountId, string AutomaticCRADULogin,
            string AutomaticCRADUPassword, string EligibleProgramDisclaimer, string NoEligibleProgramMessage,
            bool IsAutomaticRunDU, string AutomaticDULogin, string AutomaticDUPassword,
            E_F1003SubmissionReceiptT F1003SubmissionReceiptT, Guid PreApprovalLetterId,
            Guid PreQualificationLetterdId, string ShortApplicationConfirmation,
            string FullApplicationConfirmation, string HelpLinkPagesHostURL,
            bool IsHelpForGetRatesEnabled, bool IsHelpForApplicantLoginEnabled,
            bool IsHelpForApplicantLoanPipelineEnabled, bool IsHelpForPersonalInformationEnabled,
            bool IsHelpForLoanInformationEnabled, bool IsHelpForPropertyInformationEnabled,
            bool IsHelpForLoanOfficerEnabled, bool IsHelpForAssetInformationEnabled,
            bool IsHelpForLiabilityInformationEnabled, bool IsHelpForExpenseInformationEnabled,
            bool IsHelpForIncomeInformationEnabled, bool IsHelpForDeclarationsEnabled,
            bool IsHelpForSelectLoanEnabled, bool IsHelpForSubmitApplicationEnabled,
            bool IsHelpForLoanStatusAndDocExchangeEnabled, bool IsHelpForEmploymentEnabled,
            string automaticDUCraId, string automaticDUCraLogin, string automaticDUCraPassword,
            bool isAskForMemberId, HashSet<int> enabledCustomPmlFields_FullApp, HashSet<int> enabledCustomPmlFields_QuickPricer, string armProgramDisclosuresURL,
            E_SelectsLoanProgramInFullSubmission selectsLoanProgramInFullSubmission, Guid defaultLoanTemplateId, bool submissionsShouldBecomeLoans,
            bool isGenerateLeadOnShortAppSubmission, E_AskConsumerForSubjPropertyT askConsumerForSubjProperty, E_AskConsumerForSubjValue askConsumerForSubjValue, string loanStatusConfigJson,
            bool isAllowRespaOnly_Purchase, bool isAllowRespaOnly_Refinance, bool isAllowRespaOnly_HomeEquity,
            bool enableShortApplications, bool enableFullApplications, bool allowRegisterNewAccounts, string ContactInfoMessage)
        {
            m_BrokerId = BrokerId;
            m_Name = Name;
            m_IsEnabled = IsEnabled;
            m_ReferralUrl = ReferralUrl;
            m_StyleSheetUrl = StyleSheetUrl;
            m_IsAskForReferralSource = IsAskForReferralSource;
            m_DisplayedReferralSources = ReferralSources;
            m_IsAllowLockPeriodSelection = IsAllowLockPeriodSelection;
            m_AvailableLockPeriods = AvailableLockPeriods;
            m_LeadManagerUserId = LeadManagerUserId;
            m_LoanOfficerUserId = LoanOfficerUserId;
            m_IsHideLoanOfficer = IsHideLoanOfficer;
            m_GetRatesPageDisclaimer = GetRatesPageDisclaimer;
            m_OnlineApplicationTerms = OnlineApplicationTerms;
            m_OnlineApplicationCreditCheck = OnlineApplicationCreditCheck;
            m_IsAutomaticPullCredit = IsAutomaticPullCredit;
            m_AutomaticCRAId = AutomaticCRAId;
            m_AutomaticCRALogin = AutomaticCRALogin;
            m_AutomaticCRAPassword = AutomaticCRAPassword;
            m_AutomaticCRAAccountId = AutomaticCRAAccountId;
            m_AutomaticCRADULogin = AutomaticCRADULogin;
            m_AutomaticCRADUPassword = AutomaticCRADUPassword;
            m_EligibleProgramDisclaimer = EligibleProgramDisclaimer;
            m_NoEligibleProgramMessage = NoEligibleProgramMessage;
            m_IsAutomaticRunDU = IsAutomaticRunDU;
            m_AutomaticDULogin = AutomaticDULogin;
            m_AutomaticDUPassword = AutomaticDUPassword;
            m_F1003SubmissionReceiptT = F1003SubmissionReceiptT;
            m_PreApprovalLetterId = PreApprovalLetterId;
            m_PreQualificationLetterdId = PreQualificationLetterdId;
            m_ShortApplicationConfirmation = ShortApplicationConfirmation;
            m_FullApplicationConfirmation = FullApplicationConfirmation;
            m_ContactInfoMessage = ContactInfoMessage;
            m_HelpLinkPagesHostURL = HelpLinkPagesHostURL;
            m_IsHelpForGetRatesEnabled = IsHelpForGetRatesEnabled;
            m_IsHelpForApplicantLoginEnabled = IsHelpForApplicantLoginEnabled;
            m_IsHelpForApplicantLoanPipelineEnabled = IsHelpForApplicantLoanPipelineEnabled;
            m_IsHelpForPersonalInformationEnabled = IsHelpForPersonalInformationEnabled;
            m_IsHelpForLoanInformationEnabled = IsHelpForLoanInformationEnabled;
            m_IsHelpForPropertyInformationEnabled = IsHelpForPropertyInformationEnabled;
            m_IsHelpForLoanOfficerEnabled = IsHelpForLoanOfficerEnabled;
            m_IsHelpForAssetInformationEnabled = IsHelpForAssetInformationEnabled;
            m_IsHelpForLiabilityInformationEnabled = IsHelpForLiabilityInformationEnabled;
            m_IsHelpForExpenseInformationEnabled = IsHelpForExpenseInformationEnabled;
            m_IsHelpForIncomeInformationEnabled = IsHelpForIncomeInformationEnabled;
            m_IsHelpForDeclarationsEnabled = IsHelpForDeclarationsEnabled;
            m_IsHelpForSelectLoanEnabled = IsHelpForSelectLoanEnabled;
            m_IsHelpForSubmitApplicationEnabled = IsHelpForSubmitApplicationEnabled;
            m_IsHelpForLoanStatusAndDocExchangeEnabled = IsHelpForLoanStatusAndDocExchangeEnabled;
            m_IsHelpForEmploymentEnabled = IsHelpForEmploymentEnabled;
            m_IsAskForMemberId = isAskForMemberId;
            this.AutomaticDUCraId = automaticDUCraId;
            this.AutomaticDUCraLogin = automaticDUCraLogin;
            this.AutomaticDUCraPassword = automaticDUCraPassword;
            m_enabledCustomPmlFields_FullApp = enabledCustomPmlFields_FullApp;
            m_enabledCustomPmlFields_QuickPricer = enabledCustomPmlFields_QuickPricer;
            m_ARMProgramDisclosuresURL = armProgramDisclosuresURL;
            m_SelectsLoanProgramInFullSubmission = selectsLoanProgramInFullSubmission;
            m_defaultLoanTemplateId = defaultLoanTemplateId;
            m_SubmissionsShouldBecomeLoans = submissionsShouldBecomeLoans;
            this.isGenerateLeadOnShortAppSubmission = isGenerateLeadOnShortAppSubmission;
            this.askConsumerForSubjProperty = askConsumerForSubjProperty;
            this.askConsumerForSubjValue = askConsumerForSubjValue;
            this.LoanStatusConfigJson = loanStatusConfigJson;
            this.IsAllowRespaOnly_Purchase = isAllowRespaOnly_Purchase;
            this.IsAllowRespaOnly_Refinance = isAllowRespaOnly_Refinance;
            this.IsAllowRespaOnly_HomeEquity = isAllowRespaOnly_HomeEquity;
            this.EnableShortApplications = enableShortApplications;
            this.EnableFullApplications = enableFullApplications;
            this.AllowRegisterNewAccounts = allowRegisterNewAccounts;
        }
        /// <summary>
        /// Use ConsumerPortalConfig.Retrieve()
        /// </summary>
        private ConsumerPortalConfig(DbDataReader row)
        {
            m_isNew = false;
            m_Id = (Guid)row["Id"];
            m_BrokerId = (Guid)row["BrokerId"];
            m_Name = (string)row["Name"];
            m_IsEnabled = (bool)row["IsEnabled"];
            m_ReferralUrl = (string)row["ReferralUrl"];
            m_StyleSheetUrl = (string)row["StyleSheetUrl"];
            m_IsAskForReferralSource = (bool)row["IsAskForReferralSource"];
            DisplayedReferralSourcesXmlContent = (string)row["ReferralSourceListXmlContent"].ToString();
            m_IsAllowLockPeriodSelection = (bool)row["IsAllowLockPeriodSelection"];
            AvailableLockPeriodsXmlContent = row["LockPeriodXmlContent"].ToString();
            m_LeadManagerUserId = (Guid)row["LeadManagerUserId"];
            m_LoanOfficerUserId = (Guid)row["LoanOfficerUserId"];
            m_IsHideLoanOfficer = (bool)row["IsHideLoanOfficer"];
            m_GetRatesPageDisclaimer = (string)row["GetRatesPageDisclaimer"];
            m_OnlineApplicationTerms = (string)row["OnlineApplicationTerms"];
            m_OnlineApplicationCreditCheck = (string)row["OnlineApplicationCreditCheck"];
            m_IsAutomaticPullCredit = (bool)row["IsAutomaticPullCredit"];
            m_AutomaticCRAId = row["AutomaticCRAId"] == DBNull.Value ? Guid.Empty : (Guid)row["AutomaticCRAId"];
            m_AutomaticCRALogin = (string)row["AutomaticCRALogin"];
            m_AutomaticCRAPassword = (string)row["AutomaticCRAPassword"];
            m_AutomaticCRAAccountId = (string)row["AutomaticCRAAccountId"];
            m_AutomaticCRADULogin = (string)row["AutomaticCRADULogin"];
            m_AutomaticCRADUPassword = (string)row["AutomaticCRADUPassword"];
            m_EligibleProgramDisclaimer = (string)row["EligibleProgramDisclaimer"];
            m_NoEligibleProgramMessage = (string)row["NoEligibleProgramMessage"];
            m_IsAutomaticRunDU = (bool)row["IsAutomaticRunDU"];
            m_AutomaticDULogin = (string)row["AutomaticDULogin"];
            m_AutomaticDUPassword = (string)row["AutomaticDUPassword"];
            m_F1003SubmissionReceiptT = (E_F1003SubmissionReceiptT)row["F1003SubmissionReceiptT"];
            m_PreApprovalLetterId = row["PreApprovalLetterId"] == DBNull.Value ? Guid.Empty : (Guid)row["PreApprovalLetterId"];
            m_PreQualificationLetterdId = row["PreQualificationLetterdId"] == DBNull.Value ? Guid.Empty : (Guid)row["PreQualificationLetterdId"];
            m_ShortApplicationConfirmation = (string)row["ShortApplicationConfirmation"];
            m_FullApplicationConfirmation = (string)row["FullApplicationConfirmation"];
            m_ContactInfoMessage = (string)row["ContactInfoMessage"];
            m_HelpLinkPagesHostURL = (string)row["HelpLinkPagesHostURL"];
            m_IsHelpForGetRatesEnabled = (bool)row["IsHelpForGetRatesEnabledEnabled"];
            m_IsHelpForApplicantLoginEnabled = (bool)row["IsHelpForApplicantLoginEnabled"];
            m_IsHelpForApplicantLoanPipelineEnabled = (bool)row["IsHelpForApplicantLoanPipelineEnabled"];
            m_IsHelpForPersonalInformationEnabled = (bool)row["IsHelpForPersonalInformationEnabled"];
            m_IsHelpForLoanInformationEnabled = (bool)row["IsHelpForLoanInformationEnabled"];
            m_IsHelpForPropertyInformationEnabled = (bool)row["IsHelpForPropertyInformationEnabled"];
            m_IsHelpForLoanOfficerEnabled = (bool)row["IsHelpForLoanOfficerEnabled"];
            m_IsHelpForAssetInformationEnabled = (bool)row["IsHelpForAssetInformationEnabled"];
            m_IsHelpForLiabilityInformationEnabled = (bool)row["IsHelpForLiabilityInformationEnabled"];
            m_IsHelpForExpenseInformationEnabled = (bool)row["IsHelpForExpenseInformationEnabled"];
            m_IsHelpForIncomeInformationEnabled = (bool)row["IsHelpForIncomeInformationEnabled"];
            m_IsHelpForDeclarationsEnabled = (bool)row["IsHelpForDeclarationsEnabled"];
            m_IsHelpForSelectLoanEnabled = (bool)row["IsHelpForSelectLoanEnabled"];
            m_IsHelpForSubmitApplicationEnabled = (bool)row["IsHelpForSubmitApplicationEnabled"];
            m_IsHelpForLoanStatusAndDocExchangeEnabled = (bool)row["IsHelpForLoanStatusAndDocExchange"];
            m_IsHelpForEmploymentEnabled = (bool)row["IsHelpForEmploymentEnabled"];
            m_IsAskForMemberId = (bool)row["IsAskForMemberId"];
            this.AutomaticDUCraId = (string)row["AutomaticDUCraId"];
            this.AutomaticDUCraLogin = (string)row["AutomaticDUCraLogin"];
            this.AutomaticDUCraPassword = (string)row["AutomaticDUCraPassword"];
            EnabledCustomPmlFieldXmlContent = (string)row["EnabledCustomPmlFieldXmlContent"];
            EnabledCustomPmlFieldXmlContent_QuickPricer = (string)row["EnabledCustomPmlFieldXmlContent_QuickPricer"];
            m_ARMProgramDisclosuresURL = (string)row["ARMProgramDisclosuresURL"];
            m_SelectsLoanProgramInFullSubmission = (E_SelectsLoanProgramInFullSubmission)row["SelectsLoanProgramInFullSubmission"];
            m_defaultLoanTemplateId = row["DefaultLoanTemplateId"] == DBNull.Value ? Guid.Empty : (Guid)row["DefaultLoanTemplateId"];
            m_SubmissionsShouldBecomeLoans = (bool)row["SubmissionsShouldBecomeLoans"];
            this.isGenerateLeadOnShortAppSubmission = (bool)row["IsGenerateLeadOnShortAppSubmission"];
            this.AskConsumerForSubjProperty = (E_AskConsumerForSubjPropertyT)row["AskConsumerForSubjProperty"];
            this.AskConsumerForSubjValue = (E_AskConsumerForSubjValue)row["AskConsumerForSubjValue"];
            this.LoanStatusConfigJson = (string)row["LoanStatusConfigJson"];
            this.IsAllowRespaOnly_Purchase = row["IsAllowRespaOnly_Purchase"] == DBNull.Value ? false : (bool)row["IsAllowRespaOnly_Purchase"];
            this.IsAllowRespaOnly_Refinance = row["IsAllowRespaOnly_Refinance"] == DBNull.Value ? false : (bool)row["IsAllowRespaOnly_Refinance"];
            this.IsAllowRespaOnly_HomeEquity = row["IsAllowRespaOnly_HomeEquity"] == DBNull.Value ? false : (bool)row["IsAllowRespaOnly_HomeEquity"];
            this.EnableShortApplications = (bool)row["EnableShortApplications"];
            this.EnableFullApplications = (bool)row["EnableFullApplications"];
            this.AllowRegisterNewAccounts = (bool)row["AllowRegisterNewAccounts"];
        }


        public void Save()
        {
            // If Automatic CRA ID is not empty, check that it is valid before saving.
            if (m_AutomaticCRAId != Guid.Empty)
            {
                ValidateAutomaticCraId();
            }

            if (m_isNew)
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "ConsumerPortal_Add", 3, GetParameters());
            else
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "ConsumerPortal_Update", 3, GetParameters());
            m_isNew = false;
        }

        private void ValidateAutomaticCraId()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@ComId", m_AutomaticCRAId),
                                            new SqlParameter("@returnInvalid", true)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveServiceCompanyByID", parameters))
            {
                if (!reader.Read())
                {
                    throw new CBaseException(ErrorMessages.Generic, $"Automatic CRA ID is invalid. AutomaticCRAId: {m_AutomaticCRAId}.");
                }
            }
        }

        public SqlParameter[] GetParameters()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("Id", m_Id));
            parameters.Add(new SqlParameter("BrokerId", m_BrokerId));
            parameters.Add(new SqlParameter("Name", m_Name));
            parameters.Add(new SqlParameter("IsEnabled", m_IsEnabled));
            parameters.Add(new SqlParameter("ReferralUrl", m_ReferralUrl));
            parameters.Add(new SqlParameter("StyleSheetUrl", m_StyleSheetUrl));
            parameters.Add(new SqlParameter("IsAskForReferralSource", m_IsAskForReferralSource));
            parameters.Add(new SqlParameter("ReferralSourceListXmlContent", DisplayedReferralSourcesXmlContent));
            parameters.Add(new SqlParameter("IsAllowLockPeriodSelection", m_IsAllowLockPeriodSelection));
            parameters.Add(new SqlParameter("LockPeriodXmlContent", AvailableLockPeriodsXmlContent));
            if (m_isNew || m_LeadManagerUserId != Guid.Empty) parameters.Add(new SqlParameter("LeadManagerUserId", m_LeadManagerUserId));
            if (m_isNew || m_LoanOfficerUserId != Guid.Empty) parameters.Add(new SqlParameter("LoanOfficerUserId", m_LoanOfficerUserId));
            parameters.Add(new SqlParameter("IsHideLoanOfficer", m_IsHideLoanOfficer));
            parameters.Add(new SqlParameter("GetRatesPageDisclaimer", m_GetRatesPageDisclaimer));
            parameters.Add(new SqlParameter("OnlineApplicationTerms", m_OnlineApplicationTerms));
            parameters.Add(new SqlParameter("OnlineApplicationCreditCheck", m_OnlineApplicationCreditCheck));
            parameters.Add(new SqlParameter("IsAutomaticPullCredit", m_IsAutomaticPullCredit));
            if (m_AutomaticCRAId != Guid.Empty) parameters.Add(new SqlParameter("AutomaticCRAId", m_AutomaticCRAId));
            parameters.Add(new SqlParameter("AutomaticCRALogin", m_AutomaticCRALogin));
            if (m_isNew || !string.IsNullOrEmpty(m_AutomaticCRAPassword)) parameters.Add(new SqlParameter("AutomaticCRAPassword", m_AutomaticCRAPassword));
            parameters.Add(new SqlParameter("AutomaticCRAAccountId", m_AutomaticCRAAccountId));
            parameters.Add(new SqlParameter("AutomaticCRADULogin", m_AutomaticCRADULogin));
            if (m_isNew || !string.IsNullOrEmpty(m_AutomaticCRADUPassword)) parameters.Add(new SqlParameter("AutomaticCRADUPassword", m_AutomaticCRADUPassword));
            parameters.Add(new SqlParameter("EligibleProgramDisclaimer", m_EligibleProgramDisclaimer));
            parameters.Add(new SqlParameter("NoEligibleProgramMessage", m_NoEligibleProgramMessage));
            parameters.Add(new SqlParameter("IsAutomaticRunDU", m_IsAutomaticRunDU));
            parameters.Add(new SqlParameter("AutomaticDULogin", m_AutomaticDULogin));
            if (m_isNew || !string.IsNullOrEmpty(m_AutomaticDUPassword)) parameters.Add(new SqlParameter("AutomaticDUPassword", m_AutomaticDUPassword));
            parameters.Add(new SqlParameter("F1003SubmissionReceiptT", m_F1003SubmissionReceiptT));
            if (m_PreApprovalLetterId != Guid.Empty) parameters.Add(new SqlParameter("PreApprovalLetterId", m_PreApprovalLetterId));
            if (m_PreQualificationLetterdId != Guid.Empty) parameters.Add(new SqlParameter("PreQualificationLetterdId", m_PreQualificationLetterdId));
            parameters.Add(new SqlParameter("ShortApplicationConfirmation", m_ShortApplicationConfirmation));
            parameters.Add(new SqlParameter("FullApplicationConfirmation", m_FullApplicationConfirmation));
            parameters.Add(new SqlParameter("ContactInfoMessage", m_ContactInfoMessage));
            parameters.Add(new SqlParameter("HelpLinkPagesHostURL", m_HelpLinkPagesHostURL));
            parameters.Add(new SqlParameter("IsHelpForGetRatesEnabledEnabled", m_IsHelpForGetRatesEnabled));
            parameters.Add(new SqlParameter("IsHelpForApplicantLoginEnabled", m_IsHelpForApplicantLoginEnabled));
            parameters.Add(new SqlParameter("IsHelpForApplicantLoanPipelineEnabled", m_IsHelpForApplicantLoanPipelineEnabled));
            parameters.Add(new SqlParameter("IsHelpForPersonalInformationEnabled", m_IsHelpForPersonalInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForLoanInformationEnabled", m_IsHelpForLoanInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForPropertyInformationEnabled", m_IsHelpForPropertyInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForLoanOfficerEnabled", m_IsHelpForLoanOfficerEnabled));
            parameters.Add(new SqlParameter("IsHelpForAssetInformationEnabled", m_IsHelpForAssetInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForLiabilityInformationEnabled", m_IsHelpForLiabilityInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForExpenseInformationEnabled", m_IsHelpForExpenseInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForIncomeInformationEnabled", m_IsHelpForIncomeInformationEnabled));
            parameters.Add(new SqlParameter("IsHelpForDeclarationsEnabled", m_IsHelpForDeclarationsEnabled));
            parameters.Add(new SqlParameter("IsHelpForSelectLoanEnabled", m_IsHelpForSelectLoanEnabled));
            parameters.Add(new SqlParameter("IsHelpForSubmitApplicationEnabled", m_IsHelpForSubmitApplicationEnabled));
            parameters.Add(new SqlParameter("IsHelpForLoanStatusAndDocExchange", m_IsHelpForLoanStatusAndDocExchangeEnabled));
            parameters.Add(new SqlParameter("IsHelpForEmploymentEnabled", m_IsHelpForEmploymentEnabled));
            parameters.Add(new SqlParameter("IsAskForMemberId", m_IsAskForMemberId));
            parameters.Add(new SqlParameter("@AutomaticDUCraId", this.AutomaticDUCraId));
            parameters.Add(new SqlParameter("@AutomaticDUCraLogin", this.AutomaticDUCraLogin));
            if (m_isNew || !string.IsNullOrEmpty(AutomaticDUCraPassword))
            {
                parameters.Add(new SqlParameter("@AutomaticDUCraPassword", this.AutomaticDUCraPassword));
            }
            parameters.Add(new SqlParameter("@EnabledCustomPmlFieldXmlContent", EnabledCustomPmlFieldXmlContent));
            parameters.Add(new SqlParameter("@EnabledCustomPmlFieldXmlContent_QuickPricer", EnabledCustomPmlFieldXmlContent_QuickPricer));
            parameters.Add(new SqlParameter("@ARMProgramDisclosuresURL", m_ARMProgramDisclosuresURL));
            parameters.Add(new SqlParameter("@SelectsLoanProgramInFullSubmission", m_SelectsLoanProgramInFullSubmission));
            if (m_defaultLoanTemplateId != Guid.Empty) parameters.Add(new SqlParameter("@DefaultLoanTemplateId", m_defaultLoanTemplateId));
            parameters.Add(new SqlParameter("@SubmissionsShouldBecomeLoans", m_SubmissionsShouldBecomeLoans));
            parameters.Add(new SqlParameter("@IsGenerateLeadOnShortAppSubmission", this.isGenerateLeadOnShortAppSubmission));
            parameters.Add(new SqlParameter("@AskConsumerForSubjProperty", this.askConsumerForSubjProperty));
            parameters.Add(new SqlParameter("@AskConsumerForSubjValue", this.askConsumerForSubjValue));
            parameters.Add(new SqlParameter("@LoanStatusConfigJson", this.LoanStatusConfigJson));

            parameters.Add(new SqlParameter("@IsAllowRespaOnly_Purchase", this.IsAllowRespaOnly_Purchase));
            parameters.Add(new SqlParameter("@IsAllowRespaOnly_Refinance", this.IsAllowRespaOnly_Refinance));
            parameters.Add(new SqlParameter("@IsAllowRespaOnly_HomeEquity", this.IsAllowRespaOnly_HomeEquity));

            parameters.Add(new SqlParameter("@EnableShortApplications", this.EnableShortApplications));
            parameters.Add(new SqlParameter("@EnableFullApplications", this.EnableFullApplications));
            parameters.Add(new SqlParameter("@AllowRegisterNewAccounts", this.AllowRegisterNewAccounts));

            return parameters.ToArray();
        }

        public static ConsumerPortalConfig RetrieveWithoutBrokerIdSlowSlow(Guid id)
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("Id", id)
                                        };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ConsumerPortal_Retrieve", parameters))
                {
                    if (reader.Read())
                    {
                        return new ConsumerPortalConfig(reader);
                    }
                }
            }

            throw new NotFoundException("Consumer Portal Not Found", "ConsumerPortalConfig.Retrieve(Id) failed with Id=" + id);

        }

        public static ConsumerPortalConfig Retrieve(Guid brokerId, Guid Id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("Id", Id)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ConsumerPortal_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    return new ConsumerPortalConfig(reader);
                }
            }

            throw new NotFoundException("Consumer Portal Not Found", "ConsumerPortalConfig.Retrieve(Id) failed with Id=" + Id);
        }

        public static List<ConsumerPortalIdData> RetrievePortalsForBroker(Guid BrokerId)
        {
            List<ConsumerPortalIdData> data = new List<ConsumerPortalIdData>();

            SqlParameter[] parameters = {
                                            new SqlParameter("BrokerId", BrokerId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "ConsumerPortal_RetrieveListForBroker", parameters))
            {
                while (reader.Read())
                {
                    data.Add(new ConsumerPortalIdData((Guid)reader["Id"], (string)reader["Name"]));
                }
            }

            return data;
        }
    }
    public class ConsumerPortalIdData
    {
        private static string RATE_WATCH_PAGE = "RateWatchWidget.aspx";
        private static string GET_RATES_PAGE = "GetRate.aspx";
        private static string BORROWER_LOGIN_PAGE = "Login.aspx";
        private Guid m_Id;
        private string m_Name;

        public Guid Id { get { return m_Id; } }
        public string Name { get { return m_Name; } }

        public string RateWatchUrl
        {
            get
            {
                return
                    string.Format("{0}/{1}?siteid={2}",
                    ConstStage.ConsumerPortalVersion2BaseUrl,
                    RATE_WATCH_PAGE, Id.ToString());
            }
        }

        public string GetRatesUrl
        {
            get
            {
                return
                    string.Format("{0}/{1}?siteid={2}",
                    ConstStage.ConsumerPortalVersion2BaseUrl,
                    GET_RATES_PAGE, Id.ToString());
            }
        }
        public string BorrowerLoginUrl
        {
            get
            {
                return
                string.Format("{0}/{1}?siteid={2}",
                   ConstStage.ConsumerPortalVersion2BaseUrl,
                   BORROWER_LOGIN_PAGE, Id.ToString());
            }
        }
        internal ConsumerPortalIdData(Guid Id, string Name)
        {
            m_Id = Id;
            m_Name = Name;
        }

        public static ConsumerPortalIdData GetUrlDataFor(Guid portalId)
        {
            return new ConsumerPortalIdData(portalId, "");
        }
    }
    public enum E_F1003SubmissionReceiptT
    {
        PreApprovalLetter = 0,
        PreQualificationLetter = 1,
        ConfirmationOnly = 2
    }
    public class ReferralSource
    {
        public string Name { get; set; }
        public int? Id { get; set; }
    }
    public class DateConfig
    {
        public string FieldId { get; set; }
        public string SystemDescription { get; set; }
        public string CustomDescription { get; set; }

        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(this.CustomDescription))
                {
                    return this.SystemDescription;
                }

                return this.CustomDescription;
            }
        }

        public DateConfig()
        {
        }
    }
}
