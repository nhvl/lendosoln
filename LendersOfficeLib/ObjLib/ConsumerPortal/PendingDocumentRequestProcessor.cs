﻿// <copyright file="PendingDocumentRequestProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/5/2014 4:37:12 PM 
// </summary>
namespace LendersOffice.ObjLib.ConsumerPortal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Processes scheduled document requests.
    /// </summary>
    public class PendingDocumentRequestProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description of this process.
        /// </summary>
        /// <value>
        /// The description of this runnable.
        /// </value>
        public string Description
        {
            get { return "Processes document requests which have not been sent to the borrower."; }
        }

        /// <summary>
        /// Retrieve and send the document requests that are scheduled to be sent.
        /// </summary>
        public void Run()
        {
            IEnumerable<IGrouping<Guid, PendingDocumentRequest>> requestsByLoanId;
            try
            {
                requestsByLoanId = PendingDocumentRequestHandler.GetScheduledRequests()
                    .GroupBy(r => r.LoanId);
            }
            catch (Exception exc)
            {
                var errorMessage = "[PendingDocumentRequestProcessor]Unexpected error when retrieving pending " +
                    "document requests in continuous processor.";
                Tools.LogErrorWithCriticalTracking(errorMessage, exc);
                throw;
            }

            foreach (var requestGroup in requestsByLoanId)
            {
                try
                {
                    PendingDocumentRequestHandler.SendRequestsForLoanId(requestGroup.Key, requestGroup.ToArray());
                }
                catch (Exception exc)
                {
                    var errorMessage = "[PendingDocumentRequestProcessor]Unexpected error when sending pending " +
                        "document requests for loan " + requestGroup.Key;
                    Tools.LogErrorWithCriticalTracking(errorMessage, exc);
                    throw;
                }
            }
        }
    }
}
