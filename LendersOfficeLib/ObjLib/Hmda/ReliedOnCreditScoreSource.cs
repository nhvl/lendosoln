﻿namespace LendersOffice.ObjLib.Hmda
{
    /// <summary>
    /// Credit Score source for the Credit Score reported to HMDA relied on values.
    /// </summary>
    public enum ReliedOnCreditScoreSource
    {
        /// <summary>
        /// No credit source.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Credit Score source: Type 1.
        /// </summary>
        Type1 = 1,

        /// <summary>
        /// Credit Score source: Type 2.
        /// </summary>
        Type2 = 2,

        /// <summary>
        /// Credit Score source: Type 2 Soft.
        /// </summary>
        Type2Soft = 3,

        /// <summary>
        /// Credit Score source: Type 3.
        /// </summary>
        Type3 = 4,        

        /// <summary>
        /// Credit Score source: Other.
        /// </summary>
        Other = 5
    }
}
