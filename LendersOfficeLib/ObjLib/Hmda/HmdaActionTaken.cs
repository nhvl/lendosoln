﻿namespace LendersOffice.ObjLib.Hmda
{
    /// <summary>
    /// Possible HMDA actions.
    /// </summary>
    public enum HmdaActionTaken
    {
        /// <summary>
        /// Blank (padding).
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Loan Originated.
        /// </summary>
        LoanOriginated = 1,

        /// <summary>
        /// Application approved but not accepted.
        /// </summary>
        ApplicationApprovedButNotAccepted = 2,

        /// <summary>
        /// Application denied.
        /// </summary>
        ApplicationDenied = 3,

        /// <summary>
        /// Application withdrawn by applicant.
        /// </summary>
        ApplicationWithdrawnByApplicant = 4,

        /// <summary>
        /// File closed for incompleteness.
        /// </summary>
        FileClosedForIncompleteness = 5,

        /// <summary>
        /// Purchased loan.
        /// </summary>
        PurchasedLoan = 6,

        /// <summary>
        /// Preapproval request denied.
        /// </summary>
        PreapprovalRequestDenied = 7,

        /// <summary>
        /// Preapproval request approved but not accepted.
        /// </summary>
        PreapprovalRequestApprovedButNotAccepted = 8
    }
}
