﻿namespace LendersOffice.ObjLib.Hmda
{
    /// <summary>
    /// Enum that defines credit score mode for relied on hmda values.
    /// </summary>
    public enum ReliedOnCreditScoreMode
    {
        /// <summary>
        /// No Credit Score Mode selected.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Applicant's individual score.
        /// </summary>
        ApplicantIndividualScore = 1,

        /// <summary>
        /// Single Score.
        /// </summary>
        SingleScore = 2,
    }
}
