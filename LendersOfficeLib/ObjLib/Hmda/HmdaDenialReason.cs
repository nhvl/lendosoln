﻿namespace LendersOffice.ObjLib.Hmda
{
    /// <summary>
    /// HDMA denail reasons.
    /// </summary>
    public enum HmdaDenialReason
    {
        /// <summary>
        /// Blank (padding).
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Debt-to-income ratio.
        /// </summary>
        DebtToIncomeRatio = 1,

        /// <summary>
        /// Employment history.
        /// </summary>
        EmploymentHistory = 2,

        /// <summary>
        /// Credit history.
        /// </summary>
        CreditHistory = 3,

        /// <summary>
        /// Collateral (padding).
        /// </summary>
        Collateral = 4,

        /// <summary>
        /// Insufficient cash (downpayment, closing costs).
        /// </summary>
        InsufficientCash = 5,

        /// <summary>
        /// Unverifiable information.
        /// </summary>
        UnverifiableInformation = 6,

        /// <summary>
        /// Unverifiable information.
        /// </summary>
        CreditApplicationIncomplete = 7,

        /// <summary>
        /// Credit application incomplete.
        /// </summary>
        MortgageInsuranceDenied = 8,

        /// <summary>
        /// Other (padding).
        /// </summary>
        Other = 9,

        /// <summary>
        /// Not applicable.
        /// </summary>
        NotApplicable = 10
    }
}
