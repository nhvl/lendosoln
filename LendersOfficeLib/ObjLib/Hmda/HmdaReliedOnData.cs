﻿namespace LendersOffice.ObjLib.Hmda
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Data class for HMDA relied on.
    /// </summary>
    public static class HmdaReliedOnData
    {
        /// <summary>
        /// Dictionary that maps relied on credit score source enum to guid string.
        /// </summary>
        /// <remarks>This should contain the same mapping as GuidToReliedOnCreditScoreSourceMap.</remarks>
        private static Dictionary<ReliedOnCreditScoreSource, string> reliedOnCreditScoreSourceToGuidMap = new Dictionary<ReliedOnCreditScoreSource, string>()
        {
            { ReliedOnCreditScoreSource.Blank, "39f84806-6171-4d86-aac5-411b12a3d4ef" },
            { ReliedOnCreditScoreSource.Type1, "1545ddd0-2b35-4cb5-a600-baa0b325a41c" },
            { ReliedOnCreditScoreSource.Type2, "7f90c3c4-7613-4d86-8a5c-f6c57b4e6235" },
            { ReliedOnCreditScoreSource.Type2Soft, "2c79d999-300b-4395-ab1a-014eb80a5bab" },
            { ReliedOnCreditScoreSource.Type3, "c7d337d1-9e58-4998-86fe-46c9bf9be1f1" },
            { ReliedOnCreditScoreSource.Other, "4ca1df44-7cfb-4f44-8e3f-df17c3210fe9" }
        };

        /// <summary>
        /// Member variable storing the relied on credit score source to guid map dictionary.
        /// </summary>
        private static ReadOnlyDictionary<ReliedOnCreditScoreSource, string> initializedDictionary = new ReadOnlyDictionary<ReliedOnCreditScoreSource, string>(reliedOnCreditScoreSourceToGuidMap);

        /// <summary>
        /// Gets the read only dictionary that maps relied on credit score source to guid.
        /// </summary>
        /// <value>The private dictionary that shouldn't be changed.</value>
        public static ReadOnlyDictionary<ReliedOnCreditScoreSource, string> ReliedOnCreditScoreSourceToGuidMap
        {
            get
            {
                return initializedDictionary;
            }
        }
    }
}