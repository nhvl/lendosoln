﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.ManualInvestorProductExpiration
{
    public enum E_InvestorProductDisabledStatus
    {
        Disabled = 0,
        DisabledTillNextLpeUpdate = 1,
    }
    public class InvestorProductItem
    {
        private string m_investorName;
        private string m_productCode;
        private E_InvestorProductDisabledStatus m_disableStatus;
        private string m_messagesToSubmittingUsers;
        private DateTime m_disableEntryModifiedDate;
        private Guid m_disableEntryModifiedByEmployeId;
        private string m_disableEntryModifiedByUserName;
        private DateTime m_disableDateTime;
        private bool m_isHiddenFromResult;
        private string m_notes;
        private Guid m_policyId;

        public string InvestorName 
        {
            get { return m_investorName; }  
        }
        public string ProductCode 
        {
            get { return m_productCode; }
        }
        public E_InvestorProductDisabledStatus DisableStatus 
        {
            get { return m_disableStatus; }
        }
        public string MessagesToSubmittingUsers 
        {
            get { return m_messagesToSubmittingUsers; }
        }
        public DateTime DisableEntryModifiedDate
        {
            get { return m_disableEntryModifiedDate; }
        }
        public bool IsHiddenFromResult
        {
            get { return m_isHiddenFromResult; }
        }
        public string PriceResultLabel
        {
            get { return m_isHiddenFromResult ? "Hidden" : "Red Pricing"; }
        }
        public Guid DisableEntryModifiedByEmployeeId
        {
            get { return m_disableEntryModifiedByEmployeId; }
        }
        public string DisableEntryModifiedByUserName
        {
            get { return m_disableEntryModifiedByUserName; }
        }

        public DateTime DisableDateTime 
        {
            get { return m_disableDateTime; }
        }
        public string Notes
        {
            get { return m_notes; }
        }
        public Guid LockPolicyId
        {
            get { return m_policyId; }
        }
        public override bool Equals(object obj)
        {
            InvestorProductItem _obj = obj as InvestorProductItem;
            if (null == _obj)
                return false;

            return m_investorName == _obj.m_investorName && m_productCode == _obj.m_productCode;
        }
        public override int GetHashCode()
        {
            if (null == m_investorName)
                return 0;

            return m_investorName.GetHashCode();
        }

        public InvestorProductItem(string investorName, string productCode, Guid policyId, E_InvestorProductDisabledStatus disableStatus, string messageToSubmittingUsers,
            DateTime disableEntryModifiedDate, Guid disableEntryModifiedByEmployeeId, string disableEntryModifiedByUserName, DateTime disableDateTime, bool isHiddenFromResult, string notes)
        {
            m_investorName = investorName;
            m_productCode = productCode;
            m_policyId = policyId;

            m_disableStatus = disableStatus;
            m_messagesToSubmittingUsers = messageToSubmittingUsers;
            m_disableDateTime = disableDateTime;
            m_disableEntryModifiedDate = disableEntryModifiedDate;
            m_disableEntryModifiedByEmployeId = disableEntryModifiedByEmployeeId;
            m_disableEntryModifiedByUserName = disableEntryModifiedByUserName;
            m_isHiddenFromResult = isHiddenFromResult;
            m_notes = notes;
        }
    }
}
