﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.HttpModule;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.RatePrice;
using LendersOffice.RatePrice.Helpers;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;

namespace LendersOffice.ManualInvestorProductExpiration
{
    public class InvestorProductManager
    {
        private static string ConvertDisableStatus(E_InvestorProductDisabledStatus status)
        {
            switch (status)
            {
                case E_InvestorProductDisabledStatus.Disabled: return "Disabled";
                case E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate: return "DisabledTillNextPricingUpdate";
                default:
                    throw new UnhandledEnumException(status);
            }
        }
        private static E_InvestorProductDisabledStatus ConvertDisableStatus(string status)
        {
            switch (status)
            {
                case "Disabled": return E_InvestorProductDisabledStatus.Disabled;
                case "DisabledTillNextPricingUpdate": return E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate;
                default:
                    Tools.LogBug("Unhandle disable status=" + status + ". Default to Disabled");
                    return E_InvestorProductDisabledStatus.Disabled;
            }
        }
        public static List<InvestorProductItem> ListManualDisabledInvestorProductItem(Guid sBrokerId)
        {
            return ListManualDisabledInvestorProductItem(sBrokerId, Guid.Empty, false);
        }
        public static List<InvestorProductItem> ListManualDisabledInvestorProductItem(Guid sBrokerId, Guid sPolicyId)
        {
            return ListManualDisabledInvestorProductItem(sBrokerId, sPolicyId, true);
        }
        private static List<InvestorProductItem> ListManualDisabledInvestorProductItem(Guid sBrokerId, Guid sPolicyId, bool haveProductsBeenMigratedFromBroker)
        {
            List<InvestorProductItem> list = new List<InvestorProductItem>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            string procedureName;
            var parameters = new List<SqlParameter>() { new SqlParameter("@BrokerId", sBrokerId) };
            if (haveProductsBeenMigratedFromBroker)
            {
                if (sPolicyId != Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@PolicyId", sPolicyId));
                    procedureName = "ListManualDisabledInvestorProductByBrokerIdPerPolicy";
                }
                else
                {
                    procedureName = "ListManualDisabledInvestorProductByBrokerIdAllPolicies";
                }
            }
            else // if (false == haveProductsBeenMigratedFromBroker)
            {
                procedureName = "ListManualDisabledInvestorProductByBrokerId";
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, procedureName, parameters))
            {
                while (reader.Read())
                {
                    string investorName = (string)reader["InvestorName"];
                    string productCode = (string)reader["ProductCode"];
                    E_InvestorProductDisabledStatus disableStatus = ConvertDisableStatus((string)reader["DisableStatus"]);
                    string messagesToSubmittingUsers = (string)reader["MessagesToSubmittingUsers"];
                    DateTime disableDateTime = (DateTime)reader["DisableDateTime"];
                    DateTime disableEntryModifiedDate = (DateTime)reader["DisableEntryModifiedDate"];
                    Guid disableEntryModifiedByEmployeeId = (Guid)reader["DisableEntryModifiedByEmployeeId"];
                    string disableEntryModifiedByUserName = (string)reader["DisableEntryModifiedByUserName"];
                    bool isHiddenFromResult = (bool)reader["IsHiddenFromResult"];
                    string notes = (string)reader["Notes"];
                    Guid policyId;
                    if (haveProductsBeenMigratedFromBroker)
                    {
                        policyId = (Guid)reader["LockPolicyId"];
                    }
                    else
                    {
                         policyId = Guid.Empty;
                    }

                    list.Add(new InvestorProductItem(investorName, productCode, policyId, disableStatus, messagesToSubmittingUsers,
                        disableEntryModifiedDate, disableEntryModifiedByEmployeeId, disableEntryModifiedByUserName, disableDateTime, isHiddenFromResult, notes));
                }
            }
            stopwatch.Stop();
            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (null != monitorItem)
            {
                monitorItem.AddTimingDetail("InvestorProductManager.ListManualDisabledInvestorProductItem", stopwatch.ElapsedMilliseconds);
            }

            return list;
        }

        public static void AddForMigration(Guid brokerID,Guid defaultLPID,string investorName,string productCode,E_InvestorProductDisabledStatus status,string message,bool isHiddenFromResult,string notes,AbstractUserPrincipal principal, Guid employeeId,string employeeName)
        {
            Add(brokerID, defaultLPID, investorName, productCode, status, message, isHiddenFromResult, notes, principal, false /* isAfterMigration */, employeeId, employeeName, false);
        }
        //public static void Add(Guid sBrokerId, string investorName, string productCode, E_InvestorProductDisabledStatus status, string message, bool isHiddenFromResult, string notes, AbstractUserPrincipal principal)
        //{
        //    Add(sBrokerId, Guid.Empty, investorName, productCode, status, message, isHiddenFromResult, notes, principal, false /* isAfterMigration */);
        //}
        /// <summary>
        /// Adds the specified product to the disabled products list.
        /// </summary>
        /// <param name="sBrokerId">The Broker Id to disable for.</param>
        /// <param name="sPolicyId">The lock policy id for which the product is being disabled.</param>
        /// <param name="investorName">The name of the investor for the product.</param>
        /// <param name="productCode">The product code of the product.</param>
        /// <param name="status">The disabled status to assign to the specified product.</param>
        /// <param name="message">The message to display to users when they are unable to lock.</param>
        /// <param name="isHiddenFromResult">Boolean specifies whether or not to hide from pricing.  If false, will use red pricing.</param>
        /// <param name="notes">Internal notes for lock desk users.</param>
        /// <param name="principal">The user making this request.</param>
        /// <exception cref="DataAccess.CBaseException"><paramref name="sPolicyId"/> is Guid.Empty.</exception>
        /// <exception cref="DataAccess.CBaseException"><paramref name="principal"/> is null.</exception>
        public static void Add(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode, E_InvestorProductDisabledStatus status, string message, bool isHiddenFromResult, string notes, AbstractUserPrincipal principal)
        {
            Add(sBrokerId, sPolicyId, investorName, productCode, status, message, isHiddenFromResult, notes, principal, true /* isAfterMigration */);
        }
        private static void Add(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode, E_InvestorProductDisabledStatus status, string message, bool isHiddenFromResult, string notes, AbstractUserPrincipal principal, bool isAfterMigration)
        {
            Add(sBrokerId, sPolicyId, investorName, productCode, status, message, isHiddenFromResult, notes, principal, isAfterMigration, Guid.Empty, null, true);
        }
        private static void Add(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode, E_InvestorProductDisabledStatus status, string message, bool isHiddenFromResult, string notes, AbstractUserPrincipal principal, bool isAfterMigration, Guid employeeId, string employeeName, bool usePrincipalsInfo)
        {
            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Could not obtain authentication information for auditing purpose");
            }

            string procedureName;
            var parameters = new List<SqlParameter>() {
                                            new SqlParameter("@BrokerId", sBrokerId),
                                            new SqlParameter("@InvestorName", investorName),
                                            new SqlParameter("@ProductCode", productCode),
                                            new SqlParameter("@DisableStatus", ConvertDisableStatus(status)),
                                            new SqlParameter("@MessagesToSubmittingUsers", message),
                                            new SqlParameter("@IsHiddenFromResult", isHiddenFromResult ),
                                            new SqlParameter("@Notes", notes)
                                        };
            if (false == usePrincipalsInfo)
            {
                parameters.Add(new SqlParameter("@DisableEntryModifiedByEmployeeId", employeeId));
                parameters.Add(new SqlParameter("@DisableEntryModifiedByUserName", employeeName));
            }
            else
            {
                parameters.Add(new SqlParameter("@DisableEntryModifiedByEmployeeId", principal.EmployeeId));
                parameters.Add(new SqlParameter("@DisableEntryModifiedByUserName", principal.DisplayName));
            }


            if (isAfterMigration || false == usePrincipalsInfo)  // this is for migration.
            {
                if (sPolicyId != Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@PolicyId", sPolicyId));
                    procedureName = "AddManualDisabledInvestorProductPerPolicy";
                }
                else
                {
                    // 7/18/2014 dd - Removed the use of using LockPolicyId=Guid.Empty as a way to add to all lock policies.
                    //                Calling method need to iterate through each policy manually.
                    throw new CBaseException("Lock policy is required.", "No longer support LockPolicy=GUid.Empty scenario.");
                    //procedureName = "AddManualDisabledInvestorProductPerBrokerAllPolicies";
                }
            }
            else //if (false == isAfterMigration)
            {
                procedureName = "AddManualDisabledInvestorProduct";
            }

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, procedureName, 3, parameters);

        }

        public static void Remove(Guid sBrokerId, string investorName, string productCode)
        {
            Remove(sBrokerId, Guid.Empty, investorName, productCode, false);
        }
        public static void Remove(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode)
        {
            Remove(sBrokerId, sPolicyId, investorName, productCode, true);
        }
        private static void Remove(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode, bool isAfterMigration)
        {
            if (null == investorName || null == productCode)
                return;

            string procedureName;
            var parameters = new List<SqlParameter>() {
                                    new SqlParameter("@BrokerId", sBrokerId),
                                    new SqlParameter("@InvestorName", investorName),
                                    new SqlParameter("@ProductCode", productCode),
                                };
            if (isAfterMigration)
            {
                parameters.Add(new SqlParameter("@PolicyId", sPolicyId));
                procedureName = "DeleteManualDisabledInvestorProductPerPolicy";
            }
            else
            {
                procedureName = "DeleteManualDisabledInvestorProduct";
            }

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, procedureName, 3, parameters);
        }

        public static InvestorProductItem RetrieveManualInvestorProduct(Guid sBrokerId, Guid sPolicyId, string investorName, string productCode, RateOptionExpirationDataLoader dataLoader)
        {
            InvestorProductItem item = null;
            ManualDisabledInvestorProductInfo info = dataLoader.GetMigratedManualDisabledInvestorProductInfo(sBrokerId, investorName, productCode, sPolicyId, shouldLog: false);
            if (info != null)
            {
                item = new InvestorProductItem(info.InvestorName, info.ProductCode, info.LockPolicyId, ConvertDisableStatus(info.DisableStatus), info.MessageToSubmittingUsers,
                    info.DisableEntryModifiedDate, info.DisableEntryModifiedByEmployeeId, info.DisableEntryModifiedByUserName, info.DisableDateTime, info.IsHiddenFromResult, info.Notes);
            }

            return item;
        }

        /// <summary>
        /// Retrieves all product types by broker. Does not intersect with master
        /// </summary>
        /// <param name="sBrokerId"></param>
        /// <returns></returns>
        public static List<string> RetrieveProductTypes(Guid sBrokerId)
        {
            List<string> items = new List<string>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListProductTypesByBrokerId",
                new SqlParameter("@sBrokerId", sBrokerId)))
            {
                while (reader.Read())
                {
                    items.Add((string)reader["llpProductType"]);
                }
            }

            return items;
        }


        /// <summary>
        /// Retrieves all unique investor name for given broker. Does not intersect with master
        /// </summary>
        /// <param name="sBrokerId"></param>
        /// <returns></returns>
        public static List<string> RetrieveInvestors(BrokerDB broker)
        {
            HashSet<string> uniqueSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            
            foreach (var o in InvestorNameUtils.ListInvestorProductByBroker(broker))
            {
                uniqueSet.Add(o.Key);
            }

            return new List<string>(uniqueSet);
        }


        private static List<KeyValuePair<string, List<string>>> ListValidInvestorProductByBrokerId(BrokerDB broker)
        {
            Guid sBrokerId = broker.BrokerID;

            // Return a list of Investor+ProductCode for this particular lender. Only Investor+ProductCode that has ratesheet expiration enable will return.
            string cacheKey = "ListValidInvestorProductByBrokerId_" + sBrokerId;
            List<KeyValuePair<string, List<string>>> masterList = (List<KeyValuePair<string, List<string>>>)LOCache.Get(cacheKey);

            if (null == masterList)
            {
                // Step 1 - Get only valid Investor + Product from the system.
                List<KeyValuePair<string, string>> globalInvestorProductList = InvestorNameUtils.ListInvestorProductHasRateSheetExpirationFeature(broker);

                // Step 2 - Get only valid Investor + Product for this lender.
                IEnumerable<KeyValuePair<string, string>> investorProductByBrokerList = InvestorNameUtils.ListInvestorProductByBroker(broker);

                // Step 3 - Filter out item that does not exist in both list.
                masterList = new List<KeyValuePair<string, List<string>>>();
                foreach (KeyValuePair<string, string> key in investorProductByBrokerList)
                {
                    bool isFound = false;
                    for (int i = 0; i < globalInvestorProductList.Count; i++)
                    {
                        KeyValuePair<string, string> _temp = globalInvestorProductList[i];
                        if (key.Key.ToLower() == _temp.Key.ToLower() && key.Value.ToLower() == _temp.Value.ToLower())
                        {
                            isFound = true;
                            globalInvestorProductList.RemoveAt(i);
                            break;
                        }
                    }
                    if (isFound)
                    {
                        bool isInsertMasterList = false;
                        foreach (KeyValuePair<string, List<string>> _t2Key in masterList)
                        {
                            if (_t2Key.Key.ToLower() == key.Key.ToLower())
                            {
                                _t2Key.Value.Add(key.Value);
                                isInsertMasterList = true;
                                break;
                            }
                        }
                        if (!isInsertMasterList)
                        {
                            List<string> _list = new List<string>();
                            _list.Add(key.Value);
                            KeyValuePair<string, List<string>> _item = new KeyValuePair<string, List<string>>(key.Key, _list);
                            masterList.Add(_item);
                        }
                    }
                }
                LOCache.Set(cacheKey, masterList, DateTime.Now.AddMinutes(30)); // Cache the object for 30 minutes.
            }
            return masterList;

        }
        public static List<string> ListAllInvestors(BrokerDB broker)
        {
            List<string> result = new List<string>();

            List<KeyValuePair<string, List<string>>> list = ListValidInvestorProductByBrokerId(broker);
            if (null != list)
            {
                foreach (KeyValuePair<string, List<string>> key in list)
                {
                    result.Add(key.Key);
                }
            }
            return result;
        }
        public static List<string> ListAllProductCodeByInvestor(BrokerDB broker, string investorName)
        {
            List<KeyValuePair<string, List<string>>> list = ListValidInvestorProductByBrokerId(broker);
            if (null != list && null != investorName)
            {
                investorName = investorName.ToLower();

                foreach (KeyValuePair<string, List<string>> key in list)
                {
                    if (key.Key.ToLower() == investorName)
                        return key.Value;
                }
            }
            return new List<string>();
        }

        public static List<LoanProgramSummaryItem> ListProgramSummaryByInvestorProduct(Guid sBrokerId, LockPolicy lockPolicy, string investorName, string productCode)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(sBrokerId);
            List<LoanProgramSummaryItem> results = new List<LoanProgramSummaryItem>();
            List<string> investorNamesForRateLockCutOff = new List<string>() { investorName };
            List<Tuple<string, string>> investorProductCodePairs = new List<Tuple<string, string>>() { new Tuple<string, string>(investorName, productCode) };
            RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(investorNamesForRateLockCutOff, null, investorProductCodePairs, brokerDB);
            SqlParameter[] parameters = {
                                            new SqlParameter("@sBrokerId", sBrokerId),
                                            new SqlParameter("@lLpInvestorNm", investorName),
                                            new SqlParameter("@ProductCode", productCode)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProductByInvestorProduct", parameters))
            {
                while (reader.Read())
                {
                    string lLpTemplateNm = (string)reader["lLpTemplateNm"];
                    string lpeAcceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                    long lpeAcceptableRsFileVersionNumber = (long)reader["LpeAcceptableRsFileVersionNumber"];

                    RateOptionExpirationResult rateStatus = CApplicantPrice.DetermineRateOptionExpiration(lockPolicy, E_LpeRsExpirationByPassPermissionT.NoByPass, lpeAcceptableRsFileId, lpeAcceptableRsFileVersionNumber,
                        investorName, productCode, true /* byPassManualDisabled */, dataLoader);

                    string rateStatusDesc = "Current";
                    if (rateStatus.IsBlockedRateLockSubmission)
                    {
                        rateStatusDesc = "Expired";
                    }

                    results.Add(new LoanProgramSummaryItem(investorName, productCode, lLpTemplateNm, rateStatusDesc));
                }
            }
            return results;
        }
    
}
}
