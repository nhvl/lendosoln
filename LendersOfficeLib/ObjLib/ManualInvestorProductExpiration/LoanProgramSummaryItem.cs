﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LendersOffice.ManualInvestorProductExpiration
{
    public class LoanProgramSummaryItem
    {
        private string m_investorName;
        private string m_productCode;
        private string m_loanProgramName;
        private string m_pricingStatus;

        public string InvestorName
        {
            get { return m_investorName; }
        }
        public string ProductCode
        {
            get { return m_productCode; }
        }
        public string LoanProgramName
        {
            get { return m_loanProgramName; }
        }
        public string PricingStatus
        {
            get { return m_pricingStatus; }
        }

        public LoanProgramSummaryItem(string investorName, string productCode, string loanProgramName, string pricingStatus)
        {
            m_investorName = investorName;
            m_productCode = productCode;
            m_loanProgramName = loanProgramName;
            m_pricingStatus = pricingStatus;
        }
    }
}
