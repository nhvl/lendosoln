using System;
using System.Collections;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for RuleList.
	/// </summary>
	public class RuleList : ArrayList
	{
		public RuleList() : base()
		{}

		public RuleList(ICollection c) : base(c)
		{}

		public int Add(Rule rule)
		{
			if (rule == null)
				throw new ArgumentNullException("A null object cannot be added to the rule list.");

			return base.Add(rule);
		}
		
		

	}
}
