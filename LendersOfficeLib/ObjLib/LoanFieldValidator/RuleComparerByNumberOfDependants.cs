using System;
using System.Collections;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for RuleComparerByNumberOfDependants.
	/// </summary>
	public class RuleComparerByNumberOfDependants : IComparer
	{
		public RuleComparerByNumberOfDependants()
		{}

		public int Compare(object object1, object object2)
		{
			Rule r1 = (Rule)object1;
			Rule r2 = (Rule)object2;

			/*
			if (r1.node.Prev.Count == r2.node.Prev.Count)
				return 0;
			else if (r1.node.Prev.Count > r2.node.Prev.Count)
				return 1;
			else return -1;
			*/
			if (r1.Dependencies.Count == r2.Dependencies.Count)
				return 0;
			else if (r1.Dependencies.Count > r2.Dependencies.Count)
				return 1;
			else return -1;
		}
	}
}
