using System;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for CRuleBase.
	/// </summary>
	public abstract class CRuleBase : IRuleBase
	{
		private Object _m_object;
		private RuleTable Rules;
		private Log log;

		public CRuleBase()
		{
			Rules = new RuleTable();
			log = new Log();
		}

		#region Message Logging
		private void LogMessage(MsgSeverity sev, string msg, Rule r1)
		{
			log.Add(new RuleMessage(sev, msg, r1));
		}

		private MethodInfo calledFrom()
		{
			StackTrace st = new StackTrace();
			
			if (st.FrameCount <= 2)
				throw new CBaseException(ErrorMessages.Generic, "Exception when reporting a message; this method can only be called within a rule.");
			
			StackFrame sf = st.GetFrame(2);
			return (MethodInfo)sf.GetMethod();
		}
		
		public void ReportError(string message)
		{
			if (message == null)
				throw new ArgumentNullException("Exception in 'ReportError'; The parameter is required and should be different from 'null'.");

			MethodInfo mInfo = calledFrom();
			if (Rule.isRule(mInfo))
			{
				Rule rule = Rules.GetBySignature(mInfo.Name);
				LogMessage(MsgSeverity.Error, message, rule);
				rule.disable();
			}
			else
			{
				throw new CBaseException(ErrorMessages.Generic, "Exception when reporting a message; 'ReportError' can only be called within a rule.");
			}
		}

		public void ReportWarning(string message)
		{
			if (message == null)
				throw new ArgumentNullException("Exception in 'ReportWarning'; The parameter is required and should be different from 'null'.");

			MethodInfo mInfo = calledFrom();
			if (Rule.isRule(mInfo))
			{
				Rule rule = Rules.GetBySignature(mInfo.Name);
				LogMessage(MsgSeverity.Warning, message, rule);
			}
			else
			{
                throw new CBaseException(ErrorMessages.Generic, "Exception when reporting a message; 'ReportWarning' can only be called within a rule.");
			}
		}

		public void ReportInfo(string message)
		{
			if (message == null)
				throw new ArgumentNullException("Exception in 'ReportInfo'; The parameter is required and should be different from 'null'.");

			MethodInfo mInfo = calledFrom();
			if (Rule.isRule(mInfo))
			{
				Rule rule = Rules.GetBySignature(mInfo.Name);
				LogMessage(MsgSeverity.Info, message, rule);
			}
			else
			{
                throw new CBaseException(ErrorMessages.Generic, "Exception when reporting a message; 'ReportInfo' can only be called within a rule.");
			}
		}

		public string getErrors()
		{
			string result = "";

			RuleMessage[] messages = log.getErrors();
			foreach (RuleMessage msg in messages)
			{
				result += msg.ToString() + "\n";
			}
			return result;
		}

		public string[] getErrorsArray()
		{
			string[] result;
			RuleMessage[] messages = log.getErrors();
			result = new string[messages.Length];

			int i=0;
			foreach (RuleMessage msg in messages)
			{
				result[i++] = msg.ToString();
			}
			return result;
		}

		public string getWarnings()
		{
			string result = "";

			RuleMessage[] messages = log.getWarnings();
			foreach (RuleMessage msg in messages)
			{
				result += msg.ToString() + "\n";
			}
			return result;
		}

		public string getInfo()
		{
			string result = "";

			RuleMessage[] messages = log.getInfo();
			foreach (RuleMessage msg in messages)
			{
				result += msg.ToString() + "\n";
			}
			return result;
		}

		public string getAllMessages()
		{
			string result = "";

			RuleMessage[] messages = log.getAllMessages();
			foreach (RuleMessage msg in messages)
			{
				result += msg.ToString() + "\n";
			}
			return result;
		}

		#endregion

		#region Underlying Object Access
		private bool checkAllowedName(MethodInfo mInfo, string fieldName, string type)
		{
			if (Rule.isRule(mInfo))
			{
				switch (type)
				{
					case "get":
						if (Rules.GetBySignature(mInfo.Name).getInputs.Contains(fieldName) || Rules.GetBySignature(mInfo.Name).getOutputs.Contains(fieldName))
							return true;
						return false;
					case "set":
						if (Rules.GetBySignature(mInfo.Name).getOutputs.Contains(fieldName) || Rules.GetBySignature(mInfo.Name).getOutputs.Contains(fieldName))
							return true;
						return false;
					default:
						throw new ArgumentException("checkAllowedName only supports 'get' or 'set' types.");
				}
			}
			else
				throw new ArgumentException("GetField or SetField were not called from a rule.");
		}

		public object GetField(string fieldName)
		{
			if (_m_object == null)
				throw new NullReferenceException("Exception in 'GetField'; the underlying object has not been yet initialized.");
			
			StackTrace st = new StackTrace();
			if (st.FrameCount <= 1)
                throw new CBaseException(ErrorMessages.Generic, "Exception in 'GetField'; this method can only be called within a rule.");

			if (fieldName == null)
				throw new ArgumentNullException("Exception in 'GetField'; The parameter is required and should be different from 'null'.");

			StackFrame sf = st.GetFrame(1);
			MethodInfo mInfo = (MethodInfo)sf.GetMethod();
			string ruleSignature = mInfo.Name;

			if (checkAllowedName(mInfo, fieldName, "get"))
			{
				return _m_object.GetType().GetField(fieldName).GetValue(_m_object);
			}
			else
                throw new CBaseException(ErrorMessages.Generic, "Rule '" + ruleSignature + "' is not allowed to set field '" + fieldName + "' since it's not specified in the InputFields parameter.");
		}

        /*
        public bool SetField(string fieldName, object value)
        {
            if (_m_object == null)
                throw new CBaseException(ErrorMessages.Generic, "Exception in 'SetField'; the underlying object has not been yet initialized.");
			
            StackTrace st = new StackTrace();
            if (st.FrameCount <= 1)
                throw new CBaseException(ErrorMessages.Generic, "Exception in 'SetField'; this method can only be called within a rule.");

            if (fieldName == null || value == null)
                throw new ArgumentNullException("Exception in 'SetField'; Both parameters are required and should be different from 'null'.");
			
            StackFrame sf = st.GetFrame(1);
            MethodInfo mInfo = (MethodInfo)sf.GetMethod();
            string ruleSignature = mInfo.Name;
			
            if (checkAllowedName(mInfo, fieldName, "set"))
            {
                _m_object.GetType().GetField(fieldName).SetValue(_m_object, value);
                return true;
            }
            else
                throw new CBaseException(ErrorMessages.Generic, "Rule '" + ruleSignature + "' is not allowed to set field '" + fieldName + "' since it's not specified in the OutputFields parameter.");
			
        }
        */
        #endregion


        public void ValidateRules(Object obj)
		{
			if (obj == null)
                throw new CBaseException(ErrorMessages.Generic, "An object instance should be passed to this method.");
			
			if (_m_object != obj)
			{
				//Run Validate rules for the first time or with a new object
				Debug.WriteLine("Validate rules for the first time.");

				_m_object = obj;
				
				RuleList list = FindRules();
				
				if (!Rules.Empty)
					Rules.reset();

				Rules.AddList(list);
				Rules.BuildGraph();
				Rules.Execute();
			}
			else
			{
				//Run Validate rules not for the first time
				Debug.WriteLine("Revalidate rules after another attempt.");
				//-- reset messages
				log.reset();
				//-- reset "toRun" in each rule
				foreach (Rule rule in Rules.GetAll())
					rule.restart();
				//-- Execute again
				Rules.Execute();
			}
		}

		public bool hasErrors
		{
			get
			{
				if (log == null)
					throw new ArgumentNullException("Cannot check the errors because the log is null.");
				if (log.hasErrors)
					return true;
				
				return false;
			}
		}


		private RuleList FindRules()
		{
			BindingFlags myFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
			Type typeInput = Type.GetType(ConstRules.BaseName + "InputFieldsAttribute");
			Type thisType = this.GetType();
			Type baseClass = Type.GetType(ConstRules.BaseName + "CRuleBase");
			RuleList list = new RuleList();

			if (!thisType.IsSubclassOf(baseClass))
                throw new CBaseException(ErrorMessages.Generic, "This method needs to be called from an object derived from CRuleBase.");
			
			MethodInfo[] methodList = thisType.GetMethods(myFlags);
			foreach (MethodInfo method in methodList)
			{
				Object[] Attributes = method.GetCustomAttributes(typeInput,false);
				if (Attributes != null && Attributes.Length != 0)
				{
					Rule rule = new Rule(Rules, method, this);
					list.Add(rule);
				}
			}
			return list;
		}
			
	}
}
