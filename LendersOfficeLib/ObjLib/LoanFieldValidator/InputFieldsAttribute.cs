using System;
using System.Collections;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for InputFieldsAttribute.
	/// </summary>
	
	[ AttributeUsage( AttributeTargets.Method,AllowMultiple = false )]
	public class InputFieldsAttribute : Attribute
	{
		private string m_stringList;
		private Hashtable m_Strings;

		public InputFieldsAttribute( params string[] values )
		{
			m_stringList = "";
			m_Strings = new Hashtable();

			foreach ( string sCocat in values )
			{
				m_stringList = m_stringList + sCocat + "|";
				m_Strings.Add(sCocat, true);
			}
			int length = m_stringList.Length;
			m_stringList = m_stringList.Substring(0, (--length) );
		}

		public Hashtable getFields()
		{
			return m_Strings;
		}
		
	}
}
