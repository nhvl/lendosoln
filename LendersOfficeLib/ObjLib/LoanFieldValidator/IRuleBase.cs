using System;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for IRuleBase.
	/// </summary>
	public interface IRuleBase
	{
		void ValidateRules(Object obj);
		object GetField(string name);
		//bool SetField(string name, object value);
		void ReportError(string message);
		void ReportWarning(string message);
		void ReportInfo(string msgmessage);
	}
}
