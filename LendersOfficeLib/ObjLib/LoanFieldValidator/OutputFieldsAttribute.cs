using System;
using System.Collections;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for OutputFieldsAttribute.
	/// </summary>
	
	[ AttributeUsage( AttributeTargets.Method, AllowMultiple = false )]
	public class OutputFieldsAttribute : Attribute
	{
		private string m_stringList;
		private Hashtable m_Strings;

		public OutputFieldsAttribute( params string[] values )
		{
			m_stringList = "";
			m_Strings = new Hashtable();

			foreach ( string sCocat in values )
			{
				m_Strings.Add(sCocat, true);
				m_stringList = m_stringList + sCocat + "|";
			}
			int length = m_stringList.Length;
			m_stringList = m_stringList.Substring(0, (--length) );
		}

		public Hashtable getFields()
		{
			return m_Strings;
		}

	}
}
