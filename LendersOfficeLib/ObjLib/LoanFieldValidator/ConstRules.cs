using System;
using System.Reflection;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for ConstRules.
	/// </summary>
	public class ConstRules
	{
		public ConstRules(){}

		public static string BaseName
		{
			get
			{
				string name = typeof(ConstRules).FullName;
				return name.Substring(0,name.LastIndexOf(".")) + ".";
			}
		}
	}
}
