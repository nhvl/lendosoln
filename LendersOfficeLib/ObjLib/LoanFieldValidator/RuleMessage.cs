using System;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{

	public enum MsgSeverity
	{
		Error,
		Warning,
		Info,
		NotSpecified
	}
	
	
	/// <summary>
	/// Summary description for RuleMessage.
	/// </summary>
	public class RuleMessage
	{
		private MsgSeverity m_Severity;
		private string m_Text;
		private Rule m_rule;
		private string m_FieldName;

		public RuleMessage(MsgSeverity sev, string msg, Rule r1)
		{
			if (msg == null || r1 == null)
				throw new ArgumentNullException("A message with null arguments cannot be created.");
			
			m_FieldName = "";
			m_Severity = sev;
			m_Text = msg;
			m_rule = r1;
		}

		public MsgSeverity Severity
		{
			get
			{
				return m_Severity;
			}
		}

		public string Text
		{
			get
			{
				return m_Text;
			}
		}

		public Rule rule
		{
			get
			{
				return m_rule;
			}
		}

		public string FieldName
		{
			get
			{
				return m_FieldName;
			}
			set
			{
				m_FieldName = value;
			}
		}

		public override string ToString()
		{
			string result = "";
			switch (m_Severity)
			{
				case MsgSeverity.Error:
					result += "ERROR - ";
					break;
				case MsgSeverity.Warning:
					result += "WARNING - ";
					break;
				case MsgSeverity.Info:
					result += "INFO - ";
					break;
			}
			
			result += rule.Signature;
			if (m_FieldName != null && m_FieldName.Length != 0)
				result += "/" + m_FieldName;
			result += " - " + m_Text;
			return result;
		}


	}
}
