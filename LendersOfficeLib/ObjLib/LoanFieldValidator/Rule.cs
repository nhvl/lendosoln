using System;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for Rule.
	/// </summary>
	public class Rule
	{
		private string name;
		private string signature;
		private Hashtable InputFields;
		private Hashtable OutputFields;
		private Hashtable m_DependsOn;
		private MethodInfo mInfo;
		private Object m_Object;
		private RuleTable m_table;
		public Node node;
		public bool isRun;
		public bool isEnabled;
		public bool isFailed;

		#region Rule Properties
		//should always be true (not really needed)
		public bool hasInputs
		{
			get
			{
				if (InputFields != null && InputFields.Count != 0)
					return true;
				else
					return false;
			}
		}

		//can be true or false
		public bool hasOutputs
		{
			get
			{
				if (OutputFields != null && OutputFields.Count != 0)
					return true;
				else
					return false;
			}
		}

		public Hashtable getOutputs
		{
			get
			{
				return OutputFields;
			}
		}


		public Hashtable getInputs
		{
			get
			{
				return InputFields;
			}
		}

		public String Name
		{
			get
			{
				return this.name;
			}
		}

		public String Signature
		{
			get
			{
				return this.signature;
			}
		}

		public Rule DependsOn
		{
			set
			{
				if (!m_DependsOn.Contains(value))
					m_DependsOn.Add(value.name, value);
			}
		}

		public Hashtable Dependencies
		{
			get
			{
				return m_DependsOn;
			}
		}

		public MethodInfo method
		{
			get
			{
				return mInfo;
			}
		}

		#endregion

		public Rule(RuleTable rules, MethodInfo method, Object obj)
		{
			if (rules == null || method == null || obj == null)
				throw new ArgumentNullException("The constructor needs all parameters in order to build the rule.");

			InputFields = new Hashtable();
			OutputFields = new Hashtable();
			m_DependsOn = new Hashtable();

			m_Object = obj;
			m_table = rules;

			Object[] Attributes = null;
			node = new Node(this);

			Type typeInput = Type.GetType(ConstRules.BaseName + "InputFieldsAttribute");
			Type typeOutput = Type.GetType(ConstRules.BaseName + "OutputFieldsAttribute");
			mInfo = method;
			this.signature = method.Name;
			this.isEnabled = true;
			this.isRun = false;
			this.isFailed = false;

			checkThisRule();

			Attributes = method.GetCustomAttributes(typeInput,false);
			if (Attributes != null && Attributes.Length != 0)
			{
				InputFieldsAttribute attr = (InputFieldsAttribute)Attributes[0];
				foreach (string key in attr.getFields().Keys)
				{
					if (!InputFields.Contains(key))
						InputFields.Add(key, true);

				}
			}

			Attributes = method.GetCustomAttributes(typeOutput,false);
			if (Attributes != null && Attributes.Length != 0)
			{
				OutputFieldsAttribute attr = (OutputFieldsAttribute)Attributes[0];
				foreach (string key in attr.getFields().Keys)
				{
					if (!OutputFields.Contains(key))
						OutputFields.Add(key, true);

				}
			}

		}

		public void AddTo(RuleTable rTable)
		{
			AddRuleAndCreateName(rTable);
		}


		private void AddRuleAndCreateName(RuleTable rules)
		{
			string number = "0000" + (rules.Count+1).ToString();
			number = number.Substring(number.Length-4);
			name = "Rule" + number;
			rules.Add(this);
		}


		private void checkThisRule()
		{
			//CHECK THAT:
			//method.ReturnType	   -- should be void!
			if (!mInfo.ReturnType.Equals(typeof(void)))
			{
				throw new CBaseException(ErrorMessages.Generic, "By definition, rules are methods with a 'void' return type. However, method '" + this.signature + "' has a different return type.");
			}

			//method.GetParameters -- should have no parameters!
			if (mInfo.GetParameters().Length != 0)
			{
				throw new CBaseException(ErrorMessages.Generic, "By definition, rules are methods that receive 0 parameters. However, method '" + this.signature + "' currently receives " + mInfo.GetParameters().Length + ".");
			}

			//Input and Output fields exist in the underlying object
			Type type = m_Object.GetType();
			FieldInfo field;
			foreach (string fieldName in getInputs.Keys)
			{
				field = type.GetField(fieldName);
				if (field == null)
					throw new NullReferenceException("Field '" + fieldName + "' referenced in rule '" + Signature + "' does not exist.");
				field = null;
			}
			foreach (string fieldName in getOutputs.Keys)
			{
				field = type.GetField(fieldName);
				if (field == null)
					throw new NullReferenceException("Field '" + fieldName + "' referenced in rule '" + Signature + "' does not exist.");
				field = null;
			}

		}

		private bool isAllowedToRun()
		{
			bool printMessage = true;

			foreach (Rule rule in this.node.Next)
			{
				if (!rule.isFailed)
				{
					if (!rule.isRun && printMessage)
					{
						Debug.WriteLine("Rule " + this.signature + " ran before its ancestor (" + rule.signature + ").");
						printMessage = false;
					}
				}
				else
				{
					return false;
				}
			}
			return true;
		}


		public bool run()
		{
			if (this.isAllowedToRun())
			{
				this.isRun = true;
				foreach (Rule rule in this.node.Prev)
					rule.node.DependantRan();
				
				this.method.Invoke(m_Object, null);
			}
			return !this.isFailed;
		}

		public void disable()
		{
				this.isFailed = true;
		}

		public void restart()
		{
			this.isFailed = false;
			this.isRun = false;
			this.isEnabled = true;
			this.node.restart();
		}

		public override string ToString()
		{
			return "Name: " + this.name + " Signature: " + this.signature;
		}

		public static bool isRule(MethodInfo method)
		{
			if (method == null)
				throw new ArgumentNullException("'isRule' cannot receive a null parameter.");

			Type typeInput = Type.GetType(ConstRules.BaseName + "InputFieldsAttribute");

			Object[] Attributes = method.GetCustomAttributes(typeInput,false);
			if (Attributes != null && Attributes.Length != 0)
			{
				return true;
			}
			return false;
		}
	}
}
