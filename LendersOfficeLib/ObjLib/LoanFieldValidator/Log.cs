using System;
using System.Collections;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for Log.
	/// </summary>
	public class Log
	{
		private ArrayList m_msgList;
		private int m_error;
		private int m_warning;
 
		public Log()
		{
			m_msgList = new ArrayList();
			m_error = 0;
			m_warning = 0;
		}

		public ArrayList messages
		{
			get
			{
				return m_msgList;
			}
		}

		public RuleMessage[] getErrors()
		{
			return filterBySeverity(MsgSeverity.Error);
		}

		public RuleMessage[] getWarnings()
		{
			return filterBySeverity(MsgSeverity.Warning);
		}

		public RuleMessage[] getInfo()
		{
			return filterBySeverity(MsgSeverity.Info);
		}

		public RuleMessage[] getAllMessages()
		{
			return filterBySeverity(MsgSeverity.Info);
		}


		public bool hasErrors
		{
			get
			{
				return (m_error != 0);
			}
		}

		public bool hasWarnings
		{
			get
			{
				return (m_warning != 0);
			}
		}

		public void Add(RuleMessage msg)
		{
			if (msg == null)
				throw new ArgumentNullException("Arguments cannot be null.");

			m_msgList.Add(msg);
			if (msg.Severity == MsgSeverity.Error)
				m_error++;
			if (msg.Severity == MsgSeverity.Warning)
				m_warning++;
		}


		private RuleMessage[] filterBySeverity(MsgSeverity sev)
		{
			ArrayList list = new ArrayList();
			foreach (RuleMessage msg in m_msgList)
			{
				if (msg.Severity == sev || sev.Equals(MsgSeverity.NotSpecified))
				{
					list.Add(msg);
				}
			}
			RuleMessage[] final_list = new RuleMessage[list.Count];
			list.CopyTo(final_list);
			return final_list;
		}

		public void reset()
		{
			m_msgList.Clear();
			m_error = 0;
			m_warning = 0;
		}
	}
}
