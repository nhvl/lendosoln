using System;
using System.Collections;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for Node.
	/// </summary>
	public class Node
	{
		private Rule m_ownRule;
		private string m_state;
		private ArrayList m_next;
		private ArrayList m_prev;
		private int m_toRun;
		private int m_InitialToRun;

		public Node(Rule rule)
		{
			m_next = new ArrayList();
			m_prev = new ArrayList();
			m_state = "white";
			m_ownRule = rule;
			m_toRun = 0;
			m_InitialToRun = 0;
		}

		public void restart()
		{
			m_toRun = m_InitialToRun;
		}

		public string Color
		{
			get
			{
				return m_state;
			}
			set
			{
				m_state = value;
			}
		}

		public void DependantRan()
		{
			if (m_toRun != 0)
				m_toRun--;
		}

		public int toRun
		{
			get
			{
				return m_toRun;
			}
		}

		public bool isEmpty
		{
			get
			{
				return (m_next.Count == 0);
			}

		}

		public ArrayList Next
		{
			get
			{
				return m_next;
			}
		}

		public ArrayList Prev
		{
			get
			{
				return m_prev;
			}
		}

		public Rule AddNext
		{
			set
			{
				m_next.Add(value);
				m_toRun++;
				m_InitialToRun++;
			}
		}

		public Rule AddPrev
		{
			set
			{
				m_prev.Add(value);
			}
		}


		public void dfs(Stack stack)
		{
			if (Color.Equals("white"))
			{
				stack.Push(m_ownRule);
				Color = "gray";
				foreach (Object obj in Prev)
				{
					Node node = ((Rule)obj).node;
					node.dfs(stack);
				}
				Color = "black";
				stack.Pop();
			}
			else if (Color.Equals("gray"))
			{
				//Error detected. Print the cycle out.
				ArrayList cycle = new ArrayList();
				string str = "";
				
				while (stack.Count != 0)
				{
					cycle.Add(stack.Pop());
				}

				cycle.Reverse();
				cycle.RemoveRange(0,cycle.IndexOf(m_ownRule));
				cycle.Add(m_ownRule);
				
				foreach (Rule rule in cycle)
				{
					str += rule.Signature + " --> ";
				}
				str = str.Substring(0,str.Length-5);
				throw new CBaseException(ErrorMessages.Generic, "A dependency cycle has been detected between several rules. Cycle: " + str);
			}
		}

		public void sort(ArrayList runList)
		{
			if (Color.Equals("black"))
			{
				runList.Add(m_ownRule);
				foreach (Object obj in Prev)
				{
					Node node = ((Rule)obj).node;
					node.sort(runList);
				}
				Color = "white";
			}
		}

	}
}
