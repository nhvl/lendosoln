using System;
using System.Collections;
using System.Reflection;
using System.Diagnostics;

namespace LendersOfficeApp.ObjLib.LoanFieldValidator
{
	/// <summary>
	/// Summary description for RuleTable.
	/// </summary>
	public class RuleTable
	{
		private Hashtable RulesByName;
		private Hashtable RulesBySignature;

		public RuleTable()
		{
			RulesByName = new Hashtable();
			RulesBySignature = new Hashtable();
		}

		#region Accessor Methods
		public void Add(Rule rule)
		{
			if (rule == null)
				throw new ArgumentNullException("A rule cannot be added to the RuleTable if it references a null object.");
			RulesByName.Add(rule.Name,rule);
			RulesBySignature.Add(rule.Signature,rule);
		}

		public void reset()
		{
			if (RulesByName != null && RulesByName.Count != 0)
				RulesByName.Clear();

			if (RulesBySignature != null && RulesBySignature.Count != 0)
				RulesBySignature.Clear();
		}

		public ICollection GetAll()
		{
			return RulesByName.Values;
		}

		public bool ContainsName(string str)
		{
			if (str == null)
				return false;
			return RulesByName.Contains(str);
		}

		public bool ContainsSignature(string str)
		{
			if (str == null)
				return false;
			return RulesBySignature.Contains(str);
		}

		public Rule GetByName(string str)
		{
			if (str == null)
				throw new ArgumentNullException("Cannot look a rule up in the hashtable if the key is a null object.");
			return (Rule)RulesByName[str];
		}

		public Rule GetBySignature(string str)
		{
			if (str == null)
				throw new ArgumentNullException("Cannot look a rule up in the hashtable if the key is a null object.");
			return (Rule)RulesBySignature[str];
		}

		public int Count
		{
			get
			{
				return RulesByName.Count;
			}
		}

		public bool Empty
		{
			get
			{
				return (RulesByName.Count == 0) && (RulesBySignature.Count == 0);
			}
		}
		#endregion
	

		public void AddList(RuleList list)
		{
			foreach (Rule rule in list)
			{
				rule.AddTo(this);
			}
		}

		public void BuildGraph()
		{
			BuildDependencies();
			//SearchGraphForCycles();
		}


		private void BuildDependencies()
		{
			Hashtable outputs = new Hashtable();
			Hashtable inputs = new Hashtable();
			ArrayList al = null;
			
			foreach (Rule rule in RulesByName.Values)
			{
				foreach (string keyIn in rule.getInputs.Keys)
				{
					if (!inputs.Contains(keyIn))
					{
						al = new ArrayList();
						al.Add(rule);
						inputs.Add(keyIn,al);
					}
					else
					{
						((ArrayList)inputs[keyIn]).Add(rule);
					}
				}
				foreach (string keyOut in rule.getOutputs.Keys)
				{
					if (!outputs.Contains(keyOut))
					{
						al = new ArrayList();
						al.Add(rule);
						outputs.Add(keyOut,al);
					}
					else
					{
						((ArrayList)outputs[keyOut]).Add(rule);
					}
				}
			}

			foreach (Rule rule in RulesByName.Values)
			{
				foreach (string keyIn in rule.getInputs.Keys)
				{
					if (outputs.Contains(keyIn))
					{
						al = (ArrayList)outputs[keyIn];
						foreach (Rule rule2 in al)
						{
							rule.DependsOn = rule2;
							rule.node.AddNext = rule2;
							rule2.node.AddPrev = rule;
						}
					}
				}
			}
		}
	
		private void SearchGraphForCycles()
		{
			Stack stack = new Stack();

			//Loof for cycles in the graph by performing a graph traversal
			//using a recurvise 'Colored DFS Approach'
			foreach (Rule rule in RulesByName.Values)
			{
				if (rule.node.isEmpty)
					rule.node.dfs(stack);
			}
			//After the DFS traversal all nodes should have been colored black
			//Failure to assert this condition means that there is AT LEAST
			//one cycle in the graph.
			//An exception will be thrown with the first found cycle.
			foreach (Rule rule in RulesByName.Values)
			{
				if (!rule.node.Color.Equals("black"))
					rule.node.dfs(stack);
			}
			Debug.WriteLine("No dependencies found.");
		}

		//public void Execute(Object obj)
		public void Execute()
		{
			RuleList runList = new RuleList(RulesByName.Values);
			runList.Sort(new RuleComparerByNumberOfDependants());

			int totalRules = Count;
			int passedRules = 0;
			int failedRules = 0;
			int invokedRules = 0;
			int disabledRules = 0;

			Queue q = new Queue(runList.Count);

			foreach (Rule rule in runList)
			{
				Execute1Rule(rule, q);
			}

			foreach (Rule rule in runList)
			{
				if (rule.isRun)
					invokedRules++;
				if (rule.isFailed)
					failedRules++;
			}
			
			passedRules = invokedRules - failedRules;
			disabledRules = totalRules - invokedRules;
			
			Debug.WriteLine(String.Format("{0} out of {1} rules were invoked.",invokedRules,totalRules));
			Debug.WriteLine(String.Format("{0} out of {1} rules passed ({2} failed).",passedRules,invokedRules, failedRules));
			Debug.WriteLine(String.Format("{0} out of {1} rules were not invoked due to its dependencies.",disabledRules,totalRules));

		}

		private void Execute1Rule(Rule rule, Queue q)
		{
			if (rule.isEnabled)
			{
				if (!rule.isRun)
				{
					rule.run();
				}
				foreach (Rule dep in rule.node.Prev)
				{
					if ((dep.node.toRun < 1) && dep.isEnabled && !dep.isRun)
						q.Enqueue(dep);
				}
				while (q.Count != 0)
				{
					Rule dep = (Rule)q.Dequeue();
					Execute1Rule(dep,q);
				}
			}
		}


		public void PrintDependencies()
		{
			foreach (Rule rule in RulesByName.Values)
			{
				Debug.WriteLine(rule.ToString());
				string msg = "   depends on: ";
				foreach (Rule rule2 in rule.Dependencies.Values)
				{
					msg += rule2.Name + " ";
				}
				Debug.WriteLine(msg);
			}
		}
	
	}
}
