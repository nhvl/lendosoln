﻿namespace LendersOffice.ObjLib.LoXml
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using DataAccess;

    /// <summary>
    /// LOXmlParser converts a string into an XmlDocument. The string should be a 
    /// representation of a LoXml document. The class will then try to validate certain aspects of it. 
    /// It can be overwritten to add more validation and enable/disable sections of the document.
    /// </summary>
    public class LoXmlInputParser
    {
        /// <summary>
        /// A list of warnings encountered with the LoXml document.
        /// </summary>
        private LinkedList<string> warnings;

        /// <summary>
        /// A list of errors encountered with the LoXml document.
        /// </summary>
        private LinkedList<string> errors;

        /// <summary>
        /// The XML representation of the input.
        /// </summary>
        private XmlDocument document;

        /// <summary>
        /// The input representing LoXml.
        /// </summary>
        private string xmlContent;
        
        /// <summary>
        /// A value indicating whether the given xml has a valid root tag.
        /// </summary>
        private bool? isValidLoXmlFormat; 

        /// <summary>
        /// Initializes a new instance of the <see cref="LoXmlInputParser"/> class.
        /// </summary>
        /// <param name="xml">The LoXml input string.</param>
        public LoXmlInputParser(string xml)
        {
            this.xmlContent = xml;
            this.warnings = new LinkedList<string>();
            this.errors = new LinkedList<string>();
        }

        /// <summary>
        /// Returns the LoXml XmlDocument that passed all validation checks.
        /// </summary>
        /// <returns>Returns a XmlDocument with only valid LoXml content. Throw XmlException if it cannot parse the xml.</returns>
        public XmlDocument GetValidatedDocument()
        {
            if (!this.isValidLoXmlFormat.HasValue)
            {
                this.ValidateDocument();
            }

            return this.document;
        }

        /// <summary>
        /// Gets a list of warnings that were encountered when validating the document.
        /// </summary>
        /// <returns>The set of warnings associated to this LoXml.</returns>
        public IEnumerable<string> GetWarnings()
        {
            return this.warnings;
        }

        /// <summary>
        /// Gets the errors that were encountered when validating the document.
        /// </summary>
        /// <returns>The errors that were encountered when validating the document.</returns>
        public IEnumerable<string> GetErrors()
        {
            return this.errors;
        }

        /// <summary>
        /// Adds a warning to the set of warnings encountered when validating the document.
        /// </summary>
        /// <param name="message">The warning to add.</param>
        protected void AddWarning(string message)
        {
            this.warnings.AddLast(message);
        }

        /// <summary>
        /// Adds a warning to the set of warnings encountered when validating the document.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="values">The format string parameters.</param>
        protected void AddWarning(string format, params object[] values)
        {
            string message = string.Format(format, values);
            this.AddWarning(message);
        }

        /// <summary>
        /// Adds an error to the list of errors encountering when validating the document.
        /// </summary>
        /// <param name="error">The error to add.</param>
        protected void AddError(string error)
        {
            this.errors.AddLast(error);
        }

        /// <summary>
        /// Gets a value indicating whether the given XmlNode represents a valid loan or application
        /// field.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        /// <returns>A value indicating whether the element is a valid field.</returns>
        protected virtual bool IsValidLoanOrAppField(XmlNode element)
        {
            // We should check PageDataUtilities however LoXml can contains fields that are not used by page data utilities.
            return true;
        }

        /// <summary>
        /// Gets a value indicating whether the LoXml document can specify
        /// credit ordering.
        /// </summary>
        /// <param name="element">The element to validate. Can be xml attribute or element.</param>
        /// <returns>A value indicating whether credit can be ordered.</returns>
        protected virtual bool CanOrderCredit(XmlNode element)
        {
            return true;
        }

        /// <summary>
        /// Gets a value indicating whether the LoXml document can specify collection updates.
        /// </summary>
        /// <param name="element">The element that contains the collection.</param>
        /// <returns>A value indicating whether the given collection can be updated.</returns>
        protected virtual bool CanUpdateCollection(XmlNode element)
        {
            return true;
        }

        /// <summary>
        /// Runs the validation checks on the XmlDocument. 
        /// </summary>
        private void ValidateDocument()
        {
            this.document = Tools.CreateXmlDoc(this.xmlContent);
            this.isValidLoXmlFormat = true;

            XmlElement root = (XmlElement)this.document.SelectSingleNode("//LOXmlFormat");
            if (root == null)
            {
                throw new XmlException("Missing LOXmlFormat root element.");
            }

            string version = root.GetAttribute("version");
            if (version != LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion)
            {
                this.AddWarning("Mismatch version. Expected version= {0}", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion);
            }

            var checks = new[]
            {
                new { XPath = "//loan/field", Validate = new Func<XmlNode, bool>((s) => this.IsValidLoanOrAppField(s)) },
                new { XPath = "//loan/applicant/field", Validate = new Func<XmlNode, bool>((s) => this.IsValidLoanOrAppField(s)) },
                new { XPath = "//loan/applicant/credit", Validate = new Func<XmlNode, bool>((s) => this.CanOrderCredit(s)) },
                new { XPath = "//loan/applicant/credit_mismo", Validate = new Func<XmlNode, bool>((s) => this.CanOrderCredit(s)) },
                new { XPath = "//loan/applicant/collection", Validate = new Func<XmlNode, bool>((s) => this.CanUpdateCollection(s)) },
                new { XPath = "//loan/collection", Validate = new Func<XmlNode, bool>((s) => this.CanUpdateCollection(s)) },
                new { XPath = "//loan/applicant/@mcl_instant_view_id", Validate = new Func<XmlNode, bool>((s) => this.CanOrderCredit(s)) },
                new { XPath = "//loan/applicant/@mcl_report_id", Validate = new Func<XmlNode, bool>((s) => this.CanOrderCredit(s)) },
            };

            var nodesToRemove = new LinkedList<XmlNode>();

            foreach (var check in checks)
            {
                foreach (XmlNode node in this.document.SelectNodes(check.XPath))
                {
                    if (!check.Validate(node))
                    {
                        nodesToRemove.AddFirst(node);
                    }
                }
            }

            foreach (XmlNode node in nodesToRemove)
            {
                if (node is XmlElement)
                {
                    node.ParentNode.RemoveChild(node);
                }
                else if (node is XmlAttribute)
                {
                    var attribute = (XmlAttribute)node;
                    attribute.OwnerElement.Attributes.Remove(attribute);
                }
                else
                {
                    throw CBaseException.GenericException("Unhandled node type.");
                }
            }
        }
    }
}
