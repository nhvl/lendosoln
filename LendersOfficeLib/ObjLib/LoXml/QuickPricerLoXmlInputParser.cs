﻿namespace LendersOffice.ObjLib.LoXml
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    /// <summary>
    /// A validator for QuickPricer LoXml documents.
    /// </summary>
    public class QuickPricerLoXmlInputParser : LoXmlInputParser
    {
        /// <summary>
        /// A set of blocked fields.
        /// </summary>
        private HashSet<string> blockedFields;

        /// <summary>
        /// A value indicating whether a credit request was contained within the document.
        /// </summary>
        private bool hasCreditRequest;

        /// <summary>
        /// Borrower paid originator compensation fields. These may be restricted.
        /// </summary>
        private string[] borrowerPaidCompFields;

        /// <summary>
        /// The originator compensation settings.
        /// </summary>
        private OriginatorCompensationSettings originatorCompensationSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickPricerLoXmlInputParser"/> class.
        /// </summary>
        /// <param name="xml">The LoXml input string.</param>
        /// <param name="originatorCompensationSettings">The originator compensation settings.</param>
        public QuickPricerLoXmlInputParser(string xml, OriginatorCompensationSettings originatorCompensationSettings) : base(xml)
        {
            this.originatorCompensationSettings = originatorCompensationSettings;

            this.blockedFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                "breakratelock",
                "suspendratelock",
                "breakinvestorratelock",
                "suspendinvestorratelock",
                "updatestatusofarchiveinunknownstatus",
                "updatependingloanestimatearchivestatus",
                "semployeeprocessorexternallogin",
                "semployeecreditauditorlogin",
                "semployeesecondarylogin",
                "semployeeloanofficerassistantlogin",
                "semployeejuniorprocessorlogin",
                "semployeejuniorunderwriterlogin",
                "semployeecollateralagentlogin",
                "semployeedisclosuredesklogin",
                "semployeelegalauditorlogin",
                "semployeepostcloserlogin",
                "semployeepostcloserexternallogin",
                "semployeeqccompliancelogin",
                "semployeepurchaserlogin",
                "semployeeservicinglogin",
                "semployeecloserlogin",
                "semployeesecondaryexternallogin",
                "semployeemanagerlogin",
                "semployeerealestateagentlogin",
                "semployeeunderwriterlogin",
                "semployeelockdesklogin",
                "semployeeprocessorlogin",
                "semployeecallcenteragentlogin",
                "semployeeloanopenerlogin",
                "semployeelenderaccexeclogin",
                "semployeeshipperlogin",
                "semployeefunderlogin",
                "semployeedocdrawerlogin",
                "semployeeinsuringlogin"
            };

            this.borrowerPaidCompFields = new[]
            {
                "sOriginatorCompensationBorrPaidPc",
                "sOriginatorCompensationBorrPaidBaseT",
                "sOriginatorCompensationBorrPaidMb"
            };
        }

        /// <summary>
        /// Gets a value indicating whether the LoXml input can contain credit request.
        /// This is hardcoded to false for quick pricer loans.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        /// <returns>A false value.</returns>
        protected override bool CanOrderCredit(XmlNode element)
        {
            if (!this.hasCreditRequest)
            {
                this.AddError("Credit request is not for use with the quick pricer.");
                this.hasCreditRequest = true;
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether the LoXml input can contain update collection commands. 
        /// </summary>
        /// <param name="element">The element to validate.</param>
        /// <returns>Returns true for closing cost related collections otherwise false.</returns>
        protected override bool CanUpdateCollection(XmlNode element)
        {
            if (element.NodeType == XmlNodeType.Element)
            {
                XmlElement el = (XmlElement)element;
                string id = el.GetAttribute("id").Trim().ToLower();

                if (id != "sclosingcostset" && id != "ssellerresponsibleclosingcostset" && id != "sselectedproductcodefilter")
                {
                    this.AddError($"Collection {id} is not recognized or is not for use with the quick pricer.");
                    return false;
                }
            }

            return base.CanUpdateCollection(element);
        }

        /// <summary>
        /// Gets a value indicating whether the LoXml input can contain the given field.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        /// <returns>True unless the field is in the blocked set list.</returns>
        protected override bool IsValidLoanOrAppField(XmlNode element)
        {
            if (element.NodeType == XmlNodeType.Element)
            {
                XmlElement el = (XmlElement)element;
                string id = el.GetAttribute("id").Trim();
                if (!string.IsNullOrEmpty(id))
                {
                    var fieldAlwaysBlocked = this.blockedFields.Contains(id);
                    var allowUpdatingCompSource = this.originatorCompensationSettings.AllowUpdatingSource;
                    var updatingCompSource = string.Equals(id, nameof(DataAccess.CPageBase.sOriginatorCompensationPaymentSourceT), StringComparison.OrdinalIgnoreCase);
                    var compIsBorrowerPaid = this.originatorCompensationSettings.CurrentSource == DataAccess.E_sOriginatorCompensationPaymentSourceT.BorrowerPaid;
                    var updatingBorrowerPaidCompFields = this.borrowerPaidCompFields.Contains(id, StringComparer.OrdinalIgnoreCase);

                    if (fieldAlwaysBlocked
                        || (!allowUpdatingCompSource && updatingCompSource)
                        || (!allowUpdatingCompSource && !compIsBorrowerPaid && updatingBorrowerPaidCompFields))
                    {
                        this.AddError($"Field {id} is not for use with the quick pricer.");
                        return false;
                    }
                }
            }

            return base.IsValidLoanOrAppField(element);
        }

        /// <summary>
        /// Contains the originator compensation data points that will affect QP2 file validation.
        /// </summary>
        public class OriginatorCompensationSettings
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="OriginatorCompensationSettings"/> class.
            /// </summary>
            /// <param name="allowUpdatingSource">A value indicating whether the compensation source can be changed.</param>
            /// <param name="currentSource">The current compensation source from the loan file.</param>
            public OriginatorCompensationSettings(bool allowUpdatingSource, DataAccess.E_sOriginatorCompensationPaymentSourceT currentSource)
            {
                this.AllowUpdatingSource = allowUpdatingSource;
                this.CurrentSource = currentSource;
            }

            /// <summary>
            /// Gets a value indicating whether the originator compensation source can be changed.
            /// </summary>
            /// <value>A value indicating whether the originator compensation source can be changed.</value>
            public bool AllowUpdatingSource { get; }

            /// <summary>
            /// Gets the compensation source for the loan.
            /// </summary>
            /// <value>The compensation source for the loan.</value>
            public DataAccess.E_sOriginatorCompensationPaymentSourceT CurrentSource { get; }
        }
    }
}
