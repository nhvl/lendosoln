﻿namespace LendersOffice.ObjLib.LoXml
{
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Exception for LoXml validation errors.
    /// </summary>
    public class InvalidLoXmlException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidLoXmlException"/> class.
        /// </summary>
        /// <param name="errors">The errors.</param>
        public InvalidLoXmlException(IEnumerable<string> errors)
            : base("Invalid LoXml.", "The LoXml contained invalid fields.")
        {
            this.Errors = errors;
        }

        /// <summary>
        /// Gets the errors from parsing the LoXml.
        /// </summary>
        /// <value>The errors from parsing the LoXml.</value>
        public IEnumerable<string> Errors { get; }
    }
}
