﻿// <copyright file="PricingGroupAssociationItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is PricingGroupAssociationItem class.
//      Author: David Dao
//      Date: 04-06-2015
// </summary>
namespace LendersOffice.ObjLib.PriceGroups
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using Admin;
    using DataAccess;
    using RatePrice.FileBasedPricing;

    /// <summary>
    /// This class represents price group association using data from LOAN_PROGRAM_TEMPLATE and LPE_PRICE_GROUP table.
    /// </summary>
    public class PricingGroupAssociationItem
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="PricingGroupAssociationItem" /> class from being created.
        /// </summary>
        private PricingGroupAssociationItem()
        {
        }

        /// <summary>
        /// Gets the id of loan program.
        /// </summary>
        /// <value>The id of loan program.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public Guid lLpTemplateId { get; private set; }

        /// <summary>
        /// Gets the descriptive name of loan program.
        /// </summary>
        /// <value>The descriptive name of loan program.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string lLpTemplateNm { get; private set; }

        /// <summary>
        /// Gets the lien position of loan program.
        /// </summary>
        /// <value>Lien position of loan program.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public E_sLienPosT lLienPosT { get; private set; }

        /// <summary>
        /// Gets the product code.
        /// </summary>
        /// <value>The product code.</value>
        public string ProductCode { get; private set; }

        /// <summary>
        /// Gets the associated loan type.
        /// </summary>
        /// <value>The associated loan type.</value>
        public E_sLT LoanType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the investor name of loan program.
        /// </summary>
        /// <value>The investor name of loan program.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string lLpInvestorNm { get; private set; }

        /// <summary>
        /// Gets the <code>data trac</code> program id.
        /// </summary>
        /// <value>The <code>data trac</code> program id.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string lDataTracLpId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this entry belong to price group.
        /// </summary>
        /// <value>This entry belong to price group.</value>
        public bool HasPriceGroupInformation { get; private set; }

        /// <summary>
        /// Gets the price group id.
        /// </summary>
        /// <value>The price group id.</value>
        public Guid? LpePriceGroupId { get; private set; }

        /// <summary>
        /// Gets the profit margin in price group.
        /// </summary>
        /// <value>The profit margin in price group.</value>
        public decimal? LenderProfitMargin { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the max <code>ysp</code> is locked.
        /// </summary>
        /// <value>The max <code>ysp</code> is locked.</value>
        public bool? LenderMaxYspAdjustLckd { get; private set; }

        /// <summary>
        /// Gets the max <code>ysp</code> adjust value in price group.
        /// </summary>
        /// <value>The max <code>ysp</code> adjust value in price group.</value>
        public decimal? LenderMaxYspAdjust { get; private set; }

        /// <summary>
        /// Gets the rate margin in price group.
        /// </summary>
        /// <value>The rate margin in price group.</value>
        public decimal? LenderRateMargin { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the program is include in price group.
        /// </summary>
        /// <value>Whether the program is include in price group.</value>
        public bool? IsValid { get; private set; }

        /// <summary>
        /// Gets the margin in price group.
        /// </summary>
        /// <value>The margin in price group.</value>
        public decimal? LenderMarginMargin { get; private set; }

        /// <summary>
        /// Gets the rate sheet file id.
        /// </summary>
        /// <value>The rate sheet file id.</value>
        public string LpeAcceptableRsFileId { get; private set; }

        /// <summary>
        /// Gets the rate sheet file version number.
        /// </summary>
        /// <value>The rate sheet file version number.</value>
        public long? LpeAcceptableRsFileVersionNumber { get; private set; }

        /// <summary>
        /// Gets or sets the rate sheet download start date.
        /// </summary>
        /// <value>The rate sheet download start date.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string lRateSheetDownloadStartD { get; set; }

        /// <summary>
        /// Gets or sets the rate sheet download end date.
        /// </summary>
        /// <value>The rate sheet download end date.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string lRateSheetDownloadEndD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether item is hide from result.
        /// </summary>
        /// <value>Indicating whether item is hide from result.</value>
        public bool IsHiddenFromResult { get; set; }

        /// <summary>
        /// Gets or sets the block message display to user.
        /// </summary>
        /// <value>The block message display to user.</value>
        public string BlockMessage { get; set; }

        /// <summary>
        /// Return a list of price group association settings.
        /// </summary>
        /// <param name="broker">The broker object.</param>
        /// <param name="lpePriceGroupId">Price group id.</param>
        /// <returns>List of price group association.</returns>
        public static IEnumerable<PricingGroupAssociationItem> ListPricingGroupAssociation(BrokerDB broker, Guid lpePriceGroupId)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                // Use pricing from LPE database.
                return ListPricingGroupAssociation(broker.BrokerID, lpePriceGroupId);
            }
            else
            {
                return ListPricingGroupAssociationByActualPricingBroker(broker, lpePriceGroupId);
            }
        }

        /// <summary>
        /// Return a list of price group association settings.
        /// </summary>
        /// <param name="brokerId">Broker id of the price group.</param>
        /// <param name="lpePriceGroupId">The id of the price group.</param>
        /// <returns>List of price group association settings.</returns>
        private static IEnumerable<PricingGroupAssociationItem> ListPricingGroupAssociation(Guid brokerId, Guid lpePriceGroupId)
        {
            List<PricingGroupAssociationItem> list = new List<PricingGroupAssociationItem>();

            Dictionary<Guid, PriceGroupProduct> dictionary = new Dictionary<Guid, PriceGroupProduct>();

            if (lpePriceGroupId != Guid.Empty)
            {
                foreach (var product in PriceGroupProduct.ListByPriceGroupId(brokerId, lpePriceGroupId))
                {
                    dictionary.Add(product.LpTemplateId, product);
                }
            }

            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "LOAN_PROGRAM_TEMPLATE_ListForPriceGroupAssociationByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    PricingGroupAssociationItem item = new PricingGroupAssociationItem();

                    item.lLpTemplateId = (Guid)reader["lLpTemplateId"];
                    item.lLpTemplateNm = (string)reader["lLpTemplateNm"];
                    item.lLpInvestorNm = (string)reader["lLpInvestorNm"];
                    item.lLienPosT = (E_sLienPosT)(int)reader["lLienPosT"];
                    item.ProductCode = (string)reader["ProductCode"];
                    item.LoanType = (E_sLT)(int)reader["lLT"];
                    item.lLpInvestorNm = (string)reader["lLpInvestorNm"];
                    item.lDataTracLpId = (string)reader["lDataTracLpId"];
                    item.LpeAcceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                    item.LpeAcceptableRsFileVersionNumber = (long)reader["LpeAcceptableRsFileVersionNumber"];

                    PriceGroupProduct priceGroupProduct = null;
                    if (dictionary.TryGetValue(item.lLpTemplateId, out priceGroupProduct))
                    {
                        item.HasPriceGroupInformation = true;
                        item.LpePriceGroupId = priceGroupProduct.LpePriceGroupId;
                        item.LenderProfitMargin = priceGroupProduct.LenderProfitMargin;
                        item.LenderMaxYspAdjustLckd = priceGroupProduct.LenderMaxYspAdjustLckd;
                        item.LenderMaxYspAdjust = priceGroupProduct.LenderMaxYspAdjust;
                        item.LenderRateMargin = priceGroupProduct.LenderRateMargin;
                        item.IsValid = priceGroupProduct.IsValid;
                        item.LenderMarginMargin = priceGroupProduct.LenderMarginMargin;
                    }

                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of price group association settings.
        /// </summary>
        /// <param name="broker">Broker of the price group.</param>
        /// <param name="lpePriceGroupId">The id of the price group.</param>
        /// <returns>List of price group association settings.</returns>
        private static IEnumerable<PricingGroupAssociationItem> ListPricingGroupAssociationByActualPricingBroker(BrokerDB broker, Guid lpePriceGroupId)
        {
            List<PricingGroupAssociationItem> list = new List<PricingGroupAssociationItem>();

            Dictionary<Guid, PriceGroupProduct> dictionary = new Dictionary<Guid, PriceGroupProduct>();

            if (lpePriceGroupId != Guid.Empty)
            {
                foreach (var product in PriceGroupProduct.ListByPriceGroupId(broker.BrokerID, lpePriceGroupId))
                {
                    dictionary.Add(product.LpTemplateId, product);
                }
            }

            FileBasedSnapshot snapshot = broker.RetrieveLatestManualImport();

            // Loop through snapshot to find all loan program by brokerid.
            var loanProgramTemplateList = snapshot.GetLoanProgramTemplateByBrokerId(broker.ActualPricingBrokerId);

            foreach (var loanProgramTemplate in loanProgramTemplateList)
            {
                if (loanProgramTemplate.IsEnabled == false || loanProgramTemplate.IsMaster == true || loanProgramTemplate.IsLpe == false)
                {
                    continue;
                }

                PricingGroupAssociationItem item = new PricingGroupAssociationItem();

                item.lLpTemplateId = loanProgramTemplate.lLpTemplateId;
                item.lLpTemplateNm = loanProgramTemplate.lLpTemplateNm;
                item.lLpInvestorNm = loanProgramTemplate.lLpInvestorNm;
                item.lLienPosT = loanProgramTemplate.lLienPosT;
                item.ProductCode = loanProgramTemplate.ProductCode;
                item.LoanType = loanProgramTemplate.lLT;
                item.lLpInvestorNm = loanProgramTemplate.lLpInvestorNm;

                FileBasedRateOptions rateOptions = null;
                if (snapshot.TryGetRateOptions(loanProgramTemplate.SrcRateOptionsProgId, out rateOptions))
                {
                    item.LpeAcceptableRsFileId = rateOptions.LpeAcceptableRsFileId;
                    item.LpeAcceptableRsFileVersionNumber = rateOptions.LpeAcceptableRsFileVersionNumber;
                    if (!rateOptions.lRateSheetDownloadStartD.Date.Equals(new DateTime(2000, 1, 1)))
                    {
                        item.lRateSheetDownloadStartD = rateOptions.lRateSheetDownloadStartD.ToString();
                    }
                }

                PriceGroupProduct priceGroupProduct = null;
                if (dictionary.TryGetValue(item.lLpTemplateId, out priceGroupProduct))
                {
                    item.HasPriceGroupInformation = true;
                    item.LpePriceGroupId = priceGroupProduct.LpePriceGroupId;
                    item.LenderProfitMargin = priceGroupProduct.LenderProfitMargin;
                    item.LenderMaxYspAdjustLckd = priceGroupProduct.LenderMaxYspAdjustLckd;
                    item.LenderMaxYspAdjust = priceGroupProduct.LenderMaxYspAdjust;
                    item.LenderRateMargin = priceGroupProduct.LenderRateMargin;
                    item.IsValid = priceGroupProduct.IsValid;
                    item.LenderMarginMargin = priceGroupProduct.LenderMarginMargin;
                }

                list.Add(item);
            }

            return list;
        }
    }
}
