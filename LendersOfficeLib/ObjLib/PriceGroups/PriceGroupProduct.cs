﻿// <copyright file="PriceGroupProduct.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/24/2014 3:47:53 PM 
// </summary>
namespace LendersOffice.ObjLib.PriceGroups
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// Represent a record of LPE_PRICE_GROUP_PRODUCT.
    /// </summary>
    public class PriceGroupProduct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PriceGroupProduct" /> class.
        /// </summary>
        /// <param name="reader">Sql Data Reader object.</param>
        private PriceGroupProduct(DbDataReader reader)
        {
            this.LpePriceGroupId = (Guid)reader["LpePriceGroupId"];
            this.LpTemplateId = (Guid)reader["lLpTemplateId"];
            this.LenderProfitMargin = (decimal)reader["LenderProfitMargin"];
            this.LenderMaxYspAdjustLckd = (bool)reader["LenderMaxYspAdjustLckd"];
            this.LenderMaxYspAdjust = (decimal)reader["LenderMaxYspAdjust"];
            this.LenderRateMargin = (decimal)reader["LenderRateMargin"];
            this.IsValid = (bool)reader["IsValid"];
            this.LenderMarginMargin = (decimal)reader["LenderMarginMargin"];
        }

        /// <summary>
        /// Gets the price group id.
        /// </summary>
        /// <value>The price group id.</value>
        public Guid LpePriceGroupId { get; private set; }

        /// <summary>
        /// Gets the loan program template id.
        /// </summary>
        /// <value>The loan program template id.</value>
        public Guid LpTemplateId { get; private set; }

        /// <summary>
        /// Gets the lender profit margin.
        /// </summary>
        /// <value>The lender profit margin.</value>
        public decimal LenderProfitMargin { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the lender max YSP adjust overridable.
        /// </summary>
        /// <value>The lender max YSP adjust overridable.</value>
        public bool LenderMaxYspAdjustLckd { get; private set; }

        /// <summary>
        /// Gets the lender max YSP adjustment.
        /// </summary>
        /// <value>The lender max YSP adjustment.</value>
        public decimal LenderMaxYspAdjust { get; private set; }

        /// <summary>
        /// Gets the lender rate margin.
        /// </summary>
        /// <value>The lender rate margin.</value>
        public decimal LenderRateMargin { get; private set; }

        /// <summary>
        /// Gets the lender margin margin.
        /// </summary>
        /// <value>The lender margin margin.</value>
        public decimal LenderMarginMargin { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the loan program template is attached to price group.
        /// </summary>
        /// <value>Whether the loan program template is attached to price group.</value>
        public bool IsValid { get; private set; }

        /// <summary>
        /// List all the programs settings in the price group.
        /// </summary>
        /// <param name="brokerId">A broker id.</param>
        /// <param name="lpePriceGroupId">A price group id.</param>
        /// <returns>The list of programs settings in the price group.</returns>
        public static IEnumerable<PriceGroupProduct> ListByPriceGroupId(Guid brokerId, Guid lpePriceGroupId)
        {
            IEnumerable<PriceGroupProduct> list = null;
            using (CStoredProcedureExec transaction = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    transaction.BeginTransactionForWrite();

                    list = ListByPriceGroupId(transaction, brokerId, lpePriceGroupId);
                    transaction.CommitTransaction();
                }
                catch
                {
                    transaction.RollbackTransaction();
                    throw;
                }
            }

            return list;
        }

        /// <summary>
        /// List all the programs settings in the price group.
        /// </summary>
        /// <param name="transaction">A SQL transaction to run query.</param>
        /// <param name="brokerId">A broker id.</param>
        /// <param name="lpePriceGroupId">A price group id.</param>
        /// <returns>The list of programs settings in the price group.</returns>
        internal static IEnumerable<PriceGroupProduct> ListByPriceGroupId(CStoredProcedureExec transaction, Guid brokerId, Guid lpePriceGroupId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LpePriceGroupId", lpePriceGroupId)
            };

            List<PriceGroupProduct> list = new List<PriceGroupProduct>();

            using (DbDataReader reader = transaction.ExecuteReader("LPE_PRICE_GROUP_PRODUCT_ListByPriceGroupId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new PriceGroupProduct(reader));
                }
            }

            return list;
        }
    }
}