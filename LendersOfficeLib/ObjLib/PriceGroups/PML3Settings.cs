﻿namespace LendersOffice.ObjLib.PriceGroups
{    
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Settings specific to enabling PML 3.
    /// </summary>
    public class PML3Settings
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Boolean bit that will force the user to user PML 3.
        /// </summary>
        private bool forcePML3 = false;

        /// <summary>
        /// String form of the bit to force PML 3.
        /// </summary>
        private string forcePML3String;

        /// <summary>
        /// The employee group name that is enabled, if it is restricted to a specific group.
        /// </summary>
        private string enabledEmployeeGroupName;

        /// <summary>
        /// The OC group name that is enabled, if it is restricted to a specific group.
        /// </summary>
        private string enabledOCGroupName;

        /// <summary>
        /// The boolean that determines if this bit exists in the temp options.
        /// </summary>
        private bool enablePml3TempOptionExists;

        /// <summary>
        /// Initializes a new instance of the <see cref="PML3Settings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The match from the lender's temp option XML content.</param>
        public PML3Settings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.enablePml3TempOptionExists = true;

                this.enabledEmployeeGroupName = match.Groups["EmployeeGroup"].Value;
                this.enabledOCGroupName = match.Groups["OCGroup"].Value;                                
                this.forcePML3String = match.Groups["ForcePML3"].Value;
                bool.TryParse(this.forcePML3String, out this.forcePML3);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the PML 3.0 UI is enabled for all employee groups.
        /// </summary>
        /// <value>True if enabled for all employee groups. Otherwise, false.</value>
        public bool IsUnconditionallyEnabled
        {
            get
            {
                return this.enablePml3TempOptionExists && this.forcePML3;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the PML 3.0 UI bit exits in Broker editor temp options.
        /// </summary>
        /// <value>True if the bit exists as a temp option. False otherwise.</value>
        public bool EnablePml3TempOptionExists
        {
            get
            {
                return this.enablePml3TempOptionExists;
            }
        }

        /// <summary>
        /// Determines if the updated internal pricer UI is enabled for the given principal.
        /// </summary>
        /// <param name="principal">The principal to evaluate.</param>
        /// <returns>True if the user has access. Otherwise, false.</returns>
        public bool IsEnabledForCurrentUser(AbstractUserPrincipal principal)
        {
            if (!this.enablePml3TempOptionExists)
            {
                return false;
            }

            if (this.forcePML3)
            {
                return true;
            }

            bool blankEmployeeName = string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName);
            bool blankOCName = string.IsNullOrWhiteSpace(this.enabledOCGroupName);                        

            if (principal.Type == "B")
            {
                if (blankEmployeeName)
                {
                    return true;
                }

                if (this.enabledEmployeeGroupName == "EmptyGroup")
                {
                    return false;
                }

                return principal.IsInEmployeeGroup(this.enabledEmployeeGroupName);
            }
            else
            {
                if (blankOCName)
                {
                    return true;
                }

                if (this.enabledOCGroupName == "EmptyGroup")
                {
                    return false;
                }

                return principal.IsInPmlBrokerGroup(this.enabledOCGroupName);
            }            
        }

        /// <summary>
        /// Validates that the employee group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateEmployeeGroup()
        {
            return !this.enablePml3TempOptionExists 
                || string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName)
                || this.enabledEmployeeGroupName == "EmptyGroup"
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.Employee, this.enabledEmployeeGroupName);
        }

        /// <summary>
        /// Validates that the OC group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateOCGroup()
        {
            return !this.enablePml3TempOptionExists 
                || string.IsNullOrWhiteSpace(this.enabledOCGroupName)
                || this.enabledOCGroupName == "EmptyGroup"
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.PmlBroker, this.enabledOCGroupName);
        }

        /// <summary>
        /// Validates the force PML 3 bit, if it exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateForcePML3()
        {
            return !this.enablePml3TempOptionExists || bool.TryParse(this.forcePML3String, out this.forcePML3);
        }
    }    
}
