﻿namespace LendersOffice.ObjLib.PriceGroups.Model
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Represents individual row in LPE_PRICE_GROUP_PRODUCT.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public sealed class LpePriceGroup
    {
        /// <summary>
        /// Gets or sets price group id.
        /// </summary>
        /// <value>Price group id.</value>
        public Guid LpePriceGroupId { get; set; }

        /// <summary>
        /// Gets or sets the actual price group id that will be use to search for price policy attachment.
        /// </summary>
        /// <value>The actual price group id that will be use to search for price policy attachment.</value>
        public Guid ActualPriceGroupId { get; set; }

        /// <summary>
        /// Gets or sets broker id.
        /// </summary>
        /// <value>Broker Id.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether price group is enabled.
        /// </summary>
        /// <value>Whether price group is enabled.</value>
        public bool ExternalPriceGroupEnabled { get; set; }

        /// <summary>
        /// Gets or sets price group name.
        /// </summary>
        /// <value>Price group name.</value>
        public string LpePriceGroupName { get; set; }

        /// <summary>
        /// Gets or sets lender paid compensation option.
        /// </summary>
        /// <value>Lender paid compensation option.</value>
        public E_LenderPaidOriginatorCompensationOptionT LenderPaidOriginatorCompensationOptionT { get; set; }

        /// <summary>
        /// Gets or sets the lock policy id.
        /// </summary>
        /// <value>Lock policy id.</value>
        public Guid LockPolicyId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether display pml fee in 100 format.
        /// </summary>
        /// <value>Wehther display fee in 100 format.</value>
        public bool DisplayPmlFeeIn100Format { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to round up lpe fee.
        /// </summary>
        /// <value>Whether to round up lpe fee.</value>
        public bool IsRoundUpLpeFee { get; set; }

        /// <summary>
        /// Gets or sets lpe fee round up interval.
        /// </summary>
        /// <value>Lpe fee round up interval.</value>
        public decimal RoundUpLpeFeeToInterval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to always display an exact par rate option.
        /// </summary>
        /// <value>Whether to always display an exact par rate option.</value>
        public bool IsAlwaysDisplayExactParRateOption { get; set; }

        /// <summary>
        /// Gets or sets a list of product's settings in price group.
        /// </summary>
        /// <value>List of product's settings in price group.</value>
        public List<LpePriceGroupProduct> ProductList { get; set; }
    }
}