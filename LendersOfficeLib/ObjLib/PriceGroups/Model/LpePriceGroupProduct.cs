﻿namespace LendersOffice.ObjLib.PriceGroups.Model
{
    using System;

    /// <summary>
    /// Represents individual row in LPE_PRICE_GROUP_PRODUCT.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public sealed class LpePriceGroupProduct
    {
        /// <summary>
        /// Gets or sets a value indicating whether it is valid.
        /// </summary>
        /// <value>Whether entry is valid.</value>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets program template id.
        /// </summary>
        /// <value>Program template id.</value>
        public Guid LoanProgramTemplateId { get; set; }

        /// <summary>
        /// Gets or sets lender profit margin.
        /// </summary>
        /// <value>Lender profit margin.</value>
        public decimal LenderProfitMargin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether lock calculation of max ysp.
        /// </summary>
        /// <value>Lock calculation of max ysp.</value>
        public bool LenderMaxYspAdjustLckd { get; set; }

        /// <summary>
        /// Gets or sets lender max ysp.
        /// </summary>
        /// <value>Lender max ysp.</value>
        public decimal LenderMaxYspAdjust { get; set; }

        /// <summary>
        /// Gets or sets the margin of rate.
        /// </summary>
        /// <value>Margin of rate.</value>
        public decimal LenderRateMargin { get; set; }

        /// <summary>
        /// Gets or sets the margin of margin.
        /// </summary>
        /// <value>Margin of margin.</value>
        public decimal LenderMarginMargin { get; set; }
    }
}