﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.ObjLib.PriceGroups.Model;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.PriceGroups
{
    public class PriceGroup
    {
        private bool isNewRecord = false;

        public Guid ID { get; private set; }

        public Guid ActualPriceGroupId { get; set; }
        public Guid BrokerID { get; private set; }
        public bool ExternalPriceGroupEnabled { get; set; }
        public E_LenderPaidOriginatorCompensationOptionT LenderPaidOriginatorCompensationOptionT { get; set; }
        public string Name { get; set; }
        public Guid? LockPolicyID { get; set; } // should be non-nullable after migration
        public bool DisplayPmlFeeIn100Format { get; set; }
        public bool IsRoundUpLpeFee { get; set; }
        public decimal RoundUpLpeFeeToInterval { get; set; }
        public bool IsAlwaysDisplayExactParRateOption { get; set; }

        public SHA256Checksum ContentKey { get; private set; }
        private PriceGroup()
        {
        }

        private PriceGroup(IDataReader reader)
        {
            this.isNewRecord = false;

            this.BrokerID = (Guid)reader["BrokerId"];
            this.ActualPriceGroupId = (Guid)reader["ActualPriceGroupId"];
            this.ID = (Guid)reader["LpePriceGroupId"];
            this.ExternalPriceGroupEnabled = (bool)reader["ExternalPriceGroupEnabled"];
            this.Name = (string)reader["LpePriceGroupName"];
            this.LenderPaidOriginatorCompensationOptionT = (E_LenderPaidOriginatorCompensationOptionT)reader["LenderPaidOriginatorCompensationOptionT"];
            this.DisplayPmlFeeIn100Format = (bool)reader["DisplayPmlFeeIn100Format"];
            this.IsRoundUpLpeFee = (bool)reader["IsRoundUpLpeFee"];
            this.RoundUpLpeFeeToInterval = (decimal)reader["RoundUpLpeFeeToInterval"];
            this.IsAlwaysDisplayExactParRateOption = (bool)reader["IsAlwaysDisplayExactParRateOption"];

            if (DBNull.Value != reader["LockPolicyID"])
            {
                this.LockPolicyID = (Guid)reader["LockPolicyID"];
            }
            else
            {
                this.LockPolicyID = null;
            }

            this.ContentKey = SHA256Checksum.Invalid;
            if (DBNull.Value != reader["ContentKey"])
            {
                var v = SHA256Checksum.Create((string)reader["ContentKey"]);

                if (v != null)
                {
                    this.ContentKey = v.Value;
                }
            }
        }

        public void Save(AbstractUserPrincipal principal, IEnumerable<LpePriceGroupProduct> updatePriceGroupProductList)
        {
            if (principal == null)
            {
                throw CBaseException.GenericException("principal cannot be null");
            }

            bool isSystemPrincipal = principal is SystemUserPrincipal;

            if (!isSystemPrincipal && principal.BrokerId != this.BrokerID)
            {
                // If the principal is not SystemUserPrincipal then we will require brokerid to match.
                throw new AccessDenied();
            }

            using (CStoredProcedureExec transaction = new CStoredProcedureExec(this.BrokerID))
            {
                try
                {
                    transaction.BeginTransactionForWrite();

                    // Save Basic Info.
                    this.CreateOrUpdateLpePriceGroup(transaction);

                    // Update lpe price group product association.
                    this.UpdateLpePriceGroupProduct(transaction, updatePriceGroupProductList);

                    var priceGroupModel = this.GenerateLpePriceGroupModel(transaction);

                    // Generate json
                    var jsonFilePath = SerializationHelper.JsonSerializeToFile(priceGroupModel);

                    var checksum = EncryptionHelper.ComputeSHA256Hash(jsonFilePath);

                    FileDBTools.WriteFile(E_FileDB.Normal, checksum.Value, jsonFilePath.Value);

                    // Record history.
                    this.RecordRevisionHistory(transaction, principal, checksum);

                    transaction.CommitTransaction();
                }
                catch
                {
                    // 11/28/2016 - dd - During the roll back, I will not delete the filedb that already create.
                    //                   Since we use checksum as a key, the checksum may belong to previous history (assume no change made).
                    //                   Therefore delete the filedb may cause old history to be remove.
                    transaction.RollbackTransaction();
                    throw;
                }
            }
        }

        private void RecordRevisionHistory(CStoredProcedureExec transaction, AbstractUserPrincipal principal, SHA256Checksum contentKey)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerID),
                new SqlParameter("@LpePriceGroupId", this.ID),
                new SqlParameter("@ContentKey", contentKey.Value),
                new SqlParameter("@ModifiedUserId", principal.UserId),
                new SqlParameter("@ModifiedDate", DateTime.Now)
            };

            transaction.ExecuteNonQuery("LPE_PRICE_GROUP_InsertRevision", parameters);
        }
        private LpePriceGroup GenerateLpePriceGroupModel(CStoredProcedureExec transaction)
        {
            LpePriceGroup priceGroup = new LpePriceGroup();
            priceGroup.BrokerId = this.BrokerID;
            priceGroup.ActualPriceGroupId = this.ActualPriceGroupId;
            priceGroup.LpePriceGroupId = this.ID;
            priceGroup.LpePriceGroupName = this.Name;
            priceGroup.DisplayPmlFeeIn100Format = this.DisplayPmlFeeIn100Format;
            priceGroup.ExternalPriceGroupEnabled = this.ExternalPriceGroupEnabled;
            priceGroup.IsRoundUpLpeFee = this.IsRoundUpLpeFee;
            priceGroup.LenderPaidOriginatorCompensationOptionT = this.LenderPaidOriginatorCompensationOptionT;
            if (this.LockPolicyID.HasValue)
            {
                priceGroup.LockPolicyId = this.LockPolicyID.Value;
            }

            priceGroup.RoundUpLpeFeeToInterval = this.RoundUpLpeFeeToInterval;
            priceGroup.IsAlwaysDisplayExactParRateOption = this.IsAlwaysDisplayExactParRateOption;

            priceGroup.ProductList = new List<LpePriceGroupProduct>();

            foreach (var o in PriceGroupProduct.ListByPriceGroupId(transaction, this.BrokerID, this.ID))
            {
                LpePriceGroupProduct item = new LpePriceGroupProduct();
                item.LoanProgramTemplateId = o.LpTemplateId;
                item.LenderProfitMargin = o.LenderProfitMargin;
                item.LenderMaxYspAdjustLckd = o.LenderMaxYspAdjustLckd;
                item.LenderMaxYspAdjust = o.LenderMaxYspAdjust;
                item.LenderRateMargin = o.LenderRateMargin;
                item.IsValid = o.IsValid;
                item.LenderMarginMargin = o.LenderMarginMargin;

                priceGroup.ProductList.Add(item);
            }

            return priceGroup;
        }

        private void CreateOrUpdateLpePriceGroup(CStoredProcedureExec transaction)
        {
            if (!this.isNewRecord && !this.ExternalPriceGroupEnabled)
            {
                this.CheckIfDisabledPriceGroupInUse(transaction);
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@LpePriceGroupId", this.ID),
                new SqlParameter("@BrokerId", this.BrokerID),
                new SqlParameter("@ExternalPriceGroupEnabled", this.ExternalPriceGroupEnabled),
                new SqlParameter("@LpePriceGroupName", this.Name),
                new SqlParameter("@LenderPaidOriginatorCompensationOptionT", this.LenderPaidOriginatorCompensationOptionT),
                new SqlParameter("@LockPolicyID", this.LockPolicyID),
                new SqlParameter("@DisplayPmlFeeIn100Format", this.DisplayPmlFeeIn100Format),
                new SqlParameter("@IsRoundUpLpeFee", this.IsRoundUpLpeFee),
                new SqlParameter("@RoundUpLpeFeeToInterval", this.RoundUpLpeFeeToInterval),
                new SqlParameter("@IsAlwaysDisplayExactParRateOption", this.IsAlwaysDisplayExactParRateOption),
                new SqlParameter("@ActualPriceGroupId", this.ActualPriceGroupId)
            };

            string storedProcedureName = this.isNewRecord ? "CreatePricingGroupById" : "UpdatePricingGroupById";
            transaction.ExecuteNonQuery(storedProcedureName, 0, parameters);
            this.isNewRecord = false;
        }

        /// <summary>
        /// Determines whether this price group is in use at the user, branch, originating company,
        /// or TPO portal level as a default.
        /// </summary>
        /// <param name="transaction">
        /// The transaction used to retrieve data from the database.
        /// </param>
        private void CheckIfDisabledPriceGroupInUse(CStoredProcedureExec transaction)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@LpePriceGroupId", this.ID),
                new SqlParameter("@BrokerId", this.BrokerID),
            };

            var errorMessages = new List<string>(6);
            using (var reader = transaction.ExecuteReader("CheckIfDisabledPriceGroupInUse", parameters))
            {
                if (reader.Read())
                {
                    if (reader.SafeBool("IsSetForBUser"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisableBUserPriceGroup);
                    }

                    if (reader.SafeBool("IsSetForPUser"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisablePUserPriceGroup);
                    }

                    if (reader.SafeBool("IsSetForBranch"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisableBranchDefaultPriceGroup);
                    }

                    if (reader.SafeBool("IsSetForOc"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisableOriginatingCompanyPriceGroup);
                    }

                    if (reader.SafeBool("IsSetForCorporate"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisableCorporateDefaultPriceGroup);
                    }

                    if (reader.SafeBool("IsSetForTpoPortalDefault"))
                    {
                        errorMessages.Add(ErrorMessages.CannotDisableTpoPortalDefaultPriceGroup);
                    }
                }
            }

            if (errorMessages.Count > 0)
            {
                var errorMessage = string.Join(Environment.NewLine, errorMessages);
                throw new CBaseException(errorMessage, errorMessage);
            }
        }

        private void UpdateLpePriceGroupProduct(CStoredProcedureExec transaction, IEnumerable<LpePriceGroupProduct> updatePriceGroupProductList)
        {
            if (updatePriceGroupProductList == null)
            {
                return;
            }

            foreach (var item in updatePriceGroupProductList)
            {
                SqlParameter[] parameters = {
                                                        new SqlParameter("@LpePriceGroupId", this.ID),
                                                        new SqlParameter("@lLpTemplateId", item.LoanProgramTemplateId),
                                                        new SqlParameter("@LenderProfitMargin", item.LenderProfitMargin),
                                                        new SqlParameter("@LenderMaxYspAdjustLckd", item.LenderMaxYspAdjustLckd),
                                                        new SqlParameter("@LenderMaxYspAdjust", item.LenderMaxYspAdjust),
                                                        new SqlParameter("@LenderRateMargin", item.LenderRateMargin),
                                                        new SqlParameter("@LenderMarginMargin", item.LenderMarginMargin),
                                                        new SqlParameter("@IsValid", item.IsValid)
                                                    };

                transaction.ExecuteNonQuery("UpdatePriceGroupProduct", 0, parameters);
            }
        }

        public static bool IsLpePriceGroupNameAvailable(Guid brokerId, Guid lpePriceGroupId, string name)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@LpePriceGroupId", lpePriceGroupId),
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@LpePriceGroupName", name)
                                        };

            bool isAvailable = false;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IsLpePriceGroupNameExisting", parameters))
            {
                isAvailable = !reader.Read(); // Only valid if stored procedure return no result.
            }

            return isAvailable;
        }

        public static PriceGroup CreateNew(Guid brokerId)
        {
            var priceGroup = new PriceGroup();
            priceGroup.BrokerID = brokerId;
            priceGroup.ID = Guid.NewGuid();
            priceGroup.ExternalPriceGroupEnabled = true;
            priceGroup.isNewRecord = true;

            // Set default from BrokerDB.
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);
            if (broker.DefaultLockPolicyID.HasValue)
            {
                priceGroup.LockPolicyID = broker.DefaultLockPolicyID.Value;
            }

            priceGroup.DisplayPmlFeeIn100Format = broker.DisplayPmlFeeIn100Format;
            priceGroup.IsRoundUpLpeFee = broker.RoundUpLpeFee;
            priceGroup.RoundUpLpeFeeToInterval = broker.RoundUpLpeFeeInterval;
            priceGroup.LenderPaidOriginatorCompensationOptionT = E_LenderPaidOriginatorCompensationOptionT.AddOriginatorCompensationToPricing;
            priceGroup.IsAlwaysDisplayExactParRateOption = broker.IsAlwaysDisplayExactParRateOption;
            return priceGroup;
        }

        public static PriceGroup RetrieveByID(Guid priceGroupID, Guid brokerID)
        {
            PriceGroup pg = (PriceGroup)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.PriceGroupKey(priceGroupID));
            if (pg == null)
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@LpePriceGroupId", priceGroupID)
                    , new SqlParameter("@BrokerId", brokerID)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "RetrievePricingGroupById", parameters))
                {
                    if (reader.Read())
                    {
                        pg = new PriceGroup(reader);
                    }
                    else
                    {
                        throw new NotFoundException("no such pricegroup id: " + priceGroupID + " broker id: " + brokerID);
                    }
                }

                if (pg.BrokerID != brokerID || pg.ID != priceGroupID)
                {
                    // 11/22/2016 - dd - This check to make sure we did not make mistake in the store procedure retrieve.
                    throw CBaseException.GenericException("Broker Id OR Price Group Id is not match with expected value.");
                }
                CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.PriceGroupKey(priceGroupID), pg);
            }

            return pg;
        }

        public static PriceGroup RetrieveByName(string priceGroupName, Guid brokerID)
        {
            var parameters = new SqlParameter[]
                {
                    new SqlParameter("@LpePriceGroupName", priceGroupName)
                    , new SqlParameter("@BrokerId", brokerID)
                };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "RetrievePricingGroupByName", parameters))
            {
                if (reader.Read())
                {
                    return new PriceGroup(reader);
                }
                else
                {
                    throw new NotFoundException("pricegroup name not found: " + priceGroupName + " broker id: " + brokerID);
                }
            }
        }

        public static IEnumerable<PriceGroup> RetrieveAllFromBroker(Guid brokerID)
        {
            List<PriceGroup> pgs = new List<PriceGroup>();

            var parameters = new SqlParameter[]
                {
                    new SqlParameter("@BrokerId", brokerID)
                };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "RetrievePricingGroupsByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    PriceGroup pg = new PriceGroup(reader);

                    pgs.Add(pg);
                }
            }

            return pgs;
        }

        public static IDictionary<Guid, PriceGroup> ListAllSlow()
        {
            Dictionary<Guid, PriceGroup> priceGroupList = new Dictionary<Guid, PriceGroup>();

            foreach (DbConnectionInfo connectionInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "LPE_PRICE_GROUP_ListAll", null))
                {
                    while (reader.Read())
                    {
                        var brokerId = (Guid)reader["BrokerId"];

                        var brokerConnectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

                        // To prevent adding duplicate price groups, only add
                        // this price group if the owning broker uses this
                        // database.
                        if (connectionInfo.Id != brokerConnectionInfo.Id)
                        {
                            continue;
                        }

                        PriceGroup priceGroup = new PriceGroup(reader);

                        priceGroupList.Add(priceGroup.ID, priceGroup);
                    }
                }
            }

            return priceGroupList;
        }
    }
}