﻿// <copyright file="LocationEntry.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/6/2015 7:01:53 PM
// </summary>
namespace LendersOffice.ObjLib.Zipcode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents a City State Zip FIPS code location.
    /// </summary>
    public class LocationEntry 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationEntry" /> class.
        /// </summary>
        /// <param name="county">The county of the location county.</param>
        /// <param name="state">The state of the location state.</param>
        /// <param name="city">The city of the location entry.</param>
        /// <param name="zipcode">The zip code of the location entry.</param>
        /// <param name="fips">The FIPS county code.</param>
        public LocationEntry(string county, string state, string city, string zipcode, string fips)
        {
            this.County = county;
            this.State = state;
            this.City = city;
            this.Zipcode = zipcode;
            this.FipsCountyCode = fips;
        }

        /// <summary>
        /// Gets the city for this location entry.
        /// </summary>
        /// <value>The city of the location entry.</value>
        public string City { get; private set; }

        /// <summary>
        /// Gets the county.
        /// </summary>
        /// <value>The county of the location entry.</value>
        public string County { get; private set; }

        /// <summary>
        /// Gets the FIPS county code.
        /// </summary>
        /// <value>The FIPS county code.</value>
        public string FipsCountyCode { get; private set; }

        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>The state of the location entry.</value>
        public string State { get; private set; }

        /// <summary>
        /// Gets the zip code.
        /// </summary>
        /// <value>The zip code of the location entry.</value>
        public string Zipcode { get; private set; }
    }
}
