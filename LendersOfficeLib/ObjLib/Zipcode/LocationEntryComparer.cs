﻿// <copyright file="LocationEntryComparer.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/6/2015 7:17:17 PM
// </summary>
namespace LendersOffice.ObjLib.Zipcode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Compares to location entries.
    /// </summary>
    public class LocationEntryComparer : IEqualityComparer<LocationEntry>
    {
        /// <summary>
        /// Compares two location entries. 
        /// </summary>
        /// <param name="x">The first location entry.</param>
        /// <param name="y">The second location entry.</param>
        /// <returns>Whether both location entries are the same.</returns>
        public bool Equals(LocationEntry x, LocationEntry y)
        {
            bool isTheSame = x.City.Equals(y.City);
            isTheSame &= x.County.Equals(y.County);
            isTheSame &= x.FipsCountyCode.TrimStart(new char[] { '0' }).Equals(y.FipsCountyCode.TrimStart(new char[] { '0' }));
            isTheSame &= x.State.Equals(y.State);
            isTheSame &= x.Zipcode.Equals(y.Zipcode);

            return isTheSame;
        }

        /// <summary>
        /// Gets the hash code of the give location entry.
        /// </summary>
        /// <param name="obj">The location entry to get the object.</param>
        /// <returns>The hash code of the given object.</returns>
        public int GetHashCode(LocationEntry obj)
        {
            int hashCode = obj.City.GetHashCode() * 17;
            hashCode += obj.County.GetHashCode() * 17;
            hashCode += obj.FipsCountyCode.TrimStart(new char[] { '0' }).GetHashCode() * 17;
            hashCode += obj.State.GetHashCode() * 17;
            hashCode += obj.Zipcode.GetHashCode() * 17;

            return hashCode;
        }
    }
}
