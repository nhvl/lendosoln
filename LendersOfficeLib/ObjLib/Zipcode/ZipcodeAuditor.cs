﻿// <copyright file="ZipcodeAuditor.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/6/2015 6:17:23 PM
// </summary>
namespace LendersOffice.ObjLib.Zipcode
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using ClosedXML.Excel;
    using DataAccess;
    using LendersOffice.Common.TextImport;

    /// <summary>
    /// Deals with the auditing the existing zip codes and latest data.
    /// </summary>
    public class ZipcodeAuditor
    {
        /// <summary>
        /// The new data from the import file.
        /// </summary>
        private HashSet<LocationEntry> newData;

        /// <summary>
        /// The new data from the import file that is currently not in the database.
        /// </summary>
        private HashSet<LocationEntry> newDataWorkingSet;

        /// <summary>
        /// The source data from the database.
        /// </summary>
        private HashSet<LocationEntry> srcData;

        /// <summary>
        /// The matching data found in both sets.
        /// </summary>
        private HashSet<LocationEntry> matchingData;

        /// <summary>
        /// The set that will contain only the obsolete data from the source.
        /// </summary>
        private HashSet<LocationEntry> srcDataWorkingSet;

        /// <summary>
        /// Takes a CSV file and parses it for comparison.
        /// </summary>
        /// <param name="csvFilePath">The zip code CSV data file.</param>
        /// <param name="errors">List of errors ran into parsing the file.</param>
        /// <returns>Whether the file was processed correctly.</returns>
        public bool ProcessUpdateFile(string csvFilePath, out ILookup<int, string> errors)
        {
            if (!csvFilePath.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("CSV files only.");
            }
            
            DataTable table = DataParsing.FileToDataTable(csvFilePath, true, string.Empty, out errors);

            if (table == null)
            {
                return false;
            }

            DataColumn zipcode = table.Columns["ZipCode"];
            DataColumn city = table.Columns["CityMixedCase"];
            DataColumn county = table.Columns["County"];
            DataColumn state = table.Columns["State"];
            DataColumn stateFips = table.Columns["StateFIPS"];
            DataColumn countyFips = table.Columns["CountyFIPS"];

            if (zipcode == null ||
                city == null ||
                county == null ||
                state == null ||
                stateFips == null ||
                countyFips == null)
            {
                return false;
            }

            this.newData = new HashSet<LocationEntry>(new LocationEntryComparer());
            this.newDataWorkingSet = new HashSet<LocationEntry>(new LocationEntryComparer());

            string z, ci, co, s, f;

            TextInfo info = Thread.CurrentThread.CurrentCulture.TextInfo;

            foreach (DataRow row in table.Rows)
            {
                z = row[zipcode].ToString();
                ci = row[city].ToString();
                co = info.ToTitleCase(row[county].ToString().ToLower());
                s = row[state].ToString();
                f = string.Format("{0:2}{1:D3}", row[stateFips], row[countyFips]);

                LocationEntry e = new LocationEntry(co, s, ci, z, f);
                this.newData.Add(e);
                this.newDataWorkingSet.Add(e);
            }

            return true;
        }

        /// <summary>
        /// Compares the current data set with the new dataset.
        /// </summary>
        /// <returns>A path to an excel report.</returns>
        public string CompareAndGenerateReport()
        {
            this.matchingData = new HashSet<LocationEntry>(new LocationEntryComparer());

            this.PopulateCurrentZipcode();

            foreach (LocationEntry entry in this.newData)
            {
                if (this.srcData.Contains(entry))
                {
                    this.newDataWorkingSet.Remove(entry);
                    this.srcDataWorkingSet.Remove(entry);

                    this.matchingData.Add(entry);
                }
            }

            return this.GenerateXlsxReport();
        }

        /// <summary>
        /// Loads the current zip code data from the database.
        /// </summary>
        private void PopulateCurrentZipcode()
        {
            this.srcData = new HashSet<LocationEntry>(new LocationEntryComparer());
            this.srcDataWorkingSet = new HashSet<LocationEntry>(new LocationEntryComparer());

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "tmp_GetZipData"))
            {
                while (reader.Read())
                {
                    LocationEntry e = new LocationEntry(
                        reader["County"].ToString(),
                        reader["State"].ToString(),
                        reader["City"].ToString(),
                        reader["Zipcode"].ToString(),
                        reader["FipsCountyCode"].ToString());

                    this.srcDataWorkingSet.Add(e);
                    this.srcData.Add(e);
                }
            }
        }

        /// <summary>
        /// Generates a report of the zip code comparisons.
        /// </summary>
        /// <returns>A path to the XLSX report.</returns>
        private string GenerateXlsxReport()
        {
            var workbook = new XLWorkbook();
            
            IXLWorksheet[] worksheets = 
            {
                workbook.Worksheets.Add("MergeView"),
                workbook.Worksheets.Add("Obsolete Locations"),
                workbook.Worksheets.Add("New Locations"),
                workbook.Worksheets.Add("Matching Locations"), 
                workbook.Worksheets.Add("Source Data"), 
                workbook.Worksheets.Add("New Data"),
            };

            IList<LocationEntry>[] data = 
            {
                this.srcDataWorkingSet.Union(this.newDataWorkingSet).OrderBy(p => p.Zipcode).ToList(),
                this.srcDataWorkingSet.OrderBy(p => p.Zipcode).ToList(),
                this.newDataWorkingSet.OrderBy(p => p.Zipcode).ToList(),
                this.matchingData.OrderBy(p => p.Zipcode).ToList(),
                this.srcData.OrderBy(p => p.Zipcode).ToList(),
                this.newData.OrderBy(p => p.Zipcode).ToList(),
            };

            foreach (IXLWorksheet worksheet in worksheets)
            {
                worksheet.Cell("A1").SetDataType(XLCellValues.Text);
                worksheet.Cell("A1").SetValue("Zipcode");
              
                worksheet.Cell("B1").SetDataType(XLCellValues.Text);
                worksheet.Cell("B1").SetValue("State");

                worksheet.Cell("C1").SetDataType(XLCellValues.Text);
                worksheet.Cell("C1").SetValue("City");

                worksheet.Cell("D1").SetDataType(XLCellValues.Text);
                worksheet.Cell("D1").SetValue("County");

                worksheet.Cell("E1").SetDataType(XLCellValues.Text);
                worksheet.Cell("E1").SetValue("FipsCountyCode");

                worksheet.Column(1).Style.NumberFormat.Format = "@";
                worksheet.Column(5).Style.NumberFormat.Format = "@";
            }

            for (int i = 0; i < data.Length; i++)
            {
                IXLWorksheet worksheet = worksheets[i];
                IList<LocationEntry> entries = data[i];
                bool isFirstWorksheet = i == 0;

                if (isFirstWorksheet)
                {
                    worksheet.Cell("F1").SetDataType(XLCellValues.Text);
                    worksheet.Cell("F1").Value = "Action";
                }

                for (int y = 0; y < entries.Count(); y++)
                {
                    LocationEntry entry = entries[y];
                    int row = y + 2;

                    worksheet.Cell(row, 1).SetValue(entry.Zipcode);
                    worksheet.Cell(row, 2).SetValue(entry.State);
                    worksheet.Cell(row, 3).SetValue(entry.City);
                    worksheet.Cell(row, 4).SetValue(entry.County);
                    worksheet.Cell(row, 5).SetValue(entry.FipsCountyCode);

                    if (isFirstWorksheet)
                    {
                        bool isNewData = this.newData.Contains(entry);
                        worksheet.Cell(row, 6).Value = isNewData ? "Add" : "Remove"; 
                    }
                }
            }

            string path = TempFileUtils.NewTempFilePath() + ".xlsx";
            workbook.SaveAs(path);

            return path;
        }
    }
}
