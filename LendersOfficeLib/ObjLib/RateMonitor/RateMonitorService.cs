﻿namespace LendersOffice.ObjLib.RateMonitor
{
    using System;
    using DataAccess;

    public class RateMonitorService : CommonProjectLib.Runnable.IRunnable
    {
        private static RateMonitorProcessor processor;

        public string Description
        {
            get
            {
                return "Runs pricing against certain scenarios. OPM case 27476";
            }
        }

        static RateMonitorService()
        {
            try
            {
            processor = new RateMonitorProcessor();
        }
            catch (Exception exp)
            {
                Tools.LogError("[RateMonitorService] static constructor failed to instantiate new RateMonitorProcessor.", exp);
                throw;
            }
        }

        public void Run()
        {
            try
            {
                Tools.LogInfo("[RateMonitorService] started.");

            if (processor.CheckRunningHours())
            {
                if (processor.IsDone)
                {
                        processor.StartNextBatch(); // NOTE: This sets IsDone to false;
                }

                while (!processor.IsDone)
                {
                    if (!processor.CheckRunningHours())
                    {
                        break;
                    }

                    processor.ProcessMonitorsForNextLoan();
                }
            }

                Tools.LogInfo("[RateMonitorService] finished.");
        }
            catch (Exception exp)
            {
                Tools.LogError("[RateMonitorService] encountered an unexpected exception.", exp);
                throw;
    }
        }
    }
}