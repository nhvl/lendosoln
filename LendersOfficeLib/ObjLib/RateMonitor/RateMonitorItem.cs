﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.ObjLib.RateMonitor
{
    public class RateMonitorItem
    {
        public Guid LoanId { get; private set; }
        public Guid CreatedByUserId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public long MonitorId { get; private set; }
        public string EmailAddresses { get; private set; }
        public string BPDesc { get; private set; }
        public decimal Rate { get; private set; }
        public decimal Point { get; private set; }
        public char CreatedByUserType { get; private set; }
        
        public RateMonitorItem(
            Guid sLId,
            Guid userId,
            DateTime createdD,
            long monitorId,
            string email,
            string bp,
            decimal rate,
            decimal point,
            char usertype)
        {
            LoanId = sLId;
            CreatedByUserId = userId;
            CreatedDate = createdD;
            MonitorId = monitorId;
            EmailAddresses = email;
            BPDesc = bp;
            Rate = rate;
            Point = point;
            CreatedByUserType = usertype;
        }

        public bool DisableMonitor(string reason, DateTime time)
        {
            Guid brokerId = Guid.Empty;
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(this.LoanId, out brokerId);

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@monitorId", MonitorId),
                                                new SqlParameter("@disableReason", reason),
                                                new SqlParameter("@currDate", time)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(connInfo, "RateMonitorDisable", 2, parameters);
                return true;
            }
            catch (SqlException e)
            {
                Tools.LogError("Error executing stored procedure 'dbo.RateMonitorDisable'", e);
                return false;
            }
        }
    }
}
