﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using DataAccess;
using DataAccess.LoanComparison;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Admin;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.ObjLib.RateMonitor
{
    public class RateMonitorXml
    {
        public class MonitorResultItem
        {
            public CApplicantRateOption RateOption;
            public CApplicantPriceXml PriceXml;
            public MonitorResultItem(CApplicantRateOption option, CApplicantPriceXml price)
            {
                RateOption = option;
                PriceXml = price;
            }
        }

        
        public static void Generate(XmlWriter writer, Guid LoanID, MonitorResultItem[] data)
        {
            LosConvert convert = new LosConvert();
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(RateMonitorXml));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            BrokerDB brokerdb = dataLoan.BrokerDB;

            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT &&
                false == brokerdb.AllowRateMonitorForQP2FilesInEmdeddedPml && //! this needs updating...
                false == brokerdb.AllowRateMonitorForQP2FilesInTpoPml)
            {
                var usrMsg = "The broker has not enabled rate monitors for quickpricer sandboxed loans.";
                var devMsg = string.Format(
                    "LoanID '{0}', Broker: {1}",
                    LoanID,
                    brokerdb.AsSimpleStringForLog());
                throw new CBaseException(usrMsg, devMsg);
            }

            string logoHtml = "";
            bool showTextInstead = false;
            string logoText = brokerdb.Name;
            BranchDB branch = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
            branch.Retrieve();

            #region logo settings
            if (branch.IsDisplayNmModified)
            {
                logoText = branch.DisplayNm;
                showTextInstead = true;
            }
            if (!showTextInstead)
            {
                try
                {
                    byte[] logo = GetLogoBytes(brokerdb.PmlSiteID);
                    logoHtml = string.Format("data:image/gif;base64,{0}", System.Convert.ToBase64String(logo));
                }
                catch (FileNotFoundException e)
                {
                    Tools.LogError(e);
                }
            }
            #endregion
            
            writer.WriteStartElement("RateMonitor");

            #region loan info
            writer.WriteStartElement("LoanInfo");
            writer.WriteAttributeString("sLNm", dataLoan.sLNm);
            writer.WriteElementString("sLPurposeTDesc", dataLoan.sLPurposeTDesc);
            writer.WriteElementString("EmbeddedLogo", logoHtml);
            writer.WriteElementString("LogoText", logoText);
            writer.WriteElementString("ShowTextInstead", showTextInstead.ToString());
            writer.WriteEndElement();
            #endregion

            #region loan officer
            CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("LoanOfficer");
            writer.WriteElementString("Identity", CombineFields(
                Tuple.Create("", agent.AgentName),
                Tuple.Create("LIC#", agent.LicenseNumOfAgent),
                Tuple.Create("NMLS#", agent.LoanOriginatorIdentifier)));
            writer.WriteElementString("Address", agent.StreetAddr_SingleLine);
            writer.WriteElementString("ContactInfo", CombineFields(
                Tuple.Create("Tel:", agent.Phone),
                Tuple.Create("Email:", agent.EmailAddr)));
            writer.WriteElementString("Phone", agent.Phone);
            writer.WriteEndElement(); // </loan_officer>
            #endregion

            #region borrowers

            writer.WriteStartElement("Borrowers");
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData appData = dataLoan.GetAppData(i);
                writer.WriteStartElement("Borrower");
                writer.WriteElementString("ContactInfo", CombineFields(
                    Tuple.Create("", appData.aBNm),
                    Tuple.Create("Tel:", appData.aBHPhone),
                    Tuple.Create("Email:", appData.aBEmail)
                    ));
                writer.WriteEndElement();
                if (appData.aBHasSpouse)
                {
                    writer.WriteStartElement("Borrower");
                    writer.WriteElementString("ContactInfo", CombineFields(
                      Tuple.Create("", appData.aCNm),
                      Tuple.Create("Tel:", appData.aCHPhone),
                      Tuple.Create("Email:", appData.aCEmail)
                      ));
                    writer.WriteEndElement();
                }

            }
            writer.WriteEndElement();

            #endregion

            #region results
            writer.WriteStartElement("Results");
            writer.WriteAttributeString("date", DateTime.Now.ToString("G"));
            int count = 0;
            int pageCount = 0;

            foreach (MonitorResultItem item in data)
            {

                if (count % 3 == 0)
                {
                    pageCount += 1;
                    if (count != 0)
                    {
                        writer.WriteEndElement();
                    }
                    writer.WriteStartElement("Group");
                    writer.WriteAttributeString("Page", pageCount.ToString());
                }

                writer.WriteStartElement("LoanProgram");
                
                CApplicantRateOption rateOption = item.RateOption;
                CApplicantPriceXml priceXml = item.PriceXml;
                writer.WriteElementString("sSpAddr", dataLoan.sSpAddr);
                writer.WriteElementString("sSpCity", dataLoan.sSpCity + ", ");
                writer.WriteElementString("sSpZip", dataLoan.sSpZip);
                writer.WriteElementString("sProdSpT", Tools.GetsProdSpTDescription(dataLoan.sProdSpT) + ", ");
                writer.WriteElementString("sProdSpStructureT", Tools.GetStructureTypeDescription(dataLoan.sProdSpStructureT));
                writer.WriteElementString("sHouseValPe", dataLoan.sHouseValPe_rep);
                writer.WriteElementString("sDownPmtPcPe", dataLoan.sDownPmtPcPe_rep);
                writer.WriteElementString("sEquityPe", dataLoan.sEquityPe_rep);
                writer.WriteElementString("sProdRLckdDays", dataLoan.sProdRLckdDays_rep);

                writer.WriteElementString("sProThisMPmt", "$" + rateOption.FirstPmtAmt_rep);
                writer.WriteElementString("sProRealETx", dataLoan.sProRealETxPe_rep);
                writer.WriteElementString("sProHazIns", dataLoan.sProHazInsPe_rep);
                writer.WriteElementString("sProMIns", rateOption.sProMIns);
                writer.WriteElementString("sProHoAssocDues", dataLoan.sProHoAssocDuesPe_rep);
                writer.WriteElementString("sProOHExp", dataLoan.sProOHExpPe_rep);
                writer.WriteElementString("sMonthlyPmt", rateOption.sMonthlyPmtPe);

                writer.WriteElementString("LpTemplateNm", rateOption.LpTemplateNm);
                writer.WriteElementString("sFfUfmipFinanced", rateOption.sFfUfmipFinanced);
                writer.WriteElementString("sLAmtCalcPe", rateOption.sLAmtCalcPe);
                writer.WriteElementString("sProdImpound", dataLoan.sProdImpound ? "Yes" : "No");
                writer.WriteElementString("sApr", rateOption.Apr_rep + "%");
                writer.WriteElementString("sFinalLAmt", rateOption.sFinalLAmt);
                writer.WriteElementString("CurrentPITIPayment", rateOption.CurrentPITIPayment);

                writer.WriteElementString("DTI", rateOption.BottomRatio_rep + "%");
                writer.WriteElementString("TotalCashClose", rateOption.CashToClose);
                writer.WriteElementString("sFinCharge", rateOption.sFinCharge);
                writer.WriteElementString("ClosingCost", rateOption.ClosingCost);

                writer.WriteElementString("EstimatedReservesDollars", rateOption.EstimatedReservesDollars);
                writer.WriteElementString("EstimatedReservesMonths", rateOption.Reserves);

                writer.WriteElementString("PrincipalPaidOverHorizon", rateOption.PrincipalPaidOverHorizon);
                writer.WriteElementString("FinanceChargesPaidOverHorizon", rateOption.FinanceChargesPaidOverHorizon);
                writer.WriteElementString("RemainingBalanceAfterHorizon", rateOption.RemainingBalanceAfterHorizon);
                writer.WriteElementString("SavingsVersusCurrentLoan", rateOption.SavingsVersusCurrentLoan);
                writer.WriteElementString("PitiaSavings", rateOption.PitiaSavings);
                writer.WriteElementString("RecoupClosingCostsMonths", rateOption.BreakEvenPoint);
                var sProdRLckdExpiredD = dataLoan.CalculateRateLockExpirationDate(convert, CDateTime.Create(DateTime.Now), int.Parse(dataLoan.sProdRLckdDays_rep));

                writer.WriteElementString("sProdRLckdExpiredD", sProdRLckdExpiredD.DateTimeForComputation.ToShortDateString());

                writer.WriteElementString("Rate", rateOption.Rate_rep + "%");

                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                bool includeOComp = (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                 && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                 && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);

                writer.WriteElementString("Points", includeOComp ? rateOption.PointIncludingOriginatorComp_rep : rateOption.Point_rep);

                if (priceXml.AreRatesExpired)
                {
                    writer.WriteElementString("RatesAreExpired", "True");
                }
                count++;
                writer.WriteEndElement(); // </LoanProgram>
            }
            writer.WriteEndElement(); // </Group>
            writer.WriteEndElement(); // </Results>
            #endregion

            writer.WriteEndElement(); // </RateMonitor>
        }

        private static string CombineFields(params Tuple<string, string>[] labelsAndFields)
        {
            StringBuilder data = new StringBuilder();
            foreach (var field in labelsAndFields)
            {
                if (string.IsNullOrEmpty(field.Item2))
                {
                    continue;
                }

                if (data.Length != 0)
                {
                    data.Append(", ");
                }

                data.Append(field.Item1);
                if (field.Item1.Length > 0)
                {
                    data.Append(" ");
                }
                data.Append(field.Item2);
            }
            return data.ToString();
        }
        
        private static byte[] GetLogoBytes(Guid pmlsiteid)
        {
            string fileDBKey = string.Empty;
            fileDBKey = pmlsiteid.ToString().ToLower() + ".logo.gif";
            return FileDBTools.ReadData(E_FileDB.Normal, fileDBKey);
        }
    }
    
}
