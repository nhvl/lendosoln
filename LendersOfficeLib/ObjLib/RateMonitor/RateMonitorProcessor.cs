﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Xml;
using DataAccess;
using DataAccess.LoanComparison;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Email;
using LendersOffice.ObjLib.QuickPricer;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.RateMonitor
{
    public class RateMonitorProcessor : MarshalByRefObject
    {
        #region Static access
        public static int CompletedToday
        {
            get {
                try
                {
                    DateTime now = DateTime.Now;
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@OlderThan", new DateTime(now.Year, now.Month, now.Day))
                                                };

                    foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                    {
                        using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "RateMonitorsLastRunDates", parameters))
                        {
                            if (reader.Read())
                            {
                                return 0; //At least one active monitor not run today (0=false)
                            }
                        }
                    }

                    return 1; //Reader didn't find anything; CompletedToday = true
                }
                catch(SqlException e)
                {
                    Tools.LogError("Error accessing database on RateMonitorProcessor.CompletedToday", e);
                    return -1;
                }
            }
        }
        private static string RateMonitorXsltResource
        {
            get { return "LendersOffice.ObjLib.RateMonitor.RateMonitorCert.xslt.config"; }
        }
        #endregion

        private bool m_isBatchDone;
        private bool m_runningHours;

        Dictionary<DbConnectionInfo, long> batchIds;

        internal bool IsDone
        {
            get { return m_isBatchDone; }
        }
        internal bool CheckRunningHours()
        {
            DateTime now = DateTime.Now;
            if (m_runningHours)
            {
                if (now.Hour >= 18) // Stop at 6PM
                {
                    m_runningHours = false;
                    Tools.LogInfo("[RATE MONITOR] Service stopped.");
                    if (CompletedToday == 0)
                    {
                        Tools.LogErrorWithCriticalTracking("[RATE MONITOR] Not all monitors completed in one day.");
                    }
                }
            }
            else if(now.Hour < 18)
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveLpeReleaseCurrent"))
                {
                    if (reader.Read())
                    {
                        DateTime lastRun = (DateTime)reader["DataLastModifiedD"];
                        if (lastRun > new DateTime(now.Year, now.Month, now.Day))
                        {
                            m_runningHours = true;
                            Tools.LogInfo("[RATE MONITOR] Service started.");
            			}
                    }
                }
            }
            return m_runningHours;
        }


        public RateMonitorProcessor()
        {
            m_isBatchDone = true;
            m_runningHours = false;

            batchIds = new Dictionary<DbConnectionInfo, long>();
        }

        public void StartNextBatch()
        {
            //Only allow next batches if previous one is finished
            if (!IsDone)
            {
                Tools.LogError("Illegal call to RateMonitorProcessor.StartNextBatch()");
                return;
            }

            try
            {
                // BatchId needs to be cleared between runs.
                batchIds.Clear();
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    // try to start at maximum(previously-run batch)+1
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RateMonitorsGetLastBatchNumber", null))
                    {
                        object value;
                        if (reader.Read() && (value = reader["StartBatch"]) != DBNull.Value)
                        {
                            batchIds.Add(connInfo, ((long)value) + 1);
                        }
                        else
                        {
                            batchIds.Add(connInfo, 1);
                        }

                        m_isBatchDone = false;
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("Error executing stored procedure 'dbo.RateMonitorGetLastBatchNumber'", e);
            }
            catch (CBaseException e)
            {
                Tools.LogError("Error in RateMonitorProcessor.StartNextBatch():", e);
            }
            
        }

        private LoanProgramByInvestorSet GetLoanProgramsToRunLpe(Guid priceGroupId, BrokerDB broker, params string[] bpDescs)
        {
            LoanProgramByInvestorSet set = new LoanProgramByInvestorSet();
            List<TemplateRowData> cache = new List<TemplateRowData>();
            #region populate cache

            foreach (TemplateRowData row in TemplateRowData.List(DataSrc.LpeSrc, broker, priceGroupId))
            {
                cache.Add(row);
            }
            #endregion

            foreach (TemplateRowData rowData in cache)
            {
                RateMergeGroup group = new RateMergeGroup(rowData.Key);
                if (bpDescs.Contains(group.Name, StringComparer.OrdinalIgnoreCase))
                {
                    set.Add(rowData.lLpTemplateId, rowData.lLpTemplateNm, rowData.productCode);
                }
            }
            return set;
        }

        private UnderwritingResultItem RunPricing(AbstractUserPrincipal principal, LoanProgramByInvestorSet programSet, Guid loanId, Guid priceGroupId)
        {
            PricingState state = new PricingState();
            state.IsForRateMonitor = true;
            Guid id = DistributeUnderwritingEngine.SubmitPinToEngine(principal, loanId, priceGroupId, state, programSet, state.SecondLoanMonthlyPayment);

            bool isReady = false;
            do
            {
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000);
                isReady = LpeDistributeResults.IsResultAvailable(id);
            } while (!isReady);

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(id);
            return resultItem;
        }

        public Dictionary<long, RateMonitorXml.MonitorResultItem> RunPricingAndGetResultsByMonitorId(AbstractUserPrincipal principal, CPageData dataLoan, IEnumerable<RateMonitorItem> rateMonitors )
        {
            try
            {
                Dictionary<string, RateMonitorItem> rateMonitorsByDescription = new Dictionary<string, RateMonitorItem>(StringComparer.OrdinalIgnoreCase);

                foreach (RateMonitorItem rateMonitor in rateMonitors)
                {
                    if (rateMonitorsByDescription.ContainsKey(rateMonitor.BPDesc))
                    {
                        continue; //we should invalid this rate monitor..
                    }
                    rateMonitorsByDescription.Add(rateMonitor.BPDesc, rateMonitor);
                }

                LoanProgramByInvestorSet set = GetLoanProgramsToRunLpe(dataLoan.sProdLpePriceGroupId, dataLoan.BrokerDB, rateMonitorsByDescription.Keys.ToArray());
                if (set.InvestorList.Count == 0)
                {
                    return null; //means error
                }
                
                var pricingResults = RunPricing(principal, set, dataLoan.sLId, dataLoan.sProdLpePriceGroupId);
                if (pricingResults.IsErrorMessage)
                {
                    return null; //means error
                }

                Dictionary<long, RateMonitorXml.MonitorResultItem> results = new Dictionary<long, RateMonitorXml.MonitorResultItem>();


                
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                bool includeOComp = (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                                 && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                                 && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);

                foreach (var rateMergeGroup in pricingResults.GetRateMergeResult(BrokerDB.RetrieveById(dataLoan.sBrokerId)))
                {
                    if (rateMonitorsByDescription.ContainsKey(rateMergeGroup.Name))
                    {
                        RateMonitorItem item = rateMonitorsByDescription[rateMergeGroup.Name];
                        foreach (CApplicantRateOption rate in rateMergeGroup.RateOptions)
                        {

                            decimal point = includeOComp ? rate.PointIncludingOriginatorComp : rate.Point;
                            if (rate.Rate <= item.Rate && point <= item.Point && !rate.IsRateExpired && !rate.IsDisqualified )
                            {
                                CApplicantPriceXml priceXml = null;
                                foreach (CApplicantPriceXml xml in pricingResults.GetEligibleLoanProgramList())
                                {
                                    if (xml.lLpTemplateId == rate.LpTemplateId)
                                    {
                                        priceXml = xml;
                                        break;
                                    }
                                }

                                if (priceXml != null)
                                {
                                    RateMonitorXml.MonitorResultItem resultItem = new RateMonitorXml.MonitorResultItem(rate, priceXml);
                                    
                                    //If better hit already found, don't overwrite new resultItem
                                    if (results.ContainsKey(item.MonitorId))
                                    {
                                        point = includeOComp ? resultItem.RateOption.PointIncludingOriginatorComp : resultItem.RateOption.Point;
                                        RateMonitorXml.MonitorResultItem existing = results[item.MonitorId];

                                        decimal existingPoint = includeOComp ? existing.RateOption.PointIncludingOriginatorComp : resultItem.RateOption.Point;
                                        if (existing.RateOption.Rate < resultItem.RateOption.Rate ||
                                            (existing.RateOption.Rate == resultItem.RateOption.Rate && existing.RateOption.Point < point))
                                        {
                                            continue;
                                        }
                                    }
                                    results[item.MonitorId] = resultItem;                                        
                                }
                                //else error
                            }
                        }
                    }
                }
                return results; //did not find rate merge in results
            }
            catch (CBaseException e)
            {
                Tools.LogError("Error in RateMonitorProcessor.RunPricingAndGetResultsByMonitorId():", e);
                return null;
            }
        }
    
        public void ProcessMonitorsForNextLoan()
        {
            //Only allow next loans if current batch isn't done yet
            if (IsDone)
            {
                Tools.LogError("Illegal call to RateMonitorProcessor.ProcessMonitorsForNextLoan()");
                return;
            }
            try
            {
                DateTime processLoanStart = DateTime.Now;
                var monitors = new Dictionary<long, RateMonitorItem>();

                this.ResetPrincipal();

                #region Load Monitors from Stored Procedure
                try
                {
                    foreach (KeyValuePair<DbConnectionInfo, long> kvp in batchIds)
                    {
                        DbConnectionInfo connInfo = kvp.Key;
                        long batchId = kvp.Value;
                        SqlParameter[] parameters = {
                                                    new SqlParameter("@batchId", batchId),
                                                    new SqlParameter("@currDate", processLoanStart)
                                                };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RateMonitorsForNextLoan", parameters))
                        {
                            while (reader.Read())
                            {
                                long monitorid = Convert.ToInt64(reader["id"]);
                                monitors.Add(monitorid,
                                    new RateMonitorItem(
                                        (Guid)reader["sLId"],
                                        (Guid)reader["CreatedByUserId"],
                                        (DateTime)reader["createdDate"],
                                        monitorid,
                                        reader["EmailAddresses"].ToString(),
                                        reader["BPDesc"].ToString(),
                                        Convert.ToDecimal(reader["Rate"]),
                                        Convert.ToDecimal(reader["Point"]),
                                        ((string)reader["CreatedByUserType"])[0]));
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Tools.LogError("Error executing stored procedure 'dbo.RateMonitorsForNextLoan'", e);
                    return;
                }
                #endregion

                if (monitors.Count > 0)
                {
                    m_isBatchDone = false;

                    var representativeMonitor = monitors.Values.First();
                    Guid loanid = representativeMonitor.LoanId;
                    string disableReason = this.GetLoanStatusDisableReason(loanid);

                    //If it's not a Lead or if it it has a Lock (Requested), remove all associated monitors
                    IEnumerable<RateMonitorItem> monitorValues;
                    if (disableReason != null)
                    {
                        monitorValues = monitors.Values.ToList();
                        foreach (RateMonitorItem monitor in monitorValues)
                        {
                            monitor.DisableMonitor(disableReason, processLoanStart);
                            monitors.Remove(monitor.MonitorId);
                        }

                        // If the loan is no longer applicable, we can skip
                        // the remaining processing.
                        return;
                    }

                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanid, typeof(RateMonitorProcessor));
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.InitLoad();

                    var principals = new Dictionary<Guid, AbstractUserPrincipal>();
                    monitorValues = monitors.Values.ToList(); ;
                    foreach (var monitor in monitorValues)
                    {
                        AbstractUserPrincipal principal;
                        if(principals.ContainsKey(monitor.CreatedByUserId))
                        {
                            principal = principals[monitor.CreatedByUserId];   
                        }
                        else
                        {
                            principal = PrincipalFactory.RetrievePrincipalForUser(dataLoan.sBrokerId, monitor.CreatedByUserId, Char.ToString(monitor.CreatedByUserType));
                            principals.Add(monitor.CreatedByUserId, principal); // fine to add null principal.
                        }
                        if (false == dataLoan.BrokerDB.IsNewPmlUIEnabled)
                        {
                            if (null == principal)
                            {
                                disableReason = "User has no principal.";
                                monitor.DisableMonitor(disableReason, processLoanStart);
                                monitors.Remove(monitor.MonitorId);
                            }
                            else if (false == principal.IsNewPmlUIEnabled)
                            {
                                disableReason = "User not using New PML";
                                monitor.DisableMonitor(disableReason, processLoanStart);
                                monitors.Remove(monitor.MonitorId);
                            }
                        }
                    }

                    if(monitors.Count > 0)
                    {
                        List<RateMonitorItem> workingMonitors = new List<RateMonitorItem>();

                        #region filter out expired monitors
                        foreach (RateMonitorItem monitor in monitors.Values)
                        {
                            DateTime d = monitor.CreatedDate;

                            //Expire after 12 months
                            if (processLoanStart >= d.AddMonths(12) 
                                && E_sLoanFileT.QuickPricer2Sandboxed != dataLoan.sLoanFileT) 
                            {
                                // don't expire for quickpricer 2.0 files which may be used more like scenario savers.
                                monitor.DisableMonitor("Monitor Expired", processLoanStart);
                            }
                            else
                            {
                                workingMonitors.Add(monitor);
                            }
                        }
                        #endregion

                        workingMonitors = FilterOutQP2DisabledMonitors(workingMonitors, dataLoan.sLoanFileT, dataLoan.BrokerDB, processLoanStart); //! hm... maybe don't need this just yet...

                        AbstractUserPrincipal representativePrincipal= null;
                        if (workingMonitors.Count > 0)
                        {
                            var representativeWorkingMonitor = workingMonitors.First();

                            representativePrincipal = GetPrincipal(representativeMonitor, dataLoan.sLoanFileT, dataLoan.sBrokerId, dataLoan.sEmployeeLoanRepId);
                            if (null == representativePrincipal)
                            {
                                foreach (var monitor in workingMonitors)
                                {
                                    monitor.DisableMonitor("Principal was null", processLoanStart);
                                }
                                workingMonitors = new List<RateMonitorItem>();
                            }
                        }

                        if (workingMonitors.Count > 0 && null != representativePrincipal)
                        {
                            // For pricing to function correctly, we need the system to
                            // price using the user's principal. This is so that audits 
                            // and certain loan fields using the user's broker function
                            // correctly during the pricing run. The monitor's principal
                            // will be reset once this batch is complete.
                            this.SetPrincipal(representativePrincipal);

                            Dictionary<long, RateMonitorXml.MonitorResultItem> results = RunPricingAndGetResultsByMonitorId(representativePrincipal, dataLoan, workingMonitors);

                            this.ResetPrincipal();

                            if (results != null && results.Count > 0)
                            {
                                try
                                {
                                    RateMonitorProcessor.SendRateMonitorEmail(
                                        loanid,
                                        dataLoan,
                                        results,
                                        monitors,
                                        processLoanStart);
                                }
                                catch (CBaseException e)
                                {
                                    Tools.LogError(e);
                                }
                            }
                        }
                    }
                }
                else //No loans remaining for this batch
                {
                    m_isBatchDone = true;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine("RateMonitorProcessor - Completed Batches #:");

                    foreach (KeyValuePair<DbConnectionInfo, long> kvp in batchIds)
                    {
                        sb.AppendLine($"DB:{kvp.Key.Description} , Batches #:{kvp.Value}");
                    }
                }
            }
            catch(CBaseException e)
            {
                Tools.LogError("Error in RateMonitorProcessor.ProcessMonitorsForNextLoan():", e);
            }
        }

        private string GetLoanStatusDisableReason(Guid loanid)
        {
            var brokerId = Guid.Empty;

            try
            {
                brokerId = Tools.GetBrokerIdByLoanID(loanid);
            }
            catch (LoanNotFoundException)
            {
                return "Loan File Not Found";
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@LoanId", loanid),
                new SqlParameter("@BrokerId", brokerId)
            };

            var factory = new Adapter.StoredProcedureAdapterFactory();
            var storedProcedureExecutor = factory.Create(DbAccessUtils.GetDbProvider(), TimeoutInSeconds.Thirty);

            var procedureName = StoredProcedureName.Create("GetLoanDataForRateMonitorDisableCheck");
            if (!procedureName.HasValue)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError);
            }

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = storedProcedureExecutor.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                if (!reader.Read())
                {
                    return "Loan File Not Found";
                }

                if (!Convert.ToBoolean(reader["IsValid"]))
                {
                    return "Loan File Deleted";
                }

                if (!Tools.IsStatusLead((E_sStatusT)reader["sStatusT"]))
                {
                    return "Loan Status No Longer Lead";
                }

                if ((E_sRateLockStatusT)reader["sRateLockStatusT"] != E_sRateLockStatusT.NotLocked)
                {
                    return "Loan Rate Locked or Lock Requested";
                }

                return null;
            }
        }

        private static void SendRateMonitorEmail(
            Guid loanid, 
            CPageData dataLoan,
            Dictionary<long, RateMonitorXml.MonitorResultItem> results,
            Dictionary<long, RateMonitorItem> monitors,
            DateTime processLoanStart)
        {
            Dictionary<string, List<RateMonitorXml.MonitorResultItem>> emailsToResults = new Dictionary<string, List<RateMonitorXml.MonitorResultItem>>();

            foreach (long id in results.Keys)
            {
                var monitorItem = monitors[id];

                if (string.IsNullOrWhiteSpace(monitorItem.EmailAddresses) == false)
                {
                    string[] emails = monitorItem.EmailAddresses.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string email in emails)
                    {
                        if (!emailsToResults.ContainsKey(email))
                        {
                            emailsToResults.Add(email, new List<RateMonitorXml.MonitorResultItem>());
                        }

                        emailsToResults[email].Add(results[id]);
                    }
                }

                monitorItem.DisableMonitor("Rate Monitor Hit", processLoanStart);
            }

            foreach (KeyValuePair<string, List<RateMonitorXml.MonitorResultItem>> kvp in emailsToResults)
            {
                string resultXml = GenerateReport(loanid, kvp.Value.ToArray());
                string htmlForReport = XslTransformHelper.TransformFromEmbeddedResource(RateMonitorXsltResource, resultXml, null);
                byte[] pdfBytes = CPageBase.ConvertToPdf(htmlForReport);

                CBaseEmail email = new CBaseEmail(dataLoan.sBrokerId)
                {
                    From = ConstStage.DefaultDoNotReplyAddress,
                    
                    IsHtmlEmail = false,
                    To = kvp.Key   
                };

                if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                {
                    string monitorName = QuickPricer2LoanPoolManager.GetRateMonitorNameByLoanID(dataLoan.sBrokerId, dataLoan.sLId, monitors.Values.First().CreatedByUserId);
                    email.Subject = string.Format("{0} - Rate Monitor Notification", monitorName, dataLoan.sLNm);
                }
                else
                {
                    email.Subject = string.Format("{0} - {1} - Rate Monitor Notification", dataLoan.GetAppData(0).aBLastNm, dataLoan.sLNm);
                }

                email.AddAttachment("RateMonitorReport.pdf", pdfBytes);

                email.Send();
            }
        }

        private List<RateMonitorItem> FilterOutQP2DisabledMonitors(IEnumerable<RateMonitorItem> monitors, E_sLoanFileT sLoanFileT, BrokerDB broker, DateTime processLoanStart)
        {
            var workingMonitorsExceptQP2DisabledMonitors = new List<RateMonitorItem>();
            foreach (var monitor in monitors)
            {
                if (E_sLoanFileT.QuickPricer2Sandboxed == sLoanFileT)
                {
                    if (monitor.CreatedByUserType == 'B' && false == broker.AllowRateMonitorForQP2FilesInEmdeddedPml)
                    {
                        var disableReason = "Embedded quickpricer sandboxed.";
                        monitor.DisableMonitor(disableReason, processLoanStart);
                    }
                    else if (monitor.CreatedByUserType == 'P' && false == broker.AllowRateMonitorForQP2FilesInTpoPml)
                    {
                        var disableReason = "Originator Portal quickpricer sandboxed.";
                        monitor.DisableMonitor(disableReason, processLoanStart);
                    }
                    else
                    {
                        workingMonitorsExceptQP2DisabledMonitors.Add(monitor);
                    }
                }
                else
                {
                    workingMonitorsExceptQP2DisabledMonitors.Add(monitor);
                }
            }
            return workingMonitorsExceptQP2DisabledMonitors;
        }

        private AbstractUserPrincipal GetPrincipal(RateMonitorItem monitor, E_sLoanFileT loanFileT, Guid brokerID, Guid loanOfficerID)
        {
            if (E_sLoanFileT.QuickPricer2Sandboxed == loanFileT)
            {
                // quickpricer loan files do not need a loan officer assigned for pricing.
                // also, only one user ever uses the quickpricer2 file, so we only need to look at the first.
                return PrincipalFactory.RetrievePrincipalForUser(
                    brokerID, 
                    monitor.CreatedByUserId, 
                    Char.ToString(monitor.CreatedByUserType));
            }
            else
            {
                // non-quickpricer loan files do need a loan officer assigned prior to pricing.
                Guid? loUserID;
                char? loUserType;
                if (Tools.TryGetUserInfoByEmployeeId(brokerID, loanOfficerID, out loUserID, out loUserType))
                {
                    if (loUserID.HasValue && loUserType.HasValue)
                    {
                        return PrincipalFactory.RetrievePrincipalForUser(
                            brokerID,
                            loUserID.Value,
                            Char.ToString(loUserType.Value));
                    }
                }
            }
            return null;
        }

        private void SetPrincipal(AbstractUserPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
        }

        private void ResetPrincipal()
        {
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        private static string GenerateReport(Guid loanId, params RateMonitorXml.MonitorResultItem[] results)
        {
            string result;
            using (StringWriter8 writer = new StringWriter8())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    RateMonitorXml.Generate(xmlWriter, loanId, results);
                    xmlWriter.Flush();
                }
                result = writer.ToString();
            }
            return result;
        }
    }
}