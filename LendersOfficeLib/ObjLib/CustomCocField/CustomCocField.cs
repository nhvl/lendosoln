﻿namespace LendersOffice.ObjLib.CustomCocField
{
    using System.Data;

    /// <summary>
    /// Custom change of circumstance Field object.
    /// </summary>
    public class CustomCocField
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomCocField" /> class.
        /// </summary>
        /// <param name="reader">IDataReader object.</param>
        public CustomCocField(IDataReader reader)
        {
            this.FieldId = reader["FieldId"].ToString();
            this.Description = reader["Description"].ToString();
            this.IsInstant = (bool)reader["IsInstant"];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomCocField" /> class.
        /// </summary>
        /// <param name="fieldId">Field Id describing the custom coc field.</param>
        /// <param name="description">Description for the field id.</param>
        /// <param name="isInstant">Whether the field is instantly processed or delayed.</param>
        public CustomCocField(string fieldId, string description, bool isInstant)
        {
            this.FieldId = fieldId;
            this.Description = description;
            this.IsInstant = isInstant;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="CustomCocField" /> class from being created.
        /// </summary>
        private CustomCocField()
        {
        }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        public string FieldId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the custom coc field is processed immediately or delayed.
        /// </summary>
        public bool IsInstant { get; set; }
    }
}
