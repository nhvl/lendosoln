﻿namespace LendersOffice.ObjLib.CustomCocField
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;    

    /// <summary>
    /// Custom coc field list helper class.
    /// </summary>
    public static class CustomCocFieldListHelper
    {
        /// <summary>
        /// Retrieves the custom coc fields by broker id.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <returns>Custom coc field list.</returns>
        public static List<CustomCocField> RetrieveByBrokerId(Guid brokerId)
        {
            var list = new List<CustomCocField>();

            if (brokerId == null)
            {
                return list;
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, StoredProcedureName.Create("CUSTOM_COC_FIELD_LIST_RETRIEVE").Value, parameters, TimeoutInSeconds.Default))
                {
                    while (reader.Read())
                    {
                        list.Add(new CustomCocField(reader));
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Saves a given list of custom fields.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="fields">List of custom coc fields to save.</param>
        public static void Save(Guid brokerId, List<CustomCocField> fields)
        {
            var customCocFieldDictionary = new Dictionary<string, Type>()
            {
                { "FieldId", typeof(string) },
                { "Description", typeof(string) },
                { "IsInstant", typeof(bool) }
            };

            var customCocFieldTable = DbAccessUtils.InitializeTableValueDataTable("CustomCocFieldTableType", customCocFieldDictionary);

            foreach (var field in fields)
            {
                customCocFieldTable.Rows.Add(field.FieldId, field.Description, field.IsInstant);
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@CustomCocFieldList", customCocFieldTable),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("CUSTOM_COC_FIELD_LIST_SAVE").Value, parameters, TimeoutInSeconds.Default);                
            }
        }
    }
}
