﻿// <copyright file="LoanAssignmentSettings.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   8/18/2014 4:44:00 PM 
// </summary>
namespace LendersOffice.ObjLib.MortgagePool
{
    /// <summary>
    /// Contains settings indicating which fields need to be assigned on a 
    /// loan file when it is assigned to a pool.
    /// </summary>
    public class LoanAssignmentSettings
    {
        /// <summary>
        /// Initializes a new instance of the LoanAssignmentSettings class.
        /// </summary>
        public LoanAssignmentSettings()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LoanAssignmentSettings class.
        /// </summary>
        /// <param name="isSetBackEndBasePrice">
        /// Whether the pool price should be copied to the back end rate
        /// lock base price field.
        /// </param>
        /// <param name="isSetBackEndRateLock">
        /// Whether the back-end rate should be locked.
        /// </param>
        /// <param name="isSetDeliveryFee">
        /// Whether a pool delivery fee should be added to the back-end
        /// rate lock adjustments.
        /// </param>
        /// <param name="deliveryFee">
        /// The delivery fee to set. This value will be ignored if 
        /// isSetDeliveryFee is false.
        /// </param>
        public LoanAssignmentSettings(
            bool isSetBackEndBasePrice,
            bool isSetBackEndRateLock,
            bool isSetDeliveryFee,
            decimal deliveryFee)
        {
            this.IsSetBackEndBasePrice = isSetBackEndBasePrice;
            this.IsSetBackEndRateLock = isSetBackEndRateLock;
            this.IsSetDeliveryFee = isSetDeliveryFee;
            this.DeliveryFee = deliveryFee;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the pool price should be copied 
        /// to the back end rate lock base price field.
        /// </summary>
        /// <value>
        /// Whether the pool price should be copied to the back end rate lock
        /// base price field.
        /// </value>
        public bool IsSetBackEndBasePrice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the back-end rate should be locked.
        /// </summary>
        /// <value>Whether the back-end rate should be locked.</value>
        public bool IsSetBackEndRateLock { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a pool delivery fee should be added to the 
        /// back-end rate lock adjustments.
        /// </summary>
        /// <value>
        /// Whether a pool delivery fee should be added to the back-end rate
        /// lock adjustments.
        /// </value>
        public bool IsSetDeliveryFee { get; set; }

        /// <summary>
        /// Gets or sets the delivery fee to set. This value will be ignored if 
        /// IsSetDeliveryFee is false.
        /// </summary>
        /// <value>
        /// The delivery fee to set. This value will be ignored if 
        /// isSetDeliveryFee is false.
        /// </value>
        public decimal DeliveryFee { get; set; }
    }
}
