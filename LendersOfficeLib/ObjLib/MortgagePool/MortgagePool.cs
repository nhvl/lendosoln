﻿namespace LendersOffice.ObjLib.MortgagePool
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.HttpModule;
    using LendersOffice.ObjLib.BatchOperationError;
    using LendersOffice.ObjLib.CapitalMarkets;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.GinnieNet;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    #region Enums
    public enum E_CommitmentType
    {
        None = 0,
        Mandatory,
        BestEfforts,
        AOT,
        DT,
        ForwardCommitment,
        Securitization,
        MBSSwap,
        Other
    }

    public enum E_SettlementType
    {
        LeaveBlank = 0,
        Cash = 1,
        GNMAI = 2,
        GNMAII = 3,
        FNMA = 4,
        FHLMC = 5,
        Other = 6,
        Umbs = 7
    }

    public enum E_DeliveryType
    {
        LeaveBlank = 0,
        GinnieMaeSingleLender = 1,
        GinnieMaeMultipleLender = 2,
        FannieMajors = 3,
        FannieMae = 4,
        FreddieMacGoldPC = 5,
        Other = 6,
        UniformMbs = 7
    }

    public enum E_BasePricingCalcType
    {
        Manually = 0,
        FromBackEnd,
        PoolLevel,
        Table
    }

    public enum E_LLPAType
    {
        Manually = 0,
        FromBackEnd
    }

    public enum E_SRPType
    {
        Manually = 0,
        FromBackEnd,
        PoolLevel
    }

    public enum E_BlendPairOffCalcType
    {
        None = 0,
        WorstCase,
        CurrentMarket
    }
    #endregion

    public class MortgagePool
    {
        private const string PoolAssignmentLockMessage = "Locked automatically on assignment to pool.";
        public const string PoolDeliveryAdjustmentDescription = "Pool delivery fee";
        private const string UnableToAssignBackEndBasePrice = "Failed to assign loan to the pool due to note rate not being in the pool pricing table.";
        private const string UnableToDetermineRateLockExpirationDate = "Unable to determine the rate lock expiration date. Please enter a valid commitment expiration date.";
        private const string UnableToSetBackEndBasePriceForFileWithLock = "Unable to set back-end base price because file already has back-end rate lock.";
        private const string UnableToSetDeliveryFeeForFileWithLock = "Unable to set delivery fee because file already has back-end rate lock.";
        private const string UnableToSetBackEndRateLockForFileWithLock = "Unable to set the back-end rate lock because file already has back-end lock.";

        #region Fields

        private static Dictionary<E_MortgagePoolAgencyT, List<E_MortgagePoolIndexT>> s_agencyValidIndices
            = new Dictionary<E_MortgagePoolAgencyT, List<E_MortgagePoolIndexT>>()
            {
                {E_MortgagePoolAgencyT.GinnieMae,
                    new List<E_MortgagePoolIndexT>()
                    {
                        E_MortgagePoolIndexT.CMT,
                        E_MortgagePoolIndexT.LIBOR
                    }},

                {E_MortgagePoolAgencyT.FannieMae,
                    new List<E_MortgagePoolIndexT>()
                    {
                    }},

                {E_MortgagePoolAgencyT.FreddieMac,
                    new List<E_MortgagePoolIndexT>()
                    {
                    }}
            };

        public static Dictionary<E_MortgagePoolAgencyT, List<E_MortgagePoolIndexT>> AgencyValidIndices => s_agencyValidIndices;

        public static int num_SettlementAccounts = 2;

        private Dictionary<string, object> m_dirtyFields;
        private List<string> m_nullableFields;
        private Guid m_brokerId;
        private LosConvert convert;

        public long PoolId { get; }

        public Guid BrokerId => this.m_brokerId;

        // When changing this method, be sure to update the the property's references
        // and following stored procedures/functions:
        // - FindMortgagePools
        // - FindMortgagePoolsByNumber
        // - FindPoolIdForBrokerIdAgencyPoolNum
        // - RetrievePoolNumberByAgency
        public string PoolNumberByAgency
        {
            get
            {
                switch (AgencyT)
                {
                    case E_MortgagePoolAgencyT.GinnieMae:
                        return BasePoolNumber + IssueTypeCode + PoolTypeCode;
                    case E_MortgagePoolAgencyT.FannieMae:
                    case E_MortgagePoolAgencyT.FreddieMac:
                        return this.PoolPrefix + "-" + this.BasePoolNumber;
                    default:
                        return BasePoolNumber;
                }
            }
        }

        private CDateTime m_OpenedD;
        public CDateTime OpenedD
        {
            get { return m_OpenedD; }
            set { m_OpenedD = value; m_dirtyFields["OpenedD"] = value; }
        }
        public string OpenedD_rep
        {
            get { return OpenedD.ToString(convert); }
        }

        private CDateTime m_DeliveredD;
        public CDateTime DeliveredD
        {
            get { return m_DeliveredD; }
            set { m_DeliveredD = value; m_dirtyFields["DeliveredD"] = value; }
        }
        public string DeliveredD_rep
        {
            get { return DeliveredD.ToString(convert); }
        }

        private CDateTime m_ClosedD;
        public CDateTime ClosedD
        {
            get { return m_ClosedD; }
            set { m_ClosedD = value; m_dirtyFields["ClosedD"] = value; }
        }
        public string ClosedD_rep
        {
            get { return ClosedD.ToString(convert); }
        }

        private CDateTime m_IssueD;
        public CDateTime IssueD
        {
            get { return m_IssueD; }
            set { m_IssueD = value; m_dirtyFields["IssueD"] = value; }
        }
        public string IssueD_rep
        {
            get { return IssueD.ToString(convert); }
        }

        private CDateTime m_SettlementD;
        public CDateTime SettlementD
        {
            get { return m_SettlementD; }
            set { m_SettlementD = value; m_dirtyFields["SettlementD"] = value; }
        }
        public string SettlementD_rep
        {
            get { return SettlementD.ToString(convert); }
        }

        private CDateTime m_InitialPmtD;
        public CDateTime InitialPmtD
        {
            get { return m_InitialPmtD; }
            set { m_InitialPmtD = value; m_dirtyFields["InitialPmtD"] = value; }
        }
        public string InitialPmtD_rep
        {
            get { return InitialPmtD.ToString(convert); }
        }

        private CDateTime m_UnpaidBalanceD;
        public CDateTime UnpaidBalanceD
        {
            get { return m_UnpaidBalanceD; }
            set { m_UnpaidBalanceD = value; m_dirtyFields["UnpaidBalanceD"] = value; }
        }
        public string UnpaidBalanceD_rep
        {
            get { return UnpaidBalanceD.ToString(convert); }
        }

        private CDateTime m_MaturityD;
        public CDateTime MaturityD
        {
            get { return m_MaturityD; }
            set { m_MaturityD = value; m_dirtyFields["MaturityD"] = value; }
        }
        public string MaturityD_rep
        {
            get { return MaturityD.ToString(convert); }
        }

        private E_MortgagePoolAgencyT m_AgencyT;
        public E_MortgagePoolAgencyT AgencyT
        {
            get { return m_AgencyT; }
            set { m_AgencyT = value; m_dirtyFields["AgencyT"] = value; }
        }
        public string AgencyT_rep
        {
            get
            {
                switch (AgencyT)
                {
                    case E_MortgagePoolAgencyT.FannieMae:
                        return "Fannie Mae";
                    case E_MortgagePoolAgencyT.FreddieMac:
                        return "Freddie Mac";
                    case E_MortgagePoolAgencyT.GinnieMae:
                        return "Ginnie Mae";
                    default:
                        return "";
                }
            }
        }

        private string m_InternalId;
        public string InternalId
        {
            get { return m_InternalId; }
            set { m_InternalId = value; m_dirtyFields["InternalId"] = value; }
        }

        public Guid? PublicId
        {
            get;
            private set;
        }

        private string m_BasePoolNumber;
        public string BasePoolNumber
        {
            get { return m_BasePoolNumber; }
            set { m_BasePoolNumber = value; m_dirtyFields["BasePoolNumber"] = value; }
        }

        private string m_IssueTypeCode;
        public string IssueTypeCode
        {
            get { return m_IssueTypeCode; }
            set { m_IssueTypeCode = value; m_dirtyFields["IssueTypeCode"] = value; }
        }

        private string m_PoolTypeCode;
        public string PoolTypeCode
        {
            get { return m_PoolTypeCode; }
            set { m_PoolTypeCode = value; m_dirtyFields["PoolTypeCode"] = value; }
        }

        private string m_IssuerID;
        public string IssuerID
        {
            get { return m_IssuerID; }
            set { m_IssuerID = value; m_dirtyFields["IssuerID"] = value; }
        }

        private string m_CustodianID;
        public string CustodianID
        {
            get { return m_CustodianID; }
            set { m_CustodianID = value; m_dirtyFields["CustodianID"] = value; }
        }

        private Guid? encryptionKeyId;
        private string unencryptedTaxId;

        public string TaxID
        {
            get { return this.unencryptedTaxId; }
            set
            {
                this.unencryptedTaxId = value;
                this.m_dirtyFields["TaxID"] = value;
            }
        }

        private decimal m_SecurityR;
        public decimal SecurityR
        {
            get { return m_SecurityR; }
            set { m_SecurityR = value; m_dirtyFields["SecurityR"] = value; }
        }

        public string SecurityR_rep
        {
            get { return convert.ToRateString(SecurityR); }
        }

        private int m_PoolTerm;
        public int Term
        {
            get { return m_PoolTerm; }
            set { m_PoolTerm = value; m_dirtyFields["PoolTerm"] = value; }
        }

        private E_MortgagePoolAmortMethT m_AmortizationMethodT;
        public E_MortgagePoolAmortMethT AmortizationMethodT
        {
            get { return m_AmortizationMethodT; }
            set { m_AmortizationMethodT = value; m_dirtyFields["AmortizationMethodT"] = value; }
        }
        public string AmortizationMethodT_rep
        {
            get
            {
                switch (AmortizationMethodT)
                {
                    case E_MortgagePoolAmortMethT.ConcurrentDate:
                        return "Concurrent Date";
                    case E_MortgagePoolAmortMethT.InternalReserve:
                        return "Internal Reserve";
                    default:
                        return "";
                }
            }
        }

        private E_sFinMethT m_AmortizationT;
        public E_sFinMethT AmortizationT
        {
            get { return m_AmortizationT; }
            set { m_AmortizationT = value; m_dirtyFields["AmortizationT"] = value; }
        }
        public string AmortizationT_rep
        {
            get
            {
                switch (AmortizationT)
                {
                    case E_sFinMethT.ARM:
                        return "ARM";
                    case E_sFinMethT.Fixed:
                        return "Fixed";
                    case E_sFinMethT.Graduated:
                        return "Graduated";
                    default:
                        return "";
                }
            }
        }

        private CDateTime m_BookEntryD;
        public CDateTime BookEntryD
        {
            get { return m_BookEntryD; }
            set { m_BookEntryD = value; m_dirtyFields["BookEntryD"] = value; }
        }
        public string BookEntryD_rep
        {
            get { return BookEntryD.ToString(convert); }
        }


        private string m_PoolPrefix;
        public string PoolPrefix
        {
            get { return m_PoolPrefix; }
            set { m_PoolPrefix = value; m_dirtyFields["PoolPrefix"] = value; }
        }


        private string m_PoolSuffix;
        public string PoolSuffix
        {
            get { return m_PoolSuffix; }
            set { m_PoolSuffix = value; m_dirtyFields["PoolSuffix"] = value; }
        }


        private E_sLT m_MortgageT;
        public E_sLT MortgageT
        {
            get { return m_MortgageT; }
            set { m_MortgageT = value; m_dirtyFields["MortgageT"] = value; }
        }


        private string m_SellerID;
        public string SellerID
        {
            get { return m_SellerID; }
            set { m_SellerID = value; m_dirtyFields["SellerID"] = value; }
        }


        private string m_ServicerID;
        public string ServicerID
        {
            get { return m_ServicerID; }
            set { m_ServicerID = value; m_dirtyFields["ServicerID"] = value; }
        }


        private string m_DocumentCustodianID;
        public string DocumentCustodianID
        {
            get { return m_DocumentCustodianID; }
            set { m_DocumentCustodianID = value; m_dirtyFields["DocumentCustodianID"] = value; }
        }


        private int? m_RemittanceD;
        public int? RemittanceD
        {
            get { return m_RemittanceD; }
            set { m_RemittanceD = value; m_dirtyFields["RemittanceD"] = value; }
        }
        public string RemittanceD_rep
        {
            get { return m_RemittanceD.ToString(); }
            set
            {
                RemittanceD = TryParseInt(value);
            }
        }

        private string m_InvestorName;
        public string InvestorName
        {
            get { return m_InvestorName; }
            set { m_InvestorName = value; m_dirtyFields["InvestorName"] = value; }
        }


        private E_CommitmentType m_CommitmentType;
        public E_CommitmentType CommitmentType
        {
            get { return m_CommitmentType; }
            set { m_CommitmentType = value; m_dirtyFields["CommitmentType"] = value; }
        }


        private E_SettlementType m_SettlementType;
        public E_SettlementType SettlementType
        {
            get { return m_SettlementType; }
            set { m_SettlementType = value; m_dirtyFields["SettlementType"] = value; }
        }


        private E_DeliveryType m_DeliveryType;
        public E_DeliveryType DeliveryType
        {
            get { return m_DeliveryType; }
            set { m_DeliveryType = value; m_dirtyFields["DeliveryType"] = value; }
        }


        private string m_CommitmentNum;
        public string CommitmentNum
        {
            get { return m_CommitmentNum; }
            set { m_CommitmentNum = value; m_dirtyFields["CommitmentNum"] = value; }
        }


        private CDateTime m_CommitmentDate;
        public CDateTime CommitmentDate
        {
            get { return m_CommitmentDate; }
            set { m_CommitmentDate = value; m_dirtyFields["CommitmentDate"] = value; }
        }

        public string CommitmentDate_rep
        {
            get { return CommitmentDate.ToString(convert); }
            set { CommitmentDate = CDateTime.Create(value, convert); }

        }


        private CDateTime m_CommitmentExpires;
        public CDateTime CommitmentExpires
        {
            get { return m_CommitmentExpires; }
            set { m_CommitmentExpires = value; m_dirtyFields["CommitmentExpires"] = value; }
        }
        public string CommitmentExpires_rep
        {
            get { return CommitmentExpires.ToString(convert); }
            set { CommitmentExpires = CDateTime.Create(value, convert); }
        }



        private bool m_CommitmentBasisLckd;
        public bool CommitmentBasisLckd
        {
            get { return m_CommitmentBasisLckd; }
            set { m_CommitmentBasisLckd = value; m_dirtyFields["CommitmentBasisLckd"] = value; }
        }


        private decimal m_CommitmentBasis;
        public decimal CommitmentBasisManual
        {
            get { return m_CommitmentBasis; }
            set { m_CommitmentBasis = value; m_dirtyFields["CommitmentBasis"] = value; }
        }


        private decimal m_Tolerance;
        public decimal Tolerance
        {
            get { return m_Tolerance; }
            set { m_Tolerance = value; m_dirtyFields["Tolerance"] = value; }
        }


        private E_BasePricingCalcType m_BasePricingCalcType;
        public E_BasePricingCalcType BasePricingCalcType
        {
            get { return m_BasePricingCalcType; }
            set { m_BasePricingCalcType = value; m_dirtyFields["BasePricingCalcType"] = value; }
        }


        private bool m_BasePricingPoolLevelLckd;
        public bool BasePricingPoolLevelLckd
        {
            get { return m_BasePricingPoolLevelLckd; }
            set { m_BasePricingPoolLevelLckd = value; m_dirtyFields["BasePricingPoolLevelLckd"] = value; }
        }


        private decimal m_BasePricingPoolLevelManual;
        public decimal BasePricingPoolLevelManual
        {
            get { return m_BasePricingPoolLevelManual; }
            set { m_BasePricingPoolLevelManual = value; m_dirtyFields["BasePricingPoolLevelManual"] = value; }
        }


        private decimal m_BasePricingManual;
        public decimal BasePricingManual
        {
            get { return m_BasePricingManual; }
            set { m_BasePricingManual = value; m_dirtyFields["BasePricingManual"] = value; }
        }


        private string m_BasePricingTableJSON;
        public string BasePricingTableJSON
        {
            get { return m_BasePricingTableJSON; }
            set
            {
                m_BasePricingTableJSON = value;
                if (string.IsNullOrEmpty(value))
                {
                    // 10/3/2014 dd - Construct an empty dictionary when input is empty string.
                    x_basePricingTable = new Dictionary<decimal, decimal>();
                }
                else
                {
                    x_basePricingTable = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<string, string>>(m_BasePricingTableJSON)
                        .ToDictionary(kvp => decimal.Parse(kvp.Key), kvp => decimal.Parse(kvp.Value));
                }
                m_dirtyFields["BasePricingTable"] = value;
            }
        }

        private Dictionary<decimal, decimal> x_basePricingTable = null;
        /// <summary>
        /// A table that maps loan note rate (sNoteIR) to price.
        /// </summary>
        public Dictionary<decimal, decimal> BasePricingTable
        {
            get
            {
                if (x_basePricingTable != null) return x_basePricingTable;

                if (string.IsNullOrEmpty(m_BasePricingTableJSON))
                {
                    x_basePricingTable = new Dictionary<decimal, decimal>();
                }
                else
                {
                    x_basePricingTable = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<string, string>>(m_BasePricingTableJSON)
                        .ToDictionary(kvp => decimal.Parse(kvp.Key), kvp => decimal.Parse(kvp.Value));
                }

                return x_basePricingTable;
            }
        }


        private E_LLPAType m_LLPAType;
        public E_LLPAType LLPAType
        {
            get { return m_LLPAType; }
            set { m_LLPAType = value; m_dirtyFields["LLPAType"] = value; }
        }


        private decimal m_LLPAManual;
        public decimal LLPAManual
        {
            get { return m_LLPAManual; }
            set { m_LLPAManual = value; m_dirtyFields["LLPAManual"] = value; }
        }


        private E_SRPType m_SRPType;
        public E_SRPType SRPType
        {
            get { return m_SRPType; }
            set { m_SRPType = value; m_dirtyFields["SRPType"] = value; }
        }


        private decimal m_SRPManual;
        public decimal SRPManual
        {
            get { return m_SRPManual; }
            set { m_SRPManual = value; m_dirtyFields["SRPManual"] = value; }
        }


        private decimal m_SRPPoolLevel;
        public decimal SRPPoolLevel
        {
            get { return m_SRPPoolLevel; }
            set { m_SRPPoolLevel = value; m_dirtyFields["SRPPoolLevel"] = value; }
        }


        private E_BlendPairOffCalcType m_BlendPairOffCalcType;
        public E_BlendPairOffCalcType BlendPairOffCalcType
        {
            get { return m_BlendPairOffCalcType; }
            set { m_BlendPairOffCalcType = value; m_dirtyFields["BlendPairOffCalcType"] = value; }
        }


        private decimal m_BlendPairOffMarketPrice;
        public decimal BlendPairOffMarketPrice
        {
            get { return m_BlendPairOffMarketPrice; }
            set { m_BlendPairOffMarketPrice = value; m_dirtyFields["BlendPairOffMarketPrice"] = value; }
        }




        public int? TryParseInt(string val)
        {
            int v;
            if (!int.TryParse(val, out v))
            {
                return null;
            }
            else
            {
                return v;
            }
        }



        private E_PoolStructureT m_StructureT;
        public E_PoolStructureT StructureT
        {
            get { return m_StructureT; }
            set { m_StructureT = value; m_dirtyFields["StructureT"] = value; }
        }

        private string m_FeatureCodesJSON;
        public string FeatureCodesJSON
        {
            get { return m_FeatureCodesJSON; }
            set { m_FeatureCodesJSON = value; m_dirtyFields["FeatureCodes"] = value; }

        }
        public List<string> FeatureCodes
        {
            get
            {
                if (string.IsNullOrEmpty(m_FeatureCodesJSON)) return new List<string>();
                return ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(m_FeatureCodesJSON);
            }
        }


        private int? m_OwnershipPc;
        public int? OwnershipPc
        {
            get { return m_OwnershipPc; }
            set { m_OwnershipPc = value; m_dirtyFields["OwnershipPc"] = value; }
        }

        public string OwnershipPc_rep
        {
            get { return convert.ToRateString(OwnershipPc); }
            set
            {
                OwnershipPc = TryParseInt(value);
            }
        }


        private bool m_IsInterestOnly;
        public bool IsInterestOnly
        {
            get { return m_IsInterestOnly; }
            set { m_IsInterestOnly = value; m_dirtyFields["IsInterestOnly"] = value; }
        }


        private bool m_IsBalloon;
        public bool IsBalloon
        {
            get { return m_IsBalloon; }
            set { m_IsBalloon = value; m_dirtyFields["IsBalloon"] = value; }
        }


        private bool m_IsAssumable;
        public bool IsAssumable
        {
            get { return m_IsAssumable; }
            set { m_IsAssumable = value; m_dirtyFields["IsAssumable"] = value; }
        }


        private string m_ARMPlanNum;
        public string ARMPlanNum
        {
            get { return m_ARMPlanNum; }
            set { m_ARMPlanNum = value; m_dirtyFields["ARMPlanNum"] = value; }
        }


        private E_PoolRoundingT m_RoundingT;
        public E_PoolRoundingT RoundingT
        {
            get { return m_RoundingT; }
            set { m_RoundingT = value; m_dirtyFields["RoundingT"] = value; }
        }

        public string RoundingPc_rep
        {
            get
            {
                return convert.ToRateString(GetRoundingPercent(m_RoundingT));
            }
        }

        private decimal GetRoundingPercent(E_PoolRoundingT rounding)
        {
            switch (rounding)
            {
                case E_PoolRoundingT.Down01:
                    return 0.01M;
                case E_PoolRoundingT.Down125:
                case E_PoolRoundingT.Nearest125:
                case E_PoolRoundingT.Up125:
                    return 0.125M;
                case E_PoolRoundingT.Down25:
                case E_PoolRoundingT.Nearest25:
                case E_PoolRoundingT.Up25:
                    return 0.25M;
                case E_PoolRoundingT.NoRounding:
                default: return 0M;

            }
        }


        private int m_InterestRateChangeLookbackDays;
        public int InterestRateChangeLookbackDays
        {
            get { return m_InterestRateChangeLookbackDays; }
            set { m_InterestRateChangeLookbackDays = value; m_dirtyFields["InterestRateChangeLookbackDays"] = value; }
        }

        public string InterestRateChangeLookbackDays_rep
        {
            get { return m_InterestRateChangeLookbackDays.ToString(); }
            set
            {
                int temp;
                int.TryParse(value, out temp);
                InterestRateChangeLookbackDays = temp;
            }
        }


        private decimal m_MBSMarginPc;
        public decimal MBSMarginPc
        {
            get { return m_MBSMarginPc; }
            set { m_MBSMarginPc = value; m_dirtyFields["MBSMarginPc"] = value; }
        }

        public string MBSMarginPc_rep { get { return convert.ToRateString(MBSMarginPc); } }


        private E_PoolAccrualRateStructT m_AccrualRateStructureT;
        public E_PoolAccrualRateStructT AccrualRateStructureT
        {
            get { return m_AccrualRateStructureT; }
            set { m_AccrualRateStructureT = value; m_dirtyFields["AccrualRateStructureType"] = value; }
        }


        private decimal m_FixedServicingFeePc;
        public decimal FixedServicingFeePc
        {
            get { return m_FixedServicingFeePc; }
            set { m_FixedServicingFeePc = value; m_dirtyFields["FixedServicingFeePc"] = value; }
        }

        public string FixedServicingFeePc_rep { get { return convert.ToRateString(FixedServicingFeePc); } }



        private decimal? m_MinAccrualR;
        public decimal? MinAccrualR
        {
            get { return m_MinAccrualR; }
            set { m_MinAccrualR = value; m_dirtyFields["MinAccrualR"] = value; }
        }
        public string MinAccrualR_rep { get { return convert.ToRateString(MinAccrualR); } }

        private decimal? m_MaxAccrualR;
        public decimal? MaxAccrualR
        {
            get { return m_MaxAccrualR; }
            set { m_MaxAccrualR = value; m_dirtyFields["MaxAccrualR"] = value; }
        }
        public string MaxAccrualR_rep { get { return convert.ToRateString(MaxAccrualR); } }

        public string CommitmentBasisManual_rep
        {
            get { return ToMoney(CommitmentBasis); }
            set { CommitmentBasisManual = FromMoney(value); }
        }

        public string Tolerance_rep
        {
            get { return ToPercent(Tolerance); }
            set { Tolerance = FromPercent(value); }
        }

        public string BasePricingPoolLevelManual_rep
        {
            get { return ToPrice(BasePricingPoolLevelManual); }
            set { BasePricingPoolLevelManual = FromPrice(value); }
        }


        public string BasePricingManual_rep
        {
            get { return ToMoney(BasePricingManual); }
            set { BasePricingManual = FromMoney(value); }
        }


        public string LLPAManual_rep
        {
            get { return ToMoney(LLPAManual); }
            set { LLPAManual = FromMoney(value); }
        }


        public string SRPManual_rep
        {
            get { return ToMoney(SRPManual); }
            set { SRPManual = FromMoney(value); }
        }


        public string SRPPoolLevel_rep
        {
            get { return ToPrice(SRPPoolLevel); }
            set { SRPPoolLevel = FromPrice(value); }
        }


        public string BlendPairOffMarketPrice_rep
        {
            get { return ToPrice(BlendPairOffMarketPrice); }
            set { BlendPairOffMarketPrice = FromPrice(value); }
        }




        private decimal m_SecurityRMargin;
        public decimal SecurityRMargin
        {
            get { return m_SecurityRMargin; }
            set { m_SecurityRMargin = value; m_dirtyFields["SecurityRMargin"] = value; }
        }
        public string SecurityRMargin_rep
        {
            get { return convert.ToRateString(SecurityRMargin); }
        }

        private CDateTime m_SecurityChangeD;
        public CDateTime SecurityChangeD
        {
            get { return m_SecurityChangeD; }
            set { m_SecurityChangeD = value; m_dirtyFields["SecurityChangeD"] = value; }
        }
        public string SecurityChangeD_rep
        {
            get { return m_SecurityChangeD.ToString(convert); }
        }

        private E_MortgagePoolIndexT m_PoolIndexT;
        public E_MortgagePoolIndexT IndexT
        {
            get { return m_PoolIndexT; }
            set { m_PoolIndexT = value; m_dirtyFields["PoolIndexT"] = value; }
        }
        public string IndexT_rep
        {
            get
            {
                switch (IndexT)
                {
                    case E_MortgagePoolIndexT.CMT:
                        return "CMT";
                    case E_MortgagePoolIndexT.LIBOR:
                        return "LIBOR";
                    default:
                        return "";
                }
            }
        }

        private bool m_IsBondFinancePool;
        public bool IsBondFinancePool
        {
            get { return m_IsBondFinancePool; }
            set { m_IsBondFinancePool = value; m_dirtyFields["IsBondFinancePool"] = value; }
        }

        private E_MortgagePoolCertAndAgreementT m_CertAndAgreementT;
        public E_MortgagePoolCertAndAgreementT CertAndAgreementT
        {
            get { return m_CertAndAgreementT; }
            set { m_CertAndAgreementT = value; m_dirtyFields["CertAndAgreementT"] = value; }
        }

        private bool m_IsFormHUD11711ASendToDocumentCustodian;
        public bool IsFormHUD11711ASentToDocumentCustodian
        {
            get { return m_IsFormHUD11711ASendToDocumentCustodian; }
            set { m_IsFormHUD11711ASendToDocumentCustodian = value; m_dirtyFields["IsFormHUD11711ASendToDocumentCustodian"] = value; }
        }
        
        private string m_PIAccountNum;
        /// <summary>
        /// The Principal and Interest bank account number for this pool.
        /// </summary>
        public Sensitive<string> PIAccountNum
        {
            get
            {
                return this.m_PIAccountNum;
            }

            set
            {
                this.m_PIAccountNum = value.Value;
                this.m_dirtyFields["PIAccountNum"] = value.Value;
            }
        }

        private string m_PIRoutingNum;
        /// <summary>
        /// The Principal and Interest routing number for this pool.
        /// </summary>
        public Sensitive<string> PIRoutingNum
        {
            get
            {
                return this.m_PIRoutingNum;
            }

            set
            {
                this.m_PIRoutingNum = value.Value;
                this.m_dirtyFields["PIRoutingNum"] = value.Value;
            }
        }

        private string m_TIAccountNum;
        /// <summary>
        /// The Taxes and Insurance bank account number for this pool.
        /// </summary>
        public Sensitive<string> TIAccountNum
        {
            get
            {
                return this.m_TIAccountNum;
            }

            set
            {
                this.m_TIAccountNum = value.Value;
                this.m_dirtyFields["TaxAndInsuranceAccountNumber"] = value.Value;
            }
        }

        private string m_TIRoutingNum;
        /// <summary>
        /// The Taxes and Insurance routing number for this pool.
        /// </summary>
        public Sensitive<string> TIRoutingNum
        {
            get
            {
                return this.m_TIRoutingNum;
            }

            set
            {
                this.m_TIRoutingNum = value.Value;
                this.m_dirtyFields["TaxAndInsuranceRoutingNumber"] = value.Value;
            }
        }

        /// <summary>
        /// A list of settlement account objects associated with the pool.
        /// </summary>
        public List<SettlementAccount> SettlementAccounts = new List<SettlementAccount>();

        /// <summary>
        /// Contains account number, routing number, ABA name, and third party account name for settlement accounts associated with the pool.
        /// </summary>
        public class SettlementAccount
        {
            /// <summary>
            /// Creates a new instance of the <see cref="SettlementAccount"/> class.
            /// </summary>
            /// <param name="parent">The Pool which will contain this Account. For setting dirty fields and writing to the database.</param>
            /// <param name="sequenceNumber">The number of this account in sequence. 1-based because it corresponds to names in the DB.</param>
            /// <param name="accountNumber">The bank account number.</param>
            /// <param name="routingNumber">The bank routing number.</param>
            /// <param name="abaName">The American Banking Assosciation abbreviated name of the account holder.</param>
            /// <param name="thirdPartyAccountName">The third party account name.</param>
            public SettlementAccount(MortgagePool parent, int sequenceNumber, string accountNumber, string routingNumber, string abaName, string thirdPartyAccountName, decimal originalSubscriptionAmount)
            {
                this.Parent = parent;
                this.SequenceNumber = sequenceNumber;
                this.AccountNumber = accountNumber;
                this.RoutingNumber = routingNumber;
                this.ABAName = abaName;
                this.ThirdPartyAccountName = thirdPartyAccountName;
                this.SecurityInvestorOriginalSubscriptionAmount = originalSubscriptionAmount;
            }

            /// <summary>
            /// The Pool which will contain this Account. For setting dirty fields and writing to the database.
            /// </summary>
            private MortgagePool Parent;

            /// <summary>
            /// The number of this account in sequence. 1-based because it corresponds to names in the DB.
            /// </summary>
            private int SequenceNumber;
            
            private string m_AccountNumber;
            
            public Sensitive<string> AccountNumber
            {
                get { return m_AccountNumber; }
                set
                {
                    m_AccountNumber = value.Value;
                    Parent.m_dirtyFields["SettlementAccountNumber" + this.SequenceNumber] = value.Value;
                }
            }
            
            private string m_RoutingNumber;
            
            public Sensitive<string> RoutingNumber
            {
                get { return m_RoutingNumber; }
                set
                {
                    m_RoutingNumber = value.Value;
                    Parent.m_dirtyFields["SettlementRoutingNumber" + this.SequenceNumber] = value.Value;
                }
            }

            private string m_ABAName;
            
            public string ABAName
            {
                get { return m_ABAName; }
                set
                {
                    m_ABAName = value;
                    Parent.m_dirtyFields["SettlementABAName" + this.SequenceNumber] = value;
                }
            }

            private string m_ThirdPartyAccountName;

            public string ThirdPartyAccountName
            {
                get { return m_ThirdPartyAccountName; }
                set
                {
                    m_ThirdPartyAccountName = value;
                    Parent.m_dirtyFields["SettlementThirdPartyAccountName" + this.SequenceNumber] = value;
                }
            }

            private decimal m_SecurityInvestorOriginalSubscriptionAmount;
            public decimal SecurityInvestorOriginalSubscriptionAmount
            {
                get { return m_SecurityInvestorOriginalSubscriptionAmount; }
                set
                {
                    m_SecurityInvestorOriginalSubscriptionAmount = value;
                    Parent.m_dirtyFields["SecurityInvestorOriginalSubscriptionAmount" + this.SequenceNumber] = value;
                }
            }
        }

        private bool m_GovernmentBondFinance;
        public bool GovernmentBondFinance
        {
            get { return m_GovernmentBondFinance; }
            set
            {
                m_GovernmentBondFinance = value;
                this.m_dirtyFields["GovernmentBondFinance"] = value;
            }
        }

        private E_GovernmentBondFinancingProgramType m_GovernmentBondFinancingProgramType;
        public E_GovernmentBondFinancingProgramType GovernmentBondFinancingProgramType
        {
            get { return m_GovernmentBondFinancingProgramType; }
            set
            {
                m_GovernmentBondFinancingProgramType = value;
                this.m_dirtyFields["GovernmentBondFinancingProgramType"] = value;
            }
        }

        private string m_GovernmentBondFinancingProgramName;
        public string GovernmentBondFinancingProgramName
        {
            get { return m_GovernmentBondFinancingProgramName; }
            set
            {
                m_GovernmentBondFinancingProgramName = value;
                this.m_dirtyFields["GovernmentBondFinancingProgramName"] = value;
            }
        }

        private E_sStatusT m_SearchMinimumStatusT;
        public E_sStatusT SearchMinimumStatusT
        {
            get { return m_SearchMinimumStatusT; }
            set { m_SearchMinimumStatusT = value; m_dirtyFields["SearchMinimumStatusT"] = value; }
        }

        //Computed Fields:
        public decimal TotalCurrentBalance { get; private set; }
        public string TotalCurrentBalance_rep
        {
            get { return convert.ToMoneyString(TotalCurrentBalance, FormatDirection.ToRep); }
        }

        public decimal LowR { get; private set; }
        public string LowR_rep
        {
            get { return convert.ToRateString(LowR); }
        }

        public decimal HighR { get; private set; }
        public string HighR_rep
        {
            get { return convert.ToRateString(HighR); }
        }

        public int LoanCount { get; private set; }

        public decimal BackEndBasePricing { get; private set; }
        public string BackEndBasePricing_rep
        {
            get
            {
                return ToPrice(BackEndBasePricing);
            }
        }

        public decimal BasePricingPoolLevel
        {
            get
            {
                if (BasePricingPoolLevelLckd) return BasePricingPoolLevelManual;
                return CalculatePoolLevelBasePricing();
            }
        }

        public decimal UnpaidBalanceAmountAllocated
        {
            get { return this.TotalCurrentBalance; }
        }
        public string UnpaidBalanceAmountAllocated_rep
        {
            get { return this.TotalCurrentBalance_rep; }
        }

        public decimal UnpaidBalanceAmountShipped { get; private set; }
        public string UnpaidBalanceAmountShipped_rep
        {
            get { return this.convert.ToMoneyString(this.UnpaidBalanceAmountShipped, FormatDirection.ToRep); }
        }

        public decimal UnpaidBalanceAmountPurchased { get; private set; }
        public string UnpaidBalanceAmountPurchased_rep
        {
            get { return this.convert.ToMoneyString(this.UnpaidBalanceAmountPurchased, FormatDirection.ToRep); }
        }

        public decimal ProfitabilityAmountAllocated { get; private set; }
        public string ProfitabilityAmountAllocated_rep
        {
            get { return this.convert.ToMoneyString(this.ProfitabilityAmountAllocated, FormatDirection.ToRep); }
        }

        public decimal ProfitabilityAmountShipped { get; private set; }
        public string ProfitabilityAmountShipped_rep
        {
            get { return this.convert.ToMoneyString(this.ProfitabilityAmountShipped, FormatDirection.ToRep); }
        }

        public decimal ProfitabilityAmountPurchased { get; private set; }
        public string ProfitabilityAmountPurchased_rep
        {
            get { return this.convert.ToMoneyString(this.ProfitabilityAmountPurchased, FormatDirection.ToRep); }
        }

        public decimal FinalPriceAmt
        {
            get { return this.BasePricingAmt + this.BlendPairOffAmt + this.LLPAAmt + this.SRPAmt; }
        }

        public string FinalPriceAmt_rep
        {
            get { return this.convert.ToMoneyString(this.FinalPriceAmt, FormatDirection.ToRep); }
        }

        private decimal RawFrontEndRateLockPricing { get; set; }

        public decimal TotalProfit
        {
            get { return this.FinalPriceAmt - this.RawFrontEndRateLockPricing; }
        }
        public string TotalProfit_rep
        {
            get { return this.convert.ToMoneyString(this.TotalProfit, FormatDirection.ToRep); }
        }

        private decimal? x_commitmentBasis;
        private decimal? x_calcPoolLevelBasePricing;
        public decimal CalculatePoolLevelBasePricing()
        {
            if (!x_calcPoolLevelBasePricing.HasValue) CalcTradeStats();
            return x_calcPoolLevelBasePricing.Value;

        }

        private void CalcTradeStats()
        {
            //This is terrible, the trade statistics should be available as raw decimals
            var TradeStats = new TradeStatisticsDTO(GetTrades());
            decimal basePricing;
            if (decimal.TryParse(TradeStats.WeightedAvgPrice, out basePricing))
            {
                x_calcPoolLevelBasePricing = basePricing;
            }
            else
            {
                x_calcPoolLevelBasePricing = 0m;
            }
            x_commitmentBasis = convert.ToMoney(TradeStats.Amount);
        }

        public decimal OverUnderTolerance
        {
            get
            {
                if (TotalCurrentBalance >= CommitmentBasis)
                {
                    return Math.Max(0, TotalCurrentBalance - CommitmentBasis * (1 + Tolerance));
                }
                else
                {
                    return Math.Min(0, TotalCurrentBalance - CommitmentBasis * (1 - Tolerance));
                }
            }
        }

        public string OverUnderTolerance_rep { get { return ToMoney(OverUnderTolerance); } }

        public decimal CommitmentBasisCalc
        {
            get
            {
                if (!x_commitmentBasis.HasValue) CalcTradeStats();
                return x_commitmentBasis.Value;
            }
        }

        public decimal CommitmentBasis
        {
            get
            {
                if (CommitmentBasisLckd) return CommitmentBasisManual;
                return CommitmentBasisCalc;
            }
        }

        public string CommitmentBasis_rep { get { return ToMoney(CommitmentBasis); } }

        public decimal BasePricingAmt
        {
            get
            {
                if (BasePricingCalcType == E_BasePricingCalcType.Manually) return BasePricingManual;

                return TotalCurrentBalance * BasePrice / 100;
            }
        }

        public decimal BasePrice
        {
            get
            {
                switch (BasePricingCalcType)
                {
                    case (E_BasePricingCalcType.FromBackEnd): return BackEndBasePricing;
                    case (E_BasePricingCalcType.Manually):
                        if (this.TotalCurrentBalance == 0)
                        {
                            return 0;
                        }

                        return BasePricingManual / TotalCurrentBalance;
                    case (E_BasePricingCalcType.PoolLevel): return BasePricingPoolLevel;
                    case (E_BasePricingCalcType.Table): return BasePricingTablePrice;
                    default: throw new NotSupportedException();
                }
            }
        }

        private decimal BasePricingTablePrice
        {
            get
            {
                if (TotalCurrentBalance == 0) return 0;

                decimal total = 0;
                foreach (var rateAndAmt in NoteRateToLoanAmount)
                {
                    var rate = rateAndAmt.Key;
                    var amts = rateAndAmt.Value;
                    decimal price;
                    if (!BasePricingTable.TryGetValue(rate, out price))
                    {
                        price = 0;
                    }

                    foreach (var amt in amts)
                    {
                        total += price * amt;
                    }
                }

                return total / TotalCurrentBalance;
            }
        }

        private decimal? x_BackendLLPA;
        public decimal CalculateBackendLLPA()
        {
            if (x_BackendLLPA.HasValue) return x_BackendLLPA.Value;
            CalculateSRPAndLLPA();
            return x_BackendLLPA.Value;
        }

        private decimal? x_BackendSRP;
        public decimal CalculateBackendSRP()
        {
            if (x_BackendSRP.HasValue) return x_BackendSRP.Value;
            CalculateSRPAndLLPA();
            return x_BackendSRP.Value;
        }

        private void CalculateSRPAndLLPA()
        {
            x_BackendLLPA = 0;
            x_BackendSRP = 0;
            //We can't calculate either of these without a TotalCurrentBalance, so don't even try unless we have it.
            if (TotalCurrentBalance == 0) return;

            CalculatePerLoanStatistics();
        }

        private Dictionary<decimal, List<decimal>> x_noteRateTable = null;
        public Dictionary<decimal, List<decimal>> NoteRateToLoanAmount
        {
            get
            {
                if (x_noteRateTable != null) return x_noteRateTable;
                CalculatePerLoanStatistics();
                return x_noteRateTable;
            }
        }

        //This is a temporary hack because adding an "is SRP" checkbox was not included in the spec.
        //Adjustments already have an isSRP field, it's just always false.
        private static Regex isSRP = new Regex("^(SRP$|SRP[^a-zA-Z])", RegexOptions.Compiled);
        //These are expensive because they require loading every loan in the file, but they can all be calculated in the same pass
        private void CalculatePerLoanStatistics()
        {
            decimal SRPAmt = 0;
            decimal LLPAAmt = 0;
            x_BackendLLPA = 100;
            x_BackendSRP = 100;

            using (PerformanceMonitorItem.Time("MortgagePools calculating per-loan statistics for pool id " + PoolId))
            {
                x_noteRateTable = new Dictionary<decimal, List<decimal>>();
                foreach (var id in FetchLoansInPool())
                {
                    var data = new CPageData(id, "MortgagePool", new[] { "sInvestorLockAdjustments", "sServicingUnpaidPrincipalBalance", "sNoteIR" });
                    data.InitLoad();

                    if (!x_noteRateTable.ContainsKey(data.sNoteIR)) x_noteRateTable[data.sNoteIR] = new List<decimal>();
                    x_noteRateTable[data.sNoteIR].Add(data.sServicingUnpaidPrincipalBalance);

                    foreach (var adjustment in data.sInvestorLockAdjustments)
                    {
                        decimal priceTemp;
                        if (decimal.TryParse(adjustment.Price.Trim('%'), out priceTemp))
                        {
                            var amt = priceTemp / 100 * data.sServicingUnpaidPrincipalBalance;
                            if (adjustment.IsSRPAdjustment || isSRP.Match(adjustment.Description).Success)
                            {
                                SRPAmt += amt;
                            }
                            else
                            {
                                LLPAAmt += amt;
                            }
                        }
                    }
                }
            }
            if (TotalCurrentBalance == 0) return;
            x_BackendLLPA = LLPAAmt / TotalCurrentBalance * 100;
            x_BackendSRP = SRPAmt / TotalCurrentBalance * 100;
        }

        public decimal LLPAAmt
        {
            get
            {
                if (LLPAType == E_LLPAType.Manually) return LLPAManual;
                else return CalculateBackendLLPA() * TotalCurrentBalance / 100;
            }
        }

        public decimal SRPAmt
        {
            get
            {
                switch (SRPType)
                {
                    case (E_SRPType.FromBackEnd): return CalculateBackendSRP() * TotalCurrentBalance / 100;
                    case (E_SRPType.Manually): return SRPManual;
                    case (E_SRPType.PoolLevel): return SRPPoolLevel * TotalCurrentBalance / 100;
                    default: throw new NotSupportedException();
                }
            }
        }

        public decimal BlendPairOffAmt
        {
            get
            {
                var worstCase = Math.Min(OverUnderTolerance * -BlendPairOffAmtPartial, 0);
                switch (BlendPairOffCalcType)
                {
                    case (E_BlendPairOffCalcType.None): return 0m;

                    case (E_BlendPairOffCalcType.CurrentMarket):
                        if (OverUnderTolerance < 0) return worstCase;
                        return OverUnderTolerance * -BlendPairOffAmtPartial;

                    case (E_BlendPairOffCalcType.WorstCase): return worstCase;
                    default: throw new NotSupportedException();
                }
            }
        }

        public decimal BlendPairOffPrice
        {
            get
            {
                if (BlendPairOffCalcType == E_BlendPairOffCalcType.None || TotalCurrentBalance == 0) return 0;
                return (BasePricingAmt + BlendPairOffAmt) / TotalCurrentBalance * 100;
            }
        }

        private decimal BlendPairOffAmtPartial
        {
            get
            {
                if (TotalCurrentBalance == 0) return 0;
                return BasePricingAmt / TotalCurrentBalance - BlendPairOffMarketPrice / 100;
            }
        }

        public string BackendLLPA_rep { get { return ToPrice(CalculateBackendLLPA()); } }
        public string BackendSRP_rep { get { return ToPrice(CalculateBackendSRP()); } }

        public string BasePricingAmt_rep { get { return ToMoney(BasePricingAmt); } }
        public string LLPAAmt_rep { get { return ToMoney(LLPAAmt); } }
        public string SRPAmt_rep { get { return ToMoney(SRPAmt); } }

        public string BlendPairOffAmt_rep { get { return ToMoney(BlendPairOffAmt); } }
        public string BlendPairOffPrice_rep { get { return ToPrice(BlendPairOffPrice); } }

        private CDateTime earlyDeliveryDate;
        public CDateTime EarlyDeliveryDate
        {
            get
            {
                return this.earlyDeliveryDate;
            }
            set
            {
                this.earlyDeliveryDate = value;
                this.m_dirtyFields["EarlyDeliveryDate"] = value;
            }
        }

        public string EarlyDeliveryDate_rep
        {
            get { return this.EarlyDeliveryDate.ToString(this.convert); }
            set { this.EarlyDeliveryDate = CDateTime.Create(value, this.convert); }
        }

        private CDateTime assignDate;
        public CDateTime AssignDate
        {
            get
            {
                return this.assignDate;
            }
            set
            {
                this.assignDate = value;
                this.m_dirtyFields["AssignDate"] = value;
            }
        }

        public string AssignDate_rep
        {
            get { return this.AssignDate.ToString(this.convert); }
            set { this.AssignDate = CDateTime.Create(value, this.convert); }
        }

        private CDateTime confirmDate;
        public CDateTime ConfirmDate
        {
            get
            {
                return this.confirmDate;
            }
            set
            {
                this.confirmDate = value;
                this.m_dirtyFields["ConfirmDate"] = value;
            }
        }

        public string ConfirmDate_rep
        {
            get { return this.ConfirmDate.ToString(this.convert); }
            set { this.ConfirmDate = CDateTime.Create(value, this.convert); }
        }

        public decimal OpenBalance
        {
            get
            {
                return this.CommitmentBasis - this.TotalCurrentBalance;
            }
        }

        public string OpenBalance_rep { get { return ToMoney(this.OpenBalance); } }

        private string notes;
        public string Notes
        {
            get
            {
                return this.notes;
            }
            set
            {
                this.notes = value;
                this.m_dirtyFields["Notes"] = value;
            }
        }

        #endregion

        #region Constructors

        public MortgagePool(long poolid) : this(poolid, PrincipalFactory.CurrentPrincipal.BrokerId)
        {
        }

        public MortgagePool(long poolid, Guid brokerId)
        {
            convert = new LosConvert();
            PoolId = poolid;
            m_nullableFields = new List<string>();
            m_dirtyFields = new Dictionary<string, object>();
            LoadData(brokerId);
        }

        #endregion

        #region Methods
        private void SetTaxIdFields(DataRow row)
        {
            var encryptedTaxId = row["TaxIdEncrypted"] as byte[];
            this.encryptionKeyId = (Guid)row["EncryptionKeyId"];
            var encryptionKey = EncryptionKeyIdentifier.Create(this.encryptionKeyId.Value).Value;
            var decryptedValue = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey, encryptedTaxId);
            this.unencryptedTaxId = decryptedValue ?? string.Empty;
        }

        public string ToMoney(decimal input)
        {
            return convert.ToMoneyString(input, FormatDirection.ToRep);
        }

        public decimal FromMoney(string input)
        {
            return convert.ToMoney(input);
        }

        public string ToPrice(decimal input)
        {
            return input.ToString("##0.000000");
        }

        public decimal FromPrice(string input)
        {
            decimal res = 0;
            decimal.TryParse(input, out res);
            return res;
        }

        public string ToPercent(decimal input)
        {
            return convert.ToRateString(input);
        }

        public decimal FromPercent(string input)
        {
            return convert.ToRate(input);
        }

        public IPoolExporter GetExporter(bool pdd)
        {
            switch (AgencyT)
            {
                case E_MortgagePoolAgencyT.FannieMae:
                case E_MortgagePoolAgencyT.FreddieMac:
                    return new UlddPoolExporter(PoolId);
                case E_MortgagePoolAgencyT.GinnieMae:
                    return pdd ? (IPoolExporter)new GinnieMaePddExporter(PoolId) : (IPoolExporter)new GinnieNetExporter(PoolId);
                default:
                    return null;
            }
        }

        

        private void LoadData(Guid brokerId)
        {
            try
            {
                DataRow m_poolDataRow;
                DataSet m_dsPool = StoredProcedureHelper.ExecuteDataSet(brokerId, "MortgagePoolLoad", new SqlParameter("@PoolId", PoolId));
                if (m_dsPool.Tables[0].Rows.Count > 0)
                {
                    m_poolDataRow = m_dsPool.Tables[0].Rows[0];
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "No Mortgage Pool with PoolId=" + PoolId);
                }

                m_brokerId = (Guid)m_poolDataRow["BrokerId"];
                if (brokerId != m_brokerId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Current User BrokerId != Pool " + PoolId + " BrokerId");
                }
                m_OpenedD = ReadDateTime(m_poolDataRow, "OpenedD");
                m_DeliveredD = ReadDateTime(m_poolDataRow, "DeliveredD");
                m_ClosedD = ReadDateTime(m_poolDataRow, "ClosedD");
                m_IssueD = ReadDateTime(m_poolDataRow, "IssueD");
                m_SettlementD = ReadDateTime(m_poolDataRow, "SettlementD");
                m_InitialPmtD = ReadDateTime(m_poolDataRow, "InitialPmtD");
                m_UnpaidBalanceD = ReadDateTime(m_poolDataRow, "UnpaidBalanceD");
                m_MaturityD = ReadDateTime(m_poolDataRow, "MaturityD");
                m_SecurityChangeD = ReadDateTime(m_poolDataRow, "SecurityChangeD");
                PublicId = m_poolDataRow["PublicId"] as Guid?;
                m_InternalId = m_poolDataRow["InternalId"].ToString();

                m_BasePoolNumber = m_poolDataRow["BasePoolNumber"].ToString();
                m_IssueTypeCode = m_poolDataRow["IssueTypeCode"].ToString();
                m_PoolTypeCode = m_poolDataRow["PoolTypeCode"].ToString();
                m_IssuerID = m_poolDataRow["IssuerID"].ToString();
                m_CustodianID = m_poolDataRow["CustodianID"].ToString();
                this.SetTaxIdFields(m_poolDataRow);
                m_PIAccountNum = m_poolDataRow["PIAccountNum"].ToString();
                m_PIRoutingNum = m_poolDataRow["PIRoutingNum"].ToString();

                m_TIAccountNum = m_poolDataRow["TaxAndInsuranceAccountNumber"].ToString();
                m_TIRoutingNum = m_poolDataRow["TaxAndInsuranceRoutingNumber"].ToString();

                for (int i = 1; i <= num_SettlementAccounts; i++)
                {
                    if (SettlementAccounts.Count >= i)
                    {
                        SettlementAccounts[i - 1].AccountNumber = m_poolDataRow["SettlementAccountNumber" + i].ToString();
                        SettlementAccounts[i - 1].RoutingNumber = m_poolDataRow["SettlementRoutingNumber" + i].ToString();
                        SettlementAccounts[i - 1].ABAName = m_poolDataRow["SettlementABAName" + i].ToString();
                        SettlementAccounts[i - 1].ThirdPartyAccountName = m_poolDataRow["SettlementThirdPartyAccountName" + i].ToString();
                        try
                        { 
                            SettlementAccounts[0].SecurityInvestorOriginalSubscriptionAmount = Convert.ToDecimal(m_poolDataRow["SecurityInvestorOriginalSubscriptionAmount" + i]);
                        }
                        catch (InvalidCastException)
                        {
                            SettlementAccounts[0].SecurityInvestorOriginalSubscriptionAmount = 0;
                        }
                    }
                    else
                    {
                        decimal originalSubscriptionAmount;
                        try
                        {
                            originalSubscriptionAmount = Convert.ToDecimal(m_poolDataRow["SecurityInvestorOriginalSubscriptionAmount" + i]);
                        }
                        catch (InvalidCastException)
                        {
                            originalSubscriptionAmount = 0;
                        }
                        SettlementAccounts.Add(new SettlementAccount(this, i,
                            m_poolDataRow["SettlementAccountNumber" + i].ToString(),
                            m_poolDataRow["SettlementRoutingNumber" + i].ToString(),
                            m_poolDataRow["SettlementABAName" + i].ToString(),
                            m_poolDataRow["SettlementThirdPartyAccountName" + i].ToString(),
                            originalSubscriptionAmount));
                    }
                }

                m_AgencyT = (E_MortgagePoolAgencyT)Enum.Parse(typeof(E_MortgagePoolAgencyT), m_poolDataRow["AgencyT"].ToString());
                m_AmortizationT = (E_sFinMethT)Enum.Parse(typeof(E_sFinMethT), m_poolDataRow["AmortizationT"].ToString());
                m_AmortizationMethodT = (E_MortgagePoolAmortMethT)Enum.Parse(typeof(E_MortgagePoolAmortMethT), m_poolDataRow["AmortizationMethodT"].ToString());
                m_PoolIndexT = (E_MortgagePoolIndexT)Enum.Parse(typeof(E_MortgagePoolIndexT), m_poolDataRow["PoolIndexT"].ToString());
                m_CertAndAgreementT = (E_MortgagePoolCertAndAgreementT)Enum.Parse(typeof(E_MortgagePoolCertAndAgreementT), m_poolDataRow["CertAndAgreementT"].ToString());
                m_SearchMinimumStatusT = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), m_poolDataRow["SearchMinimumStatusT"].ToString());

                try { m_SecurityR = Convert.ToDecimal(m_poolDataRow["SecurityR"]); }
                catch (InvalidCastException) { }
                try { m_PoolTerm = Convert.ToInt32(m_poolDataRow["PoolTerm"]); }
                catch (InvalidCastException) { }
                try { m_SecurityRMargin = Convert.ToDecimal(m_poolDataRow["SecurityRMargin"]); }
                catch (InvalidCastException) { }
                try { m_IsBondFinancePool = Convert.ToBoolean(m_poolDataRow["IsBondFinancePool"]); }
                catch (InvalidCastException) { }
                try { m_IsFormHUD11711ASendToDocumentCustodian = Convert.ToBoolean(m_poolDataRow["IsFormHUD11711ASendToDocumentCustodian"]); }
                catch (InvalidCastException) { }

                TotalCurrentBalance = Convert.ToDecimal(m_poolDataRow["TotalCurrentBalance"]);
                LowR = Convert.ToDecimal(m_poolDataRow["LowR"]);
                HighR = Convert.ToDecimal(m_poolDataRow["HighR"]);
                LoanCount = Convert.ToInt32(m_poolDataRow["LoanCount"]);

                m_BookEntryD = ReadDateTime(m_poolDataRow, "BookEntryD");
                m_PoolPrefix = m_poolDataRow["PoolPrefix"].ToString();
                m_PoolSuffix = m_poolDataRow["PoolSuffix"].ToString();
                m_MortgageT = (E_sLT)Convert.ToInt32(m_poolDataRow["MortgageT"]);
                m_SellerID = m_poolDataRow["SellerID"].ToString();
                m_ServicerID = m_poolDataRow["ServicerID"].ToString();
                m_DocumentCustodianID = m_poolDataRow["DocumentCustodianID"].ToString();
                m_RemittanceD = ToNullable<int>(m_poolDataRow, "RemittanceD", Convert.ToInt32);
                m_StructureT = (E_PoolStructureT)Convert.ToInt32(m_poolDataRow["StructureT"]);
                m_FeatureCodesJSON = m_poolDataRow["FeatureCodes"].ToString();
                m_OwnershipPc = ToNullable<int>(m_poolDataRow, "OwnershipPc", Convert.ToInt32);
                m_IsInterestOnly = Convert.ToBoolean(m_poolDataRow["IsInterestOnly"]);
                m_IsBalloon = Convert.ToBoolean(m_poolDataRow["IsBalloon"]);
                m_IsAssumable = Convert.ToBoolean(m_poolDataRow["IsAssumable"]);
                m_ARMPlanNum = m_poolDataRow["ARMPlanNum"].ToString();
                m_RoundingT = (E_PoolRoundingT)Convert.ToInt32(m_poolDataRow["RoundingT"]);
                m_InterestRateChangeLookbackDays = Convert.ToInt32(m_poolDataRow["InterestRateChangeLookbackDays"]);
                m_MBSMarginPc = Convert.ToDecimal(m_poolDataRow["MBSMarginPc"]);
                m_AccrualRateStructureT = (E_PoolAccrualRateStructT)Convert.ToInt32(m_poolDataRow["AccrualRateStructureType"]);
                m_FixedServicingFeePc = Convert.ToDecimal(m_poolDataRow["FixedServicingFeePc"]);
                m_MinAccrualR = ToNullable<decimal>(m_poolDataRow, "MinAccrualR", Convert.ToDecimal);
                m_MaxAccrualR = ToNullable<decimal>(m_poolDataRow, "MaxAccrualR", Convert.ToDecimal);

                m_InvestorName = m_poolDataRow["InvestorName"].ToString();
                m_CommitmentType = (E_CommitmentType)Convert.ToInt32(m_poolDataRow["CommitmentType"]);
                m_SettlementType = (E_SettlementType)Convert.ToInt32(m_poolDataRow["SettlementType"]);
                m_DeliveryType = (E_DeliveryType)Convert.ToInt32(m_poolDataRow["DeliveryType"]);
                m_CommitmentNum = m_poolDataRow["CommitmentNum"].ToString();
                m_CommitmentDate = ReadDateTime(m_poolDataRow, "CommitmentDate");
                m_CommitmentExpires = ReadDateTime(m_poolDataRow, "CommitmentExpires");
                m_CommitmentBasisLckd = Convert.ToBoolean(m_poolDataRow["CommitmentBasisLckd"]);
                m_CommitmentBasis = Convert.ToDecimal(m_poolDataRow["CommitmentBasis"]);
                m_Tolerance = Convert.ToDecimal(m_poolDataRow["Tolerance"]);
                m_BasePricingCalcType = (E_BasePricingCalcType)Convert.ToInt32(m_poolDataRow["BasePricingCalcType"]);
                m_BasePricingPoolLevelLckd = Convert.ToBoolean(m_poolDataRow["BasePricingPoolLevelLckd"]);
                m_BasePricingPoolLevelManual = Convert.ToDecimal(m_poolDataRow["BasePricingPoolLevelManual"]);
                m_BasePricingManual = Convert.ToDecimal(m_poolDataRow["BasePricingManual"]);
                m_BasePricingTableJSON = m_poolDataRow["BasePricingTable"].ToString();
                m_LLPAType = (E_LLPAType)Convert.ToInt32(m_poolDataRow["LLPAType"]);
                m_LLPAManual = Convert.ToDecimal(m_poolDataRow["LLPAManual"]);
                m_SRPType = (E_SRPType)Convert.ToInt32(m_poolDataRow["SRPType"]);
                m_SRPManual = Convert.ToDecimal(m_poolDataRow["SRPManual"]);
                m_SRPPoolLevel = Convert.ToDecimal(m_poolDataRow["SRPPoolLevel"]);
                m_BlendPairOffCalcType = (E_BlendPairOffCalcType)Convert.ToInt32(m_poolDataRow["BlendPairOffCalcType"]);
                m_BlendPairOffMarketPrice = Convert.ToDecimal(m_poolDataRow["BlendPairOffMarketPrice"]);
                m_GovernmentBondFinance = Convert.ToBoolean(m_poolDataRow["GovernmentBondFinance"]);
                m_GovernmentBondFinancingProgramType = (E_GovernmentBondFinancingProgramType)Enum.Parse(typeof(E_GovernmentBondFinancingProgramType), m_poolDataRow["GovernmentBondFinancingProgramType"].ToString());
                m_GovernmentBondFinancingProgramName = m_poolDataRow["GovernmentBondFinancingProgramName"].ToString();

                this.earlyDeliveryDate = this.ReadDateTime(m_poolDataRow, "EarlyDeliveryDate");
                this.assignDate = this.ReadDateTime(m_poolDataRow, "AssignDate");
                this.confirmDate = this.ReadDateTime(m_poolDataRow, "ConfirmDate");
                this.Notes = m_poolDataRow["Notes"].ToString();

                //This is the sum of sInvestorLockBaseBrokComp1PcPrice * sServicingUnpaidPrincipalBalance
                //for all loans in the pool. Needs to be divided by Total Current Balance (which is the sum of sServicingUnpaidPrincipalBalance)
                //to get the real value.
                decimal rawBasePricing = Convert.ToDecimal(m_poolDataRow["RawBackEndBrokerBasePricing"]);
                if (TotalCurrentBalance != 0)
                {
                    BackEndBasePricing = rawBasePricing / TotalCurrentBalance;
                }

                this.RawFrontEndRateLockPricing = Convert.ToDecimal(m_poolDataRow["RawFrontEndRateLockPricing"]);
                this.UnpaidBalanceAmountShipped = Convert.ToDecimal(m_poolDataRow["UnpaidBalanceAmountShipped"]);
                this.UnpaidBalanceAmountPurchased = Convert.ToDecimal(m_poolDataRow["UnpaidBalanceAmountPurchased"]);
                this.ProfitabilityAmountAllocated = Convert.ToDecimal(m_poolDataRow["ProfitabilityAmountAllocated"]);
                this.ProfitabilityAmountShipped = Convert.ToDecimal(m_poolDataRow["ProfitabilityAmountShipped"]);
                this.ProfitabilityAmountPurchased = Convert.ToDecimal(m_poolDataRow["ProfitabilityAmountPurchased"]);
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.LoadData() threw SqlException", e);
            }
        }

        private T? ToNullable<T>(DataRow row, string fieldName, Func<object, T> convert) where T : struct
        {
            var fromDb = row[fieldName];
            m_nullableFields.Add(fieldName);
            m_dirtyFields[fieldName] = fromDb;

            if (fromDb == DBNull.Value) return null;
            return convert(fromDb);
        }

        private CDateTime ReadDateTime(DataRow row, string fieldName)
        {
            CDateTime value;
            try
            {
                value = CDateTime.Create(Convert.ToDateTime(row[fieldName]));
            }
            catch (InvalidCastException)
            {
                value = CDateTime.InvalidWrapValue;
            }

            m_nullableFields.Add(fieldName);
            m_dirtyFields[fieldName] = value;
            return value;
        }

        public bool SaveData()
        {
            if (m_dirtyFields.Count > 0)
            {
                List<string> toRemove = new List<string>();

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PoolId", PoolId));
                SqlParameter param;
                foreach (string field in m_dirtyFields.Keys)
                {
                    var parameterName = field;
                    object o = m_dirtyFields[field];

                    if (field == "TaxID")
                    {
                        // Encrypt migrated tax IDs before sending to the database.
                        parameterName = "TaxIdEncrypted";
                        var encryptionKey = EncryptionKeyIdentifier.Create(this.encryptionKeyId.Value).Value;
                        o = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(encryptionKey, (string)o);
                    }
                    else if (o is CDateTime)
                    {
                        o = ((CDateTime)o).DateTimeForDBWriting;
                    }
                    else if (o is Enum)
                    {
                        o = (int)o;
                    }

                    param = new SqlParameter("@" + parameterName, o);
                    parameters.Add(param);

                    //Retain datetimes in dirty fields;
                    //necessary for writing null over existing values
                    if (!m_nullableFields.Contains(field))
                        toRemove.Add(field);
                }
                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "MortgagePoolUpdate", 5, parameters);
                    foreach (string field in toRemove)
                    {
                        m_dirtyFields.Remove(field);
                    }
                }
                catch (SqlException e)
                {
                    Tools.LogError("MortgagePool.SaveData() threw SqlException", e);
                    return false;
                }
            }
            return true;
        }

        public List<Guid> FetchLoansInPool()
        {
            return FetchLoansInPool(PoolId, m_brokerId);
        }

        public void AssignLoans(IEnumerable<Guid> idList)
        {
            var settings = new LoanAssignmentSettings(false, false, false, 0);
            BatchOperationError<LoanError> batchOperationError = null;
            this.AssignLoans(idList, settings, out batchOperationError);
        }

        /// <summary>
        /// Sets the back end base price for the loan file. If the base price type
        /// is Table, the loan's note rate must be in the base pricing table.
        /// </summary>
        /// <param name="dataLoan">The loan to set back-end base price for.</param>
        /// <returns>
        /// Returns true if the back-end base price could be set. Otherwise, false.
        /// </returns>
        private void SetBackEndBasePrice(CPageData dataLoan, out LoanError error)
        {
            error = null;

            if (dataLoan.sIsInvestorRateLocked)
            {
                error = new LoanError(
                    dataLoan.sLId,
                    dataLoan.sLNm,
                    UnableToSetBackEndBasePriceForFileWithLock);

                return;
            }
            else if (this.BasePricingCalcType == E_BasePricingCalcType.Table
                && !this.BasePricingTable.ContainsKey(dataLoan.sNoteIR))
            {
                error = new LoanError(
                    dataLoan.sLId,
                    dataLoan.sLNm,
                    UnableToAssignBackEndBasePrice);

                return;
            }

            try
            {
                var oldInvestorLockPriceRowLockedT = dataLoan.sInvestorLockPriceRowLockedT;
                dataLoan.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;

                decimal price;

                if (this.BasePricingCalcType == E_BasePricingCalcType.Table)
                {
                    price = this.BasePricingTable[dataLoan.sNoteIR];
                }
                else
                {
                    price = this.BasePrice;
                }

                dataLoan.LoadInvestorLockData(
                    dataLoan.sInvestorLockBaseNoteIR_rep,
                    this.convert.ToRateString(100m - price),
                    dataLoan.sInvestorLockBaseRAdjMarginR_rep,
                    dataLoan.sInvestorLockBaseOptionArmTeaserR_rep);

                dataLoan.sInvestorLockPriceRowLockedT = oldInvestorLockPriceRowLockedT;
            }
            catch (CBaseException e)
            {
                error = new LoanError(dataLoan.sLId, dataLoan.sLNm, e.UserMessage);
            }
        }

        private void SetDeliveryFee(CPageData dataLoan, decimal deliveryFee, out LoanError error)
        {
            error = null;

            if (dataLoan.sIsInvestorRateLocked)
            {
                error = new LoanError(
                    dataLoan.sLId,
                    dataLoan.sLNm,
                    UnableToSetDeliveryFeeForFileWithLock);

                return;
            }

            try
            {
                dataLoan.UpdateInvestorLockAdjustmentFee(
                   PoolDeliveryAdjustmentDescription,
                   deliveryFee);
            }
            catch (CBaseException e)
            {
                error = new LoanError(dataLoan.sLId, dataLoan.sLNm, e.UserMessage);
            }
        }

        private void SetBackEndRateLock(CPageData dataLoan, out LoanError error)
        {
            error = null;

            if (dataLoan.sIsInvestorRateLocked)
            {
                error = new LoanError(
                    dataLoan.sLId,
                    dataLoan.sLNm,
                    UnableToSetBackEndRateLockForFileWithLock);

                return;
            }
            else if (!this.CommitmentExpires.IsValid)
            {
                error = new LoanError(
                    dataLoan.sLId,
                    dataLoan.sLNm,
                    UnableToDetermineRateLockExpirationDate);

                return;
            }

            try
            {
                dataLoan.sInvestorLockRLckdD = CDateTime.Create(DateTime.Today);

                dataLoan.sInvestorLockRLckdDays = (int)this.CommitmentExpires.DateTimeForComputation
                    .Subtract(DateTime.Today)
                    .TotalDays;

                var oldInvestorLockPriceRowLockedT = dataLoan.sInvestorLockPriceRowLockedT;
                dataLoan.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;

                // Set the note rate on the back-end lock page.
                dataLoan.LoadInvestorLockData(
                    dataLoan.sNoteIR_rep,
                    dataLoan.sInvestorLockBaseBrokComp1PcFee_rep,
                    dataLoan.sInvestorLockBaseRAdjMarginR_rep,
                    dataLoan.sInvestorLockBaseOptionArmTeaserR_rep);

                dataLoan.sInvestorLockPriceRowLockedT = oldInvestorLockPriceRowLockedT;

                dataLoan.LockInvestorRate(
                    BrokerUserPrincipal.CurrentPrincipal.LoginNm,
                    PoolAssignmentLockMessage);
            }
            catch (CBaseException e)
            {
                error = new LoanError(dataLoan.sLId, dataLoan.sLNm, e.UserMessage);
            }
        }

        public void AssignLoans(IEnumerable<Guid> idList, LoanAssignmentSettings settings, out BatchOperationError<LoanError> batchError)
        {
            if (idList == null)
            {
                throw new ArgumentNullException("idList");
            }
            else if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            batchError = new BatchOperationError<LoanError>(
                "The following errors occurred while assigning the loan(s) to this pool:");

            foreach (Guid loanId in idList)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(MortgagePool));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (dataLoan.sMortgagePoolId != null)
                {
                    Tools.LogBug("Error: Tried assigning Loan:" + loanId + " to MortgagePool:" + PoolId + "; already assigned to MortgagePool:" + dataLoan.sMortgagePoolId);
                    continue;
                }

                LoanError error = null;

                if (settings.IsSetBackEndBasePrice)
                {
                    this.SetBackEndBasePrice(dataLoan, out error);

                    if (error != null)
                    {
                        batchError.Add(error);
                        continue;
                    }
                }

                if (settings.IsSetDeliveryFee)
                {
                    this.SetDeliveryFee(dataLoan, settings.DeliveryFee, out error);

                    if (error != null)
                    {
                        batchError.Add(error);
                        continue;
                    }
                }

                if (settings.IsSetBackEndRateLock)
                {
                    this.SetBackEndRateLock(dataLoan, out error);

                    if (error != null)
                    {
                        batchError.Add(error);
                        continue;
                    }
                }

                dataLoan.sMortgagePoolId = PoolId;

                try
                {
                    dataLoan.Save();
                }
                catch (CBaseException e)
                {
                    error = new LoanError(
                        dataLoan.sLId,
                        dataLoan.sLNm,
                        e.UserMessage);

                    batchError.Add(error);
                    continue;
                }
            }

            //For re-computing calculated fields
            LoadData(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
        }

        public void RemoveLoans(params Guid[] idList)
        {
            RemoveLoans(idList.AsEnumerable());
        }

        public void RemoveLoans(IEnumerable<Guid> idList)
        {
            foreach (Guid loanId in idList)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(MortgagePool));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                if (dataLoan.sMortgagePoolId == PoolId)
                    dataLoan.sMortgagePoolId = null;
                else
                    Tools.LogBug("Error: Tried removing Loan:" + loanId + " from MortgagePool:" + PoolId + "; Was assigned to different MortgagePool:" + dataLoan.sMortgagePoolId);
                dataLoan.Save();
            }

            //For re-computing calculated fields
            LoadData(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
        }

        public void SetFormatTarget(FormatTarget target)
        {
            convert = new LosConvert(target);
        }

        List<Trade> x_trades = null;

        public List<Trade> GetTrades()
        {
            if (x_trades != null) return x_trades;
            return RefreshTrades();
        }

        public List<Trade> RefreshTrades()
        {
            var parameters = new SqlParameter[]{ 
                new SqlParameter("@BrokerId", m_brokerId),
                new SqlParameter("@PoolId", PoolId)
            };

            x_trades = new List<Trade>();
            using (var reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "MBS_FetchTradesInPool", parameters))
            {
                while (reader.Read())
                {
                    x_trades.Add(new Trade(reader));
                }
            }

            return x_trades;
        }

        public string ToRep(decimal value)
        {
            return convert.ToMoneyString(value, FormatDirection.ToRep);
        }

        #endregion

        #region Static Methods


        public static bool LookupPoolNumber(string poolNumber, bool allowPartials)
        {
            bool result = false;
            try
            {
                Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                SqlParameter[] parameters = {
                                                new SqlParameter("@poolNumber", poolNumber.TrimWhitespaceAndBOM()),
                    new SqlParameter("@allowPartials", (allowPartials ? 1 : 0)),
                    new SqlParameter("@BrokerId", brokerId)
                                            };
                //Do a search for mortgage pools with this number, but only see if there's at least one;
                //Dev Note: Should be migrated to a dedicated stored procedure - paoloa 9/7/2012
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePoolsByNumber", parameters))
                {
                    result = reader.Read();
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.LookupPoolNumber() threw SqlException", e);
            }
            return result;
        }

        public static List<MortgagePool> FindMortgagePools(Guid brokerId)
        {
            List<MortgagePool> mortgagePools = new List<MortgagePool>();
            SqlParameter[] parameter = { new SqlParameter("@BrokerId", brokerId) };
            List<long> poolIds = new List<long>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePools", null, parameter))
            {
                while (reader.Read())
                {
                    poolIds.Add((long)reader["PoolId"]);
                }
            }

            List<MortgagePool> pools = new List<MortgagePool>(poolIds.Count);
            foreach (long poolId in poolIds)
            {
                pools.Add(new MortgagePool(poolId, brokerId));
            }

            return pools;
        }

        public static DataTable FindMortgagePools(bool? isOpen, E_MortgagePoolAgencyT? agency, E_sFinMethT? amort,
           string poolNumber, bool allowPartialPoolNum, string InternalId, bool allowPartialPoolId, string commitmentNum,
            bool allowPartialCommitmentNum)
        {
            List<MortgagePool> mortgagePools = new List<MortgagePool>();
            List<SqlParameter> parameters = new List<SqlParameter>();

            // By Filters
            SqlParameter param;
            if (isOpen != null)
            {
                param = new SqlParameter("@PoolStatus", SqlDbType.Int);
                param.Value = ((bool)isOpen ? 1 : 0);
                parameters.Add(param);
            }
            if (agency != null)
            {
                param = new SqlParameter("@AgencyT", SqlDbType.Int);
                param.Value = (int)agency;
                parameters.Add(param);
            }
            if (amort != null)
            {
                param = new SqlParameter("@AmortT", SqlDbType.Int);
                param.Value = (int)amort;
                parameters.Add(param);
            }

            // By Number
            if (!string.IsNullOrEmpty(poolNumber.TrimWhitespaceAndBOM()))
            {
                parameters.Add(new SqlParameter("@poolNumber", poolNumber.TrimWhitespaceAndBOM()));
                parameters.Add(new SqlParameter("@allowPartialPoolNum", (allowPartialPoolNum ? 1 : 0)));
            }

            // By Internal ID
            if (!string.IsNullOrEmpty(InternalId.TrimWhitespaceAndBOM()))
            {
                parameters.Add(new SqlParameter("@InternalId", InternalId.TrimWhitespaceAndBOM()));
                parameters.Add(new SqlParameter("@allowPartialPoolId", (allowPartialPoolId ? 1 : 0)));
            }

            // By Commitment Number 
            if(!string.IsNullOrEmpty(commitmentNum.TrimWhitespaceAndBOM()))
            {
                parameters.Add(new SqlParameter("@CommitmentNum", commitmentNum.TrimWhitespaceAndBOM()));
                parameters.Add(new SqlParameter("@allowPartialCommitmentNumber", (allowPartialCommitmentNum ? 1 : 0)));
            }

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            parameters.Add(new SqlParameter("@BrokerId", brokerId));

            DataTable dt = GetDataTableTemplate();
            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePools", parameters))
                {
                    MortgagePool p;
                    while (reader.Read())
                    {
                        p = new MortgagePool((long)reader["PoolId"], brokerId);
                        dt.Rows.Add(p.PoolId,
                            p.PoolNumberByAgency,
                            (p.ClosedD == null || !p.ClosedD.IsValid ? "Open" : "Closed"),
                            p.AgencyT_rep,
                            p.AmortizationT_rep,
                            p.CommitmentNum,
                            p.DeliveredD,
                            p.SecurityR_rep,
                            p.LoanCount,
                            p.TotalCurrentBalance_rep,
                            p.InternalId);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.FindMortgagePools(filters) threw SqlException", e);
            }
            return dt;
        }

        public static DataTable FindMortgagePools(bool? isOpen, E_MortgagePoolAgencyT? agency, E_sFinMethT? amort)
        {
            List<MortgagePool> mortgagePools = new List<MortgagePool>();
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param;
            if (isOpen != null)
            {
                param = new SqlParameter("@PoolStatus", SqlDbType.Int);
                param.Value = ((bool)isOpen ? 1 : 0);
                parameters.Add(param);
            }
            if (agency != null)
            {
                param = new SqlParameter("@AgencyT", SqlDbType.Int);
                param.Value = (int)agency;
                parameters.Add(param);
            }
            if (amort != null)
            {
                param = new SqlParameter("@AmortT", SqlDbType.Int);
                param.Value = (int)amort;
                parameters.Add(param);
            }

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            parameters.Add(new SqlParameter("@BrokerId", brokerId));

            DataTable dt = GetDataTableTemplate();
            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePoolsByFilters", parameters))
                {
                    MortgagePool p;
                    while (reader.Read())
                    {
                        p = new MortgagePool((long)reader["PoolId"], brokerId);
                        dt.Rows.Add(p.PoolId,
                            p.PoolNumberByAgency,
                            (p.ClosedD == null || !p.ClosedD.IsValid ? "Open" : "Closed"),
                            p.AgencyT_rep,
                            p.AmortizationT_rep,
                            p.CommitmentNum,
                            p.DeliveredD,
                            p.SecurityR_rep,
                            p.LoanCount,
                            p.TotalCurrentBalance_rep,
                            p.InternalId);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.FindMortgagePools(filters) threw SqlException", e);
            }
            return dt;
        }

        public static DataTable FindMortgagePools(string poolNumber, bool allowPartials)
        {
            List<MortgagePool> mortgagePools = new List<MortgagePool>();

            DataTable dt = GetDataTableTemplate();
            try
            {
                Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                SqlParameter[] parameters = {
                                                new SqlParameter("@poolNumber", poolNumber.TrimWhitespaceAndBOM()),
                                                new SqlParameter("@allowPartials", (allowPartials ? 1 : 0)),
                                                new SqlParameter("@BrokerId", brokerId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePoolsByNumber", parameters))
                {
                    MortgagePool p;
                    while (reader.Read())
                    {
                        p = new MortgagePool((long)reader["PoolId"], brokerId);
                        dt.Rows.Add(p.PoolId,
                            p.PoolNumberByAgency,
                            (p.ClosedD == null ? "Open" : "Closed"),
                            p.AgencyT_rep,
                            p.AmortizationT_rep,
                            p.CommitmentNum,
                            p.DeliveredD,
                            p.SecurityR_rep,
                            p.LoanCount,
                            p.TotalCurrentBalance_rep,
                            p.InternalId);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.FindMortgagePools(number) threw SqlException", e);
            }
            return dt;
        }

        public static DataTable FindMortgagePoolsByInternalID(string InternalId, bool allowPartials)
        {
            List<MortgagePool> mortgagePools = new List<MortgagePool>();
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            DataTable dt = GetDataTableTemplate();
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@InternalId", InternalId.TrimWhitespaceAndBOM()),
                                                new SqlParameter("@allowPartials", (allowPartials ? 1 : 0)),
                                                new SqlParameter("@BrokerId", BrokerUserPrincipal.CurrentPrincipal.BrokerId)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindMortgagePoolsByInternalId", parameters))
                {
                    MortgagePool p;
                    while (reader.Read())
                    {
                        p = new MortgagePool((long)reader["PoolId"], brokerId);
                        dt.Rows.Add(p.PoolId,
                            p.PoolNumberByAgency,
                            (p.ClosedD == null ? "Open" : "Closed"),
                            p.AgencyT_rep,
                            p.AmortizationT_rep,
                            p.CommitmentNum,
                            p.DeliveredD,
                            p.SecurityR_rep,
                            p.LoanCount,
                            p.TotalCurrentBalance_rep,
                            p.InternalId);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.FindMortgagePoolsByInternalID(string) threw SqlException", e);
            }
            return dt;
        }

        private static DataTable GetDataTableTemplate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PoolId", typeof(long));
            dt.Columns.Add("PoolNumberByAgency", typeof(string));
            dt.Columns.Add("PoolStatus", typeof(string));
            dt.Columns.Add("AgencyT", typeof(string));
            dt.Columns.Add("AmortizationT", typeof(string));
            dt.Columns.Add("CommitmentNum", typeof(string));
            dt.Columns.Add("DeliveredD", typeof(CDateTime));
            dt.Columns.Add("SecurityR", typeof(string));
            dt.Columns.Add("LoanCount", typeof(int));
            dt.Columns.Add("TotalCurrentBalance", typeof(string));
            dt.Columns.Add("InternalId", typeof(string));
            return dt;
        }

        public static long CreatePool(string internalId)
        {
            try
            {
                var encryptionKeyDriver = GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create();
                var encryptionKey = encryptionKeyDriver.GenerateKey().Key;

                SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                    new SqlParameter("@InternalId", internalId.Length > 36 ? internalId.Substring(0, 36) : internalId),
                    new SqlParameter("@EncryptionKeyId", encryptionKey.Value),
                    new SqlParameter("@PublicId", Guid.NewGuid())
                };

                var newId = StoredProcedureHelper.ExecuteScalar(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "MortgagePoolNew", parameters);
                return Convert.ToInt64(newId);
            }
            catch (SqlException e)
            {
                Tools.LogError("Error Creating New Mortgage Pool", e);
                return -1;
            }
        }
        public static bool DeletePool(Guid brokerId, long poolId)
        {
            SqlParameter[] parameters = {
                                                new SqlParameter("@PoolId", poolId)
                                            };
            int poolsDeleted = StoredProcedureHelper.ExecuteNonQuery(brokerId, "MortgagePoolDelete", 3, parameters);
            return poolsDeleted == -1;
        }

        public static List<Guid> FetchLoansInPool(long PoolId, Guid m_brokerId)
        {
            List<Guid> loansInPool = new List<Guid>();
            SqlParameter poolId = new SqlParameter("@poolid", SqlDbType.BigInt);
            SqlParameter brokerId = new SqlParameter("@brokerid", SqlDbType.UniqueIdentifier);
            poolId.Value = PoolId;
            brokerId.Value = m_brokerId;

            SqlParameter[] parameters = {
                                            poolId,
                                            brokerId
                                        };
            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "FindLoansByMortgagePool", parameters))
                {
                    while (reader.Read())
                    {
                        loansInPool.Add((Guid)reader["sLId"]);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.FetchLoansInPool() threw SqlException", e);
                throw;
            }
            return loansInPool;
        }

        // Checks if the CurrentPool pool number is a duplicate of any other pool number for a given agency and brokerid.
        // If the resulting pool number is not a duplicate or the fields used to name the pool number are blank, it is valid.
        public static bool ValidPoolNumber(MortgagePool pool)
        {

            switch (pool.AgencyT)
            {
                case E_MortgagePoolAgencyT.FannieMae:
                case E_MortgagePoolAgencyT.FreddieMac:
                    if (pool.PoolPrefix.TrimWhitespaceAndBOM() == "" && pool.BasePoolNumber.TrimWhitespaceAndBOM() == "") return true;
                    break;
                case E_MortgagePoolAgencyT.GinnieMae:
                    if (pool.PoolNumberByAgency.TrimWhitespaceAndBOM() == "") return true;
                    break;
                case E_MortgagePoolAgencyT.LeaveBlank:
                    throw new CBaseException(ErrorMessages.Generic, "Pool should never have blank agency at save.");
                default:
                    throw new UnhandledEnumException(pool.AgencyT);
            }

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@AgencyT", (int)pool.AgencyT));
            parameters.Add(new SqlParameter("@PoolTypeCode", pool.PoolTypeCode));
            parameters.Add(new SqlParameter("@BasePoolNumber", pool.BasePoolNumber));
            parameters.Add(new SqlParameter("@IssueTypeCode", pool.IssueTypeCode));
            parameters.Add(new SqlParameter("@PendingPoolNum", pool.PoolNumberByAgency));
            parameters.Add(new SqlParameter("@PoolPrefix", pool.PoolPrefix));

            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindPoolIdForBrokerIdAgencyPoolNum", parameters))
                {
                    // If any other PoolId is associated with this number, then fail
                    while (reader.Read())
                    {
                        if ((long)reader["PoolId"] != pool.PoolId)
                            return false;
                    }

                    return true;
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("MortgagePool.ValidPoolNumber() threw SqlException", e);
                throw;
            }
        }

        public class CandidatesResult
        {
            public Report Candidates;
            public int Total;
        }

        public static CandidatesResult FetchLoanAssignmentCandidates(CandidateLoanQueryParameters p, Query q)
        {
            LoanReporting reportRunner = new LoanReporting();
            reportRunner.MaxSqlRowCount = 250;
            reportRunner.MaxViewableRowCount = 250;

            q = CandidateLoanQuery.GetQuery(p, q);

            var report = reportRunner.RunReport(q, BrokerUserPrincipal.CurrentPrincipal, true, E_ReportExtentScopeT.Default);

            return new CandidatesResult
            {
                Candidates = report,
                Total = report.Count
            };
        }

        public static string GetPoolAutoId()
        {
            SqlParameter PoolCounterParameter = new SqlParameter("@PoolCounter", SqlDbType.BigInt);
            PoolCounterParameter.Direction = ParameterDirection.Output;

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            List<SqlParameter> parameters = new List<SqlParameter>(3);
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(PoolCounterParameter);

            long PoolCounter = 0;  // return "" by default
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "MortgagePool_GetPoolCounter", 5, parameters.ToArray());
            if (PoolCounterParameter.Value != DBNull.Value)
            {
                PoolCounter = (long)PoolCounterParameter.Value;
            }

            return PoolCounter.ToString();
        }

        #endregion
    }
}
