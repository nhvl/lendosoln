﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.QueryProcessor;

namespace LendersOffice.ObjLib.MortgagePool
{
    public static class CandidateLoanQuery
    {
        public static string DefaultQuery = @"
<Query xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Label>
    <Title>Assign Loans Report</Title>
    <SavedBy>Rodrigo Noronha</SavedBy>
    <SavedOn>2014-08-18T15:41:14.2243048-07:00</SavedOn>
    <SavedId>387395dc-017b-4644-ad29-d9339ffc81eb</SavedId>
    <Version>4</Version>
  </Label>
  <Columns>
    <Items>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sLNm</Id>
        <Name>Loan Number</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>aBLastNm</Id>
        <Name>Borr Last Name</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dec"" Kind=""Pct"" Show=""true"">
        <Format>N3</Format>
        <Id>sNoteIR</Id>
        <Name>Note Rate</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Enu"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sFinMethT</Id>
        <Name>Amortization Type</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Int"" Kind=""Cnt"" Show=""true"">
        <Format>#,#;-#,#;0</Format>
        <Id>sTerm</Id>
        <Name>Term</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Int"" Kind=""Cnt"" Show=""true"">
        <Format>#,#;-#,#;0</Format>
        <Id>sDue</Id>
        <Name>Due</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Enu"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sLT</Id>
        <Name>Loan Type</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sLpTemplateNm</Id>
        <Name>Loan Program Name</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dec"" Kind=""Csh"" Show=""true"">
        <Format>C</Format>
        <Id>sFinalLAmt</Id>
        <Name>Total Loan Amount</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dec"" Kind=""Pct"" Show=""true"">
        <Format>N3</Format>
        <Id>sLtvR</Id>
        <Name>LTV</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Enu"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sStatusT</Id>
        <Name>Loan Status</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dtm"" Kind=""Cal"" Show=""true"">
        <Format>M/d/yyyy</Format>
        <Id>sSchedFundD</Id>
        <Name>Scheduled Funding</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dtm"" Kind=""Cal"" Show=""true"">
        <Format>M/d/yyyy</Format>
        <Id>sLPurchaseD</Id>
        <Name>Loan Sold Date</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
    </Items>
  </Columns>
  <Sorting>
    <Items />
  </Sorting>
  <GroupBy>
    <Items />
  </GroupBy>
  <Relates Type=""An"">
    <Items />
  </Relates>
  <Filters Type=""An"">
  </Filters>
  <Options>
    <Items />
  </Options>
  <Details>This report is empty.  Customize it by adding fields, conditions, status events with ranges, etc. and save it with a new name.</Details>
  <Id>9041886c-3c96-49e6-b6e2-9b294ba2565b</Id>
  <DisplayTablesOnSeparatePages>false</DisplayTablesOnSeparatePages>
  <RestrictToLoanIds />
</Query>
";
        public static Query GetQuery(CandidateLoanQueryParameters p)
        {
            return GetQuery(p, DefaultQuery);
        }

        public static Query GetQuery(CandidateLoanQueryParameters p, Query q)
        {
            /*The structure we want to create is:
            1 -> top level AND of: 
                *rate > my min rate
                *rate < my max rate
                *term = my term
                *amort type = my amort type
                *status != any status less than my min status 
                *   or lead canceled/declined or loan canceled/rejected/suspended 8/21/13 GF OPM 113060.
                *if IncludeBackendRateLock is not set, sInvestorLockRateLockStatusT = NotLocked
                *2 -> second level OR of:
                    * status = any status greater than my min status
                    (I don't know if this is actually necessary, but it's how custom reports does it)
            */
            var AlwaysExcludeStatuses = new List<E_sStatusT>() 
            { 
                E_sStatusT.Lead_Canceled,
                E_sStatusT.Lead_Declined,
                E_sStatusT.Loan_Canceled, 
                E_sStatusT.Loan_Rejected, 
                E_sStatusT.Loan_Suspended,
                E_sStatusT.Loan_CounterOffer,
                E_sStatusT.Loan_Archived,
                E_sStatusT.Loan_Withdrawn
            };
            var ExcludeStatuses = CPageBase.s_sStatusT_Order.TakeWhile(a => a != p.MinStatus)
                .Concat(AlwaysExcludeStatuses)
                .Distinct();
            var IncludeStatuses = CPageBase.s_sStatusT_Order.SkipWhile(a => a != p.MinStatus)
                .Where(a => !AlwaysExcludeStatuses.Contains(a));

            Conditions TopLevelAnd = new Conditions(E_ConditionsType.An);
            if (p.MinRate.HasValue) TopLevelAnd.Add(new Condition(E_ConditionType.Ge, "sNoteIR", p.MinRate));
            if (p.MaxRate.HasValue) TopLevelAnd.Add(new Condition(E_ConditionType.Le, "sNoteIR", p.MaxRate));
            if (p.Term.HasValue) TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sTerm", p.Term));
            if (!p.IncludeBackEndRateLockLoans)
            {
                TopLevelAnd.Add(new Condition(E_ConditionType.Eq,
                                                "sInvestorLockRateLockStatusT",
                                                E_sInvestorLockRateLockStatusT.NotLocked));
            }
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sFinMethT", p.AmortType));
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sLT", p.LoanType));

            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sMortgagePoolId", new Argument() { Type = E_ArgumentType.Const, Value = null }));
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sLPurchaseD", new Argument() { Type = E_ArgumentType.Const, Value = null }));
            // 8/21/2013 GF - OPM 113060 Also exclude loans that have valid value for sDisbursementD, sReconciledD.
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sDisbursementD", new Argument() { Type = E_ArgumentType.Const, Value = null }));
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sReconciledD", new Argument() { Type = E_ArgumentType.Const, Value = null }));

            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sCanceledD", new Argument() { Type = E_ArgumentType.Const, Value = null }));
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sRejectD", new Argument() { Type = E_ArgumentType.Const, Value = null }));
            TopLevelAnd.Add(new Condition(E_ConditionType.Eq, "sWithdrawnD", new Argument() { Type = E_ArgumentType.Const, Value = null }));

            foreach (var ignoreStatus in ExcludeStatuses)
            {
                TopLevelAnd.Add(new Condition(E_ConditionType.Ne, "sStatusT", ignoreStatus));
            }

            Conditions SecondLevelOr = new Conditions(E_ConditionsType.Or);
            foreach (var includeStatus in IncludeStatuses)
            {
                SecondLevelOr.Add(new Condition(E_ConditionType.Eq, "sStatusT", includeStatus));
            }

            TopLevelAnd.Add(SecondLevelOr);

            q.Filters = TopLevelAnd;

            return q;
        }
    }

    public struct CandidateLoanQueryParameters : IEquatable<CandidateLoanQueryParameters>
    {
        public E_sLT LoanType;
        public E_sFinMethT AmortType;
        public int? Term;

        public decimal? MinRate;
        public decimal? MaxRate;
        public E_sStatusT MinStatus;

        public bool IncludeBackEndRateLockLoans;

        #region IEquatable<CandidateLoanQueryParameters> Members

        bool IEquatable<CandidateLoanQueryParameters>.Equals(CandidateLoanQueryParameters other)
        {
            return this.LoanType == other.LoanType &&
                    this.AmortType == other.AmortType &&
                    this.Term == other.Term &&
                    this.MinRate == other.MinRate &&
                    this.MaxRate == other.MaxRate &&
                    this.MinStatus == other.MinStatus;
        }

        #endregion
    }
}
