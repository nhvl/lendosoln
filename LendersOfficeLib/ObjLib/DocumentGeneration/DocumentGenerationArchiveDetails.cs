﻿namespace LendersOffice.ObjLib.DocumentGeneration
{
    using Common.SerializationTypes;

    /// <summary>
    /// Represents the details about an archive created because disclosures were generated.
    /// </summary>
    public class DocumentGenerationArchiveDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentGenerationArchiveDetails"/> class.
        /// </summary>
        public DocumentGenerationArchiveDetails()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether an archive in the Unknown status was created.
        /// </summary>
        /// <value>True if an archive in the Unknown status was created. Otherwise, false.</value>
        public bool CreatedArchiveInUnknownStatus { get; set; }

        /// <summary>
        /// Gets or sets the type of the unknown archive.
        /// </summary>
        /// <value>The type of the archive, if an unknown archive was created. Otherwise, null.</value>
        public ClosingCostArchive.E_ClosingCostArchiveType? UnknownArchiveType { get; set; }

        /// <summary>
        /// Gets or sets the date of the unknown archive.
        /// </summary>
        /// <value>The date of the archive, if an unknown archive was created. Otherwise, null.</value>
        public string UnknownArchiveDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the unknown archive was part of a CoC chain.
        /// </summary>
        /// <value>True if the unknown archive had changes from a CoC. Otherwise, false.</value>
        public bool UnknownArchiveWasSourcedFromPendingCoC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there are both LE and CD archives in unknown status.
        /// </summary>
        /// <value>True if there are exactly a single CD and a single LE archives that are in unknown status. Otherwise, false.</value>
        public bool HasBothCDAndLEArchiveInUnknownStatus { get; set; }
    }
}
