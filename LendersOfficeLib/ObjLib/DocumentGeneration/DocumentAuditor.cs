﻿// <copyright file="DocumentAuditor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   9/5/2014 5:15:04 PM
// </summary>
namespace LendersOffice.ObjLib.DocumentGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Migration;
    using LendersOffice.Security;

    /// <summary>
    /// Performs document generation audits and updates the loan file as needed.
    /// </summary>
    public class DocumentAuditor
    {
        /// <summary>
        /// Warning message for NFLP initial disclosures in New Mexico.
        /// </summary>
        private const string NewMexicoInitialDisclosuresWarningMessage =
            "New Mexico requires that NFLP disclose to the borrower the existence " +
            "of all loans available for which the borrower qualifies that " +
            "have terms that are as favorable or more favorable than those " +
            "loans offered to the borrower by the loan originator. To ensure " +
            "compliance with New Mexico law, NFLP must provide a copy of " +
            "the Loan Comparison Report from Price My Loan (including all " +
            "loan options that are as favorable or more favorable) with the " +
            "initial disclosures delivered to the borrower(s).";

        /// <summary>
        /// Warning message for when the GFE archive's value for Upfront MIP
        /// is different from that on file.
        /// </summary>
        private const string UpfrontMipDiscrepancyWarningMessage =
            "The Upfront MIP amount on the loan file is different than the " +
            "value in the GFE Archive. This may cause the Last Disclosed " +
            "APR field to not match what is on the disclosures. Please review " +
            "the APR in the document package against the Last Disclosed APR field.";

        /// <summary>
        /// The purposes we consider to be refinance for the purpose of  auto-
        /// calculating the estimated closing date.
        /// </summary>
        private static E_sLPurposeT[] refiPurposes = new E_sLPurposeT[] 
        {
            E_sLPurposeT.Refin,
            E_sLPurposeT.RefinCashout,
            E_sLPurposeT.FhaStreamlinedRefinance,
            E_sLPurposeT.VaIrrrl,
            E_sLPurposeT.HomeEquity
        };

        /// <summary>
        /// Broker id of NFLP.
        /// </summary>
        private readonly Guid nflpBrokerID = new Guid("c2f3a0f9-eb17-4e66-a951-ff5013f3efde");

        /// <summary>
        /// The document vendor with which to perform the audit.
        /// </summary>
        private IDocumentVendor vendor = null;

        /// <summary>
        /// The loan to audit.
        /// </summary>
        private CPageData loan = null;

        /// <summary>
        /// Indicates whether changes have been made to the loan file by this instance.
        /// </summary>
        private bool loanIsDirty = false;

        /// <summary>
        /// The ID of the loan.
        /// </summary>
        private Guid loanID;

        /// <summary>
        /// The ID of the application.
        /// </summary>
        private Guid applicationID;

        /// <summary>
        /// If true, checks if user has doc generation permissions before auditing.
        /// </summary>
        private bool enforcePermissions = true;

        /// <summary>
        /// Initializes a new instance of the DocumentAuditor class.
        /// </summary>
        /// <param name="vendor">The vendor with which to run the audit.</param>
        /// <param name="loanID">The loan ID to audit.</param>
        /// <param name="applicationID">The application ID to audit.</param>
        /// <param name="enforcePermissions">If true, checks if user has doc generation permissions before auditing.</param>
        public DocumentAuditor(
            IDocumentVendor vendor,
            Guid loanID,
            Guid applicationID,
            bool enforcePermissions = true)
        {
            if (vendor == null)
            {
                throw new ArgumentNullException();
            }

            this.vendor = vendor;
            this.loanID = loanID;
            this.applicationID = applicationID;
            this.enforcePermissions = enforcePermissions;
        }

        /// <summary>
        /// The potential sources for the temporary archive.
        /// </summary>
        public enum TemporaryArchiveSource
        {
            /// <summary>
            /// Create the temporary archive from live data.
            /// </summary>
            LiveData = 0,

            /// <summary>
            /// Creating the temporary archive from the archive in the Pending Document Generation status.
            /// </summary>
            PendingArchive = 1
        }

        /// <summary>
        /// Gets the loan. The loan is initially loaded with its data state set 
        /// to $E_DataState.InitSave$.
        /// </summary>
        private CPageData Loan
        {
            get
            {
                if (this.loan == null)
                {
                    this.loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                        this.loanID,
                        typeof(DocumentAuditor));
                    this.loan.InitSave(ConstAppDavid.SkipVersionCheck);
                }

                return this.loan;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the caller must provide the source for the temporary archive.
        /// </summary>
        /// <param name="package">The document package for the audit.</param>
        /// <param name="disclosureRegulation">The disclosure regulation of the file.</param>
        /// <param name="hasLastDisclosedLoanEstimateArchive">Indicates whether there is a last disclosed LE archive on the file.</param>
        /// <param name="hasLoanEstimateArchiveInPendingStatus">Indicates whether there is pending doc generation LE archive on the file.</param>
        /// <returns>True if caller must prompt. Otherwise, false.</returns>
        public static bool MustSpecifyTemporaryArchiveSource(
            VendorConfig.DocumentPackage package,
            E_sDisclosureRegulationT disclosureRegulation,
            bool hasLastDisclosedLoanEstimateArchive,
            bool hasLoanEstimateArchiveInPendingStatus)
        {
            return disclosureRegulation == E_sDisclosureRegulationT.TRID
                && hasLoanEstimateArchiveInPendingStatus
                && ((!package.HasClosingDisclosure && !hasLastDisclosedLoanEstimateArchive) || package.HasClosingDisclosure);
        }

        /// <summary>
        /// Gets a value indicating whether the caller must provide the source for the temporary archive.
        /// </summary>
        /// <param name="package">The document package for the audit.</param>
        /// <returns>True if caller must prompt. Otherwise, false.</returns>
        public bool MustSpecifyTemporaryArchiveSource(VendorConfig.DocumentPackage package)
        {
            return MustSpecifyTemporaryArchiveSource(
                package,
                this.Loan.sDisclosureRegulationT,
                this.Loan.sHasLastDisclosedLoanEstimateArchive,
                this.Loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        /// <summary>
        /// Audits the given loan and application. Updates the loan file as needed.
        /// </summary>
        /// <param name="package">The package type for DocMagic or the package ID for other vendors.</param>
        /// <param name="planCode">The plan code for DocMagic or the loan program id for other vendors.</param>
        /// <param name="investorCode">The "transfer to" investor code.</param>
        /// <param name="investorName">The "transfer to" investor name.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <param name="tempArchiveSource">The source for the temporary archive.</param>
        /// <returns>The audit result.</returns>
        public DocumentVendorResult<Audit> Audit(
            string package,
            string planCode,
            string investorCode,
            string investorName,
            AbstractUserPrincipal principal,
            TemporaryArchiveSource? tempArchiveSource = null)
        {
            // OPM 235850 - Check that user has workflow privileges before allowing audit to continue.
            string failureReasons;
            if (this.enforcePermissions && !this.vendor.CanGenerateDocuments(principal, this.loanID, package, out failureReasons))
            {
                throw new AccessDenied(failureReasons, failureReasons);
            }

            var documentPackage = this.vendor.Config.RetrievePackage(package);

            bool requireTempArchiveSource = this.MustSpecifyTemporaryArchiveSource(documentPackage);

            if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.Loan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                var userMessage = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveDocs;
                var devMessage = "Cannot generate documents when file is in invalid state.";
                throw new CBaseException(userMessage, devMessage);
            }
            else if (tempArchiveSource == null && requireTempArchiveSource)
            {
                throw new InvalidOperationException("Cannot determine source for temporary archive.");
            }
            else if (tempArchiveSource != null && !requireTempArchiveSource)
            {
                throw new InvalidOperationException("Cannot specify temporary archive source when not required.");
            }
            else if (this.Loan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus &&
                documentPackage.HasLoanEstimate)
            {
                throw new InvalidOperationException(
                    ErrorMessages.ArchiveError.CannotGenerateLEAfterIncludingLEInCD);
            }

            this.UpdatePlanCodeName(planCode);

            bool useTemporaryArchive = this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                documentPackage != VendorConfig.DocumentPackage.InvalidPackage;

            if (useTemporaryArchive)
            {
                this.Loan.CreateTempArchive(documentPackage.HasClosingDisclosure, tempArchiveSource, true, principal);
            }
            else if (this.Loan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID &&
                documentPackage != VendorConfig.DocumentPackage.InvalidPackage &&
                documentPackage.HasClosingDisclosure == false)
            {
                // opm 233683 ejm - Do not refresh the last disclosed archive if the package contains a closing disclosure.
                this.RefreshLastDisclosedArchive();
            }

            this.UpdateTransferToInvestorInfo(investorCode, investorName);

            this.UpdateEstimatedCloseDate();

            this.UpdateSettlementChargesExportSource(package);

            this.SaveChanges();

            var customAuditMessages = this.GenerateCustomAuditMessages(package);

            var audit = this.vendor.AuditLoan(
                this.loanID,
                this.applicationID,
                package,
                planCode,
                principal,
                preventUsingTemporaryArchive: !useTemporaryArchive);

            this.UpdateDocumentPortalUrl(audit);

            this.SaveChanges();

            if (customAuditMessages.Count > 0)
            {
                var auditMessages = audit.Result.Results
                    .Union(customAuditMessages);

                var auditWithCustomMessages = new Audit(
                    auditMessages,
                    audit.Result.FurtherInfo.ToStringOrEmpty(),
                    audit.Result.DocumentPortal.ToStringOrEmpty());

                return new DocumentVendorResult<Audit>(
                    auditWithCustomMessages,
                    audit.ErrorMessage);
            }
            else
            {
                return audit;
            }
        }

        /// <summary>
        /// Updates the loan's plan code if it doesn't match what was used for 
        /// the audit.
        /// </summary>
        /// <param name="planCodeName">The plan code name that was used for the audit.</param>
        private void UpdatePlanCodeName(string planCodeName)
        {
            if (!this.Loan.sDocMagicPlanCodeNm.Equals(planCodeName, StringComparison.InvariantCultureIgnoreCase))
            {
                // This is expensive.
                this.Loan.sDocMagicPlanCodeNm = planCodeName;
                this.loanIsDirty = true;
            }
        }

                /// <summary>
        /// Refreshes the data in the last disclosed archive to update it to the live
        /// data for non-fee data.
        /// </summary>
        /// <remarks>
        /// This allows the data source for various fields (archive or live) to be
        /// chosen by the datalayer.  This is by comparison to the older style
        /// exemplified by the MISMO 2.6 export, which usually chooses the data
        /// source on a per data point basis.
        /// </remarks>
        private void RefreshLastDisclosedArchive()
        {
            if (this.Loan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                && this.Loan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID
                && this.Loan.BrokerDB.IsGFEandCoCVersioningEnabled
                && this.Loan.sLastDisclosedGFEArchiveId != Guid.Empty)
            {
                Tools.LogInfo("Doing a blank CoC on last disclosed archive.");
                this.loanIsDirty |= this.Loan.RefreshClosingCostCoc();
            }
        }

        /// <summary>
        /// Updates the "transfer to" investor information if it does not match
        /// what is on file.
        /// </summary>
        /// <param name="investorName">The investor name.</param>
        /// <param name="investorCode">The investor code.</param>
        private void UpdateTransferToInvestorInfo(string investorName, string investorCode)
        {
            bool supportsTransferToInvestors = this.vendor.Skin.SupportsTransferToInvestors;

            var investorNameToSet = supportsTransferToInvestors ? investorName : string.Empty;
            var investorCodeToSet = supportsTransferToInvestors ? investorCode : string.Empty;

            if (!this.Loan.sDocMagicTransferToInvestorNm.Equals(investorNameToSet, StringComparison.InvariantCultureIgnoreCase)
                || !this.Loan.sDocMagicTransferToInvestorCode.Equals(investorCodeToSet, StringComparison.InvariantCultureIgnoreCase))
            {
                this.Loan.sDocMagicTransferToInvestorNm = investorNameToSet;
                this.Loan.sDocMagicTransferToInvestorCode = investorCodeToSet;
                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Updates the estimated closing date based on the loan file.
        /// </summary>
        private void UpdateEstimatedCloseDate()
        {
            if (refiPurposes.Contains(this.Loan.sLPurposeT) && !this.Loan.sEstCloseD.IsValid)
            {
                var date = DateTime.Today.AddMonths(1);
                bool pushSundayHolidaysToMonday = !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                    this.Loan.sLoanVersionT,
                    LoanVersionT.V28_DontRollSundayHolidaysToMonday);

                while (date.DayOfWeek == DayOfWeek.Saturday
                    || date.DayOfWeek == DayOfWeek.Sunday 
                    || BankingHoliday.IsBankHoliday(date, pushSundayHolidaysToMonday))
                {
                    date = date.AddDays(1);
                }

                this.Loan.sEstCloseD = CDateTime.Create(date);
                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Updates the settlement charges export source on the loan file.
        /// </summary>
        /// <param name="packageType">The package used to run the audit.</param>
        private void UpdateSettlementChargesExportSource(string packageType)
        {
            if (!(this.vendor is DocMagicDocumentVendor))
            {
                if (this.vendor.Config.IsClosingPackage(packageType) 
                    && this.Loan.sSettlementChargesExportSource != E_SettlementChargesExportSource.SETTLEMENT)
                {
                    this.Loan.sSettlementChargesExportSource = E_SettlementChargesExportSource.SETTLEMENT;
                    this.loanIsDirty = true;
                }
                else if (!this.vendor.Config.IsClosingPackage(packageType) 
                    && this.Loan.sSettlementChargesExportSource != E_SettlementChargesExportSource.GFE)
                {
                    this.Loan.sSettlementChargesExportSource = E_SettlementChargesExportSource.GFE;
                    this.loanIsDirty = true;
                }
            }
        }

        /// <summary>
        /// Generates custom audits that are not handled by the document vendor.
        /// </summary>
        /// <param name="packageType">The package used to run the audit.</param>
        /// <returns>A list of AuditLines.</returns>
        private List<AuditLine> GenerateCustomAuditMessages(string packageType)
        {
            var customAuditMessages = new List<AuditLine>();

            if (this.Loan.sBrokerId == this.nflpBrokerID && this.vendor.Config.VendorName.Contains("DocuTech"))
            {
                // OPM 150549 - Initial Disclosures in New Mexico
                if (this.vendor.Config.IsInitialDisclosurePackage(packageType) && this.Loan.sSpState == "NM")
                {
                    customAuditMessages.Add(
                        new AuditLine(
                            NewMexicoInitialDisclosuresWarningMessage,
                            AuditSeverity.Warning,
                            null,
                            null,
                            null,
                            this.loanID,
                            Guid.Empty));
                }
            }

            // opm 171896 - Display warning when we cannot determine the last disclosed APR.
            if (this.Loan.BrokerDB.IsGFEandCoCVersioningEnabled
                && this.Loan.sLastDisclosedGFEArchiveD != null
                && this.Loan.LastDisclosedGFEArchive != null)
            {
                var fieldDependencies = 
                    CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(DocumentAuditor))
                        .Union(CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release);

                var dataLoanWithGFEArchiveApplied = new CFullAccessPageData(
                    this.loanID,
                    fieldDependencies);
                dataLoanWithGFEArchiveApplied.InitLoad();

                dataLoanWithGFEArchiveApplied.ApplyGFEArchiveExcludingFields(
                    this.Loan.LastDisclosedGFEArchive,
                    ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc);

                if (this.Loan.sFfUfmip1003 != dataLoanWithGFEArchiveApplied.sFfUfmip1003)
                {
                    customAuditMessages.Add(
                        new AuditLine(
                            UpfrontMipDiscrepancyWarningMessage,
                            AuditSeverity.Warning,
                            null,
                            null,
                            null,
                            this.loanID,
                            Guid.Empty));
                }
            }

            return customAuditMessages;
        }

        /// <summary>
        /// Updates the document portal url on the loan file.
        /// </summary>
        /// <param name="auditResult">
        /// The result of auditing the file with the document vendor.
        /// </param>
        private void UpdateDocumentPortalUrl(DocumentVendorResult<Audit> auditResult)
        {
            if (auditResult.Result != null &&
                auditResult.Result.DocumentPortal != null)
            {
                this.Loan.sDocVendorPortalUrl = auditResult.Result.DocumentPortal.ToStringOrEmpty();
                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Saves the changes made to the loan, clears the dirty bit, and 
        /// resets the data state to $E_DataState.InitSave$.
        /// </summary>
        private void SaveChanges()
        {
            if (!this.loanIsDirty)
            {
                return;
            }

            this.Loan.Save();
            this.loanIsDirty = false;
            this.Loan.InitSave(ConstAppDavid.SkipVersionCheck);
        }
    }
}
