﻿// <copyright file="AuditHtmlGenerator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   9/9/2014 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.DocumentGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.AntiXss;
    using LendersOffice.Integration.DocumentVendor;

    /// <summary>
    /// Generates HTML for document audit results.
    /// </summary>
    public static class AuditHtmlGenerator
    {
        /// <summary>
        /// The template for audit list items.
        /// </summary>
        private const string EntryHtml = "<li><span class='AuditEntry {0}'>{1} </span>{2}</li>";

        /// <summary>
        /// The template for the edit link for each item.
        /// </summary>
        private const string EditLinkFormat = @" - <a class='EditLink'>edit</a><input type='hidden' value='{0}' />";

        /// <summary>
        /// The template for the details link for each item.
        /// </summary>
        private const string DetailFormat = @" <span class='Action detail-showhide'>show details</span> <div class='details-text Hidden'>{0}</div> ";

        /// <summary>
        /// Generates an unordered list of audit items. The audit results are 
        /// first sorted by severity.
        /// </summary>
        /// <param name="auditResult">The audit for which to generate the HTML.</param>
        /// <param name="preAuditErrors">Optional. A list of error messages generated by LQB prior to generating an audit.</param>
        /// <param name="generationResult">Optional. The document generation result.</param>
        /// <returns>The HTML for the audit.</returns>
        public static string GenerateAuditHtml(
            DocumentVendorResult<Audit> auditResult,
            List<string> preAuditErrors = null,
            DocumentVendorResult<IDocumentGenerationResult> generationResult = null)
        {
            if ((preAuditErrors == null || preAuditErrors.Count == 0) && auditResult == null)
            {
                throw new ArgumentNullException("auditResult", "Audit must have a non-null result if there were no pre-Audit errors.");
            }

            var errorHtml = new StringBuilder("<ul id='AuditList'>");

            errorHtml.AppendNonNullLines(BuildPreAuditErrorMessages(preAuditErrors));
            errorHtml.AppendNonNullLine(BuildErrorMessage(auditResult));
            errorHtml.AppendNonNullLines(BuildAuditResults(auditResult));
            errorHtml.AppendNonNullLine(BuildErrorMessage(generationResult));

            errorHtml.AppendLine("</ul>");

            return errorHtml.ToString();
        }

        /// <summary>
        /// Builds an error message HTML entry if <paramref name="result"/> has an error.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="result">The result of the request to the document vendor.</param>
        /// <returns>An error message HTML entry if there is an error, null otherwise.</returns>
        private static string BuildErrorMessage<T>(DocumentVendorResult<T> result)
        {
            return result != null && result.HasError ? MakeEntryHtmlForError(result.ErrorMessage) : null;
        }

        /// <summary>
        /// Builds the Pre-Audit HTML error messages to show to the user.
        /// </summary>
        /// <param name="preAuditErrors">The error messages to show, as simple strings.</param>
        /// <returns>The HTML entries for the specified messages.</returns>
        private static IEnumerable<string> BuildPreAuditErrorMessages(List<string> preAuditErrors)
        {
            return preAuditErrors == null ? null : preAuditErrors.Select(errorMessage => MakeSimpleEntryHtml(AuditSeverity.Fatal, "PRE-AUDIT FAILURE", errorMessage));
        }

        /// <summary>
        /// Builds an enumeration of the HTML entries for the audit results.
        /// </summary>
        /// <param name="auditResult">The audit result to build the HTML entries from.</param>
        /// <returns>The HTML entries of the audit result.</returns>
        private static IEnumerable<string> BuildAuditResults(DocumentVendorResult<Audit> auditResult)
        {
            if (auditResult == null || auditResult.Result == null)
            {
                yield break;
            }

            Audit audit = auditResult.Result;
            if (audit.FurtherInfo != null)
            {
                yield return "<li class='extra-fields'>Additional data may be required: <a href=\"" +
                                     audit.FurtherInfo +
                                     "\" target=\"DVFurtherInfo\">click here to view additional fields</a>";
            }

            foreach (var auditLine in audit.Results.OrderByDescending(a => a.Severity))
            {
                yield return MakeEntryHtml(auditLine);
            }

            // If we didn't get any results and the status is ok, then no issues were found
            if (!audit.Results.Any())
            {
                 yield return string.Format(EntryHtml, "OK", string.Empty, "Audit Complete, no issues found");
            }
        }

        /// <summary>
        /// Appends the non-null lines contained within <paramref name="values"/> to <paramref name="stringBuilder"/> as individual lines.
        /// </summary>
        /// <param name="stringBuilder">The <see cref="System.Text.StringBuilder"/> to which the lines will be appended.</param>
        /// <param name="values">The enumeration of strings to append.</param>
        /// <returns><paramref name="stringBuilder"/> with the lines possibly appended.</returns>
        private static StringBuilder AppendNonNullLines(this StringBuilder stringBuilder, IEnumerable<string> values)
        {
            StringBuilder returnValue = stringBuilder;
            if (values != null)
            {
                foreach (string value in values)
                {
                    returnValue = stringBuilder.AppendNonNullLine(value);
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Appends a line to <paramref name="stringBuilder"/>, provided <paramref name="value"/> is not null.
        /// </summary>
        /// <param name="stringBuilder">The <see cref="System.Text.StringBuilder"/> to which the line will be appended.</param>
        /// <param name="value">The string to append.</param>
        /// <returns><paramref name="stringBuilder"/> with the line possibly appended.</returns>
        private static StringBuilder AppendNonNullLine(this StringBuilder stringBuilder, string value)
        {
            return value == null ? stringBuilder : stringBuilder.AppendLine(value);
        }

        /// <summary>
        /// Constructs the edit link for an audit line.
        /// </summary>
        /// <param name="linkInfo">The audit line.</param>
        /// <returns>The HTML for the edit link for the given audit line.</returns>
        private static string MakeEditLink(AuditLine linkInfo)
        {
            if (string.IsNullOrEmpty(linkInfo.EditPage))
            {
                return string.Empty;
            }

            string path = linkInfo.EditPage;
            char initDelim = '?';
            if (path.Contains('?'))
            {
                initDelim = '&';
            }

            string field = string.Empty;

            if (!string.IsNullOrEmpty(linkInfo.FieldId))
            {
                field = "&highlightId=" + linkInfo.FieldId;
            }

            path = path + initDelim + "loanid=" + linkInfo.LoanId + "&appid=" + linkInfo.AppId + field;

            path = AspxTools.SafeUrl(path).Trim('"');

            return string.Format(EditLinkFormat, path);
        }

        /// <summary>
        /// Generates the HTML for an individual audit line. Includes the type, 
        /// edit link, and details link.
        /// </summary>
        /// <param name="linkInfo">The audit line.</param>
        /// <returns>The HTML for the audit line.</returns>
        private static string MakeEntryHtml(AuditLine linkInfo)
        {
            string errorType = linkInfo.Severity.ToString().ToUpper();

            string editLink = MakeEditLink(linkInfo);

            string details = MakeDetails(linkInfo.AuditDetails);

            return MakeEntryHtmlFromInputs(linkInfo.Severity, errorType, linkInfo.Message, editLink, details);
        }

        /// <summary>
        /// Makes the audit entry HTML for a vendor error.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>The list item for the error message.</returns>
        private static string MakeEntryHtmlForError(string errorMessage)
        {
            return MakeSimpleEntryHtml(AuditSeverity.Fatal, "FATAL", errorMessage);
        }

        /// <summary>
        /// Generates the HTML for a simple entry in the audit results.
        /// </summary>
        /// <param name="severityClass">The class of the <paramref name="errorTypeName"/> in the list item.</param>
        /// <param name="errorTypeName">The name of the error type to display to the user.</param>
        /// <param name="message">The message to display to the user.</param>
        /// <returns>The Html of the list item.</returns>
        private static string MakeSimpleEntryHtml(AuditSeverity severityClass, string errorTypeName, string message)
        {
            return MakeEntryHtmlFromInputs(severityClass, errorTypeName, ReplaceNewlines(message, string.Empty), string.Empty, string.Empty);
        }

        /// <summary>
        /// Do not use directly; call either <see cref="MakeEntryHtml"/> or <see cref="MakeSimpleEntryHtml"/>.
        /// </summary>
        /// <param name="errorTypeClass">The class of the <paramref name="errorTypeName"/> in the list item.</param>
        /// <param name="errorTypeName">The name of the error type to display to the user.</param>
        /// <param name="message">The message to display to the user.</param>
        /// <param name="editLink">The edit link to display to the user.</param>
        /// <param name="details">The details to display with the audit message.</param>
        /// <returns>The Html of the list item.</returns>
        private static string MakeEntryHtmlFromInputs(AuditSeverity errorTypeClass, string errorTypeName, string message, string editLink, string details)
        {
            return string.Format(EntryHtml, errorTypeClass.ToString().ToUpper(), errorTypeName, AspxTools.HtmlString(message.Trim('.', ' ')) + editLink + details);
        }

        /// <summary>
        /// Generates the details section HTML for an audit line.
        /// </summary>
        /// <param name="details">The details of the audit line.</param>
        /// <returns>The HTML for the details section for the audit line.</returns>
        private static string MakeDetails(string details)
        {
            if (string.IsNullOrEmpty(details))
            { 
                return string.Empty; 
            }

            return string.Format(DetailFormat, ReplaceNewlines(AspxTools.HtmlString(details), null));
        }

        /// <summary>
        /// Replaces escaped HTML with line breaks.
        /// </summary>
        /// <param name="input">
        /// The string in which to replace the escaped newlines.
        /// </param>
        /// <param name="replacementString">
        /// The string to replace the escaped newlines. If null, uses default of
        /// &lt;br /&gt;.
        /// </param>
        /// <remarks>
        /// For some reason, on the client side these escaped html entities are 
        /// completely stripped (but only in IE, they're just interpreted in 
        /// Firefox, it's weird) So we need to replace them with newlines on the
        /// server side.
        /// </remarks>
        /// <returns>
        /// The string with escaped newlines replaced by line breaks.
        /// </returns>
        private static string ReplaceNewlines(string input, string replacementString)
        {
            return input.Replace("&#x0D;", string.Empty).Replace("&#x0A;", replacementString ?? "<br />");
        }
    }
}
