﻿namespace LendersOffice.ObjLib.DocumentGeneration
{
    /// <summary>
    /// Represents the status of a document generation request.
    /// </summary>
    public class DocumentGenerationRequestStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentGenerationRequestStatus"/> class.
        /// </summary>
        public DocumentGenerationRequestStatus()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the request has completed.
        /// </summary>
        /// <value>True if the doc request has been completed. Otherwise, false.</value>
        public bool RequestCompleted { get; set; }

        /// <summary>
        /// Gets or sets the archive details associated with a doc order.
        /// </summary>
        /// <value>The archive details if the request has completed. Should be null if the request has not completed.</value>
        public DocumentGenerationArchiveDetails ArchiveDetails { get; set; }
    }
}
