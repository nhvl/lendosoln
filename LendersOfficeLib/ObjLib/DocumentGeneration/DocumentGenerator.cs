﻿// <copyright file="DocumentGenerator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   9/8/2014 2:58:27 PM 
// </summary>
namespace LendersOffice.ObjLib.DocumentGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Common.SerializationTypes;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;

    /// <summary>
    /// Handles generating documents and updating the loan file.
    /// </summary>
    /// <remarks>
    /// When adding operations that update loan data, you MUST set the
    /// loanIsDirty bit if you want the changes to be saved. There are certain
    /// situations where your changes may coincidentally piggy-back on some
    /// other change, but don't count on it. The only exception is loan data
    /// which, upon being set, stores the results to FileDB without requiring a
    /// call to Save().
    /// </remarks>
    public class DocumentGenerator
    {
        /// <summary>
        /// The vendor with which to generate documents.
        /// </summary>
        private readonly IDocumentVendor vendor;

        /// <summary>
        /// The id of the loan.
        /// </summary>
        private readonly Guid loanID;

        /// <summary>
        /// The id of the application.
        /// </summary>
        private readonly Guid applicationID;

        /// <summary>
        /// The loan for which to generate documents. Should only be accessed by the Loan property.
        /// </summary>
        private CPageData loan;

        /// <summary>
        /// Dirty bit for the loan.
        /// </summary>
        private bool loanIsDirty;

        /// <summary>
        /// If true, checks if user has doc generation permissions before generating docs.
        /// </summary>
        private bool enforcePermissions = true;

        /// <summary>
        /// Initializes a new instance of the DocumentGenerator class.
        /// </summary>
        /// <param name="vendor">The vendor.</param>
        /// <param name="loanID">The loan id.</param>
        /// <param name="applicationID">The application id.</param>
        /// <param name="enforcePermissions">If true, checks if user has doc generation permissions before generating docs.</param>
        public DocumentGenerator(
            IDocumentVendor vendor,
            Guid loanID,
            Guid applicationID,
            bool enforcePermissions = true)
        {
            if (vendor == null)
            {
                throw new ArgumentNullException("vendor");
            }

            this.vendor = vendor;
            this.loanID = loanID;
            this.applicationID = applicationID;
            this.enforcePermissions = enforcePermissions;
        }

        /// <summary>
        /// Gets a value indicating whether the vendor is the legacy, hard-coded <see cref="LendersOffice.Integration.DocumentVendor.DocMagicDocumentVendor"/>.
        /// </summary>
        private bool VendorIsLegacyDocMagic
        {
            get { return this.vendor.Config.VendorId == Guid.Empty; }
        }

        /// <summary>
        /// Gets the loan for which to generate documents. Lazy loads and calls 
        /// <c>InitSave</c> after initialization. After saving, <c>InitSave</c>
        /// will need to be called again.
        /// </summary>
        private CPageData Loan
        {
            get
            {
                if (this.loan == null)
                {
                    this.loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                        this.loanID,
                        typeof(DocumentGenerator));
                    this.loan.InitSave(ConstAppDavid.SkipVersionCheck);
                }

                return this.loan;
            }
        }

        /// <summary>
        /// Generates documents and updates the loan file as necessary.
        /// </summary>
        /// <param name="packageTypeId">The package type/id.</param>
        /// <param name="loanProgramId">The Id of the loan program specified for the loan.</param>
        /// <param name="permissions">The permissions to use when generating the documents for this loan.</param>
        /// <param name="isEDisclosureRequested">Indicates whether the caller has requested for the document vendor to disclose the documents electronically.</param>
        /// <param name="isECloseRequested">Indicates whether the caller has requested the loan closing to occur electronically.</param>
        /// <param name="isESignRequested">Indicates whether the caller has requested for the document vendor to allow the documents to be electronically signed.</param>
        /// <param name="isMersRegistrationEnabled">Indicates whether <c>MERS</c> registration is enabled.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <param name="emailDocumentInformation">Optional. The information specifying how the caller would like the documents to be sent via email.</param>
        /// <param name="manualFulfillmentInformation">Optional. The information specifying how the caller would like the documents to be sent in the mail.</param>
        /// <param name="documentFormat">Optional. The format of the documents to return. Default is PDF.</param>
        /// <param name="forms">Optional. The forms to include in the documents.</param>
        /// <param name="isPreviewOrder">Indicates whether the request is for preview documents. Default is false.</param>
        /// <param name="additionalNotificationEmails">A list of additional ESign notification emails to send to vendors.</param>
        /// <returns>The document generation result.</returns>
        public DocumentVendorResult<IDocumentGenerationResult> Generate(
            string packageTypeId,
            string loanProgramId,
            Permissions permissions,
            bool isEDisclosureRequested,
            bool isECloseRequested,
            bool isESignRequested,
            bool isMersRegistrationEnabled,
            AbstractUserPrincipal principal,
            EmailDocumentInformation emailDocumentInformation = null,
            ManualFulfillmentInformation manualFulfillmentInformation = null,
            string documentFormat = "PDF",
            IEnumerable<FormInfo> forms = null,
            bool isPreviewOrder = false,
            IEnumerable<string> additionalNotificationEmails = null)
        {
            var documentGenerationRequest = this.vendor.GetDocumentGenerationRequest(this.applicationID, this.loanID);

            if (documentFormat == "DBK" && isESignRequested)
            {
                throw new CBaseException("Cannot use DocMaster format and allow signing", "Cannot use DocMaster format and allow signing; this should not have been allowed through the UI.");
            }

            var packageLookupResult = this.vendor.GetAvailableDocumentPackages(this.loanID, principal);
            DocumentPackage? matchingPackage = !packageLookupResult.HasError ? packageLookupResult.Result.Cast<DocumentPackage?>().FirstOrDefault(package => package.Value.Type == packageTypeId) : null;
            if (!matchingPackage.HasValue)
            {
                return DocumentVendorResult.Error<IDocumentGenerationResult>(packageLookupResult.ErrorMessage ?? "Unable to load package.", null);
            }

            documentGenerationRequest.PackageType = packageTypeId;
            documentGenerationRequest.PackageName = matchingPackage.Value.Description;
            documentGenerationRequest.LoanProgramId = loanProgramId;
            documentGenerationRequest.DocumentFormat = documentFormat;

            if (forms != null)
            {
                documentGenerationRequest.IndividualForms.AddRange(forms);
            }

            if (!isPreviewOrder)
            {
                List<string> esignNotificationEmails = new List<string>(additionalNotificationEmails ?? new List<string>());

                // 10/28/2015 BS - Case 229862. Temporary eSignNotification for DocMagic
                if (permissions.EnableESign && isESignRequested
                    && this.vendor.Config.VendorName.Contains("DocMagic") && documentGenerationRequest is ConfiguredDocumentGenerationRequest
                    && ConstStage.EnableESignNotificationEmailFromDocumentFrameworkRequests
                    && !this.Loan.BrokerDB.DisableESignNotificationEmailFromDocumentFrameworkRequests)
                {
                    esignNotificationEmails.Add(ConstStage.ESignEmailAddress);
                }

                if (permissions.EnableEDisclose && isEDisclosureRequested)
                {
                    documentGenerationRequest.SendEDisclosures(permissions.EnableESign && isESignRequested);
                    if (this.vendor.Config.PlatformType == E_DocumentVendor.DocuTech || this.vendor.Config.PlatformType == E_DocumentVendor.DocMagic)
                    {
                        string esignNotificationAddress = this.Loan.GetAgentOfRole(this.Loan.BrokerDB.DocutechEsignNotificationEmailAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject).EmailAddr;
                        if (this.Loan.BrokerDB.UseCustomField60AsEsignNotificationList
                            && !string.IsNullOrWhiteSpace(this.Loan.sCustomField60Notes))
                        {
                            esignNotificationAddress = this.Loan.sCustomField60Notes;
                        }

                        esignNotificationEmails.Add(esignNotificationAddress);
                    }
                }

                if (documentGenerationRequest is ConfiguredDocumentGenerationRequest)
                {
                    var docFrameworkRequest = documentGenerationRequest as ConfiguredDocumentGenerationRequest;
                    docFrameworkRequest.SetEClose(permissions.EnableEClose && isECloseRequested);

                    if (esignNotificationEmails.Any())
                    {
                        docFrameworkRequest.SetEsignNotificationEmail(string.Join(";", esignNotificationEmails));
                    }
                }

                if (permissions.EnableEmailDocuments && emailDocumentInformation != null)
                {
                    documentGenerationRequest.EmailDocumentsTo(
                        emailDocumentInformation.EmailAddress,
                        emailDocumentInformation.RequiredPassword,
                        emailDocumentInformation.IsNotificationOnRetrievalRequested,
                        emailDocumentInformation.PasswordHint);
                }

                // 7/24/2014 tj - 84544 - Create Option to disable DSI Print and Deliver in Generate Docs (DocMagic only)
                if (permissions.EnableManualFulfillment && (!this.VendorIsLegacyDocMagic || this.Loan.BrokerDB.EnableDsiPrintAndDeliverOption)
                    && manualFulfillmentInformation != null)
                {
                    documentGenerationRequest.EnableDsiPrintAndDeliver(
                        manualFulfillmentInformation.Attention,
                        manualFulfillmentInformation.Name,
                        manualFulfillmentInformation.Phone,
                        manualFulfillmentInformation.Notes,
                        manualFulfillmentInformation.StreetAddress,
                        manualFulfillmentInformation.City,
                        manualFulfillmentInformation.State,
                        manualFulfillmentInformation.Zipcode);
                }

                if (permissions.EnableMERSRegistration)
                {
                    documentGenerationRequest.IsMersRegistrationEnabled = isMersRegistrationEnabled;
                }
            }

            return this.Generate(packageTypeId, documentGenerationRequest, principal, isPreviewOrder);
        }

        /// <summary>
        /// Generates documents and updates the loan file as necessary.
        /// </summary>
        /// <param name="package">The package type/id.</param>
        /// <param name="request">The document request. Must be completely set up.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <param name="isPreviewOrder">Indicates whether the request is for preview documents.</param>
        /// <returns>The document generation result.</returns>
        public DocumentVendorResult<IDocumentGenerationResult> Generate(
            string package,
            IDocumentGenerationRequest request,
            AbstractUserPrincipal principal,
            bool isPreviewOrder = false)
        {
            // OPM 235850 - Check that user has workflow privileges before allowing doc generation.
            string failureReasons;
            if (this.enforcePermissions && !this.vendor.CanGenerateDocuments(principal, this.loanID, package, out failureReasons))
            {
                throw new AccessDenied(failureReasons, failureReasons);
            }

            this.VerifyInitialLoanEstimateNotAssociatedWithInvalidArchive();

            this.VerifyNoClosingCostArchivesHaveUnknownStatus();

            this.VerifyNotGeneratingInvalidLoanEstimate(package);

            this.UpdateSettlementChargesExportSource(package);

            this.SaveChanges();

            var result = request.RequestDocuments(isPreviewOrder, principal);

            decimal? lastDisclosedApr = null;
            if (!result.HasError)
            {
                if (isPreviewOrder)
                {
                    if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        this.Loan.DeleteTemporaryArchive();
                    }
                }
                else
                {
                    bool createdArchiveInUnknownStatus = false;
                    ClosingCostArchive.E_ClosingCostArchiveType? unknownArchiveType = null;
                    string unknownArchiveDate = null;
                    bool unknownArchiveWasSourcedFromPendingCoC = false;
                    bool hasBothCDAndLEArchiveInUnknownStatus = false;

                    if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        var archiveStatus = request.IsManualFulfillment || request.IsSendEDisclosures || request.IsEClosing ?
                            ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed :
                            ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown;
                        var docPackage = this.vendor.Config.RetrievePackage(package);

                        if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            (docPackage.HasLoanEstimate || docPackage.HasClosingDisclosure))
                        {
                            var archiveUsedForDocumentGeneration = this.Loan.SaveTemporaryArchive(archiveStatus, docPackage.Name);

                            this.Loan.UpdateTridDisclosureDatesForDocGeneration(
                                archiveUsedForDocumentGeneration,
                                request.IsSendEDisclosures,
                                request.IsEClosing,
                                request.IsManualFulfillment,
                                result.TransactionId,
                                result.Result.DocVendorApr,
                                this.vendor.Config.VendorId,
                                result.Result.DocCode,
                                this.Loan.BrokerDB.DefaultIssuedDateToToday,
                                result.UcdDocumentId,
                                result.Result.VendorProvidedDisclosureMetadata);

                            lastDisclosedApr = archiveUsedForDocumentGeneration.GetRateValue("sApr");
                            createdArchiveInUnknownStatus = archiveUsedForDocumentGeneration.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown;

                            if (createdArchiveInUnknownStatus)
                            {
                                unknownArchiveType = archiveUsedForDocumentGeneration.ClosingCostArchiveType;
                                unknownArchiveDate = archiveUsedForDocumentGeneration.DateArchived;
                                unknownArchiveWasSourcedFromPendingCoC = archiveUsedForDocumentGeneration.WasSourcedFromPendingCoC;

                                if (this.Loan.sHasBothCDAndLEArchiveInUnknownStatus)
                                {
                                    hasBothCDAndLEArchiveInUnknownStatus = true;
                                }
                            }
                        }
                        else if (docPackage != VendorConfig.DocumentPackage.InvalidPackage)
                        {
                            this.Loan.DeleteTemporaryArchive();
                        }
                    }

                    this.UpdateDisclosureStatus(
                        package,
                        request,
                        result.Result,
                        lastDisclosedApr);

                    this.UpdateHmdaCreditDenialInfo(principal, package);
                    this.UpdateLateChargePercent(result.Result.DocVendorLateChargePercent);

                    result.Result.CreatedArchiveInUnknownStatus = createdArchiveInUnknownStatus;
                    result.Result.UnknownArchiveType = unknownArchiveType;
                    result.Result.UnknownArchiveDate = unknownArchiveDate;
                    result.Result.UnknownArchiveWasSourcedFromPendingCoC = unknownArchiveWasSourcedFromPendingCoC;
                    result.Result.HasBothCDAndLEArchiveInUnknownStatus = hasBothCDAndLEArchiveInUnknownStatus;

                    this.SaveChanges();
                    this.RecordOrderToDb(request, result);
                }
            }

            return result;
        }

        /// <summary>
        /// Ensures that documents will not be generated if the loan file has
        /// a loan estimate marked as initial that is associated with an invalid
        /// archive. Throws a CBaseException if any invalid archives are found.
        /// </summary>
        private void VerifyInitialLoanEstimateNotAssociatedWithInvalidArchive()
        {
            if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.Loan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                var userMessage = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveDocs;
                var devMessage = "Cannot generate documents when file is in invalid state.";
                throw new CBaseException(userMessage, devMessage);
            }
        }

        /// <summary>
        /// Ensures that documents will not be generated if the loan file has
        /// any loan estimate archives in an unknown status. Throws a
        /// CBaseException if any invalid archives are found.
        /// </summary>
        private void VerifyNoClosingCostArchivesHaveUnknownStatus()
        {
            if (this.Loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.Loan.sHasClosingCostArchiveInUnknownStatus)
            {
                var archive = this.Loan.sClosingCostArchiveInUnknownStatus;
                var userMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                    archive.DateArchived,
                    archive.ClosingCostArchiveType,
                    userCanDetermineStatus: false);
                var devMessage = "Cannot generate documents when there is a closing cost archive in an unknown status.";
                throw new CBaseException(userMessage, devMessage);
            }
        }

        /// <summary>
        /// Verifies that we do not allow a Loan Estimate to be generated
        /// after there is a Loan Estimate archive marked as Included in Closing
        /// Disclosure.
        /// </summary>
        /// <param name="package">The package that is being generated.</param>
        private void VerifyNotGeneratingInvalidLoanEstimate(string package)
        {
            var docPackage = this.vendor.Config.RetrievePackage(package);

            if (docPackage.HasLoanEstimate
                && this.Loan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus)
            {
                var userMessage = ErrorMessages.ArchiveError.CannotGenerateLEAfterIncludingLEInCD;
                var devMessage = "Cannot generate Loan Estimate after disclosing a Loan Estimate as part of a Closing Disclosure.";
                throw new CBaseException(userMessage, devMessage);
            }
        }

        /// <summary>
        /// Updates the settlement charges export source. Will set to SETTLEMENT
        /// if it is a closing package.
        /// </summary>
        /// <param name="packageType">The package type.</param>
        private void UpdateSettlementChargesExportSource(string packageType)
        {
            if (!(this.vendor is DocMagicDocumentVendor))
            {
                if (this.vendor.Config.IsClosingPackage(packageType))
                {
                    this.Loan.sSettlementChargesExportSource = E_SettlementChargesExportSource.SETTLEMENT;
                }
                else
                {
                    this.Loan.sSettlementChargesExportSource = E_SettlementChargesExportSource.GFE;
                }

                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Update disclosure-related fields and process any disclosure triggers.
        /// </summary>
        /// <param name="package">The package type/id.</param>
        /// <param name="request">The document generation request.</param>
        /// <param name="result">The document generation result.</param>
        /// <param name="lastDisclosedApr">If applicable, the value to set as the last disclosed APR.</param>
        private void UpdateDisclosureStatus(
            string package,
            IDocumentGenerationRequest request,
            IDocumentGenerationResult result,
            decimal? lastDisclosedApr)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            else if (result == null)
            {
                throw new ArgumentNullException("result");
            }

            this.loanIsDirty = this.Loan.UpdateDisclosureStatus(
                                        package, // This is distinct for ConstAppDavid.DocuTech_Temp_GFE_Redisclosure_PackageID, which causes the package id at the start to be different from the actual package id
                                        lastDisclosedApr,
                                        result.IsDisclosurePackage,
                                        result.IsClosingPackage,
                                        result.IsInitialDisclosurePackage,
                                        request.IsSendEDisclosures,
                                        request.IsEClosing,
                                        request.IsManualFulfillment,
                                        result.PackageType,
                                        result.PackageName);
        }

        /// <summary>
        /// Updates the HMDA Credit Denial info for applicable packages.
        /// </summary>
        /// <param name="principal">User principal.</param>
        /// <param name="package">The package type/id.</param>
        private void UpdateHmdaCreditDenialInfo(AbstractUserPrincipal principal, string package)
        {
            // OPM 130633 credit denial / adverse action: DocMagic -> Adverse; DocuTech -> 6081
            if (string.Equals(package, "Adverse", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(package, "6081", StringComparison.OrdinalIgnoreCase))
            {
                this.Loan.sHmdaDeniedFormDoneD_rep = System.DateTime.Today.ToString("d");
                this.Loan.sHmdaDeniedFormDone = true;
                this.Loan.sHmdaDeniedFormDoneBy = principal == null || principal is SystemUserPrincipal ?
                    "System User" : principal.DisplayName;

                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Updates <see cref="CPageData.sLateChargePc"/> based on the value specified by the document Vendor.
        /// </summary>
        /// <param name="docVendorLateChargePercent">The value specified by the document vendor for the late charge percent.</param>
        private void UpdateLateChargePercent(decimal? docVendorLateChargePercent)
        {
            if (!docVendorLateChargePercent.HasValue)
            {
                return;
            }

            var converter = new LosConvert(); // creating a new instance since we want to explicitly use the default format target.
            string lateChargePc = converter.ToRateString(docVendorLateChargePercent);
            if (lateChargePc != this.loan.sLateChargePc)
            {
                this.Loan.sLateChargePc = lateChargePc;
                this.loanIsDirty = true;
            }
        }

        /// <summary>
        /// Saves a successful document order to the database for billing purposes.
        /// </summary>
        /// <param name="request">The request associated with the document order.</param>
        /// <param name="result">The result of the document order.</param>
        private void RecordOrderToDb(IDocumentGenerationRequest request, DocumentVendorResult<IDocumentGenerationResult> result)
        {
            if (string.IsNullOrEmpty(result.TransactionId))
            {
                Tools.LogError($"Could not record document order to the database. Document vendor {this.vendor.Config.VendorName} (ID: {this.vendor.Config.VendorId}) did not provide a TransactionID in their response.");
                return;
            }

            using (var exec = new CStoredProcedureExec(this.Loan.sBrokerId))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    var orderIdOutput = new SqlParameter()
                    {
                        ParameterName = "@OrderId",
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output
                    };

                    var principal = PrincipalFactory.CurrentPrincipal;
                    var parameters = new SqlParameter[]
                    {
                        orderIdOutput,
                        new SqlParameter("@BrokerId", this.Loan.sBrokerId),
                        new SqlParameter("@LoanId", this.Loan.sLId),
                        new SqlParameter("@TransactionId", result.TransactionId),
                        new SqlParameter("@PackageName", result.Result.PackageName.Name),
                        new SqlParameter("@OrderedBy", principal?.DisplayName ?? "System User"),
                        new SqlParameter("@OrderedByUserId", principal?.UserId ?? Guid.Empty),
                        new SqlParameter("@OrderedDate", DateTime.Now),
                        new SqlParameter("@VendorName", this.vendor.Config.VendorName),
                        new SqlParameter("@MERSRegistrationSelected", request.IsMersRegistrationEnabled),
                        new SqlParameter("@EDiscloseSelected", request.IsSendEDisclosures),
                        new SqlParameter("@ESignSelected", request.IsESignEnabled),
                        new SqlParameter("@ManualFulfillmentSelected", request.IsManualFulfillment),
                        new SqlParameter("@EmailDocumentsSelected", request.IsEmailDocumentsSelected),
                        new SqlParameter("@IndividualFormsSelected", request.IndividualForms.Any()),
                    };

                    exec.ExecuteNonQuery("DOCUMENT_FRAMEWORK_ORDER_Create", nRetry: 3, parameters: parameters);

                    int orderId = (int)orderIdOutput.Value;
                    foreach (var form in request.IndividualForms)
                    {
                        var formParameters = new SqlParameter[]
                        {
                            new SqlParameter("@OrderId", orderId),
                            new SqlParameter("@FormId", form.ID)
                        };

                        exec.ExecuteNonQuery("DOCUMENT_FRAMEWORK_ORDER_INDIVIDUAL_FORM_Create", nRetry: 3, parameters: formParameters);
                    }

                    exec.CommitTransaction();
                }
                catch (SqlException exc)
                {
                    Tools.LogError($"Unable to record document order to the database.", exc);
                    exec.RollbackTransaction();
                }
                catch (CBaseException exc)
                {
                    Tools.LogError($"Unable to record document order to the database.", exc);
                    exec.RollbackTransaction();
                }
            }
        }

        /// <summary>
        /// Saves changes to the loan, clears the dirty bit, and calls 
        /// <c>InitSave</c>.
        /// </summary>
        private void SaveChanges()
        {
            if (!this.loanIsDirty)
            {
                return;
            }

            this.Loan.Save();
            this.loanIsDirty = false;
            this.Loan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        /// <summary>
        /// Struct to represent an archive that was created as part of the document
        /// generation process.
        /// </summary>
        private struct ArchiveCreated
        {
            /// <summary>
            /// The type of the archive.
            /// </summary>
            public readonly ClosingCostArchive.E_ClosingCostArchiveType ArchiveType;

            /// <summary>
            /// The status of the archive.
            /// </summary>
            public readonly ClosingCostArchive.E_ClosingCostArchiveStatus Status;

            /// <summary>
            /// Initializes a new instance of the <see cref="ArchiveCreated"/> struct.
            /// </summary>
            /// <param name="type">The type of the archive.</param>
            /// <param name="status">The status of the archive.</param>
            public ArchiveCreated(
                ClosingCostArchive.E_ClosingCostArchiveType type,
                ClosingCostArchive.E_ClosingCostArchiveStatus status)
            {
                this.ArchiveType = type;
                this.Status = status;
            }
        }

        /// <summary>
        /// Record class of the specification for how the documents should be emailed.
        /// </summary>
        public class EmailDocumentInformation
        {
            /// <summary>
            /// The email address to receive the documents.
            /// </summary>
            public readonly string EmailAddress;

            /// <summary>
            /// The password required to open the documents, or the empty string if no password is desired.
            /// </summary>
            public readonly string RequiredPassword;

            /// <summary>
            /// A hint about the value of the password.
            /// </summary>
            public readonly string PasswordHint;

            /// <summary>
            /// A boolean value indicating whether notification on retrieval is needed. Only used by the DocMagic legacy integration.
            /// </summary>
            public readonly bool IsNotificationOnRetrievalRequested;

            /// <summary>
            /// Initializes a new instance of the <see cref="EmailDocumentInformation"/> class.
            /// </summary>
            /// <param name="emailAddress">The email address to receive the documents.</param>
            /// <param name="isPasswordRequired">A boolean value indicating whether or not a password is required to open the documents.</param>
            /// <param name="requiredPassword">The password required to open the documents.</param>
            /// <param name="passwordHint">A hint about the value of the password.</param>
            /// <param name="isNotificationOnRetrievalRequested">A boolean value indicating whether notification on retrieval is needed. Only used by the DocMagic legacy integration.</param>
            public EmailDocumentInformation(string emailAddress, bool isPasswordRequired, string requiredPassword, string passwordHint, bool isNotificationOnRetrievalRequested)
            {
                this.EmailAddress = emailAddress;
                this.RequiredPassword = isPasswordRequired ? requiredPassword : string.Empty;
                this.PasswordHint = passwordHint;
                this.IsNotificationOnRetrievalRequested = isNotificationOnRetrievalRequested;
            }
        }

        /// <summary>
        /// Record class of the specification for how the documents should be manually fulfilled.
        /// </summary>
        public class ManualFulfillmentInformation
        {
            /// <summary>
            /// The name portion of the address.
            /// </summary>
            public readonly string Name;

            /// <summary>
            /// The street portion of the address.
            /// </summary>
            public readonly string StreetAddress;

            /// <summary>
            /// The city portion of the address.
            /// </summary>
            public readonly string City;

            /// <summary>
            /// The state portion of the address.
            /// </summary>
            public readonly string State;

            /// <summary>
            /// The zip code portion of the address.
            /// </summary>
            public readonly string Zipcode;

            /// <summary>
            /// The attention portion of the address.
            /// </summary>
            public readonly string Attention;

            /// <summary>
            /// The phone number of the recipient.
            /// </summary>
            public readonly string Phone;

            /// <summary>
            /// Notes about the delivery.
            /// </summary>
            public readonly string Notes;

            /// <summary>
            /// Initializes a new instance of the <see cref="ManualFulfillmentInformation"/> class.
            /// </summary>
            /// <param name="name">The name portion of the address.</param>
            /// <param name="streetAddress">The street portion of the address.</param>
            /// <param name="city">The city portion of the address.</param>
            /// <param name="state">The state portion of the address.</param>
            /// <param name="zipcode">The zip code portion of the address.</param>
            /// <param name="attention">The attention portion of the address.</param>
            /// <param name="phone">The phone number of the recipient.</param>
            /// <param name="notes">Notes about the delivery.</param>
            public ManualFulfillmentInformation(string name, string streetAddress, string city, string state, string zipcode, string attention, string phone, string notes)
            {
                this.Name = name;
                this.StreetAddress = streetAddress;
                this.City = city;
                this.State = state;
                this.Zipcode = zipcode;
                this.Attention = attention;
                this.Phone = phone;
                this.Notes = notes;
            }
        }
    }
}
