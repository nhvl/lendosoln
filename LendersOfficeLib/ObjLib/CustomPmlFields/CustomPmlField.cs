﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Serialization;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using LendersOffice.Common;

namespace LendersOffice.CustomPmlFields
{
    /// <summary>
    /// Class to keep track of the information needed for a custom PML field.
    /// 
    /// When modifying the model, you may have to update the areas that 
    /// populate UI based on the Custom PML Fields. This includes the 
    /// PMLDefaultValues page and the PML LoanFilePricer.js file.
    /// </summary>
    public class CustomPmlField : IXmlSerializable
    {
        private string m_description = "";
        private E_CustomPmlFieldType m_type = E_CustomPmlFieldType.Blank;
        private Dictionary<int, string> m_enumMap = null;
        private int m_rank = -1;
        private int m_keywordNum = -1;
        private E_CustomPmlFieldVisibilityT m_visibility = E_CustomPmlFieldVisibilityT.AllLoans;
        
        public CustomPmlField()
        {
            m_enumMap = new Dictionary<int, string>();
        }

        public CustomPmlField(int keyword)
        {
            m_enumMap = new Dictionary<int, string>();
            m_keywordNum = keyword;
        }

        /// <summary>
        /// The label the user will see.
        /// </summary>
        public string Description
        {
            set { m_description = value; }
            get { return m_description; }
        }

        /// <summary>
        /// The type of control the field will be.
        /// </summary>
        public E_CustomPmlFieldType Type
        {
            set { m_type = value; }
            get { return m_type; }
        }

        /// <summary>
        /// Indicates which types of loans this field should be shown for.
        /// </summary>
        public E_CustomPmlFieldVisibilityT VisibilityType
        {
            set { m_visibility = value; }
            get { return m_visibility; }
        }

        public string VisibilityType_rep
        {
            get
            {
                switch (VisibilityType)
                {
                    case E_CustomPmlFieldVisibilityT.AllLoans: return "All Loans";
                    case E_CustomPmlFieldVisibilityT.PurchaseLoans: return "Purchase Loans";
                    case E_CustomPmlFieldVisibilityT.RefinanceLoans: return "Refinance Loans";
                    case E_CustomPmlFieldVisibilityT.HomeEquityLoans: return "Home Equity Loans";
                    default: throw new UnhandledEnumException(VisibilityType);
                }
            }
        }

        public bool DisplayFieldForLoanPurpose(E_sLPurposeT loanPurpose)
        {
            switch (VisibilityType)
            {
                case E_CustomPmlFieldVisibilityT.AllLoans:
                    return true;
                case E_CustomPmlFieldVisibilityT.PurchaseLoans:
                    return loanPurpose == E_sLPurposeT.Purchase;
                case E_CustomPmlFieldVisibilityT.RefinanceLoans:
                    return loanPurpose == E_sLPurposeT.Refin 
                        || loanPurpose == E_sLPurposeT.RefinCashout
                        || loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance
                        || loanPurpose == E_sLPurposeT.VaIrrrl;
                case E_CustomPmlFieldVisibilityT.HomeEquityLoans: 
                    return loanPurpose == E_sLPurposeT.HomeEquity;
                default: throw new UnhandledEnumException(VisibilityType);
            }
        }

        /// <summary>
        /// Indicates the order in which the fields should be shown. 1 corresponds to the top.
        /// </summary>
        public int Rank
        {
            set { m_rank = value; }
            get { return m_rank; }
        }

        /// <summary>
        /// This essentially id's the field.
        /// </summary>
        public int KeywordNum
        {
            get { return m_keywordNum; }
        }

        // 1/21/13 gf - opm 130492. No longer have field know it's keyword. This
        // will be determined in the pages that need to display it.
        //public string Keyword
        //{
        //    get { return String.Format("CustomPMLField_{0}", m_keywordNum); }
        //}

        [System.Web.Script.Serialization.ScriptIgnore]
        public Dictionary<int, string> EnumMap
        {
            get { return m_enumMap; }
        }

        public List<KeyValuePair<int,string>> EnumMapKeyValuePairs
        {
            get
            {
                var keyValuePairs = new List<KeyValuePair<int, string>>(m_enumMap.Keys.Count);
                foreach (int key in EnumMap.Keys)
                {
                    keyValuePairs.Add(new KeyValuePair<int, string>(key, m_enumMap[key]));
                }
                return keyValuePairs;
            }
        }

        public string EnumMapString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (int key in m_enumMap.Keys)
                {
                    sb.Append(String.Format("{0}:{1}\r\n", key, m_enumMap[key]));
                }
                // Trim to get rid of trailing newline
                return sb.ToString().TrimWhitespaceAndBOM();
            }
        }

        /// <summary>
        /// Sets the enum map key value pairs by parsing the string.
        /// Expects format [key]:[value] and that each pair is on its own line.
        /// </summary>
        /// <param name="s"></param>
        /// <exception cref="CBaseException">
        /// Thrown when the input is not correctly formatted, a duplicate key is 
        /// found, or when a blank value is found for a key.
        /// </exception>
        public void SetEnumMapFromString(string s)
        {
            if (String.IsNullOrEmpty(s))
                return;

            m_enumMap.Clear();

            string[] lines = s.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                string[] keyValue = line.Split(new char[] { ':' });
                int key;
                if (!int.TryParse(keyValue[0], out key))
                {
                    string msg = String.Format("Invalid key ({0}) for keyword. Integer value required.", keyValue[0]);
                    throw new CBaseException(msg, "Integer value required.");
                }
                string value = keyValue[1];
                if (value.TrimWhitespaceAndBOM() == "")
                {
                    throw new CBaseException("Encountered empty value in dropdown enumeration map.", 
                                             "Empty values are not allowed in enum map.");
                }
                if (m_enumMap.ContainsKey(key))
                {
                    string msg = String.Format("Duplicate enumeration map key ({0}) found for keyword. Keys must be unique.", key);
                    throw new CBaseException(msg, "Error building enum map dictionary because of duplicate key.");
                }

                m_enumMap.Add(key, value);
            }
        }

        /// <summary>
        /// Returns the descriptive representation of an enum value.
        /// Will throw an exception if it is given invalid data.
        /// </summary>
        /// <param name="val">The string representation of the key value.</param>
        /// <returns>The value of the enum map for the key</returns>
        /// <exception cref="CBaseException">
        /// Thrown when val cannot be parsed as an int or the enum map does not 
        /// contain the key.
        /// </exception>
        public string GetEnumRep(string val)
        {
            int key;
            if (!int.TryParse(val, out key))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Key to enum map is not an integer.");
            }
            else if (!m_enumMap.ContainsKey(key))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Key to enum map is out of range.");
            }

            return m_enumMap[key];
        }

        public bool IsValid
        {
            get
            {
                bool validDesc = !String.IsNullOrEmpty(m_description.TrimWhitespaceAndBOM());
                bool validType = m_type != E_CustomPmlFieldType.Blank;

                return validDesc && validType;
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Keyword", m_keywordNum.ToString("d"));
            writer.WriteAttributeString("Visibility", m_visibility.ToString("d"));
            writer.WriteElementString("Description", m_description);
            writer.WriteElementString("Type", m_type.ToString("d"));
            writer.WriteElementString("Rank", m_rank.ToString("d"));
            // write out the dictionary
            writer.WriteStartElement("EnumMap");
            foreach (int key in m_enumMap.Keys)
            {
                writer.WriteStartElement("KeyValuePair");
                writer.WriteElementString("Key", key.ToString("d"));
                writer.WriteElementString("Value", m_enumMap[key]);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        public void ReadXml(XmlReader reader)
        {
            m_keywordNum = int.Parse(reader.GetAttribute("Keyword"));
            // 10/9/2013 gf - opm 139257 added a visibility type.
            var visibility = reader.GetAttribute("Visibility");
            if (visibility != null)
            {
                m_visibility = (E_CustomPmlFieldVisibilityT)Enum.Parse(typeof(E_CustomPmlFieldVisibilityT), visibility);
            }
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmptyElement)
            {
                return;
            }

            m_description = reader.ReadElementString("Description");
            m_type = (E_CustomPmlFieldType)Enum.Parse(typeof(E_CustomPmlFieldType), reader.ReadElementString("Type"));
            m_rank = int.Parse(reader.ReadElementString("Rank"));

            // Now parse the dictionary
            isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement("EnumMap");
            if (!isEmptyElement)
            {
                while (reader.NodeType == XmlNodeType.Element && reader.Name == "KeyValuePair")
                {
                    reader.ReadStartElement("KeyValuePair");
                    int key = int.Parse(reader.ReadElementString("Key"));
                    string value = reader.ReadElementString("Value");
                    m_enumMap[key] = value;
                    reader.ReadEndElement();
                }
                reader.ReadEndElement(); // enum map
            }

            reader.ReadEndElement(); // wrapper.
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

    }

    // 1/22/14 gf - simple object for transmitting id/value to/from client.
    public class CustomPmlFieldInfo
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
