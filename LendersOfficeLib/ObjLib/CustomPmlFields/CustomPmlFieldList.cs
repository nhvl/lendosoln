﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Serialization;
using System.Collections;
using System.IO;
using LendersOffice.Common;
using System.Xml;
using System.Xml.Schema;
using LendersOffice.Constants;

namespace LendersOffice.CustomPmlFields
{
    /// <summary>
    /// Class for managing the CustomPmlFields associated with a broker.
    /// There are 20 custom fields for each broker.
    /// 
    /// The "list" of custom fields is represented internally as a dictionary.
    /// This was done because there should only ever be 1 custom field 
    /// associated with each keyword.
    /// </summary>
    public class CustomPmlFieldList : IXmlSerializable
    {
        private Dictionary<int, CustomPmlField> m_fieldsDict = null;
        private int m_numFields = 0;

        public CustomPmlFieldList()
        {
            m_fieldsDict = new Dictionary<int, CustomPmlField>();
        }

        public CustomPmlFieldList(int numFields)
        {
            m_numFields = numFields;
            m_fieldsDict = new Dictionary<int, CustomPmlField>(numFields);
        }

        public void PopulateDefaults()
        {
            CustomPmlField field;
            for (var i = 1; i <= m_numFields; i++)
            {
                field = new CustomPmlField(i);
                field.Rank = i;
                m_fieldsDict.Add(i, field);
            }
        }

        public List<CustomPmlField> Fields
        {
            get { return m_fieldsDict.Values.ToList(); }
        }

        public List<CustomPmlField> FieldsSortedByRank
        {
            get { return Fields.OrderBy(x => x.Rank).ToList(); }
        }

        public List<CustomPmlField> ValidFieldsSortedByRank
        {
            get { return FieldsSortedByRank.Where(x => x.IsValid).ToList(); }
        }

        /// <summary>
        /// Add the given field to the collection of fields.
        /// Will throw an exception if there is already a field with the same
        /// keyword in the list.
        /// </summary>
        /// <param name="field">The field to add.</param>
        /// <exception cref="CBaseException">
        /// Thrown when there is already a field for the same keyword in the list.
        /// </exception>
        public void Add(CustomPmlField field)
        {
            bool invalidField = m_fieldsDict.ContainsKey(field.KeywordNum);
            if (invalidField)
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to add the same keyword multiple times.");
            }
            m_fieldsDict.Add(field.KeywordNum, field);
        }

        /// <summary>
        /// Get the given custom field.
        /// </summary>
        /// <param name="customFieldNum"></param>
        /// <returns></returns>
        public CustomPmlField Get(int customFieldNum)
        {
            if (!m_fieldsDict.ContainsKey(customFieldNum))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to get non-existent custom pml field.");
            }
            return m_fieldsDict[customFieldNum];
        }

        /// <summary>
        /// Retrieve the type for the field that corresponds to keyword CustomPmlField_X.
        /// </summary>
        /// <param name="customFieldNum">The keyword number for the field.</param>
        /// <returns>
        /// The type of the field that corresponds to the given keyword number.
        /// </returns>
        /// <exception cref="CBaseException">Thrown when the given keyword number does not exist.</exception>
        public E_CustomPmlFieldType GetTypeForField(int customFieldNum)
        {
            if (!m_fieldsDict.ContainsKey(customFieldNum))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to get type of non-existent custom pml field.");
            }
            return m_fieldsDict[customFieldNum].Type;
        }

        /// <summary>
        /// Determine if the given key is a valid option for a field.
        /// </summary>
        /// <param name="customFieldNum">The keyword number for the field.</param>
        /// <param name="key">The key to test.</param>
        /// <returns>True if the enum map for the field contains the key. Otherwise, false.</returns>
        /// <exception cref="CBaseException">Thrown when the given keyword number does not exist.</exception>
        public bool IsValidKeyForField(int customFieldNum, int key)
        {
            if (!m_fieldsDict.ContainsKey(customFieldNum))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to access non-existent custom pml field.");
            }
            return m_fieldsDict[customFieldNum].EnumMap.ContainsKey(key);
        }

        /// <summary>
        /// Determine if a field is defined with a valid description and type.
        /// </summary>
        /// <param name="customFieldNum">Custom field number to check.</param>
        /// <returns>True if it has a valid description and type. Otherwise, false.</returns>
        /// <exception cref="CBaseException">Thrown when the given keyword number does not exist.</exception>
        public bool IsFieldDefined(int customFieldNum)
        {
            if (!m_fieldsDict.ContainsKey(customFieldNum))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to get validity of non-existent custom pml field.");
            }
            return m_fieldsDict[customFieldNum].IsValid;
        }

        /// <summary>
        /// Determine if the field should have a valid value for the loan purpose.
        /// </summary>
        /// <param name="customFieldNum"></param>
        /// <param name="loanPurpose"></param>
        /// <returns>True if the field should have a value. Otherwise, false.</returns>
        public bool IsFieldValidForLoanPurpose(int customFieldNum, E_sLPurposeT loanPurpose)
        {
            if (!m_fieldsDict.ContainsKey(customFieldNum))
            {
                throw new CBaseException("Error processing Custom PML Fields.", "Trying to get validity of non-existent custom pml field.");
            }
            return m_fieldsDict[customFieldNum].DisplayFieldForLoanPurpose(loanPurpose);
        }

        /// <summary>
        /// Deserialize the string into instance of list.
        /// </summary>
        /// <param name="sXml">XML string.</param>
        /// <param name="defaultNumFields">The number of fields to populate if de-serialization fails.</param>
        /// <returns>Deserialized instance. If the string is null or empty it returns a new default object.</returns>
        public static CustomPmlFieldList ToObject(String sXml, int defaultNumFields)
        {
            if (String.IsNullOrEmpty(sXml))
            {
                var list = new CustomPmlFieldList(defaultNumFields);
                list.PopulateDefaults();
                return list;
            }

            // Deserialize the string into a list
            XmlSerializer serializer = new XmlSerializer(typeof(CustomPmlFieldList));
            StringReader reader = new StringReader(sXml);

            try
            {
                var fields = serializer.Deserialize(reader) as CustomPmlFieldList;

                while (fields.m_fieldsDict.Count < defaultNumFields)
                {
                    var fieldToAdd = new CustomPmlField(fields.m_fieldsDict.Count + 1)
                    {
                        Rank = fields.m_fieldsDict.Count + 1
                    };

                    fields.m_fieldsDict.Add(fieldToAdd.KeywordNum, fieldToAdd);
                }

                return fields;
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError(exc);

                var list = new CustomPmlFieldList(defaultNumFields);
                list.PopulateDefaults();
                return list;
            }
        }

        /// <summary>
        /// Serialize this instance as XML document.
        /// </summary>
        /// <returns>String representation of this instance as XML</returns>
        public override string ToString()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomPmlFieldList));
            StringWriter8 writer = new StringWriter8();

            serializer.Serialize(writer, this);

            return writer.ToString();
        }

        public void WriteXml(XmlWriter writer)
        {
            // Just write out all of the custom PML fields in the list.
            var serializer = new XmlSerializer(typeof(CustomPmlField));
            foreach (CustomPmlField field in m_fieldsDict.Values)
            {
                serializer.Serialize(writer, field);
            }
        }

        public void ReadXml(XmlReader reader)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmptyElement)
            {
                return;
            }
            // Deserialize all of the custom PML fields.
            var serializer = new XmlSerializer(typeof(CustomPmlField));
            while (reader.NodeType == XmlNodeType.Element && reader.Name == "CustomPmlField")
            {
                var field = (CustomPmlField)serializer.Deserialize(reader);
                m_fieldsDict.Add(field.KeywordNum, field);
            }
            reader.ReadEndElement();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }
    }
}
