﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;

namespace LendersOffice.CustomPmlFields
{
    /// <summary>
    /// Common functions for displaying the custom PML fields on a loan editor page.
    /// </summary>
    public class CustomPmlFieldUtilities
    {
        /// <summary>
        /// Bind the enum map associated with the custom field to the DDL.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="field"></param>
        public static void BindCustomFieldDDL(DropDownList ddl, CustomPmlField field)
        {
            ListItem item = new ListItem("", "");
            ddl.Items.Add(item);

            foreach (int key in field.EnumMap.Keys)
            {
                item = new ListItem(field.EnumMap[key], key.ToString("d"));
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// Set the preset to either money, percent, or numeric based on field type and add the CustomPMLField class.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        public static void SetControlAttributesForFieldType(WebControl control, E_CustomPmlFieldType type)
        {
            switch (type)
            {
                case E_CustomPmlFieldType.NumericMoney:
                    control.Attributes["preset"] = "money";
                    break;
                case E_CustomPmlFieldType.NumericPercent:
                    control.Attributes["preset"] = "percent";
                    break;
                case E_CustomPmlFieldType.NumericGeneric:
                    control.Attributes["preset"] = "numeric";
                    break;
            }
            control.Attributes["class"] = control.Attributes["class"] + " CustomPMLField";
        }

        /// <summary>
        /// Set the value of a custom pml field input control from a string value.
        /// Supports TextBox and DDL.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="value"></param>
        public static void SetControlValue(WebControl control, string value)
        {
            if (control is TextBox)
            {
                ((TextBox)control).Text = value;
            }
            else if (control is DropDownList)
            {
                var ddl = control as DropDownList;
                int intVal;
                if (int.TryParse(value, out intVal))
                {
                    Tools.SetDropDownListValue(ddl, intVal);
                }
                // If this cannot be parsed as an int then don't set the value.
            }
        }

        /// <summary>
        /// Get a control to display the field type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static WebControl GetControlForFieldType(E_CustomPmlFieldType type)
        {
            switch (type)
            {
                case E_CustomPmlFieldType.Dropdown:
                    return new DropDownList();
                case E_CustomPmlFieldType.NumericGeneric:
                case E_CustomPmlFieldType.NumericMoney:
                case E_CustomPmlFieldType.NumericPercent:
                    return new TextBox();
                case E_CustomPmlFieldType.Blank:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid field type.");
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static IEnumerable<HtmlTableRow> GetLoanOfficerPricingTableRows(Guid brokerId)
        {
            var brokerDB = BrokerDB.RetrieveById(brokerId);
            var customFields = brokerDB.CustomPricingPolicyFieldList;
            var rows = new List<HtmlTableRow>();

            foreach (var field in brokerDB.CustomPricingPolicyFieldList.ValidFieldsSortedByRank)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell labelCell = new HtmlTableCell();
                labelCell.Attributes["class"] = "FieldLabel CustomLOPPFieldLabel";
                HtmlTableCell fieldCell = new HtmlTableCell();

                Literal label = new Literal();
                label.Text = field.Description;
                labelCell.Controls.Add(label);

                WebControl control = CustomPmlFieldUtilities.GetControlForFieldType(field.Type);
                control.ID = String.Format("CustomPricingPolicyField{0}", field.KeywordNum);
                control.Attributes["class"] = control.ID;
                CustomPmlFieldUtilities.SetControlAttributesForFieldType(control, field.Type);

                if (control is DropDownList)
                {
                    CustomPmlFieldUtilities.BindCustomFieldDDL(control as DropDownList, field);
                }

                fieldCell.Controls.Add(control);

                label = new Literal();
                label.Text = field.VisibilityType_rep;
                row.Controls.Add(labelCell);
                row.Controls.Add(fieldCell);

                rows.Add(row);
            }

            return rows;
        }

        public static List<CustomPmlFieldInfo> GetCustomPricingPolicyInfoForEmployee(BrokerDB brokerDB, EmployeeDB empDB)
        {
            var customPricingPolicyFields = new List<CustomPmlFieldInfo>();
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(1))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField1", Value = empDB.CustomPricingPolicyField1Fixed });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(2))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField2", Value = empDB.CustomPricingPolicyField2Fixed });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(3))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField3", Value = empDB.CustomPricingPolicyField3Fixed });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(4))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField4", Value = empDB.CustomPricingPolicyField4Fixed });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(5))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField5", Value = empDB.CustomPricingPolicyField5Fixed });
            }
            return customPricingPolicyFields;
        }
    }
}
