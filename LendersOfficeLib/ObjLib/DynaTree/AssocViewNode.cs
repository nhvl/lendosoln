﻿namespace LendersOffice.ObjLib.DynaTree
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Dynatree Node for AssocView page with custom elements.
    /// </summary>
    public class AssocViewNode : DynaTreeNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssocViewNode"/> class.
        /// </summary>
        public AssocViewNode() : base()
        {
            // For AssocViewNode, always hide the folder/page icon.
            // Also, hide the checkbox by default.
            this.addClass = "no-icon hide-checkbox";
        }

        /// <summary>
        /// Bit to determine the visibility of the checkbox for an AssocViewNode
        /// </summary>
        public bool CheckBox { get; set; }       

        /// <summary>
        /// Defines type for the node.
        /// </summary>
        public string Type { get; set; }
    }
}
