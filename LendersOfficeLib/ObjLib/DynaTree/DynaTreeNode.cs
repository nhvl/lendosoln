﻿namespace LendersOffice.ObjLib.DynaTree
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for dynatree nodes.
    /// See node options (section 4.2.1) of http://wwwendt.de/tech/dynatree/doc/dynatree-doc.html for reference.
    /// </summary>
    public class DynaTreeNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DynaTreeNode"/> class.
        /// </summary>
        public DynaTreeNode()
        {
            children = new List<DynaTreeNode>();
            customAttributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Defines the title of the node.
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// Determines whether the node is a folder.
        /// </summary>
        public bool isFolder { get; set; }

        /// <summary>
        /// An ID that uniquely identifies the node.
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Stores links on the node.
        /// </summary>
        public string href { get; set; }

        /// <summary>
        /// Initial expanded status.
        /// </summary>
        public bool expand { get; set; }

        /// <summary>
        /// Initial select status.
        /// </summary>
        public bool select { get; set; }

        /// <summary>
        /// Custom image for the node.
        /// </summary>
        public string icon { get; set; }

        /// <summary>
        /// Used to add css classes to the node.
        /// </summary>
        public string addClass { get; set; }

        /// <summary>
        /// List of children dynatree nodes.
        /// </summary>
        public List<DynaTreeNode> children { get; set; }

        /// <summary>
        /// The name of the method to run when the node is created.
        /// </summary>
        public string onCreate { get; set; }

        /// <summary>
        /// Gets or sets the node's custom attributes.
        /// </summary>
        public Dictionary<string, string> customAttributes { get; set; }

        /// <summary>
        /// Gets or sets whether the node renders immediately or if it renders when the node expands the parent.
        /// </summary>
        public bool isLazy { get; set; } = false;

        /// <summary>
        /// Gets or sets whether a node should be expanded after its been created.
        /// </summary>
        public bool isCollapsedAfterRender { get; set; }

        /// <summary>
        /// Gets or sets whether a node should display a checkbox or not.
        /// </summary>
        public bool hideCheckbox { get; set; } = false;
    }
}
