﻿namespace LendersOffice.ObjLib.Events
{
    using System;

    /// <summary>
    /// Provides a simple container for settings
    /// when adding a new <see cref="LoanEvent{TEnum}"/>.
    /// </summary>
    /// <typeparam name="TEnum">
    /// The type of the event.
    /// </typeparam>
    public class LoanEventAdditionSettings<TEnum>
    {
        /// <summary>
        /// Gets or sets the type of the event.
        /// </summary>
        /// <value>
        /// The type of the event.
        /// </value>
        public TEnum EventType { get; set; }

        /// <summary>
        /// Gets or sets the notes for the event.
        /// </summary>
        /// <value>
        /// The notes for the event.
        /// </value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the source for the event.
        /// </summary>
        /// <value>
        /// The source for the event.
        /// </value>
        /// <remarks>
        /// If not supplied, <seealso cref="LoanEventSource.Other"/>
        /// will be used.
        /// </remarks>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the reason for the creation of the loan event.
        /// </summary>
        /// <value>
        /// The reason for the creation of the loan event.
        /// </value>
        public string CreationReason { get; set; }

        /// <summary>
        /// Gets or sets any supplemental data to store for the event.
        /// </summary>
        /// <value>
        /// Any supplemental data to store for the event.
        /// </value>
        public string SupplementalData { get; set; }

        /// <summary>
        /// Gets or sets the date and time for when the event was created.
        /// </summary>
        /// <value>
        /// The date and time for when the event was created.
        /// </value>
        public DateTime? Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the ID of an associated document for the event, if any.
        /// </summary>
        /// <value>
        /// The ID of an associated document for the event or null if no document
        /// is associated with the event.
        /// </value>
        public Guid? AssociatedDocumentId { get; set; }
    }
}
