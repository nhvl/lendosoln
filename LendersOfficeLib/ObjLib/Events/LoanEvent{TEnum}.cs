﻿namespace LendersOffice.ObjLib.Events
{
    using System;

    /// <summary>
    /// Represents a generic loan event.
    /// </summary>
    /// <typeparam name="TEnum">
    /// The type of the event.
    /// </typeparam>
    public class LoanEvent<TEnum>
    {
        /// <summary>
        /// Gets or sets the ID for the event.
        /// </summary>
        /// <value>
        /// The ID for the event.
        /// </value>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Gets or sets the type of the event.
        /// </summary>
        /// <value>
        /// The type of the event.
        /// </value>
        public TEnum Type { get; set; }

        /// <summary>
        /// Gets or sets the timestamp of the event.
        /// </summary>
        /// <value>
        /// The timestamp of the event.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the notes for the event.
        /// </summary>
        /// <value>
        /// The notes for the event.
        /// </value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the source of the loan event.
        /// </summary>
        /// <value>
        /// The source for the loan event.
        /// </value>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the reason for why the event was created.
        /// </summary>
        /// <value>
        /// The reason why the event was created.
        /// </value>
        public string CreationReason { get; set; }

        /// <summary>
        /// Gets or sets any supplemental data for the loan event.
        /// </summary>
        /// <value>
        /// The supplemental data for the loan event.
        /// </value>
        public string SupplementalData { get; set; }

        /// <summary>
        /// Gets or sets the ID of the associated audit for the event, if any.
        /// </summary>
        /// <value>
        /// The ID of the associated audit for the event or null if no association exists.
        /// </value>
        public Guid? AssociatedAuditId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the associated document for the event, if any.
        /// </summary>
        /// <value>
        /// The ID of the associated document for the event or null if no association exists.
        /// </value>
        public Guid? AssociatedDocumentId { get; set; }
    }
}
