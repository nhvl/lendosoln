﻿namespace LendersOffice.ObjLib.Events
{
    /// <summary>
    /// Provides a collection of source locations for <see cref="LoanEvent{TEnum}.Source"/>.
    /// </summary>
    public static class LoanEventSource
    {
        /// <summary>
        /// The event was sourced from a location not defined by
        /// other members of this class.
        /// </summary>
        public const string Other = "Other";

        /// <summary>
        /// Loan event sources for disclosures.
        /// </summary>
        public static class Disclosure
        {
            /// <summary>
            /// Event was sourced from the Disclosure Center.
            /// </summary>
            public const string DisclosureCenter = "LendingQB Disclosure Center";

            /// <summary>
            /// Loan events for the Originator Portal initial disclosures wizard.
            /// </summary>
            public static class OriginatorPortalInitialDisclosures
            {
                /// <summary>
                /// Event was sourced from the first step of the wizard.
                /// </summary>
                public const string StepOne = "Originator Portal Ordering Wizard - 1. Request Review";

                /// <summary>
                /// Event was sourced from the second step of the wizard.
                /// </summary>
                public const string StepTwo = "Originator Portal Ordering Wizard - 2. Document Vendor Audit";

                /// <summary>
                /// Event was sourced from the third step of the wizard.
                /// </summary>
                public const string StepThree = "Originator Portal Ordering Wizard - 3. Review Document Preview";

                /// <summary>
                /// Event was sourced from the fourth step of the wizard.
                /// </summary>
                public const string StepFour = "Originator Portal Ordering Wizard - 4. Request Complete";
            }

            /// <summary>
            /// Loan events for the Originator Portal initial disclosures wizard.
            /// </summary>
            public static class OriginatorPortalInitialDisclosuresWithAntiSteering
            {
                /// <summary>
                /// Event was sourced from the first step of the wizard.
                /// </summary>
                public const string StepOne = "Originator Portal Ordering Wizard - 1. Anti-Steering Disclosure";

                /// <summary>
                /// Event was sourced from the first step of the wizard.
                /// </summary>
                public const string StepTwo = "Originator Portal Ordering Wizard - 2. Request Review";

                /// <summary>
                /// Event was sourced from the second step of the wizard.
                /// </summary>
                public const string StepThree = "Originator Portal Ordering Wizard - 3. Document Vendor Audit";

                /// <summary>
                /// Event was sourced from the third step of the wizard.
                /// </summary>
                public const string StepFour = "Originator Portal Ordering Wizard - 4. Review Document Preview";

                /// <summary>
                /// Event was sourced from the fourth step of the wizard.
                /// </summary>
                public const string StepFive = "Originator Portal Ordering Wizard - 5. Request Complete";
            }

            /// <summary>
            /// Loan events for the Originator Portal initial disclosures wizard using the now obsoleted
            /// "TPO" branding. Use <see cref="OriginatorPortalInitialDisclosures"/> instead, as this is
            /// kept for legacy loan event sources.
            /// </summary>
            public static class TpoPortalInitialDisclosures_Obsolete
            {
                /// <summary>
                /// Event was sourced from the first step of the wizard.
                /// </summary>
                public const string StepOne = "TPO Ordering Wizard - 1. Request Review";

                /// <summary>
                /// Event was sourced from the second step of the wizard.
                /// </summary>
                public const string StepTwo = "TPO Ordering Wizard - 2. Document Vendor Audit";
            }
        }
    }
}
