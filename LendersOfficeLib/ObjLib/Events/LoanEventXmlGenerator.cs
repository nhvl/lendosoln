﻿namespace LendersOffice.ObjLib.Events
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Common;
    using ConfigSystem.ChecklistGeneration;

    /// <summary>
    /// Provides a set of utilities for generating XML from loan events.
    /// </summary>
    public static class LoanEventXmlGenerator
    {
        /// <summary>
        /// Represents the initial capacity for the <seealso cref="StringBuilder"/>
        /// containing the generated XML.
        /// </summary>
        private const int InitialXmlCapacity = 5000;

        /// <summary>
        /// Generates XML for the specified <paramref name="collection"/>.
        /// </summary>
        /// <typeparam name="TEnum">
        /// The type of events in the collection.
        /// </typeparam>
        /// <param name="collection">
        /// The collection of loan events.
        /// </param>
        /// <returns>
        /// The generated XML.
        /// </returns>
        public static string GenerateXml<TEnum>(IEnumerable<LoanEvent<TEnum>> collection)
        {
            var sb = new StringBuilder(InitialXmlCapacity);
            using (var writer = new XmlTextWriter(new StringWriter(sb)))
            {
                writer.WriteStartElement("events");

                foreach (var loanEvent in collection)
                {
                    GenerateXml(writer, loanEvent);
                }

                writer.WriteEndElement(); // events
            }

            return sb.ToString();
        }

        /// <summary>
        /// Generates XML for the specified <paramref name="loanEvent"/>.
        /// </summary>
        /// <typeparam name="TEnum">
        /// The type of the event.
        /// </typeparam>
        /// <param name="writer">
        /// The writer to write data.
        /// </param>
        /// <param name="loanEvent">
        /// The loan event.
        /// </param>
        public static void GenerateXml<TEnum>(XmlWriter writer, LoanEvent<TEnum> loanEvent)
        {
            writer.WriteStartElement("data");

            WriteAttribute(writer, nameof(loanEvent.Id), loanEvent.Id.ToString());
            WriteAttribute(writer, nameof(loanEvent.Timestamp), loanEvent.Timestamp.ToString("s")); // ISO 8601 format
            WriteAttribute(writer, nameof(loanEvent.Notes), loanEvent.Notes);
            WriteAttribute(writer, nameof(loanEvent.Source), loanEvent.Source);
            WriteAttribute(writer, nameof(loanEvent.CreationReason), loanEvent.CreationReason);

            if (string.IsNullOrEmpty(loanEvent.SupplementalData))
            {
                WriteAttribute(writer, "AuditIndicator", "N");
                WriteAttribute(writer, "RequirementsIndicator", "N");
            }
            else
            {
                WriteSupplementalData(writer, loanEvent);
            }

            writer.WriteEndElement(); // data
        }

        /// <summary>
        /// Writes supplemental data for the specified <paramref name="loanEvent"/>.
        /// </summary>
        /// <typeparam name="TEnum">
        /// The type of the event.
        /// </typeparam>
        /// <param name="writer">
        /// The writer to write data.
        /// </param>
        /// <param name="loanEvent">
        /// The loan event.
        /// </param>
        private static void WriteSupplementalData<TEnum>(XmlWriter writer, LoanEvent<TEnum> loanEvent)
        {
            var hasRequirementsIndicator = loanEvent.Source.EqualsOneOf(
                LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepOne,
                LoanEventSource.Disclosure.TpoPortalInitialDisclosures_Obsolete.StepOne);

            var hasAuditIndicator = loanEvent.Source.EqualsOneOf(
                LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepTwo,
                LoanEventSource.Disclosure.TpoPortalInitialDisclosures_Obsolete.StepTwo);

            var indicators = new Dictionary<string, bool>
            {
                ["RequirementsIndicator"] = hasRequirementsIndicator,
                ["AuditIndicator"] = hasAuditIndicator
            };

            foreach (var indicator in indicators)
            {
                WriteAttribute(writer, indicator.Key, indicator.Value ? "Y" : "N");
            }

            if (hasRequirementsIndicator)
            {
                WriteRequirementsChecklist(writer, loanEvent);
                return;
            }

            if (hasAuditIndicator)
            {
                WriteLoanAuditList(writer, loanEvent);
            }
        }

        /// <summary>
        /// Writes the "Requirements Checklist" for the loan.
        /// </summary>
        /// <typeparam name="TEnum">
        /// The type of the event.
        /// </typeparam>
        /// <param name="writer">
        /// The writer to write data.
        /// </param>
        /// <param name="loanEvent">
        /// The loan event.
        /// </param>
        private static void WriteRequirementsChecklist<TEnum>(XmlWriter writer, LoanEvent<TEnum> loanEvent)
        {
            writer.WriteStartElement("requirements");

            var checklist = SerializationHelper.JsonNetDeserialize<List<WorkflowResultChecklistItem>>(loanEvent.SupplementalData);
            foreach (var item in checklist)
            {
                writer.WriteStartElement("item");

                writer.WriteAttributeString("Result", item.Pass.ToString());
                writer.WriteAttributeString("Description", item.Description);

                writer.WriteEndElement(); // item
            }

            writer.WriteEndElement(); // requirements
        }

        /// <summary>
        /// Writes the audit line items returned by the vendor for the loan.
        /// </summary>
        /// <typeparam name="TEnum">
        /// The type of the event.
        /// </typeparam>
        /// <param name="writer">
        /// The writer to write data.
        /// </param>
        /// <param name="loanEvent">
        /// The loan event.
        /// </param>
        private static void WriteLoanAuditList<TEnum>(XmlWriter writer, LoanEvent<TEnum> loanEvent)
        {
            writer.WriteStartElement("audit");

            var auditItems = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<string, string>>>(loanEvent.SupplementalData);
            foreach (var auditLine in auditItems)
            {
                writer.WriteStartElement("item");

                writer.WriteAttributeString("Severity", auditLine.Key);
                writer.WriteAttributeString("Message", auditLine.Value);

                writer.WriteEndElement(); // item
            }

            writer.WriteEndElement(); // audit
        }

        /// <summary>
        /// Writes an attribute if the value if not null or empty.
        /// </summary>
        /// <param name="writer">
        /// The writer to write data.
        /// </param>
        /// <param name="name">
        /// The name of the attribute.
        /// </param>
        /// <param name="value">
        /// The value of the attribute.
        /// </param>
        private static void WriteAttribute(XmlWriter writer, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                writer.WriteAttributeString(name, value);
            }
        }
    }
}
