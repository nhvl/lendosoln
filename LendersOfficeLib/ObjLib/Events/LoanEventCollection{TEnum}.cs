﻿namespace LendersOffice.ObjLib.Events
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a collection of loan events.
    /// </summary>
    /// <typeparam name="TEnum">
    /// The type of the event, represented as an enumeration.
    /// </typeparam>
    public abstract class LoanEventCollection<TEnum> : IEnumerable<LoanEvent<TEnum>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEventCollection{TEnum}"/> class.
        /// </summary>
        /// <param name="json">
        /// The JSON string containing the events.
        /// </param>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        protected LoanEventCollection(string json, LosConvert losConvert, Action<AbstractAuditItem> auditAddAction)
            : this(losConvert, auditAddAction)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                this.Events = new List<LoanEvent<TEnum>>();
            }
            else
            {
                this.Events = SerializationHelper.JsonNetDeserialize<List<LoanEvent<TEnum>>>(json);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEventCollection{TEnum}"/> class.
        /// </summary>
        /// <param name="events">
        /// The events for the collection.
        /// </param>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        protected LoanEventCollection(List<LoanEvent<TEnum>> events, LosConvert losConvert, Action<AbstractAuditItem> auditAddAction)
            : this(losConvert, auditAddAction)
        {
            this.Events = events;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEventCollection{TEnum}"/> class.
        /// </summary>
        /// <param name="losConvert">
        /// The converter used to render dates.
        /// </param>
        /// <param name="auditAddAction">
        /// The action used to add audits to the loan.
        /// </param>
        private LoanEventCollection(LosConvert losConvert, Action<AbstractAuditItem> auditAddAction)
        {
            this.LosConvert = losConvert;
            this.AuditAddAction = auditAddAction;
        }

        /// <summary>
        /// Gets the status of the latest event on file.
        /// </summary>
        /// <value>
        /// The status of the latest event on file or <code>default(T)</code>
        /// if no events have been logged.
        /// </value>
        public abstract TEnum LatestEventStatus { get; }

        /// <summary>
        /// Gets the ID of the latest event in the collection.
        /// </summary>
        /// <value>
        /// The ID of the latest event in the collection.
        /// </value>
        public Guid? LatestEventId => this.LatestEvent?.Id;

        /// <summary>
        /// Gets the status of the latest event on file in string format.
        /// </summary>
        /// <value>
        /// The status of the latest event on file in string format.
        /// </value>
        public abstract string LatestEventStatusRep { get; }

        /// <summary>
        /// Gets the date and time of the latest event on file.
        /// </summary>
        /// <value>
        /// The date and time of the latest event on file.
        /// </value>
        public CDateTime LatestEventDatetime => CDateTime.Create(this.LatestEvent?.Timestamp ?? CDateTime.InvalidDateTime);

        /// <summary>
        /// Gets the date and time of the latest event on file in string format.
        /// </summary>
        /// <value>
        /// The date and time of the latest event on file in string format.
        /// </value>
        public string LatestEventDatetimeRep => this.LatestEventDatetime.ToStringWithTime("G");

        /// <summary>
        /// Gets the notes for the latest event on file.
        /// </summary>
        /// <value>
        /// The notes for the latest event on file.
        /// </value>
        public string LatestEventNotes => this.LatestEvent?.Notes ?? string.Empty;

        /// <summary>
        /// Gets the ID of the audit item associated with the latest event, if any.
        /// </summary>
        /// <value>
        /// The ID of the audit item associated with the latest event or null if
        /// no association exists.
        /// </value>
        public Guid? LatestEventAssociatedAuditId => this.LatestEvent?.AssociatedAuditId;

        /// <summary>
        /// Gets the ID of the document associated with the latest event, if any.
        /// </summary>
        /// <value>
        /// The ID of the document associated with the latest event or null if
        /// no association exists.
        /// </value>
        public Guid? LatestEventAssociatedDocId => this.LatestEvent?.AssociatedDocumentId;

        /// <summary>
        /// Gets the latest event on file.
        /// </summary>
        /// <value>
        /// The latest event on file or null if no events are on file.
        /// </value>
        protected LoanEvent<TEnum> LatestEvent => this.Events.OrderByDescending(disclosureEvent => disclosureEvent.Timestamp).FirstOrDefault();

        /// <summary>
        /// Gets or sets the value for the list of events.
        /// </summary>
        /// <value>
        /// The list of events.
        /// </value>
        private List<LoanEvent<TEnum>> Events { get; set; }

        /// <summary>
        /// Gets or sets the converter used to render dates.
        /// </summary>
        /// <value>
        /// The converter used to render dates.
        /// </value>
        private LosConvert LosConvert { get; set; }

        /// <summary>
        /// Gets or sets the action used to add an audit item to the loan.
        /// </summary>
        /// <value>
        /// The action used to add audit items.
        /// </value>
        private Action<AbstractAuditItem> AuditAddAction { get; set; }

        /// <summary>
        /// Determines whether the collection has an event with the specified <paramref name="eventId"/>.
        /// </summary>
        /// <param name="eventId">
        /// The ID of the event to check.
        /// </param>
        /// <returns>
        /// True if the collection has an event with the ID, false otherwise.
        /// </returns>
        public bool HasEvent(Guid eventId) => this.Events.Any(loanEvent => loanEvent.Id == eventId);

        /// <summary>
        /// Gets the event list for this collection.
        /// </summary>
        /// <returns>
        /// The event list for this collection.
        /// </returns>
        public IReadOnlyList<LoanEvent<TEnum>> GetEventList() => this.Events.AsReadOnly();

        /// <summary>
        /// Adds an event to the collection, recording an audit item to the loan.
        /// </summary>
        /// <param name="principal">
        /// The principal adding the event.
        /// </param>
        /// <param name="settings">
        /// The settings for the new event.
        /// </param>
        public virtual void AddEvent(AbstractUserPrincipal principal, LoanEventAdditionSettings<TEnum> settings)
        {
            if (!this.IsAllowedTransitionToNextType(settings.EventType))
            {
                throw new GenericUserErrorMessageException($"Transition from {this.LatestEventStatus} to {settings.EventType} is not valid.");
            }

            var newEvent = new LoanEvent<TEnum>()
            {
                Type = settings.EventType,
                Notes = settings.Notes,
                Source = settings.Source ?? LoanEventSource.Other,
                CreationReason = settings.CreationReason,
                SupplementalData = settings.SupplementalData,
                Timestamp = settings.Timestamp ?? DateTime.Now,
                AssociatedDocumentId = settings.AssociatedDocumentId
            };

            var auditItem = this.CreateAuditItem(principal, newEvent);
            newEvent.AssociatedAuditId = auditItem.ID;

            this.AuditAddAction(auditItem);
            this.Events.Add(newEvent);
        }

        /// <summary>
        /// Gets the enumerator for the collection.
        /// </summary>
        /// <returns>
        /// The enumerator for the collection.
        /// </returns>
        public IEnumerator<LoanEvent<TEnum>> GetEnumerator() => this.Events.GetEnumerator();

        /// <summary>
        /// Gets the enumerator for the collection.
        /// </summary>
        /// <returns>
        /// The enumerator for the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator() => this.Events.GetEnumerator();

        /// <summary>
        /// Converts this instance to a serialized string.
        /// </summary>
        /// <returns>
        /// This instance as a serialized string.
        /// </returns>
        public override string ToString() => SerializationHelper.JsonNetSerialize(this.Events);

        /// <summary>
        /// Gets a value indicating whether a transition is allowed from <see cref="LatestEventStatus"/>
        /// to <paramref name="newStatus"/>.
        /// </summary>
        /// <param name="newStatus">
        /// The new status to check.
        /// </param>
        /// <returns>
        /// True if the transition is allowed, false otherwise.
        /// </returns>
        protected abstract bool IsAllowedTransitionToNextType(TEnum newStatus);

        /// <summary>
        /// Creates an audit item using the specified <paramref name="principal"/>.
        /// </summary>
        /// <param name="principal">
        /// The principal for the audit.
        /// </param>
        /// <param name="eventForAudit">
        /// The new event.
        /// </param>
        /// <returns>
        /// The audit item.
        /// </returns>
        protected abstract AbstractAuditItem CreateAuditItem(AbstractUserPrincipal principal, LoanEvent<TEnum> eventForAudit);
    }
}
