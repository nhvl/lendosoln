﻿// <copyright file="XmlSanitizingStream.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   8/27/2014 3:16:57 PM 
// </summary>
namespace LendersOffice.ObjLib.XML
{
    using System;
    using System.IO;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// Removes illegal characters from the stream in real time. If you use
    /// this stream do know that the length property is not correct. 
    /// Note: This class needs more testing.
    /// </summary>
    public class XmlSanitizingStream : StreamReader
    {
        /// <summary>
        /// Represents the value of End of file.
        /// </summary>
        private const int EOF = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlSanitizingStream" /> class.
        /// </summary>
        /// <param name="streamToSanitize">The stream to sanitize.</param>
        public XmlSanitizingStream(Stream streamToSanitize)
            : base(streamToSanitize, true) 
        {
        }

        /// <summary> 
        /// Reads the next xml safe value from the base stream. If the 
        /// next value is not safe it will move on to the next one.
        /// </summary>
        /// <returns>A valid xml value from the base stream.</returns>
        public override int Read()
        {
            int nextCharacter;

            do
            {
                if ((nextCharacter = base.Read()) == EOF)
                {
                    break;
                }
            }
            while (!Tools.IsLegalXmlChar(nextCharacter));

            return nextCharacter;
        }

        /// <summary> 
        /// Reads the next value without moving the stream forward. This will 
        /// find the next valid xml value.
        /// </summary>
        /// <returns>A valid xml value from the base stream.</returns>
        public override int Peek()
        {
            int nextCharacter;

            bool keepLooking; 

            do
            {
                nextCharacter = base.Peek();
                keepLooking = !Tools.IsLegalXmlChar(nextCharacter) && (nextCharacter = base.Read()) != EOF;
            }
            while (keepLooking);

            return nextCharacter;
        }

        /// <summary> 
        /// Reads a specified maximum number of characters from the current stream into
        /// a buffer, beginning at the specified index.
        /// </summary>
        /// <param name="buffer">
        /// When this method returns, contains the specified character array with the
        /// values between index and (index + count - 1) replaced by the characters read
        /// from the current source.
        /// </param>
        /// <param name="index">The index of buffer at which to begin writing.</param>
        /// <param name="count"> The maximum number of characters to read.</param>
        /// <returns>
        /// The number of characters that have been read, or 0 if at the end of the stream
        /// and no data was read. The number will be less than or equal to the count
        /// parameter, depending on whether the data is available within the stream.</returns>
        public override int Read(char[] buffer, int index, int count)
        {
            if (buffer == null) 
            {
                throw new ArgumentNullException("buffer");
            }

            if (index < 0) 
            {
                throw new ArgumentOutOfRangeException("index");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if ((buffer.Length - index) < count) 
            {
                throw new ArgumentException();
            }

            int num = 0;

            do 
            {
                int num2 = this.Read();
                
                if (num2 == EOF)
                {
                    return num;
                }

                buffer[index + num++] = (char)num2;
            }
            while (num < count);

            return num;
        }

        /// <summary> Reads a specified maximum number of characters from the current stream into
        /// a buffer, beginning at the specified index. This will keep going until the buffer is
        /// filled unlike Read.
        /// </summary>
        /// <param name="buffer"> When this method returns, contains the specified character array with the
        /// values between index and (index + count - 1) replaced by the characters read
        /// from the current source.
        /// </param>
        /// <param name="index">The index of buffer at which to begin writing.</param>
        /// <param name="count"> The maximum number of characters to read.</param>
        /// <returns> The number of characters that have been read, or 0 if at the end of the stream
        /// and no data was read. The number will be less than or equal to the count
        /// parameter, depending on whether there is more data or not. </returns>
        public override int ReadBlock(char[] buffer, int index, int count)
        {
            int num;

            int num2 = 0;

            do
            {
                num2 += num = this.Read(buffer, index + num2, count - num2);
            }
            while ((num > 0) && (num2 < count));

            return num2;
        }

        /// <summary> Reads a line of characters from the current stream and returns the data as
        /// a string.
        /// </summary>
        /// <returns>The next line from the input stream, or null if the end of the input stream
        /// is reached.
        /// </returns>
        public override string ReadLine()
        {
            StringBuilder builder = new StringBuilder();

            while (true)
            {
                int num = this.Read();

                if (num == EOF)
                {
                    if (builder.Length > 0)
                    {
                        return builder.ToString();
                    }

                    return null;
                }
                else if (num == 13 || num == 10)
                {
                    if ((num == 13) && (this.Peek() == 10))
                    {
                        this.Read();
                    }

                    return builder.ToString();
                }

                builder.Append((char)num);
            }
        }

        /// <summary> Reads the stream from the current position to the end of the stream.
        /// </summary>
        /// <returns> The rest of the stream as a string, from the current position to the end.
        /// If the current position is at the end of the stream, returns an empty string ("").
        /// </returns>
        public override string ReadToEnd()
        {
            int num;

            char[] buffer = new char[0x1000];

            StringBuilder builder = new StringBuilder(0x1000);

            while ((num = this.Read(buffer, 0, buffer.Length)) != 0)
            {
                builder.Append(buffer, 0, num);
            }

            return builder.ToString();
        }
    }
}