using System;
using System.Collections;
using System.Data;
using System.Threading;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.DataLock
{
	/// <summary>
	/// For now, we track the last saved version number of the referenced
	/// data object.
	/// </summary>

	public class LockRecord
	{
		/// <summary>
		/// For now, we track the last saved version number of the
		/// referenced data object.
		/// </summary>

		private Int32 m_Version = -1;
		private Guid       m_Id = Guid.Empty;

		public Int32 Version
		{
			// Access member.

			set
			{
				m_Version = value;
			}
			get
			{
				return m_Version;
			}
		}

		public Guid Id
		{
			// Access member.

			set
			{
				m_Id = value;
			}
			get
			{
				return m_Id;
			}
		}

	}

	/// <summary>
	/// Keep track of the recent data save records.  Each one, though
	/// transient in memory, must be kept as long as the data object can
	/// be accessed (i.e., the application's lifetime).  We will soon
	/// move to a persisted model that stores these records in the db,
	/// so that multiple applications can participate.
	/// </summary>

	public class LockController
	{
		/// <summary>
		/// Keep track of the recent data save records.  Each one,
		/// though transient in memory, must be kept as long as the data
		/// object can be accessed (i.e., the application's lifetime).
		/// We will soon move to a persisted model that stores these
		/// records in the db, so that multiple applications can
		/// participate.
		/// </summary>

		private Hashtable m_Table = new Hashtable();

		public LockRecord this[ Guid lockId ]
		{
			// Access member.

			get
			{
				return m_Table[ lockId ] as LockRecord;
			}
		}

		public Int32 Count
		{
			// Access member.

			get
			{
				return m_Table.Count;
			}
		}

		/// <summary>
		/// Add a new record to the table.  We will punt if a duplicate
		/// is being added.
		/// </summary>

		public void Add( Guid lockId , LockRecord lR )
		{
			// Insert a new entry into the hashtable.

			m_Table.Add( lockId , lR );
		}

		/// <summary>
		/// Clear out all existing entries from the table.
		/// </summary>

		public void Clear()
		{
			// Nix all records.

			m_Table.Clear();
		}
	}

	/// <summary>
	/// Provides simplified controller interaction, so that everybody
	/// uses the lock table the same way.
	/// </summary>

	public abstract class LockAdapter
	{
		/// <summary>
		/// Maintain a reference back to the master lock table for
		/// the particular data object domain.
		/// </summary>

		private LockController m_Controller = null;

		/// <summary>
		/// Keep track of the recorded version at the time of load.
		/// </summary>

		private Int32 m_CurrentVersion = -1;
		private Guid          m_LockId = Guid.Empty;

		public LockRecord Record
		{
			// Access member.

			get
			{
				LockRecord lR = new LockRecord();

				if( m_Controller == null )
				{
					throw new InvalidOperationException( "Adapter needs a valid controller." );
				}

				if( m_LockId != Guid.Empty )
				{
					lock( m_Controller )
					{
						lR = m_Controller[ m_LockId ];
					}
				}

				return lR;
			}
		}

		public String State
		{
			// Access member.

			set
			{
				String[] sP = value.Split( ':' );

				if( sP.Length != 2 )
				{
					throw new ArgumentException( "Lock adapter state consists of <id>:<version>." , "State" );
				}

				m_CurrentVersion = Int32.Parse( sP[ 1 ] );

				m_LockId = new Guid( sP[ 0 ] );
			}
			get
			{
				return m_LockId + ":" + m_CurrentVersion;
			}
		}

		/// <summary>
		/// Load the data object's version information from our controller.
		/// </summary>

		public LockAdapter Retrieve()
		{
			// Access the controller and load the current version.  We
			// lock down access to the table's record so we know we
			// are getting latest.

			if( m_Controller == null )
			{
				throw new InvalidOperationException( "Adapter needs a valid controller." );
			}

			if( m_LockId != Guid.Empty )
			{
				lock( m_Controller )
				{
					LockRecord lR = m_Controller[ m_LockId ];

					if( lR == null )
					{
						m_Controller.Add( m_LockId , lR = new LockRecord() );

						lR.Id      = m_LockId;
						lR.Version = 0;
					}

					m_CurrentVersion = lR.Version;
				}
			}

			return this;
		}

		/// <summary>
		/// Check the current version against our cached copy.  Punt if
		/// out of sync.  Perform this check inside a record lock to
		/// preserve integrity.
		/// </summary>

		public void Check()
		{
			// All we do here is check the version.  This check should
			// be done inside a lock on the associated record.

			if( m_Controller == null )
			{
				throw new InvalidOperationException( "Adapter needs a valid controller." );
			}

			if( m_LockId != Guid.Empty )
			{
				LockRecord lR;

				lock( m_Controller )
				{
					lR = m_Controller[ m_LockId ];
				}

				if( lR == null )
				{
					throw new InvalidOperationException( "Adapter can't find lock record for managed object." );
				}

				lock( lR )
				{
					if( m_CurrentVersion != lR.Version )
					{
						throw new VersionOutOfSync( "Data object's version has changed since last retrieve." );
					}
				}
			}
		}

		/// <summary>
		/// Increment the version counter for a data object that was
		/// successfully saved.  We need to do this inside a greater
		/// lock on the data object's record.
		/// </summary>

		public void Bump()
		{
			// Increment the version counter for a data object that
			// was successfully saved.  We need to do this inside a
			// greater lock on the data object's record.

			if( m_Controller == null )
			{
				throw new InvalidOperationException( "Adapter needs a valid controller." );
			}

			if( m_LockId != Guid.Empty )
			{
				LockRecord lR;

				lock( m_Controller )
				{
					lR = m_Controller[ m_LockId ];
				}

				if( lR == null )
				{
					throw new InvalidOperationException( "Adapter can't find lock record for managed object." );
				}

				lock( lR )
				{
					++lR.Version;
				}
			}
		}

		#region ( Constructor )

		/// <summary>
		/// Construct default.
		/// </summary>

		public LockAdapter( LockController lC , Guid lockId )
		{
			// Initialize members.

			m_Controller = lC;
			m_LockId     = lockId;
		}

		#endregion

	}

	/// <summary>
	/// Normalize exception wrapping so we quickly detect save
	/// collisions.
	/// </summary>

	public class LockException : CBaseException
	{
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public LockException( String message , Exception inner ) 
            : base(ErrorMessages.Generic, message + ".\r\n\r\n" + inner.ToString())
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public LockException( String message ) 
            : base(ErrorMessages.Generic, message)
		{
		}
	
		/// <summary>
		/// Construct default.
		/// </summary>

        public LockException()
            : base(ErrorMessages.Generic, ErrorMessages.Generic)
		{
		}

		#endregion

	}

	/// <summary>
	/// Normalize exception wrapping so we quickly detect save
	/// collisions.
	/// </summary>

	public class VersionOutOfSync : LockException
	{
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public VersionOutOfSync( String message , Exception inner ) : base( message , inner )
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public VersionOutOfSync( String message ) : base( message )
		{
		}
	
		/// <summary>
		/// Construct default.
		/// </summary>

		public VersionOutOfSync()
		{
		}

		#endregion

	}
    
}