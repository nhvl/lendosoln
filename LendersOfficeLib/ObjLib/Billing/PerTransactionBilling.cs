﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;

namespace LendersOffice.ObjLib.Billing
{
    public static class PerTransactionBilling
    {


        /// <summary>
        /// Returns a list of months in which at least one loan was billed for the given broker.
        /// </summary>
        /// <param name="BrokerId"></param>
        /// <returns></returns>
        public static Dictionary<DateTime, int> GetBilledMonths(Guid BrokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("BrokerId", BrokerId)
                                        };
            var dt = StoredProcedureHelper.ExecuteDataTable(BrokerId, "Billing_PTB_AllBilledMonths", parameters);
            var ret = new Dictionary<DateTime, int>(dt.Rows.Count);

            foreach (DataRow dr in dt.Rows)
            {
                ret.Add((DateTime)dr["BilledMonth"], (int)dr["LoanCount"]);
            }

            return ret;
        }

        /// <summary>
        /// Marks a loan as billed, if it is valid to bill the loan right now. 
        /// This version of the method will load up the loan in order to grab the BrokerId,
        /// so use the overload with the BrokerId if you already have it.
        /// </summary>
        /// <param name="LoanId"></param>
        /// <param name="type"></param>
        /// <param name="docVendor">Document Vendor. If closing package is ordered, then it will be doc vendor. Otherwise, it will be UnknownOtherNone</param>
        /// <returns>True if the loan was billed, false otherwise</returns>
        /// <exception cref="ArgumentException">If the broker this loan belongs to doesn't have per transaction billing enabled</exception>
        public static bool BillLoan(Guid LoanId, E_BillingReason type, E_DocumentVendor docVendor)
        {
            var loan = CPageData.CreateUsingSmartDependency(LoanId, typeof(PerTransactionBilling));
            loan.InitLoad();

            var BrokerId = loan.sBrokerId;

            return BillLoan(LoanId, BrokerId, type, DateTime.Now, docVendor);
        }

        /// <summary>
        /// Marks a loan as billed, if it is valid to bill the loan right now.
        /// </summary>
        /// <param name="LoanId"></param>
        /// <param name="BrokerId"></param>
        /// <param name="type"></param>
        /// <param name="docVendor">Document Vendor. If closing package is ordered, then it will be doc vendor. Otherwise, it will be UnknownOtherNone</param>
        /// <returns>True if the loan was billed, false otherwise</returns>
        public static bool BillLoan(Guid LoanId, Guid BrokerId, E_BillingReason type, E_DocumentVendor docVendor)
        {
            return BillLoan(LoanId, BrokerId, type, DateTime.Now, docVendor);
        }

        /// <summary>
        /// Marks a loan as billed, if it is valid to bill at the given time.
        /// 
        /// </summary>
        /// <param name="LoanId"></param>
        /// <param name="BrokerId"></param>
        /// <param name="type"></param>
        /// <param name="when">The time at which to bill the loan</param>
        /// <param name="docVendor">Document Vendor. If closing package is ordered, then it will be doc vendor. Otherwise, it will be UnknownOtherNone </param>
        /// <returns>True if the loan was billed, false otherwise</returns>
        public static bool BillLoan(Guid LoanId, Guid BrokerId, E_BillingReason type, DateTime when, E_DocumentVendor docVendor)
        {
            var broker = BrokerDB.RetrieveById(BrokerId);
            if (broker.BillingVersion != E_BrokerBillingVersion.PerTransaction)
            {
                throw new ArgumentException("Broker does not have Per Transaction Billing enabled");
            }

            when = new DateTime(when.Year, when.Month, when.Day);
            var billable = false;
            try
            {
                var parameters = new List<SqlParameter> {
                                                new SqlParameter("sLId", LoanId),
                                                new SqlParameter("OnDate", when),
                                                new SqlParameter("BillingType", type)
                                            };

                var billableAndDocVendor = StoredProcedureHelper.ExecuteDataTable(BrokerId, "Billing_PTB_IsLoanValidForBilling", parameters);

                parameters =  new List<SqlParameter> {
                        new SqlParameter("sLId", LoanId),
                        new SqlParameter("BrokerId", BrokerId),
                        new SqlParameter("BillingType", type),
                        new SqlParameter("DocumentVendor", docVendor)
                    };

                if (billableAndDocVendor.Rows.Count > 0)
                {
                    var firstRow = billableAndDocVendor.Rows[0];
                    E_DocumentVendor originalDocVendor = (E_DocumentVendor)firstRow["DocVendor"];
                    DateTime billedDate = (DateTime)firstRow["BillingDate"];
                    billedDate = new DateTime(billedDate.Year, billedDate.Month, billedDate.Day);

                    if (docVendor != originalDocVendor && originalDocVendor == 0)
                    {
                        parameters.Add(new SqlParameter("BillingDate", billedDate));
                        StoredProcedureHelper.ExecuteNonQuery(BrokerId, "Billing_PTB_UpdateDocVendor", 3, parameters);
                    }
                }
                else
                {
                    parameters.Add(new SqlParameter("BillingDate", when));
                    StoredProcedureHelper.ExecuteNonQuery(BrokerId, "Billing_PTB_BillLoan", 3, parameters);
                    billable = true;
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking("There was an error marking loan guid {" + LoanId + "} as billed.", e);
                throw;
            }

            return billable;
        }

        public class BillingEntry
        {
            public Guid LoanId { get; private set; }
            public string LoanNumber { get; private set; }
            public string Borrower { get; private set; }
            public string LoanOfficer { get; private set; }
            public string BranchName { get; private set; }
            public string BranchCode { get; private set; }
            public E_BillingReason BillingType { get; private set; }
            public DateTime BilledDate { get; private set; }

            public BillingEntry(Guid slid, string slnm, string borrnm, string lonm, string branchnm, E_BillingReason bt, DateTime billed)
            {
                LoanId = slid;
                LoanNumber = slnm;
                Borrower = borrnm;
                LoanOfficer = lonm;
                BranchName = branchnm;
                BillingType = bt;
                BilledDate = billed;
            }

            public BillingEntry(DataRow dr)
            {
                LoanId = (Guid)dr["LoanId"];
                LoanNumber = dr["LoanNumber"].ToString();
                Borrower = dr["BorrowerName"].ToString();
                LoanOfficer = dr["LoanOfficer"].ToString();
                BranchName = dr["BranchName"].ToString();
                BranchCode = dr["BranchCode"].ToString();
                BillingType = (E_BillingReason)dr["BillingType"];
                BilledDate = (DateTime)dr["BillingDate"];
            }
        }

        /// <summary>
        /// Creates a per transaction billing object containing entries for every loan that the given broker was billed for in the 
        /// given month.
        /// </summary>
        /// <param name="BrokerId">The broker to look up</param>
        /// <param name="month">The month to look up - only the month and year parts of the DateTime are considered, day and time are ignored</param>
        /// <returns></returns>
        public static List<BillingEntry> GetBrokerBilledLoans(Guid BrokerId, DateTime month)
        {
            var startMonth = new DateTime(month.Year, month.Month, 1);
            var endMonth = startMonth.AddMonths(1);

            if (startMonth > DateTime.Now) throw new ArgumentException("Month is after today!", "month");
            var temp = new List<BillingEntry>();

            SqlParameter[] parameters = {
                                            new SqlParameter("StartDate", startMonth),
                                            new SqlParameter("EndDate", endMonth),
                                            new SqlParameter("BrokerId", BrokerId)
                                        };

            DataTable dt = StoredProcedureHelper.ExecuteDataTable(BrokerId, "Billing_PTB_GetBilledLoans", parameters);


            foreach (DataRow dr in dt.Rows)
            {
                temp.Add(new BillingEntry(dr));
            }

            return temp;
        }
    }
}
