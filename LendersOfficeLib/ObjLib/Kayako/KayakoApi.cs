﻿namespace LendersOffice.ObjLib.Kayako
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Constants;
    using DataAccess;
    
    /// <summary>
    /// Implements a rest Api to Kayako. Currently it only supports user search which is the only thing that is needed.
    /// </summary>
    public class KayakoApi
    {
        /// <summary>
        /// The api end point url to the kayako site.
        /// </summary>
        private string url;

        /// <summary>
        /// The salt which the signature is signed with.
        /// </summary>
        private string salt;

        /// <summary>
        /// The signature to verify the request.
        /// </summary>
        private string signature;

        /// <summary>
        /// The api key needed to work with the kayako api.
        /// </summary>
        private string apiKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="KayakoApi" /> class.
        /// </summary>
        /// <param name="url">The api endpoint.</param>
        /// <param name="apiKey">The api key to authenticate with the api.</param>
        /// <param name="secretKey">The secret key to sign api request with.</param>
        public KayakoApi(string url, string apiKey, string secretKey)
        {
            this.apiKey = apiKey;
            this.url = url;
            this.GenerateSaltAndSignature(secretKey);
        }

        /// <summary>
        /// Helper method to easily call the kayako api.
        /// </summary>
        /// <param name="email">The email to search for.</param>
        /// <returns>A value indicating whether we could find the user.</returns>
        public static UserCheckResult CheckUserExistance(string email)
        {
            if (ConstStage.SupportSiteIsEmailVerificationEnabled)
            {
                KayakoApi api = new KayakoApi(ConstStage.SupportSiteApiUrl, ConstStage.SupportSiteApiKey, ConstStage.SupportSiteApiSecretKey);
                return api.DoesUserExist(email);
            }

            return UserCheckResult.DoesNotExist;
        }

        /// <summary>
        /// Gets a value indicating whether the user exists or not.
        /// </summary>
        /// <param name="email">The email to search for.</param>
        /// <returns>A value indicating whether the user exist, doesn't not exist or could not hit the api.</returns>
        public UserCheckResult DoesUserExist(string email)
        {
            email = email.Trim();
            string url = $"{this.url}?Base/UserSearch";
            StringBuilder sb = new StringBuilder($"query={email}");
            var result = this.GetUrl(url, sb, "POST");
            if (string.IsNullOrEmpty(result))
            {
                return UserCheckResult.Failure;
            }

            try
            {
                XDocument doc = XDocument.Parse(result);
                XElement root = doc.Root;
                foreach (var element in root.Elements("user"))
                {
                    if (element.Element("email").Value.Equals(email, StringComparison.OrdinalIgnoreCase))
                    {
                        return UserCheckResult.Exist;
                    }
                }

                return UserCheckResult.DoesNotExist;
            }
            catch (XmlException)
            {
                Tools.LogError("Could not decipher response: " + result); 
                return UserCheckResult.Failure;
            }
        }

        /// <summary>
        /// Generates needed parameters for the rest api.
        /// </summary>
        /// <param name="secretKey">The secret key use to sign the request.</param>
        private void GenerateSaltAndSignature(string secretKey)
        {
            this.salt = System.Guid.NewGuid().ToString();
            HMACSHA256 hashObject = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey));
            var signatureBytes = hashObject.ComputeHash(Encoding.UTF8.GetBytes(this.salt));
            this.signature = Convert.ToBase64String(signatureBytes);
        }
        
        /// <summary>
        /// Performs the API hit to kayako and returns the data.
        /// </summary>
        /// <param name="requestUrl">The url to hit.</param>
        /// <param name="parameters">The parameters to send.</param>
        /// <param name="method">Get or Post.</param>
        /// <returns>The response or null if it could not hit.</returns>
        private string GetUrl(string requestUrl, StringBuilder parameters, string method)
        {
            WebRequest webRequest = WebRequest.Create(requestUrl);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = method;

            parameters.Append($"&salt={this.salt}");
            parameters.Append($"&signature={this.signature}");
            parameters.Append($"&apikey={this.apiKey}");

            byte[] bytes = Encoding.ASCII.GetBytes(parameters.ToString());
            webRequest.ContentLength = bytes.Length;
            try
            {
                using (var os = webRequest.GetRequestStream())
                {
                    os.Write(bytes, 0, bytes.Length);
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                    string s = sr.ReadToEnd();
                    return s;
                }
            }
            catch (WebException e)
            {
                Tools.LogError(e);
                return null;
            }
        }
    }
}
