﻿// <copyright file="AuthManager.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/17/2015 4:19:57 PM
// </summary>
namespace LendersOffice.ObjLib.Kayako
{
    using System;
    using System.Net.Mail;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using Newtonsoft.Json;

    /// <summary>
    /// Responsible for creating and consuming authentication 
    /// request from LQB users.
    /// </summary>
    public class AuthManager
    {
        /// <summary>
        /// A prefix to append to the authentication id. This prefix ensures the 
        /// authentication manager only loads items that were made by it.
        /// </summary>
        private const string KeyPrefix = "KAYAKO_";

        /// <summary>
        /// The amount of minutes the ticket will be valid for after it is created.
        /// </summary>
        private TimeSpan expirationTimeSpan = TimeSpan.FromMinutes(10);

        /// <summary>
        /// Gets a authentication url that represents an authentication ticket for the given principal.
        /// </summary>
        /// <param name="principal">The principal of the user.</param>
        /// <returns>An authentication key that can be used to retrieve the authentication ticket.</returns>
        public string GetAuthUrlFor(AbstractUserPrincipal principal)
        {
            if (principal.Type != "B")
            {
                throw CBaseException.GenericException("Can only use B principals.");
            }

            AuthTicket ticket = this.GetAuthTicketFor(principal.BrokerDB, principal.EmployeeId);
            string partialKey = this.GetSingleUsePartialKey(); 
            string serializedTicket = this.SerializeAuthTicket(ticket);
            AutoExpiredTextCache.AddToCache(serializedTicket, this.expirationTimeSpan, this.GetCacheKey(partialKey));
            return string.Format(ConstStage.SupportSystemAuthUrlFormat, partialKey);
        }

        /// <summary>
        /// Gets the authentication ticket from a given key. The authentication ticket will be deleted after this.
        /// </summary>
        /// <param name="key">The authentication tickets key.</param>
        /// <returns>The authentication ticket.</returns>
        public AuthTicket RetrieveAuthTicketFrom(string key)
        {
            string fullKey = this.GetCacheKey(key);
            string data = AutoExpiredTextCache.GetFromCache(fullKey);

            if (string.IsNullOrEmpty(data))
            {
                Tools.LogInfo("Kayako key not found "  + key + " full key " + fullKey);
                return null;
            }

            Tools.LogInfo("Kayako key found " + key + " full key " + fullKey);
            return this.ReadAuthTicket(data);
        }

        /// <summary>
        /// Gets the Kayako required XML for the sign on process.
        /// </summary>
        /// <param name="ticket">The ticket we are serializing.</param>
        /// <returns>The Kayako login share representation.</returns>
        public string GetKayakoXMLForAuthTicket(AuthTicket ticket)
        {
            XDocument doc = new XDocument(new XElement("loginshare"));
            XElement root = doc.Root;

            if (ticket == null)
            {
                root.Add(new XElement("result", "0"));
                root.Add(new XElement("message", "Invalid ticket."));
            }
            else
            {
                root.Add(new XElement("result", "1"));

                root.Add(new XElement(
                    "user",
                    new XElement("usergroup", ticket.Usergroup),
                    new XElement("fullname", ticket.FullName),
                    new XElement("designation", string.Empty),
                    new XElement("organization", ticket.Organization),
                    new XElement("organizationtype", "restricted"),
                    new XElement("customercode", ticket.CustomerCode),
                    new XElement("phone", ticket.PhoneNumber),
                    new XElement("role", ticket.Role),
                    new XElement(
                        "emails",
                        new XElement("email", ticket.EmailAddress))));
            }

            return doc.ToString();
        }

        /// <summary>
        /// Gets a partial unique key for caching the authentication ticket.
        /// </summary>
        /// <returns>A unique key that will represent the authentication ticket.</returns>
        private string GetSingleUsePartialKey()
        {
            return Tools.ShortenGuid(Guid.NewGuid());
        }

        /// <summary>
        /// Gets a unique single use authentication key. 
        /// </summary>
        /// <param name="partialSingleUseKey">The key representing the ticket.</param>
        /// <returns>A unique single use authentication key.</returns>
        private string GetCacheKey(string partialSingleUseKey)
        {
            return string.Concat(AuthManager.KeyPrefix, partialSingleUseKey);
        }

        /// <summary>
        /// Gets an exception for invalid emails.
        /// </summary>
        /// <returns>An exception notifying users there is something wrong with their email address.</returns>
        private CBaseException GenerateInvalidEmailException()
        {
            return new CBaseException("A valid email address is required before using the support system.", "Invalid email address");
        }

        /// <summary>
        /// Gets a authentication ticket for the given employee id.
        /// </summary>
        /// <param name="broker">The broker for the given employee id.</param>
        /// <param name="employeeId">The employee id.</param>
        /// <returns>A authentication ticket representing the user.</returns>
        private AuthTicket GetAuthTicketFor(BrokerDB broker, Guid employeeId)
        {
            EmployeeDB db = EmployeeDB.RetrieveById(broker.BrokerID, employeeId);

            if (string.IsNullOrEmpty(db.SupportEmail))
            {
                throw this.GenerateInvalidEmailException();
            }

            // I won't bother unit testing here since other unit tests cover
            // the basics of email validation and only oddball emails are 
            // likely to get treated differently here...which we'll discover
            // via live production activity.
            string validatedEmailAddress = null;
            if (ConstStage.UseNewEmailer)
            {
                // Use regular expression in the LqbGrammar library to validate
                LqbGrammar.DataTypes.EmailAddress? emailAddress = LqbGrammar.DataTypes.EmailAddress.Create(db.SupportEmail);
                if (emailAddress != null)
                {
                    validatedEmailAddress = emailAddress.ToString();
                }
            }

            if (validatedEmailAddress == null)
            {
                try
                {
                    // Use the native .net framework email validation
                    MailAddress emailAddress = new MailAddress(db.SupportEmail);
                    validatedEmailAddress = emailAddress.Address;
                }
                catch (FormatException)
                {
                    throw this.GenerateInvalidEmailException();
                }
            }
            
            string userRole = "user";

            EmployeeRoles eRs = new EmployeeRoles(broker.BrokerID, employeeId);

            if (eRs.IsInRole(E_RoleT.Administrator))
            {
                userRole = "manager";
            }

            string usergroup = "Registered";

            if (broker.SuiteType == BrokerSuiteType.VendorTest)
            {
                usergroup = "Vendor";
            }
            else if (db.CanContactSupport || broker.IsAllowAllUsersToUseKayako)
            {
                usergroup = "AllowSupport";
            }

            AuthTicket ticket = new AuthTicket()
            {
                FullName = db.FullName,
                EmailAddress = validatedEmailAddress,
                Organization = broker.Name,
                CustomerCode = broker.CustomerCode,
                Role = userRole,
                Usergroup = usergroup,
                Referrer = ConstStage.NonContextBaseUrl
            };

            return ticket;
        }

        /// <summary>
        /// Gets a string containing the authentication ticket.
        /// </summary>
        /// <param name="ticket">The authentication ticket that will be serialized.</param>
        /// <returns>The serialized authentication ticket.</returns>
        private string SerializeAuthTicket(AuthTicket ticket)
        {
            return JsonConvert.SerializeObject(ticket);
        }

        /// <summary>
        /// Gets the authentication ticket represented by the given string.
        /// </summary>
        /// <param name="serializedTicket">The string representation of the authentication ticket.</param>
        /// <returns>The authentication ticket represented by the serialized ticket.</returns>
        private AuthTicket ReadAuthTicket(string serializedTicket)
        {
            return JsonConvert.DeserializeObject<AuthTicket>(serializedTicket);
        }
    }
}