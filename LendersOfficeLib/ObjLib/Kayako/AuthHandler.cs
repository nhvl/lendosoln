﻿// <copyright file="AuthHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/17/2015 4:11:53 PM
// </summary>
namespace LendersOffice.ObjLib.Kayako
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

    /// <summary>
    /// A HTTP Handler responsible for returning user information belonging to either a 
    /// secret key or a LendingQB authentication cookie.
    /// </summary>
    public class AuthHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reusable. This is always true
        /// as the handler does not have any state per request.
        /// </summary>
        /// <value>Returns true since the handler has no unique state per request.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given incoming request.
        /// </summary>
        /// <param name="context">The HTTP context to process.</param>
        public void ProcessRequest(HttpContext context)
        {
            string key = context.Request["authkey"];

            // string cookie = context.Request["cookie"]; 
            AuthManager manager = new AuthManager();
            AuthTicket ticket = manager.RetrieveAuthTicketFrom(key);
            string xml = manager.GetKayakoXMLForAuthTicket(ticket);
            context.Response.ContentType = "application/xml";
            context.Response.Write(xml);
        }
    }
}
