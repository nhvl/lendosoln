﻿namespace LendersOffice.ObjLib.Kayako
{
    /// <summary>
    /// Set of returns code for kayako's user search.
    /// </summary>
    public enum UserCheckResult
    {
        /// <summary>
        /// The user was found.
        /// </summary>
        Exist = 0,
        
        /// <summary>
        /// The user was not found.
        /// </summary>
        DoesNotExist = 1,

        /// <summary>
        /// There was an error checking if the user exist.
        /// </summary>
        Failure = 2
    }
}
