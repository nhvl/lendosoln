﻿// <copyright file="AuthTicket.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/17/2015 4:20:13 PM
// </summary>
namespace LendersOffice.ObjLib.Kayako
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;

    /// <summary>
    /// A plain old object containing the needed information
    /// to authenticate a user on the Kayako support system.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class AuthTicket
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthTicket" /> class.
        /// </summary>
        public AuthTicket()
        {
            this.CreationDate = DateTime.Now;

            // By default, all users belong to the "Registered" usergroup. Only 
            // those that have been granted access to contact LQB support will 
            // be a part of the usergroup "AllowSupport".
            this.Usergroup = "Registered";
        }

        /// <summary>
        /// Gets or sets the name of the user this authentication ticket represents.
        /// </summary>
        /// <value>The users full name.</value>
        public string FullName { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the organization the user is in.
        /// </summary>
        /// <value>The users organization.</value>
        public string Organization { get; set; }

        /// <summary>
        /// Gets or sets the email address of the user the authentication ticket represents.
        /// </summary>
        /// <value>The users email address.</value>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the user this authentication ticket represents.
        /// </summary>
        /// <value>The users phone number.</value>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets the date the ticket was created.
        /// </summary>
        /// <value>The tickets creation date.</value>
        public DateTime CreationDate { get; private set; }

        /// <summary>
        /// Gets or sets the Customer Code.
        /// </summary>
        /// <value>The customer code.</value>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Gets or sets the role the user is. 
        /// </summary>
        /// <value>User if normal user otherwise manager.</value>
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the user group the user is in. 
        /// </summary>
        /// <value>Registered if normal, AllowSupport if the user can contact LQB support.</value>
        public string Usergroup { get; set; }

        /// <summary>
        /// Gets or sets the URL for the LOS system.
        /// </summary>
        /// <value>The top level URL to the los system.</value>
        public string Referrer { get; set; }
    }
}
