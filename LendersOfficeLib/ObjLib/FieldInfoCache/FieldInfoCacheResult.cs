﻿namespace LendersOffice.ObjLib.FieldInfoCache
{
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Provides a simple wrapper for field info cache data.
    /// </summary>
    public class FieldInfoCacheResult
    {
        /// <summary>
        /// Gets or sets the caching set obtained from the cache.
        /// </summary>
        /// <value>
        /// The caching set.
        /// </value>
        public UpdateResult CachingSet { get; set; }

        /// <summary>
        /// Gets or sets the main lookup obtained from the cache.
        /// </summary>
        /// <value>
        /// The main lookup.
        /// </value>
        public IReadOnlyDictionary<string, FieldProperties> MainLookup { get; set; }

        /// <summary>
        /// Gets or sets the dependency lookup for non-obsolete fields obtained from the cache.
        /// </summary>
        /// <value>
        /// The dependency lookup.
        /// </value>
        public IReadOnlyDictionary<FieldSpec, StringList> DependencyLookupForNonObsoleteFields { get; set; }
    }
}
