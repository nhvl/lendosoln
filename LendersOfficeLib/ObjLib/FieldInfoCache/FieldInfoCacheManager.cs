﻿namespace LendersOffice.ObjLib.FieldInfoCache
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Resources;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using Models;

    /// <summary>
    /// Provides a means for managing the <seealso cref="CFieldInfoTable"/> cache.
    /// </summary>
    /// <remarks>
    /// The getters for the properties of this class are made private so that external
    /// code cannot alter the objects that the properties reference, allowing this class
    /// to maintain a readonly representation of the relevant field info table data.
    /// </remarks>
    public class FieldInfoCacheManager
    {
        /// <summary>
        /// Represents the name of the resource file containing the cache data.
        /// </summary>
        private const string FieldInfoCacheBaseName = "FieldInfoTableResources";

        /// <summary>
        /// Represents the key used to save and load the caching set.
        /// </summary>
        private const string CachingSetKey = "CachingSet";

        /// <summary>
        /// Represents the key used to save and load the main <seealso cref="FieldProperties"/> lookup.
        /// </summary>
        private const string MainLookupKey = "MainLookup";

        /// <summary>
        /// Represents the key used to save and load the dictionary mapping non-obsolete fields to their 
        /// dependent fields.
        /// </summary>
        private const string NonObsoletePropertyLookupKey = "NonObsoletePropertyLookup";

        /// <summary>
        /// Gets or sets the caching set that will be added to the field info cache.
        /// </summary>
        /// <value>
        /// The <seealso cref="UpdateResult"/> for the caching set.
        /// </value>
        internal UpdateResult CachingSet { private get; set; }

        /// <summary>
        /// Gets or sets the main property lookup that will be added to the field info cache.
        /// </summary>
        /// <value>
        /// The <seealso cref="IReadOnlyDictionary{TKey, TValue}"/> for the main lookup.
        /// </value>
        internal IReadOnlyDictionary<string, FieldProperties> MainLookup { private get; set; }

        /// <summary>
        /// Gets or sets the non-obsolete field dependency lookup that will be added to the field info cache.
        /// </summary>
        /// <value>
        /// The <seealso cref="IReadOnlyDictionary{TKey, TValue}"/> for the non-obsolete field lookup.
        /// </value>
        internal IReadOnlyDictionary<FieldSpec, StringList> DependencyLookupForNonObsoleteFields { private get; set; }

        /// <summary>
        /// Creates the caching resource, storing the file at the location 
        /// specified with <see cref="ConstSite.FieldInfoCacheFilePath"/>.
        /// </summary>
        public void CreateCacheResource()
        {
            if (string.IsNullOrWhiteSpace(ConstSite.FieldInfoCacheFilePath))
            {
                throw new InvalidOperationException("Field info cache file path is not defined in ConstSite.");
            }

            var fieldInfoTable = CFieldInfoTable.GetNoCacheInstance();
            fieldInfoTable.Accept(this);

            var cachingSetModel = new ResultModel(this.CachingSet);
            var serializedCachingSet = SerializationHelper.JsonNetSerialize(cachingSetModel);

            var mainLookupModel = this.MainLookup.Values.Select(fieldProperty => new FieldPropertiesModel(fieldProperty));
            var serializedMainLookup = SerializationHelper.JsonNetAnonymousSerialize(mainLookupModel);

            var nonObsoleteLookupModel = this.DependencyLookupForNonObsoleteFields.Select(kvp => 
                new KeyValuePair<FieldSpecModel, StringListModel>(new FieldSpecModel(kvp.Key), new StringListModel(kvp.Value)));

            var serializedNonObsoletePropertyLookup = SerializationHelper.JsonNetAnonymousSerialize(nonObsoleteLookupModel);

            var cacheFilePath = Path.Combine(ConstSite.FieldInfoCacheFilePath, FieldInfoCacheBaseName + ".resources");
            using (var writer = new ResourceWriter(cacheFilePath))
            {
                writer.AddResource(CachingSetKey, serializedCachingSet);
                writer.AddResource(MainLookupKey, serializedMainLookup);
                writer.AddResource(NonObsoletePropertyLookupKey, serializedNonObsoletePropertyLookup);
            }
        }

        /// <summary>
        /// Obtains the properties present in the cached resource file.
        /// </summary>
        /// <returns>
        /// A <see cref="FieldInfoCacheResult"/> with the cached properties.
        /// </returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "This method returns null if an error occurs while retrieving data from the field info cache and has a dedicated fallback.")]
        public FieldInfoCacheResult GetCachedProperties()
        {
            try
            {
                var manager = ResourceManager.CreateFileBasedResourceManager(FieldInfoCacheBaseName, ConstSite.FieldInfoCacheFilePath, usingResourceSet: null);

                var deserializedCachingSetModel = SerializationHelper.JsonNetDeserialize<ResultModel>(manager.GetString(CachingSetKey));
                var cachingSet = new UpdateResult(deserializedCachingSetModel);

                var deserializedMainLookupModel = SerializationHelper.JsonNetDeserialize<List<FieldPropertiesModel>>(manager.GetString(MainLookupKey));
                var mainLookup = deserializedMainLookupModel.ToDictionary(model => model.FieldName, model => new FieldProperties(model), StringComparer.OrdinalIgnoreCase);

                var deserializedNonObsoleteLookupModel = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<FieldSpecModel, StringListModel>>>(
                    manager.GetString(NonObsoletePropertyLookupKey));

                var nonObsoletePropertyLookup = deserializedNonObsoleteLookupModel.ToDictionary(kvp => FieldSpec.Create(kvp.Key.Name), kvp => new StringList(kvp.Value));

                return new FieldInfoCacheResult
                {
                    CachingSet = cachingSet,
                    MainLookup = mainLookup,
                    DependencyLookupForNonObsoleteFields = nonObsoletePropertyLookup
                };
            }
            catch (Exception exc)
            {
                Tools.LogError("Exception encountered while retrieving field info cache file, falling back to reflection method.", exc);
                return null;
            }
        }
    }
}
