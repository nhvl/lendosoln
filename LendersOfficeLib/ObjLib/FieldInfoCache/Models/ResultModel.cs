﻿namespace LendersOffice.ObjLib.FieldInfoCache.Models
{
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides a serializable model for the <see cref="UpdateResult"/> 
    /// and <seealso cref="DependResult"/> classes.
    /// </summary>
    [DataContract]
    public class ResultModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultModel"/> class.
        /// This constructor is included for serialization purposes only.
        /// </summary>
        public ResultModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultModel"/> class
        /// using data from the specified <paramref name="result"/>.
        /// </summary>
        /// <param name="result">
        /// The <seealso cref="UpdateResult"/> used to populate the model.
        /// </param>
        public ResultModel(UpdateResult result)
        {
            this.DbFields = new StringListModel(result.DbFields);
            this.UpdateFields = new StringListModel(result.UpdateFields);
        }

        /// <summary>
        /// Gets or sets the database fields for the result.
        /// </summary>
        /// <value>
        /// The database fields for the result.
        /// </value>
        [DataMember(Name = "df")]
        public StringListModel DbFields { get; set; }

        /// <summary>
        /// Gets or sets the update fields for the result.
        /// </summary>
        /// <value>
        /// The update fields for the result.
        /// </value>
        [DataMember(Name = "uf")]
        public StringListModel UpdateFields { get; set; }
    }
}
