﻿namespace LendersOffice.ObjLib.FieldInfoCache.Models
{
    using System;
    using System.ComponentModel;
    using System.Globalization;

    /// <summary>
    /// Provides a means for converting a serialized <seealso cref="FieldSpecModel"/>
    /// into an instance of that class.
    /// </summary>
    internal class FieldSpecModelTypeConverter : TypeConverter
    {
        /// <summary>
        /// Determines whether the specified <paramref name="sourceType"/>
        /// can be converted into a <seealso cref="FieldSpecModel"/>.
        /// </summary>
        /// <param name="context">
        /// The context for the converter.
        /// </param>
        /// <param name="sourceType">
        /// The source type for the conversion.
        /// </param>
        /// <returns>
        /// True if the conversion can be done, false otherwise.
        /// </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> to a <seealso cref="FieldSpecModel"/>.
        /// </summary>
        /// <param name="context">
        /// The parameter is not used.
        /// </param>
        /// <param name="culture">
        /// The parameter is not used.
        /// </param>
        /// <param name="value">
        /// The string value for the <seealso cref="FieldSpecModel"/>.
        /// </param>
        /// <returns>
        /// A new <seealso cref="FieldSpecModel"/> instance.
        /// </returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new FieldSpecModel(value.ToString());
        }
    }
}
