﻿namespace LendersOffice.ObjLib.FieldInfoCache.Models
{
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides a serializable model for the <see cref="FieldProperties"/> class.
    /// </summary>
    [DataContract]
    public class FieldPropertiesModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldPropertiesModel"/> class.
        /// This constructor is included for serialization purposes only.
        /// </summary>
        public FieldPropertiesModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldPropertiesModel"/> class
        /// using data from the specified <paramref name="properties"/>.
        /// </summary>
        /// <param name="properties">
        /// The <seealso cref="FieldProperties"/> used to populate the model.
        /// </param>
        public FieldPropertiesModel(FieldProperties properties)
        {
            this.FieldName = properties.FieldName;
            this.FieldSpec = new FieldSpecModel(properties.FieldSpec);
            this.DependencyFields = new StringListModel(properties.DependencyFields);
            this.AffectedCacheFields = new StringListModel(properties.AffectedCacheFields);
            this.AffectedFields = new StringListModel(properties.AffectedFields);
            this.IsObsolete = properties.IsObsolete;
        }

        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>
        /// The name of the field.
        /// </value>
        [DataMember(Name = "fpn")]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the <seealso cref="FieldSpecModel"/> for the field.
        /// </summary>
        /// <value>
        /// The field spec for the field.
        /// </value>
        [DataMember(Name = "fs")]
        public FieldSpecModel FieldSpec { get; set; }

        /// <summary>
        /// Gets or sets the dependency fields for the model.
        /// </summary>
        /// <value>
        /// The dependency fields for the field.
        /// </value>
        [DataMember(Name = "df")]
        public StringListModel DependencyFields { get; set; }

        /// <summary>
        /// Gets or sets the affected cache fields for the model.
        /// </summary>
        /// <value>
        /// The affected cache fields for the field.
        /// </value>
        [DataMember(Name = "acf")]
        public StringListModel AffectedCacheFields { get; set; }

        /// <summary>
        /// Gets or sets the affected fields for the model.
        /// </summary>
        /// <value>
        /// The affected fields for the field.
        /// </value>
        [DataMember(Name = "af")]
        public StringListModel AffectedFields { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field is obsolete.
        /// </summary>
        /// <value>
        /// True if the field is obsolete, false otherwise.
        /// </value>
        [DataMember(Name = "o")]
        public bool IsObsolete { get; set; }
    }
}
