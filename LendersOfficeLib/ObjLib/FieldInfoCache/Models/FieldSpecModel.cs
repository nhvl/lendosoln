﻿namespace LendersOffice.ObjLib.FieldInfoCache.Models
{
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides a serializable model for the <see cref="FieldSpec"/> class.
    /// </summary>
    [DataContract]
    [TypeConverter(typeof(FieldSpecModelTypeConverter))]
    public class FieldSpecModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldSpecModel"/> class.
        /// This constructor is included for serialization purposes only.
        /// </summary>
        public FieldSpecModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldSpecModel"/> class
        /// using data from the specified <paramref name="fieldSpec"/>.
        /// </summary>
        /// <param name="fieldSpec">
        /// The <seealso cref="FieldSpec"/> used to populate the model.
        /// </param>
        public FieldSpecModel(FieldSpec fieldSpec)
        {
            this.Name = fieldSpec.Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldSpecModel"/> class
        /// using the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">
        /// The name for the field.
        /// </param>
        /// <remarks>
        /// This constructor is intended to be used only within 
        /// the <seealso cref="FieldSpecModelTypeConverter"/>.
        /// </remarks>
        internal FieldSpecModel(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets the name for the field.
        /// </summary>
        /// <value>
        /// The name for the field.
        /// </value>
        [DataMember(Name = "fsn")]
        public string Name { get; set; }

        /// <summary>
        /// Converts this instance to its string representation.
        /// </summary>
        /// <returns>
        /// The string representation of this instance.
        /// </returns>
        /// <remarks>
        /// Overriding this call is required for the serialization methods
        /// to correctly serialize this class to and from a dictionary key for
        /// <see cref="FieldInfoCacheManager.DependencyLookupForNonObsoleteFields"/>.
        /// </remarks>
        public override string ToString() => this.Name;
    }
}
