﻿namespace LendersOffice.ObjLib.FieldInfoCache.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides a serializable model for the <see cref="StringList"/> class.
    /// </summary>
    [DataContract]
    public class StringListModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StringListModel"/> class.
        /// This constructor is included for serialization purposes only.
        /// </summary>
        public StringListModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringListModel"/> class
        /// using data from the specified <paramref name="stringList"/>.
        /// </summary>
        /// <param name="stringList">
        /// The <seealso cref="StringList"/> used to populate the model.
        /// </param>
        public StringListModel(StringList stringList)
        {
            this.FieldSpecList = stringList.GetFieldSpecList().Select(fieldSpec => new FieldSpecModel(fieldSpec));
        }

        /// <summary>
        /// Gets or sets the list of field specs for the list.
        /// </summary>
        /// <value>
        /// The list of field specs.
        /// </value>
        [DataMember(Name = "fsl")]
        public IEnumerable<FieldSpecModel> FieldSpecList { get; set; }
    }
}
