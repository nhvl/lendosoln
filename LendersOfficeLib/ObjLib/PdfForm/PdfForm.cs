﻿namespace LendersOffice.PdfForm
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Threading;
    using DataAccess;
    using Drivers.SqlServerDB;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Pdf;
    using LendersOffice.PdfGenerator;
    using LendersOffice.PdfLayout;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using PdfRasterizerLib;

    public class PdfForm
    {
        private static string GetPngFileName(Guid formId, int page, Guid sessionId)
        {
            return string.Format("{0}\\{1}_{2}_{3}.png", ConstApp.TempFolder, formId, sessionId, page);
        }

        private static List<PdfForm> s_standardFormList => standardFormPdfListInitializer.Value.Item1;
        private static List<ConstructorInfo> s_verificationPdfList => standardFormPdfListInitializer.Value.Item2;

        private static LqbSingleThreadInitializeLazy<Tuple<List<PdfForm>,List<ConstructorInfo>>> standardFormPdfListInitializer = null;


        static PdfForm()
        {
            
            Func<Tuple<List<PdfForm>, List<ConstructorInfo>>> initializer = () =>
            {
                Tuple<List<PdfForm>, List<ConstructorInfo>> forms = new Tuple<List<PdfForm>, List<ConstructorInfo>>(new List<PdfForm>(), new List<ConstructorInfo>());
                foreach (var pdfItem in PdfPrintList.RetrieveOnlyPrintItem())
                {
                    if (pdfItem.IsExcludeEsign)
                    {
                        // 6/8/2010 dd - OPM 52220 - We exclude certain obsolete form from standard printlist in esign.
                        continue;
                    }
                    ConstructorInfo constructor = PDFClassHashTable.GetConstructor(pdfItem.Type);
                    if (null != constructor)
                    {
                        try
                        {
                            if (constructor.ReflectedType == typeof(CustomFormList) ||
                                constructor.ReflectedType == typeof(CustomPDFFormList))
                            {
                                // 5/26/2010 dd - Skip when encounter custom Word form and custom pdf list.
                                continue;
                            }
                            if (constructor.ReflectedType.IsSubclassOf(typeof(AbstractVerificationListPDF)))
                            {
                                // 5/26/2010 dd - No support for VOD, VOE yet.
                                forms.Item2.Add(constructor);
                                continue;
                            }

                            IPDFPrintItem obj = (IPDFPrintItem)constructor.Invoke(new object[0]);


                            PdfForm pdfForm = new PdfForm();
                            pdfForm.m_standardFormType = pdfItem.Type;

                            pdfForm.FormId = constructor.ReflectedType.GUID;
                            pdfForm.BrokerId = Guid.Empty;
                            pdfForm.IsStandardForm = true;
                            pdfForm.Description = pdfItem.Description;
                            pdfForm.PreviewLink = obj.Name + ".aspx";

                            if (obj is LendersOffice.PdfGenerator.AbstractPDFFile)
                            {
                                LendersOffice.PdfGenerator.AbstractPDFFile _abstractPdfFile = obj as LendersOffice.PdfGenerator.AbstractPDFFile;
                                if (_abstractPdfFile.IsContainSignature == false)
                                {
                                    continue; // 6/9/2010 dd - OPM 52418 - We do not display the PdfForms list if there is no signature field in our standard form.
                                }

                                pdfForm.FileName = _abstractPdfFile.PdfFile;
                                pdfForm.IsSignable = _abstractPdfFile.IsContainSignature;
                                pdfForm.m_isContainBorrowerSignature = _abstractPdfFile.IsContainBorrowerSignature;
                                pdfForm.m_isContainCoborrowerSignature = _abstractPdfFile.IsContainCoborrowerSignature;
                                pdfForm.Layout = _abstractPdfFile.PdfFormLayout;
                            }
                            else if (obj is AbstractBatchPDF)
                            {
                                AbstractBatchPDF _abstractPdfFile = obj as AbstractBatchPDF;
                                if (_abstractPdfFile.IsContainSignature == false)
                                {
                                    continue; // 6/9/2010 dd - OPM 52418 - We do not display the PdfForms list if there is no signature field in our standard form.
                                }

                                pdfForm.IsSignable = _abstractPdfFile.IsContainSignature;
                                pdfForm.FileName = obj.Name + ".pdf";
                                pdfForm.m_isContainBorrowerSignature = _abstractPdfFile.IsContainBorrowerSignature;
                                pdfForm.m_isContainCoborrowerSignature = _abstractPdfFile.IsContainCoborrowerSignature;
                                //pdfForm.Layout = _abstractPdfFile.PdfFormLayout;
                            }
                            else
                            {
                                continue;
                                //    pdfForm.IsSignable = false;
                                //    pdfForm.FileName = obj.Name + ".pdf";
                            }
                            forms.Item1.Add(pdfForm);
                        }
                        catch (Exception exc)
                        {
                            throw new Exception(pdfItem.Type, exc);
                        }
                    }

                }

                return forms;
            };

            standardFormPdfListInitializer = new LqbSingleThreadInitializeLazy<Tuple<List<PdfForm>, List<ConstructorInfo>>>(initializer);
        }

        private string m_standardFormType;

        private PdfForm()
        {
        }

        public bool IsNew { get; private set; }

        public Guid FormId { get; private set; }
        public Guid BrokerId { get; private set; }
        public string Description { get; set; }
        public bool IsSignable { get; private set; }
        public bool IsStandardForm { get; private set; }
        public string FileName { get; set; }
        public string PreviewLink { get; set; }
        public Guid RecordId { get; private set; } // Only applicable to verification form.
        private byte[] x_pdfContent = null;
        private bool m_isPdfContentUpdated = false;

        public byte[] PdfContent
        {
            get
            {
                if (IsStandardForm)
                {
                    throw new NotSupportedException();
                }
                else
                {
                    if (x_pdfContent == null)
                    {
                        x_pdfContent = FileDBTools.ReadData(E_FileDB.EDMS, FormId.ToString());
                    }
                    return x_pdfContent;
                }
            }
            set 
            {
                if (IsStandardForm)
                {
                    return;
                }
                // 6/3/2010 dd - Perform a check to see if content pass the following criteria.
                // 1. A valid pdf
                // 2. PDF that does not contain owner password.
                try
                {
                    PdfReader reader = new PdfReader(value);
                    if (!reader.IsOpenedWithFullPermissions)
                    {
                        throw new InsufficientPdfPermissionException();
                    }
                }
                catch (IOException e)
                {
                    if (e.Message.Contains("Bad user Password"))
                    {
                        throw new InsufficientPdfPermissionException();
                    }
                    else
                    {
                        throw new InvalidPDFFileException();
                    }
                }


                x_pdfContent = value;
                m_isPdfContentUpdated = true;
            }
        }

        public List<PdfPageItem> GetPdfPageInfoList()
        {
            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(PdfContent);

            int pageCount = pdfReader.NumberOfPages;
            List<PdfPageItem> list = new List<PdfPageItem>(pageCount);
            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);
                list.Add(new PdfPageItem() { DocId = FormId, Page = i, Version = 0, Rotation = 0, PageHeight = (int)size.Height, PageWidth = (int)size.Width });

            }
            return list;
        }
        public byte[] GeneratePrintPdfAndPdfLayout(Guid sLId, Guid aAppId, Guid recordId, out PdfFormLayout pdfLayout)
        {
            IPDFGenerator pdf = null;
            pdfLayout = this.Layout;
            bool isBatchPdfControl = false;
            if (IsStandardForm)
            {
                ConstructorInfo constructor = PDFClassHashTable.GetConstructor(m_standardFormType);
                if (null != constructor)
                {
                    IPDFPrintItem obj = (IPDFPrintItem) constructor.Invoke(new object[0]);
                    NameValueCollection nv = new NameValueCollection();
                    nv["loanid"] = sLId.ToString();
                    nv["appid"] = aAppId.ToString();
                    nv["applicationid"] = aAppId.ToString(); // 6/7/2010 dd - TODO: Combine applicationid and appid into one.
                    nv["recordid"] = recordId.ToString();
                    obj.Arguments = nv;

                    pdf = obj as IPDFGenerator;

                    if (typeof(LendersOffice.PdfGenerator.BatchPDFControl).IsAssignableFrom(constructor.ReflectedType))
                    {
                        isBatchPdfControl = true;
                    }
                    
                }
            }
            else
            {
                CCustomPDF customPdf = new CCustomPDF();
                NameValueCollection nv = new NameValueCollection();
                nv["loanid"] = sLId.ToString();
                nv["appid"] = aAppId.ToString();
                nv["applicationid"] = aAppId.ToString(); // 6/7/2010 dd - TODO: Combine applicationid and appid into one.
                nv["custompdfid"] = FormId.ToString();
                customPdf.Arguments = nv;
                pdf = customPdf as IPDFGenerator;
            }
            MemoryStream stream = new MemoryStream();
            pdf.GeneratePDF(stream, "", "");
            byte[] content = stream.ToArray();//pdf.GeneratePDF("", ""); // 6/3/2010 dd - We want to generate unencrypt pdf form so Consumer Portal can attach fax cover sheet.

            if (isBatchPdfControl)
            {
                // 6/7/2010 dd - Get PdfLayout only work after BatchPdfControl invoke GeneratePdf method.
                pdfLayout = ((BatchPDFControl)pdf).GetPdfLayout();
            }
            return content;

        }
        private int x_numberOfPages = -1;
        public int NumberOfPages
        {
            get
            {
                if (x_numberOfPages < 0)
                {
                    PdfReader pdfReader = new PdfReader(PdfContent);
                    x_numberOfPages = pdfReader.NumberOfPages;
                }
                return x_numberOfPages;
            }
        }

        public byte[] GetPng(int page, Guid sessionId)
        {
            if (page < 0)
            {
                throw new CBaseException("Invalid page " + page, "Invalid page " + page);
            }

            // 6/2/2010 dd - Without versioning of the Pdf Form, we can not cache the png image.
            //IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
            //List<byte[]> pngContentList = rasterizer.ConvertToPng(PdfContent);
            //if (null == pngContentList)
            //{
            //    throw new CBaseException("Invalid Pdf.", "Invalid Pdf");
            //}

            //if (page >= pngContentList.Count)
            //{
            //    throw new CBaseException("Invalid page " + page, "Invalid page " + page);
            //}
            //return pngContentList[page];
            
                         
            byte[] buffer = null;

            string fileName = GetPngFileName(FormId, page, sessionId);
            if (File.Exists(fileName))
            {
                buffer = File.ReadAllBytes(fileName);
            }
            else
            {
                IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
                List<byte[]> pngContentList = rasterizer.ConvertToPng(PdfContent, ConstAppDavid.PdfPngScaling);
                if (null == pngContentList)
                {
                    throw new CBaseException("Invalid Pdf.", "Invalid Pdf");
                }
                for (int i = 0; i < pngContentList.Count; i++)
                {
                    string outputFileName = GetPngFileName(FormId, i, sessionId);
                    File.WriteAllBytes(outputFileName, pngContentList[i]);
                    if (i == page)
                    {
                        buffer = pngContentList[i];
                    }
                }
                if (page >= pngContentList.Count)
                {
                    throw new CBaseException("Invalid page " + page, "Invalid page " + page);
                }
            }
            return buffer;
             
        }

        //public byte[] GetPng(int page)
        //{
        //    // 6/2/2010 dd - Without versioning of the Pdf Form, we can not cache the png image.
        //    return GetPng(page, true);
        //}
        private PdfFormLayout m_layout = null;
        public PdfFormLayout Layout
        {
            get
            {
                // 5/11/2010 dd - Because we are store layout as the varchar(max) in database, we do not want
                // to load this field everytime we load up this object. Therefore we do lazy-load.
                if (m_layout == null)
                {
                    m_layout = new PdfFormLayout();

                    if (FormId != Guid.Empty)
                    {
                        string content = string.Empty;

                        if (IsStandardForm == false)
                        {
                            // 5/16/2015 dd - Only retrieve layout from database for non standard form.
                            SqlParameter[] parameters = {
                                            new SqlParameter("@FormId", FormId)
                                            , new SqlParameter("@BrokerId", BrokerId)
                                        };

                            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "RetrieveLoFormLayoutById", parameters))
                            {
                                if (reader.Read())
                                {
                                    content = (string)reader["FieldMetadata"];
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(content))
                        {
                            m_layout.LoadContent(content);
                        }
                    }
                }
                return m_layout;
            }
            set
            {
                m_layout = value;
                if (null == m_layout)
                {
                    IsSignable = false;
                }
                else
                {
                    IsSignable = m_layout.IsContainSignature;
                }
            }
        }

        private bool m_isContainBorrowerSignature;
        public bool IsContainBorrowerSignature
        {
            get 
            {
                if (IsStandardForm)
                {
                    return m_isContainBorrowerSignature;
                }
                else
                {
                    return Layout.IsContainBorrowerSignature;
                }
            }

        }

        private bool m_isContainCoborrowerSignature;
        public bool IsContainCoborrowerSignature
        {
            get 
            {
                if (IsStandardForm)
                {
                    return m_isContainCoborrowerSignature;
                }
                else
                {
                    return Layout.IsContainCoborrowerSignature;
                }
            }
        }

        public void Save()
        {
            if (IsStandardForm)
            {
                return;
            }
            string storedProcedureName = "";


            if (IsNew)
            {
                storedProcedureName = "CreateLoForm";
            }
            else
            {
                storedProcedureName = "UpdateLoForm";
            }
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@FormId", FormId));
            parameters.Add(new SqlParameter("@BrokerId", BrokerId));
            parameters.Add(new SqlParameter("@Description", Description));
            parameters.Add(new SqlParameter("@IsSignable", IsSignable));
            parameters.Add(new SqlParameter("@IsStandardForm", IsStandardForm));
            parameters.Add(new SqlParameter("@FileName", FileName));

            if (null != m_layout) 
            {
                StringBuilder sb = new StringBuilder();
                using (StringWriter writer = new StringWriter(sb)) {
                    m_layout.Save(writer);
                }
                parameters.Add(new SqlParameter("@FieldMetaData", sb.ToString()));
            }

            int ret = StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, storedProcedureName, 3, parameters);
            if (ret > 0)
            {
                IsNew = false;
                if (m_isPdfContentUpdated)
                {
                    FileDBTools.WriteData(E_FileDB.EDMS, FormId.ToString(), PdfContent);
                }
            }


        }

        public static void SavePages(Guid formId, List<PdfPageItem> pages)
        {
            PdfForm form = PdfForm.LoadById(formId);
            
            PdfReader reader = new PdfReader(form.PdfContent);
            Document document = new Document();
            MemoryStream outputStream = new MemoryStream();
            PdfCopy pdfWriter = new PdfCopy(document, outputStream);
            document.Open();
            foreach (PdfPageItem item in pages)
            {
                if (item.DocId != formId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "PdfForm does not support concatenate file from different document.");
                }
                if (item.Rotation != 0)
                {
                    int rotation = reader.GetPageRotation(item.Page) + item.Rotation;
                    reader.GetPageN(item.Page).Put(PdfName.ROTATE, new PdfNumber(rotation));
                }
                pdfWriter.AddPage(pdfWriter.GetImportedPage(reader, item.Page));
            }
            document.Close();

            form.PdfContent = outputStream.ToArray();
            form.Save();
        }

        public static PdfForm LoadById(Guid formId)
        {
            if (Guid.Empty == formId)
            {
                throw new NotFoundException("Form could not be found.");
            }

            foreach (var o in s_standardFormList)
            {
                if (o.FormId == formId)
                {
                    return o;
                }
            }
            foreach (ConstructorInfo constructor in s_verificationPdfList)
            {
                AbstractVerificationListPDF verificationList = (AbstractVerificationListPDF)constructor.Invoke(new object[0]);
                IEnumerable<PdfVerificationItem> items = null;
                LendersOffice.Pdf.AbstractPDFFile pdf = null;

                verificationList.GetInfo(null, out items, out pdf);
                if (pdf.GetType().GUID == formId)
                {
                    PdfForm o = new PdfForm();
                    o.m_standardFormType = pdf.GetType().FullName;
                    o.FormId = pdf.GetType().GUID;
                    o.IsStandardForm = true;
                    o.PreviewLink = pdf.Name + ".aspx";
                    o.FileName = pdf.PdfFile;
                    o.IsSignable = pdf.IsContainSignature;
                    o.m_isContainBorrowerSignature = pdf.IsContainBorrowerSignature;
                    o.m_isContainCoborrowerSignature = pdf.IsContainCoborrowerSignature;
                    o.Layout = pdf.PdfFormLayout;


                    return o;
                }
            }
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new GenericUserErrorMessageException("Principal is null when invoke PdfForm.LoadById");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@FormId", formId)
                                            , new SqlParameter("@BrokerId", principal.BrokerId)
                                        };

            PdfForm form = null;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveLoFormById", parameters))
            {
                if (reader.Read())
                {
                    form = new PdfForm();
                    form.FormId = formId;
                    form.BrokerId = principal.BrokerId;

                    form.Description = (string)reader["Description"];
                    form.IsSignable = (bool)reader["IsSignable"];
                    form.IsStandardForm = (bool)reader["IsStandardForm"];
                    form.FileName = (string)reader["FileName"];

                }
            }
            if (form == null)
            {
                throw new NotFoundException("Form could not be found.");
            }
            return form;
        }

        public static PdfForm LoadConsumerFormById(Guid formId)
        {
            if (Guid.Empty == formId)
            {
                throw new NotFoundException("Form could not be found.");
            }
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (!(principal is ConsumerUserPrincipal) && !(principal is ConsumerPortalUserPrincipal))
            {
                throw new GenericUserErrorMessageException("Principal is null when invoke PdfForm.LoadConsumerFormById");
            }

            bool isValidForm = FileDBTools.DoesFileExist(E_FileDB.EDMS, formId.ToString());

            PdfForm form = null;
            if (isValidForm)
            {
                form = new PdfForm();
                form.FormId = formId;
                form.BrokerId = principal.BrokerId;
                form.IsSignable = true;
            }

            if (form == null)
            {
                throw new NotFoundException("Form could not be found.");
            }
            return form;
        }

        /// <summary>
        /// Gets the description of a custom PDF form by ID.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the form.
        /// </param>
        /// <param name="formId">
        /// The ID of the form.
        /// </param>
        /// <returns>
        /// The description of the custom PDF form.
        /// </returns>
        public static string GetCustomPdfDescriptionById(Guid brokerId, Guid formId)
        {
            var procedureName = StoredProcedureName.Create("LO_FORM_GetDescriptionById").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@FormId", formId)
            };

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            {
                var result = StoredProcedureDriverHelper.ExecuteScalar(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                return result.ToString();
            }
        }

        public static PdfForm CreateNew()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new GenericUserErrorMessageException("Principal is null when invoke PdfForm.CreateNew");
            }

            PdfForm form = new PdfForm();
            form.FormId = Guid.NewGuid();
            form.BrokerId = principal.BrokerId;
            form.FileName = string.Empty;
            form.Description = string.Empty;
            form.IsSignable = false;
            form.IsStandardForm = false;
            form.IsNew = true;
            return form;
        }

        public static void DeleteById(Guid formId)
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new GenericUserErrorMessageException("Principal is null when invoke PdfForm.DeleteById");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@FormId", formId),
                                            new SqlParameter("@BrokerId", principal.BrokerId)
                                        };
            int ret = StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "DeleteLoFormById", 3, parameters);
            if (ret > 0)
            {
                // 5/11/2010 dd - Only delete FileDB if DeleteLoFormByID stored procedure delete actual row.
                FileDBTools.Delete(E_FileDB.EDMS,formId.ToString());
            }
        }

        /// <summary>
        /// Retrieve standard form and custom pdf form.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<PdfForm> RetrieveAllFormsOfCurrentBroker(Guid sLId, Guid aAppId)
        {
            var list = RetrieveCustomFormsOfCurrentBroker().ToList();


            foreach (PdfForm form in s_standardFormList)
            {
                list.Add(form);
            }
            CPageData dataLoan = new CPrintData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);


            foreach (ConstructorInfo constructor in s_verificationPdfList)
            {
                AbstractVerificationListPDF verificationList = (AbstractVerificationListPDF) constructor.Invoke(new object[0]);
                IEnumerable<PdfVerificationItem> items = null;
                LendersOffice.Pdf.AbstractPDFFile pdf = null;

                verificationList.GetInfo(dataApp, out items, out pdf);

                foreach (var o in items)
                {
                    NameValueCollection arguments = new NameValueCollection();
                    arguments["recordid"] = o.RecordId.ToString();
                    arguments["displayname"] = o.DisplayName;

                    pdf.Arguments = arguments;
                    PdfForm form = new PdfForm();
                    form.m_standardFormType = constructor.ReflectedType.FullName;
                    form.FormId = pdf.GetType().GUID;
                    form.IsStandardForm = true;
                    form.Description = pdf.Description;
                    form.PreviewLink = pdf.Name + ".aspx";
                    form.FileName = pdf.PdfFile;
                    form.IsSignable = pdf.IsContainSignature;
                    form.m_isContainBorrowerSignature = pdf.IsContainBorrowerSignature;
                    form.m_isContainCoborrowerSignature = pdf.IsContainCoborrowerSignature;
                    form.Layout = pdf.PdfFormLayout;
                    form.RecordId = o.RecordId;
                    list.Add(form);
                }

                
            }
            return list.OrderBy(o=> o.Description);
        }

        /// <summary>
        /// List all the custom pdf from the current brokerid.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<PdfForm> RetrieveCustomFormsOfCurrentBroker()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new GenericUserErrorMessageException("Principal is null when invoke PdfForm.RetrieveAll");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", principal.BrokerId)
                                        };
            List<PdfForm> list = new List<PdfForm>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "ListLoFormByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    PdfForm form = new PdfForm();
                    form.BrokerId = principal.BrokerId;

                    form.FormId = (Guid)reader["FormId"];
                    form.Description = (string)reader["Description"];
                    form.IsSignable = (bool)reader["IsSignable"];
                    form.IsStandardForm = (bool)reader["IsStandardForm"];
                    form.FileName = (string)reader["FileName"];

                    list.Add(form);
                }
            }
            return list;

        }

    }

    [Serializable]
    public class PdfFormCache
    {
        public Guid formId;
        public int NumberOfPages;
        public byte[] pdfContent;
        public List<byte[]> pngPages;

        private static string GetCacheFileName(Guid formId, Guid sessionId, long consumerId)
        {
            return string.Format("{0}\\{1}_{2}_{3}.pdf", ConstApp.TempFolder, consumerId, formId, sessionId);
        }

        public static PdfFormCache CacheThis(PdfForm form, Guid SessionId)
        {
            if (form == null || SessionId == Guid.Empty)
                return null;

            long consumerId; 
            if (PrincipalFactory.CurrentPrincipal is ConsumerUserPrincipal)
            {
                var p = (ConsumerUserPrincipal)PrincipalFactory.CurrentPrincipal;
                consumerId = p.ConsumerId;
            }
            else if (PrincipalFactory.CurrentPrincipal is ConsumerPortalUserPrincipal)
            {
                var p = (ConsumerPortalUserPrincipal)PrincipalFactory.CurrentPrincipal;
                consumerId = p.Id;
            }
            else
            {
                return null;
            }

            PdfFormCache cache = new PdfFormCache();
            cache.formId = form.FormId;
            cache.NumberOfPages = form.NumberOfPages;
            cache.pdfContent = form.PdfContent;

            for (int i = 0; i < form.NumberOfPages; i++)
            {
                byte[] pngPage = form.GetPng(i, SessionId);
                if (pngPage == null) return null;
                cache.pngPages.Add(pngPage);
            }

            string path = GetCacheFileName(form.FormId, SessionId, consumerId);

            using (Stream textWriter = File.Open(path, FileMode.Create))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(textWriter, cache);
            }

            return cache;
        }

        public static PdfFormCache LoadPdfFormCache(Guid form, Guid Session)
        {
            long consumerId;
            if (Thread.CurrentPrincipal is ConsumerUserPrincipal)
            {
                var p = (ConsumerUserPrincipal)PrincipalFactory.CurrentPrincipal;
                consumerId = p.ConsumerId;
            }
            else if (Thread.CurrentPrincipal is ConsumerPortalUserPrincipal)
            {
                var p = (ConsumerPortalUserPrincipal)PrincipalFactory.CurrentPrincipal;
                consumerId = p.Id;
            }
            else
            {
                return null;
            }
            string path = GetCacheFileName(form, Session, consumerId);
            PdfFormCache serializableObject = null;

            if (File.Exists(path))
            {
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    serializableObject = bFormatter.Deserialize(stream) as PdfFormCache;
                }
            }
            return serializableObject;
        }

        private PdfFormCache(){
            formId = Guid.Empty;
            NumberOfPages = 0;
            pdfContent = null;
            pngPages = new List<byte[]>();
        }


    }
}
