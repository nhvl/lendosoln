﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;

namespace LendersOffice.PdfForm
{
    public static class PdfFormHelper
    {
        public static string GetStandardPdfPath(string pdfFileName)
        {
            return ConstSite.StandardPdfLocation + pdfFileName;
            //Assembly assembly = Assembly.GetAssembly(typeof(PdfFormsHelper));

            //string name = "PdfForms." + pdfFileName;
            //return assembly.GetManifestResourceStream(name);
        }

        public static string GetStandardPdfLayoutPath(string pdfFileName)
        {
            return ConstSite.StandardPdfLocation + pdfFileName + ".xml";
            //Assembly assembly = Assembly.GetAssembly(typeof(PdfFormsHelper));

            //string name = "PdfForms." + pdfFileName + ".xml";
            //return assembly.GetManifestResourceStream(name);

        }
    }
}
