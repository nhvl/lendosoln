﻿// <copyright file="InputFieldNameSuffix.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   3/16/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// List of suffix used in input field model.
    /// </summary>
    public static class InputFieldNameSuffix
    {
        /// <summary>
        /// Suffix for lock property.
        /// </summary>
        public static readonly string Lock = "Lckd";

        /// <summary>
        /// Suffix for readonly attribute.
        /// </summary>
        public static readonly string ReadOnly = "ReadOnly";

        /// <summary>
        /// Suffix for visible attribute.
        /// </summary>
        public static readonly string Visible = "Visible";

        /// <summary>
        /// Suffix for zip property.
        /// </summary>
        public static readonly string Zip = "Zip";

        /// <summary>
        /// Suffix for state property.
        /// </summary>
        public static readonly string State = "State";

        /// <summary>
        /// Suffix for county property.
        /// </summary>
        public static readonly string County = "County";

        /// <summary>
        /// Suffix for city property.
        /// </summary>
        public static readonly string City = "City";

        /// <summary>
        /// Suffix for representation attribute.
        /// </summary>
        public static readonly string Representation = "_rep";

        /// <summary>
        /// Suffix for phone property.
        /// </summary>
        public static readonly string Phone = "Phone";

        /// <summary>
        /// Suffix for fax property.
        /// </summary>
        public static readonly string Fax = "Fax";

        /// <summary>
        /// Suffix for a list.
        /// </summary>
        public static readonly string List = "s";

        /// <summary>
        /// Suffix for template for record of a list.
        /// </summary>
        public static readonly string ListRecordTemplate = "Template";

        /// <summary>
        /// Suffix for options.
        /// </summary>
        public static readonly string Options = "Options";
    }
}
