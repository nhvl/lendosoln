﻿namespace LendersOffice.ObjLib.PriceGroups
{    
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Settings specific to enabling CrossBrowser.
    /// </summary>
    public class CrossBrowserSettings 
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The employee group name that is enabled, if it is restricted to a specific group.
        /// </summary>
        private string enabledEmployeeGroupName;

        /// <summary>
        /// The boolean that determines if this bit exists in the temp options.
        /// </summary>
        private bool enableCrossBrowserTempOptionExists;

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossBrowserSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The match from the lender's temp option XML content.</param>
        public CrossBrowserSettings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.enableCrossBrowserTempOptionExists = true;
                this.enabledEmployeeGroupName = match.Groups["EmployeeGroup"].Value;
            }
        }

        /// <summary>
        /// Determines if the updated internal pricer UI is enabled for the given principal.
        /// </summary>
        /// <param name="principal">The principal to evaluate.</param>
        /// <returns>True if the user has access. Otherwise, false.</returns>
        public bool IsEnabledForCurrentUser(AbstractUserPrincipal principal)
        {
            if (!this.enableCrossBrowserTempOptionExists)
            {
                return false;
            }

            bool blankEmployeeName = string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName);

            if (principal.Type == "B")
            {
                if (blankEmployeeName)
                {
                    return true;
                }

                if (this.enabledEmployeeGroupName == "EmptyGroup")
                {
                    return false;
                }

                return principal.IsInEmployeeGroup(this.enabledEmployeeGroupName);
            }

            return false;        
        }

        /// <summary>
        /// Validates that the employee group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateEmployeeGroup()
        {
            return !this.enableCrossBrowserTempOptionExists
                || string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName)
                || this.enabledEmployeeGroupName == "EmptyGroup"
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.Employee, this.enabledEmployeeGroupName);
        }
    }    
}