﻿// <copyright file="DataAppModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Data model for <code>CAppData</code>.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DataAppModel
    {
        /// <summary>
        /// Gets or sets a set of simple fields.
        /// </summary>
        /// <value>A set of simple fields.</value>
        [JsonProperty("fields")]
        public SimpleObjectInputFieldModel Fields { get; set; }

        /// <summary>
        /// Gets or sets a liability collection.
        /// </summary>
        /// <value>A liability collection.</value>
        [JsonProperty("aLiaCollection")]
        public Dictionary<string, object> LiaCollection { get; set; }

        /// <summary>
        /// Gets or sets an asset collection.
        /// </summary>
        /// <value>An asset collection.</value>
        [JsonProperty("aAssetCollection")]
        public Dictionary<string, object> AssetCollection { get; set; }

        /// <summary>
        /// Gets or sets a borrower employment collection.
        /// </summary>
        /// <value>A borrower employment collection.</value>
        [JsonProperty("aBEmpCollection")]
        public Dictionary<string, object> BEmpCollection { get; set; }

        /// <summary>
        /// Gets or sets a coborrower employment collection.
        /// </summary>
        /// <value>A coborrower employment collection.</value>
        [JsonProperty("aCEmpCollection")]
        public List<SimpleObjectInputFieldModel> CEmpCollection { get; set; }

        /// <summary>
        /// Gets or sets a borrower aliases name.
        /// </summary>
        /// <value>A borrower aliases collection.</value>
        [JsonProperty("aBAliases")]
        public List<InputFieldModel> BAliases { get; set; }

        /// <summary>
        /// Gets or sets a coborrower aliases name.
        /// </summary>
        /// <value>A coborrower aliases collection.</value>
        [JsonProperty("aCAliases")]
        public List<InputFieldModel> CAliases { get; set; }

        /// <summary>
        /// Gets or sets of other income list.
        /// </summary>
        /// <value>List of other incom input.</value>
        [JsonProperty("aOtherIncomeList")]
        public ListModel OtherIncomeList { get; set; }

        /// <summary>
        /// Gets or sets a reo collection.
        /// </summary>
        /// <value>A reo collection.</value>
        [JsonProperty("aReCollection")]
        public Dictionary<string, object> ReCollection { get; set; }
    }
}