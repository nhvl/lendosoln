﻿// <copyright file="InputFieldModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;

    /// <summary>
    /// E_CalculationType set calculating actions if the field is changed.
    /// </summary>
    public enum E_CalculationType
    {
        /// <summary>No calculate.</summary>
        CalculateNormal = 0,
        
        /// <summary>Normal calculate.</summary>
        NoCalculate = 1,
        
        /// <summary>Zip calculate.</summary>
        CalculateZip = 2
    }

    /// <summary>
    /// A data model that represent an input fields to render on client side.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class InputFieldModel
    {
        /// <summary>
        /// Internal variable of property InputFieldType.
        /// </summary>
        private InputFieldType type;

        /// <summary>
        /// Internal variable of property Value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the InputFieldModel class.
        /// </summary>
        public InputFieldModel()
        {
            this.value = string.Empty;
            this.type = InputFieldType.Unknown;
        }

        /// <summary>
        /// Initializes a new instance of the InputFieldModel class, using object to saving the number of code dealing with every input value type.
        /// </summary>
        /// <param name="valueOject">Value of field input.</param>///
        /// <param name="readOnly">The readonly attribut of field input.</param>
        /// <param name="hidden">The hidden attribut of field input.</param>
        /// <param name="customInputType">The custom value type of field input.</param>customOptions
        /// <param name="customOptions">The custom drop down list options of field input.</param>
        public InputFieldModel(object valueOject, bool readOnly = false, bool hidden = false, InputFieldType customInputType = InputFieldType.Unknown, IEnumerable<KeyValuePair<string, string>> customOptions = null)
        {
            if (valueOject is string)
            {
                this.value = valueOject as string;
                this.type = InputFieldType.String;
            }
            else if (valueOject is bool)
            {
                this.value = ((bool)valueOject).ToString();
                this.type = InputFieldType.Checkbox;
            }
            else if (valueOject is Guid)
            {
                this.value = ((Guid)valueOject).ToString();
                this.type = InputFieldType.String;
            }
            else if (valueOject is DateTime)
            {
                var date = (DateTime)valueOject;
                this.value = date == DateTime.MinValue ? null : JsonConvert.SerializeObject(date).Replace("\"", string.Empty);
                this.type = InputFieldType.Date;
            }
            else if (valueOject is DataAccess.CDateTime)
            {
                DataAccess.CDateTime timeObj = valueOject as DataAccess.CDateTime;
                this.value = timeObj.IsValid ? JsonConvert.SerializeObject(timeObj.DateTimeForComputationWithTime).Replace("\"", string.Empty) : null;
                this.type = InputFieldType.Date;
            }
            else if (valueOject is Enum)
            {
                this.value = Convert.ToInt32(valueOject).ToString();

                if (customInputType != InputFieldType.Radio)
                {
                    this.type = InputFieldType.DropDownList;
                    this.Options = DataAccess.EnumUtilities.GetValuesFromType(valueOject.GetType());
                }
            }
            else if (valueOject is decimal)
            {
                this.value = Convert.ToDecimal(valueOject).ToString();
                this.type = InputFieldType.Money;
            }
            else if (valueOject is double)
            {
                this.value = Convert.ToDouble(valueOject).ToString();
                this.type = InputFieldType.Money;
            }
            else if (valueOject is int)
            {
                this.value = Convert.ToInt32(valueOject).ToString();
                this.type = InputFieldType.Number;
            }
            else if (valueOject is Sensitive<string>)
            {
                this.value = ((Sensitive<string>)valueOject).Value;
                this.type = InputFieldType.String;
            }
            else if (valueOject is long)
            {
                this.value = Convert.ToInt64(valueOject).ToString();
                this.type = InputFieldType.Number;
            }
            else if (valueOject == null)
            {
                this.type = InputFieldType.String;
            }
            else
            {
                throw new NotSupportedException("This object type is not yet supported: " + valueOject.GetType());   
            }

            this.IsReadOnly = readOnly;
            this.IsHidden = hidden;

            if (customOptions != null)
            {
                this.Options = customOptions;
            }

            if (customInputType != InputFieldType.Unknown)
            {
                this.type = customInputType;
            }
        }

        /// <summary>
        /// Initializes a new instance of the InputFieldModel class.
        /// </summary>
        /// <param name="value">Value of field input.</param>
        /// <param name="type">The type of field input.</param>
        /// <param name="readOnly">The readonly attribut of field input.</param>
        /// <param name="options">The options attribut of field input.</param>
        /// <param name="hidden">The hidden attribut of field input.</param>
        public InputFieldModel(string value, InputFieldType type, bool readOnly = false, IEnumerable<KeyValuePair<string, string>> options = null, bool hidden = false)
        {
            this.value = value;
            this.type = type;
            this.IsReadOnly = readOnly;
            this.Options = options;
            this.IsHidden = hidden;
        }

        /// <summary>
        /// Gets or sets the type of input.
        /// </summary>
        /// <value>The type of input.</value>
        [JsonProperty("t")]
        public InputFieldType Type 
        { 
            get 
            { 
                return this.type; 
            } 

            set 
            {
                this.type = value; 
                this.InternalHandleNullValue(); 
            } 
        }

        /// <summary>
        /// Gets or sets the string value of input. Regardless the type of input the value will always cast to string.
        /// </summary>
        /// <value>The string value of input.</value>
        [JsonProperty("v")]
        public string Value 
        { 
            get 
            { 
                return this.value; 
            } 

            set 
            { 
                this.value = value;
                this.InternalHandleNullValue(); 
            } 
        }

        /// <summary>
        /// Gets or sets the maximum number of characters for string input.
        /// </summary>
        /// <value>The maximum number of characters for tring input.</value>
        [JsonProperty("maxLength")]
        public int MaxLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether input is mark as read only.
        /// </summary>
        /// <value>Whether input is mark as read only.</value>
        [JsonProperty("r")]
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Gets or sets front-end calculate type.
        /// </summary>
        /// <value>Calculate type.</value>
        [JsonProperty("calcAction")]
        public E_CalculationType CalcActionT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether input is hidden on user form.
        /// </summary>
        /// <value>Whether input is hidden on user form.</value>
        [JsonProperty("h")]
        public bool IsHidden { get; set; }

        /// <summary>
        /// Gets or sets a value indicating style will be used for this input.
        /// </summary>
        /// <value>List of style the input must have.</value>
        [JsonProperty("s")]
        public IEnumerable<string> Styles { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the input value is required.
        /// </summary>
        /// <value>A value indicating whether the input value is required.</value>
        [JsonProperty("required")]
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets a list of options for drop down list input type.
        /// </summary>
        /// <value>A list of options for drop down list input type.</value>
        [JsonProperty("o")]
        public IEnumerable<KeyValuePair<string, string>> Options { get; set; }

        /// <summary>
        ///  Bind the value to object depending on that object type.
        /// </summary>
        /// <param name="objectType">The type that value must be parsed in.</param>
        /// <returns>The value in object type, return null if can not parse the value.</returns>
        public object ValueFromType(Type objectType) 
        {
            object valueOject = null;
            if (objectType.IsAssignableFrom(typeof(string)))
            {
                if (this.type != InputFieldType.Date)
                {
                    valueOject = this.value; 
                }
                else
                {
                    DateTime tempDate;
                    if (DateTime.TryParse(this.value, out tempDate))
                    {
                        valueOject = new DataAccess.LosConvert().ToDateTimeString(tempDate, false);
                    }
                }
            }
            else if (objectType.IsAssignableFrom(typeof(bool)))
            {
                bool tempBool;
                if (bool.TryParse(this.value, out tempBool))
                {
                    valueOject = tempBool;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(Guid)))
            {
                valueOject = new Guid(this.value);
            }
            else if (objectType.IsAssignableFrom(typeof(DateTime)))
            {
                DateTime tempDate;
                if (DateTime.TryParse(this.value, out tempDate))
                {
                    valueOject = tempDate;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(DataAccess.CDateTime)))
            {
                DateTime tempDate;
                if (DateTime.TryParse(this.value, out tempDate))
                {
                    valueOject = DataAccess.CDateTime.Create(tempDate);
                }
            }
            else if (objectType.IsAssignableFrom(typeof(int)) || objectType.IsEnum)
            {
                int tempInt;
                if (int.TryParse(this.value, out tempInt))
                {
                    valueOject = tempInt;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(long)))
            {
                long tempLong;
                if (long.TryParse(this.value, out tempLong))
                {
                    valueOject = tempLong;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(double)))
            {
                double tempDouble;
                if (double.TryParse(this.value, out tempDouble))
                {
                    valueOject = tempDouble;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(float)))
            {
                float tempFloat;
                if (float.TryParse(this.value, out tempFloat))
                {
                    valueOject = tempFloat;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(decimal)))
            {
                decimal tempDecimal;
                if (decimal.TryParse(this.value, out tempDecimal))
                {
                    valueOject = tempDecimal;
                }
            }
            else if (objectType.IsAssignableFrom(typeof(Sensitive<string>)))
            {
                valueOject = new Sensitive<string>(this.value);
            }
            else
            {
                throw new NotSupportedException("This object type is not yet supported.");
            }

            return valueOject;
        }

        /// <summary>
        /// Bind the value of a field model to object property.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <typeparam name="S">The second generic type parameter, for object property type.</typeparam>
        /// <param name="target">The object need to be change property.</param>
        /// <param name="outExpr">The expression to identfy the object property.</param>
        public void BindObjectFromModel<T, S>(T target, Expression<Func<T, S>> outExpr)
        {
            MemberExpression expr;
            if (outExpr.Body is MemberExpression)
            {
                expr = (MemberExpression)outExpr.Body;
            }
            else
            {
                var op = ((UnaryExpression)outExpr.Body).Operand;
                expr = (MemberExpression)op;
            }

            var prop = (PropertyInfo)expr.Member;
            if (prop.SetMethod != null)
            {
                object objectValue = this.ValueFromType(prop.PropertyType);
                if (objectValue != null)
                {
                    prop.SetValue(target, objectValue, null);
                }
            }
        }

        /// <summary>
        /// Check current field is valid or not.
        /// </summary>
        /// <returns>Return false if field is not valid.</returns>
        public bool IsValid() 
        {
            if (this.Required)
            {
                if (this.Type == InputFieldType.String || this.Type == InputFieldType.TextArea)
                {
                    if (string.IsNullOrEmpty(this.Value))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Update the nil value to fit its input type.
        /// </summary>
        private void InternalHandleNullValue()
        {
            if (string.IsNullOrEmpty(this.value))
            {
                this.value = this.type.IsStringField() ? string.Empty : null;
            }
        }
    }
}