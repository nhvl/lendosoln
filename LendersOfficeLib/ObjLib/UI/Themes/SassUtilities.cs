﻿// <copyright file="SassUtilities.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Admin;
    using Common;
    using Constants;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSass;

    /// <summary>
    /// Provides a set of utilities for working with SASS.
    /// </summary>
    public static class SassUtilities
    {
        /// <summary>
        /// Retrieves the default color theme for a lender using the lender's
        /// PML site ID.
        /// </summary>
        /// <param name="lenderPmlSiteId">
        /// The PML site ID of the lender.
        /// </param>
        /// <returns>
        /// The color theme.
        /// </returns>
        public static ColorTheme GetColorThemeFromLenderPmlSiteId(Guid lenderPmlSiteId)
        {
            if (lenderPmlSiteId != Guid.Empty)
            {
                var brokerId = BrokerDB.GetBrokerIdByBrokerPmlSiteId(lenderPmlSiteId);
                if (brokerId != Guid.Empty)
                {
                    var themeCollection = ThemeManager.GetThemeCollection(brokerId, ThemeCollectionType.TpoPortal);
                    return themeCollection.GetTheme(themeCollection.SelectedThemeId);
                }
            }

            return SassDefaults.LqbDefaultColorTheme;
        }

        /// <summary>
        /// Compiles the specified <paramref name="selectedTheme"/> to CSS.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        public static string CompileColorTheme(ColorTheme selectedTheme)
        {
            if (selectedTheme.Id == SassDefaults.LqbDefaultColorTheme.Id)
            {
                return SassDefaults.CompiledDefaultTheme;
            }

            return SassUtilities.CompileTheme(selectedTheme);
        }

        /// <summary>
        /// Compiles the specified <paramref name="selectedTheme"/> to CSS
        /// for previewing.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        public static string CompileColorThemeForPreview(ColorTheme selectedTheme)
        {
            return CompileSass(selectedTheme, SassDefaults.LqbMasterPreviewContent);
        }

        /// <summary>
        /// Compiles the navigation for the specified <paramref name="selectedTheme"/> to CSS.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS for the navigation.
        /// </returns>
        public static string CompileColorThemeNavigation(ColorTheme selectedTheme)
        {
            if (selectedTheme.Id == SassDefaults.LqbDefaultColorTheme.Id)
            {
                return SassDefaults.CompiledDefaultNavigationCss;
            }

            return SassUtilities.CompileNavigation(selectedTheme);
        }

        /// <summary>
        /// Creates a dictionary of variable name - variable value pairs.
        /// </summary>
        /// <param name="lines">
        /// The lines containing SASS variables.
        /// </param>
        /// <param name="removeDefaultKeyword">
        /// True if the <code>!default</code> keyword should be removed from
        /// a variable declaration, false otherwise.
        /// </param>
        /// <returns>
        /// A dictionary of variable name - variable value pairs.
        /// </returns>
        public static Dictionary<string, string> CreateVariableDictionary(IEnumerable<string> lines, bool removeDefaultKeyword)
        {
            var result = new Dictionary<string, string>();

            var localLevel = 0;
            foreach (var line in lines)
            {
                var lineT = line.Split(new[] { "//" }, StringSplitOptions.None)[0].Trim();

                if (lineT.Contains("{"))
                {
                    localLevel++;
                }
                else if (lineT.Contains("}"))
                {
                    localLevel--;
                }

                if (localLevel > 0)
                {
                    continue;
                }

                if (lineT.Contains("$") && lineT.Contains(":"))
                {
                    string[] props = lineT.Split(':');
                    if (props[0].IndexOf("$") == 0)
                    {
                        var propertyVal = props[1].Replace(";", string.Empty);

                        if (removeDefaultKeyword)
                        {
                            propertyVal = propertyVal.Replace("!default", string.Empty);
                        }
                        
                        if (!result.ContainsKey(props[0]))
                        {
                            result.Add(props[0], propertyVal.Trim());
                        }
                        else
                        {
                            result[props[0]] = propertyVal.Trim();
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Stores a theme to the cache for previewing.
        /// </summary>
        /// <param name="themeId">
        /// The ID of the theme.
        /// </param>
        /// <param name="name">
        /// The name of the theme.
        /// </param>
        /// <param name="variables">
        /// The variables for the theme.
        /// </param>
        /// <returns>
        /// The ID of the cache entry.
        /// </returns>
        public static string StoreThemeToCache(Guid themeId, string name, Dictionary<string, string> variables)
        {
            const double PreviewCacheDuration = 5.0;

            var cacheTheme = new ColorTheme()
            {
                Id = themeId,
                Name = name,
                Variables = variables
            };

            var serializedTheme = SerializationHelper.JsonNetSerialize(cacheTheme);
            return AutoExpiredTextCache.AddToCache(serializedTheme, TimeSpan.FromMinutes(PreviewCacheDuration));
        }

        /// <summary>
        /// Retrieves a previously saved theme's variables from the cache for previewing.
        /// </summary>
        /// <param name="cacheId">
        /// The ID of the cache entry.
        /// </param>
        /// <returns>
        /// The variables for the theme.
        /// </returns>
        public static ColorTheme GetColorThemeFromCache(string cacheId)
        {
            var cacheEntry = AutoExpiredTextCache.GetFromCache(cacheId);
            return SerializationHelper.JsonNetDeserialize<ColorTheme>(cacheEntry);
        }

        /// <summary>
        /// Determines whether the variables in the specified <paramref name="variableList"/> 
        /// have valid SASS variable values.
        /// </summary>
        /// <param name="variableList">
        /// The list of variables.
        /// </param>
        /// <exception cref="CBaseException">
        /// One or more variables have invalid values.
        /// </exception>
        public static void ValidateVariableValueList(IEnumerable<KeyValuePair<string, string>> variableList)
        {
            var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
            var driver = factory.Create();

            var badValues = new List<string>();
            foreach (var kvp in variableList)
            {
                if (!driver.IsMatch(RegularExpressionString.SassVariableValue, kvp.Value))
                {
                    badValues.Add(kvp.Value);
                }
            }

            if (badValues.Any())
            {
                var errorMessage = ErrorMessages.TpoColorTheme.InvalidColorValue + Environment.NewLine + string.Join(Environment.NewLine, badValues);
                throw new CBaseException(errorMessage, errorMessage);
            }
        }

        /// <summary>
        /// Compiles the specified <paramref name="selectedTheme"/> to CSS.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        internal static string CompileTheme(ColorTheme selectedTheme)
        {
            return CompileSass(selectedTheme, SassDefaults.LqbMasterSassContent);
        }

        /// <summary>
        /// Compiles the specified <paramref name="selectedTheme"/> to navigation CSS.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        internal static string CompileNavigation(ColorTheme selectedTheme)
        {
            return CompileSass(selectedTheme, SassDefaults.LqbMasterNavigationContent);
        }

        /// <summary>
        /// Compiles the CSS rules for the "Pricing" page in PML.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        internal static string CompilePricing(ColorTheme selectedTheme)
        {
            return CompileSass(selectedTheme, SassDefaults.LqbMasterPricingContent);
        }

        /// <summary>
        /// Compiles SASS content for the specified <paramref name="selectedTheme"/>
        /// using <paramref name="contentBase"/> as the main source of SASS rules.
        /// </summary>
        /// <param name="selectedTheme">
        /// The color theme to compile.
        /// </param>
        /// <param name="contentBase">
        /// The source for SASS rules.
        /// </param>
        /// <returns>
        /// The compiled CSS.
        /// </returns>
        private static string CompileSass(ColorTheme selectedTheme, string contentBase)
        {
            var builder = new StringBuilder();

            builder.AppendLine(string.Join(string.Empty, selectedTheme.Variables.Select(kvp => kvp.Key + ": " + kvp.Value + ";")));
            builder.AppendLine(SassDefaults.LqbMasterVariablesContent);
            builder.AppendLine(SassDefaults.LqbMasterFunctionContent);
            builder.AppendLine(contentBase);

            var compiler = new SassCompiler();
            return compiler.Compile(builder.ToString(), OutputStyle.Compressed, sourceComments: false);
        }
    }
}
