﻿// <copyright file="ThemeCollectionType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    /// <summary>
    /// Represents the type of a theme collection.
    /// </summary>
    public enum ThemeCollectionType
    {
        /// <summary>
        /// A theme collection for the TPO portal.
        /// </summary>
        TpoPortal = 1
    }
}
