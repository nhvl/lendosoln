﻿// <copyright file="SassDefaults.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a set of defaults for working with SASS.
    /// </summary>
    public static class SassDefaults
    {
        /// <summary>
        /// Represents the name of the LendingQB default theme master SASS file.
        /// </summary>
        private const string LqbDefaultMasterSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterSCSS.scss";

        /// <summary>
        /// Represents the name of the LendingQB default theme master variables SASS file.
        /// </summary>
        private const string LqbDefaultMasterVariableSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterVariables.scss";

        /// <summary>
        /// Represents the name of the LendingQB default theme master function SASS file.
        /// </summary>
        private const string LqbDefaultMasterFunctionSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterFunc.scss";

        /// <summary>
        /// Represents the name of the LendingQB default theme master navigation SASS file.
        /// </summary>
        private const string LqbDefaultMasterNavigationSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterNavigation.scss";

        /// <summary>
        /// Represents the name of the LendingQB default theme master pricing SASS file.
        /// </summary>
        private const string LqbDefaultMasterPricingSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterPricing.scss";

        /// <summary>
        /// Represents the name of the LendingQB default theme master preview SASS file.
        /// </summary>
        private const string LqbDefaultMasterPreviewSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterPreview.scss";

        /// <summary>
        /// Represents the ID of the LendingQB default theme.
        /// </summary>
        private static readonly Guid LqbDefaultColorThemeId = new Guid("11111111-1111-1111-1111-111111111111");

        /// <summary>
        /// Initializes static members of the <see cref="SassDefaults"/> class.
        /// </summary>
        static SassDefaults()
        {
            var assembly = typeof(SassDefaults).Assembly;

            LqbMasterSassContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterSassFileName);
            LqbMasterVariablesContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterVariableSassFileName);
            LqbMasterFunctionContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterFunctionSassFileName);
            LqbMasterNavigationContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterNavigationSassFileName);
            LqbMasterPricingContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterPricingSassFileName);
            LqbMasterPreviewContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterPreviewSassFileName);

            LqbDefaultColorTheme = GetLqbDefaultColorTheme();

            CompiledDefaultTheme = SassUtilities.CompileTheme(LqbDefaultColorTheme);
            CompiledDefaultNavigationCss = SassUtilities.CompileNavigation(LqbDefaultColorTheme);
            CompiledDefaultPricingCss = SassUtilities.CompilePricing(LqbDefaultColorTheme);

            UiEditorNameToSassVariableNameMappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                ["PageHeaderBackground"] = "$primary-high",
                ["PageHeaderText"] = "$font-default-header-bg",
                ["PageHeaderAccent"] = "$accent-header-bg",
                ["PageHeaderSaveButtonText"] = "$save-button-color-text",
                ["PageHeaderSaveButtonBackground"] = "$save-button-color-bg",
                ["SideNavigationBackground"] = "$primary-preferred",
                ["SideNavigationSelection"] = "$nav-selected",
                ["SideNavigationCollapseExpandButton"] = "$secondary-preferred",
                ["SideNavigationText"] = "$font-nav",
                ["ContentHeader"] = "$primary-low",
                ["ContentIconButtonTab"] = "$accent-high",
                ["ContentLinkSortedColumn"] = "$accent-preferred",
                ["ContentTooltip"] = "$secondary-high",
                ["ContentTooltipText"] = "$font-tooltip",
                ["ContentText"] = "$font-content",
                ["ContentRaisedButtonText"] = "$font-btn-raised",
                ["ContentFlatButtonText"] = "$font-btn-flat",
                ["ContentProgressBarPast"] = "$progress-bar-preferred",
                ["ContentProgressBarCurrent"] = "$progress-bar-high"
            };

            SassVariableNameToUiEditorNameMappings = UiEditorNameToSassVariableNameMappings.ToDictionary(
                kvp => kvp.Value,
                kvp => kvp.Key,
                StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the LendingQB default color theme.
        /// </summary>
        /// <value>
        /// The LendingQB default color theme.
        /// </value>
        public static ColorTheme LqbDefaultColorTheme { get; }

        /// <summary>
        /// Gets the mappings between the color theme editor's variable names
        /// to the names of the SASS variables.
        /// </summary>
        /// <value>
        /// The mappings between editor variable names and SASS variable names.
        /// </value>
        public static IReadOnlyDictionary<string, string> UiEditorNameToSassVariableNameMappings { get; }

        /// <summary>
        /// Gets the mappings between the names of the SASS variables to the 
        /// color theme editor's variable names.
        /// </summary>
        /// <value>
        /// The mappings between SASS variable names and editor variable names.
        /// </value>
        public static IReadOnlyDictionary<string, string> SassVariableNameToUiEditorNameMappings { get; }

        /// <summary>
        /// Gets the compiled pricing page CSS.
        /// </summary>
        /// <value>
        /// The compiled CSS.
        /// </value>
        public static string CompiledDefaultPricingCss { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterSassContent { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master variables SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterVariablesContent { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master function SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterFunctionContent { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master navigation SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterNavigationContent { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master pricing SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterPricingContent { get; }

        /// <summary>
        /// Gets the SASS content for the LendingQB master preview SASS file.
        /// </summary>
        /// <value>
        /// The SASS content.
        /// </value>
        internal static string LqbMasterPreviewContent { get; }

        /// <summary>
        /// Gets the compiled CSS for the LQB default color theme.
        /// </summary>
        /// <value>
        /// The compiled CSS.
        /// </value>
        internal static string CompiledDefaultTheme { get; }

        /// <summary>
        /// Gets the compiled navigation CSS for the LQB default color theme.
        /// </summary>
        /// <value>
        /// The compiled CSS.
        /// </value>
        internal static string CompiledDefaultNavigationCss { get; }

        /// <summary>
        /// Retrieves the color theme for the LendingQB default color scheme.
        /// </summary>
        /// <returns>
        /// The color theme for the LendingQB default color scheme.
        /// </returns>
        private static ColorTheme GetLqbDefaultColorTheme()
        {
            var variableList = SassDefaults.LqbMasterVariablesContent.NormalizeNewLines().Split(
                new[] { "\r\n" }, 
                StringSplitOptions.RemoveEmptyEntries);

            return new ColorTheme()
            {
                Id = SassDefaults.LqbDefaultColorThemeId,
                Name = "LendingQB Default Color Theme",
                Variables = SassUtilities.CreateVariableDictionary(variableList, removeDefaultKeyword: true)
            };
        }

        /// <summary>
        /// Retrieves the embedded SASS content from the assembly using the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="embeddedResourceAssembly">
        /// The assembly containing the embedded file.
        /// </param>
        /// <param name="name">
        /// The name of the embedded file.
        /// </param>
        /// <returns>
        /// The content of the embedded file.
        /// </returns>
        private static string GetEmbeddedSassContent(System.Reflection.Assembly embeddedResourceAssembly, string name)
        {
            using (var stream = new StreamReader(embeddedResourceAssembly.GetManifestResourceStream(name)))
            {
                return stream.ReadToEnd();
            }
        }
    }
}
