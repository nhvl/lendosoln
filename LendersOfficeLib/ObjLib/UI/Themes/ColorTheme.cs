﻿// <copyright file="ColorTheme.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Represents a color theme as a set of SASS color variables.
    /// </summary>
    public class ColorTheme
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColorTheme"/> class.
        /// </summary>
        public ColorTheme()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorTheme"/> class,
        /// populating data from the specified <paramref name="source"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the theme.
        /// </param>
        /// <param name="source">
        /// The source for the color theme's data.
        /// </param>
        internal ColorTheme(Guid brokerId, IReadOnlyDictionary<string, object> source)
        {
            this.BrokerId = brokerId;
            this.Id = (Guid)source["ColorThemeId"];
            this.Name = source["Name"].ToString();
            this.Variables = new Dictionary<string, string>(SassDefaults.LqbDefaultColorTheme.Variables);

            foreach (var key in source.Keys)
            {
                string sassVariableName;
                if (SassDefaults.UiEditorNameToSassVariableNameMappings.TryGetValue(key, out sassVariableName) &&
                    !string.IsNullOrWhiteSpace(source[key].ToString()))
                {
                    this.Variables[sassVariableName] = source[key].ToString();
                }
            }
        }

        /// <summary>
        /// Gets the broker ID of the theme.
        /// </summary>
        /// <value>
        /// The broker ID of the theme.
        /// </value>
        public Guid BrokerId { get; internal set; }

        /// <summary>
        /// Gets the ID of the theme.
        /// </summary>
        /// <value>
        /// The ID of the theme.
        /// </value>
        public Guid Id { get; internal set; }

        /// <summary>
        /// Gets or sets the name of the theme.
        /// </summary>
        /// <value>
        /// The name of the theme.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the variable name - variable value pairs for the theme.
        /// </summary>
        /// <value>
        /// The variable name - variable value pairs.
        /// </value>
        public Dictionary<string, string> Variables { get; internal set; } = new Dictionary<string, string>();

        /// <summary>
        /// Saves data for the color theme.
        /// </summary>
        /// <param name="collectionId">
        /// The ID of the collection containing this theme.
        /// </param>
        /// <param name="executor">
        /// The transactional stored procedure executor.
        /// </param>
        internal void Save(int collectionId, CStoredProcedureExec executor)
        {
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@CollectionId", collectionId),
                new SqlParameter("@ColorThemeId", this.Id),
                new SqlParameter("@Name", this.Name)
            };
            
            parameters.AddRange(this.GetUpdatedVariables());

            executor.ExecuteNonQueryWithCustomTimeout(
                "ColorTheme_Save", 
                nRetry: 1,
                nTimeoutSeconds: 30,
                bSendOnError: false,
                parameters: parameters);
        }

        /// <summary>
        /// Deletes the theme.
        /// </summary>
        /// <param name="collectionId">
        /// The ID of the collection containing this color theme.
        /// </param>
        /// <param name="executor">
        /// The stored procedure executor for the transaction.
        /// </param>
        internal void Delete(int collectionId, CStoredProcedureExec executor)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@CollectionId", collectionId),
                new SqlParameter("@ColorThemeId", this.Id),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            executor.ExecuteNonQuery("ColorTheme_Delete", parameters);
        }

        /// <summary>
        /// Gets a list of updated variable values to store in the database.
        /// </summary>
        /// <returns>
        /// The list of updated variables.
        /// </returns>
        private IEnumerable<SqlParameter> GetUpdatedVariables()
        {
            return this.Variables
                            .Where(variable => SassDefaults.SassVariableNameToUiEditorNameMappings.ContainsKey(variable.Key))
                            .Select(variable =>
                            {
                                var columnName = SassDefaults.SassVariableNameToUiEditorNameMappings[variable.Key];
                                return new SqlParameter(columnName, variable.Value);
                            });
        }
    }
}
