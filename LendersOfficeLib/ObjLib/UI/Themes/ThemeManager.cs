﻿// <copyright file="ThemeManager.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    using System;
    using CommonProjectLib.Caching;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;

    /// <summary>
    /// Provides a means for managing theme collections and compiled theme CSS.
    /// </summary>
    public static class ThemeManager
    {
        /// <summary>
        /// Gets the theme collection for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="type">
        /// The type of the theme collection.
        /// </param>
        /// <returns>
        /// The theme collection for the broker.
        /// </returns>
        public static ThemeCollection GetThemeCollection(Guid brokerId, ThemeCollectionType type)
        {
            using (PerformanceMonitorItem.Time(nameof(GetThemeCollection) + ":: Type " + type))
            {
                var key = GetThemeCollectionKey(brokerId, type);

                var themeCollection = MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                    key,
                    () => ThemeCollection.GetVersion(brokerId, type),
                    () => GetThemeCollectionForCache(brokerId, type),
                    ConstStage.SassCacheDuration);

                return themeCollection;
            }
        }

        /// <summary>
        /// Gets the compiled CSS for the color theme with the specified <paramref name="themeId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for the theme.
        /// </param>
        /// <param name="themeId">
        /// The ID of the theme.
        /// </param>
        /// <param name="type">
        /// The type of theme collection containing the theme.
        /// </param>
        /// <returns>
        /// The compiled CSS for the theme.
        /// </returns>
        public static string GetCssForColorTheme(Guid brokerId, Guid themeId, ThemeCollectionType type)
        {
            if (themeId == SassDefaults.LqbDefaultColorTheme.Id || !ConstStage.AllowCustomizedTpoThemes)
            {
                return SassDefaults.CompiledDefaultTheme;
            }
            
            using (PerformanceMonitorItem.Time(nameof(GetCssForColorTheme) + ":: Theme " + themeId + " Type " + type))
            {
                var themeCollection = GetThemeCollection(brokerId, type);
                var theme = themeCollection.GetTheme(themeId);
                return SassUtilities.CompileColorTheme(theme);
            }
        }

        /// <summary>
        /// Gets the compiled CSS for a page's navigation using the color theme 
        /// with the specified <paramref name="themeId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for the theme.
        /// </param>
        /// <param name="themeId">
        /// The ID of the theme.
        /// </param>
        /// <returns>
        /// The compiled CSS for the navigation.
        /// </returns>
        public static string GetCssForNavigation(Guid brokerId, Guid themeId)
        {
            if (themeId == SassDefaults.LqbDefaultColorTheme.Id || !ConstStage.AllowCustomizedTpoThemes)
            {
                return SassDefaults.CompiledDefaultNavigationCss;
            }

            using (PerformanceMonitorItem.Time(nameof(GetCssForNavigation) + ":: Theme " + themeId))
            {
                var themeCollection = GetThemeCollection(brokerId, ThemeCollectionType.TpoPortal);
                var theme = themeCollection.GetTheme(themeId);
                return SassUtilities.CompileColorThemeNavigation(theme);
            }
        }

        /// <summary>
        /// Gets the theme collection to store in the theme cache.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the collection.
        /// </param>
        /// <param name="type">
        /// The type of the theme collection.
        /// </param>
        /// <returns>
        /// The theme collection for the cache.
        /// </returns>
        private static ThemeCollection GetThemeCollectionForCache(Guid brokerId, ThemeCollectionType type)
        {
            var themeCollection = new ThemeCollection(brokerId, type);

            // If this is a new theme collection, save it to the database
            // to set up the collection ID and prep for adding color themes.
            if (!themeCollection.Retrieve())
            {
                themeCollection.Save();
            }

            return themeCollection;
        }

        /// <summary>
        /// Gets the key used to load and save theme collections.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the collection.
        /// </param>
        /// <param name="type">
        /// The type of the theme collection.
        /// </param>
        /// <returns>
        /// The theme collection key.
        /// </returns>
        private static string GetThemeCollectionKey(Guid brokerId, ThemeCollectionType type) => brokerId.ToString("N") + "_" + type + "_themecollection";
    }
}
