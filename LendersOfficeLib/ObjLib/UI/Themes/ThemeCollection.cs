﻿// <copyright file="ThemeCollection.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 4/21/2017
// </summary>
namespace LendersOffice.ObjLib.UI.Themes
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using CommonProjectLib.Caching;
    using DataAccess;

    /// <summary>
    /// Represents a collection of themes.
    /// </summary>
    public class ThemeCollection : IVersionable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ThemeCollection"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the collection's broker.
        /// </param>
        /// <param name="type">
        /// The type of the collection.
        /// </param>
        /// <remarks>
        /// This constructor is made internal to force external code to use <seealso cref="ThemeManager"/>.
        /// </remarks>
        internal ThemeCollection(Guid brokerId, ThemeCollectionType type)
        {
            this.BrokerId = brokerId;
            this.Type = type;
        }

        /// <summary>
        /// Gets the broker ID for the theme collection.
        /// </summary>
        /// <value>
        /// The broker ID for the theme collection.
        /// </value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the ID for the theme collection.
        /// </summary>
        /// <value>
        /// The ID for the theme collection.
        /// </value>
        public int CollectionId { get; private set; }

        /// <summary>
        /// Gets the type of the theme collection.
        /// </summary>
        /// <value>
        /// The type of the theme collection.
        /// </value>
        public ThemeCollectionType Type { get; private set; }

        /// <summary>
        /// Gets the selected theme for the collection.
        /// </summary>
        /// <value>
        /// The selected theme for the collection.
        /// </value>
        public Guid SelectedThemeId { get; private set; }

        /// <summary>
        /// Gets the version of the theme collection.
        /// </summary>
        /// <value>
        /// The version of the theme collection.
        /// </value>
        public long Version { get; private set; }

        /// <summary>
        /// Gets or sets the color themes for the collection.
        /// </summary>
        /// <value>
        /// The color themes.
        /// </value>
        private Dictionary<Guid, ColorTheme> Themes { get; set; } = new Dictionary<Guid, ColorTheme>();

        /// <summary>
        /// Gets the version of the collection with the specified <paramref name="brokerId"/>
        /// and <paramref name="type"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The broker ID for the collection.
        /// </param>
        /// <param name="type">
        /// The type for the collection.
        /// </param>
        /// <returns>
        /// The version for the collection.
        /// </returns>
        public static long GetVersion(Guid brokerId, ThemeCollectionType type)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ThemeCollectionType", type)
            };

            return Convert.ToInt64(StoredProcedureHelper.ExecuteScalar(brokerId, "ThemeCollection_GetVersion", parameters));
        }

        /// <summary>
        /// Retrieves data for the theme collection.
        /// </summary>
        /// <returns>
        /// True if the retrieval is successful, false otherwise.
        /// </returns>
        public bool Retrieve()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@ThemeCollectionType", this.Type),
            };

            var colorThemesRows = new List<IReadOnlyDictionary<string, object>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "ThemeCollection_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    this.CollectionId = (int)reader["ThemeCollectionId"];
                    this.SelectedThemeId = (Guid)reader["SelectedThemeId"];
                    this.Version = (long)reader["Version"];

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            colorThemesRows.Add(reader.ToDictionary());
                        }
                    }
                }
                else
                {
                    this.SelectedThemeId = SassDefaults.LqbDefaultColorTheme.Id;
                    return false;
                }
            }

            foreach (var themeData in colorThemesRows)
            {
                var newTheme = new ColorTheme(this.BrokerId, themeData);
                this.Themes.Add(newTheme.Id, newTheme);
            }

            return true;
        }

        /// <summary>
        /// Gets a value indicating whether the theme collection has the specified theme.
        /// </summary>
        /// <param name="themeId">
        /// The ID of the theme to check.
        /// </param>
        /// <returns>
        /// True if the collection has the theme or the theme is <seealso cref="SassDefaults.LqbDefaultColorTheme"/>,
        /// false otherwise.
        /// </returns>
        public bool HasTheme(Guid themeId) => themeId == SassDefaults.LqbDefaultColorTheme.Id || this.Themes.ContainsKey(themeId);

        /// <summary>
        /// Gets the themes in this collection.
        /// </summary>
        /// <returns>
        /// The themes in this collection.
        /// </returns>
        public IEnumerable<ColorTheme> GetThemes()
        {
            return this.Themes.Values;
        }

        /// <summary>
        /// Retrieves the theme with the specified <paramref name="themeId"/>.
        /// </summary>
        /// <param name="themeId">
        /// The ID of the theme to retrieve.
        /// </param>
        /// <returns>
        /// The color theme.
        /// </returns>
        public ColorTheme GetTheme(Guid themeId)
        {
            if (themeId == SassDefaults.LqbDefaultColorTheme.Id)
            {
                return SassDefaults.LqbDefaultColorTheme;
            }

            ColorTheme theme = null;
            if (!this.Themes.TryGetValue(themeId, out theme))
            {
                throw new KeyNotFoundException("Could not locate theme " + themeId);
            }

            return theme;
        }

        /// <summary>
        /// Retrieves the theme with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">
        /// The name of the theme to retrieve.
        /// </param>
        /// <returns>
        /// The color theme.
        /// </returns>
        public ColorTheme GetTheme(string name)
        {
            var trimmedName = name?.Trim();

            if (string.IsNullOrEmpty(trimmedName))
            {
                return null;
            }

            if (string.Equals(SassDefaults.LqbDefaultColorTheme.Name, trimmedName, StringComparison.OrdinalIgnoreCase))
            {
                return SassDefaults.LqbDefaultColorTheme;
            }

            return this.Themes.Values.FirstOrDefault(theme => string.Equals(theme.Name, trimmedName, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Adds a theme to the collection.
        /// </summary>
        /// <param name="name">
        /// The name of the new theme to add.
        /// </param>
        /// <returns>
        /// The created color theme.
        /// </returns>
        public ColorTheme AddTheme(string name)
        {
            if (string.Equals(SassDefaults.LqbDefaultColorTheme.Name, name, StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException(SassDefaults.LqbDefaultColorTheme.Name + " is a reserved color theme name.");
            }

            var newTheme = new ColorTheme()
            {
                BrokerId = this.BrokerId,
                Id = Guid.NewGuid(),
                Name = name,
                Variables = new Dictionary<string, string>(SassDefaults.LqbDefaultColorTheme.Variables)
            };

            this.Themes.Add(newTheme.Id, newTheme);
            return newTheme;
        }

        /// <summary>
        /// Updates an existing theme in the collection.
        /// </summary>
        /// <param name="updatedTheme">
        /// The updated theme.
        /// </param>
        public void UpdateTheme(ColorTheme updatedTheme)
        {
            if (updatedTheme.Id == SassDefaults.LqbDefaultColorTheme.Id)
            {
                Tools.LogInfoWithStackTrace("[OPM 365311] Attempt made to edit the LQB default color theme.");
                return;
            }

            if (!this.Themes.ContainsKey(updatedTheme.Id))
            {
                throw new ArgumentException("Cannot update non-existent theme " + updatedTheme.Name);
            }

            this.Themes[updatedTheme.Id] = updatedTheme;
        }

        /// <summary>
        /// Deletes a theme from the collection.
        /// </summary>
        /// <param name="executor">
        /// The stored procedure executor for the transaction.
        /// </param>
        /// <param name="themeId">
        /// The ID of the theme to delete.
        /// </param>
        public void DeleteTheme(CStoredProcedureExec executor, Guid themeId)
        {
            ColorTheme themeToDelete;
            if (themeId == SassDefaults.LqbDefaultColorTheme.Id ||
                !this.Themes.TryGetValue(themeId, out themeToDelete))
            {
                return;
            }

            themeToDelete.Delete(this.CollectionId, executor);

            this.Themes.Remove(themeId);
            if (themeId == this.SelectedThemeId)
            {
                this.SelectedThemeId = SassDefaults.LqbDefaultColorTheme.Id;
            }
        }

        /// <summary>
        /// Set the selected theme for the collection.
        /// </summary>
        /// <param name="selectedThemeId">
        /// The selected theme.
        /// </param>
        public void SetSelectedTheme(Guid selectedThemeId)
        {
            if (!this.HasTheme(selectedThemeId))
            {
                throw new KeyNotFoundException("Cannot set selected theme to nonexistent theme " + selectedThemeId);
            }

            this.SelectedThemeId = selectedThemeId;
        }

        /// <summary>
        /// Saves the theme collection.
        /// </summary>
        public void Save()
        {
            using (var executor = new CStoredProcedureExec(this.BrokerId))
            {
                try
                {
                    executor.BeginTransactionForWrite();

                    var collectionIdParameter = new SqlParameter("@ThemeCollectionId", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    SqlParameter[] parameters = 
                    {
                        new SqlParameter("@BrokerId", this.BrokerId),
                        new SqlParameter("@ThemeCollectionType", this.Type),
                        collectionIdParameter,
                        new SqlParameter("@SelectedThemeId", this.SelectedThemeId),
                        new SqlParameter("@Version", this.Version)
                    };
                    
                    executor.ExecuteNonQuery("ThemeCollection_Save", parameters);

                    var collectionId = (int)collectionIdParameter.Value;

                    foreach (var theme in this.GetThemes())
                    {
                        theme.Save(collectionId, executor);
                    }

                    executor.CommitTransaction();
                }
                catch
                {
                    executor.RollbackTransaction();
                    throw;
                }
            }
        }
    }
}
