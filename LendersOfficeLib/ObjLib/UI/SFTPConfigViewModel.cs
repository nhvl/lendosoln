﻿namespace LendersOffice.ObjLib.UI
{
    using System;

    /// <summary>
    /// SFTP config view model.
    /// </summary>
    public class SFTPConfigViewModel
    {
        /// <summary>
        /// Gets or sets broker id of the settings.
        /// </summary>
        /// <value>Broker Id.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the display name of broker.
        /// </summary>
        /// <value>Display name of broker.</value>
        public string BrokerDisplayName { get; set; }

        /// <summary>
        /// Gets or sets notes of the settings.
        /// </summary>
        /// <value>Notes of the settings.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the SFTP Config is valid.
        /// </summary>
        /// <value>Whether the config is valid.</value>
        public bool IsValid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the export id.
        /// </summary>
        /// <value>The export id.</value>
        public int ExportId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the export name.
        /// </summary>
        /// <value>The export name.</value>
        public string ExportName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the map name.
        /// </summary>
        /// <value>The map name.</value>
        public string MapName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public Guid? UserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user login name.
        /// </summary>
        /// <value>The user login name.</value>
        public string UserLoginName { get; set; }

        /// <summary>
        /// Gets or sets the report id.
        /// </summary>
        /// <value>The report id.</value>
        public Guid? ReportId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the report name.
        /// </summary>
        /// <value>The report name.</value>
        public string ReportName { get; set; }

        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>The protocol.</value>
        public string Protocol
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the host name.
        /// </summary>
        /// <value>The host name.</value>
        public string HostName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>The login.</value>
        public string Login
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the encrypted password.
        /// </summary>
        /// <value>The encrypted password.</value>
        public string EncryptedPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the port number.
        /// </summary>
        /// <value>The port number.</value>
        public int? Port
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SSH Fingerprint.
        /// </summary>
        /// <value>The SSH fingerprint.</value>
        public string SSHFingerprint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the destination path.
        /// </summary>
        /// <value>The destination path.</value>
        public string DestPath
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use the .Net framework.
        /// </summary>
        /// <value>Whether to use the .Net framework.</value>
        public bool Use_DotNet
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an int representing the WinSCP.FtpSecure enum.
        /// </summary>
        /// <value>Int representing the WinSCP.FtpSecure enum.</value>
        public string FtpSecure
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SSL host fingerprint.
        /// </summary>
        /// <value>The SSL Host fingerprint.</value>
        public string SSLHostFingerprint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use passive mode.
        /// </summary>
        /// <value>Whether passive mode is used.</value>
        public bool UsePassive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the additional post url.
        /// </summary>
        /// <value>The additional post url.</value>
        public string AdditionalPostUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets any comments.
        /// </summary>
        /// <value>The comments.</value>
        public string Comments
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets where the email is from.
        /// </summary>
        /// <value>The from section of the email.</value>
        public string From
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets where the email is being sent to.
        /// </summary>
        /// <value>The to section of the email.</value>
        public string To
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the attached file in the email.
        /// </summary>
        /// <value>The name of the file attached in the email.</value>
        public string FileName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the subject of the email.
        /// </summary>
        /// <value>The subject section of the email.</value>
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the message of the email.
        /// </summary>
        /// <value>The message section of the email.</value>
        public string Message
        {
            get;
            set;
        }
    }
}
