﻿// <copyright file="LqbInputModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: huyn
// Date: 1/27/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.UI;

    /// <summary>
    /// The model to detect if a property have a input custom fields.
    /// </summary>
    public class LqbInputModel
    {
        /// <summary>
        /// Gets or sets the input field type. Detect if a property should use a custom input field type.
        /// </summary>
        /// <value>The input field type.</value>
        public InputFieldType Type { get; set; }

        /// <summary>
        /// Gets or sets the input field name.
        /// </summary>
        /// <value>The input field name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this property is invalid to create a input field model.
        /// </summary>
        /// <value>The invalid attribute of the property.</value>
        public bool Invalid { get; set; }

        /// <summary>
        /// Gets or sets the enum type of the input field model. We will use this enum to fetch options for the model.
        /// </summary>
        /// <value>The enum type of the input field model.</value>
        public Type EnumType { get; set; }

        /// <summary>
        /// Gets or sets the maximum length of the value in input field model.
        /// </summary>
        /// <value>The maximum length of the value in input field model.</value>
        public int MaxLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this property is required to input.
        /// </summary>
        /// <value>A value indicating whether this property is required to input.</value>
        public bool Required { get; set; }
    }
}
