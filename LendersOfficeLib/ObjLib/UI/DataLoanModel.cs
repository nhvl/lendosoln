﻿// <copyright file="DataLoanModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Data model for <code>CPageData</code>.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DataLoanModel
    {
        /// <summary>
        /// Gets or sets the version of the data loan object.
        /// </summary>
        /// <value>The version of the data loan object.</value>
        [JsonProperty("version")]
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets the list of id and value of the loan summary.
        /// </summary>
        /// <value>The list of id and value of the loan summary.</value>
        [JsonProperty("summary")]
        public Dictionary<string, string> SummaryValues { get; set; }

        /// <summary>
        /// Gets or sets the list of applicants.
        /// </summary>
        /// <value>The list of applicants name and id.</value>
        [JsonProperty("applicants")]
        public List<Dictionary<string, string>> Applicants { get; set; }

        /// <summary>
        /// Gets or sets of title borrowers.
        /// </summary>
        /// <value>List of other title borrowers.</value>
        [JsonProperty("sTitleBorrowers")]
        public ListModel TitleBorrowers { get; set; }

        /// <summary>
        /// Gets or sets a set of simple fields.
        /// </summary>
        /// <value>Set of simple fields.</value>
        [JsonProperty("fields")]
        public SimpleObjectInputFieldModel Fields { get; set; }

        /// <summary>
        /// Gets or sets a collection of documents.
        /// </summary>
        /// <value>A collection of documents.</value>
        [JsonProperty("sDocCollection")]
        public Dictionary<string, object> Documents { get; set; }

        /// <summary>
        /// Gets or sets data visible broker adjustment.
        /// </summary>
        /// <value>Data visible broker adjustment.</value>
        [JsonProperty("sVisibleBrokerAdjustmentData")]
        public Dictionary<string, object> VisibleBrokerAdjustmentData { get; set; }

        /// <summary>
        /// Gets or sets a collection of preparer.
        /// </summary>
        /// <value>A collection of preparer.</value>
        [JsonProperty("sPreparerOfForm")]
        public Dictionary<string, SimpleObjectInputFieldModel> PreparerOfForm { get; set; }

        /// <summary>
        /// Gets or sets a collection of agent.
        /// </summary>
        /// <value>A collection of agent.</value>
        [JsonProperty("sAgentOfRole")]
        public Dictionary<string, List<Dictionary<string, object>>> AgentOfRole { get; set; }

        /// <summary>
        /// Gets or sets a collection of agent.
        /// </summary>
        /// <value>A collection of agent.</value>
        [JsonProperty("sAgentList")]
        public ListModel AgentList { get; set; }

        /// <summary>
        /// Gets or sets an agent.
        /// </summary>
        /// <value>An agent record.</value>
        [JsonProperty("AgentRecord")]
        public Dictionary<string, object> AgentRecord { get; set; }

        /// <summary>
        /// Gets or sets a collection of applicants. Used in Credit Score page.
        /// </summary>
        /// <value>A collection of applicants.</value>
        [JsonProperty("sApplicantList")]
        public ListModel ApplicantList { get; set; }

        /// <summary>
        /// Gets or sets a collection of agent commission.
        /// </summary>
        /// <value>A collection of agent commission.</value>
        [JsonProperty("AgentCommission")]
        public ListModel AgentCommission { get; set; }

        /// <summary>
        /// Gets or sets a collection of Linked Loan Info.
        /// </summary>
        /// <value>A collection of sLinkedLoanInfo.</value>
        [JsonProperty("sLinkedLoanInfo")]
        public Dictionary<string, object> LinkedLoanInfo { get; set; }

        /// <summary>
        /// Gets or sets a collection of EmployeeLoanAssignment.
        /// </summary>
        /// <value>A collection of EmployeeLoanAssignment.</value>
        [JsonProperty("EmployeeLoanAssignment")]
        public ListModel EmployeeLoanAssignment { get; set; }

        /// <summary>
        /// Gets or sets a collection of pmlBroker.
        /// </summary>
        /// <value>A collection of pmlBroker.</value>
        [JsonProperty("PmlBroker")]
        public Dictionary<string, object> PmlBroker { get; set; }

        /// <summary>
        /// Gets or sets a data model for <code>CAppData</code>.
        /// </summary>
        /// <value>A data model for <code>CAppData</code>.</value>
        [JsonProperty("app")]
        public DataAppModel App { get; set; }

        /// <summary>
        /// Gets or sets of list of sller.
        /// </summary>
        /// <value>List of sellers' info.</value>
        [JsonProperty("sSellerCollection")]
        public ListModel Sellers { get; set; }

        /// <summary>
        /// Gets or sets of list of alternate lenders.
        /// </summary>
        /// <value>List of alternate lenders.</value>
        [JsonProperty("alternateLenders")]
        public ListModel AlternateLenders { get; set; }

        /// <summary>
        /// Gets or sets of list of sller.
        /// </summary>
        /// <value>List of sellers' info.</value>
        [JsonProperty("sHomeownerCounselingOrganizationCollection")]
        public ListModel HomeownerCounselingOrganization { get; set; }

        /// <summary>
        /// Gets or sets of list of preparer fields.
        /// </summary>
        /// <value>List of preparer fields.</value>
        [JsonProperty("preparerFields")]
        public Dictionary<string, Dictionary<string, object>> PreparerFields { get; set; }

        /// <summary>
        /// Gets or sets of information of the broker.
        /// </summary>
        /// <value>List of broker information.</value>
        [JsonProperty("BrokerUser")]
        public Dictionary<string, object> BrokerUser { get; set; }

        /// <summary>
        /// Gets or sets of list of closing cost set.
        /// </summary>
        /// <value>List of closing cost set.</value>
        [JsonProperty("sClosingCosts")]
        public Dictionary<string, object> ClosingCostSet { get; set; }

        /// <summary>
        /// Gets or sets of list of draw schedules in construction loan.
        /// </summary>
        /// <value>List of draw schedules.</value>
        [JsonProperty("sDrawSchedules")]
        public Dictionary<string, object> DrawSchedules { get; set; }

        /// <summary>
        /// Gets or sets of information of the broker DB.
        /// </summary>
        /// <value>List of broker DB information.</value>
        [JsonProperty("BrokerDB")]
        public Dictionary<string, object> BrokerDB { get; set; }

        /// <summary>
        /// Gets or sets of settlement service provider.
        /// </summary>
        /// <value>List of settlement service provider.</value>
        [JsonProperty("SettlementServiceProviders")]
        public Dictionary<string, ListModel> SettlementServiceProviders { get; set; }

        /// <summary>
        /// Gets or sets of list of avaliable settlement service providers grouped by fee type.
        /// </summary>
        /// <value>List of service providers grouped by fee type.</value>
        [JsonProperty("AvailableSettlementServiceProviderGroups")]
        public IEnumerable<Dictionary<string, object>> AvailableSettlementServiceProviderGroups { get; set; }

        /// <summary>
        /// Gets or sets of settlement service provider.
        /// </summary>
        /// <value>List of settlement service provider.</value>
        [JsonProperty("AssignedContactsFromFees")]
        public IEnumerable<Dictionary<string, object>> AssignedContactsFromFees { get; set; }

        /// <summary>
        /// Gets or sets a matrix of initial escrow.
        /// </summary>
        /// <value>A matrix of initial escrow.</value>
        [JsonProperty("InitialEscrowAcc")]
        public InputFieldModel[,] InitialEscrowAcc { get; set; }

        /// <summary>
        /// Gets or sets value of aggregate escrow account.
        /// </summary>
        /// <value>Value of aggregate escrow account.</value>
        [JsonProperty("AggregateEscrowAccount")]
        public Dictionary<string, object> AggregateEscrowAccount { get; set; }

        /// <summary>
        /// Gets or sets of ClosingDisclosureDates.
        /// </summary>
        /// <value>List of ClosingDisclosureDates.</value>
        [JsonProperty("sClosingDisclosureDatesInfo")]
        public ListModel ClosingDisclosureDatesInfo { get; set; }

        /// <summary>
        /// Gets or sets of LoanEstimateDates.
        /// </summary>
        /// <value>List of LoanEstimateDates.</value>
        [JsonProperty("sLoanEstimateDatesInfo")]
        public ListModel LoanEstimateDatesInfo { get; set; }

        /// <summary>
        /// Gets or sets a list of fee changes.
        /// </summary>
        /// <value>List of fee changes.</value>
        [JsonProperty("FeeChanges")]
        public IEnumerable<Dictionary<string, object>> FeeChanges { get; set; }

        /// <summary>
        /// Gets or sets a list of archived changes.
        /// </summary>
        /// <value>List of archived changes.</value>
        [JsonProperty("SelectedCocArchive")]
        public Dictionary<string, object> SelectedCocArchive { get; set; }

        /// <summary>
        /// Gets or sets a list of amortization data.
        /// </summary>
        /// <value>List of archived changes.</value>
        [JsonProperty("Amortization")]
        public Dictionary<string, object> Amortization { get; set; }

        /// <summary>
        /// Add simple field to data loan model. This method will handle when Fields property is null.
        /// </summary>
        /// <param name="fieldId">Field id for data loan.</param>
        /// <param name="fieldModel">Field model.</param>
        public void AddField(string fieldId, InputFieldModel fieldModel)
        {
            if (this.Fields == null)
            {
                this.Fields = new SimpleObjectInputFieldModel();
            }

            this.Fields.Add(fieldId, fieldModel);
        }

        /// <summary>
        /// Add simple field to data app model. This method will handle when App property is null.
        /// </summary>
        /// <param name="fieldId">Field id for data app.</param>
        /// <param name="fieldModel">Field model.</param>
        public void AddAppField(string fieldId, InputFieldModel fieldModel)
        {
            if (this.App == null)
            {
                this.App = new DataAppModel();
            }

            if (this.App.Fields == null)
            {
                this.App.Fields = new SimpleObjectInputFieldModel();
            }

            this.App.Fields[fieldId] = fieldModel;
        }

        /// <summary>
        /// Set the summary value of this loan.
        /// </summary>
        /// <param name="fieldId">Field id for loan summary.</param>
        /// <param name="value">Loan summary value.</param>
        public void AddSummary(string fieldId, string value)
        {
            if (this.SummaryValues == null)
            {
                this.SummaryValues = new Dictionary<string, string>();
            }

            this.SummaryValues[fieldId] = value;
        }

        /// <summary>
        /// Add the applicant name to the summary section.
        /// </summary>
        /// <param name="applicantId">Id of applicant.</param>
        /// <param name="borrName">Name of borrower.</param>
        /// <param name="coborrName">Name of coborrower.</param>
        /// <param name="isPrimary">Determine if this applicant is the primary one.</param>
        public void AddApplicantInfo(Guid applicantId, string borrName, string coborrName, bool isPrimary)
        {
            if (this.Applicants == null)
            {
                this.Applicants = new List<Dictionary<string, string>>();
            }

            var applicantDict = new Dictionary<string, string>();
            applicantDict.Add("borrName", borrName.ToString());
            applicantDict.Add("isPrimary", isPrimary.ToString());
            applicantDict.Add("appId", applicantId.ToString());
            if (!string.IsNullOrEmpty(coborrName))
            {
                applicantDict.Add("coBorrName", coborrName.ToString());
            }

            this.Applicants.Add(applicantDict);
        }

        /// <summary>
        /// Add the service providers Model to the SettlementServiceProviders section.
        /// </summary>
        /// <param name="name">Name of the providers.</param>
        /// <param name="serviceProvidersModel">The model of the providers.</param>
        public void AddSettlementServiceProviders(string name, ListModel serviceProvidersModel)
        {
            if (this.SettlementServiceProviders == null)
            {
                this.SettlementServiceProviders = new Dictionary<string, ListModel>();
            }

            this.SettlementServiceProviders.Add(name, serviceProvidersModel);
        }
    }
}