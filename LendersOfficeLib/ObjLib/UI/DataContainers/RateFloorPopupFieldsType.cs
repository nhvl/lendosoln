﻿namespace LendersOffice.UI.DataContainers
{
    /// <summary>
    /// Enum used to tell us which set of Rate Floor Fields we are using.
    /// </summary>
    public enum RateFloorPopupFieldsType
    {
        /// <summary>
        /// For the regular Rate Floor fields on the loan.
        /// </summary>
        Regular = 0,

        /// <summary>
        /// For the Construction Loan Rate Floor fields.
        /// </summary>
        Construction = 1
    }
}
