﻿namespace LendersOffice.UI.DataContainers
{
    using DataAccess;

    /// <summary>
    /// Container for the Declaration Explanation.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Mimic loan property names.")]
    public class DeclarationExplanationDataContainer
    {
        /// <summary>
        /// Gets or sets a value indicating the aBNm field.
        /// </summary>
        /// <value>The name of the borrower.</value>
        public string aBNm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCNm field.
        /// </summary>
        /// <value>The name of the borrower.</value>
        public string aCNm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBSsn field.
        /// </summary>
        /// <value>The social security number of the borrower.</value>
        public string aBSsn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCSsn field.
        /// </summary>
        /// <value>The social security number of the borrower.</value>
        public string aCSsn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecJudgmentExplanation field.
        /// </summary>
        /// <value>The explanation for judgment declaration for the borrower.</value>
        public string aBDecJudgmentExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecJudgmentExplanation field.
        /// </summary>
        /// <value>The explanation for judgment declaration for the coborrower.</value>
        public string aCDecJudgmentExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecBankruptExplanation field.
        /// </summary>
        /// <value>The explanation for bankrupt declaration for the borrower.</value>
        public string aBDecBankruptExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecBankruptExplanation field.
        /// </summary>
        /// <value>The explanation for bankrupt declaration for the coborrower.</value>
        public string aCDecBankruptExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecForeclosureExplanation field.
        /// </summary>
        /// <value>The explanation for foreclosure declaration for the borrower.</value>
        public string aBDecForeclosureExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecForeclosureExplanation field.
        /// </summary>
        /// <value>The explanation for foreclosure declaration for the coborrower.</value>
        public string aCDecForeclosureExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecLawsuitExplanation field.
        /// </summary>
        /// <value>The explanation for lawsuit declaration for the borrower.</value>
        public string aBDecLawsuitExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecLawsuitExplanation field.
        /// </summary>
        /// <value>The explanation for lawsuit declaration for the coborrower.</value>
        public string aCDecLawsuitExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecObligatedExplanation field.
        /// </summary>
        /// <value>The explanation for obligated declaration for the borrower.</value>
        public string aBDecObligatedExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecObligatedExplanation field.
        /// </summary>
        /// <value>The explanation for obligated declaration for the coborrower.</value>
        public string aCDecObligatedExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecDelinquentExplanation field.
        /// </summary>
        /// <value>The explanation for deliquent declaration for the borrower.</value>
        public string aBDecDelinquentExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecDelinquentExplanation field.
        /// </summary>
        /// <value>The explanation for deliquent declaration for the coborrower.</value>
        public string aCDecDelinquentExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecAlimonyExplanation field.
        /// </summary>
        /// <value>The explanation for alimony declaration for the borrower.</value>
        public string aBDecAlimonyExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecAlimonyExplanation field.
        /// </summary>
        /// <value>The explanation for alimony declaration for the coborrower.</value>
        public string aCDecAlimonyExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecBorrowingExplanation field.
        /// </summary>
        /// <value>The explanation for borrowing declaration for the borrower.</value>
        public string aBDecBorrowingExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecBorrowingExplanation field.
        /// </summary>
        /// <value>The explanation for borrowing declaration for the coborrower.</value>
        public string aCDecBorrowingExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aBDecBorrowingExplanation field.
        /// </summary>
        /// <value>The explanation for endorser declaration for the borrower.</value>
        public string aBDecEndorserExplanation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating the aCDecBorrowingExplanation field.
        /// </summary>
        /// <value>The explanation for endorser declaration for the coborrower.</value>
        public string aCDecEndorserExplanation
        {
            get;
            set;
        }
    }
}
