﻿namespace LendersOffice.UI.DataContainers
{
    using DataAccess;

    /// <summary>
    /// Container class to hold field values for the RateFloorPopup.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Mimic loan property names.")]
    public class RateFloorPopupFields
    {
        /// <summary>
        /// Gets or sets an enum indicating which set of Rate Floor Fields used.
        /// </summary>
        public RateFloorPopupFieldsType FieldsType { get; set; } = RateFloorPopupFieldsType.Regular;

        /// <summary>
        /// Gets or sets the string value for sRAdjLifeCapR.
        /// </summary>
        /// <value>String value for sRadjLifeCapR.</value>
        public string sRAdjLifeCapR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sRAdjFloorR.
        /// </summary>
        /// <value>The string value for sRAdjFloorR.</value>
        public string sRAdjFloorR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sNoteIR.
        /// </summary>
        /// <value>The string value for sNoteIR.</value>
        public string sNoteIR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for sRAdjFloorCalcT.
        /// </summary>
        /// <value>The value for sRAdjFloorCalcT.</value>
        public RateAdjFloorCalcT sRAdjFloorCalcT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the label based on sRAdjFloorCalcT.
        /// </summary>
        /// <value>The label based on sRAdjFloorCalcT.</value>
        public string sRAdjFloorCalcTLabel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sRAdjFloorBaseR.
        /// </summary>
        /// <value>The value for sRAdjFloorBaseR.</value>
        public string sRAdjFloorBaseR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sRAdjFloorAddR.
        /// </summary>
        /// <value>The string value for sRadjFloorAddR.</value>
        public string sRAdjFloorAddR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sRAdjFloorTotalR.
        /// </summary>
        /// <value>The string value for sRAdjFloorTotalR.</value>
        public string sRAdjFloorTotalR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sRAdjFloorLifeCapR.
        /// </summary>
        /// <value>The string value for sRAdjFloorLifeCapR.</value>
        public string sRAdjFloorLifeCapR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string value for sIsRAdjFloorRReadOnly.
        /// </summary>
        /// <value>The string value for sIsRAdjFloorRReadOnly.</value>
        public string sIsRAdjFloorRReadOnly
        {
            get;
            set;
        }
    }
}
