﻿namespace LendersOffice.ObjLib.UI.DataContainers
{
    using System;

    /// <summary>
    /// Class to hold property info details.
    /// </summary>
    public class PropertyInfoDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyInfoDetails"/> class.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="borrowerName">The borrower name.</param>
        /// <param name="borrowerEmail">The borrower email.</param>
        /// <param name="address">The street address.</param>
        /// <param name="city">The city of the property.</param>
        /// <param name="state">The state of the property.</param>
        /// <param name="zip">The zip code.</param>
        public PropertyInfoDetails(Guid? appId, string borrowerName, string borrowerEmail, string address, string city, string state, string zip)
        {
            this.AppId = appId;
            this.BorrowerName = borrowerName;
            this.BorrowerEmail = borrowerEmail;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zip;
        }

        /// <summary>
        /// Gets or sets the borrower name.
        /// </summary>
        public string BorrowerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the borrower email.
        /// </summary>
        public string BorrowerEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the app id.
        /// </summary>
        public Guid? AppId
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        public string Address
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string Zip
        {
            get; set;
        }
    }
}
