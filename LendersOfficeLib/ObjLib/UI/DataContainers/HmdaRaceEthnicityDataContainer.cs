﻿namespace LendersOffice.UI.DataContainers
{
    using DataAccess;

    /// <summary>
    /// Container for the Hmda Race and Ethnicity control.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Mimic loan property names.")]
    public class HmdaRaceEthnicityDataContainer
    {
        /// <summary>
        /// Gets or sets the borrower interview method.
        /// </summary>
        /// <value>The borrower interview method.</value>
        public E_aIntrvwrMethodT aBInterviewMethodT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the coborrower interview method.
        /// </summary>
        /// <value>The coborrower interview method.</value>
        public E_aIntrvwrMethodT aCInterviewMethodT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets whether the borrower is hispanic or not.
        /// </summary>
        /// <value>Whether the borrower is hispanic or not.</value>
        public E_aHispanicT aBHispanicT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the fallback value for aBHispanicT.
        /// </summary>
        /// <value>The fallback value for aBHispanicT.</value>
        public E_aHispanicT aBHispanictFallback
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets whether the coborrower is hispanic or not.
        /// </summary>
        /// <value>Whether the coborrower is hispanic or not.</value>
        public E_aHispanicT aCHispanicT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the fallback value for aCHispanicT.
        /// </summary>
        /// <value>The fallback value for aCHispanicT.</value>
        public E_aHispanicT aCHispanicTFallback
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Mexican.
        /// </summary>
        /// <value>Whether the borrower is Mexican.</value>
        public bool aBIsMexican
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Mexican.
        /// </summary>
        /// <value>Whether the coborrower is Mexican.</value>
        public bool aCIsMexican
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Puerto Rican.
        /// </summary>
        /// <value>Whether the borrower is Puerto Rican.</value>
        public bool aBIsPuertoRican
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Pureto Rican.
        /// </summary>
        /// <value>Whether the coborrower is Puerto Rican.</value>
        public bool aCIsPuertoRican
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Cuban.
        /// </summary>
        /// <value>Whether the borrower is Cuban.</value>
        public bool aBIsCuban
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Cuban.
        /// </summary>
        /// <value>Whether the coborrower is Cuban.</value>
        public bool aCIsCuban
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is some other Hispanic or Latino.
        /// </summary>
        /// <value>Whether the borrower is some other Hispanic or Latino.</value>
        public bool aBIsOtherHispanicOrLatino
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is some other Hispanic or Latino.
        /// </summary>
        /// <value>Whether the coborrower is some other Hispanic or Latino.</value>
        public bool aCIsOtherHispanicOrLatino
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if marked as other Hispanic or Latino.
        /// </summary>
        /// <value>The description if marked as other Hispanic or Latino.</value>
        public string aBOtherHispanicOrLatinoDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if coborrower marked as other Hispanic or Latino.
        /// </summary>
        /// <value>The description if coborrower marked as other Hispanic or Latino.</value>
        public string aCOtherHispanicOrLatinoDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower does not wish to provide ethnicity.
        /// </summary>
        /// <value>Whether the borrower does not wish to provide ethnicity.</value>
        public bool aBDoesNotWishToProvideEthnicity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower does not wish to provide ethnicity.
        /// </summary>
        /// <value>Whether the coborrower does not wish to provide ethnicity.</value>
        public bool aCDoesNotWishToProvideEthnicity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is American Indian.
        /// </summary>
        /// <value>Whether the borrower is American Indian.</value>
        public bool aBIsAmericanIndian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is American Indian.
        /// </summary>
        /// <value>Whether the coborrower is American Indian.</value>
        public bool aCIsAmericanIndian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if borrower marked as American Indian.
        /// </summary>
        /// <value>Description if borrower marked as American Indian.</value>
        public string aBOtherAmericanIndianDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if coborrower marked as American Indian.
        /// </summary>
        /// <value>Description if coborrower marked as American Indian.</value>
        public string aCOtherAmericanIndianDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Asian.
        /// </summary>
        /// <value>Whether the borrower is Asian.</value>
        public bool aBIsAsian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Asian.
        /// </summary>
        /// <value>Whether the coborrower is Asian.</value>
        public bool aCIsAsian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Asian Indian.
        /// </summary>
        /// <value>Whether the borrower is Asian Indian.</value>
        public bool aBIsAsianIndian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Asian Indian.
        /// </summary>
        /// <value>Whether the coborrower is Asian Indian.</value>
        public bool aCIsAsianIndian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Chinese.
        /// </summary>
        /// <value>Whether the borrower is Chinese.</value>
        public bool aBIsChinese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Chinese.
        /// </summary>
        /// <value>Whether the coborrower is Chinese.</value>
        public bool aCIsChinese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Filipino.
        /// </summary>
        /// <value>Whether the borrower is Filipino.</value>
        public bool aBIsFilipino
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Filipino.
        /// </summary>
        /// <value>Whether the coborrower is Filipino.</value>
        public bool aCIsFilipino
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Japanese.
        /// </summary>
        /// <value>Whether the borrower is Japanese.</value>
        public bool aBIsJapanese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Japanese.
        /// </summary>
        /// <value>Whether the borrower is Japanese.</value>
        public bool aCIsJapanese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Korean.
        /// </summary>
        /// <value>Whether the borrower is Korean.</value>
        public bool aBIsKorean
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Korean.
        /// </summary>
        /// <value>Whether the coborrower is Korean.</value>
        public bool aCIsKorean
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Vietnamese.
        /// </summary>
        /// <value>Whether the borrower is Vietnamese.</value>
        public bool aBIsVietnamese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Vietnamese.
        /// </summary>
        /// <value>Whether the coborrower is Vietnamese.</value>
        public bool aCIsVietnamese
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is other Asian.
        /// </summary>
        /// <value>Whether the borrower is other Asian.</value>
        public bool aBIsOtherAsian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is other Asian.
        /// </summary>
        /// <value>Whether the coborrower is other Asian.</value>
        public bool aCIsOtherAsian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if the borrower is marked as other Asian.
        /// </summary>
        /// <value>The description of the borrower is marked as other Asian.</value>
        public string aBOtherAsianDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if the coborrower is marked as other Asian.
        /// </summary>
        /// <value>The description if the coborrower is marked as other Asian.</value>
        public string aCOtherAsianDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Black or African American.
        /// </summary>
        /// <value>Whether the borrower is Black or African American.</value>
        public bool aBIsBlack
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Black or African American.
        /// </summary>
        /// <value>Whether the coborrower is Black or African American.</value>
        public bool aCIsBlack
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is a Pacific Islander.
        /// </summary>
        /// <value>Whether the borrower is a Pacific Islander.</value>
        public bool aBIsPacificIslander
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is a Pacific Islander.
        /// </summary>
        /// <value>Whether the coborrower is a Pacific Islander.</value>
        public bool aCIsPacificIslander
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is a Native Hawaiian.
        /// </summary>
        /// <value>Whether the borrower is a Native Hawaiian.</value>
        public bool aBIsNativeHawaiian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is a Native Hawaiian.
        /// </summary>
        /// <value>Whether the coborrower is a Native Hawaiian.</value>
        public bool aCIsNativeHawaiian
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Gumanian or Chamorro.
        /// </summary>
        /// <value>Whether the borrower is Guamanian or Chamorro.</value>
        public bool aBIsGuamanianOrChamorro
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Guamanian or Chamorro.
        /// </summary>
        /// <value>Whether the coborrower is Guamanian or Chamorro.</value>
        public bool aCIsGuamanianOrChamorro
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is Samoan.
        /// </summary>
        /// <value>Whether the borrower is Samoan.</value>
        public bool aBIsSamoan
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is Samoan.
        /// </summary>
        /// <value>Whether the coborrower is Samoan.</value>
        public bool aCIsSamoan
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is other Pacific Islander.
        /// </summary>
        /// <value>Whether the borrower is other Pacific Islander.</value>
        public bool aBIsOtherPacificIslander
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is other Pacific Islander.
        /// </summary>
        /// <value>Whether the coborrower is other Pacific Islander.</value>
        public bool aCIsOtherPacificIslander
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if the borrower is marked as other Pacific Islander.
        /// </summary>
        /// <value>Description if the borrower is marked as other Pacific Islander.</value>
        public string aBOtherPacificIslanderDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description if the coborrower is marked as other Pacific Islander.
        /// </summary>
        /// <value>Description if the coborrower is marked as other Pacific Islander.</value>
        public string aCOtherPacificIslanderDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is White.
        /// </summary>
        /// <value>Whether the borrower is White.</value>
        public bool aBIsWhite
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower is White.
        /// </summary>
        /// <value>Whether the coborrower is White.</value>
        public bool aCIsWhite
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower does not wish to provide race.
        /// </summary>
        /// <value>Whether the borrower does not wish to provide race.</value>
        public bool aBDoesNotWishToProvideRace
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower does not wish to provide race.
        /// </summary>
        /// <value>Whether the coborrower does not wish to provide race.</value>
        public bool aCDoesNotWishToProvideRace
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the borrower gender.
        /// </summary>
        /// <value>The borrower gender.</value>
        public E_GenderT aBGender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the fallback for aBGender.
        /// </summary>
        /// <value>The fallback for aBGender.</value>
        public E_GenderT aBGenderFallback
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the coborrower gender.
        /// </summary>
        /// <value>The coborrower gender.</value>
        public E_GenderT aCGender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the fallback for aCGender.
        /// </summary>
        /// <value>The fallback for aCGender.</value>
        public E_GenderT aCGenderFallback
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the ethnicity was collected by observiation.
        /// </summary>
        /// <value>If the borrower ethnicity was colledcted by observation.</value>
        public E_TriState aBEthnicityCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the coborrower ethnicity was collected by observiation.
        /// </summary>
        /// <value>If the coborrower ethnicity was colledcted by observation.</value>
        public E_TriState aCEthnicityCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the borrower gender was collected by observiation.
        /// </summary>
        /// <value>If the borrower gender was colledcted by observation.</value>
        public E_TriState aBSexCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the coborrower gender was collected by observiation.
        /// </summary>
        /// <value>If the coborrower gender was colledcted by observation.</value>
        public E_TriState aCSexCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the borrower race was collected by observiation.
        /// </summary>
        /// <value>If the borrower race was colledcted by observation.</value>
        public E_TriState aBRaceCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets if the coborrower race was collected by observiation.
        /// </summary>
        /// <value>If the coborrower race was colledcted by observation.</value>
        public E_TriState aCRaceCollectedByObservationOrSurname
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether borrower info is furnished.
        /// </summary>
        /// <value>Whether borrower info is furnished.</value>
        public bool aBNoFurnish
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the aBNoFurnish field is locked.
        /// </summary>
        /// <value>Whether the aBNoFurnish is locked.</value>
        public bool aBNoFurnishLckd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coborrower info is furnished.
        /// </summary>
        /// <value>Whether the coborrower info is furnished.</value>
        public bool aCNoFurnish
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the aCNoFurnish field is locked.
        /// </summary>
        /// <value>Whether the aCNoFurnish field is locked.</value>
        public bool aCNoFurnishLckd
        {
            get;
            set;
        }
    }
}
