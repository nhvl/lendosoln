﻿namespace LendersOffice.ObjLib.UI.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides a set of constants for the workflow page display library.
    /// </summary>
    public class WorkflowPageDisplayConstants
    {
        /// <summary>
        /// Gets the marker for the model indicating the entire
        /// collection is being selected.
        /// </summary>
        /// <value>
        /// The marker indicating the entire collection.
        /// </value>
        public string WholeCollectionMarker => DataPath.WholeCollectionIdentifier;

        /// <summary>
        /// Gets the marker for the model indicating a subset
        /// of a collection is being selected.
        /// </summary>
        /// <value>
        /// The marker indicating a subset of the collection.
        /// </value>
        public string CollectionSubsetMarker => DataPath.SubsetMarker;

        /// <summary>
        /// Gets the mapping between path type and path data for the page.
        /// </summary>
        /// <remarks>
        /// This is currently implemented as a one-to-one mapping
        /// between type name keys and type name values, but may
        /// change in the future if the framework needs more data
        /// for all types or on a per-type basis.
        /// </remarks>
        /// <value>
        /// The path type dictionary.
        /// </value>
        public Dictionary<string, string> PathTypes => new[]
        {
            typeof(DataPathBasicElement),
            typeof(DataPathSelectionElement),
            typeof(DataPathCollectionElement)
        }.ToDictionary(type => type.Name, type => type.Name);
    }
}
