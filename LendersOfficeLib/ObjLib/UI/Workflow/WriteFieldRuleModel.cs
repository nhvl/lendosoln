﻿namespace LendersOffice.ObjLib.UI.Workflow
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a model for a write field rule.
    /// </summary>
    public class WriteFieldRuleModel
    {
        /// <summary>
        /// Gets or sets the ID of the write field rule.
        /// </summary>
        /// <remarks>
        /// This property is populated when the write field rule 
        /// is using a specific field ID. When the write field rule
        /// is using a data path, this property will be null and
        /// <see cref="Paths"/> will be populated.
        /// </remarks>
        /// <value>
        /// The ID of the write field rule.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the list of path models for the write field rule.
        /// </summary>
        /// <value>
        /// The path for the write field rule.
        /// </value>
        public List<WorkflowDataPathModel> Paths { get; set; }

        /// <summary>
        /// Gets or sets the only values that the field can be set to.
        /// </summary>
        /// <value>
        /// The list of accepted values.
        /// </value>
        public HashSet<string> AcceptedValues { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field can be cleared.
        /// </summary>
        /// <value>
        /// True if the field can be cleared, false otherwise.
        /// </value>
        public bool CanClear { get; set; }

        /// <summary>
        /// Gets or sets the title for the rule, i.e. the denial message.
        /// </summary>
        /// <value>
        /// The title for the rule.
        /// </value>
        public string Title { get; set; }
    }
}
