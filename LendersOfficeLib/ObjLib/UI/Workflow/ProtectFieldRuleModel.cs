﻿namespace LendersOffice.ObjLib.UI.Workflow
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a model for a protect field rule.
    /// </summary>
    public class ProtectFieldRuleModel
    {
        /// <summary>
        /// Gets or sets the ID of the protect field rule.
        /// </summary>
        /// <remarks>
        /// This property is populated when the protect field rule 
        /// is using a specific field ID. When the protect field rule
        /// is using a data path, this property will be null and
        /// <see cref="Paths"/> will be populated.
        /// </remarks>
        /// <value>
        /// The ID for the model.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the list of path models for the protect field rule.
        /// </summary>
        /// <value>
        /// The paths for the model.
        /// </value>
        public List<WorkflowDataPathModel> Paths { get; set; }

        /// <summary>
        /// Gets or sets the values that the field cannot be set to.
        /// </summary>
        /// <value>
        /// The list of values that cannot be set.
        /// </value>
        public HashSet<string> NotSettableValues { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field can be updated.
        /// </summary>
        /// <value>
        /// True if the field can be updated, false otherwise.
        /// </value>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// Gets or sets the title for the rule, i.e. the denial message.
        /// </summary>
        /// <value>
        /// The title for the rule.
        /// </value>
        public string Title { get; set; }
    }
}
