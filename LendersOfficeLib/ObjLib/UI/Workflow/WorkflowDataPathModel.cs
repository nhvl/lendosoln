﻿namespace LendersOffice.ObjLib.UI.Workflow
{
    /// <summary>
    /// Provides a model for a <see cref="LqbGrammar.DataTypes.PathDispatch.DataPath"/>.
    /// </summary>
    public class WorkflowDataPathModel
    {
        /// <summary>
        /// Gets or sets the name of the path element.
        /// </summary>
        /// <value>
        /// The name of the path element.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the path element type.
        /// </summary>
        /// <value>
        /// The type of the path element.
        /// </value>
        public string Type { get; set; }
    }
}