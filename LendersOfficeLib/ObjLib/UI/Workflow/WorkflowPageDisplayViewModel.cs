namespace LendersOffice.ObjLib.UI.Workflow
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a model for page display
    /// generated via workflow rules.
    /// </summary>
    /// <remarks>
    /// Keep the name of the registered model
    /// object in sync with the WorkflowPageDisplay
    /// file for the application.
    /// </remarks>
    public class WorkflowPageDisplayViewModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether the user has full
        /// write access to the loan file.
        /// </summary>
        /// <value>
        /// True if the user has full write access to the loan,
        /// false otherwise.
        /// </value>
        public bool HasFullWriteAccess { get; set; }

        /// <summary>
        /// Gets or sets the field protect rules for the model.
        /// </summary>
        /// <value>
        /// A list of complex types for the field protect rules.
        /// </value>
        public List<ProtectFieldRuleModel> FieldProtectRules { get; set; }

        /// <summary>
        /// Gets or sets the write field rules for the model.
        /// </summary>
        /// <value>
        /// A list of complex types for the write field rules.
        /// </value>
        public List<WriteFieldRuleModel> WriteFieldRules { get; set; }
    }
}