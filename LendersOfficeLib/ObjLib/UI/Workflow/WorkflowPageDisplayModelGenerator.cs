namespace LendersOffice.ObjLib.UI.Workflow
{
    using System.Collections.Generic;
    using DataAccess;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides a means for generating page display models
    /// based on workflow.
    /// </summary>
    public class WorkflowPageDisplayModelGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowPageDisplayModelGenerator"/> class.
        /// </summary>
        /// <param name="loanFileRules">
        /// The loan file field rule repository to load field protect and field write rules.
        /// </param>
        public WorkflowPageDisplayModelGenerator(ILoanFileRuleRepository loanFileRules)
        {
            this.HasFullWriteAccess = loanFileRules.HasFullWrite;
            this.FieldProtectRules = loanFileRules.GetProtectFieldRules();
            this.WriteFieldRules = loanFileRules.GetWriteFieldRules();
        }

        /// <summary>
        /// Gets a value indicating whether the user has full
        /// write access to the loan file.
        /// </summary>
        /// <value>
        /// True if the user has full write access to the loan,
        /// false otherwise.
        /// </value>
        private bool HasFullWriteAccess { get; }

        /// <summary>
        /// Gets or sets the field protection rules for the model.
        /// </summary>
        /// <value>
        /// The field protection rules for the model.
        /// </value>
        private IEnumerable<ProtectFieldRule> FieldProtectRules { get; set; }

        /// <summary>
        /// Gets or sets the field write rules for the model.
        /// </summary>
        /// <value>
        /// The field write rules for the model.
        /// </value>
        private IEnumerable<WriteFieldRule> WriteFieldRules { get; set; }

        /// <summary>
        /// Generates the model.
        /// </summary>
        /// <returns>
        /// The generated model.
        /// </returns>
        public WorkflowPageDisplayViewModel Generate()
        {
            return new WorkflowPageDisplayViewModel()
            {
                HasFullWriteAccess = this.HasFullWriteAccess,
                FieldProtectRules = this.GenerateFieldProtectRules(),
                WriteFieldRules = this.GenerateWriteFieldRules()
            };
        }

        /// <summary>
        /// Generates field protect rules for the model.
        /// </summary>
        /// <returns>
        /// A list of models for the field protect rules.
        /// </returns>
        private List<ProtectFieldRuleModel> GenerateFieldProtectRules()
        {
            var list = new List<ProtectFieldRuleModel>();
            foreach (var rule in this.FieldProtectRules)
            {
                var model = new ProtectFieldRuleModel()
                {
                    NotSettableValues = rule.SpecificValues,
                    CanUpdate = rule.CanBeUpdated,
                    Title = string.Join("\n", rule.FailureMessages)
                };

                var path = this.GetFieldPathForRuleId(rule.FieldId);
                if (path == null)
                {
                    model.Id = rule.FieldId;
                }
                else
                {
                    model.Paths = path;
                }

                list.Add(model);
            }

            return list;
        }

        /// <summary>
        /// Generates write field rules for the model.
        /// </summary>
        /// <returns>
        /// A list of models for the write field rules.
        /// </returns>
        private List<WriteFieldRuleModel> GenerateWriteFieldRules()
        {
            var list = new List<WriteFieldRuleModel>();

            foreach (var rule in this.WriteFieldRules)
            {
                var model = new WriteFieldRuleModel()
                {
                    AcceptedValues = rule.SpecificValues,
                    CanClear = rule.CanBeCleared,
                    Title = string.Join("\n", rule.FailureMessages)
                };

                var path = this.GetFieldPathForRuleId(rule.FieldId);
                if (path == null)
                {
                    model.Id = rule.FieldId;
                }
                else
                {
                    model.Paths = path;
                }

                list.Add(model);
            }

            return list;
        }

        /// <summary>
        /// Gets the list of data path models for the rule ID.
        /// </summary>
        /// <param name="fieldId">
        /// The field ID for the rule.
        /// </param>
        /// <returns>
        /// The list of field path models if the field is a data path, null otherwise.
        /// </returns>
        private List<WorkflowDataPathModel> GetFieldPathForRuleId(string fieldId)
        {
            if (fieldId.StartsWith(DataPath.PathHeader, System.StringComparison.OrdinalIgnoreCase))
            {
                var path = DataPath.Create(fieldId);
                return this.GetDataPathIdModel(path);
            }

            if (fieldId.StartsWith("coll|", System.StringComparison.OrdinalIgnoreCase))
            {
                var path = Tools.GenerateDataPathFromCollRule(fieldId);
                return this.GetDataPathIdModel(path);
            }

            return null;
        }

        /// <summary>
        /// Gets the list of path models for a data path.
        /// </summary>
        /// <param name="dataPath">
        /// The data path.
        /// </param>
        /// <returns>
        /// A list of models for the data path.
        /// </returns>
        private List<WorkflowDataPathModel> GetDataPathIdModel(DataPath dataPath)
        {
            var modelList = new List<WorkflowDataPathModel>(dataPath.PathList.Count);

            foreach (var path in dataPath.PathList)
            {
                modelList.Add(new WorkflowDataPathModel()
                {
                    Name = path.Name,
                    Type = path.GetType().Name
                });
            }

            return modelList;
        }
    }
}