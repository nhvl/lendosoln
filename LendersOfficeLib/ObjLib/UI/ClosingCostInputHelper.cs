﻿// <copyright file="ClosingCostInputHelper.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   7/9/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Use to serialize and deserialize Closing cost set to input modal.
    /// </summary>
    public static class ClosingCostInputHelper
    {
        /// <summary>
        /// Store the guid id of default fee.
        /// </summary>
        private static ReadOnlyDictionary<string, Guid> defaultFees;

        /// <summary>
        /// Initializes static members of the <see cref="ClosingCostInputHelper" /> class.
        /// </summary>
        static ClosingCostInputHelper()
        {
            if (ClosingCostInputHelper.defaultFees == null)
            {
                var newDefaultFee = new Dictionary<string, Guid>();

                foreach (var field in typeof(DefaultSystemClosingCostFee).GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    var val = field.GetValue(null);
                    if (val != null && val is Guid)
                    {
                        newDefaultFee.Add(field.Name, (Guid)val);
                    }
                }

                ClosingCostInputHelper.defaultFees = new ReadOnlyDictionary<string, Guid>(newDefaultFee);
            }
        }

        /// <summary>
        /// Return the json of closing cost of a loan.
        /// </summary>
        /// <param name="borrowerClosingCostSet">Borrower closing cost set to be serialize.</param>
        /// <param name="principal">Pricipal of authenitcated session.</param>
        /// <returns>An dictionary of closing cost of a loan.</returns>
        public static Dictionary<string, object> ToInputModel(BorrowerClosingCostSet borrowerClosingCostSet, AbstractUserPrincipal principal)
        {
            var inputModelDict = new Dictionary<string, object>();

            var sections = borrowerClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost);

            FeeSetupClosingCostSet closingCostSet = principal.BrokerDB.GetUnlinkedClosingCostSet();

            var sectionModels = new List<Dictionary<string, object>>();

            foreach (var section in sections)
            {
                var setionModel = ClosingCostInputHelper.ToSectionInputModel(section, principal, borrowerClosingCostSet);

                var fees = new List<BaseClosingCostFee>();
                FeeSetupClosingCostFeeSection setupSection = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeEstimate, section.SectionName);

                foreach (BorrowerClosingCostFeeSection closingSection in sections)
                {
                    foreach (BorrowerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                setupSection.SetExcludeFeeList(fees, borrowerClosingCostSet.DataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);
                setionModel.Add("NewFeeTemplates", setupSection.FilteredClosingCostFeeList.Select(x => ClosingCostInputHelper.ToFeeInputModel(x, principal, borrowerClosingCostSet)));

                sectionModels.Add(setionModel);
            }

            inputModelDict.Add("sections", sectionModels);

            var disableTPAffIfHasContact = borrowerClosingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Retail || borrowerClosingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || borrowerClosingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Broker;
            inputModelDict.Add("DisableTPAffIfHasContact", disableTPAffIfHasContact);

            inputModelDict.Add("CanReadDFLP", principal.HasPermission(Permission.AllowCloserRead));
            inputModelDict.Add("CanSetDFLP", principal.HasPermission(Permission.AllowCloserWrite));

            var hasStandaloneLenderOrigCompFee = borrowerClosingCostSet.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && borrowerClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
            borrowerClosingCostSet.DataLoan.sGfeIsTPOTransaction && borrowerClosingCostSet.DataLoan.sClosingCostSet.StandaloneLenderOrigCompFee != null;

            if (hasStandaloneLenderOrigCompFee)
            {
                inputModelDict.Add("StandaloneLenderOrigCompFee", ClosingCostInputHelper.ToFeeInputModel(borrowerClosingCostSet.DataLoan.sClosingCostSet.StandaloneLenderOrigCompFee, principal, borrowerClosingCostSet));
            }

            BorrowerClosingCostFee lastBorrFee = null;
            if (borrowerClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                lastBorrFee = (BorrowerClosingCostFee)borrowerClosingCostSet.DataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
            }

            inputModelDict.Add("LastSavedBorrowerOrigCompFee", lastBorrFee != null ? ClosingCostInputHelper.ToFeeInputModel(lastBorrFee, principal, borrowerClosingCostSet) : null);

            var feeServiceHistory = new DataAccess.FeeService.FeeServiceApplication.ClosingCostApplicationHistory(borrowerClosingCostSet.DataLoan.sFeeServiceApplicationHistoryXmlContent);
            inputModelDict.Add("feeServiceHistory", feeServiceHistory);
            inputModelDict.Add("defaultFees", ClosingCostInputHelper.defaultFees);

            return inputModelDict;
        }

        /// <summary>
        /// Update closing cost collection from an input field model.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update closing costs.</param>
        /// <returns>A new closing cost set deserialized from input model.</returns>
        public static BorrowerClosingCostSet BindInputModel(Dictionary<string, object> inputModelDict)
        {
            var set = new BorrowerClosingCostSet(string.Empty);
            if (inputModelDict.ContainsKey("sections"))
            {
                var sectionInputModels = SerializationHelper.JsonNetDeserialize<List<Dictionary<string, object>>>(inputModelDict["sections"].ToString());
                foreach (var sectionInputModel in sectionInputModels)
                {
                    if (sectionInputModel.ContainsKey("ClosingCostFeeList"))
                    {
                        var feeInputModels = SerializationHelper.JsonNetDeserialize<List<Dictionary<string, object>>>(sectionInputModel["ClosingCostFeeList"].ToString());
                        foreach (var feeInputModel in feeInputModels)
                        {
                            var fee = new BorrowerClosingCostFee();
                            ClosingCostInputHelper.BindFeeInputModel(fee, feeInputModel);
                            set.AddOrUpdate(fee);
                        }
                    }
                }
            }

            return set;
        }

        /// <summary>
        /// Return the json of closing cost of a loan.
        /// </summary>
        /// <param name="closingCostFee">Base closing cost fee to be serialize.</param>
        /// <param name="principal">Pricipal of authenitcated session.</param>
        /// <param name="closingCostSet">The closing cost set of current fee.</param>  
        /// <returns>An dictionary of closing cost of a loan.</returns>
        private static Dictionary<string, object> ToFeeInputModel(BaseClosingCostFee closingCostFee, AbstractUserPrincipal principal, BorrowerClosingCostSet closingCostSet)
        {
            Dictionary<string, object> inputModelDict = new Dictionary<string, object>();

            var isRequireFeesFromDropDown = closingCostSet.DataLoan.sIsRequireFeesFromDropDown;

            var isSectionB = closingCostFee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionB;
            var isSectionC = closingCostFee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC;
            var isSectionE = closingCostFee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionE;

            var can_shopReadOnly = isRequireFeesFromDropDown || isSectionB || isSectionC;

            inputModelDict.Add("apr", new InputFieldModel(closingCostFee.IsApr, readOnly: isRequireFeesFromDropDown));
            inputModelDict.Add("can_shop", new InputFieldModel(closingCostFee.CanShop));
            inputModelDict.Add("changeSect", new InputFieldModel(closingCostFee.CanChangeSection));
            inputModelDict.Add("dflp", new InputFieldModel(closingCostFee.Dflp, readOnly: !principal.HasPermission(Permission.AllowCloserWrite)));
            inputModelDict.Add("disc_sect", new InputFieldModel(closingCostFee.IntegratedDisclosureSectionT));
            inputModelDict.Add("editableSystem", new InputFieldModel(closingCostFee.IsEditableSystemFee));
            inputModelDict.Add("fha", new InputFieldModel(closingCostFee.IsFhaAllowable, readOnly: isRequireFeesFromDropDown));
            inputModelDict.Add("gfeGrps", new InputFieldModel(closingCostFee.GfeSectionGroup));
            inputModelDict.Add("hudline", new InputFieldModel(closingCostFee.HudLine));
            inputModelDict.Add("id", new InputFieldModel(closingCostFee.UniqueId));
            inputModelDict.Add("is_optional", new InputFieldModel(closingCostFee.IsOptional, readOnly: isSectionE));
            inputModelDict.Add("is_system", new InputFieldModel(closingCostFee.IsSystemLegacyFee));
            inputModelDict.Add("is_title", new InputFieldModel(closingCostFee.IsTitleFee, readOnly: isRequireFeesFromDropDown));
            inputModelDict.Add("legacy", new InputFieldModel(closingCostFee.LegacyGfeFieldT));
            inputModelDict.Add("mismo", new InputFieldModel(closingCostFee.MismoFeeT));
            inputModelDict.Add("section", new InputFieldModel(closingCostFee.GfeSectionT));
            inputModelDict.Add("typeid", new InputFieldModel(closingCostFee.ClosingCostFeeTypeId));
            inputModelDict.Add("va", new InputFieldModel(closingCostFee.IsVaAllowable));

            var beneModel = new InputFieldModel(closingCostFee.Beneficiary, readOnly: isRequireFeesFromDropDown);

            var hasPRMFeature = principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            if (!beneModel.IsReadOnly)
            {
                var beneOptions = beneModel.Options.OrderBy(x => x.Value).ToList();
                beneOptions.RemoveAll(x =>
                {
                    int agentValue;
                    if (int.TryParse(x.Key, out agentValue))
                    {
                        var agentRoleType = (E_AgentRoleT)agentValue;

                        if (agentRoleType == E_AgentRoleT.BrokerProcessor || agentRoleType == E_AgentRoleT.HazardInsurance)
                        {
                            return true;
                        }

                        bool isPmlRole = agentRoleType == E_AgentRoleT.BrokerProcessor
                                        || agentRoleType == E_AgentRoleT.ExternalSecondary
                                        || agentRoleType == E_AgentRoleT.ExternalPostCloser;
                        return !hasPRMFeature && isPmlRole;
                    }

                    return false;
                });

                var otherIndex = beneOptions.FindIndex(x => x.Key == E_AgentRoleT.Other.ToString("d"));
                if (otherIndex >= 0)
                {
                    var otherOption = beneOptions[otherIndex];
                    beneOptions.RemoveAt(otherIndex);
                    beneOptions.Add(otherOption);
                }

                beneModel.Options = beneOptions;
            }

            inputModelDict.Add("bene", beneModel);
            inputModelDict.Add("desc", new InputFieldModel(closingCostFee.Description));

            if (closingCostFee is LoanClosingCostFee)
            {
                var loanClosingCostFee = closingCostFee as LoanClosingCostFee;
                var descReadOnly = isRequireFeesFromDropDown || loanClosingCostFee.ChildLoanFormula.FormulaT == E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount || loanClosingCostFee.ChildLoanFormula.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses;
                inputModelDict["desc"] = new InputFieldModel(closingCostFee.Description, readOnly: descReadOnly);

                var isManuallySetThirdPartyAffiliateProps = closingCostSet.DataLoan.sIsManuallySetThirdPartyAffiliateProps;
                var disableTPAffIfHasContact = closingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Retail || closingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || closingCostSet.DataLoan.sBranchChannelT == E_BranchChannelT.Broker;
                var emptyBene = loanClosingCostFee.BeneficiaryAgentId == Guid.Empty;
                var thirdPartyReadOnly = !isManuallySetThirdPartyAffiliateProps && disableTPAffIfHasContact && emptyBene;
                var affReadOnly = thirdPartyReadOnly || !closingCostFee.IsThirdParty;

                inputModelDict.Add("tp", new InputFieldModel(closingCostFee.IsThirdParty, readOnly: thirdPartyReadOnly));
                inputModelDict.Add("aff", new InputFieldModel(closingCostFee.IsAffiliate, readOnly: affReadOnly));

                inputModelDict.Add("org_desc", new InputFieldModel(loanClosingCostFee.OriginalDescription));
                inputModelDict.Add("did_shop", new InputFieldModel(loanClosingCostFee.DidShop));
                inputModelDict.Add("disable_bene_auto", new InputFieldModel(loanClosingCostFee.DisableBeneficiaryAutomation));
                inputModelDict.Add("bene_desc", new InputFieldModel(loanClosingCostFee.BeneficiaryDescription));
                inputModelDict.Add("bene_id", new InputFieldModel(loanClosingCostFee.BeneficiaryAgentId));
                inputModelDict.Add("responsible", new InputFieldModel(loanClosingCostFee.GfeResponsiblePartyT));
                inputModelDict.Add("is_bf", new InputFieldModel(loanClosingCostFee.IsBonaFide));
                inputModelDict.Add("total", new InputFieldModel(loanClosingCostFee.TotalAmount_rep, type: InputFieldType.Money));

                var payments = loanClosingCostFee.Payments.Select(x => ClosingCostInputHelper.ToPaymentInputModel(x, loanClosingCostFee));

                // Override payments value from base class.
                inputModelDict["payments"] = payments;

                if (loanClosingCostFee.ChildLoanFormula != null)
                {
                    inputModelDict.Add("f", ClosingCostInputHelper.ToFormulaInputModel(loanClosingCostFee.ChildLoanFormula));
                }

                if (loanClosingCostFee is BorrowerClosingCostFee)
                {
                    var borrClosingCostFee = closingCostFee as BorrowerClosingCostFee;
                    
                    inputModelDict.Add("is_qm", new InputFieldModel(borrClosingCostFee.IsIncludedInQm));
                    inputModelDict.Add("prov", new InputFieldModel(borrClosingCostFee.ProviderChosenByLender));
                    inputModelDict.Add("prov_choice", new InputFieldModel(borrClosingCostFee.GfeProviderChoiceT));
                    inputModelDict.Add("qm_amount", new InputFieldModel(borrClosingCostFee.QmAmount));
                    inputModelDict.Add("qm_warning", new InputFieldModel(borrClosingCostFee.IsShowQmWarning));

                    inputModelDict["can_shop"] = new InputFieldModel(borrClosingCostFee.CanShop);
                    inputModelDict["did_shop"] = new InputFieldModel(borrClosingCostFee.DidShop, readOnly: !loanClosingCostFee.CanShop);
                }
            }

            return inputModelDict;
        }

        /// <summary>
        /// Update closing cost collection from an input field model.
        /// </summary>
        /// <param name="closingCostFee">Borrower closing cost fee to be updated.</param>
        /// <param name="inputModelDict">Input field model to update closing costs.</param>
        private static void BindFeeInputModel(BorrowerClosingCostFee closingCostFee, Dictionary<string, object> inputModelDict)
        {
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "aff", closingCostFee, x => x.IsAffiliate);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "apr", closingCostFee, x => x.IsApr);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "bene", closingCostFee, x => x.Beneficiary);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "can_shop", closingCostFee, x => x.CanShop);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "changeSect", closingCostFee, x => x.CanChangeSection);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "desc", closingCostFee, x => x.Description);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "dflp", closingCostFee, x => x.Dflp);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "disc_sect", closingCostFee, x => x.IntegratedDisclosureSectionT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "editableSystem", closingCostFee, x => x.IsEditableSystemFee);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "fha", closingCostFee, x => x.IsFhaAllowable);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "gfeGrps", closingCostFee, x => x.GfeSectionGroup);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "hudline", closingCostFee, x => x.HudLine);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "id", closingCostFee, x => x.UniqueId);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "is_optional", closingCostFee, x => x.IsOptional);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "is_system", closingCostFee, x => x.IsSystemLegacyFee);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "is_title", closingCostFee, x => x.IsTitleFee);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "legacy", closingCostFee, x => x.LegacyGfeFieldT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "mismo", closingCostFee, x => x.MismoFeeT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "section", closingCostFee, x => x.GfeSectionT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "tp", closingCostFee, x => x.IsThirdParty);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "typeid", closingCostFee, x => x.ClosingCostFeeTypeId);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "va", closingCostFee, x => x.IsVaAllowable);

            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "org_desc", closingCostFee, x => x.OriginalDescription);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "did_shop", closingCostFee, x => x.DidShop);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "disable_bene_auto", closingCostFee, x => x.DisableBeneficiaryAutomation);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "bene_desc", closingCostFee, x => x.BeneficiaryDescription);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "bene_id", closingCostFee, x => x.BeneficiaryAgentId);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "responsible", closingCostFee, x => x.GfeResponsiblePartyT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "is_bf", closingCostFee, x => x.IsBonaFide);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "total", closingCostFee, x => x.TotalAmount_rep);
            
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "is_qm", closingCostFee, x => x.IsIncludedInQm);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "prov", closingCostFee, x => x.ProviderChosenByLender);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "prov_choice", closingCostFee, x => x.GfeProviderChoiceT);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "qm_amount", closingCostFee, x => x.QmAmount);
            ClosingCostInputHelper.BindPropertyFromInputModel(inputModelDict, "qm_warning", closingCostFee, x => x.IsShowQmWarning);

            if (inputModelDict.ContainsKey("f"))
            {
                var formulaInputModels = SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict["f"].ToString());
                var formula = new BorrowerClosingCostFormula();
                ClosingCostInputHelper.BindFormulaInputModel(formula, formulaInputModels);
                closingCostFee.ChildLoanFormula = formula;
            }

            if (inputModelDict.ContainsKey("payments"))
            {
                closingCostFee.ClearPayments();

                var paymentInputModels = SerializationHelper.JsonNetDeserialize<List<SimpleObjectInputFieldModel>>(inputModelDict["payments"].ToString());

                var paymentModelCount = paymentInputModels.Count();

                if (paymentModelCount > 0)
                {
                    if (paymentModelCount == 1)
                    {
                        if (paymentInputModels[0].ContainsKey("paid_by"))
                        {
                            string paidByValue = paymentInputModels[0]["paid_by"].Value;

                            if (paidByValue == "-1")
                            {
                                var newPayment = new BorrowerClosingCostFeePayment();
                                newPayment.Amount = 0;
                                newPayment.Entity = E_AgentRoleT.Other;
                                newPayment.IsMade = false;
                                newPayment.IsSystemGenerated = true;
                                newPayment.SetParent(closingCostFee);
                                closingCostFee.AddPayment(newPayment);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i < paymentModelCount; i++)
                        {
                            var payment = new BorrowerClosingCostFeePayment();
                            ClosingCostInputHelper.BindPaymentInputModel(payment, paymentInputModels[i]);
                            closingCostFee.AddPayment(payment);
                        }
                    }

                    ClosingCostInputHelper.BindPaymentInputModel(closingCostFee.Payments.ToArray()[0], paymentInputModels[0]);
                }
            }

            return;
        }

        /// <summary>
        /// Return the json of closing cost section of a loan.
        /// </summary>
        /// <param name="closingCostFeeSection">Borrower closing cost fee section to be serialize.</param>
        /// <param name="principal">Pricipal of authenitcated session.</param>
        /// <param name="closingCostSet">The closing cost set of current section.</param>  
        /// <returns>An dictionary of closing cost of a loan.</returns>
        private static Dictionary<string, object> ToSectionInputModel(BaseClosingCostFeeSection closingCostFeeSection, AbstractUserPrincipal principal, BorrowerClosingCostSet closingCostSet)
        {
            var inputModelDict = new Dictionary<string, object>();

            inputModelDict.Add("SectionName", new InputFieldModel(closingCostFeeSection.SectionName, InputFieldType.String, readOnly: true));
            inputModelDict.Add("HudLineStart", new InputFieldModel(closingCostFeeSection.HudLineStart.ToString(), InputFieldType.Number, readOnly: true));
            inputModelDict.Add("HudLineEnd", new InputFieldModel(closingCostFeeSection.HudLineEnd.ToString(), InputFieldType.Number, readOnly: true));
            inputModelDict.Add("SectionType", new InputFieldModel(Convert.ToInt32(closingCostFeeSection.SectionType).ToString(), InputFieldType.DropDownList, readOnly: true, options: EnumUtilities.GetValuesFromType(closingCostFeeSection.SectionType.GetType())));

            inputModelDict.Add("ClosingCostFeeList", closingCostFeeSection.FilteredClosingCostFeeList.Select(x => ClosingCostInputHelper.ToFeeInputModel(x, principal, closingCostSet)));

            LosConvert losConvert = new LosConvert();
            if (closingCostFeeSection is BorrowerClosingCostFeeSection)
            {
                var borClosingCosFeeSection = closingCostFeeSection as BorrowerClosingCostFeeSection;
                
                inputModelDict.Add("TotalAmount", new InputFieldModel(losConvert.ToDecimal(borClosingCosFeeSection.TotalAmount_rep)));
                inputModelDict.Add("TotalAmountBorrPaid", new InputFieldModel(losConvert.ToDecimal(borClosingCosFeeSection.TotalAmountBorrPaid_rep)));
            }

            return inputModelDict;
        }

        /// <summary>
        /// Return the json of closing cost fee payment of a loan.
        /// </summary>
        /// <param name="closingCostFeePayment">Loan closing cost fee payment to be serialize.</param>
        /// <param name="parentFee">The parent closing cost fee.</param>
        /// <returns>An dictionary of closing cost of a loan.</returns>
        private static SimpleObjectInputFieldModel ToPaymentInputModel(LoanClosingCostFeePayment closingCostFeePayment, LoanClosingCostFee parentFee)
        {
            var inputModelDict = new SimpleObjectInputFieldModel();

            var paymentReadOnly = closingCostFeePayment.IsSystemGenerated;
            inputModelDict.Add("id", new InputFieldModel(closingCostFeePayment.Id, readOnly: paymentReadOnly));
            inputModelDict.Add("is_system", new InputFieldModel(closingCostFeePayment.IsSystemGenerated, readOnly: paymentReadOnly));
            inputModelDict.Add("ent", new InputFieldModel(closingCostFeePayment.Entity, readOnly: paymentReadOnly));
            inputModelDict.Add("made", new InputFieldModel(closingCostFeePayment.IsMade, readOnly: paymentReadOnly));
            inputModelDict.Add("amt", new InputFieldModel(closingCostFeePayment.Amount, readOnly: paymentReadOnly));
            inputModelDict.Add("pmt_at", new InputFieldModel(closingCostFeePayment.GfeClosingCostFeePaymentTimingT, readOnly: paymentReadOnly || closingCostFeePayment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance));
            inputModelDict.Add("responsible", new InputFieldModel(closingCostFeePayment.ResponsiblePartyT, readOnly: paymentReadOnly || closingCostFeePayment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance));
            inputModelDict.Add("pmt_dt", new InputFieldModel(closingCostFeePayment.PaymentDate, customInputType: InputFieldType.Date, readOnly: closingCostFeePayment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing || paymentReadOnly));

            if (closingCostFeePayment is BorrowerClosingCostFeePayment)
            {
                ClosingCostInputHelper.UpdatePaymentInputModel(inputModelDict, closingCostFeePayment as BorrowerClosingCostFeePayment, parentFee);
            }

            return inputModelDict;
        }

        /// <summary>
        /// Update the json of closing cost fee payment of a loan for borrower payment.
        /// </summary>
        /// <param name="inputModelDict">The input model to be updated.</param>
        /// <param name="closingCostFeePayment">Borrower closing cost fee payment to be serialize.</param>
        /// <param name="parentFee">The parent closing cost fee.</param>
        private static void UpdatePaymentInputModel(SimpleObjectInputFieldModel inputModelDict, BorrowerClosingCostFeePayment closingCostFeePayment, LoanClosingCostFee parentFee)
        {
            inputModelDict.Add("cashPdPmt", new InputFieldModel(closingCostFeePayment.IsCashPdPayment));
            inputModelDict.Add("mipFinancedPmt", new InputFieldModel(closingCostFeePayment.IsMIPFinancedPayment));

            var paymentReadOnly = closingCostFeePayment.IsSystemGenerated;
            var paidByType = new InputFieldModel(closingCostFeePayment.PaidByT, readOnly: paymentReadOnly);

            var paydByEditedOpts = paidByType.Options.ToList();

            var isSinglePayment = parentFee.Payments.Count() == 1;
            var isSplitableFeeType = parentFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
            var isSplitableFormula = parentFee.ParentClosingCostSet.DataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 && parentFee.FormulaT != E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring && parentFee.FormulaT != E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront;
            var isNoLegacy = parentFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId;
            var isStandaloneFee = parentFee.ParentClosingCostSet.DataLoan.sClosingCostSet.StandaloneLenderOrigCompFee == parentFee;

            paydByEditedOpts.RemoveAll(x => x.Key == E_ClosingCostFeePaymentPaidByT.Other.ToString("d") || x.Key == E_ClosingCostFeePaymentPaidByT.LeaveBlank.ToString("d"));

            if (!isSinglePayment)
            {
                if (closingCostFeePayment.IsCashPdPayment)
                {
                    paydByEditedOpts.RemoveAll(x => x.Key == E_ClosingCostFeePaymentPaidByT.BorrowerFinance.ToString("d"));
                }
            }
            else if ((isSplitableFeeType || isSplitableFormula) && !isStandaloneFee)
            {
                paydByEditedOpts.Insert(0, new KeyValuePair<string, string>("-1", "--split--"));
            }

            if (isNoLegacy)
            {
                paydByEditedOpts.RemoveAll(x => x.Key == E_ClosingCostFeePaymentPaidByT.Lender.ToString("d"));
            }

            paidByType.Options = paydByEditedOpts;

            inputModelDict.Add("paid_by", paidByType);
        }

        /// <summary>
        /// Update closing cost fee payment collection from an input field model.
        /// </summary>
        /// <param name="closingCostFeePayment">Borrower closing cost fee payment to be updated.</param>
        /// <param name="inputModelDict">Input field model to update closing costs.</param>
        private static void BindPaymentInputModel(LoanClosingCostFeePayment closingCostFeePayment, SimpleObjectInputFieldModel inputModelDict)
        {
            inputModelDict.BindObjectFromModel("id", closingCostFeePayment, x => x.Id);
            inputModelDict.BindObjectFromModel("is_system", closingCostFeePayment, x => x.IsSystemGenerated);
            inputModelDict.BindObjectFromModel("ent", closingCostFeePayment, x => x.Entity);
            inputModelDict.BindObjectFromModel("made", closingCostFeePayment, x => x.IsMade);
            inputModelDict.BindObjectFromModel("amt", closingCostFeePayment, x => x.Amount);
            inputModelDict.BindObjectFromModel("pmt_at", closingCostFeePayment, x => x.GfeClosingCostFeePaymentTimingT);
            inputModelDict.BindObjectFromModel("responsible", closingCostFeePayment, x => x.ResponsiblePartyT);
            inputModelDict.BindObjectFromModel("pmt_dt", closingCostFeePayment, x => x.PaymentDate);

            if (inputModelDict.ContainsKey("paid_by"))
            {
                string paidByValue = inputModelDict["paid_by"].Value;

                if (paidByValue != "-1")
                {
                    inputModelDict.BindObjectFromModel("paid_by", closingCostFeePayment, x => x.PaidByT);
                }
            }

            if (closingCostFeePayment is BorrowerClosingCostFeePayment)
            {
                var borrclosingCostFeePayment = closingCostFeePayment as BorrowerClosingCostFeePayment;

                inputModelDict.BindObjectFromModel("mipFinancedPmt", borrclosingCostFeePayment, x => x.IsMIPFinancedPayment);
                inputModelDict.BindObjectFromModel("cashPdPmt", borrclosingCostFeePayment, x => x.IsCashPdPayment);
            }
        }

        /// <summary>
        /// Return the json of closing cost fee formula of a loan.
        /// </summary>
        /// <param name="closingCostFeeFormula">Borrower closing cost fee formula to be serialize.</param>
        /// <returns>An dictionary of closing cost of a loan.</returns>
        private static SimpleObjectInputFieldModel ToFormulaInputModel(LoanClosingCostFormula closingCostFeeFormula)
        {
            var inputModelDict = new SimpleObjectInputFieldModel();
            inputModelDict.Add("t", new InputFieldModel(closingCostFeeFormula.FormulaT));
            inputModelDict.Add("pb", new InputFieldModel(closingCostFeeFormula.PercentTotalAmount));
            inputModelDict.Add("base", new InputFieldModel(closingCostFeeFormula.BaseAmount));
            inputModelDict.Add("p", new InputFieldModel(closingCostFeeFormula.Percent, customInputType: InputFieldType.Percent));
            inputModelDict.Add("period", new InputFieldModel(closingCostFeeFormula.NumberOfPeriods));
            inputModelDict.Add("pt", new InputFieldModel(closingCostFeeFormula.PercentBaseT));

            return inputModelDict;
        }

        /// <summary>
        /// Update closing cost fee formula from an input field model.
        /// </summary>
        /// <param name="closingCostFeeFormula">Borrower closing cost fee formula to be updated.</param>
        /// <param name="inputModelDict">Input field model to update closing costs.</param>
        private static void BindFormulaInputModel(BorrowerClosingCostFormula closingCostFeeFormula, SimpleObjectInputFieldModel inputModelDict)
        {
            inputModelDict.BindObjectFromModel("t", closingCostFeeFormula, x => x.FormulaT);
            inputModelDict.BindObjectFromModel("pb", closingCostFeeFormula, x => x.PercentTotalAmount);
            inputModelDict.BindObjectFromModel("base", closingCostFeeFormula, x => x.BaseAmount);
            inputModelDict.BindObjectFromModel("p", closingCostFeeFormula, x => x.Percent);
            inputModelDict.BindObjectFromModel("period", closingCostFeeFormula, x => x.NumberOfPeriods);
            inputModelDict.BindObjectFromModel("pt", closingCostFeeFormula, x => x.PercentBaseT);

            return;
        }

        /// <summary>
        /// Find a property field from input model and update it.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <typeparam name="S">The second generic type parameter, for object property type.</typeparam>
        /// <param name="inputModel">The input model used to bind data from.</param>
        /// <param name="fieldname">The field name of property in input model.</param>
        /// <param name="target">The object need to be change property.</param>
        /// <param name="outExpr">The expression to identfy the object property.</param>
        private static void BindPropertyFromInputModel<T, S>(Dictionary<string, object> inputModel, string fieldname, T target, Expression<Func<T, S>> outExpr)
        {
            if (inputModel.ContainsKey(fieldname))
            {
                var fieldModel = SerializationHelper.JsonNetDeserialize<InputFieldModel>(inputModel[fieldname].ToString());
                fieldModel.BindObjectFromModel(target, outExpr);
            }
        }
    }
}
