﻿// <copyright file="PoolModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   4/29/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.ObjLib.MortgagePool;

    /// <summary>
    /// The data structure for the trades and pools page in the new UI.
    /// </summary>
    public class PoolModel : BasicModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PoolModel"/> class.
        /// </summary>
        public PoolModel()
        {
        }

        /// <summary>
        /// Gets or sets the feature codes.
        /// </summary>
        /// <value>The feature codes.</value>
        public List<string> FeatureCodes { get; set; }

        /// <summary>
        /// Gets or sets the Settlement Accounts.
        /// </summary>
        /// <value>A list of settlement accounts.</value>
        public List<Dictionary<string, object>> SettlementAccounts
        {
            get; set;
        }

        /// <summary>
        /// Adds a new key value pair to the Loan Summary dictionary.
        /// </summary>
        /// <param name="key">The name of the field.</param>
        /// <param name="value">The value of the field.</param>
        public void AddSummaryField(string key, string value)
        {
            if (this.SummaryValues == null)
            {
                this.SummaryValues = new Dictionary<string, string>();
            }

            this.SummaryValues[key] = value;
        }
    }
}
