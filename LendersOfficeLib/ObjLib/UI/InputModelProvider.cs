﻿// <copyright file="InputModelProvider.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   3/16/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The interface is used  serialize  and  deserialize  object  to input model.
    /// </summary>
    public class InputModelProvider
    {
        /// <summary>
        /// A cache to store the property list of classes.
        /// </summary>
        private static Dictionary<string, Dictionary<string, PropertyItem>> classPropertyCache;

        /// <summary>
        /// Return the enum of input model to return from and item.
        /// </summary>
        private enum ModelReturnType
        {
            /// <summary>
            /// Item will return a nested object model.
            /// </summary>
            NestedInputModel,

            /// <summary>
            /// Item will return a list of model.
            /// </summary>
            IEnumerableInputModel,

            /// <summary>
            /// Item will return an input field model.
            /// </summary>
            InputFieldModel,

            /// <summary>
            /// Item will skip and return nothing.
            /// </summary>
            None,
        }

        /// <summary>
        /// Return the input model of an object.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <param name="inputObject">Object to extract its propety info.</param>
        /// <returns>Return the input field model of the input object. Throw exception if object is null.</returns>
        public static Dictionary<string, object> GetInputModel<T>(T inputObject) where T : class
        {
            if (inputObject == null)
            {
                throw new NullReferenceException();
            }

            var propItemDict = InputModelProvider.GetPropertyItemsFromCache(inputObject.GetType());

            var inputModeltDict = propItemDict.ToDictionary(x => x.Key, y => y.Value.ToInputObject(inputObject));
            var filteredInputModelDict = inputModeltDict.Where(x => x.Value != null).ToDictionary(i => i.Key, i => i.Value);

            return filteredInputModelDict;
        }

        /// <summary>
        /// Return the input model of a property of certain object.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <param name="inputObject">Object to extract its propety info.</param>
        /// <param name="fieldName">Name of property we want to extract.</param>
        /// <returns>Return the input field model of specific property of certain object. Throw exception if object is null.</returns>
        public static InputFieldModel GetInputPropertyModel<T>(T inputObject, string fieldName) where T : class
        {
            if (inputObject == null)
            {
                throw new NullReferenceException();
            }

            var propItemDict = InputModelProvider.GetPropertyItemsFromCache(inputObject.GetType());

            if (propItemDict.ContainsKey(fieldName))
            {
                var temp = propItemDict[fieldName];
                if (temp.ModelType == ModelReturnType.InputFieldModel)
                {
                    return temp.ToInputModel(inputObject);
                }
            }

            return null;
        }

        /// <summary>
        /// Bind an input model to an object.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <param name="inputObject">Object which's value will be binded.</param>
        /// <param name="inputModel">Input model to change object value.</param>
        public static void BindObjectFromInputModel<T>(T inputObject, Dictionary<string, object> inputModel) where T : class
        {
            if (inputObject == null)
            {
                throw new NullReferenceException();
            }

            var propItemDict = InputModelProvider.GetPropertyItemsFromCache(inputObject.GetType());

            foreach (var itemPair in inputModel)
            {
                if (propItemDict.ContainsKey(itemPair.Key))
                {
                    var jsonModel = SerializationHelper.JsonNetDeserialize<JToken>(itemPair.Value.ToString());
                    var propItem = propItemDict[itemPair.Key];

                    if (propItem != null && jsonModel != null)
                    {
                        propItem.BindFromInputModel(inputObject, jsonModel);
                    }
                }
            }
        }

        /// <summary>
        /// Calculate zipcode from json object.
        /// </summary>
        /// <param name="inputModelString">Object json input as string.</param>
        /// <returns>Return new data loan model as string.</returns>
        public static string CalculateZipcode(string inputModelString)
        {
            var jsonObject = JObject.Parse(inputModelString);
            return InputModelProvider.CalculateZipcode(jsonObject);
        }

        /// <summary>
        /// Calculate zipcode from json object.
        /// </summary>
        /// <param name="jsonObject">Object json input.</param>
        /// <returns>Return new data loan model as string.</returns>
        private static string CalculateZipcode(JObject jsonObject)
        {
            foreach (var jsonProp in jsonObject.Properties())
            {
                if (jsonProp.Value == null)
                {
                    continue;
                }
                else if (jsonProp.Value is JValue)
                {
                    continue;
                }
                else if (jsonProp.Value is JArray)
                {
                    JArray jsonObjs = jsonProp.Value as JArray;
                    foreach (var jsonObj in jsonObjs)
                    {
                        if (jsonObj is JObject)
                        {
                            InputModelProvider.CalculateZipcode((JObject)jsonObj);
                        }
                    }
                }
                else if (jsonProp.Value is JObject)
                {
                    if ((jsonProp.Name.EndsWith(InputFieldNameSuffix.Zip) || jsonProp.Name.EndsWith("Zipcode")) && jsonProp.Value["v"] is JValue)
                    {
                        string addrName = jsonProp.Name.Remove(jsonProp.Name.Length - (jsonProp.Name.EndsWith(InputFieldNameSuffix.Zip) ? InputFieldNameSuffix.Zip.Length : "Zipcode".Length));
                        string county, city, state;
                        if (Tools.GetCountyCityStateByZip(jsonProp.Value["v"].ToString(), out county, out city, out state))
                        {
                            string stateField = addrName + InputFieldNameSuffix.State;
                            var stateJson = jsonObject[stateField];
                            if (stateJson is JObject && stateJson["v"] is JValue)
                            {
                                stateJson["v"] = new JValue(state);
                            }

                            string countyField = addrName + InputFieldNameSuffix.County;
                            var countyJson = jsonObject[countyField];
                            if (countyJson is JObject && countyJson["v"] is JValue)
                            {
                                countyJson["v"] = new JValue(county);
                            }

                            string cityField = addrName + InputFieldNameSuffix.City;
                            var cityJson = jsonObject[cityField];
                            if (cityJson is JObject && cityJson["v"] is JValue)
                            {
                                cityJson["v"] = new JValue(city);
                            }

                            continue;
                        }
                    }

                    InputModelProvider.CalculateZipcode((JObject)jsonProp.Value);
                }
            }

            return jsonObject.ToString();
        }

        /// <summary>
        /// Get property item dictionary from cache.
        /// </summary>
        /// <param name="objectType">Input type to ger property items of.</param>
        /// <returns>Property item dictionary .</returns>
        private static Dictionary<string, PropertyItem> GetPropertyItemsFromCache(Type objectType)
        {
            if (InputModelProvider.classPropertyCache == null)
            {
                InputModelProvider.classPropertyCache = new Dictionary<string, Dictionary<string, PropertyItem>>();
            }

            var objectTypeName = objectType.ToString();

            if (!InputModelProvider.classPropertyCache.ContainsKey(objectTypeName))
            {
                var properties = objectType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                var propItemDict = properties.ToDictionary(k => k.Name, v => new PropertyItem(v));
                foreach (var propKeyPair in propItemDict)
                {
                    var propItem = propKeyPair.Value;
                    var propOriginName = propKeyPair.Key;
                    PropertyItem tempItem;

                    string readonlyFieldName = propOriginName + InputFieldNameSuffix.ReadOnly;
                    if (propItemDict.TryGetValue(readonlyFieldName, out tempItem))
                    {
                        propItem.InvokeReadOnlyGetMethod = tempItem.InvokeGetMethod;
                    }

                    string lockedFieldName = propOriginName + InputFieldNameSuffix.Lock;
                    if (propItemDict.TryGetValue(lockedFieldName, out tempItem))
                    {
                        propItem.InvokeLockGetMethod = tempItem.InvokeGetMethod;
                    }

                    string optionsFieldName = propOriginName + InputFieldNameSuffix.Options;
                    if (propItemDict.TryGetValue(optionsFieldName, out tempItem))
                    {
                        propItem.InvokeOptionsGetMethod = tempItem.InvokeGetMethod;
                    }

                    if (propItem.ModelType == ModelReturnType.IEnumerableInputModel && propOriginName.EndsWith(InputFieldNameSuffix.List))
                    {
                        string templateFieldName = propOriginName.Remove(propOriginName.Length - InputFieldNameSuffix.List.Length) + InputFieldNameSuffix.ListRecordTemplate;
                        if (propItemDict.TryGetValue(templateFieldName, out tempItem))
                        {
                            propItem.InvokeTemplateGetMethod = tempItem.InvokeGetMethod;
                            propItem.PropertyType = tempItem.PropertyType;
                        }
                    }
                }

                InputModelProvider.classPropertyCache[objectTypeName] = propItemDict.Where(x => x.Value.ModelType != ModelReturnType.None).ToDictionary(i => i.Value.InputFieldName, i => i.Value);
            }

            return classPropertyCache[objectTypeName];
        }

        /// <summary>
        /// Store the info of a property.
        /// </summary>
        private class PropertyItem
        {
            /// <summary>
            /// List of available property type to extract to input model.
            /// </summary>
            private static List<Type> availablePropType = new List<Type> { typeof(string), typeof(DateTime), typeof(DateTime?), typeof(bool), typeof(decimal), typeof(double), typeof(float), typeof(int), typeof(long), typeof(CDateTime), typeof(Guid), typeof(Sensitive<string>) };

            /// <summary>
            /// Initializes a new instance of the <see cref="PropertyItem"/> class.
            /// </summary>
            /// <param name="propInfo">The property info to be converted.</param>
            /// <returns>PropertyItem extract from input property info.</returns>
            public PropertyItem(PropertyInfo propInfo)
            {
                this.InvokeGetMethod = propInfo.GetGetMethod();
                this.InvokeSetMethod = propInfo.GetSetMethod();
                this.PropertyType = propInfo.PropertyType;
                this.InputFieldName = propInfo.Name;

                object[] attrs = propInfo.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    LqbInputModelAttribute inputModelAttr = attr as LqbInputModelAttribute;
                    if (inputModelAttr != null)
                    {
                        var customModel = inputModelAttr.Model;
                        if (!string.IsNullOrEmpty(customModel.Name))
                        {
                            this.InputFieldName = customModel.Name;
                        }

                        this.CustomInputModel = customModel;
                    }
                }

                var invalidGetMethod = this.InvokeGetMethod == null;
                var isCustomInvalidModel = this.CustomInputModel != null && this.CustomInputModel.Invalid;
                var isAttributeField = this.InputFieldName.EndsWith(InputFieldNameSuffix.ReadOnly) || this.InputFieldName.EndsWith(InputFieldNameSuffix.Visible) || this.InputFieldName.EndsWith(InputFieldNameSuffix.Options);

                if (invalidGetMethod || isCustomInvalidModel || isAttributeField)
                {
                    this.ModelType = ModelReturnType.None;
                }
                else
                {
                    if (PropertyItem.InputFieldExtractableType(this.PropertyType))
                    {
                        this.ModelType = ModelReturnType.InputFieldModel;
                    }
                    else if (this.PropertyType.GetInterfaces().Contains(typeof(System.Collections.IEnumerable)))
                    {
                        this.ModelType = ModelReturnType.IEnumerableInputModel;
                    }
                    else
                    {
                        this.ModelType = ModelReturnType.NestedInputModel;
                    }
                }
            }

            /// <summary>
            /// Gets type of the input field name.
            /// </summary>
            public string InputFieldName { get; private set; }

            /// <summary>
            /// Gets or sets of the property.
            /// </summary>
            public Type PropertyType { get; set; }

            /// <summary>
            /// Gets method of that property.
            /// </summary>
            public MethodInfo InvokeGetMethod { get; private set; }

            /// <summary>
            /// Gets method of that property.
            /// </summary>
            public MethodInfo InvokeSetMethod { get; private set; }

            /// <summary>
            /// Gets or sets method of that property.
            /// </summary>
            public MethodInfo InvokeLockGetMethod { get; set; }

            /// <summary>
            /// Gets or sets method of that property.
            /// </summary>
            public MethodInfo InvokeReadOnlyGetMethod { get; set; }

            /// <summary>
            /// Gets or sets method of that property.
            /// </summary>
            public MethodInfo InvokeVisibleGetMethod { get; set; }

            /// <summary>
            /// Gets or sets options method of that property.
            /// </summary>
            public MethodInfo InvokeOptionsGetMethod { get; set; }

            /// <summary>
            /// Gets or sets template get method of an array.
            /// </summary>
            public MethodInfo InvokeTemplateGetMethod { get; set; }

            /// <summary>
            /// Gets custom atribute of that property.
            /// </summary>
            public LqbInputModel CustomInputModel { get; private set; }

            /// <summary>
            /// Gets the type of model this property return.
            /// </summary>
            public ModelReturnType ModelType { get; private set; }

            /// <summary>
            /// Return an input field object.
            /// </summary>
            /// <param name="inputObject">Object to be extracted value.</param>
            /// <returns>Input field object get from the property info and input object.</returns>
            public object ToInputObject(object inputObject)
            {
                switch (this.ModelType)
                {
                    case ModelReturnType.InputFieldModel:
                        return this.ToInputModel(inputObject);
                    case ModelReturnType.IEnumerableInputModel:
                        object propValues = this.InvokeGetMethod.Invoke(inputObject, null);

                        if (propValues != null)
                        {
                            return ((System.Collections.IEnumerable)propValues).Cast<object>().Select(x =>
                                {
                                    if (PropertyItem.InputFieldExtractableType(x.GetType()))
                                    {
                                        return new InputFieldModel(x) as object;
                                    }
                                    else
                                    {
                                        return InputModelProvider.GetInputModel(x) as object;
                                    }
                                });
                        }

                        break;
                    case ModelReturnType.NestedInputModel:

                        object propValue = this.InvokeGetMethod.Invoke(inputObject, null);

                        if (propValue != null)
                        {
                            return InputModelProvider.GetInputModel(propValue);
                        }

                        break;
                    case ModelReturnType.None:
                        return null;
                    default:
                        throw new InvalidOperationException();
                }

                return null;
            }

            /// <summary>
            /// Return an input field model.
            /// </summary>
            /// <param name="inputObject">Object to be extracted value.</param>
            /// <returns>Input field model get from the property info and input object.</returns>
            public InputFieldModel ToInputModel(object inputObject)
            {
                var propValue = this.InvokeGetMethod.Invoke(inputObject, null);

                InputFieldModel inputFieldModel;
                if (propValue != null)
                {
                    inputFieldModel = new InputFieldModel(propValue);
                }
                else
                {
                    inputFieldModel = new InputFieldModel();
                    inputFieldModel.Type = this.GetInputTypeOfNullObject();
                }

                // Check readonly attribute.
                if (this.InvokeSetMethod == null)
                {
                    inputFieldModel.IsReadOnly = true;
                }
                else
                {
                    if (this.InvokeReadOnlyGetMethod != null)
                    {
                        bool isReadOnly = (bool)this.InvokeReadOnlyGetMethod.Invoke(inputObject, null);
                        if (isReadOnly)
                        {
                            inputFieldModel.IsReadOnly = true;
                        }
                    }

                    if (this.InvokeLockGetMethod != null)
                    {
                        bool isLocked = (bool)this.InvokeLockGetMethod.Invoke(inputObject, null);
                        if (!isLocked)
                        {
                            inputFieldModel.IsReadOnly = true;
                        }
                    }
                }

                // Check visible attribute.
                if (this.InvokeVisibleGetMethod != null)
                {
                    bool isVisible = (bool)this.InvokeVisibleGetMethod.Invoke(inputObject, null);
                    if (!isVisible)
                    {
                        inputFieldModel.IsHidden = true;
                    }
                }

                if (this.InvokeOptionsGetMethod != null)
                {
                    inputFieldModel.Options = (List<KeyValuePair<string, string>>)this.InvokeOptionsGetMethod.Invoke(inputObject, null);
                }

                if (this.InputFieldName.EndsWith(InputFieldNameSuffix.Lock))
                {
                    inputFieldModel.Type = InputFieldType.Lock;
                }
                else if (this.InputFieldName.EndsWith(InputFieldNameSuffix.State))
                {
                    inputFieldModel.Type = InputFieldType.DropDownList;
                    inputFieldModel.Options = EnumUtilities.GetValuesFromField("state", PrincipalFactory.CurrentPrincipal);
                }
                else if (this.InputFieldName.EndsWith(InputFieldNameSuffix.Zip) || this.InputFieldName.EndsWith("Zipcode"))
                {
                    inputFieldModel.Type = InputFieldType.Zipcode;
                    inputFieldModel.CalcActionT = E_CalculationType.CalculateZip;
                }
                else if (this.InputFieldName.EndsWith(InputFieldNameSuffix.Phone) || this.InputFieldName.EndsWith(InputFieldNameSuffix.Fax))
                {
                    inputFieldModel.Type = InputFieldType.Phone;
                }

                if (this.CustomInputModel != null)
                {
                    if (this.CustomInputModel.EnumType != null)
                    {
                        inputFieldModel.Options = EnumUtilities.GetValuesFromType(this.CustomInputModel.EnumType);
                    }

                    if (this.CustomInputModel.Type != InputFieldType.Unknown)
                    {
                        inputFieldModel.Type = this.CustomInputModel.Type;
                    }

                    if (this.CustomInputModel.MaxLength > 0)
                    {
                        inputFieldModel.MaxLength = this.CustomInputModel.MaxLength;
                    }

                    inputFieldModel.Required = this.CustomInputModel.Required;
                }

                // If field don't need options, remove it.
                if (!inputFieldModel.Type.HasOptionField())
                {
                    inputFieldModel.Options = null;
                }

                return inputFieldModel;
            }

            /// <summary>
            /// Bind input model to object.
            /// </summary>
            /// <param name="inputObject">Object of which value will be updated.</param>
            /// <param name="jsonModel">The model used for binding.</param>
            public void BindFromInputModel(object inputObject, JToken jsonModel)
            {
                if (this.InvokeSetMethod == null)
                {
                    return;
                }

                switch (this.ModelType)
                {
                    case ModelReturnType.InputFieldModel:
                        var model = SerializationHelper.JsonNetDeserialize<InputFieldModel>(jsonModel.ToString());
                        object objectValue = model.ValueFromType(this.PropertyType);

                        if (objectValue != null)
                        {
                            this.InvokeSetMethod.Invoke(inputObject, new object[] { objectValue });
                        }

                        return;
                    case ModelReturnType.IEnumerableInputModel:
                        if (jsonModel is JArray && this.InvokeTemplateGetMethod != null)
                        {
                            var valueObjects = ((JArray)jsonModel).Select(x =>
                                {
                                    object templateValue = this.InvokeTemplateGetMethod.Invoke(inputObject, null);
                                    var valueType = templateValue.GetType();
                                    if (PropertyItem.InputFieldExtractableType(valueType))
                                    {
                                        var valueModel = SerializationHelper.JsonNetDeserialize<InputFieldModel>(x.ToString());
                                        templateValue = valueModel.ValueFromType(valueType);
                                    }
                                    else
                                    {
                                        InputModelProvider.BindObjectFromInputModel(templateValue, SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(x.ToString()));
                                    }

                                    return templateValue;
                                }).ToList();
                            this.InvokeSetMethod.Invoke(inputObject, new object[] { valueObjects });
                        }

                        return;
                    case ModelReturnType.NestedInputModel:
                        if (jsonModel is JObject)
                        {
                            object currentValue = this.InvokeGetMethod.Invoke(inputObject, null);
                            if (currentValue != null)
                            {
                                InputModelProvider.BindObjectFromInputModel(currentValue, SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(jsonModel.ToString()));
                                this.InvokeSetMethod.Invoke(inputObject, new object[] { currentValue });
                            }
                        }

                        return;
                    default:
                        throw new InvalidOperationException();
                }
            }

            /// <summary>
            /// Check if a type can export model.
            /// </summary>
            /// <param name="t">Input type.</param>
            /// <returns>True if this type can be extracted to input field model. OTherwise return false.</returns>
            private static bool InputFieldExtractableType(Type t)
            {
                return PropertyItem.availablePropType.Contains(t) || t.IsEnum;
            }

            /// <summary>
            /// Create a input field type.
            /// </summary>
            /// <returns>Input field type matched with the property info.</returns>
            private InputFieldType GetInputTypeOfNullObject()
            {
                if (this.PropertyType.Equals(typeof(string)))
                {
                    return InputFieldType.String;
                }
                else if (this.PropertyType.Equals(typeof(CDateTime)))
                {
                    return InputFieldType.Date;
                }
                else
                {
                    return InputFieldType.Unknown;
                }
            }
        }
    }
}
