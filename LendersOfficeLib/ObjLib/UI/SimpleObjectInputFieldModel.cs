﻿// <copyright file="SimpleObjectInputFieldModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataAccess;

    /// <summary>
    /// Just an alias for dictionary object.
    /// </summary>
    public class SimpleObjectInputFieldModel : Dictionary<string, InputFieldModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleObjectInputFieldModel" /> class.
        /// </summary>
        public SimpleObjectInputFieldModel()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        /// <summary>
        /// Return the value of a field in model.
        /// </summary>
        /// <param name="field">Field name.</param>
        /// <returns>Return value of existed matched field. If field doesn't exist, return empty string. </returns>
        public string ValueFromModel(string field)
        {
            if (this.ContainsKey(field))
            {
                return this[field].Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Bind the value of a field model to object property.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <typeparam name="S">The second generic type parameter, for object property type.</typeparam>
        /// <param name="fieldname">Name of the field in json model.</param>
        /// <param name="target">The object need to be change property.</param>
        /// <param name="outExpr">The expression to identfy the object property.</param>
        public void BindObjectFromModel<T, S>(string fieldname, T target, Expression<Func<T, S>> outExpr)
        {
            if (this.ContainsKey(fieldname))
            {
                this[fieldname].BindObjectFromModel(target, outExpr);
            }
        }

        /// <summary>
        /// Calculate ZipCode.
        /// </summary>
        public void CalculateZipCode()
        {
            foreach (var kvp in this)
            {
                string fieldId = kvp.Key;
                if (kvp.Value.Type != InputFieldType.Zipcode)
                {
                    continue;
                }

                var addrList = ParseZipcode(kvp.Key, kvp.Value.Value);
                foreach (KeyValuePair<string, string> keyValPair in addrList)
                {
                    if (this.ContainsKey(keyValPair.Key))
                    {
                        this[keyValPair.Key].Value = keyValPair.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Use to calculate zipcode and return fields will be updated.
        /// </summary>
        /// <param name="fieldId">Zipcode field ID.</param>
        /// <param name="zipCode">Zipcode value.</param>
        /// <returns>List of KeyValuePair containt fields need to be updated and upting values of those field.</returns>
        private static List<KeyValuePair<string, string>> ParseZipcode(string fieldId, string zipCode)
        {
            List<KeyValuePair<string, string>> keyPairs = new List<KeyValuePair<string, string>>();
            int prefixLength = fieldId.IndexOf("Zip");
            var prefix = string.Empty;
            if (prefixLength > 0)
            {
                prefix = fieldId.Substring(0, prefixLength);
            }

            string county, city, state;
            if (Tools.GetCountyCityStateByZip(zipCode, out county, out city, out state))
            {
                keyPairs.Add(new KeyValuePair<string, string>(prefix + "State", state));
                keyPairs.Add(new KeyValuePair<string, string>(prefix + "City", city));
                keyPairs.Add(new KeyValuePair<string, string>(prefix + "County", county));
            }

            return keyPairs;
        }
    }
}