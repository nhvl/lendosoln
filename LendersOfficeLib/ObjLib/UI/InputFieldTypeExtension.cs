﻿// <copyright file="InputFieldTypeExtension.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System.Linq;

    /// <summary>
    /// A list of input type.
    /// </summary>
    public enum InputFieldType
    {
        /// <summary>
        /// Unknown input type.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// A string input type.
        /// </summary>
        String = 1,

        /// <summary>
        /// A multilines input type.
        /// </summary>
        TextArea = 2,

        /// <summary>
        /// A money input type.
        /// </summary>
        Money = 3,

        /// <summary>
        /// A percent input type.
        /// </summary>
        Percent = 4,

        /// <summary>
        /// A ssn input type.
        /// </summary>
        Ssn = 5,

        /// <summary>
        /// A phone input type.
        /// </summary>
        Phone = 6,

        /// <summary>
        /// A number input type.
        /// </summary>
        Number = 7,

        /// <summary>
        /// A checkbox input type.
        /// </summary>
        Checkbox = 8,

        /// <summary>
        /// A date input type.
        /// </summary>
        Date = 9,

        /// <summary>
        /// A drop down input type.
        /// </summary>
        DropDownList = 10,

        /// <summary>
        /// A locked input type.
        /// </summary>
        Lock = 11,

        /// <summary>
        /// A zipcode string, should be in 5 digit.
        /// </summary>
        Zipcode = 12,

        /// <summary>
        /// A radio button.
        /// </summary>
        Radio = 13,

        /// <summary>
        /// A text box with dropdown.
        /// </summary>
        StringWithDropDown = 14
    }

    /// <summary>
    /// Extend a checking method for InputFieldType enum.
    /// </summary>
    public static class InputFieldTypeExtension
    {
        /// <summary>
        /// List of fields that should be not treated as string.
        /// </summary>
        private static InputFieldType[] unstringFields = { InputFieldType.Checkbox, InputFieldType.Date, InputFieldType.Lock };

        /// <summary>
        /// List of fields that should have options.
        /// </summary>
        private static InputFieldType[] hasOptionsFields = { InputFieldType.DropDownList, InputFieldType.Radio, InputFieldType.StringWithDropDown };

        /// <summary>
        /// Check a field type is a string field type.
        /// </summary>
        /// <param name="fieldType">The type to be checked.</param>
        /// <returns>True if field type is string. Otherwise return false.</returns>
        public static bool IsStringField(this InputFieldType fieldType)
        {
            return !unstringFields.Contains(fieldType);
        }

        /// <summary>
        /// Check a field type has option.
        /// </summary>
        /// <param name="fieldType">The type to be checked.</param>
        /// <returns>True if field type has option. Otherwise return false.</returns>
        public static bool HasOptionField(this InputFieldType fieldType)
        {
            return hasOptionsFields.Contains(fieldType);
        }
    }
}
