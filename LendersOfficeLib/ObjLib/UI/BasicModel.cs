﻿// <copyright file="BasicModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// This basic model contains the essentials for a data model for the 2016 UI Redesign pages.
    /// </summary>
    public class BasicModel
    {
        /// <summary>
        /// Initializes a new instance of the BasicModel class.
        /// </summary>
        public BasicModel()
        {
            this.SummaryValues = new Dictionary<string, string>();
            this.Fields = new SimpleObjectInputFieldModel();
        }

        /// <summary>
        /// Gets or sets the list of id and value of the loan summary.
        /// </summary>
        /// <value>The list of id and value of the loan summary.</value>
        [JsonProperty("summary")]
        public Dictionary<string, string> SummaryValues { get; set; }

        /// <summary>
        /// Gets or sets a set of simple fields.
        /// </summary>
        /// <value>A set of simple fields.</value>
        [JsonProperty("fields")]
        public SimpleObjectInputFieldModel Fields { get; set; }

        /// <summary>
        /// Set the summary value of this loan.
        /// </summary>
        /// <param name="fieldId">Field id for loan summary.</param>
        /// <param name="value">Loan summary value.</param>
        public void AddSummary(string fieldId, string value)
        {
            if (this.SummaryValues == null)
            {
                this.SummaryValues = new Dictionary<string, string>();
            }

            this.SummaryValues[fieldId] = value;
        }
    }
}
