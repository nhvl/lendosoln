﻿// <copyright file="LqbInputModelAttribute.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: huyn
// Date: 1/27/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The attribute to add LqbInputModel to a property.
    /// </summary>
    public class LqbInputModelAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the LqbInputModelAttribute class.
        /// </summary>
        /// <param name="type">Optional input type for model.</param>
        /// <param name="name">Optional input type for name.</param>
        /// <param name="invalid">Optional input type for invalid.</param>
        /// <param name="enumType">Enum type for fetching options.</param>
        /// <param name="maxLength">Maximum length of the value.</param>
        /// <param name="required">Detected of a field is required.</param>
        public LqbInputModelAttribute(InputFieldType type = InputFieldType.Unknown, string name = null, bool invalid = false, Type enumType = null, int maxLength = 0, bool required = false)
        {
            this.Model = new LqbInputModel();
            this.Model.Type = type;
            this.Model.Name = name;
            this.Model.Invalid = invalid;
            this.Model.EnumType = enumType;
            this.Model.MaxLength = maxLength;
            this.Model.Required = required;
        }

        /// <summary>
        /// Gets or sets the input field model. It will be added to datalayer to detect if a property should use a custom in input model.
        /// </summary>
        /// <value>The input model.</value>
        public LqbInputModel Model { get; set; }
    }
}
