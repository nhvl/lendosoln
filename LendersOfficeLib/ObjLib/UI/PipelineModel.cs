﻿// <copyright file="PipelineModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   4/29/2016
// </summary>

namespace LendersOffice.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.Controllers;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOffice.Services;
    using Newtonsoft.Json;

    /// <summary>
    /// The data structure for the trades and pools page in the new UI.
    /// </summary>
    public class PipelineModel : BasicModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineModel"/> class.
        /// </summary>
        public PipelineModel()
            : base()
        {
            this.BrokerFields = new SimpleObjectInputFieldModel();
            this.BrokerAddress = new SimpleObjectInputFieldModel();
        }

        /// <summary>
        /// Gets or sets the needed information for displaying the pipeline tabs and their corresponding tables correctly.
        /// </summary>
        /// <value>An object containing the active index and properties for each tab pipeline.</value>
        [JsonProperty("PipelineView")]
        public CPipelineView PipelineView
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the data that will be displayed in a pipeline table.
        /// </summary>
        /// <value>An object containing the data that will be displayed in the pipeline table.</value>
        public TableInfo TableInfo { get; set; }

        /// <summary>
        /// Gets or sets a set of simple fields.
        /// </summary>
        /// <value>A set of simple fields.</value>
        [JsonProperty("brokerAddress")]
        public SimpleObjectInputFieldModel BrokerAddress { get; set; }

        /// <summary>
        /// Gets or sets a set of simple fields.
        /// </summary>
        /// <value>A set of simple fields.</value>
        [JsonProperty("broker")]
        public SimpleObjectInputFieldModel BrokerFields { get; set; }

        /// <summary>
        /// Gets or sets loan naming scheme options.
        /// </summary>
        /// <value>Loan naming scheme options.</value>
        [JsonProperty("namingOptions")]
        public Dictionary<string, object> NamingOptions { get; set; }

        /// <summary>
        /// Gets or sets a list of broker fair lending notice addresses.
        /// </summary>
        /// <value>A list of broker fair lending notice addresses.</value>
        [JsonProperty("fairLendNoticeAddress")]
        public IEnumerable<InputFieldModel> FairLendingNoticeAddress { get; set; }

        /// <summary>
        /// Gets or sets a list company liceneses.
        /// </summary>
        /// <value>A list of company liceneses.</value>
        [JsonProperty("ComLicenses")]
        public IEnumerable<Dictionary<string, object>> CompanyLicenses { get; set; }

        /// <summary>
        /// Gets or sets a template of company licenses.
        /// </summary>
        /// <value>A template of company licenses.</value>
        public Dictionary<string, object> CompanyLicenseTemplate { get; set; }

        /// <summary>
        /// Gets or sets a template of white list IP addresses.
        /// </summary>
        /// <value>A template of white list IP addresses.</value>
        public Dictionary<string, object> IpWhiteListTemplate { get; set; }

        /// <summary>
        /// Gets or sets a list of white list IP addresses.
        /// </summary>
        /// <value>A list of white list IP addresses.</value>
        public IEnumerable<SimpleObjectInputFieldModel> IpWhiteList { get; set; }

        /// <summary>
        /// Gets or sets a list of credit report agents.
        /// </summary>
        /// <value>A list of credit report agents.</value>
        [JsonProperty("creditAgents")]
        public IEnumerable<SimpleObjectInputFieldModel> CreditReportAgents { get; set; }

        /// <summary>
        /// Gets or sets a password option model.
        /// </summary>
        /// <value>A password option model.</value>
        public SimpleObjectInputFieldModel PasswordOption { get; set; }

        /// <summary>
        /// Gets or sets a hidden CRA list.
        /// </summary>
        /// <value>A list of hidden CRA.</value>
        public IEnumerable<KeyValuePair<string, string>> HiddenCras { get; set; }

        /// <summary>
        /// Gets or sets CRA list dropdown.
        /// </summary>
        /// <value>CRA list dropdown.</value>
        public InputFieldModel CraList { get; set; }

        /// <summary>
        /// Gets or sets a list of branch prefixes.
        /// </summary>
        /// <value>A list of branch prefixes.</value>
        public IEnumerable<SimpleObjectInputFieldModel> BranchPrefixes { get; set; }

        /// <summary>
        /// Adds a new key value pair to the Loan Summary dictionary.
        /// </summary>
        /// <param name="key">The name of the field.</param>
        /// <param name="value">The value of the field.</param>
        public void AddSummaryField(string key, string value)
        {
            if (this.SummaryValues == null)
            {
                this.SummaryValues = new Dictionary<string, string>();
            }

            this.SummaryValues[key] = value;
        }
    }
}
