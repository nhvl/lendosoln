﻿// <copyright file="ListModel.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   12/3/2015
// </summary>

namespace LendersOffice.UI
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Data model for a list.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ListModel
    {
        /// <summary>
        /// Gets or sets the real data of a list.
        /// </summary>
        /// <value>List of values.</value>
        [JsonProperty("data")]
        public List<Dictionary<string, object>> Values { get; set; }

        /// <summary>
        /// Gets or sets the example of an empty record.
        /// </summary>
        /// <value>Input model of template.</value>
        [JsonProperty("template")]
        public object Template { get; set; }

        /// <summary>
        /// Gets or sets the extra properties.
        /// </summary>
        /// <value>Config object.</value>
        [JsonProperty("config")]
        public object Config { get; set; }
    }
}
