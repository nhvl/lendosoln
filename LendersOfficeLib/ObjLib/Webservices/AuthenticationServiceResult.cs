﻿namespace LendersOffice.ObjLib.Webservices
{
    using System.Xml;

    /// <summary>
    /// The AuthenticationServiceResult's purpose is to generate the response for Authentication requests.
    /// </summary>
    public class AuthenticationServiceResult : ServiceResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationServiceResult"/> class.
        /// </summary>
        /// <param name="userTagName">The tag used for writing the response, which depends on the user's type. <see cref="AuthenticationServiceResult.UserTagName"></see>"/>.</param>
        /// <param name="encryptedTicketData">The encrypted ticket used for authenticating the user.</param>
        /// <param name="site">The site code indicating where authentication ticket is valid.</param>
        public AuthenticationServiceResult(string userTagName, string encryptedTicketData, string site)
        {
            this.UserTagName = userTagName;
            this.EncryptedTicketData = encryptedTicketData;
            this.Site = site;
        }

        /// <summary>
        /// Gets the tag name for the type of User that will be authentication.  For Example, 'PML_USER_TICKET'.
        /// </summary>
        /// <value>The tag for the element generated in the xml response.</value>
        public string UserTagName { get; private set; }

        /// <summary>
        /// Gets the Encrypted ticket used for Authentication.
        /// </summary>
        /// <value>The encrypted authentication ticket.</value>
        public string EncryptedTicketData { get; private set; }

        /// <summary>
        /// Gets the site code indicating where the authentication ticket is valid.
        /// </summary>
        public string Site { get; private set; }

        /// <summary>
        /// Takes in the given writer, and writes the user's credentials, customer code, and encrypted ticket.
        /// </summary>
        /// <param name="writer">The writer to write the response to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            writer.WriteStartElement(this.UserTagName);
            writer.WriteAttributeString("EncryptedTicket", this.EncryptedTicketData);
            writer.WriteAttributeString("Site", this.Site);
            writer.WriteEndElement();
        }
    }
}
