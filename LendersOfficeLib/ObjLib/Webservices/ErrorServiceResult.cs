﻿// <copyright file = "ErrorServiceResult.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Represents a error response in webservices.
    /// </summary>
    public sealed class ErrorServiceResult : ServiceResult
    {
        /// <summary>
        /// A value indicating whether the root element in the response should be LOXml.
        /// </summary>
        private bool includeLoXmlFormatTag;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorServiceResult"/> class.
        /// </summary>
        /// <param name="exception">A CBaseException with the error information.</param>
        /// <param name="includeLoXmlFormatRootTag">A value indicating whether the response should use the LOXml root.</param>
        public ErrorServiceResult(CBaseException exception, bool includeLoXmlFormatRootTag = false)
            : this(exception.UserMessage, includeLoXmlFormatRootTag)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorServiceResult"/> class.
        /// </summary>
        /// <param name="error">The error message to return.</param>
        /// <param name="includeLoXmlFormatRootTag">A value indicating whether the response should use the LOXml root.</param>
        public ErrorServiceResult(string error, bool includeLoXmlFormatRootTag = false)
            : this(includeLoXmlFormatRootTag)
        {
            this.AppendError(error);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorServiceResult"/> class.
        /// </summary>
        /// <param name="errors">The initial errors to include.</param>
        /// <param name="includeLoXmlFormatRootTag">A value indicating whether the response should use the LOXml root.</param>
        public ErrorServiceResult(IEnumerable<string> errors, bool includeLoXmlFormatRootTag = false)
            : this(includeLoXmlFormatRootTag)
        {
            foreach (var error in errors)
            {
                this.AppendError(error);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorServiceResult"/> class.
        /// </summary>
        /// <param name="includeLoXmlFormatRootTag">Whether the LOXml root tag should be included.</param>
        public ErrorServiceResult(bool includeLoXmlFormatRootTag = false)
        {
            this.Status = ServiceResultStatus.Error;
            this.includeLoXmlFormatTag = includeLoXmlFormatRootTag;
        }

        /// <summary>
        /// Gets a value indicating whether the root tag for the response should be LoXml.
        /// </summary>
        /// <returns>A value indicating whether the response root should be LoXmlFormat.</returns>
        protected override bool IncludeLOXmlFormatRoot()
        {
            return this.includeLoXmlFormatTag;
        }
    }
}
