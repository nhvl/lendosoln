﻿// <copyright file = "DocumentGenerationOptions.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System.Collections.Generic;
    using LendersOffice.Common;

    /// <summary>
    /// Contains the possible options for generating documents via web services.
    /// </summary>
    public class DocumentGenerationOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentGenerationOptions"/> class.
        /// </summary>
        public DocumentGenerationOptions()
        {
            this.ElectronicDelivery = new ElectronicDeliveryOptions();
            this.AdditionalServices = new AdditionalServicesOptions();
            this.SpecifiedForms = new SpecifiedFormsOptions();
            this.EmailSettings = new EmailOptions();
        }

        /// <summary>
        /// Gets or sets the electronic delivery options.
        /// </summary>
        /// <value>The electronic delivery options.</value>
        public ElectronicDeliveryOptions ElectronicDelivery
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the additional service options.
        /// </summary>
        /// <value>The additional service options.</value>
        public AdditionalServicesOptions AdditionalServices
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the options for forms that are supposed to be generated.
        /// </summary>
        /// <value>The options for forms that are supposed to be generated.</value>
        public SpecifiedFormsOptions SpecifiedForms
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the email settings for document generation via webservice.
        /// </summary>
        /// <value>The email settings for document generation via webservice.</value>
        public EmailOptions EmailSettings
        {
            get;
            set;
        }

        /// <summary>
        /// Deserializes the xml string into a <see cref="DocumentGenerationOptions"/> object.
        /// </summary>
        /// <param name="xmlContent">The xml string to deserialize.</param>
        /// <returns>The deserializes object if successful.</returns>
        public static DocumentGenerationOptions DeserializeFromXml(string xmlContent)
        {
            return SerializationHelper.XmlDeserialize(xmlContent, typeof(DocumentGenerationOptions)) as DocumentGenerationOptions;
        }

        /// <summary>
        /// Serializes this object into xml.
        /// </summary>
        /// <returns>The xml string.</returns>
        public string ToXml()
        {
            return SerializationHelper.XmlSerialize(this);
        }
    }
}
