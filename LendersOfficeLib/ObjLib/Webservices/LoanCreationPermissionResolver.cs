﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines a permission resolver for evaluating a user's ability to create various sorts of loan files.
    /// </summary>
    public abstract class LoanCreationPermissionResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCreationPermissionResolver"/> class.
        /// </summary>
        /// <param name="principal">The user who will be performing the action.</param>
        public LoanCreationPermissionResolver(AbstractUserPrincipal principal)
        {
            this.Principal = principal;
        }

        /// <summary>
        /// Gets a value indicating whether the user can create a lead.
        /// </summary>
        /// <value>A value indicating if the user can create a lead.</value>
        public abstract bool CanCreateLead { get; }

        /// <summary>
        /// Gets a value indicating whether the user can create a test file.
        /// </summary>
        /// <value>A value indicating if the user can create a test file.</value>
        public abstract bool CanCreateTestFile { get; }

        /// <summary>
        /// Gets a value indicating whether the user can create a standard loan file.
        /// </summary>
        /// <value>A value indicating if the user can create a standard loan file.</value>
        public abstract bool CanCreateVanillaLoan { get; }

        /// <summary>
        /// Gets the user who will be performing the action.
        /// </summary>
        /// <value>The user who will be performing the action.</value>
        protected AbstractUserPrincipal Principal { get; }

        /// <summary>
        /// Gets an instance of <see cref="LoanCreationPermissionResolver"/> to resolve permissions for the specified user.
        /// </summary>
        /// <param name="principal">The user in need of the permission check.</param>
        /// <returns>A permission resolver for the specified user.</returns>
        internal static LoanCreationPermissionResolver GetPermissionResolverForUser(AbstractUserPrincipal principal)
        {
            if (principal.Type == "B")
            {
                return new LendersOfficeUserPermissionResolver(principal);
            }
            else if (principal.Type == "P")
            {
                return new TpoUserPermissionResolver(principal);
            }
            else if (principal.Type == "D")
            {
                return new ConsumerPortalUserPermissionResolver(principal);
            }

            Tools.LogWarning($"Unhandled principal of type {principal.GetType()} during loan creation permission check.\r\n"
                + $"UserId={principal.UserId}; Login={principal.LoginNm}; Type={principal.Type}. User will be permitted to create the loan.");
            return new AlwaysPassResolver(principal);
        }

        /// <summary>
        /// Defines a permission resolution handling that allows all actions.
        /// </summary>
        public class AlwaysPassResolver : LoanCreationPermissionResolver
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AlwaysPassResolver"/> class.
            /// </summary>
            /// <param name="principal">The user who will be performing the action.</param>
            public AlwaysPassResolver(AbstractUserPrincipal principal)
                : base(principal)
            {
            }

            /// <summary>
            /// Gets a value indicating whether the user can create a lead.
            /// </summary>
            /// <value>A value indicating if the user can create a lead.</value>
            public override bool CanCreateLead => true;

            /// <summary>
            /// Gets a value indicating whether the user can create a test file.
            /// </summary>
            /// <value>A value indicating if the user can create a test file.</value>
            public override bool CanCreateTestFile => true;

            /// <summary>
            /// Gets a value indicating whether the user can create a standard loan file.
            /// </summary>
            /// <value>A value indicating if the user can create a standard loan file.</value>
            public override bool CanCreateVanillaLoan => true;
        }

        /// <summary>
        /// Defines the permission resolution handling for a Lender's Office/standard LQB user.
        /// </summary>
        private class LendersOfficeUserPermissionResolver : LoanCreationPermissionResolver
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LendersOfficeUserPermissionResolver"/> class.
            /// </summary>
            /// <param name="principal">The user who will be performing the action.</param>
            public LendersOfficeUserPermissionResolver(AbstractUserPrincipal principal)
                : base(principal)
            {
            }

            /// <summary>
            /// Gets a value indicating whether the user can create a lead.
            /// </summary>
            /// <value>A value indicating if the user can create a lead.</value>
            public override bool CanCreateLead => this.Principal.HasPermission(Permission.AllowCreatingNewLeadFiles);

            /// <summary>
            /// Gets a value indicating whether the user can create a test file.
            /// </summary>
            /// <value>A value indicating if the user can create a test file.</value>
            public override bool CanCreateTestFile => this.Principal.HasPermission(Permission.AllowCreatingTestLoans);

            /// <summary>
            /// Gets a value indicating whether the user can create a standard loan file.
            /// </summary>
            /// <value>A value indicating if the user can create a standard loan file.</value>
            public override bool CanCreateVanillaLoan => this.Principal.HasPermission(Permission.AllowCreatingNewLoanFiles);
        }

        /// <summary>
        /// Defines the permission resolution handling for a TPO Portal/PML user.
        /// </summary>
        private class TpoUserPermissionResolver : LoanCreationPermissionResolver
        {
            /// <summary>
            /// A cached instance of the originating company of the TPO User.
            /// </summary>
            private readonly Lazy<PmlBroker> originatingCompany;

            /// <summary>
            /// Initializes a new instance of the <see cref="TpoUserPermissionResolver"/> class.
            /// </summary>
            /// <param name="principal">The user who will be performing the action.</param>
            public TpoUserPermissionResolver(AbstractUserPrincipal principal)
                : base(principal)
            {
                this.originatingCompany = new Lazy<PmlBroker>(() => PmlBroker.RetrievePmlBrokerById(principal.PmlBrokerId, principal.BrokerId));
            }

            /// <summary>
            /// Gets a value indicating whether the user can create a lead.
            /// </summary>
            /// <value>A value indicating if the user can create a lead.</value>
            public override bool CanCreateLead => false;

            /// <summary>
            /// Gets a value indicating whether the user can create a test file.
            /// </summary>
            /// <value>A value indicating if the user can create a test file.</value>
            public override bool CanCreateTestFile => false;

            /// <summary>
            /// Gets a value indicating whether the user can create a standard loan file.
            /// </summary>
            /// <value>A value indicating if the user can create a standard loan file.</value>
            public override bool CanCreateVanillaLoan
            {
                get
                {
                    return this.Principal.BrokerDB.IsAllowExternalUserToCreateNewLoan
                        && this.Principal.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.Pml_LoanOfficer, E_RoleT.Pml_BrokerProcessor, E_RoleT.Pml_Secondary, E_RoleT.Pml_PostCloser })
                        && (this.Principal.PortalMode != E_PortalMode.Broker || (this.originatingCompany.Value.Roles.Contains(E_OCRoles.Broker) && this.Principal.HasPermission(Permission.AllowCreatingWholesaleChannelLoans)))
                        && (this.Principal.PortalMode != E_PortalMode.MiniCorrespondent || (this.originatingCompany.Value.Roles.Contains(E_OCRoles.MiniCorrespondent) && this.Principal.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans)))
                        && (this.Principal.PortalMode != E_PortalMode.Correspondent || (this.originatingCompany.Value.Roles.Contains(E_OCRoles.Correspondent) && this.Principal.HasPermission(Permission.AllowCreatingCorrChannelLoans)));
                }
            }
        }

        /// <summary>
        /// Defines the permission resolution handling for a Consumer Portal user.
        /// </summary>
        private class ConsumerPortalUserPermissionResolver : LoanCreationPermissionResolver
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ConsumerPortalUserPermissionResolver"/> class.
            /// </summary>
            /// <param name="principal">The user who will be performing the action.</param>
            public ConsumerPortalUserPermissionResolver(AbstractUserPrincipal principal)
                : base(principal)
            {
            }

            /// <summary>
            /// Gets a value indicating whether the user can create a lead.
            /// </summary>
            /// <value>A value indicating if the user can create a lead.</value>
            public override bool CanCreateLead => true;

            /// <summary>
            /// Gets a value indicating whether the user can create a test file.
            /// </summary>
            /// <value>A value indicating if the user can create a test file.</value>
            public override bool CanCreateTestFile => false;

            /// <summary>
            /// Gets a value indicating whether the user can create a standard loan file.
            /// </summary>
            /// <value>A value indicating if the user can create a standard loan file.</value>
            public override bool CanCreateVanillaLoan => true;
        }
    }
}
