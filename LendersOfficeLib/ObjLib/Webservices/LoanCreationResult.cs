﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;

    /// <summary>
    /// Represents the result of a loan creation operation.
    /// </summary>
    public class LoanCreationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCreationResult"/> class.
        /// </summary>
        /// <param name="isSuccessful">A value indicating whether the loan creation was successful.</param>
        /// <param name="errorMessage">The error message returned if the loan creation was unsuccessful.</param>
        /// <param name="loanIdentifier">The <see cref="DataAccess.CPageData.sLId"/> of the created loan.</param>
        /// <param name="loanName">The <see cref="DataAccess.CPageData.sLNm"/> of the created loan.</param>
        /// <param name="sourceLoanIdentifier">The <see cref="DataAccess.CPageData.sLId"/> of the template used to create the loan, or null if no template was used.</param>
        private LoanCreationResult(bool isSuccessful, string errorMessage, Guid loanIdentifier, string loanName, Guid? sourceLoanIdentifier)
        {
            this.IsSuccessful = isSuccessful;
            this.ErrorMessage = errorMessage;
            this.LoanIdentifier = loanIdentifier;
            this.LoanName = loanName;
            this.SourceLoanIdentifier = sourceLoanIdentifier;
        }

        /// <summary>
        /// Gets a value indicating whether the loan creation was successful.
        /// </summary>
        /// <value>A value indicating whether the loan creation was successful.</value>
        public bool IsSuccessful { get; }

        /// <summary>
        /// Gets the error message returned if the loan creation was unsuccessful.
        /// </summary>
        /// <value>The error message returned if the loan creation was unsuccessful.</value>
        public string ErrorMessage { get; }

        /// <summary>
        /// Gets the <see cref="DataAccess.CPageData.sLId"/> of the created loan.
        /// </summary>
        /// <value>The <see cref="DataAccess.CPageData.sLId"/> of the created loan.</value>
        public Guid LoanIdentifier { get; }

        /// <summary>
        /// Gets the <see cref="DataAccess.CPageData.sLNm"/> of the created loan.
        /// </summary>
        /// <value>The <see cref="DataAccess.CPageData.sLNm"/> of the created loan.</value>
        public string LoanName { get; }

        /// <summary>
        /// Gets the <see cref="DataAccess.CPageData.sLId"/> of the template used to create the loan, or null if no template was used.
        /// </summary>
        /// <value>The <see cref="DataAccess.CPageData.sLId"/> of the template used to create the loan, or null if no template was used.</value>
        public Guid? SourceLoanIdentifier { get; }

        /// <summary>
        /// Creates a creation result instance for a successful loan creation.
        /// </summary>
        /// <param name="loanIdentifier">The <see cref="DataAccess.CPageData.sLId"/> of the created loan.</param>
        /// <param name="loanName">The <see cref="DataAccess.CPageData.sLNm"/> of the created loan.</param>
        /// <param name="sourceLoanIdentifier">The <see cref="DataAccess.CPageData.sLId"/> of the template used to create the loan, or null if no template was used.</param>
        /// <returns>The creation result instance.</returns>
        public static LoanCreationResult Success(Guid loanIdentifier, string loanName, Guid? sourceLoanIdentifier)
        {
            return new LoanCreationResult(true, null, loanIdentifier, loanName, sourceLoanIdentifier);
        }

        /// <summary>
        /// Creates a creation result instance for an unsuccessful loan creation.
        /// </summary>
        /// <param name="errorMessage">The error message to send to the user.</param>
        /// <returns>The creation result instance.</returns>
        public static LoanCreationResult Failure(string errorMessage)
        {
            return new LoanCreationResult(false, errorMessage, default(Guid), default(string), default(Guid?));
        }
    }
}
