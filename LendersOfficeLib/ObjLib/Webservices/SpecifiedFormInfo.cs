﻿// <copyright file = "SpecifiedFormInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System.Xml.Serialization;

    /// <summary>
    /// A class that holds the information needed to specify the forms to generate.
    /// </summary>
    public class SpecifiedFormInfo
    {
        /// <summary>
        /// Gets or sets the form's borrower number.
        /// </summary>
        /// <value>The form's borrower number.</value>
        [XmlAttribute]
        public string BorrowerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form's id.
        /// </summary>
        /// <value>The form's id.</value>
        [XmlAttribute]
        public string Id
        {
            get;
            set;
        }
    }
}
