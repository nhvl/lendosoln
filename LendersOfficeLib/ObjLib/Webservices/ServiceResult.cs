﻿namespace LendersOffice.ObjLib.Webservices
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;

    /// <summary>
    /// Generates consistent webservice result xml. Classes that inherit 
    /// can overwrite the content in the result tag. 
    /// </summary>
    public class ServiceResult
    {
        /// <summary>
        /// List of warnings for the service result.
        /// </summary>
        private List<string> warningList = new List<string>();

        /// <summary>
        /// List of errors for the service result.
        /// </summary>
        private List<string> errorList = new List<string>();

        /// <summary>
        /// Gets a list of additional property attributes (name, value) to add to the result element.
        /// </summary>
        public Dictionary<string, string> AdditionalProperties { get; private set; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the service result status.
        /// </summary>
        /// <value>The status of the service call.</value>
        public ServiceResultStatus Status { get; set; }

        /// <summary>
        /// Gets a value indicating whether the result contains errors.
        /// </summary>
        /// <value>A boolean indicating whether the result contains errors.</value>
        public bool HasErrors => this.ErrorList.Any();

        /// <summary>
        /// Gets the raw error list.
        /// </summary>
        /// <returns>The raw error list.</returns>
        protected IReadOnlyCollection<string> ErrorList
        {
            get
            {
                return this.errorList;
            }
        }

        /// <summary>
        /// Add a new warning to the service result.
        /// </summary>
        /// <param name="warning">The warning to add to the service result.</param>
        public void AppendWarning(string warning)
        {
            if (string.IsNullOrEmpty(warning))
            {
                return;
            }

            this.warningList.Add(warning);
        }

        /// <summary>
        /// Add a new error to the service result.
        /// </summary>
        /// <param name="error">The error to add to the service result.</param>
        public void AppendError(string error)
        {
            if (string.IsNullOrEmpty(error))
            {
                return;
            }

            this.errorList.Add(error);
        }

        /// <summary>
        /// Generates the service result.
        /// </summary>
        /// <returns>The webservice response in the proper format.</returns>
        public string ToResponse()
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            XmlWriter writer = XmlWriter.Create(sb, settings);
            bool includeLOXmlFormatTag = this.IncludeLOXmlFormatRoot();
            
            if (includeLOXmlFormatTag)
            {
                writer.WriteStartElement("LOXmlFormat");
                writer.WriteAttributeString("version", this.GetLoXmlFormatVersion());
            }

            writer.WriteStartElement("result");
            if (this.Status == ServiceResultStatus.Auto && this.errorList.Count > 0)
            {
                this.Status = ServiceResultStatus.Error;
            }
            else if (this.Status == ServiceResultStatus.Auto && this.warningList.Count > 0)
            {
                this.Status = ServiceResultStatus.OKWithWarning;
            }
            else if (this.Status == ServiceResultStatus.Auto)
            {
                this.Status = ServiceResultStatus.OK;
            }

            writer.WriteAttributeString("status", this.Status.ToString());
            foreach (var attributeVal in this.AdditionalProperties)
            {
                if (attributeVal.Key == "status")
                {
                    continue;
                }

                writer.WriteAttributeString(attributeVal.Key, attributeVal.Value);
            }

            switch (this.Status)
            {
                case ServiceResultStatus.OK:
                    // NO-OP
                    break;
                case ServiceResultStatus.OKWithWarning:
                    writer.WriteStartElement("Warnings");

                    foreach (string warning in this.warningList)
                    {
                        writer.WriteElementString("Warning", warning);
                    }

                    writer.WriteEndElement(); // </Warnings>
                    break;
                case ServiceResultStatus.Error:
                    writer.WriteStartElement("Errors");

                    foreach (string error in this.errorList)
                    {
                        writer.WriteElementString("Error", error);
                    }

                    writer.WriteEndElement(); // </Errors>
                    break;

                default:
                    throw new UnhandledEnumException(this.Status);
            }

            this.WriteResponseResult(writer);

            writer.WriteEndElement(); // </result>

            if (includeLOXmlFormatTag)
            {
                writer.WriteEndElement();
            }

            writer.Flush();
            return sb.ToString();
        }

        /// <summary>
        /// Implementing classes can override this to return more inside the result tag.
        /// </summary>
        /// <param name="writer">The xml writer that should be used to write the response.</param>
        protected virtual void WriteResponseResult(XmlWriter writer)
        {
        }

        /// <summary>
        /// Gets a value indicating whether the root element should be LOXmlFormat. 
        /// </summary>
        /// <returns>This is always false.</returns>
        protected virtual bool IncludeLOXmlFormatRoot()
        {
            return false;
        }

        /// <summary>
        /// Gets a value indicating the LoXml version number. Can be overwritten in child class.
        /// </summary>
        /// <returns>Always returns 1.0.</returns>
        protected virtual string GetLoXmlFormatVersion()
        {
            return "1.0";
        }
    }
}
