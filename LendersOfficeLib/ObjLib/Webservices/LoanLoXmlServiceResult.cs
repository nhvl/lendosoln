﻿namespace LendersOffice.ObjLib.Webservices
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// LO Xml service result. This one is for outputting loan values.
    /// </summary>
    public class LoanLoXmlServiceResult : LoXmlServiceResult
    {
        /// <summary>
        /// The elements to place inside of the loan element.
        /// </summary>
        private List<XElement> loanElements;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanLoXmlServiceResult"/> class.
        /// </summary>
        public LoanLoXmlServiceResult()
        {
            this.loanElements = new List<XElement>();
        }

        /// <summary>
        /// Adds the XElement to the list of elements to place inside the loan element.
        /// </summary>
        /// <param name="element">The element to place in the loan element.</param>
        public void AddLoanElement(XElement element)
        {
            this.loanElements.Add(element);
        }

        /// <summary>
        /// Writes the xml elements to the xml writer. 
        /// This will add all elements add to the loanElement list as children of a loan element only if the status is OK.
        /// </summary>
        /// <param name="writer">The writer to write to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            base.WriteResponseResult(writer);

            if (this.Status == ServiceResultStatus.OK)
            {
                XElement loanElement = new XElement("loan");
                foreach (var element in this.loanElements)
                {
                    loanElement.Add(element);
                }

                loanElement.WriteTo(writer);
            }
        }
    }
}
