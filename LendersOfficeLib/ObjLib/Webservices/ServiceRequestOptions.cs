﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using LendersOffice.Conversions;

    /// <summary>
    /// This class is intended to serve as an object form of the options we read passed into a web service.
    /// </summary>
    /// <example>
    /// See <c>Loan.asmx::CreateWithOptions</c> for a simple example of a method needing this.
    /// The following is the intended sort of XML to be parsed here:
    /// <![CDATA[
    /// <LOXmlFormat version="1.0">
    ///     <field id="key1">value1</field>
    ///     <field id="key2">value2</field>
    /// </LOXmlFormat>
    /// ]]>
    /// </example>
    public class ServiceRequestOptions
    {
        /// <summary>
        /// The <c>LOXml</c> used to generate the current instance, for logging.
        /// </summary>
        private string sourceLOXml;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceRequestOptions"/> class.
        /// </summary>
        /// <param name="fields">The field data from the options.</param>
        /// <param name="collections">The collection items from the options xml.</param>
        /// <param name="sourceLOXml">The <c>LOXml</c> used to generate the current instance.</param>
        private ServiceRequestOptions(IReadOnlyDictionary<string, string> fields, IReadOnlyDictionary<string, List<ServiceRequestOptions>> collections, string sourceLOXml)
        {
            this.Fields = fields;
            this.Collections = collections;
            this.sourceLOXml = sourceLOXml;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ServiceRequestOptions" /> class from being created.
        /// </summary>
        private ServiceRequestOptions()
        {
            this.Fields = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            this.Collections = new Dictionary<string, List<ServiceRequestOptions>>(StringComparer.OrdinalIgnoreCase);
            this.sourceLOXml = string.Empty;
        }

        /// <summary>
        /// Gets the field data of the options.
        /// </summary>
        /// <value>The field data of the options.</value>
        public IReadOnlyDictionary<string, string> Fields { get; }

        /// <summary>
        /// Gets the collection items included in the options.
        /// </summary>
        /// <value>The collection items included in the options.</value>
        public IReadOnlyDictionary<string, List<ServiceRequestOptions>> Collections
        {
            get;
        }

        /// <summary>
        /// Parses a request options <c>LOXmlFormat</c> element string into its object form.  This will include parsing the empty string as a default instance.
        /// </summary>
        /// <param name="requestOptionsLOXml">The XML of the request options, or blank for a default instance.</param>
        /// <returns>A parsing result to wrap both the resulting object and any error that may have occurred during parsing.</returns>
        public static ParseResult<ServiceRequestOptions> ParseRequestOptions(string requestOptionsLOXml)
        {
            if (string.IsNullOrEmpty(requestOptionsLOXml))
            {
                return ParseResult<ServiceRequestOptions>.Success(requestOptionsLOXml, new ServiceRequestOptions());
            }

            XElement rootElement;
            try
            {
                rootElement = XElement.Parse(requestOptionsLOXml);
            }
            catch (System.Xml.XmlException)
            {
                return ParseResult<ServiceRequestOptions>.Failure(requestOptionsLOXml, ErrorMessages.WebServices.InvalidInputXml);
            }

            if (rootElement.Name.LocalName != "LOXmlFormat")
            {
                return ParseResult<ServiceRequestOptions>.Failure(requestOptionsLOXml, ErrorMessages.WebServices.InvalidInputXml);
            }

            return ParseOptions(rootElement);
        }

        /// <summary>
        /// Generates the XML to log, given a few fields to mask.
        /// </summary>
        /// <param name="fieldsToMask">The field ids to censor, provided they have non-blank values.</param>
        /// <returns>The XML to log.</returns>
        public string GenerateLogLOXml(string[] fieldsToMask)
        {
            if (string.IsNullOrEmpty(this.sourceLOXml))
            {
                return string.Empty;
            }

            if (fieldsToMask == null)
            {
                return this.sourceLOXml;
            }

            var rootElement = XElement.Parse(this.sourceLOXml);
            foreach (var fieldElement in rootElement.Descendants("field"))
            {
                string id = fieldElement.Attribute("id")?.Value;
                if (fieldsToMask.Contains(id, StringComparer.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(fieldElement.Value))
                {
                    fieldElement.Value = Constants.ConstAppDavid.FakePasswordDisplay;
                }
            }

            return rootElement.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Parses the fields and collections in the xml.
        /// </summary>
        /// <param name="rootElement">The parsed options xml.</param>
        /// <returns>The results of parsing the options xml.</returns>
        private static ParseResult<ServiceRequestOptions> ParseOptions(XElement rootElement)
        {
            Dictionary<string, string> fields = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, List<ServiceRequestOptions>> collections = new Dictionary<string, List<ServiceRequestOptions>>(StringComparer.OrdinalIgnoreCase);
            var rootElementString = rootElement.ToString(SaveOptions.DisableFormatting);

            foreach (XElement fieldElement in rootElement.Elements("field"))
            {
                string id = fieldElement.Attribute("id")?.Value;
                if (string.IsNullOrEmpty(id) || fields.ContainsKey(id))
                {
                    return ParseResult<ServiceRequestOptions>.Failure(rootElementString, "Expected unique id attributes on each field element.");
                }

                fields.Add(id, fieldElement.Value);
            }

            foreach (XElement collectionElement in rootElement.Elements("collection"))
            {
                string id = collectionElement.Attribute("id")?.Value;
                if (string.IsNullOrEmpty(id) || collections.ContainsKey(id))
                {
                    return ParseResult<ServiceRequestOptions>.Failure(rootElementString, "Expected unique id attributes on each collection element.");
                }

                List<ServiceRequestOptions> records = new List<ServiceRequestOptions>();
                foreach (var recordElement in collectionElement.Elements("record"))
                {
                    ParseResult<ServiceRequestOptions> recordFields = ServiceRequestOptions.ParseOptions(recordElement);
                    if (!recordFields.IsSuccessful)
                    {
                        return ParseResult<ServiceRequestOptions>.Failure(rootElementString, ErrorMessages.WebServices.InvalidInputXml);
                    }

                    records.Add(recordFields.ValueOrDefault);
                }

                collections.Add(id, records);
            }

            return ParseResult<ServiceRequestOptions>.Success(rootElementString, new ServiceRequestOptions(fields, collections, rootElementString));
        }
    }
}
