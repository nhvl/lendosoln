﻿// <copyright file = "DocumentFrameworkSuccessfulAuditTicket.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Runtime.Serialization;
    using Common;
    using DataAccess;

    /// <summary>
    /// This class holds metadata associated with a LQB Document Framework audit that executed successfully. This is required in order to generate documents via web services.
    /// </summary>
    [Serializable]
    public class DocumentFrameworkSuccessfulAuditTicket : ISerializable
    {
        /// <summary>
        /// The amount of time we store this ticket in the auto expire text cache.
        /// </summary>
        private static readonly TimeSpan ExpirationTimeSpan = new TimeSpan(0, 5, 0);

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentFrameworkSuccessfulAuditTicket"/> class.
        /// </summary>
        /// <param name="userId">The LQB user id to which this ticket is issued.</param>
        /// <param name="loanId">The loan id to which this ticket is issued.</param>
        /// <param name="vendorId">The vendor id to which this ticket is issued.</param>
        /// <param name="vendorUserName">The vendor user name to which this ticket is issued.</param>
        /// <param name="vendorPassword">The vendor password to which this ticket is issued.</param>
        /// <param name="vendorAccountId">The vendor account ID to which this ticket is issued.</param>
        /// <param name="packageId">The package id to which this ticket is issued.</param>
        /// <param name="planCodeId">The plan code id to which this ticket is issued.</param>
        public DocumentFrameworkSuccessfulAuditTicket(Guid userId, Guid loanId, Guid vendorId, string vendorUserName, string vendorPassword, string vendorAccountId, string packageId, string planCodeId)
        {
            this.UserId = userId;
            this.LoanId = loanId;
            this.VendorId = vendorId;
            this.VendorUserName = vendorUserName;
            this.VendorPasswordEncrypted = EncryptionHelper.EncryptDocumentFramework(vendorPassword);
            this.VendorAccountId = vendorAccountId;
            this.PackageId = packageId;
            this.PlanCodeId = planCodeId;
            this.IssuedDate = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentFrameworkSuccessfulAuditTicket"/> class.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination <see cref="StreamingContext"/> for this serialization. </param>
        public DocumentFrameworkSuccessfulAuditTicket(SerializationInfo info, StreamingContext context)
        {
            this.UserId = (Guid)info.GetValue("UserId", typeof(Guid));
            this.LoanId = (Guid)info.GetValue("LoanId", typeof(Guid));
            this.VendorId = (Guid)info.GetValue("VendorId", typeof(Guid));
            this.VendorUserName = info.GetString("VendorUserName");
            this.VendorPasswordEncrypted = (byte[])info.GetValue("VendorPasswordEncrypted", typeof(byte[]));
            this.VendorAccountId = info.GetString("VendorAccountId");
            this.PackageId = info.GetString("PackageId");
            this.PlanCodeId = info.GetString("PlanCodeId");
            this.IssuedDate = new DateTime((long)info.GetValue("IssuedDate", typeof(long)));
        }

        /// <summary>
        /// Gets the date in which the document framework audit ticket was originally issued.
        /// </summary>
        /// <value>The date in which the document framework audit ticket was originally issued.</value>
        public DateTime IssuedDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the LQB userId to which this ticket is issued.
        /// </summary>
        /// <value>The LQB user id to which this ticket is ussed.</value>
        public Guid UserId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the loan id to which this ticket is issued.
        /// </summary>
        /// <value>The loan id to which this ticket is ussed.</value>
        public Guid LoanId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the vendor id to which this ticket is issued.
        /// </summary>
        /// <value>The vendor id to which this ticket is issued.</value>
        public Guid VendorId
        {
            get;
            private set;
        }

        /// <summary>
        ///  Gets the vendor userName to which this ticket is issued.
        /// </summary>
        /// <value>The vendor user name to which this ticket is ussed.</value>
        public string VendorUserName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the vendor account ID to which this ticket is issued.
        /// </summary>
        /// <value>The vendor account id to which this ticket is issued.</value>
        public string VendorAccountId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the package ID to which this ticket is issued.
        /// </summary>
        /// <value>The package id to which this ticket is ussed.</value>
        public string PackageId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the plan code ID to which this ticket is issued.
        /// </summary>
        /// <value>The plan code id to which this ticket is ussed.</value>
        public string PlanCodeId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the encrypted vendor password to which this ticket is issued.
        /// </summary>
        /// <value>The vendor password to which this ticket is issued. This is encrypted.</value>
        private byte[] VendorPasswordEncrypted
        {
            get;
            set;
        }

        /// <summary>
        /// Tries to retrieve a <see cref="DocumentFrameworkSuccessfulAuditTicket"/> from the auto expire text cache.
        /// </summary>
        /// <param name="key">The key of the object we're trying to retrieve.</param>
        /// <returns>The <see cref="DocumentFrameworkSuccessfulAuditTicket"/> object if successful, null otherwise.</returns>
        public static DocumentFrameworkSuccessfulAuditTicket TryRetrieveFromCache(string key)
        {
            return Tools.DeserializeFromCache(key, false) as DocumentFrameworkSuccessfulAuditTicket;
        }

        /// <summary>
        /// Populates a <see cref="SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination <see cref="StreamingContext"/> for this serialization. </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("UserId", this.UserId);
            info.AddValue("LoanId", this.LoanId);
            info.AddValue("VendorId", this.VendorId);
            info.AddValue("VendorUserName", this.VendorUserName);
            info.AddValue("VendorPasswordEncrypted", this.VendorPasswordEncrypted);
            info.AddValue("VendorAccountId", this.VendorAccountId);
            info.AddValue("PackageId", this.PackageId);
            info.AddValue("PlanCodeId", this.PlanCodeId);
            info.AddValue("IssuedDate", this.IssuedDate.Ticks);
        }

        /// <summary>
        /// Serializes and stores this object to the auto expire text cache for <see cref="ExpirationTimeSpan"/> amount of time.
        /// </summary>
        /// <returns>The key to retrieve this object from the auto expire text cache.</returns>
        public string SerializeAndCache()
        {
            return Tools.SerializeAndCacheFor(this, ExpirationTimeSpan);
        }

        /// <summary>
        /// Decrypts the encrypted password.
        /// </summary>
        /// <returns>The unecrypted password.</returns>
        public string GetPasswordDecypted()
        {
            return EncryptionHelper.DecryptDocumentFramework(this.VendorPasswordEncrypted);
        }
    }
}
