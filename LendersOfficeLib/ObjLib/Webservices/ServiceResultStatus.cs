﻿// <copyright file = "ServiceResultStatus.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System;

    /// <summary>
    /// Enumeration representing the webservice result response status.
    /// </summary>
    public enum ServiceResultStatus
    {
        /// <summary>
        /// Automatically determine status, based on ServiceResult error and warning messages.
        /// </summary>
        Auto,

        /// <summary>
        /// No Issues with the response.
        /// </summary>
        OK,

        /// <summary>
        /// There were issues but we were able to do the requested operation.
        /// </summary>
        OKWithWarning,

        /// <summary>
        /// There was an error with the response.
        /// </summary>
        Error
    }
}
