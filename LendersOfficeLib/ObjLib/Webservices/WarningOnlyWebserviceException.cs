﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// This exception is meant to wrap around exceptions thrown by webservices that should not generate critical emails.
    /// </summary>
    public class WarningOnlyWebserviceException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WarningOnlyWebserviceException"/> class.
        /// </summary>
        /// <param name="exception">The inner exception.</param>
        public WarningOnlyWebserviceException(Exception exception)
            : base(ErrorMessages.Generic, exception)
        {
        }
    }
}
