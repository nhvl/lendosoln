﻿// <copyright file = "AdditionalServicesOptions.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    /// <summary>
    /// Class that contains the options for additional services.
    /// </summary>
    public class AdditionalServicesOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether this has been marked for manual fulfillment.
        /// </summary>
        /// <value>Whether this has been marked for manual fulfillment.</value>
        public bool IsManualFulfillment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attention value if marked as manual fulfillment.
        /// </summary>
        /// <value>The attention value.</value>
        public string Attention
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name value if marked for manual fulfillment.
        /// </summary>
        /// <value>The name value if marked for manual fulfillment.</value>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the street value if marked for manual fulfillment.
        /// </summary>
        /// <value>The street value if marked for manual fulfillment.</value>
        public string Street
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the city value if marked for manual fulfillment.
        /// </summary>
        /// <value>The city value if marked for manual fulfillment.</value>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the state value if marked for manual fulfillment.
        /// </summary>
        /// <value>The state value if marked for manual fulfillment.</value>
        public string State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the zip value if marked for manual fulfillment.
        /// </summary>
        /// <value>The zip value if marked for manual fulfillment.</value>
        public string Zip
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the phone value if marked for manual fulfillment.
        /// </summary>
        /// <value>The phone value if marked for manual fulfillment.</value>
        public string Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the notes value if marked for manual fulfillment.
        /// </summary>
        /// <value>The notes value if marked for manual fulfillment.</value>
        public string Notes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is marked for MERS registration.
        /// </summary>
        /// <value>A value indicating whether this is marked for MERS registration.</value>
        public bool IsMersRegistration
        {
            get;
            set;
        }
    }
}
