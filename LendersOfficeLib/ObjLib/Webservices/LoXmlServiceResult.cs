﻿// <copyright file = "LoXmlServiceResult.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System.Collections.Generic;
    using System.Security;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Service results with the LOXml tags.
    /// </summary>
    public class LoXmlServiceResult : ServiceResult
    {
        /// <summary>
        /// List of result elements to add to the result xml.
        /// </summary>
        private List<XElement> resultElements;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoXmlServiceResult"/> class.
        /// </summary>
        public LoXmlServiceResult()
        {
            this.resultElements = new List<XElement>();
        }

        /// <summary>
        /// Creates a field element.
        /// </summary>
        /// <param name="id">The id of the field.</param>
        /// <param name="value">The value of the field.</param>
        /// <returns>An XElement containing the field properties.</returns>
        public static XElement CreateFieldElement(string id, string value)
        {
            return new XElement("field", new XAttribute("id", id), value);
        }

        /// <summary>
        /// Creates a collection record element.
        /// </summary>
        /// <param name="index">The index of the record.</param>
        /// <param name="type">The type of the record.</param>
        /// <returns>A record XElement with the index and type attributes.</returns>
        public static XElement CreateCollectionRecordElement(int index, string type)
        {
            return new XElement("record", new XAttribute("index", index), new XAttribute("type", type));
        }

        /// <summary>
        /// Creates a collection element.
        /// </summary>
        /// <param name="id">The id of the collection.</param>
        /// <param name="records">The records to add to this collection element.</param>
        /// <returns>The collection element with the records as its children.</returns>
        public static XElement CreateCollectionElement(string id, params XElement[] records)
        {
            return CreateCollectionElement(id, (IEnumerable<XElement>)records);
        }

        /// <summary>
        /// Creates a collection element.
        /// </summary>
        /// <param name="id">The id of the collection.</param>
        /// <param name="records">The records to add to this collection element.</param>
        /// <returns>The collection element with the records as its children.</returns>
        public static XElement CreateCollectionElement(string id, IEnumerable<XElement> records)
        {
            XElement collectionElement = new XElement("collection", new XAttribute("id", id));
            collectionElement.Add(records);
            return collectionElement;
        }

        /// <summary>
        /// Adds a result element. Will create an XElement out of the string.
        /// </summary>
        /// <param name="name">The name of the element.</param>
        /// <param name="result">The result as a string.</param>
        public void AddResultElement(string name, string result)
        {
            XElement element = new XElement(name, result);
            this.resultElements.Add(element);
        }

        /// <summary>
        /// Adds a result element.
        /// </summary>
        /// <param name="result">The result as an XElement.</param>
        public void AddResultElement(XElement result)
        {
            this.resultElements.Add(result);
        }

        /// <summary>
        /// Whether or not this includes the LOXml format root.
        /// </summary>
        /// <returns>Will always be true.</returns>
        protected override bool IncludeLOXmlFormatRoot()
        {
            return true;
        }

        /// <summary>
        /// Writes the response into the result XML.
        /// </summary>
        /// <param name="writer">The writer to write to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            foreach (XElement element in this.resultElements)
            {
                element.WriteTo(writer);
            }
        }
    }
}
