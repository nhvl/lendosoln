﻿// <copyright file = "ElectronicDeliveryOptions.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    /// <summary>
    /// A class representing the electronic delivery options for generating documents through web services.
    /// </summary>
    public class ElectronicDeliveryOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether eDisclosures should be sent.
        /// </summary>
        /// <value>A value indicating whether eDisclosures should be sent.</value>
        public bool ShouldSendEDisclosures
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether eClose is allowed.
        /// </summary>
        /// <value>A value indicating whether eClose is allowed.</value>
        public bool AllowEClose
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether online signing is allowed.
        /// </summary>
        /// <value>A value indicating whether online signing is allowed.</value>
        public bool AllowOnlineSigning
        {
            get;
            set;
        }
    }
}
