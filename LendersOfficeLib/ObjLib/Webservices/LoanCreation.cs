﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Contains the operation of loan creation for web services.
    /// </summary>
    /// <remarks>
    /// While we can't unify everything in web services here, it would be nice to (in the long run) move web services loan creation into a common location.
    /// </remarks>
    public class LoanCreation
    {
        /// <summary>
        /// Creates a loan as the specified user with the requested options.
        /// </summary>
        /// <param name="principal">The user creating the loan.</param>
        /// <param name="options">The options determining the way the loan is made.</param>
        /// <param name="permissionResolver">A permission resolution context to use when resolving the user's permissions.</param>
        /// <returns>The result of creating the loan.</returns>
        /// <remarks>
        /// Ideally, <paramref name="permissionResolver"/> would be instantiated within this method, but at this point it exists for legacy web services
        /// which do not have permission checks.
        /// </remarks>
        public static LoanCreationResult CreateWithOptions(AbstractUserPrincipal principal, LoanCreationOptions options, LoanCreationPermissionResolver permissionResolver = null)
        {
            permissionResolver = permissionResolver ?? LoanCreationPermissionResolver.GetPermissionResolverForUser(principal);
            try
            {
                ConsumerPortalUserPrincipal consumerPrincipal = principal as ConsumerPortalUserPrincipal;
                AbstractUserPrincipal createLoanAsPrincipal = consumerPrincipal?.GetImpersonatePrincipal() ?? principal;

                Guid templateId = options.TemplateId;
                if (options.IsLead && consumerPrincipal != null)
                {
                    // 7/1/2013 dd - eOPM 438678 - When consumer portal create a lead use the PML Loan template.
                    templateId = createLoanAsPrincipal.BrokerDB.PmlLoanTemplateID;
                }

                E_LoanCreationSource source = options.IsLead ? E_LoanCreationSource.CreateLeadUsingWebservice : E_LoanCreationSource.CreateBlankLoanUsingWebservice;
                if (templateId != Guid.Empty)
                {
                    source = E_LoanCreationSource.CreateFromTemplateUsingWebservice;
                }

                if (!CanCreateLoan(permissionResolver, options))
                {
                    return LoanCreationResult.Failure(ErrorMessages.GenericAccessDenied);
                }

                Guid? branchIdToUse = options.SourceUser?.BranchId;

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, source);
                if (options.IsLead && options.IsTestFile)
                {
                    throw new CBaseException(ErrorMessages.NewTestLeadsUnsupported, ErrorMessages.NewTestLeadsUnsupported);
                }
                else if (options.IsLead)
                {
                    creator.CreateLead(templateId, branchIdOverride: branchIdToUse, sourceUser: options.SourceUser);
                }
                else if (options.IsTestFile)
                {
                    creator.CreateNewTestFile(templateId, branchIdOverride: branchIdToUse, sourceUser: options.SourceUser);
                }
                else if (templateId != Guid.Empty)
                {
                    creator.CreateLoanFromTemplate(templateId, branchIdOverride: branchIdToUse, sourceUser: options.SourceUser);
                }
                else
                {
                    creator.CreateBlankLoanFile(branchIdOverride: branchIdToUse, sourceUser: options.SourceUser);
                }

                if (consumerPrincipal != null)
                {
                    ConsumerPortal.ConsumerPortalUser.LinkLoan(consumerPrincipal.BrokerId, consumerPrincipal.Id, creator.sLId);
                }

                if (options.EmployeesToAssign != null)
                {
                    string error;
                    if (!AssignRoles(options.EmployeesToAssign, creator.sLId, out error))
                    {
                        return LoanCreationResult.Failure(error);
                    }
                }

                return LoanCreationResult.Success(creator.sLId, creator.LoanName, templateId == Guid.Empty ? default(Guid?) : templateId);
            }
            catch (CBaseException exception)
            {
                Tools.LogError(exception);
                return LoanCreationResult.Failure(exception.UserMessage);
            }
            catch (LqbGrammar.Exceptions.LqbException exception)
            {
                Tools.LogError(exception);
                return LoanCreationResult.Failure(exception.Message);
            }
        }

        /// <summary>
        /// Assigns the roles to the loan file.
        /// </summary>
        /// <param name="rolesToEmployees">The roles and employees to assign.</param>
        /// <param name="loanId">The loan to modify.</param>
        /// <param name="error">Any errors encountered.</param>
        /// <returns>True if assigned, false otherwise.</returns>
        private static bool AssignRoles(Dictionary<E_RoleT, Guid> rolesToEmployees, Guid loanId, out string error)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                        loanId,
                        typeof(LoanCreation));
            loan.AllowSaveWhileQP2Sandboxed = true;
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            foreach (var roleEmployee in rolesToEmployees)
            {
                if (roleEmployee.Key == E_RoleT.LoanOfficer || roleEmployee.Key == E_RoleT.Pml_LoanOfficer)
                {
                    // These two roles are assigned to the same field.
                    loan.sEmployeeLoanRepId = roleEmployee.Value;
                }
                else
                {
                    error = $"Unable to assign role of type {Role.GetRoleDescription(roleEmployee.Key)}.";
                    return false;
                }
            }

            loan.Save();
            error = string.Empty;
            return true;
        }

        /// <summary>
        /// Given a resolver and selected set of options, returns a value indicating whether the specified loan can be created.
        /// </summary>
        /// <param name="resolver">The permission resolution context.</param>
        /// <param name="options">The options used to specify the loan.</param>
        /// <returns>A value indicating whether the specified loan can be created.</returns>
        private static bool CanCreateLoan(LoanCreationPermissionResolver resolver, LoanCreationOptions options)
        {
            bool? result = null;
            if (options.IsLead)
            {
                result = (result ?? true) && resolver.CanCreateLead;
            }

            if (options.IsTestFile)
            {
                result = (result ?? true) && resolver.CanCreateTestFile;
            }

            return result ?? resolver.CanCreateVanillaLoan;
        }
    }
}
