﻿// <copyright file = "DocFrameworkMethodContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using Audit;
    using Common.SerializationTypes;
    using ConfigSystem.Operations;
    using DataAccess;
    using Integration.DocumentVendor.LQBDocumentRequest;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocumentGeneration;
    using LendersOffice.Security;

    /// <summary>
    /// Holds the methods needed to generate documents through web services.
    /// </summary>
    public class DocFrameworkMethodContainer
    {
        /// <summary>
        /// Will generate a response with the appropriate package types and plan codes. Or it will generate an error response if an error occurs.
        /// </summary>
        /// <param name="principal">The principal obtained using the authentication ticket.</param>
        /// <param name="loanNumber">LendingQB loan number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <returns>Returns an LOXml formatted response with either available doc packages with permissions and plan codes or reason for error.</returns>
        public static string GeneratePackageTypeAndPlanCodeResponse(AbstractUserPrincipal principal, string loanNumber, string vendorId, string userName, string password, string accountId)
        {
            CPageData dataLoan;
            ConfiguredDocumentVendor docVendor;

            string initializationErrorResponse;
            if (!DocumentFrameworkValidateAndInitialize(principal, loanNumber, vendorId, userName, password, accountId, out dataLoan, out docVendor, out initializationErrorResponse))
            {
                return initializationErrorResponse;
            }

            LoXmlServiceResult xmlResult = new LoXmlServiceResult();
            xmlResult.Status = ServiceResultStatus.Error;

            DocumentVendorResult<List<DocumentPackage>> packages;
            try
            {
                packages = docVendor.GetAvailableDocumentPackages(dataLoan.sLId, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                return xmlResult.ToResponse();
            }
            
            if (packages.HasError)
            {
                xmlResult.AppendError(packages.ErrorMessage);
                return xmlResult.ToResponse();
            }

            bool isESignPossibleForThisBrokerAndLoan = dataLoan.BrokerDB.IsDocMagicESignEnabled &&
                                                       (dataLoan.sLT != E_sLT.UsdaRural || dataLoan.BrokerDB.ShowAllowOnlineSigningOptionForUSDA) &&
                                                       !(dataLoan.BrokerDB.HideAllowOnlineSigningOptionForFHA && dataLoan.sLT == E_sLT.FHA);

            XElement packageListElement = new XElement("PackageList");
            foreach (var p in packages.Result)
            {
                DocumentVendorResult<Permissions> permissions = null;
                try
                {
                    permissions = docVendor.GetPermissions(dataLoan.sLId, p.AllowEPortal, p.DisableESign, p.AllowEClose, p.AllowEPortal, p.Type, principal);
                }
                catch (CBaseException exception)
                {
                    xmlResult.AppendError(exception.UserMessage);
                    Tools.LogError(exception);
                    return xmlResult.ToResponse();
                }

                if (permissions.HasError)
                {
                    xmlResult.AppendError(permissions.ErrorMessage);
                    return xmlResult.ToResponse();
                }

                XElement permissionsAsElement = new XElement(
                                                             "Permissions",
                                                             new XAttribute("MERSRegistration", permissions.Result.EnableMERSRegistration),
                                                             new XAttribute("EDisclose", permissions.Result.EnableEDisclose),
                                                             new XAttribute("ESign", isESignPossibleForThisBrokerAndLoan && permissions.Result.EnableESign),
                                                             new XAttribute("EClose", permissions.Result.EnableEClose),
                                                             new XAttribute("ManualFulfillment", permissions.Result.EnableManualFulfillment),
                                                             new XAttribute("EmailDocuments", permissions.Result.EnableEmailDocuments),
                                                             new XAttribute("EmailPasswords", permissions.Result.EnableEmailPasswords),
                                                             new XAttribute("EmailRetrievalNotification", permissions.Result.EnableEmailRetrievalNotification),
                                                             new XAttribute("IndividualDocumentGeneration", permissions.Result.EnableIndividualDocumentGeneration));

                XElement packageAsElement = new XElement(
                                                         "Package",
                                                         new XAttribute("Description", p.Description),
                                                         new XAttribute("Id", p.Type),
                                                         permissionsAsElement);

                packageListElement.Add(packageAsElement);
            }

            DocumentVendorResult<Dictionary<string, IEnumerable<PlanCode>>> planCodes = null;
            try
            {
                planCodes = docVendor.GetAvailablePlanCodes(dataLoan.sLId, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                xmlResult.ToResponse();
            }

            if (planCodes.HasError)
            {
                xmlResult.AppendError(planCodes.ErrorMessage);
                return xmlResult.ToResponse();
            }

            XElement planCodeListElement = new XElement("PlanCodeList");
            foreach (var type in planCodes.Result.Keys)
            {
                foreach (var planCode in planCodes.Result[type].OrderBy(a => a.Description))
                {
                    planCodeListElement.Add(new XElement("PlanCode", new XAttribute("Description", planCode.Description), new XAttribute("Id", planCode.Code)));
                }
            }

            xmlResult.Status = ServiceResultStatus.OK;
            xmlResult.AddResultElement(packageListElement);
            xmlResult.AddResultElement(planCodeListElement);
            return xmlResult.ToResponse();
        }

        /// <summary>
        /// Generates the audit and generates Xml that holds the results. Or returns error Xml if needed.
        /// </summary>
        /// <param name="principal">The principal obtained using the authentication ticket.</param>
        /// <param name="loanNumber">LendingQB Loan Number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="packageId">Document package to be requested from vendor.</param>
        /// <param name="planCodeId">Plan code id, representing loan program requested from the vendor.</param>
        /// <returns>Returns the results of LQB Document Framework audit. If successful, a token will be issued to allow requesting of documents.</returns>
        public static string GenerateAuditResponse(AbstractUserPrincipal principal, string loanNumber, string vendorId, string userName, string password, string accountId, string packageId, string planCodeId)
        {
            CPageData dataLoan;
            ConfiguredDocumentVendor docVendor;

            string initializeErrorResponse;
            if (!DocumentFrameworkValidateAndInitialize(principal, loanNumber, vendorId, userName, password, accountId, out dataLoan, out docVendor, out initializeErrorResponse))
            {
                return initializeErrorResponse;
            }

            LoXmlServiceResult xmlResult = new LoXmlServiceResult();
            xmlResult.Status = ServiceResultStatus.Error;

            var documentPackage = docVendor.Config.RetrievePackage(packageId);
            if (documentPackage == VendorConfig.DocumentPackage.InvalidPackage)
            {
                xmlResult.AppendError("Invalid packageId.");
                return xmlResult.ToResponse();
            }

            // Can't run audit if the initial loan estimate archive is invalid.
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                xmlResult.AppendError(ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveDocs);
                return xmlResult.ToResponse();
            }

            // Can't run if archive in unknown status.
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && dataLoan.sHasClosingCostArchiveInUnknownStatus)
            {
                var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                xmlResult.AppendError(ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(archive.DateArchived, archive.ClosingCostArchiveType, userCanDetermineStatus: false));
                return xmlResult.ToResponse();
            }

            DocumentAuditor auditor = new DocumentAuditor(docVendor, dataLoan.sLId, dataLoan.GetAppData(0).aAppId);

            // If the temporary archive needs a source specified, then we can't run. 
            if (auditor.MustSpecifyTemporaryArchiveSource(documentPackage))
            {
                var archive = dataLoan.sLoanEstimateArchiveInPendingStatus;
                xmlResult.AppendError(ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(archive.DateArchived, archive.Status, userCanDetermineStatus: false));
                return xmlResult.ToResponse();
            }

            if (dataLoan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus && documentPackage.HasLoanEstimate)
            {
                xmlResult.AppendError(ErrorMessages.ArchiveError.CannotGenerateLEAfterIncludingLEInCD);
                return xmlResult.ToResponse();
            }

            DocumentVendorResult<Audit> auditResults;
            try
            {
                auditResults = auditor.Audit(packageId, planCodeId, string.Empty, string.Empty, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                return xmlResult.ToResponse();
            }

            if (auditResults.HasError)
            {
                xmlResult.AppendError(auditResults.ErrorMessage);
                return xmlResult.ToResponse();
            }

            AuditSeverity mostSevereResult = AuditSeverity.Misc;
            string overallResultMessage = string.Empty;
            XElement auditResultList = new XElement("AuditResults");
            foreach (AuditLine auditResult in auditResults.Result.Results.OrderByDescending(a => a.Severity))
            {
                if (auditResult.Severity == AuditSeverity.Fatal)
                {
                    mostSevereResult = AuditSeverity.Fatal;
                }
                else if (auditResult.Severity == AuditSeverity.Critical && mostSevereResult != AuditSeverity.Fatal)
                {
                    mostSevereResult = AuditSeverity.Critical;
                }
                else if (auditResult.Severity == AuditSeverity.Warning && mostSevereResult != AuditSeverity.Fatal && mostSevereResult != AuditSeverity.Critical)
                {
                    mostSevereResult = AuditSeverity.Warning;
                }

                XElement auditResultElement = new XElement(
                                                           "AuditMessage",
                                                           new XAttribute("Severity", auditResult.Severity.ToString("G")),
                                                           new XAttribute("Message", auditResult.Message),
                                                           new XAttribute("AuditDetails", LendersOffice.AntiXss.AspxTools.HtmlString(auditResult.AuditDetails)));

                auditResultList.Add(auditResultElement);
            }

            if (mostSevereResult == AuditSeverity.Fatal)
            {
                xmlResult.Status = ServiceResultStatus.Error;
                xmlResult.AppendError("Audit executed successfully but results contain fatal remarks. Documents cannot be generated.");
                xmlResult.AddResultElement(auditResultList);

                return xmlResult.ToResponse();
            }
            else if (mostSevereResult == AuditSeverity.Critical)
            {
                xmlResult.Status = ServiceResultStatus.OKWithWarning;
                xmlResult.AppendWarning("Audit executed successfully but results contain critical remarks. Please review prior to generating documents.");
            }
            else if (mostSevereResult == AuditSeverity.Warning)
            {
                xmlResult.Status = ServiceResultStatus.OKWithWarning;
                xmlResult.AppendWarning("Audit executed successfully but results contain warning remarks. Recommend review prior to generating documents.");
            }
            else
            {
                xmlResult.Status = ServiceResultStatus.OK;
                xmlResult.AddResultElement("message", "Audit Complete, no issues found.");
            }

            xmlResult.AddResultElement(auditResultList);

            DocumentFrameworkSuccessfulAuditTicket ticket = new DocumentFrameworkSuccessfulAuditTicket(PrincipalFactory.CurrentPrincipal.UserId, dataLoan.sLId, Guid.Parse(vendorId), userName, password, accountId, packageId, planCodeId);
            XElement ticketElement = new XElement("Ticket", ticket.SerializeAndCache());
            xmlResult.AddResultElement(ticketElement);

            return xmlResult.ToResponse();
        }

        /// <summary>
        /// Generates a response containing the forms that can be chosen for generation or an error if needed.
        /// </summary>
        /// <param name="principal">The principal obtained from the authentication ticket.</param>
        /// <param name="loanNumber">LendingQB Loan Number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="packageId">Document package to be requested from vendor.</param>
        /// <param name="planCodeId">Plan code id, representing loan program requested from the vendor.</param>
        /// <returns>Returns a list of individual forms that can be generated at document request via LQB Document Framework.</returns>
        public static string GenerateFormListResponse(AbstractUserPrincipal principal, string loanNumber, string vendorId, string userName, string password, string accountId, string packageId, string planCodeId)
        {
            CPageData dataLoan;
            ConfiguredDocumentVendor docVendor;
            string validationResponse;
            if (!DocumentFrameworkValidateAndInitialize(principal, loanNumber, vendorId, userName, password, accountId, out dataLoan, out docVendor, out validationResponse))
            {
                return validationResponse;
            }

            LoXmlServiceResult xmlResult = new LoXmlServiceResult();
            xmlResult.Status = ServiceResultStatus.Error;

            DocumentVendorResult<List<FormInfo>> forms;
            try
            {
                forms = docVendor.GetFormList(dataLoan.sLId, planCodeId, packageId, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                return xmlResult.ToResponse();
            }

            if (forms.HasError)
            {
                xmlResult.AppendError(forms.ErrorMessage);
                return xmlResult.ToResponse();
            }

            XElement formsList = new XElement("FormList");
            foreach (var form in forms.Result)
            {
                XElement formInfo = new XElement(
                                                 "Form",
                                                 new XAttribute("Name", form.Name),
                                                 new XAttribute("BorrowerNumber", form.BorrowerNumber),
                                                 new XAttribute("Id", form.ID));

                formsList.Add(formInfo);
            }

            xmlResult.Status = ServiceResultStatus.OK;
            xmlResult.AddResultElement(formsList);
            return xmlResult.ToResponse();
        }

        /// <summary>
        /// Generates the documents and creates Xml holding a key that allows them to download the document. Or returns error Xml if needed.
        /// </summary>
        /// <param name="principal">The principal obtained from the authentication ticket.</param>
        /// <param name="loanNumber">LendingQB Loan Number.</param>
        /// <param name="auditTicket">The token obtained from DocumentFrameworkPerformAudit.</param>
        /// <param name="optionsXml">Xml containing requested options at document generation.</param>
        /// <returns>Returns Xml containing a key that will allow the user to download the document. Error Xml if an error has occured.</returns>
        public static string CreateDocumentGenerationResponse(AbstractUserPrincipal principal, string loanNumber, string auditTicket, string optionsXml)
        {
            LoXmlServiceResult xmlResult = new LoXmlServiceResult();
            xmlResult.Status = ServiceResultStatus.Error;

            if (string.IsNullOrEmpty(auditTicket))
            {
                xmlResult.AppendError("Please provide an audit ticket.");
                return xmlResult.ToResponse();
            }

            if (string.IsNullOrEmpty(optionsXml))
            {
                xmlResult.AppendError("Please provide document generation options.");
                return xmlResult.ToResponse();
            }

            DocumentFrameworkSuccessfulAuditTicket ticket = DocumentFrameworkSuccessfulAuditTicket.TryRetrieveFromCache(auditTicket);
            if (ticket == null)
            {
                xmlResult.AppendError("Invalid audit ticket.");
                return xmlResult.ToResponse();
            }

            CPageData dataLoan;
            ConfiguredDocumentVendor docVendor;
            string validationError;
            if (!DocumentFrameworkValidateAndInitialize(principal, loanNumber, ticket.VendorId.ToString(), ticket.VendorUserName, ticket.GetPasswordDecypted(), ticket.VendorAccountId, out dataLoan, out docVendor, out validationError))
            {
                return validationError;
            }

            if (dataLoan.sLId != ticket.LoanId)
            {
                xmlResult.AppendError("Audit ticket is not valid for this loan.");
                return xmlResult.ToResponse();
            }

            DocumentVendorResult<List<DocumentPackage>> packages = null;
            try
            {
                packages = docVendor.GetAvailableDocumentPackages(dataLoan.sLId, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                xmlResult.ToResponse();
            }

            if (packages.HasError)
            {
                xmlResult.AppendError(packages.ErrorMessage);
                return xmlResult.ToResponse();
            }

            DocumentPackage selectedDocPackage = packages.Result.FirstOrDefault(p => p.Type == ticket.PackageId);
            if (selectedDocPackage.Equals(default(DocumentPackage)))
            {
                xmlResult.AppendError("Document Package " + ticket.PackageId + " is not available.");
                return xmlResult.ToResponse();
            }

            DocumentVendorResult<Permissions> permissions = null;
            try
            {
                permissions = docVendor.GetPermissions(dataLoan.sLId, selectedDocPackage.AllowEPortal, selectedDocPackage.DisableESign, selectedDocPackage.AllowEClose, selectedDocPackage.AllowEPortal, selectedDocPackage.Type, principal);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                xmlResult.ToResponse();
            }

            if (permissions.HasError)
            {
                xmlResult.AppendError(permissions.ErrorMessage);
                return xmlResult.ToResponse();
            }

            DocumentGenerationOptions options = DocumentGenerationOptions.DeserializeFromXml(optionsXml);
            Permissions permissionResults = permissions.Result;

            // Validate ALL the options. I really really hope this is everything from the UI and the code behind.
            bool sendEDisclosures;
            if (!permissionResults.EnableEDisclose)
            {
                sendEDisclosures = false;
            }
            else
            {
                sendEDisclosures = options.ElectronicDelivery.ShouldSendEDisclosures;
            }

            bool requestEClose = false;
            if (permissionResults.EnableEClose)
            {
                requestEClose = options.ElectronicDelivery.AllowEClose;
            }

            bool allowOnlineSigning;
            if (sendEDisclosures &&
                dataLoan.BrokerDB.IsDocMagicESignEnabled &&
                permissionResults.EnableESign &&
                (dataLoan.sLT != E_sLT.UsdaRural || dataLoan.BrokerDB.ShowAllowOnlineSigningOptionForUSDA) &&
                !(dataLoan.BrokerDB.HideAllowOnlineSigningOptionForFHA && dataLoan.sLT == E_sLT.FHA))
            {
                allowOnlineSigning = options.ElectronicDelivery.AllowOnlineSigning;
            }
            else
            {
                allowOnlineSigning = false;
            }

            bool shouldEmailDocuments;
            if (!permissionResults.EnableEmailDocuments)
            {
                shouldEmailDocuments = false;
            }
            else
            {
                shouldEmailDocuments = options.EmailSettings.ShouldEmailDocuments;
            }

            string emailToAddress;
            if (!shouldEmailDocuments)
            {
                emailToAddress = string.Empty;
            }
            else if (shouldEmailDocuments && string.IsNullOrEmpty(options.EmailSettings.EmailAddress))
            {
                xmlResult.AppendError("Please specify a destination email address.");
                return xmlResult.ToResponse();
            }
            else
            {
                emailToAddress = options.EmailSettings.EmailAddress;
            }

            bool shouldRequirePassword = false;
            if (!shouldEmailDocuments || !permissionResults.EnableEmailPasswords)
            {
                shouldRequirePassword = false;
            }
            else
            {
                shouldRequirePassword = options.EmailSettings.RequiresPassword;
            }

            string requiredPassword = string.Empty;
            if (!shouldRequirePassword)
            {
                requiredPassword = string.Empty;
            }
            else if (shouldRequirePassword && string.IsNullOrEmpty(options.EmailSettings.Password))
            {
                xmlResult.AppendError("Please provide a password.");
                xmlResult.ToResponse();
            }
            else
            {
                requiredPassword = options.EmailSettings.Password;
            }

            bool shouldNotifyOnRetrieval = false;
            if (!shouldEmailDocuments || !permissionResults.EnableEmailRetrievalNotification)
            {
                shouldNotifyOnRetrieval = false;
            }
            else
            {
                shouldNotifyOnRetrieval = options.EmailSettings.ShouldNotifyOnRetrieval;
            }

            DocumentGenerator.EmailDocumentInformation emailInfo;
            if (!shouldEmailDocuments)
            {
                emailInfo = null;
            }
            else
            {
                emailInfo = new DocumentGenerator.EmailDocumentInformation(
                                                                           emailAddress: emailToAddress,
                                                                           isPasswordRequired: shouldRequirePassword,
                                                                           requiredPassword: requiredPassword,
                                                                           passwordHint: "Other",
                                                                           isNotificationOnRetrievalRequested: shouldNotifyOnRetrieval);
            }

            bool shouldGenerateSpecificForms = false;
            if (!permissionResults.EnableIndividualDocumentGeneration)
            {
                shouldGenerateSpecificForms = false;
            }
            else
            {
                shouldGenerateSpecificForms = options.SpecifiedForms.ShouldGenerateSpecificForms;
            }

            List<FormInfo> selectedForms;
            if (!shouldGenerateSpecificForms)
            {
                selectedForms = null;
            }
            else if (shouldGenerateSpecificForms && options.SpecifiedForms.SelectedForms.Count == 0)
            {
                xmlResult.AppendError("Please specify the individual forms to generate.");
                return xmlResult.ToResponse();
            }
            else
            {
                selectedForms = new List<FormInfo>();

                foreach (SpecifiedFormInfo formInfo in options.SpecifiedForms.SelectedForms)
                {
                    selectedForms.Add(new FormInfo(borrowerNumber: formInfo.BorrowerNumber, id: formInfo.Id));
                }
            }

            bool isManualFulfillment;
            if (!permissionResults.EnableManualFulfillment ||
                sendEDisclosures ||
                (ticket.VendorId == Guid.Empty && !dataLoan.BrokerDB.EnableDsiPrintAndDeliverOption))
            {
                isManualFulfillment = false;
            }
            else
            {
                isManualFulfillment = options.AdditionalServices.IsManualFulfillment;
            }

            DocumentGenerator.ManualFulfillmentInformation manualFulfillmentInfo;
            if (!isManualFulfillment)
            {
                manualFulfillmentInfo = null;
            }
            else
            {
                if (string.IsNullOrEmpty(Tools.SafeStateCode(options.AdditionalServices.State)))
                {
                    xmlResult.AppendError("Invalid state code " + options.AdditionalServices.State);
                    return xmlResult.ToResponse();
                }

                manualFulfillmentInfo = new DocumentGenerator.ManualFulfillmentInformation(
                                                                                           name: options.AdditionalServices.Name,
                                                                                           streetAddress: options.AdditionalServices.Street,
                                                                                           city: options.AdditionalServices.City,
                                                                                           state: options.AdditionalServices.State,
                                                                                           zipcode: options.AdditionalServices.Zip,
                                                                                           attention: options.AdditionalServices.Attention,
                                                                                           phone: options.AdditionalServices.Phone,
                                                                                           notes: options.AdditionalServices.Notes);
            }

            bool isMersRegistration;
            if (!permissionResults.EnableMERSRegistration)
            {
                isMersRegistration = false;
            }
            else
            {
                isMersRegistration = options.AdditionalServices.IsMersRegistration;
            }

            bool isPreviewOrder = false;
            string documentFormat = "PDF";

            DocumentGenerator documentGenerator = new DocumentGenerator(docVendor, dataLoan.sLId, dataLoan.GetAppData(0).aAppId);
            DocumentVendorResult<IDocumentGenerationResult> generationResult;
            try
            {
                generationResult = documentGenerator.Generate(
                                                              packageTypeId: ticket.PackageId,
                                                              loanProgramId: ticket.PlanCodeId,
                                                              permissions: permissionResults,
                                                              isEDisclosureRequested: sendEDisclosures,
                                                              isECloseRequested: requestEClose,
                                                              isESignRequested: allowOnlineSigning,
                                                              isMersRegistrationEnabled: isMersRegistration,
                                                              principal: principal,
                                                              emailDocumentInformation: emailInfo,
                                                              manualFulfillmentInformation: manualFulfillmentInfo,
                                                              documentFormat: documentFormat,
                                                              forms: selectedForms,
                                                              isPreviewOrder: isPreviewOrder);
            }
            catch (CBaseException exception)
            {
                xmlResult.AppendError(exception.UserMessage);
                Tools.LogError(exception);
                return xmlResult.ToResponse();
            }

            if (generationResult.HasError)
            {
                xmlResult.AppendError(generationResult.ErrorMessage);
                return xmlResult.ToResponse();
            }

            AutoExpiredTextCache.RemoveFromCacheImmediately(auditTicket);
            var configuredResult = generationResult.Result as ConfiguredDocumentGenerationResult;
            string keyForKey = AutoExpiredTextCache.AddToCache(configuredResult.PdfFileDbKey, new TimeSpan(0, 5, 0));

            xmlResult.Status = ServiceResultStatus.OK;
            xmlResult.AddResultElement("downloadKey", keyForKey);
            return xmlResult.ToResponse();
        }

        /// <summary>
        /// Downloads the generated document using the provided download key.
        /// </summary>
        /// <param name="downloadKey">The key to use to download the generated document.</param>
        /// <returns>The generated document.</returns>
        public static byte[] DownloadGeneratedDocs(string downloadKey)
        {
            string fileDbKey = AutoExpiredTextCache.GetFromCache(downloadKey);
            if (fileDbKey == null)
            {
                throw new CBaseException("Unable to validate downloadKey. It may be expired.", "downloadKey invalid.");
            }

            AutoExpiredTextCache.RemoveFromCacheImmediately(downloadKey);
            try
            {
                return FileDBTools.ReadData(E_FileDB.Temp, fileDbKey);
            }
            catch (FileNotFoundException)
            {
                throw new CBaseException("Unable to retrieve documents.", "Could not find documents in FileDB. Key: " + fileDbKey);
            }
        }

        /// <summary>
        /// Executes boiler plate code needed to initialize the LQB Document Framework. This consists of validation of auth ticket, loan number, workflow permissions, vendorId, vendor credentials, and permission to use vendor with given loan type. 
        /// It also initializes both the loan object and configured document vendor object for interaction via the Document Framework.
        /// </summary>
        /// <param name="principal">The principal obtained using the authentication ticket.</param>
        /// <param name="loanNumber">LendingQb loan number.</param>
        /// <param name="vendorIdString">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="dataLoan">The data loan to populate.</param>
        /// <param name="docVendor">A configured document vendor matching the vendorId. This will be null if the method terminates due to error prior to document vendor object construction.</param>
        /// <param name="errorResponse">If false, an LOXml formatted error response to be returned to API user. Otherwise, an empty string.</param>
        /// <returns>Returns a boolean indicating whether the Document Framework boilerplate code executed successfully.</returns>
        public static bool DocumentFrameworkValidateAndInitialize(AbstractUserPrincipal principal, string loanNumber, string vendorIdString, string userName, string password, string accountId, out CPageData dataLoan, out ConfiguredDocumentVendor docVendor, out string errorResponse)
        {
            dataLoan = null;
            docVendor = null;

            LoXmlServiceResult xmlResult = new LoXmlServiceResult();
            xmlResult.Status = ServiceResultStatus.Error;

            // Validate loan number
            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanId == Guid.Empty)
            {
                var exc = new CBaseException("[" + loanNumber + "] is an invalid loan number.", "Integration Error: [" + loanNumber + "] is an invalid loan number.");
                xmlResult.AppendError(exc.UserMessage);
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            // Validate workflow permissions.
            try
            {
                AuthServiceHelper.CheckLoanAuthorization(loanId, WorkflowOperations.ReadLoan);
            }
            catch (AccessDenied exc)
            {
                xmlResult.AppendError(exc.DeveloperMessage);
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            // Verify vendorIdString can be parsed into Guid. Although DocMagic Legacy integration is represented by empty GUID, it was retired with TRID.
            Guid vendorId;
            if (!Guid.TryParse(vendorIdString, out vendorId))
            {
                xmlResult.AppendError("vendorId parameter is not a valid GUID.");
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            // Retrieve vendor from database. Validate username/password/accountid as needed.
            VendorConfig vendorConfig = null;
            try
            {
                vendorConfig = VendorConfig.Retrieve(vendorId);
            }
            catch (NotFoundException exc)
            {
                xmlResult.AppendError(exc.DeveloperMessage);
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            bool isMissingAnyCredentials = false;
            if (vendorConfig.UsesUsername && string.IsNullOrWhiteSpace(userName))
            {
                xmlResult.AppendError("Username is required.");
                isMissingAnyCredentials = true;
            }

            if (vendorConfig.UsesPassword && string.IsNullOrEmpty(password))
            {
                xmlResult.AppendError("Password is required.");
                isMissingAnyCredentials = true;
            }

            if (vendorConfig.UsesAccountId && string.IsNullOrEmpty(accountId))
            {
                xmlResult.AppendError("Account ID is required.");
                isMissingAnyCredentials = true;
            }

            if (isMissingAnyCredentials)
            {
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            docVendor = new ConfiguredDocumentVendor(vendorConfig, principal.BrokerId, userName, password, accountId);

            // Verify that lender has this vendor enabled.
            var docVendorSettings = principal.BrokerDB.AllActiveDocumentVendors.FirstOrDefault(v => v.VendorId == vendorId);
            if (docVendorSettings == null)
            {
                xmlResult.AppendError("vendorId: " + vendorId + " is not enabled for this account.");
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            // Load loan and confirm that vendor is enabled for the specific loan type, whether it be production or test file.
            dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(DocFrameworkMethodContainer));
            dataLoan.InitLoad();

            if (dataLoan.sLoanFileT == E_sLoanFileT.Loan)
            {
                if (!docVendorSettings.IsEnableForProductionLoans)
                {
                    xmlResult.AppendError("vendorId: " + vendorId + " is not enabled for production loans.");
                    errorResponse = xmlResult.ToResponse();
                    return false;
                }
            }
            else if (dataLoan.sLoanFileT == E_sLoanFileT.Test)
            {
                if (!docVendorSettings.IsEnableForTestLoans)
                {
                    xmlResult.AppendError("vendorId: " + vendorId + " is not enabled for test loans.");
                    errorResponse = xmlResult.ToResponse();
                    return false;
                }
            }
            else
            {
                xmlResult.AppendError("Cannot order docs on this loan type: " + dataLoan.sLoanFileT);
                errorResponse = xmlResult.ToResponse();
                return false;
            }

            errorResponse = string.Empty;
            return true;
        }

        /// <summary>
        /// Parses a document vendor type from an LOXml query containing //loan/field[@id='DocumentVendor'].
        /// </summary>
        /// <param name="xmlQuery">The LOXml query to parse.</param>
        /// <returns>The parsed <see cref="E_DocumentVendor"/> value.</returns>
        public static E_DocumentVendor ParseVendor(string xmlQuery)
        {
            XmlDocument parsedQuery = Tools.CreateXmlDoc(xmlQuery);
            XmlNode vendorAttributeNode = parsedQuery.SelectSingleNode("//loan/field[@id='DocumentVendor']");
            E_DocumentVendor parsedVendor;
            if (vendorAttributeNode == null)
            {
                // Default is IDS if not specified
                parsedVendor = E_DocumentVendor.IDS;
            }
            else if (!Enum.TryParse(vendorAttributeNode.InnerText.Replace(" ", string.Empty), true, out parsedVendor))
            {
                string userMessage = "[" + vendorAttributeNode.Value + "] is not a valid DocumentVendor type.";
                throw new CBaseException(userMessage, userMessage + " Raw XML: " + xmlQuery);
            }

            return parsedVendor;
        }

        /// <summary>
        /// Creates an LQB document request string for export.
        /// </summary>
        /// <param name="principal">The user making the web service request.</param>
        /// <param name="loanId">The loan ID of the loan to export data for.</param>
        /// <param name="xmlQuery">An XML query containing document package information.</param>
        /// <returns>The LQB document framework request string containing MISMO and LOXml.</returns>
        public static string CreateLqbDocumentRequest(AbstractUserPrincipal principal, Guid loanId, string xmlQuery)
        {
            E_DocumentVendor vendor = ParseVendor(xmlQuery);
            var vendorConfig = new VendorConfig()
            {
                PlatformType = vendor,
                VendorName = vendor.ToString()
            };
            DocumentVendorServer server = new DocumentVendorServer(vendorConfig, principal.BrokerId, null);
            var parsedPackage = LOFormatExporter.GetDocumentPackageFromLoXml(xmlQuery, exportIsLoxmlFormat: false);

            if (!parsedPackage.IsSuccessful)
            {
                var result = new LoXmlServiceResult() { Status = ServiceResultStatus.Error };
                result.AppendError(parsedPackage.ErrorMessage);
                return result.ToResponse();
            }

            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(DocFrameworkMethodContainer));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            var documentRequest = new LQBDocumentRequest();
            documentRequest.Request = new Request
            {
                RequestType = RequestRequestType.RequestDocuments,
                Item = new DocumentRequest()
                {
                    LoanProgramID = loanData.sDocMagicPlanCodeNm,
                    PackageID = string.Empty
                },
                PostbackUrl = ConstStage.DocFrameworkPostbackUrl
            };

            // Adapted from DocumentAuditor.cs
            bool useTemporaryArchive = loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            if (useTemporaryArchive)
            {
                loanData.CreateTempArchive(parsedPackage.ValueOrDefault.HasClosingDisclosure, DocumentAuditor.TemporaryArchiveSource.LiveData, true, principal);
            }
            else if (loanData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                && loanData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID
                && loanData.BrokerDB.IsGFEandCoCVersioningEnabled
                && loanData.sLastDisclosedGFEArchiveId != Guid.Empty
                && parsedPackage.ValueOrDefault.HasClosingDisclosure == false)
            {
                loanData.RefreshClosingCostCoc();
            }

            loanData.Save();

            List<string> feeDiscrepancies;
            return server.GetDocumentFrameworkPayload(documentRequest, loanId, null, out feeDiscrepancies, principal, package: parsedPackage.ValueOrDefault, preventUsingTemporaryArchive: false);
        }

        /// <summary>
        /// Create a closing disclosure archive.
        /// </summary>
        /// <param name="loanId">The loan ID of the loan to create a disclosure archive for.</param>
        /// <param name="status">The status to create the archive with.</param>
        /// <returns>The created archive's ID, or null if no archive was made.</returns>
        public static LoXmlServiceResult CreateClosingDisclosureArchive(Guid loanId, ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            CPageData loanData = new CPageData(loanId, new List<string> { "sfArchiveClosingCosts", "sClosingCostArchive", "sDisclosureRegulationT", "sClosingCostFeeVersionT" });
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;

            if (loanData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                loanData.ArchiveClosingCosts(
                    ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure,
                    E_GFEArchivedReasonT.ManuallyArchived,
                    status);
                loanData.Save();

                // Return the ID and status of the newly created archive in the response.
                // The CPageBase.ArchiveClosingCosts() method guarantees that the newest archive is at index 0.
                Guid archiveId = loanData.sClosingCostArchive[0].Id;
                var archiveIdElement = archiveId == null ? null : LoXmlServiceResult.CreateFieldElement("Id", archiveId.ToString());
                result.AddResultElement(
                    new XElement(
                        "loan",
                        LoXmlServiceResult.CreateCollectionElement(
                            "DisclosureArchiveMetadata",
                            new XElement(
                                "record",
                                new XAttribute("index", 0),
                                new XAttribute("type", "DisclosureArchiveMetadata"),
                                archiveIdElement,
                                LoXmlServiceResult.CreateFieldElement("Status", status.ToString())))));

                result.Status = ServiceResultStatus.OK;
                return result;
            }
            else
            {
                Tools.LogError("Loan.asmx::CreateClosingDisclosure called without new fee type mode and TRID status.");
                result.Status = ServiceResultStatus.Error;
                result.AppendError("The loan isn't in the correct status to archive a Closing Disclosure.");
                return result;
            }
        }

        /// <summary>
        /// Records an Initial Disclosure Event.
        /// </summary>
        /// <param name="principal">The user who called the method.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="dataXml">Any data that they passed in.</param>
        /// <param name="result">The results.</param>
        public static void RecordInitialDisclosureEvent(AbstractUserPrincipal principal, string loanNumber, string dataXml, LoXmlServiceResult result)
        {
            result.Status = ServiceResultStatus.Error;

            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanId == Guid.Empty)
            {
                result.AppendError($"Loan with loan number {loanNumber} not found.");
                return;
            }

            if (!AuthServiceHelper.IsOperationAuthorized(loanId, WorkflowOperations.AllowRecordingExternalDisclosureEvents))
            {
                result.AppendError($"You do not have permission to perform this request.");
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(DocFrameworkMethodContainer));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                result.AppendError("There is already a Last Disclosed Loan Estimate Archive.");
                return;
            }

            if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
            {
                result.AppendError("There is already a Loan Estimate archive in the Pending status.");
                return;
            }

            DateTime createdDate = DateTime.Now;
            DateTime issuedDate = DateTime.Now;
            DateTime receivedDate = DateTime.Now;
            Guid transactionId = Guid.NewGuid();
            E_DeliveryMethodT deliveryMethod = E_DeliveryMethodT.LeaveEmpty;
            Dictionary<string, string> dataValues = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(dataXml))
            {
                XDocument doc = XDocument.Parse(dataXml);
                IEnumerable<XElement> loanEstimateDates = from el in doc.Element("LOXmlFormat").Element("loan").Elements("collection")
                                                          where el.Attribute("id").Value.Equals("sLoanEstimateDatesInfo", StringComparison.OrdinalIgnoreCase)
                                                          select el;
                if (loanEstimateDates.Count() != 1)
                {
                    result.AppendError("Please specify only one sLoanEstimateDatesInfo collection.");
                    return;
                }

                var leRecords = loanEstimateDates.First().Elements("record");
                if (leRecords.Count() != 1)
                {
                    result.AppendError("Please specify only one LoanEstimateDates record.");
                    return;
                }

                foreach (var field in leRecords.First().Elements("field"))
                {
                    string id = field.Attribute("id").Value;
                    if (id.Equals("CreatedDate", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!DateTime.TryParse(field.Value, out createdDate))
                        {
                            result.AppendError($"Invalid value for field {id}: {field.Value}.");
                            return;
                        }

                        dataValues.Add("Created Date", createdDate.ToString());
                    }
                    else if (id.Equals("IssuedDate", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!DateTime.TryParse(field.Value, out issuedDate))
                        {
                            result.AppendError($"Invalid value for field {id}: {field.Value}.");
                            return;
                        }

                        dataValues.Add("Issued Date", issuedDate.ToString());
                    }
                    else if (id.Equals("ReceivedDate", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!DateTime.TryParse(field.Value, out receivedDate))
                        {
                            result.AppendError($"Invalid value for field {id}: {field.Value}.");
                            return;
                        }

                        dataValues.Add("Received Date", receivedDate.ToString());
                    }
                    else if (id.Equals("DeliveryMethod", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!Enum.TryParse(field.Value, out deliveryMethod) || !Enum.IsDefined(typeof(E_DeliveryMethodT), deliveryMethod))
                        {
                            result.AppendError($"Invalid value for field {id}: {field.Value}.");
                            return;
                        }

                        dataValues.Add("Delivery Method", deliveryMethod.ToString());
                    }
                    else if (id.Equals("TransactionId", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!Guid.TryParse(field.Value, out transactionId))
                        {
                            result.AppendError($"Invalid value for field {id}: {field.Value}.");
                            return;
                        }

                        dataValues.Add("Transaction ID", transactionId.ToString());
                    }
                    else
                    {
                        result.AppendError($"Field {id} is not available for use.");
                        return;
                    }
                }
            }

            try
            {
                ClosingCostArchive archive = dataLoan.CreateAndSetArchiveForInitialDisclosureEvent(principal, dataValues);
                dataLoan.UpdateTridDisclosureDatesForDocGeneration(
                    archive: archive,
                    requestIsSendEDisclosure: true,
                    requestIsEClosed: false,
                    requestIsManualFulfillment: false,
                    resultTransactionId: transactionId.ToString(),
                    docVendorApr: null,
                    docVendorId: null,
                    docCode: null,
                    defaultIssuedDateToToday: principal.BrokerDB.DefaultIssuedDateToToday,
                    createdDate: createdDate,
                    issuedDate: issuedDate,
                    receivedDate: receivedDate,
                    deliveryMethod: deliveryMethod);
                var lastDisclosedApr = archive.GetRateValue("sApr");
                InitialDisclosureEventRecordedAudit disclosureEventAudit = new InitialDisclosureEventRecordedAudit(principal, dataValues);
                dataLoan.UpdateDisclosureStatusForExternalInitialDisclosure(lastDisclosedApr, disclosureEventAudit);
                dataLoan.Save();
            }
            catch (CBaseException exception)
            {
                Tools.LogError(exception);
                result.AppendError(exception.UserMessage);
                return;
            }

            result.Status = ServiceResultStatus.OK;
        }
    }
}
