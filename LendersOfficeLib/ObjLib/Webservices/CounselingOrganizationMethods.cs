﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using DataAccess.CounselingOrganization;
    using DataAccess.HomeownerCounselingOrganizations;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Webservice methods for counseling organizations.
    /// </summary>
    public class CounselingOrganizationMethods
    {
        /// <summary>
        /// Grabs the top 10 closest counseling organizations from the CFPB and saves them onto the file.
        /// </summary>
        /// <param name="loanNumber">The loan number to save to.</param>
        /// <param name="principal">The principal of the user who called this webservice.</param>
        /// <returns>The webservice xml result.</returns>
        public static string GenerateClosestCounselingOrganization(string loanNumber, AbstractUserPrincipal principal)
        {
            Guid brokerId = principal.BrokerId;
            Guid loanId = Tools.GetLoanIdByLoanName(brokerId, loanNumber);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(CounselingOrganizationMethods));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Want to use the zipcode of the primary app.
            string zipcode = dataLoan.GetAppData(0).aBZip;

            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;

            string cfpbData;
            string errors;
            if (CounselingOrganizationUtilities.FetchDataFromCFPB(zipcode, out errors, out cfpbData))
            {
                if (string.IsNullOrEmpty(cfpbData))
                {
                    result.AppendError("Unable to load data from CFPB");
                    return result.ToResponse();
                }

                CfpbAgencyList agencies = CounselingOrganizationUtilities.CreateAgencyListFromJson(cfpbData);
                if (agencies.CounselingAgencies == null || agencies.CounselingAgencies.Count == 0)
                {
                    result.AppendError($"CFPB was unable to find housing counselor data for zip code {zipcode} of the borrower's present address.");
                    return result.ToResponse();
                }
                else if (agencies.CounselingAgencies.Count < 10)
                {
                    result.AppendError("We were unable to load all 10 Counseling Organizations from CFPB");
                    return result.ToResponse();
                }
                else
                {
                    HomeownerCounselingOrganizationCollection organizationCollection = new HomeownerCounselingOrganizationCollection();

                    List<XElement> xmlRecords = new List<XElement>();
                    for (int i = 0; i < agencies.CounselingAgencies.Count; i++)
                    {
                        var agency = agencies.CounselingAgencies[i];
                        var trueAgency = CounselingOrganizationUtilities.CreateHomeownerCounselingOrgFromCfpbData(agency);
                        organizationCollection.Add(trueAgency);

                        var recordElement = LoXmlServiceResult.CreateCollectionRecordElement(i + 1, "HomeownerCounselingOrganization");
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Id", trueAgency.Id.ToString()));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Name", trueAgency.Name));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Phone", trueAgency.Phone));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Email", trueAgency.Email));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Website", trueAgency.Website));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Services", trueAgency.Services));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Languages", trueAgency.Languages));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("StreetAddress", trueAgency.Address.StreetAddress));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("StreetAddress2", trueAgency.Address.StreetAddress2));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("City", trueAgency.Address.City));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("State", trueAgency.Address.State));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Zipcode", trueAgency.Address.Zipcode));
                        recordElement.Add(LoXmlServiceResult.CreateFieldElement("Distance", trueAgency.Distance));
                        xmlRecords.Add(recordElement);
                    }

                    dataLoan.sHomeownerCounselingOrganizationCollection = organizationCollection;
                    dataLoan.sHomeownerCounselingOrganizationLastModifiedD = CDateTime.Create(DateTime.Today);

                    try
                    {
                        dataLoan.Save();
                    }
                    catch (CBaseException exc)
                    {
                        Tools.LogError(exc);
                        result.AppendError("An unexpected error occurred");
                        return result.ToResponse();
                    }

                    // If we're here, we were successful in generating and saving the agencies. So we can go ahead and generate our xml result.
                    LoanLoXmlServiceResult validResult = new LoanLoXmlServiceResult();
                    validResult.Status = ServiceResultStatus.OK;
                    validResult.AddLoanElement(LoXmlServiceResult.CreateCollectionElement("sHomeownerCounselingOrganizationCollection", xmlRecords));
                    return validResult.ToResponse();
                }
            }
            else
            {
                result.AppendError(errors);
                return result.ToResponse();
            }
        }
    }
}
