﻿// <copyright file = "EmailOptions.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    /// <summary>
    /// Class that holds all email options.
    /// </summary>
    public class EmailOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether this document should be emailed.
        /// </summary>
        /// <value>A value indicating whether this document should be emailed.</value>
        public bool ShouldEmailDocuments
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the email address to send to.
        /// </summary>
        /// <value>The email address to send to.</value>
        public string EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the document will need a password.
        /// </summary>
        /// <value>A value indicating whether the document will need a password.</value>
        public bool RequiresPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the password for the document.
        /// </summary>
        /// <value>The password for the document.</value>
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this should be notified on retrieval.
        /// </summary>
        /// <value>A value indicating whether this should be notified on retrieval.</value>
        public bool ShouldNotifyOnRetrieval
        {
            get;
            set;
        }
    }
}
