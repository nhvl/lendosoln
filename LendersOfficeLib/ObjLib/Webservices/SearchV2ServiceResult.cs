﻿// <copyright file = "SearchV2ServiceResult.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    /// <summary>
    /// Represents a webservice search response. 
    /// </summary>
    public sealed class SearchV2ServiceResult : ServiceResult
    {
        /// <summary>
        /// The list of hits in the result.
        /// </summary>
        private IEnumerable<LoanSearch.Basic.LoanDetails> loanDetails;

        /// <summary>
        /// Sets the results of the search.
        /// </summary>
        /// <param name="results">The results of the loan.</param>
        public void SetResults(IEnumerable<LoanSearch.Basic.LoanDetails> results)
        {
            this.loanDetails = results;
        }

        /// <summary>
        /// Writes extra result content in the response. In this case it writes the
        /// search results.
        /// </summary>
        /// <param name="writer">The xml writer to use.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            if (this.Status == ServiceResultStatus.Error)
            {
                return;
            }

            writer.WriteStartElement("collection");
            writer.WriteAttributeString("id", "loanlist");

            if (this.loanDetails != null)
            {
                foreach (var loanDetail in this.loanDetails)
                {
                    writer.WriteStartElement("record");
                    this.WriteFieldValue(writer, "sLNm", loanDetail.sLNm);
                    this.WriteFieldValue(writer, "sLPurposeT", loanDetail.sLPurposeT.ToString());
                    this.WriteFieldValue(writer, "sStatusT", loanDetail.sStatusT.ToString());
                    this.WriteFieldValue(writer, "sStatusD", loanDetail.sStatusD);
                    this.WriteFieldValue(writer, "sLienPosT", loanDetail.sLienPosT.ToString());
                    this.WriteFieldValue(writer, "sGsePurposeT", DataAccess.Tools.Get_sGseSptFriendlyDisplay(loanDetail.sGseSpT));
                    this.WriteFieldValue(writer, "sSpAddr", loanDetail.sSpAddr);
                    this.WriteFieldValue(writer, "sSpCity", loanDetail.sSpCity);
                    this.WriteFieldValue(writer, "sSpState", loanDetail.sSpState);
                    this.WriteFieldValue(writer, "sSpZip", loanDetail.sSpZip);
                    this.WriteFieldValue(writer, "sSpCounty", loanDetail.sSpCounty);
                    this.WriteFieldValue(writer, "sPrimaryAppId", loanDetail.sPrimaryAppId?.ToString() ?? string.Empty);

                    foreach (var app in loanDetail.Applicants)
                    {
                        writer.WriteStartElement("applicant");
                        this.WriteFieldValue(writer, "aAppId", app.aAppId.ToString());
                        this.WriteFieldValue(writer, "aBFirstNm", app.aBFirstNm);
                        this.WriteFieldValue(writer, "aBLastNm", app.aBLastNm);
                        this.WriteFieldValue(writer, "aBSsnLastFour", app.aBSsnLastFour);
                        this.WriteFieldValue(writer, "aCFirstNm", app.aCFirstNm);
                        this.WriteFieldValue(writer, "aCLastNm", app.aCLastNm);
                        this.WriteFieldValue(writer, "aCSsnLastFour", app.aCSsnLastFour);
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets a value indicating whether the root element should be LoXml.
        /// </summary>
        /// <returns>Always true.</returns>
        protected override bool IncludeLOXmlFormatRoot()
        {
            return true;
        }

        /// <summary>
        /// Writes a field tag with an id attrivate and value as the inner text.
        /// </summary>
        /// <param name="writer">The xml writer to write to.</param>
        /// <param name="id">The id attribute.</param>
        /// <param name="value">The inner text of the field element.</param>
        private void WriteFieldValue(XmlWriter writer, string id, string value)
        {
            writer.WriteStartElement("field");
            writer.WriteAttributeString("id", id);
            writer.WriteValue(value);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes a field tag with an id attrivate and value as the inner text. If value
        /// is null it does not write anything.
        /// </summary>
        /// <param name="writer">The xml writer to write to.</param>
        /// <param name="id">The id attribute.</param>
        /// <param name="value">The date to write as the value.</param>
        private void WriteFieldValue(XmlWriter writer, string id, DateTime? value)
        {
            if (value.HasValue)
            {
                this.WriteFieldValue(writer, id, value.Value.ToShortDateString());
            }
            else
            {
                this.WriteFieldValue(writer, id, string.Empty);
            }
        }
    }
}
