﻿// <copyright file = "SpecifiedFormsOptions.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/22/2016
// </summary>
namespace LendersOffice.ObjLib.Webservices
{
    using System.Collections.Generic;

    /// <summary>
    /// Options for selected which forms to generate.
    /// </summary>
    public class SpecifiedFormsOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedFormsOptions"/> class.
        /// </summary>
        public SpecifiedFormsOptions()
        {
            this.SelectedForms = new List<SpecifiedFormInfo>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether only specific forms should be generated.
        /// </summary>
        /// <value>A value indicating whether only specific forms should be generated.</value>
        public bool ShouldGenerateSpecificForms
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of info for all forms that should be generated.
        /// </summary>
        /// <value>A list of info for all forms that should be generated.</value>
        public List<SpecifiedFormInfo> SelectedForms
        {
            get;
            set;
        }
    }
}
