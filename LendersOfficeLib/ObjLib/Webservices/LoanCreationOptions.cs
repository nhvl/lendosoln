﻿namespace LendersOffice.ObjLib.Webservices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Admin;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions;
    using LendersOffice.Security;

    /// <summary>
    /// Defines a set of options to be passed into the loan creation process.
    /// </summary>
    public class LoanCreationOptions
    {
        /// <summary>
        /// Dictionary containing additional strings that we accept as valid roles.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, E_RoleT> AdditionalValidRoleDescriptions = new Dictionary<string, E_RoleT>(StringComparer.OrdinalIgnoreCase)
        {
            { "LoanOfficer", E_RoleT.LoanOfficer }
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCreationOptions"/> class.
        /// </summary>
        /// <param name="isLead">A value indicating whether the created loan file should be a lead file.</param>
        /// <param name="isTestFile">A value indicating whether the created loan file should be a test file.</param>
        /// <param name="templateId">The identifier of the template to create the loan from, or <see cref="Guid.Empty"/> if no template is desired.</param>
        /// <param name="employeesToAssign">Dictionary mapping the roles to the users that are to be assigned to the loan file.</param>
        /// <param name="sourceUser">Principal of the user that will be the source of some information used during loan creation.</param>
        public LoanCreationOptions(bool isLead = false, bool isTestFile = false, Guid templateId = default(Guid), Dictionary<E_RoleT, Guid> employeesToAssign = null, AbstractUserPrincipal sourceUser = null)
        {
            this.IsLead = isLead;
            this.IsTestFile = isTestFile;
            this.TemplateId = templateId;
            this.EmployeesToAssign = employeesToAssign;
            this.SourceUser = sourceUser;
        }

        /// <summary>
        /// Gets a value indicating whether the created loan file should be a lead file.
        /// </summary>
        /// <value>A value indicating whether the created loan file should be a lead file.</value>
        public bool IsLead { get; }

        /// <summary>
        /// Gets a value indicating whether the created loan file should be a test file.
        /// </summary>
        /// <value>A value indicating whether the created loan file should be a test file.</value>
        public bool IsTestFile { get; }

        /// <summary>
        /// Gets the identifier of the template to create the loan from, or <see cref="Guid.Empty"/> if no template is desired.
        /// </summary>
        /// <value>The identifier of the template to create the loan from, or <see cref="Guid.Empty"/> if no template is desired.</value>
        public Guid TemplateId { get; }

        /// <summary>
        /// Gets a dictionary mapping the roles to the users that are to be assigned to the loan file.
        /// </summary>
        /// <value>The users to be assigned to the loan file.</value>
        public Dictionary<E_RoleT, Guid> EmployeesToAssign { get; }

        /// <summary>
        /// Gets the principal of the user that will be the source of some information used during loan creation. In particular:
        /// 1. Branch Id
        /// 2. Initial loan assignments.
        /// 3. Loan assignments using relationships.
        /// 4. Final branch and branch channel assignment.
        /// </summary>
        /// <value>Principal of the user that will be the source of some information used during loan creation.</value>
        public AbstractUserPrincipal SourceUser { get; }

        /// <summary>
        /// Parses a loan creation options XML string into its object form.
        /// </summary>
        /// <param name="optionsXml">The options XML to parse.</param>
        /// <param name="creator">The principal of the user creating the file.</param>
        /// <returns>A parsing result to wrap both the resulting object and any error that may have occurred during parsing.</returns>
        public static ParseResult<LoanCreationOptions> ParseLoanCreationOptionsXml(string optionsXml, AbstractUserPrincipal creator)
        {
            var brokerId = creator.BrokerId;
            ParseResult<ServiceRequestOptions> requestOptionsParseResult = ServiceRequestOptions.ParseRequestOptions(optionsXml);
            if (!requestOptionsParseResult.IsSuccessful)
            {
                return ParseResult<LoanCreationOptions>.Failure(optionsXml, requestOptionsParseResult.ErrorMessage);
            }

            ServiceRequestOptions requestOptions = requestOptionsParseResult.ValueOrDefault;

            bool isLead = requestOptions.Fields.GetValueOrNull("IsLead")?.ToNullable<bool>(bool.TryParse) ?? false;
            bool isTestFile = requestOptions.Fields.GetValueOrNull("IsTestFile")?.ToNullable<bool>(bool.TryParse) ?? false;

            Guid templateId = requestOptions.Fields.GetValueOrNull("TemplateId")?.ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;
            string templateName = requestOptions.Fields.GetValueOrNull("TemplateNm");
            if (templateId == Guid.Empty && !string.IsNullOrEmpty(templateName))
            {
                templateId = DataAccess.Tools.GetLoanIdByLoanName(brokerId, templateName);
                if (templateId == Guid.Empty)
                {
                    return ParseResult<LoanCreationOptions>.Failure(optionsXml, ErrorMessages.CannotFindLoanTemplate);
                }
            }

            if (isLead && isTestFile)
            {
                return ParseResult<LoanCreationOptions>.Failure(optionsXml, ErrorMessages.NewTestLeadsUnsupported);
            }

            AbstractUserPrincipal sourceUser = null;
            Dictionary<E_RoleT, Guid> employeesToAssign = null;
            var assignments = requestOptions.Collections.GetValueOrNull("Assignments");
            if (assignments != null)
            {
                string parseError = null;
                var assignmentParseResults = ParseAssignments(creator, assignments, out parseError);
                if (assignmentParseResults == null)
                {
                    return ParseResult<LoanCreationOptions>.Failure(optionsXml, parseError);
                }

                employeesToAssign = assignmentParseResults.Item1;
                sourceUser = assignmentParseResults.Item2;
            }

            return ParseResult<LoanCreationOptions>.Success(
                optionsXml,
                new LoanCreationOptions(
                    isLead: isLead, 
                    isTestFile: isTestFile,
                    templateId: templateId,
                    employeesToAssign: employeesToAssign,
                    sourceUser: sourceUser));
        }

        /// <summary>
        /// Parses and validates the role assignments.
        /// </summary>
        /// <param name="creator">The principal of the user creating the file.</param>
        /// <param name="assignments">The assignments to parse.</param>
        /// <param name="error">Errors if there are any.</param>
        /// <returns>A tuple. First item is the roles/employees to assign. Second item is the source user if any.</returns>
        private static Tuple<Dictionary<E_RoleT, Guid>, AbstractUserPrincipal> ParseAssignments(AbstractUserPrincipal creator, List<ServiceRequestOptions> assignments, out string error)
        {
            var employeesToAssign = new Dictionary<E_RoleT, Guid>();
            AbstractUserPrincipal sourceUser = null;

            Dictionary<string, E_RoleT> validRoleDescriptions = Role.AllRoles.ToDictionary((role) => role.Desc, (role) => role.RoleT, StringComparer.OrdinalIgnoreCase);
            BrokerLoanAssignmentTable possibleEmployees = new BrokerLoanAssignmentTable();
            foreach (var record in assignments)
            {
                var login = record.Fields.GetValueOrNull("Login");
                var userType = record.Fields.GetValueOrNull("UserType")?.ToUpper();
                var roleDesc = record.Fields.GetValueOrNull("Role");

                if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(userType) || string.IsNullOrEmpty(roleDesc))
                {
                    error = $"Employee not fully specified. Login: {login} UserType: {userType} Role: {roleDesc}";
                    return null;
                }

                if (userType != "B" && userType != "P")
                {
                    error = "Please specify the employee as a 'P'(TPO) or 'B'(LQB) user.";
                    return null;
                }

                E_RoleT role;
                if (!validRoleDescriptions.TryGetValue(roleDesc, out role) && !AdditionalValidRoleDescriptions.TryGetValue(roleDesc, out role))
                {
                    error = $"Invalid role {roleDesc} specified.";
                    return null;
                }

                // Temporary for now. We only want to deal with loan officers.
                if (role != E_RoleT.LoanOfficer && role != E_RoleT.Pml_LoanOfficer)
                {
                    error = $"Only Loan Officers can be assigned this way.";
                    return null;
                }

                if (employeesToAssign.ContainsKey(role))
                {
                    error = $"Multiple employees assigned to role {roleDesc}.";
                    return null;
                }

                var employee = GetEmployee(possibleEmployees, creator, login, userType, role);
                if (employee == null)
                {
                    error = $"Unable to find employee eligible for this role. Login: {login} UserType: {userType} Role: {roleDesc}.";
                    return null;
                }

                employeesToAssign.Add(role, employee.EmployeeId);
                if (role == E_RoleT.LoanOfficer)
                {
                    // We're going to assume that if they specify a loan officer, they want to use it as the source user.
                    sourceUser = PrincipalFactory.RetrievePrincipalForUser(creator.BrokerId, employee.UserId, employee.UserType);
                }
            }

            error = null;
            return new Tuple<Dictionary<E_RoleT, Guid>, AbstractUserPrincipal>(employeesToAssign, sourceUser);
        }

        /// <summary>
        /// Checks if the login name belongs to an employee that the assigner can assign to the designated role.
        /// </summary>
        /// <param name="possibleEmployees">The loan assignment table that may have the role already populated.</param>
        /// <param name="assigner">The assigner.</param>
        /// <param name="loginName">The login name of the user to assign.</param>
        /// <param name="userType">The user type. Ensures that we get the exact employee since login names don't need to be unique between B and P users.</param>
        /// <param name="role">The role to assign the user to.</param>
        /// <returns>The specifications of the user if found. Null otherwise.</returns>
        private static BrokerLoanAssignmentTable.Spec GetEmployee(BrokerLoanAssignmentTable possibleEmployees, AbstractUserPrincipal assigner, string loginName, string userType, E_RoleT role)
        {
            if (possibleEmployees[role] == null)
            {
                Guid branchId = assigner.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) ? Guid.Empty : assigner.BranchId;
                possibleEmployees.Retrieve(assigner.BrokerId, branchId, string.Empty, role, E_EmployeeStatusFilterT.ActiveOnly);
            }

            IEnumerable<BrokerLoanAssignmentTable.Spec> usersForThisRole = possibleEmployees[role];
            return usersForThisRole.FirstOrDefault((spec) => loginName.Trim().Equals(spec.LoginName, StringComparison.OrdinalIgnoreCase) && 
                                                             userType.Trim().Equals(spec.UserType, StringComparison.OrdinalIgnoreCase));
        }
    }
}
