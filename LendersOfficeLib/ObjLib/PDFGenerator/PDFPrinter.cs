using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.PdfLayout;
namespace LendersOffice.PdfGenerator
{
    /// <summary>
    /// This class represents one single PDF form file.
    /// </summary>
    public class PDFForm 
    {
        private Dictionary<string, string> m_formValues;

        public PDFForm(string fileName, string layoutFileName, string pdfFile) 
        {
            FileName = fileName;
            LayoutFileName = layoutFileName;
            m_formValues = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            PdfFile = pdfFile;    
        }

        public string FileName { get; private set;}

        public string LayoutFileName { get; private set;}
        public string PdfFile { get; private set; }

        public string this[string key]
        {
            get { return (string) m_formValues[key]; }
            set { m_formValues[key] = value; }
        }
        
        public ICollection Keys 
        {
            get { return m_formValues.Keys; }
        }
    }

	public class PDFPrinter
	{
		public PDFPrinter()
		{
		}

        public bool IsTestMode { get; set; }
        /// <summary>
        /// If both owner password and user password are blank then no encryption will be set.
        /// If only user password is blank  then document will protected from modifying with owner password.
        /// if owner password is blank and user pasword is set then document will set owner document same as user password.
        /// </summary>
        /// <param name="pages"></param>
        /// <param name="ownerPassword"></param>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        private void PrintImpl(List<PDFForm> pages, string ownerPassword, string userPassword, Stream outputStream)
        {
            Document document = new Document();
            string tempFile = TempFileUtils.NewTempFilePath();

            // OPM 219992.  It is desired to write to file, then stream file to response in chunks.
            using (FileStream fileStream = File.Create(tempFile))
            {
                PdfCopy writer = new PdfCopy(document, fileStream);
                if (!string.IsNullOrEmpty(ownerPassword) || !string.IsNullOrEmpty(userPassword))
                {
                    if (string.IsNullOrEmpty(ownerPassword))
                    {
                        ownerPassword = userPassword;
                    }
                    writer.SetEncryption(true, userPassword, ownerPassword, PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_SCREENREADERS);
                }
                document.Open();

                foreach (PDFForm page in pages)
                {
                    try
                    {
                        PdfFormContainer container = null;
                        if (page.FileName.StartsWith(ConstAppDavid.CustomPdfProtocolPrefix))
                        {
                            Guid formId = new Guid(page.FileName.Substring(ConstAppDavid.CustomPdfProtocolPrefix.Length));
                            PdfForm.PdfForm pdfForm = PdfForm.PdfForm.LoadById(formId);

                            container = new PdfFormContainer(pdfForm.PdfContent, pdfForm.Layout);
                        }
                        else
                        {
                            PdfFormLayout layout = new PdfFormLayout();
                            layout.Load(LendersOffice.PdfForm.PdfFormHelper.GetStandardPdfLayoutPath(page.PdfFile));

                            container = new PdfFormContainer(LendersOffice.PdfForm.PdfFormHelper.GetStandardPdfPath(page.PdfFile), layout);
                        }
                        container.IsTestMode = IsTestMode;

                        foreach (string key in page.Keys)
                        {
                            string value = page[key];
                            if (null != value)
                            {
                                container.SetFieldValue(key, value);
                            }
                        }
                        container.AppendTo(writer);
                    }
                    catch (Exception exc)
                    {
                        Tools.LogErrorWithCriticalTracking("Unable to render " + page.PdfFile + " :: " + page.FileName, exc);
                        throw;
                    }

                }

                document.Close();
            }

            // OPM 219992.  It is desired to do this in chunks from file.
            FileInfo fileInfo = new FileInfo(tempFile);
            int blockSize = 32768;

            if (fileInfo.Length < blockSize)
            {
                blockSize = (int)fileInfo.Length;
            }

            byte[] buffer = new byte[blockSize];
            int bytesRead;

            using (FileStream fs = fileInfo.OpenRead())
            {
                while ((bytesRead = fs.Read(buffer, 0, blockSize)) > 0)
                {
                    outputStream.Write(buffer, 0, bytesRead);
                }
                outputStream.Flush();
            }
        }


        public void Print(Stream outputStream, List<PDFForm> pages, string ownerPassword, string userPassword)
        {
            PrintImpl(pages, ownerPassword, userPassword, outputStream);
        }


	}


}
