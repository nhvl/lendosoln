namespace LendersOffice.PdfGenerator
{
    using System.Collections.Generic;
    using System.IO;

    using LendersOffice.PdfLayout;

    /// <summary>
    /// Concatenate multiple instances of <see cref="AbstractPDFFile"/> output into one PDF file.
    /// This is a more specific form of <see cref="GenericBatchPDF"/>, which only requires <see cref="IPDFGenerator"/>.<para/>
    /// Note that this class will make assumptions which may not allow for proper processing of custom PDFs (<see cref="LendersOffice.Pdf.CCustomPDF"/>).
    /// </summary>
    /// <seealso cref="GenericBatchPDF"/>
    public class BatchPDFControl : IPDFGenerator
	{
        private List<AbstractPDFFile> m_list;

		public BatchPDFControl()
		{
            m_list = new List<AbstractPDFFile>();
		}

        /// <summary>
        /// Append to pdf file.
        /// </summary>
        /// <param name="pdf"></param>
        public void Add(AbstractPDFFile pdf) 
        {
            IsTestMode = pdf.IsTestMode;
            m_list.Add(pdf);
        }


        public bool HasPdfFormLayout => true;

        public PdfFormLayout PdfFormLayout => this.GetPdfLayout();

        public PdfFormLayout GetPdfLayout()
        {
            // 6/7/2010 dd - Call this method after you invoke GeneratePdf method.
            // 2018-02 tj - despite the naming, page and m_pages are actually complete forms.
            // This code thus assumes that every form that is included in m_list is only one
            // page.  This works fine for standard PDFs, but Custom PDFs may be any number of
            // pages, so this code will not work for general custom PDFs.

            PdfFormLayout layout = new PdfFormLayout();
            int currentPageNum = 1;
            foreach (AbstractPDFFile f in m_list)
            {
                f.Initialize();
                foreach (var page in f.m_pages)
                {
                    foreach (var field in f.PdfFormLayout.FieldList)
                    {
                        field.PageNumber = currentPageNum;
                        var copy = new PdfField(field);
                        layout.Add(copy);
                    }
                    currentPageNum++;
                }
                f.Reset();
            }
            return layout;
        }
        public void Add(BatchPDFControl pdf) 
        {
            m_list.AddRange(pdf.m_list);
        }

        public int Count 
        {
            get { return m_list.Count; }
        }
        public PDFPageSize PageSize 
        {
            get { return PDFPageSize.Legal; }
        }

        public string PdfName
        {
            get;
            set;
        }
        /// <summary>
        /// Override this method if you are inherit from this class to do initialization.
        /// This method call when GeneratePDF is invoked.
        /// </summary>
        public virtual void Initialize() 
        {
        }

        protected bool IsTestMode { get; set; }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            GeneratePdfImpl(outputStream, ownerPassword, userPassword);
        }
        private void GeneratePdfImpl(Stream outputStream, string ownerPassword, string userPassword)
        {
            Initialize();
            List<PDFForm> list = new List<PDFForm>();

            foreach (AbstractPDFFile pdf in m_list)
            {
                pdf.Initialize();
                list.AddRange(pdf.m_pages);
                pdf.Reset();
            }

            PDFPrinter printer = new PDFPrinter();
            printer.IsTestMode = IsTestMode;
            printer.Print(outputStream, list, ownerPassword, userPassword);
        }

	}
}
