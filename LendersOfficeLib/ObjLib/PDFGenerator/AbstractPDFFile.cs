using System;
using System.Collections.Generic;
using System.IO;
using LendersOffice.PdfLayout;

namespace LendersOffice.PdfGenerator
{
	/// <summary>
	/// This class represents a collection of PDFForm object.
	/// 
	/// Usage:
	///    Inherit and override ApplyData method.
	///    Invoke GeneratePDF() to return a byte array of the created pdf.
	/// </summary>
	public abstract class AbstractPDFFile : IPDFGenerator
	{
        internal List<PDFForm> m_pages = new List<PDFForm>();
        private PDFForm m_currentForm;
		public AbstractPDFFile()
		{
		}
        protected abstract string PdfFileName
        {
            get;
        }
        protected abstract string PdfFileLayoutName
        {
            get;
        }
        public abstract PDFPageSize PageSize 
        {
            get;
        }
        public string PdfName
        {
            get;
            set;
        }
        public abstract string PdfFile
        {
            get;
        }

        public bool HasPdfFormLayout => true;

        private PdfFormLayout x_formLayout;

        /// <summary>
        /// Gets the standard PDF form layout for this document.  This will fail for custom PDFs.
        /// </summary>
        public virtual PdfFormLayout PdfFormLayout
        {
            get
            {
                if (x_formLayout == null)
                {
                    x_formLayout = new PdfFormLayout();
                    x_formLayout.Load(LendersOffice.PdfForm.PdfFormHelper.GetStandardPdfLayoutPath(PdfFile));
                }

                return x_formLayout;
            }
        }

        public bool IsContainSignature => this.PdfFormLayout.IsContainSignature;

        public bool IsContainBorrowerSignature => this.PdfFormLayout.IsContainBorrowerSignature;

        public bool IsContainCoborrowerSignature => this.PdfFormLayout.IsContainCoborrowerSignature;

        protected void AddNewPage() 
        {
            m_currentForm = new PDFForm(PdfFileName, PdfFileLayoutName, PdfFile);
            m_pages.Add(m_currentForm);
        }

        protected abstract void ApplyData();

        protected void AddFormFieldData(string fieldName, bool value) 
        {
            //AddFormFieldData(fieldName, value.ToString(), false);
            //AddFormFieldData(fieldName, value ? "1" : "0", false);
            AddFormFieldData(fieldName + ":" + (value ? "1" : "0"), "Yes", false);
        }
        protected void AddFormFieldData(string fieldName, Enum value) 
        {
            //AddFormFieldData(fieldName, value.ToString("D"), true);
            AddFormFieldData(fieldName + ":" + value.ToString("D"), "Yes", false);
        }
        /// <summary>
        /// Apply value to field. Convert value to upper case.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        protected void AddFormFieldData(string fieldName, string value) 
        {
            AddFormFieldData(fieldName, value, true);
        }
        protected void AddFormFieldData(string fieldName, string value, bool isUpper) 
        {
            if (value == null)
                return;

            // Note: Since all values are now convert to upper case, therefore
            // existing checkbox that expect "True" as export value will no
            // longer get check correctly. Out of laziness to go through all the pdf
            // locating and fixing the export value, I will just catch the 'True', and
            // 'False' here and not convert to upper case. David D 8/26/2002
            if (value == "True" || value == "False") 
            {
                m_currentForm[fieldName] = value;
            } 
            else 
            {
                m_currentForm[fieldName] = isUpper ? value.ToUpper() : value;
            }

        }
        protected string GetFormFieldData(string fieldName)
        {
            return m_currentForm[fieldName];
        }


        /// <summary>
        /// Create name/value hash table for this form.
        /// </summary>
        internal void Initialize() 
        {
            m_currentForm = new PDFForm(PdfFileName, PdfFileLayoutName, PdfFile);
            m_pages.Add(m_currentForm);
            ApplyData();

        }

        /// <summary>
        /// Call this method, if you are manually invoke Initialize();
        /// </summary>
        internal void Reset() 
        {
            m_pages = new List<PDFForm>();
        }

        public bool IsTestMode { get; set; }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            GeneratePdfImpl(outputStream, ownerPassword, userPassword);
        }

        private void GeneratePdfImpl(Stream outputStream, string ownerPassword, string userPassword)
        {
            Initialize();
            PDFPrinter printer = new PDFPrinter();
            printer.IsTestMode = IsTestMode;
            printer.Print(outputStream, m_pages, ownerPassword, userPassword);
            Reset();
        }
    }
}
