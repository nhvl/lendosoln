namespace LendersOffice.PdfGenerator
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using iTextSharp.text;
    using iTextSharp.text.pdf;

    /// <summary>
    /// Provides a simple interface to generating one PDF from multiple instances of <see cref="IPDFGenerator"/>.
    /// If using instances of <see cref="AbstractPDFFile"/>, consider using <see cref="BatchPDFControl"/> instead.
    /// </summary>
    /// <seealso cref="BatchPDFControl"/>
    public class GenericBatchPDF : IPDFGenerator
    {
        private List<IPDFGenerator> m_list;

        public GenericBatchPDF()
        {
            m_list = new List<IPDFGenerator>();
        }

        /// <summary>
        /// Gets the number of PDF files in the batch.
        /// </summary>
        public int Count => this.m_list.Count;

        public void Add(IPDFGenerator pdf)
        {
            m_list.Add(pdf);
        }

        public PDFPageSize PageSize
        {
            get { return PDFPageSize.Legal; }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="PdfFormLayout"/> has data.
        /// This will be false until <see cref="GeneratePDF(Stream, string, string)"/> is called.
        /// </summary>
        /// <remarks>
        /// Ideally, this would be immutable and set when the instance is created, but since this
        /// class is intended to handle any instance of <see cref="IPDFGenerator"/>, there won't
        /// be a way to know how long the PDFs will be until they're actually generated.
        /// </remarks>
        public bool HasPdfFormLayout { get; private set; }

        /// <summary>
        /// Gets the layout of the form data applied to the current form.
        /// This will be null if there is no data or <see cref="GeneratePDF(Stream, string, string)"/> has not been called.
        /// </summary>
        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout { get; private set; }

        public string PdfName { get; set; }

        private void GeneratePdfImpl(string ownerPassword, string userPassword, Stream outputStream)
        {
            Document document = new Document();
            PdfCopy writer = new PdfCopy(document, outputStream);

            if (!string.IsNullOrEmpty(ownerPassword) || !string.IsNullOrEmpty(userPassword))
            {
                if (string.IsNullOrEmpty(ownerPassword))
                {
                    ownerPassword = userPassword;
                }
                writer.SetEncryption(true, userPassword, ownerPassword, PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_SCREENREADERS);
            }

            document.Open();
            int processedPageCount = 0;
            var layout = new PdfLayout.PdfFormLayout();
            foreach (IPDFGenerator pdf in m_list)
            {
                byte[] buffer = null;
                using (MemoryStream stream = new MemoryStream())
                {
                    pdf.GeneratePDF(stream, "", "");
                    buffer = stream.ToArray();
                }

                PdfReader reader = new PdfReader(buffer);
                int numberOfPages = reader.NumberOfPages;
                for (int i = 1; i <= numberOfPages; i++)
                {
                    writer.AddPage(writer.GetImportedPage(reader, i));
                }

                if (pdf.HasPdfFormLayout)
                {
                    foreach (var field in pdf.PdfFormLayout.FieldList)
                    {
                        layout.Add(new PdfLayout.PdfField(field)
                        {
                            PageNumber = processedPageCount + field.PageNumber
                        });
                    }
                }

                processedPageCount += numberOfPages;
            }

            document.Close();

            this.HasPdfFormLayout = layout.FieldList.Any();
            this.PdfFormLayout = this.HasPdfFormLayout ? layout : null;
        }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            GeneratePdfImpl(ownerPassword, userPassword, outputStream);
        }

    }
}
