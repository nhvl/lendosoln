using System;
using System.IO;

namespace LendersOffice.PdfGenerator
{
    public enum PDFPageSize 
    {
        Letter = 0,
        Legal = 1
    }
	/// <summary>
	/// Summary description for IPDFGenerator.
	/// </summary>
	public interface IPDFGenerator
	{

        void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword);
        /// <summary>
        /// Indicate whether the paper size of this PDF.
        /// </summary>
        PDFPageSize PageSize
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="PdfFormLayout"/> has data.
        /// </summary>
        bool HasPdfFormLayout { get; }

        /// <summary>
        /// Gets the layout of the form data applied to the current form.
        /// </summary>
        /// <remarks>
        /// This is exposed for copying signature location metadata to the PDF so we can tell DocuSign signature locations.
        /// </remarks>
        LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout { get; }

        string PdfName
        {
            get;
            set;
        }
	}

}
