﻿namespace LendersOffice.ObjLib.PDFGenerator
{
    using LendersOffice.Pdf;
    using PdfGenerator;

    /// <summary>
    /// Interface for pdf print items that know how they are generated.
    /// </summary>
    public interface IPDFPrintItemGenerator : IPDFPrintItem, IPDFGenerator
    {
    }
}
