﻿namespace LendersOffice.ObjLib.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// A view of comments for LendersOffice users.
    /// </summary>
    [DataContract]
    public class ViewOfCommentForLoUser
    {
        /// <summary>
        /// For displaying dates we don't need to present minutes or seconds.
        /// </summary>
        private static readonly string DateFormatForViewing = "M/dd/yyyy h:mmtt";

        /// <summary>
        /// For parsing dates, we do want to account for minutes and seconds as people may be commenting seconds apart.
        /// </summary>
        private static readonly string DateFormatForParsing = "yyyy-MM-dd hh:mm:sstt";

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCommentForLoUser"/> class. <para/>
        /// This is only used for deserialization.
        /// </summary>
        public ViewOfCommentForLoUser()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCommentForLoUser"/> class. <para/>
        /// </summary>
        /// <param name="comment">The comment we want the view of.</param>
        /// <param name="timeZoneOfViewer">The time zone of the viewer.</param>
        /// <param name="permissionLevelId">The permission level of the comment, inherited from the conversation.</param>
        /// <param name="categoryId">The id of the category of the conversation.</param>
        public ViewOfCommentForLoUser(Comment comment, TimeZoneInfo timeZoneOfViewer, long? permissionLevelId, long categoryId)
        {
            var createdDateUtc = DateTime.SpecifyKind(DateTime.ParseExact(comment.Created.ToString(DateFormatForParsing), DateFormatForParsing, new System.Globalization.CultureInfo("en-US")), DateTimeKind.Utc);
            var createdDateZonedTime = Tools.GetZonedTimeFromUtcTimeAndViewerTimeZone(createdDateUtc, timeZoneOfViewer);

            this.Lines = comment.Value.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None)
                        .SelectMany(l => l.Split(new char[] { '\r' }, StringSplitOptions.None))
                        .SelectMany(l => l.Split(new char[] { '\n' }, StringSplitOptions.None)).ToList();

            this.CreatedDateForJs = new DateForJs(createdDateZonedTime);
            this.CreatedDateStringForDisplay = Tools.GetUtcDateTimeAsStringWithTimeZone(createdDateUtc, timeZoneOfViewer, DateFormatForViewing);
            this.CommenterName = comment.Commenter.ToString();
            this.Id = comment.Identity.Id;
            this.Depth = comment.Depth.Value;
            this.IsHidden = comment.IsHidden;
            this.HiderFullName = comment.HiderFullName.HasValue ? comment.HiderFullName.ToString() : string.Empty;

            // These fields come from the conversation.  They are merely for convenience when working with jquery templates.
            this.PermissionLevelId = permissionLevelId;
            this.CategoryId = categoryId;
        }

        /// <summary>
        /// Gets or sets the view's lines of text.
        /// </summary>
        /// <value>The lines of text of the comment.</value>
        [DataMember(Name = "lines")]
        public List<string> Lines { get; set; }

        /// <summary>
        /// Gets or sets the view's date string as it should be displayed in the UI.
        /// </summary>
        /// <value>The date as it should appear in the UI of the comment.</value>
        [DataMember(Name = "createdDateStringForDisplay")]
        public string CreatedDateStringForDisplay { get; set; }

        /// <summary>
        /// Gets or sets the view's date that can be easily converted to or from a JavaScript date.
        /// </summary>
        /// <value>The date of the comment.</value>
        [DataMember(Name = "createdDateForJs")]
        public DateForJs CreatedDateForJs { get; set; }

        /// <summary>
        /// Gets or sets the name of the commenter.
        /// </summary>
        /// <value>The name of the commenter.</value>
        [DataMember(Name = "commenterName")]
        public string CommenterName { get; set; }

        /// <summary>
        /// Gets or sets the id of the view of the comment.
        /// </summary>
        /// <value>The id of the comment.</value>
        [DataMember(Name = "id")]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the depth of the view of the comment.
        /// </summary>
        /// <value>The depth of the comment relative to the top of the comment tree.</value>
        [DataMember(Name = "depth")]
        public int Depth { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level that this comment has, which is inherited from the conversation.
        /// </summary>
        /// <value>The id of the permission level that this comment has, which is inherited from the conversation..</value>
        [DataMember(Name = "permissionLevelId")]
        public long? PermissionLevelId { get; set; }

        /// <summary>
        /// Gets or sets the id of the category associated with the conversation.
        /// </summary>
        /// <value>The id fo the category for the conversation.</value>
        [DataMember(Name = "categoryId")]
        public long CategoryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the comment is hidden.
        /// </summary>
        /// <value>True iff the comment is hidden.</value>
        [DataMember(Name = "isHidden")]
        public bool IsHidden { get; set; }

        /// <summary>
        /// Gets or sets the full name of the person who hid the comment, or null.
        /// </summary>
        /// <value>The full name of the person who hid the comment, or null.</value>
        [DataMember(Name = "hiderFullName")]
        public string HiderFullName { get; set; }
    }
}
