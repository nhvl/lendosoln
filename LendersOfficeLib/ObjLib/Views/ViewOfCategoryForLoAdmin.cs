﻿namespace LendersOffice.ObjLib.Views
{
    using System.Runtime.Serialization;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// A view of a category specifically for LoAdmin.
    /// </summary>
    [DataContract]
    public class ViewOfCategoryForLoAdmin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCategoryForLoAdmin" /> class. <para/>
        /// Intended only for deserialization.
        /// </summary>
        public ViewOfCategoryForLoAdmin()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCategoryForLoAdmin" /> class. <para/>
        /// </summary>
        /// <param name="c">The category this view represented at the time of construction.</param>
        public ViewOfCategoryForLoAdmin(Category c)
        {
            this.Id = c.Identity.Id;
            this.Name = c.Identity.Name.ToString();
            this.IsActive = c.IsActive;
            this.DisplayName = c.DisplayName.ToString();
            this.DefaultPermissionLevelId = c.DefaultPermissionLevelId;
        }

        /// <summary>
        /// Gets or sets the id in the view of the category.
        /// </summary>
        /// <value>The id of the category.</value>
        [DataMember(Name = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets the name in the view of the category.
        /// </summary>
        /// <value>The name of the category.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display name for the view of the category.
        /// </summary>
        /// <value>The display name of the category.</value>
        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the category is active.
        /// </summary>
        /// <value>The state of activeness for the category.</value>
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the id of the default permission level associated with this category.
        /// </summary>
        /// <value>The id of the default permission level associated with this category, or null if none.</value>
        [DataMember(Name = "defaultPermissionLevelId")]
        public int? DefaultPermissionLevelId { get; set; }

        /// <summary>
        /// Returns a category for the loadmin view of the category.
        /// </summary>
        /// <param name="brokerIdString">The id of the broker this category belongs to.</param>
        /// <returns>A category corresponding to this view.</returns>
        public Category ToCategory(string brokerIdString)
        {
            return new Category()
            {
                IsActive = this.IsActive,
                Identity = new CategoryReference()
                {
                    Name = CommentCategoryName.Create(this.Name).Value,
                    OwnerId = CommentCategoryNamespace.Create(brokerIdString).Value,
                    Id = this.Id.HasValue ? this.Id.Value : -1
                },
                DisplayName = CommentCategoryName.Create(this.DisplayName).Value,
                DefaultPermissionLevelId = this.DefaultPermissionLevelId
            };
        }
    }
}
