﻿namespace LendersOffice.ObjLib.Views
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A view of dates that can easily be converted to a date in javascript.
    /// </summary>
    [DataContract]
    public class DateForJs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateForJs"/> class.
        /// </summary>
        /// <param name="dt">The date time this object represents.</param>
        public DateForJs(DateTime dt)
        {
            this.Year = dt.Year;
            this.ZeroBasedMonth = dt.Month - 1;
            this.DateOfMonth = dt.Day;
            this.Hour = dt.Hour;
            this.Minute = dt.Minute;
            this.Second = dt.Second;
        }

        /// <summary>
        /// Gets or sets the year of the date.
        /// </summary>
        /// <value>The year of the date.</value>
        [DataMember(Name = "year")]
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets the zero-based index for the month of the date.
        /// </summary>
        /// <value>The zero-based index for the month of the date.  January is 0, December is 11.</value>
        [DataMember(Name = "zeroBasedMonth")]
        public int ZeroBasedMonth { get; set; }

        /// <summary>
        /// Gets or sets the day of the month of the date.
        /// </summary>
        /// <value>The day of the month of the date.</value>
        [DataMember(Name = "dateOfMonth")]
        public int DateOfMonth { get; set; }

        /// <summary>
        /// Gets or sets the hour of the date.
        /// </summary>
        /// <value>The hour of the date.</value>
        [DataMember(Name = "hour")]
        public int Hour { get; set; }

        /// <summary>
        /// Gets or sets the minute of the date.
        /// </summary>
        /// <value>The minute of the date.</value>
        [DataMember(Name = "minute")]
        public int Minute { get; set; }

        /// <summary>
        /// Gets or sets the second of the date.
        /// </summary>
        /// <value>The second of the date.</value>
        [DataMember(Name = "second")]
        public int Second { get; set; }
    }
}
