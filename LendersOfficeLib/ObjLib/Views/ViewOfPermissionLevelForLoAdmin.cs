﻿namespace LendersOffice.ObjLib.Views
{
    using System;
    using System.Runtime.Serialization;
    using Security.Authorization.ConversationLog;

    /// <summary>
    /// A view of the permission level for the permission levels (plural) editor in LoAdmin.
    /// </summary>
    [DataContract]
    public class ViewOfPermissionLevelForLoAdmin
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level is active.
        /// </summary>
        /// <value>True iff the permission level is active.</value>
        [DataMember(Name = "isActive")]
        public bool IsActive
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the permission level.
        /// </summary>
        /// <value>The name of the permission level.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level.
        /// </summary>
        /// <value>The id of the permission level.</value>
        [DataMember(Name = "id")]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the description of the permission level.
        /// </summary>
        /// <value>The description of the permission level.</value>
        [DataMember(Name = "description")]
        public string Description
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the id of the broker of the permission level.
        /// </summary>
        /// <value>The id of the broker of the permission level.</value>
        [DataMember(Name = "brokerId")]
        public Guid BrokerId
        {
            get; set;
        }

        /// <summary>
        /// Gets a view of a permission level from the permission level.
        /// </summary>
        /// <param name="p">The permission level we want the view of.</param>
        /// <returns>A view of ther permission level for the permission levels editor.</returns>
        public static ViewOfPermissionLevelForLoAdmin FromPermissionLevel(ConversationLogPermissionLevel p)
        {
            var v = new ViewOfPermissionLevelForLoAdmin();

            v.IsActive = p.IsActive;
            v.Id = p.Id.Value;
            v.Description = p.Description;
            v.Name = p.Name;
            v.BrokerId = p.BrokerId.Value;

            return v;
        }
    }
}
