﻿namespace LendersOffice.ObjLib.Views
{
    using System.Runtime.Serialization;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// A view of categories for lendersoffice users.
    /// </summary>
    [DataContract]
    public class ViewOfCategoryForLoUser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCategoryForLoUser"/> class.
        /// </summary>
        public ViewOfCategoryForLoUser()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfCategoryForLoUser"/> class.
        /// </summary>
        /// <param name="c">The category this object represents.</param>
        public ViewOfCategoryForLoUser(Category c)
        {
            this.Id = c.Identity.Id;
            this.Name = c.Identity.Name.ToString();
            this.IsActive = c.IsActive;
            this.DisplayName = c.DisplayName.ToString();
            this.DefaultPermissionLevelId = c.DefaultPermissionLevelId;
        }

        /// <summary>
        /// Gets or sets the value of the id of the category.
        /// </summary>
        /// <value>The id of the category.</value>
        [DataMember(Name = "id")]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the value of the name of the category.
        /// </summary>
        /// <value>The name of the category.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value of the display name of the category.
        /// </summary>
        /// <value>The display name of the category.</value>
        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the category is active.
        /// </summary>
        /// <value>Whether or not the category is active.</value>
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the id of the default permission level associated with this category.
        /// </summary>
        /// <value>The id of the default permission level associated with this category, or null if none.</value>
        [DataMember(Name = "defaultPermissionLevelId")]
        public int? DefaultPermissionLevelId { get; set; }
    }
}
