﻿namespace LendersOffice.ObjLib.Views
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// A view of conversations for LendersOffice users.
    /// </summary>
    [DataContract]
    public class ViewOfConversationForLoUser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfConversationForLoUser"/> class.
        /// This is only used for deserialization.
        /// </summary>
        public ViewOfConversationForLoUser()
        {
            this.Comments = new System.Collections.Generic.List<ViewOfCommentForLoUser>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOfConversationForLoUser"/> class.
        /// </summary>
        /// <param name="conv">The conversation this view represents.</param>
        /// <param name="timeZoneOfViewer">The time zone of the viewer.</param>
        public ViewOfConversationForLoUser(Conversation conv, TimeZoneInfo timeZoneOfViewer)
        {
            this.CategoryId = conv.Category.Id;
            this.Comments = conv.Comments.Select(c => new ViewOfCommentForLoUser(c, timeZoneOfViewer, conv.PermissionLevelId, conv.Category.Id)).ToList();
            this.PermissionLevelId = conv.PermissionLevelId;
        }
        
        /// <summary>
        /// Gets or sets the value of the category id of the conversation.
        /// </summary>
        /// <value>The category id of the conversation.</value>
        [DataMember(Name = "categoryId")]
        public long CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the value of the ordered comments for the conversation.
        /// </summary>
        /// <value>The ordered list of comments of the conversation.</value>
        [DataMember(Name = "comments")]
        public System.Collections.Generic.List<ViewOfCommentForLoUser> Comments
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the id of the permission level associated with the conversation. <para></para>
        /// Null will mean not yet migrated, which will be interpreted as the Default.
        /// </summary>
        /// <value>The id of the permission level associated with the conversation, or null if not yet migrated.</value>
        [DataMember(Name = "permissionLevelId")]
        public long? PermissionLevelId
        {
            get; set;
        }
    }
}
