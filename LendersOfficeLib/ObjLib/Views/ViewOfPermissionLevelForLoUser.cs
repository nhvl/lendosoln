﻿namespace LendersOffice.ObjLib.Views
{
    using System.Runtime.Serialization;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;
    using LqbGrammar.DataTypes;    

    /// <summary>
    /// A view of a permission level for lo users.
    /// </summary>
    [DataContract]
    public class ViewOfPermissionLevelForLoUser
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level is active.
        /// </summary>
        /// <value>True iff the permission level is active.</value>
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level.  Null would mean not yet migrated.
        /// </summary>
        /// <value>The id of the permisison level, or null for the not yet migrated version.</value>
        [DataMember(Name = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the permission level.
        /// </summary>
        /// <value>The name of the permisison level.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the permission level.
        /// </summary>
        /// <value>The description of the permisison level.</value>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level can be posted to by the current user.
        /// </summary>
        /// <value>True iff the permission level can be posted to by the current user.</value>
        [DataMember(Name = "canPost")]
        public bool CanPost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level can be replied to by the current user.
        /// </summary>
        /// <value>True iff the permission level can be replied to by the current user.</value>
        [DataMember(Name = "canReply")]
        public bool CanReply { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level can have it's comments hidden by the current user.
        /// </summary>
        /// <value>True iff the permission level can have it's comments hidden by the current user.</value>
        [DataMember(Name = "canHide")]
        public bool CanHide { get; set; }

        /// <summary>
        /// A method of converting a permission level to a view of the permission level for the user.
        /// </summary>
        /// <param name="token">The security token of the user.</param>
        /// <param name="p">The permission level of interest.</param>
        /// <returns>A view of the permission level for the lo user.</returns>
        public static ViewOfPermissionLevelForLoUser FromPermissionLevel(SecurityToken token, ConversationLogPermissionLevel p)
        {
            var v = new ViewOfPermissionLevelForLoUser()
            {
                IsActive = p.IsActive,
                Id = p.Id,
                Name = p.Name,
                Description = p.Description,
                CanPost = PermissionLevelAuthorizer.Instance.CanRun(token, p, ConvoLogOperationType.Post),
                CanReply = PermissionLevelAuthorizer.Instance.CanRun(token, p, ConvoLogOperationType.Reply),
                CanHide = PermissionLevelAuthorizer.Instance.CanRun(token, p, ConvoLogOperationType.Hide),
            };

            return v;
        }
    }
}
