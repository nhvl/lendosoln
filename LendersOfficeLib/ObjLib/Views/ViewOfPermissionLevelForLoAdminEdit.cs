﻿namespace LendersOffice.ObjLib.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using Admin;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using Security.Authorization.ConversationLog;

    /// <summary>
    /// A view of the permission level for the permission level (singular) editor in LoAdmin.
    /// </summary>
    [DataContract]
    public class ViewOfPermissionLevelForLoAdminEdit
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not the permission level is active.
        /// </summary>
        /// <value>True iff the permission level is active.</value>
        [DataMember(Name = "isActive")]
        public bool IsActive
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the permission level.
        /// </summary>
        /// <value>The name of the permission level.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level.
        /// </summary>
        /// <value>The id of the permission level.</value>
        [DataMember(Name = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets the description of the permission level.
        /// </summary>
        /// <value>The description of the permission level.</value>
        [DataMember(Name = "description")]
        public string Description
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the id of the broker of the permission level.
        /// </summary>
        /// <value>The id of the broker of the permission level.</value>
        [DataMember(Name = "brokerId")]
        public Guid BrokerId
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets checked state structure of the permission level.<para></para>
        /// i.e. For each role/group and each operation, whether or not that role/group is enabled.
        /// </summary>
        /// <value>The checked states for all the roles/groups for every operation for the permission level.</value>
        [DataMember(Name = "checkedStates")]
        public Dictionary<string, Dictionary<ConvoLogOperationType, Dictionary<Guid, bool>>> CheckedStates
        {
            get; set;
        }

        /// <summary>
        /// Gets a view of a permission level from the permission level.
        /// </summary>
        /// <param name="p">The permission level we want the view of.</param>
        /// <returns>A view of ther permission level for the permission level editor.</returns>
        public static ViewOfPermissionLevelForLoAdminEdit FromPermissionLevel(ConversationLogPermissionLevel p)
        {
            var v = new ViewOfPermissionLevelForLoAdminEdit();

            v.IsActive = p.IsActive;
            v.Id = p.Id;
            v.Description = p.Description;
            v.BrokerId = p.BrokerId.Value;
            v.Name = p.Name;

            var checkedStates = new Dictionary<string, Dictionary<ConvoLogOperationType, Dictionary<Guid, bool>>>();
            var allRoleIds = Role.AllRoles.Select(r => r.Id);
            var allGroupIds = GroupDB.GetAllGroupIdsAndNames(p.BrokerId.Value, GroupType.Employee).Select(g => g.Item1);

            {
                var roleCheckedStates = new Dictionary<ConvoLogOperationType, Dictionary<Guid, bool>>();

                foreach (ConvoLogOperationType operationType in Enum.GetValues(typeof(ConvoLogOperationType)))
                {
                    var enabledRoleIdsForOp = p.GetRoleIdsForOperationType(operationType);
                    var checkStatesForRoleAndOp = new Dictionary<Guid, bool>();

                    foreach (var roleId in allRoleIds)
                    {
                        checkStatesForRoleAndOp.Add(roleId, enabledRoleIdsForOp.Contains(roleId));
                    }

                    roleCheckedStates.Add(operationType, checkStatesForRoleAndOp);
                }

                checkedStates.Add("role", roleCheckedStates);
            }

            {
                var groupCheckedStates = new Dictionary<ConvoLogOperationType, Dictionary<Guid, bool>>();

                foreach (ConvoLogOperationType operationType in Enum.GetValues(typeof(ConvoLogOperationType)))
                {
                    var enabledGroupIdsForOp = p.GetGroupIdsForOperationType(operationType);
                    var checkStatesForGroupAndOp = new Dictionary<Guid, bool>();

                    foreach (var groupId in allGroupIds)
                    {
                        checkStatesForGroupAndOp.Add(groupId, enabledGroupIdsForOp.Contains(groupId));
                    }

                    groupCheckedStates.Add(operationType, checkStatesForGroupAndOp);
                }

                checkedStates.Add("group", groupCheckedStates);
            }

            v.CheckedStates = checkedStates;

            return v;
        }

        /// <summary>
        /// Returns the permission level from the view.
        /// </summary>
        /// <returns>A permission level from the view.</returns>
        public ConversationLogPermissionLevel AsPermissionLevel()
        {
            ConversationLogPermissionLevel c;
            var brokerId = BrokerIdentifier.Create(this.BrokerId.ToString()).Value;

            if (this.Id.HasValue)
            {
                c = ConversationLogPermissionLevel.GetPermissionLevelById(brokerId, this.Id.Value);
            }
            else
            {
                c = new ConversationLogPermissionLevel(brokerId);
            }

            c.IsActive = this.IsActive;
            c.Name = this.Name;
            c.Description = this.Description;

            {
                var checkedStatesByOpForRole = this.CheckedStates["role"];
                foreach (ConvoLogOperationType operationType in Enum.GetValues(typeof(ConvoLogOperationType)))
                {
                    var checkedStatesForOp = checkedStatesByOpForRole[operationType];
                    var enabledRoleIds = checkedStatesForOp.Where(kvp => kvp.Value).Select(kvp => kvp.Key);
                    c.SetRoleIdsForOperationType(operationType, enabledRoleIds);
                }
            }

            {
                var checkedStatesByOpForGroup = this.CheckedStates["group"];
                foreach (ConvoLogOperationType operationType in Enum.GetValues(typeof(ConvoLogOperationType)))
                {
                    var checkedStatesForOp = checkedStatesByOpForGroup[operationType];
                    var enabledGroupIds = checkedStatesForOp.Where(kvp => kvp.Value).Select(kvp => kvp.Key);
                    c.SetGroupIdsForOperationType(operationType, enabledGroupIds);
                }
            }

            return c;
        }
    }
}
