using System;
using System.Data.Common;
using System.Data.SqlClient;
using CommonLib;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.HttpModule;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;
using System.Collections.Generic;
using System.Data;

namespace LendersOfficeApp.ObjLib.Licensing
{
	/// <summary>
	/// TODO: When renewing a license, need to expire the old one. Right now there is a bug in favor
	/// of the user (not a big deal)
	/// </summary>
	public class License
	{
		public const string LICENSE_PLAN_TYPE_ENFORCE = "YES" ;
		public const int RENEWAL_DISPLAY_DAYS = 10 ; // # of days remaining on license before renewal message is displayed
		public const int LICENSE_PLAN_COST_PER_YEAR = 100;
        

		public License(Guid brokerId, Guid licenseId)
		{
			m_licenseId = licenseId ;
            this.BrokerId = brokerId;

            using (PerformanceMonitorItem.Time("LendersOfficeApp.ObjLib.Licensing.License::ctor"))
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@LicenseId", m_licenseId)
                                            };

                using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(brokerId, "GetLicense", parameters))
                {
                    if (!sdr.Read())
                        throw new DataAccess.CBaseException("Unable to find license.", "Unable to find license. License Id=" + licenseId);

                    m_licenseNumber = Convert.ToInt32(sdr["LicenseNumber"]);
                    m_licenseOwnerBrokerId = new Guid(Convert.ToString(sdr["LicenseOwnerBrokerId"]));
                    m_seatCount = Convert.ToInt32(sdr["SeatCount"]);
                    m_openSeatCount = Convert.ToInt32(sdr["OpenSeatCount"]);
                    m_description = Convert.ToString(sdr["Description"]);
                    m_PurchaseDate = Convert.ToDateTime(sdr["PurchaseDate"]);
                    m_ExpirationDate = Convert.ToDateTime(sdr["ExpirationDate"]);
                }
            }
		}

        public Guid BrokerId { get; private set; }

		public Guid LicenseId
		{
			get { return m_licenseId ; }
		}
		public Guid LicenseOwnerBrokerId
		{
			get { return m_licenseOwnerBrokerId ; }
		}
		/// <summary>
		/// This is a more user-friendly method to identify licenses
		/// </summary>
		public int LicenseNumber
		{
			get { return m_licenseNumber ; }
		}
		public int SeatCount
		{
			get { return m_seatCount ; }
		}
		public int OpenSeatCount
		{
			get { return m_openSeatCount ; }
		}
		public string Description
		{
			get { return m_description ; }
		}
		public DateTime PurchaseDate
		{
			get { return m_PurchaseDate ; }
		}
		public DateTime ExpirationDate
		{
			get { return m_ExpirationDate ; }
		}
		public bool IsActive
		{
			get { return DateTime.Now < m_ExpirationDate ; }
		}
		public License PreviousLicense
		{
			get
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@RenewLicenseId", m_licenseId)
                                            };

				using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(BrokerId, "GetLicenseIdFromRenewLicenseId", parameters))
				{
					if (sdr.Read())
						return new License(this.BrokerId, (Guid) sdr["LicenseId"]) ;
					else
						return null ;
				}
			}
		}

        /// <summary>
        /// Updates the # of seats that are available for a license.
        /// </summary>
        public static void UpdateOpenSeatCount(Guid brokerId, Guid licenseId)
		{
            var listParams = new SqlParameter[] { new SqlParameter("@licenceId", licenseId) };
            string sql = "SELECT COUNT(*) AS SeatUsed FROM BROKER_USER WHERE CurrentLicenseId=@licenceId";

            int seatUsed = 0;
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read())
                {
                    throw new CBaseException(ErrorMessages.FailedToRetrieveBroker, "Broker_User record not found");
                }

                seatUsed = Convert.ToInt32(sdr["SeatUsed"]);
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            listParams = new SqlParameter[] { new SqlParameter("@usedCount", seatUsed), new SqlParameter("@licenceId", licenseId) };
            sql = "UPDATE LICENSE SET OpenSeatCount = SeatCount - @usedCount WHERE LicenseId=@licenceId";

            DBUpdateUtility.Update(brokerId, sql, null, listParams);
		}

		Guid m_licenseId ;
		Guid m_licenseOwnerBrokerId ;
		int m_licenseNumber ;
		int m_seatCount ;
		int m_openSeatCount ;
		string m_description ;
		DateTime m_PurchaseDate ;
		DateTime m_ExpirationDate ;
	}

	public class LicenseEstimate
	{
		public LicenseEstimate(int seatCount, double costPerSeat, DateTime expirationDate)
		{
			m_SeatCount = seatCount ;
			m_CostPerSeat = costPerSeat ;
			m_ExpirationDate = expirationDate ;
		}

		public int SeatCount
		{
			get { return m_SeatCount ; }
		}
		public double CostPerSeat
		{
			get { return m_CostPerSeat ; }
		}
		public double TotalCost
		{
			get { return m_SeatCount*m_CostPerSeat ; }
		}
		public DateTime ExpirationDate
		{
			get { return m_ExpirationDate ; }
		}
		int m_SeatCount ;
		double m_CostPerSeat ;
		DateTime m_ExpirationDate ;
	}

	public class BrokerUser
	{

        private License m_license = null;

        public BrokerUser(Guid userId, string displayName)
        {
            Tools.LogWarning("No broker id is provide");

            if (userId == Guid.Empty)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid BrokerUserId detected");
            }
            this.BrokerId = Guid.Empty;
            this.UserId = userId;
            this.DisplayName = displayName;
            this.EmployeeId = Guid.Empty;
        }

		public BrokerUser(Guid brokerId, Guid userId, string displayName)
		{
            if (userId == Guid.Empty)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid BrokerUserId detected");
            }
            this.BrokerId = brokerId;
			this.UserId = userId ;
			this.DisplayName = displayName ;
            this.EmployeeId = Guid.Empty;
		}
        public BrokerUser(Guid userId, Guid brokerId, Guid employeeId, string displayName, string permissions, string userType) 
        {
            this.UserId = userId;
            this.BrokerId = brokerId;
            this.EmployeeId = employeeId;
            this.Permissions = permissions;
            this.DisplayName = displayName;
            this.UserType = userType;
        }
		public string DisplayName { get; private set; }
        public Guid BrokerId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public Guid UserId { get; private set; }
        public string Permissions { get; private set; }

        private string UserType { get; set; }

		public License License
		{
			get
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", this.UserId)
                                            };

                string sql = "SELECT CurrentLicenseId FROM BROKER_USER WHERE UserId=@UserId";

                License license = null;
                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    if (!sdr.Read())
                    {
                        throw new CBaseException(ErrorMessages.FailedToRetrieveBroker, "Broker_User record not found");
                    }

                    if (!DBNull.Value.Equals(sdr["CurrentLicenseId"]))
                    {
                        license = new License(this.BrokerId, new Guid(sdr["CurrentLicenseId"].ToString()));
                    }
                };

                DBSelectUtility.ProcessDBData(this.BrokerId, sql, null, parameters, readHandler);

                return license;
            }
        }

        /// <summary>
        /// Assign this BrokerUser a new license
        /// </summary>
        public void AssignLicense(License newLicense)
        {
            // Security check to ensure that this license can be assigned to this BrokerUser by checking to see if the BrokerIds are the same
            CheckUserMatchesLicense(this.BrokerId, this.UserId, newLicense.LicenseOwnerBrokerId);

            Guid previousLicenseId = Guid.Empty;
            if (this.License != null)
                previousLicenseId = this.License.LicenseId;

            UpdateLicense(this.BrokerId, this.UserId, newLicense.LicenseId);

            // reset the previous license Open Seat Count
            if (previousLicenseId != Guid.Empty)
            {
                License.UpdateOpenSeatCount(this.BrokerId, previousLicenseId);
            }

            // Update the new license Open Seat Count
            License.UpdateOpenSeatCount(this.BrokerId, newLicense.LicenseId);

            // Cache the new license object
            m_license = newLicense;
        }

        public static void UpdateLicense(Guid brokerId, Guid userId, Guid newLicenseId)
        {
            var sql = "UPDATE BROKER_USER SET CurrentLicenseId=@CurrentLicenseId WHERE UserId=@UserId";

            var parameters = new SqlParameter[] {
                    new SqlParameter("@CurrentLicenseId", newLicenseId),
                    new SqlParameter("@UserId", userId)
            };

            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        public static void CheckUserMatchesLicense(Guid brokerId, Guid userId, Guid licenseOwnerBrokerId)
        {
            string sql = "SELECT BrokerId FROM EMPLOYEE a, BRANCH B WHERE a.EmployeeUserId=@UserId AND a.BranchId=b.BranchId";
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (sdr.Read())
                {
                    if (licenseOwnerBrokerId != new Guid(sdr["BrokerId"].ToString()))
                        throw new CBaseException(ErrorMessages.InvalidLicenseAssignment, ErrorMessages.InvalidLicenseAssignment);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.InvalidLicenseAssignment, ErrorMessages.InvalidLicenseAssignment);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
        }

        /// <summary>
        /// Set the current license to nothing
        /// </summary>
        public void UnassignLicense()
        {
            Guid previousLicenseId;
			if (m_license != null)
				previousLicenseId = m_license.LicenseId ;
			else
			{
                previousLicenseId = RetrieveCurrentLicense(this.BrokerId, this.UserId);
			}

            SetCurrentLicenseNull(this.BrokerId, this.UserId);
			
			m_license = null ;

            if (previousLicenseId != Guid.Empty)
            {
                License.UpdateOpenSeatCount(this.BrokerId, previousLicenseId);
            }
		}

        public static void SetCurrentLicenseNull(Guid brokerId, Guid userId)
        {
            string sql = "UPDATE BROKER_USER SET CurrentLicenseId=NULL WHERE UserId=@UserId";
            var parameters = new SqlParameter[] { new SqlParameter("@UserId", userId) };

            var sqlQuery = SQLQueryString.Create(sql);

            using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
            {
                SqlServerHelper.Update(sqlConnection, null, sqlQuery.Value, parameters, TimeoutInSeconds.Thirty);
            }
        }

        public static Guid RetrieveCurrentLicense(Guid brokerId, Guid userId)
        {
            string sql = "SELECT CurrentLicenseId FROM BROKER_USER WHERE UserId=@UserId";
            var parameters = new SqlParameter[] { new SqlParameter("@UserId", userId) };

            Guid licenseId = Guid.Empty;

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read())
                    throw new CBaseException(ErrorMessages.FailedToRetrieveBroker, "Broker_User record not found");

                if (DBNull.Value.Equals(sdr["CurrentLicenseId"]))
                    licenseId = Guid.Empty;
                else
                    licenseId = new Guid(sdr["CurrentLicenseId"].ToString());
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return licenseId;
        }

        public bool EnforceLicensing()
        {
            LendersOffice.Security.BrokerUserPermissions permissions = null;

            if (this.EmployeeId == Guid.Empty) 
            {
                using (PerformanceMonitorItem.Time("BrokerUser.EnforceLicensing"))
                {
                    Guid employeeId = RetrieveEmployeeId(this.BrokerId, this.UserId);

                    /// Check to see if the user is a PML user/manager or not. If he is, licensing enforcement isn't required for this user
                    permissions = new LendersOffice.Security.BrokerUserPermissions(this.BrokerId, employeeId);
                }
            } 
            else 
            {
                permissions = new LendersOffice.Security.BrokerUserPermissions(this.BrokerId, this.EmployeeId, this.Permissions, this.UserType);
            }

            if (permissions.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms))
            {
                return false;
            }

			return true ;
		}

        public static Guid RetrieveEmployeeId(Guid brokerId, Guid userId)
        {
            string sql = "SELECT EmployeeId FROM Employee WHERE EmployeeUserId=@UserId";
            SqlParameter[] parameters = { new SqlParameter("@UserId", userId) };

            Guid employeeId = Guid.Empty;
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (sdr.Read())
                {
                    employeeId = new Guid(sdr["EmployeeId"].ToString());
                }
                else
                {
                    throw new CBaseException(ErrorMessages.FailedToFindLicensePlanInfo, ErrorMessages.FailedToFindLicensePlanInfo);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return employeeId;
        }

        public bool HasValidLicense()
		{
			return License != null && License.IsActive ;
		}
        /// <summary>
        /// Checks to see if we should display the license renewal page to the user. Returns TRUE if yes.
        /// </summary>
        /// <returns></returns>
        public bool DisplayRenewalReminder()
        {
            if (this.License == null)
                return false;

            TimeSpan ts = License.ExpirationDate - DateTime.Now;
            if (Math.Abs(ts.Days) > License.RENEWAL_DISPLAY_DAYS) // do not display if there's still more than X days before license expires
                return false;

            // check to see when was the last time the license renewal page was displayed
            bool isNull = CheckLastRenewalPage(this.BrokerId, this.UserId);

            return isNull;
        }

        public static bool CheckLastRenewalPage(Guid brokerId, Guid userId)
        {
            string sql = "SELECT LicenseRenewalViewD FROM Broker_User WHERE UserId=@UserId";
            SqlParameter[] parameters = { new SqlParameter("@UserId", userId) };

            bool isNull = false;
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read())
                {
                    throw new CBaseException(ErrorMessages.FailedToRetrieveBroker, "Broker User record not found");
                }

                if (DBNull.Value.Equals(sdr["LicenseRenewalViewD"])) // renewal never viewed
                {
                    isNull = true;
                }
                else
                {
                    var ts = DateTime.Now - Convert.ToDateTime(sdr["LicenseRenewalViewD"]);
                    if (Math.Abs(ts.TotalHours) >= 24) // display if last view time is >= than 24 hours ago; basically once per day
                    {
                        isNull = true;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return isNull;
        }

        /// <summary>
        /// Update the renew reminder date with the current date
        /// </summary>
        /// <returns></returns>
        public void UpdateRenewReminderDate()
		{
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", this.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "UpdateLicenseRenewalReminderDate", 2, parameters);
		}

	}

	public class Licensee
	{
		public Licensee(Guid brokerId)
		{
			m_brokerId = brokerId ;
		}
		public string CustomerCode
		{
			get
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", m_brokerId)
                                            };
				using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(m_brokerId, "GetCustomerCodeByBrokerId", parameters))
				{
					if (!sdr.Read())
						throw new CBaseException(ErrorMessages.FailedToRetrieveBroker, "Broker record not found") ;

					return SafeConvert.ToString(sdr["CustomerCode"]) ;
				}
			}
		}
		public DbDataReader OpenActiveLicenses
		{
			get
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", m_brokerId)
                                            };
				return StoredProcedureHelper.ExecuteReader(m_brokerId, "ListOpenActiveLicense", parameters) ;
			}
		}
		public DbDataReader AllActiveLicenses
		{
			get
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", m_brokerId)
                                            };
				return StoredProcedureHelper.ExecuteReader(m_brokerId, "ListActiveLicense", parameters) ;
			}
		}
		public DbDataReader AllLicenses
		{
			get
			{
                SqlParameter[] parameters = {
                                new SqlParameter("@BrokerId", m_brokerId)
                            };
				return StoredProcedureHelper.ExecuteReader(m_brokerId, "ListAllLicense", parameters) ;
			}
		}
		public LicenseEstimate GetLicenseEstimate(int seatCount, bool isRenew, License licenseToRenew)
		{
			DateTime expirationDate ;
			int costPerSeat ;

			// 05/18/06-bd-Eliminate pro-rated licensing from the system
			costPerSeat =  License.LICENSE_PLAN_COST_PER_YEAR;

			if (isRenew)
			{
				if (licenseToRenew == null) throw new CBaseException(ErrorMessages.Generic, "License to renew is null") ;
				expirationDate = licenseToRenew.ExpirationDate.AddMonths(12) ;
			}
			else
				expirationDate = DateTime.Now.AddMonths(12) ;

			return new LicenseEstimate(seatCount, costPerSeat, expirationDate) ;
		}
		public Guid RecordPayment(string description, double amt, string paidBy, string authCode)
		{
			Guid paymentId = Guid.NewGuid() ;
            if (null == authCode)
                authCode = "";

            SqlParameter[] parameters = {
                                            new SqlParameter("@PaymentId", paymentId),
                                            new SqlParameter("@LicenseId", Guid.Empty),
                                            new SqlParameter("@Amount", amt),
                                            new SqlParameter("@Description", description),
                                            new SqlParameter("@PaidBy", paidBy),
                                            new SqlParameter("@AuthCode", authCode)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "InsertPayment", 2, parameters);

			return paymentId ;
		}

        public Guid AddLicense(int seatCount, string description, Guid paymentId, Guid previousLicenseId, DateTime expirationDate)
        {
            /// insert data
            Guid licenseId = CreateLicenseRecord(this.m_brokerId, seatCount, description, previousLicenseId, expirationDate);

            if (previousLicenseId != Guid.Empty)
			{
                // Update all BrokerUser records on the old license to use the new one
                UpdateLicense(this.m_brokerId, previousLicenseId, licenseId);

				// Update the previous License Open Seat Count
				License.UpdateOpenSeatCount(m_brokerId, previousLicenseId) ;
			}

			License.UpdateOpenSeatCount(m_brokerId, licenseId) ;

			// update payment record if necessary
			if (paymentId != Guid.Empty)
			{
                UpdatePaymentRecord(this.m_brokerId, paymentId, licenseId);
			}

			return licenseId ;
		}

        public static void UpdatePaymentRecord(Guid brokerId, Guid paymentId, Guid licenseId)
        {
            var sql = "UPDATE PAYMENT SET LicenseId=@p0 WHERE PaymentId=@p1";
            var listParams = new SqlParameter[] { new SqlParameter("@p0", licenseId), new SqlParameter("@p1", paymentId) };

            DBUpdateUtility.Update(brokerId, sql, null, listParams);
        }

        public static void UpdateLicense(Guid brokerId, Guid oldLicenseId, Guid newLicenseId)
        {
            string sql = "UPDATE BROKER_USER SET CurrentLicenseId = @newValue WHERE CurrentLicenseId = @oldValue";
            var parameters = new SqlParameter[] { new SqlParameter("@newValue", newLicenseId), new SqlParameter("@oldValue", oldLicenseId) };

            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        public static Guid CreateLicenseRecord(Guid brokerId, int seatCount, string description, Guid previousLicenseId, DateTime expirationDate)
        {
            Guid licenseId = Guid.NewGuid();

            DataAccess.SQLInsertStringBuilder sqlb = new DataAccess.SQLInsertStringBuilder();
            var paramList = new List<SqlParameter>();

            sqlb.AddNonString("LicenseId", "@p0");
            paramList.Add(new SqlParameter("@p0", licenseId));

            sqlb.AddNonString("LicenseOwnerBrokerId", "@p1");
            paramList.Add(new SqlParameter("@p1", brokerId));

            sqlb.AddNonString("PurchaseDate", "GETDATE()");

            sqlb.AddNonString("ExpirationDate", "@p2");
            paramList.Add(new SqlParameter("@p2", expirationDate));

            sqlb.AddNonString("SeatCount", "@p3");
            paramList.Add(new SqlParameter("@p3", seatCount));

            sqlb.AddNonString("OpenSeatCount", "@p4");
            paramList.Add(new SqlParameter("@p4", seatCount));

            sqlb.AddNonString("Description", "@p5");
            paramList.Add(DbAccessUtils.SqlParameterForVarchar("@p5", description));

            sqlb.AddNonString("RenewLicenseId", "@p6");
            if (previousLicenseId != Guid.Empty)
            {
                paramList.Add(new SqlParameter("@p6", previousLicenseId));
            }
            else
            {
                paramList.Add(new SqlParameter("@p6", Guid.Empty));
            }

            string sql = string.Format("INSERT INTO LICENSE {0}", sqlb.ToString());

            DBInsertUtility.InsertNoKey(brokerId, sql, null, paramList);
            return licenseId;
        }

        public void UpdateLicense(Guid licenseId, int seatCount, string description, DateTime expirationDate)
		{
            SqlParameter[] parameters = {
                                            new SqlParameter("@LicenseId", licenseId),
                                            new SqlParameter("@ExpirationDate", expirationDate),
                                            new SqlParameter("@SeatCount", seatCount),
                                            new SqlParameter("@Description", description)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "UpdateLicense", 2, parameters);

			License.UpdateOpenSeatCount(m_brokerId, licenseId) ;
		}

		public bool UseNewLicensingPlan()
		{
            //9/13/07 - We now always want to use the new licensing plan
			return true ;
		}

		private Guid m_brokerId ;
	}

	public class CCard
	{
		public CommonLib.CreditCardType CardType
		{
			get { return m_cardType ; }
			set { m_cardType = value ; }
		}
		public string CardNumber
		{
			get { return m_cardNumber ; }
			set { m_cardNumber = value ; }
		}
		public string BillingName
		{
			get { return m_billingName ; }
			set { m_billingName = value ; }
		}
		public string BillingStreet
		{
			get { return m_billingStreet ; }
			set { m_billingStreet = value ; }
		}
		public string BillingCity
		{
			get { return m_billingCity ; }
			set { m_billingCity = value ; }
		}
		public string BillingState
		{
			get { return m_billingState ; }
			set { m_billingState = value ; }
		}
		public string BillingZip
		{
			get { return m_billingZip ; }
			set { m_billingZip = value ; }
		}
		public int ExpMonth
		{
			get { return m_expMonth ; }
			set { m_expMonth = value ; }
		}
		public int ExpYear
		{
			get { return m_expYear ; }
			set { m_expYear = value ; }
		}
		public string CVV
		{
			get { return m_cvv ; }
			set { m_cvv = value ; }
		}
		CommonLib.CreditCardType m_cardType ;
		string m_cardNumber ;
		string m_billingName ;
		string m_billingStreet ;
		string m_billingCity ;
		string m_billingState ;
		string m_billingZip ;
		int m_expMonth ;
		int m_expYear ;
		string m_cvv ;
	}

	public class CCProcessor
	{
		private const bool IS_TEST = false ; // *!*!*!*!*! change this to false when testing is done

		private const string TEST_STOREID = "1909000082" ; // this test store Id is obsolete 30 days from 03/31/06
		private const string DESCRIPTION = "LENDERSOFFICE.COM SEAT LICENSE" ;
		string m_errorMessage = "" ;
		string m_authCode = "" ;

		public bool Process(CCard card, double amount, string description)
		{
			Tools.LogInfo(string.Format("{0}: Card payment processing begin", card.BillingName)) ;

			string storeId = IS_TEST ? TEST_STOREID : LendersOffice.Constants.ConstApp.LinkpointStoreId ;
			string orderid = GetOrderId(card) ;

			MeridianLink.CCGateway.CreditCardInfo cardInfo;

            try
            {
                cardInfo = new MeridianLink.CCGateway.CreditCardInfo(card.BillingName, card.BillingStreet, card.BillingCity, card.BillingState, card.BillingZip, card.CardNumber, null, card.CVV, card.ExpMonth, card.ExpYear);
            }
            catch (ApplicationException e)
            {
                m_errorMessage = e.Message;
                return false;
            }

			MeridianLink.CCGateway.LinkPointAPIGateway3.TransactionInfo tranInfo = new MeridianLink.CCGateway.LinkPointAPIGateway3.TransactionInfo(storeId, IS_TEST, orderid, null, amount) ;
			MeridianLink.CCGateway.LinkPointAPIGateway3.DescriptiveInfo descInfo = new MeridianLink.CCGateway.LinkPointAPIGateway3.DescriptiveInfo(DESCRIPTION, null) ;
			
			// Auth
			MeridianLink.CCGateway.LinkPointAPIGateway3.ParsedResponse response = MeridianLink.CCGateway.LinkPointAPIGateway3.Authorize(cardInfo, tranInfo, descInfo) ;

			// Settle if success
			if (response.Success)
			{
                Tools.LogInfo(string.Format("{0}: Card payment Authorized", card.BillingName));

				response = MeridianLink.CCGateway.LinkPointAPIGateway3.Settle(cardInfo, tranInfo, descInfo) ;
			}

			// Result
			if (response.Success)
			{
				m_authCode = response.ApprovalCode ;

                Tools.LogInfo(string.Format("{0}: Card payment Settled", card.BillingName));
				return true ;
			}
			else
			{
				m_errorMessage = response.Error ;

                Tools.LogInfo(string.Format("{0}: Card payment FAILED. Reason {1}\n{2}", card.BillingName, m_errorMessage, response.ResponseXml.OuterXml));
				return false ;
			}
		}
		public string ErrorMessage
		{
			get { return m_errorMessage ; }
		}
		public string AuthCode
		{
			get { return m_authCode ; }
		}

		private string GetOrderId(CCard card)
		{
			// hopefully the algorithm for this orderID is sufficiently unique
			DateTime now = DateTime.Now ;
			return string.Format("LOSEAT_{0}{1:00}{2:00}_{3:00}{4:00}{5:00}_{6}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, SafeString.Right(card.CardNumber, 4)) ;
		}		
	}
}
