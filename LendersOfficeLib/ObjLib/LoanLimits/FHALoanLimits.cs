﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.IO;
using LendersOffice.Common;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;

namespace LendersOfficeApp.ObjLib.LoanLimitsData
{
    /// <summary>
    /// Parses loan limit data and filters out those which dont belong. The propper format file is expected 
    /// and will throw errors if not given. It also filters out the entries from the file that do not match 203b. 
    /// </summary>
    public sealed class FHALoanLimitDatabaseUpdater : LoanLimitDatabaseUpdater
    {
        public FHALoanLimitDatabaseUpdater(DateTime effectiveDate)
            : base("UpdateFHAForFIPSCode")
        {
            m_effectiveDate = effectiveDate;
        }
        protected override bool Ignore(LoanLimitEntry e)
        {
            return e.SoaCode != "203B" || base.Ignore(e);
        }
    }
    public enum E_LoanLimitType
    {
        FHA,
        GSE
    }
    /// <summary>
    /// Represents the loan limits stored in the database. The class consist of static methods to update the database 
    /// and to retrieve the loan limits for either a FHA or GSE loan given the fips code. 
    /// </summary>
    public sealed class LoanLimit
    {
        private decimal m_limit1Units;
        private decimal m_limit2Units;
        private decimal m_limit3Units;
        private decimal m_limit4Units;
        private int m_fipsCode;
        private DateTime m_lastRevised;

        /// <summary>
        /// Gets the limits for 1 unit. 
        /// </summary>
        public decimal Limit1Units
        {
            get
            {
                return m_limit1Units;
            }
        }
        /// <summary>
        /// Gets the limit for 2 units. 
        /// </summary>
        public decimal Limit2Units
        {
            get
            {
                return m_limit2Units;
            }
        }
        /// <summary>
        /// Gets the limit for 3 units.
        /// </summary>
        public decimal Limit3Units
        {
            get
            {
                return m_limit3Units;
            }
        }
        /// <summary>
        /// Gets the limit for 4 units. 
        /// </summary>
        public decimal Limit4Units
        {
            get
            {
                return m_limit4Units;
            }
        }
        /// <summary>
        /// Gets the fips code for this instance.
        /// </summary>
        public int FipsCode
        {
            get
            {
                return m_fipsCode;
            }
        }
        /// <summary>
        /// Gets the last revised date for this instance.
        /// </summary>
        public DateTime LastRevised
        {
            get
            {
                return m_lastRevised;
            }
        }
        /// <summary>
        /// The constructor is private because an instance should only be created by GetLoanLimitFor
        /// </summary>
        private LoanLimit()
        {

        }

        /// <summary>
        /// Retrieves the loan limits for the given type by fips code. 
        /// </summary>
        /// <param name="type">The type of loan limits interested in</param>
        /// <param name="fips">The fips code of the loan limits</param>
        /// <returns>Loan Limit object containing the 4 limits, fips code, and last revised date. This will return NULL if the table 
        /// does not contain a zero fips code.  If for some reason the fips code is not in the table the standard entry should be returned (0). 
        /// This can be checked by comparing the input fips code and the result fips code.</returns>
        /// <exception cref="ArgumentException"> This is thrown when the type is not in the statement. </exception>
        public static LoanLimit GetLoanLimitFor(E_LoanLimitType type, int fips, DateTime effectiveDate)
        {

            lock (s_lock)
            {
                if (s_fhaLimitsDictionary == null || s_gseLimitsDictionary == null)
                {
                    LoadFhaAndGseToMemory();
                }
            }

            if (type == E_LoanLimitType.GSE)
            {
                // Sanitize the date here.  It is data entry mistake to be this old.  Should not be possible.
                DateTime oldDate = new DateTime(1901, 1, 1);
                if (effectiveDate < oldDate) effectiveDate = oldDate;

                SortedDictionary<DateTime, LoanLimit> limits = null;
                if (s_gseLimitsDictionary.TryGetValue(fips, out limits))
                {
                    // Try to get the most recent limits that went into effect before or on this date.
                    // If DB is setup correctly, should always have one.
                    var loanLimit = limits.Reverse().FirstOrDefault(p => p.Key <= effectiveDate);

                    if (loanLimit.Equals(default(KeyValuePair<DateTime, LoanLimit>)))
                    {
                        Tools.LogWarning($"Unable to obtain GSE loan limits for FIPS code {fips} using effective date {effectiveDate}, falling back to default.");
                    }
                    else
                    {
                        return loanLimit.Value;
                    }
                }
                else
                {
                    Tools.LogWarning($"No GSE loan limits found for FIPS code {fips}, falling back to default.");
                }

                // Retrieve default value
                return s_gseLimitsDictionary[0].First().Value;

            }
            else if (type == E_LoanLimitType.FHA)
            {
                // Sanitize the date here.  It is data entry mistake to be this old.  Should not be possible.
                DateTime oldDate = new DateTime(1901, 1, 1);
                if (effectiveDate < oldDate) effectiveDate = oldDate;

                SortedDictionary<DateTime, LoanLimit> limits = null;
                if (s_fhaLimitsDictionary.TryGetValue(fips, out limits))
                {
                    // Try to get the most recent limits that went into effect before or on this date.
                    // If DB is setup correctly, should always have one.
                    var loanLimit = limits.Reverse().FirstOrDefault(p => p.Key <= effectiveDate);

                    if (loanLimit.Equals(default(KeyValuePair<DateTime, LoanLimit>)))
                    {
                        Tools.LogWarning($"Unable to obtain FHA loan limits for FIPS code {fips} using effective date {effectiveDate}, falling back to default.");
                    }
                    else
                    {
                        return loanLimit.Value;
                    }
                }
                else
                {
                    Tools.LogWarning($"No FHA loan limits found for FIPS code {fips}, falling back to default.");
                }

                // Retrieve default value
                return s_fhaLimitsDictionary[0].First().Value;

            }
            else
            {
                throw new UnhandledEnumException(type);
            }

        }

        private static Dictionary<int, SortedDictionary<DateTime,LoanLimit>> s_fhaLimitsDictionary = null;
        private static Dictionary<int, SortedDictionary<DateTime,LoanLimit>> s_gseLimitsDictionary = null;
        private static object s_lock = new object();
        private static void LoadFhaAndGseToMemory()
        {
            s_fhaLimitsDictionary = new Dictionary<int, SortedDictionary<DateTime, LoanLimit>>();
            HandleFhaLoanLimits(s_fhaLimitsDictionary);

            s_gseLimitsDictionary = new Dictionary<int, SortedDictionary<DateTime, LoanLimit>>();
            HandleConformingLoanLimits(s_gseLimitsDictionary);
        }

        public static void HandleConformingLoanLimits(Dictionary<int, SortedDictionary<DateTime, LoanLimit>> data)
        {
            string sql = @"SELECT Limit1Units, Limit2Units, Limit3Units, Limit4Units, FipsCountyCode, LastRevised, EffectiveDate FROM CONFORMING_LOAN_LIMITS";
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    int fipsCode = System.Convert.ToInt32(reader["FipsCountyCode"]);
                    DateTime effectiveDate = (DateTime)reader["EffectiveDate"];

                    LoanLimit loanLimit = new LoanLimit()
                    {
                        m_limit1Units = System.Convert.ToDecimal(reader["Limit1Units"]),
                        m_limit2Units = System.Convert.ToDecimal(reader["Limit2Units"]),
                        m_limit3Units = System.Convert.ToDecimal(reader["Limit3Units"]),
                        m_limit4Units = System.Convert.ToDecimal(reader["Limit4Units"]),
                        m_fipsCode = fipsCode,
                        m_lastRevised = (DateTime)reader["LastRevised"]
                    };

                    if (data.ContainsKey(fipsCode))
                    {
                        data[fipsCode].Add(effectiveDate, loanLimit);
                    }
                    else
                    {
                        var codeList = new SortedDictionary<DateTime, LoanLimit>();
                        codeList.Add(effectiveDate, loanLimit);
                        data.Add(fipsCode, codeList);
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, sql, null, null, readHandler);
        }

        public static void HandleFhaLoanLimits(Dictionary<int, SortedDictionary<DateTime, LoanLimit>> data)
        {
            string sql = @"SELECT Limit1Units, Limit2Units, Limit3Units, Limit4Units, FipsCountyCode, LastRevised, EffectiveDate FROM FHA_LOAN_LIMITS";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    int fipsCode = System.Convert.ToInt32(reader["FipsCountyCode"]);
                    DateTime effectiveDate = (DateTime)reader["EffectiveDate"];

                    LoanLimit loanLimit = new LoanLimit()
                    {
                        m_limit1Units = System.Convert.ToDecimal(reader["Limit1Units"]),
                        m_limit2Units = System.Convert.ToDecimal(reader["Limit2Units"]),
                        m_limit3Units = System.Convert.ToDecimal(reader["Limit3Units"]),
                        m_limit4Units = System.Convert.ToDecimal(reader["Limit4Units"]),
                        m_fipsCode = fipsCode,
                        m_lastRevised = (DateTime)reader["LastRevised"]
                    };


                    if (data.ContainsKey(fipsCode))
                    {
                        data[fipsCode].Add(effectiveDate, loanLimit);
                    }
                    else
                    {
                        var codeList = new SortedDictionary<DateTime, LoanLimit>();
                        codeList.Add(effectiveDate, loanLimit);
                        data.Add(fipsCode, codeList);
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, sql, null, null, readHandler);
        }

        /// <summary>
        /// Updates the database loan limits table with the data from the given url. 
        /// </summary>
        /// <param name="e">The type of data the url contains ( FHA or GSE at time of writing ) </param>
        /// <param name="url">The url to the data.</param>
        /// <exception cref="ArgumentException"> Throws one in case of bad url or if data cannot be parsed. It will contain the message and  a reason. </exception>
        public static void UpdateDB(E_LoanLimitType e, string url, DateTime effectiveDate)
        {
            LoanLimitDatabaseUpdater dbupdate = null;
            List<LoanLimitEntry> entries = null;

            string data;
            try
            {
                data = Tools.GetURL(url);
                FixedLengthLoanLimitParser parser = new FixedLengthLoanLimitParser(data);
                entries = parser.GetLoanLimitEntries().ToList();
            }
            catch (Exception ex)
            {
                Tools.LogError(ex);
                throw new ArgumentException("Could not get or parse the contents from the url : " + url, ex);
            }


            Tools.LogInfo("Starting to run Update for : " + e.ToString());
            switch (e)
            {
                case E_LoanLimitType.FHA:
                    dbupdate = new FHALoanLimitDatabaseUpdater(effectiveDate);
                    break;
                case E_LoanLimitType.GSE:

                    dbupdate = new GSELoanLimitDatabaseUpdater(effectiveDate);
                    break;
                default:
                    throw new ArgumentException("Loan Limit Type Not supported " + e.ToString());
            }

            try
            {
                dbupdate.Run(entries);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Invalid file specified by url maybe?", ex);
            }
            Tools.LogInfo("Ending run for : " + e.ToString());
        }
    }
}