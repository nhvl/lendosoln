﻿using System;
using System.Collections.Generic;
using System.Collections; 
using System.Collections.ObjectModel; 
using System.Linq;
using System.Web;
using System.IO;
using LendersOffice.Common;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;



namespace LendersOfficeApp.ObjLib.LoanLimitsData
{
    /// <summary>
    /// Parses loan limit data and filters out those which dont belong. The propper format file is expected 
    /// and will throw errors if not given. It also filters out the entries from the file that do not match GSE. 
    /// </summary>
    public sealed class GSELoanLimitDatabaseUpdater : LoanLimitDatabaseUpdater
    {
        public GSELoanLimitDatabaseUpdater(DateTime effectiveDate)
            : base("UpdateGSEForFIPSCode")  
        {
            m_effectiveDate = effectiveDate;
        }
        /// <summary>
        /// Tells the parser to ignore entries that are not gse  and whatever else the parent decides to ignore. 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override bool Ignore(LoanLimitEntry e)
        {
            return e.SoaCode != "GSE" || base.Ignore(e) ;
        }
    }
}
