﻿using System;
using System.Collections.Generic;
using System.IO;
using LendersOffice.Common;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Data;

namespace LendersOfficeApp.ObjLib.LoanLimitsData
{
    public abstract class LoanLimitParser
    {
        public abstract IEnumerable<LoanLimitEntry> GetLoanLimitEntries(); 
    }

    /// <summary>
    /// Parses a string containing limit data.
    /// This class can be inherited from and the ranges can be overwritten. 
    /// </summary>
    public class FixedLengthLoanLimitParser : LoanLimitParser
    {
        #region ranges and variables
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_Limit1 = new LoanLimitFieldRange() { Start = 73, End = 79 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_Limit2 = new LoanLimitFieldRange() { Start = 80, End = 86 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_Limit3 = new LoanLimitFieldRange() { Start = 87, End = 93 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_Limit4 = new LoanLimitFieldRange() { Start = 94, End = 100 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_StateCode = new LoanLimitFieldRange() { Start = 101, End = 102 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_CountyCode = new LoanLimitFieldRange() { Start = 103, End = 105 };
        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_LastRevised = new LoanLimitFieldRange() { Start = 155, End = 162 };

        /// <summary>
        /// Remember to subtract 1 from start and end if you are using the limits file 
        /// </summary>
        protected LoanLimitFieldRange m_SoaCode = new LoanLimitFieldRange() { Start = 60, End = 64 };
        
        protected string data;

        #endregion

        public FixedLengthLoanLimitParser(string data)
        {
            this.data = data;
        }

        private IEnumerable<string> ReadLines(string data)
        {
            using (StringReader reader = new StringReader(data))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }

        /// <summary>
        /// Parses the given loan limit data.
        /// </summary>
        /// <param name="loanLimitData">Properly formatted string as described by limitsd.txt</param>
        /// <returns>Iterates through each line returning a limit for each one</returns>
        public override IEnumerable<LoanLimitEntry> GetLoanLimitEntries()
        {
            foreach (string line in this.ReadLines(this.data))
            {
                yield return new LoanLimitEntry()
                {
                    State = GetField(line, m_StateCode).TrimWhitespaceAndBOM(),
                    CountyCode = GetField(line, m_CountyCode),
                    Limit1Units = GetField(line, m_Limit1),
                    Limit2Units = GetField(line, m_Limit2),
                    Limit3Units = GetField(line, m_Limit3),
                    Limit4Units = GetField(line, m_Limit4),
                    LastRevised = GetField(line, m_LastRevised),
                    SoaCode = GetField(line, m_SoaCode).TrimWhitespaceAndBOM()
                };
            }
        }

        /// <summary>
        /// Takes the current line from the file and retrieves the characters specified in the range. 
        /// </summary>
        /// <param name="currentLine">The line being parsed.</param>
        /// <param name="currentRange">The range of the desired field.</param>
        /// <exception cref="ArgumentOutOfRangeException "> Generally wont happen unless the wrong file format is given. </exception>
        /// <returns>The characters at the place described by the range.</returns>
        private string GetField(string currentLine, LoanLimitFieldRange currentRange)
        {
            return currentLine.Substring(currentRange.Start, currentRange.Length);
        }

        public class LoanLimitFieldRange
        {
            public int Start { get; set; }
            public int End { get; set; }
            public int Length
            {
                get { return End - Start + 1; } //Its inclusive hence + 1
            }
        }
    }

    public abstract class LoanLimitDatabaseUpdater
    {
        private string m_storeProcedureName;

        protected DateTime? m_effectiveDate = null;

        /// <summary>
        /// Instantiates a new LoanLimitDatabaseUpdater. 
        /// </summary>
        /// <param name="storeProcName">The store procedure's name that updates the database.</param>
        /// <param name="loanLimits">The list of loan limits.</param>
        protected LoanLimitDatabaseUpdater(string storeProcName)
        {
            this.m_storeProcedureName = storeProcName;
        }

        /// <summary>
        /// Returns whether the give loan limit entry should be ignored. This base method should be called from 
        /// children because it filters by state. The state AS MP or GU should be ignored.  
        /// </summary>
        /// <param name="limit">object representing a line from the file</param>
        /// <returns>If the entry should not be put into the database.</returns>
        protected virtual bool Ignore(LoanLimitEntry limit)
        {
            try
            {
                int fipsCode = limit.FipsCode;
                if (fipsCode == 0)
                {
                    Tools.LogInfo("LoanLimitDBUpdater:Found Standard entry. ");
                }
            }
            catch (ArgumentException e)
            {
                Tools.LogError("There were problems getting a fips code from the given Loan Limit.", e);
                return true;
            }

            return Regex.IsMatch(limit.State, "AS|MP|GU");
        }
        /// <summary>
        /// Takes the given loan limit entry and saves it to the database. It will update it if it already exist. It logs an exception if a pb error occurs. 
        /// </summary>
        /// <param name="limit">object representing a line from the file.</param>
        private void SaveLoanLimit(LoanLimitEntry limit)
        {
            List<SqlParameter> parameters = new List<SqlParameter> (new[] { 
											new SqlParameter("@FipsCode",    limit.FipsCode ), 
											new SqlParameter("@Limit1Units", limit.Limit1Units ), 
											new SqlParameter("@Limit2Units", limit.Limit2Units),
											new SqlParameter("@Limit3Units", limit.Limit3Units),
											new SqlParameter("@Limit4Units", limit.Limit4Units ),
											new SqlParameter("@LastRevised", limit.LastRevised) } );
            if (m_effectiveDate.HasValue)
            {
                parameters.Add(new SqlParameter("@EffectiveDate", m_effectiveDate.Value));
            }

            try
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, m_storeProcedureName, 0, parameters);
            }
            catch (Exception e)
            {
                Tools.LogError(String.Format("There was a an error adding FipsCode: {0}", limit.FipsCode), e);
            }
        }

        public void Run(IEnumerable<LoanLimitEntry> loanLimits)
        {
            foreach (LoanLimitEntry limit in loanLimits)
            {
                if (Ignore(limit))
                {
                    Tools.LogInfo("ignoring.");
                    continue;
                }

                SaveLoanLimit(limit);
            }
        }

    }

    /// <summary>
    /// Encapsulates the interesting data from a loan limits file.  
    /// </summary>
    public class LoanLimitEntry
    {
        private int? _fipsCode = null;
        public string Limit1Units { get; set; }
        public string Limit2Units { get; set; }
        public string Limit3Units { get; set; }
        public string Limit4Units { get; set; }
        public string LastRevised { get; set; }
        public string CountyCode { get; set; }
        public string State { get; set; }
        public string SoaCode { get; set; }
        public int FipsCode
        {
            get
            {
                if (_fipsCode != null)
                {
                    return _fipsCode.Value;
                }

                if (String.IsNullOrEmpty(State))  //the field has empty data for standard. Fips code should be zero 
                {
                    return (_fipsCode = 0).Value;
                }

                int state = StateInfo.Instance.StateCode(State);

                if (state == -1)
                {
                    throw new ArgumentException("State was not found in the StateInfo.", "StateCode");
                }

                int county;

                if (!Int32.TryParse(CountyCode, out county))
                {
                    throw new ArgumentException("County was not a valid number for fips code generation.", "CountyCode");
                }
                int fips;
                if (!Int32.TryParse(state.ToString() + CountyCode, out fips))
                {
                    throw new ArgumentException("Could not convert state and county to fips.");
                }

                return (_fipsCode = fips).Value;
            }
        }
    }

}