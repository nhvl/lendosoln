﻿namespace LendersOffice.Common
{
    /// <summary>
    /// Sensitive report type enum.
    /// </summary>
    public enum SensitiveReportType
    {
        /// <summary>
        /// Custom report.
        /// </summary>
        CustomReport = 0,

        /// <summary>
        /// Batch export.
        /// </summary>
        BatchExport = 1
    }
}
