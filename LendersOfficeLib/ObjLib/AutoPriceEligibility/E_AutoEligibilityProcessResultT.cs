﻿namespace DataAccess
{
    /// <summary>
    /// Describes the type of automatic process result.
    /// </summary>
    public enum E_AutoEligibilityProcessResultT
    {
        /// <summary>
        /// Default value: No result yet.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Eligible result.
        /// </summary>
        Eligible = 1,

        /// <summary>
        /// Ineligible result.
        /// </summary>
        Ineligible = 2,

        /// <summary>
        /// Unable to Determine result.
        /// </summary>
        UnableToDetermine = 3,

        /// <summary>
        /// Report was manually cleared.
        /// </summary>
        ReportCleared = 4
    }
}