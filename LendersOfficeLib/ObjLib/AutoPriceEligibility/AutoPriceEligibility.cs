﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Threading;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// Collection of static methods to enable the overnight pricing 
    /// and eligibility feature.
    /// </summary>
    public class AutoPriceEligibility
    {
        /// <summary>
        /// Add all applicable loans at all applicable lenders to the overnight
        /// queue.
        /// </summary>
        public static void EnqueueAllApplicableLoans()
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                // 1. Get the list of lenders who particpate in this feature on this DB.
                var applicableLenders = new List<Guid>();
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokersUsingAutoPriceEligibilityProcess", null))
                {
                    while (reader.Read())
                    {
                        applicableLenders.Add((Guid)reader["BrokerId"]);
                    }
                }

                // 2. Get the list of loans that are currently applicable to the process.
                var applicableLoans = new List<Guid>();
                foreach (var brokerId in applicableLenders)
                {
                    applicableLoans.AddRange(GetParticipatingLoansAtBroker(brokerId));
                }

                // 3. Enqueue each loan.
                EnqueueLoans(applicableLoans);
            }
        }

        /// <summary>
        /// Add all loans at the lender.
        /// </summary>
        /// <param name="brokerId">The broker id of the lender.</param>
        public static void EnqueueApplicableLoansForBroker(Guid brokerId)
        {
            var applicableLoans = GetParticipatingLoansAtBroker(brokerId);
            EnqueueLoans(applicableLoans);
        }

        /// <summary>
        /// Add a single loan to the processing queue.
        /// </summary>
        /// <param name="loanId">Loan Id of the loan to process.</param>
        public static void EnqueueLoan(Guid loanId)
        {
            EnqueueLoans(new Guid[] { loanId });
        }

        /// <summary>
        /// Run the price and eligibility check on the single loan.
        /// </summary>
        /// <param name="loanId">Loan Id of the loan to process.</param>
        /// <param name="isSkippedLoan">True if the loan was skipped.</param>
        public static void RunPriceAndEligibilityCheck(Guid loanId, out bool isSkippedLoan)
        {
            // The time of the run is when it starts, rather than
            // when it finishes and we want time to be same for 
            // both price and eligibility per run.
            var runTime = DateTime.Now;

            // This is considered an automatic system operation,
            // so we want to disable the security checks.
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AutoPriceEligibility));
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            isSkippedLoan = LoanCanBeSkipped(dataLoan);

            AbstractUserPrincipal runningPrincipal = null;
            try
            {
                runningPrincipal = GetPrincipalForRunning(dataLoan);

                if (!isSkippedLoan)
                {
                    Thread.CurrentPrincipal = runningPrincipal;

                    var currentRate = dataLoan.sNoteIR;
                    var currentPoint = GetCurrentPoint(dataLoan);
                    dataLoan.sAutoPriceProcessResultHistoricalPrice = 0;
                    dataLoan.sAutoPriceProcessResultLockedPrice = 100 - currentPoint;
                    dataLoan.sAutoPriceProcessResultLockedRate = currentRate;

                    // We want the latest Front-end submission, similar to what the UI does.
                    var lastUsedSnapshot = dataLoan.SubmissionSnapshots.Where(
                        p => p.SubmissionType == E_sPricingModeT.InternalBrokerPricing
                        || p.SubmissionType == E_sPricingModeT.RegularPricing
                        || p.SubmissionType == E_sPricingModeT.Undefined)
                    .OrderBy(p => p.SubmissionDate).LastOrDefault();
                    if (lastUsedSnapshot != null)
                    {
                        var options = HybridLoanProgramSetOption.CreateHistoricalPricingOption(dataLoan.BrokerDB.HistoricalPricingSettings, lastUsedSnapshot);
                        UnderwritingResultItem resultItem = null;
                        Guid requestId = Guid.Empty;

                        for (int i = 0; i < 2; ++i)
                        {
                            requestId = DistributeUnderwritingEngine.RequestInternalPricingResult(
                            PrincipalFactory.CurrentPrincipal,
                            loanId,
                            E_sPricingModeT.InternalBrokerPricing,
                            options,
                            isAutoProcess: true);

                            try
                            {
                                resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);
                                break;
                            }
                            catch (DistributeTimeoutException exc)
                            {
                                // If there is an LPE timeout, allow it to retry.  We 
                                // have seen memory issues cause a pricing server to
                                // take the request for processing, and not compute a result.
                                Tools.LogError($"Pricing result timeout: {loanId.ToString()}, retry: {i}.", exc);
                            }

                            Tools.SleepAndWakeupRandomlyWithin(300, 1200);
                        }

                        // Principal switch here so pricing works, but audit events are from
                        // the special user.
                        Thread.CurrentPrincipal = SystemUserPrincipal.OvernightPricingUser(runningPrincipal.UserId);

                        if (resultItem == null || resultItem.IsErrorMessage)
                        {
                            // These types of distributed and pricing errors
                            // should result in unable to determine status.
                            string errorMsg = $"No result in overnight pricing process. requestId={requestId}."
                                + ((resultItem != null) ? resultItem.ErrorMessage : string.Empty);

                            SetAsUnableToDetermine(dataLoan, errorMsg);
                        }
                        else
                        {
                            // When looking at this logic, please recall that we are pricing in 
                            // only the single registed program.  It would be either eligible,
                            // ineligible, or in unable-to-calculate state.
                            var eligibles = resultItem.GetEligibleLoanProgramList();
                            if (eligibles.Any())
                            {
                                // Program itself is eligible.
                                var program = eligibles.First();
                                var thisOption = program.ApplicantRateOptions.FirstOrDefault(p => p.Rate == currentRate);

                                if (thisOption == null)
                                {
                                    // An option at this rate is not present.
                                    dataLoan.sAutoEligibilityProcessResultT = E_AutoEligibilityProcessResultT.Ineligible;
                                    dataLoan.sAutoPriceProcessResultT = E_AutoPriceProcessResultT.PriceChangeDetected;
                                }
                                else
                                {
                                    // We already know the program is qualified, so if the rate is 
                                    // qualified, we have eligibility.
                                    dataLoan.sAutoEligibilityProcessResultT = thisOption.IsDisqualified ?
                                        E_AutoEligibilityProcessResultT.Ineligible
                                        : E_AutoEligibilityProcessResultT.Eligible;

                                    var historicalPoint = GetRateOptionPoint(thisOption, dataLoan);

                                    dataLoan.sAutoPriceProcessResultT = historicalPoint == currentPoint ?
                                        E_AutoPriceProcessResultT.NoChangeDetected
                                        : E_AutoPriceProcessResultT.PriceChangeDetected;

                                    dataLoan.sAutoPriceProcessResultHistoricalPrice = 100 - historicalPoint;
                                }
                            }
                            else
                            {
                                // At this point at program level, we are either ineligible or 
                                // unable-to-calculate.
                                var ineligibles = resultItem.GetIneligibleLoanProgramList();
                                if (ineligibles.Any())
                                {
                                    // Program is ineligible, but we can still check pricing, 
                                    // and also do not care if the rate is denied or not.
                                    dataLoan.sAutoEligibilityProcessResultT = E_AutoEligibilityProcessResultT.Ineligible;

                                    var program = ineligibles.First();
                                    var thisOption = program.ApplicantRateOptions.FirstOrDefault(p => p.Rate == currentRate);

                                    if (thisOption == null)
                                    {
                                        // This could happen with a manual update of the Front-End
                                        // Rate Lock, but we have to consider it unable to determine.
                                        SetAsUnableToDetermine(dataLoan, "Locked rate is not offered.");
                                    }
                                    else
                                    {
                                        var historicalPoint = GetRateOptionPoint(thisOption, dataLoan);

                                        dataLoan.sAutoPriceProcessResultT = (historicalPoint == currentPoint) ?
                                            E_AutoPriceProcessResultT.NoChangeDetected
                                            : E_AutoPriceProcessResultT.PriceChangeDetected;

                                        dataLoan.sAutoPriceProcessResultHistoricalPrice = 100 - historicalPoint;
                                    }
                                }
                                else
                                {
                                    // The program was neither eligible nor ineligible.
                                    // We could end up here because: 
                                    // 1. Program not present in result set at all, possibly due to program removal or price group.
                                    // 2. Eligibility unable to be calculated. (in UI would show it in the "Needs more info" section)
                                    // Either way, we cannot determine the pricing or eligibility result.
                                    SetAsUnableToDetermine(dataLoan, "Program not in result.");
                                }
                            }
                        }
                    }
                    else
                    {
                        // ERROR: There is no last used snapshot to use.
                        SetAsUnableToDetermine(dataLoan, "There is no last used snapshot.");
                    }
                }
                else
                {
                    // Loan was skipped--make sure to save in correct overnight
                    // process system user principal.
                    Thread.CurrentPrincipal = SystemUserPrincipal.OvernightPricingUser(runningPrincipal.UserId);
                }
            }
            catch (CBaseException exc)
            {
                if (runningPrincipal == null)
                {
                    // We were not able to pull an LO/Secondary from this loan file.  
                    // This should be very rare.  An OCD-applicable loan is far enough in the process
                    // where removing an LO/Secondary would not be a normal action.
                    HandleMissingPrincipal(dataLoan);
                }

                // Any issue in pricing, we want to try to set value to loan file
                // and not let single loan errors kill the batch.
                SetAsUnableToDetermine(dataLoan, "Unable to determine in auto pricing.", exc);
            }

            // To simplify, we are assuming this is the only save going on, so 
            // we can know that the resulting version will be bumped by one.  
            // If for some reason a midnight user is working on the loan,
            // after we load, in most cases it will still be bumped to be
            // considered behind for the next run as they work on it.
            dataLoan.sAutoPriceEligibilityResultFileVersion = dataLoan.sFileVersion + 1;

            dataLoan.sAutoPriceProcessResultD = CDateTime.Create(runTime);
            dataLoan.sAutoEligibilityProcessResultD = CDateTime.Create(runTime);
            dataLoan.Save();
        }

        /// <summary>
        /// Handle missing running principal.
        /// </summary>
        /// <param name="dataLoan">The loan file to be checked.</param>
        private static void HandleMissingPrincipal(CPageData dataLoan)
        {
            // The system was unsuccessful in calculating a user principal from the loan file.
            // The system must still save the result, but we cannot trust the current principal
            // belongs to the lender of this loan.
            // The save audit events will show "Overnight Pricer" rather than the user chosen.
            var currentBrokerId = dataLoan.sBrokerId;
            var currentPrincipal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (currentPrincipal != null)
            {
                if (currentPrincipal.BrokerId == currentBrokerId)
                {
                    // This user is already a lender match--no need to look up another.
                    Thread.CurrentPrincipal = SystemUserPrincipal.OvernightPricingUser(currentPrincipal.UserId);
                    return;
                }
            }

            // We need to find an active user at this lender.
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", currentBrokerId),
                new SqlParameter("@IsActiveFilter", true)
             };

            // This is not the ideal way to do this, but considering that we've only seen this issue 
            // once in a year of running this feature, there may not be a significant value in adding
            // more specialized logic here.  Just grab the first active id.
            using (var reader = StoredProcedureHelper.ExecuteReader(currentBrokerId, "ListEmployeeByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    Thread.CurrentPrincipal = SystemUserPrincipal.OvernightPricingUser((Guid)reader["EmployeeUserId"]);
                    return;
                }
            }

            // This should not happen.  A lender with no active users is not a lender that
            // would have OCD loans.
            Tools.LogBug("No active users at lender: " + currentBrokerId);
        }

        /// <summary>
        /// Get the current point value considering LO Comp.
        /// </summary>
        /// <param name="option">The rate option to consider.</param>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>The appropriate point value.</returns>
        private static decimal GetRateOptionPoint(CApplicantRateOption option, CPageData dataLoan)
        {
            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                return option.PointIncludingOriginatorComp;
            }
            else
            {
                return option.Point_;
            }
        }

        /// <summary>
        /// Gets the current point value of the loan file considering LO Comp.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>The appropriate point value.</returns>
        private static decimal GetCurrentPoint(CPageData dataLoan)
        {
            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                return dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee;
            }
            else
            {
                return dataLoan.sBrokComp1Pc;
            }
        }

        /// <summary>
        /// Set the loan file as being unable to determine.
        /// </summary>
        /// <param name="dataLoan">The loanfile to set.</param>
        /// <param name="reason">The reason it is unable to determine.</param>
        /// <param name="exception">The exception if applicable.</param>
        private static void SetAsUnableToDetermine(CPageData dataLoan, string reason, Exception exception)
        {
            dataLoan.sAutoEligibilityProcessResultT = E_AutoEligibilityProcessResultT.UnableToDetermine;
            dataLoan.sAutoPriceProcessResultT = E_AutoPriceProcessResultT.UnableToDetermine;

            if (exception == null)
            {
                Tools.LogWarning(reason + " LoanId: " + dataLoan.sLId.ToString());
            }
            else
            {
                Tools.LogWarning(reason + " LoanId: " + dataLoan.sLId.ToString(), exception);
            }
        }

        /// <summary>
        /// Set the loan file as being unable to determine.
        /// </summary>
        /// <param name="dataLoan">The loanfile to set.</param>
        /// <param name="reason">The reason it is unable to determine.</param>
        private static void SetAsUnableToDetermine(CPageData dataLoan, string reason)
        {
            SetAsUnableToDetermine(dataLoan, reason, null);
        }

        /// <summary>
        /// Returns true if a loan can have its pricing run skipped.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>True if the loan can be skipped.</returns>
        private static bool LoanCanBeSkipped(CPageData dataLoan)
        {
            // We want to skip loans if they have not changed since last run.
            // Exception: We have to run UnableToDetermine loans every time,
            // since those ones may have been put into that status by non-loan
            // issues, such as a network hiccup or timeout.
            return
                dataLoan.sAutoPriceEligibilityResultFileVersion == dataLoan.sFileVersion
                    && dataLoan.sAutoEligibilityProcessResultT != E_AutoEligibilityProcessResultT.UnableToDetermine
                    && dataLoan.sAutoPriceProcessResultT != E_AutoPriceProcessResultT.UnableToDetermine;
        }

        /// <summary>
        /// Enqueue a list of loanids into the queue for processing.
        /// </summary>
        /// <param name="loanIds">LoanIds to enqueue.</param>
        private static void EnqueueLoans(IEnumerable<Guid> loanIds)
        {
            var autoPriceEligibilityQueue = new DBMessageQueue(ConstMsg.AutoPriceEligibilityQueue);

            foreach (var loanId in loanIds)
            {
                autoPriceEligibilityQueue.SendIfNew(loanId.ToString(), loanId.ToString(), string.Empty);
            }
            
            // For tracking usage.
            Tools.LogInfo($"[AutoPrice] Enqueued {loanIds.Count()} loans for processing.");
        }

        /// <summary>
        /// Get all the applicable loans at the broker (lender).
        /// </summary>
        /// <param name="brokerId">The brokerId of the lender.</param>
        /// <returns>All applicable loans at the lender.</returns>
        private static IEnumerable<Guid> GetParticipatingLoansAtBroker(Guid brokerId)
        {
            var applicableLoans = new List<Guid>();
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansUsingAutoPriceEligibilityProcessByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    applicableLoans.Add((Guid)reader["sLId"]);
                }
            }

            return applicableLoans;
        }

        /// <summary>
        /// Get principal for running the engine.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>Selected principal.  Throws CBaseException if none found.</returns>
        private static AbstractUserPrincipal GetPrincipalForRunning(CPageData dataLoan)
        {
            // Pricing requires a real principal.
            // So we do this similar to how the rate monitor does.
            Guid employeeId;
            if (dataLoan.BranchChannelT == E_BranchChannelT.Correspondent)
            {
                employeeId = dataLoan.sEmployeeSecondaryId != Guid.Empty ?
                    dataLoan.sEmployeeSecondaryId
                    : dataLoan.sEmployeeExternalSecondaryId;
            }
            else
            {
                employeeId = dataLoan.sEmployeeLoanRepId;
            }

            if (employeeId == Guid.Empty)
            {
                // Per PDE consider this invalid scenario.
                // LO or secondary has to be assigned for result to be valid.
                throw new GenericUserErrorMessageException("No valid LO or Secondary.");
            }

            Guid? userId;
            char? userType;
            AbstractUserPrincipal userPrincipal = null;
            if (Tools.TryGetUserInfoByEmployeeId(dataLoan.sBrokerId, employeeId, out userId, out userType))
            {
                if (userId.HasValue && userType.HasValue)
                {
                    userPrincipal = PrincipalFactory.RetrievePrincipalForUser(
                        dataLoan.sBrokerId,
                        userId.Value,
                        char.ToString(userType.Value));
                }
            }

            if (userPrincipal == null)
            {
                throw new GenericUserErrorMessageException("Could not get user Principal. EmployeeId:" + employeeId);
            }

            return userPrincipal;
        }
    }
}
