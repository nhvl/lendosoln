﻿namespace DataAccess
{
    /// <summary>
    /// Describes the type of automatic process result.
    /// </summary>
    public enum E_IncludeInAutoPriceEligibilityProcessT
    {
        /// <summary>
        /// Calculate the value.
        /// </summary>
        Calculate = 0,

        /// <summary>
        /// Force exclusion of the file.
        /// </summary>
        ForceExclude = 1
    }
}