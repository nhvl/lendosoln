﻿namespace DataAccess
{
    /// <summary>
    /// Describes the type of automatic process result.
    /// </summary>
    public enum E_AutoPriceProcessResultT
    {
        /// <summary>
        /// Default value: No result yet.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Price change was detected.
        /// </summary>
        PriceChangeDetected = 1,

        /// <summary>
        /// No change was detected.
        /// </summary>
        NoChangeDetected = 2,

        /// <summary>
        /// Unable to Determine result.
        /// </summary>
        UnableToDetermine = 3,

        /// <summary>
        /// Report was manually cleared.
        /// </summary>
        ReportCleared = 4
    }
}
