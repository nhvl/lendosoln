﻿namespace LendersOffice.HttpHandlers
{
    using System.Web;

    /// <summary>
    /// Put in place to support the options web request. It is in use for options.
    /// This allows us to add our own headers via our http modules.
    /// </summary>
    public class NoOpHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the is reusable flag.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Processes http request. This is a no op.
        /// </summary>
        /// <param name="context">The web request.</param>
        public void ProcessRequest(HttpContext context)
        {
        }
    }
}
