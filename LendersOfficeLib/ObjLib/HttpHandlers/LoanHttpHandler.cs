﻿// <copyright file="LoanHttpHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/12/2015
// </summary>

namespace LendersOffice.HttpHandlers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.UI;
    using Newtonsoft.Json;

    /// <summary>
    /// A HTTP Handler responsible for returning basic loan operations: Load / Save / Calculate.
    /// </summary>
    public class LoanHttpHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reusable. This is always true
        /// as the handler does not have any state per request.
        /// </summary>
        /// <value>Returns true since the handler has no unique state per request.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given incoming request.
        /// </summary>
        /// <param name="context">The HTTP context to process.</param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;

            if (request.HttpMethod != "POST")
            {
                throw new NotSupportedException("Only POST is allow");
            }

            string op = request.QueryString["op"];

            if (string.IsNullOrEmpty(op))
            {
                throw new NotSupportedException("op is required.");
            }

            Dictionary<string, InputFieldModel> result = null;
            Guid loanId = RequestHelper.GetGuid("loanid", Guid.Empty);
            Guid applicantId = RequestHelper.GetGuid("appid", Guid.Empty);

            if (op == "load")
            {
                List<string> fieldList = this.DeserializeRequest<List<string>>(request);

                result = PageDataUIController.Load(loanId, applicantId, fieldList);
            }
            else if (op == "save")
            {
                var inputModel = this.DeserializeRequest<Dictionary<string, InputFieldModel>>(request);

                result = PageDataUIController.Save(loanId, applicantId, inputModel);
            }
            else if (op == "calculate")
            {
                var inputModel = this.DeserializeRequest<Dictionary<string, InputFieldModel>>(request);

                result = PageDataUIController.Calculate(loanId, applicantId, inputModel);
            }
            else
            {
                throw new NotSupportedException("op=[" + op + "] is undefined.");
            }

            this.SerializeResponse(context.Response, result);
        }

        /// <summary>
        /// Deserialize the request.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="request">A <code>JSON</code> http request.</param>
        /// <returns>The input object from request.</returns>
        private T DeserializeRequest<T>(HttpRequest request)
        {
            using (StreamReader streamReader = new StreamReader(request.InputStream))
            {
                using (JsonReader reader = new JsonTextReader(streamReader))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.DefaultValueHandling = DefaultValueHandling.Ignore;

                    return serializer.Deserialize<T>(reader);
                }
            }
        }

        /// <summary>
        /// Serialize result to Http Response.
        /// </summary>
        /// <param name="response">The web response.</param>
        /// <param name="result">Result to serialize.</param>
        private void SerializeResponse(HttpResponse response, Dictionary<string, InputFieldModel> result)
        {
            using (JsonWriter writer = new JsonTextWriter(response.Output))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.DefaultValueHandling = DefaultValueHandling.Ignore;

                serializer.Serialize(writer, result);
            }
        }
    }
}
