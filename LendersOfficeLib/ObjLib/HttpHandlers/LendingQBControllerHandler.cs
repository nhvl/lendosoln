﻿// <copyright file="LendingQBControllerHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>
namespace LendersOffice.HttpHandlers
{
    using System;
    using System.Web;
    using LendersOffice.Controllers;
    using Newtonsoft.Json;

    /// <summary>
    /// A HTTP Handler that will route all lqbapi controllers.
    /// </summary>
    public class LendingQBControllerHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reusable. This is always true
        /// as the handler does not have any state per request.
        /// </summary>
        /// <value>Returns true since the handler has no unique state per request.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given incoming request.
        /// </summary>
        /// <param name="context">The HTTP context to process.</param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;

            if (request.HttpMethod != "POST")
            {
                // For security reason we only support POST.
                throw new NotSupportedException("Only POST is allow");
            }

            // Get ControllerName.
            // path = lqbapi/{controllerName}.ashx.
            int start = request.Path.LastIndexOf("/");
            int end = request.Path.IndexOf(".ashx", start);

            if (end < 0)
            {
                throw new NotSupportedException("Only ASHX files is supported.");
            }

            string op = request.QueryString["op"];

            if (string.IsNullOrEmpty(op))
            {
                throw new NotSupportedException("op is required.");
            }

            string controllerName = request.Path.Substring(start + 1, end - start - 1);

            AbstractLendingQBController controller = LendingQBControllerConfig.Get(controllerName);
            controller.Initialize(context);

            object result = controller.Main(op);

            if (result == null)
            {
                context.Response.StatusCode = 204; // 204 No Content
            }
            else
            {
                context.Response.ContentType = "application/json";
                this.SerializeResponse(context.Response, result);
            }
        }

        /// <summary>
        /// Serialize result to Http Response.
        /// </summary>
        /// <param name="response">The web response.</param>
        /// <param name="result">Result to serialize.</param>
        private void SerializeResponse(HttpResponse response, object result)
        {
            using (JsonWriter writer = new JsonTextWriter(response.Output))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.DefaultValueHandling = DefaultValueHandling.Ignore;

                serializer.Serialize(writer, result);
            }
        }
    }
}