﻿namespace LendersOffice.ObjLib.HttpHandlers
{
    using System;
    using System.Data.SqlClient;
    using System.IO;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// This class is for internal usage. This will check various type of resources (i.e: loginname, loanid, etc..) to
    /// see if the site has the id. The use case is for the web service proxy to hit all various site to check which
    /// login name it belong to.
    /// </summary>
    public class CheckInternalResourceHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether handler is resusable.
        /// </summary>
        public bool IsReusable => true;

        /// <summary>
        /// Process the http request.
        /// </summary>
        /// <param name="context">The web request.</param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;

            if (request.HttpMethod != "POST")
            {
                throw new NotSupportedException(); // Only support POST.
            }

            string currentIP = RequestHelper.ClientIP;

            if (!ConstStage.CheckInternalResourceWhitelistIps.Contains(currentIP))
            {
                throw new NotSupportedException(); // not from whitelist
            }

            var internalResourceRequest = this.GetRequest(request);

            CheckInternalResourceResponse response = null;

            if (internalResourceRequest.Type == "LoginName")
            {
                response = this.CheckLoginName(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "AuthId")
            {
                response = this.CheckAuthIdExists(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "CustomerCode")
            {
                response = this.CheckCustomerCode(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "BrokerId")
            {
                response = this.CheckBrokerId(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "LoanId")
            {
                response = this.CheckLoanId(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "ConsumerPortalId")
            {
                response = this.CheckConsumerPortalId(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "OcrRequestId")
            {
                response = this.CheckOcrRequestId(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "GDMSFileNumber")
            {
                response = this.CheckGDMSFileNumber(internalResourceRequest.Value);
            }
            else if (internalResourceRequest.Type == "DocuSignEnvelopeId")
            {
                response = this.CheckDocuSignEnvelopeId(internalResourceRequest.Value);
            }
            else
            {
                throw CBaseException.GenericException($"[{internalResourceRequest.Type}] is not supported.");
            }

            SerializationHelper.JsonSerializeToStream(context.Response.Output, response);
        }

        /// <summary>
        /// Check to see if DocuSign envelope id exists.
        /// </summary>
        /// <param name="value">DocuSign envelope id.</param>
        /// <returns>Whether envelope id exists.</returns>
        private CheckInternalResourceResponse CheckDocuSignEnvelopeId(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();
            var envelope = DocuSignEnvelope.LoadByOnlyEnvelopeId(value);

            if (envelope != null)
            {
                response.HasResource = true;
            }

            return response;
        }

        /// <summary>
        /// Check to see if GDMS file number exists.
        /// </summary>
        /// <param name="value">GDMS file number.</param>
        /// <returns>Whether GDMS file number exists.</returns>
        private CheckInternalResourceResponse CheckGDMSFileNumber(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            int id = 0;

            if (int.TryParse(value, out id))
            {
                var orderInfo = GDMSAppraisalOrderInfo.Load(id);

                if (orderInfo != null)
                {
                    response.HasResource = true;
                }
            }

            return response;
        }

        /// <summary>
        /// Check to see if ocr request id available.
        /// </summary>
        /// <param name="value">OCR request id.</param>
        /// <returns>Whether request id is available.</returns>
        private CheckInternalResourceResponse CheckOcrRequestId(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            var requestId = OcrRequestId.Create(value);
            if (requestId.HasValue)
            {
                var brokerId = OcrRequest.GetBrokerIdFromRequestId(requestId.Value);

                if (brokerId.HasValue)
                {
                    response.HasResource = true;
                }
            }

            return response;
        }

        /// <summary>
        /// Check if B-user login name exists.
        /// </summary>
        /// <param name="value">Login name.</param>
        /// <returns>Whether login name exists.</returns>
        private CheckInternalResourceResponse CheckLoginName(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            try
            {
                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByLoginName(value, out brokerId);
                response.HasResource = true;
            }
            catch (NotFoundException)
            {
                response.HasResource = false;
            }

            return response;
        }

        /// <summary>
        /// Check if the authentication id exists in the database, not if it has expired or if ther's a valid principal associated.
        /// </summary>
        /// <param name="authenticationIdString">The string representing the guid of the authentication id.</param>
        /// <returns>Whether the authentication id exists in the db.</returns>
        private CheckInternalResourceResponse CheckAuthIdExists(string authenticationIdString)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            Guid authenticationID;

            if (!Guid.TryParse(authenticationIdString, out authenticationID))
            {
                response.HasResource = false;
                return response;
            }

            // based on PrincipalFactory.Create(Guid authenticationId), but without the expiration or principal checks.
            foreach (DbConnectionInfo o in DbConnectionInfo.ListAll())
            {
                SqlParameter[] ps = 
                    {
                        new SqlParameter("@ByPassTicket", authenticationID)
                    };

                using (var reader = StoredProcedureHelper.ExecuteReader(o, "ALL_USER_GetUserIdByByPassTicket", ps))
                {
                    if (reader.Read())
                    {
                        response.HasResource = true;
                        return response;
                    }
                }
            }

            response.HasResource = false;

            return response;
        }

        /// <summary>
        /// Check if customer code exists.
        /// </summary>
        /// <param name="customerCode">Customer code.</param>
        /// <returns>Whether customer code exists.</returns>
        private CheckInternalResourceResponse CheckCustomerCode(string customerCode)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            try
            {
                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByCustomerCode(customerCode, out brokerId);

                response.HasResource = true;
            }
            catch (NotFoundException)
            {
                response.HasResource = false;
            }

            return response;
        }

        /// <summary>
        /// Check consumer portal id.
        /// </summary>
        /// <param name="value">Consumer portal id.</param>
        /// <returns>Whether consumer portal id exists.</returns>
        private CheckInternalResourceResponse CheckConsumerPortalId(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            try
            {
                Guid id = Guid.Empty;

                if (Guid.TryParse(value, out id))
                {
                    ConsumerPortalConfig config = ConsumerPortalConfig.RetrieveWithoutBrokerIdSlowSlow(id);
                    response.HasResource = true;
                }
            }
            catch (NotFoundException)
            {
                response.HasResource = false;
            }

            return response;
        }

        /// <summary>
        /// Check loan id.
        /// </summary>
        /// <param name="value">Unique loan id.</param>
        /// <returns>Whether loan id exists.</returns>
        private CheckInternalResourceResponse CheckLoanId(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            try
            {
                Guid loanId = Guid.Empty;

                if (Guid.TryParse(value, out loanId))
                {
                    Guid brokerId = Guid.Empty;
                    DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);
                    response.HasResource = true;
                }
            }
            catch (NotFoundException)
            {
                response.HasResource = false;
            }

            return response;
        }

        /// <summary>
        /// Check broker id.
        /// </summary>
        /// <param name="value">Broker id.</param>
        /// <returns>Whether broker id exists.</returns>
        private CheckInternalResourceResponse CheckBrokerId(string value)
        {
            CheckInternalResourceResponse response = new CheckInternalResourceResponse();

            try
            {
                Guid brokerId = Guid.Empty;

                if (Guid.TryParse(value, out brokerId))
                {
                    DbConnectionInfo.GetConnection(brokerId);

                    response.HasResource = true;
                }
            }
            catch (NotFoundException)
            {
                response.HasResource = false;
            }

            return response;
        }

        /// <summary>
        /// Parse the HTTP body content and construct an internal resource request object.
        /// </summary>
        /// <param name="request">HTTP request.</param>
        /// <returns>Internal resource request object.</returns>
        private CheckInternalResourceRequest GetRequest(HttpRequest request)
        {
            string json = string.Empty;

            using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
            {
                json = reader.ReadToEnd();
            }

            return SerializationHelper.JsonNetDeserialize<CheckInternalResourceRequest>(json);
        }

        /// <summary>
        /// A request object to check for internal resource.
        /// </summary>
        internal class CheckInternalResourceRequest
        {
            /// <summary>
            /// Gets or sets the type of resource to check.
            /// </summary>
            public string Type { get; set; }

            /// <summary>
            /// Gets or sets the value of resource to check.
            /// </summary>
            public string Value { get; set; }
        }

        /// <summary>
        /// A response object. Tthe default value of HasResource is false. therefore when HasResource is false, the property will not serialize.
        /// </summary>
        internal class CheckInternalResourceResponse
        {
            /// <summary>
            /// Gets or sets a value indicating whether resource exists.
            /// </summary>
            public bool HasResource { get; set; }
        }
    }
}