using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.ObjLib.RatesheetExpiration;

namespace LendersOfficeApp.ObjLib.AcceptableRatesheet
{
    public class AcceptableRsFile
    {
        string  m_ratesheetFileId;

        public string RatesheetFileId
        {
            get
            {
                return m_ratesheetFileId;
            }

            set
            {
                if( IsNew == false )
                {
                    throw new CBaseException(ErrorMessages.Generic, "RateSheetFile : don't allow to modify RatesheetFileId");
                }
                m_ratesheetFileId = value;
            }
        }

        public string DeploymentType { get; set; }

        public long InvestorXlsFileId { get; set; }

        public string InvestorXlsFilename { get; set; }

        public bool UseForBothMapAndBot { get; set; }

        public bool IsNew { get; private set; }

        // OPM 26294
        public static void ExpireOnNewRatesheetDetected(string[] investorXlsFileNames)
        {
            foreach (string investorXlsFileName in investorXlsFileNames)
            {
                ExpireOnNewRatesheetDetected(investorXlsFileName);
            }
        }

        public static void ExpireOnNewRatesheetDetected(string investorXlsFileName)
        {
            if (string.IsNullOrEmpty(investorXlsFileName)) { return; }

            // OPM 29001 - First check the InvestorExpiration object to determine if we really want to do this for the given investor
            if (!InvestorExpirationFactory.ExpireUponNewRatesheetDownload(investorXlsFileName)) { return; }

            try
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_File_ExpireOnNewRSDetected", 0, new SqlParameter("@InvestorXlsFileName", investorXlsFileName));
                Tools.LogInfo(
                    string.Join(Environment.NewLine,
                        "<RATESHEET_EXPIRATION>",
                        $"Ratesheet:{investorXlsFileName}",
                        "Status: EXPIRED",
                        $"Reason: Ratesheet '{investorXlsFileName}' is new, and is therefore being expired (by marking its deployment type as 'UseVersionAfterGeneratingAnOutputFileOk').",
                        $"Location: {nameof(AcceptableRsFile)}.{nameof(AcceptableRsFile.ExpireOnNewRatesheetDetected)} function.")
                );
            }
            catch (Exception e)
            {
                Tools.LogError("Error while expiring a newly detected ratesheet with InvestorXlsFileName = " + investorXlsFileName, e);
            }
        }

        public AcceptableRsFile( string rateId )
        {
            rateId = rateId.TrimWhitespaceAndBOM();
            using(var reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "RS_File_GetById", new SqlParameter("@LpeAcceptableRsFileId", rateId)))
            {
                if (!reader.Read())
                {
                    var a = new MissingAcceptableRsFileException(rateId);

                    // 1/29/2013 dd - Per SAE (Yin & Isaac) anything ends with "_POLICY.csv" is safe to ignore.
                    bool isSendToEOPM = ! (
                        rateId.EndsWith("_POLICY.csv", StringComparison.OrdinalIgnoreCase) ||
                        rateId.EndsWith(" POLICY.csv", StringComparison.OrdinalIgnoreCase) ||
                        rateId.EndsWith("_MASTER-CSV.csv", StringComparison.OrdinalIgnoreCase) );

                    if (isSendToEOPM)
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.MapProcessor,
                            $"Missing output files and investor ratesheet entry. {rateId}",
                            $"{ErrorMessages.RecordDoesntExist} LPE_ACCEPTABLE_RS_FILE table doesn't have the record : {rateId} {a}");
                    }

                    throw a;
                }

                m_ratesheetFileId   = (string)reader["LpeAcceptableRsFileId"];
                DeploymentType      = (string)reader["DeploymentType"];
                UseForBothMapAndBot = (bool)reader["IsBothRsMapAndBotWorking"];
                InvestorXlsFileId   = (long)reader["InvestorXlsFileId"];
                IsNew               = false;
            }
        }

        public AcceptableRsFile()
        {
            m_ratesheetFileId   = "";
            DeploymentType      = "UseVersion";
            UseForBothMapAndBot = true;
            InvestorXlsFilename = "";
            InvestorXlsFileId   = -1;
            IsNew               = true;
        }

        public void Save()
        {
            if( string.IsNullOrEmpty(m_ratesheetFileId) )
                throw new CBaseException(ErrorMessages.FailedToSave, "RateSheetFile.Save() : expect nonempty fileId");

            var parameters = new[]
            {
                new SqlParameter("@LpeAcceptableRsFileId", RatesheetFileId),
                new SqlParameter("@DeploymentType", DeploymentType),
                new SqlParameter("@IsBothRsMapAndBotWorking", UseForBothMapAndBot),
                new SqlParameter("@InvestorXlsFileId", InvestorXlsFileId)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, IsNew ? "RS_File_Create": "RS_File_Update" , 0, parameters);
            IsNew = false;
        }

        public static void RemoveFromDB( string rateId )
        {
            if (string.IsNullOrEmpty(rateId))
                throw new CBaseException(ErrorMessages.Generic, "AcceptableRsFileVersion.RemoveFromDB() : expect nonempty fileId");

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_File_Delete", 0, new SqlParameter("@LpeAcceptableRsFileId", rateId));
        }
    }

    public class MissingAcceptableRsFileException : CBaseException
    {
        public MissingAcceptableRsFileException(string ratesheetId) :
            base($"{ErrorMessages.RecordDoesntExist}. [{ratesheetId}]", $"LPE_ACCEPTABLE_RS_FILE table doesn\'t have the record : {ratesheetId}")
        {
            this.IsEmailDeveloper = false; // Already send the notification to SAE.

        }
    }

    public class AcceptableRsFileVersion
    {
        string    m_ratesheetFileId;
        long      m_versionNumber;

        public string RatesheetFileId
        {
            get
            {
                return m_ratesheetFileId;
            }

            set
            {
                if( IsNew == false )
                {
                    throw new CBaseException(ErrorMessages.Generic, "AcceptableRateSheetFileVersion : don't allow to modify RatesheetFileId");
                }
                m_ratesheetFileId = value;
            }
        }

        public bool IsNew { get; private set; }

        public long VersionNumber
        {
            get
            {
                return  m_versionNumber;
            }

            set
            {
                if( IsNew == false )
                {
                    throw new CBaseException(ErrorMessages.Generic, "AcceptableRsFileVersion : Cannot modify VersionNumber if the version exists already.");
                }
                m_versionNumber = value;

            }
        }

        public DateTime FirstEffective { get; set; }

        public DateTime LastestEffective { get; set; }

        public bool  IsExpirationIssuedByInvestor { get; set; }

        public DateTime ModifiedD { get; private set; }

        public DateTime CreatedD { get; private set; }

        public AcceptableRsFileVersion( string rateId )
        {
            rateId = rateId.TrimWhitespaceAndBOM();
            using(var reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "GetLatestRsFileVersion", new SqlParameter("@LpeAcceptableRsFileId", rateId)))
            {
                if( !reader.Read() )
                {
                    IsNew            = true;
                    m_ratesheetFileId  = rateId;
                    m_versionNumber    = -1;
                    FirstEffective   = SmallDateTime.MinValue;
                    LastestEffective = SmallDateTime.MinValue;
                    IsExpirationIssuedByInvestor = false;
                }
                else
                {
                    long version = (long)reader["VersionNumber"];
                    Init( rateId, version);
                }
            }
        }

        public AcceptableRsFileVersion( string rateId, long version )
        {
            Init( rateId, version );
        }

        public void Init( string rateId, long version )
        {
            rateId = rateId.TrimWhitespaceAndBOM();
            var parameters = new[]
            {
                new SqlParameter("@LpeAcceptableRsFileId", rateId),
                new SqlParameter("@VersionNumber", version)
            };

            using(var reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "RS_FileVersion_GetByIdAndVersion", parameters))
            {
                if (!reader.Read())
                {
                    throw new CBaseException(ErrorMessages.RecordDoesntExist, "LPE_ACCEPTABLE_RS_FILE_VERSION table doesn't have the record : " + rateId );
                }

                m_ratesheetFileId   = (string)reader["LpeAcceptableRsFileId"];
                m_versionNumber     = (long)reader["VersionNumber"];
                FirstEffective  = (DateTime)reader["FirstEffectiveDateTime"];
                LastestEffective  = (DateTime)reader["LatestEffectiveDateTime"];
                IsExpirationIssuedByInvestor = (bool)reader["isExpirationIssuedByInvestor"];
                CreatedD        = (DateTime)reader["EntryCreatedD"];
                ModifiedD           = (DateTime)reader["ModifiedD"];
                IsNew               = false;
            }
        }

        public AcceptableRsFileVersion( )
        {
            m_ratesheetFileId  = "";
            m_versionNumber    = 1;
            FirstEffective   = DateTime.Now;
            LastestEffective = FirstEffective;
            IsExpirationIssuedByInvestor = false;
            CreatedD        = DateTime.MinValue;
            ModifiedD           = DateTime.MinValue;

            IsNew            = true;
        }

        public void Save()
        {
            if (string.IsNullOrEmpty(m_ratesheetFileId))
            {
                throw new CBaseException(ErrorMessages.FailedToSave, "AcceptableRateSheetFileVersion.Save() : expect nonempty fileId");
            }

            var versionParam = new SqlParameter("@VersionNumber", IsNew ? -1 : m_versionNumber);
            if (IsNew) { versionParam.Direction = ParameterDirection.Output; }

            var parameters = new[]
            {
                new SqlParameter("@LpeAcceptableRsFileId", m_ratesheetFileId),
                new SqlParameter("@FirstEffectiveDateTime", FirstEffective),
                new SqlParameter("@LatestEffectiveDateTime", LastestEffective),
                new SqlParameter("@IsExpirationIssuedByInvestor", IsExpirationIssuedByInvestor),
                versionParam
            };

            var sp = IsNew ? "RS_FileVersion_Create" : "RS_FileVersion_Update";


            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, sp , 0, parameters);
            if( IsNew )
            {
                m_versionNumber = (long)versionParam.Value;
                IsNew         = false;
            }
            // refresh by re-reading record
            Init( m_ratesheetFileId, m_versionNumber );
        }

        public static void RemoveFromDB( string rateId, long version )
        {
            if( string.IsNullOrEmpty(rateId) )
                throw new CBaseException(ErrorMessages.Generic, "AcceptableRsFileVersion.Save() : expect nonempty fileId");

            var parameters = new[]
            {
                new SqlParameter("@LpeAcceptableRsFileId", rateId),
                new SqlParameter("@VersionNumber", version)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_FileVersion_Delete", 0, parameters);
        }

        public static void SetExpirationIssuedByInvestorValue(string ratesheetName, bool isExpirationIssuedByInvestor)
        {
            // OPM 21897 - remove the IsExpirationIssuedByInvestor value since we've downloaded a new ratesheet

            var sqlParameters = new[]
            {
                new SqlParameter("@InvestorXlsFileName", ratesheetName),
                new SqlParameter("@IsExpirationIssuedByInvestor", isExpirationIssuedByInvestor)
            };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_FileVersion_SetIsExpirationIssuedByInvestorByInvXlsFile", 0, sqlParameters);

        }

        public static void SetExpirationIssuedByInvestorValue(string[] ratesheetNames, bool isExpirationIssuedByInvestor)
        {
            foreach (string ratesheetName in ratesheetNames)
            {
                SetExpirationIssuedByInvestorValue(ratesheetName, isExpirationIssuedByInvestor);
            }
        }
    }

    public class InvestorXlsFile
    {
        public long   InvestorXlsFileId              { get; private set; }
        public string InvestorXlsFileName            { get; set; }
        public string EffectiveDateTimeWorksheetName { get; set; }
        public string EffectiveDateTimeLandMarkText  { get; set; }
        public int    EffectiveDateTimeRowOffset     { get; set; }
        public int    EffectiveDateTimeColumnOffset  { get; set; }
        public bool   IsNew                          { get; private set; }
        public bool   ExcludeFromInvPricingWarnings  { get; set; }

        public static InvestorXlsFile RetrieveByXlsFileName(string xlsFileName)
        {
            var parameters = new[]{ new SqlParameter("@InvestorXlsFileName", xlsFileName) };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_InvestorXlsFile_GetByName", parameters))
            {
                if (!reader.Read())
                {
                    throw new NotFoundException(ErrorMessages.RecordDoesntExist, "INVESTOR_XLS_FILE table doesn't have the record : " + xlsFileName);
                }

                var f = new InvestorXlsFile();
                f.GetData(reader);
                return f;
            }
        }
        public InvestorXlsFile(long xslFileId)
        {
            using(var reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "RS_InvestorXlsFile_GetById",
                                                  new SqlParameter("@InvestorXlsFileId", xslFileId)))
            {
                if (!reader.Read())
                {
                    throw new CBaseException(ErrorMessages.RecordDoesntExist, "INVESTOR_XLS_FILE table doesn't have the record : " + xslFileId );
                }

                this.GetData(reader);
            }
        }
        void GetData(IDataReader reader)
        {
            InvestorXlsFileId               = (long)   reader["InvestorXlsFileId"];
            InvestorXlsFileName             = (string) reader["InvestorXlsFileName"];
            EffectiveDateTimeWorksheetName  = (string) reader["EffectiveDateTimeWorksheetName"];
            EffectiveDateTimeLandMarkText   = (string) reader["EffectiveDateTimeLandMarkText"];
            EffectiveDateTimeRowOffset      = (int)    reader["EffectiveDateTimeRowOffset"];
            EffectiveDateTimeColumnOffset   = (int)    reader["EffectiveDateTimeColumnOffset"];
            ExcludeFromInvPricingWarnings   = (bool)   reader["ExcludeFromInvPricingWarnings"];
            IsNew                           = false;
        }

        public InvestorXlsFile()
        {
            InvestorXlsFileId               = -1L;
            InvestorXlsFileName             = "";
            EffectiveDateTimeWorksheetName  = "";
            EffectiveDateTimeLandMarkText   = "";
            EffectiveDateTimeRowOffset      = -1;
            EffectiveDateTimeColumnOffset   = -1;
            ExcludeFromInvPricingWarnings   = false;
            IsNew                           = true;
        }

        public void Save()
        {
            var idParam = new SqlParameter("@InvestorXlsFileId", InvestorXlsFileId) { Direction = IsNew ? ParameterDirection.Output : ParameterDirection.Input };

            var parameters = new[]
            {
                idParam,
                new SqlParameter("@InvestorXlsFileName", InvestorXlsFileName),
                new SqlParameter("@EffectiveDateTimeWorksheetName", EffectiveDateTimeWorksheetName),
                new SqlParameter("@EffectiveDateTimeLandMarkText", EffectiveDateTimeLandMarkText),
                new SqlParameter("@EffectiveDateTimeRowOffset", EffectiveDateTimeRowOffset),
                new SqlParameter("@EffectiveDateTimeColumnOffset", EffectiveDateTimeColumnOffset),
                new SqlParameter("@ExcludeFromInvPricingWarnings", ExcludeFromInvPricingWarnings)
            };

            var sp = IsNew ? "RS_InvestorXlsFile_Create" : "RS_InvestorXlsFile_Update";

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, sp , 0, parameters);

            if (IsNew)
            {
                InvestorXlsFileId = (long) idParam.Value;
                IsNew = false;
            }
        }

        public static void RemoveFromDB( long xslFileId )
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_InvestorXlsFile_Delete", 0,
                                                  new SqlParameter("@InvestorXlsFileId", xslFileId));
        }

        public static Dictionary<long, string> GetAllIdAndFileName2()
        {
            const string sSQL = "SELECT InvestorXlsFileId, InvestorXlsFileName from INVESTOR_XLS_FILE";

            var dict = new Dictionary<long, string>();
            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, reader =>
            {
                while (reader.Read())
                {
                    dict.Add((long)reader["InvestorXlsFileId"], reader["InvestorXlsFileName"] as string);
                }
            });
            return dict;
        }

        public static Hashtable GetAllIdAndFileName()
        {
            string sSQL = "SELECT InvestorXlsFileId, InvestorXlsFileName from INVESTOR_XLS_FILE" ;

            Hashtable table = new Hashtable();
            Action<IDataReader> readHandler = (IDataReader reader) =>
            {
                while (reader.Read())
                {
                    table[(long)reader["InvestorXlsFileId"]] = (string)reader["InvestorXlsFileName"];
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, readHandler);
            return table;
        }
    }
}
