﻿namespace LendersOffice.ObjLib.Renovation
{
    /// <summary>
    /// Enum class for FHA Property AcquisitionT.
    /// </summary>
    public enum FHAPropAcquisitionT
    {
        /// <summary>
        /// Not Available.
        /// </summary>
        NA = 0,

        /// <summary>
        /// Less than 12 months prior to case number assignment date.
        /// </summary>
        LessThan12MonthsPriorToCaseNumberAssignmentDate = 1,

        /// <summary>
        /// Greater than or equal to 12 months prior to case number assignment date.
        /// </summary>
        GreaterThanOrEqualTo12MonthsPriorToCaseNumberAssignmentDate = 2,

        /// <summary>
        /// Less than 12 months through inheritance or gift from family member.
        /// </summary>
        LessThan12MonthsThroughInheritanceOrGiftFromFamilyMember = 3
    }
}
