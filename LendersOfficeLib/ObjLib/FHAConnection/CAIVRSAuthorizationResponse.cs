﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Net;
using System.IO;
using CommonProjectLib.Common.Lib;
using LendersOffice.Audit;
using LendersOffice.ObjLib.Audit;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.FHAConnection
{
    public class CAIVRSAuthorizationResponse : AbstractFHAConnectionResponse
    {

        public CAIVRSAuthorizationResponse(string result, CPageData dataLoan)
            : base(result, dataLoan)
        {
            this.LogFieldDump();
        }

        public override void ApplyResultToLoan()
        {
            EnforceCanReadResult();

            // Per borrower:
            // auth_cd (CAIVRSAuthorizationCode) -> aFHABCaivrsNum, aFHACCaivrsNum
            SetCaivrsNums();

            m_dataLoan.sFHACAVIRSResultXmlContent = m_resultXml;

            m_dataLoan.Save();

            // We saved successfully, so create audit event.

            AbstractAuditItem audit =
                new FHAConnectionAuditItem(PrincipalFactory.CurrentPrincipal
                    , "CAIVRS Authorization"
                    , m_resultXml
                    , m_dataLoan.sAgencyCaseNum
                    );

            AuditManager.RecordAudit(m_dataLoan.sLId, audit);
        }
    }
}