﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Xml;
using LendersOffice.Common;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.ObjLib.FHAConnection
{
    abstract public class AbstractFHAConnectionResponse
    {
        public IEnumerable<string> StatusMessages { get; private set; }
        public bool IsError { get; private set; }

        protected CPageData m_dataLoan;
        protected string m_resultXml = string.Empty;
        protected XDocument m_resultXDoc = null;
        
        // key=field name from FHAC docs; value= linq queries to get the data from the xml.
        // We could use this more universally as IEnumberable<object>, but generics are invariant until C#4.0.
        // (ie. IEnumerable<SomeClass> is not a subtype of IEnumerable<object>)
        // Hopefully using dictionary will make it easier to debug this integration--
        // a dump of keys and their query result will show what was parsed out of response.
        protected Dictionary<string, IEnumerable<string>> m_fieldMap = new Dictionary<string, IEnumerable<string>>();

        protected bool CanReadResult
        {
            get { return (m_resultXDoc != null && IsError == false); }
        }

        protected void EnforceCanReadResult()
        {
            if (CanReadResult == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot query result in error state.");
            }
        }

        // Should set all values to the loan file based on the response content and create audit event.
        abstract public void ApplyResultToLoan();

        public AbstractFHAConnectionResponse(string result, CPageData dataLoan)
        {
            m_dataLoan = dataLoan;
            m_resultXml = result;
            try
            {
                m_resultXDoc = XDocument.Parse(m_resultXml);
            }
            catch (XmlException exc)
            {
                IsError = true;
                m_resultXDoc = null;
                Tools.LogError("An error occurred while parsing the XML response from FHAC B2G " + Environment.NewLine + m_resultXml, exc);
                StatusMessages = new List<string>( new string[] { "Unable to read result." } );
                return;
            }

            InitFieldMap();

            // Error response
            IsError = ( string.Equals("Error",m_fieldMap["status_flag"].First(), StringComparison.CurrentCultureIgnoreCase) 
                || string.Equals("Unavailable",m_fieldMap["status_flag"].First(), StringComparison.CurrentCultureIgnoreCase)
                //|| string.Equals("WARNING", m_fieldMap["status_flag"].First(), StringComparison.CurrentCultureIgnoreCase)
                );
            StatusMessages = m_fieldMap["status_message"].ToList();
        }

        #region Field Mapping

        private void InitFieldMap()
        {
            m_fieldMap.Add("status_message",
                from statusMsg in m_resultXDoc.Descendants("PROCESSSTATUS").Elements("ProcessStatusMessage")
                select statusMsg.Value);

            m_fieldMap.Add("status_flag",
                     from statusCode in m_resultXDoc.Descendants("PROCESSSTATUS")
                     select statusCode.Element("ProcessStatusCode").Value);
        }

        /// <summary>
        /// Set the field if it is found. Optional stomp options can be passed in. Default is it sets the value only if the previous value is empty.
        /// </summary>
        protected void SetField(string field, string oldValue, Action<string> setValue, StompingOptions stompOption = StompingOptions.OnlyIfEmpty)
        {
            this.SetField(field, string.IsNullOrEmpty(oldValue), setValue, stompOption);
        }

        protected virtual void SetField(string field, bool isEmpty, Action<string> setValue, StompingOptions stompOption = StompingOptions.OnlyIfEmpty)
        {
            string result;
            if (TryGetString(field, out result))
            {
                if (stompOption == StompingOptions.Always ||
                    (stompOption == StompingOptions.OnlyIfEmpty && isEmpty))
                {
                    setValue(result);
                }
            }
        }

        protected bool TryGetString( string name, out string strVal )
        {
            // We could add caching here by name if we need it, but
            // there will not likely be much repeated access.

            EnforceCanReadResult();

            IEnumerable<string> strings;
            if (m_fieldMap.TryGetValue(name, out strings))
            {
                try
                {
                    strVal = CleanResultString(strings.First());
                    return true;
                }
                catch (InvalidOperationException)
                {
                    // Thrown when list is empty
                    strVal = null;
                    return false;
                }
            }
            else
            {
                // Meaning trying to pull a value for an undefined field.
                throw new CBaseException(ErrorMessages.Generic, "Unknown key: " + name);
            }
        }


        /// <summary>
        /// Returns field if found, otherwise empty string. Consider TryGetString to handle missing field.
        /// </summary>
        protected string GetString( string name )
        {
            // We could add caching here by name if we need it, but
            // there will not likely be much repeated access.

            EnforceCanReadResult();

            IEnumerable<string> strings;
            if (m_fieldMap.TryGetValue(name, out strings))
            {
                try
                {
                    return CleanResultString(strings.First());
                }
                catch (InvalidOperationException)
                {
                    // Thrown if there are no elements in the list.
                    
                    return "";

                }
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unknown key: " + name);
            }
        }

        private string CleanResultString(string result)
        {
            if (result.Length > 3
                && result.Replace("0", "").Length == 0)
            {
                // Sometimes FHAC returns a series of zeros instead of a blank entry.
                // Per PDE, if string is all zeros and longer than 3 characters, 
                // treat it as blank.
                return "";
            }
            return result;
        }

        protected void LogFieldDump()
        {
            string fieldDump = this.FieldDump;
            if (!string.IsNullOrEmpty(fieldDump))
            {
                Tools.LogInfo(fieldDump);
            }
        }

        protected string FieldDump
        {
            get
            {
                if (CanReadResult)
                {
                    // DEBUG FIELD DUMP.
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("FIELD DUMP:");
                    foreach (string field in m_fieldMap.Keys)
                    {
                        List<string> values = m_fieldMap[field].ToList();

                        if (values.Count == 0)
                        {
                            sb.AppendLine(field + ": " + "[NOT FOUND]"  );
                        }
                        else
                        {
                            foreach (string str in values)
                            {
                                sb.AppendLine(field + ": " + str );
                            }
                        }

                    }

                    if (BorrowerList.Count != 0)
                    {
                        sb.Append(Environment.NewLine);

                        sb.AppendLine("Parsed Borrowers:");
                        foreach (var borr in BorrowerList)
                        {
                            sb.AppendLine("SSN: " + borr.SSN);
                            sb.AppendLine("FirstName: " + borr.FirstName);
                            sb.AppendLine("MiddleName: " + borr.MiddleName);
                            sb.AppendLine("LastName: " + borr.LastName);
                            sb.AppendLine("BorrowerBirthDate: " + borr.BorrowerBirthDate);
                            sb.AppendLine("CAIVRSAuthorizationCode: " + borr.CAIVRSAuthorizationCode);
                        }
                    }
                    return sb.ToString();
                }
                return string.Empty;
            }
        }

#endregion

        #region Loading Helpers
        
        protected class ParsedBorrower
        {
            public string SSN;
            public string FirstName;
            public string MiddleName;
            public string LastName;
            public string BorrowerBirthDate;
            public string CAIVRSAuthorizationCode;
        }

        private List<ParsedBorrower> m_BorrowerList;
        protected List<ParsedBorrower> BorrowerList
        {
            get
            {
                EnforceCanReadResult();

                if (m_BorrowerList == null)
                {
                    var borrowers = from borrower in m_resultXDoc.Descendants("BORROWER")
                                    select new ParsedBorrower()
                                    {
                                        SSN = borrower.Element("SSN") != null ? borrower.Element("SSN").Value : string.Empty,
                                        FirstName = borrower.Element("FirstName") != null ? borrower.Element("FirstName").Value : string.Empty,
                                        MiddleName = borrower.Element("MiddleName") != null ? borrower.Element("MiddleName").Value : string.Empty,
                                        LastName = borrower.Element("LastName") != null ? borrower.Element("LastName").Value : string.Empty,
                                        BorrowerBirthDate = borrower.Element("BorrowerBirthDate") != null ? borrower.Element("BorrowerBirthDate").Value : string.Empty,
                                        CAIVRSAuthorizationCode = borrower.Element("CAIVRSAuthorizationCode") != null ? borrower.Element("CAIVRSAuthorizationCode").Value : string.Empty
                                    };

                    m_BorrowerList = borrowers.ToList();
                }

                return m_BorrowerList;
            }
        }

        private Tuple<int, bool> FindBorrower(string ssn)
        {
            if (!string.IsNullOrEmpty(ssn))
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData currentApp = m_dataLoan.GetAppData(i);
                    if (currentApp.aBSsn.Replace("-", "") == ssn)
                    {
                        return new Tuple<int, bool>(i, true /*isPrimary*/);
                    }

                    if (currentApp.aCSsn.Replace("-", "") == ssn)
                    {
                        return new Tuple<int, bool>(i, false /*isPrimary*/);
                    }
                }
            }
            
            return null;
        }

        protected void SetBorrowerData()
        {
            foreach (var borr in BorrowerList)
            {
                Tuple<int, bool> borrowerLocation = FindBorrower(borr.SSN);
                if (borrowerLocation != null)
                {
                    CAppData appData = m_dataLoan.GetAppData(borrowerLocation.Item1);
                    if (borrowerLocation.Item2 == true /*IsPrimary*/ )
                    {
                        if (!string.IsNullOrEmpty(borr.FirstName)) appData.aBFirstNm = borr.FirstName;
                        if (!string.IsNullOrEmpty(borr.MiddleName)) appData.aBMidNm = borr.MiddleName;
                        if (!string.IsNullOrEmpty(borr.LastName)) appData.aBLastNm = borr.LastName;
                        if (!string.IsNullOrEmpty(borr.BorrowerBirthDate)) appData.aBDob_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(borr.BorrowerBirthDate);
                        if (!string.IsNullOrEmpty(borr.CAIVRSAuthorizationCode)) appData.aFHABCaivrsNum = borr.CAIVRSAuthorizationCode;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(borr.FirstName)) appData.aCFirstNm = borr.FirstName;
                        if (!string.IsNullOrEmpty(borr.MiddleName)) appData.aCMidNm = borr.MiddleName;
                        if (!string.IsNullOrEmpty(borr.LastName)) appData.aCLastNm = borr.LastName;
                        if (!string.IsNullOrEmpty(borr.BorrowerBirthDate)) appData.aCDob_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(borr.BorrowerBirthDate);
                        if (!string.IsNullOrEmpty(borr.CAIVRSAuthorizationCode)) appData.aFHACCaivrsNum = borr.CAIVRSAuthorizationCode;
                    }
                }
            }
        }

        protected void SetCaivrsNums()
        {
            foreach (var borr in BorrowerList.Where(b => b.CAIVRSAuthorizationCode != string.Empty))
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData currentApp = m_dataLoan.GetAppData(i);
                    if (currentApp.aBSsn.Replace("-", "") == borr.SSN)
                    {
                        currentApp.aFHABCaivrsNum = borr.CAIVRSAuthorizationCode;
                        break;
                    }

                    if (currentApp.aCSsn.Replace("-", "") == borr.SSN)
                    {
                        currentApp.aFHACCaivrsNum = borr.CAIVRSAuthorizationCode;
                        break;
                    }
                }
            }
        }

        protected E_sFHACaseT GetsFHACaseT(string CaseTypeCD)
        {
            switch (CaseTypeCD)
            {
                case "RegularHUD": return E_sFHACaseT.Regular_HUD;
                case "IrregularHUD": return E_sFHACaseT.Irregular_HUD;
                case "HUDVA/CRV": return E_sFHACaseT.HUD_VACRV;
                case "DEVA/CRV": return E_sFHACaseT.DE_VACRV;
                case "RegularDE": return E_sFHACaseT.Regular_DE;
                
                // -- uncomment after test.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown CaseTypeCD: " + CaseTypeCD);
            }
            return E_sFHACaseT.Regular_DE;
        }

        protected E_sFHAProcessingT GetFHAVAProcessingCode(string FHAVAProcessingCode)
        {
            // N/A, HECM, CoinsuranceConversion, MilitarySales, RealEstateOwned, CoinsuranceEndorsements. 
            switch (FHAVAProcessingCode)
            {
                case "N/A": return E_sFHAProcessingT.NA;
                case "HECM": return E_sFHAProcessingT.NA;
                case "CoinsuranceConversion": return E_sFHAProcessingT.CoinsuranceConversion;
                case "MilitarySales": return E_sFHAProcessingT.MilitarySales;
                case "RealEstateOwned": return E_sFHAProcessingT.REOWithAppraisal;
                case "CoinsuranceEndorsements": return E_sFHAProcessingT.CoinsuranceEndorsements;
                
                // -- uncomment after test.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown FHAVAProcessingCode: " + FHAVAProcessingCode);
            }
            return E_sFHAProcessingT.NA;
        }

        protected E_sFHAFinT GetFHAVAFinancingCode(string FHAVAFinancingCode)
        {
            //N/A, Private, GNMA, FNMA Only collected for Case type 2 with processing type RealEstateOwned and CoinsuranceEndorsements

            switch (FHAVAFinancingCode)
            {
                case "N/A": return E_sFHAFinT.NA;
                case "Private": return E_sFHAFinT.Private;
                case "GNMA": return E_sFHAFinT.GNMA;
                case "FNMA": return E_sFHAFinT.FNMA;
                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown FHAVAFinancingCode: " + FHAVAFinancingCode);
            }
            return E_sFHAFinT.NA;

        }

        protected E_sFHAPUDCondoT GetFHAVAApprovedCondominiumClassIndicator(string FHAVAApprovedCondominiumClassIndicator)
        {
            switch (FHAVAApprovedCondominiumClassIndicator)
            {
                case "Condominium": return E_sFHAPUDCondoT.Condo;
                case "PUD": return E_sFHAPUDCondoT.PUD;
                case "Subdivision": return E_sFHAPUDCondoT.Subdivision;
                case "NA": return E_sFHAPUDCondoT.NA;
                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown FHAVAApprovedCondominiumClassIndicator: " + FHAVAApprovedCondominiumClassIndicator);

            }
            return E_sFHAPUDCondoT.Unspecified;
        }

        protected E_sFHAConstCodeT GetBuildingStatusType(string BuildingStatusType)
        {
            switch (BuildingStatusType)
            {
                case "Proposed": return E_sFHAConstCodeT.Proposed;
                case "SubstantiallyRehabilitated": return E_sFHAConstCodeT.SubstantialRehab;
                case "UnderConstruction": return E_sFHAConstCodeT.UnderConstruction;
                case "Existing": return E_sFHAConstCodeT.Existing;
                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown BuildingStatusType: " + BuildingStatusType);
            }

            return E_sFHAConstCodeT.Unspecified;
        }

        protected E_sFHAComplianceInspectionAssignmentT GetInspectionType(string InspectionType)
        {
            switch (InspectionType)
            {
                case "Mortgagee": return E_sFHAComplianceInspectionAssignmentT.Mortgagee;
                case "LenderSelect": return E_sFHAComplianceInspectionAssignmentT.LenderSelect;
                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown InspectionType: " + InspectionType);
            }
            return E_sFHAComplianceInspectionAssignmentT.NA;
        }


        protected E_sFinMethT GetsFinMethT(string Amortization )
        {
            switch (Amortization)
            {
                case "Fixed": return E_sFinMethT.Fixed;
                case "AdjustableRateMortgage": return E_sFinMethT.ARM;
                case "GraduatedPaymentMortgage":return E_sFinMethT.Graduated;
                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown Amortization: " + Amortization);
            }

            return E_sFinMethT.Fixed;
        }

        protected E_sFHAADPHousingProgramT GetsFHAADPHousingProgramT(string HousingProgram)
        {
            switch (HousingProgram)
            {
                case "FHAStandardMortgageProgram203b": return  E_sFHAADPHousingProgramT.FHAStandard203b;
                case "Condominium234c": return  E_sFHAADPHousingProgramT.Condominium203b;
                case "Improvements203k": return  E_sFHAADPHousingProgramT.Improvements203k;
                case "ImprovementsCondominium203k": return  E_sFHAADPHousingProgramT.Improvements203k;
                case "UrbanRenewal220:": return  E_sFHAADPHousingProgramT.UrbanRenewal220k;

                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown Amortization: " + Amortization);
            }
            
            return E_sFHAADPHousingProgramT.Other;
        }


        protected E_sFHAADPSpecialProgramT GetsFHAADPSpecialProgramT(string SpecialProgram)
        {
            switch (SpecialProgram)
            {
                case "NoSpecialProgram": return  E_sFHAADPSpecialProgramT.NoSpecialProgram; 
                case "IndianLands": return  E_sFHAADPSpecialProgramT.IndianLands; 
                case "HawaiianHomelands": return  E_sFHAADPSpecialProgramT.HawaiianHomelands; 
                case "MilitaryImpactArea": return  E_sFHAADPSpecialProgramT.MilitaryImpactArea; 
                case "223eLocationWaiver": return  E_sFHAADPSpecialProgramT.LocationWaiver223e;

                // -- uncomment after test phase.
                //default: throw new CBaseException(ErrorMessages.Generic, "Unknown Amortization: " + Amortization);
            }
            return E_sFHAADPSpecialProgramT.NoSpecialProgram;
        }

        /// <summary>
        /// Imports the given spot lot indicator value.
        /// </summary>
        /// <param name="spotLotIndicator">The spot lot indicator.</param>
        /// <returns>The corresponding <see cref="E_sFHAPUDSiteCondoT"/> value.</returns>
        protected E_sFHAPUDSiteCondoT GetsFHAPUDSiteCondoT(string spotLotIndicator)
        {
            switch (spotLotIndicator)
            {
                case "":
                case "N":
                    return E_sFHAPUDSiteCondoT.NA;
                case "SiteCondo":
                    return E_sFHAPUDSiteCondoT.SiteCondo;
                case "Y":
                    return E_sFHAPUDSiteCondoT.SpotLot;
                default:
                    throw new ArgumentException($"{spotLotIndicator} is not a valid value for sFHAPUDSiteCondoT.");
            }
        }

        #endregion

        protected enum StompingOptions
        {
            OnlyIfEmpty,
            Always
        }
    }
    
}
