﻿namespace LendersOffice.ObjLib.FHAConnection
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using LendersOffice.Constants;

    public class HoldsTrackingRequest : AbstractFHAConnectionRequest
    {
        protected override string PilotURL
        {
            get { return ConstStage.FHAConnectionPilotDomain + "/b2b/chums/f17rqcxml.cfm"; }
        }
        protected override string ProductionURL
        {
            get { return ConstStage.FHAConnectionProductionDomain + "/b2b/chums/f17rqcxml.cfm"; }
        }
        protected override Type ResponseType
        {
            get { return typeof(HoldsTrackingResponse); }
        }        

        public HoldsTrackingRequest(Guid sLId)
            : base(sLId)
        {
        }
        
        private enum E_HoldsSubmissionMode
        {
            ByLastName = 0,
            ByCaseId = 1,
            ListAll = 2
        }
        
        private E_HoldsSubmissionMode m_submissionMode = E_HoldsSubmissionMode.ByLastName;
        private string m_caseId = null;

        public override AbstractFHAConnectionResponse Submit(string userNm, string password)
        {

            // Submit by last name
            string result = SubmitToFha(userNm, password);
            HoldsTrackingResponse response = new HoldsTrackingResponse(result, m_dataLoan);

            if ( response.IsError )
            {
                // Error response, try searching for ALL
                m_submissionMode = E_HoldsSubmissionMode.ListAll;
                LoadData();
                response = new HoldsTrackingResponse(SubmitToFha(userNm, password), m_dataLoan);

                if (response.IsError) return response; // Errors contained within -- UI with display status messages.
            }

            m_caseId = response.FindMatch();
                
            if (!String.IsNullOrEmpty(m_caseId))
            {
                // We found a match. Make another request with caseid
                m_submissionMode = E_HoldsSubmissionMode.ByCaseId;
                LoadData();
                response = new HoldsTrackingResponse (SubmitToFha(userNm, password), m_dataLoan);

                return response;
            }
            else
            {
                // We got non-error result, but no match found 
                return GetErrorResponse(new List<string> ( new string[]{"No matching cases were found."}));
            }
        }

        protected override void LoadData()
        {
            XDocument requestDoc = new XDocument(
                new XElement("MORTGAGEDATA",
                    new XAttribute("MISMOVersionID", "1.0.1"),
                    new XElement("PROCESSSTATUS",
                        m_submissionMode == E_HoldsSubmissionMode.ListAll ? new XElement("ProcessStatusCode", "ALL" ): null, // mode
                        new XElement("ProcessStatusRequestor", BROWSER_STRING)), // browser
                    new XElement("APPLICATION",
                        new XElement("UNDERWRITINGCASE",
                            new XElement("FHAVA",
                                m_submissionMode == E_HoldsSubmissionMode.ByCaseId ?  new XElement("ClasRAKeyValue", m_caseId) : null,  // ClasRaKey
                                new XElement("FHAVAOriginatorIdentifier", m_dataLoan.sFHALenderIdCode)))), // origid
                        m_submissionMode == E_HoldsSubmissionMode.ByLastName ?
                        new XElement("BORROWER",
                            m_submissionMode == E_HoldsSubmissionMode.ByLastName ? new XElement("LastName", m_dataLoan.GetAppData(0).aBLastNm) : null // name
                            ) : null
                            
                            ));

            m_requestXml = "<?xml version=\"1.0\" ?>" // XDeclaration does not let you do this.
                + requestDoc.ToString();

        }

        #region Submission Helpers

        private HoldsTrackingResponse GetErrorResponse(List<string> errors)
        {
            // Build error response
            string resultString = @"<?xml version=""1.0"" standalone=""yes""?><MORTGAGEDATA MISMOVersionID=""1.0.1""><PROCESSSTATUS><ProcessStatusCode>Error</ProcessStatusCode>";
            errors.ForEach(str => resultString += "<ProcessStatusMessage>" + str + "</ProcessStatusMessage>");
            resultString +="</PROCESSSTATUS></MORTGAGEDATA>";

            return new HoldsTrackingResponse(resultString, m_dataLoan);
        }


        #endregion

    }

    

}