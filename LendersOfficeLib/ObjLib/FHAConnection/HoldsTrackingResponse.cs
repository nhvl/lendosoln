﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Net;
using System.IO;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using CommonLib;
using LendersOffice.Audit;
using LendersOffice.ObjLib.Audit;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.FHAConnection
{
    public class HoldsTrackingResponse : AbstractFHAConnectionResponse
    {
        public HoldsTrackingResponse(string result, CPageData dataLoan)
            : base(result, dataLoan)
        {
             BuildFieldList();

             this.LogFieldDump();
        }

        private void BuildFieldList()
        {
            if (CanReadResult == false) return;

            m_fieldMap.Add("notesn",
                from item in m_resultXDoc.Descendants("PROCESSSTATUS").Elements("ProcessStatusMessage")
                where item.Attribute("ProcessStatusMessageType") != null
                select item.Value);

            m_fieldMap.Add("priorpd",
                from item in m_resultXDoc.Descendants("PROPERTY").Attributes("PriorPropertyDisposition")
                select item.Value);

            m_fieldMap.Add("city",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("City")
                select item.Value);

            m_fieldMap.Add("state",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("State")
                select item.Value);

            m_fieldMap.Add("county",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("CountyName")
                select item.Value);

            m_fieldMap.Add("zip",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("PostalCode")
                select item.Value);

            m_fieldMap.Add("suffix",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("ApartmentOrUnit")
                select item.Value);

            m_fieldMap.Add("pre",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionPrefix")
                select item.Value);

            m_fieldMap.Add("hse_no",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("HouseNumber")
                select item.Value);

            m_fieldMap.Add("street",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetName")
                select item.Value);

            m_fieldMap.Add("post",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionSuffix")
                select item.Value);

            m_fieldMap.Add("type",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetSuffix")
                select item.Value);

            m_fieldMap.Add("moyr_comp",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("StructureBuiltYear")
                select item.Value);

            m_fieldMap.Add("liv_units",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("FinancedNumberOfUnits")
                select item.Value);

            m_fieldMap.Add("blk_plat_blk",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionBlk")
                select item.Value);

            m_fieldMap.Add("blk_plat_plt",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionPlt")
                select item.Value);

            m_fieldMap.Add("lot",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionLot")
                select item.Value);

            m_fieldMap.Add("tpo_ein",
                from item in m_resultXDoc.Descendants("LOAN_ORIGINATOR").Attributes("EmployerIdentificationNumber")
                select item.Value);

            m_fieldMap.Add("prev_case",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAPreviousCaseNumber")
                select item.Value);

            m_fieldMap.Add("adp_code",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAADPCode")
                select item.Value);

            m_fieldMap.Add("sr_refi",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAStreamlinedRefinanceIndicator")
                select item.Value);

            m_fieldMap.Add("fo_code",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAFieldOfficeCode")
                select item.Value);

            m_fieldMap.Add("case_no",
                from item in m_resultXDoc.Descendants("APPLICATION").Elements("AgencyCaseIdentifier")
                select item.Value);

            m_fieldMap.Add("case_ref",
                from item in m_resultXDoc.Descendants("APPLICATION").Elements("LenderCaseIdentifier")
                select item.Value);

            m_fieldMap.Add("close_dt",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("ClosingPackageReceivedDate")
                select item.Value);

            m_fieldMap.Add("case_type",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("CaseTypeCD")
                select item.Value);

            m_fieldMap.Add("pgm_id",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAProgramCodeIdentifier")
                select item.Value);

            m_fieldMap.Add("sponsor",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVASponsorIdentifier")
                select item.Value);

            m_fieldMap.Add("originator",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAOriginatorIdentifier")
                select item.Value);

            m_fieldMap.Add("procsng",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAProcessingCode")
                select item.Value);

            m_fieldMap.Add("financing",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAFinancingCode")
                select item.Value);

            m_fieldMap.Add("consult_id",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVA203KConsultantIdentifier")
                select item.Value);

            m_fieldMap.Add("nt_auth",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Attributes("FHAVAStreamlinedReFinanceAuthorizationNumber")
                select item.Value);

            m_fieldMap.Add("nt_o_pval",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIOriginalPropertyValue")
                where !string.IsNullOrWhiteSpace(item.Value)
                select item.Value);

            m_fieldMap.Add("ct_name",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIContactName")
                select item.Value);

            m_fieldMap.Add("ct_phone",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIContactPhone")
                select item.Value);

            m_fieldMap.Add("orig_name",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("PartyName") != null
                select item.Element("PartyName").Value);

            m_fieldMap.Add("orig_addrs",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("Address1") != null
                select item.Element("Address1").Value);

            m_fieldMap.Add("spnsr_name",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("PartyName") != null
                select item.Element("PartyName").Value);

            m_fieldMap.Add("spnsr_addrs",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("Address1") != null
                select item.Element("Address1").Value);

            m_fieldMap.Add("insp_id",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Inspector"
                select item.Attribute("PARTYID").Value);

            m_fieldMap.Add("spc_id",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("PROJECTID")
                select item.Value);

            m_fieldMap.Add("spc_name",
                from item in m_resultXDoc.Descendants("PROJECT").Elements("ProjectName")
                select item.Value);

            m_fieldMap.Add("spc_ind",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumClassIndicator")
                select item.Value);

            m_fieldMap.Add("spc_phase",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumCooperativePhase")
                select item.Value);

            m_fieldMap.Add("spot_lot",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumCooperativeSpotLot")
                select item.Value);

            m_fieldMap.Add("constn_code",
                from item in m_resultXDoc.Descendants("VALUATIONS").Attributes("BuildingStatusType")
                select item.Value);

            m_fieldMap.Add("insp_type",
                from item in m_resultXDoc.Descendants("VALUATIONS").Elements("InspectionType")
                select item.Value);

        }

        public override void ApplyResultToLoan()
        {
            EnforceCanReadResult();
            SetCaivrsNums();
            SetField("notesn", m_dataLoan.sFHALenderNotes, value => m_dataLoan.sFHALenderNotes = value);

            SetField("city", m_dataLoan.sSpCity, value => m_dataLoan.sSpCity = value);
            SetField("state", m_dataLoan.sSpState, value => m_dataLoan.sSpState = value);
            SetField("zip", m_dataLoan.sSpZip, value => m_dataLoan.sSpZip = value);

            //m_dataLoan.sSpCounty = GetString("county"); // Need mapping for FIPS.

            string moyr_comp = GetString("moyr_comp");
            if (String.IsNullOrEmpty(moyr_comp) == false)
            {
                DateTime PropertyCompletedDate;
                if (DateTime.TryParse(FhaConnectionUtil.ConvertDateStringFromFHAFormat(moyr_comp), out PropertyCompletedDate))
                {
                    if (m_dataLoan.sSpMonthBuiltT == E_sSpMonthBuiltT.Blank)
                    {
                    m_dataLoan.sSpMonthBuiltT = (E_sSpMonthBuiltT)PropertyCompletedDate.Month;
                    }

                    if (string.IsNullOrEmpty(m_dataLoan.sYrBuilt))
                    {
                        m_dataLoan.sYrBuilt = PropertyCompletedDate.Year.ToString();
                    }
                }
            }

            SetField("liv_units", m_dataLoan.sUnitsNum_rep, value => m_dataLoan.sUnitsNum_rep = value);

            if (string.IsNullOrEmpty(m_dataLoan.sSpBlockPlatNum))
            {
            FhaConnectionUtil.SetCombinedValues(
                value => m_dataLoan.sSpBlockPlatNum = value,
                GetString("blk_plat_blk"),
                GetString("blk_plat_plt"));
            }

            SetField("lot", m_dataLoan.sSpLotNum, value => m_dataLoan.sSpLotNum = value);
            SetField("tpo_ein", m_dataLoan.sFHASponsoredOriginatorEIN, value =>
            {
                try
                {
                    m_dataLoan.sFHASponsoredOriginatorEIN = value;
                }
                catch (FieldInvalidValueException)
                {
                    Tools.LogInfo($"Unable to import sFHASponsoredOriginatorEIN. LoanId: {this.m_dataLoan.sLId}, Value: {value}");
                }
            });
            SetField("prev_case", m_dataLoan.sFHAPreviousCaseNum, value => m_dataLoan.sFHAPreviousCaseNum = value);
            SetField("adp_code", m_dataLoan.sFHAADPCode, value => m_dataLoan.sFHAADPCode = value);

            SetField("prev_case", m_dataLoan.sFHAPreviousCaseNum, value => m_dataLoan.sFHAPreviousCaseNum = value);
            SetField("adp_code", m_dataLoan.sFHAADPCode, value => m_dataLoan.sFHAADPCode = value);

            SetField("fo_code", m_dataLoan.sFHAFieldOfficeCode, value => m_dataLoan.sFHAFieldOfficeCode = value);
            SetField("case_no", m_dataLoan.sAgencyCaseNum, value => m_dataLoan.sAgencyCaseNum = value, StompingOptions.Always);
            SetField("case_ref", m_dataLoan.sLenderCaseNum, value => m_dataLoan.sLenderCaseNum = value);
            Action<string> setEstCloseD = (value) =>
                {
                    m_dataLoan.sEstCloseD_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(value);
                    m_dataLoan.sEstCloseDLckd = true;
                };
            SetField("close_dt", m_dataLoan.sEstCloseD_rep, setEstCloseD);
            SetField("pgm_id", m_dataLoan.sFHAProgramId, value => m_dataLoan.sFHAProgramId = value);
            SetField("sponsor", m_dataLoan.sFHASponsorAgentIdCode, value => m_dataLoan.sFHASponsorAgentIdCode = value);
            SetField("originator", m_dataLoan.sFHALenderIdCode, value => m_dataLoan.sFHALenderIdCode = value);
            SetField("procsng", m_dataLoan.sFHAProcessingT == E_sFHAProcessingT.NA, value => m_dataLoan.sFHAProcessingT = GetFHAVAProcessingCode(value));
            SetField("financing",m_dataLoan.sFHAFinT == E_sFHAFinT.NA, value => m_dataLoan.sFHAFinT = GetFHAVAFinancingCode(value));
            SetField("consult_id", m_dataLoan.sFHA203kConsultantId, value => m_dataLoan.sFHA203kConsultantId = value);
            SetField("nt_auth", m_dataLoan.sFHARefiAuthNum, value => m_dataLoan.sFHARefiAuthNum = value);
            SetField("nt_o_pval", m_dataLoan.sOriginalAppraisedValue_rep, value => m_dataLoan.sOriginalAppraisedValue_rep = value);
            SetField("ct_name", m_dataLoan.sFHANewCaseContactNm, value => m_dataLoan.sFHANewCaseContactNm = value);
            SetField("ct_phone", m_dataLoan.sFHANewCaseContactPhone, value => m_dataLoan.sFHANewCaseContactPhone = value);

            //// SetBorrowerData();      // bn_name, bn_ssn, can_auth - OPM 230811: FHAC concatenates first and middle name. This is their bug, but immediate solution is to not import borrower info.
            SetOriginatorData();    // orig_name, orig_addrs
            SetSponsorData();       // spnsr_name, spnsr_addrs

            SetField("insp_id", m_dataLoan.sFHAComplianceInspectionAssignmentID, value => m_dataLoan.sFHAComplianceInspectionAssignmentID = value);

            SetsCpmProjectId();     // spc_id
            SetsProjNm();           // spc_name
            SetsFHAPUDCondoT();     // spc_ind
            SetField("spc_phase", m_dataLoan.sFHAPUDSubmissionPhase, value => m_dataLoan.sFHAPUDSubmissionPhase = value);

            SetField("spot_lot", m_dataLoan.sFHAPUDSiteCondoT == E_sFHAPUDSiteCondoT.NA, value => m_dataLoan.sFHAPUDSiteCondoT = value == "Yes" ? E_sFHAPUDSiteCondoT.SpotLot : E_sFHAPUDSiteCondoT.SiteCondo);
            SetsFHAConstCodeT(); // constn_code
            SetField("insp_type", m_dataLoan.sFHAComplianceInspectionAssignmentT == E_sFHAComplianceInspectionAssignmentT.NA, value => m_dataLoan.sFHAComplianceInspectionAssignmentT = GetInspectionType(value));

            // Holds tracking request replaces Case Number results
            m_dataLoan.sFHACaseNumberResultXmlContent = m_resultXml;

            m_dataLoan.Save();

            // We saved successfully, so create audit event.

            AbstractAuditItem audit =
                new FHAConnectionAuditItem(PrincipalFactory.CurrentPrincipal
                    , "Holds Tracking"
                    , m_resultXml
                    , m_dataLoan.sAgencyCaseNum
                    );

            AuditManager.RecordAudit(m_dataLoan.sLId, audit);

        }

        private void SetOriginatorData()
        {
            bool isUpdate = false;

            IPreparerFields field = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.CreateNew);
            string originatorOriginalName = field.CompanyName;

            string companyName = GetString("orig_name");
            if (string.IsNullOrEmpty(companyName) == false)
            {
                if (string.IsNullOrEmpty(field.CompanyName))
                {
                field.CompanyName = companyName;
                isUpdate = true;
            }
            }

            string address = GetString("orig_addrs");
            if (string.IsNullOrEmpty(address) == false)
            {
                Address addr = new Address();
                addr.ParseCityStateZip(address);
                if (string.IsNullOrEmpty(field.City))
                {
                    field.City = addr.City;
                }

                if (string.IsNullOrEmpty(field.State))
                {
                    field.State = addr.State;
                }

                if (string.IsNullOrEmpty(field.Zip))
                {
                    field.Zip = addr.Zipcode;
                }
            }

            if ( isUpdate )
                field.Update();
        }

        private void SetSponsorData()
        {
            bool isUpdate = false;

            IPreparerFields field = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.CreateNew);
            string originatorOriginalName = field.CompanyName;

            string companyName = GetString("spnsr_name");
            if (string.IsNullOrEmpty(companyName) == false)
            {
                if (string.IsNullOrEmpty(field.CompanyName))
                {
                field.CompanyName = companyName;
                isUpdate = true;
            }
            }

            string address = GetString("spnsr_addrs");
            if (string.IsNullOrEmpty(address) == false)
            {
                Address addr = new Address();
                addr.ParseCityStateZip(address);
                if (string.IsNullOrEmpty(field.City))
                {
                    field.City = addr.City;
                }

                if (string.IsNullOrEmpty(field.State))
                {
                   field.State = addr.State;
                }

                if (string.IsNullOrEmpty(field.Zip))
                {
                    field.Zip = addr.Zipcode;
                }
            }

            if (isUpdate)
                field.Update();
        }


        private void SetsCpmProjectId()
        {
            string sCpmProjectId = m_dataLoan.sCpmProjectId;
            string spc_id = GetString("spc_id");

            if (!string.IsNullOrEmpty(spc_id) && string.IsNullOrEmpty(m_dataLoan.sCpmProjectId))
            {

                if (sCpmProjectId.Length > 9 && spc_id.Length > 9 && spc_id.Substring(0, 9) == sCpmProjectId.Substring(0, 9))
                {
                    // First 9 characters match means do not overwrite
                }
                else
                {
                    m_dataLoan.sCpmProjectId = spc_id;
                }
            }
        }

        private void SetsProjNm()
        {
            string sProjNm = m_dataLoan.sProjNm;
            string spc_name = GetString("spc_name");

            if (!string.IsNullOrEmpty(spc_name) && string.IsNullOrEmpty(sProjNm))
            {
                if (sProjNm.Length > 30 && spc_name.Length > 30 && spc_name.Substring(0, 30) == sProjNm.Substring(0, 30))
                {
                    // First 30 characters match means do not overwrite
                }
                else
                {
                    m_dataLoan.sProjNm = spc_name;
                }
            }
        }

        private void SetsFHAPUDCondoT()
        {
            string spc_ind = GetString("spc_ind");

            if (!string.IsNullOrEmpty(spc_ind))
            {
                if (m_dataLoan.sFHAPUDCondoT == E_sFHAPUDCondoT.NA)
                {
                    m_dataLoan.sFHAPUDCondoT = GetFHAVAApprovedCondominiumClassIndicator(spc_ind);
                }

                E_sFHAPUDCondoT result = m_dataLoan.sFHAPUDCondoT;

                if (m_dataLoan.sGseSpT == E_sGseSpT.LeaveBlank)
                {
                    if (result == E_sFHAPUDCondoT.PUD)
                    {
                        m_dataLoan.sGseSpT = E_sGseSpT.PUD;
                    }
                    else if (result == E_sFHAPUDCondoT.Condo)
                    {
                        m_dataLoan.sGseSpT = E_sGseSpT.Condominium;
                    }
                }
            }
        }

        private void SetsFHAConstCodeT()
        {
            string constn_code = GetString("constn_code");

            if (!string.IsNullOrEmpty(constn_code))
            {
                E_sFHAConstCodeT code = GetBuildingStatusType(constn_code);
                if (m_dataLoan.sFHAConstCodeT == E_sFHAConstCodeT.Unspecified)
                {
                m_dataLoan.sFHAConstCodeT = code;
                }

                if (m_dataLoan.sFHAConstructionT == E_sFHAConstructionT.Blank)
                {
                    if (code == E_sFHAConstCodeT.New)
                    {
                        m_dataLoan.sFHAConstructionT = E_sFHAConstructionT.New;
                    }
                    else if (code == E_sFHAConstCodeT.Existing)
                    {
                        m_dataLoan.sFHAConstructionT = E_sFHAConstructionT.Existing;
                    }
                    else if (code == E_sFHAConstCodeT.Proposed)
                    {
                        m_dataLoan.sFHAConstructionT = E_sFHAConstructionT.Proposed;
                    }
                }
            }
        }

        internal string FindMatch()
        {
            EnforceCanReadResult();

            var potentialMatches =
                from item in m_resultXDoc.Descendants("CASEDATA")
                select new
                {
                    CaseId = (string)item.Attributes("CASEID").FirstOrDefault(),
                    Originator = (string)item.Elements("UNDERWRITINGCASE").Elements("FHAVA").Elements("FHAVAOriginatorIdentifier").FirstOrDefault(),
                    UnparsedBorr = (string)item.Elements("BORROWER").Elements("UnparsedName").FirstOrDefault(),
                    HouseNumber = (string)item.Elements("PROPERTY").Elements("PARSEDSTREETNAME").Elements("HouseNumber").FirstOrDefault(),
                    StreetName = (string)item.Elements("PROPERTY").Elements("PARSEDSTREETNAME").Elements("StreetName").FirstOrDefault(),
                    PostalCode = (string)item.Element("PROPERTY").Element("PostalCode").Value
                };

            foreach (var fhaCase in potentialMatches)
            {
                if (string.IsNullOrEmpty(fhaCase.CaseId)
                    || string.IsNullOrEmpty(fhaCase.Originator)
                    || string.IsNullOrEmpty(fhaCase.UnparsedBorr)
                    || string.IsNullOrEmpty(fhaCase.HouseNumber)
                    || string.IsNullOrEmpty(fhaCase.StreetName)
                    || string.IsNullOrEmpty(fhaCase.PostalCode)
                    )
                {
                    continue; // Not enough data to make a match.
                }

                if (fhaCase.Originator != m_dataLoan.sFHALenderIdCode)
                    continue; // This case does not match for OriginatorId

                Name borrName = new Name();
                borrName.ParseName(fhaCase.UnparsedBorr);
                bool foundBorrower = false;
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData currentApp = m_dataLoan.GetAppData(i);
                    if (string.Equals(currentApp.aBFirstNm, borrName.FirstName, StringComparison.CurrentCultureIgnoreCase)
                        && string.Equals(currentApp.aBLastNm, borrName.LastName, StringComparison.CurrentCultureIgnoreCase) )
                    {
                        foundBorrower = true;
                        break;
                    }

                    if (currentApp.aCIsDefined
                        && string.Equals(currentApp.aCFirstNm, borrName.FirstName, StringComparison.CurrentCultureIgnoreCase)
                        && string.Equals(currentApp.aCLastNm, borrName.LastName, StringComparison.CurrentCultureIgnoreCase) )
                    {
                        foundBorrower = true;
                        break;
                    }
                }

                if (foundBorrower == false)
                    continue; // This case does not match borrower name

                string zip = fhaCase.PostalCode.Length > 5 ? fhaCase.PostalCode.Substring(0, 5) : fhaCase.PostalCode;
                Address address = new Address();
                address.ParseStreetAddress(m_dataLoan.sSpAddr);

                if ( !string.Equals(address.StreetNumber, fhaCase.HouseNumber, StringComparison.CurrentCultureIgnoreCase)
                    || !string.Equals(address.StreetName, fhaCase.StreetName, StringComparison.CurrentCultureIgnoreCase)
                    || !string.Equals(zip, m_dataLoan.sSpZip, StringComparison.CurrentCultureIgnoreCase)
                    )
                    continue;  // This case does not have a matching address

                // This case is a match
                return fhaCase.CaseId;
            }

            // No matches found!
            return null;
        }
    }
}