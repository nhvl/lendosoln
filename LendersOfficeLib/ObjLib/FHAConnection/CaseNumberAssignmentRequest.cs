﻿namespace LendersOffice.ObjLib.FHAConnection
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using global::CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Constants;

    public class CaseNumberAssignmentRequest : AbstractFHAConnectionRequest
    {
        protected override string PilotURL
        {
            get { return ConstStage.FHAConnectionPilotDomain + "/b2b/chums/f17rlcxml.cfm"; }
        }
        protected override string ProductionURL
        {
            get { return ConstStage.FHAConnectionProductionDomain + "/b2b/chums/f17rlcxml.cfm"; }
        }
        protected override Type ResponseType
        {
            get { return typeof(CaseNumberAssignmentResponse); }
        }
        public CaseNumberAssignmentRequest(Guid sLId, bool IsForExistingCase) : this(sLId, IsForExistingCase, false) { }
        public CaseNumberAssignmentRequest(Guid sLId, bool IsForExistingCase, bool IsOverrideAddressValidation)
            : base(sLId)
        {
            m_isForExistingCase = IsForExistingCase;
            m_isOverrideAddressValidation = IsOverrideAddressValidation;
            LoadData();
        }

        public override AbstractFHAConnectionResponse Submit(string userNm, string password)
        {
            string result = SubmitToFha(userNm, password);

            CaseNumberAssignmentResponse response = new CaseNumberAssignmentResponse(result, m_dataLoan, this.m_isForExistingCase);

            return response;
            
        }

        private bool m_isForExistingCase = false;
        private bool m_isOverrideAddressValidation = false;

        protected override void LoadData()
        {
            XDocument requestDoc = new XDocument(
                new XElement("MORTGAGEDATA",
                    new XAttribute("MISMOVersionID", "1.0.1"),
                    new XElement("PROCESSSTATUS",
                        new XElement("ProcessStatusRequestor", BROWSER_STRING), // browser
                        new XElement("ProcessStatusCode", m_isForExistingCase ? "Update" : "Add" ), // status_flag
                        new XElement("UserStatusMessage", m_dataLoan.sFHALenderNotes, // notesn
                            new XAttribute("sCode", "0")), // n 
                        new XElement("PropertyAddressValidationIndicator", m_isOverrideAddressValidation ? "Override" : "Validate" ), //addr_stat
                        new XElement("BorrowerValidationIndicator", "Validate")), //borr_stat

                    new XElement("APPLICATION",
                        new XAttribute("LoanPurposeType", /* prind */ m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase ? "ForwardPurchase" : "ForwardRefinance"),
                       m_isForExistingCase ? new XElement("AgencyCaseIdentifier", m_dataLoan.sAgencyCaseNum.Replace("-","")) : null, // caseno
                        new XElement("LenderCaseIdentifier", m_dataLoan.sLenderCaseNum), // caseref
                        new XElement("UNDERWRITINGCASE",
                            m_dataLoan.sFhaLenderIdT == E_sFhaLenderIdT.SponsoredOriginatorEIN ?
                            new XElement("LOAN_ORIGINATOR",
                                new XAttribute("_EmployerIdentificationNumber", m_dataLoan.sFHASponsoredOriginatorEIN.Replace("-","") ), //ein - Originator
                                new XAttribute("_NationwideMortgageLicensingSystem", NationwideMortgageLicensingSystemOriginator)) : null, //nmls - Originator
                            new XElement("LOAN_OFFICER",
                                new XAttribute("_NationwideMortgageLicensingSystem", NationwideMortgageLicensingSystemOfficer), //nmls - Officer
                                new XAttribute("_FirstName", FirstName), //fn
                                new XAttribute("_LastName", LastName)), //ln
                            new XElement("BORROWER_APPLICATION",
                                new XAttribute("_Certification", m_dataLoan.sFHALenderHasThisApplication ? "Yes" : "No" )), //certify
                            new XElement("FHAVA",
                                new XAttribute("FHAVAADPCode", m_dataLoan.sFHAADPCode), //adpcode
                                IncludeFHAVAPriorFinancingIndicator ? new XAttribute("FHAVAPriorFinancingIndicator", FHAVAPriorFinancingIndicator) : null, //priorfha
                                IncludeRefinanceCashOut ? new XAttribute("RefinanceCashOut", RefinanceCashOut) : null, //cash_out
                                IncludeFHAVAStreamlinedRefi ? new XAttribute("FHAVAStreamlinedRefinanceIndicator", FHAVAStreamlinedRefinanceIndicator) : null, //srrefi
                                IncludeFHAVAPreviousCaseNumber ? new XAttribute("FHAVAPreviousCaseNumber", m_dataLoan.sFHAPreviousCaseNum) : null, //prevcase
                                new XAttribute("Amortization", Amortization), //adp_chara
                                IncludeHousingProgram ? new XAttribute("HousingProgram", HousingProgram): null, //adp_charh
                                IncludeSpecialProgram ? new XAttribute("SpecialProgram", SpecialProgram): null, //adp_chars
                                new XAttribute("BuyDown", m_dataLoan.sBuydown? "Yes" : "No"), //adp_charb
                                new XAttribute("PrincipalWriteDown", PrincipalWriteDown), //prin_wd
                                IncludeClosingPackageReceivedDate ? new XElement("ClosingPackageReceivedDate", ToFHACDateFormat(m_dataLoan.sEstCloseD_rep)) : null, // clsg_dt
                                new XElement("CaseTypeCD", CaseTypeCD ), // casetype
                                IncludeFHAVAFinancingCode ? new XElement("FHAVAFinancingCode", FHAVAFinancingCode) : null, //financing
                                new XElement("FHAVAFieldOfficeCode", m_dataLoan.sFHAFieldOfficeCode), //focode
                                new XElement("FHAVAInspectorAssignedIdentifier", m_dataLoan.sFHAComplianceInspectionAssignmentID), //insp_id
                                IncludeFHAVAOriginatorIdentifier ? new XElement("FHAVAOriginatorIdentifier",
                                    new XAttribute("_NationwideMortgageLicensingSystem", NationwideMortgageLicensingSystemOriginator), //nlms FHAVAORiginatorIdentifier
                                    m_dataLoan.sFHALenderIdCode) : null, // orig

                                new XElement("FHAVAProgramCodeIdentifier", m_dataLoan.sFHAProgramId), //pgmid
                                new XElement("FHAVAProcessingCode", FHAVAProcessingCode), //procsng
                                new XElement("FHAVA203KConsultantIdentifier", m_dataLoan.sFHA203kConsultantId, //consultant_id
                                    m_dataLoan.sFHA203kType != E_sFHA203kType.NA ? new XAttribute("_203kType", m_dataLoan.sFHA203kType) : null), 
                                IncludeFHAVASponsorIdentifier ? new XElement("FHAVASponsorIdentifier", m_dataLoan.sFHASponsorAgentIdCode) : null, //sponsor
                                m_dataLoan.sLT == E_sLT.VA ? new XElement("FHAVAVetransExpirationDate", ToFHACDateFormat(m_dataLoan.GetAppData(0).aVALAnalysisExpirationD_rep)) : null, //va_exp_dt
                                m_dataLoan.sLT == E_sLT.VA ? new XElement("FHAVAVetransReferenceNumber", m_dataLoan.sVACRVNum) : null, //va_no
                                IncludeFHAVARefiContact ? new XElement("FHAVAStreamlinedReFinance",
                                    m_isForExistingCase ? new XAttribute("FHAVAStreamlinedReFinanceAuthorizationNumber", m_dataLoan.sFHAPreviousCaseNum) : null, //nt_auth
                                    new XElement("FHAVASRFIContactName", m_dataLoan.sFHANewCaseContactNm), //ct_name
                                    new XElement("FHAVASRFIContactPhone", m_dataLoan.sFHANewCaseContactPhone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", ""))) : null //ct_phone
                                    ))),
                    new XElement("PROPERTY",
                        IncludePriorPropertyDisposition ? new XAttribute("PriorPropertyDisposition", m_dataLoan.sFHACasePreviouslySoldByHUDAsREO ? "Y" : "N" ) : null, //priorpd
                        new XAttribute("PROPERTYID", 1),
                        new XAttribute("PropertyType", "Subject"),
                        new XElement("City", m_dataLoan.sSpCity), // city
                        new XElement("State", m_dataLoan.sSpState), // state
                        new XElement("CountyCode", m_dataLoan.sSpCountyFipsLast3Digits), // county
                        new XElement("PostalCode", m_dataLoan.sSpZip), // zip_cd
                    
                        new XElement("PARSEDSTREETNAME",
                            new XElement("ApartmentOrUnit", ApartmentOrUnit), // suffix
                            new XElement("DirectionPrefix", DirectionPrefix), // pre
                            new XElement("HouseNumber", HouseNumber), // hse_no
                            new XElement("StreetName", StreetName), // street
                            new XElement("DirectionSuffix", DirectionSuffix), // post
                            new XElement("StreetSuffix", StreetSuffix))), // type
                    
                    new XElement("SUBJECTPROPERTY",
                        new XElement("StructureBuiltYear", StructureBuiltYear), //moyr_comp
                        new XElement("StructureAge", StructureAge), //age_comp
                        new XElement("FinancedNumberOfUnits", m_dataLoan.sUnitsNum), // liv_units
                        new XElement("SubjectPropertyShortLegalDescriptionBlk", m_dataLoan.sSpBlockPlatNum ), // blk_plat
                        new XElement("SubjectPropertyShortLegalDescriptionPlt", m_dataLoan.sSpBlockPlatNum ), // blk_plat
                        new XElement("SubjectPropertyShortLegalDescriptionLot", m_dataLoan.sSpLotNum )), // lot

                    new XElement("VALUATIONS",
                        new XAttribute("BuildingStatusType", BuildingStatusType), //const_code
                        new XElement("InspectionType", InspectionType)), // insp_type

                    from BorrowerInfo borr in m_borrowerList
                    select new XElement("BORROWER",
                        new XAttribute("BORROWERID", borr.BorrowerNum), // n
                        new XElement("BorrowerBirthDate", ToFHACDateFormat(borr.DOB)),
                        new XElement("FirstName", borr.FirstName), // bn_name
                        new XElement("MiddleName", borr.MiddleName), // bn_name
                        new XElement("LastName", borr.LastName), // bn_name
                        new XElement("NameSuffix", borr.Suffix),
                        new XElement("SSN", borr.Ssn)), // bn_ssn

                    IncludeProject ? new XElement("PROJECT",
                        new XAttribute("PROJECTID", m_dataLoan.sCpmProjectId), // spc_id
                        new XAttribute("FHAVAApprovedCondominiumClassIndicator", FHAVAApprovedCondominiumClassIndicator), //spc_ind
                        new XAttribute("FHAVAApprovedCondominiumCooperativePhase", m_dataLoan.sFHAPUDSubmissionPhase), //spc_phase
                        new XAttribute("FHAVAApprovedCondominiumCooperativeSpotLot", FHAVAApprovedCondominiumCooperativeSpotLot), //spot_lot
                        new XElement("ProjectName", m_dataLoan.sProjNm)) : null, //spc_name
                    new XElement("PRODUCT",
                        new XElement("LoanAmortizationTermMonths", m_dataLoan.sTerm_rep)) // loan_term
                        ));

            m_requestXml = "<?xml version=\"1.0\" standalone=\"yes\"?>" // XDeclaration does not let you do this.
                + requestDoc.ToString();
        }

        #region FieldMethods
        private string NationwideMortgageLicensingSystemOriginator
        {
            get 
            {
                IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                return  interviewer.CompanyLoanOriginatorIdentifier;
            }
        }
        
        private string NationwideMortgageLicensingSystemOfficer
        {
            get
            {
                IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                return interviewer.LoanOriginatorIdentifier;
            }
        }
        
        private string FirstName
        {
            get
            {
                CAgentFields officialLoanOfficer = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                return ParseName(officialLoanOfficer.AgentName).Item1;
            }
        }
        
        private string LastName
        { 
            get 
            {
                CAgentFields officialLoanOfficer = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                return ParseName(officialLoanOfficer.AgentName).Item2;
            }
        }

        private Tuple<string, string> ParseName( string fullName )
        {
            Tuple<string, string> name = Tuple.Create<string,string>(fullName, fullName);

            CommonLib.Name parsedName = new CommonLib.Name();
            parsedName.ParseName(fullName);

            return Tuple.Create<string, string>(parsedName.FirstName, parsedName.LastName);

        }

        private bool IncludeFHAVAPriorFinancingIndicator
        {
            get
            {
                return m_dataLoan.sLT == E_sLT.FHA && m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase;
            }
        }

        /// <summary>
        /// Gets a value representing the FHA PUD Site Condo field for export to FHA.
        /// </summary>
        private string FHAVAApprovedCondominiumCooperativeSpotLot
        {
            get
            {
                switch (m_dataLoan.sFHAPUDSiteCondoT)
                {
                    case E_sFHAPUDSiteCondoT.NA:
                        return string.Empty;
                    case E_sFHAPUDSiteCondoT.SiteCondo:
                        return "SiteCondo";
                    case E_sFHAPUDSiteCondoT.SpotLot:
                        // SpotLot is deprecated since 2010, this shouldn't happen except for old loans
                        return "Y";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAPUDSiteCondoT);
                }
            }
        }

        private string FHAVAPriorFinancingIndicator
        {
            get
            {
                switch (m_dataLoan.sTotalScoreRefiT)
                {
                    case E_sTotalScoreRefiT.ConventionalToFHA:
                        return "No";
                    case E_sTotalScoreRefiT.FHAToFHANonStreamline:
                        return "Yes";
                    case E_sTotalScoreRefiT.Streamline:
                    case E_sTotalScoreRefiT.HOPEForHomeowners:
                    case E_sTotalScoreRefiT.LeaveBlank:
                    case E_sTotalScoreRefiT.Unknown:
                        return string.Empty;
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sTotalScoreRefiT);
                }
            }
        }

        private bool IncludeRefinanceCashOut
        {
            get
            {
                return m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase;
            }

        }


        private string RefinanceCashOut
        {
            get
            {
                return (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity) ? "Yes" : "No";
            }

        }

        private bool IncludeFHAVAStreamlinedRefi
        {
            get
            {
                return m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance;
            }
        }

        private bool IncludeFHAVARefiContact
        {
            get
            {
                return ((m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance) || 
                    (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin) ||
                    (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout) ||
                    (m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity));
            }
        }

        private string FHAVAStreamlinedRefinanceIndicator
        {
            get
            {
                return m_dataLoan.sFHAPurposeIsStreamlineRefiWithAppr ? "Yes" : "No";
            }
        }



        private bool IncludeFHAVAPreviousCaseNumber
        {
            get
            {
                return 
                    (
                    (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase
                      && m_dataLoan.sFHACasePreviouslySoldByHUDAsREO)
                    ||
                    (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase
                      && FHAVAPriorFinancingIndicator == "Yes")
                      );
            }
        }

        private string Amortization
        {
            get
            {
                switch (m_dataLoan.sFinMethT)
                {
                    case E_sFinMethT.Fixed:
                        return "Fixed";
                    case E_sFinMethT.ARM:
                        return "AdjustableRateMortgage";
                    case E_sFinMethT.Graduated:
                        return "GraduatedPaymentMortgage";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFinMethT);
                }
            }
        }

        private bool IncludeHousingProgram
        {
            get
            {
                return m_dataLoan.sLT == E_sLT.FHA;
            }
        }

        private string HousingProgram
        {
            get
            {
                switch (m_dataLoan.sFHAADPHousingProgramT)
                {
                    case E_sFHAADPHousingProgramT.FHAStandard203b:
                        return "FHAStandardMortgageProgram203b";
                    case E_sFHAADPHousingProgramT.Condominium203b:
                        return "Condominium234c";
                    case E_sFHAADPHousingProgramT.Improvements203k:
                        return "Improvements203k";
                    case E_sFHAADPHousingProgramT.UrbanRenewal220k:
                        return "UrbanRenewal220";
                    case E_sFHAADPHousingProgramT.Other:
                    case E_sFHAADPHousingProgramT.Unspecified:
                        return "Other";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAADPHousingProgramT);
                }

            }
        }


        private bool IncludeSpecialProgram
        {
            get
            {
                return m_dataLoan.sLT == E_sLT.FHA;
            }
        }

        private string SpecialProgram
        {
            get
            {
                switch (m_dataLoan.sFHAADPSpecialProgramT)
                {
                    case E_sFHAADPSpecialProgramT.NoSpecialProgram:
                        return "NoSpecialProgram";
                    case E_sFHAADPSpecialProgramT.IndianLands:
                        return "IndianLands";
                    case E_sFHAADPSpecialProgramT.HawaiianHomelands:
                        return "HawaiianHomelands";
                    case E_sFHAADPSpecialProgramT.MilitaryImpactArea:
                        return "MilitaryImpactArea";
                    case E_sFHAADPSpecialProgramT.LocationWaiver223e:
                        return "223eLocationWaiver";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAADPSpecialProgramT);
                }
            }
        }

        private string PrincipalWriteDown
        {
            get
            {
                switch (m_dataLoan.sFHAADPPrincipalWriteDownT)
                {
                    case E_sFHAADPPrincipalWriteDownT.No:
                        return "No";
                    case E_sFHAADPPrincipalWriteDownT.GreaterThanOrEqualToTenPercent:
                    case E_sFHAADPPrincipalWriteDownT.LessThanTenPercent:
                        return "Yes";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAADPPrincipalWriteDownT);
                }
            }
        }

        private bool IncludeClosingPackageReceivedDate
        {
            get
            {
                return (m_dataLoan.sLT == E_sLT.FHA) && (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
                    && (m_dataLoan.sTotalScoreRefiT == E_sTotalScoreRefiT.FHAToFHANonStreamline);
            }
        }

        private string CaseTypeCD
        {
            get
            {
                switch (m_dataLoan.sFHACaseT)
                {
                    case E_sFHACaseT.DE_VACRV:
                        return "DEVA/CRV";
                    case E_sFHACaseT.HUD_VACRV:
                        return "HUDVA/CRV";
                    case E_sFHACaseT.Irregular_HUD:
                        return "IrregularHUD";
                    case E_sFHACaseT.Regular_DE:
                        return "RegularDE";
                    case E_sFHACaseT.Regular_HUD:
                        return "RegularHUD";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHACaseT);
                }
            }
        }

        private bool IncludeFHAVAFinancingCode
        {
            get
            {
                return m_dataLoan.sFHACaseT == E_sFHACaseT.Irregular_HUD &&
                    (m_dataLoan.sFHAProcessingT == E_sFHAProcessingT.REOWithAppraisal
                    || m_dataLoan.sFHAProcessingT == E_sFHAProcessingT.REOWithoutAppraisal
                    || m_dataLoan.sFHAProcessingT == E_sFHAProcessingT.CoinsuranceEndorsements);
            }
        }

        private string FHAVAFinancingCode
        {
            get
            {
                switch (m_dataLoan.sFHAFinT)
                {
                    case E_sFHAFinT.NA:
                        return "N/A";
                    case E_sFHAFinT.Private:
                        return "Private";
                    case E_sFHAFinT.GNMA:
                        return "GNMA";
                    case E_sFHAFinT.FNMA:
                        return "FNMA";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAFinT);
                }
            }
        }

        private bool IncludeFHAVAOriginatorIdentifier
        {
            get
            {
                return m_dataLoan.sFhaLenderIdT != E_sFhaLenderIdT.SponsoredOriginatorEIN;
            }
        }
        
        private string FHAVAProcessingCode
        {
            get
            {
                switch (m_dataLoan.sFHAProcessingT)
                {
                    case E_sFHAProcessingT.NA:
                        return "N/A";
                    case E_sFHAProcessingT.REOWithAppraisal:
                        return "RealEstateOwnedWithAppraisal";
                    case E_sFHAProcessingT.REOWithoutAppraisal:
                        return "RealEstateOwnedWithoutAppraisal";
                    case E_sFHAProcessingT.CoinsuranceConversion:
                        return "CoinsuranceConversion";
                    case E_sFHAProcessingT.CoinsuranceEndorsements:
                        return "CoinsuranceEndorsements";
                    case E_sFHAProcessingT.MilitarySales:
                        return "MilitarySales";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAProcessingT);
                }
            }
        }

        private bool IncludeFHAVASponsorIdentifier
        {
            get
            {
                return m_dataLoan.sFHASponsorAgentIdCode.Length != 0;
            }
        }

        private bool IncludeFHAVAStreamlinedReFinance
        {
            get
            {
                return m_isForExistingCase && m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance;
            }

        }

        private bool IncludePriorPropertyDisposition
        {
            get
            {
                return m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase;
            }
        }

        private string ToFHACDateFormat(string date)
        {
            DateTime parsedDate;
            if (DateTime.TryParse(date, out parsedDate))
            {
                Func<string, string> padZero = (s) => s.Length == 1 ? "0" + s : s;

                return parsedDate.Year.ToString() + padZero(parsedDate.Month.ToString()) + padZero(parsedDate.Day.ToString());
                
            }
            else
            {
                return string.Empty;
            }

        }

        private string BuiltMonth
        {
            get
            {
                string monthPart = "01";
                switch (m_dataLoan.sSpMonthBuiltT)
                {
                    case E_sSpMonthBuiltT.February: monthPart = "02"; break;
                    case E_sSpMonthBuiltT.March: monthPart = "03"; break;
                    case E_sSpMonthBuiltT.April: monthPart = "04"; break;
                    case E_sSpMonthBuiltT.May: monthPart = "05"; break;
                    case E_sSpMonthBuiltT.June: monthPart = "06"; break;
                    case E_sSpMonthBuiltT.July: monthPart = "07"; break;
                    case E_sSpMonthBuiltT.August: monthPart = "08"; break;
                    case E_sSpMonthBuiltT.September: monthPart = "09"; break;
                    case E_sSpMonthBuiltT.October: monthPart = "10"; break;
                    case E_sSpMonthBuiltT.November: monthPart = "11"; break;
                    case E_sSpMonthBuiltT.December: monthPart = "12"; break;
                }


                return monthPart;
            }

        }

        private string StructureBuiltYear
        {
            get
            {
                // Their format is YYYYMMDD
                // Per FHA: "Day portion of address is required for formatting but is not processed or stored"
                return ( m_dataLoan.sYrBuilt + BuiltMonth + "01");
            }
        }

        
        private string StructureAge
        {
            get
            {

                DateTime builtYear;

                if (DateTime.TryParse(BuiltMonth + "/01/" + m_dataLoan.sYrBuilt, out builtYear))
                {
                    TimeSpan span = (DateTime.Now - builtYear);
                    return ((int)(span.TotalDays / 365)).ToString();
                }
                else
                {
                    Tools.LogError("Could not calculate StructureAge. Built:" + StructureBuiltYear);
                    return string.Empty;
                }
            }
        }


        private string BuildingStatusType
        {
            get
            {
                switch (m_dataLoan.sFHAConstCodeT)
                {
                    case E_sFHAConstCodeT.Existing:
                        return "Existing";
                    case E_sFHAConstCodeT.New:
                        return "NewConstruction";
                    case E_sFHAConstCodeT.Proposed:
                        return "Proposed";
                    case E_sFHAConstCodeT.SubstantialRehab:
                        return "SubstantiallyRehabilitated";
                    case E_sFHAConstCodeT.UnderConstruction:
                        return "UnderConstruction";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAConstCodeT);
                }
            }
        }

        private string InspectionType
        {
            get
            {
                switch (m_dataLoan.sFHAComplianceInspectionAssignmentT)
                {
                    case E_sFHAComplianceInspectionAssignmentT.NA:
                        return string.Empty;
                    case E_sFHAComplianceInspectionAssignmentT.Roster:
                        return "Roster";
                    case E_sFHAComplianceInspectionAssignmentT.Mortgagee:
                        return "Mortgagee";
                    case E_sFHAComplianceInspectionAssignmentT.LenderSelect:
                        return "LenderSelect";
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAComplianceInspectionAssignmentT);
                }
            }
        }

        private bool IncludeProject
        {
            get
            {
                return (m_dataLoan.sGseSpT == E_sGseSpT.Condominium
                    || m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium
                    || m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium
                    || m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
                    || m_dataLoan.sGseSpT == E_sGseSpT.Condominium
                    || m_dataLoan.sGseSpT == E_sGseSpT.PUD
                    );
            }
        }

        private string FHAVAApprovedCondominiumClassIndicator
        {
            get
            {
                switch (m_dataLoan.sFHAPUDCondoT)
                {
                    case E_sFHAPUDCondoT.PUD:
                        return "PUD";
                    case E_sFHAPUDCondoT.Condo:
                        return "Condominium";
                    case E_sFHAPUDCondoT.Subdivision:
                        return "Subdivision";
                    case E_sFHAPUDCondoT.NA:
                        return string.Empty;
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAPUDCondoT);
                }
            }
        }

        private CommonLib.Address x_Address = null;
        private CommonLib.Address ParsedAddress
        {
            get
            {
                if (x_Address == null)
                {
                    x_Address = new CommonLib.Address();
                    x_Address.ParseStreetAddress(m_dataLoan.sSpAddr);
                }

                return x_Address;
            }
        }

        private string ApartmentOrUnit
        {
            get
            {
                return m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStUnit : ParsedAddress.AptNum;
            }
        }

        private string DirectionPrefix
        {
            get
            {
                return m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStDir : ParsedAddress.sStreetDir;
            }
        }

        private string HouseNumber
        {
            get
            {
                return  m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStNum : ParsedAddress.StreetNumber;
            }
        }

        private string StreetName
        {
            get
            {
                return m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStName : ParsedAddress.StreetName;
            }
        }

        private string DirectionSuffix
        {
            get
            {
                return m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStPostDir : ParsedAddress.sStPostDir;
            }
        }

        private string StreetSuffix
        {
            get
            {
                return m_dataLoan.sUseParsedAddressFieldsForIntegrations ? m_dataLoan.sSpStType : CommonLib.Address.USPSStreetTypeFromMLStreetType(ParsedAddress.StreetType);
            }
        }
        #endregion
    }

}