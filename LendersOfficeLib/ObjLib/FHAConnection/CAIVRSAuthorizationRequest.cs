﻿namespace LendersOffice.ObjLib.FHAConnection
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Constants;

    public class CAIVRSAuthorizationRequest : AbstractFHAConnectionRequest
    {
        protected override string PilotURL
        {
            get { return ConstStage.FHAConnectionPilotDomain + "/b2b/chums/f17cvrsxml.cfm"; }
        }
        protected override string ProductionURL
        {
            get { return ConstStage.FHAConnectionProductionDomain + "/b2b/chums/f17cvrsxml.cfm"; }
        }
        protected override Type ResponseType
        {
            get { return typeof(CAIVRSAuthorizationResponse); }
        }


        public CAIVRSAuthorizationRequest(Guid sLId)
            : base(sLId)
        {
        }

        public override AbstractFHAConnectionResponse Submit(string userNm, string password)
        {
            string result = SubmitToFha(userNm, password);

            CAIVRSAuthorizationResponse response = new CAIVRSAuthorizationResponse(result, m_dataLoan);

            return response;
        }

        protected override void LoadData()
        {
            XDocument requestDoc = new XDocument(
                new XElement("MORTGAGEDATA",
                    new XAttribute("MISMOVersionID", "1.0.1"),
                    new XElement("PROCESSSTATUS",
                        new XElement("ProcessStatusRequestor", BROWSER_STRING)), // browser
                        from BorrowerInfo borr in m_borrowerList
                        select new XElement("BORROWER",
                            new XAttribute("BORROWERID", borr.BorrowerNum), // Borrowerid
                            new XElement("SSN", borr.Ssn)), //ssn
                    new XElement("APPLICATION",
                        new XElement("UNDERWRITINGCASE",
                            new XElement("FHAVA",
                                new XElement("FHAVAOriginatorIdentifier", m_dataLoan.sLT == E_sLT.VA ? m_dataLoan.sVALenderIdCode : m_dataLoan.sFHALenderIdCode), // orig_id
                                new XElement("FHAVAAgencyIdentifier", GetsFHAVAAgencyIdentifier(m_dataLoan.sFHAAgencyT)) // agency_id
                                
                        )))));

            m_requestXml = "<?xml version=\"1.0\" standalone=\"yes\"?>" // XDeclaration does not let you do this.
                + requestDoc.ToString();
        }

        private string GetsFHAVAAgencyIdentifier(E_sFHAAgencyT sFHAAgencyT)
        {
                switch (sFHAAgencyT)
                {
                    case E_sFHAAgencyT.Unspecified: return "Unspecified";
                    case E_sFHAAgencyT.HUDFHASingleFamily: return "FHASingleFamily";
                    case E_sFHAAgencyT.HUDFHATitleI: return "FHATitleOne";
                    case E_sFHAAgencyT.HUDNativeAmericanPrograms: return "HUDNativeAmericanPrograms";
                    case E_sFHAAgencyT.USDAFarmServices: return "USDA FarmServices";
                    case E_sFHAAgencyT.USDARuralDevelopment: return "USDA FarmServices";
                    case E_sFHAAgencyT.USDARuralHousing: return "USDARuralDevelopment";
                    case E_sFHAAgencyT.VeteransAffairs: return "DepartmentOfVeteransAffairs";
                    default:
                        throw new UnhandledEnumException(sFHAAgencyT);
                }
        }
    }

}