﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Net;
using System.IO;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using LendersOffice.Audit;
using LendersOffice.ObjLib.Audit;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.FHAConnection
{
    public class CaseNumberAssignmentResponse : AbstractFHAConnectionResponse
    {
        private bool isForExistingCase;

        public CaseNumberAssignmentResponse(string result, CPageData dataLoan, bool isForExistingCase)
            : base(result, dataLoan)
        {
            this.isForExistingCase = isForExistingCase;
            BuildFieldList();

            this.LogFieldDump();
        }

        private void BuildFieldList()
        {
            if (CanReadResult == false) return;

            m_fieldMap.Add("notesn",
                from item in m_resultXDoc.Descendants("PROCESSSTATUS").Elements("ProcessStatusMessage")
                where item.Attribute("ProcessStatusMessageType") != null
                select item.Value);

            m_fieldMap.Add("prind",
                from item in m_resultXDoc.Descendants("APPLICATION").Elements("LoanPurposeType")
                select item.Value);

            m_fieldMap.Add("casetype",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("CaseTypeCD")
                select item.Value);

            m_fieldMap.Add("caseno",
                from item in m_resultXDoc.Descendants("APPLICATION").Elements("AgencyCaseIdentifier")
                select item.Value);

            m_fieldMap.Add("caseref",
                from item in m_resultXDoc.Descendants("APPLICATION").Elements("LenderCaseIdentifier")
                select item.Value);

            m_fieldMap.Add("ein",
                from item in m_resultXDoc.Descendants("LOAN_ORIGINATOR").Attributes("EmployerIdentificationNumber")
                select item.Value);

            m_fieldMap.Add("officer:nmls",
                from item in m_resultXDoc.Descendants("LOAN_OFFICER").Attributes("_NationwideMortgageLicensingSystem")
                select item.Value);

            m_fieldMap.Add("certify",
                from item in m_resultXDoc.Descendants("BORROWER_APPLICATION").Attributes("_Certification")
                select item.Value);

            m_fieldMap.Add("rec_date",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("CaseReceivedDate")
                select item.Value);

            m_fieldMap.Add("clsg_dt",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("ClosingPackageReceivedDate")
                select item.Value);

            m_fieldMap.Add("adpcode",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAADPCode")
                select item.Value);

            m_fieldMap.Add("priorfha",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAPriorFinancingIndicator")
                select item.Value);

            m_fieldMap.Add("cash_out",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("RefinanceCashOut")
                select item.Value);

            m_fieldMap.Add("srrefi",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAStreamlinedRefinanceIndicator")
                select item.Value);

            m_fieldMap.Add("prevcase",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAPreviousCaseNumber")
                select item.Value);

            m_fieldMap.Add("adp_chara",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("Amortization")
                select item.Value);

            m_fieldMap.Add("adp_charh",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("HousingProgram")
                select item.Value);

            m_fieldMap.Add("adp_chars",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("SpecialProgram")
                select item.Value);

            m_fieldMap.Add("adp_charb",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("BuyDown")
                select item.Value);

            m_fieldMap.Add("prin_wd",
                from item in m_resultXDoc.Descendants("FHAVA").Attributes("PrincipalWriteDown")
                select item.Value);

            m_fieldMap.Add("financing",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAFinancingCode")
                select item.Value);

            m_fieldMap.Add("focode",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAFieldOfficeCode")
                select item.Value);

            m_fieldMap.Add("insp_id",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Inspector"
                select item.Attribute("PARTYID").Value);

            m_fieldMap.Add("insp_type",
                from item in m_resultXDoc.Descendants("VALUATIONS").Elements("InspectionType")
                select item.Value);

            m_fieldMap.Add("orig",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAOriginatorIdentifier")
                select item.Value);

            m_fieldMap.Add("orig_name",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("PartyName") != null
                select item.Element("PartyName").Value);

            m_fieldMap.Add("orig_city",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("City") != null
                select item.Element("City").Value);

            m_fieldMap.Add("orig_state",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("State") != null
                select item.Element("State").Value);

            m_fieldMap.Add("orig_zip",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("Zip") != null
                select item.Element("Zip").Value);

            m_fieldMap.Add("pgmid",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAProgramCodeIdentifier")
                select item.Value);

            m_fieldMap.Add("procsng",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAProcessingCode")
                select item.Value);

            m_fieldMap.Add("consultant_id",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVA203KConsultantIdentifier")
                select item.Value);

            m_fieldMap.Add("sponsor",
                from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVASponsorIdentifier")
                select item.Value);

            m_fieldMap.Add("spnsr_name",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("PartyName") != null
                select item.Element("PartyName").Value);

            m_fieldMap.Add("spnsr_city",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("City") != null
                select item.Element("City").Value);

            m_fieldMap.Add("spnsr_state",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("State") != null
                select item.Element("State").Value);

            m_fieldMap.Add("spnsr_zip",
                from item in m_resultXDoc.Descendants("PARTY")
                where item.Attribute("PartyType").Value == "Lender"
                && item.Element("PostalCode") != null
                select item.Element("PostalCode").Value);

            m_fieldMap.Add("nt_o_pval",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIOriginalPropertyValue")
                where !string.IsNullOrWhiteSpace(item.Value)
                select item.Value);

            m_fieldMap.Add("ct_name",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIContactName")
                select item.Value);

            m_fieldMap.Add("ct_phone",
                from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Elements("FHAVASRFIContactPhone")
                select item.Value);

            m_fieldMap.Add("priorpd",
                from item in m_resultXDoc.Descendants("PROPERTY").Attributes("PriorPropertyDisposition")
                select item.Value);

            m_fieldMap.Add("city",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("city")
                select item.Value);

            m_fieldMap.Add("state",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("state")
                select item.Value);

            m_fieldMap.Add("county",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("CountyName")
                select item.Value);

            m_fieldMap.Add("zip_cd",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("PostalCode")
                select item.Value);

            m_fieldMap.Add("suffix",
                from item in m_resultXDoc.Descendants("PROPERTY").Elements("ApartmentOrUnit")
                select item.Value);

            m_fieldMap.Add("pre",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionPrefix")
                select item.Value);

            m_fieldMap.Add("hse_no",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("HouseNumber")
                select item.Value);

            m_fieldMap.Add("street",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetName")
                select item.Value);

            m_fieldMap.Add("post",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionSuffix")
                select item.Value);

            m_fieldMap.Add("type",
                from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetSuffix")
                select item.Value);

            m_fieldMap.Add("moyr_comp",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("StructureBuiltYear")
                select item.Value);

            m_fieldMap.Add("liv_units",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("FinancedNumberOfUnits")
                select item.Value);

            m_fieldMap.Add("blk_plat_blk",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionBlk")
                select item.Value);

            m_fieldMap.Add("blk_plat_plt",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionPlt")
                select item.Value);

            m_fieldMap.Add("lot",
                from item in m_resultXDoc.Descendants("SubjectProperty").Elements("SubjectPropertyShortLegalDescriptionLot")
                select item.Value);

            m_fieldMap.Add("spc_id",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("PROJECTID")
                select item.Value);

            m_fieldMap.Add("spc_name",
                from item in m_resultXDoc.Descendants("PROJECT").Elements("ProjectName")
                select item.Value);

            m_fieldMap.Add("spc_ind",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumClassIndicator")
                select item.Value);

            m_fieldMap.Add("spc_phase",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumCooperativePhase")
                select item.Value);

            m_fieldMap.Add("spot_lot",
                from item in m_resultXDoc.Descendants("PROJECT").Attributes("FHAVAApprovedCondominiumCooperativeSpotLot")
                select item.Value);

            m_fieldMap.Add("loan_term",
                from item in m_resultXDoc.Descendants("PRODUCT").Elements("LoanAmortizationTermMonths")
                select item.Value);

        }

        public override void ApplyResultToLoan()
        {
            EnforceCanReadResult();
            SetCaivrsNums();
            SetField("notesn", m_dataLoan.sFHALenderNotes, value => m_dataLoan.sFHALenderNotes = value);
            if (this.isForExistingCase)
            {
                SetField("caseno", m_dataLoan.sAgencyCaseNum, value => m_dataLoan.sAgencyCaseNum = value);
            }
            else
            {
                SetField("caseno", m_dataLoan.sAgencyCaseNum, value => m_dataLoan.sAgencyCaseNum = value, StompingOptions.Always);
            }

            SetField("notesn", m_dataLoan.sFHALenderNotes, value => m_dataLoan.sFHALenderNotes = value);

            SetField("caseno", m_dataLoan.sAgencyCaseNum, value => m_dataLoan.sAgencyCaseNum = value);
            SetField("caseref", m_dataLoan.sLenderCaseNum, value => m_dataLoan.sLenderCaseNum = value);
            SetField("ein", m_dataLoan.sFHASponsoredOriginatorEIN, value =>
            {
                try
                {
                    m_dataLoan.sFHASponsoredOriginatorEIN = value;
                }
                catch (FieldInvalidValueException)
                {
                    Tools.LogInfo($"Unable to import sFHASponsoredOriginatorEIN. LoanId: {this.m_dataLoan.sLId}, Value: {value}");
                }
            });

            SetLoanOriginator(); // officer:nmls

            Action<string> setEstCloseD = (value) =>
                {
                    m_dataLoan.sEstCloseD_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(value);
                    m_dataLoan.sEstCloseDLckd = true;
                };
            SetField("clsg_dt", m_dataLoan.sEstCloseD_rep, setEstCloseD);
            SetField("rec_date", m_dataLoan.sCaseAssignmentD_rep, value => m_dataLoan.sCaseAssignmentD_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(value));
            SetField("adpcode", m_dataLoan.sFHAADPCode, value => m_dataLoan.sFHAADPCode = value);
            SetField("priorfha", m_dataLoan.sTotalScoreRefiT == E_sTotalScoreRefiT.LeaveBlank, value => m_dataLoan.sTotalScoreRefiT = value == "Yes" ? E_sTotalScoreRefiT.FHAToFHANonStreamline : E_sTotalScoreRefiT.ConventionalToFHA);
            SetField("prevcase", m_dataLoan.sFHAPreviousCaseNum, value => m_dataLoan.sFHAPreviousCaseNum = value);
            SetField("adp_charh", m_dataLoan.sFHAADPHousingProgramT == E_sFHAADPHousingProgramT.Unspecified, value => m_dataLoan.sFHAADPHousingProgramT = GetsFHAADPHousingProgramT(value));
            SetField("adp_chars", m_dataLoan.sFHAADPSpecialProgramT == E_sFHAADPSpecialProgramT.NoSpecialProgram, value => m_dataLoan.sFHAADPSpecialProgramT = GetsFHAADPSpecialProgramT(value));
            SetField("financing", m_dataLoan.sFHAFinT == E_sFHAFinT.NA, value => m_dataLoan.sFHAFinT = GetFHAVAFinancingCode(value));
            SetField("focode", m_dataLoan.sFHAFieldOfficeCode, value => m_dataLoan.sFHAFieldOfficeCode = value);
            SetField("insp_id", m_dataLoan.sFHAComplianceInspectionAssignmentID, value => m_dataLoan.sFHAComplianceInspectionAssignmentID = value);
            SetField("insp_type", m_dataLoan.sFHAComplianceInspectionAssignmentT == E_sFHAComplianceInspectionAssignmentT.NA, value => m_dataLoan.sFHAComplianceInspectionAssignmentT = GetInspectionType(value));
            SetField("orig", m_dataLoan.sFHALenderIdCode, value => m_dataLoan.sFHALenderIdCode = value);

            SetOriginatorData(); // orig_name, orig_city, orig_state, orig_zip

            SetField("pgmid", m_dataLoan.sFHAProgramId, value => m_dataLoan.sFHAProgramId = value);
            SetField("procsng", m_dataLoan.sFHAProcessingT == E_sFHAProcessingT.NA, value => m_dataLoan.sFHAProcessingT = GetFHAVAProcessingCode(value));
            SetField("consultant_id", m_dataLoan.sFHA203kConsultantId, value => m_dataLoan.sFHA203kConsultantId = value);
            SetField("sponsor", m_dataLoan.sFHASponsorAgentIdCode, value => m_dataLoan.sFHASponsorAgentIdCode = value);

            SetSponsorData(); // spnsr_name, spnsr_city, spnsr_state, spnsr_zip
            SetField("nt_o_pval", m_dataLoan.sOriginalAppraisedValue == 0, value => m_dataLoan.sOriginalAppraisedValue_rep = value);

            SetField("ct_name", m_dataLoan.sFHANewCaseContactNm, value => m_dataLoan.sFHANewCaseContactNm = value);
            SetField("ct_phone", m_dataLoan.sFHANewCaseContactPhone, value => m_dataLoan.sFHANewCaseContactPhone = value);

            SetField("city", m_dataLoan.sSpCity, value => m_dataLoan.sSpCity = value);

            SetField("state", m_dataLoan.sSpState, value => m_dataLoan.sSpState = value);
            SetField("zip_cd", m_dataLoan.sSpZip, value => m_dataLoan.sSpZip = value);

            //m_dataLoan.sSpCounty = GetString("county"); // Need mapping for FIPS.

            string moyr_comp = GetString("moyr_comp");
            if (String.IsNullOrEmpty(moyr_comp) == false)
            {
                DateTime PropertyCompletedDate;
                if (DateTime.TryParse( FhaConnectionUtil.ConvertDateStringFromFHAFormat(moyr_comp), out PropertyCompletedDate))
                {
                    if (m_dataLoan.sSpMonthBuiltT == E_sSpMonthBuiltT.Blank)
                    {
                        m_dataLoan.sSpMonthBuiltT = (E_sSpMonthBuiltT)PropertyCompletedDate.Month;
                    }

                    if (string.IsNullOrEmpty(m_dataLoan.sYrBuilt))
                    {
                        m_dataLoan.sYrBuilt = PropertyCompletedDate.Year.ToString();
                    }
                }
            }

            SetField("liv_units", m_dataLoan.sUnitsNum_rep, value => m_dataLoan.sUnitsNum_rep = value);

            if (string.IsNullOrEmpty(m_dataLoan.sSpBlockPlatNum))
            {
                FhaConnectionUtil.SetCombinedValues(
                    value => m_dataLoan.sSpBlockPlatNum = value,
                    GetString("blk_plat_blk"),
                    GetString("blk_plat_plt"));
            }

            SetField("lot", m_dataLoan.sSpLotNum, value => m_dataLoan.sSpLotNum = value);

            //// SetBorrowerData(); - OPM 230811: FHAC concatenates first and middle name. This is their bug, but immediate solution is to not import borrower info.

            SetsCpmProjectId();     // spc_id
            SetsProjNm();           // spc_name
            SetsFHAPUDCondoT();     // spc_ind
            SetField("spc_phase", m_dataLoan.sFHAPUDSubmissionPhase, value => m_dataLoan.sFHAPUDSubmissionPhase = value);

            SetField("spot_lot", m_dataLoan.sFHAPUDSiteCondoT == E_sFHAPUDSiteCondoT.NA, value => m_dataLoan.sFHAPUDSiteCondoT = GetsFHAPUDSiteCondoT(value));

            SetField("loan_term", m_dataLoan.sTerm_rep, value => m_dataLoan.sTerm_rep = value);


            m_dataLoan.sFHACaseNumberResultXmlContent = m_resultXml;

            m_dataLoan.Save();

            // We saved successfully, so create audit event.

            AbstractAuditItem audit =
                new FHAConnectionAuditItem(PrincipalFactory.CurrentPrincipal
                    , "New Case Number Assignment"
                    , m_resultXml
                    , m_dataLoan.sAgencyCaseNum
                    );

            AuditManager.RecordAudit(m_dataLoan.sLId, audit);
        }

        private void SetLoanOriginator()
        {
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            if (string.IsNullOrEmpty(interviewer.CompanyLoanOriginatorIdentifier))
            {
                interviewer.CompanyLoanOriginatorIdentifier = GetString("officer:nmls");
                interviewer.Update();
            }
        }

        private void SetOriginatorData()
        {
            bool isUpdate = false;

            IPreparerFields field = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.CreateNew);
            string originatorOriginalName = field.CompanyName;

            string companyName = GetString("orig_name");
            if (!string.IsNullOrEmpty(companyName) && string.IsNullOrEmpty(field.CompanyName))
            {
                field.CompanyName = companyName;
                isUpdate = true;
            }


            string city = GetString("orig_city");
            string state = GetString("orig_state");
            string zip = GetString("orig_zip");
            if (string.IsNullOrEmpty(companyName) == false
                && string.IsNullOrEmpty(companyName) == false
                && string.IsNullOrEmpty(companyName) == false)
            {
                if (string.IsNullOrEmpty(field.City))
                {
                    field.City = city;
                    isUpdate = true;
                }

                if (string.IsNullOrEmpty(field.State))
                {
                    field.State = state;
                    isUpdate = true;
                }

                if (string.IsNullOrEmpty(field.Zip))
                {
                    field.Zip = zip;
                    isUpdate = true;
                }
            }

            if (isUpdate)
            {
                field.Update();
            }
        }

        private void SetSponsorData()
        {
            bool isUpdate = false;

            IPreparerFields field = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.CreateNew);
            string originatorOriginalName = field.CompanyName;

            string companyName = GetString("spnsr_name");
            if (!string.IsNullOrEmpty(companyName) && string.IsNullOrEmpty(field.CompanyName))
            {
                field.CompanyName = companyName;
                isUpdate = true;
            }

            string city = GetString("spnsr_city");
            string state = GetString("spnsr_state");
            string zip = GetString("spnsr_zip");
            if (string.IsNullOrEmpty(companyName) == false
                && string.IsNullOrEmpty(companyName) == false
                && string.IsNullOrEmpty(companyName) == false)
            {
                if (string.IsNullOrEmpty(field.City))
                {
                    field.City = city;
                    isUpdate = true;
                }

                if (string.IsNullOrEmpty(field.City))
                {
                field.State = state;
                    isUpdate = true;
                }

                if (string.IsNullOrEmpty(field.Zip))
                {
                    field.Zip = zip;
                    isUpdate = true;
                }
            }

            if (isUpdate)
            {
                field.Update();
            }
        }

        private void SetsCpmProjectId()
        {
            string sCpmProjectId = m_dataLoan.sCpmProjectId;
            string spc_id = GetString("spc_id");

            if (!string.IsNullOrEmpty(spc_id) && string.IsNullOrEmpty(sCpmProjectId))
            {

                if (sCpmProjectId.Length > 9 && spc_id.Length > 9 && spc_id.Substring(0, 9) == sCpmProjectId.Substring(0, 9))
                {
                    // First 9 characters match means do not overwrite
                }
                else
                {
                    m_dataLoan.sCpmProjectId = spc_id;
                }
            }
        }

        private void SetsProjNm()
        {
            string sProjNm = m_dataLoan.sProjNm;
            string spc_name = GetString("spc_name");

            if (!string.IsNullOrEmpty(spc_name) && string.IsNullOrEmpty(sProjNm))
            {
                if (sProjNm.Length > 30 && spc_name.Length > 30 && spc_name.Substring(0, 30) == sProjNm.Substring(0, 30))
                {
                    // First 30 characters match means do not overwrite
                }
                else
                {
                    m_dataLoan.sProjNm = spc_name;
                }
            }
        }

        private void SetsFHAPUDCondoT()
        {
            string spc_ind = GetString("spc_ind");

            if (!string.IsNullOrEmpty(spc_ind))
            {
                if (m_dataLoan.sFHAPUDCondoT == E_sFHAPUDCondoT.NA)
                {
                    m_dataLoan.sFHAPUDCondoT = GetFHAVAApprovedCondominiumClassIndicator(spc_ind);
                }

                if (m_dataLoan.sGseSpT == E_sGseSpT.LeaveBlank)
                {
                    E_sFHAPUDCondoT result = m_dataLoan.sFHAPUDCondoT;
                    if (result == E_sFHAPUDCondoT.PUD)
                    {
                        m_dataLoan.sGseSpT = E_sGseSpT.PUD;
                    }
                    else if (result == E_sFHAPUDCondoT.Condo)
                    {
                        m_dataLoan.sGseSpT = E_sGseSpT.Condominium;
                    }
                }
            }
        }

    }
}