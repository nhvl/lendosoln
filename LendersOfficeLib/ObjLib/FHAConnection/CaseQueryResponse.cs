﻿using System.Linq;
using DataAccess;
using System.Xml.Linq;
using LendersOffice.ObjLib.Audit;
using LendersOffice.Audit;
using LendersOffice.Security;
using System;

namespace LendersOffice.ObjLib.FHAConnection
{
    public class CaseQueryResponse : AbstractFHAConnectionResponse
    {
        public CaseQueryResponse(string result, CPageData dataLoan)
        : base(result, dataLoan)
        {
            BuildFieldList();

            this.LogFieldDump();
        }

        private void BuildFieldList()
        {
            if (CanReadResult == false) return;

            m_fieldMap.Add("umipf",
        from item in m_resultXDoc.Descendants("FHAVAMortgageInsurancePremium").Attributes("UpfrontMIPFactor")
        select item.Value);

            m_fieldMap.Add("amipf",
        from item in m_resultXDoc.Descendants("FHAVAMortgageInsurancePremium").Attributes("AnnualMIPFactor")
        select item.Value);

            m_fieldMap.Add("caseno",
        from item in m_resultXDoc.Descendants("APPLICATION").Elements("AgencyCaseIdentifier")
        select item.Value);

            m_fieldMap.Add("clsg_date",
        from item in m_resultXDoc.Descendants("UNDERWRITINGCASE").Elements("LoanScheduledClosingDate")
        select item.Value);

            m_fieldMap.Add("adp_code",
        from item in m_resultXDoc.Descendants("FHAVA").Attributes("FHAVAADPCode")
        select item.Value);

            m_fieldMap.Add("ctcd",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("CaseTypeCD")
        select item.Value);

            m_fieldMap.Add("recdt",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("CaseReceivedDate")
        select item.Value);

            m_fieldMap.Add("orig",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAOriginatorIdentifier")
        select item.Value);

            m_fieldMap.Add("tpo_ein",
        from item in m_resultXDoc.Descendants("LOAN_ORIGINATOR").Attributes("_EmployerIdentificationNumber")
        select item.Value);

            m_fieldMap.Add("spon",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVASponsorIdentifier")
        select item.Value);

            m_fieldMap.Add("prev_case_no",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVAPreviousCaseNumber")
        select item.Value);

            m_fieldMap.Add("sccase",
        from item in m_resultXDoc.Descendants("FHAVA").Elements("FHAVATotalScorecardCase")
        select item.Value);

            m_fieldMap.Add("city",
        from item in m_resultXDoc.Descendants("PROPERTY").Elements("city")
        select item.Value);

            m_fieldMap.Add("state",
        from item in m_resultXDoc.Descendants("PROPERTY").Elements("state")
        select item.Value);

            m_fieldMap.Add("PostalCode",
        from item in m_resultXDoc.Descendants("PROPERTY").Elements("PostalCode")
        select item.Value);

            m_fieldMap.Add("CountyCode",
        from item in m_resultXDoc.Descendants("PROPERTY").Elements("CountyCode")
        select item.Value);

            m_fieldMap.Add("HouseNumber",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("HouseNumber")
        select item.Value);

            m_fieldMap.Add("DirectionPrefix",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionPrefix")
        select item.Value);

            m_fieldMap.Add("StreetName",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetName")
        select item.Value);

            m_fieldMap.Add("StreetSuffix",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("StreetSuffix")
        select item.Value);

            m_fieldMap.Add("DirectionSuffix",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("DirectionSuffix")
        select item.Value);

            m_fieldMap.Add("ApartmentOrUnit",
        from item in m_resultXDoc.Descendants("PARSEDSTREETNAME").Elements("ApartmentOrUnit")
        select item.Value);

            m_fieldMap.Add("SubjectPropertyAppraisedValueAmount",
        from item in m_resultXDoc.Descendants("SUBJECTPROPERTY").Elements("SubjectPropertyAppraisedValueAmount")
        select item.Value);

            m_fieldMap.Add("SubjectPropertyShortLegalDescriptionBlk",
        from item in m_resultXDoc.Descendants("SUBJECTPROPERTY").Elements("SubjectPropertyShortLegalDescriptionBlk")
        select item.Value);

            m_fieldMap.Add("SubjectPropertyShortLegalDescriptionPlt",
        from item in m_resultXDoc.Descendants("SUBJECTPROPERTY").Elements("SubjectPropertyShortLegalDescriptionPlt")
        select item.Value);

            m_fieldMap.Add("SubjectPropertyShortLegalDescriptionLot",
        from item in m_resultXDoc.Descendants("SUBJECTPROPERTY").Elements("SubjectPropertyShortLegalDescriptionLot")
        select item.Value);

            m_fieldMap.Add("MortgageInsuranceOriginalAppraisalAmount",
        from item in m_resultXDoc.Descendants("MORTGAGEINSURANCE").Elements("MortgageInsuranceOriginalAppraisalAmount")
        select item.Value);

            m_fieldMap.Add("LoanAmortizationTermMonths",
        from item in m_resultXDoc.Descendants("PRODUCT").Elements("LoanAmortizationTermMonths")
        select item.Value);

            m_fieldMap.Add("FHAVAStreamlinedReFinanceAuthorizationNumber",
        from item in m_resultXDoc.Descendants("FHAVAStreamlinedReFinance").Attributes("FHAVAStreamlinedReFinanceAuthorizationNumber")
        select item.Value);

        }

        // don't set the field value if it's 0 or blank for CaseQueryResponses 125838
        protected override void SetField(string field, bool isEmpty, Action<string> setValue, StompingOptions stompOption = StompingOptions.OnlyIfEmpty)
        {
            #region ( Conditions to Not Set It )
            string result;
            // skip unknown fieldnames
            if (!TryGetString(field, out result))
            {
                return;
            }

            // skip blank values
            if (string.IsNullOrEmpty(result))
            {
                return;
            }

            // skip zero values
            decimal resultingDecimal;
            if (decimal.TryParse(result, out resultingDecimal))
            {
                if (resultingDecimal == 0)
                {
                    return;
                }
            }
            #endregion

            if (stompOption == StompingOptions.Always ||
                (stompOption == StompingOptions.OnlyIfEmpty && isEmpty))
            {
                setValue(result);
            }
        }

        public override void ApplyResultToLoan()
        {
            SetCaivrsNums();
            SetField("umipf", m_dataLoan.sFfUfmipR == 0, value => m_dataLoan.sFfUfmipR_rep = value);
            SetField("amipf", m_dataLoan.sProMInsR == 0, value => m_dataLoan.sProMInsR_rep = value);
            SetField("caseno", m_dataLoan.sAgencyCaseNum, value => m_dataLoan.sAgencyCaseNum = value);
            Action<string> setEstCloseD = (value) =>
                {
                    m_dataLoan.sEstCloseD_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(value);
                    m_dataLoan.sEstCloseDLckd = true;
                };
            SetField("clsg_date", m_dataLoan.sEstCloseD_rep, setEstCloseD);
            SetField("adp_code", m_dataLoan.sFHAADPCode, value => m_dataLoan.sFHAADPCode = value);
            SetField("orig", m_dataLoan.sFHALenderIdCode, value => m_dataLoan.sFHALenderIdCode = value);
            SetField("tpo_ein", m_dataLoan.sFHASponsoredOriginatorEIN, value =>
            {
                try
                {
                    m_dataLoan.sFHASponsoredOriginatorEIN = value;
                }
                catch (FieldInvalidValueException)
                {
                    Tools.LogInfo($"Unable to import sFHASponsoredOriginatorEIN. LoanId: {this.m_dataLoan.sLId}, Value: {value}");
                }
            });
            SetField("spon", m_dataLoan.sFHASponsorAgentIdCode, value => m_dataLoan.sFHASponsorAgentIdCode = value);
            SetField("prev_case_no", m_dataLoan.sFHAPreviousCaseNum, value => m_dataLoan.sFHAPreviousCaseNum = value);
            SetField("sccase", m_dataLoan.sFHAScoreByTotalTri == E_TriState.Blank, value => m_dataLoan.sFHAScoreByTotalTri = value == "Yes" ? E_TriState.Yes : E_TriState.No);
            SetField("city", m_dataLoan.sSpCity, value => m_dataLoan.sSpCity = value);
            SetField("state", m_dataLoan.sSpState, value => m_dataLoan.sSpState = value);
            SetField("PostalCode", m_dataLoan.sSpZip, value => m_dataLoan.sSpZip = value);
            SetField("recdt", m_dataLoan.sCaseAssignmentD_rep, value => m_dataLoan.sCaseAssignmentD_rep = FhaConnectionUtil.ConvertDateStringFromFHAFormat(value));

            SetField("SubjectPropertyAppraisedValueAmount", m_dataLoan.sApprVal == 0, value => m_dataLoan.sApprVal_rep = value);

            if (string.IsNullOrEmpty(m_dataLoan.sSpBlockPlatNum))
            {
            FhaConnectionUtil.SetCombinedValues(
                value => m_dataLoan.sSpBlockPlatNum = value,
                GetString("SubjectPropertyShortLegalDescriptionBlk"),
                GetString("SubjectPropertyShortLegalDescriptionPlt"));
            }

            SetField("SubjectPropertyShortLegalDescriptionLot", m_dataLoan.sSpLotNum, value => m_dataLoan.sSpLotNum = value);
            SetField("MortgageInsuranceOriginalAppraisalAmount", m_dataLoan.sOriginalAppraisedValue == 0, value => m_dataLoan.sOriginalAppraisedValue_rep = value);

            //m_dataLoan.sSpCounty = GetString("CountyCode"); // Need to be able to set county by FIPS.

            SetField("LoanAmortizationTermMonths", m_dataLoan.sTerm == 0, value => m_dataLoan.sTerm_rep = value);
            SetField("FHAVAStreamlinedReFinanceAuthorizationNumber", m_dataLoan.sFHARefiAuthNum, value => m_dataLoan.sFHARefiAuthNum = value);

            m_dataLoan.sFHACaseQueryResultXmlContent = m_resultXml;

            m_dataLoan.Save();

            // We saved successfully, so create audit event.

            AbstractAuditItem audit =
                new FHAConnectionAuditItem (PrincipalFactory.CurrentPrincipal
                    , "Case Query"
                    , m_resultXml
                    , m_dataLoan.sAgencyCaseNum
                    );

            AuditManager.RecordAudit(m_dataLoan.sLId, audit);

        }
    }
}