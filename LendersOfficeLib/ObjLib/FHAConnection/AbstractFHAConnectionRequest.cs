﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Net;
using System.IO;
using LendersOffice.Constants;

namespace LendersOffice.ObjLib.FHAConnection
{
    abstract public class AbstractFHAConnectionRequest
    {
        protected string m_requestXml = string.Empty;
        protected CPageData m_dataLoan;
        protected IEnumerable<BorrowerInfo> m_borrowerList;
        protected abstract string ProductionURL { get; }
        protected abstract string PilotURL { get; }
        protected abstract Type ResponseType { get; }
        protected const string BROWSER_STRING = "lendingQB";

        abstract protected void LoadData();
        abstract public AbstractFHAConnectionResponse Submit(string userNm, string password);

        public AbstractFHAConnectionRequest(Guid sLId)
        {
            List<string> fields = CPageData.GetCPageBaseAndCAppDataDependencyList(this.GetType()).ToList();
            fields.AddRange( CPageData.GetCPageBaseAndCAppDataDependencyList( ResponseType ));
            
            // The fields needed for BorrowerInfo loading.
            fields.AddRange(new string[] { "aBSsn", "aBFirstNm", "aBMidNm", "aBLastNm", "aCIsDefined", "aCSsn", "aCFirstNm", "aCMidNm", "aCLastNm", "aBSuffix", "aCSuffix", "aBDob", "aCDob", "aFHABCaivrsNum", "aFHACCaivrsNum" });

            CPageData dataLoan = new CPageData(sLId, "AbstractFHAConnectionRequest", fields);
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            m_dataLoan = dataLoan;
            LoadBorrowers();
            LoadData();
        }

        protected string SubmitToFha(string userNm, string password)
        {
            string result = string.Empty;

            WebRequest request = null;
            try
            {

                string url = ConstAppDavid.CurrentServerLocation == ServerLocation.Production ? ProductionURL : PilotURL;

                request = WebRequest.Create(url);

                request.Timeout = 30000; //ms
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Credentials = new NetworkCredential(userNm, password);

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write("inputData=" + m_requestXml);
                writer.Close();

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    const int BUFFER_SIZE = 50000;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int n = 0;

                    StringBuilder sb = new StringBuilder();

                    using (Stream stream = response.GetResponseStream())
                    {
                        while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
                        {
                            sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
                        }
                    }

                    string responseStr = sb.ToString();

                    Tools.LogInfo(String.Format(
                        "FHA CONNECTION REQUEST: {1}{0}URL:{4}{0}REQUEST:{0}{2}{0}RESPONSE:{0}{3}"
                        , Environment.NewLine + Environment.NewLine
                        , this.GetType().Name
                        , m_requestXml
                        , responseStr
                        , url
                        ));

                    return responseStr;
                }
            }
            catch (WebException exc)
            {
                Tools.LogError("Fail to FHAC B2G. " + Environment.NewLine + "REQUEST: " + m_requestXml, exc);
                
                if ( exc.Status == WebExceptionStatus.ProtocolError)
                {
                    HttpWebResponse response = exc.Response as HttpWebResponse;
                    if ( response != null && ( response.StatusCode == HttpStatusCode.Unauthorized ))
                    {
                        // User name and password was not accepted by FHAC
                        return GetErrorResponse(new string[] {"Authentication Failed."}.ToList());
                    }
                }

                #region // TEMP Debug logging for OPM 91478
                // The above code sometimes gets skipped, even though it is a 401 (Unauthorized) response.
                // Logging more information here to see if we can find out why.

                string debugLog = "FHAConnection WebException" + Environment.NewLine;
                debugLog += "exc.Status: " + exc.Status + Environment.NewLine;
                debugLog += "exc.Message: " + exc.Message + Environment.NewLine;
                
                HttpWebResponse debugResponse = exc.Response as HttpWebResponse;
                if (debugResponse != null)
                {
                    debugLog += "response.StatusCode: " + debugResponse.StatusCode + Environment.NewLine;
                    debugLog += "response.StatusDescription: " + debugResponse.StatusDescription + Environment.NewLine;
                }
                else
                {
                    debugLog += "Null response object" + Environment.NewLine;
                }
                Tools.LogError(debugLog, exc);
                #endregion


                // Per spec, for any other network issue, display: FHA Connection Is Down.
                return GetErrorResponse(new string[] { "FHA Connection Is Down." }.ToList());
            }
            finally
            {
                if (request != null) request.GetRequestStream().Close();
            }
        }

        private string GetErrorResponse(List<string> errors)
        {
            // Reconsider this design.
            string resultString = @"<?xml version=""1.0"" standalone=""yes""?><MORTGAGEDATA MISMOVersionID=""1.0.1""><PROCESSSTATUS><ProcessStatusCode>Error</ProcessStatusCode>";
            errors.ForEach(str => resultString += "<ProcessStatusMessage>" + str + "</ProcessStatusMessage>");
            resultString += "</PROCESSSTATUS></MORTGAGEDATA>";

            return resultString;
        }

        public override string ToString()
        {
            return m_requestXml;
        }

        #region Load Helpers
        private void LoadBorrowers()
        {
            List<BorrowerInfo> borrList = new List<BorrowerInfo>();
            int borrowerCount = 1;
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                CAppData currentApp = m_dataLoan.GetAppData(i);
                borrList.Add(new BorrowerInfo() 
                { 
                    BorrowerNum = borrowerCount++, 
                    Ssn = currentApp.aBSsn.Replace("-",""),
                    FirstName = currentApp.aBFirstNm,
                    MiddleName = currentApp.aBMidNm,
                    LastName = currentApp.aBLastNm,
                    Suffix = currentApp.aBSuffix,
                    DOB = currentApp.aBDob_rep
                });

                if (borrowerCount > 5) break; // FHAC takes max 5 borrowers.

                if (currentApp.aCIsDefined)
                {
                    borrList.Add(new BorrowerInfo()
                    {
                        BorrowerNum = borrowerCount++,
                        Ssn = currentApp.aCSsn.Replace("-", ""),
                        FirstName = currentApp.aCFirstNm,
                        MiddleName = currentApp.aCMidNm,
                        LastName = currentApp.aCLastNm,
                        Suffix = currentApp.aCSuffix,
                        DOB = currentApp.aCDob_rep
                    });
                    
                    if (borrowerCount > 5) break;
                }
                
            }
            m_borrowerList = borrList;
        }

        // POD for cycling through borrowers
        protected class BorrowerInfo
        {
            public int BorrowerNum;
            public string Ssn;
            public string FirstName;
            public string MiddleName;
            public string LastName;
            public string Suffix;
            public string DOB;
        }
        #endregion

    }
}
