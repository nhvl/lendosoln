﻿namespace LendersOffice.ObjLib.FHAConnection
{
    using System;
    using System.Xml.Linq;
    using LendersOffice.Constants;

    public class CaseQueryRequest : AbstractFHAConnectionRequest
    {
        protected override string PilotURL
        {
            get { return ConstStage.FHAConnectionPilotDomain + "/b2b/chums/f17cqxml.cfm"; }
        }
        protected override string ProductionURL
        {
            get { return  ConstStage.FHAConnectionProductionDomain + "/b2b/chums/f17cqxml.cfm"; }
        }
        protected override Type ResponseType
        {
            get { return typeof(CaseQueryResponse); }
        }

        public CaseQueryRequest(Guid sLId)
            : base(sLId)
        {
        }

        public override AbstractFHAConnectionResponse Submit(string userNm, string password)
        {
            string result = SubmitToFha(userNm, password);

            CaseQueryResponse response = new CaseQueryResponse(result, m_dataLoan);

            return response;
        }

        protected override void LoadData()
        {
            XDocument requestDoc = new XDocument(
                new XElement("MORTGAGEDATA",
                    new XAttribute("MISMOVersionID", "1.0.1"),
                    new XElement("PROCESSSTATUS",
                        new XElement("ProcessStatusCode", "Query"), // mode
                        new XElement("ProcessStatusRequestor", BROWSER_STRING)), // browser
                    new XElement("APPLICATION",
                        new XElement("AgencyCaseIdentifier", m_dataLoan.sAgencyCaseNum), // caseno
                        new XElement("UNDERWRITINGCASE",
                            new XElement("FHAVA",
                                new XElement("FHAVAFieldOfficeCode", m_dataLoan.sFHAFieldOfficeCode ) //focode
                        )))));

            m_requestXml = "<?xml version=\"1.0\" standalone=\"yes\"?>" // XDeclaration does not let you do this.
                + requestDoc.ToString();
        }

    }

}