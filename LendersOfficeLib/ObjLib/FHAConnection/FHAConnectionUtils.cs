﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;

namespace LendersOffice.ObjLib.FHAConnection
{
    public class FHAFieldOffice
    {
        public string Code;
        public string City;
        public string State;
    }

    public class FHAProgram
    {
        public string Id {get; set;}
        public string Name { get; set; }
    }

    public class FhaConnectionUtil
    {
        #region Data Helpers
        /// <summary>
        /// Convert from YYYYMMDD to MM/DD/YYYY. Returns blank if fail.
        /// </summary>
        public static string ConvertDateStringFromFHAFormat(string fhaDate)
        {
            if (fhaDate.Length == 8)
            {
                string newDateStr = fhaDate.Substring(4, 2) + "/" + fhaDate.Substring(6, 2) + "/" + fhaDate.Substring(0, 4);
                DateTime newDate;
                if (DateTime.TryParse(newDateStr, out newDate))
                {
                    return newDateStr;
                }
            }

            return string.Empty;
        }


        private static void SetCombinedValues(Action<string> setValue, Func<string, bool> shouldSetValue, string delimiter, params string[] str)
        {
            string result = string.Join(delimiter, str.Where(p => !string.IsNullOrEmpty(p)));

            if (!string.IsNullOrEmpty(result) && shouldSetValue(result))
            {
                setValue(result);
            }
        }

        public static void SetByConditionCombinedValuesSpaced(Action<string> setValue, Func<string, bool> shouldSetValue, params string[] str)
        {
            SetCombinedValues(setValue, shouldSetValue, " ", str);
        }

        public static void SetCombinedValues(Action<string> setValue, params string[] str)
        {
            SetCombinedValues(setValue, v => true, "", str);
        }

        #endregion

        // This list rarely changes, so hardcode.
        public static readonly List<FHAFieldOffice> FhaFieldOfficeList = new List<FHAFieldOffice>(
            new FHAFieldOffice[] {
                 new FHAFieldOffice() { Code="0202", City="Albany", State="NY" }
                , new FHAFieldOffice() { Code="0602", City="Albuquerque", State="NM" }
                , new FHAFieldOffice() { Code="1006", City="Anchorage", State="AK" }
                , new FHAFieldOffice() { Code="0406", City="Atlanta", State="GA" }
                , new FHAFieldOffice() { Code="0306", City="Baltimore", State="MD" }
                , new FHAFieldOffice() { Code="0102", City="Bangor", State="ME" }
                , new FHAFieldOffice() { Code="0409", City="Birmingham", State="AL" }
                , new FHAFieldOffice() { Code="1008", City="Boise", State="ID" }
                , new FHAFieldOffice() { Code="0106", City="Boston", State="MA" }
                , new FHAFieldOffice() { Code="0206", City="Buffalo ", State="NY" }
                , new FHAFieldOffice() { Code="0110", City="Burlington", State="VT" }
                , new FHAFieldOffice() { Code="0216", City="Camden", State="NJ" }
                , new FHAFieldOffice() { Code="0805", City="Casper", State="WY" }
                , new FHAFieldOffice() { Code="0315", City="Charleston", State="WV" }
                , new FHAFieldOffice() { Code="0506", City="Chicago", State="IL" }
                , new FHAFieldOffice() { Code="0510", City="Cincinnati", State="OH" }
                , new FHAFieldOffice() { Code="0512", City="Cleveland", State="OH" }
                , new FHAFieldOffice() { Code="0416", City="Columbia", State="SC" }
                , new FHAFieldOffice() { Code="0516", City="Columbus", State="OH" }
                , new FHAFieldOffice() { Code="0616", City="Dallas", State="TX" }
                , new FHAFieldOffice() { Code="0806", City="Denver", State="CO" }
                , new FHAFieldOffice() { Code="0705", City="Des Moines", State="IA" }
                , new FHAFieldOffice() { Code="0528", City="Detroit", State="MI" }
                , new FHAFieldOffice() { Code="0815", City="Fargo", State="ND" }
                , new FHAFieldOffice() { Code="0544", City="Flint", State="MI" }
                , new FHAFieldOffice() { Code="0905", City="Fresno", State="CA" }
                , new FHAFieldOffice() { Code="0621", City="Ft. Worth", State="TX" }
                , new FHAFieldOffice() { Code="0533", City="Grand Rapids", State="MI" }
                , new FHAFieldOffice() { Code="0419", City="Greensboro", State="NC" }
                , new FHAFieldOffice() { Code="0126", City="Hartford", State="CT" }
                , new FHAFieldOffice() { Code="0820", City="Helena", State="MT" }
                , new FHAFieldOffice() { Code="0908", City="Honolulu", State="HI" }
                , new FHAFieldOffice() { Code="0624", City="Houston", State="TX" }
                , new FHAFieldOffice() { Code="0536", City="Indianapolis", State="IN" }
                , new FHAFieldOffice() { Code="0426", City="Jackson", State="MS" }
                , new FHAFieldOffice() { Code="0429", City="Jacksonville", State="FL" }
                , new FHAFieldOffice() { Code="0716", City="KansasCity", State="KS" }
                , new FHAFieldOffice() { Code="0437", City="Knoxville", State="TN" }
                , new FHAFieldOffice() { Code="0944", City="Las Vegas", State="NV" }
                , new FHAFieldOffice() { Code="0637", City="Little Rock", State="AR" }
                , new FHAFieldOffice() { Code="0916", City="Los Angeles", State="CA" }
                , new FHAFieldOffice() { Code="0436", City="Louisville", State="KY" }
                , new FHAFieldOffice() { Code="0641", City="Lubbock", State="TX" }
                , new FHAFieldOffice() { Code="0136", City="Manchester", State="NH" }
                , new FHAFieldOffice() { Code="0440", City="Memphis", State="TN" }
                , new FHAFieldOffice() { Code="0414", City="Miami", State="FL" }
                , new FHAFieldOffice() { Code="0539", City="Milwaukee", State="WI" }
                , new FHAFieldOffice() { Code="0546", City="Minn.St.Paul", State="MN" }
                , new FHAFieldOffice() { Code="0443", City="Nashville", State="TN" }
                , new FHAFieldOffice() { Code="0236", City="New York", State="NY" }
                , new FHAFieldOffice() { Code="0648", City="NewOrleans", State="LA" }
                , new FHAFieldOffice() { Code="0239", City="Newark", State="NJ" }
                , new FHAFieldOffice() { Code="0656", City="OklahomaCity", State="OK" }
                , new FHAFieldOffice() { Code="0726", City="Omaha", State="NE" }
                , new FHAFieldOffice() { Code="0444", City="Orlando", State="FL" }
                , new FHAFieldOffice() { Code="0326", City="Philadelphia", State="PA" }
                , new FHAFieldOffice() { Code="0920", City="Phoenix", State="AZ" }
                , new FHAFieldOffice() { Code="0328", City="Pittsburgh", State="PA" }
                , new FHAFieldOffice() { Code="1016", City="Portland", State="OR" }
                , new FHAFieldOffice() { Code="0143", City="Providence", State="RI" }
                , new FHAFieldOffice() { Code="0925", City="Reno", State="NV" }
                , new FHAFieldOffice() { Code="0336", City="Richmond", State="VA" }
                , new FHAFieldOffice() { Code="0930", City="Sacramento", State="CA" }
                , new FHAFieldOffice() { Code="0830", City="Salt Lake City", State="UT" }
                , new FHAFieldOffice() { Code="0659", City="San Antonio", State="TX" }
                , new FHAFieldOffice() { Code="0933", City="San Diego", State="CA" }
                , new FHAFieldOffice() { Code="0939", City="San Francisco", State="CA" }
                , new FHAFieldOffice() { Code="0446", City="San Juan", State="PR" }
                , new FHAFieldOffice() { Code="0943", City="Santa Ana", State="CA" }
                , new FHAFieldOffice() { Code="1019", City="Seattle", State="WA" }
                , new FHAFieldOffice() { Code="0662", City="Shreveport", State="LA" }
                , new FHAFieldOffice() { Code="0835", City="Sioux Falls", State="SD" }
                , new FHAFieldOffice() { Code="1025", City="Spokane", State="WA" }
                , new FHAFieldOffice() { Code="0550", City="Springfield", State="IL" }
                , new FHAFieldOffice() { Code="0736", City="St. Louis", State="MO" }
                , new FHAFieldOffice() { Code="0450", City="Tampa", State="FL" }
                , new FHAFieldOffice() { Code="0740", City="Topeka", State="KS" }
                , new FHAFieldOffice() { Code="0945", City="Tucson", State="AZ" }
                , new FHAFieldOffice() { Code="0670", City="Tulsa", State="OK" }
                , new FHAFieldOffice() { Code="0339", City="Washington", State="DC" }
                , new FHAFieldOffice() { Code="0344", City="Wilmington", State="DE" }
            });

        // This list rarely changes, so hardcode.
        public static readonly List<FHAProgram> FhaProgramList = new List<FHAProgram>(
            new FHAProgram[] {
                 new FHAProgram() { Id="00", Name="(00)-Default" }
                , new FHAProgram() { Id="36", Name="(36)-Construction/Permanent Properties" }
                , new FHAProgram() { Id="02", Name="(02)-Disaster Housing" }
                , new FHAProgram() { Id="04", Name="(04)-Farm homes on more than 5 acres" }
                , new FHAProgram() { Id="01", Name="(01)-Housing for the Elderly" }
                , new FHAProgram() { Id="10", Name="(10)-Housing for Veterans" }
                , new FHAProgram() { Id="32", Name="(32)-Housing in older declining areas" }
                , new FHAProgram() { Id="05", Name="(05)-HUD Employee Loans" }
                , new FHAProgram() { Id="46", Name="(46)-Indian Claim Area" }
                , new FHAProgram() { Id="52", Name="(52)-Interest Buy-down provision" }
                , new FHAProgram() { Id="03", Name="(03)-Low Cost Housing in suburbs" }
                , new FHAProgram() { Id="77", Name="(77)-Manufactured Homes" }
                , new FHAProgram() { Id="40", Name="(40)-REO Sale - rehab - Others" }
                , new FHAProgram() { Id="41", Name="(41)-REO Sale - rehab - Urban Renewal" }
                , new FHAProgram() { Id="31", Name="(31)-Resale of conv. financed - non-Veteran" }
                , new FHAProgram() { Id="30", Name="(30)-Resale of conv. financed - Veteran" }
                , new FHAProgram() { Id="07", Name="(07)-Section 223(a) Mortgages" }
                , new FHAProgram() { Id="88", Name="(88)-Section 8 Subsidy" }
                , new FHAProgram() { Id="65", Name="(65)-Shared Equity Mortgages" }
                , new FHAProgram() { Id="51", Name="(51)-Solar Dwelling for non-Veteran" }
                , new FHAProgram() { Id="50", Name="(50)-Solar Dwelling for Veteran" }
                , new FHAProgram() { Id="13", Name="(13)-Special Veteran Previsions" }
                , new FHAProgram() { Id="53", Name="(53)-State Purchase Mortgages - REO Sale" }
                , new FHAProgram() { Id="60", Name="(60)-Subject to second trust" }
                , new FHAProgram() { Id="08", Name="(08)-Urban Renewal Area Mortgages" }
                , new FHAProgram() { Id="14", Name="(14)-Veteran in urban renewal area" }
            });


    }
}