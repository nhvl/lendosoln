﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:key name="PARTYID" match="MULTICASEDATA/CASEDATA/PARTY" use="@PARTYID" />

<xsl:template match="MULTICASEDATA">
    <html>
        <head id="Head1">
            <title>FHA Connection Results Viewer</title>
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
            <style type="text/css">
                .FieldLabel { font-style: italic; }
                .FieldValue { font-weight: bold; }
                .overLabel { display: block; }
                * { vertical-align: top; }
                .BorderTable { border: 1px solid lightgrey; padding: 0 5px 0 5px; margin: 1em 0 1em 0; }
                .SectionTitle { text-align: center; }
                #StatusCode { padding-left: 2em; }
            </style>
        </head>
        <body>
            <div id="CaseNumberAssignmentTab">
                <h3>Case Number Assignment</h3>

                <div>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="LQB_DATA/ImageFileName" />
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            FHA Connection Logo
                        </xsl:attribute>
                    </img>
                </div>

                <h3>
                    Case Number Assignment Results <xsl:value-of select="LQB_DATA/TEST" />
                </h3>

                <h4 id="StatusCode">
                    <xsl:choose>
                        <xsl:when test="PROCESSSTATUS/ProcessStatusCode = 'SuccessUpdatesAllowed' or PROCESSSTATUS/ProcessStatusCode = 'SuccessNoUpdateAllowed' or PROCESSSTATUS/ProcessStatusCode = 'SuccessNoUpdateAllowed' " >
                            <span class="FieldValue">Success</span>
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="FieldValue">
                                <xsl:value-of select="PROCESSSTATUS/ProcessStatusCode"/>
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                </h4>
                <div id="ProcessStatusMessages">
                    <span class="FieldValue">
                        <xsl:value-of select="PROCESSSTATUS/ProcessStatusMessage[1]"/>
                    </span>
                    <br />
                    <span class="FieldValue">
                        <xsl:value-of select="CASEDATA/FHAVACaseMessage"/>
                    </span>
                    <br />
                    <xsl:for-each select="PROCESSSTATUS/ProcessStatusMessage[position() != 1]">
                        <xsl:value-of select="." />
                        <br />
                    </xsl:for-each>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * General Information *
                    </h3>

                    <xsl:if test="CASEDATA/FHAVACaseMessage/@FHAVAStatusDate != '' ">
                        <label class="FieldLabel">Request status date: </label>
                        <span class="FieldValue">
                            <span class="FieldValue">
                                <xsl:call-template name="EmptyCheck">
                                    <xsl:with-param name="Selection" select="CASEDATA/FHAVACaseMessage/@FHAVAStatusDate" />
                                    <xsl:with-param name="Type" select=" 'Date' " />
                                </xsl:call-template>
                            </span>
                        </span>
                    </xsl:if>

                    <br />

                    <br />

                    <label class="FieldLabel">FHA Case Number: </label>
                    <span class="FieldValue">
                        <xsl:call-template name="EmptyCheck">
                            <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/AgencyCaseIdentifier" />
                        </xsl:call-template>
                    </span>

                    <br />
                    <br />

                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Field Office: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAFieldOfficeCode" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Lender Case Ref: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/LenderCaseIdentifier" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Is this a Sponsored Originator Case? </label>
                                <xsl:choose>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber != '' ">
                                        <span class="FieldValue">Yes</span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span class="FieldValue">No</span>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>

                            <td colspan="2">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <xsl:choose>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber != '' ">
                                        <xsl:call-template name="OriginatorIDAndAddress">
                                            <xsl:with-param name="OriginatorIDLabel" select="'Sponsored Originator EIN:'" />
                                            <xsl:with-param name="OriginatorID" select="CASEDATA/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber" />
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAOriginatorIdentifier/@_NationwideMortgageLicensingSystem != '' ">
                                        <xsl:call-template name="OriginatorIDAndAddress">
                                            <xsl:with-param name="OriginatorIDLabel" select="'Originator ID:'" />
                                            <xsl:with-param name="OriginatorID" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAOriginatorIdentifier" />
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="OriginatorIDAndAddress">
                                            <xsl:with-param name="OriginatorIDLabel" select="'Originator ID:'" />
                                            <xsl:with-param name="OriginatorID" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier" />
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <br />
                                <label class="FieldLabel">NMLS ID: </label>
                                <xsl:choose>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber != '' ">
                                        <span class="FieldValue">
                                            <xsl:call-template name="EmptyCheck">
                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_NationwideMortgageLicensingSystem" />
                                                <xsl:with-param name="Type" select=" 'NoZero' "/>
                                            </xsl:call-template>
                                        </span>
                                    </xsl:when>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAOriginatorIdentifier/@_NationwideMortgageLicensingSystem != '' ">
                                      <span class="FieldValue">
                                        <xsl:call-template name="EmptyCheck">
                                          <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAOriginatorIdentifier/@_NationwideMortgageLicensingSystem" />
                                          <xsl:with-param name="Type" select=" 'NoZero' " />
                                        </xsl:call-template>
                                      </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span class="FieldValue">
                                            <xsl:call-template name="EmptyCheck">
                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier/@_NationwideMortgageLicensingSystem" />
                                                <xsl:with-param name="Type" select=" 'NoZero' " />
                                            </xsl:call-template>
                                        </span>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Sponsor/Agent ID: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier)/@PARTYID" />
                                        <xsl:with-param name="Type" select=" 'NoZero' " />
                                    </xsl:call-template>
                                </span>
                                <br />
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier)/PartyName" />
                                    </xsl:call-template>
                                </span>
                                <br />
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier)/Address1" />
                                    </xsl:call-template>
                                </span>
                                <br />
                                <label class="FieldLabel">NMLS ID: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier/@_NationwideMortgageLicensingSystem" />
                                        <xsl:with-param name="Type" select=" 'NoZero' "/>
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <label class="FieldLabel">Loan Officer Name: </label>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/UNDERWRITINGCASE/LOAN_OFFICER/@_FirstName" />
                                </span>
                                <xsl:text> </xsl:text>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/UNDERWRITINGCASE/LOAN_OFFICER/@_MiddleInitial" />
                                </span>
                                <xsl:text> </xsl:text>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/UNDERWRITINGCASE/LOAN_OFFICER/@_LastName" />
                                </span>
                                <xsl:text> </xsl:text>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/UNDERWRITINGCASE/LOAN_OFFICER/@_Suffix" />
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <label class="FieldLabel">Loan Officer NMLS ID: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/LOAN_OFFICER/@_NationwideMortgageLicensingSystem" />
                                        <xsl:with-param name="Type" select=" 'NoZero' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">Case Type: </label>
                                <span class="FieldValue">
                                    <xsl:choose>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD = 'RegularHUD' ">
                                            Regular HUD
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD = 'IrregularHUD' ">
                                            Irregular HUD
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD = 'HUDVA' ">
                                            HUD VA/CRV
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD = 'DEVA' ">
                                            DE VA/CRV
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD = 'RegularDE' ">
                                            Regular DE
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="EmptyCheck">
                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/CaseTypeCD" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Construction Code: </label>
                                <span class="FieldValue">
                                    <xsl:choose>
                                        <xsl:when test="VALUATIONS/@BuildingStatusType = 'Proposed' ">
                                            Proposed Construction
                                        </xsl:when>
                                        <xsl:when test="VALUATIONS/@BuildingStatusType = 'SubstantiallyRehabilitated' ">
                                            Substantial Rehabilitation
                                        </xsl:when>
                                        <xsl:when test="VALUATIONS/@BuildingStatusType = 'UnderConstruction' ">
                                            Under Construction
                                        </xsl:when>
                                        <xsl:when test="VALUATIONS/@BuildingStatusType = 'Existing' ">
                                            Existing Construction
                                        </xsl:when>
                                        <xsl:when test="VALUATIONS/@BuildingStatusType = 'New' ">
                                            New (Less than 1 Year)
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="EmptyCheck">
                                                <xsl:with-param name="Selection" select="VALUATIONS/@BuildingStatusType" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Processing Type: </label>
                                <span class="FieldValue">
                                    <xsl:choose>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'N/A' ">
                                            N/A
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'HECM' ">
                                            N/A
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'CoinsuranceConversion' ">
                                            Coinsurance Conversion
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'MilitarySales' ">
                                            Military Sales
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'RealEstateOwned' ">
                                            REO w/ Appraisal
                                        </xsl:when>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode = 'CoinsuranceEndorsements' ">
                                            Coinsurance Endorsements
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="EmptyCheck">
                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProcessingCode" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Financing Type: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAFinancingCode" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">ADP Code: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAADPCode" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Living Units: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/SubjectProperty/FinancedNumberOfUnits" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Program ID:  </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAProgramCodeIdentifier" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Loan Term: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="PRODUCT/LoanAmortizationTermMonths" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <fieldset>
                                    <legend>
                                        ADP Code Characteristics:
                                    </legend>

                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <label class="FieldLabel overLabel">Amortization Type </label>
                                                <span class="FieldValue">
                                                    <xsl:choose>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@Amortization = 'Fixed' " >
                                                            Fixed Rate
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@Amortization = 'AdjustableRateMortgage' " >
                                                            ARM
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@Amortization = 'GraduatedPaymentMortgage' " >
                                                            Graduated
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:call-template name="EmptyCheck">
                                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@Amortization" />
                                                            </xsl:call-template>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </span>
                                            </td>

                                            <td>
                                                <span class="FieldLabel overLabel">Housing Program</span>
                                                <span class="FieldValue">
                                                    <xsl:choose>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@HousingProgram = 'FHAStandardMortgageProgram203b' " >
                                                            FHA Standard Mortgage Program (203b)
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@HousingProgram = 'Condominium234c' " >
                                                            Condominium (203b)
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@HousingProgram = 'Improvements203k' " >
                                                            Improvements (203k)
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@HousingProgram = 'ImprovementsCondominium203k' " >
                                                            Improvements (203k)
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@HousingProgram = 'UrbanRenewal220' " >
                                                            Urban Renewal (220)
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:call-template name="EmptyCheck">
                                                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/@HousingProgram" />
                                                            </xsl:call-template>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </span>
                                            </td>

                                            <td>
                                                <span class="FieldLabel overLabel">Property Type</span>
                                                <span class="FieldValue">
                                                    <xsl:call-template name="EmptyCheck">
                                                        <xsl:with-param name="Selection" select="LQB_DATA/PropertyType" />
                                                    </xsl:call-template>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <span class="FieldLabel overLabel">Special Program</span>
                                                <span class="FieldValue">
                                                    <xsl:choose>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram = 'NoSpecialProgram' ">
                                                            No Special Program
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram = 'IndianLands' ">
                                                            Indian Lands
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram = 'HawaiianHomelands' ">
                                                            Hawaiian Homelands
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram = 'MilitaryImpactArea' ">
                                                            Military Impact Area
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram = '223eLocationWaiver' ">
                                                            223(e) Location Waiver
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:call-template name="EmptyCheck">
                                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram" />
                                                            </xsl:call-template>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    <xsl:value-of select="CASEDATA/UNDERWRITINGCASE/FHAVA/@SpecialProgram" />
                                                </span>
                                            </td>

                                            <td>
                                                <span class="FieldLabel overLabel">Buydown</span>
                                                <span class="FieldValue">
                                                    <xsl:call-template name="EmptyCheck">
                                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@BuyDown" />
                                                    </xsl:call-template>
                                                </span>
                                            </td>

                                            <td>
                                                <span class="FieldLabel overLabel">Principal Write-down</span>
                                                <span class="FieldValue">
                                                    <xsl:choose>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@PrincipalWriteDown = 'No' ">
                                                            No
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@PrincipalWriteDown = 'GreaterThanOrEqualToTenPercent' ">
                                                            &gt;= 10% of first existing lien
                                                        </xsl:when>
                                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@PrincipalWriteDown = 'LessThanTenPercent' ">
                                                            &lt; 10% of first existing lien
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:call-template name="EmptyCheck">
                                                                <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@PrincipalWriteDown" />
                                                            </xsl:call-template>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                This is a
                                <xsl:if test="CASEDATA/APPLICATION/@LoanPurposeType = 'FR' ">
                                    <xsl:choose>
                                        <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAPriorFinancingIndicator = 'Y' ">
                                            Prior FHA
                                        </xsl:when>
                                        <xsl:otherwise>
                                            Conventional
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:if>
                                <xsl:if test="CASEDATA/UNDERWRITINGCASE/FHAVA/@RefinanceCashOut = 'Yes' ">
                                    Cash Out
                                </xsl:if>
                                <xsl:choose>
                                    <xsl:when test="CASEDATA/APPLICATION/@LoanPurposeType = 'FR' ">
                                        Forward Refinance
                                    </xsl:when>
                                    <xsl:otherwise>
                                        Forward Purchase
                                    </xsl:otherwise>
                                </xsl:choose>
                                case.
                                <xsl:if test="PROPERTY/@PriorPropertyDisposition = 'Y' ">
                                    This case was previously sold by HUD as Real Estate Owned.
                                </xsl:if>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * As Required *
                    </h3>

                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Streamline Refi:  </label>
                              <xsl:choose>
                                <xsl:when test="CASEDATA/APPLICATION/@LoanPurposeType = 'FR' and CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAStreamlinedRefinanceIndicator">
                                  <xsl:choose>
                                    <xsl:when test="CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAStreamlinedRefinanceIndicator = 'Yes' or CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAStreamlinedRefinanceIndicator = 'Y'">
                                      <span class="FieldValue">w/ Appraisal </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <span class="FieldValue">w/o Appraisal </span>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                  <span class="FieldValue">N/A </span>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Prev Case No: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/@FHAVAPreviousCaseNumber" />
                                        <xsl:with-param name="Type" select=" 'NoZero' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                203k Consultant ID:
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVA203KConsultantIdentifier" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Consultant Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVA203KConsultantIdentifier)/PartyName" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="FieldLabel overLabel">PUD/Condo Indicator:</span>
                                <span ID="spc_ind"></span>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/PROJECT/@FHAVAApprovedCondominiumClassIndicator" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">PUD/Condo ID:</span>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/PROJECT/@PROJECTID" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Submission:</span>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/PROJECT/@FHAVAApprovedCondominiumCooperativePhase" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Site Condo:</span>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/PROJECT/@FHAVAApprovedCondominiumCooperativeSpotLot" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Month/Yr Completed: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/SubjectProperty/StructureBuiltYear" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">VA CRV Expire Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAVetransExpirationDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">VA CRV Number: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAVetransReferenceNumber" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">HECM Counsel TIN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAGHECM/FHAVAHECMCounselID" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">HECM Counsel Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAGHECM/FHAVAHECMCounselID)/PartyName" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">HECM Counseling Certicate Number: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAHECM/FHAVAHECMCounselCertificate" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">HECM Counsel Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAHECM/FHAVAHECMCounselDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Projected Closing Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/ClosingPackageReceivedDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Contact Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIContactName" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                            </td>
                            <td>
                                <label class="FieldLabel">Contact Phone: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIContactPhone" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * Property Address *
                    </h3>

                    <table width="100%">
                        <tr>
                            <td>
                                <span class="FieldLabel overLabel">Hse No</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/HouseNumber" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Unit</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/ApartmentOrUnit" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Pre</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/DirectionPrefix" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Street</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/StreetName" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Type</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/StreetSuffix" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Post</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PARSEDSTREETNAME/DirectionSuffix" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Lot</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="SUBJECTPROPERTY/SubjectPropertyShortLegalDescriptionLot" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Blk/Plat</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="SUBJECTPROPERTY/SubjectPropertyShortLegalDescriptionBlk" />
                                </span>
                                <xsl:text> </xsl:text>
                                <span class="FieldValue">
                                    <xsl:value-of select="SUBJECTPROPERTY/SubjectPropertyShortLegalDescriptionPlt" />
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="FieldLabel overLabel">City</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/City" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">St</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/State" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">Zip Code</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/PostalCode" />
                                </span>
                            </td>

                            <td>
                                <span class="FieldLabel overLabel">County</span>
                                <span class="FieldValue">
                                    <xsl:value-of select="CASEDATA/PROPERTY/CountyName" />
                                </span>
                            </td>

                            <td colspan="4"></td>
                        </tr>
                    </table>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * Compliance Inspection Fields *
                    </h3>

                    <table width="100%">
                        <tr>
                            <td>
                                <label class="FieldLabel">Assignment Choice: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/VALUATIONS/InspectionType" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Inspector Name: </label>
                                <span ID="insp_name"></span>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="key('PARTYID', CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAInspectorAssignedIdentifier)/PartyName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Inspector ID: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAInspectorAssignedIdentifier" />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * Borrower Information *
                    </h3>

                    I certify that the lender associated with this case number has
                    an active loan application for this property address and listed
                    borrower(s):
                    <span class="FieldValue">
                        <xsl:call-template name="EmptyCheck">
                            <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/BORROWER_APPLICATION/@_Certification" />
                        </xsl:call-template>
                    </span>

                    <table width="100%">
                        <tr>
                            <td>
                                <label class="FieldLabel">Borrower Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[1]/UnparsedName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">SSN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[1]/SSN" />
                                        <xsl:with-param name="Type" select=" 'SSN' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Birth Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[1]/BorrowerBirthDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">Coborrower Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[2]/UnparsedName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">SSN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[2]/SSN" />
                                        <xsl:with-param name="Type" select=" 'SSN' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Birth Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[2]/BorrowerBirthDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">Coborrower Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[3]/UnparsedName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">SSN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[3]/SSN" />
                                        <xsl:with-param name="Type" select=" 'SSN' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Birth Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[3]/BorrowerBirthDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">Coborrower Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[4]/UnparsedName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">SSN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[4]/SSN" />
                                        <xsl:with-param name="Type" select=" 'SSN' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Birth Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[4]/BorrowerBirthDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="FieldLabel">Coborrower Name: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[5]/UnparsedName" />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">SSN: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[5]/SSN" />
                                        <xsl:with-param name="Type" select=" 'SSN' " />
                                    </xsl:call-template>
                                </span>
                            </td>

                            <td>
                                <label class="FieldLabel">Birth Date: </label>
                                <span class="FieldValue">
                                    <xsl:call-template name="EmptyCheck">
                                        <xsl:with-param name="Selection" select="CASEDATA/BORROWER[5]/BorrowerBirthDate" />
                                        <xsl:with-param name="Type" select=" 'Date' " />
                                    </xsl:call-template>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * CAIVRS Claim/Default Data *
                    </h3>

                    <table width="100%">
                        <xsl:for-each select="CASEDATA/BORROWER">
                            <tr>
                                <td>
                                    <label class="FieldLabel">SSN: </label>
                                    <span class="FieldValue">
                                        <xsl:call-template name="EmptyCheck">
                                            <xsl:with-param name="Selection" select="SSN" />
                                            <xsl:with-param name="Type" select=" 'SSN' " />
                                        </xsl:call-template>
                                    </span>
                                </td>

                                <td>
                                    <label class="FieldLabel">Authorization: </label>
                                    <span class="FieldValue">
                                        <xsl:call-template name="EmptyCheck">
                                            <xsl:with-param name="Selection" select="CAIVRSAuthorizationCode" />
                                        </xsl:call-template>
                                    </span>
                                </td>

                                <xsl:choose>
                                    <xsl:when test="CAIVRSAgencyNegativeReports/CaseNumber != '' and CAIVRSAgencyNegativeReports/CaseType != '' ">
                                        <td>
                                            <label class="FieldLabel">Agency Name: </label>
                                            <span class="FieldValue">
                                                <xsl:call-template name="EmptyCheck">
                                                    <xsl:with-param name="Selection" select="CAIVRSAgencyNegativeReports/AgencyName" />
                                                </xsl:call-template>
                                            </span>
                                        </td>

                                        <td>
                                            <label class="FieldLabel">Case Number: </label>
                                            <span class="FieldValue">
                                                <xsl:call-template name="EmptyCheck">
                                                    <xsl:with-param name="Selection" select="CAIVRSAgencyNegativeReports/CaseNumber" />
                                                </xsl:call-template>
                                            </span>
                                        </td>

                                        <td>
                                            <label class="FieldLabel">Case Type: </label>
                                            <span class="FieldValue">
                                                <xsl:call-template name="EmptyCheck">
                                                    <xsl:with-param name="Selection" select="CAIVRSAgencyNegativeReports/CaseType" />
                                                </xsl:call-template>
                                            </span>
                                        </td>

                                        <td>
                                            <label class="FieldLabel">Phone Referral: </label>
                                            <span class="FieldValue">
                                                <xsl:call-template name="EmptyCheck">
                                                    <xsl:with-param name="Selection" select="CAIVRSAgencyNegativeReports/PhoneReferral" />
                                                </xsl:call-template>
                                            </span>
                                        </td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <td>
                                            <label class="FieldValue">No Claims/Defaults on file </label>
                                        </td>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </tr>
                        </xsl:for-each>
                    </table>
                </div>

              <xsl:choose>
                <xsl:when test="CASEDATA/APPLICATION/@LoanPurposeType = 'FP'"></xsl:when>
                <xsl:otherwise>
                  <div class="BorderTable">
                    <h3 class="SectionTitle">
                      * Refinance Authorization *
                    </h3>

                    <div>
                      <span class="FieldLabel overLabel">New Case Projected Closing Date:</span>
                      <span class="FieldValue">
                        <xsl:call-template name="EmptyCheck">
                          <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/ClosingPackageReceivedDate" />
                          <xsl:with-param name="Type" select=" 'Date' " />
                        </xsl:call-template>
                      </span>
                    </div>

                    <hr />

                    <table width="100%">
                      <tr>
                        <td colspan="3">
                          <label class="FieldLabel">Orig. Borrower Name: </label>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIName" />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td colspan="3">
                          <label class="FieldLabel">Property Located at: </label>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIAddr" />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">New Closing Month </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIClosing1" />
                              <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIClosing2" />
                              <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Computed Premium </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIPremium" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIPremium" />
                              <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Period of Insurance </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFILoanTerm1" />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFILoanTerm2" />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Old Term (in months) </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFITerm" />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFITerm" />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Original Mortgage Amt </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIMortagee" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIMortagee" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Refund Ufmip Factor (%) </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIUnearnedMIPFactor1" />
                              <xsl:with-param name="Type" select=" 'Percent' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIUnearnedMIPFactor2" />
                              <xsl:with-param name="Type" select=" 'Percent' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">UFMIP Earned by HUD </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIEarnedMIP1" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIEarnedMIP2" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Unearned UFMIP </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIUnearnedMIP1" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIUnearnedMIP2" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Original Property Value </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIOriginalPropertyValue" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIOriginalPropertyValue" />
                              <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Authorization No. </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/@FHAVAStreamlinedReFinanceAuthorizationNumber" />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label class="FieldLabel">Expiration Date </label>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIExpirationDate" />
                              <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                          </span>
                        </td>
                        <td>
                          <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                              <xsl:with-param name="Selection" select="CASEDATA/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/FHAVASRFIExpirationDate" />
                              <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </div>
                </xsl:otherwise>
              </xsl:choose>

                <div class="BorderTable">
                    <h3 class="SectionTitle">
                        * Lender Notes *
                    </h3>

                    <span class="FieldLabel overLabel">Lender Notes:</span>
                    <span class="FieldValue">
                        <xsl:value-of select="PROCESSSTATUS/UserStatusMessage" />
                    </span>
                </div>
            </div>
        </body>
    </html>
</xsl:template>

<!--
  Formatting for the Originator ID (or Sponsored Originator EIN) and the selection of the party name and address.
-->
<xsl:template name="OriginatorIDAndAddress">
    <xsl:param name="OriginatorIDLabel" />
    <xsl:param name="OriginatorID" />
    <label class="FieldLabel"><xsl:value-of select="$OriginatorIDLabel" />  </label>
    <span class="FieldValue">
        <xsl:call-template name="EmptyCheck">
            <xsl:with-param name="Selection" select="$OriginatorID" />
            <xsl:with-param name="Type" select=" 'NoZero' " />
        </xsl:call-template>
    </span>
    <br />
    <span class="FieldValue">
        <xsl:call-template name="EmptyCheck">
            <xsl:with-param name="Selection" select="key('PARTYID', $OriginatorID)/PartyName" />
        </xsl:call-template>
    </span>
    <br />
    <span class="FieldValue">
        <xsl:call-template name="EmptyCheck">
            <xsl:with-param name="Selection" select="key('PARTYID', $OriginatorID)/Address1" />
        </xsl:call-template>
    </span>
</xsl:template>

<!--
    If the selection is empty, output "Not Entered". If the node is missing, output "N/A".
    Otherwise, output the value of the node.
-->
<xsl:template name="EmptyCheck">
    <xsl:param name="Selection" />
    <xsl:param name="Type" select=" 'text' "/>
    <xsl:choose>
        <xsl:when test="$Selection = '' ">
            Not Entered
        </xsl:when>
        <xsl:when test="not($Selection)">
            N/A
        </xsl:when>
        <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="$Type = 'Date' ">
                    <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Money' ">
                    <xsl:call-template name="FormatMoney">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Percent' ">
                    <xsl:call-template name="FormatPercent">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'SSN' ">
                    <xsl:call-template name="FormatSSN">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'NoZero' ">
                    <xsl:call-template name="FormatNoZero">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Selection" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--
    Dates come in yyyymmdd format. We want to show them as mm/dd/yyyy.
-->
  <xsl:template name="FormatDate">
    <xsl:param name="Text" />
    <xsl:choose>
      <!-- Check for integers -->
      <xsl:when test="floor($Text) = $Text">
        <xsl:variable name="yyyy">
          <xsl:value-of select="substring($Text,1,4)" />
        </xsl:variable>
        <xsl:variable name="mm">
          <xsl:value-of select="substring($Text,5,2)" />
        </xsl:variable>
        <xsl:variable name="dd">
          <xsl:value-of select="substring($Text,7,2)" />
        </xsl:variable>
        <xsl:value-of select="$mm" />/<xsl:value-of select="$dd" />/<xsl:value-of select="$yyyy" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$Text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
<!--
    Append a $ in front of money values.
-->
<xsl:template name="FormatMoney">
    <xsl:param name="Text" />
    $<xsl:value-of select="$Text" />
</xsl:template>

<!--
    Append a % at the end of percentage values.
-->
<xsl:template name="FormatPercent">
    <xsl:param name="Text" />
    <xsl:value-of select="$Text" />%
</xsl:template>

<!--
    Show nothing when we get a placeholder value back.
-->
  <xsl:template name="FormatNoZero">
      <xsl:param name="Text" />
        <xsl:choose>
          <xsl:when test="$Text = '0000000000'
                       or $Text = '00000000000'
                       or $Text = '000000000000'
                       or $Text = '0000000000000'">
            Not Entered
          </xsl:when>
          <xsl:otherwise>
              <xsl:value-of select="$Text" />
          </xsl:otherwise>
      </xsl:choose>
  </xsl:template>


<!--
    SSNs come in xxxyyzzzz format. They should be shown as xxx-yy-zzzz.
-->
<xsl:template name="FormatSSN">
    <xsl:param name="Text" />
    <xsl:variable name="xxx">
        <xsl:value-of select="substring($Text,1,3)" />
    </xsl:variable>
    <xsl:variable name="yy">
        <xsl:value-of select="substring($Text,4,2)" />
    </xsl:variable>
    <xsl:variable name="zzzz">
        <xsl:value-of select="substring($Text,6,4)" />
    </xsl:variable>
    <xsl:value-of select="$xxx" />-<xsl:value-of select="$yy" />-<xsl:value-of select="$zzzz" />
</xsl:template>

</xsl:stylesheet>

