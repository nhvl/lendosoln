﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:key name="PARTYID" match="MORTGAGEDATA/PARTY" use="@PARTYID" />

<xsl:template match="MORTGAGEDATA">
<html>
    <head id="Head1">
        <title>FHA Connection Results Viewer</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <style type="text/css">
            .FieldLabel { font-style: italic; }
            .FieldValue { font-weight: bold; }
            .overLabel { display: block; }
            * { vertical-align: top; }
            .BorderTable { border: 1px solid lightgrey; padding: 0 5px 0 5px; margin: 1em 0 1em 0; }
            .SectionTitle { text-align: center; }
            td { padding: 0.25em 0 0.25em 0; }
            #StatusCode {  padding-left: 2em; }
        </style>
    </head>
    <body>
        <div id="header">
            <div>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="LQB_DATA/ImageFileName" />
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        FHA Connection Logo
                    </xsl:attribute>
                </img>
            </div>

            <h3>Case Query Results</h3>

            <h4 id="StatusCode">
                <xsl:choose>
                    <xsl:when test="PROCESSSTATUS/ProcessStatusCode = 'SuccessUpdatesAllowed' or PROCESSSTATUS/ProcessStatusCode = 'SuccessNoUpdateAllowed' " >
                        <span class="FieldValue">Success</span>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="PROCESSSTATUS/ProcessStatusCode"/>
                    </xsl:otherwise>
                </xsl:choose>
            </h4>
            <div id="ProcessStatusMessages">
                <xsl:for-each select="PROCESSSTATUS/ProcessStatusMessage">
                    <xsl:value-of select="." />
                    <br />
                </xsl:for-each>
            </div>
            <br />

            <table width="100%">
                <tr>
                    <td>
                        <label class="FieldLabel">FHA Case Number: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/AgencyCaseIdentifier" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">Borrower Name: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="BORROWER[1]/UnparsedName" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Property Address: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="PROPERTY/Address1" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <xsl:if test="Address2 != '' ">
                    <tr>
                        <td>
                        </td>
                        <td colspan="3">
                            <span class="FieldValue">
                                <xsl:value-of select="PROPERTY/Address2" />
                            </span>
                        </td>
                    </tr>
                </xsl:if>

                <tr>
                    <td>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:value-of select="PROPERTY/city" />
                        </span>
                        <xsl:text>, </xsl:text>
                        <span class="FieldValue">
                            <xsl:value-of select="PROPERTY/state" />
                        </span>
                        <xsl:text> </xsl:text>
                        <span class="FieldValue">
                            <xsl:value-of select="PROPERTY/PostalCode" />
                        </span>
                        <xsl:text> </xsl:text>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <hr />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Originator Name: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                              <xsl:choose>
                                <xsl:when test="APPLICATION/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber != ''">
                                  <xsl:call-template name="EmptyCheck">
                                    <xsl:with-param name="Selection" select="key('PARTYID', APPLICATION/UNDERWRITINGCASE/LOAN_ORIGINATOR/@_EmployerIdentificationNumber)/PartyName" />
                                  </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:call-template name="EmptyCheck">
                                    <xsl:with-param name="Selection" select="key('PARTYID', APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAOriginatorIdentifier)/PartyName" />
                                  </xsl:call-template>
                                </xsl:otherwise>
                              </xsl:choose>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">Sponsor/Agent Name: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="key('PARTYID', APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVASponsorIdentifier)/PartyName" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Case Type: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/CaseTypeDescription" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Endorsement Processed by: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="EndorsementAuthority" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Binder Status: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="BinderStatus" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Appraiser License: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="VALUATIONS/AppraiserLicenseIdentifier" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Case Number Assigned Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/CaseReceivedDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Appraiser Name: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="key('PARTYID', VALUATIONS/AppraiserLicenseIdentifier)/PartyName" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Appraisal Logged Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="VALUATIONS/AppraisalEffectiveDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Closing Package Received Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/ClosingPackageReceivedDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                    </td>
                    <td>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Insurance Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCERESPONSE/FHAVAEndorsmentDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                        <!-- Endorsment *sic* -->
                    </td>

                    <td>
                        <label class="FieldLabel">Closing Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/LoanScheduledClosingDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Upfront MIP Due: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsuranceRequired" />
                                <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Upfront MIP Received Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsuranceReceived" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Upfront MIP Received: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsurancePaid" />
                                <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Upfront MIP Late Due? </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsurancePenalty" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Upfront MIP Factor: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAMortgageInsurancePremium/@UpfrontMIPFactor" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Annual MIP Factor: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAMortgageInsurancePremium/@AnnualMIPFactor" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">
                            <xsl:choose>
                                <xsl:when test="MORTGAGEINSURANCE/MortgageInsuranceOriginalAppraisalAmount != '' ">
                                    Original Property Value:
                                </xsl:when>
                                <xsl:otherwise>
                                    Appraised Value:
                                </xsl:otherwise>
                            </xsl:choose>
                        </label>
                    </td>
                    <td>
                        <xsl:if test="MORTGAGEINSURANCE/MortgageInsuranceOriginalAppraisalAmount != '00000000' ">
                            <xsl:choose>
                                <xsl:when test="MORTGAGEINSURANCE/MortgageInsuranceOriginalAppraisalAmount != '' ">
                                    <span class="FieldValue">
                                        <xsl:call-template name="EmptyCheck">
                                            <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsuranceOriginalAppraisalAmount" />
                                            <xsl:with-param name="Type" select=" 'Money' " />
                                        </xsl:call-template>
                                    </span>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="FieldValue">
                                        <xsl:call-template name="EmptyCheck">
                                            <xsl:with-param name="Selection" select="SUBJECTPROPERTY/SubjectPropertyAppraisedValueAmount" />
                                            <xsl:with-param name="Type" select=" 'Money' " />
                                        </xsl:call-template>
                                    </span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </td>

                    <td>
                        <label class="FieldLabel">TOTAL Score Processed: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVATotalScorecardCase" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Loan Term: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="PRODUCT/LoanAmortizationTermMonths" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Mortgage Amount: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/BaseLoanAmount" />
                                <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Prior Loan Before 7/1/91? </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/PriorLoanClosedBefore19990701" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Refi Auth No.: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAStreamlinedReFinance/@FHAVAStreamlinedReFinanceAuthorizationNumber" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Last Action Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="PROCESSSTATUS/LastActionTakenDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Principal Reduction: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAPrincipalReduction" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Last Action </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="PROCESSSTATUS/LastActionTaken" />
                            </xsl:call-template>
                        </span>
                    </td>

                    <td>
                        <label class="FieldLabel">Indemnification Date: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/FHAVAIndemnityDate" />
                                <xsl:with-param name="Type" select=" 'Date' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Preprocessing Reject: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="APPLICATION/UNDERWRITINGCASE/FHAVA/PreProcessingReject" />
                            </xsl:call-template>
                        </span>
                    </td>

                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Uninsurable Location: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MortgageInsuranceLocationUninsurable" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Uninsurable Property: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MortgageInsurancePropertyUnisurable" />
                            </xsl:call-template>
                        </span>
                        <xsl:comment>Unisurable *sic*</xsl:comment>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">NOR Comments: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCERESPONSE/MorgageInsuranceFHAVAnor/MortgageInsuranceFHAVAnorMessage" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="FieldLabel">Unpaid Balance @ 78%: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsuranceUnpaidPrincipalBalance" />
                                <xsl:with-param name="Type" select=" 'Money' " />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">Final Bill Date: </label>
                    </td>
                    <td colspan="3">
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="MORTGAGEINSURANCE/MortgageInsurancePremiumMaturationDate" />
                            </xsl:call-template>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
</xsl:template>

<!--
    If the selection is empty, output "Not Entered". If the node is missing, output "N/A".
    Otherwise, output the value of the node.
-->
<xsl:template name="EmptyCheck">
    <xsl:param name="Selection" />
    <xsl:param name="Type" select=" 'text' "/>
    <xsl:choose>
        <xsl:when test="$Selection = '' ">
            Not Entered
        </xsl:when>
        <xsl:when test="not($Selection)">
            N/A
        </xsl:when>
        <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="$Type = 'Date' ">
                    <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Money' ">
                    <xsl:call-template name="FormatMoney">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Percent' ">
                    <xsl:call-template name="FormatPercent">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'SSN' ">
                    <xsl:call-template name="FormatSSN">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'NoZero' ">
                    <xsl:call-template name="FormatNoZero">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Selection" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--
    Dates come in yyyymmdd format. We want to show them as mm/dd/yyyy.
-->
  <xsl:template name="FormatDate">
    <xsl:param name="Text" />
    <xsl:choose>
      <!-- Check for integers -->
      <xsl:when test="floor($Text) = $Text">
        <xsl:variable name="yyyy">
          <xsl:value-of select="substring($Text,1,4)" />
        </xsl:variable>
        <xsl:variable name="mm">
          <xsl:value-of select="substring($Text,5,2)" />
        </xsl:variable>
        <xsl:variable name="dd">
          <xsl:value-of select="substring($Text,7,2)" />
        </xsl:variable>
        <xsl:value-of select="$mm" />/<xsl:value-of select="$dd" />/<xsl:value-of select="$yyyy" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$Text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<!--
    Append a $ in front of money values.
-->
<xsl:template name="FormatMoney">
    <xsl:param name="Text" />
    $<xsl:value-of select="$Text" />
</xsl:template>

<!--
    Append a % at the end of percentage values.
-->
<xsl:template name="FormatPercent">
    <xsl:param name="Text" />
    <xsl:value-of select="$Text" />%
</xsl:template>

<!--
    Show nothing when we get a placeholder value back.
-->
<xsl:template name="FormatNoZero">
    <xsl:param name="Text" />
    <xsl:choose>
        <xsl:when test="$Text = '0000000000'
                     or $Text = '00000000000'
                     or $Text = '000000000000'
                     or $Text = '0000000000000'">
          Not Entered
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$Text" />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!--
    SSNs come in xxxyyzzzz format. They should be shown as xxx-yy-zzzz.
-->
<xsl:template name="FormatSSN">
    <xsl:param name="Text" />
    <xsl:variable name="xxx">
        <xsl:value-of select="substring($Text,1,3)" />
    </xsl:variable>
    <xsl:variable name="yy">
        <xsl:value-of select="substring($Text,4,2)" />
    </xsl:variable>
    <xsl:variable name="zzzz">
        <xsl:value-of select="substring($Text,6,4)" />
    </xsl:variable>
    <xsl:value-of select="$xxx" />-<xsl:value-of select="$yy" />-<xsl:value-of select="$zzzz" />
</xsl:template>

</xsl:stylesheet>
