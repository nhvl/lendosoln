﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="MORTGAGEDATA">
<html>
    <head id="Head1">
        <title>FHA Connection Results Viewer</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <style type="text/css">
            .FieldLabel { font-style: italic; }
            .FieldValue { font-weight: bold; }
            .overLabel { display: block; }
            * { vertical-align: top; }
            .BorderTable { border: 1px solid lightgrey; padding: 0 5px 0 5px; margin: 1em 0 1em 0; }
            .SectionTitle { text-align: center; }
            #borrowerInfo { margin: 0 5em 0 5em; }
            .invis { visibility: hidden; }
            #StatusCode { padding-left: 2em; }
        </style>
    </head>
    <body>
        <div id="header">
            <div>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="LQB_DATA/ImageFileName" />
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        FHA Connection Logo
                    </xsl:attribute>
                </img>
            </div>

            <h3>CAIVRS Authorization</h3>

            <h4 id="StatusCode">
                <xsl:choose>
                    <xsl:when test="PROCESSSTATUS/ProcessStatusCode = 'SuccessUpdatesAllowed' or PROCESSSTATUS/ProcessStatusCode = 'SuccessNoUpdateAllowed' or PROCESSSTATUS/ProcessStatusCode = 'SuccessNoUpdatesAllowed'" >
                        <span class="FieldValue">Success</span>
                    </xsl:when>
                    <xsl:otherwise>
                        <span class="FieldValue">
                            <xsl:value-of select="PROCESSSTATUS/ProcessStatusCode"/>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
            </h4>
            <div id="ProcessStatusMessages">
                <xsl:for-each select="PROCESSSTATUS/ProcessStatusMessage">
                    <xsl:value-of select="." />
                    <br />
                </xsl:for-each>
            </div>
            <br />
        </div>
        <div id="borrowerInfo">
            <table class="BorrowerInfo" width="100%">
                <tr>
                    <td>
                        <label class="FieldLabel">Borrower </label>
                    </td>
                    <td>
                        <label class="FieldLabel">SSN </label>
                        <span class="FieldValue">
                            <xsl:call-template name="EmptyCheck">
                                <xsl:with-param name="Selection" select="BORROWER[1]/SSN" />
                                <xsl:with-param name="Type" select=" 'SSN' " />
                            </xsl:call-template>
                        </span>
                    </td>
                    <td>
                        <label class="FieldLabel">Authorization Number: </label>
                    </td>
                    <td>
                        <span class="FieldValue">
                            <xsl:value-of select="BORROWER[1]/CAIVRSAuthorizationCode" />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel overLabel">Agency Name </label>
                        <span class="FieldValue">
                            <xsl:value-of select="AgencyName" />
                        </span>
                    </td>
                    <td>
                        <label class="FieldLabel overLabel">Case Number </label>
                        <span class="FieldValue">
                            <xsl:value-of select="CaseNumber" />
                        </span>
                    </td>
                    <td>
                        <label class="FieldLabel overLabel">Case Type </label>
                        <span class="FieldValue">
                            <xsl:value-of select="CaseType" />
                        </span>
                    </td>
                    <td>
                        <label class="FieldLabel overLabel">Phone Referral </label>
                        <span class="FieldValue">
                            <xsl:value-of select="PhoneReferral" />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span class="invis">.</span>
                    </td>
                </tr>
                <xsl:for-each select="BORROWER[position() > 1]">
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                Coborrower <xsl:value-of select="position()" />
                            </label>
                        </td>
                        <td>
                            <label class="FieldLabel">SSN </label>
                            <span class="FieldValue">
                                <xsl:call-template name="EmptyCheck">
                                    <xsl:with-param name="Selection" select="SSN" />
                                    <xsl:with-param name="Type" select=" 'SSN' " />
                                </xsl:call-template>
                            </span>
                        </td>
                        <td>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                        <td>
                            <span class="FieldValue">
                                <xsl:value-of select="CAIVRSAuthorizationCode" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel overLabel">Agency Name </label>
                            <span class="FieldValue">
                                <xsl:value-of select="CAIVRSAgencyNegativeReports/AgencyName" />
                            </span>
                        </td>
                        <td>
                            <label class="FieldLabel overLabel">Case Number </label>
                            <span class="FieldValue">
                                <xsl:value-of select="CAIVRSAgencyNegativeReports/CaseNumber" />
                            </span>
                        </td>
                        <td>
                            <label class="FieldLabel overLabel">Case Type </label>
                            <span class="FieldValue">
                                <xsl:value-of select="CAIVRSAgencyNegativeReports/CaseType" />
                            </span>
                        </td>
                        <td>
                            <label class="FieldLabel overLabel">Phone Referral </label>
                            <span class="FieldValue">
                                <xsl:value-of select="CAIVRSAgencyNegativeReports/PhoneReferral" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span class="invis">invis</span>
                        </td>
                    </tr>
                </xsl:for-each>
                <xsl:if test="count(BORROWER) = 0">
                    <tr>
                        <td colspan="4">
                            <label class="FieldLabel">Borrower N/A &#160;</label>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(BORROWER) &lt;= 1">
                    <tr>
                        <td colspan="4">
                            <label class="FieldLabel">Coborrower 1 N/A &#160;</label>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(BORROWER) &lt;= 2">
                    <tr>
                        <td colspan="4">
                            <label class="FieldLabel">Coborrower 2 N/A &#160;</label>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(BORROWER) &lt;= 3">
                    <tr>
                        <td colspan="4">
                            <label class="FieldLabel">Coborrower 3 N/A &#160;</label>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(BORROWER) &lt;= 4">
                    <tr>
                        <td colspan="4">
                            <label class="FieldLabel">Coborrower 4 N/A &#160;</label>
                            <label class="FieldLabel">Authorization Number: </label>
                        </td>
                    </tr>
                </xsl:if>
            </table>
        </div>
    </body>
</html>
</xsl:template>

<!--
    If the selection is empty, output "Not Entered". If the node is missing, output "N/A".
    Otherwise, output the value of the node.
-->
<xsl:template name="EmptyCheck">
    <xsl:param name="Selection" />
    <xsl:param name="Type" select=" 'text' "/>
    <xsl:choose>
        <xsl:when test="$Selection = '' ">
            Not Entered
        </xsl:when>
        <xsl:when test="not($Selection)">
            N/A
        </xsl:when>
        <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="$Type = 'Date' ">
                    <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Money' ">
                    <xsl:call-template name="FormatMoney">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'Percent' ">
                    <xsl:call-template name="FormatPercent">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'SSN' ">
                    <xsl:call-template name="FormatSSN">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$Type = 'NoZero' ">
                    <xsl:call-template name="FormatNoZero">
                        <xsl:with-param name="Text" select="$Selection" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Selection" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--
    Dates come in yyyymmdd format. We want to show them as mm/dd/yyyy.
-->
  <xsl:template name="FormatDate">
    <xsl:param name="Text" />
    <xsl:choose>
      <!-- Check for integers -->
      <xsl:when test="floor($Text) = $Text">
        <xsl:variable name="yyyy">
          <xsl:value-of select="substring($Text,1,4)" />
        </xsl:variable>
        <xsl:variable name="mm">
          <xsl:value-of select="substring($Text,5,2)" />
        </xsl:variable>
        <xsl:variable name="dd">
          <xsl:value-of select="substring($Text,7,2)" />
        </xsl:variable>
        <xsl:value-of select="$mm" />/<xsl:value-of select="$dd" />/<xsl:value-of select="$yyyy" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$Text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
<!--
    Append a $ in front of money values.
-->
<xsl:template name="FormatMoney">
    <xsl:param name="Text" />
    $<xsl:value-of select="$Text" />
</xsl:template>

<!--
    Append a % at the end of percentage values.
-->
<xsl:template name="FormatPercent">
    <xsl:param name="Text" />
    <xsl:value-of select="$Text" />%
</xsl:template>

<!--
    Show nothing when we get a placeholder value back.
-->
<xsl:template name="FormatNoZero">
    <xsl:param name="Text" />
    <xsl:choose>
        <xsl:when test="$Text = '0000000000'
                     or $Text = '00000000000'
                     or $Text = '000000000000'
                     or $Text = '0000000000000'">
          Not Entered
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$Text" />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!--
    SSNs come in xxxyyzzzz format. They should be shown as xxx-yy-zzzz.
-->
<xsl:template name="FormatSSN">
    <xsl:param name="Text" />
    <xsl:variable name="xxx">
        <xsl:value-of select="substring($Text,1,3)" />
    </xsl:variable>
    <xsl:variable name="yy">
        <xsl:value-of select="substring($Text,4,2)" />
    </xsl:variable>
    <xsl:variable name="zzzz">
        <xsl:value-of select="substring($Text,6,4)" />
    </xsl:variable>
    <xsl:value-of select="$xxx" />-<xsl:value-of select="$yy" />-<xsl:value-of select="$zzzz" />
</xsl:template>

</xsl:stylesheet>

