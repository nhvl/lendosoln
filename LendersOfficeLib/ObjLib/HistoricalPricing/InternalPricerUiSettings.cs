﻿namespace LendersOffice.ObjLib.HistoricalPricing
{
    using System;
    using System.Text.RegularExpressions;
    using Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a lender's internal pricing UI settings.
    /// </summary>
    public class InternalPricerUiSettings
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// A value indicating whether the new internal pricer UI is enabled.
        /// </summary>
        private bool enabled;

        /// <summary>
        /// The employee group name that is enabled, if it is restricted to a
        /// specific group.
        /// </summary>
        private string enabledEmployeeGroupName;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternalPricerUiSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The match from the broker's temp option settings.</param>
        public InternalPricerUiSettings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.enabled = true;
                this.enabledEmployeeGroupName = match.Groups["EmployeeGroup"].Value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the updated internal pricer
        /// UI is enabled for all employee groups.
        /// </summary>
        /// <value>True if enabled for all employee groups. Otherwise, false.</value>
        public bool IsUnconditionallyEnabled
        {
            get
            {
                return this.enabled && string.IsNullOrEmpty(this.enabledEmployeeGroupName);
            }
        }

        /// <summary>
        /// Determines if the updated internal pricer UI is enabled for the given principal.
        /// </summary>
        /// <param name="principal">The principal to evaluate.</param>
        /// <returns>True if the user has access. Otherwise, false.</returns>
        public bool IsEnabled(AbstractUserPrincipal principal)
        {
            return this.enabled
                && (string.IsNullOrEmpty(this.enabledEmployeeGroupName)
                    || principal.IsInEmployeeGroup(this.enabledEmployeeGroupName));
        }

        /// <summary>
        /// Validates that the employee group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateEmployeeGroup()
        {
            return string.IsNullOrEmpty(this.enabledEmployeeGroupName)
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.Employee, this.enabledEmployeeGroupName);
        }
    }
}
