﻿namespace LendersOffice.ObjLib.HistoricalPricing
{
    using System;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;

    /// <summary>
    /// Custom JSON converter for SHA256Checksum type.
    /// </summary>
    public class SHA256ChecksumConverter : JsonConverter
    {
        /// <summary>
        /// Determines whether the converter can be used for a given type.
        /// </summary>
        /// <param name="objectType">The type to check.</param>
        /// <returns>True if the converter can be used for the type. Otherwise, false.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(SHA256Checksum);
        }

        /// <summary>
        /// Parses a SHA256Checksum from the json.
        /// </summary>
        /// <param name="reader">The json reader.</param>
        /// <param name="objectType">The object type to parse.</param>
        /// <param name="existingValue">The object's existing value.</param>
        /// <param name="serializer">The serializer.</param>
        /// <returns>The parsed checksum.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return SHA256Checksum.Create(reader.Value as string) ?? SHA256Checksum.Invalid;
        }

        /// <summary>
        /// Serializes a SHA256Checksum to json.
        /// </summary>
        /// <param name="writer">The json writer.</param>
        /// <param name="value">The value to serialize.</param>
        /// <param name="serializer">The serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((SHA256Checksum)value).Value);
        }
    }
}
