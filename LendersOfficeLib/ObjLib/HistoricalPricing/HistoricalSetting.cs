﻿namespace LendersOffice.ObjLib.HistoricalPricing
{
    /// <summary>
    /// Setting to distinguish between historical and current values.
    /// </summary>
    public enum HistoricalSetting
    {
        /// <summary>
        /// Use the current value.
        /// </summary>
        Current = 0,

        /// <summary>
        /// Use the historical value.
        /// </summary>
        Historical = 1
    }
}
