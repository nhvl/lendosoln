﻿namespace LendersOffice.ObjLib.HistoricalPricing
{
    using System;
    using System.Text.RegularExpressions;
    using CommonProjectLib.Common;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a lender's historical pricing settings.
    /// </summary>
    public class HistoricalPricingSettings
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Whether or not the feature is enabled.
        /// </summary>
        private bool enabled;

        /// <summary>
        /// The employee group name that is enabled, if it is restricted to a
        /// specific group.
        /// </summary>
        private string enabledEmployeeGroupName;

        /// <summary>
        /// Initializes a new instance of the <see cref="HistoricalPricingSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The match from the lender's temp option XML content.</param>
        public HistoricalPricingSettings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.enabled = true;
                this.enabledEmployeeGroupName = match.Groups["EmployeeGroup"].Value;
                this.FeeService = this.ParseHistoricalSetting(nameof(this.FeeService), match.Groups["FeeService"].Value);
                this.PriceGroup = this.ParseHistoricalSetting(nameof(this.PriceGroup), match.Groups["PriceGroups"].Value);
                this.LenderFees = this.ParseHistoricalSetting(nameof(this.LenderFees), match.Groups["LenderFees"].Value);
            }
        }

        /// <summary>
        /// Gets the historical fee service setting.
        /// </summary>
        /// <value>The historical fee service setting.</value>
        public HistoricalSetting FeeService { get; private set; }

        /// <summary>
        /// Gets the historical price group setting.
        /// </summary>
        /// <value>The historical price group setting.</value>
        public HistoricalSetting PriceGroup { get; private set; }

        /// <summary>
        /// Gets the historical lender fees setting.
        /// </summary>
        /// <value>The historical lender fees setting.</value>
        public HistoricalSetting LenderFees { get; private set; }

        /// <summary>
        /// Gets a value indicating whether historical pricing is enabled for all employee groups.
        /// </summary>
        /// <value>True if enabled for all employee groups. Otherwise, false.</value>
        public bool IsUnconditionallyEnabled
        {
            get
            {
                return this.enabled && string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName);
            }
        }

        /// <summary>
        /// Gets a value indicating whether historical pricing is enabled for specific employee groups.
        /// </summary>
        public bool IsTesting
        {
            get
            {
                return this.enabled && !string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName);
            }
        }

        /// <summary>
        /// Determines if historical pricing is enabled for the given principal.
        /// </summary>
        /// <param name="principal">The principal to evaluate.</param>
        /// <returns>True if the user has access. Otherwise, false.</returns>
        public bool IsEnabled(AbstractUserPrincipal principal)
        {
            return this.enabled 
                && (string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName) 
                    || principal.IsInEmployeeGroup(this.enabledEmployeeGroupName));
        }

        /// <summary>
        /// Validates that the employee group name, if specified, exists.
        /// </summary>
        /// <returns>True if its valid. Otherwise, false.</returns>
        public bool ValidateEmployeeGroup()
        {
            return string.IsNullOrWhiteSpace(this.enabledEmployeeGroupName)
                || GroupDB.IfGroupNameExist(this.brokerId, GroupType.Employee, this.enabledEmployeeGroupName);
        }

        /// <summary>
        /// Parses historical settings out of the temp option.
        /// </summary>
        /// <param name="field">The field name being parsed.</param>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed setting.</returns>
        private HistoricalSetting ParseHistoricalSetting(string field, string value)
        {
            var setting = HistoricalSetting.Current;
            if (!Enum.TryParse(value, out setting)
                || !Enum.IsDefined(typeof(HistoricalSetting), value))
            {
                throw new CBaseException(
                    $"Invalid setting for {field}.",
                    $"Error parsing value of {field} in historical pricing settings for {this.brokerId}, value: {value}");
            }

            return setting;
        }
    }
}
