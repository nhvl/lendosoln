﻿namespace LendersOffice.ObjLib.HistoricalPricing
{
    using System;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Maintains data about a submission for historical pricing purposes.
    /// </summary>
    public class SubmissionSnapshot
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubmissionSnapshot"/> class.
        /// </summary>
        /// <remarks>Intended for serialization purposes only.</remarks>
        public SubmissionSnapshot()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmissionSnapshot"/> class
        /// from a data container.
        /// </summary>
        /// <param name="data">The data container.</param>
        public SubmissionSnapshot(IDataContainer data)
        {
            this.Id = (int)data["Id"];
            this.LoanId = (Guid)data["LoanId"];
            this.BrokerId = (Guid)data["BrokerId"];
            this.SnapshotDataLastModifiedD = (DateTime)data["SnapshotDataLastModifiedD"];
            this.SnapshotFileKey = SHA256Checksum.Create((string)data["SnapshotFileKey"]) ?? SHA256Checksum.Invalid;
            this.PriceGroupId = (Guid)data["PriceGroupId"];
            this.PriceGroupFileKey = SHA256Checksum.Create((string)data["PriceGroupFileKey"]) ?? SHA256Checksum.Invalid;
            this.FeeServiceRevisionId = data["FeeServiceRevisionId"] as int?;
            this.SubmissionType = (E_sPricingModeT)data["SubmissionType"];
            this.SubmissionDate = (DateTime)data["SubmissionDate"];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmissionSnapshot"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="snapshotDataLastModifiedD">The last modified date of the snapshot.</param>
        /// <param name="snapshotFileKey">The file key of the snapshot.</param>
        /// <param name="priceGroupId">The price group id.</param>
        /// <param name="priceGroupFileKey">The file key of the price group.</param>
        /// <param name="feeServiceRevisionId">The id of the fee service revision.</param>
        /// <param name="submissionType">The type of submission.</param>
        /// <param name="submissionDate">The date of submission.</param>
        public SubmissionSnapshot(
            Guid loanId,
            Guid brokerId,
            DateTime snapshotDataLastModifiedD,
            SHA256Checksum snapshotFileKey,
            Guid priceGroupId,
            SHA256Checksum priceGroupFileKey,
            int feeServiceRevisionId,
            E_sPricingModeT submissionType,
            DateTime submissionDate)
        {
            this.LoanId = loanId;
            this.BrokerId = brokerId;
            this.SnapshotDataLastModifiedD = snapshotDataLastModifiedD;
            this.SnapshotFileKey = snapshotFileKey;
            this.PriceGroupId = priceGroupId;
            this.PriceGroupFileKey = priceGroupFileKey;

            if (feeServiceRevisionId != CApplicantPrice.InvalidFeeServiceRevisionId)
            {
                this.FeeServiceRevisionId = feeServiceRevisionId;
            }

            this.SubmissionType = submissionType;
            this.SubmissionDate = submissionDate;
        }

        /// <summary>
        /// Gets the id of the record.
        /// </summary>
        /// <value>
        /// Null for newly created records, will have a value when the record has 
        /// been loaded from the database.
        /// </value>
        [JsonProperty]
        public int? Id { get; private set; }

        /// <summary>
        /// Gets the id of the loan.
        /// </summary>
        /// <value>The id of the loan.</value>
        [JsonProperty]
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the id of the broker.
        /// </summary>
        /// <value>The id of the broker.</value>
        [JsonProperty]
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the date the snapshot content was last modified.
        /// </summary>
        /// <value>The date the snapshot content was last modified.</value>
        [JsonProperty]
        public DateTime SnapshotDataLastModifiedD { get; private set; }

        /// <summary>
        /// Gets the file key of the snapshot.
        /// </summary>
        /// <value>The file key of the snapshot.</value>
        [JsonProperty]
        [JsonConverter(typeof(SHA256ChecksumConverter))]
        public SHA256Checksum SnapshotFileKey { get; private set; }

        /// <summary>
        /// Gets the id of the price group.
        /// </summary>
        /// <value>The id of the price group.</value>
        [JsonProperty]
        public Guid PriceGroupId { get; private set; }

        /// <summary>
        /// Gets the file key of the price group.
        /// </summary>
        /// <value>The file key of the price group.</value>
        [JsonProperty]
        [JsonConverter(typeof(SHA256ChecksumConverter))]
        public SHA256Checksum PriceGroupFileKey { get; private set; }

        /// <summary>
        /// Gets the id of the fee service revision.
        /// </summary>
        /// <value>The id of the fee service revision.</value>
        [JsonProperty]
        public int? FeeServiceRevisionId { get; private set; }

        /// <summary>
        /// Gets the type of the submission.
        /// </summary>
        /// <value>The type of the submission.</value>
        [JsonProperty]
        public E_sPricingModeT SubmissionType { get; private set; }

        /// <summary>
        /// Gets the date of the submission.
        /// </summary>
        /// <value>The date of the submission.</value>
        [JsonProperty]
        public DateTime SubmissionDate { get; private set; }
    }
}