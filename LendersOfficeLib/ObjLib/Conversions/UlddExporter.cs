﻿namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.DocMagicLib;
    using ObjLib.Conversions;
    using ULDD;

    public class UlddExportErrorItem
    {
        public UlddExportErrorItem()
        {
        }
        public UlddExportErrorItem(string fieldId, string errorMessage, string pageLocation)
        {
            this.FieldId = fieldId;
            this.ErrorMessage = errorMessage;
            this.PageLocation = pageLocation;
        }
        public string FieldId
        {
            get; set;
        }
        public string ErrorMessage
        {
            get; set;
        }
        public string PageLocation
        {
            get; set;
        }
    }
    public class UlddExportError
    {
        public Guid sLId
        {
            get; set;
        }
        public string sLNm
        {
            get; set;
        }
        public Guid appid
        {
            get; set;
        }

        public UlddExportError(Guid _sLId, string _sLNm, Guid _appid)
        {
            this.sLId = _sLId;
            this.sLNm = _sLNm;
            this.appid = _appid;
            m_errorList = new List<UlddExportErrorItem>();
        }
        private List<UlddExportErrorItem> m_errorList = null;
        public IEnumerable<UlddExportErrorItem> ErrorList
        {
            get
            {
                return m_errorList;
            }
        }

        internal void Add(UlddExportErrorItem error)
        {
            m_errorList.Add(error);
        }
    }
    public class UlddExporter
    {

        // 8/27/2012 dd - FNMA Validation is at https://loandelivery.intgfanniemae.com/mismoxtt/
        private const string MismoReferenceModelIdentifier = "3.0.0.263.12";
        private const string AboutVersion_Freddie_Phase2 = "FRE 3.3.0";
        private const string AboutVersion_Freddie_Phase3 = "FRE 4.0.0";
        private const string LoanDeliveryFilePreparerIdentifier = "VMLILQ01D";
        private const string LoanDeliveryFilePreparerIdentifier_Freddie = "000184";
        //private CPageData dataLoan = null;
        //private List<Guid> m_loanIdList = new List<Guid>();
        //private CPageData dataLoanLinked = null;
        private List<CPageData> m_dataLoanList = new List<CPageData>();
        //private CAppData m_dataApp = null;
        private bool m_isLQAExport = false;
        protected const string AboutVersion_Fnma_Phase2 = "FNM 2.0";
        protected const string AboutVersion_Fnma_Phase3 = "FNM 3.0";

        private CPageData GetPageData(Guid loanid)
        {
            return new CFullAccessPageData(loanid,
             CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(UlddExporter)).Union(
             CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release)
             );
        }

        private CPageData LoadDataLoan(Guid sLId, E_sGseDeliveryTargetT? sGseDeliveryTargetT)
        {
            CPageData dataLoan = GetPageData(sLId);
            dataLoan.InitLoad();
            //if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && dataLoan.LastDisclosedGFEArchive != null) // temporarily disable opm 142081
            //{
            //    dataLoan.ApplyGFEArchive(dataLoan.LastDisclosedGFEArchive);
            //}
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            Phase3Parts[dataLoan.sLId] = dataLoan.BrokerDB.GetUlddPhase3Settings.GetPart(dataLoan.sGseDeliveryTargetT, dataLoan.sLoanFileT == E_sLoanFileT.Test);
            if (sGseDeliveryTargetT.HasValue)
            {
                dataLoan.sGseDeliveryTargetT = sGseDeliveryTargetT.Value;
            }
            return dataLoan;
        }
        public UlddExporter(IEnumerable<Guid> sLIdList, E_sGseDeliveryTargetT? sGseDeliveryTargetT)
        {
            foreach (var id in sLIdList)
            {
                //m_loanIdList.Add(id);
                m_dataLoanList.Add(LoadDataLoan(id, sGseDeliveryTargetT));
            }
        }

        public UlddExporter(Guid sLId, bool isLQAExport)
        {
            m_isLQAExport = isLQAExport;
            m_dataLoanList.Add(LoadDataLoan(sLId, E_sGseDeliveryTargetT.FreddieMac));
        }

        public UlddExporter(Guid sLId, E_sGseDeliveryTargetT sGseDeliveryTargetT)
        {
            m_dataLoanList.Add(LoadDataLoan(sLId, sGseDeliveryTargetT));

        }

        public UlddExporter(Guid sLId)
        {
            //m_loanIdList.Add(sLId);
            m_dataLoanList.Add(LoadDataLoan(sLId, null));
            //m_dataLoanList.Add(LoadDataLoan(sLId));
            //dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(UlddExporter));
            //dataLoan.InitLoad();
            //dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            //if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
            //{
            //    dataLoanLinked = CPageData.CreateUsingSmartDependency(dataLoan.sLinkedLoanInfo.LinkedLId, typeof(UlddExporter));
            //    dataLoanLinked.InitLoad();
            //    dataLoanLinked.SetFormatTarget(FormatTarget.MismoClosing);
            //}

            //m_dataApp = dataLoan.GetAppData(0); // Use Primary App.
        }
        private void AddCustomError(UlddExportError uld, string fieldName, string errorMessage, string pageInfo)
        {
            var field = LendersOffice.CustomFormField.FormField.RetrieveById(fieldName);
            var friendlyName = (field == null) ? "(" + fieldName + ")" : "\"" + field.FriendlyName + "\"" + "(" + fieldName + ")";
            uld.Add(new UlddExportErrorItem(fieldName, friendlyName + errorMessage, pageInfo));
        }
        private void AddError(UlddExportError uld, string fieldName, string requiredWhen, string pageInfo)
        {
            if (requiredWhen == "Always")
            {
                AddCustomError(uld, fieldName, " is required for all loan files", pageInfo);
            }
            else
            {
                AddCustomError(uld, fieldName, " is required when " + requiredWhen + ". ", pageInfo);
            }
        }
        private static readonly DateTime AltenateLenderRequiredDate = new DateTime(2011, 11, 26);
        private UlddExportError VerifyLoan(CPageData dataLoan, E_sGseDeliveryTargetT originalGseDeliveryTarget, string originalsLNm)
        {
            UlddExportError ulddExportError = new UlddExportError(dataLoan.sLId, dataLoan.sLNm, dataLoan.GetAppData(0).aAppId);

            if (string.IsNullOrEmpty(dataLoan.sLenNum))
            {
                ulddExportError.Add(new UlddExportErrorItem("sLenNum", ErrorMessages.ULDD.EmptyLenderID, "Shipping:: GSE Delivery"));
            }

            if (string.IsNullOrEmpty(dataLoan.sDocumentNoteD_rep))
            {
                ulddExportError.Add(new UlddExportErrorItem("sDocumentNoteD", ErrorMessages.ULDD.EmptyNoteDate, "Status:: General"));
            }

            if (string.IsNullOrEmpty(dataLoan.sSchedDueD1_rep))
            {
                ulddExportError.Add(new UlddExportErrorItem("sSchedDueD1", ErrorMessages.ULDD.Empty1PaymentDueDate, "Truth In Lending OR Funding:: Settlement Charges"));
            }
            if (dataLoan.sHouseVal == 0)
            {
                ulddExportError.Add(new UlddExportErrorItem("sHouseVal", ErrorMessages.ULDD.EmptyHouseValue, "Loan Info:: This Loan Info"));
            }
            if (DateTime.Today >= AltenateLenderRequiredDate)
            {
                if (dataLoan.sDocMagicAlternateLenderId == Guid.Empty)
                {
                    var lenderAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (CAgentFields.Empty == lenderAgent || string.IsNullOrEmpty(lenderAgent.CompanyName))
                    {
                        ulddExportError.Add(new UlddExportErrorItem("sDocMagicAlternateLenderId",
                            ErrorMessages.ULDD.AltLenderRequired,
                            "Status:: General"));
                    }
                }
            }

            try
            {
                if (E_sGseDeliveryTargetT.GinnieMae == dataLoan.sGseDeliveryTargetT)
                {
                    ulddExportError.Add(new UlddExportErrorItem("sGseDeliveryTargetT", ErrorMessages.ULDD.GSeTargetGinnieMaeExportNotAvailable, "Shipping:: GSE Delivery"));
                }
                if (E_sGseDeliveryTargetT.Other_None == dataLoan.sGseDeliveryTargetT)
                {
                    ulddExportError.Add(new UlddExportErrorItem("sGseDeliveryTargetT", ErrorMessages.ULDD.GseTargetOtherExportNotAvailable, "Shipping:: GSE Delivery"));
                }



                if (dataLoan.sGseDeliveryTargetT != originalGseDeliveryTarget)
                {
                    ulddExportError.Add(new UlddExportErrorItem("sGseDeliveryTargetT", "This loan is targeted for delivery to "
                    + dataLoan.sGseDeliveryTargetT.ToString("G") + ", but loan number "
                    + originalsLNm + " is targeted for delivery to "
                    + originalGseDeliveryTarget.ToString("G") + " All loans in the export must be targeted for delivery to the same GSE.",
                    "Shipping:: GSE Delivery"));
                }
                var uld = ulddExportError;
                var dataApp = dataLoan.GetAppData(0);
                // Always
                try // some are added to try catch blocks as they threw exceptions.
                {
                    if (dataLoan.sMonthlyPmt == 0)
                    {
                        AddError(uld, "sMonthlyPmt", "Always", "Loan Info:: This Loan Info");
                    }
                }
                catch (CBaseException e)
                {
                    uld.Add(new UlddExportErrorItem("sMonthlyPmt", "sMonthlyPmt had exception: " + e.UserMessage, "Loan Info:: This Loan Info"));
                    Tools.LogError(e);
                }

                if (string.IsNullOrEmpty(dataLoan.sYrBuilt))
                {
                    AddError(uld, "sYrBuilt", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataApp.aBDecPastOwnership))
                {
                    AddError(uld, "aBDecPastOwnership", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataLoan.sSpAddr))
                {
                    AddError(uld, "sSpAddr", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataLoan.sSpCity))
                {
                    AddError(uld, "sSpCity", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataLoan.sSpZip))
                {
                    AddError(uld, "sSpZip", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataLoan.sSpState))
                {
                    AddError(uld, "sSpState", "Always", "1003");
                }
                if (dataLoan.sGseSpT == E_sGseSpT.LeaveBlank)
                {
                    AddError(uld, "sGseSpT", "Always", "1003");
                }
                //if (dataLoan.sProdSpStructureT == E_sProdSpStructureT.) { AddError(uld, "sProdSpStructureT", "Always", "1003"); }
                if (dataLoan.sUnitsNum == 0)
                {
                    AddError(uld, "sUnitsNum", "Always", "1003");
                }
                //if(dataLoan.sOccT == E_sOccT.){ AddError(uld, "sOccT", "Always", "1003"); }
                if (dataLoan.sTerm == 0)
                {
                    AddError(uld, "sTerm", "Always", "1003");
                }
                //if(dataLoan.sFinMethT == E_sFinMethT.){ AddError(uld, "sFinMethT", "Always", "1003"); }
                if (string.IsNullOrEmpty(dataLoan.sDocumentNoteD_rep))
                {
                    AddError(uld, "sDocumentNoteD", "Always", "Status:: General");
                }
                if (dataLoan.sDue == 0)
                {
                    AddError(uld, "sDue", "Always", "1003");
                }
                if (dataLoan.sLTotI == 0)
                {
                    AddError(uld, "sLTotI", "Always", "1003");
                }
                //if(dataLoan.sLienPosT == E_sLienPosT.){ AddError(uld, "sLienPosT", "Always", "1003"); }
                //if(dataLoan.sLPurposeT == E_sLPurposeT.){ AddError(uld, "sLPurposeT", "Always", "1003"); }
                if (dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    AddCustomError(uld, nameof(CPageData.sLPurposeT), "Construction products are not supported.", "1003");
                }
                //if(dataLoan.sLT == E_sLT.){ AddError(uld, "sLT", "Always", "1003"); }
                if (dataLoan.sFinalLAmt == 0)
                {
                    AddError(uld, "sFinalLAmt", "Always", "1003");
                }
                if (false == dataApp.aBAddrMailUsePresentAddr)
                {
                    if (string.IsNullOrEmpty(dataApp.aBAddrMail))
                    {
                        AddError(uld, "aBAddrMail", "not aBAddrMailUsePresentAddr", "1003");
                    }
                    if (string.IsNullOrEmpty(dataApp.aBCityMail))
                    {
                        AddError(uld, "aBCityMail", "not aBAddrMailUsePresentAddr", "1003");
                    }
                    if (string.IsNullOrEmpty(dataApp.aBZipMail))
                    {
                        AddError(uld, "aBZipMail", "not aBAddrMailUsePresentAddr", "1003");
                    }
                    if (string.IsNullOrEmpty(dataApp.aBStateMail))
                    {
                        AddError(uld, "aBStateMail", "not aBAddrMailUsePresentAddr", "1003");
                    }
                }
                //if(string.IsNullOrEmpty(dataApp.aCFirstNm)){ AddError(uld, "aCFirstNm", "Always", "1003"); }
                //if(string.IsNullOrEmpty(dataApp.aCLastNm)){ AddError(uld, "aCLastNm", "Always", "1003"); }
                //if(string.IsNullOrEmpty(dataApp.aCMidNm)){ AddError(uld, "aCMidNm", "Always", "1003"); }
                if (string.IsNullOrEmpty(dataApp.aBFirstNm))
                {
                    AddError(uld, "aBFirstNm", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataApp.aBLastNm))
                {
                    AddError(uld, "aBLastNm", "Always", "1003");
                }
                //if (string.IsNullOrEmpty(dataApp.aBMidNm)) { AddError(uld, "aBMidNm", "Always", "1003"); } // sk 107368
                //if(dataApp.aCAge == 0){ AddError(uld, "aCAge", "Always", "1003"); }
                //if(dataApp.aCDob.IsValid){ AddError(uld, "aCDob", "Always", "1003"); }
                //if(dataApp.aCTotI == 0){ AddError(uld, "aCTotI", "Always", "1003"); }
                if (dataApp.aBAge == 0)
                {
                    AddError(uld, "aBAge", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataApp.aBDob_rep))
                {
                    AddError(uld, "aBDob", "Always", "1003");
                }
                //if (dataApp.aBTotI == 0) { AddError(uld, "aBTotI", "Always", "1003"); }
                //if(dataApp.aCGender == 0){ AddError(uld, "aCGender", "Always", "1003"); }
                if (dataApp.aBGender == E_GenderT.LeaveBlank)
                {
                    AddError(uld, "aBGender", "Always", "1003");
                }
                //if(dataApp.aCHispanicT == E_aHispanicT.LeaveBlank){ AddError(uld, "aCHispanicT", "Always", "1003"); }
                //if (dataApp.aBHispanicT == E_aHispanicT.LeaveBlank) { AddError(uld, "ABHispanicT", "Always", "1003"); }
                //Don't know what to do for aBIsAsian, it is a Boolean
                //Don't know what to do for aBIsBlack, it is a Boolean
                //Don't know what to do for aBIsPacificIslander, it is a Boolean
                //Don't know what to do for aBIsWhite, it is a Boolean
                //Don't know what to do for aBIsAmericanIndian, it is a Boolean
                //if (string.IsNullOrEmpty(dataApp.aCSsn)) { AddError(uld, "aCSsn", "Always", "1003"); }
                if (string.IsNullOrEmpty(dataApp.aBSsn))
                {
                    AddError(uld, "aBSsn", "Always", "1003");
                }
                if (!dataLoan.sSpState.Equals("DE", StringComparison.OrdinalIgnoreCase) &&
                    !dataLoan.sSpState.Equals("ME", StringComparison.OrdinalIgnoreCase) &&
                    !dataLoan.sSpState.Equals("MO", StringComparison.OrdinalIgnoreCase) &&
                    string.IsNullOrEmpty(dataLoan.sApp1003InterviewerLoanOriginationCompanyIdentifier))
                {
                    AddError(uld, "sApp1003InterviewerLoanOriginationCompanyIdentifier", "Always", "1003");
                }
                if (string.IsNullOrEmpty(dataLoan.sApp1003InterviewerLoanOriginatorIdentifier))
                {
                    uld.Add(new UlddExportErrorItem("sApp1003InterviewerLoanOriginatorIdentifier",
                        "Loan Originator NMLS ID (sApp1003InterviewerLoanOriginatorIdentifier) is required for all loan files.", "1003 page 3:: Section 'To be Completed by Loan Originator'"));
                }
                if (string.IsNullOrEmpty(dataLoan.sLenLNum))
                {
                    AddError(uld, "sLenLNum", "Always", "Forms:: 1008 Single");
                }

                if (string.IsNullOrEmpty(dataLoan.sGseDeliveryOwnershipPc_rep))
                {
                    AddError(uld, "sGseDeliveryOwnershipPc", "Always", "Shipping:: GSE Delivery");
                }
                //if(dataLoan.sGseDeliveryRemittanceT == E_sGseDeliveryRemittanceT.){ AddError(uld, "sGseDeliveryRemittanceT", "Always", "CMS to set"); }
                //if (string.IsNullOrEmpty(dataLoan.sLenLNum)) { AddError(uld, "sLenNum", "Always", "Forms:: 1008 Single"); }
                if (string.IsNullOrEmpty(dataLoan.sGseDeliveryServicerId))
                {
                    AddError(uld, "sGseDeliveryServicerId", "Always", "Shipping:: GSE Delivery");
                }
                if (string.IsNullOrEmpty(dataLoan.sGseDeliveryDocumentCustodianId))
                {
                    AddError(uld, "sGseDeliveryDocumentCustodianId", "Always", "Shipping:: GSE Delivery");
                }
                if (dataLoan.sBranchChannelT == E_BranchChannelT.Blank)
                {
                    AddError(uld, "sBranchChannelT", "Always", "Status:: General");
                }
                //if (dataLoan.sProFloodIns == 0) { AddError(uld, "sProFloodIns", "Always", "GFE/HUD-1"); }
                //Don't know what to do for sWillEscrowBeWaived, it is a Boolean
                if (dataLoan.sApprVal == 0)
                {
                    AddError(uld, "sApprVal", "Always", "Loan Info:: This Loan Info");
                }
                //if(dataLoan.sLPurposeT == E_sLPurposeT.){ AddError(uld, "sLPurposeT", "Always", "Lender to provide"); }
                if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.LeaveBlank)
                {
                    AddError(uld, "sSpValuationMethodT", "Always", "Subject Property:: Details");
                }
                if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FieldReview)
                {
                    CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (agent == null || !agent.IsValid || String.IsNullOrEmpty(agent.LicenseNumOfAgent))
                        AddError(uld, "sSpValuationMethodT", ErrorMessages.ULDD.FieldReviewAppraiserLicenseNumberRequired, "Subject Property:: Details");
                }

                if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.PriorAppraisalUsed)
                {
                    AddCustomError(uld, "sSpValuationMethodT", ": value may not be \"Prior Appraisal Used\" in ULDD 2.0 export to FNMA", "Subject Property:: Details");
                }

                if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2)
                    && dataLoan.sSpAppraisalFormT == E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport)
                {
                    AddCustomError(uld, "sSpAppraisalFormT", ": value may not be \"Appraisal Update and/or Completion Report\" in ULDD 3.0 export to FNMA", "Subject Property:: Details");
                }

                //Don't know what to do for sHmdaReportAsHoepaLoan, it is a Boolean
                if (dataLoan.sHmdaAprRateSpreadLckd && string.IsNullOrEmpty(dataLoan.sHmdaAprRateSpread))
                {
                    AddError(uld, "sHmdaAprRateSpread", "Always", "Status:: HMDA");
                }
                //if (string.IsNullOrEmpty(dataLoan.sFundD_rep)) { AddError(uld, "sFundD", "Always", "Lender to provide"); }
                if (string.IsNullOrEmpty(dataLoan.sAppSubmittedD_rep))
                {
                    AddError(uld, "sAppSubmittedD_rep", "Always", "Status:: Processing");
                }
                try
                {
                    if (dataLoan.sProThisMPmt == 0)
                    {
                        AddError(uld, "sProThisMPmt", "Always", "1003 Page 1");
                    }
                }
                catch (CBaseException e)
                {
                    AddError(ulddExportError, "sProThisMPmt", "sProThisMPmt had exception: " + e.UserMessage, "1003 Page 1");
                    Tools.LogError(e);
                }
                if (string.IsNullOrEmpty(dataLoan.sRLckdD_rep))
                {
                    AddError(uld, "sRLckdD", "Always", "Status:: General");
                }
                var isProd3rdPartyUwResultT = (int)dataLoan.sProd3rdPartyUwResultT;
                if ((1 <= isProd3rdPartyUwResultT) && (isProd3rdPartyUwResultT <= 20) && (isProd3rdPartyUwResultT != 14)) //! make this explicit later.
                {
                    if (string.IsNullOrEmpty(dataLoan.sDuCaseId) && string.IsNullOrEmpty(dataLoan.sLpAusKey))
                    {
                        AddError(uld, "sDuCaseId, sLpAusKey", "sProd3rdPartyUwResultT is DU or LP", "1008 Combined");
                    }
                }
                //Couldn't find sAusRecommendation if matches the enum. If not or blank, use sProd3rdPartyUwResultT // removing.
                //if (string.IsNullOrEmpty(dataLoan.sMersMin)) { AddError(uld, "sMersMin", "Always", "Lender to provide"); }
                //if (dataApp.aBExperianScore == 0) { AddError(uld, "aBExperianScore", "Always", "Credit report"); }
                //if (dataApp.aBTransUnionScore == 0) { AddError(uld, "aBTransUnionScore", "Always", "Credit report"); }
                //if (dataApp.aBEquifaxScore == 0) { AddError(uld, "aBEquifaxScore", "Always", "Credit report"); }
                //if (dataApp.aCExperianScore == 0) { AddError(uld, "aCExperianScore", "Always", "Credit report"); }
                //if (dataApp.aCTransUnionScore == 0) { AddError(uld, "aCTransUnionScore", "Always", "Credit report"); }
                //if (dataApp.aCEquifaxScore == 0) { AddError(uld, "aCEquifaxScore", "Always", "Credit report"); }

                if (dataLoan.sOccT == E_sOccT.Investment || dataLoan.sUnitsNum > 1)
                {
                    //// 1/7/2015 IR - OPM 199136 - FHLMC: Bedroom Count / Eligible Rent not sent if no appraisal is used.
                    if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac
                        || ToMismo(dataLoan.sSpValuationMethodT) == PropertyValuationMethodType.FullAppraisal
                        || ToMismo(dataLoan.sSpValuationMethodT) == PropertyValuationMethodType.PriorAppraisalUsed)
                    {
                        if (string.IsNullOrEmpty(dataLoan.sSpUnit2BedroomsCount_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit1BedroomsCount_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit3BedroomsCount_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit4BedroomsCount_rep))
                        {
                            AddError(ulddExportError, "sSpUnits1BedroomsCount,sSpUnits2BedroomsCount,sSpUnits3BedroomsCount,sSpUnits4BedroomsCount",
                                "sOccT == Investment || sUnitsNum > 1", "Subject Property:: Details");
                        }
                        if (string.IsNullOrEmpty(dataLoan.sSpUnit1EligibleRent_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit2EligibleRent_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit3EligibleRent_rep) &&
                            string.IsNullOrEmpty(dataLoan.sSpUnit4EligibleRent_rep))
                        {
                            AddError(ulddExportError, "sSpUnit4EligibleRent,sSpUnit3EligibleRent,sSpUnit2EligibleRent,sSpUnit1EligibleRent",
                                "sOccT == Investment || sUnitsNum > 1", "Subject Property:: Details");
                        }
                    }
                }

                if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
                {
                    if (string.IsNullOrEmpty(dataLoan.sInitialDeposit_Escrow_rep))
                    {
                        AddError(uld, "sInitialDeposit_Escrow", "sGseDeliveryTargetT is FreddieMac", "Financing:: Servicing");
                    }

                }

                if (dataApp.aBDecPastOwnership == "N")
                {
                    //if (dataApp.aCHomeOwnershipCounselingSourceT == E_aHomeOwnershipCounselingSourceT.LeaveBlank) { AddError(uld, "aCHomeOwnershipCounselingSourceT", "Per borrower if aBDecPastOwnership = 'N'", ""); }
                    //if (dataApp.aCHomeOwnershipCounselingFormatT == E_aHomeOwnershipCounselingFormatT.LeaveBlank) { AddError(uld, "aCHomeOwnershipCounselingFormatT", "Per borrower if aBDecPastOwnership = 'N'", ""); }
                    //if (dataApp.aBHomeOwnershipCounselingSourceT == E_aHomeOwnershipCounselingSourceT.LeaveBlank) { AddError(uld, "aBHomeOwnershipCounselingSourceT", "Per borrower if aBDecPastOwnership = 'N'", "Underwriting:: Community Lending"); }
                    //if (dataApp.aBHomeOwnershipCounselingFormatT == E_aHomeOwnershipCounselingFormatT.LeaveBlank) { AddError(uld, "aBHomeOwnershipCounselingFormatT", "Per borrower if aBDecPastOwnership = 'N'", "Underwriting:: Community Lending"); }
                }

                if (dataLoan.sHasTempBuydown)
                {
                    if (string.IsNullOrEmpty(dataLoan.sQualIR_rep))
                    {
                        AddError(uld, "sQualIR", "sGseDeliveryTargetT is FreddieMac and sHasTempBuydown is True", "Forms:: 1003 Page 1");
                    }

                    var terms = new[] { dataLoan.sBuydwnMon1, dataLoan.sBuydwnMon2, dataLoan.sBuydwnMon3, dataLoan.sBuydwnMon4, dataLoan.sBuydwnMon5 };
                    var rates = new[] { dataLoan.sBuydwnR1, dataLoan.sBuydwnR2, dataLoan.sBuydwnR3, dataLoan.sBuydwnR4, dataLoan.sBuydwnR5 };
                    bool remainingTermsMustBeZero = false;
                    for (int i = 0; i < 5; i++)
                    {
                        if (terms[i] != 0 && rates[i] == 0)
                        {
                            ulddExportError.Add(new UlddExportErrorItem("sBuydwnR" + (i + 1).ToString(), ErrorMessages.Buydown.MismatchedRate, "Truth In Lending"));
                        }
                        else if (terms[i] == 0)
                        {
                            if (rates[i] != 0)
                            {
                                ulddExportError.Add(new UlddExportErrorItem("sBuydwnMon" + (i + 1).ToString(), ErrorMessages.Buydown.MismatchedTerm, "Truth In Lending"));
                            }

                            remainingTermsMustBeZero = true;
                        }
                        else if (remainingTermsMustBeZero)
                        {
                            ulddExportError.Add(new UlddExportErrorItem("sBuydwnMon" + (i + 1).ToString(), ErrorMessages.Buydown.PrematureEmptyTerm, "Truth In Lending"));
                        }
                    }

                    var rateChanges = new[] { (rates[0] - rates[1]), (rates[1] - rates[2]), (rates[2] - rates[3]), (rates[3] - rates[4]) };
                    bool remainingRateChangesMustBeZero = false;
                    foreach (var change in rateChanges)
                    {
                        if (change == 0)
                        {
                            remainingRateChangesMustBeZero = true;
                        }
                        else if (remainingRateChangesMustBeZero)
                        {
                            ulddExportError.Add(new UlddExportErrorItem("sBuydwnR1 - sBuydwnR5", ErrorMessages.Buydown.PrematureEmptyRateChange, "Truth In Lending"));
                        }
                        else if (change != dataLoan.sBuydwnR1stIncrease)
                        {
                            ulddExportError.Add(new UlddExportErrorItem("sBuydwnR1 - sBuydwnR5", ErrorMessages.Buydown.NonMatchingRateChange, "Truth In Lending"));
                        }
                    }
                }

                if (dataLoan.sFinMethT == E_sFinMethT.ARM)
                {
                    if (dataLoan.sArmIndexT == E_sArmIndexT.LeaveBlank && dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LeaveBlank)
                    {
                        AddError(uld, "sArmIndexT, sFreddieArmIndexT", "sFinMethT = ARM", "Forms:: ARM Program Disclosure");
                    }
                    if (string.IsNullOrEmpty(dataLoan.sArmIndexLeadDays_rep))
                    {
                        AddError(uld, "sArmIndexLeadDays", "sFinMethT = ARM", "Forms:: ARM Program Disclosure");
                    }
                    if (dataLoan.sNoteIR == 0)
                    {
                        AddError(uld, "sNoteIR", "sFinMethT = ARM", "GFE");
                    }
                    if ((dataLoan.sRAdjLifeCapR + dataLoan.sNoteIR) == 0)
                    {
                        AddError(uld, "sRAdjLifeCapR+sNoteIR", "sFinMethT = ARM", "GFE");
                    }
                    //if (dataLoan.sRAdjFloorR == 0) { AddError(uld, "sRAdjFloorR", "sFinMethT = ARM", "Truth In Lending"); }
                    if (dataLoan.sRAdjRoundToR == 0)
                    {
                        AddError(uld, "sRAdjRoundToR", "sFinMethT = ARM", "Truth In Lending");
                    }
                    //if (dataLoan.sRAdjRoundT == 0) { AddError(uld, "sRAdjRoundT", "sFinMethT = ARM", "Truth In Lending"); }
                    if (dataLoan.sRAdjMarginR == 0)
                    {
                        AddError(uld, "sRAdjMarginR", "sFinMethT = ARM", "Truth In Lending");
                    }
                    if (dataLoan.sRAdj1stCapR == 0)
                    {
                        AddError(uld, "sRAdj1stCapR", "sFinMethT = ARM", "GFE");
                    }
                    if (dataLoan.sRAdjCapMon == 0)
                    {
                        AddError(uld, "sRAdjCapMon", "sFinMethT = ARM", "GFE");
                    }
                    if (dataLoan.sRAdjCapR == 0)
                    {
                        AddError(uld, "sRAdjCapR", "sFinMethT = ARM", "GFE");
                    }
                    if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                    {
                        if (string.IsNullOrEmpty(dataLoan.sGseDeliveryPlanId))
                        {
                            AddError(uld, "sGseDeliveryPlanId", "sFinMethT = ARM", "Shipping:: GSE Delivery");
                        }
                    }
                }


                if (dataLoan.sGseDeliveryLoanDeliveryT == E_sGseDeliveryLoanDeliveryT.MBS)
                {
                    if (string.IsNullOrEmpty(dataLoan.sGseDeliveryBaseGuaranteeF_rep) && string.IsNullOrEmpty(dataLoan.sGseDeliveryGuaranteeF_rep)
                        && string.IsNullOrEmpty(dataLoan.sGseDeliveryRemittanceNumOfDays_rep) && string.IsNullOrEmpty(dataLoan.sGseDeliveryScheduleUPBAmt_rep))
                    {
                        AddError(ulddExportError, "sGseDeliveryBaseGuaranteeF,sGseDeliveryGuaranteeF,sGseDeliveryRemittanceNumOfDays,sGseDeliveryScheduleUPBAmt", "sGseDeliveryLoanDeliveryT == MBS", "Shipping:: GSE Delivery");
                    }
                }
                if (dataLoan.sGseDeliveryLoanDeliveryT == E_sGseDeliveryLoanDeliveryT.WholeLoan)
                {
                    if (string.IsNullOrEmpty(dataLoan.sGseDeliveryCommitmentId))
                    {
                        AddError(uld, "sGseDeliveryCommitmentId", "sGseDeliveryLoanDeliveryT = Whole Loan", "Shipping:: GSE Delivery");
                    }
                    if (E_sGseDeliveryTargetT.FannieMae == dataLoan.sGseDeliveryTargetT && string.IsNullOrEmpty(dataLoan.sGseDeliveryPayeeId))
                    {
                        AddError(uld, "sGseDeliveryPayeeId", "sGseDeliveryLoanDeliveryT = Whole Loan", "Shipping:: GSE Delivery");
                    }
                }

                if (E_sGseSpT.Condominium == dataLoan.sGseSpT || E_sGseSpT.Cooperative == dataLoan.sGseSpT ||
                               E_sGseSpT.DetachedCondominium == dataLoan.sGseSpT || E_sGseSpT.HighRiseCondominium == dataLoan.sGseSpT
                                || E_sGseSpT.ManufacturedHomeCondominium == dataLoan.sGseSpT)
                {
                    // DT iOPM 148528 - Old audit was too aggressive. Bringing into conformance with actual requirements.
                    if (E_sGseDeliveryTargetT.FannieMae == dataLoan.sGseDeliveryTargetT)
                    {
                        if (dataLoan.sSpProjectStatusT == E_sSpProjectStatusT.LeaveBlank && string.IsNullOrEmpty(dataLoan.sCpmProjectId))
                        {
                            AddError(uld, "sSpProjectStatusT", "sGseSpT is a Condo/Coop/Attached PUD type and no CPM Project ID has been entered", "Subject Property:: Details");
                        }
                        //if (string.IsNullOrEmpty(dataLoan.sCpmProjectId)) { AddError(uld, "sCpmProjectId", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                        if (dataLoan.sSpProjectAttachmentT == E_sSpProjectAttachmentT.LeaveBlank && string.IsNullOrEmpty(dataLoan.sCpmProjectId))
                        {
                            AddError(uld, "sSpProjectAttachmentT", "sGseSpT is a Condo/Coop/Attached PUD type and no CPM Project ID has been entered", "Subject Property:: Details");
                        }
                        //if (dataLoan.sSpProjectClassFannieT == E_sSpProjectClassFannieT.LeaveBlank) { AddError(uld, "sSpProjectClassFannieT", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                        //if (dataLoan.sSpProjectClassFreddieT == E_sSpProjectClassFreddieT.LeaveBlank) { AddError(uld, "sSpProjectClassFreddieT", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                        if (dataLoan.sSpProjectDesignT == E_sSpProjectDesignT.LeaveBlank && E_sSpValuationMethodT.None != dataLoan.sSpValuationMethodT)
                        {
                            AddError(uld, "sSpProjectDesignT", "sGseSpT is a Condo/Coop/Attached PUD type and the property valuation method is not 'None'", "Subject Property:: Details");
                        }
                        if (string.IsNullOrEmpty(dataLoan.sSpProjectDwellingUnitCount_rep) && E_sSpValuationMethodT.None != dataLoan.sSpValuationMethodT && string.IsNullOrEmpty(dataLoan.sCpmProjectId))
                        {
                            AddError(uld, "sSpProjectDwellingUnitCount", "sGseSpT is a Condo/Coop/Attached PUD type, no CPM Project ID has been entered, and the property valuation method is not 'None'", "Subject Property:: Details");
                        }
                        if (string.IsNullOrEmpty(dataLoan.sSpProjectDwellingUnitSoldCount_rep) && E_sSpValuationMethodT.None != dataLoan.sSpValuationMethodT && string.IsNullOrEmpty(dataLoan.sCpmProjectId))
                        {
                            AddError(uld, "sSpProjectDwellingUnitSoldCount", "sGseSpT is a Condo/Coop/Attached PUD type, no CPM Project ID has been entered, and the property valuation method is not 'None'", "Subject Property:: Details");
                        }
                        if (string.IsNullOrEmpty(dataLoan.sProjNm))
                        {
                            AddError(uld, "sProjNm", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details");
                        }
                    }
                    else if (E_sGseDeliveryTargetT.FreddieMac == dataLoan.sGseDeliveryTargetT)
                    {
                        bool isCondoNotExemptFromReview = dataLoan.sGseSpT != E_sGseSpT.Cooperative
                                && !CalculateProjectClassificationIdentifier(dataLoan).Equals("ExemptFromReview", StringComparison.OrdinalIgnoreCase);
                        bool isCooperative = dataLoan.sGseSpT == E_sGseSpT.Cooperative;

                        if (isCondoNotExemptFromReview)
                        {
                            if (dataLoan.sSpProjectStatusT == E_sSpProjectStatusT.LeaveBlank)
                            {
                                AddError(uld, "sSpProjectStatusT", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access or Same Servicer", "Subject Property:: Details");
                            }
                            if (dataLoan.sSpProjectAttachmentT == E_sSpProjectAttachmentT.LeaveBlank)
                            {
                                AddError(uld, "sSpProjectAttachmentT", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access or Same Servicer", "Subject Property:: Details");
                            }
                            if (dataLoan.sSpProjectDesignT == E_sSpProjectDesignT.LeaveBlank)
                            {
                                AddError(uld, "sSpProjectDesignT", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access or Same Servicer", "Subject Property:: Details");
                            }
                        }
                        if (isCooperative && dataLoan.sSpProjectAttachmentT == E_sSpProjectAttachmentT.LeaveBlank && dataLoan.sSpProjectDesignT == E_sSpProjectDesignT.LeaveBlank)
                        {
                            AddError(uld, "sSpProjectAttachmentT OR sSpProjectDesignT", "sGseSpT is a Cooperative", "Subject Property:: Details");
                        }
                        if (isCondoNotExemptFromReview || isCooperative)
                        {
                            if (string.IsNullOrEmpty(dataLoan.sSpProjectDwellingUnitCount_rep))
                            {
                                AddError(uld, "sSpProjectDwellingUnitCount", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access/Same Servicer or Coop", "Subject Property:: Details");
                            }
                            if (string.IsNullOrEmpty(dataLoan.sSpProjectDwellingUnitSoldCount_rep))
                            {
                                AddError(uld, "sSpProjectDwellingUnitSoldCount", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access/Same Servicer or Coop", "Subject Property:: Details");
                            }
                            if (string.IsNullOrEmpty(dataLoan.sProjNm))
                            {
                                AddError(uld, "sProjNm", "sGseSpT is a Condo and FNMA/FHLMC/FHLB Refinance Program is not Open Access/Same Servicer or Coop", "Subject Property:: Details");
                            }
                        }
                        //if (string.IsNullOrEmpty(dataLoan.sCpmProjectId)) { AddError(uld, "sCpmProjectId", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                        //if (dataLoan.sSpProjectClassFannieT == E_sSpProjectClassFannieT.LeaveBlank) { AddError(uld, "sSpProjectClassFannieT", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                        //if (dataLoan.sSpProjectClassFreddieT == E_sSpProjectClassFreddieT.LeaveBlank) { AddError(uld, "sSpProjectClassFreddieT", "sGseSpT is a Condo/Coop/Attached PUD type", "Subject Property:: Details"); }
                    }
                }

                if (((dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                    || (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                    && (dataLoan.sGseSpT == E_sGseSpT.Condominium
                        || dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium
                        || dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium
                        || dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
                        || dataLoan.sGseSpT == E_sGseSpT.Cooperative
                        || dataLoan.sGseSpT == E_sGseSpT.PUD))
                {
                    var hoaAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.HomeOwnerAssociation, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (hoaAgent.IsValid && string.IsNullOrEmpty(hoaAgent.TaxId))
                    {
                        AddError(uld, "Homeowner Association Tax ID", "sGseSpT is a Condo, Cooperative, or PUD and there is a Homeowner's Association Agent", "Agents");
                    }
                }

                if (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                {
                    if (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.LeaveBlank)
                    {
                        AddError(uld, "sGseRefPurposeT", "sLPurposeT is Refinance Cashout", "Forms:: 1008 Single");
                    }
                }

                if (dataLoan.sLT == E_sLT.FHA)
                {
                    if (string.IsNullOrEmpty(dataLoan.sFHAHousingActSection))
                    {
                        AddError(uld, "sFHAHousingActSection", "sLT = FHA", "FHA:: FHA Addendum");
                    }
                }

                if (dataLoan.sMiInsuranceT != E_sMiInsuranceT.None)
                {
                    if (dataLoan.sMiCompanyNmT == E_sMiCompanyNmT.CMG)
                    {
                        AddCustomError(uld, "sMiCompanyNmT", "CMG is no longer a valid MI company value.", "Insuring:: MI Policy");
                    }
                    if (dataLoan.sMiCompanyNmT == E_sMiCompanyNmT.SONYMA)
                    {
                        AddCustomError(uld, "sMiCompanyNmT", "SONYMA is no longer a valid MI company value.", "Insuring:: MI Policy");
                    }
                    if (string.IsNullOrEmpty(dataLoan.sMiCertId))
                    {
                        AddError(uld, "sMiCertId", "sMiInsuranceT != Blank/None", "Insuring:: MI Policy");
                    }
                    if (dataLoan.sMiCompanyNmT == E_sMiCompanyNmT.LeaveBlank)
                    {
                        AddError(uld, "sMiCompanyNmT", "sMiInsuranceT != Blank/None", "Insuring:: MI Policy");
                    }
                    if (string.IsNullOrEmpty(dataLoan.sMiLenderPaidCoverage_rep))
                    {
                        AddError(uld, "sMiLenderPaidCoverage", "sMiInsuranceT != Blank/None", "Insuring:: MI Policy");
                    }
                }
                else if (dataLoan.sLT == E_sLT.Conventional && dataLoan.sMiReasonForAbsenceT == E_sMiReasonForAbsenceT.None)
                {
                    AddError(uld, "sMiReasonForAbsenceT", "sMiInsuranceT = Blank/None", "Insuring:: MI Policy");
                }

                // 6/9/2014 AV - 183847 Remove ULDD audit rule requiring sMiLenderPaidAdj when LPMI, always export LenderPaidMIInterestRateAdjustmentPercent if LPMI
                //if (dataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
                //{
                //    if (string.IsNullOrEmpty(dataLoan.sMiLenderPaidAdj_rep)) { AddError(uld, "sMiLenderPaidAdj", "sMiInsuranceT != Lender Paid", "Insuring:: MI Policy"); }
                //}

                if (dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.DURefiPlus || dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.RefiPlus
                    || dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiOpenAccess
                    || dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiSameServicer)
                {
                    if (string.IsNullOrEmpty(dataLoan.sSpGseLoanNumCurrentLoan) && E_sSpGseRefiProgramT.DURefiPlus != dataLoan.sSpGseRefiProgramT)
                    {
                        AddError(uld, "sSpGseLoanNumCurrentLoan", "sSpGseRefiProgramT is a HARP type", "Subject Property:: Details");
                    }
                }

                if ((dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiOpenAccess
                    || dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiSameServicer)
                    && dataLoan.sMiReasonForAbsenceT != E_sMiReasonForAbsenceT.NoMIBasedOnMortgageBeingRefinanced)
                {
                    AddError(uld, "sMiReasonForAbsenceT = NoMIBasedOnMortgageBeingRefinanced", "sSpGseRefiProgramT = ReliefRefiOpenAccess OR ReliefRefiSameServicer", "Insuring:: MI Policy");
                }

                if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.AutomatedValuationModel)
                {
                    if (dataLoan.sSpAvmModelT == 0)
                    {
                        AddError(uld, "sSpAvmModelT", "sSpValuationMethodT = Automated Valuation Model", "Subject Property:: Details");
                    }
                }
                if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.None)
                {
                    if (dataLoan.sSpGseCollateralProgramT == E_sSpGseCollateralProgramT.None)
                    {
                        AddError(uld, "sSpGseCollateralProgramT", "sSpValuationMethodT = None", "Subject Property:: Details");
                    }
                }
                if ((dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && dataLoan.sLT == E_sLT.Conventional && (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.DriveBy || dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FullAppraisal)) ||
                    (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.DriveBy || dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.PriorAppraisalUsed || dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FullAppraisal)))
                {
                    if (string.IsNullOrEmpty(dataLoan.sSpAppraisalId))
                    {
                        AddError(uld, "sSpAppraisalId", "sSpValuationMethodT is an appraisal type", "Subject Property:: Details");
                    }
                    var appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (appraiser != CAgentFields.Empty)
                    {
                        if (string.IsNullOrEmpty(appraiser.LicenseNumOfAgent))
                        {
                            AddError(ulddExportError, "Appraiser Licence Number", "sSpValuationMethodT is an appraisal type", "Status:: Agents:: Appraiser");
                        }
                    }
                    else
                    {
                        AddError(ulddExportError, "Appraiser (need one)", "sSpValuationMethodT is an appraisal type", "Status:: Agents");
                    }
                }




                // sSpValuationMethodT is not none
                if (dataLoan.sSpValuationMethodT != E_sSpValuationMethodT.None && dataLoan.sSpValuationMethodT != E_sSpValuationMethodT.LeaveBlank)
                {
                    if (string.IsNullOrEmpty(dataLoan.sSpValuationEffectiveD_rep))
                    {
                        AddError(uld, "sSpValuationEffectiveD", "sSpValuationMethodT is not none", "Subject Property:: Details");
                    }
                }


                // There is a linked loan // ignore for now.
                //Couldn't find sFinMethT of linked loan
                //Couldn't find sDocumentNoteD from linked loan
                //Couldn't find sTerm of linked loan
                //Couldn't find sSchedDueD1 of linked loan
                //Couldn't find sFinalLAmt of the linked loan
                //Couldn't find sLT of linked loan
                // sServicingCollectedAndDisbursedFunds_Principal of the linked loan (there might be existing data layer for this). 
                //if (dataLoan.sFinalLAmt == 0) { AddError(uld, "SFinalLAmt", "sServicingCollectedAndDisbursedFunds_Principal of the linked loan (there might be existing data layer for this).", "There is a linked loan"); }
            }
            catch (CBaseException c)
            {
                ulddExportError.Add(new UlddExportErrorItem("exception", "Exception: " + c.UserMessage, "Please find using tool"));
                Tools.LogError(c);
            }



            if (ulddExportError.ErrorList.Count() == 0)
            {
                return null;
            }
            else
            {
                return ulddExportError;
            }
        }

        /// <summary>
        /// Maps a loan ID to a ULDD phase 3 part to use for that loan.
        /// </summary>
        private Dictionary<Guid, UlddPhase3Part> Phase3Parts = new Dictionary<Guid, UlddPhase3Part>();

        public bool Verify(out List<UlddExportError> errorList)
        {
            errorList = new List<UlddExportError>();

            if (m_dataLoanList.Count > 0)
            {
                var firstType = m_dataLoanList[0].sGseDeliveryTargetT;
                var firstLNm = m_dataLoanList[0].sLNm;
                foreach (CPageData dataLoan in m_dataLoanList)
                {
                    UlddExportError error = VerifyLoan(dataLoan, firstType, firstLNm);
                    if (error != null)
                    {
                        errorList.Add(error);
                    }
                }
            }


            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }
        public void Export(string path)
        {
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
            {
                using (XmlTextWriter writer = new XmlTextWriter(fs.Stream, new UTF8Encoding(false)))
                {
                    writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                    Message message = CreateMessage();
                    message.WriteXml(writer);
                }
            };

            BinaryFileHelper.OpenNew(path, writeHandler);
        }
        public string Export()
        {
            Message message = CreateMessage();

            string xml;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

                message.WriteXml(writer);

                writer.Flush();

                xml = System.Text.Encoding.ASCII.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }
            Tools.LogInfo("UlddExporter.Export " + m_dataLoanList.Count + " loan(s).");

            return xml;

        }

        protected virtual AboutVersion CreateAboutVersion()
        {
            AboutVersion aboutVersion = new AboutVersion();


            if (m_dataLoanList.Count > 0)
            {
                var dataLoan = m_dataLoanList[0];

                if (m_dataLoanList[0].sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                {
                    aboutVersion.AboutVersionIdentifier = Phase3Parts[dataLoan.sLId] == UlddPhase3Part.UsePhase2Instead ? AboutVersion_Fnma_Phase2 : AboutVersion_Fnma_Phase3;
                }
                else if (m_dataLoanList[0].sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
                {
                    aboutVersion.AboutVersionIdentifier = Phase3Parts[dataLoan.sLId] == UlddPhase3Part.UsePhase2Instead ? AboutVersion_Freddie_Phase2 : AboutVersion_Freddie_Phase3;
                }
                else
                {
                    aboutVersion.AboutVersionIdentifier = AboutVersion_Fnma_Phase2;
                }

            }

            aboutVersion.CreatedDatetime = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
            // aboutVersion.DataVersionIdentifier = ;
            // aboutVersion.DataVersionName = ;
            // aboutVersion.SequenceNumber = ;

            return aboutVersion;
        }

        private AboutVersions CreateAboutVersions()
        {
            AboutVersions aboutVersions = new AboutVersions();

            aboutVersions.AboutVersionList.Add(CreateAboutVersion());

            return aboutVersions;
        }

        private Ach CreateAch()
        {
            Ach ach = new Ach();

            // ach.ACH_ABARoutingAndTransitIdentifier = ;
            // ach.ACHAccountType = ;
            // ach.ACHAdditionalEscrowAmount = ;
            // ach.ACHAdditionalPrincipalAmount = ;
            // ach.ACHBankAccountIdentifier = ;
            // ach.ACHDraftAfterDueDateDayCount = ;
            // ach.ACHDraftFrequencyType = ;
            // ach.ACHDraftingPartyName = ;
            // ach.ACHInstitutionTelegraphicAbbreviationName = ;
            // ach.ACHPendingDraftEffectiveDate = ;
            // ach.ACHReceiverSubaccountName = ;
            // ach.ACHType = ;
            // ach.ACHWireAmount = ;

            return ach;
        }

        private Addendum CreateAddendum()
        {
            Addendum addendum = new Addendum();

            // addendum.GraduatedPaymentAddendumIndicator = ;
            // addendum.GrowingEquityAddendumIndicator = ;

            return addendum;
        }
        private Address CreateAddress(string addr, string city, string state, string postalCode)
        {
            if (string.IsNullOrEmpty(addr) && string.IsNullOrEmpty(city) && string.IsNullOrEmpty(state)
                && string.IsNullOrEmpty(postalCode))
            {
                return null;
            }
            Address address = new Address();
            address.AddressLineText = addr;
            address.CityName = city;
            address.PostalCode = postalCode;
            address.StateCode = state;

            return address;
        }
        private Address CreateAddress()
        {
            Address address = new Address();

            // address.AddressAdditionalLineText = ;
            // address.AddressLineText = ;
            // address.AddressType = ;
            // address.AddressTypeOtherDescription = ;
            // address.AddressUnitDesignatorType = ;
            // address.AddressUnitDesignatorTypeOtherDescription = ;
            // address.AddressUnitIdentifier = ;
            // address.CarrierRouteCode = ;
            // address.CityName = ;
            // address.CountryCode = ;
            // address.CountryName = ;
            // address.CountyCode = ;
            // address.CountyName = ;
            // address.DeliveryPointBarCodeCheckNumber = ;
            // address.DeliveryPointBarCodeNumber = ;
            // address.MailStopCode = ;
            // address.PlusFourZipCodeNumber = ;
            // address.PostalCode = ;
            // address.PostOfficeBoxIdentifier = ;
            // address.RuralRouteIdentifier = ;
            // address.StateCode = ;
            // address.StateName = ;
            // address.StreetName = ;
            // address.StreetPostDirectionalText = ;
            // address.StreetPreDirectionalText = ;
            // address.StreetPrimaryNumberText = ;
            // address.StreetSuffixText = ;
            // address.SequenceNumber = ;

            return address;
        }

        private Addresses CreateAddresses()
        {
            Addresses addresses = new Addresses();

            // addresses.AddressList.Add(CreateAddress());

            return addresses;
        }

        private Adjustment CreateAdjustmentCurrent(CPageData dataLoan)
        {
            Adjustment adjustment = new Adjustment();

            adjustment.RateOrPaymentChangeOccurrences = CreateRateOrPaymentChangeOccurencesCurrent(dataLoan);

            return adjustment;
        }
        private RateOrPaymentChangeOccurrences CreateRateOrPaymentChangeOccurencesCurrent(CPageData dataLoan)
        {
            RateOrPaymentChangeOccurrences rateOrPaymentChangeOccurences = new RateOrPaymentChangeOccurrences();
            rateOrPaymentChangeOccurences.RateOrPaymentChangeOccurrenceList.Add(CreateRateOrPaymentChangeOccurenceCurrent(dataLoan));
            return rateOrPaymentChangeOccurences;
        }
        private RateOrPaymentChangeOccurrence CreateRateOrPaymentChangeOccurenceCurrent(CPageData dataLoan)
        {
            RateOrPaymentChangeOccurrence rateOrPaymentChangeOccurence = new RateOrPaymentChangeOccurrence();

            if (dataLoan.sAmortTable != null && dataLoan.sAmortTable.nRows > 0)
            {
                decimal rate = dataLoan.sAmortTable.Items[0].Rate;
                for (int i = 1; i < dataLoan.sAmortTable.nRows; i++)
                {
                    AmortItem item = dataLoan.sAmortTable.Items[i];
                    if (item.Rate != rate && DateTime.Now < item.DueDate.DateTimeForComputation.AddMonths(-1))
                    {
                        rateOrPaymentChangeOccurence.NextRateAdjustmentEffectiveDate = dataLoan.m_convertLos.ToDateTimeString(item.DueDate.DateTimeForComputation.AddMonths(-1));
                        break;
                    }

                    rate = item.Rate;
                }
            }
            return rateOrPaymentChangeOccurence;
        }
        private Adjustment CreateAdjustment(CPageData dataLoan)
        {
            Adjustment adjustment = new Adjustment();

            // adjustment.ConversionAdjustment = CreateConversionAdjustment();
            adjustment.InterestRateAdjustment = CreateInterestRateAdjustment(dataLoan);
            // adjustment.PrincipalAndInterestPaymentAdjustment = CreatePrincipalAndInterestPaymentAdjustment();
            // adjustment.RateOrPaymentChangeOccurrences = CreateRateOrPaymentChangeOccurrences();

            return adjustment;
        }

        private AffordableLending CreateAffordableLending()
        {
            AffordableLending affordableLending = new AffordableLending();

            // affordableLending.FNMCommunityLendingProductType = ;
            // affordableLending.FNMCommunityLendingProductTypeOtherDescription = ;
            // affordableLending.FNMCommunitySecondsIndicator = ;
            // affordableLending.FNMNeighborsMortgageEligibilityIndicator = ;
            // affordableLending.HUDIncomeLimitAdjustmentPercent = ;
            // affordableLending.HUDLendingIncomeLimitAmount = ;
            // affordableLending.HUDMedianIncomeAmount = ;

            return affordableLending;
        }

        private Alias CreateAlias()
        {
            Alias alias = new Alias();

            // alias.AliasDetail = CreateAliasDetail();
            // alias.Name = CreateName();
            // alias.TaxpayerIdentifiers = CreateTaxpayerIdentifiers();
            // alias.SequenceNumber = ;

            return alias;
        }

        private Aliases CreateAliases()
        {
            Aliases aliases = new Aliases();

            // aliases.AliasList.Add(CreateAlias());

            return aliases;
        }

        private AliasDetail CreateAliasDetail()
        {
            AliasDetail aliasDetail = new AliasDetail();

            // aliasDetail.AliasAccountIdentifier = ;
            // aliasDetail.AliasCreditorName = ;
            // aliasDetail.AliasTaxReturnIndicator = ;
            // aliasDetail.AliasType = ;
            // aliasDetail.AliasTypeOtherDescription = ;

            return aliasDetail;
        }

        private AllongeToNote CreateAllongeToNote()
        {
            AllongeToNote allongeToNote = new AllongeToNote();

            // allongeToNote.AllongeToNoteDate = ;
            // allongeToNote.AllongeToNoteExecutedByDescription = ;
            // allongeToNote.AllongeToNoteInFavorOfDescription = ;
            // allongeToNote.AllongeToNotePayToTheOrderOfDescription = ;
            // allongeToNote.AllongeToNoteWithoutRecourseDescription = ;

            return allongeToNote;
        }

        private Amortization CreateAmortization(CPageData dataLoan)
        {
            Amortization amortization = new Amortization();

            amortization.AmortizationRule = CreateAmortizationRule(dataLoan);
            // amortization.AmortizationScheduleItems = CreateAmortizationScheduleItems();

            return amortization;
        }

        private AmortizationRule CreateAmortizationRule(CPageData dataLoan)
        {
            AmortizationRule amortizationRule = new AmortizationRule();

            // amortizationRule.LoanAmortizationMaximumTermMonthsCount = ;
            amortizationRule.LoanAmortizationPeriodCount = dataLoan.sTerm_rep;
            amortizationRule.LoanAmortizationPeriodType = LoanAmortizationPeriodType.Month;
            amortizationRule.LoanAmortizationType = ToMismo(dataLoan.sFinMethT);
            // amortizationRule.LoanAmortizationTypeOtherDescription = ;

            return amortizationRule;
        }

        protected LoanAmortizationType ToMismo(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed:
                    return LoanAmortizationType.Fixed;
                case E_sFinMethT.ARM:
                    return LoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Graduated:
                    return LoanAmortizationType.GraduatedPaymentARM;
                default:
                    throw new UnhandledEnumException(sFinMethT);
            }
            throw new NotImplementedException();
        }

        private AmortizationScheduleItem CreateAmortizationScheduleItem()
        {
            AmortizationScheduleItem amortizationScheduleItem = new AmortizationScheduleItem();

            // amortizationScheduleItem.AmortizationScheduleEndingBalanceAmount = ;
            // amortizationScheduleItem.AmortizationScheduleInterestPortionAmount = ;
            // amortizationScheduleItem.AmortizationScheduleInterestRatePercent = ;
            // amortizationScheduleItem.AmortizationScheduleLoanToValuePercent = ;
            // amortizationScheduleItem.AmortizationScheduleMIPaymentAmount = ;
            // amortizationScheduleItem.AmortizationSchedulePaymentAmount = ;
            // amortizationScheduleItem.AmortizationSchedulePaymentDueDate = ;
            // amortizationScheduleItem.AmortizationSchedulePaymentNumber = ;
            // amortizationScheduleItem.AmortizationSchedulePrincipalPortionAmount = ;
            // amortizationScheduleItem.SequenceNumber = ;

            return amortizationScheduleItem;
        }

        private AmortizationScheduleItems CreateAmortizationScheduleItems()
        {
            AmortizationScheduleItems amortizationScheduleItems = new AmortizationScheduleItems();

            // amortizationScheduleItems.AmortizationScheduleItemList.Add(CreateAmortizationScheduleItem());

            return amortizationScheduleItems;
        }

        private ApplicationInterviewer CreateApplicationInterviewer()
        {
            ApplicationInterviewer applicationInterviewer = new ApplicationInterviewer();

            // applicationInterviewer.ContactPoints = CreateContactPoints();
            // applicationInterviewer.InterviewerDetail = CreateInterviewerDetail();
            // applicationInterviewer.InterviewerEmployer = CreateInterviewerEmployer();
            // applicationInterviewer.Name = CreateName();

            return applicationInterviewer;
        }

        private Appraiser CreateAppraiser()
        {
            Appraiser appraiser = new Appraiser();

            // appraiser.AppraiserDetail = CreateAppraiserDetail();
            // appraiser.AppraiserLicense = CreateAppraiserLicense();

            return appraiser;
        }

        private AppraiserDetail CreateAppraiserDetail()
        {
            AppraiserDetail appraiserDetail = new AppraiserDetail();

            // appraiserDetail.AppraiserCompanyName = ;

            return appraiserDetail;
        }

        private AppraiserLicense CreateAppraiserLicense()
        {
            AppraiserLicense appraiserLicense = new AppraiserLicense();

            // appraiserLicense.AppraiserLicenseExpirationDate = ;
            // appraiserLicense.AppraiserLicenseIdentifier = ;
            // appraiserLicense.AppraiserLicenseStateName = ;
            // appraiserLicense.AppraiserLicenseType = ;
            // appraiserLicense.AppraiserLicenseTypeOtherDescription = ;

            return appraiserLicense;
        }

        private AppraiserSupervisor CreateAppraiserSupervisor()
        {
            AppraiserSupervisor appraiserSupervisor = new AppraiserSupervisor();

            // appraiserSupervisor.AppraiserLicense = CreateAppraiserLicense();
            // appraiserSupervisor.AppraiserSupervisorDetail = CreateAppraiserSupervisorDetail();

            return appraiserSupervisor;
        }

        private AppraiserSupervisorDetail CreateAppraiserSupervisorDetail()
        {
            AppraiserSupervisorDetail appraiserSupervisorDetail = new AppraiserSupervisorDetail();

            // appraiserSupervisorDetail.AppraiserSupervisorCompanyName = ;

            return appraiserSupervisorDetail;
        }

        private Asset CreateAsset()
        {
            Asset asset = new Asset();

            // asset.AssetDetail = CreateAssetDetail();
            // asset.AssetDocumentations = CreateAssetDocumentations();
            // asset.AssetHolder = CreateAssetHolder();
            // asset.Verification = CreateVerification();
            // asset.SequenceNumber = ;

            return asset;
        }

        private Assets CreateAssets()
        {
            Assets assets = new Assets();

            // assets.AssetList.Add(CreateAsset());
            // assets.OwnedProperties = CreateOwnedProperties();

            return assets;
        }

        private AssetDetail CreateAssetDetail()
        {
            AssetDetail assetDetail = new AssetDetail();

            // assetDetail.AssetAccountIdentifier = ;
            // assetDetail.AssetAccountInNameOfDescription = ;
            // assetDetail.AssetAccountType = ;
            // assetDetail.AssetAccountTypeOtherDescription = ;
            // assetDetail.AssetCashOrMarketValueAmount = ;
            // assetDetail.AssetDescription = ;
            // assetDetail.AssetType = ;
            // assetDetail.AssetTypeOtherDescription = ;
            // assetDetail.AutomobileMakeDescription = ;
            // assetDetail.AutomobileModelYear = ;
            // assetDetail.LifeInsuranceFaceValueAmount = ;
            // assetDetail.StockBondMutualFundShareCount = ;
            // assetDetail.VerifiedIndicator = ;

            return assetDetail;
        }

        private AssetDocumentation CreateAssetDocumentation()
        {
            AssetDocumentation assetDocumentation = new AssetDocumentation();

            // assetDocumentation.AssetDocumentationType = ;
            // assetDocumentation.AssetDocumentationTypeOtherDescription = ;
            // assetDocumentation.AssetVerificationRangeCount = ;
            // assetDocumentation.AssetVerificationRangeType = ;
            // assetDocumentation.AssetVerificationRangeTypeOtherDescription = ;
            // assetDocumentation.DocumentationStateType = ;
            // assetDocumentation.DocumentationStateTypeOtherDescription = ;
            // assetDocumentation.SequenceNumber = ;

            return assetDocumentation;
        }

        private AssetDocumentations CreateAssetDocumentations()
        {
            AssetDocumentations assetDocumentations = new AssetDocumentations();

            // assetDocumentations.AssetDocumentationList.Add(CreateAssetDocumentation());

            return assetDocumentations;
        }

        private AssetHolder CreateAssetHolder()
        {
            AssetHolder assetHolder = new AssetHolder();

            // assetHolder.Address = CreateAddress();
            // assetHolder.ContactPoints = CreateContactPoints();
            // assetHolder.Name = CreateName();

            return assetHolder;
        }

        private Assignment CreateAssignment()
        {
            Assignment assignment = new Assignment();

            // assignment.AssignmentDate = ;

            return assignment;
        }

        private Assumability CreateAssumability()
        {
            Assumability assumability = new Assumability();

            // assumability.AssumabilityOccurrences = CreateAssumabilityOccurrences();
            // assumability.AssumabilityRule = CreateAssumabilityRule();

            return assumability;
        }

        private AssumabilityOccurrence CreateAssumabilityOccurrence()
        {
            AssumabilityOccurrence assumabilityOccurrence = new AssumabilityOccurrence();

            // assumabilityOccurrence.AssumptionDate = ;
            // assumabilityOccurrence.SequenceNumber = ;

            return assumabilityOccurrence;
        }

        private AssumabilityOccurrences CreateAssumabilityOccurrences()
        {
            AssumabilityOccurrences assumabilityOccurrences = new AssumabilityOccurrences();

            // assumabilityOccurrences.AssumabilityOccurrenceList.Add(CreateAssumabilityOccurrence());

            return assumabilityOccurrences;
        }

        private AssumabilityRule CreateAssumabilityRule()
        {
            AssumabilityRule assumabilityRule = new AssumabilityRule();

            // assumabilityRule.AssumabilityBeginDate = ;
            // assumabilityRule.AssumabilityTermMonthsCount = ;
            // assumabilityRule.AssumabilityType = ;
            // assumabilityRule.AssumabilityTypeOtherDescription = ;
            // assumabilityRule.ConditionsToAssumabilityIndicator = ;

            return assumabilityRule;
        }

        private AuditTrail CreateAuditTrail()
        {
            AuditTrail auditTrail = new AuditTrail();

            // auditTrail.AuditTrailEntries = CreateAuditTrailEntries();

            return auditTrail;
        }

        private AuditTrailEntries CreateAuditTrailEntries()
        {
            AuditTrailEntries auditTrailEntries = new AuditTrailEntries();

            // auditTrailEntries.AuditTrailEntryList.Add(CreateAuditTrailEntry());

            return auditTrailEntries;
        }

        private AuditTrailEntry CreateAuditTrailEntry()
        {
            AuditTrailEntry auditTrailEntry = new AuditTrailEntry();

            // auditTrailEntry.AuditTrailEntryDetail = CreateAuditTrailEntryDetail();
            // auditTrailEntry.AuditTrailEntryEvidence = CreateAuditTrailEntryEvidence();
            // auditTrailEntry.SequenceNumber = ;

            return auditTrailEntry;
        }

        private AuditTrailEntryDetail CreateAuditTrailEntryDetail()
        {
            AuditTrailEntryDetail auditTrailEntryDetail = new AuditTrailEntryDetail();

            // auditTrailEntryDetail.EntryDatetime = ;
            // auditTrailEntryDetail.EntryDescription = ;
            // auditTrailEntryDetail.EntryType = ;
            // auditTrailEntryDetail.EntryTypeOtherDescription = ;
            // auditTrailEntryDetail.PerformedByOrganizationName = ;
            // auditTrailEntryDetail.PerformedBySystemName = ;
            // auditTrailEntryDetail.PerformedBySystemEntryIdentifier = ;

            return auditTrailEntryDetail;
        }

        private AuditTrailEntryEvidence CreateAuditTrailEntryEvidence()
        {
            AuditTrailEntryEvidence auditTrailEntryEvidence = new AuditTrailEntryEvidence();

            // auditTrailEntryEvidence.ForeignObject = CreateForeignObject();

            return auditTrailEntryEvidence;
        }

        private Aus CreateAus()
        {
            Aus aus = new Aus();


            return aus;
        }

        /// <summary>
        /// Creates the AU container.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="ucdCaseFileId">UCD case file id if it should be sent. Don't populate this if you don't want to use it.</param>
        /// <returns></returns>
        private AutomatedUnderwriting CreateAutomatedUnderwriting(CPageData dataLoan, string ucdCaseFileId = null)
        {
            AutomatedUnderwriting automatedUnderwriting = new AutomatedUnderwriting();

            if (!string.IsNullOrEmpty(ucdCaseFileId))
            {
                automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = ucdCaseFileId;
            }
            else if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac || m_isLQAExport)
            {
                automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = dataLoan.sLpAusKey;
            }
            else
            {
                automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = dataLoan.sDuCaseId;
            }

            // automatedUnderwriting.AutomatedUnderwritingDecisionDatetime = ;
            // automatedUnderwriting.AutomatedUnderwritingEvaluationStatusDescription = ;
            // automatedUnderwriting.AutomatedUnderwritingMethodVersionIdentifier = ;
            // automatedUnderwriting.AutomatedUnderwritingProcessDescription = ;

            // automatedUnderwriting.AutomatedUnderwritingSystemDocumentWaiverIndicator = ;
            // automatedUnderwriting.AutomatedUnderwritingSystemResultValue = ;
            if (dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.NA
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ApproveEligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ApproveIneligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ReferEligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ReferIneligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_AcceptEligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_AcceptIneligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferEligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferIneligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible
    && dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible)
            {
                string desc = string.Empty;
                switch (dataLoan.sProd3rdPartyUwResultT)
                {
                    case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                        desc = "ApproveEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                        desc = "ApproveIneligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                        desc = "EAIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                        desc = "EAIIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                        desc = "EAIIIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                        desc = "ReferEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                        desc = "ReferIneligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                    case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                        desc = "ReferWithCaution";
                        break;
                    case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                    case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                        desc = "Accept";
                        break;
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                        desc = "CautionEligibleForAMinus";
                        break;
                    case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                    case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                        desc = "C1Caution";
                        break;
                    case E_sProd3rdPartyUwResultT.Lp_Refer:
                        desc = "Unknown";
                        break;
                    case E_sProd3rdPartyUwResultT.OutOfScope:
                        desc = "OutofScope";
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sProd3rdPartyUwResultT);
                }
                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = desc;
            }
            if (dataLoan.sIsDuUw)
            {
                automatedUnderwriting.AutomatedUnderwritingSystemType = AutomatedUnderwritingSystemType.DesktopUnderwriter;
            }
            else if (dataLoan.sIsLpUw)
            {
                if (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2 && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
                {
                    automatedUnderwriting.AutomatedUnderwritingSystemType = AutomatedUnderwritingSystemType.Other;
                    automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = "LoanProductAdvisor";
                }
                else
                {
                    automatedUnderwriting.AutomatedUnderwritingSystemType = AutomatedUnderwritingSystemType.LoanProspector;
                }
            }
            else if (dataLoan.sIsOtherUw)
            {
                automatedUnderwriting.AutomatedUnderwritingSystemType = AutomatedUnderwritingSystemType.Other;
            }
            // automatedUnderwriting.AutomatedUnderwritingSystemType = ;
            // automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = ;
            // automatedUnderwriting.DesktopUnderwriterRecommendationType = ;
            // automatedUnderwriting.FREPurchaseEligibilityType = ;
            // automatedUnderwriting.LoanProspectorClassificationDescription = ;
            // automatedUnderwriting.LoanProspectorDocumentationClassificationDescription = ;
            // automatedUnderwriting.SequenceNumber = ;

            return automatedUnderwriting;
        }

        private AutomatedUnderwritings CreateAutomatedUnderwritings(CPageData dataLoan)
        {
            AutomatedUnderwritings automatedUnderwritings = new AutomatedUnderwritings();

            var deliveryMarkedForInclusion = dataLoan.sUcdDeliveryCollection.FirstOrDefault(ucd => ucd.IncludeInUldd);
            automatedUnderwritings.AutomatedUnderwritingList.Add(this.CreateAutomatedUnderwriting(dataLoan, deliveryMarkedForInclusion?.CaseFileId));
            return automatedUnderwritings;
        }

        private Avm CreateAvm(CPageData dataLoan)
        {
            Avm avm = new Avm();

            switch (dataLoan.sSpAvmModelT)
            {
                case E_sSpAvmModelT.LeaveBlank:
                    avm = null;
                    break;
                case E_sSpAvmModelT.AutomatedPropertyService:
                    avm.AVMModelNameType = AVMModelNameType.AutomatedPropertyService;
                    break;
                case E_sSpAvmModelT.Casa:
                    avm.AVMModelNameType = AVMModelNameType.Casa;
                    break;
                case E_sSpAvmModelT.FidelityHansen:
                    avm.AVMModelNameType = AVMModelNameType.FidelityHansen;
                    break;
                case E_sSpAvmModelT.HomePriceAnalyzer:
                    avm.AVMModelNameType = AVMModelNameType.HomePriceAnalyzer;
                    break;
                case E_sSpAvmModelT.HomePriceIndex:
                    avm.AVMModelNameType = AVMModelNameType.HomePriceIndex;
                    break;
                case E_sSpAvmModelT.HomeValueExplorer:
                    avm.AVMModelNameType = AVMModelNameType.HomeValueExplorer;
                    break;
                case E_sSpAvmModelT.Indicator:
                    avm.AVMModelNameType = AVMModelNameType.Indicator;
                    break;
                case E_sSpAvmModelT.MTM:
                    avm.AVMModelNameType = AVMModelNameType.Other;
                    avm.AVMModelNameTypeOtherDescription = "MTM";
                    break;
                case E_sSpAvmModelT.NetValue:
                    avm.AVMModelNameType = AVMModelNameType.NetValue;
                    break;
                case E_sSpAvmModelT.Pass:
                    avm.AVMModelNameType = AVMModelNameType.Pass;
                    break;
                case E_sSpAvmModelT.PropertySurveyAnalysisReport:
                    avm.AVMModelNameType = AVMModelNameType.PropertySurveyAnalysisReport;
                    break;
                case E_sSpAvmModelT.ValueFinder:
                    avm.AVMModelNameType = AVMModelNameType.ValueFinder;
                    break;
                case E_sSpAvmModelT.ValuePoint:
                    avm.AVMModelNameType = AVMModelNameType.ValuePoint;
                    break;
                case E_sSpAvmModelT.ValuePoint4:
                    avm.AVMModelNameType = AVMModelNameType.ValuePoint4;
                    break;
                case E_sSpAvmModelT.ValuePointPlus:
                    avm.AVMModelNameType = AVMModelNameType.ValuePointPlus;
                    break;
                case E_sSpAvmModelT.ValueSure:
                    avm.AVMModelNameType = AVMModelNameType.ValueSure;
                    break;
                case E_sSpAvmModelT.ValueWizard:
                    avm.AVMModelNameType = AVMModelNameType.ValueWizard;
                    break;
                case E_sSpAvmModelT.ValueWizardPlus:
                    avm.AVMModelNameType = AVMModelNameType.ValueWizardPlus;
                    break;
                case E_sSpAvmModelT.VeroIndexPlus:
                    avm.AVMModelNameType = AVMModelNameType.VeroIndexPlus;
                    break;
                case E_sSpAvmModelT.VeroValue:
                    avm.AVMModelNameType = AVMModelNameType.VeroValue;
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sSpAvmModelT);
            }
            // avm.AVMConfidenceScoreIndicator = ;
            // avm.AVMConfidenceScoreValue = ;
            // avm.AVMDate = ;
            // avm.AVMIndicatedValueAmount = ;
            // avm.AVMHighValueRangeAmount = ;
            // avm.AVMLowValueRangeAmount = ;
            // avm.AVMMethodType = ;
            // avm.AVMMethodTypeOtherDescription = ;

            // avm.AVMServiceProviderName = ;
            // avm.AVMSystemName = ;
            // avm.SequenceNumber = ;

            return avm;
        }

        private Avms CreateAvms(CPageData dataLoan)
        {
            Avms avms = new Avms();

            avms.AvmList.Add(CreateAvm(dataLoan));

            return avms;
        }

        private Bankruptcy CreateBankruptcy()
        {
            Bankruptcy bankruptcy = new Bankruptcy();

            // bankruptcy.BankruptcyAdjustmentPercent = ;
            // bankruptcy.BankruptcyAssetsAmount = ;
            // bankruptcy.BankruptcyCaseIdentifier = ;
            // bankruptcy.BankruptcyChapterType = ;
            // bankruptcy.BankruptcyChapterTypeOtherDescription = ;
            // bankruptcy.BankruptcyExemptAmount = ;
            // bankruptcy.BankruptcyLiabilitiesAmount = ;
            // bankruptcy.BankruptcyPetitionDate = ;
            // bankruptcy.BankruptcyRepaymentPercent = ;
            // bankruptcy.BankruptcyResolutionDate = ;
            // bankruptcy.BankruptcyType = ;
            // bankruptcy.BankruptcyVoluntaryIndicator = ;

            return bankruptcy;
        }

        private BillingAddress CreateBillingAddress()
        {
            BillingAddress billingAddress = new BillingAddress();

            // billingAddress.Address = CreateAddress();
            // billingAddress.ContactPoints = CreateContactPoints();
            // billingAddress.Name = CreateName();

            return billingAddress;
        }

        private BirthInformation CreateBirthInformation()
        {
            BirthInformation birthInformation = new BirthInformation();

            // birthInformation.MothersMaidenName = ;
            // birthInformation.CityTownVillageOfBirthName = ;
            // birthInformation.CountryOfBirthName = ;
            // birthInformation.StateProvinceOfBirthName = ;

            return birthInformation;
        }

        private Borrower CreateBorrower()
        {
            Borrower borrower = new Borrower();

            // borrower.Bankruptcy = CreateBankruptcy();
            // borrower.BorrowerDetail = CreateBorrowerDetail();
            // borrower.CounselingConfirmation = CreateCounselingConfirmation();
            // borrower.CreditScores = CreateCreditScores();
            // borrower.CurrentIncome = CreateCurrentIncome();
            // borrower.Declaration = CreateDeclaration();
            // borrower.Dependents = CreateDependents();
            // borrower.Employers = CreateEmployers();
            // borrower.GovernmentBorrower = CreateGovernmentBorrower();
            // borrower.GovernmentMonitoring = CreateGovernmentMonitoring();
            // borrower.PresentHousingExpenses = CreatePresentHousingExpenses();
            // borrower.Residences = CreateResidences();
            // borrower.NearestLivingRelative = CreateNearestLivingRelative();
            // borrower.SolicitationPreferences = CreateSolicitationPreferences();
            // borrower.Summaries = CreateSummaries();

            return borrower;
        }

        private BorrowerDetail CreateBorrowerDetail()
        {
            BorrowerDetail borrowerDetail = new BorrowerDetail();

            // borrowerDetail.BorrowerAgeAtApplicationYearsCount = ;
            // borrowerDetail.BorrowerApplicationSignedDate = ;
            // borrowerDetail.BorrowerBirthDate = ;
            // borrowerDetail.BorrowerCanadianSocialInsuranceNumberIdentifier = ;
            // borrowerDetail.BorrowerCharacteristicType = ;
            // borrowerDetail.BorrowerCharacteristicTypeOtherDescription = ;
            // borrowerDetail.BorrowerClassificationType = ;
            // borrowerDetail.BorrowerIsCosignerIndicator = ;
            // borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator = ;
            // borrowerDetail.BorrowerQualifyingIncomeAmount = ;
            // borrowerDetail.BorrowerRelationshipTitleType = ;
            // borrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ;
            // borrowerDetail.BorrowerSameAsBuilderIndicator = ;
            // borrowerDetail.BorrowerTotalMortgagedPropertiesCount = ;
            // borrowerDetail.CreditFileBorrowerAgeYearsCount = ;
            // borrowerDetail.CreditReportAuthorizationIndicator = ;
            // borrowerDetail.CreditReportIdentifier = ;
            // borrowerDetail.DependentCount = ;
            // borrowerDetail.JointAssetLiabilityReportingType = ;
            // borrowerDetail.MaritalStatusType = ;
            // borrowerDetail.SchoolingYearsCount = ;
            // borrowerDetail.SignedAuthorizationToRequestTaxRecordsIndicator = ;
            // borrowerDetail.SSNCertificationIndicator = ;
            // borrowerDetail.TaxRecordsObtainedIndicator = ;

            return borrowerDetail;
        }

        private Builder CreateBuilder()
        {
            Builder builder = new Builder();

            // builder.BuilderLicenseIdentifier = ;
            // builder.BuilderLicenseStateName = ;

            return builder;
        }

        private Buydown CreateBuydown(CPageData dataLoan)
        {
            Buydown buydown = new Buydown();

            buydown.BuydownContributors = CreateBuydownContributors(dataLoan);
            // buydown.BuydownOccurrences = CreateBuydownOccurrences();
            buydown.BuydownRule = CreateBuydownRule(dataLoan);
            // buydown.BuydownSchedules = CreateBuydownSchedules();

            return buydown;
        }

        private BuydownContributor CreateBuydownContributor(CPageData dataLoan)
        {
            BuydownContributor buydownContributor = new BuydownContributor();

            buydownContributor.BuydownContributorDetail = CreateBuydownContributorDetail(dataLoan);
            // buydownContributor.Name = CreateName();
            // buydownContributor.SequenceNumber = ;

            return buydownContributor;
        }

        private BuydownContributors CreateBuydownContributors(CPageData dataLoan)
        {
            BuydownContributors buydownContributors = new BuydownContributors();

            buydownContributors.BuydownContributorList.Add(CreateBuydownContributor(dataLoan));
            // buydownContributors.BuydownContributorsSummary = CreateBuydownContributorsSummary();

            return buydownContributors;
        }

        private BuydownContributorsSummary CreateBuydownContributorsSummary()
        {
            BuydownContributorsSummary buydownContributorsSummary = new BuydownContributorsSummary();

            // buydownContributorsSummary.BuydownContributorTotalSubsidyAmount = ;

            return buydownContributorsSummary;
        }

        private BuydownContributorDetail CreateBuydownContributorDetail(CPageData dataLoan)
        {
            BuydownContributorDetail buydownContributorDetail = new BuydownContributorDetail();

            // buydownContributorDetail.BuydownContributionMethodType = ;
            // buydownContributorDetail.BuydownContributionMethodTypeOtherDescription = ;
            // buydownContributorDetail.BuydownContributorAmount = ;
            // buydownContributorDetail.BuydownContributorPercent = ;
            buydownContributorDetail.BuydownContributorType = ToMismo(dataLoan.sBuydownContributorT);

            if (buydownContributorDetail.BuydownContributorType == BuydownContributorType.Other)
            {
                buydownContributorDetail.BuydownContributorTypeOtherDescription = "InterestedThirdParty";
            }

            return buydownContributorDetail;
        }

        /// <summary>
        /// Converts the given buydown contributor to a MISMO enumerated value.
        /// </summary>
        /// <param name="contributor">The entity who is paying for the buydown.</param>
        /// <returns>The corresponding <see cref="BuydownContributorType" /> value.</returns>
        private BuydownContributorType ToMismo(E_sBuydownContributorT contributor)
        {
            switch (contributor)
            {
                case E_sBuydownContributorT.Borrower:
                    return BuydownContributorType.Borrower;
                case E_sBuydownContributorT.LenderPremiumFinanced:
                    return BuydownContributorType.Lender;
                case E_sBuydownContributorT.Builder:
                case E_sBuydownContributorT.Employer:
                case E_sBuydownContributorT.NonParentRelative:
                case E_sBuydownContributorT.Parent:
                case E_sBuydownContributorT.Seller:
                case E_sBuydownContributorT.Unassigned:
                case E_sBuydownContributorT.UnrelatedFriend:
                case E_sBuydownContributorT.LeaveBlank:
                case E_sBuydownContributorT.Other:
                    return BuydownContributorType.Other;
                default:
                    throw new UnhandledEnumException(contributor);
            }
        }

        private BuydownOccurrence CreateBuydownOccurrence()
        {
            BuydownOccurrence buydownOccurrence = new BuydownOccurrence();

            // buydownOccurrence.BuydownInitialEffectiveInterestRatePercent = ;
            // buydownOccurrence.BuydownOccurrenceEffectiveDate = ;
            // buydownOccurrence.RemainingBuydownBalanceAmount = ;
            // buydownOccurrence.SequenceNumber = ;

            return buydownOccurrence;
        }

        private BuydownOccurrences CreateBuydownOccurrences()
        {
            BuydownOccurrences buydownOccurrences = new BuydownOccurrences();

            // buydownOccurrences.BuydownOccurrenceList.Add(CreateBuydownOccurrence());

            return buydownOccurrences;
        }

        private BuydownRule CreateBuydownRule(CPageData dataLoan)
        {
            BuydownRule buydownRule = new BuydownRule();

            // buydownRule.BuydownBaseDateType = ;
            // buydownRule.BuydownBaseDateTypeOtherDescription = ;
            // buydownRule.BuydownCalculationType = ;
            buydownRule.BuydownChangeFrequencyMonthsCount = dataLoan.sBuydwnMon1_rep;
            buydownRule.BuydownDurationMonthsCount = dataLoan.sBuydwnTotalMon_rep;
            buydownRule.BuydownIncreaseRatePercent = dataLoan.sBuydwnR1stIncrease_rep;
            buydownRule.BuydownInitialDiscountPercent = dataLoan.sBuydwnR1_rep;
            // buydownRule.BuydownLenderFundingIndicator = ;

            return buydownRule;
        }

        private BuydownSchedule CreateBuydownSchedule()
        {
            BuydownSchedule buydownSchedule = new BuydownSchedule();

            // buydownSchedule.BuydownScheduleAdjustmentPercent = ;
            // buydownSchedule.BuydownSchedulePeriodIdentifier = ;
            // buydownSchedule.BuydownSchedulePeriodicPaymentAmount = ;
            // buydownSchedule.BuydownSchedulePeriodicPaymentEffectiveDate = ;
            // buydownSchedule.BuydownSchedulePeriodicPaymentsCount = ;
            // buydownSchedule.SequenceNumber = ;

            return buydownSchedule;
        }

        private BuydownSchedules CreateBuydownSchedules()
        {
            BuydownSchedules buydownSchedules = new BuydownSchedules();

            // buydownSchedules.BuydownScheduleList.Add(CreateBuydownSchedule());

            return buydownSchedules;
        }

        private CashRemittanceSummaryNotification CreateCashRemittanceSummaryNotification()
        {
            CashRemittanceSummaryNotification cashRemittanceSummaryNotification = new CashRemittanceSummaryNotification();

            // cashRemittanceSummaryNotification.InvestorRemittanceAmount = ;
            // cashRemittanceSummaryNotification.InvestorRemittanceEffectiveDate = ;
            // cashRemittanceSummaryNotification.InvestorRemittanceType = ;
            // cashRemittanceSummaryNotification.InvestorRemittanceTypeOtherDescription = ;

            return cashRemittanceSummaryNotification;
        }

        private CensusInformation CreateCensusInformation()
        {
            CensusInformation censusInformation = new CensusInformation();

            // censusInformation.CensusBlockGroupIdentifier = ;
            // censusInformation.CensusBlockIdentifier = ;
            // censusInformation.CensusTractBaseIdentifier = ;
            // censusInformation.CensusTractIdentifier = ;
            // censusInformation.CensusTractSuffixIdentifier = ;

            return censusInformation;
        }

        private ClosingAgent CreateClosingAgent()
        {
            ClosingAgent closingAgent = new ClosingAgent();

            // closingAgent.ClosingAgentType = ;
            // closingAgent.ClosingAgentTypeOtherDescription = ;

            return closingAgent;
        }

        private ClosingCostFund CreateClosingCostFund(E_sCommunityLendingClosingCostT costT, E_sCommunityLendingClosingCostSourceT sourceT, string amt)
        {
            decimal v = 0;
            decimal.TryParse(amt, out v);

            if (v == 0)
                return null;

            ClosingCostFund closingCostFund = new ClosingCostFund();

            closingCostFund.ClosingCostContributionAmount = amt;
            if (costT != E_sCommunityLendingClosingCostT.LeaveBlank)
            {
                if (costT == E_sCommunityLendingClosingCostT.AggregatedRemainingTypes)
                {
                    closingCostFund.ClosingCostFundsTypeOtherDescription = "AggregatedRemainingTypes";
                    closingCostFund.ClosingCostFundsType = ClosingCostFundsType.Other;
                }
                else if (costT == E_sCommunityLendingClosingCostT.SecondaryHELOC)
                {
                    closingCostFund.ClosingCostFundsTypeOtherDescription = "SecondaryFinancingClosedEnd";
                    closingCostFund.ClosingCostFundsType = ClosingCostFundsType.Other;
                }
                else if (costT == E_sCommunityLendingClosingCostT.SecondaryClosedEnd)
                {
                    closingCostFund.ClosingCostFundsTypeOtherDescription = "SecondaryFinancingHELOC";
                    closingCostFund.ClosingCostFundsType = ClosingCostFundsType.Other;

                }

                else
                {
                    closingCostFund.ClosingCostFundsType = ToMismo(costT);
                }
            }

            if (sourceT != E_sCommunityLendingClosingCostSourceT.LeaveBlank)
            {
                if (sourceT == E_sCommunityLendingClosingCostSourceT.UsdaRuralHousing)
                {
                    closingCostFund.ClosingCostSourceTypeOtherDescription = "UsdaRuralHousing";
                    closingCostFund.ClosingCostSourceType = ClosingCostSourceType.Other;
                }
                else if (sourceT == E_sCommunityLendingClosingCostSourceT.FHLBAffordableHousingProgram)
                {
                    closingCostFund.ClosingCostSourceTypeOtherDescription = "FHLBAffordableHousingProgram";
                    closingCostFund.ClosingCostSourceType = ClosingCostSourceType.Other;
                }
                else if (sourceT == E_sCommunityLendingClosingCostSourceT.AggregatedRemainingTypes)
                {
                    closingCostFund.ClosingCostSourceTypeOtherDescription = "AggregatedRemainingSourceTypes";
                    closingCostFund.ClosingCostSourceType = ClosingCostSourceType.Other;
                }
                else
                {
                    closingCostFund.ClosingCostSourceType = ToMismo(sourceT);
                }

            }

            // closingCostFund.SequenceNumber = ;

            return closingCostFund;
        }

        private ClosingCostSourceType ToMismo(E_sCommunityLendingClosingCostSourceT sourceT)
        {
            switch (sourceT)
            {
                case E_sCommunityLendingClosingCostSourceT.Borrower:
                    return ClosingCostSourceType.Borrower;
                case E_sCommunityLendingClosingCostSourceT.CommunityNonProfit:
                    return ClosingCostSourceType.CommunityNonProfit;
                case E_sCommunityLendingClosingCostSourceT.FederalAgency:
                    return ClosingCostSourceType.FederalAgency;
                case E_sCommunityLendingClosingCostSourceT.Lender:
                    return ClosingCostSourceType.Lender;
                case E_sCommunityLendingClosingCostSourceT.LocalAgency:
                    return ClosingCostSourceType.LocalAgency;
                case E_sCommunityLendingClosingCostSourceT.PropertySeller:
                    return ClosingCostSourceType.PropertySeller;
                case E_sCommunityLendingClosingCostSourceT.Relative:
                    return ClosingCostSourceType.Relative;
                case E_sCommunityLendingClosingCostSourceT.ReligiousNonProfit:
                    return ClosingCostSourceType.ReligiousNonProfit;
                case E_sCommunityLendingClosingCostSourceT.StateAgency:
                    return ClosingCostSourceType.StateAgency;
                case E_sCommunityLendingClosingCostSourceT.Employer:
                    return ClosingCostSourceType.Employer;
                default:
                    throw new UnhandledEnumException(sourceT);
            }

        }

        private ClosingCostFundsType ToMismo(E_sCommunityLendingClosingCostT sCommunityLendingClosingCostT)
        {
            switch (sCommunityLendingClosingCostT)
            {
                case E_sCommunityLendingClosingCostT.BridgeLoan:
                    return ClosingCostFundsType.BridgeLoan;
                case E_sCommunityLendingClosingCostT.CashOnHand:
                    return ClosingCostFundsType.CashOnHand;
                case E_sCommunityLendingClosingCostT.CheckingSavings:
                    return ClosingCostFundsType.CheckingSavings;
                case E_sCommunityLendingClosingCostT.Contribution:
                    return ClosingCostFundsType.Contribution;
                case E_sCommunityLendingClosingCostT.CreditCard:
                    return ClosingCostFundsType.CreditCard;
                case E_sCommunityLendingClosingCostT.GiftFunds:
                    return ClosingCostFundsType.GiftFunds;
                case E_sCommunityLendingClosingCostT.Grant:
                    return ClosingCostFundsType.Grant;
                case E_sCommunityLendingClosingCostT.PremiumFunds:
                    return ClosingCostFundsType.PremiumFunds;
                case E_sCommunityLendingClosingCostT.SecuredLoan:
                    return ClosingCostFundsType.SecuredLoan;
                case E_sCommunityLendingClosingCostT.SweatEquity:
                    return ClosingCostFundsType.SweatEquity;
                case E_sCommunityLendingClosingCostT.UnsecuredBorrowedFunds:
                    return ClosingCostFundsType.UnsecuredBorrowedFunds;
                case E_sCommunityLendingClosingCostT.EquityOnSoldProperty:
                    return ClosingCostFundsType.EquityOnSoldProperty;
                case E_sCommunityLendingClosingCostT.EquityOnSubjectProperty:
                    return ClosingCostFundsType.EquityOnSubjectProperty;
                case E_sCommunityLendingClosingCostT.ForgivableSecuredLoan:
                    return ClosingCostFundsType.ForgivableSecuredLoan;
                case E_sCommunityLendingClosingCostT.LifeInsuranceCashValue:
                    return ClosingCostFundsType.LifeInsuranceCashValue;
                case E_sCommunityLendingClosingCostT.LotEquity:
                    return ClosingCostFundsType.LotEquity;
                case E_sCommunityLendingClosingCostT.RentWithOptionToPurchase:
                    return ClosingCostFundsType.RentWithOptionToPurchase;
                case E_sCommunityLendingClosingCostT.RetirementFunds:
                    return ClosingCostFundsType.RetirementFunds;
                case E_sCommunityLendingClosingCostT.SaleOfChattel:
                    return ClosingCostFundsType.SaleOfChattel;
                case E_sCommunityLendingClosingCostT.StocksAndBonds:
                    return ClosingCostFundsType.StocksAndBonds;
                case E_sCommunityLendingClosingCostT.TradeEquity:
                    return ClosingCostFundsType.TradeEquity;
                case E_sCommunityLendingClosingCostT.TrustFunds:
                    return ClosingCostFundsType.TrustFunds;
                default:
                    throw new UnhandledEnumException(sCommunityLendingClosingCostT);
            }
        }

        private ClosingCostFunds CreateClosingCostFunds(CPageData dataLoan)
        {
            ClosingCostFunds closingCostFunds = null;

            if (dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingClosingCost1Amt_rep) != 0
    || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingClosingCost2Amt_rep) != 0
    || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingClosingCost3Amt_rep) != 0
    || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingClosingCost4Amt_rep) != 0)
            {
                closingCostFunds = new ClosingCostFunds();
                closingCostFunds.ClosingCostFundList.Add(CreateClosingCostFund(dataLoan.sCommunityLendingClosingCost1T, dataLoan.sCommunityLendingClosingCostSource1T, dataLoan.sCommunityLendingClosingCost1Amt_rep));
                closingCostFunds.ClosingCostFundList.Add(CreateClosingCostFund(dataLoan.sCommunityLendingClosingCost2T, dataLoan.sCommunityLendingClosingCostSource2T, dataLoan.sCommunityLendingClosingCost2Amt_rep));
                closingCostFunds.ClosingCostFundList.Add(CreateClosingCostFund(dataLoan.sCommunityLendingClosingCost3T, dataLoan.sCommunityLendingClosingCostSource3T, dataLoan.sCommunityLendingClosingCost3Amt_rep));
                closingCostFunds.ClosingCostFundList.Add(CreateClosingCostFund(dataLoan.sCommunityLendingClosingCost4T, dataLoan.sCommunityLendingClosingCostSource4T, dataLoan.sCommunityLendingClosingCost4Amt_rep));

            }

            return closingCostFunds;
        }

        private ClosingInformation CreateClosingInformation(CPageData dataLoan)
        {
            ClosingCostFunds closingCostFunds = CreateClosingCostFunds(dataLoan);
            CollectedOtherFunds collectedOtherFunds = CreateCollectedOtherFunds(dataLoan);

            if (closingCostFunds == null && collectedOtherFunds == null)
            {
                return null;
            }

            ClosingInformation closingInformation = new ClosingInformation();


            closingInformation.ClosingCostFunds = closingCostFunds;
            // closingInformation.ClosingInformationDetail = CreateClosingInformationDetail();
            // closingInformation.ClosingInstruction = CreateClosingInstruction();
            closingInformation.CollectedOtherFunds = collectedOtherFunds;
            // closingInformation.Compensations = CreateCompensations();
            // closingInformation.InterimInterests = CreateInterimInterests();

            return closingInformation;
        }

        private ClosingInformationDetail CreateClosingInformationDetail()
        {
            ClosingInformationDetail closingInformationDetail = new ClosingInformationDetail();

            // closingInformationDetail.ClosingAgentOrderNumberIdentifier = ;
            // closingInformationDetail.ClosingDate = ;
            // closingInformationDetail.ClosingDocumentReceivedDate = ;
            // closingInformationDetail.CurrentRateSetDate = ;
            // closingInformationDetail.DisbursementDate = ;
            // closingInformationDetail.DocumentOrderClassificationType = ;
            // closingInformationDetail.DocumentPreparationDate = ;
            // closingInformationDetail.EstimatedPrepaidDaysCount = ;
            // closingInformationDetail.EstimatedPrepaidDaysPaidByType = ;
            // closingInformationDetail.EstimatedPrepaidDaysPaidByTypeOtherDescription = ;
            // closingInformationDetail.FundByDate = ;
            // closingInformationDetail.LoanEstimatedClosingDate = ;
            // closingInformationDetail.LockExpirationDate = ;
            // closingInformationDetail.LoanScheduledClosingDate = ;
            // closingInformationDetail.RescissionDate = ;

            return closingInformationDetail;
        }

        private ClosingInstruction CreateClosingInstruction()
        {
            ClosingInstruction closingInstruction = new ClosingInstruction();

            // closingInstruction.ClosingInstructionConditions = CreateClosingInstructionConditions();
            // closingInstruction.ClosingInstructionDetail = CreateClosingInstructionDetail();

            return closingInstruction;
        }

        private ClosingInstructionCondition CreateClosingInstructionCondition()
        {
            ClosingInstructionCondition closingInstructionCondition = new ClosingInstructionCondition();

            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionApprovedByName = ;
            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionDate = ;
            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionResponsiblePartyType = ;
            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionResponsiblePartyTypeOtherDescription = ;
            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionTimeframeType = ;
            // closingInstructionCondition.ClosingInstructionsConditionSatisfactionTimeframeTypeOtherDescription = ;
            // closingInstructionCondition.ClosingInstructionsDescription = ;
            // closingInstructionCondition.ClosingInstructionsMetIndicator = ;
            // closingInstructionCondition.ClosingInstructionsWaivedIndicator = ;
            // closingInstructionCondition.SequenceNumber = ;

            return closingInstructionCondition;
        }

        private ClosingInstructionConditions CreateClosingInstructionConditions()
        {
            ClosingInstructionConditions closingInstructionConditions = new ClosingInstructionConditions();

            // closingInstructionConditions.ClosingInstructionConditionList.Add(CreateClosingInstructionCondition());

            return closingInstructionConditions;
        }

        private ClosingInstructionDetail CreateClosingInstructionDetail()
        {
            ClosingInstructionDetail closingInstructionDetail = new ClosingInstructionDetail();

            // closingInstructionDetail.ClosingInstructionsConsolidatedClosingConditionsDescription = ;
            // closingInstructionDetail.ClosingInstructionsPropertyTaxMessageDescription = ;
            // closingInstructionDetail.ClosingInstructionsTermiteReportRequiredIndicator = ;
            // closingInstructionDetail.FundingCutoffTime = ;
            // closingInstructionDetail.HoursDocumentsNeededPriorToDisbursementCount = ;
            // closingInstructionDetail.LeadBasedPaintCertificationRequiredIndicator = ;

            return closingInstructionDetail;
        }

        private Collateral CreateCollateral(CPageData dataLoan)
        {
            Collateral collateral = new Collateral();

            // collateral.CollateralDetail = CreateCollateralDetail();
            // collateral.PledgedAssets = CreatePledgedAssets();
            collateral.Properties = CreateProperties(dataLoan);
            // collateral.SequenceNumber = ;

            return collateral;
        }

        private Collaterals CreateCollaterals(CPageData dataLoan)
        {
            Collaterals collaterals = new Collaterals();

            collaterals.CollateralList.Add(CreateCollateral(dataLoan));

            return collaterals;
        }

        private CollateralDetail CreateCollateralDetail()
        {
            CollateralDetail collateralDetail = new CollateralDetail();

            // collateralDetail.LienPriorityExceptionType = ;
            // collateralDetail.LienPriorityExceptionTypeOtherDescription = ;

            return collateralDetail;
        }

        private CollectedOtherFund CreateCollectedOtherFund(CPageData dataLoan)
        {
            if (dataLoan.m_convertLos.ToMoney(dataLoan.sInitialDeposit_Escrow_rep) == 0.0M)
                return null;

            CollectedOtherFund collectedOtherFund = new CollectedOtherFund();

            collectedOtherFund.OtherFundsCollectedAtClosingAmount = dataLoan.sInitialDeposit_Escrow_rep;
            collectedOtherFund.OtherFundsCollectedAtClosingType = OtherFundsCollectedAtClosingType.EscrowFunds;
            // collectedOtherFund.OtherFundsCollectedAtClosingTypeOtherDescription = ;
            // collectedOtherFund.SequenceNumber = ;

            return collectedOtherFund;
        }

        private CollectedOtherFunds CreateCollectedOtherFunds(CPageData dataLoan)
        {
            if (dataLoan.m_convertLos.ToDecimal(dataLoan.sInitialDeposit_Escrow_rep) == 0)
                return null;

            CollectedOtherFunds collectedOtherFunds = new CollectedOtherFunds();

            collectedOtherFunds.CollectedOtherFundList.Add(CreateCollectedOtherFund(dataLoan));

            return collectedOtherFunds;
        }

        private CombinedLtv CreateCombinedLtv(CPageData dataLoan)
        {
            CombinedLtv combinedLtv = new CombinedLtv();

            combinedLtv.CombinedLTVRatioPercent = RoundLTVRatioPercent(dataLoan.sCltvR);
            if (dataLoan.sLienPosT != E_sLienPosT.Second &&
                (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac || dataLoan.sSubFinT == E_sSubFinT.Heloc || dataLoan.sIsOFinCreditLineInDrawPeriod))
            {
                try
                {
                    combinedLtv.HomeEquityCombinedLTVRatioPercent = RoundLTVRatioPercent(dataLoan.sHcltvR);
                }
                catch (DivideByZeroException)
                {
                    // 9/24/2013 dd - House value can be zero which cause Divide by zero exception.
                }
            }
            // combinedLtv.LTVCalculationDate = ;
            // combinedLtv.OriginalLTVIndicator = ;
            // combinedLtv.SequenceNumber = ;

            return combinedLtv;
        }
        private string RoundLTVRatioPercent(decimal value)
        {
            // Rounding rules:
            // Check the formatting for LTVRatioPercent, it looks like it should be multiplied by 100. 
            // Also, according to the Fannie notes, it looks like they want whole numbers:
            //
            // Only whole numbers will be supported at this time.  The LTVRatioPercent must be truncated 
            // (shortened) to two decimal places.  The truncated result must be rounded up to the next 
            // whole percent.  For example:  96.001% will be delivered as 96; 80.01% will be delivered as 81.

            int result = decimal.ToInt32(value * 100);

            if (result % 100 > 0)
                result += 100;

            result /= 100;

            return result.ToString();
        }

        private CombinedLtvs CreateCombinedLtvs(CPageData dataLoan)
        {
            CombinedLtvs combinedLtvs = new CombinedLtvs();

            combinedLtvs.CombinedLtvList.Add(CreateCombinedLtv(dataLoan));

            return combinedLtvs;
        }

        private Compensation CreateCompensation()
        {
            Compensation compensation = new Compensation();

            // compensation.CompensationAmount = ;
            // compensation.CompensationPaidByType = ;
            // compensation.CompensationPaidToType = ;
            // compensation.CompensationPercent = ;
            // compensation.CompensationType = ;
            // compensation.CompensationTypeOtherDescription = ;
            // compensation.SequenceNumber = ;

            return compensation;
        }

        private Compensations CreateCompensations()
        {
            Compensations compensations = new Compensations();

            // compensations.CompensationList.Add(CreateCompensation());

            return compensations;
        }

        private Construction CreateConstruction()
        {
            Construction construction = new Construction();

            // construction.ConstructionImprovementCostsAmount = ;
            // construction.ConstructionLoanType = ;
            // construction.ConstructionPeriodInterestRatePercent = ;
            // construction.ConstructionPeriodNumberOfMonthsCount = ;
            // construction.ConstructionPhaseInterestPaymentFrequencyType = ;
            // construction.ConstructionPhaseInterestPaymentFrequencyTypeOther_Description = ;
            // construction.ConstructionPhaseInterestPaymentMethodType = ;
            // construction.ConstructionPhaseInterestPaymentMethodTypeOtherDescription = ;
            // construction.ConstructionPhaseInterestPaymentType = ;
            // construction.ConstructionPhaseInterestPaymentTypeOtherDescription = ;
            // construction.ConstructionToPermanentClosingFeatureType = ;
            // construction.ConstructionToPermanentClosingFeatureTypeOtherDescription = ;
            // construction.ConstructionToPermanentClosingType = ;
            // construction.ConstructionToPermanentClosingTypeOtherDescription = ;
            // construction.ConstructionToPermanentFirstPaymentDueDate = ;
            // construction.ConstructionToPermanentRecertificationDate = ;
            // construction.ConstructionToPermanentRecertificationValueAmount = ;
            // construction.LandAppraisedValueAmount = ;
            // construction.LandEstimatedValueAmount = ;
            // construction.LandOriginalCostAmount = ;

            return construction;
        }

        private Contact CreateContact()
        {
            Contact contact = new Contact();

            // contact.ContactDetail = CreateContactDetail();
            // contact.ContactPoints = CreateContactPoints();
            // contact.Name = CreateName();
            // contact.SequenceNumber = ;

            return contact;
        }

        private Contacts CreateContacts()
        {
            Contacts contacts = new Contacts();

            // contacts.ContactList.Add(CreateContact());

            return contacts;
        }

        private ContactDetail CreateContactDetail()
        {
            ContactDetail contactDetail = new ContactDetail();

            // contactDetail.AuthorizedToSignIndicator = ;
            // contactDetail.ContactPartyIdentifier = ;

            return contactDetail;
        }

        private ContactPoint CreateContactPoint()
        {
            ContactPoint contactPoint = new ContactPoint();

            // contactPoint.ContactPointEmailValue = ;
            // contactPoint.ContactPointFaxValue = ;
            // contactPoint.ContactPointTelephoneValue = ;
            // contactPoint.ContactPointOtherValue = ;
            // contactPoint.ContactPointOtherValueDescription = ;
            // contactPoint.ContactPointRoleType = ;
            // contactPoint.ContactPointRoleTypeOtherDescription = ;
            // contactPoint.ContactPointPreferenceIndicator = ;
            // contactPoint.SequenceNumber = ;

            return contactPoint;
        }

        private ContactPoints CreateContactPoints()
        {
            ContactPoints contactPoints = new ContactPoints();

            // contactPoints.ContactPointList.Add(CreateContactPoint());

            return contactPoints;
        }

        private ConversionAdjustment CreateConversionAdjustment()
        {
            ConversionAdjustment conversionAdjustment = new ConversionAdjustment();

            // conversionAdjustment.ConversionAdjustmentLifetimeAdjustmentRule = CreateConversionAdjustmentLifetimeAdjustmentRule();
            // conversionAdjustment.ConversionOptionPeriodAdjustmentRules = CreateConversionOptionPeriodAdjustmentRules();
            // conversionAdjustment.IndexRules = CreateIndexRules();

            return conversionAdjustment;
        }

        private ConversionAdjustmentLifetimeAdjustmentRule CreateConversionAdjustmentLifetimeAdjustmentRule()
        {
            ConversionAdjustmentLifetimeAdjustmentRule conversionAdjustmentLifetimeAdjustmentRule = new ConversionAdjustmentLifetimeAdjustmentRule();

            // conversionAdjustmentLifetimeAdjustmentRule.ConversionDateMaximumExtensionMonthsCount = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionMaximumAllowedCount = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionDurationMonthsCount = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionMarginRatePercent = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionMaximumRatePercent = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionMinimumRatePercent = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionScheduleType = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionScheduleTypeOtherDescription = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionType = ;
            // conversionAdjustmentLifetimeAdjustmentRule.ConversionTypeOtherDescription = ;

            return conversionAdjustmentLifetimeAdjustmentRule;
        }

        private ConversionOptionPeriodAdjustmentRule CreateConversionOptionPeriodAdjustmentRule()
        {
            ConversionOptionPeriodAdjustmentRule conversionOptionPeriodAdjustmentRule = new ConversionOptionPeriodAdjustmentRule();

            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodAdjustmentEffectiveDate = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodDemandNotificationFrequencyCount = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodExpirationDate = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodFeeAmount = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodFeeBalanceCalculationType = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodFeePercent = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodReplyDaysCount = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodRoundingPercent = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodRoundingType = ;
            // conversionOptionPeriodAdjustmentRule.ConversionOptionPeriodType = ;
            // conversionOptionPeriodAdjustmentRule.ConversionRateCalculationMethodDescription = ;
            // conversionOptionPeriodAdjustmentRule.SequenceNumber = ;

            return conversionOptionPeriodAdjustmentRule;
        }

        private ConversionOptionPeriodAdjustmentRules CreateConversionOptionPeriodAdjustmentRules()
        {
            ConversionOptionPeriodAdjustmentRules conversionOptionPeriodAdjustmentRules = new ConversionOptionPeriodAdjustmentRules();

            // conversionOptionPeriodAdjustmentRules.ConversionOptionPeriodAdjustmentRuleList.Add(CreateConversionOptionPeriodAdjustmentRule());

            return conversionOptionPeriodAdjustmentRules;
        }

        private CounselingConfirmation CreateCounselingConfirmation()
        {
            CounselingConfirmation counselingConfirmation = new CounselingConfirmation();

            // counselingConfirmation.CounselingConfirmationIndicator = ;
            // counselingConfirmation.CounselingConfirmationType = ;
            // counselingConfirmation.CounselingConfirmationTypeOtherDescription = ;
            // counselingConfirmation.CounselingFormatType = ;
            // counselingConfirmation.CounselingFormatTypeOtherDescription = ;

            return counselingConfirmation;
        }

        private Credit CreateCredit()
        {
            Credit credit = new Credit();


            return credit;
        }

        private CreditBureauReporting CreateCreditBureauReporting()
        {
            CreditBureauReporting creditBureauReporting = new CreditBureauReporting();

            // creditBureauReporting.CreditCommentCode = ;
            // creditBureauReporting.CreditCommentSourceType = ;
            // creditBureauReporting.CreditCommentExpirationDate = ;
            // creditBureauReporting.CreditLiabilityAccountStatusType = ;
            // creditBureauReporting.CreditLiabilityCurrentRatingCode = ;
            // creditBureauReporting.CreditLiabilityCurrentRatingType = ;
            // creditBureauReporting.CreditLiabilityFirstReportedDefaultDate = ;

            return creditBureauReporting;
        }

        private CreditComment CreateCreditComment()
        {
            CreditComment creditComment = new CreditComment();

            // creditComment.CreditCommentCode = ;
            // creditComment.CreditCommentCodeSourceType = ;
            // creditComment.CreditCommentCodeSourceTypeOtherDescription = ;
            // creditComment.CreditCommentReportedDate = ;
            // creditComment.CreditCommentSourceType = ;
            // creditComment.CreditCommentText = ;
            // creditComment.CreditCommentType = ;
            // creditComment.CreditCommentTypeOtherDescription = ;
            // creditComment.SequenceNumber = ;

            return creditComment;
        }

        private CreditComments CreateCreditComments()
        {
            CreditComments creditComments = new CreditComments();

            // creditComments.CreditCommentList.Add(CreateCreditComment());

            return creditComments;
        }

        private CreditEnhancement CreateCreditEnhancement()
        {
            CreditEnhancement creditEnhancement = new CreditEnhancement();

            // creditEnhancement.CreditEnhancementEffectiveDate = ;
            // creditEnhancement.CreditEnhancementEffectivePeriodType = ;
            // creditEnhancement.CreditEnhancementExpirationDate = ;
            // creditEnhancement.CreditEnhancementPartyRoleType = ;
            // creditEnhancement.CreditEnhancementPeriodBasedIndicator = ;
            // creditEnhancement.CreditEnhancementType = ;
            // creditEnhancement.CreditEnhancementTypeOtherDescription = ;
            // creditEnhancement.SequenceNumber = ;

            return creditEnhancement;
        }

        private CreditEnhancements CreateCreditEnhancements()
        {
            CreditEnhancements creditEnhancements = new CreditEnhancements();

            // creditEnhancements.CreditEnhancementList.Add(CreateCreditEnhancement());

            return creditEnhancements;
        }

        private CreditScore CreateCreditScore()
        {
            CreditScore creditScore = new CreditScore();

            // creditScore.CreditScoreDetail = CreateCreditScoreDetail();
            // creditScore.CreditScoreFactors = CreateCreditScoreFactors();
            // creditScore.CreditScoreProvider = CreateCreditScoreProvider();
            // creditScore.SequenceNumber = ;

            return creditScore;
        }

        private CreditScores CreateCreditScores()
        {
            CreditScores creditScores = new CreditScores();

            // creditScores.CreditScoreList.Add(CreateCreditScore());

            return creditScores;
        }

        private CreditScoreDetail CreateCreditScoreDetail()
        {
            CreditScoreDetail creditScoreDetail = new CreditScoreDetail();

            // creditScoreDetail.CreditReportIdentifier = ;
            // creditScoreDetail.CreditReportType = ;
            // creditScoreDetail.CreditReportTypeOtherDescription = ;
            // creditScoreDetail.CreditRepositorySingleSourceIndicator = ;
            // creditScoreDetail.CreditRepositorySourceIndicator = ;
            // creditScoreDetail.CreditRepositorySourceType = ;
            // creditScoreDetail.CreditRepositorySourceTypeOtherDescription = ;
            // creditScoreDetail.CreditScoreCategoryType = ;
            // creditScoreDetail.CreditScoreCategoryTypeOtherDescription = ;
            // creditScoreDetail.CreditScoreDate = ;
            // creditScoreDetail.CreditScoreExclusionReasonType = ;
            // creditScoreDetail.CreditScoreFACTAInquiriesIndicator = ;
            // creditScoreDetail.CreditScoreImpairmentType = ;
            // creditScoreDetail.CreditScoreImpairmentTypeOtherDescription = ;
            // creditScoreDetail.CreditScoreMaximumValue = ;
            // creditScoreDetail.CreditScoreMinimumValue = ;
            // creditScoreDetail.CreditScoreModelNameType = ;
            // creditScoreDetail.CreditScoreModelNameTypeOtherDescription = ;
            // creditScoreDetail.CreditScoreModelRangeOfScoresDescription = ;
            // creditScoreDetail.CreditScoreValue = ;

            return creditScoreDetail;
        }

        private CreditScoreFactor CreateCreditScoreFactor()
        {
            CreditScoreFactor creditScoreFactor = new CreditScoreFactor();

            // creditScoreFactor.CreditScoreFactorCode = ;
            // creditScoreFactor.CreditScoreFactorText = ;
            // creditScoreFactor.SequenceNumber = ;

            return creditScoreFactor;
        }

        private CreditScoreFactors CreateCreditScoreFactors()
        {
            CreditScoreFactors creditScoreFactors = new CreditScoreFactors();

            // creditScoreFactors.CreditScoreFactorList.Add(CreateCreditScoreFactor());

            return creditScoreFactors;
        }

        private CreditScoreProvider CreateCreditScoreProvider()
        {
            CreditScoreProvider creditScoreProvider = new CreditScoreProvider();

            // creditScoreProvider.Address = CreateAddress();
            // creditScoreProvider.CreditScoreProviderDetail = CreateCreditScoreProviderDetail();

            return creditScoreProvider;
        }

        private CreditScoreProvider CreateCreditScoreProvider(string creditScoreProviderName)
        {
            CreditScoreProvider creditScoreProvider = new CreditScoreProvider();

            creditScoreProvider.CreditScoreProviderDetail = CreateCreditScoreProviderDetail(creditScoreProviderName);

            return creditScoreProvider;
        }

        private CreditScoreProviderDetail CreateCreditScoreProviderDetail()
        {
            CreditScoreProviderDetail creditScoreProviderDetail = new CreditScoreProviderDetail();

            // creditScoreProviderDetail.CreditScoreProviderName = ;
            // creditScoreProviderDetail.CreditScoreProviderURLDescription = ;

            return creditScoreProviderDetail;
        }

        private CreditScoreProviderDetail CreateCreditScoreProviderDetail(string creditScoreProviderName)
        {
            CreditScoreProviderDetail creditScoreProviderDetail = new CreditScoreProviderDetail();

            creditScoreProviderDetail.CreditScoreProviderName = creditScoreProviderName;
            // creditScoreProviderDetail.CreditScoreProviderURLDescription = ;

            return creditScoreProviderDetail;
        }

        private CurrentIncome CreateCurrentIncome()
        {
            CurrentIncome currentIncome = new CurrentIncome();

            // currentIncome.CurrentIncomeDetail = CreateCurrentIncomeDetail();
            // currentIncome.CurrentIncomeItems = CreateCurrentIncomeItems();
            // currentIncome.Verification = CreateVerification();

            return currentIncome;
        }

        private CurrentIncomeDetail CreateCurrentIncomeDetail()
        {
            CurrentIncomeDetail currentIncomeDetail = new CurrentIncomeDetail();

            // currentIncomeDetail.URLABorrowerTotalMonthlyIncomeAmount = ;
            // currentIncomeDetail.URLABorrowerTotalOtherIncomeAmount = ;

            return currentIncomeDetail;
        }

        private CurrentIncomeItem CreateCurrentIncomeItem()
        {
            CurrentIncomeItem currentIncomeItem = new CurrentIncomeItem();

            // currentIncomeItem.CurrentIncomeItemDetail = CreateCurrentIncomeItemDetail();
            // currentIncomeItem.IncomeDocumentations = CreateIncomeDocumentations();
            // currentIncomeItem.Verification = CreateVerification();
            // currentIncomeItem.SequenceNumber = ;

            return currentIncomeItem;
        }

        private CurrentIncomeItems CreateCurrentIncomeItems()
        {
            CurrentIncomeItems currentIncomeItems = new CurrentIncomeItems();

            // currentIncomeItems.CurrentIncomeItemList.Add(CreateCurrentIncomeItem());

            return currentIncomeItems;
        }

        private CurrentIncomeItemDetail CreateCurrentIncomeItemDetail()
        {
            CurrentIncomeItemDetail currentIncomeItemDetail = new CurrentIncomeItemDetail();

            // currentIncomeItemDetail.CurrentIncomeMonthlyTotalAmount = ;
            // currentIncomeItemDetail.IncomeFederalTaxExemptIndicator = ;
            // currentIncomeItemDetail.IncomeType = ;
            // currentIncomeItemDetail.IncomeTypeOtherDescription = ;

            return currentIncomeItemDetail;
        }

        private DataChange CreateDataChange()
        {
            DataChange dataChange = new DataChange();

            // dataChange.DataItemChangeRequest = CreateDataItemChangeRequest();

            return dataChange;
        }

        private DataItemChange CreateDataItemChange()
        {
            DataItemChange dataItemChange = new DataItemChange();

            // dataItemChange.DataItemChangeEffectiveDate = ;
            // dataItemChange.DataItemChangeExpirationDate = ;
            // dataItemChange.DataItemChangePriorValue = ;
            // dataItemChange.DataItemChangeType = ;
            // dataItemChange.DataItemChangeValue = ;
            // dataItemChange.DataItemChangeXPath = ;
            // dataItemChange.SequenceNumber = ;

            return dataItemChange;
        }

        private DataItemChanges CreateDataItemChanges()
        {
            DataItemChanges dataItemChanges = new DataItemChanges();

            // dataItemChanges.DataItemChangeList.Add(CreateDataItemChange());

            return dataItemChanges;
        }

        private DataItemChangeContext CreateDataItemChangeContext()
        {
            DataItemChangeContext dataItemChangeContext = new DataItemChangeContext();

            // dataItemChangeContext.DataItemChangeContextDetail = CreateDataItemChangeContextDetail();
            // dataItemChangeContext.DataItemChanges = CreateDataItemChanges();
            // dataItemChangeContext.SequenceNumber = ;

            return dataItemChangeContext;
        }

        private DataItemChangeContexts CreateDataItemChangeContexts()
        {
            DataItemChangeContexts dataItemChangeContexts = new DataItemChangeContexts();

            // dataItemChangeContexts.DataItemChangeContextList.Add(CreateDataItemChangeContext());

            return dataItemChangeContexts;
        }

        private DataItemChangeContextDetail CreateDataItemChangeContextDetail()
        {
            DataItemChangeContextDetail dataItemChangeContextDetail = new DataItemChangeContextDetail();

            // dataItemChangeContextDetail.DataItemChangeContextDescription = ;
            // dataItemChangeContextDetail.DataItemChangeContextXPath = ;

            return dataItemChangeContextDetail;
        }

        private DataItemChangeRequest CreateDataItemChangeRequest()
        {
            DataItemChangeRequest dataItemChangeRequest = new DataItemChangeRequest();

            // dataItemChangeRequest.DataItemChangeContexts = CreateDataItemChangeContexts();
            // dataItemChangeRequest.DataItemChangeRequestDetail = CreateDataItemChangeRequestDetail();

            return dataItemChangeRequest;
        }

        private DataItemChangeRequestDetail CreateDataItemChangeRequestDetail()
        {
            DataItemChangeRequestDetail dataItemChangeRequestDetail = new DataItemChangeRequestDetail();

            // dataItemChangeRequestDetail.DataItemChangeAbsoluteXPath = ;
            // dataItemChangeRequestDetail.RequestDatetime = ;

            return dataItemChangeRequestDetail;
        }

        private Deal CreateDeal(CPageData dataLoan)
        {
            Deal deal = new Deal();

            // deal.Reference = CreateReference();
            // deal.AboutVersions = CreateAboutVersions();
            // deal.Assets = CreateAssets();
            deal.Collaterals = CreateCollaterals(dataLoan);
            // deal.Expenses = CreateExpenses();
            // deal.Liabilities = CreateLiabilities();
            deal.Loans = CreateLoans(dataLoan);
            // deal.Relationships = CreateRelationships();
            // deal.Services = CreateServices();
            // deal.SequenceNumber = ;
            // deal.MISMOReferenceModelIdentifier = ;
            #region Parties
            deal.Parties.PartyList.Add(CreatePartyAppraiser(dataLoan));

            string appraiserLicenseIdentifier = string.Empty;
            List<CAgentFields> agentList = dataLoan.GetAgentsOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            foreach (CAgentFields agent in agentList)
            {
                if (agent.AgentRoleT == E_AgentRoleT.Other && agent.OtherAgentRoleTDesc == "AppraiserSupervisor")
                {

                    appraiserLicenseIdentifier = agent.LicenseNumOfAgent;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(appraiserLicenseIdentifier))
            {
                deal.Parties.PartyList.Add(CreatePartyAppraiserSupervisor(appraiserLicenseIdentifier));
            }

            //// Up to 4 borrowers allowed for Fannie Mae ULDD 2.0 Export. 5 for Freddie Mac. 
            int numBorrowersToExport = (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac) ? 5 : 4;
            IEnumerable<Party> borrowersToExport = new List<Party>();
            borrowersToExport = dataLoan.Apps.Aggregate(borrowersToExport, (borrowerList, app) => borrowerList.Concat(GetBorrowersFromApp(dataLoan, app))).Take(numBorrowersToExport);

            deal.Parties.PartyList.AddRange(borrowersToExport);

            deal.Parties.PartyList.Add(CreatePartyLoanOriginationCompany(dataLoan));
            deal.Parties.PartyList.Add(CreatePartyLoanOriginator(dataLoan));
            deal.Parties.PartyList.Add(CreateParty(PartyRoleType.LoanSeller, dataLoan.sLenNum));
            deal.Parties.PartyList.Add(CreatePartyNotePayTo(dataLoan));
            deal.Parties.PartyList.Add(CreateParty(PartyRoleType.Payee, dataLoan.sGseDeliveryPayeeId));

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                deal.Parties.PartyList.Add(CreateParty(PartyRoleType.DocumentCustodian, dataLoan.sGseDeliveryDocumentCustodianId));
                deal.Parties.PartyList.Add(CreateParty(PartyRoleType.Servicer, dataLoan.sGseDeliveryServicerId));
            }

            if (((dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                || (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2)))
            {
                if (dataLoan.sGseSpT == E_sGseSpT.Condominium)
                {
                    deal.Parties.PartyList.Add(CreatePartyHomeownersAssociation(dataLoan));
                }
                
                if (dataLoan.sWarehouseLenderRolodexId != -1 && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                {
                    deal.Parties.PartyList.Add(CreateParty(PartyRoleType.WarehouseLender, dataLoan.sWarehouseLenderFannieMaeId));
                }
                else if (dataLoan.sWarehouseLenderRolodexId != -1 && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
                {
                    deal.Parties.PartyList.Add(CreateParty(PartyRoleType.WarehouseLender, dataLoan.sWarehouseLenderFreddieMacId));
                }
            }
            #endregion
            return deal;
        }

        private List<Party> GetBorrowersFromApp(CPageData dataLoan, CAppData dataApp)
        {
            var partyList = new List<Party>();
            if (dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature == false)
            {
                partyList.Add(CreatePartyBorrower(dataLoan.sBrokerId, dataApp, E_BorrowerModeT.Borrower, dataLoan.sGseDeliveryTargetT, dataLoan.sLPurposeT));
                if (dataApp.aBHasSpouse)
                {
                    partyList.Add(CreatePartyBorrower(dataLoan.sBrokerId, dataApp, E_BorrowerModeT.Coborrower, dataLoan.sGseDeliveryTargetT, dataLoan.sLPurposeT));
                }
            }
            else
            {
                // OPM 184017. Keep non-purchasing borrowers out of export
                if (dataApp.aBTypeT != E_aTypeT.NonTitleSpouse && dataApp.aBTypeT != E_aTypeT.TitleOnly)
                {
                    partyList.Add(CreatePartyBorrower(dataLoan.sBrokerId, dataApp, E_BorrowerModeT.Borrower, dataLoan.sGseDeliveryTargetT, dataLoan.sLPurposeT));
                }

                if (dataApp.aBHasSpouse && dataApp.aCTypeT != E_aTypeT.NonTitleSpouse && dataApp.aCTypeT != E_aTypeT.TitleOnly)
                {
                    partyList.Add(CreatePartyBorrower(dataLoan.sBrokerId, dataApp, E_BorrowerModeT.Coborrower, dataLoan.sGseDeliveryTargetT, dataLoan.sLPurposeT));
                }
            }

            return partyList;
        }

        private Party CreatePartyBorrower(Guid brokerId, CAppData dataApp, E_BorrowerModeT borrowerModeT, E_sGseDeliveryTargetT sGseDeliveryTargetT, E_sLPurposeT sLPurposeT)
        {
            dataApp.BorrowerModeT = borrowerModeT;

            Party party = new Party();
            party.Addresses = CreateAddresses(dataApp);

            bool borrowerIsTrustor = false;
            if (dataApp.LoanData.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
            {
                foreach (var trustor in dataApp.LoanData.sTrustCollection.Trustors)
                {
                    if (string.Equals(trustor.Name, dataApp.aNm, StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(trustor.Name, dataApp.aFirstNm + " " + dataApp.aLastNm, StringComparison.OrdinalIgnoreCase))
                    {
                        borrowerIsTrustor = true;
                        break;
                    }
                }
            }

            if (borrowerIsTrustor)
            {
                party.LegalEntity = CreateLegalEntity(dataApp);
            }
            else
            {
                party.Individual = CreateIndividual(dataApp, sGseDeliveryTargetT);
            }

            Role role = new Role();
            role.RoleDetail.PartyRoleType = PartyRoleType.Borrower;
            role.Borrower = CreateBorrower(brokerId, dataApp, sGseDeliveryTargetT, sLPurposeT);

            party.Roles.RoleList.Add(role);
            party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(dataApp);
            return party;
        }

        private Individual CreateIndividual(CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT)
        {
            Individual individual = new Individual();

            individual.Name = CreateName(dataApp, sGseDeliveryTargetT);

            return individual;
        }

        private LegalEntity CreateLegalEntity(CAppData dataApp)
        {
            LegalEntity legalEntity = new LegalEntity();

            legalEntity.LegalEntityDetail.FullName = dataApp.aNm;
            legalEntity.LegalEntityDetail.LegalEntityType = LegalEntityType.Other;
            legalEntity.LegalEntityDetail.LegalEntityTypeOtherDescription = "LivingTrust";

            return legalEntity;

        }

        private Name CreateName(CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT)
        {
            Name name = new Name();

            name.FirstName = dataApp.aFirstNm;
            if (!string.IsNullOrEmpty(dataApp.aMidNm))
            {
                if (sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                {
                    name.MiddleName = dataApp.aMidNm.Substring(0, 1);
                }
                else
                {
                    name.MiddleName = dataApp.aMidNm;
                }
            }

            name.SuffixName = dataApp.aSuffix;
            name.LastName = dataApp.aLastNm;

            return name;

        }

        private Addresses CreateAddresses(CAppData dataApp)
        {
            Addresses addresses = new Addresses();
            addresses.AddressList.Add(CreateAddress(dataApp));
            Address address = new Address();
            return addresses;
        }

        private Address CreateAddress(CAppData dataApp)
        {
            Address address = new Address();
            if (string.IsNullOrEmpty(dataApp.aAddrMail)
    && string.IsNullOrEmpty(dataApp.aCityMail)
    && string.IsNullOrEmpty(dataApp.aStateMail)
    && string.IsNullOrEmpty(dataApp.aZipMail))
            {
                return null;
            }



            address.AddressLineText = dataApp.aAddrMail;
            address.CityName = dataApp.aCityMail;
            address.PostalCode = dataApp.aZipMail;
            address.StateCode = dataApp.aStateMail;
            address.AddressType = AddressType.Mailing;
            address.CountryCode = "US";

            return address;

        }

        private TaxpayerIdentifiers CreateTaxpayerIdentifiers(CAppData dataApp)
        {
            TaxpayerIdentifiers taxpayerIdentifiers = new TaxpayerIdentifiers();
            taxpayerIdentifiers.TaxpayerIdentifierList.Add(CreateTaxpayerIdentifier(dataApp));
            return taxpayerIdentifiers;
        }

        private TaxpayerIdentifier CreateTaxpayerIdentifier(CAppData dataApp)
        {
            TaxpayerIdentifier taxpayerIdentifier = new TaxpayerIdentifier();

            taxpayerIdentifier.TaxpayerIdentifierValue = dataApp.aSsn;

            if (!string.IsNullOrEmpty(dataApp.aSsn) && dataApp.aSsn[0] == '9')
            {
                taxpayerIdentifier.TaxpayerIdentifierType = TaxpayerIdentifierType.IndividualTaxpayerIdentificationNumber;
            }
            else
            {
                taxpayerIdentifier.TaxpayerIdentifierType = TaxpayerIdentifierType.SocialSecurityNumber;
            }

            return taxpayerIdentifier;
        }

        private Borrower CreateBorrower(Guid brokerId, CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT, E_sLPurposeT sLPurposeT)
        {
            Borrower borrower = new Borrower();

            borrower.BorrowerDetail = CreateBorrowerDetail(dataApp);
            borrower.CounselingConfirmation = CreateCounselingConfirmation(dataApp);
            borrower.CreditScores = CreateCreditScores(brokerId, dataApp, sGseDeliveryTargetT);
            borrower.Declaration = CreateDeclaration(dataApp, sGseDeliveryTargetT, sLPurposeT);
            borrower.Employers = CreateEmployers(dataApp);
            borrower.GovernmentMonitoring = CreateGovernmentMonitoring(dataApp);
            return borrower;
        }

        private GovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp)
        {
            GovernmentMonitoring governmentMonitoring = new GovernmentMonitoring();

            governmentMonitoring.HmdaRaces = CreateHmdaRaces(dataApp);
            governmentMonitoring.GovernmentMonitoringDetail = CreateGovernmentMonitoringDetail(dataApp);

            if (Phase3Parts[dataApp.LoanData.sLId] == UlddPhase3Part.Part2)
            {
                governmentMonitoring.Extension = this.CreateGovernmentMonitoringExtension(dataApp);
            }

            return governmentMonitoring;
        }

        private GovernmentMonitoringDetail CreateGovernmentMonitoringDetail(CAppData dataApp)
        {
            GovernmentMonitoringDetail governmentMonitoringDetail = new GovernmentMonitoringDetail();

            governmentMonitoringDetail.GenderType = ToMismo(dataApp.aGenderFallback);
            governmentMonitoringDetail.HMDAEthnicityType = ToMismo(dataApp.aHispanicTFallback);

            // HMDA is Phase 3 part 2 for both GSEs.
            if (Phase3Parts[dataApp.LoanData.sLId] == UlddPhase3Part.Part2)
            {
                governmentMonitoringDetail.Extension = this.CreateGovernmentMonitoringDetailExtension(dataApp);
            }

            return governmentMonitoringDetail;
        }

        private GovernmentMonitoringDetailExtension CreateGovernmentMonitoringDetailExtension(CAppData dataApp)
        {
            GovernmentMonitoringDetailExtension extension = new GovernmentMonitoringDetailExtension();

            extension.OtherHMDAGenderCollectedBasedOnVisualObservationOrNameIndicator = dataApp.aSexCollectedByObservationOrSurname.ToNullableBool();
            extension.OtherHMDAGenderRefusalIndicator = new E_GenderT[] { E_GenderT.Unfurnished, E_GenderT.MaleAndNotFurnished, E_GenderT.FemaleAndNotFurnished, E_GenderT.MaleFemaleNotFurnished }.Contains(dataApp.aGender);
            extension.OtherHMDAGenderType = ToHmdaGenderMismo(dataApp.aGender, dataApp.aInterviewMethodT);

            extension.OtherHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator = dataApp.aEthnicityCollectedByObservationOrSurname.ToNullableBool();
            extension.OtherHMDAEthnicityRefusalIndicator = dataApp.aDoesNotWishToProvideEthnicity;

            extension.OtherHMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator = dataApp.aRaceCollectedByObservationOrSurname.ToNullableBool();
            extension.OtherHMDARaceRefusalIndicator = dataApp.aDoesNotWishToProvideRace;

            return extension;
        }

        private GovernmentMonitoringExtension CreateGovernmentMonitoringExtension(CAppData dataApp)
        {
            var extension = new GovernmentMonitoringExtension();
            if (dataApp.aHispanicT.HasFlag(E_aHispanicT.Hispanic))
            {
                extension.OtherHMDAEthnicities.HMDAEthnicityList.Add(new HmdaEthnicity() { HMDAEthnicityType = HMDAEthnicityBase.HispanicOrLatino });
            }

            if (dataApp.aHispanicT.HasFlag(E_aHispanicT.NotHispanic))
            {
                extension.OtherHMDAEthnicities.HMDAEthnicityList.Add(new HmdaEthnicity() { HMDAEthnicityType = HMDAEthnicityBase.NotHispanicOrLatino });
            }

            if (dataApp.aInterviewMethodT != E_aIntrvwrMethodT.FaceToFace && dataApp.aInterviewMethodT != E_aIntrvwrMethodT.LeaveBlank && dataApp.aDoesNotWishToProvideEthnicity)
            {
                extension.OtherHMDAEthnicities.HMDAEthnicityList.Add(new HmdaEthnicity() { HMDAEthnicityType = HMDAEthnicityBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication });
            }

            if (dataApp.aIsCuban)
            {
                extension.OtherHMDAEthnicityOrigins.HMDAEthnicityOriginList.Add(new HmdaEthnicityOrigin() { HMDAEthnicityOriginType = HMDAEthnicityOriginBase.Cuban });
            }

            if (dataApp.aIsMexican)
            {
                extension.OtherHMDAEthnicityOrigins.HMDAEthnicityOriginList.Add(new HmdaEthnicityOrigin() { HMDAEthnicityOriginType = HMDAEthnicityOriginBase.Mexican });
            }

            if (dataApp.aIsPuertoRican)
            {
                extension.OtherHMDAEthnicityOrigins.HMDAEthnicityOriginList.Add(new HmdaEthnicityOrigin() { HMDAEthnicityOriginType = HMDAEthnicityOriginBase.PuertoRican });
            }

            if (dataApp.aIsOtherHispanicOrLatino)
            {
                extension.OtherHMDAEthnicityOrigins.HMDAEthnicityOriginList.Add(new HmdaEthnicityOrigin()
                    {
                        HMDAEthnicityOriginType = HMDAEthnicityOriginBase.Other,
                        HMDAEthnicityOriginTypeOtherDescription = string.IsNullOrEmpty(dataApp.aOtherHispanicOrLatinoDescription) ? null : dataApp.aOtherHispanicOrLatinoDescription
                    });
            }

            return extension;
        }

        private static HMDAGenderBase ToHmdaGenderMismo(E_GenderT GenderT, E_aIntrvwrMethodT interviewMethod)
        {
            switch (GenderT)
            {
                case E_GenderT.MaleAndFemale:
                case E_GenderT.MaleFemaleNotFurnished:
                    return HMDAGenderBase.ApplicantHasSelectedBothMaleAndFemale;
                case E_GenderT.Male:
                case E_GenderT.MaleAndNotFurnished:
                    return HMDAGenderBase.Male;
                case E_GenderT.Female:
                case E_GenderT.FemaleAndNotFurnished:
                    return HMDAGenderBase.Female;
                case E_GenderT.Unfurnished:
                    return HMDAGenderBase.LeaveBlank;
                case E_GenderT.LeaveBlank:
                    if (interviewMethod != E_aIntrvwrMethodT.LeaveBlank && interviewMethod != E_aIntrvwrMethodT.FaceToFace)
                    {
                        return HMDAGenderBase.InformationNotProvidedUnknown;
                    }
                    else
                    {
                        return HMDAGenderBase.LeaveBlank;
                    }
                case E_GenderT.NA:
                    return HMDAGenderBase.NotApplicable;
                default:
                    throw new UnhandledEnumException(GenderT);
            }
        }

        private GenderType ToMismo(E_GenderT GenderT)
        {
            switch (GenderT)
            {
                case E_GenderT.Male:
                    return GenderType.Male;
                case E_GenderT.Female:
                    return GenderType.Female;
                case E_GenderT.Unfurnished:
                case E_GenderT.LeaveBlank:
                case E_GenderT.NA:
                    return GenderType.InformationNotProvidedUnknown;
                default:
                    throw new UnhandledEnumException(GenderT);
            }
        }
        private HMDAEthnicityType ToMismo(E_aHispanicT aHispanicT)
        {
            switch (aHispanicT)
            {
                case E_aHispanicT.Hispanic:
                    return HMDAEthnicityType.HispanicOrLatino;
                case E_aHispanicT.NotHispanic:
                    return HMDAEthnicityType.NotHispanicOrLatino;
                case E_aHispanicT.LeaveBlank:
                    return HMDAEthnicityType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
                default:
                    throw new UnhandledEnumException(aHispanicT);
            }
        }

        private HmdaRaces CreateHmdaRaces(CAppData dataApp)
        {
            HmdaRaces hmdaRaces = new HmdaRaces();

            if (Phase3Parts[dataApp.LoanData.sLId] == UlddPhase3Part.Part2)
            {
                if (dataApp.aIsAmericanIndian)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.AmericanIndianOrAlaskaNative,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.AmericanIndianOrAlaskaNative,
                            OtherHMDARaceTypeAdditionalDescription = string.IsNullOrEmpty(dataApp.aOtherAmericanIndianDescription) ? null : dataApp.aOtherAmericanIndianDescription
                        }
                    };
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }

                if (dataApp.aIsAsian)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.Asian,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.Asian
                        }
                    };
                    if (dataApp.aIsAsianIndian)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.AsianIndian });
                    }
                    if (dataApp.aIsChinese)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Chinese });
                    }
                    if (dataApp.aIsFilipino)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Filipino });
                    }
                    if (dataApp.aIsJapanese)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Japanese });
                    }
                    if (dataApp.aIsKorean)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Korean });
                    }
                    if (dataApp.aIsVietnamese)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Vietnamese });
                    }
                    if (dataApp.aIsOtherAsian)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(
                            new HmdaRaceDesignation
                            {
                                HMDARaceDesignationType = HMDARaceDesignationBase.OtherAsian,
                                HMDARaceDesignationOtherAsianDescription = string.IsNullOrEmpty(dataApp.aOtherAsianDescription) ? null : dataApp.aOtherAsianDescription
                            });
                    }
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }

                if (dataApp.aIsBlack)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.BlackOrAfricanAmerican,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.BlackOrAfricanAmerican
                        }
                    };
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }

                if (dataApp.aIsPacificIslander)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.NativeHawaiianOrOtherPacificIslander,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.NativeHawaiianOrOtherPacificIslander
                        }
                    };
                    if (dataApp.aIsNativeHawaiian)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.NativeHawaiian });
                    }
                    if (dataApp.aIsGuamanianOrChamorro)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.GuamanianOrChamorro });
                    }
                    if (dataApp.aIsSamoan)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(new HmdaRaceDesignation { HMDARaceDesignationType = HMDARaceDesignationBase.Samoan });
                    }
                    if (dataApp.aIsOtherPacificIslander)
                    {
                        hmdaRace.Extension.OtherHMDARaceDesignations.HMDARaceDesignationList.Add(
                            new HmdaRaceDesignation
                            {
                                HMDARaceDesignationType = HMDARaceDesignationBase.OtherPacificIslander,
                                HMDARaceDesignationOtherPacificIslanderDescription = string.IsNullOrEmpty(dataApp.aOtherPacificIslanderDescription) ? null : dataApp.aOtherPacificIslanderDescription
                            });
                    }
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }

                if (dataApp.aIsWhite)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.White,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.White
                        }
                    };
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }

                if (dataApp.aDoesNotWishToProvideRace)
                {
                    var hmdaRace = new HmdaRace()
                    {
                        HMDARaceType = HMDARaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
                        Extension = new HmdaRaceExtension()
                        {
                            OtherHMDARaceType = HMDARaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication
                        }
                    };
                    hmdaRaces.HmdaRaceList.Add(hmdaRace);
                }
            }
            else
            {
                hmdaRaces.HmdaRaceList.Add(CreateHmdaRace(dataApp));
            }

            return hmdaRaces;
        }

        private HmdaRace CreateHmdaRace(CAppData dataApp)
        {
            HmdaRace hmdaRace = new HmdaRace();

            if (dataApp.aIsAmericanIndian)
            {
                hmdaRace.HMDARaceType = HMDARaceType.AmericanIndianOrAlaskaNative;
            }
            else if (dataApp.aIsAsian)
            {
                hmdaRace.HMDARaceType = HMDARaceType.Asian;
            }
            else if (dataApp.aIsBlack)
            {
                hmdaRace.HMDARaceType = HMDARaceType.BlackOrAfricanAmerican;
            }
            else if (dataApp.aIsPacificIslander)
            {
                hmdaRace.HMDARaceType = HMDARaceType.NativeHawaiianOrOtherPacificIslander;
            }
            else if (dataApp.aIsWhite)
            {
                hmdaRace.HMDARaceType = HMDARaceType.White;
            }
            else
            {
                hmdaRace.HMDARaceType = HMDARaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
            }

            return hmdaRace;
        }

        private Declaration CreateDeclaration(CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT, E_sLPurposeT sLPurposeT)
        {
            Declaration declaration = new Declaration();

            declaration.DeclarationDetail = CreateDeclarationDetail(dataApp, sGseDeliveryTargetT, sLPurposeT);
            return declaration;
        }

        private DeclarationDetail CreateDeclarationDetail(CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT, E_sLPurposeT sLPurposeT)
        {
            DeclarationDetail declarationDetail = new DeclarationDetail();

            //// 12/27/2014 IR - OPM 197829 - PartyRoleType == Borrower (Hard-coded true) && LoanPurposeType == Purchase && PropertyUsageType == PrimaryResidence
            if (sLPurposeT == E_sLPurposeT.Purchase && dataApp.LoanData.sOccT == E_sOccT.PrimaryResidence)
            {
                //// ir - OPM 190193
                if (dataApp.LoanData.BrokerDB.Export_sHas1stTimeBuyer_as_FTHB_for_ULDD)
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = dataApp.LoanData.sHas1stTimeBuyer;
                }
                else if (dataApp.aDecPastOwnership.Equals("Y"))
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = false;
                }
                else if (dataApp.aDecPastOwnership.Equals("N"))
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = true;
                }
                else
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = false;
                }
            }

            if (dataApp.aDecCitizen == "Y")
            {
                declarationDetail.CitizenshipResidencyType = CitizenshipResidencyType.USCitizen;
            }
            else if (dataApp.aDecCitizen != "Y" && dataApp.aDecResidency == "Y")
            {
                declarationDetail.CitizenshipResidencyType = CitizenshipResidencyType.PermanentResidentAlien;
            }
            else if (dataApp.aDecCitizen != "Y" && dataApp.aDecResidency == "N")
            {
                declarationDetail.CitizenshipResidencyType = CitizenshipResidencyType.NonPermanentResidentAlien;
            }
                        
            switch (dataApp.BorrowerModeT)
            {
                case E_BorrowerModeT.Borrower:
                    declarationDetail.BankruptcyIndicator = dataApp.aBDecBankrupt == "Y";
                    declarationDetail.LoanForeclosureOrJudgmentIndicator = this.GetLoanForeclosureOrJudgmentIndicator(dataApp.aBDecObligated);
                    break;
                case E_BorrowerModeT.Coborrower:
                    declarationDetail.BankruptcyIndicator = dataApp.aCDecBankrupt == "Y";
                    declarationDetail.LoanForeclosureOrJudgmentIndicator = this.GetLoanForeclosureOrJudgmentIndicator(dataApp.aCDecObligated);
                    break;
                default:
                    throw new UnhandledEnumException(dataApp.BorrowerModeT);
            }

            return declarationDetail;
        }

        private bool? GetLoanForeclosureOrJudgmentIndicator(string loanObligationIndicator)
        {
            if (string.IsNullOrWhiteSpace(loanObligationIndicator))
            {
                return null;
            }

            return string.Equals(loanObligationIndicator, "Y", StringComparison.OrdinalIgnoreCase);
        }

        private CreditScores CreateCreditScores(Guid brokerId, CAppData dataApp, E_sGseDeliveryTargetT sGseDeliveryTargetT)
        {
            CreditScores creditScores = new CreditScores();
            CreditScore creditScore = new CreditScore();

            int EquifaxScore = 0;
            try
            {
                EquifaxScore = Convert.ToInt32(dataApp.aEquifaxScore);
            }
            catch (FormatException) { }

            int ExperianScore = 0;
            try
            {
                ExperianScore = Convert.ToInt32(dataApp.aExperianScore);
            }
            catch (FormatException) { }

            int TransUnionScore = 0;
            try
            {
                TransUnionScore = Convert.ToInt32(dataApp.aTransUnionScore);
            }
            catch (FormatException) { }

            int score = 0;
            int creditScoresOnFile = 0;
            CreditRepositorySourceType type = CreditRepositorySourceType.TransUnion;

            if (EquifaxScore > 0)
                creditScoresOnFile++;
            if (ExperianScore > 0)
                creditScoresOnFile++;
            if (TransUnionScore > 0)
                creditScoresOnFile++;

            if (creditScoresOnFile == 0)
            {
                // Non-traditional credit scenario
                creditScore.CreditScoreDetail.CreditRepositorySourceIndicator = false;
                creditScores.CreditScoreList.Add(creditScore);
                return creditScores;
            }
            else if (creditScoresOnFile == 1)
            {
                if (TransUnionScore > score)
                {
                    score = TransUnionScore;
                    type = CreditRepositorySourceType.TransUnion;
                }
                else if (ExperianScore > score)
                {
                    score = ExperianScore;
                    type = CreditRepositorySourceType.Experian;
                }
                else if (EquifaxScore > score)
                {
                    score = EquifaxScore;
                    type = CreditRepositorySourceType.Equifax;
                }
            }
            // If exactly one of the scores is 0, middle of three (incl. 0) is the same as lower of two (excl. 0)
            else if (creditScoresOnFile == 2 || creditScoresOnFile == 3)
            {
                if ((ExperianScore >= TransUnionScore && TransUnionScore >= EquifaxScore) ||
                    (ExperianScore <= TransUnionScore && TransUnionScore <= EquifaxScore))
                {
                    score = TransUnionScore;
                    type = CreditRepositorySourceType.TransUnion;
                }
                else if ((EquifaxScore >= ExperianScore && ExperianScore >= TransUnionScore) ||
                         (EquifaxScore <= ExperianScore && ExperianScore <= TransUnionScore))
                {
                    score = ExperianScore;
                    type = CreditRepositorySourceType.Experian;
                }
                else
                {
                    score = EquifaxScore;
                    type = CreditRepositorySourceType.Equifax;
                }
            }

            if (sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && !dataApp.aCreditReportIdLckd)
            {
                // ir - iOPM 190789: Lifted from OrderCredit.aspx.cs
                Guid ApplicationID = dataApp.aAppId;
                string lastCra = string.Empty;
                string creditScoreProviderName = string.Empty;

                SqlParameter[] parameters = { new SqlParameter("@ApplicationID", ApplicationID) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {
                        if (reader["CrAccProxyId"] is DBNull)
                            lastCra = reader["ComId"].ToString();
                        else
                            lastCra = reader["CrAccProxyId"].ToString();

                        // 10/24/2005 dd - if LastCra == POINT Dummy then set lastCra = null
                        if (new Guid(lastCra) == ConstAppDavid.DummyPointServiceCompany)
                            lastCra = null;
                    }
                }

                if (!string.IsNullOrEmpty(lastCra) &&
                    LoanProspectorCreditReportingCompanyMapping.TryGetLpCrcCodeForUlddExport(new Guid(lastCra), out creditScoreProviderName))
                {
                    creditScore.CreditScoreProvider = CreateCreditScoreProvider(creditScoreProviderName);
                    type = CreditRepositorySourceType.MergedData;
                }
            }

            creditScore.CreditScoreDetail.CreditReportIdentifier = dataApp.aCreditReportId;            

            creditScore.CreditScoreDetail.CreditRepositorySourceIndicator = true;
            creditScore.CreditScoreDetail.CreditRepositorySourceType = type;
            creditScore.CreditScoreDetail.CreditScoreValue = score.ToString();

            creditScores.CreditScoreList.Add(creditScore);

            return creditScores;
        }

        private CounselingConfirmation CreateCounselingConfirmation(CAppData dataApp)
        {
            CounselingConfirmation counselingConfirmation = new CounselingConfirmation();

            E_aHomeOwnershipCounselingSourceT sourceT = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? dataApp.aBHomeOwnershipCounselingSourceT : dataApp.aCHomeOwnershipCounselingSourceT;
            E_aHomeOwnershipCounselingFormatT formatT = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? dataApp.aBHomeOwnershipCounselingFormatT : dataApp.aCHomeOwnershipCounselingFormatT;

            switch (sourceT)
            {
                case E_aHomeOwnershipCounselingSourceT.BorrowerDidNotParticipate:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.Other;
                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = "BorrowerDidNotParticipate";
                    break;
                case E_aHomeOwnershipCounselingSourceT.GovernmentAgency:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.GovernmentAgency;
                    break;
                case E_aHomeOwnershipCounselingSourceT.HUDApprovedCounselingAgency:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.HUDApprovedCounselingAgency;
                    break;
                case E_aHomeOwnershipCounselingSourceT.LenderTrainedCounseling:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.LenderTrainedCounseling;
                    break;
                case E_aHomeOwnershipCounselingSourceT.MortgageInsuranceCompany:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.Other;
                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = "MortgageInsuranceCompany";
                    break;
                case E_aHomeOwnershipCounselingSourceT.NoBorrowerCounseling:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.NoBorrowerCounseling;
                    break;
                case E_aHomeOwnershipCounselingSourceT.NonProfitOrganization:
                    counselingConfirmation.CounselingConfirmationType = CounselingConfirmationType.Other;
                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = "NonProfitOrganization";
                    break;
            }

            switch (formatT)
            {
                case E_aHomeOwnershipCounselingFormatT.BorrowerDidNotParticipate:
                    counselingConfirmation.CounselingFormatType = ULDD.CounselingFormatType.Other;
                    counselingConfirmation.CounselingFormatTypeOtherDescription = "BorrowerDidNotParticipate";
                    break;
                case E_aHomeOwnershipCounselingFormatT.BorrowerEducationNotRequired:
                    counselingConfirmation.CounselingFormatType = ULDD.CounselingFormatType.BorrowerEducationNotRequired;
                    break;
                case E_aHomeOwnershipCounselingFormatT.Classroom:
                    counselingConfirmation.CounselingFormatType = ULDD.CounselingFormatType.Classroom;
                    break;
                case E_aHomeOwnershipCounselingFormatT.HomeStudy:
                    counselingConfirmation.CounselingFormatType = ULDD.CounselingFormatType.HomeStudy;
                    break;
                case E_aHomeOwnershipCounselingFormatT.Individual:
                    counselingConfirmation.CounselingFormatType = ULDD.CounselingFormatType.Individual;
                    break;
            }

            return counselingConfirmation;
        }

        private BorrowerDetail CreateBorrowerDetail(CAppData dataApp)
        {
            BorrowerDetail borrowerDetail = new BorrowerDetail();
            borrowerDetail.BorrowerAgeAtApplicationYearsCount = dataApp.aAge_rep;
            borrowerDetail.BorrowerBirthDate = dataApp.aDob_rep;
            borrowerDetail.BorrowerClassificationType = (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower && dataApp.aIsPrimary) ? BorrowerClassificationType.Primary : BorrowerClassificationType.Secondary;

            if (borrowerDetail.BorrowerClassificationType == BorrowerClassificationType.Primary)
            {
                borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator = dataApp.aAddrMailSourceT == E_aAddrMailSourceT.SubjectPropertyAddress
                                                                            || (dataApp.aAddrMailSourceT == E_aAddrMailSourceT.PresentAddress
                                                                            && dataApp.aBorrowerAddressSameAsSubjectProperty);
            }

            borrowerDetail.BorrowerQualifyingIncomeAmount = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? TruncateMoneyAmount(dataApp.aBTotI_rep) : TruncateMoneyAmount(dataApp.aCTotI_rep);
            return borrowerDetail;
        }
        private Party CreatePartyAppraiser(CPageData dataLoan)
        {
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            Role role = new Role();
            role.RoleDetail.PartyRoleType = PartyRoleType.Appraiser;
            role.Appraiser.AppraiserLicense.AppraiserLicenseIdentifier = agent.LicenseNumOfAgent;

            Party party = new Party();
            party.Roles.RoleList.Add(role);

            return party;

        }
        private Party CreatePartyAppraiserSupervisor(string appraiserLicenseIdentifier)
        {
            Role role = new Role();
            role.RoleDetail.PartyRoleType = PartyRoleType.AppraiserSupervisor;

            role.AppraiserSupervisor.AppraiserLicense.AppraiserLicenseIdentifier = appraiserLicenseIdentifier;

            Party party = new Party();
            party.Roles.RoleList.Add(role);

            return party;

        }
        private Party CreatePartyLoanOriginationCompany(CPageData dataLoan)
        {
            // sg 11/30/2015 - OPM 231889: If no NMLS ID for DE, ME, or MO, send default IDs.
            string originationCompanyIdentifier = dataLoan.sApp1003InterviewerLoanOriginationCompanyIdentifier;
            if (string.IsNullOrEmpty(originationCompanyIdentifier))
            {
                if (dataLoan.sSpState.Equals("DE", StringComparison.OrdinalIgnoreCase))
                {
                    originationCompanyIdentifier = "1001";
                }
                else if (dataLoan.sSpState.Equals("ME", StringComparison.OrdinalIgnoreCase))
                {
                    originationCompanyIdentifier = "1002";
                }
                else if (dataLoan.sSpState.Equals("MO", StringComparison.OrdinalIgnoreCase))
                {
                    originationCompanyIdentifier = "1003";
                }
            }

            return CreateParty(PartyRoleType.LoanOriginationCompany, originationCompanyIdentifier);
        }

        private Party CreatePartyLoanOriginator(CPageData dataLoan)
        {
            Party party = CreateParty(PartyRoleType.LoanOriginator, dataLoan.sApp1003InterviewerLoanOriginatorIdentifier);

            if (dataLoan.sBranchChannelT != E_BranchChannelT.Blank)
            {
                party.Roles.RoleList[0].LoanOriginator.LoanOriginatorType = ToMismo(dataLoan.sBranchChannelT);
            }
            return party;
        }

        private LoanOriginatorType ToMismo(E_BranchChannelT sBranchChannelT)
        {
            switch (sBranchChannelT)
            {
                case E_BranchChannelT.Blank:
                    return LoanOriginatorType.Undefined;
                case E_BranchChannelT.Retail:
                    return LoanOriginatorType.Lender;
                case E_BranchChannelT.Correspondent:
                    return LoanOriginatorType.Correspondent;
                case E_BranchChannelT.Wholesale:
                case E_BranchChannelT.Broker:
                    return LoanOriginatorType.Broker;
                default:
                    throw new UnhandledEnumException(sBranchChannelT);
            }
        }
        private Party CreatePartyNotePayTo(CPageData dataLoan)
        {
            LegalEntity legalEntity = new LegalEntity();

            if (dataLoan.sDocMagicAlternateLenderId != Guid.Empty)
            {
                DocMagicAlternateLender altLender = new DocMagicAlternateLender(dataLoan.sBrokerId, dataLoan.sDocMagicAlternateLenderId);
                legalEntity.LegalEntityDetail.FullName = altLender.MakePaymentsToName;
            }
            else
            {
                var lenderAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (lenderAgent != CAgentFields.Empty)
                {
                    legalEntity.LegalEntityDetail.FullName = lenderAgent.CompanyName;
                }
            }

            Role role = new Role();
            role.RoleDetail.PartyRoleType = PartyRoleType.NotePayTo;

            Party party = new Party();
            party.Roles.RoleList.Add(role);

            party.LegalEntity = legalEntity;
            return party;
        }

        private Party CreatePartyHomeownersAssociation(CPageData dataLoan)
        {
            var hoaAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.HomeOwnerAssociation, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!hoaAgent.IsValid || string.IsNullOrEmpty(hoaAgent.TaxId))
            {
                return null;
            }

            Party hoaParty = new Party();
            var taxpayerIdentifier = new TaxpayerIdentifier
            {
                TaxpayerIdentifierType = TaxpayerIdentifierType.EmployerIdentificationNumber,
                TaxpayerIdentifierValue = hoaAgent.TaxId
            };
            hoaParty.TaxpayerIdentifiers = new TaxpayerIdentifiers();
            hoaParty.TaxpayerIdentifiers.TaxpayerIdentifierList.Add(taxpayerIdentifier);
            var role = new Role
            {
                RoleDetail = new RoleDetail
                {
                    PartyRoleType = PartyRoleType.Other,
                    PartyRoleTypeOtherDescription = "HomeownersAssociation"
                }
            };
            hoaParty.Roles.RoleList.Add(role);
            return hoaParty;
        }

        protected Deals CreateDeals()
        {
            Deals deals = new Deals();

            foreach (CPageData dataLoan in m_dataLoanList)
            {
                deals.DealList.Add(CreateDeal(dataLoan));
            }

            return deals;
        }

        protected virtual DealSet CreateDealSet()
        {
            DealSet dealSet = new DealSet();

            // dealSet.Ach = CreateAch();
            // dealSet.CashRemittanceSummaryNotification = CreateCashRemittanceSummaryNotification();
            dealSet.Deals = CreateDeals();
            // dealSet.InvestorFeatures = CreateInvestorFeatures();
            // dealSet.Parties = CreateParties();
            // dealSet.Pool = CreatePool();
            // dealSet.Relationships = CreateRelationships();
            // dealSet.ServicerReporting = CreateServicerReporting();
            // dealSet.SequenceNumber = ;

            return dealSet;
        }

        private DealSets CreateDealSets()
        {
            DealSets dealSets = new DealSets();

            dealSets.DealSetList.Add(CreateDealSet());
            // dealSets.DealSetServices = CreateDealSetServices();
            // dealSets.VerificationData = CreateVerificationData();

            #region Create LoanDeliveryFilePreparer
            if (m_dataLoanList.Count > 0 && m_dataLoanList.First().sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
            {
                dealSets.Parties.PartyList.Add(CreateParty(PartyRoleType.LoanDeliveryFilePreparer, LoanDeliveryFilePreparerIdentifier_Freddie));
            }
            else
            {
                dealSets.Parties.PartyList.Add(CreateParty(PartyRoleType.LoanDeliveryFilePreparer, LoanDeliveryFilePreparerIdentifier));
            }
            #endregion
            return dealSets;
        }

        private Party CreateParty(PartyRoleType roleType, string roleIdentifier)
        {
            if (string.IsNullOrEmpty(roleIdentifier))
            {
                return null;
            }

            Party party = new Party();

            Role role = new Role();
            role.RoleDetail.PartyRoleType = roleType;
            party.Roles.RoleList.Add(role);

            PartyRoleIdentifier partyRoleIdentifier = new PartyRoleIdentifier();
            partyRoleIdentifier._PartyRoleIdentifier = roleIdentifier;
            party.Roles.PartyRoleIdentifiers.PartyRoleIdentifierList.Add(partyRoleIdentifier);

            return party;
        }

        private DealSetService CreateDealSetService()
        {
            DealSetService dealSetService = new DealSetService();

            // dealSetService.SequenceNumber = ;

            return dealSetService;
        }

        private DealSetServices CreateDealSetServices()
        {
            DealSetServices dealSetServices = new DealSetServices();

            // dealSetServices.DealSetServiceList.Add(CreateDealSetService());

            return dealSetServices;
        }

        private Declaration CreateDeclaration()
        {
            Declaration declaration = new Declaration();

            // declaration.DeclarationDetail = CreateDeclarationDetail();
            // declaration.DeclarationExplanations = CreateDeclarationExplanations();

            return declaration;
        }

        private DeclarationDetail CreateDeclarationDetail()
        {
            DeclarationDetail declarationDetail = new DeclarationDetail();

            // declarationDetail.AlimonyChildSupportObligationIndicator = ;
            // declarationDetail.BankruptcyIndicator = ;
            // declarationDetail.BorrowedDownPaymentIndicator = ;
            // declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ;
            // declarationDetail.CitizenshipResidencyType = ;
            // declarationDetail.CoMakerEndorserOfNoteIndicator = ;
            // declarationDetail.HomeownerPastThreeYearsType = ;
            // declarationDetail.IntentToOccupyType = ;
            // declarationDetail.LoanForeclosureOrJudgmentIndicator = ;
            // declarationDetail.OutstandingJudgmentsIndicator = ;
            // declarationDetail.PartyToLawsuitIndicator = ;
            // declarationDetail.PresentlyDelinquentIndicator = ;
            // declarationDetail.PriorPropertyTitleType = ;
            // declarationDetail.PriorPropertyUsageType = ;
            // declarationDetail.PropertyForeclosedPastSevenYearsIndicator = ;

            return declarationDetail;
        }

        private DeclarationExplanation CreateDeclarationExplanation()
        {
            DeclarationExplanation declarationExplanation = new DeclarationExplanation();

            // declarationExplanation.DeclarationExplanationDescription = ;
            // declarationExplanation.DeclarationExplanationType = ;
            // declarationExplanation.DeclarationExplanationTypeOtherDescription = ;
            // declarationExplanation.SequenceNumber = ;

            return declarationExplanation;
        }

        private DeclarationExplanations CreateDeclarationExplanations()
        {
            DeclarationExplanations declarationExplanations = new DeclarationExplanations();

            // declarationExplanations.DeclarationExplanationList.Add(CreateDeclarationExplanation());

            return declarationExplanations;
        }

        private DelinquencyNotification CreateDelinquencyNotification()
        {
            DelinquencyNotification delinquencyNotification = new DelinquencyNotification();

            // delinquencyNotification.DelinquencyNotificationDetail = CreateDelinquencyNotificationDetail();
            // delinquencyNotification.DelinquencyNotificationStatuses = CreateDelinquencyNotificationStatuses();
            // delinquencyNotification.DelinquencyNotificationEvents = CreateDelinquencyNotificationEvents();

            return delinquencyNotification;
        }

        private DelinquencyNotificationDetail CreateDelinquencyNotificationDetail()
        {
            DelinquencyNotificationDetail delinquencyNotificationDetail = new DelinquencyNotificationDetail();

            // delinquencyNotificationDetail.BankruptcySuspenseBalanceAmount = ;
            // delinquencyNotificationDetail.DelinquencyEffectiveDate = ;
            // delinquencyNotificationDetail.DelinquencyReportingPropertyOverallConditionType = ;
            // delinquencyNotificationDetail.DelinquencyReportingPropertyOverallConditionTypeOtherDescription = ;
            // delinquencyNotificationDetail.LoanDelinquencyReasonType = ;
            // delinquencyNotificationDetail.LoanDelinquencyReasonTypeOtherDescription = ;
            // delinquencyNotificationDetail.PropertyCurrentOccupancyType = ;
            // delinquencyNotificationDetail.RepaymentPlanDownPaymentAmount = ;
            // delinquencyNotificationDetail.RepaymentPlanMonthlyDueAmount = ;
            // delinquencyNotificationDetail.RepaymentPlanTermMonthsCount = ;
            // delinquencyNotificationDetail.TaxAdvancesOnAccountsNotEscrowedIndicator = ;

            return delinquencyNotificationDetail;
        }

        private DelinquencyNotificationEvent CreateDelinquencyNotificationEvent()
        {
            DelinquencyNotificationEvent delinquencyNotificationEvent = new DelinquencyNotificationEvent();

            // delinquencyNotificationEvent.LoanDelinquencyEventDate = ;
            // delinquencyNotificationEvent.LoanDelinquencyEventType = ;
            // delinquencyNotificationEvent.LoanDelinquencyEventTypeOtherDescription = ;
            // delinquencyNotificationEvent.SequenceNumber = ;

            return delinquencyNotificationEvent;
        }

        private DelinquencyNotificationEvents CreateDelinquencyNotificationEvents()
        {
            DelinquencyNotificationEvents delinquencyNotificationEvents = new DelinquencyNotificationEvents();

            // delinquencyNotificationEvents.DelinquencyNotificationEventList.Add(CreateDelinquencyNotificationEvent());

            return delinquencyNotificationEvents;
        }

        private DelinquencyNotificationStatus CreateDelinquencyNotificationStatus()
        {
            DelinquencyNotificationStatus delinquencyNotificationStatus = new DelinquencyNotificationStatus();

            // delinquencyNotificationStatus.DelinquentPaymentCount = ;
            // delinquencyNotificationStatus.LoanDelinquencyStatusDate = ;
            // delinquencyNotificationStatus.LoanDelinquencyStatusType = ;
            // delinquencyNotificationStatus.LoanDelinquencyStatusTypeOtherDescription = ;
            // delinquencyNotificationStatus.PaymentDelinquencyStatusType = ;
            // delinquencyNotificationStatus.SequenceNumber = ;

            return delinquencyNotificationStatus;
        }

        private DelinquencyNotificationStatuses CreateDelinquencyNotificationStatuses()
        {
            DelinquencyNotificationStatuses delinquencyNotificationStatuses = new DelinquencyNotificationStatuses();

            // delinquencyNotificationStatuses.DelinquencyNotificationStatusList.Add(CreateDelinquencyNotificationStatus());

            return delinquencyNotificationStatuses;
        }

        private DelinquencySummary CreateDelinquencySummary()
        {
            DelinquencySummary delinquencySummary = new DelinquencySummary();

            // delinquencySummary.DelinquentPaymentCount = ;
            // delinquencySummary.DelinquentPaymentsOverPastTwelveMonthsCount = ;
            // delinquencySummary.LoanDelinquencyHistoryPeriodMonthsCount = ;
            // delinquencySummary.OnTimePaymentCount = ;
            // delinquencySummary.PaymentDelinquentDaysCount = ;
            // delinquencySummary.TotalDelinquentInterestAmount = ;

            return delinquencySummary;
        }

        private Dependent CreateDependent()
        {
            Dependent dependent = new Dependent();

            // dependent.DependentAgeYearsCount = ;
            // dependent.SequenceNumber = ;

            return dependent;
        }

        private Dependents CreateDependents()
        {
            Dependents dependents = new Dependents();

            // dependents.DependentList.Add(CreateDependent());

            return dependents;
        }

        private DisclosureOnServicer CreateDisclosureOnServicer()
        {
            DisclosureOnServicer disclosureOnServicer = new DisclosureOnServicer();

            // disclosureOnServicer.ServicerDaysOfTheWeekDescription = ;
            // disclosureOnServicer.ServicerDirectInquiryToDescription = ;
            // disclosureOnServicer.ServicerHoursOfTheDayDescription = ;
            // disclosureOnServicer.ServicerInquiryTelephoneValue = ;
            // disclosureOnServicer.ServicerPaymentAcceptanceEndDate = ;
            // disclosureOnServicer.ServicerPaymentAcceptanceStartDate = ;
            // disclosureOnServicer.ServicingTransferEffectiveDate = ;
            // disclosureOnServicer.TransferOfServicingDisclosureType = ;
            // disclosureOnServicer.TransferOfServicingDisclosureTypeOtherDescription = ;

            return disclosureOnServicer;
        }

        private DisqualificationReason CreateDisqualificationReason()
        {
            DisqualificationReason disqualificationReason = new DisqualificationReason();

            // disqualificationReason.DisqualificationReasonText = ;
            // disqualificationReason.SequenceNumber = ;

            return disqualificationReason;
        }

        private DisqualificationReasons CreateDisqualificationReasons()
        {
            DisqualificationReasons disqualificationReasons = new DisqualificationReasons();

            // disqualificationReasons.DisqualificationReasonList.Add(CreateDisqualificationReason());

            return disqualificationReasons;
        }

        private Document CreateDocument()
        {
            Document document = new Document();

            // document.Reference = CreateReference();
            // document.UnknownVersion3Document = CreateUnknownVersion3Document();
            // document.AuditTrail = CreateAuditTrail();
            // document.DealSets = CreateDealSets();
            // document.Map = CreateMap();
            // document.Relationships = CreateRelationships();
            // document.Signatories = CreateSignatories();
            // document.SystemSignatures = CreateSystemSignatures();
            // document.Views = CreateViews();
            // document.AboutVersions = CreateAboutVersions();
            // document.DocumentClassification = CreateDocumentClassification();
            // document.SequenceNumber = ;
            // document.MISMOReferenceModelIdentifier = ;

            return document;
        }

        private Documentation CreateDocumentation()
        {
            Documentation documentation = new Documentation();

            // documentation.AssetDocumentationLevelIdentifier = ;
            // documentation.AssetDocumentationType = ;
            // documentation.AssetDocumentationTypeOtherDescription = ;
            // documentation.AssetVerificationRangeCount = ;
            // documentation.AssetVerificationRangeType = ;
            // documentation.AssetVerificationRangeTypeOtherDescription = ;
            // documentation.DocumentationStateType = ;
            // documentation.DocumentationStateTypeOtherDescription = ;
            // documentation.EmploymentDocumentationLevelIdentifier = ;
            // documentation.EmploymentDocumentationType = ;
            // documentation.EmploymentDocumentationTypeOtherDescription = ;
            // documentation.EmploymentVerificationRangeCount = ;
            // documentation.EmploymentVerificationRangeType = ;
            // documentation.EmploymentVerificationRangeTypeOtherDescription = ;
            // documentation.IncomeDocumentationLevelIdentifier = ;
            // documentation.IncomeDocumentationType = ;
            // documentation.IncomeDocumentationTypeOtherDescription = ;
            // documentation.IncomeVerificationRangeCount = ;
            // documentation.IncomeVerificationRangeType = ;
            // documentation.IncomeVerificationRangeTypeOtherDescription = ;
            // documentation.SequenceNumber = ;

            return documentation;
        }

        private Documentations CreateDocumentations()
        {
            Documentations documentations = new Documentations();

            // documentations.DocumentationList.Add(CreateDocumentation());

            return documentations;
        }

        private Documents CreateDocuments()
        {
            Documents documents = new Documents();

            // documents.DocumentList.Add(CreateDocument());

            return documents;
        }

        private DocumentClass CreateDocumentClass()
        {
            DocumentClass documentClass = new DocumentClass();

            // documentClass.DocumentSubtypeType = ;
            // documentClass.DocumentSubtypeTypeOtherDescription = ;
            // documentClass.DocumentSupertypeType = ;
            // documentClass.DocumentType = ;
            // documentClass.DocumentTypeOtherDescription = ;
            // documentClass.RecordingJurisdictionDocumentCode = ;
            // documentClass.SequenceNumber = ;

            return documentClass;
        }

        private DocumentClasses CreateDocumentClasses()
        {
            DocumentClasses documentClasses = new DocumentClasses();

            // documentClasses.DocumentClassList.Add(CreateDocumentClass());

            return documentClasses;
        }

        private DocumentClassification CreateDocumentClassification()
        {
            DocumentClassification documentClassification = new DocumentClassification();

            // documentClassification.DocumentClasses = CreateDocumentClasses();
            // documentClassification.DocumentClassificationDetail = CreateDocumentClassificationDetail();

            return documentClassification;
        }

        private DocumentClassificationDetail CreateDocumentClassificationDetail()
        {
            DocumentClassificationDetail documentClassificationDetail = new DocumentClassificationDetail();

            // documentClassificationDetail.AcceptableSigningMethodType = ;
            // documentClassificationDetail.DocumentCommentDescription = ;
            // documentClassificationDetail.DocumentFormIssuingEntityNumberIdentifier = ;
            // documentClassificationDetail.DocumentFormIssuingEntityVersionIdentifier = ;
            // documentClassificationDetail.DocumentFormIssuingEntityNameType = ;
            // documentClassificationDetail.DocumentFormIssuingEntityNameTypeOtherDescription = ;
            // documentClassificationDetail.DocumentFormPublisherEntityName = ;
            // documentClassificationDetail.DocumentFormPublisherNumberIdentifier = ;
            // documentClassificationDetail.DocumentFormPublisherVersionIdentifier = ;
            // documentClassificationDetail.DocumentHasBeenRecordedIndicator = ;
            // documentClassificationDetail.DocumentName = ;
            // documentClassificationDetail.DocumentNegotiableInstrumentIndicator = ;
            // documentClassificationDetail.DocumentPeriodEndDate = ;
            // documentClassificationDetail.DocumentPeriodStartDate = ;
            // documentClassificationDetail.DocumentReceiptDatetime = ;
            // documentClassificationDetail.DocumentRecordationProcessingType = ;
            // documentClassificationDetail.PageCount = ;

            return documentClassificationDetail;
        }

        private DocumentManagement CreateDocumentManagement()
        {
            DocumentManagement documentManagement = new DocumentManagement();


            return documentManagement;
        }

        private DocumentSet CreateDocumentSet()
        {
            DocumentSet documentSet = new DocumentSet();

            // documentSet.Documents = CreateDocuments();
            // documentSet.ForeignObjects = CreateForeignObjects();
            // documentSet.SequenceNumber = ;

            return documentSet;
        }

        private DocumentSets CreateDocumentSets()
        {
            DocumentSets documentSets = new DocumentSets();

            // documentSets.DocumentSetList.Add(CreateDocumentSet());
            // documentSets.ForeignObjects = CreateForeignObjects();

            return documentSets;
        }

        private DownPayment CreateDownPayment()
        {
            DownPayment downPayment = new DownPayment();

            // downPayment.DownPaymentAmount = ;
            // downPayment.DownPaymentOptionType = ;
            // downPayment.DownPaymentOptionTypeOtherDescription = ;
            // downPayment.DownPaymentSourceType = ;
            // downPayment.DownPaymentSourceTypeOtherDescription = ;
            // downPayment.DownPaymentType = ;
            // downPayment.DownPaymentTypeOtherDescription = ;
            // downPayment.PropertySellerFundingIndicator = ;
            // downPayment.SequenceNumber = ;

            return downPayment;
        }

        private DownPayments CreateDownPayments(CPageData dataLoan)
        {
            DownPayments downPayments = new DownPayments();
            if (dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingDownPaymentSource1Amt_rep) != 0
                || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingDownPaymentSource2Amt_rep) != 0
                || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingDownPaymentSource3Amt_rep) != 0
                || dataLoan.m_convertLos.ToMoney(dataLoan.sCommunityLendingDownPaymentSource4Amt_rep) != 0)
            {
                downPayments.DownPaymentList.Add(CreateDownPayment(dataLoan.sLId, dataLoan.sGseDeliveryTargetT, dataLoan.sCommunityLendingDownPayment1T, dataLoan.sCommunityLendingDownPaymentSource1T, dataLoan.sCommunityLendingDownPaymentSource1Amt_rep));
                downPayments.DownPaymentList.Add(CreateDownPayment(dataLoan.sLId, dataLoan.sGseDeliveryTargetT, dataLoan.sCommunityLendingDownPayment2T, dataLoan.sCommunityLendingDownPaymentSource2T, dataLoan.sCommunityLendingDownPaymentSource2Amt_rep));
                downPayments.DownPaymentList.Add(CreateDownPayment(dataLoan.sLId, dataLoan.sGseDeliveryTargetT, dataLoan.sCommunityLendingDownPayment3T, dataLoan.sCommunityLendingDownPaymentSource3T, dataLoan.sCommunityLendingDownPaymentSource3Amt_rep));
                downPayments.DownPaymentList.Add(CreateDownPayment(dataLoan.sLId, dataLoan.sGseDeliveryTargetT, dataLoan.sCommunityLendingDownPayment4T, dataLoan.sCommunityLendingDownPaymentSource4T, dataLoan.sCommunityLendingDownPaymentSource4Amt_rep));
            }

            return downPayments;
        }

        private DownPayment CreateDownPayment(Guid loanId, E_sGseDeliveryTargetT deliveryTarget, E_sCommunityLendingDownPaymentT paymentT, E_sCommunityLendingDownPaymentSourceT sourceT, string amt)
        {
            decimal v = 0;
            decimal.TryParse(amt, out v);
            if (v == 0)
                return null;

            DownPayment downPayment = new DownPayment();
            downPayment.DownPaymentAmount = amt;

            if (paymentT != E_sCommunityLendingDownPaymentT.LeaveBlank)
            {
                if (paymentT == E_sCommunityLendingDownPaymentT.SecondaryFinancingClosedEnd)
                {
                    downPayment.DownPaymentTypeOtherDescription = "SecondaryFinancingClosedEnd";
                    downPayment.DownPaymentType = DownPaymentType.OtherTypeOfDownPayment;

                }
                else if (paymentT == E_sCommunityLendingDownPaymentT.SecondaryFinancingHELOC)
                {
                    downPayment.DownPaymentTypeOtherDescription = "SecondaryFinancingHELOC";
                    downPayment.DownPaymentType = DownPaymentType.OtherTypeOfDownPayment;

                }
                else if (paymentT == E_sCommunityLendingDownPaymentT.AggregatedRemainingTypes)
                {
                    downPayment.DownPaymentTypeOtherDescription = "AggregatedRemainingTypes";
                    downPayment.DownPaymentType = DownPaymentType.OtherTypeOfDownPayment;
                }
                else if (paymentT == E_sCommunityLendingDownPaymentT.Grant)
                {
                    downPayment.DownPaymentTypeOtherDescription = "Grant";
                    downPayment.DownPaymentType = DownPaymentType.OtherTypeOfDownPayment;
                }
                else
                {
                    downPayment.DownPaymentType = ToMismo(paymentT);
                }
            }
            if (sourceT != E_sCommunityLendingDownPaymentSourceT.LeaveBlank)
            {
                if (sourceT == E_sCommunityLendingDownPaymentSourceT.UsdaRuralHousing)
                {
                    downPayment.DownPaymentSourceTypeOtherDescription = "UsdaRuralHousing";
                    downPayment.DownPaymentSourceType = DownPaymentSourceType.Other;
                }
                else if (sourceT == E_sCommunityLendingDownPaymentSourceT.FHLBAffordableHousingProgram)
                {
                    downPayment.DownPaymentSourceTypeOtherDescription = "FHLBAffordableHousingProgram";
                    downPayment.DownPaymentSourceType = DownPaymentSourceType.Other;
                }
                else if (sourceT == E_sCommunityLendingDownPaymentSourceT.AggregatedRemainingTypes)
                {
                    downPayment.DownPaymentSourceTypeOtherDescription = "AggregatedRemainingSourceTypes";
                    downPayment.DownPaymentSourceType = DownPaymentSourceType.Other;
                }
                else if (deliveryTarget == E_sGseDeliveryTargetT.FreddieMac && Phase3Parts[loanId] == UlddPhase3Part.Part2 && sourceT == E_sCommunityLendingDownPaymentSourceT.Grant)
                {
                    downPayment.DownPaymentSourceTypeOtherDescription = "Grant";
                    downPayment.DownPaymentSourceType = DownPaymentSourceType.Other;
                }
                else
                {
                    downPayment.DownPaymentSourceType = ToMismo(sourceT);
                }
            }

            return downPayment;

        }

        private DownPaymentSourceType ToMismo(E_sCommunityLendingDownPaymentSourceT sourceT)
        {
            switch (sourceT)
            {
                case E_sCommunityLendingDownPaymentSourceT.Borrower:
                    return DownPaymentSourceType.Borrower;
                case E_sCommunityLendingDownPaymentSourceT.CommunityNonProfit:
                    return DownPaymentSourceType.CommunityNonProfit;
                case E_sCommunityLendingDownPaymentSourceT.FederalAgency:
                    return DownPaymentSourceType.FederalAgency;
                case E_sCommunityLendingDownPaymentSourceT.LocalAgency:
                    return DownPaymentSourceType.LocalAgency;
                case E_sCommunityLendingDownPaymentSourceT.Relative:
                    return DownPaymentSourceType.Relative;
                case E_sCommunityLendingDownPaymentSourceT.ReligiousNonProfit:
                    return DownPaymentSourceType.ReligiousNonProfit;
                case E_sCommunityLendingDownPaymentSourceT.StateAgency:
                    return DownPaymentSourceType.StateAgency;
                case E_sCommunityLendingDownPaymentSourceT.Employer:
                    return DownPaymentSourceType.Employer;
                case E_sCommunityLendingDownPaymentSourceT.OriginatingLender:
                    return DownPaymentSourceType.OriginatingLender;
                case E_sCommunityLendingDownPaymentSourceT.Grant:
                    return DownPaymentSourceType.Other;
                default:
                    throw new UnhandledEnumException(sourceT);
            }
        }

        private DownPaymentType ToMismo(E_sCommunityLendingDownPaymentT sCommunityLendingDownPaymentT)
        {
            switch (sCommunityLendingDownPaymentT)
            {
                case E_sCommunityLendingDownPaymentT.BridgeLoan:
                    return DownPaymentType.BridgeLoan;
                case E_sCommunityLendingDownPaymentT.CashOnHand:
                    return DownPaymentType.CashOnHand;
                case E_sCommunityLendingDownPaymentT.CheckingSavings:
                    return DownPaymentType.CheckingSavings;
                case E_sCommunityLendingDownPaymentT.GiftFunds:
                    return DownPaymentType.GiftFunds;
                case E_sCommunityLendingDownPaymentT.SecuredBorrowedFunds:
                    return DownPaymentType.SecuredBorrowedFunds;
                case E_sCommunityLendingDownPaymentT.SweatEquity:
                    return DownPaymentType.SweatEquity;
                case E_sCommunityLendingDownPaymentT.UnsecuredBorrowedFunds:
                    return DownPaymentType.UnsecuredBorrowedFunds;
                case E_sCommunityLendingDownPaymentT.LifeInsuranceCashValue:
                    return DownPaymentType.LifeInsuranceCashValue;
                case E_sCommunityLendingDownPaymentT.LotEquity:
                    return DownPaymentType.LotEquity;
                case E_sCommunityLendingDownPaymentT.RentWithOptionToPurchase:
                    return DownPaymentType.RentWithOptionToPurchase;
                case E_sCommunityLendingDownPaymentT.SaleOfChattel:
                    return DownPaymentType.SaleOfChattel;
                case E_sCommunityLendingDownPaymentT.StocksAndBonds:
                    return DownPaymentType.StocksAndBonds;
                case E_sCommunityLendingDownPaymentT.TradeEquity:
                    return DownPaymentType.TradeEquity;
                case E_sCommunityLendingDownPaymentT.TrustFunds:
                    return DownPaymentType.TrustFunds;
                case E_sCommunityLendingDownPaymentT.EquityOnSoldProperty:
                    return DownPaymentType.EquityOnSoldProperty;
                case E_sCommunityLendingDownPaymentT.EquityOnSubjetProperty:
                    return DownPaymentType.EquityOnSubjectProperty;
                case E_sCommunityLendingDownPaymentT.ForgivableSecuredLoan:
                    return DownPaymentType.ForgivableSecuredLoan;
                case E_sCommunityLendingDownPaymentT.RetirementFunds:
                    return DownPaymentType.RetirementFunds;
                default:
                    throw new UnhandledEnumException(sCommunityLendingDownPaymentT);
            }
        }

        private Draw CreateDraw()
        {
            Draw draw = new Draw();

            // draw.DrawActivities = CreateDrawActivities();
            // draw.DrawRule = CreateDrawRule();

            return draw;
        }

        private DrawActivities CreateDrawActivities()
        {
            DrawActivities drawActivities = new DrawActivities();

            // drawActivities.DrawActivityList.Add(CreateDrawActivity());

            return drawActivities;
        }

        private DrawActivity CreateDrawActivity()
        {
            DrawActivity drawActivity = new DrawActivity();

            // drawActivity.LoanDrawAmount = ;
            // drawActivity.LoanDrawDate = ;
            // drawActivity.LoanTotalDrawAmount = ;
            // drawActivity.SequenceNumber = ;

            return drawActivity;
        }

        private DrawRule CreateDrawRule()
        {
            DrawRule drawRule = new DrawRule();

            // drawRule.LoanDrawExpirationDate = ;
            // drawRule.LoanDrawExtensionTermMonthsCount = ;
            // drawRule.LoanDrawMaximumAmount = ;
            // drawRule.LoanDrawMaximumTermMonthsCount = ;
            // drawRule.LoanDrawMinimumAmount = ;
            // drawRule.LoanDrawMinimumInitialDrawAmount = ;
            // drawRule.LoanDrawPeriodMaximumDrawCount = ;
            // drawRule.LoanDrawPeriodMonthsCount = ;
            // drawRule.LoanDrawStartPeriodMonthsCount = ;
            // drawRule.LoanDrawTermMonthsCount = ;

            return drawRule;
        }

        private ElectronicSignature CreateElectronicSignature()
        {
            ElectronicSignature electronicSignature = new ElectronicSignature();

            // electronicSignature.ForeignObject = CreateForeignObject();

            return electronicSignature;
        }

        private EligibleLoanProducts CreateEligibleLoanProducts()
        {
            EligibleLoanProducts eligibleLoanProducts = new EligibleLoanProducts();

            // eligibleLoanProducts.EligibleLoanProductList.Add(CreateEligibleLoanProduct());

            return eligibleLoanProducts;
        }

        private Employer CreateEmployer(CAppData dataApp)
        {
            Employer employer = new Employer();

            // employer.Individual = CreateIndividual();
            // employer.LegalEntity = CreateLegalEntity();
            // employer.Address = CreateAddress();
            // employer.CreditComments = CreateCreditComments();
            employer.Employment = CreateEmployment(dataApp);
            // employer.EmploymentDocumentations = CreateEmploymentDocumentations();
            // employer.Verification = CreateVerification();
            // employer.SequenceNumber = ;

            return employer;
        }

        private Employers CreateEmployers(CAppData dataApp)
        {
            Employers employers = new Employers();
            employers.EmployerList.Add(CreateEmployer(dataApp));
            return employers;
        }

        private Employment CreateEmployment(CAppData dataApp)
        {
            Employment employment = new Employment();

            // employment.EmployedAbroadIndicator = ;
            // employment.EmploymentBorrowerHomeOfficeIndicator = ;
            // employment.EmploymentBorrowerSelfEmployedIndicator = ;
            // employment.EmploymentClassificationType = ;
            // employment.EmploymentEndDate = ;
            // employment.EmploymentMonthlyIncomeAmount = ;
            // employment.EmploymentMonthsOnJobCount = ;
            // employment.EmploymentPositionDescription = ;
            // employment.EmploymentReportedDate = ;
            // employment.EmploymentStartDate = ;
            // employment.EmploymentStatusType = ;
            // employment.EmploymentTimeInLineOfWorkMonthsCount = ;
            // employment.EmploymentTimeInLineOfWorkYearsCount = ;
            // employment.EmploymentYearsOnJobCount = ;
            employment.EmploymentBorrowerSelfEmployedIndicator = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? dataApp.aBIsSelfEmplmt : dataApp.aCIsSelfEmplmt;
            // employment.SpecialBorrowerEmployerRelationshipType = ;
            // employment.SpecialBorrowerEmployerRelationshipTypeOtherDescription = ;

            return employment;
        }

        private EmploymentDocumentation CreateEmploymentDocumentation()
        {
            EmploymentDocumentation employmentDocumentation = new EmploymentDocumentation();

            // employmentDocumentation.DocumentationStateType = ;
            // employmentDocumentation.DocumentationStateTypeOtherDescription = ;
            // employmentDocumentation.EmploymentDocumentationType = ;
            // employmentDocumentation.EmploymentDocumentationTypeOtherDescription = ;
            // employmentDocumentation.EmploymentVerificationRangeCount = ;
            // employmentDocumentation.EmploymentVerificationRangeType = ;
            // employmentDocumentation.EmploymentVerificationRangeTypeOtherDescription = ;
            // employmentDocumentation.SequenceNumber = ;

            return employmentDocumentation;
        }

        private EmploymentDocumentations CreateEmploymentDocumentations()
        {
            EmploymentDocumentations employmentDocumentations = new EmploymentDocumentations();

            // employmentDocumentations.EmploymentDocumentationList.Add(CreateEmploymentDocumentation());

            return employmentDocumentations;
        }

        private Escrow CreateEscrow(CPageData dataLoan)
        {
            Escrow escrow = new Escrow();



            escrow.EscrowDetail = CreateEscrowDetail(dataLoan);
            // escrow.EscrowDisclosures = CreateEscrowDisclosures();

            EscrowItems escrowItems = CreateEscrowItems(dataLoan);

            if (escrowItems == null)
            {
                return null;
            }
            escrow.EscrowItems = escrowItems;

            return escrow;
        }

        private EscrowDetail CreateEscrowDetail(CPageData dataLoan)
        {
            EscrowDetail escrowDetail = new EscrowDetail();
            escrowDetail.EscrowBalanceAmount = dataLoan.sGfeInitialImpoundDeposit_rep;

            if (((dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae 
                    && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                || (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac
                    && Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                && !dataLoan.sWillEscrowBeWaived)
            {
                escrowDetail.EscrowBalanceAmount = dataLoan.sServicingCurrentTotalFunds_Escrow_rep;
            }

            // escrowDetail.EscrowAccountInitialBalanceAmount = ;
            // escrowDetail.EscrowAccountMinimumBalanceAmount = ;
            // escrowDetail.EscrowAggregateAccountingAdjustmentAmount = ;
            // escrowDetail.EscrowCompletionFundsBalanceAmount = ;
            // escrowDetail.EscrowCushionNumberOfMonthsCount = ;
            // escrowDetail.EscrowHoldbackAmount = ;
            // escrowDetail.InitialEscrowProjectionStatementDate = ;
            // escrowDetail.InterestOnEscrowAccruedAmount = ;
            // escrowDetail.InterestOnEscrowAccruedThroughDate = ;
            // escrowDetail.InterestOnEscrowBackupWithholdingIndicator = ;
            // escrowDetail.InterestOnEscrowIndicator = ;
            // escrowDetail.InterestOnEscrowYearToDatePostedAmount = ;
            // escrowDetail.InterestOnRestrictedEscrowIndicator = ;
            // escrowDetail.LastEscrowAnalysisDate = ;
            // escrowDetail.LastInterestOnEscrowPostedDate = ;
            // escrowDetail.MIEscrowIncludedInAggregateIndicator = ;
            // escrowDetail.RestrictedEscrowBalanceAmount = ;

            return escrowDetail;
        }

        private EscrowDisclosure CreateEscrowDisclosure()
        {
            EscrowDisclosure escrowDisclosure = new EscrowDisclosure();

            // escrowDisclosure.EscrowAccountActivityCurrentBalanceAmount = ;
            // escrowDisclosure.EscrowAccountActivityDisbursementMonth = ;
            // escrowDisclosure.EscrowAccountActivityDisbursementYear = ;
            // escrowDisclosure.EscrowAccountActivityPaymentDescriptionType = ;
            // escrowDisclosure.EscrowAccountActivityPaymentFromEscrowAccountAmount = ;
            // escrowDisclosure.EscrowAccountActivityPaymentToEscrowAccountAmount = ;
            // escrowDisclosure.EscrowItemType = ;
            // escrowDisclosure.EscrowItemTypeOtherDescription = ;
            // escrowDisclosure.SequenceNumber = ;

            return escrowDisclosure;
        }

        private EscrowDisclosures CreateEscrowDisclosures()
        {
            EscrowDisclosures escrowDisclosures = new EscrowDisclosures();

            // escrowDisclosures.EscrowDisclosureList.Add(CreateEscrowDisclosure());

            return escrowDisclosures;
        }

        private EscrowItem CreateEscrowItem(E_TriState escrowedTri, LosConvert losConvert, EscrowItemType type, string amt)
        {
            if (escrowedTri != E_TriState.Yes || losConvert.ToMoney(amt) == 0)
            {
                return null;
            }
            EscrowItem escrowItem = new EscrowItem();

            escrowItem.EscrowItemDetail = CreateEscrowItemDetail(type, amt);
            // escrowItem.EscrowItemDisbursements = CreateEscrowItemDisbursements();
            // escrowItem.EscrowPaidTo = CreateEscrowPaidTo();
            // escrowItem.SequenceNumber = ;

            return escrowItem;
        }

        private EscrowItems CreateEscrowItems(CPageData dataLoan)
        {
            EscrowItems escrowItems = new EscrowItems();

            bool isFreddieMac = dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac;
            LosConvert losConvert = dataLoan.m_convertLos;
            EscrowItem escrowItem = null;

            escrowItem = CreateEscrowItem(dataLoan.sHazInsRsrvEscrowedTri, losConvert, EscrowItemType.HazardInsurance, dataLoan.sProHazIns_rep);
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            escrowItem = CreateEscrowItem(dataLoan.sMInsRsrvEscrowedTri, losConvert, EscrowItemType.MortgageInsurance, dataLoan.sProMIns_rep);
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }
            
            if (isFreddieMac)
            {
                EscrowItemType type = GetEscrowItemType(dataLoan.sTaxTableRealETxT, EscrowItemType.CountyPropertyTax);
                escrowItem = CreateEscrowItem(dataLoan.sRealETxRsrvEscrowedTri, losConvert, type, dataLoan.sProRealETx_rep);
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.sRealETxRsrvEscrowedTri, losConvert, EscrowItemType.CountyPropertyTax, dataLoan.sProRealETx_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            if (isFreddieMac)
            {
                EscrowItemType type = GetEscrowItemType(dataLoan.sTaxTableSchoolT, EscrowItemType.SchoolPropertyTax);
                escrowItem = CreateEscrowItem(dataLoan.sSchoolTxRsrvEscrowedTri, losConvert, type, dataLoan.sProSchoolTx_rep);
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.sSchoolTxRsrvEscrowedTri, losConvert, EscrowItemType.SchoolPropertyTax, dataLoan.sProSchoolTx_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            escrowItem = CreateEscrowItem(dataLoan.sFloodInsRsrvEscrowedTri, losConvert, EscrowItemType.FloodInsurance, dataLoan.sProFloodIns_rep);
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }


            if (isFreddieMac)
            {
                // Check if either of the tax dropdowns have 1008 selected.
                // If it is not one of those, then check the insurance pages.
                escrowItem = GetEscrowItemForLineNumWithTypeValidation(dataLoan, "1008");
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.s1006RsrvEscrowedTri, losConvert, EscrowItemType.OtherTax, dataLoan.s1006ProHExp_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            if (isFreddieMac)
            {
                // Check if either of the tax dropdowns have 1009 selected.
                // If it is not one of those, then check the insurance pages.
                escrowItem = GetEscrowItemForLineNumWithTypeValidation(dataLoan, "1009");
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.s1007RsrvEscrowedTri, losConvert, EscrowItemType.OtherTax, dataLoan.s1007ProHExp_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            if (isFreddieMac)
            {
                escrowItem = GetEscrowItemForLineNumWithTypeValidation(dataLoan, "1010");
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.sU3RsrvEscrowedTri, losConvert, EscrowItemType.OtherTax, dataLoan.sProU3Rsrv_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            if (isFreddieMac)
            {
                escrowItem = GetEscrowItemForLineNumWithTypeValidation(dataLoan, "1011");
            }
            else
            {
                escrowItem = CreateEscrowItem(dataLoan.sU4RsrvEscrowedTri, losConvert, EscrowItemType.OtherTax, dataLoan.sProU4Rsrv_rep);
            }
            if (escrowItem != null)
            {
                escrowItems.EscrowItemList.Add(escrowItem);
            }

            if (escrowItems.EscrowItemList.Count == 0)
            {
                return null;
            }
            return escrowItems;
        }

        private EscrowItem GetEscrowItemForLineNumWithTypeValidation(CPageData dataLoan, string lineNum)
        {
            // Check if any of the tax dropdowns have the line selected.
            // If not found, then check the insurance pages.
            var taxTableSettlementDescT = E_TableSettlementDescT.NoInfo;
            var insSettlementChargeT = E_sInsSettlementChargeT.None;
            var amt = 0m;
            var amt_rep = "";
            var escrowedTri = default(E_TriState);
            switch (lineNum.TrimWhitespaceAndBOM())
            {
                case "1008":
                    taxTableSettlementDescT = E_TableSettlementDescT.Use1008DescT;
                    insSettlementChargeT = E_sInsSettlementChargeT.Line1008;
                    amt = dataLoan.s1006ProHExp;
                    amt_rep = dataLoan.s1006ProHExp_rep;
                    escrowedTri = dataLoan.s1006RsrvEscrowedTri;
                    break;
                case "1009":
                    taxTableSettlementDescT = E_TableSettlementDescT.Use1009DescT;
                    insSettlementChargeT = E_sInsSettlementChargeT.Line1009;
                    amt = dataLoan.s1007ProHExp;
                    amt_rep = dataLoan.s1007ProHExp_rep;
                    escrowedTri = dataLoan.s1007RsrvEscrowedTri;
                    break;
                case "1010":
                    taxTableSettlementDescT = E_TableSettlementDescT.Use1010DescT;
                    insSettlementChargeT = E_sInsSettlementChargeT.Line1010;
                    amt = dataLoan.sProU3Rsrv;
                    amt_rep = dataLoan.sProU3Rsrv_rep;
                    escrowedTri = dataLoan.sU3RsrvEscrowedTri;
                    break;
                case "1011":
                    taxTableSettlementDescT = E_TableSettlementDescT.Use1011DescT;
                    insSettlementChargeT = E_sInsSettlementChargeT.Line1011;
                    amt = dataLoan.sProU4Rsrv;
                    amt_rep = dataLoan.sProU4Rsrv_rep;
                    escrowedTri = dataLoan.sU4RsrvEscrowedTri;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Trying to determine escrow item type for unsupported line number.");
            }

            EscrowItemType type = EscrowItemType.Undefined;
            bool noMatchingTaxOrInsItem = false;
            if (dataLoan.sTaxTable1008DescT == taxTableSettlementDescT)
            {
                type = GetEscrowItemType(dataLoan.sTaxTable1008T, EscrowItemType.Undefined);
            }
            else if (dataLoan.sTaxTable1009DescT == taxTableSettlementDescT)
            {
                type = GetEscrowItemType(dataLoan.sTaxTable1009T, EscrowItemType.Undefined);
            }
            else if (dataLoan.sTaxTableU3DescT == taxTableSettlementDescT)
            {
                type = GetEscrowItemType(dataLoan.sTaxTableU3T, EscrowItemType.Undefined);
            }
            else if (dataLoan.sTaxTableU4DescT == taxTableSettlementDescT)
            {
                type = GetEscrowItemType(dataLoan.sTaxTableU4T, EscrowItemType.Undefined);
            }
            else if (dataLoan.sWindInsSettlementChargeT == insSettlementChargeT)
            {
                type = EscrowItemType.StormInsurance;
            }
            else if (dataLoan.sCondoHO6InsSettlementChargeT == insSettlementChargeT)
            {
                type = EscrowItemType.HazardInsurance;
            }
            else
            {
                noMatchingTaxOrInsItem = true;
            }

            if (amt > 0 && type == EscrowItemType.Undefined)
            {
                string devMessage = "Failed to determine type of item. Should have failed audit. " + type + " " + noMatchingTaxOrInsItem + " line item " + lineNum;
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, devMessage);
            }

            return CreateEscrowItem(escrowedTri, dataLoan.m_convertLos, type, amt_rep);
        }

        private EscrowItemType GetEscrowItemType(E_TaxTableTaxT tax, EscrowItemType defaultType)
        {
            switch (tax)
            {
                case E_TaxTableTaxT.Borough:
                    return EscrowItemType.BoroughPropertyTax;
                case E_TaxTableTaxT.City:
                    return EscrowItemType.CityPropertyTax;
                case E_TaxTableTaxT.County:
                    return EscrowItemType.CountyPropertyTax;
                case E_TaxTableTaxT.FireDist:
                    return EscrowItemType.DistrictPropertyTax;
                case E_TaxTableTaxT.LeaveBlank:
                    return defaultType;
                case E_TaxTableTaxT.LocalImprovementDist:
                    return EscrowItemType.DistrictPropertyTax;
                case E_TaxTableTaxT.Miscellaneous:
                    return EscrowItemType.OtherTax;
                case E_TaxTableTaxT.MunicipalUtilDist:
                    return EscrowItemType.DistrictPropertyTax;
                case E_TaxTableTaxT.School:
                    return EscrowItemType.SchoolPropertyTax;
                case E_TaxTableTaxT.SpecialAssessmentDist:
                    return EscrowItemType.DistrictPropertyTax;
                case E_TaxTableTaxT.Town:
                    return EscrowItemType.CityPropertyTax;
                case E_TaxTableTaxT.Township:
                    return EscrowItemType.TownshipPropertyTax;
                case E_TaxTableTaxT.Utility:
                    return EscrowItemType.OtherTax;
                case E_TaxTableTaxT.Village:
                    return EscrowItemType.CityPropertyTax;
                case E_TaxTableTaxT.WasteFeeDist:
                    return EscrowItemType.DistrictPropertyTax;
                case E_TaxTableTaxT.Water_Irrigation:
                    return EscrowItemType.OtherTax;
                case E_TaxTableTaxT.Water_Sewer:
                    return EscrowItemType.OtherTax;
                default:
                    Tools.LogBug("Unhandled enum value for E_TaxTableTaxT");
                    return defaultType;
            }

        }

        private EscrowItemDetail CreateEscrowItemDetail(EscrowItemType type, string amt)
        {
            EscrowItemDetail escrowItemDetail = new EscrowItemDetail();

            // escrowItemDetail.EscrowAnnualPaymentAmount = ;
            // escrowItemDetail.EscrowCollectedNumberOfMonthsCount = ;
            // escrowItemDetail.EscrowInsurancePolicyIdentifier = ;
            // escrowItemDetail.EscrowItemLastDisbursementDate = ;
            // escrowItemDetail.EscrowItemNextDisbursementDueDate = ;
            // escrowItemDetail.EscrowItemNextProjectedDisbursementAmount = ;
            escrowItemDetail.EscrowItemType = type;
            // escrowItemDetail.EscrowItemTypeOtherDescription = ;
            escrowItemDetail.EscrowMonthlyPaymentAmount = amt;
            // escrowItemDetail.EscrowMonthlyPaymentRoundingType = ;
            // escrowItemDetail.EscrowPaidByType = ;
            // escrowItemDetail.EscrowPaymentFrequencyType = ;
            // escrowItemDetail.EscrowPaymentFrequencyTypeOtherDescription = ;
            // escrowItemDetail.EscrowPremiumAmount = ;
            // escrowItemDetail.EscrowPremiumDurationMonthsCount = ;
            // escrowItemDetail.EscrowPremiumPaidByType = ;
            // escrowItemDetail.EscrowPremiumPaymentType = ;
            // escrowItemDetail.EscrowSpecifiedHUD1LineNumberValue = ;
            // escrowItemDetail.EscrowTotalDisbursementAmount = ;

            return escrowItemDetail;
        }

        private EscrowItemDisbursement CreateEscrowItemDisbursement()
        {
            EscrowItemDisbursement escrowItemDisbursement = new EscrowItemDisbursement();

            // escrowItemDisbursement.EscrowItemDisbursementAmount = ;
            // escrowItemDisbursement.EscrowItemDisbursementTermMonthsCount = ;
            // escrowItemDisbursement.EscrowItemDisbursedDate = ;
            // escrowItemDisbursement.EscrowItemDisbursementDate = ;
            // escrowItemDisbursement.SequenceNumber = ;

            return escrowItemDisbursement;
        }

        private EscrowItemDisbursements CreateEscrowItemDisbursements()
        {
            EscrowItemDisbursements escrowItemDisbursements = new EscrowItemDisbursements();

            // escrowItemDisbursements.EscrowItemDisbursementList.Add(CreateEscrowItemDisbursement());

            return escrowItemDisbursements;
        }

        private EscrowPaidTo CreateEscrowPaidTo()
        {
            EscrowPaidTo escrowPaidTo = new EscrowPaidTo();

            // escrowPaidTo.Individual = CreateIndividual();
            // escrowPaidTo.LegalEntity = CreateLegalEntity();
            // escrowPaidTo.Address = CreateAddress();

            return escrowPaidTo;
        }

        private Execution CreateExecution()
        {
            Execution execution = new Execution();

            // execution.Address = CreateAddress();
            // execution.ExecutionDetail = CreateExecutionDetail();
            // execution.SequenceNumber = ;

            return execution;
        }

        private Executions CreateExecutions()
        {
            Executions executions = new Executions();

            // executions.ExecutionList.Add(CreateExecution());

            return executions;
        }

        private ExecutionDetail CreateExecutionDetail()
        {
            ExecutionDetail executionDetail = new ExecutionDetail();

            // executionDetail.ActualSignatureType = ;
            // executionDetail.ActualSignatureTypeOtherDescription = ;
            // executionDetail.ExecutionDatetime = ;
            // executionDetail.ExecutionJudicialDistrictName = ;

            return executionDetail;
        }

        private Expense CreateExpense()
        {
            Expense expense = new Expense();

            // expense.AlimonyOwedToName = ;
            // expense.ExpenseDescription = ;
            // expense.ExpenseMonthlyPaymentAmount = ;
            // expense.ExpenseRemainingTermMonthsCount = ;
            // expense.ExpenseType = ;
            // expense.ExpenseTypeOtherDescription = ;
            // expense.SequenceNumber = ;

            return expense;
        }

        private Expenses CreateExpenses()
        {
            Expenses expenses = new Expenses();

            // expenses.ExpenseList.Add(CreateExpense());

            return expenses;
        }

        private Extension CreateExtension()
        {
            Extension extension = new Extension();


            return extension;
        }

        private FieldReference CreateFieldReference()
        {
            FieldReference fieldReference = new FieldReference();

            // fieldReference.FieldHeightNumber = ;
            // fieldReference.FieldPlaceholderIdentifier = ;
            // fieldReference.FieldWidthNumber = ;
            // fieldReference.MeasurementUnitType = ;
            // fieldReference.OffsetFromLeftNumber = ;
            // fieldReference.OffsetFromTopNumber = ;
            // fieldReference.PageNumber = ;

            return fieldReference;
        }

        private FipsInformation CreateFipsInformation()
        {
            FipsInformation fipsInformation = new FipsInformation();

            // fipsInformation.FIPSCountryCode = ;
            // fipsInformation.FIPSCountyCode = ;
            // fipsInformation.FIPSCountySubdivisionCode = ;
            // fipsInformation.FIPSCountySubdivisionName = ;
            // fipsInformation.FIPSCountySubdivisionType = ;
            // fipsInformation.FIPSPlaceCode = ;
            // fipsInformation.FIPSPlaceName = ;
            // fipsInformation.FIPSStateAlphaCode = ;
            // fipsInformation.FIPSStateNumericCode = ;

            return fipsInformation;
        }

        private Flood CreateFlood()
        {
            Flood flood = new Flood();

            // flood.FloodRequest = CreateFloodRequest();
            // flood.FloodResponse = CreateFloodResponse();

            return flood;
        }

        private FloodDetermination CreateFloodDetermination(CPageData dataLoan)
        {
            FloodDetermination floodDetermination = new FloodDetermination();

            floodDetermination.FloodDeterminationDetail = CreateFloodDeterminationDetail(dataLoan);
            // floodDetermination.FloodRiskFactor = CreateFloodRiskFactor();

            return floodDetermination;
        }

        private FloodDeterminationDetail CreateFloodDeterminationDetail(CPageData dataLoan)
        {
            FloodDeterminationDetail floodDeterminationDetail = new FloodDeterminationDetail();

            // floodDeterminationDetail.FloodCertificationIdentifier = ;
            // floodDeterminationDetail.FloodDeterminationLifeOfLoanIndicator = ;
            // floodDeterminationDetail.FloodPartialIndicator = ;
            // floodDeterminationDetail.FloodProductCertifyDate = ;
            // floodDeterminationDetail.NFIPCommunityIdentifier = ;
            // floodDeterminationDetail.NFIPCommunityFIRMDate = ;
            // floodDeterminationDetail.NFIPCommunityName = ;
            // floodDeterminationDetail.NFIPCommunityParticipationStartDate = ;
            // floodDeterminationDetail.NFIPCommunityParticipationStatusType = ;
            // floodDeterminationDetail.NFIPCommunityParticipationStatusTypeOtherDescription = ;
            // floodDeterminationDetail.NFIPCountyName = ;
            // floodDeterminationDetail.NFIPFloodDataRevisionType = ;
            // floodDeterminationDetail.NFIPFloodZoneIdentifier = ;
            // floodDeterminationDetail.NFIPLetterOfMapDate = ;
            // floodDeterminationDetail.NFIPMapIdentifier = ;
            // floodDeterminationDetail.NFIPMapIndicator = ;
            // floodDeterminationDetail.NFIPMapPanelIdentifier = ;
            // floodDeterminationDetail.NFIPMapPanelSuffixIdentifier = ;
            // floodDeterminationDetail.NFIPMapPanelDate = ;
            // floodDeterminationDetail.NFIPStateCode = ;
            // floodDeterminationDetail.ProtectedAreaIndicator = ;
            // floodDeterminationDetail.ProtectedAreaDesignationDate = ;
            floodDeterminationDetail.SpecialFloodHazardAreaIndicator = (dataLoan.sFloodHazardBuilding || dataLoan.sFloodHazardMobileHome);

            return floodDeterminationDetail;
        }

        private FloodRequest CreateFloodRequest()
        {
            FloodRequest floodRequest = new FloodRequest();

            // floodRequest.FloodRequestDetail = CreateFloodRequestDetail();
            // floodRequest.FloodRequestDispute = CreateFloodRequestDispute();

            return floodRequest;
        }

        private FloodRequestDetail CreateFloodRequestDetail()
        {
            FloodRequestDetail floodRequestDetail = new FloodRequestDetail();

            // floodRequestDetail.BorrowerFloodAcknowledgementLetterIndicator = ;
            // floodRequestDetail.FEMAAdditionalLenderDescription = ;
            // floodRequestDetail.FloodCertificationIdentifier = ;
            // floodRequestDetail.FloodInsuranceAmount = ;
            // floodRequestDetail.FloodRequestActionType = ;
            // floodRequestDetail.FloodRequestCommentText = ;
            // floodRequestDetail.FloodRequestRushIndicator = ;
            // floodRequestDetail.FloodTransactionIdentifier = ;
            // floodRequestDetail.NewServicerAccountIdentifier = ;
            // floodRequestDetail.OrderingSystemName = ;
            // floodRequestDetail.OrderingSystemVersionIdentifier = ;
            // floodRequestDetail.OriginalFloodDeterminationLoanIdentifier = ;

            return floodRequestDetail;
        }

        private FloodRequestDispute CreateFloodRequestDispute()
        {
            FloodRequestDispute floodRequestDispute = new FloodRequestDispute();

            // floodRequestDispute.FloodRequestDisputeDescription = ;
            // floodRequestDispute.FloodRequestDisputeItemType = ;
            // floodRequestDispute.FloodRequestDisputeItemTypeOtherDescription = ;
            // floodRequestDispute.FloodRequestDisputeSupportingDocumentsDescription = ;

            return floodRequestDispute;
        }

        private FloodResponse CreateFloodResponse()
        {
            FloodResponse floodResponse = new FloodResponse();

            // floodResponse.FloodDetermination = CreateFloodDetermination();
            // floodResponse.FloodResponseDetail = CreateFloodResponseDetail();
            // floodResponse.Loans = CreateLoans();
            // floodResponse.Parties = CreateParties();
            // floodResponse.Property = CreateProperty();

            return floodResponse;
        }

        private FloodResponseDetail CreateFloodResponseDetail()
        {
            FloodResponseDetail floodResponseDetail = new FloodResponseDetail();

            // floodResponseDetail.FEMAAdditionalLenderDescription = ;
            // floodResponseDetail.DisputeResolutionIndicator = ;
            // floodResponseDetail.FloodTransactionIdentifier = ;

            return floodResponseDetail;
        }

        private FloodRiskFactor CreateFloodRiskFactor()
        {
            FloodRiskFactor floodRiskFactor = new FloodRiskFactor();

            // floodRiskFactor.BaseFloodElevationFeetNumber = ;
            // floodRiskFactor.FloodDepthFeetNumber = ;
            // floodRiskFactor.PropertyElevationFeetNumber = ;
            // floodRiskFactor.SpecialFloodHazardAreaDistanceFeetNumber = ;

            return floodRiskFactor;
        }

        private ForeignObject CreateForeignObject()
        {
            ForeignObject foreignObject = new ForeignObject();

            // foreignObject.Reference = CreateReference();
            // foreignObject.ObjectURI = ;
            // foreignObject.EmbeddedContentXML = ;
            // foreignObject.CharacterEncodingSetType = ;
            // foreignObject.CharacterEncodingSetTypeOtherDescription = ;
            // foreignObject.MIMETypeIdentifier = ;
            // foreignObject.ObjectCreatedDatetime = ;
            // foreignObject.ObjectDescription = ;
            // foreignObject.ObjectEncodingType = ;
            // foreignObject.ObjectEncodingTypeOtherDescription = ;
            // foreignObject.OriginalCreatorDigestValue = ;
            // foreignObject.ObjectName = ;
            // foreignObject.SequenceNumber = ;

            return foreignObject;
        }

        private ForeignObjects CreateForeignObjects()
        {
            ForeignObjects foreignObjects = new ForeignObjects();

            // foreignObjects.ForeignObjectList.Add(CreateForeignObject());

            return foreignObjects;
        }

        private FormSpecificContent CreateFormSpecificContent(CPageData dataLoan)
        {
            FormSpecificContent formSpecificContent = new FormSpecificContent();

            // formSpecificContent.Assignment = CreateAssignment();
            // formSpecificContent.Execution = CreateExecution();
            // formSpecificContent.Gfe = CreateGfe();
            // formSpecificContent.Hud1 = CreateHud1();
            // formSpecificContent.Note = CreateNote();
            // formSpecificContent.RecordingEndorsements = CreateRecordingEndorsements();
            // formSpecificContent.RightToCancel = CreateRightToCancel();
            // formSpecificContent.SecurityInstrument = CreateSecurityInstrument();
            formSpecificContent.Urla = CreateUrla(dataLoan);
            // formSpecificContent.DocumentClassification = CreateDocumentClassification();
            // formSpecificContent.SequenceNumber = ;

            return formSpecificContent;
        }

        private FormSpecificContents CreateFormSpecificContents(CPageData dataLoan)
        {
            FormSpecificContents formSpecificContents = new FormSpecificContents();

            formSpecificContents.FormSpecificContentList.Add(CreateFormSpecificContent(dataLoan));

            return formSpecificContents;
        }

        private Fraud CreateFraud()
        {
            Fraud fraud = new Fraud();


            return fraud;
        }

        private FulfillmentParty CreateFulfillmentParty()
        {
            FulfillmentParty fulfillmentParty = new FulfillmentParty();

            // fulfillmentParty.FulfillmentPartyRoleType = ;
            // fulfillmentParty.FulfillmentPartyRoleTypeOtherDescription = ;
            // fulfillmentParty.FulfillmentPartyServiceType = ;
            // fulfillmentParty.FulfillmentPartyServiceTypeOtherDescription = ;

            return fulfillmentParty;
        }

        private GeneralIdentifier CreateGeneralIdentifier()
        {
            GeneralIdentifier generalIdentifier = new GeneralIdentifier();

            // generalIdentifier.CongressionalDistrictIdentifier = ;
            // generalIdentifier.CoreBasedStatisticalAreaCode = ;
            // generalIdentifier.CoreBasedStatisticalAreaDivisionCode = ;
            // generalIdentifier.JudicialDistrictName = ;
            // generalIdentifier.JudicialDivisionName = ;
            // generalIdentifier.MapReferenceIdentifier = ;
            // generalIdentifier.MapReferenceSecondIdentifier = ;
            // generalIdentifier.MSAIdentifier = ;
            // generalIdentifier.MunicipalityName = ;
            // generalIdentifier.RecordingJurisdictionName = ;
            // generalIdentifier.RecordingJurisdictionType = ;
            // generalIdentifier.RecordingJurisdictionTypeOtherDescription = ;
            // generalIdentifier.SchoolDistrictName = ;

            return generalIdentifier;
        }

        private GeospatialInformation CreateGeospatialInformation()
        {
            GeospatialInformation geospatialInformation = new GeospatialInformation();

            // geospatialInformation.LatitudeIdentifier = ;
            // geospatialInformation.LongitudeIdentifier = ;

            return geospatialInformation;
        }

        private Gfe CreateGfe()
        {
            Gfe gfe = new Gfe();


            return gfe;
        }

        private GovernmentBorrower CreateGovernmentBorrower()
        {
            GovernmentBorrower governmentBorrower = new GovernmentBorrower();

            // governmentBorrower.CAIVRSIdentifier = ;
            // governmentBorrower.FHABorrowerCertificationLeadPaintIndicator = ;
            // governmentBorrower.FHABorrowerCertificationOriginalMortgageAmount = ;
            // governmentBorrower.FHABorrowerCertificationOwnOtherPropertyIndicator = ;
            // governmentBorrower.FHABorrowerCertificationOwn4OrMoreDwellingsIndicator = ;
            // governmentBorrower.FHABorrowerCertificationPropertySoldCityName = ;
            // governmentBorrower.FHABorrowerCertificationPropertySoldPostalCode = ;
            // governmentBorrower.FHABorrowerCertificationPropertySoldStateName = ;
            // governmentBorrower.FHABorrowerCertificationPropertySoldStreetAddressLineText = ;
            // governmentBorrower.FHABorrowerCertificationPropertyToBeSoldIndicator = ;
            // governmentBorrower.FHABorrowerCertificationRentalIndicator = ;
            // governmentBorrower.FHABorrowerCertificationSalesPriceAmount = ;
            // governmentBorrower.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType = ;
            // governmentBorrower.VABorrowerCertificationOccupancyType = ;
            // governmentBorrower.VABorrowerSurvivingSpouseIndicator = ;
            // governmentBorrower.VACoBorrowerNonTaxableIncomeAmount = ;
            // governmentBorrower.VACoBorrowerTaxableIncomeAmount = ;
            // governmentBorrower.VAFederalTaxAmount = ;
            // governmentBorrower.VALocalTaxAmount = ;
            // governmentBorrower.VAPrimaryBorrowerNonTaxableIncomeAmount = ;
            // governmentBorrower.VAPrimaryBorrowerTaxableIncomeAmount = ;
            // governmentBorrower.VASocialSecurityTaxAmount = ;
            // governmentBorrower.VAStateTaxAmount = ;
            // governmentBorrower.VeteranStatusIndicator = ;

            return governmentBorrower;
        }

        private GovernmentLoan CreateGovernmentLoan(CPageData dataLoan)
        {
            if (string.IsNullOrEmpty(dataLoan.sFHAHousingActSection))
            {
                return null;
            }
            GovernmentLoan governmentLoan = new GovernmentLoan();

            // governmentLoan.AgencyProgramDescription = ;
            // governmentLoan.BorrowerFinancedFHADiscountPointsAmount = ;
            // governmentLoan.BorrowerFundingFeePercent = ;
            // governmentLoan.BorrowerHomeInspectionChosenIndicator = ;
            // governmentLoan.BorrowerPaidFHA_VAClosingCostsAmount = ;
            // governmentLoan.BorrowerPaidFHA_VAClosingCostsPercent = ;
            // governmentLoan.DaysToFHA_MIEligibilityCount = ;
            // governmentLoan.FHA_MIPremiumRefundAmount = ;
            // governmentLoan.FHA_VALoanOriginatorIdentifier = ;
            // governmentLoan.FHAAlimonyLiabilityTreatmentType = ;
            // governmentLoan.FHAAssignmentDate = ;
            // governmentLoan.FHACoverageRenewalRatePercent = ;
            // governmentLoan.FHALoanLenderIdentifier = ;
            // governmentLoan.FHALoanSponsorIdentifier = ;
            // governmentLoan.FHAEnergyRelatedRepairsOrImprovementsAmount = ;
            // governmentLoan.FHAGeneralServicesAdministrationIdentifier = ;
            // governmentLoan.FHALimitedDenialParticipationIdentifier = ;
            // governmentLoan.FHANonOwnerOccupancyRiderRule248Indicator = ;
            // governmentLoan.FHARefinanceInterestOnExistingLienAmount = ;
            // governmentLoan.FHARefinanceOriginalExistingFHACaseIdentifier = ;
            // governmentLoan.FHARefinanceOriginalExistingUpFrontMIPAmount = ;
            // governmentLoan.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier = ;
            // governmentLoan.FHAUpfrontMIPremiumPercent = ;
            // governmentLoan.GovernmentMortgageCreditCertificateAmount = ;
            // governmentLoan.GovernmentRefinanceType = ;
            // governmentLoan.GovernmentRefinanceTypeOtherDescription = ;
            // governmentLoan.HUDAdequateAvailableAssetsIndicator = ;
            // governmentLoan.HUDAdequateEffectiveIncomeIndicator = ;
            // governmentLoan.HUDCreditCharacteristicsIndicator = ;
            // governmentLoan.HUDStableEffectiveIncomeIndicator = ;
            // governmentLoan.OtherPartyPaidFHA_VAClosingCostsAmount = ;
            // governmentLoan.OtherPartyPaidFHA_VAClosingCostsPercent = ;
            // governmentLoan.PropertyEnergyEfficientHomeIndicator = ;
            // governmentLoan.RuralHousingConditionalGuarantyExpirationDate = ;
            // governmentLoan.RuralHousingConditionalGuarantyInterestRatePercent = ;

            governmentLoan.SectionOfActType = ToSectionOfActType(dataLoan.sFHAHousingActSection.ToUpper());
            // governmentLoan.SectionOfActTypeOtherDescription = ;
            // governmentLoan.SellerPaidFHA_VAClosingCostsPercent = ;
            // governmentLoan.SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator = ;
            // governmentLoan.USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount = ;
            // governmentLoan.VABorrowerCoBorrowerMarriedIndicator = ;
            // governmentLoan.VAEntitlementAmount = ;
            // governmentLoan.VAEntitlementIdentifier = ;
            // governmentLoan.VAFundingFeeExemptionType = ;
            // governmentLoan.VAFundingFeeExemptionTypeOtherDescription = ;
            // governmentLoan.VAHouseholdSizeCount = ;
            // governmentLoan.VAMaintenanceExpenseMonthlyAmount = ;
            // governmentLoan.VAResidualIncomeAmount = ;
            // governmentLoan.VAUtilityExpenseMonthlyAmount = ;

            return governmentLoan;
        }

        private SectionOfActType ToSectionOfActType(string p)
        {
            string s = Regex.Replace(p, "[^a-zA-Z0-9]", "");

            if (s == "184")
            {
                return SectionOfActType._184;
            }
            else if (s == "201S")
            {
                return SectionOfActType._201S;
            }
            else if (s == "201SD")
            {
                return SectionOfActType._201SD;
            }
            else if (s == "201U")
            {
                return SectionOfActType._201U;
            }
            else if (s == "201UD")
            {
                return SectionOfActType._201UD;
            }
            else if (s == "203B")
            {
                return SectionOfActType._203B;
            }
            else if (s == "203B2")
            {
                return SectionOfActType._203B2;
            }
            else if (s == "203B241")
            {
                return SectionOfActType._203B241;
            }
            else if (s == "203B251")
            {
                return SectionOfActType._203B251;
            }
            else if (s == "203H")
            {
                return SectionOfActType._203H;
            }
            else if (s == "203I")
            {
                return SectionOfActType._203I;
            }
            else if (s == "203K")
            {
                return SectionOfActType._203K;
            }
            else if (s == "203K241")
            {
                return SectionOfActType._203K241;
            }
            else if (s == "203K251")
            {
                return SectionOfActType._203K251;
            }
            else if (s == "213")
            {
                return SectionOfActType._213;
            }
            else if (s == "220")
            {
                return SectionOfActType._220;
            }
            else if (s == "221")
            {
                return SectionOfActType._221;
            }
            else if (s == "221D2")
            {
                return SectionOfActType._221D2;
            }
            else if (s == "221D2251")
            {
                return SectionOfActType._221D2251;
            }
            else if (s == "222")
            {
                return SectionOfActType._222;
            }
            else if (s == "223E")
            {
                return SectionOfActType._223E;
            }
            else if (s == "233")
            {
                return SectionOfActType._233;
            }
            else if (s == "234C")
            {
                return SectionOfActType._234C;
            }
            else if (s == "234C251")
            {
                return SectionOfActType._234C251;
            }
            else if (s == "235")
            {
                return SectionOfActType._235;
            }
            else if (s == "237")
            {
                return SectionOfActType._237;
            }
            else if (s == "240")
            {
                return SectionOfActType._240;
            }
            else if (s == "245")
            {
                return SectionOfActType._245;
            }
            else if (s == "247")
            {
                return SectionOfActType._247;
            }
            else if (s == "248")
            {
                return SectionOfActType._248;
            }
            else if (s == "251")
            {
                return SectionOfActType._251;
            }
            else if (s == "255")
            {
                return SectionOfActType._255;
            }
            else if (s == "256")
            {
                return SectionOfActType._256;
            }
            else if (s == "257")
            {
                return SectionOfActType._257;
            }
            else if (s == "3703")
            {
                return SectionOfActType._3703;
            }
            else if (s == "3703D")
            {
                return SectionOfActType._3703D;
            }
            else if (s == "3703D2")
            {
                return SectionOfActType._3703D2;
            }
            else if (s == "3710")
            {
                return SectionOfActType._3710;
            }
            else if (s == "3711")
            {
                return SectionOfActType._3711;
            }
            else if (s == "502")
            {
                return SectionOfActType._502;
            }
            else
            {
                return SectionOfActType.Undefined;
            }

        }

        private GovernmentMonitoring CreateGovernmentMonitoring()
        {
            GovernmentMonitoring governmentMonitoring = new GovernmentMonitoring();

            // governmentMonitoring.GovernmentMonitoringDetail = CreateGovernmentMonitoringDetail();
            // governmentMonitoring.HmdaRaces = CreateHmdaRaces();

            return governmentMonitoring;
        }

        private GovernmentMonitoringDetail CreateGovernmentMonitoringDetail()
        {
            GovernmentMonitoringDetail governmentMonitoringDetail = new GovernmentMonitoringDetail();

            // governmentMonitoringDetail.GenderType = ;
            // governmentMonitoringDetail.HMDARefusalIndicator = ;
            // governmentMonitoringDetail.HMDAEthnicityType = ;
            // governmentMonitoringDetail.RaceNationalOriginRefusalIndicator = ;

            return governmentMonitoringDetail;
        }

        private HazardInsurance CreateHazardInsurance()
        {
            HazardInsurance hazardInsurance = new HazardInsurance();

            // hazardInsurance.FloodContractFeeAmount = ;
            // hazardInsurance.HazardInsuranceCoverageAmount = ;
            // hazardInsurance.HazardInsuranceCoverageType = ;
            // hazardInsurance.HazardInsuranceCoverageTypeOtherDescription = ;
            // hazardInsurance.HazardInsurancePremiumAmount = ;
            // hazardInsurance.HazardInsurancePremiumMonthsCount = ;
            // hazardInsurance.HazardInsuranceEffectiveDate = ;
            // hazardInsurance.HazardInsuranceEscrowedIndicator = ;
            // hazardInsurance.HazardInsuranceExpirationDate = ;
            // hazardInsurance.HazardInsuranceNextPremiumDueDate = ;
            // hazardInsurance.HazardInsuranceNonStandardPolicyType = ;
            // hazardInsurance.HazardInsuranceNonStandardPolicyTypeOtherDescription = ;
            // hazardInsurance.HazardInsurancePolicyCancellationDate = ;
            // hazardInsurance.HazardInsurancePolicyIdentifier = ;
            // hazardInsurance.HazardInsuranceReplacementValueIndicator = ;
            // hazardInsurance.SequenceNumber = ;

            return hazardInsurance;
        }

        private HazardInsurances CreateHazardInsurances()
        {
            HazardInsurances hazardInsurances = new HazardInsurances();

            // hazardInsurances.HazardInsuranceList.Add(CreateHazardInsurance());

            return hazardInsurances;
        }

        private Heloc CreateHeloc()
        {
            Heloc heloc = new Heloc();

            // heloc.HelocOccurrences = CreateHelocOccurrences();
            // heloc.HelocRule = CreateHelocRule();

            return heloc;
        }

        private HelocOccurrence CreateHelocOccurrence()
        {
            HelocOccurrence helocOccurrence = new HelocOccurrence();

            // helocOccurrence.CurrentHELOCMaximumBalanceAmount = ;
            // helocOccurrence.HELOCBalanceAmount = ;
            // helocOccurrence.HELOCDailyPeriodicInterestRatePercent = ;
            // helocOccurrence.HELOCTeaserTermEndDate = ;
            // helocOccurrence.SequenceNumber = ;

            return helocOccurrence;
        }

        private HelocOccurrences CreateHelocOccurrences()
        {
            HelocOccurrences helocOccurrences = new HelocOccurrences();

            // helocOccurrences.HelocOccurrenceList.Add(CreateHelocOccurrence());

            return helocOccurrences;
        }

        private HelocRule CreateHelocRule()
        {
            HelocRule helocRule = new HelocRule();

            // helocRule.HELOCAnnualFeeAmount = ;
            // helocRule.HELOCCreditCardAccountIdentifier = ;
            // helocRule.HELOCCreditCardIndicator = ;
            // helocRule.HELOCDailyPeriodicInterestRateCalculationType = ;
            // helocRule.HELOCInitialAdvanceAmount = ;
            // helocRule.HELOCMaximumAPRPercent = ;
            // helocRule.HELOCMaximumBalanceAmount = ;
            // helocRule.HELOCMinimumAdvanceAmount = ;
            // helocRule.HELOCMinimumPaymentAmount = ;
            // helocRule.HELOCMinimumPaymentPercent = ;
            // helocRule.HELOCRepayPeriodMonthsCount = ;
            // helocRule.HELOCReturnedCheckChargeAmount = ;
            // helocRule.HELOCStopPaymentChargeAmount = ;
            // helocRule.HELOCTeaserMarginRatePercent = ;
            // helocRule.HELOCTeaserRateType = ;
            // helocRule.HELOCTeaserTermMonthsCount = ;
            // helocRule.HELOCTerminationFeeAmount = ;
            // helocRule.HELOCTerminationPeriodMonthsCount = ;

            return helocRule;
        }

        private HmdaLoan CreateHmdaLoan(CPageData dataLoan)
        {
            HmdaLoan hmdaLoan = new HmdaLoan();

            hmdaLoan.HMDA_HOEPALoanStatusIndicator = dataLoan.sHmdaReportAsHoepaLoan;
            // hmdaLoan.HMDAPurposeOfLoanType = ;
            //// ir - iOPM 190789, Per Freddie Mac:
            //// The HMDA Rate Spread Percent (Subject Loan, At Closing) is required only when it is greater than or equal to 1.50% for 1st leins, and when it is greater than 3.5% for second liens.
            if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac ||
                (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sHmdaAprRateSpreadForULDDExport >= 1.50M) ||
                (dataLoan.sLienPosT == E_sLienPosT.Second && dataLoan.sHmdaAprRateSpreadForULDDExport >= 3.50M))
            {
                // Need to format to dd.dd, rounding up.
                hmdaLoan.HMDARateSpreadPercent = Math.Round(dataLoan.sHmdaAprRateSpreadForULDDExport, 2).ToString();
            }

            return hmdaLoan;
        }
        private HmdaRace CreateHmdaRace()
        {
            HmdaRace hmdaRace = new HmdaRace();

            // hmdaRace.HMDARaceType = ;
            // hmdaRace.SequenceNumber = ;

            return hmdaRace;
        }

        private HmdaRaces CreateHmdaRaces()
        {
            HmdaRaces hmdaRaces = new HmdaRaces();

            // hmdaRaces.HmdaRaceList.Add(CreateHmdaRace());

            return hmdaRaces;
        }

        private HomeownersAssociation CreateHomeownersAssociation()
        {
            HomeownersAssociation homeownersAssociation = new HomeownersAssociation();

            // homeownersAssociation.Address = CreateAddress();
            // homeownersAssociation.LegalEntity = CreateLegalEntity();
            // homeownersAssociation.ManagementAgent = CreateManagementAgent();

            return homeownersAssociation;
        }

        private Hud1 CreateHud1()
        {
            Hud1 hud1 = new Hud1();

            // hud1.Hud1Detail = CreateHud1Detail();
            // hud1.Hud1LineItems = CreateHud1LineItems();
            // hud1.Hud1Summary = CreateHud1Summary();

            return hud1;
        }

        private Hud1Detail CreateHud1Detail()
        {
            Hud1Detail hud1Detail = new Hud1Detail();

            // hud1Detail.HUD1CashToOrFromBorrowerIndicator = ;
            // hud1Detail.HUD1CashToOrFromSellerIndicator = ;
            // hud1Detail.HUD1ConventionalInsuredIndicator = ;
            // hud1Detail.HUD1FileNumberIdentifier = ;
            // hud1Detail.HUD1SettlementDate = ;

            return hud1Detail;
        }

        private Hud1LineItem CreateHud1LineItem()
        {
            Hud1LineItem hud1LineItem = new Hud1LineItem();

            // hud1LineItem.HUD1LineItemAmount = ;
            // hud1LineItem.HUD1LineItemDescription = ;
            // hud1LineItem.HUD1LineItemFromDate = ;
            // hud1LineItem.HUD1LineItemPaidByType = ;
            // hud1LineItem.HUD1LineItemToDate = ;
            // hud1LineItem.HUD1SpecifiedHUDLineNumberValue = ;
            // hud1LineItem.SequenceNumber = ;

            return hud1LineItem;
        }

        private Hud1LineItems CreateHud1LineItems()
        {
            Hud1LineItems hud1LineItems = new Hud1LineItems();

            // hud1LineItems.Hud1LineItemList.Add(CreateHud1LineItem());

            return hud1LineItems;
        }

        private Hud1Summary CreateHud1Summary()
        {
            Hud1Summary hud1Summary = new Hud1Summary();

            // hud1Summary.CashFromBorrowerAtClosingAmount = ;
            // hud1Summary.CashFromSellerAtClosingAmount = ;
            // hud1Summary.CashToBorrowerAtClosingAmount = ;
            // hud1Summary.CashToSellerAtClosingAmount = ;

            return hud1Summary;
        }

        private IdentificationVerification CreateIdentificationVerification()
        {
            IdentificationVerification identificationVerification = new IdentificationVerification();

            // identificationVerification.BirthInformation = CreateBirthInformation();
            // identificationVerification.IdentificationVerificationDetail = CreateIdentificationVerificationDetail();
            // identificationVerification.IdentityVerificationDocuments = CreateIdentityVerificationDocuments();
            // identificationVerification.SpouseInformation = CreateSpouseInformation();

            return identificationVerification;
        }

        private IdentificationVerificationDetail CreateIdentificationVerificationDetail()
        {
            IdentificationVerificationDetail identificationVerificationDetail = new IdentificationVerificationDetail();

            // identificationVerificationDetail.CountryOfCitizenshipName = ;
            // identificationVerificationDetail.SSNAssociationsCount = ;

            return identificationVerificationDetail;
        }

        private IdentityVerificationDocument CreateIdentityVerificationDocument()
        {
            IdentityVerificationDocument identityVerificationDocument = new IdentityVerificationDocument();

            // identityVerificationDocument.IdentityDocumentIdentifier = ;
            // identityVerificationDocument.IdentityDocumentIssuedByCountryName = ;
            // identityVerificationDocument.IdentityDocumentIssuedByName = ;
            // identityVerificationDocument.IdentityDocumentIssuedByStateProvinceName = ;
            // identityVerificationDocument.IdentityDocumentIssuedByType = ;
            // identityVerificationDocument.IdentityDocumentExpirationDate = ;
            // identityVerificationDocument.IdentityDocumentIssuedDate = ;
            // identityVerificationDocument.IdentityVerificationSourceType = ;
            // identityVerificationDocument.IdentityVerificationSourceTypeOtherDescription = ;
            // identityVerificationDocument.SequenceNumber = ;

            return identityVerificationDocument;
        }

        private IdentityVerificationDocuments CreateIdentityVerificationDocuments()
        {
            IdentityVerificationDocuments identityVerificationDocuments = new IdentityVerificationDocuments();

            // identityVerificationDocuments.IdentityVerificationDocumentList.Add(CreateIdentityVerificationDocument());

            return identityVerificationDocuments;
        }

        private IncomeDocumentation CreateIncomeDocumentation()
        {
            IncomeDocumentation incomeDocumentation = new IncomeDocumentation();

            // incomeDocumentation.DocumentationStateType = ;
            // incomeDocumentation.DocumentationStateTypeOtherDescription = ;
            // incomeDocumentation.IncomeDocumentationType = ;
            // incomeDocumentation.IncomeDocumentationTypeOtherDescription = ;
            // incomeDocumentation.IncomeVerificationRangeCount = ;
            // incomeDocumentation.IncomeVerificationRangeType = ;
            // incomeDocumentation.IncomeVerificationRangeTypeOtherDescription = ;
            // incomeDocumentation.SequenceNumber = ;

            return incomeDocumentation;
        }

        private IncomeDocumentations CreateIncomeDocumentations()
        {
            IncomeDocumentations incomeDocumentations = new IncomeDocumentations();

            // incomeDocumentations.IncomeDocumentationList.Add(CreateIncomeDocumentation());

            return incomeDocumentations;
        }

        private IndexRule CreateIndexRule(CPageData dataLoan)
        {
            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.GinnieMae)
            {
                throw new NotSupportedException(LendersOffice.Common.ErrorMessages.ULDD.GSeTargetGinnieMaeExportNotAvailable);
            }
            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.Other_None)
            {
                throw new NotSupportedException(LendersOffice.Common.ErrorMessages.ULDD.GseTargetOtherExportNotAvailable);
            }
            IndexRule indexRule = new IndexRule();

            // indexRule.IndexType = ;
            // indexRule.IndexTypeOtherDescription = ;
            // indexRule.InterestAdjustmentIndexLeadDaysCount = ;
            indexRule.InterestAndPaymentAdjustmentIndexLeadDaysCount = dataLoan.sArmIndexLeadDays_rep;
            // indexRule.MinimumIndexMovementRatePercent = ;
            // indexRule.PaymentsBetweenIndexChangesCount = ;
            // indexRule.PaymentsBetweenIndexValuesCount = ;
            // indexRule.PaymentsToFirstIndexValueCount = ;
            // indexRule.SequenceNumber = ;
            // indexRule.IndexAveragingIndicator = ;
            // indexRule.IndexAveragingType = ;
            // indexRule.IndexAveragingValueCount = ;
            // indexRule.IndexCalculationMethodType = ;
            // indexRule.IndexDesignationType = ;
            // indexRule.IndexLeadMonthsCount = ;
            // indexRule.IndexLookbackType = ;
            // indexRule.IndexLookbackTypeOtherDescription = ;
            // indexRule.IndexRoundingPercent = ;
            // indexRule.IndexRoundingTimingType = ;
            // indexRule.IndexRoundingType = ;

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                #region Fannie Mae
                indexRule.IndexSourceType = IndexSourceType.Other;

                if (dataLoan.sArmIndexT != E_sArmIndexT.LeaveBlank)
                {
                    if (dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && dataLoan.sRAdjCapMon == 6)
                        indexRule.IndexSourceTypeOtherDescription = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && dataLoan.sRAdjCapMon == 36)
                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && dataLoan.sRAdjCapMon == 36)
                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesMonthlyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && dataLoan.sRAdjCapMon == 60)
                        indexRule.IndexSourceTypeOtherDescription = "5YearTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && dataLoan.sRAdjCapMon == 60)
                        indexRule.IndexSourceTypeOtherDescription = "5YearTreasuryConstantMaturitiesMonthlyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesMonthlyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.NationalMonthlyMedianCostOfFunds)
                        indexRule.IndexSourceTypeOtherDescription = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && dataLoan.sRAdjCapMon == 6)
                        indexRule.IndexSourceTypeOtherDescription = "6MonthWallStreetJournalLIBORRateDaily";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && dataLoan.sRAdjCapMon == 3)
                        indexRule.IndexSourceTypeOtherDescription = "3MonthWallStreetJournalLIBORrateDaily";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && dataLoan.sRAdjCapMon == 1)
                        indexRule.IndexSourceTypeOtherDescription = "1MonthWallStreetJournalLIBORRateDaily";
                    else if (dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearWallStreetJournalLIBORRateDaily";
                }
                else
                {
                    if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.SixMonthTreasury && dataLoan.sRAdjCapMon == 6)
                        indexRule.IndexSourceTypeOtherDescription = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && dataLoan.sRAdjCapMon == 36)
                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && dataLoan.sRAdjCapMon == 36)
                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesMonthlyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesWeeklyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesMonthlyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds)
                        indexRule.IndexSourceTypeOtherDescription = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && dataLoan.sRAdjCapMon == 6)
                        indexRule.IndexSourceTypeOtherDescription = "6MonthWallStreetJournalLIBORRateDaily";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && dataLoan.sRAdjCapMon == 3)
                        indexRule.IndexSourceTypeOtherDescription = "3MonthWallStreetJournalLIBORrateDaily";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && dataLoan.sRAdjCapMon == 1)
                        indexRule.IndexSourceTypeOtherDescription = "1MonthWallStreetJournalLIBORRateDaily";
                    else if (dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && dataLoan.sRAdjCapMon == 12)
                        indexRule.IndexSourceTypeOtherDescription = "1YearWallStreetJournalLIBORRateDaily";
                }
                #endregion
            }
            else if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
            {
                #region FreddieMac
                bool useArmIndexT = false;
                switch (dataLoan.sFreddieArmIndexT)
                {
                    case E_sFreddieArmIndexT.LeaveBlank:
                        useArmIndexT = true;
                        break;
                    case E_sFreddieArmIndexT.OneYearTreasury:
                        indexRule.IndexSourceType = IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                        break;
                    case E_sFreddieArmIndexT.ThreeYearTreasury:
                        indexRule.IndexSourceType = IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                        break;
                    case E_sFreddieArmIndexT.SixMonthTreasury:
                    case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
                    case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
                        break;
                    case E_sFreddieArmIndexT.LIBOR:
                        switch (dataLoan.sRAdjCapMon)
                        {
                            case 6:
                                indexRule.IndexSourceType = IndexSourceType.SixMonthLIBOR_WSJDaily;
                                break;
                            case 12:
                                indexRule.IndexSourceType = IndexSourceType.LIBOROneYearWSJDaily;
                                break;
                            default:
                                break;
                        }
                        break;
                    case E_sFreddieArmIndexT.Other:
                        break;
                }
                if (false == useArmIndexT)
                {
                    return indexRule;
                }
                switch (dataLoan.sArmIndexT)
                {
                    case E_sArmIndexT.LeaveBlank:
                    case E_sArmIndexT.MonthlyAvgCMT:
                        break;
                    case E_sArmIndexT.WeeklyAvgCMT:
                        switch (dataLoan.sRAdjCapMon)
                        {
                            case 36:
                                indexRule.IndexSourceType = IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                                break;
                            case 60:
                                indexRule.IndexSourceType = IndexSourceType.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
                                break;
                            case 12:
                                indexRule.IndexSourceType = IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                                break;
                            default:
                                break;
                        }
                        break;
                    case E_sArmIndexT.WeeklyAvgTAAI:
                    case E_sArmIndexT.WeeklyAvgTAABD:
                    case E_sArmIndexT.WeeklyAvgSMTI:
                    case E_sArmIndexT.DailyCDRate:
                    case E_sArmIndexT.WeeklyAvgCDRate:
                    case E_sArmIndexT.WeeklyAvgPrimeRate:
                    case E_sArmIndexT.TBillDailyValue:
                    case E_sArmIndexT.EleventhDistrictCOF:
                    case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
                        break;
                    case E_sArmIndexT.WallStreetJournalLIBOR:
                        switch (dataLoan.sRAdjCapMon)
                        {
                            case 6:
                                indexRule.IndexSourceType = IndexSourceType.SixMonthLIBOR_WSJDaily;
                                break;
                            case 12:
                                indexRule.IndexSourceType = IndexSourceType.LIBOROneYearWSJDaily;
                                break;
                            default:
                                break;
                        }
                        break;
                    case E_sArmIndexT.FannieMaeLIBOR:
                    default:
                        break;
                }

                #endregion
            }

            return indexRule;
        }

        //private void SetIndexSourceTypeForFreddieMac(IndexRule indexRule, E_sArmIndexT e_sArmIndexT, E_sFreddieArmIndexT e_sFreddieArmIndexT, int sRAdjCapMon)
        //{
        //    bool useArmIndexT = false;
        //    switch (e_sFreddieArmIndexT)
        //    {
        //        case E_sFreddieArmIndexT.LeaveBlank:
        //            useArmIndexT = true;
        //            break;
        //        case E_sFreddieArmIndexT.OneYearTreasury:
        //            indexRule.IndexSourceType = IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
        //            break;
        //        case E_sFreddieArmIndexT.ThreeYearTreasury:
        //            indexRule.IndexSourceType = IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
        //            break;
        //        case E_sFreddieArmIndexT.SixMonthTreasury:
        //        case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
        //        case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
        //            break;
        //        case E_sFreddieArmIndexT.LIBOR:
        //            switch (sRAdjCapMon)
        //            {
        //                case 6:
        //                    indexRule.IndexSourceType = IndexSourceType.SixMonthLIBOR_WSJDaily;
        //                    break;
        //                case 12:
        //                    indexRule.IndexSourceType = IndexSourceType.LIBOROneYearWSJDaily;
        //                    break;
        //                default:
        //                    break;
        //            }
        //            break;
        //        case E_sFreddieArmIndexT.Other:
        //            break;
        //    }
        //    if (false == useArmIndexT)
        //    {
        //        return;
        //    }
        //    switch (e_sArmIndexT)
        //    {
        //        case E_sArmIndexT.LeaveBlank:
        //        case E_sArmIndexT.MonthlyAvgCMT:
        //            break;
        //        case E_sArmIndexT.WeeklyAvgCMT:
        //            switch(sRAdjCapMon)
        //            {
        //                case 36:
        //                    indexRule.IndexSourceType = IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
        //                    break;
        //                case 60:
        //                    indexRule.IndexSourceType = IndexSourceType.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
        //                    break;
        //                case 12:
        //                    indexRule.IndexSourceType = IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
        //                    break;
        //                default:
        //                    break;
        //            }
        //            break;
        //        case E_sArmIndexT.WeeklyAvgTAAI:
        //        case E_sArmIndexT.WeeklyAvgTAABD:
        //        case E_sArmIndexT.WeeklyAvgSMTI:
        //        case E_sArmIndexT.DailyCDRate:
        //        case E_sArmIndexT.WeeklyAvgCDRate:
        //        case E_sArmIndexT.WeeklyAvgPrimeRate:
        //        case E_sArmIndexT.TBillDailyValue:
        //        case E_sArmIndexT.EleventhDistrictCOF:
        //        case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
        //            break;
        //        case E_sArmIndexT.WallStreetJournalLIBOR:
        //            switch (sRAdjCapMon)
        //            {
        //                case 6:
        //                    indexRule.IndexSourceType = IndexSourceType.SixMonthLIBOR_WSJDaily;
        //                    break;
        //                case 12:
        //                    indexRule.IndexSourceType = IndexSourceType.LIBOROneYearWSJDaily;
        //                    break;
        //                default:
        //                    break;
        //            }
        //            break;
        //        case E_sArmIndexT.FannieMaeLIBOR:
        //        default:
        //            break;
        //    }
        //}

        //private string GetIndexSourceDescription(E_sArmIndexT sArmIndexT, E_sFreddieArmIndexT sFreddieArmIndexT, int sRAdjCapMon)
        //{
        //    string description = string.Empty;
        //    if (sArmIndexT != E_sArmIndexT.LeaveBlank)
        //    {
        //        if (sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && sRAdjCapMon == 6)
        //            description = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && sRAdjCapMon == 36)
        //            description = "3YearTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && sRAdjCapMon == 36)
        //            description = "3YearTreasuryConstantMaturitiesMonthlyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && sRAdjCapMon == 60)
        //            description = "5YearTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && sRAdjCapMon == 60)
        //            description = "5YearTreasuryConstantMaturitiesMonthlyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && sRAdjCapMon == 12)
        //            description = "1YearTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && sRAdjCapMon == 12)
        //            description = "1YearTreasuryConstantMaturitiesMonthlyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.NationalMonthlyMedianCostOfFunds)
        //            description = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
        //        else if (sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && sRAdjCapMon == 6)
        //            description = "6MonthWallStreetJournalLIBORRateDaily";
        //        else if (sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && sRAdjCapMon == 3)
        //            description = "3MonthWallStreetJournalLIBORrateDaily";
        //        else if (sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && sRAdjCapMon == 1)
        //            description = "1MonthWallStreetJournalLIBORRateDaily";
        //        else if (sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && sRAdjCapMon == 12)
        //            description = "1YearWallStreetJournalLIBORRateDaily";
        //    }
        //    else
        //    {
        //        if (sFreddieArmIndexT == E_sFreddieArmIndexT.SixMonthTreasury && sRAdjCapMon == 6)
        //            description = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && sRAdjCapMon == 36)
        //            description = "3YearTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && sRAdjCapMon == 36)
        //            description = "3YearTreasuryConstantMaturitiesMonthlyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && sRAdjCapMon == 12)
        //            description = "1YearTreasuryConstantMaturitiesWeeklyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && sRAdjCapMon == 12)
        //            description = "1YearTreasuryConstantMaturitiesMonthlyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds)
        //            description = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && sRAdjCapMon == 6)
        //            description = "6MonthWallStreetJournalLIBORRateDaily";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && sRAdjCapMon == 3)
        //            description = "3MonthWallStreetJournalLIBORrateDaily";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && sRAdjCapMon == 1)
        //            description = "1MonthWallStreetJournalLIBORRateDaily";
        //        else if (sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && sRAdjCapMon == 12)
        //            description = "1YearWallStreetJournalLIBORRateDaily";
        //    }


        //    return description;

        //}

        private IndexRules CreateIndexRules(CPageData dataLoan)
        {
            IndexRules indexRules = new IndexRules();

            indexRules.IndexRuleList.Add(CreateIndexRule(dataLoan));

            return indexRules;
        }

        private Individual CreateIndividual()
        {
            Individual individual = new Individual();

            // individual.Aliases = CreateAliases();
            // individual.ContactPoints = CreateContactPoints();
            // individual.IdentificationVerification = CreateIdentificationVerification();
            // individual.Name = CreateName();

            return individual;
        }

        private IneligibleLoanProducts CreateIneligibleLoanProducts()
        {
            IneligibleLoanProducts ineligibleLoanProducts = new IneligibleLoanProducts();

            // ineligibleLoanProducts.IneligibleLoanProductList.Add(CreateIneligibleLoanProduct());

            return ineligibleLoanProducts;
        }

        private InteractiveField CreateInteractiveField()
        {
            InteractiveField interactiveField = new InteractiveField();

            // interactiveField.InteractiveFieldDetail = CreateInteractiveFieldDetail();
            // interactiveField.InteractiveFieldReference = CreateInteractiveFieldReference();

            return interactiveField;
        }

        private InteractiveFieldDetail CreateInteractiveFieldDetail()
        {
            InteractiveFieldDetail interactiveFieldDetail = new InteractiveFieldDetail();

            // interactiveFieldDetail.MutuallyExclusiveGroupName = ;

            return interactiveFieldDetail;
        }

        private InterestCalculation CreateInterestCalculation()
        {
            InterestCalculation interestCalculation = new InterestCalculation();

            // interestCalculation.InterestCalculationOccurrences = CreateInterestCalculationOccurrences();
            interestCalculation.InterestCalculationRules = CreateInterestCalculationRules();

            return interestCalculation;
        }

        private InterestCalculationOccurrence CreateInterestCalculationOccurrence()
        {
            InterestCalculationOccurrence interestCalculationOccurrence = new InterestCalculationOccurrence();

            // interestCalculationOccurrence.CurrentAccruedInterestAmount = ;
            // interestCalculationOccurrence.DailySimpleInterestDueAmount = ;
            // interestCalculationOccurrence.InterestDueFromClosingAmount = ;
            // interestCalculationOccurrence.SequenceNumber = ;

            return interestCalculationOccurrence;
        }

        private InterestCalculationOccurrences CreateInterestCalculationOccurrences()
        {
            InterestCalculationOccurrences interestCalculationOccurrences = new InterestCalculationOccurrences();

            // interestCalculationOccurrences.InterestCalculationOccurrenceList.Add(CreateInterestCalculationOccurrence());

            return interestCalculationOccurrences;
        }

        private InterestCalculationRule CreateInterestCalculationRule()
        {
            InterestCalculationRule interestCalculationRule = new InterestCalculationRule();

            // interestCalculationRule.InitialUnearnedInterestAmount = ;
            // interestCalculationRule.InterestAccrualType = ;
            // interestCalculationRule.InterestCalculationBasisDaysInPeriodType = ;
            // interestCalculationRule.InterestCalculationBasisDaysInPeriodTypeOtherDescription = ;
            // interestCalculationRule.InterestCalculationBasisDaysInYearCountType = ;
            // interestCalculationRule.InterestCalculationBasisType = ;
            // interestCalculationRule.InterestCalculationBasisTypeOtherDescription = ;
            // interestCalculationRule.InterestCalculationEffectiveDate = ;
            // interestCalculationRule.InterestCalculationEffectiveMonthsCount = ;
            // interestCalculationRule.InterestCalculationExpirationDate = ;
            // interestCalculationRule.InterestCalculationPeriodAdjustmentIndicator = ;
            interestCalculationRule.InterestCalculationPeriodType = InterestCalculationPeriodType.Month;
            // interestCalculationRule.InterestCalculationPurposeType = ;
            // interestCalculationRule.InterestCalculationPurposeTypeOtherDescription = ;
            interestCalculationRule.InterestCalculationType = InterestCalculationType.Simple;
            // interestCalculationRule.InterestCalculationTypeOtherDescription = ;
            // interestCalculationRule.InterestInAdvanceIndicator = ;

            // 11/1/2013 dd - FNMA does not like the date here.
            // if the date is provide then interest calculation period type must be day and simple.
            // interestCalculationRule.LoanInterestAccrualStartDate = dataLoan.sFundD_rep; 

            // interestCalculationRule.PaymentsBetweenRateChangesCount = ;
            // interestCalculationRule.SequenceNumber = ;

            return interestCalculationRule;
        }

        private InterestCalculationRules CreateInterestCalculationRules()
        {
            InterestCalculationRules interestCalculationRules = new InterestCalculationRules();

            interestCalculationRules.InterestCalculationRuleList.Add(CreateInterestCalculationRule());

            return interestCalculationRules;
        }

        private InterestOnly CreateInterestOnly(CPageData dataLoan)
        {
            if (dataLoan.sIsIOnly == false)
            {
                return null;
            }
            InterestOnly interestOnly = new InterestOnly();
            if (dataLoan.sIOnlyEndD.IsValid)
            {
                interestOnly.InterestOnlyEndDate = dataLoan.m_convertLos.ToDateTimeString(dataLoan.sIOnlyEndD.DateTimeForComputation);
            }
            // interestOnly.InterestOnlyMonthlyPaymentAmount = ;
            // interestOnly.InterestOnlyTermPaymentsCount = ;
            // interestOnly.InterestOnlyTermMonthsCount = ;

            return interestOnly;
        }

        private InterestRateAdjustment CreateInterestRateAdjustment(CPageData dataLoan)
        {
            InterestRateAdjustment interestRateAdjustment = new InterestRateAdjustment();

            interestRateAdjustment.IndexRules = CreateIndexRules(dataLoan);
            interestRateAdjustment.InterestRateLifetimeAdjustmentRule = CreateInterestRateLifetimeAdjustmentRule(dataLoan);
            interestRateAdjustment.InterestRatePerChangeAdjustmentRules = CreateInterestRatePerChangeAdjustmentRules(dataLoan);
            // interestRateAdjustment.InterestRatePeriodicAdjustmentRules = CreateInterestRatePeriodicAdjustmentRules();

            return interestRateAdjustment;
        }

        private InterestRateLifetimeAdjustmentRule CreateInterestRateLifetimeAdjustmentRule(CPageData dataLoan)
        {
            InterestRateLifetimeAdjustmentRule interestRateLifetimeAdjustmentRule = new InterestRateLifetimeAdjustmentRule();

            // interestRateLifetimeAdjustmentRule.CarryoverRateIndicator = ;
            interestRateLifetimeAdjustmentRule.CeilingRatePercent = dataLoan.m_convertLos.ToRateString(dataLoan.sRAdjLifeCapR + dataLoan.sNoteIR);
            // interestRateLifetimeAdjustmentRule.DeferredInterestExclusionIndicator = ;
            interestRateLifetimeAdjustmentRule.FirstRateChangePaymentEffectiveDate = dataLoan.m_convertLos.ToDateTimeString(dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(dataLoan.sRAdj1stCapMon));
            interestRateLifetimeAdjustmentRule.FloorRatePercent = (dataLoan.sRAdjFloorR == 0 || string.IsNullOrEmpty(dataLoan.sRAdjFloorR_rep)) ? dataLoan.sRAdjMarginR_rep : dataLoan.sRAdjFloorR_rep;
            // interestRateLifetimeAdjustmentRule.InterestRateAdjustmentCalculationMethodType = ;
            // interestRateLifetimeAdjustmentRule.InterestRateAdjustmentCalculationMethodTypeOtherDescription = ;
            // interestRateLifetimeAdjustmentRule.InterestRateAverageValueCount = ;
            // interestRateLifetimeAdjustmentRule.InterestRateLifetimeAdjustmentCeilingType = ;
            // interestRateLifetimeAdjustmentRule.InterestRateLifetimeAdjustmentCeilingTypeOtherDescription = ;
            // interestRateLifetimeAdjustmentRule.InterestRateLifetimeAdjustmentFloorType = ;
            // interestRateLifetimeAdjustmentRule.InterestRateLifetimeAdjustmentFloorTypeOtherDescription = ;
            interestRateLifetimeAdjustmentRule.InterestRateRoundingPercent = dataLoan.sRAdjRoundToR_rep;
            interestRateLifetimeAdjustmentRule.InterestRateRoundingType = ToMismo(dataLoan.sRAdjRoundT);
            // interestRateLifetimeAdjustmentRule.InterestRateTruncatedDigitsCount = ;
            // interestRateLifetimeAdjustmentRule.LoanMarginCalculationMethodType = ;
            interestRateLifetimeAdjustmentRule.MarginRatePercent = dataLoan.sRAdjMarginR_rep;
            // interestRateLifetimeAdjustmentRule.MaximumDecreaseRatePercent = ;
            // interestRateLifetimeAdjustmentRule.MaximumIncreaseRatePercent = ;
            // interestRateLifetimeAdjustmentRule.PaymentsBetweenInterestRateValuesCount = ;
            // interestRateLifetimeAdjustmentRule.PaymentsBetweenRateChangesCount = ;
            // interestRateLifetimeAdjustmentRule.TimelyPaymentRateReductionPercent = ;

            return interestRateLifetimeAdjustmentRule;
        }

        private InterestRateRoundingType ToMismo(E_sRAdjRoundT sRAdjRoundT)
        {
            switch (sRAdjRoundT)
            {
                case E_sRAdjRoundT.Normal:
                    return InterestRateRoundingType.Nearest;
                case E_sRAdjRoundT.Up:
                    return InterestRateRoundingType.Up;
                case E_sRAdjRoundT.Down:
                    return InterestRateRoundingType.Down;
                default:
                    throw new UnhandledEnumException(sRAdjRoundT);
            }
            throw new NotImplementedException();
        }

        private InterestRatePeriodicAdjustmentRule CreateInterestRatePeriodicAdjustmentRule()
        {
            InterestRatePeriodicAdjustmentRule interestRatePeriodicAdjustmentRule = new InterestRatePeriodicAdjustmentRule();

            // interestRatePeriodicAdjustmentRule.PeriodicBaseRatePercent = ;
            // interestRatePeriodicAdjustmentRule.PeriodicEffectiveDate = ;
            // interestRatePeriodicAdjustmentRule.PeriodicMaximumDecreaseRatePercent = ;
            // interestRatePeriodicAdjustmentRule.PeriodicMaximumIncreaseRatePercent = ;
            // interestRatePeriodicAdjustmentRule.PeriodicNextBaseRateSelectionDate = ;
            // interestRatePeriodicAdjustmentRule.PeriodicPaymentsBetweenBaseRatesCount = ;
            // interestRatePeriodicAdjustmentRule.SequenceNumber = ;

            return interestRatePeriodicAdjustmentRule;
        }

        private InterestRatePeriodicAdjustmentRules CreateInterestRatePeriodicAdjustmentRules()
        {
            InterestRatePeriodicAdjustmentRules interestRatePeriodicAdjustmentRules = new InterestRatePeriodicAdjustmentRules();

            // interestRatePeriodicAdjustmentRules.InterestRatePeriodicAdjustmentRuleList.Add(CreateInterestRatePeriodicAdjustmentRule());

            return interestRatePeriodicAdjustmentRules;
        }

        private InterestRatePerChangeAdjustmentRule CreateInterestRatePerChangeAdjustmentRule()
        {
            InterestRatePerChangeAdjustmentRule interestRatePerChangeAdjustmentRule = new InterestRatePerChangeAdjustmentRule();

            // interestRatePerChangeAdjustmentRule.AdjustmentRuleType = ;
            // interestRatePerChangeAdjustmentRule.NoInterestRateCapsFeatureDescription = ;
            // interestRatePerChangeAdjustmentRule.PaymentsBetweenRateChangeNoticesCount = ;
            // interestRatePerChangeAdjustmentRule.PaymentsBetweenRateChangesCount = ;
            // interestRatePerChangeAdjustmentRule.PaymentsToFirstInterestRateValueCount = ;
            // interestRatePerChangeAdjustmentRule.PerChangeCeilingRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeFactorOfIndexPercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeFloorRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeInterestRateRoundingTimingType = ;
            // interestRatePerChangeAdjustmentRule.PerChangeMaximumDecreaseRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeMaximumIncreaseRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeMinimumDecreaseRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeMinimumIncreaseRatePercent = ;
            // interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentCalculationMethodDescription = ;
            // interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentEffectiveDate = ;
            // interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentEffectiveMonthsCount = ;
            // interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = ;
            // interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentPaymentsCount = ;
            // interestRatePerChangeAdjustmentRule.SequenceNumber = ;

            return interestRatePerChangeAdjustmentRule;
        }

        private InterestRatePerChangeAdjustmentRules CreateInterestRatePerChangeAdjustmentRules(CPageData dataLoan)
        {
            InterestRatePerChangeAdjustmentRules interestRatePerChangeAdjustmentRules = new InterestRatePerChangeAdjustmentRules();

            InterestRatePerChangeAdjustmentRule firstChange;            
            firstChange = CreateInterestRatePerChangeAdjustmentRule(AdjustmentRuleType.First, dataLoan.sRAdj1stCapR_rep, dataLoan.sRAdjCapMon_rep, dataLoan.m_convertLos.ToDateTimeString(dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(dataLoan.sRAdj1stCapMon - 1)));            
            interestRatePerChangeAdjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(firstChange);

            InterestRatePerChangeAdjustmentRule subsequenceChange = CreateInterestRatePerChangeAdjustmentRule(AdjustmentRuleType.Subsequent, dataLoan.sRAdjCapR_rep, dataLoan.sRAdjCapMon_rep, dataLoan.m_convertLos.ToDateTimeString(dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(dataLoan.sRAdj1stCapMon - 1 + dataLoan.sRAdjCapMon)));
            interestRatePerChangeAdjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(subsequenceChange);

            return interestRatePerChangeAdjustmentRules;
        }
        private InterestRatePerChangeAdjustmentRule CreateInterestRatePerChangeAdjustmentRule(AdjustmentRuleType adjustRuleType, string capR, string capMon, string effectiveDate)
        {
            InterestRatePerChangeAdjustmentRule interestRatePerChangeAdjustmentRule = new InterestRatePerChangeAdjustmentRule();
            interestRatePerChangeAdjustmentRule.AdjustmentRuleType = adjustRuleType;
            interestRatePerChangeAdjustmentRule.PerChangeMaximumDecreaseRatePercent = capR;
            interestRatePerChangeAdjustmentRule.PerChangeMaximumIncreaseRatePercent = capR;
            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentEffectiveDate = effectiveDate;
            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = capMon;

            return interestRatePerChangeAdjustmentRule;
        }
        private InterimInterest CreateInterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();

            // interimInterest.InterimDisclosureType = ;
            // interimInterest.InterimDisclosureTypeOtherDescription = ;
            // interimInterest.InterimInterestPaidFromDate = ;
            // interimInterest.InterimInterestPaidNumberOfDaysCount = ;
            // interimInterest.InterimInterestPaidThroughDate = ;
            // interimInterest.InterimInterestPerDiemCalculationMethodType = ;
            // interimInterest.InterimInterestPerDiemPaymentOptionType = ;
            // interimInterest.InterimInterestSinglePerDiemAmount = ;
            // interimInterest.InterimInterestTotalPerDiemAmount = ;
            // interimInterest.SequenceNumber = ;

            return interimInterest;
        }

        private InterimInterests CreateInterimInterests()
        {
            InterimInterests interimInterests = new InterimInterests();

            // interimInterests.InterimInterestList.Add(CreateInterimInterest());

            return interimInterests;
        }

        private InterviewerDetail CreateInterviewerDetail()
        {
            InterviewerDetail interviewerDetail = new InterviewerDetail();

            // interviewerDetail.ApplicationTakenMethodType = ;
            // interviewerDetail.InterviewerApplicationSignedDate = ;

            return interviewerDetail;
        }

        private InterviewerEmployer CreateInterviewerEmployer()
        {
            InterviewerEmployer interviewerEmployer = new InterviewerEmployer();

            // interviewerEmployer.Address = CreateAddress();
            // interviewerEmployer.Name = CreateName();

            return interviewerEmployer;
        }

        private InvestorFeature CreateInvestorFeature(string id)
        {
            InvestorFeature investorFeature = new InvestorFeature();

            // investorFeature.InvestorFeatureCategoryName = ;
            // investorFeature.InvestorFeatureDescription = ;
            investorFeature.InvestorFeatureIdentifier = id;
            // investorFeature.InvestorFeatureName = ;
            // investorFeature.SequenceNumber = ;

            return investorFeature;
        }

        private InvestorFeatures CreateInvestorFeatures(CPageData dataLoan)
        {
            InvestorFeatures investorFeatures = new InvestorFeatures();

            string[] investorFeatureList = {
                                               dataLoan.sGseDeliveryFeature1Id,
                                               dataLoan.sGseDeliveryFeature2Id,
                                               dataLoan.sGseDeliveryFeature3Id,
                                               dataLoan.sGseDeliveryFeature4Id,
                                               dataLoan.sGseDeliveryFeature5Id,
                                               dataLoan.sGseDeliveryFeature6Id,
                                               dataLoan.sGseDeliveryFeature7Id,
                                               dataLoan.sGseDeliveryFeature8Id,
                                               dataLoan.sGseDeliveryFeature9Id,
                                               dataLoan.sGseDeliveryFeature10Id,
                                           };

            foreach (var id in investorFeatureList)
            {
                if (string.IsNullOrEmpty(id) == false)
                {
                    investorFeatures.InvestorFeatureList.Add(CreateInvestorFeature(id));
                }
            }
            // investorFeatures.InvestorFeatureList.Add(CreateInvestorFeature());

            return investorFeatures;
        }

        private InvestorLoanInformation CreateInvestorLoanInformation(CPageData dataLoan)
        {
            if (dataLoan.sSpInvestorCurrentLoanT == E_sSpInvestorCurrentLoanT.Unknown && string.IsNullOrEmpty(dataLoan.sSpGseLoanNumCurrentLoan))
            {
                return null;
            }
            InvestorLoanInformation investorLoanInformation = new InvestorLoanInformation();

            // investorLoanInformation.BaseGuarantyFeePercent = ;
            // investorLoanInformation.CollateralProgramIdentifier = ;
            // investorLoanInformation.ConcurrentServicingTransferIndicator = ;
            // investorLoanInformation.ContractVarianceCode = ;
            // investorLoanInformation.ContractVarianceCodeIssuerType = ;
            // investorLoanInformation.ContractVarianceCodeIssuerTypeOtherDescription = ;
            // investorLoanInformation.ExecutionType = ;
            // investorLoanInformation.ExecutionTypeOtherDescription = ;
            // investorLoanInformation.ExceptionInterestAdjustmentDayMethodType = ;
            // investorLoanInformation.FundingInterestAdjustmentDayMethodType = ;
            // investorLoanInformation.GuaranteeFeeAddOnIndicator = ;
            // investorLoanInformation.GuarantyFeeAfterAlternatePaymentMethodPercent = ;
            // investorLoanInformation.GuarantyFeePercent = ;
            // investorLoanInformation.InterestVarianceToleranceMaximumAmount = ;
            // investorLoanInformation.InvestorCollateralProgramIdentifier = ;
            // investorLoanInformation.InvestorCommitmentTakeoutPricePercent = ;
            // investorLoanInformation.InvestorDealIdentifier = ;
            // investorLoanInformation.InvestorGroupingIdentifier = ;
            // investorLoanInformation.InvestorGuarantyFeeAfterBuyupBuydownPercent = ;
            // investorLoanInformation.InvestorHeldCollateralPercent = ;
            // investorLoanInformation.InvestorInstitutionIdentifier = ;
            // investorLoanInformation.InvestorLoanCommitmentExpirationDate = ;
            // investorLoanInformation.InvestorLoanEarlyPayoffPenaltyWaiverIndicator = ;
            // investorLoanInformation.InvestorLoanEffectiveDate = ;
            // investorLoanInformation.InvestorLoanPartialPrepaymentPenaltyWaiverIndicator = ;
            // investorLoanInformation.InvestorMasterCommitmentIdentifier = ;
            // investorLoanInformation.InvestorOwnershipPercent = ;
            // investorLoanInformation.InvestorOwnershipType = ;
            // investorLoanInformation.InvestorProductPlanIdentifier = ;
            // investorLoanInformation.InvestorRequiredMarginPercent = ;
            // investorLoanInformation.InvestorRemittanceDay = ;
            // investorLoanInformation.InvestorRemittanceType = ;
            // investorLoanInformation.InvestorRemittanceTypeOtherDescription = ;
            // investorLoanInformation.InvestorReportingMethodType = ;
            // investorLoanInformation.InvestorServicingFeeAmount = ;
            // investorLoanInformation.InvestorServicingFeeRatePercent = ;
            // investorLoanInformation.LastReportedToInvestorPrincipalBalanceAmount = ;
            // investorLoanInformation.LateDeliveryGraceDaysCount = ;
            // investorLoanInformation.LenderTargetFundingDate = ;
            // investorLoanInformation.LoanAcquisitionScheduledUPBAmount = ;
            // investorLoanInformation.LoanBuyupBuydownBasisPointNumber = ;
            // investorLoanInformation.LoanBuyupBuydownType = ;
            // investorLoanInformation.LoanBuyupBuydownTypeOtherDescription = ;
            // investorLoanInformation.LoanDefaultLossPartyType = ;
            // investorLoanInformation.LoanDefaultedReportingFrequencyType = ;
            // investorLoanInformation.LoanDelinquencyAdvanceDaysCount = ;
            // investorLoanInformation.LoanFundingAdvancedAmount = ;
            // investorLoanInformation.LoanPriorOwnerName = ;
            // investorLoanInformation.LoanPurchaseDiscountAmount = ;
            // investorLoanInformation.LoanPurchasePremiumAmount = ;
            // investorLoanInformation.LoanServicingFeeBasisPointNumber = ;
            // investorLoanInformation.LoanServicingIndicator = ;
            // investorLoanInformation.LoanServicingRightsPurchaseOnlyIndicator = ;
            // investorLoanInformation.LoanYieldSpreadPremiumAmount = ;
            // investorLoanInformation.PassThroughCalculationMethodType = ;
            // investorLoanInformation.PassThroughCeilingRatePercent = ;
            // investorLoanInformation.PassThroughRatePercent = ;
            // investorLoanInformation.PrincipalVarianceToleranceMaximumAmount = ;
            investorLoanInformation.RelatedInvestorLoanIdentifier = dataLoan.sSpGseLoanNumCurrentLoan;
            investorLoanInformation.RelatedLoanInvestorType = ToMismo(dataLoan.sSpInvestorCurrentLoanT);
            // investorLoanInformation.RelatedLoanInvestorTypeOtherDescription = ;
            // investorLoanInformation.REOMarketingPartyType = ;
            // investorLoanInformation.REOMarketingPartyTypeOtherDescription = ;
            // investorLoanInformation.ServicingFeeMinimumRatePercent = ;
            // investorLoanInformation.ThirdPartyLoanAcquisitionPriceAmount = ;
            // investorLoanInformation.UPBVarianceToleranceMaximumAmount = ;
            // investorLoanInformation.WarehouseAdvanceAmount = ;
            // investorLoanInformation.WarehouseCommitmentSubcategoryIdentifier = ;

            return investorLoanInformation;
        }

        private RelatedLoanInvestorType ToMismo(E_sSpInvestorCurrentLoanT sSpInvestorCurrentLoanT)
        {
            switch (sSpInvestorCurrentLoanT)
            {
                case E_sSpInvestorCurrentLoanT.Unknown:
                    return RelatedLoanInvestorType.Undefined;
                case E_sSpInvestorCurrentLoanT.FannieMae:
                    return RelatedLoanInvestorType.FNM;
                case E_sSpInvestorCurrentLoanT.FreddieMac:
                    return RelatedLoanInvestorType.FRE;
                default:
                    throw new UnhandledEnumException(sSpInvestorCurrentLoanT);
            }
            throw new NotImplementedException();
        }

        private InvestorReportingAdditionalCharge CreateInvestorReportingAdditionalCharge()
        {
            InvestorReportingAdditionalCharge investorReportingAdditionalCharge = new InvestorReportingAdditionalCharge();

            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeAmount = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeAssessedToPartyType = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeDate = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeReversalIndicator = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeType = ;
            // investorReportingAdditionalCharge.InvestorReportingAdditionalChargeTypeOtherDescription = ;
            // investorReportingAdditionalCharge.SequenceNumber = ;

            return investorReportingAdditionalCharge;
        }

        private InvestorReportingAdditionalCharges CreateInvestorReportingAdditionalCharges()
        {
            InvestorReportingAdditionalCharges investorReportingAdditionalCharges = new InvestorReportingAdditionalCharges();

            // investorReportingAdditionalCharges.InvestorReportingAdditionalChargeList.Add(CreateInvestorReportingAdditionalCharge());

            return investorReportingAdditionalCharges;
        }

        private Landlord CreateLandlord()
        {
            Landlord landlord = new Landlord();

            // landlord.Address = CreateAddress();
            // landlord.ContactPoints = CreateContactPoints();
            // landlord.CreditComments = CreateCreditComments();
            // landlord.LandlordDetail = CreateLandlordDetail();
            // landlord.Name = CreateName();
            // landlord.Verification = CreateVerification();

            return landlord;
        }

        private LandlordDetail CreateLandlordDetail()
        {
            LandlordDetail landlordDetail = new LandlordDetail();

            // landlordDetail.MonthlyRentAmount = ;
            // landlordDetail.MonthlyRentCurrentRatingType = ;

            return landlordDetail;
        }

        private LateCharge CreateLateCharge()
        {
            LateCharge lateCharge = new LateCharge();

            // lateCharge.LateChargeOccurrences = CreateLateChargeOccurrences();
            // lateCharge.LateChargeRule = CreateLateChargeRule();

            return lateCharge;
        }

        private LateChargeOccurrence CreateLateChargeOccurrence()
        {
            LateChargeOccurrence lateChargeOccurrence = new LateChargeOccurrence();

            // lateChargeOccurrence.LateChargeLoanPaymentAmount = ;
            // lateChargeOccurrence.UncollectedLateChargeBalanceAmount = ;
            // lateChargeOccurrence.SequenceNumber = ;

            return lateChargeOccurrence;
        }

        private LateChargeOccurrences CreateLateChargeOccurrences()
        {
            LateChargeOccurrences lateChargeOccurrences = new LateChargeOccurrences();

            // lateChargeOccurrences.LateChargeOccurrenceList.Add(CreateLateChargeOccurrence());

            return lateChargeOccurrences;
        }

        private LateChargeRule CreateLateChargeRule()
        {
            LateChargeRule lateChargeRule = new LateChargeRule();

            // lateChargeRule.LateChargeAmount = ;
            // lateChargeRule.LateChargeGracePeriodDaysCount = ;
            // lateChargeRule.LateChargeMaximumAmount = ;
            // lateChargeRule.LateChargeMinimumAmount = ;
            // lateChargeRule.LateChargeRatePercent = ;
            // lateChargeRule.LateChargeType = ;

            return lateChargeRule;
        }

        private LegalAndVesting CreateLegalAndVesting()
        {
            LegalAndVesting legalAndVesting = new LegalAndVesting();

            // legalAndVesting.LegalAndVestingCommentDescription = ;
            // legalAndVesting.LegalAndVestingPlantDate = ;
            // legalAndVesting.LegalValidationIndicator = ;
            // legalAndVesting.LienDescription = ;
            // legalAndVesting.TitleHeldByName = ;
            // legalAndVesting.VestingValidationIndicator = ;
            // legalAndVesting.SequenceNumber = ;

            return legalAndVesting;
        }

        private LegalAndVestings CreateLegalAndVestings()
        {
            LegalAndVestings legalAndVestings = new LegalAndVestings();

            // legalAndVestings.LegalAndVestingList.Add(CreateLegalAndVesting());

            return legalAndVestings;
        }

        private LegalDescription CreateLegalDescription()
        {
            LegalDescription legalDescription = new LegalDescription();

            // legalDescription.ParsedLegalDescription = CreateParsedLegalDescription();
            // legalDescription.UnparsedLegalDescriptions = CreateUnparsedLegalDescriptions();
            // legalDescription.ParcelIdentifications = CreateParcelIdentifications();
            // legalDescription.SequenceNumber = ;

            return legalDescription;
        }

        private LegalDescriptions CreateLegalDescriptions()
        {
            LegalDescriptions legalDescriptions = new LegalDescriptions();

            // legalDescriptions.LegalDescriptionList.Add(CreateLegalDescription());

            return legalDescriptions;
        }

        private LegalEntity CreateLegalEntity()
        {
            LegalEntity legalEntity = new LegalEntity();

            // legalEntity.Aliases = CreateAliases();
            // legalEntity.Contacts = CreateContacts();
            // legalEntity.LegalEntityDetail = CreateLegalEntityDetail();

            return legalEntity;
        }

        private LegalEntityDetail CreateLegalEntityDetail()
        {
            LegalEntityDetail legalEntityDetail = new LegalEntityDetail();

            // legalEntityDetail.EntityHomePageURI = ;
            // legalEntityDetail.FullName = ;
            // legalEntityDetail.LegalEntityLicensingTypeDescription = ;
            // legalEntityDetail.LegalEntityOrganizedUnderTheLawsOfJurisdictionName = ;
            // legalEntityDetail.LegalEntitySuccessorClauseTextDescription = ;
            // legalEntityDetail.LegalEntityType = ;
            // legalEntityDetail.LegalEntityTypeOtherDescription = ;
            // legalEntityDetail.MERSOrganizationIdentifier = ;
            // legalEntityDetail.RegisteredDomainNameURI = ;

            return legalEntityDetail;
        }

        private Lender CreateLender()
        {
            Lender lender = new Lender();

            // lender.LenderDocumentsOrderedByName = ;
            // lender.LenderFunderName = ;
            // lender.RegulatoryAgencyLenderIdentifier = ;

            return lender;
        }

        private Liabilities CreateLiabilities()
        {
            Liabilities liabilities = new Liabilities();

            // liabilities.LiabilityList.Add(CreateLiability());

            return liabilities;
        }

        private Liability CreateLiability()
        {
            Liability liability = new Liability();

            // liability.LiabilityDetail = CreateLiabilityDetail();
            // liability.LiabilityHolder = CreateLiabilityHolder();
            // liability.Payoff = CreatePayoff();
            // liability.SequenceNumber = ;

            return liability;
        }

        private LiabilityDetail CreateLiabilityDetail()
        {
            LiabilityDetail liabilityDetail = new LiabilityDetail();

            // liabilityDetail.LiabilityAccountIdentifier = ;
            // liabilityDetail.LiabilityDescription = ;
            // liabilityDetail.LiabilityExclusionIndicator = ;
            // liabilityDetail.LiabilityMonthlyPaymentAmount = ;
            // liabilityDetail.LiabilityPayoffStatusIndicator = ;
            // liabilityDetail.LiabilityPayoffWithCurrentAssetsIndicator = ;
            // liabilityDetail.LiabilityRemainingTermMonthsCount = ;
            // liabilityDetail.LiabilityType = ;
            // liabilityDetail.LiabilityTypeOtherDescription = ;
            // liabilityDetail.LiabilityUnpaidBalanceAmount = ;
            // liabilityDetail.SubjectLoanResubordinationIndicator = ;

            return liabilityDetail;
        }

        private LiabilityHolder CreateLiabilityHolder()
        {
            LiabilityHolder liabilityHolder = new LiabilityHolder();

            // liabilityHolder.Address = CreateAddress();
            // liabilityHolder.ContactPoints = CreateContactPoints();
            // liabilityHolder.Name = CreateName();

            return liabilityHolder;
        }

        private LienHolder CreateLienHolder()
        {
            LienHolder lienHolder = new LienHolder();

            // lienHolder.LienHolderType = ;
            // lienHolder.LienHolderTypeOtherDescription = ;

            return lienHolder;
        }

        private ListingInformation CreateListingInformation()
        {
            ListingInformation listingInformation = new ListingInformation();

            // listingInformation.CurrentListPriceAmount = ;
            // listingInformation.CurrentlyListedIndicator = ;
            // listingInformation.DaysOnMarketCount = ;
            // listingInformation.LastListPriceRevisionDate = ;
            // listingInformation.ListedWithinPreviousYearDescription = ;
            // listingInformation.ListedWithinPreviousYearIndicator = ;
            // listingInformation.OriginalListPriceAmount = ;

            return listingInformation;
        }

        private Loan CreateLoan()
        {
            Loan loan = new Loan();

            // loan.Ach = CreateAch();
            // loan.Adjustment = CreateAdjustment();
            // loan.AffordableLending = CreateAffordableLending();
            // loan.Amortization = CreateAmortization();
            // loan.Assumability = CreateAssumability();
            // loan.BillingAddress = CreateBillingAddress();
            // loan.Buydown = CreateBuydown();
            // loan.ClosingInformation = CreateClosingInformation();
            // loan.Construction = CreateConstruction();
            // loan.CreditEnhancements = CreateCreditEnhancements();
            // loan.Documentations = CreateDocumentations();
            // loan.DownPayments = CreateDownPayments();
            // loan.Draw = CreateDraw();
            // loan.EligibleLoanProducts = CreateEligibleLoanProducts();
            // loan.Escrow = CreateEscrow();
            // loan.FormSpecificContents = CreateFormSpecificContents();
            // loan.GovernmentLoan = CreateGovernmentLoan();
            // loan.Heloc = CreateHeloc();
            // loan.HmdaLoan = CreateHmdaLoan();
            // loan.IneligibleLoanProducts = CreateIneligibleLoanProducts();
            // loan.InterestCalculation = CreateInterestCalculation();
            // loan.InterestOnly = CreateInterestOnly();
            // loan.InvestorFeatures = CreateInvestorFeatures();
            // loan.InvestorLoanInformation = CreateInvestorLoanInformation();
            // loan.LateCharge = CreateLateCharge();
            // loan.LoanComments = CreateLoanComments();
            // loan.LoanDetail = CreateLoanDetail();
            // loan.LoanIdentifiers = CreateLoanIdentifiers();
            // loan.LoanLevelCredit = CreateLoanLevelCredit();
            // loan.LoanPrograms = CreateLoanPrograms();
            // loan.LoanState = CreateLoanState();
            // loan.LoanStatuses = CreateLoanStatuses();
            // loan.Ltv = CreateLtv();
            // loan.Maturity = CreateMaturity();
            // loan.MersRegistrations = CreateMersRegistrations();
            // loan.MiData = CreateMiData();
            // loan.Modifications = CreateModifications();
            // loan.MortgageScores = CreateMortgageScores();
            // loan.NegativeAmortization = CreateNegativeAmortization();
            // loan.OptionalProducts = CreateOptionalProducts();
            // loan.OriginationFunds = CreateOriginationFunds();
            // loan.OriginationSystems = CreateOriginationSystems();
            // loan.Payment = CreatePayment();
            // loan.PrepaymentPenalty = CreatePrepaymentPenalty();
            // loan.ProposedHousingExpenses = CreateProposedHousingExpenses();
            // loan.PurchaseCredits = CreatePurchaseCredits();
            // loan.Qualification = CreateQualification();
            // loan.Refinance = CreateRefinance();
            // loan.Rehabilitation = CreateRehabilitation();
            // loan.Respa = CreateRespa();
            // loan.SelectedLoanProduct = CreateSelectedLoanProduct();
            // loan.Servicing = CreateServicing();
            // loan.TermsOfMortgage = CreateTermsOfMortgage();
            // loan.Underwriting = CreateUnderwriting();
            // loan.LoanRoleType = ;
            // loan.SelectionType = ;
            // loan.SequenceNumber = ;

            return loan;
        }

        private Loans CreateLoans(CPageData dataLoan)
        {
            Loans loans = new Loans();

            loans.CombinedLtvs = CreateCombinedLtvs(dataLoan);
            loans.LoanList.Add(CreateLoanClosing(dataLoan));
            loans.LoanList.Add(CreateLoanCurrent(dataLoan));

            CPageData dataLoanLinked = null;

            if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
            {
                dataLoanLinked = LoadDataLoan(dataLoan.sLinkedLoanInfo.LinkedLId, null);
            }
            if (dataLoan.sLienPosT == E_sLienPosT.Second && dataLoanLinked != null)
            {
                loans.LoanList.Add(CreateLoanClosingRelated(dataLoan, dataLoanLinked));
                loans.LoanList.Add(CreateLoanCurrentRelated(dataLoan, dataLoanLinked));
            }
            //// 2/26/2015 - ir - OPM 204950: Create related loan for Freddie Mac ULDD if subordinate financing is present.
            else if (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && dataLoan.sSubFin > 0)
            {
                loans.LoanList.Add(CreateLoanCurrentRelated2ndFreddie(dataLoan, dataLoanLinked));
            }
            else if (dataLoan.sLienPosT == E_sLienPosT.First && dataLoanLinked != null)
            {
                loans.LoanList.Add(CreateLoanCurrentRelated2ndFannie(dataLoan, dataLoanLinked));
            }


            return loans;
        }
        private Loan CreateLoanCurrentRelated(CPageData dataLoan, CPageData dataLoanLinked)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.RelatedLoan;
            loan.LoanDetail = CreateLoanDetailCurrentRelated(dataLoan);
            loan.LoanState = CreateLoanStateCurrentRelated(dataLoan);
            loan.Payment = CreatePaymentCurrentRelated(dataLoanLinked.sUPBAmount_rep);

            if (dataLoan.m_convertLos.ToMoney(dataLoan.sSubFin_rep) != 0.0M)
            {
                loan.Heloc = CreateHelocCurrentRelated(dataLoan);
            }
            return loan;
        }
        private Loan CreateLoanClosingRelated(CPageData dataLoan, CPageData dataLoanLinked)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.RelatedLoan;

            loan.Amortization = CreateAmortizationClosingRelated(dataLoanLinked);
            loan.LoanDetail = CreateLoanDetailClosingRelated(dataLoanLinked);
            loan.LoanState = CreateLoanStateClosingRelated(dataLoanLinked);
            loan.Maturity = CreateMaturityClosingRelated(dataLoanLinked);
            loan.Payment = CreatePaymentClosingRelated(dataLoanLinked);
            loan.TermsOfMortgage = CreateTermsOfMortgageClosingRelated(dataLoanLinked);
            return loan;
        }
        private TermsOfMortgage CreateTermsOfMortgageClosingRelated(CPageData dataLoanLinked)
        {
            TermsOfMortgage termsOfMortgage = new TermsOfMortgage();
            termsOfMortgage.LienPriorityType = LienPriorityType.FirstLien;
            termsOfMortgage.NoteAmount = dataLoanLinked.sFinalLAmt_rep;

            return termsOfMortgage;
        }
        private Payment CreatePaymentClosingRelated(CPageData dataLoanLinked)
        {
            Payment payment = new Payment();
            payment.PaymentRule.ScheduledFirstPaymentDate = dataLoanLinked.sSchedDueD1_rep;

            return payment;
        }
        private Maturity CreateMaturityClosingRelated(CPageData dataLoanLinked)
        {
            Maturity maturity = new Maturity();
            maturity.MaturityRule = CreateMaturityRuleClosingRelated(dataLoanLinked);

            return maturity;
        }
        private MaturityRule CreateMaturityRuleClosingRelated(CPageData dataLoanLinked)
        {
            MaturityRule maturityRule = new MaturityRule();
            maturityRule.LoanMaturityPeriodCount = dataLoanLinked.sTerm_rep;
            maturityRule.LoanMaturityPeriodType = LoanMaturityPeriodType.Month;

            return maturityRule;
        }
        private LoanState CreateLoanStateClosingRelated(CPageData dataLoanLinked)
        {
            LoanState loanState = new LoanState();
            loanState.LoanStateDate = dataLoanLinked.sDocumentNoteD_rep;
            loanState.LoanStateType = LoanStateType.AtClosing;
            return loanState;
        }
        private LoanDetail CreateLoanDetailClosingRelated(CPageData dataLoanLinked)
        {
            LoanDetail loanDetail = new LoanDetail();
            loanDetail.BalloonIndicator = dataLoanLinked.sTerm > dataLoanLinked.sDue;
            return loanDetail;
        }
        private Amortization CreateAmortizationClosingRelated(CPageData dataLoanLinked)
        {
            Amortization amortization = new Amortization();

            amortization.AmortizationRule = CreateAmortizationRuleClosingRelated(dataLoanLinked);
            return amortization;
        }
        private AmortizationRule CreateAmortizationRuleClosingRelated(CPageData dataLoanLinked)
        {
            AmortizationRule amortizationRule = new AmortizationRule();
            amortizationRule.LoanAmortizationType = ToMismo(dataLoanLinked.sFinMethT);
            return amortizationRule;
        }
        private Loan CreateLoanCurrentRelated2ndFannie(CPageData dataLoan, CPageData dataLoanLinked)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.RelatedLoan;
            loan.LoanDetail = CreateLoanDetailCurrentRelated(dataLoan);
            loan.LoanState = CreateLoanStateCurrentRelated(dataLoan);

            if (dataLoanLinked.m_convertLos.ToMoney(dataLoanLinked.sUPBAmount_rep) > 0.0M)
            {
                loan.Payment = CreatePaymentCurrentRelated(dataLoanLinked.sUPBAmount_rep);
            }
            loan.TermsOfMortgage = CreateTermsOfMortgageCurrentRelated(dataLoanLinked);

            if (loan.LoanDetail.HELOCIndicator == true)
            {
                loan.Heloc = CreateHelocCurrentRelated(dataLoan);
            }
            return loan;
        }
        private Loan CreateLoanCurrentRelated2ndFreddie(CPageData dataLoan, CPageData dataLoanLinked)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.RelatedLoan;
            loan.LoanDetail = CreateLoanDetailCurrentRelated(dataLoan);
            loan.LoanState = CreateLoanStateCurrentRelated(dataLoan);
            if (loan.LoanDetail.HELOCIndicator == true)
            {
                loan.Heloc = CreateHelocCurrentRelated(dataLoan);
            }
            else
            {
                if (dataLoanLinked == null)
                {
                    loan.Payment = CreatePaymentCurrentRelated(dataLoan.sConcurSubFin_rep);
                }
                else
                {
                    loan.Payment = CreatePaymentCurrentRelated(dataLoanLinked.sUPBAmount_rep);
                }
            }

            loan.TermsOfMortgage = CreateTermsOfMortgageCurrentRelated(dataLoanLinked);

            return loan;
        }

        private Heloc CreateHelocCurrentRelated(CPageData dataLoan)
        {
            Heloc heloc = new Heloc();

            heloc.HelocOccurrences.HelocOccurrenceList.Add(CreateHelocOccurenceRelated(dataLoan));
            return heloc;
        }
        private HelocOccurrence CreateHelocOccurenceRelated(CPageData dataLoan)
        {
            HelocOccurrence helocOccurence = new HelocOccurrence();

            helocOccurence.CurrentHELOCMaximumBalanceAmount = dataLoan.sSubFin_rep;
            helocOccurence.HELOCBalanceAmount = dataLoan.sConcurSubFin_rep;

            return helocOccurence;
        }
        private TermsOfMortgage CreateTermsOfMortgageCurrentRelated(CPageData dataLoanLinked)
        {
            TermsOfMortgage termsOfMortgage = new TermsOfMortgage();

            termsOfMortgage.LienPriorityType = LienPriorityType.SecondLien;
            if (dataLoanLinked == null) //Assume Conventional for FHLMC if there is no linked loan
            {
                termsOfMortgage.MortgageType = MortgageType.Conventional;
            }
            else
            {
                termsOfMortgage.MortgageType = ToMismo(dataLoanLinked.sLT);
            }
            return termsOfMortgage;
        }
        private Payment CreatePaymentCurrentRelated(string upbAmount)
        {
            Payment payment = new Payment();

            payment.PaymentSummary = CreatePaymentSummaryCurrentRelated(upbAmount);
            return payment;
        }
        private PaymentSummary CreatePaymentSummaryCurrentRelated(string upbAmount)
        {
            PaymentSummary paymentSummary = new PaymentSummary();
            paymentSummary.UPBAmount = upbAmount;
            return paymentSummary;
        }
        private LoanState CreateLoanStateCurrentRelated(CPageData dataLoan)
        {
            LoanState loanState = new LoanState();
            loanState.LoanStateDate = dataLoan.m_convertLos.ToDateTimeString(DateTime.Now);
            loanState.LoanStateType = LoanStateType.Current;
            return loanState;
        }
        private LoanDetail CreateLoanDetailCurrentRelated(CPageData dataLoan)
        {
            LoanDetail loanDetail = new LoanDetail();
            loanDetail.HELOCIndicator = dataLoan.sIsOFinCreditLineInDrawPeriod;

            if ((Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2) && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                loanDetail.LoanAffordableIndicator = dataLoan.sIsCommunitySecond;
            }

            return loanDetail;
        }
        private LoanComment CreateLoanCommentCurrent(CPageData dataLoan)
        {
            LoanComment loanComment = new LoanComment();

            loanComment.LoanCommentText = dataLoan.sGseDeliveryComments;

            return loanComment;
        }

        private LoanComments CreateLoanCommentsCurrent(CPageData dataLoan)
        {
            LoanComments loanComments = new LoanComments();

            loanComments.LoanCommentList.Add(CreateLoanCommentCurrent(dataLoan));

            return loanComments;
        }

        private LoanDetail CreateLoanDetailCurrent(CPageData dataLoan)
        {
            LoanDetail loanDetail = new LoanDetail();

            if (dataLoan.sTerm != dataLoan.sDue)
            {
                loanDetail.BalloonResetIndicator = false;
            }
            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                loanDetail.CurrentInterestRatePercent = dataLoan.sNoteIR_rep;
            }
            loanDetail.MortgageModificationIndicator = false;

            return loanDetail;
        }
        private LoanIdentifiers CreateLoanIdentifiersCurrent(CPageData dataLoan)
        {
            LoanIdentifiers loanIdentifiers = new LoanIdentifiers();

            LoanIdentifier loanIdentifier = new LoanIdentifier();
            if (!string.IsNullOrEmpty(dataLoan.sGseDeliveryCommitmentId) && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                loanIdentifier.InvestorCommitmentIdentifier = dataLoan.sGseDeliveryCommitmentId;
                loanIdentifiers.LoanIdentifierList.Add(loanIdentifier);
            }

            loanIdentifier = new LoanIdentifier();
            if (!string.IsNullOrEmpty(dataLoan.sGseDeliveryContractId))
            {
                loanIdentifier.InvestorContractIdentifier = dataLoan.sGseDeliveryContractId;
                loanIdentifiers.LoanIdentifierList.Add(loanIdentifier);
            }

            loanIdentifier = new LoanIdentifier();
            if (!string.IsNullOrEmpty(dataLoan.sMersMin))
            {
                loanIdentifier.MERS_MINIdentifier = dataLoan.sMersMin;
                loanIdentifiers.LoanIdentifierList.Add(loanIdentifier);
            }

            loanIdentifier = new LoanIdentifier();
            if (!string.IsNullOrEmpty(dataLoan.sLenLNum))
            {
                loanIdentifier.SellerLoanIdentifier = dataLoan.sLenLNum;
                loanIdentifiers.LoanIdentifierList.Add(loanIdentifier);
            }

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2) &&
                !string.IsNullOrEmpty(dataLoan.sGseInvestorLoanIdentifier))
            {
                loanIdentifier = new LoanIdentifier();
                loanIdentifier.InvestorLoanIdentifier = dataLoan.sGseInvestorLoanIdentifier;
                loanIdentifiers.LoanIdentifierList.Add(loanIdentifier);
            }

            // Universal loan identifier is ULDD 3 part 2 for both GSEs
            if (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2 && !string.IsNullOrEmpty(dataLoan.sUniversalLoanIdentifier))
            {
                loanIdentifiers.LoanIdentifierList.Add(CreateUniversalLoanIdentifier(dataLoan));
            }

            return loanIdentifiers;
        }
        private global::ULDD.LoanPrograms CreateLoanProgramsCurrent(CPageData dataLoan)
        {
            global::ULDD.LoanPrograms loanPrograms = new global::ULDD.LoanPrograms();

            loanPrograms.LoanProgramList.Add(CreateLoanProgramCurrent(dataLoan));

            return loanPrograms;
        }

        private LoanProgram CreateLoanProgramCurrent(CPageData dataLoan)
        {
            LoanProgram loanProgram = new LoanProgram();

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase && dataLoan.sOccT == E_sOccT.PrimaryResidence)
                {
                    foreach (var app in dataLoan.Apps)
                    {
                        bool hasCoborrower = !string.IsNullOrEmpty(app.aCNm) && !string.IsNullOrEmpty(app.aCSsn);
                        if (app.aBDecPastOwnership.Equals("N") || (hasCoborrower && app.aCDecPastOwnership.Equals("N")))
                        {
                            loanProgram.LoanProgramIdentifier = "LoanFirstTimeHomebuyer";
                            break;
                        }
                    }
                }
            }
            else
            {
                loanProgram.LoanProgramIdentifier = GetFreddieMacLoanProgramIdentifier(dataLoan.sGseFreddieMacProductT);
            }

            return loanProgram;
        }

        private string GetFreddieMacLoanProgramIdentifier(E_sGseFreddieMacProductT productType)
        {
            switch (productType)
            {
                case E_sGseFreddieMacProductT.None:
                    return null;
                case E_sGseFreddieMacProductT.AlternateRequirementsDesktopUnderwriter:
                    return "AlternateRequirementsDesktopUnderwriter";
                case E_sGseFreddieMacProductT.AlternativeFullInformation:
                    return "AlternativeFullInformation";
                case E_sGseFreddieMacProductT.AMinusMortgage:
                    return "AMinusMortgage";
                case E_sGseFreddieMacProductT.BuilderOrDeveloperAffiliated:
                    return "BuilderOrDeveloperAffiliated";
                case E_sGseFreddieMacProductT.ConstructionConversion:
                    return "ConstructionConversion";
                case E_sGseFreddieMacProductT.CorrAdvantageLoan:
                    return "CorrAdvantageLoan";
                case E_sGseFreddieMacProductT.DecliningBalanceCoOwnershipInitiative:
                    return "DecliningBalanceCoOwnershipInitiative";
                case E_sGseFreddieMacProductT.DisasterReliefProgram:
                    return "DisasterReliefProgram";
                case E_sGseFreddieMacProductT.DreaMaker:
                    return "DreaMaker";
                case E_sGseFreddieMacProductT.EnergyConservation:
                    return "EnergyConservation";
                case E_sGseFreddieMacProductT.FREOwnedCondoProject:
                    return "FREOwnedCondoProject";
                case E_sGseFreddieMacProductT.HomeOpportunity:
                    return "HomeOpportunity";
                case E_sGseFreddieMacProductT.HomePossible97:
                    return "HomePossible97";
                case E_sGseFreddieMacProductT.HomePossibleAdvantage:
                    return "HomePossibleAdvantage";
                case E_sGseFreddieMacProductT.HomePossibleAdvantageHFA:
                    return "HomePossibleAdvantageHFA";
                case E_sGseFreddieMacProductT.HomePossibleHomeReady:
                    return "HomePossibleHomeReady";
                case E_sGseFreddieMacProductT.HomePossibleMCM:
                    return "HomePossibleMCM";
                case E_sGseFreddieMacProductT.HomePossibleMCMCS:
                    return "HomePossibleMCMCS";
                case E_sGseFreddieMacProductT.HomePossibleMortgage:
                    return "HomePossibleMortgage";
                case E_sGseFreddieMacProductT.HomePossibleNeighborhoodSolution97:
                    return "HomePossibleNeighborhoodSolution97";
                case E_sGseFreddieMacProductT.LoansToFacilitateREOSales:
                    return "LoansToFacilitateREOSales";
                case E_sGseFreddieMacProductT.LongTermStandBy:
                    return "LongTermStandBy";
                case E_sGseFreddieMacProductT.MortgageRevenueBond:
                    return "MortgageRevenueBond";
                case E_sGseFreddieMacProductT.MortgageRewardsProgram:
                    return "MortgageRewardsProgram";
                case E_sGseFreddieMacProductT.MurabahaMortgage:
                    return "MurabahaMortgage";
                case E_sGseFreddieMacProductT.Negotiated97PercentLTVLoanProgram:
                    return "Negotiated97PercentLTVLoanProgram";
                case E_sGseFreddieMacProductT.NeighborhoodChampions:
                    return "NeighborhoodChampions";
                case E_sGseFreddieMacProductT.NoFeeMortgagePlus:
                    return "NoFeeMortgagePlus";
                case E_sGseFreddieMacProductT.OptimumMortgageProgram:
                    return "OptimumMortgageProgram";
                case E_sGseFreddieMacProductT.RecourseGuaranteedByThirdParty:
                    return "RecourseGuaranteedByThirdParty";
                case E_sGseFreddieMacProductT.Renovation:
                    return "Renovation";
                case E_sGseFreddieMacProductT.ShortTermStandBy:
                    return "ShortTermStandBy";
                case E_sGseFreddieMacProductT.SolarInitiative:
                    return "SolarInitiative";
                default:
                    throw new UnhandledEnumException(productType);
            }
        }

        private MICompanyNameType ToMismo(E_sMiCompanyNmT sMiCompanyNmT)
        {
            switch (sMiCompanyNmT)
            {
                case E_sMiCompanyNmT.Amerin:
                case E_sMiCompanyNmT.Arch:
                case E_sMiCompanyNmT.CAHLIF:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.CMG:
                case E_sMiCompanyNmT.CMGPre94:
                case E_sMiCompanyNmT.Commonwealth:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.Essent:
                    return MICompanyNameType.Essent;
                case E_sMiCompanyNmT.FHA:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.Genworth:
                    return MICompanyNameType.Genworth;
                case E_sMiCompanyNmT.LeaveBlank:
                    return MICompanyNameType.Undefined;
                case E_sMiCompanyNmT.MDHousing:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.MGIC:
                    return MICompanyNameType.MGIC;
                case E_sMiCompanyNmT.MIF:
                case E_sMiCompanyNmT.NationalMI:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.PMI:
                    return MICompanyNameType.PMI;
                case E_sMiCompanyNmT.Radian:
                    return MICompanyNameType.Radian;
                case E_sMiCompanyNmT.RMIC:
                    return MICompanyNameType.RMIC;
                case E_sMiCompanyNmT.RMICNC:
                case E_sMiCompanyNmT.SONYMA:
                    return MICompanyNameType.Other;
                case E_sMiCompanyNmT.Triad:
                    return MICompanyNameType.Triad;
                case E_sMiCompanyNmT.UnitedGuaranty:
                    return MICompanyNameType.UGI;
                case E_sMiCompanyNmT.USDA:
                case E_sMiCompanyNmT.VA:
                case E_sMiCompanyNmT.Verex:
                case E_sMiCompanyNmT.WiscMtgAssr:
                case E_sMiCompanyNmT.MassHousing:
                    return MICompanyNameType.Other;
                default:
                    throw new UnhandledEnumException(sMiCompanyNmT);
            }
        }

        private MiDataDetail CreateMiDataDetailCurrent(CPageData dataLoan)
        {
            MiDataDetail miDataDetail = new MiDataDetail();

            // 6/9/2014 AV - 183847 Remove ULDD audit rule requiring sMiLenderPaidAdj when LPMI, always export LenderPaidMIInterestRateAdjustmentPercent if LPMI LQB-SDE: Integrations | Milest
            if (dataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
            {
                var formatTarget = dataLoan.GetFormatTarget();
                dataLoan.SetFormatTarget(FormatTarget.XsltExport); //it actually exports 0
                miDataDetail.LenderPaidMIInterestRateAdjustmentPercent = dataLoan.sMiLenderPaidAdj_rep;
                dataLoan.SetFormatTarget(formatTarget);
            }

            miDataDetail.MICertificateIdentifier = dataLoan.sMiCertId;

            if (!string.IsNullOrEmpty(miDataDetail.MICertificateIdentifier))
            {
                if (dataLoan.sFfUfmipFinanced > 0)
                {
                    miDataDetail.MIPremiumFinancedIndicator = true;
                    miDataDetail.MIPremiumFinancedAmount = dataLoan.sFfUfmipFinanced_rep;
                }
                else
                {
                    miDataDetail.MIPremiumFinancedIndicator = false;
                }
            }

            if (dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.FHA
                && dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.USDA
                && dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.LeaveBlank
                && dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.VA)
            {
                miDataDetail.MICompanyNameType = ToMismo(dataLoan.sMiCompanyNmT);

                if (miDataDetail.MICompanyNameType == MICompanyNameType.Other)
                {
                    if (dataLoan.sMiCompanyNmT == E_sMiCompanyNmT.NationalMI)
                    {
                        miDataDetail.MICompanyNameTypeOtherDescription = "NMI";
                    }
                    else if (dataLoan.sMiCompanyNmT == E_sMiCompanyNmT.Arch)
                    {
                        miDataDetail.MICompanyNameTypeOtherDescription = "ArchMI";
                    }
                    else
                    {
                        miDataDetail.MICompanyNameTypeOtherDescription = dataLoan.sMiCompanyNmT.ToString();
                    }
                }
            }

            miDataDetail.MICoveragePercent = TruncateMoneyAmount(dataLoan.sMiLenderPaidCoverage_rep);

            if (dataLoan.sMiInsuranceT != E_sMiInsuranceT.InvestorPaid && dataLoan.sMiInsuranceT != E_sMiInsuranceT.None)
            {
                miDataDetail.MIPremiumSourceType = ToMismo(dataLoan.sMiInsuranceT);
            }
            else if (dataLoan.sLT == E_sLT.Conventional)
            {
                if (dataLoan.sMiReasonForAbsenceT == E_sMiReasonForAbsenceT.MiCanceledBasedOnCurrentLtv
                    || dataLoan.sMiReasonForAbsenceT == E_sMiReasonForAbsenceT.NoMiBasedOnOriginalLtv)
                {
                    miDataDetail.PrimaryMIAbsenceReasonType = ToMismo(dataLoan.sMiReasonForAbsenceT);
                }
                else
                {
                    miDataDetail.PrimaryMIAbsenceReasonType = PrimaryMIAbsenceReasonType.Other;
                    miDataDetail.PrimaryMIAbsenceReasonTypeOtherDescription = dataLoan.sMiReasonForAbsenceT.ToString();
                }
            }

            return miDataDetail;
        }
        private MIPremiumSourceType ToMismo(E_sMiInsuranceT sMiInsuranceT)
        {
            switch (sMiInsuranceT)
            {
                case E_sMiInsuranceT.BorrowerPaid:
                    return MIPremiumSourceType.Borrower;
                case E_sMiInsuranceT.LenderPaid:
                    return MIPremiumSourceType.Lender;
                default:
                    throw new UnhandledEnumException(sMiInsuranceT);
            }
        }
        private PrimaryMIAbsenceReasonType ToMismo(E_sMiReasonForAbsenceT sMiReasonForAbsenceT)
        {
            switch (sMiReasonForAbsenceT)
            {
                case E_sMiReasonForAbsenceT.MiCanceledBasedOnCurrentLtv:
                    return PrimaryMIAbsenceReasonType.MICanceledBasedOnCurrentLTV;
                case E_sMiReasonForAbsenceT.NoMiBasedOnOriginalLtv:
                    return PrimaryMIAbsenceReasonType.NoMIBasedOnOriginalLTV;
                default:
                    throw new UnhandledEnumException(sMiReasonForAbsenceT);
            }
        }
        private MiData CreateMiDataCurrent(CPageData dataLoan)
        {
            MiData miData = new MiData();

            miData.MiDataDetail = CreateMiDataDetailCurrent(dataLoan);

            return miData;
        }

        private PaymentComponentBreakout CreatePaymentComponentBreakoutCurrent(CPageData dataLoan)
        {
            PaymentComponentBreakout paymentComponentBreakout = new PaymentComponentBreakout();

            paymentComponentBreakout.PrincipalAndInterestPaymentAmount = dataLoan.sProThisMPmt_rep;

            return paymentComponentBreakout;
        }

        private PaymentComponentBreakouts CreatePaymentComponentBreakoutsCurrent(CPageData dataLoan)
        {
            PaymentComponentBreakouts paymentComponentBreakouts = new PaymentComponentBreakouts();

            paymentComponentBreakouts.PaymentComponentBreakoutList.Add(CreatePaymentComponentBreakoutCurrent(dataLoan));

            return paymentComponentBreakouts;
        }
        private PaymentSummary CreatePaymentSummaryCurrent(CPageData dataLoan)
        {
            PaymentSummary paymentSummary = new PaymentSummary();

            //// 12/27/2014 IR - OPM 197829 - LastPaymentReceivedDate required only if InterestCalculationPeriodType = Day. Currently, we hardcode Month only.
            //DateTime LastPaymentReceivedDate = DateTime.MinValue;
            DateTime LastPaidInstallmentDueDate = DateTime.MinValue;

            //decimal AggregateLoanCurtailmentAmount = 0.0M;
            foreach (CServicingPaymentFields spf in dataLoan.sServicingPayments)
            {
                /*if (spf.PaymentAmt > spf.DueAmt)
                {
                    AggregateLoanCurtailmentAmount += spf.PaymentAmt - spf.DueAmt;
                }*/

                /*if (spf.PaymentD > LastPaymentReceivedDate)
                {
                    LastPaymentReceivedDate = spf.PaymentD;
                }*/

                if (spf.PaymentD != DateTime.MinValue && spf.DueD > LastPaidInstallmentDueDate)
                {
                    LastPaidInstallmentDueDate = spf.DueD;
                }
            }

            if (dataLoan.m_convertLos.ToMoney(dataLoan.sServicingCollectedFunds_Principal_rep) > 0M)
            {
                paymentSummary.AggregateLoanCurtailmentAmount = TruncateMoneyAmount(dataLoan.sServicingCollectedFunds_Principal_rep);
            }

            if (LastPaidInstallmentDueDate != DateTime.MinValue)
            {
                paymentSummary.LastPaidInstallmentDueDate = dataLoan.m_convertLos.ToDateTimeString(LastPaidInstallmentDueDate);
            }
            else
            {
                if (dataLoan.sSchedDueD1.IsValid)
                {
                    paymentSummary.LastPaidInstallmentDueDate = dataLoan.m_convertLos.ToDateTimeString(dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(-1));
                }
            }

            /*if (LastPaymentReceivedDate != DateTime.MinValue)
            {
                paymentSummary.LastPaymentReceivedDate = dataLoan.m_convertLos.ToDateTimeString(LastPaymentReceivedDate);
            }*/

            paymentSummary.UPBAmount = TruncateMoneyAmount(dataLoan.sUPBAmount_rep);

            return paymentSummary;
        }

        private Payment CreatePaymentCurrent(CPageData dataLoan)
        {
            Payment payment = new Payment();

            //// ir - iOPM 190606: Set /PAYMENT_COMPONENT_BREAKOUTS/PAYMENT_COMPONENT_BREAKOUT/PrincipalAndInterestPaymentAmount for ARM only.
            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                payment.PaymentComponentBreakouts = CreatePaymentComponentBreakoutsCurrent(dataLoan);
            }
            payment.PaymentSummary = CreatePaymentSummaryCurrent(dataLoan);

            return payment;
        }

        private LoanProductDetail CreateLoanProductDetailCurrent(CPageData dataLoan)
        {
            LoanProductDetail loanProductDetail = new LoanProductDetail();

            if (dataLoan.sSpGseRefiProgramT != E_sSpGseRefiProgramT.None)
            {
                if (dataLoan.sProdIsTexas50a6Loan)
                {
                    loanProductDetail.RefinanceProgramIdentifier = "TexasEquity";
                }
                else
                {
                    switch (dataLoan.sSpGseRefiProgramT)
                    {
                        case E_sSpGseRefiProgramT.None:
                            break;
                        case E_sSpGseRefiProgramT.DURefiPlus:
                            loanProductDetail.RefinanceProgramIdentifier = "DURefiPlus";
                            break;
                        case E_sSpGseRefiProgramT.RefiPlus:
                            loanProductDetail.RefinanceProgramIdentifier = "RefiPlus";
                            break;
                        case E_sSpGseRefiProgramT.ReliefRefiOpenAccess:
                            loanProductDetail.RefinanceProgramIdentifier = "ReliefRefinanceOpenAccess";
                            break;
                        case E_sSpGseRefiProgramT.ReliefRefiSameServicer:
                            loanProductDetail.RefinanceProgramIdentifier = "ReliefRefinanceSameServicer";
                            break;
                        case E_sSpGseRefiProgramT.FHLBDisasterResponse:
                            loanProductDetail.RefinanceProgramIdentifier = "DisasterResponse";
                            break;
                    }
                }
            }

            return loanProductDetail;
        }

        private SelectedLoanProduct CreateSelectedLoanProductCurrent(CPageData dataLoan)
        {
            SelectedLoanProduct selectedLoanProduct = new SelectedLoanProduct();

            selectedLoanProduct.LoanProductDetail = CreateLoanProductDetailCurrent(dataLoan);

            return selectedLoanProduct;
        }
        private DelinquencySummary CreateDelinquencySummaryCurrent(CPageData dataLoan)
        {
            DelinquencySummary delinquencySummary = new DelinquencySummary();

            int count = 0;
            foreach (CServicingPaymentFields spf in dataLoan.sServicingPayments)
            {
                DateTime paymentD = spf.PaymentD;
                DateTime dueD = spf.DueD;

                if (dueD == DateTime.MinValue || dueD.AddMonths(12) < DateTime.Now)
                    continue;

                if (paymentD == DateTime.MinValue)
                    paymentD = DateTime.Now;

                if (dueD.Day == 1)
                    dueD = dueD.AddMonths(1);
                else
                    dueD = dueD.AddDays(30);

                if (paymentD > dueD)
                    count++;
            }

            delinquencySummary.DelinquentPaymentsOverPastTwelveMonthsCount = count.ToString();

            return delinquencySummary;
        }

        private Servicing CreateServicingCurrent(CPageData dataLoan)
        {
            Servicing servicing = new Servicing();

            servicing.DelinquencySummary = CreateDelinquencySummaryCurrent(dataLoan);

            return servicing;
        }
        private Loan CreateLoanCurrent(CPageData dataLoan)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.SubjectLoan;

            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                loan.Adjustment = CreateAdjustmentCurrent(dataLoan);
            }

            if (!dataLoan.sWillEscrowBeWaived && !m_isLQAExport)
            {
                loan.Escrow = CreateEscrow(dataLoan);
            }

            loan.InvestorFeatures = CreateInvestorFeatures(dataLoan);
            loan.InvestorLoanInformation = CreateInvestorLoanInformationCurrent(dataLoan);
            loan.LoanComments = CreateLoanCommentsCurrent(dataLoan);
            loan.LoanDetail = CreateLoanDetailCurrent(dataLoan);
            loan.LoanIdentifiers = CreateLoanIdentifiersCurrent(dataLoan);
            loan.LoanPrograms = CreateLoanProgramsCurrent(dataLoan);
            loan.LoanState = CreateLoanStateCurrent();
            loan.MiData = CreateMiDataCurrent(dataLoan);
            loan.Payment = CreatePaymentCurrent(dataLoan);

            loan.SelectedLoanProduct = CreateSelectedLoanProductCurrent(dataLoan);
            loan.Servicing = CreateServicingCurrent(dataLoan);
            return loan;
        }
        private LoanState CreateLoanStateCurrent()
        {
            LoanState loanState = new LoanState();
            loanState.LoanStateDate = DateTime.Now.ToString("yyyy-MM-dd");
            loanState.LoanStateType = LoanStateType.Current;

            return loanState;
        }
        private InvestorLoanInformation CreateInvestorLoanInformationCurrent(CPageData dataLoan)
        {
            InvestorLoanInformation investorLoanInformation = new InvestorLoanInformation();
            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
            {
                investorLoanInformation.LoanAcquisitionScheduledUPBAmount = dataLoan.sGseDeliveryScheduleUPBAmt_rep;
            }

            if (dataLoan.sGseDeliveryLoanDeliveryT == E_sGseDeliveryLoanDeliveryT.MBS)
            {
                investorLoanInformation.BaseGuarantyFeePercent = dataLoan.sGseDeliveryBaseGuaranteeFPc_rep;
                investorLoanInformation.GuarantyFeeAfterAlternatePaymentMethodPercent = dataLoan.sGseDeliveryGuaranteeFPc_rep;
                investorLoanInformation.GuarantyFeePercent = dataLoan.sGseDeliveryGuaranteeFPc_rep;
                investorLoanInformation.InvestorRemittanceDay = dataLoan.sGseDeliveryRemittanceNumOfDays_rep;
                investorLoanInformation.LoanAcquisitionScheduledUPBAmount = dataLoan.sGseDeliveryScheduleUPBAmt_rep;

                investorLoanInformation.LoanDefaultLossPartyType = ToMismo(dataLoan.sGseDeliveryLoanDefaultLossPartyT);
                investorLoanInformation.REOMarketingPartyType = ToMismo(dataLoan.sGseDeliveryREOMarketingPartyT);
            }

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac ||
                dataLoan.sGseDeliveryLoanDeliveryT == E_sGseDeliveryLoanDeliveryT.MBS)
            {
                decimal buyF = dataLoan.m_convertLos.ToRate(dataLoan.sGseDeliveryBuyF_rep);  // opm 138869, want points not percent.
                investorLoanInformation.LoanBuyupBuydownBasisPointNumber = dataLoan.m_convertLos.ToCountString((int)Math.Abs(buyF));

                if (buyF == 0)
                {
                    if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac)
                    {
                        investorLoanInformation.LoanBuyupBuydownType = LoanBuyupBuydownType.BuyupBuydownDoesNotApply;
                    }
                }
                else if (buyF < 0)
                {
                    investorLoanInformation.LoanBuyupBuydownType = LoanBuyupBuydownType.Buydown;
                }
                else
                {
                    investorLoanInformation.LoanBuyupBuydownType = LoanBuyupBuydownType.Buyup;
                }
            }

            if (ToMismo(dataLoan.sSpValuationMethodT) == PropertyValuationMethodType.None)
            {
                investorLoanInformation.InvestorCollateralProgramIdentifier = ToMismo(dataLoan.sSpGseCollateralProgramT, dataLoan.sGseDeliveryTargetT);
            }
            investorLoanInformation.InvestorOwnershipPercent = TruncateMoneyAmount(dataLoan.sGseDeliveryOwnershipPc_rep);

            if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FannieMae || dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                investorLoanInformation.InvestorProductPlanIdentifier = dataLoan.sGseDeliveryPlanId;
            }

            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                investorLoanInformation.InvestorRemittanceType = ToMismo(dataLoan.sGseDeliveryRemittanceT);
            }

            return investorLoanInformation;
        }
        private InvestorRemittanceType ToMismo(E_sGseDeliveryRemittanceT sGseDeliveryRemittanceT)
        {
            switch (sGseDeliveryRemittanceT)
            {
                case E_sGseDeliveryRemittanceT.ActualActual:
                    return InvestorRemittanceType.ActualInterestActualPrincipal;
                case E_sGseDeliveryRemittanceT.ScheduledActual:
                    return InvestorRemittanceType.ScheduledInterestActualPrincipal;
                case E_sGseDeliveryRemittanceT.ScheduledScheduled:
                    return InvestorRemittanceType.ScheduledInterestScheduledPrincipal;
                default:
                    throw new UnhandledEnumException(sGseDeliveryRemittanceT);
            }
        }

        private string ToMismo(E_sSpGseCollateralProgramT sSpGseCollateralProgramT, E_sGseDeliveryTargetT sGseDeliveryTargetT)
        {
            switch (sSpGseCollateralProgramT)
            {
                case E_sSpGseCollateralProgramT.None:
                    return string.Empty;
                case E_sSpGseCollateralProgramT.DURefiPlus:
                    return "DURefiPlusPropertyFieldworkWaiver"; //FNMA Only
                case E_sSpGseCollateralProgramT.PropertyInspectionAlternative:
                    return "PropertyInspectionAlternative"; //FHLMC Only
                case E_sSpGseCollateralProgramT.PropertyInspectionWaiver:
                    return (sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                        ? "Level1PropertyInspectionWaiver" : "PropertyInspectionWaiver";
                case E_sSpGseCollateralProgramT.PropertyInspectionReportForm2075:
                    return (sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
                        ? "DUPropertyInspectionReportForm2075" : "Form2075";
                case E_sSpGseCollateralProgramT.PropertyInspectionReportForm2070:
                    return "Form2070"; //FHLMC Only
                default:
                    throw new UnhandledEnumException(sSpGseCollateralProgramT);
            }
        }
        private LoanDefaultLossPartyType ToMismo(E_sGseDeliveryLoanDefaultLossPartyT sGseDeliveryLoanDefaultLossPartyT)
        {
            switch (sGseDeliveryLoanDefaultLossPartyT)
            {
                case E_sGseDeliveryLoanDefaultLossPartyT.Investor:
                    return LoanDefaultLossPartyType.Investor;
                case E_sGseDeliveryLoanDefaultLossPartyT.Lender:
                    return LoanDefaultLossPartyType.Lender;
                case E_sGseDeliveryLoanDefaultLossPartyT.Shared:
                    return LoanDefaultLossPartyType.Shared;
                default:
                    throw new UnhandledEnumException(sGseDeliveryLoanDefaultLossPartyT);
            }
        }
        private REOMarketingPartyType ToMismo(E_sGseDeliveryREOMarketingPartyT sGseDeliveryREOMarketingPartyT)
        {
            switch (sGseDeliveryREOMarketingPartyT)
            {
                case E_sGseDeliveryREOMarketingPartyT.Investor:
                    return REOMarketingPartyType.Investor;
                case E_sGseDeliveryREOMarketingPartyT.Lender:
                    return REOMarketingPartyType.Lender;
                default:
                    throw new UnhandledEnumException(sGseDeliveryREOMarketingPartyT);
            }
        }
        private Loan CreateLoanClosing(CPageData dataLoan)
        {
            Loan loan = new Loan();
            loan.LoanRoleType = LoanRoleType.SubjectLoan;


            // loan.Ach = CreateAch();

            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                loan.Adjustment = CreateAdjustment(dataLoan);
            }
            // loan.AffordableLending = CreateAffordableLending();
            loan.Amortization = CreateAmortization(dataLoan);
            // loan.Assumability = CreateAssumability();
            // loan.BillingAddress = CreateBillingAddress();

            if (dataLoan.sHasTempBuydown)
            {
                loan.Buydown = CreateBuydown(dataLoan);
            }

            loan.ClosingInformation = CreateClosingInformation(dataLoan);
            // loan.Construction = CreateConstruction();
            // loan.CreditEnhancements = CreateCreditEnhancements();
            // loan.Documentations = CreateDocumentations();
            loan.DownPayments = CreateDownPayments(dataLoan);
            // loan.Draw = CreateDraw();
            // loan.EligibleLoanProducts = CreateEligibleLoanProducts();
            // loan.Escrow = CreateEscrow();
            loan.FormSpecificContents = CreateFormSpecificContents(dataLoan);
            if (dataLoan.sLT == E_sLT.FHA || dataLoan.sLT == E_sLT.VA || dataLoan.sLT == E_sLT.UsdaRural)
            {
                loan.GovernmentLoan = CreateGovernmentLoan(dataLoan);
            }
            // loan.Heloc = CreateHeloc();
            loan.HmdaLoan = CreateHmdaLoan(dataLoan);
            // loan.IneligibleLoanProducts = CreateIneligibleLoanProducts();
            loan.InterestCalculation = CreateInterestCalculation();
            loan.InterestOnly = CreateInterestOnly(dataLoan);
            // loan.InvestorFeatures = CreateInvestorFeatures();
            loan.InvestorLoanInformation = CreateInvestorLoanInformation(dataLoan);
            // loan.LateCharge = CreateLateCharge();
            // loan.LoanComments = CreateLoanComments();
            loan.LoanDetail = CreateLoanDetail(dataLoan);
            // loan.LoanIdentifiers = CreateLoanIdentifiers();
            //// ir - iOPM 190789: Include /LOAN_LEVEL_CREDIT/LOAN_LEVEL_CREDIT_DETAIL/LoanLevelCreditScoreValue only if sIsLpUw is false OR sIsManualUw is true
            if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae || (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && (!dataLoan.sIsLpUw || dataLoan.sIsManualUw)))
            {
                loan.LoanLevelCredit = CreateLoanLevelCredit(dataLoan.sCreditScoreLpeQual_rep);
            }
            // loan.LoanPrograms = CreateLoanPrograms();
            loan.LoanState = CreateLoanState(dataLoan);
            // loan.LoanStatuses = CreateLoanStatuses();
            loan.Ltv = CreateLtv(dataLoan);
            loan.Maturity = CreateMaturity(dataLoan);
            // loan.MersRegistrations = CreateMersRegistrations();
            // loan.MiData = CreateMiData();
            // loan.Modifications = CreateModifications();
            // loan.MortgageScores = CreateMortgageScores();
            // loan.NegativeAmortization = CreateNegativeAmortization();
            // loan.OptionalProducts = CreateOptionalProducts();
            // loan.OriginationFunds = CreateOriginationFunds();
            // loan.OriginationSystems = CreateOriginationSystems();
            loan.Payment = CreatePayment(dataLoan);
            // loan.PrepaymentPenalty = CreatePrepaymentPenalty();
            // loan.ProposedHousingExpenses = CreateProposedHousingExpenses();
            // loan.PurchaseCredits = CreatePurchaseCredits();
            loan.Qualification = CreateQualification(dataLoan);
            loan.Refinance = CreateRefinance(dataLoan);
            // loan.Rehabilitation = CreateRehabilitation();
            // loan.Respa = CreateRespa();
            loan.SelectedLoanProduct = CreateSelectedLoanProduct(dataLoan);
            // loan.Servicing = CreateServicing();
            loan.TermsOfMortgage = CreateTermsOfMortgage(dataLoan);
            loan.Underwriting = CreateUnderwriting(dataLoan);
            // loan.SelectionType = ;
            // loan.SequenceNumber = ;

            return loan;
        }
        private LoanComment CreateLoanComment()
        {
            LoanComment loanComment = new LoanComment();

            // loanComment.LoanCommentDatetime = ;
            // loanComment.LoanCommentText = ;
            // loanComment.LoanCommentSourceDescription = ;
            // loanComment.SequenceNumber = ;

            return loanComment;
        }

        private LoanComments CreateLoanComments()
        {
            LoanComments loanComments = new LoanComments();

            // loanComments.LoanCommentList.Add(CreateLoanComment());

            return loanComments;
        }

        private LoanDelivery CreateLoanDelivery()
        {
            LoanDelivery loanDelivery = new LoanDelivery();


            return loanDelivery;
        }

        private LoanDetail CreateLoanDetail(CPageData dataLoan)
        {
            LoanDetail loanDetail = new LoanDetail();

            int borrowerCount = 0;
            //// 12/27/2014 IR - OPM 197829 - Count borrowers who are obliged by the note only.
            bool isUseNonPurchaseSpouseFeature = dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature;
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData d = dataLoan.GetAppData(i);
                if ((!isUseNonPurchaseSpouseFeature || (d.aBTypeT != E_aTypeT.NonTitleSpouse && d.aBTypeT != E_aTypeT.TitleOnly))
                    && d.aBIsValidNameSsn)
                {
                    borrowerCount++;
                }
                if ((!isUseNonPurchaseSpouseFeature || (d.aCTypeT != E_aTypeT.NonTitleSpouse && d.aCTypeT != E_aTypeT.TitleOnly))
                    && d.aCIsValidNameSsn)
                {
                    borrowerCount++;
                }
            }
            // loanDetail.AcceleratedPaymentProgramIndicator = ;
            loanDetail.ApplicationReceivedDate = dataLoan.sAppSubmittedD_rep;
            // loanDetail.ArmsLengthIndicator = ;
            if (dataLoan.sAssumeLT != E_sAssumeLT.LeaveBlank)
            {
                loanDetail.AssumabilityIndicator = dataLoan.sAssumeLT != E_sAssumeLT.MayNot;
            }
            // loanDetail.AssumedIndicator = ;
            loanDetail.BalloonIndicator = dataLoan.sTerm > dataLoan.sDue;
            // loanDetail.BalloonResetIndicator = ;
            // loanDetail.BelowMarketSubordinateFinancingIndicator = ;
            // loanDetail.BiweeklyToMonthlyOnDefaultIndicator = ;
            loanDetail.BorrowerCount = borrowerCount.ToString();
            loanDetail.BuydownTemporarySubsidyIndicator = (dataLoan.sBuydwnR1 != 0 || dataLoan.sBuydwnMon1 != 0 ||
                dataLoan.sBuydwnR2 != 0 || dataLoan.sBuydwnMon2 != 0 ||
                dataLoan.sBuydwnR3 != 0 || dataLoan.sBuydwnMon3 != 0 ||
                dataLoan.sBuydwnR4 != 0 || dataLoan.sBuydwnMon4 != 0 ||
                dataLoan.sBuydwnR5 != 0 || dataLoan.sBuydwnMon5 != 0);

            // loanDetail.CapitalizedFeesIndicator = ;
            loanDetail.CapitalizedLoanIndicator = dataLoan.sIsOptionArm;
            // loanDetail.ClosingCostFinancedIndicator = ;
            // loanDetail.CollateralPledgedToName = ;
            // loanDetail.ConcurrentOriginationIndicator = ;
            // loanDetail.ConcurrentOriginationLenderIndicator = ;
            // loanDetail.ConformingIndicator = ;
            loanDetail.ConstructionLoanIndicator = false;
            loanDetail.ConvertibleIndicator = false;
            // loanDetail.CurrentInterestRatePercent = ;
            // loanDetail.DeferredInterestBalanceAmount = ;
            // loanDetail.DemandFeatureIndicator = ;
            // loanDetail.EligibleForLenderPaidMIIndicator = ;
            // loanDetail.EmployeeLoanProgramIndicator = ;
            // loanDetail.ENoteCertifiedIndicator = ;

            if ((dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae && (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
                || (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2))
            {
                loanDetail.ENoteIndicator = dataLoan.sHasENote;
            }

            loanDetail.EscrowIndicator = dataLoan.sWillEscrowBeWaived == false;
            // loanDetail.EscrowWaiverIndicator = ;
            // loanDetail.HELOCIndicator = ;
            // loanDetail.HomeBuyersHomeownershipEducationCertificateIndicator = ;
            loanDetail.InitialFixedPeriodEffectiveMonthsCount = dataLoan.sRAdj1stCapMon_rep;
            // loanDetail.InterestCreditedToBorrowerIndicator = ;
            loanDetail.InterestOnlyIndicator = dataLoan.sIsIOnly;
            // loanDetail.LenderSelfInsuredIndicator = ;
            // loanDetail.LienHolderSameAsSubjectLoanIndicator = ;
            loanDetail.LoanAffordableIndicator = dataLoan.sIsCommunityLending || dataLoan.sIsCommLen;
            // loanDetail.LoanClosingStatusType = ;
            // loanDetail.LoanFundingDate = ;
            // loanDetail.LoanRepaymentType = ;
            // loanDetail.LoanRepaymentTypeOtherDescription = ;
            // loanDetail.LoanSaleFundingDate = ;
            // loanDetail.LoanSellerProvidedInvestmentRatingDescription = ;
            // loanDetail.MIRequiredIndicator = ;
            // loanDetail.MortgageModificationIndicator = ;
            // loanDetail.NegativeAmortizationIndicator = ;
            // loanDetail.OverdueInterestAmount = ;
            // loanDetail.PiggybackLoanIndicator = ;
            if (dataLoan.sPrepmtPenaltyT != E_sPrepmtPenaltyT.LeaveBlank)
            {
                loanDetail.PrepaymentPenaltyIndicator = dataLoan.sPrepmtPenaltyT == E_sPrepmtPenaltyT.May;
            }
            // loanDetail.PrepaymentPenaltyWaivedIndicator = ;
            // loanDetail.PrepaymentRestrictionIndicator = ;
            // loanDetail.PropertiesFinancedByLenderCount = ;
            // loanDetail.PropertyInspectionWaiverIndicator = ;
            // loanDetail.RecoverableCorporateAdvanceBalanceFromBorrowerAmount = ;
            // loanDetail.RecoverableCorporateAdvanceBalanceFromThirdPartyAmount = ;
            // loanDetail.RefundOfOverpaidInterestCalendarYearAmount = ;
            loanDetail.RelocationLoanIndicator = false;
            // loanDetail.RemainingUnearnedInterestAmount = ;
            // loanDetail.SalesConcessionAmount = ;
            // loanDetail.ServicingTransferProhibitedIndicator = ;
            // loanDetail.ServicingTransferStatusType = ;
            loanDetail.SharedEquityIndicator = false;
            // loanDetail.SignedAuthorizationToRequestTaxRecordsIndicator = ;
            // loanDetail.TaxRecordsObtainedIndicator = ;
            // loanDetail.TimelyPaymentRateReductionIndicator = ;
            loanDetail.TotalMortgagedPropertiesCount = dataLoan.sNumFinancedProperties_rep;
            if (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part1 || Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2)
            {
                loanDetail.WarehouseLenderIndicator = dataLoan.sWarehouseLenderRolodexId != -1;
            }

            return loanDetail;
        }

        private LoanIdentifier CreateUniversalLoanIdentifier(CPageData dataLoan)
        {
            LoanIdentifier loanIdentifier = new LoanIdentifier();
            loanIdentifier.Extension.OtherLoanIdentifier = dataLoan.sUniversalLoanIdentifier;
            loanIdentifier.Extension.OtherLoanIdentifierType = "UniversalLoan";

            return loanIdentifier;
        }

        private LoanLevelCredit CreateLoanLevelCredit(string sCreditScoreLpeQual_rep)
        {
            LoanLevelCredit loanLevelCredit = new LoanLevelCredit();

            // loanLevelCredit.CreditScoreProvider = CreateCreditScoreProvider();
            loanLevelCredit.LoanLevelCreditDetail = CreateLoanLevelCreditDetail(sCreditScoreLpeQual_rep);

            return loanLevelCredit;
        }

        private LoanLevelCreditDetail CreateLoanLevelCreditDetail(string sCreditScoreLpeQual_rep)
        {
            LoanLevelCreditDetail loanLevelCreditDetail = new LoanLevelCreditDetail();

            // loanLevelCreditDetail.CreditReferenceType = ;
            // loanLevelCreditDetail.CreditReferenceTypeOtherDescription = ;
            // loanLevelCreditDetail.CreditScoreImpairmentType = ;
            // loanLevelCreditDetail.CreditScoreImpairmentTypeOtherDescription = ;
            // loanLevelCreditDetail.CreditScoreModelNameType = ;
            // loanLevelCreditDetail.CreditScoreModelNameTypeOtherDescription = ;
            // loanLevelCreditDetail.LoanCreditHistoryAgeType = ;
            // loanLevelCreditDetail.LoanCreditHistoryAgeTypeOtherDescription = ;
            loanLevelCreditDetail.LoanLevelCreditScoreSelectionMethodType = LoanLevelCreditScoreSelectionMethodType.MiddleOrLowerThenLowest;
            // loanLevelCreditDetail.LoanLevelCreditScoreSelectionMethodTypeOtherDescription = ;
            loanLevelCreditDetail.LoanLevelCreditScoreValue = sCreditScoreLpeQual_rep;
            // loanLevelCreditDetail.RiskUpgradeIndicator = ;

            return loanLevelCreditDetail;
        }

        private LoanOriginator CreateLoanOriginator()
        {
            LoanOriginator loanOriginator = new LoanOriginator();

            // loanOriginator.LoanOriginatorType = ;
            // loanOriginator.LoanOriginatorTypeOtherDescription = ;

            return loanOriginator;
        }

        private LoanPriceLineItem CreateLoanPriceLineItem()
        {
            LoanPriceLineItem loanPriceLineItem = new LoanPriceLineItem();

            // loanPriceLineItem.LoanPriceLineItemAmount = ;
            // loanPriceLineItem.LoanPriceLineItemCategoryDescription = ;
            // loanPriceLineItem.LoanPriceLineItemPercent = ;
            // loanPriceLineItem.LoanPriceLineItemType = ;
            // loanPriceLineItem.LoanPriceLineItemTypeOtherDescription = ;
            // loanPriceLineItem.SequenceNumber = ;

            return loanPriceLineItem;
        }

        private LoanPriceLineItems CreateLoanPriceLineItems()
        {
            LoanPriceLineItems loanPriceLineItems = new LoanPriceLineItems();

            // loanPriceLineItems.LoanPriceLineItemList.Add(CreateLoanPriceLineItem());

            return loanPriceLineItems;
        }

        private LoanPriceQuote CreateLoanPriceQuote()
        {
            LoanPriceQuote loanPriceQuote = new LoanPriceQuote();

            // loanPriceQuote.LoanPriceQuoteDetail = CreateLoanPriceQuoteDetail();
            // loanPriceQuote.LoanPriceLineItems = CreateLoanPriceLineItems();
            // loanPriceQuote.SequenceNumber = ;

            return loanPriceQuote;
        }

        private LoanPriceQuotes CreateLoanPriceQuotes()
        {
            LoanPriceQuotes loanPriceQuotes = new LoanPriceQuotes();

            // loanPriceQuotes.LoanPriceQuoteList.Add(CreateLoanPriceQuote());

            return loanPriceQuotes;
        }

        private LoanPriceQuoteDetail CreateLoanPriceQuoteDetail()
        {
            LoanPriceQuoteDetail loanPriceQuoteDetail = new LoanPriceQuoteDetail();

            // loanPriceQuoteDetail.ExcessInvestorServiceFeeRatePercent = ;
            // loanPriceQuoteDetail.FinancingStructureDescription = ;
            // loanPriceQuoteDetail.LoanAllInPricePercent = ;
            // loanPriceQuoteDetail.LoanBasePricePercent = ;
            // loanPriceQuoteDetail.LoanPriceQuoteDatetime = ;
            // loanPriceQuoteDetail.LoanPriceQuoteExpirationDatetime = ;
            // loanPriceQuoteDetail.LoanPriceQuoteIdentifier = ;
            // loanPriceQuoteDetail.LoanPriceQuoteInterestRatePercent = ;
            // loanPriceQuoteDetail.LoanPriceQuoteType = ;
            // loanPriceQuoteDetail.LoanPriceQuoteTypeOtherDescription = ;
            // loanPriceQuoteDetail.LoanPricePercentFormatType = ;
            // loanPriceQuoteDetail.RateLockType = ;
            // loanPriceQuoteDetail.ServicingReleasePremiumPercent = ;

            return loanPriceQuoteDetail;
        }

        private SelectedLoanProduct CreateSelectedLoanProduct(CPageData dataLoan)
        {
            SelectedLoanProduct selectedLoanProduct = new SelectedLoanProduct();
            selectedLoanProduct.PriceLocks = CreatePriceLocks(dataLoan);

            return selectedLoanProduct;
        }

        private LoanProduct CreateLoanProduct()
        {
            LoanProduct loanProduct = new LoanProduct();

            // loanProduct.LoanPriceQuotes = CreateLoanPriceQuotes();
            // loanProduct.DisqualificationReasons = CreateDisqualificationReasons();
            // loanProduct.LoanProductDetail = CreateLoanProductDetail();
            // loanProduct.PriceLocks = CreatePriceLocks();
            // loanProduct.ProductCategory = CreateProductCategory();
            // loanProduct.ProductComponents = CreateProductComponents();
            // loanProduct.SequenceNumber = ;

            return loanProduct;
        }

        private LoanProductDetail CreateLoanProductDetail()
        {
            LoanProductDetail loanProductDetail = new LoanProductDetail();

            // loanProductDetail.FNMHomeImprovementProductType = ;
            // loanProductDetail.FNMProductPlanIdentifier = ;
            // loanProductDetail.LoanProductStatusType = ;
            // loanProductDetail.NegotiatedRequirementDescription = ;
            // loanProductDetail.ProductDescription = ;
            // loanProductDetail.ProductIdentifier = ;
            // loanProductDetail.ProductName = ;
            // loanProductDetail.ProductProviderName = ;
            // loanProductDetail.ProductProviderType = ;
            // loanProductDetail.ProductProviderTypeOtherDescription = ;
            // loanProductDetail.RefinanceProgramIdentifier = ;

            return loanProductDetail;
        }

        private LoanProgram CreateLoanProgram()
        {
            LoanProgram loanProgram = new LoanProgram();

            // loanProgram.LoanProgramIdentifier = ;
            // loanProgram.LoanProgramSponsoringEntityName = ;
            // loanProgram.SequenceNumber = ;

            return loanProgram;
        }

        private global::ULDD.LoanPrograms CreateLoanPrograms()
        {

            global::ULDD.LoanPrograms loanPrograms = new global::ULDD.LoanPrograms();

            // loanPrograms.LoanProgramList.Add(CreateLoanProgram());

            return loanPrograms;
        }

        private LoanState CreateLoanState(CPageData dataLoan)
        {
            LoanState loanState = new LoanState();

            loanState.LoanStateDate = dataLoan.sDocumentNoteD_rep;
            loanState.LoanStateType = LoanStateType.AtClosing;

            return loanState;
        }

        private LoanStatus CreateLoanStatus()
        {
            LoanStatus loanStatus = new LoanStatus();

            // loanStatus.LoanStatusDate = ;
            // loanStatus.LoanStatusIdentifier = ;
            // loanStatus.SequenceNumber = ;

            return loanStatus;
        }

        private LoanStatuses CreateLoanStatuses()
        {
            LoanStatuses loanStatuses = new LoanStatuses();

            // loanStatuses.LoanStatusList.Add(CreateLoanStatus());

            return loanStatuses;
        }

        private LocationIdentifier CreateLocationIdentifier()
        {
            LocationIdentifier locationIdentifier = new LocationIdentifier();

            // locationIdentifier.CensusInformation = CreateCensusInformation();
            // locationIdentifier.FipsInformation = CreateFipsInformation();
            // locationIdentifier.GeneralIdentifier = CreateGeneralIdentifier();
            // locationIdentifier.GeospatialInformation = CreateGeospatialInformation();

            return locationIdentifier;
        }

        private LossPayee CreateLossPayee()
        {
            LossPayee lossPayee = new LossPayee();

            // lossPayee.LossPayeeType = ;
            // lossPayee.LossPayeeTypeOtherDescription = ;

            return lossPayee;
        }

        private Ltv CreateLtv(CPageData dataLoan)
        {
            if (dataLoan.sHouseVal == 0)
            {
                return null;
            }
            Ltv ltv = new Ltv();

            ltv.BaseLTVRatioPercent = RoundLTVRatioPercent(dataLoan.sLAmt1003 / dataLoan.sHouseVal * 100);
            ltv.LTVRatioPercent = RoundLTVRatioPercent(dataLoan.sFinalLAmt / dataLoan.sHouseVal * 100);
            // ltv.LTVCalculationDate = ;

            return ltv;
        }

        private ManagementAgent CreateManagementAgent()
        {
            ManagementAgent managementAgent = new ManagementAgent();

            // managementAgent.Individual = CreateIndividual();
            // managementAgent.LegalEntity = CreateLegalEntity();

            return managementAgent;
        }

        private ManufacturedHome CreateManufacturedHome(E_sGseSpT sGseSpT)
        {
            ManufacturedHome manufacturedHome = new ManufacturedHome();

            manufacturedHome.ManufacturedHomeDetail = CreateManufacturedHomeDetail(sGseSpT);
            // manufacturedHome.ManufacturedHomeSections = CreateManufacturedHomeSections();

            return manufacturedHome;
        }

        private ManufacturedHomeDetail CreateManufacturedHomeDetail(E_sGseSpT sGseSpT)
        {
            ManufacturedHomeDetail manufacturedHomeDetail = new ManufacturedHomeDetail();

            // manufacturedHomeDetail.LengthFeetNumber = ;
            // manufacturedHomeDetail.ManufacturedHomeAttachedToFoundationDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeAttachedToFoundationIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeConditionDescriptionType = ;
            // manufacturedHomeDetail.ManufacturedHomeConnectedToUtilitiesIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeConnectionToUtilitiesDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeDeckDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeHUDCertificationLabelAttachedIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeHUDCertificationLabelDataSourceDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeHUDDataPlateAttachedIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeHUDDataPlateIdentifier = ;
            // manufacturedHomeDetail.ManufacturedHomeHUDDataPlateDataSourceDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeInstalledDate = ;
            // manufacturedHomeDetail.ManufacturedHomeInstallerName = ;
            // manufacturedHomeDetail.ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeManufacturerName = ;
            // manufacturedHomeDetail.ManufacturedHomeManufactureYear = ;
            // manufacturedHomeDetail.ManufacturedHomeMeetsHUDRequirementsForLocationDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeMeetsHUDRequirementsForLocationIndicator = ;
            // manufacturedHomeDetail.ManufacturedHomeModelIdentifier = ;
            // manufacturedHomeDetail.ManufacturedHomeModelYear = ;
            // manufacturedHomeDetail.ManufacturedHomeModificationsDescription = ;
            // manufacturedHomeDetail.ManufacturedHomePorchDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeSectionCount = ;
            // manufacturedHomeDetail.ManufacturedHomeSerialNumberIdentifier = ;
            // manufacturedHomeDetail.ManufacturedHomeTipOutDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeWaniganDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeWheelsDescription = ;
            // manufacturedHomeDetail.ManufacturedHomeWheelsRemovedIndicator = ;
            if (sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide)
            {
                manufacturedHomeDetail.ManufacturedHomeWidthType = ManufacturedHomeWidthType.SingleWide;
            }
            else if (sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide)
            {
                manufacturedHomeDetail.ManufacturedHomeWidthType = ManufacturedHomeWidthType.MultiWide;
            }
            // manufacturedHomeDetail.MobileHomeParkIndicator = ;
            // manufacturedHomeDetail.SquareFeetNumber = ;
            // manufacturedHomeDetail.WidthFeetNumber = ;

            return manufacturedHomeDetail;
        }

        private ManufacturedHomeSection CreateManufacturedHomeSection()
        {
            ManufacturedHomeSection manufacturedHomeSection = new ManufacturedHomeSection();

            // manufacturedHomeSection.DepthFeetNumber = ;
            // manufacturedHomeSection.ManufacturedHomeHUDCertificationLabelIdentifier = ;
            // manufacturedHomeSection.ManufacturedHomeSectionType = ;
            // manufacturedHomeSection.ManufacturedHomeSectionTypeOtherDescription = ;
            // manufacturedHomeSection.SquareFeetNumber = ;
            // manufacturedHomeSection.WidthFeetNumber = ;
            // manufacturedHomeSection.SequenceNumber = ;

            return manufacturedHomeSection;
        }

        private ManufacturedHomeSections CreateManufacturedHomeSections()
        {
            ManufacturedHomeSections manufacturedHomeSections = new ManufacturedHomeSections();

            // manufacturedHomeSections.ManufacturedHomeSectionList.Add(CreateManufacturedHomeSection());

            return manufacturedHomeSections;
        }

        private Map CreateMap()
        {
            Map map = new Map();

            // map.Template = CreateTemplate();
            // map.Transforms = CreateTransforms();

            return map;
        }

        private Maturity CreateMaturity(CPageData dataLoan)
        {
            Maturity maturity = new Maturity();

            // maturity.MaturityOccurrences = CreateMaturityOccurrences();
            maturity.MaturityRule = CreateMaturityRule(dataLoan);

            return maturity;
        }

        private MaturityOccurrence CreateMaturityOccurrence()
        {
            MaturityOccurrence maturityOccurrence = new MaturityOccurrence();

            // maturityOccurrence.LoanRemainingMaturityTermMonthsCount = ;
            // maturityOccurrence.LoanTermExtensionMonthsCount = ;
            // maturityOccurrence.SequenceNumber = ;

            return maturityOccurrence;
        }

        private MaturityOccurrences CreateMaturityOccurrences()
        {
            MaturityOccurrences maturityOccurrences = new MaturityOccurrences();

            // maturityOccurrences.MaturityOccurrenceList.Add(CreateMaturityOccurrence());

            return maturityOccurrences;
        }

        private MaturityRule CreateMaturityRule(CPageData dataLoan)
        {
            MaturityRule maturityRule = new MaturityRule();

            // maturityRule.BiweeklyComparableMonthlyMaturityDate = ;
            maturityRule.LoanMaturityDate = dataLoan.sLoanMaturityD_rep;
            maturityRule.LoanMaturityPeriodCount = dataLoan.sDue_rep;
            maturityRule.LoanMaturityPeriodType = LoanMaturityPeriodType.Month;
            // maturityRule.MaximumMaturityTermExtensionCount = ;

            return maturityRule;
        }

        private MersRegistration CreateMersRegistration()
        {
            MersRegistration mersRegistration = new MersRegistration();

            // mersRegistration.MERS_IRegistrationIndicator = ;
            // mersRegistration.MERSOriginalMortgageeOfRecordIndicator = ;
            // mersRegistration.MERSRegistrationDate = ;
            // mersRegistration.MERSRegistrationStatusType = ;
            // mersRegistration.MERSRegistrationStatusTypeOtherDescription = ;
            // mersRegistration.MERSRegistrationType = ;
            // mersRegistration.MERSRegistrationTypeOtherDescription = ;
            // mersRegistration.SequenceNumber = ;

            return mersRegistration;
        }

        private MersRegistrations CreateMersRegistrations()
        {
            MersRegistrations mersRegistrations = new MersRegistrations();

            // mersRegistrations.MersRegistrationList.Add(CreateMersRegistration());

            return mersRegistrations;
        }

        private MersService CreateMersService()
        {
            MersService mersService = new MersService();


            return mersService;
        }

        private Message CreateMessage()
        {
            Message message = new Message();

            message.MISMOReferenceModelIdentifier = MismoReferenceModelIdentifier;

            message.AboutVersions = CreateAboutVersions();
            message.DealSets = CreateDealSets();
            // message.DocumentSets = CreateDocumentSets();
            // message.Relationships = CreateRelationships();
            // message.SystemSignatures = CreateSystemSignatures();

            return message;
        }

        private Mi CreateMi()
        {
            Mi mi = new Mi();


            return mi;
        }

        private MiData CreateMiData()
        {
            MiData miData = new MiData();

            // miData.MiDataDetail = CreateMiDataDetail();
            // miData.MiPoolInsurance = CreateMiPoolInsurance();
            // miData.MiPremiumTaxes = CreateMiPremiumTaxes();
            // miData.MiRenewalPremiums = CreateMiRenewalPremiums();

            return miData;
        }

        private MiDataDetail CreateMiDataDetail()
        {
            MiDataDetail miDataDetail = new MiDataDetail();

            // miDataDetail.BorrowerMITerminationDate = ;
            // miDataDetail.LenderPaidMIInterestRateAdjustmentPercent = ;
            // miDataDetail.MI_FHAAnniversaryDate = ;
            // miDataDetail.MI_FHAEndorsementDate = ;
            // miDataDetail.MI_FHAPendingPremiumChangeDate = ;
            // miDataDetail.MI_FHAPendingPremiumAmount = ;
            // miDataDetail.MI_FHAPremiumAnniversaryYearToDateRemittanceAmount = ;
            // miDataDetail.MI_FHAProgramType = ;
            // miDataDetail.MI_FHAUpfrontPremiumAmount = ;
            // miDataDetail.MI_FHAUpfrontPremiumPercent = ;
            // miDataDetail.MI_LTVCutoffPercent = ;
            // miDataDetail.MI_LTVCutoffType = ;
            // miDataDetail.MICancellationDate = ;
            // miDataDetail.MICertificateIdentifier = ;
            // miDataDetail.MICertificationStatusType = ;
            // miDataDetail.MICertificationStatusTypeOtherDescription = ;
            // miDataDetail.MICollectedNumberOfMonthsCount = ;
            // miDataDetail.MICompanyNameType = ;
            // miDataDetail.MICompanyNameTypeOtherDescription = ;
            // miDataDetail.MIConventionalUpfrontPremiumPercent = ;
            // miDataDetail.MICoverageEffectiveDate = ;
            // miDataDetail.MICoveragePercent = ;
            // miDataDetail.MICoveragePlanType = ;
            // miDataDetail.MICoveragePlanTypeOtherDescription = ;
            // miDataDetail.MICurrentAnnualPremiumAmount = ;
            // miDataDetail.MICushionNumberOfMonthsCount = ;
            // miDataDetail.MIDurationType = ;
            // miDataDetail.MIDurationTypeOtherDescription = ;
            // miDataDetail.MIEscrowIncludedInAggregateIndicator = ;
            // miDataDetail.MIEscrowedIndicator = ;
            // miDataDetail.MIInitialPremiumAmount = ;
            // miDataDetail.MIInitialPremiumAtClosingType = ;
            // miDataDetail.MIInitialPremiumRateDurationMonthsCount = ;
            // miDataDetail.MIInitialPremiumRatePercent = ;
            // miDataDetail.MIPremiumCalculationRatePercent = ;
            // miDataDetail.MIPremiumCalculationType = ;
            // miDataDetail.MIPremiumCalendarYearAmount = ;
            // miDataDetail.MIPremiumFinancedAmount = ;
            // miDataDetail.MIPremiumFinancedIndicator = ;
            // miDataDetail.MIPremiumFromClosingAmount = ;
            // miDataDetail.MIPremiumPaymentType = ;
            // miDataDetail.MIPremiumPaymentTypeOtherDescription = ;
            // miDataDetail.MIPremiumRefundableType = ;
            // miDataDetail.MIPremiumSourceType = ;
            // miDataDetail.MIPremiumSourceTypeOtherDescription = ;
            // miDataDetail.MIPremiumTermMonthsCount = ;
            // miDataDetail.MIRenewalCalculationType = ;
            // miDataDetail.MIRenewalPremiumRatePercent = ;
            // miDataDetail.MIScheduledTerminationDate = ;
            // miDataDetail.MISourceType = ;
            // miDataDetail.PrimaryMIAbsenceReasonType = ;
            // miDataDetail.PrimaryMIAbsenceReasonTypeOtherDescription = ;
            // miDataDetail.ScheduledAmortizationMidpointDate = ;
            // miDataDetail.SellerMIPaidToDate = ;

            return miDataDetail;
        }

        private MiPoolInsurance CreateMiPoolInsurance()
        {
            MiPoolInsurance miPoolInsurance = new MiPoolInsurance();

            // miPoolInsurance.MIPoolInsuranceCertificateIdentifier = ;
            // miPoolInsurance.MIPoolInsuranceMasterCommitmentIdentifier = ;
            // miPoolInsurance.MICompanyNameType = ;
            // miPoolInsurance.MICompanyNameTypeOtherDescription = ;

            return miPoolInsurance;
        }

        private MiPremiumTax CreateMiPremiumTax()
        {
            MiPremiumTax miPremiumTax = new MiPremiumTax();

            // miPremiumTax.MIPremiumTaxCodeAmount = ;
            // miPremiumTax.MIPremiumTaxCodePercent = ;
            // miPremiumTax.MIPremiumTaxCodeType = ;
            // miPremiumTax.MIPremiumTaxingAuthorityName = ;
            // miPremiumTax.SequenceNumber = ;

            return miPremiumTax;
        }

        private MiPremiumTaxes CreateMiPremiumTaxes()
        {
            MiPremiumTaxes miPremiumTaxes = new MiPremiumTaxes();

            // miPremiumTaxes.MiPremiumTaxList.Add(CreateMiPremiumTax());

            return miPremiumTaxes;
        }

        private MiRenewalPremium CreateMiRenewalPremium()
        {
            MiRenewalPremium miRenewalPremium = new MiRenewalPremium();

            // miRenewalPremium.MIRenewalPremiumMonthlyPaymentAmount = ;
            // miRenewalPremium.MIRenewalPremiumMonthlyPaymentRoundingType = ;
            // miRenewalPremium.MIRenewalPremiumRateDurationMonthsCount = ;
            // miRenewalPremium.MIRenewalPremiumRatePercent = ;
            // miRenewalPremium.MIRenewalPremiumSequenceType = ;
            // miRenewalPremium.SequenceNumber = ;

            return miRenewalPremium;
        }

        private MiRenewalPremiums CreateMiRenewalPremiums()
        {
            MiRenewalPremiums miRenewalPremiums = new MiRenewalPremiums();

            // miRenewalPremiums.MiRenewalPremiumList.Add(CreateMiRenewalPremium());

            return miRenewalPremiums;
        }

        private Modification CreateModification()
        {
            Modification modification = new Modification();

            // modification.LoanModificationEffectiveDate = ;
            // modification.LoanModificationEventType = ;
            // modification.LoanModificationEventTypeOtherDescription = ;
            // modification.PreModificationInterestRatePercent = ;
            // modification.PreModificationPrincipalAndInterestPaymentAmount = ;
            // modification.PreModificationUPBAmount = ;
            // modification.SequenceNumber = ;

            return modification;
        }

        private Modifications CreateModifications()
        {
            Modifications modifications = new Modifications();

            // modifications.ModificationList.Add(CreateModification());

            return modifications;
        }

        private MonetaryEvent CreateMonetaryEvent()
        {
            MonetaryEvent monetaryEvent = new MonetaryEvent();

            // monetaryEvent.InvestorReportingAdditionalCharges = CreateInvestorReportingAdditionalCharges();
            // monetaryEvent.MonetaryEventDetail = CreateMonetaryEventDetail();
            // monetaryEvent.StatusChangeEvents = CreateStatusChangeEvents();
            // monetaryEvent.SequenceNumber = ;

            return monetaryEvent;
        }

        private MonetaryEvents CreateMonetaryEvents()
        {
            MonetaryEvents monetaryEvents = new MonetaryEvents();

            // monetaryEvents.MonetaryEventList.Add(CreateMonetaryEvent());

            return monetaryEvents;
        }

        private MonetaryEventDetail CreateMonetaryEventDetail()
        {
            MonetaryEventDetail monetaryEventDetail = new MonetaryEventDetail();

            // monetaryEventDetail.LoanPartialPrepaymentAmount = ;
            // monetaryEventDetail.MonetaryEventAppliedDate = ;
            // monetaryEventDetail.MonetaryEventBorrowerIncentiveCurtailmentAmount = ;
            // monetaryEventDetail.MonetaryEventDeferredPrincipalCurtailmentAmount = ;
            // monetaryEventDetail.MonetaryEventDeferredUPBAmount = ;
            // monetaryEventDetail.MonetaryEventDueDate = ;
            // monetaryEventDetail.MonetaryEventEscrowPaymentAmount = ;
            // monetaryEventDetail.MonetaryEventGrossInterestAmount = ;
            // monetaryEventDetail.MonetaryEventGrossPrincipalAmount = ;
            // monetaryEventDetail.MonetaryEventInterestBearingUPBAmount = ;
            // monetaryEventDetail.MonetaryEventInterestPaidThroughDate = ;
            // monetaryEventDetail.MonetaryEventInvestorRemittanceAmount = ;
            // monetaryEventDetail.MonetaryEventInvestorRemittanceEffectiveDate = ;
            // monetaryEventDetail.MonetaryEventNetInterestAmount = ;
            // monetaryEventDetail.MonetaryEventNetPrincipalAmount = ;
            // monetaryEventDetail.MonetaryEventOptionalProductsPaymentAmount = ;
            // monetaryEventDetail.MonetaryEventReversalIndicator = ;
            // monetaryEventDetail.MonetaryEventScheduledUPBAmount = ;
            // monetaryEventDetail.MonetaryEventType = ;
            // monetaryEventDetail.MonetaryEventTypeOtherDescription = ;
            // monetaryEventDetail.MonetaryEventUPBAmount = ;

            return monetaryEventDetail;
        }

        private MonetaryEventSummaries CreateMonetaryEventSummaries()
        {
            MonetaryEventSummaries monetaryEventSummaries = new MonetaryEventSummaries();

            // monetaryEventSummaries.MonetaryEventSummaryList.Add(CreateMonetaryEventSummary());

            return monetaryEventSummaries;
        }

        private MonetaryEventSummary CreateMonetaryEventSummary()
        {
            MonetaryEventSummary monetaryEventSummary = new MonetaryEventSummary();

            // monetaryEventSummary.InvestorReportingAdditionalCharges = CreateInvestorReportingAdditionalCharges();
            // monetaryEventSummary.MonetaryEventSummaryDetail = CreateMonetaryEventSummaryDetail();
            // monetaryEventSummary.StatusChangeEvents = CreateStatusChangeEvents();
            // monetaryEventSummary.SequenceNumber = ;

            return monetaryEventSummary;
        }

        private MonetaryEventSummaryDetail CreateMonetaryEventSummaryDetail()
        {
            MonetaryEventSummaryDetail monetaryEventSummaryDetail = new MonetaryEventSummaryDetail();

            // monetaryEventSummaryDetail.SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventDeferredPrincipalCurtailmentAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventDeferredUPBAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventEscrowPaymentAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventGrossInterestAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventGrossPrincipalAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventInterestBearingUPBAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventInterestPaidThroughDate = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventInvestorSpecialHandlingDescription = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventLastPaidInstallmentAppliedDate = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventLastPaidInstallmentDueDate = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventNetInterestAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventNetPrincipalAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventOptionalProductsPaymentAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventScheduledUPBAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryEventUPBAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryInvestorRemittanceAmount = ;
            // monetaryEventSummaryDetail.SummaryMonetaryInvestorRemittanceEffectiveDate = ;
            // monetaryEventSummaryDetail.SuspenseBalanceAmount = ;
            // monetaryEventSummaryDetail.UnremittedOptionalProductsPremiumBalanceAmount = ;

            return monetaryEventSummaryDetail;
        }

        private MortgageBroker CreateMortgageBroker()
        {
            MortgageBroker mortgageBroker = new MortgageBroker();

            // mortgageBroker.MortgageBrokerLicenseNumberIdentifier = ;

            return mortgageBroker;
        }

        private MortgageScore CreateMortgageScore()
        {
            MortgageScore mortgageScore = new MortgageScore();

            // mortgageScore.MortgageScoreDate = ;
            // mortgageScore.MortgageScoreType = ;
            // mortgageScore.MortgageScoreTypeOtherDescription = ;
            // mortgageScore.MortgageScoreValue = ;
            // mortgageScore.SequenceNumber = ;

            return mortgageScore;
        }

        private MortgageScores CreateMortgageScores()
        {
            MortgageScores mortgageScores = new MortgageScores();

            // mortgageScores.MortgageScoreList.Add(CreateMortgageScore());

            return mortgageScores;
        }

        private Name CreateName()
        {
            Name name = new Name();

            // name.EducationalAchievementsDescription = ;
            // name.FirstName = ;
            // name.FullName = ;
            // name.FunctionalTitleDescription = ;
            // name.IndividualTitleDescription = ;
            // name.LastName = ;
            // name.MiddleName = ;
            // name.PrefixName = ;
            // name.SuffixName = ;

            return name;
        }

        private NearestLivingRelative CreateNearestLivingRelative()
        {
            NearestLivingRelative nearestLivingRelative = new NearestLivingRelative();

            // nearestLivingRelative.Address = CreateAddress();
            // nearestLivingRelative.ContactPoints = CreateContactPoints();
            // nearestLivingRelative.Name = CreateName();
            // nearestLivingRelative.NearestLivingRelativeDetail = CreateNearestLivingRelativeDetail();

            return nearestLivingRelative;
        }

        private NearestLivingRelativeDetail CreateNearestLivingRelativeDetail()
        {
            NearestLivingRelativeDetail nearestLivingRelativeDetail = new NearestLivingRelativeDetail();

            // nearestLivingRelativeDetail.BorrowerNearestLivingRelativeRelationshipDescription = ;

            return nearestLivingRelativeDetail;
        }

        private NegativeAmortization CreateNegativeAmortization()
        {
            NegativeAmortization negativeAmortization = new NegativeAmortization();

            // negativeAmortization.NegativeAmortizationOccurrences = CreateNegativeAmortizationOccurrences();
            // negativeAmortization.NegativeAmortizationRule = CreateNegativeAmortizationRule();

            return negativeAmortization;
        }

        private NegativeAmortizationOccurrence CreateNegativeAmortizationOccurrence()
        {
            NegativeAmortizationOccurrence negativeAmortizationOccurrence = new NegativeAmortizationOccurrence();

            // negativeAmortizationOccurrence.CurrentNegativeAmortizationBalanceAmount = ;
            // negativeAmortizationOccurrence.SequenceNumber = ;

            return negativeAmortizationOccurrence;
        }

        private NegativeAmortizationOccurrences CreateNegativeAmortizationOccurrences()
        {
            NegativeAmortizationOccurrences negativeAmortizationOccurrences = new NegativeAmortizationOccurrences();

            // negativeAmortizationOccurrences.NegativeAmortizationOccurrenceList.Add(CreateNegativeAmortizationOccurrence());

            return negativeAmortizationOccurrences;
        }

        private NegativeAmortizationRule CreateNegativeAmortizationRule()
        {
            NegativeAmortizationRule negativeAmortizationRule = new NegativeAmortizationRule();

            // negativeAmortizationRule.LoanNegativeAmortizationResolutionType = ;
            // negativeAmortizationRule.LoanNegativeAmortizationResolutionTypeOtherDescription = ;
            // negativeAmortizationRule.MaximumPrincipalBalanceAmount = ;
            // negativeAmortizationRule.NegativeAmortizationLimitAmount = ;
            // negativeAmortizationRule.NegativeAmortizationLimitMonthsCount = ;
            // negativeAmortizationRule.NegativeAmortizationLimitPercent = ;
            // negativeAmortizationRule.NegativeAmortizationMaximumLoanBalanceAmount = ;
            // negativeAmortizationRule.NegativeAmortizationMaximumPITIAmount = ;
            // negativeAmortizationRule.NegativeAmortizationRecastType = ;
            // negativeAmortizationRule.NegativeAmortizationType = ;

            return negativeAmortizationRule;
        }

        private Notary CreateNotary()
        {
            Notary notary = new Notary();

            // notary.NotaryCertificates = CreateNotaryCertificates();
            // notary.NotaryCommission = CreateNotaryCommission();

            return notary;
        }

        private NotaryCertificate CreateNotaryCertificate()
        {
            NotaryCertificate notaryCertificate = new NotaryCertificate();

            // notaryCertificate.NotaryCertificateDetail = CreateNotaryCertificateDetail();
            // notaryCertificate.NotaryCertificateSignerIdentification = CreateNotaryCertificateSignerIdentification();
            // notaryCertificate.SequenceNumber = ;

            return notaryCertificate;
        }

        private NotaryCertificates CreateNotaryCertificates()
        {
            NotaryCertificates notaryCertificates = new NotaryCertificates();

            // notaryCertificates.NotaryCertificateList.Add(CreateNotaryCertificate());

            return notaryCertificates;
        }

        private NotaryCertificateDetail CreateNotaryCertificateDetail()
        {
            NotaryCertificateDetail notaryCertificateDetail = new NotaryCertificateDetail();

            // notaryCertificateDetail.NotaryAppearanceDate = ;
            // notaryCertificateDetail.NotaryAppearedBeforeNamesDescription = ;
            // notaryCertificateDetail.NotaryAppearedBeforeTitlesDescription = ;
            // notaryCertificateDetail.NotaryCertificateSignerCompanyName = ;
            // notaryCertificateDetail.NotaryCertificateSignerFullName = ;
            // notaryCertificateDetail.NotaryCertificateSignerTitleDescription = ;
            // notaryCertificateDetail.NotaryCertificateSigningCountyName = ;
            // notaryCertificateDetail.NotaryCertificateSigningStateName = ;

            return notaryCertificateDetail;
        }

        private NotaryCertificateSignerIdentification CreateNotaryCertificateSignerIdentification()
        {
            NotaryCertificateSignerIdentification notaryCertificateSignerIdentification = new NotaryCertificateSignerIdentification();

            // notaryCertificateSignerIdentification.NotaryCertificateSignerIdentificationDescription = ;
            // notaryCertificateSignerIdentification.NotaryCertificateSignerIdentificationType = ;

            return notaryCertificateSignerIdentification;
        }

        private NotaryCommission CreateNotaryCommission()
        {
            NotaryCommission notaryCommission = new NotaryCommission();

            // notaryCommission.NotaryCommissionBondNumberIdentifier = ;
            // notaryCommission.NotaryCommissionCityName = ;
            // notaryCommission.NotaryCommissionCountyName = ;
            // notaryCommission.NotaryCommissionExpirationDate = ;
            // notaryCommission.NotaryCommissionNumberIdentifier = ;
            // notaryCommission.NotaryCommissionStateName = ;

            return notaryCommission;
        }

        private NotarySignatureField CreateNotarySignatureField()
        {
            NotarySignatureField notarySignatureField = new NotarySignatureField();

            // notarySignatureField.NotarialCertificateLanguageFieldReference = CreateNotarialCertificateLanguageFieldReference();
            // notarySignatureField.NotarialSealFieldReference = CreateNotarialSealFieldReference();
            // notarySignatureField.NotarySignatureFieldDetail = CreateNotarySignatureFieldDetail();
            // notarySignatureField.SignatureFieldReference = CreateSignatureFieldReference();

            return notarySignatureField;
        }

        private NotarySignatureFieldDetail CreateNotarySignatureFieldDetail()
        {
            NotarySignatureFieldDetail notarySignatureFieldDetail = new NotarySignatureFieldDetail();

            // notarySignatureFieldDetail.SignatureType = ;
            // notarySignatureFieldDetail.SignatureTypeOtherDescription = ;

            return notarySignatureFieldDetail;
        }

        private Note CreateNote()
        {
            Note note = new Note();

            // note.Addendum = CreateAddendum();
            // note.AllongeToNote = CreateAllongeToNote();

            return note;
        }

        private OptionalProduct CreateOptionalProduct()
        {
            OptionalProduct optionalProduct = new OptionalProduct();

            // optionalProduct.OptionalProductCancellationReasonType = ;
            // optionalProduct.OptionalProductCancellationReasonTypeOtherDescription = ;
            // optionalProduct.OptionalProductChangeType = ;
            // optionalProduct.OptionalProductChangeTypeOtherDescription = ;
            // optionalProduct.OptionalProductEffectiveDate = ;
            // optionalProduct.OptionalProductExpirationDate = ;
            // optionalProduct.OptionalProductPlanType = ;
            // optionalProduct.OptionalProductPlanTypeOtherDescription = ;
            // optionalProduct.OptionalProductRemittanceDueDate = ;
            // optionalProduct.OptionalProductRemittancePerYearCount = ;
            // optionalProduct.OptionalProductPayeeIdentifier = ;
            // optionalProduct.OptionalProductPaymentAmount = ;
            // optionalProduct.OptionalProductPendingChangeEffectiveDate = ;
            // optionalProduct.OptionalProductPendingPaymentAmount = ;
            // optionalProduct.OptionalProductProviderAccountIdentifier = ;
            // optionalProduct.OptionalProductProvidersPlanIdentifier = ;
            // optionalProduct.SequenceNumber = ;

            return optionalProduct;
        }

        private OptionalProducts CreateOptionalProducts()
        {
            OptionalProducts optionalProducts = new OptionalProducts();

            // optionalProducts.OptionalProductList.Add(CreateOptionalProduct());

            return optionalProducts;
        }

        private OriginatingRecordingRequest CreateOriginatingRecordingRequest()
        {
            OriginatingRecordingRequest originatingRecordingRequest = new OriginatingRecordingRequest();

            // originatingRecordingRequest.DigitalSignatureDigestValue = ;
            // originatingRecordingRequest.OriginatingRecordingRequestIdentifier = ;

            return originatingRecordingRequest;
        }

        private OriginationFund CreateOriginationFund()
        {
            OriginationFund originationFund = new OriginationFund();

            // originationFund.OriginationFundsAmount = ;
            // originationFund.OriginationFundsSourceType = ;
            // originationFund.OriginationFundsSourceTypeOtherDescription = ;
            // originationFund.OriginationFundsType = ;
            // originationFund.OriginationFundsTypeOtherDescription = ;
            // originationFund.SequenceNumber = ;

            return originationFund;
        }

        private OriginationFunds CreateOriginationFunds()
        {
            OriginationFunds originationFunds = new OriginationFunds();

            // originationFunds.OriginationFundList.Add(CreateOriginationFund());

            return originationFunds;
        }

        private OriginationSystem CreateOriginationSystem()
        {
            OriginationSystem originationSystem = new OriginationSystem();

            // originationSystem.LoanOriginationSystemLoanIdentifier = ;
            // originationSystem.LoanOriginationSystemName = ;
            // originationSystem.LoanOriginationSystemVendorIdentifier = ;
            // originationSystem.LoanOriginationSystemVersionIdentifier = ;
            // originationSystem.SequenceNumber = ;

            return originationSystem;
        }

        private OriginationSystems CreateOriginationSystems()
        {
            OriginationSystems originationSystems = new OriginationSystems();

            // originationSystems.OriginationSystemList.Add(CreateOriginationSystem());

            return originationSystems;
        }

        private OwnedProperties CreateOwnedProperties()
        {
            OwnedProperties ownedProperties = new OwnedProperties();

            // ownedProperties.OwnedPropertyList.Add(CreateOwnedProperty());

            return ownedProperties;
        }

        private OwnedProperty CreateOwnedProperty()
        {
            OwnedProperty ownedProperty = new OwnedProperty();

            // ownedProperty.Address = CreateAddress();
            // ownedProperty.OwnedPropertyDetail = CreateOwnedPropertyDetail();
            // ownedProperty.SequenceNumber = ;

            return ownedProperty;
        }

        private OwnedPropertyDetail CreateOwnedPropertyDetail()
        {
            OwnedPropertyDetail ownedPropertyDetail = new OwnedPropertyDetail();

            // ownedPropertyDetail.OwnedPropertyCurrentResidenceIndicator = ;
            // ownedPropertyDetail.OwnedPropertyDispositionStatusType = ;
            // ownedPropertyDetail.OwnedPropertyLienInstallmentAmount = ;
            // ownedPropertyDetail.OwnedPropertyLienUPBAmount = ;
            // ownedPropertyDetail.OwnedPropertyMaintenanceExpenseAmount = ;
            // ownedPropertyDetail.OwnedPropertyMarketValueAmount = ;
            // ownedPropertyDetail.OwnedPropertyOwnedUnitCount = ;
            // ownedPropertyDetail.OwnedPropertyRentalIncomeGrossAmount = ;
            // ownedPropertyDetail.OwnedPropertyRentalIncomeNetAmount = ;
            // ownedPropertyDetail.OwnedPropertySubjectIndicator = ;
            // ownedPropertyDetail.PropertyUsageType = ;

            return ownedPropertyDetail;
        }

        private ParcelIdentification CreateParcelIdentification()
        {
            ParcelIdentification parcelIdentification = new ParcelIdentification();

            // parcelIdentification.ParcelIdentificationType = ;
            // parcelIdentification.ParcelIdentificationTypeOtherDescription = ;
            // parcelIdentification.ParcelIdentifier = ;
            // parcelIdentification.SequenceNumber = ;

            return parcelIdentification;
        }

        private ParcelIdentifications CreateParcelIdentifications()
        {
            ParcelIdentifications parcelIdentifications = new ParcelIdentifications();

            // parcelIdentifications.ParcelIdentificationList.Add(CreateParcelIdentification());

            return parcelIdentifications;
        }

        private ParsedLegalDescription CreateParsedLegalDescription()
        {
            ParsedLegalDescription parsedLegalDescription = new ParsedLegalDescription();

            // parsedLegalDescription.PlattedLands = CreatePlattedLands();
            // parsedLegalDescription.UnplattedLands = CreateUnplattedLands();

            return parsedLegalDescription;
        }

        private Parties CreateParties()
        {
            Parties parties = new Parties();

            // parties.PartyList.Add(CreateParty());

            return parties;
        }

        private Party CreateParty()
        {
            Party party = new Party();

            // party.Reference = CreateReference();
            // party.Individual = CreateIndividual();
            // party.LegalEntity = CreateLegalEntity();
            // party.Addresses = CreateAddresses();
            // party.Roles = CreateRoles();
            // party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers();
            // party.SequenceNumber = ;

            return party;
        }

        private PartyRoleIdentifier CreatePartyRoleIdentifier()
        {
            PartyRoleIdentifier partyRoleIdentifier = new PartyRoleIdentifier();

            // partyRoleIdentifier._PartyRoleIdentifier = ;
            // partyRoleIdentifier.SequenceNumber = ;

            return partyRoleIdentifier;
        }

        private PartyRoleIdentifiers CreatePartyRoleIdentifiers()
        {
            PartyRoleIdentifiers partyRoleIdentifiers = new PartyRoleIdentifiers();

            // partyRoleIdentifiers.PartyRoleIdentifierList.Add(CreatePartyRoleIdentifier());

            return partyRoleIdentifiers;
        }

        private Payee CreatePayee()
        {
            Payee payee = new Payee();

            // payee.PayeeRemittanceType = ;
            // payee.PayeeRemittanceTypeOtherDescription = ;
            // payee.PayeeType = ;
            // payee.PayeeTypeOtherDescription = ;

            return payee;
        }

        private Payment CreatePayment(CPageData dataLoan)
        {
            Payment payment = new Payment();

            // payment.PaymentComponentBreakouts = CreatePaymentComponentBreakouts();
            payment.PaymentRule = CreatePaymentRule(dataLoan);
            // payment.PaymentScheduleItems = CreatePaymentScheduleItems();
            // payment.PaymentSummary = CreatePaymentSummary();
            // payment.PeriodicLateCounts = CreatePeriodicLateCounts();
            // payment.SkipPayment = CreateSkipPayment();

            return payment;
        }

        private PaymentComponentBreakout CreatePaymentComponentBreakout()
        {
            PaymentComponentBreakout paymentComponentBreakout = new PaymentComponentBreakout();

            // paymentComponentBreakout.BuydownSubsidyPaymentAmount = ;
            // paymentComponentBreakout.BuydownSubsidyPaymentEffectiveDate = ;
            // paymentComponentBreakout.HUD235SubsidyPaymentAmount = ;
            // paymentComponentBreakout.HUD235SubsidyPaymentEffectiveDate = ;
            // paymentComponentBreakout.PaymentStateType = ;
            // paymentComponentBreakout.PaymentStateTypeOtherDescription = ;
            // paymentComponentBreakout.PrincipalAndInterestPaymentAmount = ;
            // paymentComponentBreakout.PrincipalAndInterestPaymentEffectiveDate = ;
            // paymentComponentBreakout.TaxAndInsurancePaymentAmount = ;
            // paymentComponentBreakout.TaxAndInsurancePaymentEffectiveDate = ;
            // paymentComponentBreakout.TotalOptionalProductsPaymentAmount = ;
            // paymentComponentBreakout.TotalOptionalProductsPaymentEffectiveDate = ;
            // paymentComponentBreakout.TotalPaymentAmount = ;
            // paymentComponentBreakout.SequenceNumber = ;

            return paymentComponentBreakout;
        }

        private PaymentComponentBreakouts CreatePaymentComponentBreakouts()
        {
            PaymentComponentBreakouts paymentComponentBreakouts = new PaymentComponentBreakouts();

            // paymentComponentBreakouts.PaymentComponentBreakoutList.Add(CreatePaymentComponentBreakout());

            return paymentComponentBreakouts;
        }

        private PaymentRule CreatePaymentRule(CPageData dataLoan)
        {
            PaymentRule paymentRule = new PaymentRule();

            // paymentRule.CurtailmentApplicationSequenceType = ;
            // paymentRule.FinalPaymentAmount = ;
            // paymentRule.FirstPrincipalReductionDate = ;
            // paymentRule.InitialInterestOnlyPaymentAmount = ;
            // paymentRule.InitialPITIPaymentAmount = ;
            // paymentRule.InitialPaymentRatePercent = ;
            if (dataLoan.sHasTempBuydown)
            {
                if (dataLoan.BrokerDB.ExportFirstNonBoughtDownPaymentAsInitialPaymentInUldd)
                {
                    var totalBoughtDownMonths = dataLoan.sBuydwnMon1 + dataLoan.sBuydwnMon2 + dataLoan.sBuydwnMon3 + dataLoan.sBuydwnMon4 + dataLoan.sBuydwnMon5;
                    int cumulativeMonthsInRows = 0;

                    // Fall back to existing behavior if we cannot find the first non bought-down payment.
                    string firstNonBoughtDownPayment = dataLoan.sProThisMQual_rep;

                    if (totalBoughtDownMonths <= dataLoan.sTerm)
                    {
                        foreach (var row in dataLoan.sAmortTable.Items)
                        {
                            if (cumulativeMonthsInRows >= totalBoughtDownMonths)
                            {
                                firstNonBoughtDownPayment = row.Pmt_rep;
                                break;
                            }

                            cumulativeMonthsInRows += row.Count;
                        }
                    }

                    paymentRule.InitialPrincipalAndInterestPaymentAmount = firstNonBoughtDownPayment;
                }
                else
                {
                    paymentRule.InitialPrincipalAndInterestPaymentAmount = dataLoan.sProThisMQual_rep;
                }
            }
            else
            {
                paymentRule.InitialPrincipalAndInterestPaymentAmount = dataLoan.sProThisMPmt_rep;
            }
            // paymentRule.InitialTaxAndInsurancePaymentAmount = ;
            // paymentRule.LoanPaymentAccumulationAmount = ;
            // paymentRule.PaymentBillingMethodType = ;
            // paymentRule.PaymentBillingStatementFrequencyType = ;
            // paymentRule.PaymentBillingStatementLeadDaysCount = ;
            paymentRule.PaymentFrequencyType = PaymentFrequencyType.Monthly;
            // paymentRule.PaymentFrequencyTypeOtherDescription = ;
            // paymentRule.PaymentOptionIndicator = ;
            // paymentRule.PaymentRemittanceDay = ;
            // paymentRule.ScheduledAnnualPaymentCount = ;
            paymentRule.ScheduledFirstPaymentDate = dataLoan.sSchedDueD1_rep;
            // paymentRule.ScheduledTotalPaymentCount = ;
            // paymentRule.ThirdPartyInvestorFeePassThroughPercent = ;
            // paymentRule.ThirdPartyInvestorInterestPassThroughPercent = ;
            // paymentRule.ThirdPartyInvestorPrincipalPassThroughPercent = ;

            return paymentRule;
        }

        private PaymentScheduleItem CreatePaymentScheduleItem()
        {
            PaymentScheduleItem paymentScheduleItem = new PaymentScheduleItem();

            // paymentScheduleItem.PaymentSchedulePaymentAmount = ;
            // paymentScheduleItem.PaymentSchedulePaymentStartDate = ;
            // paymentScheduleItem.PaymentSchedulePaymentVaryingToAmount = ;
            // paymentScheduleItem.PaymentScheduleTotalNumberOfPaymentsCount = ;
            // paymentScheduleItem.SequenceNumber = ;

            return paymentScheduleItem;
        }

        private PaymentScheduleItems CreatePaymentScheduleItems()
        {
            PaymentScheduleItems paymentScheduleItems = new PaymentScheduleItems();

            // paymentScheduleItems.PaymentScheduleItemList.Add(CreatePaymentScheduleItem());

            return paymentScheduleItems;
        }

        private PaymentSummary CreatePaymentSummary()
        {
            PaymentSummary paymentSummary = new PaymentSummary();

            // paymentSummary.AggregateLoanCurtailmentAmount = ;
            // paymentSummary.InterestPaidThroughDate = ;
            // paymentSummary.LastPaidInstallmentDueDate = ;
            // paymentSummary.LastPaymentReceivedDate = ;
            // paymentSummary.NextPaymentDueDate = ;
            // paymentSummary.PaymentHistoryPatternText = ;
            // paymentSummary.TotalPITIMonthsCount = ;
            // paymentSummary.TotalPITIReceivedAmount = ;
            // paymentSummary.UPBAmount = ;
            // paymentSummary.YearToDatePrincipalPaidAmount = ;
            // paymentSummary.YearToDateTaxesPaidAmount = ;
            // paymentSummary.YearToDateTotalInterestPaidAmount = ;

            return paymentSummary;
        }

        private Payoff CreatePayoff()
        {
            Payoff payoff = new Payoff();

            // payoff.PayoffAccountNumberIdentifier = ;
            // payoff.PayoffActionType = ;
            // payoff.PayoffActionTypeOtherDescription = ;
            // payoff.PayoffAmount = ;
            // payoff.PayoffApplicationSequenceType = ;
            // payoff.PayoffPerDiemAmount = ;
            // payoff.PayoffPrepaymentPenaltyAmount = ;
            // payoff.PayoffSpecifiedHUDLineNumberValue = ;
            // payoff.PayoffThroughDate = ;

            return payoff;
        }

        private PeriodicLateCount CreatePeriodicLateCount()
        {
            PeriodicLateCount periodicLateCount = new PeriodicLateCount();

            // periodicLateCount.Periodic120DaysLateCount = ;
            // periodicLateCount.Periodic30DaysLateCount = ;
            // periodicLateCount.Periodic60DaysLateCount = ;
            // periodicLateCount.Periodic90DaysLateCount = ;
            // periodicLateCount.PeriodicLateCountType = ;
            // periodicLateCount.PeriodicLateCountTypeOtherDescription = ;
            // periodicLateCount.SequenceNumber = ;

            return periodicLateCount;
        }

        private PeriodicLateCounts CreatePeriodicLateCounts()
        {
            PeriodicLateCounts periodicLateCounts = new PeriodicLateCounts();

            // periodicLateCounts.PeriodicLateCountList.Add(CreatePeriodicLateCount());

            return periodicLateCounts;
        }

        private PlattedLand CreatePlattedLand()
        {
            PlattedLand plattedLand = new PlattedLand();

            // plattedLand.AppurtenanceDescription = ;
            // plattedLand.AppurtenanceIdentifier = ;
            // plattedLand.AppurtenanceType = ;
            // plattedLand.AppurtenanceTypeOtherDescription = ;
            // plattedLand.PlatBlockIdentifier = ;
            // plattedLand.PlatBookIdentifier = ;
            // plattedLand.PlatBuildingIdentifier = ;
            // plattedLand.PlatIdentifier = ;
            // plattedLand.PlatInstrumentIdentifier = ;
            // plattedLand.PlatLotIdentifier = ;
            // plattedLand.PlatName = ;
            // plattedLand.PlatPageIdentifier = ;
            // plattedLand.PlatType = ;
            // plattedLand.PlatTypeOtherDescription = ;
            // plattedLand.PlatUnitIdentifier = ;
            // plattedLand.SequenceNumber = ;

            return plattedLand;
        }

        private PlattedLands CreatePlattedLands()
        {
            PlattedLands plattedLands = new PlattedLands();

            // plattedLands.PlattedLandList.Add(CreatePlattedLand());

            return plattedLands;
        }

        private PledgedAsset CreatePledgedAsset()
        {
            PledgedAsset pledgedAsset = new PledgedAsset();

            // pledgedAsset.Asset = CreateAsset();
            // pledgedAsset.SequenceNumber = ;

            return pledgedAsset;
        }

        private PledgedAssets CreatePledgedAssets()
        {
            PledgedAssets pledgedAssets = new PledgedAssets();

            // pledgedAssets.PledgedAssetList.Add(CreatePledgedAsset());

            return pledgedAssets;
        }

        private Pool CreatePool()
        {
            Pool pool = new Pool();

            // pool.PoolDetail = CreatePoolDetail();
            // pool.PoolCertificate = CreatePoolCertificate();

            return pool;
        }

        private PoolCertificate CreatePoolCertificate()
        {
            PoolCertificate poolCertificate = new PoolCertificate();

            // poolCertificate.PoolCertificateCurrentAmount = ;
            // poolCertificate.PoolCertificateHolderPayeeIdentifier = ;
            // poolCertificate.PoolCertificateIdentifier = ;
            // poolCertificate.PoolCertificateInitialPaymentDate = ;
            // poolCertificate.PoolCertificateIssueDate = ;
            // poolCertificate.PoolCertificateMaturityDate = ;
            // poolCertificate.PoolCertificateOriginalAmount = ;
            // poolCertificate.PoolCertificatePrincipalPaidAmount = ;

            return poolCertificate;
        }

        private PoolDetail CreatePoolDetail()
        {
            PoolDetail poolDetail = new PoolDetail();

            // poolDetail.PoolAccrualRateStructureType = ;
            // poolDetail.PoolAmortizationType = ;
            // poolDetail.PoolAmortizationTypeOtherDescription = ;
            // poolDetail.PoolAssumabilityIndicator = ;
            // poolDetail.PoolBalloonIndicator = ;
            // poolDetail.PoolClassType = ;
            // poolDetail.PoolClassTypeOtherDescription = ;
            // poolDetail.PoolCurrentLoanCount = ;
            // poolDetail.PoolCurrentPrincipalBalanceAmount = ;
            // poolDetail.PoolCUSIPIdentifier = ;
            // poolDetail.PoolFixedServicingFeePercent = ;
            // poolDetail.PoolGuarantyFeeRatePercent = ;
            // poolDetail.PoolIdentifier = ;
            // poolDetail.PoolInterestAdjustmentIndexLeadDaysCount = ;
            // poolDetail.PoolInterestAndPaymentAdjustmentIndexLeadDaysCount = ;
            // poolDetail.PoolInterestOnlyIndicator = ;
            // poolDetail.PoolInterestRateRoundingPercent = ;
            // poolDetail.PoolInterestRateRoundingType = ;
            // poolDetail.PoolInterestRateTruncatedDigitsCount = ;
            // poolDetail.PoolInvestorProductPlanIdentifier = ;
            // poolDetail.PoolIssueDate = ;
            // poolDetail.PoolIssuerIdentifier = ;
            // poolDetail.PoolMarginRatePercent = ;
            // poolDetail.PoolMaturityDate = ;
            // poolDetail.PoolMaximumAccrualRatePercent = ;
            // poolDetail.PoolMinimumAccrualRatePercent = ;
            // poolDetail.PoolMortgageRatePercent = ;
            // poolDetail.PoolMortgageType = ;
            // poolDetail.PoolMortgageTypeOtherDescription = ;
            // poolDetail.PoolOriginalLoanCount = ;
            // poolDetail.PoolOriginalPrincipalBalanceAmount = ;
            // poolDetail.PoolOwnershipPercent = ;
            // poolDetail.PoolPrefixIdentifier = ;
            // poolDetail.PoolScheduledPrincipalAndInterestPaymentAmount = ;
            // poolDetail.PoolScheduledRemittancePaymentDay = ;
            // poolDetail.PoolSecurityInterestRatePercent = ;
            // poolDetail.PoolSecurityIssueDateInterestRatePercent = ;
            // poolDetail.PoolServiceFeeRatePercent = ;
            // poolDetail.PoolStructureType = ;
            // poolDetail.PoolStructureTypeOtherDescription = ;
            // poolDetail.PoolSuffixIdentifier = ;
            // poolDetail.PoolUnscheduledPrincipalPaymentDay = ;
            // poolDetail.PoolingMethodType = ;
            // poolDetail.PoolingMethodTypeOtherDescription = ;
            // poolDetail.SecurityTradeBookEntryDate = ;
            // poolDetail.SecurityTradeCustomerAccountIdentifier = ;

            return poolDetail;
        }

        private PowerOfAttorney CreatePowerOfAttorney()
        {
            PowerOfAttorney powerOfAttorney = new PowerOfAttorney();

            // powerOfAttorney.PowerOfAttorneySigningCapacityText = ;

            return powerOfAttorney;
        }

        private PreferredResponse CreatePreferredResponse()
        {
            PreferredResponse preferredResponse = new PreferredResponse();

            // preferredResponse.MIMETypeIdentifier = ;
            // preferredResponse.PreferredResponseFormatType = ;
            // preferredResponse.PreferredResponseMethodType = ;
            // preferredResponse.PreferredResponseDestinationDescription = ;
            // preferredResponse.PreferredResponseFormatTypeOtherDescription = ;
            // preferredResponse.PreferredResponseMethodTypeOtherDescription = ;
            // preferredResponse.PreferredResponseUseEmbeddedFileIndicator = ;
            // preferredResponse.PreferredResponseVersionIdentifier = ;
            // preferredResponse.SequenceNumber = ;

            return preferredResponse;
        }

        private PreferredResponses CreatePreferredResponses()
        {
            PreferredResponses preferredResponses = new PreferredResponses();

            // preferredResponses.PreferredResponseList.Add(CreatePreferredResponse());

            return preferredResponses;
        }

        private PrepaymentPenalty CreatePrepaymentPenalty()
        {
            PrepaymentPenalty prepaymentPenalty = new PrepaymentPenalty();

            // prepaymentPenalty.PrepaymentPenaltyLifetimeRule = CreatePrepaymentPenaltyLifetimeRule();
            // prepaymentPenalty.PrepaymentPenaltyPerChangeRules = CreatePrepaymentPenaltyPerChangeRules();

            return prepaymentPenalty;
        }

        private PrepaymentPenaltyLifetimeRule CreatePrepaymentPenaltyLifetimeRule()
        {
            PrepaymentPenaltyLifetimeRule prepaymentPenaltyLifetimeRule = new PrepaymentPenaltyLifetimeRule();

            // prepaymentPenaltyLifetimeRule.PrepaymentFinanceChargeRefundableIndicator = ;
            // prepaymentPenaltyLifetimeRule.PrepaymentPenaltyDisclosureDate = ;
            // prepaymentPenaltyLifetimeRule.PrepaymentPenaltyMaximumLifeOfLoanAmount = ;
            // prepaymentPenaltyLifetimeRule.PrepaymentPenaltyWaiverType = ;
            // prepaymentPenaltyLifetimeRule.PrepaymentPenaltyWaiverTypeOtherDescription = ;

            return prepaymentPenaltyLifetimeRule;
        }

        private PrepaymentPenaltyPerChangeRule CreatePrepaymentPenaltyPerChangeRule()
        {
            PrepaymentPenaltyPerChangeRule prepaymentPenaltyPerChangeRule = new PrepaymentPenaltyPerChangeRule();

            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyAssessmentType = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyAssessmentTypeOtherDescription = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyCalculationBalanceType = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyCalculationBalanceTypeOtherDescription = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyCurtailmentAmount = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyCurtailmentPercent = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyEffectiveDate = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyFixedAmount = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyMaximumAmount = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyOptionType = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPercent = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodCount = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodicDaysCount = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodType = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyTextDescription = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyType = ;
            // prepaymentPenaltyPerChangeRule.PrepaymentPenaltyTypeOtherDescription = ;
            // prepaymentPenaltyPerChangeRule.SequenceNumber = ;

            return prepaymentPenaltyPerChangeRule;
        }

        private PrepaymentPenaltyPerChangeRules CreatePrepaymentPenaltyPerChangeRules()
        {
            PrepaymentPenaltyPerChangeRules prepaymentPenaltyPerChangeRules = new PrepaymentPenaltyPerChangeRules();

            // prepaymentPenaltyPerChangeRules.PrepaymentPenaltyPerChangeRuleList.Add(CreatePrepaymentPenaltyPerChangeRule());

            return prepaymentPenaltyPerChangeRules;
        }

        private PresentHousingExpense CreatePresentHousingExpense()
        {
            PresentHousingExpense presentHousingExpense = new PresentHousingExpense();

            // presentHousingExpense.HousingExpenseType = ;
            // presentHousingExpense.PresentHousingExpensePaymentAmount = ;
            // presentHousingExpense.HousingExpenseTypeOtherDescription = ;
            // presentHousingExpense.SequenceNumber = ;

            return presentHousingExpense;
        }

        private PresentHousingExpenses CreatePresentHousingExpenses()
        {
            PresentHousingExpenses presentHousingExpenses = new PresentHousingExpenses();

            // presentHousingExpenses.PresentHousingExpenseList.Add(CreatePresentHousingExpense());

            return presentHousingExpenses;
        }

        private Pria CreatePria()
        {
            Pria pria = new Pria();

            // pria.PriaRequest = CreatePriaRequest();
            // pria.PriaResponse = CreatePriaResponse();

            return pria;
        }

        private PriaConsideration CreatePriaConsideration()
        {
            PriaConsideration priaConsideration = new PriaConsideration();

            // priaConsideration.ConsiderationAmount = ;
            // priaConsideration.ConsiderationType = ;
            // priaConsideration.ConsiderationTypeOtherDescription = ;
            // priaConsideration.SequenceNumber = ;

            return priaConsideration;
        }

        private PriaConsiderations CreatePriaConsiderations()
        {
            PriaConsiderations priaConsiderations = new PriaConsiderations();

            // priaConsiderations.PriaConsiderationList.Add(CreatePriaConsideration());

            return priaConsiderations;
        }

        private PriaRequest CreatePriaRequest()
        {
            PriaRequest priaRequest = new PriaRequest();

            // priaRequest.OriginatingRecordingRequest = CreateOriginatingRecordingRequest();
            // priaRequest.PriaConsiderations = CreatePriaConsiderations();
            // priaRequest.PriaRequestDetail = CreatePriaRequestDetail();
            // priaRequest.RecordingTransactionIdentifier = CreateRecordingTransactionIdentifier();

            return priaRequest;
        }

        private PriaRequestDetail CreatePriaRequestDetail()
        {
            PriaRequestDetail priaRequestDetail = new PriaRequestDetail();

            // priaRequestDetail.PRIARequestRelatedDocumentsIndicator = ;
            // priaRequestDetail.PRIARequestType = ;
            // priaRequestDetail.PRIARequestTypeOtherDescription = ;

            return priaRequestDetail;
        }

        private PriaResponse CreatePriaResponse()
        {
            PriaResponse priaResponse = new PriaResponse();

            // priaResponse.OriginatingRecordingRequest = CreateOriginatingRecordingRequest();
            // priaResponse.PriaResponseDetail = CreatePriaResponseDetail();
            // priaResponse.RecordingEndorsement = CreateRecordingEndorsement();
            // priaResponse.RecordingErrors = CreateRecordingErrors();
            // priaResponse.RecordingTransactionIdentifier = CreateRecordingTransactionIdentifier();

            return priaResponse;
        }

        private PriaResponseDetail CreatePriaResponseDetail()
        {
            PriaResponseDetail priaResponseDetail = new PriaResponseDetail();

            // priaResponseDetail.PRIAResponseRelatedDocumentsIndicator = ;
            // priaResponseDetail.PRIAResponseType = ;
            // priaResponseDetail.PRIAResponseTypeOtherDescription = ;

            return priaResponseDetail;
        }

        private PriceLock CreatePriceLock(CPageData dataLoan)
        {
            PriceLock priceLock = new PriceLock();

            // priceLock.LoanPriceQuoteType = ;
            // priceLock.LoanPriceQuoteTypeOtherDescription = ;
            priceLock.PriceLockDatetime = dataLoan.sRLckdD_rep;
            // priceLock.PriceLockDurationDaysCount = ;
            // priceLock.PriceLockIdentifier = ;
            // priceLock.PriceLockStatusType = ;
            // priceLock.PriceLockStatusTypeOtherDescription = ;
            // priceLock.PriceQuoteIdentifier = ;
            // priceLock.PriceRequestIdentifier = ;
            // priceLock.PriceResponseIdentifier = ;
            // priceLock.RateLockPeriodDaysCount = ;
            // priceLock.RateLockRequestedExtensionDaysCount = ;
            // priceLock.SequenceNumber = ;

            return priceLock;
        }

        private PriceLocks CreatePriceLocks(CPageData dataLoan)
        {
            PriceLocks priceLocks = new PriceLocks();

            priceLocks.PriceLockList.Add(CreatePriceLock(dataLoan));

            return priceLocks;
        }

        private PrincipalAndInterestAdjustmentLimitedPaymentOption CreatePrincipalAndInterestAdjustmentLimitedPaymentOption()
        {
            PrincipalAndInterestAdjustmentLimitedPaymentOption principalAndInterestAdjustmentLimitedPaymentOption = new PrincipalAndInterestAdjustmentLimitedPaymentOption();

            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentActivationAmount = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentActivationIndicator = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentActivationPercent = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentEffectiveDate = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentOptionDescription = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.LimitedPrincipalAndInterestPaymentPullTheNoteIndicator = ;
            // principalAndInterestAdjustmentLimitedPaymentOption.SequenceNumber = ;

            return principalAndInterestAdjustmentLimitedPaymentOption;
        }

        private PrincipalAndInterestAdjustmentLimitedPaymentOptions CreatePrincipalAndInterestAdjustmentLimitedPaymentOptions()
        {
            PrincipalAndInterestAdjustmentLimitedPaymentOptions principalAndInterestAdjustmentLimitedPaymentOptions = new PrincipalAndInterestAdjustmentLimitedPaymentOptions();

            // principalAndInterestAdjustmentLimitedPaymentOptions.PrincipalAndInterestAdjustmentLimitedPaymentOptionList.Add(CreatePrincipalAndInterestAdjustmentLimitedPaymentOption());

            return principalAndInterestAdjustmentLimitedPaymentOptions;
        }

        private PrincipalAndInterestPaymentAdjustment CreatePrincipalAndInterestPaymentAdjustment()
        {
            PrincipalAndInterestPaymentAdjustment principalAndInterestPaymentAdjustment = new PrincipalAndInterestPaymentAdjustment();

            // principalAndInterestPaymentAdjustment.IndexRules = CreateIndexRules();
            // principalAndInterestPaymentAdjustment.PrincipalAndInterestAdjustmentLimitedPaymentOptions = CreatePrincipalAndInterestAdjustmentLimitedPaymentOptions();
            // principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentLifetimeAdjustmentRule = CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule();
            // principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentPerChangeAdjustmentRules = CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules();
            // principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentPeriodicAdjustmentRules = CreatePrincipalAndInterestPaymentPeriodicAdjustmentRules();

            return principalAndInterestPaymentAdjustment;
        }

        private PrincipalAndInterestPaymentLifetimeAdjustmentRule CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule()
        {
            PrincipalAndInterestPaymentLifetimeAdjustmentRule principalAndInterestPaymentLifetimeAdjustmentRule = new PrincipalAndInterestPaymentLifetimeAdjustmentRule();

            // principalAndInterestPaymentLifetimeAdjustmentRule.FinalPrincipalAndInterestPaymentChangeDate = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.FirstPrincipalAndInterestPaymentChangeDate = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.GraduatedPaymentMortgageAndGrowingEquityMortgagePrincipalAndInterestPaymentChangesCount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.GraduatedPaymentMultiplierRatePercent = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.GrowingEquityLoanPayoffYearsCount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.InitialPaymentDiscountPercent = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PaymentAdjustmentLifetimeCapAmount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PaymentAdjustmentLifetimeCapPercent = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PaymentsBetweenPrincipalAndInterestPaymentChangesCount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestCalculationPaymentPeriodType = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentDecreaseCapType = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentFinalRecastType = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentFinalRecastTypeOtherDescription = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentIncreaseCapType = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumAmount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumDecreaseRatePercent = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumExtensionCount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMinimumAmount = ;
            // principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalBalanceCalculationMethodType = ;

            return principalAndInterestPaymentLifetimeAdjustmentRule;
        }

        private PrincipalAndInterestPaymentPeriodicAdjustmentRule CreatePrincipalAndInterestPaymentPeriodicAdjustmentRule()
        {
            PrincipalAndInterestPaymentPeriodicAdjustmentRule principalAndInterestPaymentPeriodicAdjustmentRule = new PrincipalAndInterestPaymentPeriodicAdjustmentRule();

            // principalAndInterestPaymentPeriodicAdjustmentRule.AdjustmentRuleType = ;
            // principalAndInterestPaymentPeriodicAdjustmentRule.PrincipalAndInterestRecastMonthsCount = ;
            // principalAndInterestPaymentPeriodicAdjustmentRule.PeriodicEffectiveDate = ;
            // principalAndInterestPaymentPeriodicAdjustmentRule.PeriodicEffectiveMonthsCount = ;
            // principalAndInterestPaymentPeriodicAdjustmentRule.PrincipalAndInterestPaymentAnnualCapAmount = ;
            // principalAndInterestPaymentPeriodicAdjustmentRule.SequenceNumber = ;

            return principalAndInterestPaymentPeriodicAdjustmentRule;
        }

        private PrincipalAndInterestPaymentPeriodicAdjustmentRules CreatePrincipalAndInterestPaymentPeriodicAdjustmentRules()
        {
            PrincipalAndInterestPaymentPeriodicAdjustmentRules principalAndInterestPaymentPeriodicAdjustmentRules = new PrincipalAndInterestPaymentPeriodicAdjustmentRules();

            // principalAndInterestPaymentPeriodicAdjustmentRules.PrincipalAndInterestPaymentPeriodicAdjustmentRuleList.Add(CreatePrincipalAndInterestPaymentPeriodicAdjustmentRule());

            return principalAndInterestPaymentPeriodicAdjustmentRules;
        }

        private PrincipalAndInterestPaymentPerChangeAdjustmentRule CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule()
        {
            PrincipalAndInterestPaymentPerChangeAdjustmentRule principalAndInterestPaymentPerChangeAdjustmentRule = new PrincipalAndInterestPaymentPerChangeAdjustmentRule();

            // principalAndInterestPaymentPerChangeAdjustmentRule.AdjustmentRuleType = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.NoPrincipalAndInterestPaymentCapsFeatureDescription = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PaymentsBetweenPrincipalAndInterestPaymentChangesCount = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentAmount = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentCalculationMethodType = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentCalculationMethodTypeOtherDescription = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentAdjustmentPercent = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent = ;
            // principalAndInterestPaymentPerChangeAdjustmentRule.SequenceNumber = ;

            return principalAndInterestPaymentPerChangeAdjustmentRule;
        }

        private PrincipalAndInterestPaymentPerChangeAdjustmentRules CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules()
        {
            PrincipalAndInterestPaymentPerChangeAdjustmentRules principalAndInterestPaymentPerChangeAdjustmentRules = new PrincipalAndInterestPaymentPerChangeAdjustmentRules();

            // principalAndInterestPaymentPerChangeAdjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList.Add(CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule());

            return principalAndInterestPaymentPerChangeAdjustmentRules;
        }

        private ProductCategory CreateProductCategory()
        {
            ProductCategory productCategory = new ProductCategory();

            // productCategory.ProductCategoryName = ;

            return productCategory;
        }

        private ProductComponent CreateProductComponent()
        {
            ProductComponent productComponent = new ProductComponent();

            // productComponent.ProductComponentDescription = ;
            // productComponent.ProductComponentIdentifier = ;
            // productComponent.ProductComponentName = ;
            // productComponent.SequenceNumber = ;

            return productComponent;
        }

        private ProductComponents CreateProductComponents()
        {
            ProductComponents productComponents = new ProductComponents();

            // productComponents.ProductComponentList.Add(CreateProductComponent());

            return productComponents;
        }

        private Project CreateProject(CPageData dataLoan)
        {
            Project project = new Project();

            project.ProjectDetail = dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac ?
                CreateProjectDetailFreddie(dataLoan) : CreateProjectDetailFannie(dataLoan);

            return project;
        }

        private string CalculateProjectClassificationIdentifier(CPageData dataLoan)
        {
            string projectClassificationIdentifier = string.Empty;
            if (dataLoan.sLT == E_sLT.Conventional)
            {
                if (dataLoan.sSpProjectClassFannieT != E_sSpProjectClassFannieT.LeaveBlank)
                {
                    switch (dataLoan.sSpProjectClassFannieT)
                    {
                        case E_sSpProjectClassFannieT._1Coop:
                            projectClassificationIdentifier = "1";
                            break;
                        case E_sSpProjectClassFannieT._2Coop:
                            projectClassificationIdentifier = "2";
                            break;
                        case E_sSpProjectClassFannieT.EPud:
                            projectClassificationIdentifier = "E";
                            break;
                        case E_sSpProjectClassFannieT.FPud:
                            projectClassificationIdentifier = "F";
                            break;
                        case E_sSpProjectClassFannieT.GNotInProject:
                            projectClassificationIdentifier = "G";
                            break;
                        case E_sSpProjectClassFannieT.PLimitedReviewNew:
                            projectClassificationIdentifier = "P";
                            break;
                        case E_sSpProjectClassFannieT.QLimitedReviewEst:
                            projectClassificationIdentifier = "Q";
                            break;
                        case E_sSpProjectClassFannieT.RExpeditedReviewNew:
                            projectClassificationIdentifier = "R";
                            break;
                        case E_sSpProjectClassFannieT.SExpeditedReviewEst:
                            projectClassificationIdentifier = "S";
                            break;
                        case E_sSpProjectClassFannieT.TCoop:
                        case E_sSpProjectClassFannieT.TFannieReview:
                        case E_sSpProjectClassFannieT.TPud:
                            projectClassificationIdentifier = "T";
                            break;
                        case E_sSpProjectClassFannieT.UFhaApproved:
                            projectClassificationIdentifier = "U";
                            break;
                        case E_sSpProjectClassFannieT.VRefiPlus:
                            projectClassificationIdentifier = "V";
                            break;
                    }
                }
                else if (dataLoan.sSpProjectClassFreddieT != E_sSpProjectClassFreddieT.LeaveBlank)
                {
                    switch (dataLoan.sSpProjectClassFreddieT)
                    {
                        case E_sSpProjectClassFreddieT.FreddieMacStreamlinedReview:
                            projectClassificationIdentifier = "StreamlinedReview";
                            break;
                        case E_sSpProjectClassFreddieT.FreddieMacEstablishedProject:
                        case E_sSpProjectClassFreddieT.FreddieMacNewProject:
                        case E_sSpProjectClassFreddieT.FreddieMacDetachedProject:
                        case E_sSpProjectClassFreddieT.FreddieMac2To4UnitProject:
                            projectClassificationIdentifier = "FullReview";
                            break;
                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewCPM:
                            projectClassificationIdentifier = "CondominiumProjectManagerReview";
                            break;
                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewPERS:
                            projectClassificationIdentifier = "ProjectEligibilityReviewService";
                            break;
                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewFHA:
                            projectClassificationIdentifier = "FHA_Approved";
                            break;
                    }
                }
                else
                {
                    projectClassificationIdentifier = "G";
                }
                //// 1/7/2015 IR - OPM 199136 - Overwrite with ExemptFromReview if Relief Refi / Open Access.
                if (dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiSameServicer
                         || dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiOpenAccess)
                {
                    projectClassificationIdentifier = "ExemptFromReview";
                }
            }
            return projectClassificationIdentifier;

        }
        private ProjectDetail CreateProjectDetailFannie(CPageData dataLoan)
        {
            ProjectDetail projectDetail = new ProjectDetail();

            // projectDetail.AdditionalProjectConsiderationsType = ;
            // projectDetail.AdditionalProjectConsiderationsTypeOtherDescription = ;

            if (!string.IsNullOrEmpty(dataLoan.sCpmProjectId) && ToProjectLegalStructureType(dataLoan.sGseSpT) == ProjectLegalStructureType.Condominium)
            {
                projectDetail.FNMCondominiumProjectManagerProjectIdentifier = dataLoan.sCpmProjectId;
            }
            else
            {
                if (ToProjectLegalStructureType(dataLoan.sGseSpT) == ProjectLegalStructureType.Condominium)
                {
                    projectDetail.CondominiumProjectStatusType = ToMismo(dataLoan.sSpProjectStatusT);
                }
                projectDetail.ProjectAttachmentType = ToMismo(dataLoan.sSpProjectAttachmentT);
                projectDetail.ProjectDesignType = ToMismo(dataLoan.sSpProjectDesignT);
            }
            // projectDetail.ProjectAmenityImprovementsCompletedIndicator = ;
            projectDetail.ProjectClassificationIdentifier = CalculateProjectClassificationIdentifier(dataLoan);

            if (projectDetail.ProjectDesignType == ProjectDesignType.Other)
            {
                projectDetail.ProjectDesignTypeOtherDescription = "OtherSelectedOnValuationDocumentation";
            }

            projectDetail.ProjectDwellingUnitCount = dataLoan.sSpProjectDwellingUnitCount_rep;
            projectDetail.ProjectDwellingUnitsSoldCount = dataLoan.sSpProjectDwellingUnitSoldCount_rep;
            projectDetail.ProjectLegalStructureType = ToProjectLegalStructureType(dataLoan.sGseSpT);
            projectDetail.ProjectName = dataLoan.sProjNm;
            projectDetail.PUDIndicator = dataLoan.sGseSpT == E_sGseSpT.PUD;
            // projectDetail.TwoToFourUnitCondominiumProjectIndicator = ;

            return projectDetail;
        }

        private ProjectDetail CreateProjectDetailFreddie(CPageData dataLoan)
        {
            ProjectDetail projectDetail = new ProjectDetail();
            projectDetail.ProjectLegalStructureType = ToProjectLegalStructureType(dataLoan.sGseSpT);

            if (projectDetail.ProjectLegalStructureType == ProjectLegalStructureType.Condominium)
            {
                projectDetail.ProjectClassificationIdentifier = CalculateProjectClassificationIdentifier(dataLoan);
            }

            bool isCondoNotExemptFromReview = projectDetail.ProjectLegalStructureType == ProjectLegalStructureType.Condominium
                && !projectDetail.ProjectClassificationIdentifier.Equals("ExemptFromReview", StringComparison.OrdinalIgnoreCase);
            bool isCooperative = projectDetail.ProjectLegalStructureType == ProjectLegalStructureType.Cooperative;

            if (isCondoNotExemptFromReview)
            {
                projectDetail.CondominiumProjectStatusType = ToMismo(dataLoan.sSpProjectStatusT);
            }

            if (isCondoNotExemptFromReview || isCooperative)
            {
                //If Cooperative, set ProjectDesignType if ProjectAttachmentType not set. Set both for condos
                projectDetail.ProjectAttachmentType = ToMismo(dataLoan.sSpProjectAttachmentT);
                if (projectDetail.ProjectAttachmentType == ProjectAttachmentType.Undefined || !isCooperative)
                {
                    projectDetail.ProjectDesignType = ToMismo(dataLoan.sSpProjectDesignT);

                    if (projectDetail.ProjectDesignType == ProjectDesignType.Other)
                    {
                        projectDetail.ProjectDesignTypeOtherDescription = "OtherSelectedOnValuationDocumentation";
                    }
                }

                projectDetail.ProjectDwellingUnitCount = dataLoan.sSpProjectDwellingUnitCount_rep;
                projectDetail.ProjectDwellingUnitsSoldCount = dataLoan.sSpProjectDwellingUnitSoldCount_rep;
                projectDetail.ProjectName = dataLoan.sProjNm;
            }

            projectDetail.PUDIndicator = dataLoan.sGseSpT == E_sGseSpT.PUD;
            return projectDetail;
        }

        private CondominiumProjectStatusType ToMismo(E_sSpProjectStatusT sSpProjectStatusT)
        {
            switch (sSpProjectStatusT)
            {
                case E_sSpProjectStatusT.LeaveBlank:
                    return CondominiumProjectStatusType.Undefined;
                case E_sSpProjectStatusT.Existing:
                    return CondominiumProjectStatusType.Established;
                case E_sSpProjectStatusT.New:
                    return CondominiumProjectStatusType.New;
                default:
                    throw new UnhandledEnumException(sSpProjectStatusT);
            }
        }

        private ProjectAttachmentType ToMismo(E_sSpProjectAttachmentT sSpProjectAttachmentT)
        {
            switch (sSpProjectAttachmentT)
            {
                case E_sSpProjectAttachmentT.LeaveBlank:
                    return ProjectAttachmentType.Undefined;
                case E_sSpProjectAttachmentT.Attached:
                    return ProjectAttachmentType.Attached;
                case E_sSpProjectAttachmentT.Detached:
                    return ProjectAttachmentType.Detached;
                default:
                    throw new UnhandledEnumException(sSpProjectAttachmentT);
            }
        }

        private ProjectDesignType ToMismo(E_sSpProjectDesignT sSpProjectDesignT)
        {
            switch (sSpProjectDesignT)
            {
                case E_sSpProjectDesignT.LeaveBlank:
                    return ProjectDesignType.Undefined;
                case E_sSpProjectDesignT.Townhouse:
                    return ProjectDesignType.TownhouseRowhouse;
                case E_sSpProjectDesignT.LowRise:
                    return ProjectDesignType.GardenProject;
                case E_sSpProjectDesignT.MidRise:
                    return ProjectDesignType.MidriseProject;
                case E_sSpProjectDesignT.HighRise:
                    return ProjectDesignType.HighriseProject;
                case E_sSpProjectDesignT.Other:
                    return ProjectDesignType.Other;
                default:
                    throw new UnhandledEnumException(sSpProjectDesignT);
            }
        }

        private ProjectLegalStructureType ToProjectLegalStructureType(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.Cooperative:
                    return ProjectLegalStructureType.Cooperative;
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return ProjectLegalStructureType.Condominium;
                case E_sGseSpT.Detached:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.Attached:
                case E_sGseSpT.PUD:
                case E_sGseSpT.LeaveBlank:
                    return ProjectLegalStructureType.Undefined;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        private global::ULDD.Properties CreateProperties(CPageData dataLoan)
        {
            global::ULDD.Properties properties = new global::ULDD.Properties();

            properties.PropertyList.Add(CreateProperty(dataLoan));

            return properties;
        }

        private Property CreateProperty(CPageData dataLoan)
        {
            Property property = new Property();

            property.Address = CreateAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
            property.FloodDetermination = CreateFloodDetermination(dataLoan);
            // property.HazardInsurances = CreateHazardInsurances();
            // property.HomeownersAssociation = CreateHomeownersAssociation();
            // property.LegalDescriptions = CreateLegalDescriptions();
            // property.LocationIdentifier = CreateLocationIdentifier();

            //// ir - iOPM 190789: Set MANUFACTURED_HOME/MANUFACTURED_HOME_DETAIL/ManufacturedHomeWidthType for SingleWide &  SingleWide
            if (dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide || dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide)
            {
                property.ManufacturedHome = CreateManufacturedHome(dataLoan.sGseSpT);
            }

            property.Project = CreateProject(dataLoan);
            property.PropertyDetail = CreatePropertyDetail(dataLoan);
            // property.PropertyHistory = CreatePropertyHistory();
            // property.PropertyTaxes = CreatePropertyTaxes();
            // property.PropertyTitle = CreatePropertyTitle();
            property.PropertyValuations = CreatePropertyValuations(dataLoan);
            //12/27/2014 IR - OPM 197829 - Bedroom count and eligible rent only needed if full appraisal or prior appraisal used.
            if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac
                || ToMismo(dataLoan.sSpValuationMethodT) == PropertyValuationMethodType.FullAppraisal
                || ToMismo(dataLoan.sSpValuationMethodT) == PropertyValuationMethodType.PriorAppraisalUsed)
            {
                property.PropertyUnits = CreatePropertyUnits(dataLoan);
            }

            // property.SequenceNumber = ;

            return property;
        }

        private PropertyDetail CreatePropertyDetail(CPageData dataLoan)
        {
            PropertyDetail propertyDetail = new PropertyDetail();
            propertyDetail.AttachmentType = ToMismo(dataLoan.sProdSpStructureT);
            // propertyDetail.CommunityLandTrustIndicator = ;
            // propertyDetail.CommunityReinvestmentActDelineatedCommunityIndicator = ;
            propertyDetail.ConstructionMethodType = ToMismo(dataLoan.sGseSpT);
            // propertyDetail.ConstructionMethodTypeOtherDescription = ;
            // propertyDetail.ConstructionStatusType = ;
            // propertyDetail.ConstructionStatusTypeOtherDescription = ;
            // propertyDetail.DeedRestrictionIndicator = ;
            // propertyDetail.EarthquakeInsuranceRequiredIndicator = ;
            propertyDetail.FinancedUnitCount = dataLoan.sUnitsNum_rep;
            // propertyDetail.GrossLivingAreaSquareFeetNumber = ;
            // propertyDetail.GroupHomeIndicator = ;
            // propertyDetail.InvestorREOPropertyIdentifier = ;
            // propertyDetail.LandUseDescription = ;
            // propertyDetail.LandUseTypeOtherDescription = ;
            // propertyDetail.LenderDesignatedDecliningMarketIdentifier = ;
            // propertyDetail.NativeAmericanLandsType = ;
            // propertyDetail.NativeAmericanLandsTypeOtherDescription = ;
            // propertyDetail.PriorSaleInLastTwelveMonthsIndicator = ;
            // propertyDetail.PropertyAcquiredDate = ;
            // propertyDetail.PropertyAcquiredYear = ;
            // propertyDetail.PropertyAcreageNumber = ;
            // propertyDetail.PropertyCommunityLandTrustIndicator = ;
            // propertyDetail.PropertyConditionDescription = ;
            // propertyDetail.PropertyCurrentOccupantName = ;
            // propertyDetail.PropertyEarthquakeInsuranceIndicator = ;
            propertyDetail.PropertyEstateType = ToMismo(dataLoan.sEstateHeldT);
            // propertyDetail.PropertyEstateTypeOtherDescription = ;
            // propertyDetail.PropertyExistingLienAmount = ;
            propertyDetail.PropertyFloodInsuranceIndicator = dataLoan.sProFloodIns > 0;
            // propertyDetail.PropertyGroundLeaseExpirationDate = ;
            // propertyDetail.PropertyGroundLeasePerpetualIndicator = ;
            // propertyDetail.PropertyInclusionaryZoningIndicator = ;
            // propertyDetail.PropertyNeighborhoodLocationTypeOtherDescription = ;
            // propertyDetail.PropertyOccupancyStatusType = ;
            // propertyDetail.PropertyNeighborhoodLocationType = ;
            // propertyDetail.PropertyOriginalCostAmount = ;
            // propertyDetail.PropertyPreviouslyOccupiedIndicator = ;
            propertyDetail.PropertyStructureBuiltYear = dataLoan.sYrBuilt;
            // propertyDetail.PropertyStructureHabitableYearRoundIndicator = ;
            propertyDetail.PropertyUsageType = ToMismo(dataLoan.sOccT);
            // propertyDetail.PropertyUsageTypeOtherDescription = ;
            // propertyDetail.PropertyZoningCategoryType = ;
            // propertyDetail.PropertyZoningCategoryTypeOtherDescription = ;
            // propertyDetail.UniqueDwellingTypeOtherDescription = ;
            // propertyDetail.UniqueDwellingType = ;
            // propertyDetail.WindstormInsuranceRequiredIndicator = ;

            return propertyDetail;
        }

        private ConstructionMethodType ToMismo(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return ConstructionMethodType.Manufactured;
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.Attached:
                case E_sGseSpT.Detached:
                case E_sGseSpT.PUD:
                case E_sGseSpT.Condominium:
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                case E_sGseSpT.Modular:
                    return ConstructionMethodType.SiteBuilt;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        private PropertyUsageType ToMismo(E_sOccT sOccT)
        {
            switch (sOccT)
            {
                case E_sOccT.PrimaryResidence:
                    return PropertyUsageType.PrimaryResidence;
                case E_sOccT.SecondaryResidence:
                    return PropertyUsageType.SecondHome;
                case E_sOccT.Investment:
                    return PropertyUsageType.Investment;
                default:
                    throw new UnhandledEnumException(sOccT);
            }
        }

        private PropertyEstateType ToMismo(E_sEstateHeldT sEstateHeldT)
        {
            switch (sEstateHeldT)
            {
                case E_sEstateHeldT.FeeSimple:
                    return PropertyEstateType.FeeSimple;
                case E_sEstateHeldT.LeaseHold:
                    return PropertyEstateType.Leasehold;
                default:
                    throw new UnhandledEnumException(sEstateHeldT);
            }
        }

        private AttachmentType ToMismo(E_sProdSpStructureT sProdSpStructureT)
        {
            switch (sProdSpStructureT)
            {
                case E_sProdSpStructureT.Attached:
                    return AttachmentType.Attached;
                case E_sProdSpStructureT.Detached:
                    return AttachmentType.Detached;
                default:
                    throw new UnhandledEnumException(sProdSpStructureT);
            }
        }

        private PropertyHistory CreatePropertyHistory()
        {
            PropertyHistory propertyHistory = new PropertyHistory();

            // propertyHistory.ListingInformation = CreateListingInformation();
            // propertyHistory.SalesHistories = CreateSalesHistories();

            return propertyHistory;
        }

        private PropertyOwner CreatePropertyOwner()
        {
            PropertyOwner propertyOwner = new PropertyOwner();

            // propertyOwner.OwnershipPercent = ;
            // propertyOwner.RelationshipVestingType = ;
            // propertyOwner.RelationshipVestingTypeOtherDescription = ;

            return propertyOwner;
        }

        private PropertySeller CreatePropertySeller()
        {
            PropertySeller propertySeller = new PropertySeller();

            // propertySeller.MaritalStatusType = ;

            return propertySeller;
        }

        private PropertyTax CreatePropertyTax()
        {
            PropertyTax propertyTax = new PropertyTax();

            // propertyTax.PropertyTaxDetail = CreatePropertyTaxDetail();
            // propertyTax.PropertyTaxExemptions = CreatePropertyTaxExemptions();
            // propertyTax.PropertyTaxSpecials = CreatePropertyTaxSpecials();
            // propertyTax.SequenceNumber = ;

            return propertyTax;
        }

        private PropertyTaxes CreatePropertyTaxes()
        {
            PropertyTaxes propertyTaxes = new PropertyTaxes();

            // propertyTaxes.PropertyTaxList.Add(CreatePropertyTax());

            return propertyTaxes;
        }

        private PropertyTaxDetail CreatePropertyTaxDetail()
        {
            PropertyTaxDetail propertyTaxDetail = new PropertyTaxDetail();

            // propertyTaxDetail.PropertyTaxAssessmentStartYear = ;
            // propertyTaxDetail.PropertyTaxAssessmentEndYear = ;
            // propertyTaxDetail.PropertyTaxAtypicalCommentDescription = ;
            // propertyTaxDetail.PropertyTaxCertificateDate = ;
            // propertyTaxDetail.PropertyTaxCountyRateAreaIdentifier = ;
            // propertyTaxDetail.PropertyTaxExemptionIndicator = ;
            // propertyTaxDetail.PropertyTaxImprovementValueAmount = ;
            // propertyTaxDetail.PropertyTaxLandValueAmount = ;
            // propertyTaxDetail.PropertyTaxTotalAssessedValueAmount = ;
            // propertyTaxDetail.PropertyTaxTotalSpecialTaxAmount = ;
            // propertyTaxDetail.PropertyTaxTotalTaxableValueAmount = ;
            // propertyTaxDetail.PropertyTaxTotalTaxAmount = ;
            // propertyTaxDetail.PropertyTaxToValueRatePercent = ;
            // propertyTaxDetail.PropertyTaxTypicalTaxIndicator = ;
            // propertyTaxDetail.PropertyTaxYearIdentifier = ;
            // propertyTaxDetail.PropertyTaxEndYear = ;
            // propertyTaxDetail.TaxAuthorityParcelIdentifier = ;
            // propertyTaxDetail.TaxAuthorityType = ;
            // propertyTaxDetail.TaxAuthorityTypeOtherDescription = ;
            // propertyTaxDetail.TaxContractIdentifier = ;
            // propertyTaxDetail.TaxContractLifeOfLoanIndicator = ;
            // propertyTaxDetail.TaxContractOrderDate = ;
            // propertyTaxDetail.TaxContractSuffixIdentifier = ;
            // propertyTaxDetail.TaxEscrowedIndicator = ;
            // propertyTaxDetail.TaxItemExemptionIndicator = ;

            return propertyTaxDetail;
        }

        private PropertyTaxExemption CreatePropertyTaxExemption()
        {
            PropertyTaxExemption propertyTaxExemption = new PropertyTaxExemption();

            // propertyTaxExemption.PropertyTaxExemptionDescription = ;
            // propertyTaxExemption.SequenceNumber = ;

            return propertyTaxExemption;
        }

        private PropertyTaxExemptions CreatePropertyTaxExemptions()
        {
            PropertyTaxExemptions propertyTaxExemptions = new PropertyTaxExemptions();

            // propertyTaxExemptions.PropertyTaxExemptionList.Add(CreatePropertyTaxExemption());

            return propertyTaxExemptions;
        }

        private PropertyTaxSpecial CreatePropertyTaxSpecial()
        {
            PropertyTaxSpecial propertyTaxSpecial = new PropertyTaxSpecial();

            // propertyTaxSpecial.PropertyTaxSpecialDescription = ;
            // propertyTaxSpecial.PropertyTaxSpecialAmount = ;
            // propertyTaxSpecial.SequenceNumber = ;

            return propertyTaxSpecial;
        }

        private PropertyTaxSpecials CreatePropertyTaxSpecials()
        {
            PropertyTaxSpecials propertyTaxSpecials = new PropertyTaxSpecials();

            // propertyTaxSpecials.PropertyTaxSpecialList.Add(CreatePropertyTaxSpecial());

            return propertyTaxSpecials;
        }

        private PropertyTitle CreatePropertyTitle()
        {
            PropertyTitle propertyTitle = new PropertyTitle();

            // propertyTitle.SellerPropertyAcquisitionDate = ;
            // propertyTitle.TitleHolderCount = ;
            // propertyTitle.TitlePreliminaryReportDate = ;
            // propertyTitle.TitleProcessInsuranceType = ;
            // propertyTitle.TitleProcessType = ;
            // propertyTitle.TitleProcessTypeOtherDescription = ;
            // propertyTitle.TitleReportItemsDescription = ;
            // propertyTitle.TitleReportRequiredEndorsementsDescription = ;
            // propertyTitle.TitleTransferredPastSixMonthsIndicator = ;

            return propertyTitle;
        }

        private PropertyUnit CreatePropertyUnit(string bedroomCount, string unitRent)
        {
            PropertyUnit propertyUnit = new PropertyUnit();

            // propertyUnit.Address = CreateAddress();
            propertyUnit.PropertyUnitDetail = CreatePropertyUnitDetail(bedroomCount, unitRent);
            // propertyUnit.SequenceNumber = ;

            if (propertyUnit.PropertyUnitDetail == null)
            {
                return null;
            }

            return propertyUnit;
        }

        private PropertyUnits CreatePropertyUnits(CPageData dataLoan)
        {
            PropertyUnits propertyUnits = new PropertyUnits();

            if (dataLoan.sUnitsNum > 0)
            {
                propertyUnits.PropertyUnitList.Add(CreatePropertyUnit(dataLoan.sSpUnit1BedroomsCount_rep, dataLoan.sSpUnit1EligibleRent_rep));
            }
            if (dataLoan.sUnitsNum > 1)
            {
                propertyUnits.PropertyUnitList.Add(CreatePropertyUnit(dataLoan.sSpUnit2BedroomsCount_rep, dataLoan.sSpUnit2EligibleRent_rep));
            }
            if (dataLoan.sUnitsNum > 2)
            {
                propertyUnits.PropertyUnitList.Add(CreatePropertyUnit(dataLoan.sSpUnit3BedroomsCount_rep, dataLoan.sSpUnit3EligibleRent_rep));
            }
            if (dataLoan.sUnitsNum > 3)
            {
                propertyUnits.PropertyUnitList.Add(CreatePropertyUnit(dataLoan.sSpUnit4BedroomsCount_rep, dataLoan.sSpUnit4EligibleRent_rep));
            }

            if (propertyUnits.PropertyUnitList.All(propertyUnit => propertyUnit == null))
            {
                return null;
            }

            return propertyUnits;
        }

        private PropertyUnitDetail CreatePropertyUnitDetail(string bedroomCount, string unitRent)
        {
            int bedroomCountInt = 0;
            if (int.TryParse(bedroomCount, out bedroomCountInt) == false)
            {
                return null;
            }
            if (bedroomCountInt == 0)
            {
                return null;
            }

            PropertyUnitDetail propertyUnitDetail = new PropertyUnitDetail();

            propertyUnitDetail.BedroomCount = bedroomCountInt.ToString();
            propertyUnitDetail.PropertyDwellingUnitEligibleRentAmount = TruncateMoneyAmount(unitRent);
            // propertyUnitDetail.UnitIdentifier = ;

            return propertyUnitDetail;
        }
        private string TruncateMoneyAmount(string value)
        {
            //return value;

            if (string.IsNullOrEmpty(value))
                return "";

            decimal amount;
            decimal.TryParse(value, out amount);
            amount = decimal.Round(amount);

            return decimal.ToInt32(amount).ToString();
        }

        private PropertyValuation CreatePropertyValuation(CPageData dataLoan)
        {
            PropertyValuation propertyValuation = new PropertyValuation();

            propertyValuation.Avms = CreateAvms(dataLoan);
            propertyValuation.PropertyValuationDetail = CreatePropertyValuationDetail(dataLoan);
            // propertyValuation.SalesContract = CreateSalesContract();
            // propertyValuation.SequenceNumber = ;

            return propertyValuation;
        }

        private PropertyValuations CreatePropertyValuations(CPageData dataLoan)
        {
            PropertyValuations propertyValuations = new PropertyValuations();

            propertyValuations.PropertyValuationList.Add(CreatePropertyValuation(dataLoan));

            return propertyValuations;
        }

        private PropertyValuationDetail CreatePropertyValuationDetail(CPageData dataLoan)
        {
            PropertyValuationDetail propertyValuationDetail = new PropertyValuationDetail();

            if (dataLoan.sSpValuationMethodT != E_sSpValuationMethodT.None)
            {
                propertyValuationDetail.AppraisalIdentifier = dataLoan.sSpAppraisalId;
            }
            // propertyValuationDetail.PropertyEstimatedValueAmount = ;
            // propertyValuationDetail.PropertyReplacementValueAmount = ;

            string propertyValuationAmount = "";
            if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.PriorAppraisalUsed
                && dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr)
            {
                propertyValuationAmount = TruncateMoneyAmount(dataLoan.sOriginalAppraisedValue_rep);
            }
            else if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.None
                && dataLoan.sLPurposeT == E_sLPurposeT.Purchase
                && dataLoan.sApprVal == 0.0M)
            {
                propertyValuationAmount = TruncateMoneyAmount(dataLoan.sPurchPrice_rep);
            }
            else
            {
                propertyValuationAmount = TruncateMoneyAmount(dataLoan.sApprVal_rep);
            }
            propertyValuationDetail.PropertyValuationAmount = propertyValuationAmount;
            // propertyValuationDetail.PropertyValuationConclusionDependencyType = ;
            // propertyValuationDetail.PropertyValuationConclusionDependencyTypeOtherDescription = ;
            propertyValuationDetail.PropertyValuationEffectiveDate = dataLoan.sSpValuationEffectiveD_rep;
            propertyValuationDetail.PropertyValuationFormType = ToMismo(dataLoan.sSpAppraisalFormT);

            if (Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2 && dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && dataLoan.sSpAppraisalFormT == E_sSpAppraisalFormT.Other)
            {
                propertyValuationDetail.PropertyValuationFormTypeOtherDescription = "OneUnitResidentialAppraisalDeskReviewReport";
            }

            // propertyValuationDetail.PropertyValuationFormTypeOtherDescription = ;
            // propertyValuationDetail.PropertyValuationFormVersionIdentifier = ;
            // propertyValuationDetail.PropertyValuationInspectionType = ;
            // propertyValuationDetail.PropertyValuationInspectionTypeOtherDescription = ;
            propertyValuationDetail.PropertyValuationMethodType = ToMismo(dataLoan.sSpValuationMethodT);
            if (dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FieldReview)
            {
                propertyValuationDetail.PropertyValuationMethodTypeOtherDescription = E_sSpValuationMethodT.FieldReview.ToString("G");
            }
            else if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac && Phase3Parts[dataLoan.sLId] == UlddPhase3Part.Part2 && dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.DeskReview)
            {
                propertyValuationDetail.PropertyValuationMethodTypeOtherDescription = E_sSpValuationMethodT.DeskReview.ToString("G");
            }

            // propertyValuationDetail.PropertyValuationType = ;

            return propertyValuationDetail;
        }

        /// <summary>
        /// Retrieves the <see cref="PropertyValuationFormType" /> corresponding to the given appraisal form type.
        /// </summary>
        /// <param name="appraisalFormType">The appraisal form type.</param>
        /// <returns>The corresponding <see cref="PropertyValuationFormType" /> value.</returns>
        private PropertyValuationFormType ToMismo(E_sSpAppraisalFormT appraisalFormType)
        {
            switch (appraisalFormType)
            {
                case E_sSpAppraisalFormT.Blank:
                    return PropertyValuationFormType.Undefined;
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return PropertyValuationFormType.UniformResidentialAppraisalReport;
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                    return PropertyValuationFormType.ManufacturedHomeAppraisalReport;
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                    return PropertyValuationFormType.AppraisalUpdateAndOrCompletionReport;
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return PropertyValuationFormType.SmallResidentialIncomePropertyAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormType.IndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormType.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                    return PropertyValuationFormType.OneUnitResidentialAppraisalFieldReviewReport;
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                    return PropertyValuationFormType.TwoToFourUnitResidentialAppraisal;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return PropertyValuationFormType.ExteriorOnlyInspectionResidentialAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormType.IndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormType.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return PropertyValuationFormType.DesktopUnderwriterPropertyInspectionReport;
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                    return PropertyValuationFormType.LoanProspectorConditionAndMarketability;
                case E_sSpAppraisalFormT.Other:
                    return PropertyValuationFormType.Other;
                default:
                    throw new UnhandledEnumException(appraisalFormType);
            }
        }

        private PropertyValuationMethodType ToMismo(E_sSpValuationMethodT sSpValuationMethodT)
        {
            switch (sSpValuationMethodT)
            {
                case E_sSpValuationMethodT.LeaveBlank:
                    return PropertyValuationMethodType.Undefined;
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return PropertyValuationMethodType.AutomatedValuationModel;
                case E_sSpValuationMethodT.DesktopAppraisal:
                    return PropertyValuationMethodType.DesktopAppraisal;
                case E_sSpValuationMethodT.DriveBy:
                    return PropertyValuationMethodType.DriveBy;
                case E_sSpValuationMethodT.FullAppraisal:
                    return PropertyValuationMethodType.FullAppraisal;
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return PropertyValuationMethodType.PriorAppraisalUsed;
                case E_sSpValuationMethodT.None:
                    return PropertyValuationMethodType.None;
                case E_sSpValuationMethodT.FieldReview:
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.Other:
                    return PropertyValuationMethodType.Other;
                default:
                    throw new UnhandledEnumException(sSpValuationMethodT);
            }
            throw new NotImplementedException();
        }

        private ProposedHousingExpense CreateProposedHousingExpense()
        {
            ProposedHousingExpense proposedHousingExpense = new ProposedHousingExpense();

            // proposedHousingExpense.HousingExpenseType = ;
            // proposedHousingExpense.HousingExpenseTypeOtherDescription = ;
            // proposedHousingExpense.ProposedHousingExpensePaymentAmount = ;
            // proposedHousingExpense.SequenceNumber = ;

            return proposedHousingExpense;
        }

        private ProposedHousingExpenses CreateProposedHousingExpenses()
        {
            ProposedHousingExpenses proposedHousingExpenses = new ProposedHousingExpenses();

            // proposedHousingExpenses.ProposedHousingExpenseList.Add(CreateProposedHousingExpense());

            return proposedHousingExpenses;
        }

        private PurchaseCredit CreatePurchaseCredit()
        {
            PurchaseCredit purchaseCredit = new PurchaseCredit();

            // purchaseCredit.PurchaseCreditAmount = ;
            // purchaseCredit.PurchaseCreditSourceType = ;
            // purchaseCredit.PurchaseCreditSourceTypeOtherDescription = ;
            // purchaseCredit.PurchaseCreditType = ;
            // purchaseCredit.PurchaseCreditTypeOtherDescription = ;
            // purchaseCredit.SequenceNumber = ;

            return purchaseCredit;
        }

        private PurchaseCredits CreatePurchaseCredits()
        {
            PurchaseCredits purchaseCredits = new PurchaseCredits();

            // purchaseCredits.PurchaseCreditList.Add(CreatePurchaseCredit());

            return purchaseCredits;
        }

        private Qualification CreateQualification(CPageData dataLoan)
        {
            Qualification qualification = new Qualification();

            CAppData dataApp = dataLoan.GetAppData(0);
            qualification.BorrowerReservesMonthlyPaymentCount = (dataLoan.sTotalScoreReserves > 999 ? 999 : dataLoan.sTotalScoreReserves).ToString();
            // qualification.HousingExpenseRatioPercent = ;
            // qualification.ProjectedReservesAmount = ;
            // qualification.QualifyingPaymentAmount = ;
            // qualification.QualifyingRatePercent = ;
            // qualification.QualifyingRateType = ;
            // qualification.QualifyingRateTypeOtherDescription = ;
            // qualification.SummaryInterestedPartyContributionsPercent = ;
            // qualification.TotalDebtExpenseRatioPercent = ;
            qualification.TotalLiabilitiesMonthlyPaymentAmount = TruncateMoneyAmount(dataLoan.sTransmTotMonPmt_rep);
            // qualification.TotalMonthlyCurrentHousingExpenseAmount = ;
            qualification.TotalMonthlyIncomeAmount = TruncateMoneyAmount(dataLoan.sLTotI_rep);
            qualification.TotalMonthlyProposedHousingExpenseAmount = TruncateMoneyAmount(dataLoan.sTransmProTotHExp_rep);
            // qualification.TotalVerifiedReservesAmount = ;
            // qualification.VerifiedAssetsTotalAmount = ;

            return qualification;
        }

        private RateOrPaymentChangeOccurrence CreateRateOrPaymentChangeOccurrence()
        {
            RateOrPaymentChangeOccurrence rateOrPaymentChangeOccurrence = new RateOrPaymentChangeOccurrence();

            // rateOrPaymentChangeOccurrence.AdjustmentChangeEffectiveDueDate = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeIndexRatePercent = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeInterestRatePercent = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeOccurrenceExtendedTermMonthsCount = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeOccurrencePassThroughRatePercent = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeOccurrenceType = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeOccurrenceTypeOtherDescription = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangePrincipalAndInterestPaymentAmount = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeProjectedPrincipalBalanceAmount = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeServiceFeeAmount = ;
            // rateOrPaymentChangeOccurrence.AdjustmentChangeServiceFeeRatePercent = ;
            // rateOrPaymentChangeOccurrence.AdjustmentRuleType = ;
            // rateOrPaymentChangeOccurrence.BalloonResetDate = ;
            // rateOrPaymentChangeOccurrence.CarryoverRatePercent = ;
            // rateOrPaymentChangeOccurrence.ConvertibleStatusType = ;
            // rateOrPaymentChangeOccurrence.LastRateChangeNotificationDate = ;
            // rateOrPaymentChangeOccurrence.LatestConversionEffectiveDate = ;
            // rateOrPaymentChangeOccurrence.NextConversionOptionEffectiveDate = ;
            // rateOrPaymentChangeOccurrence.NextConversionOptionNoticeDate = ;
            // rateOrPaymentChangeOccurrence.NextDemandConversionOptionNoticeDate = ;
            // rateOrPaymentChangeOccurrence.NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate = ;
            // rateOrPaymentChangeOccurrence.NextIgnoreRateAdjustmentCapsDate = ;
            // rateOrPaymentChangeOccurrence.NextPrincipalAndInterestPaymentChangeEffectiveDate = ;
            // rateOrPaymentChangeOccurrence.NextRateAdjustmentEffectiveDate = ;
            // rateOrPaymentChangeOccurrence.NextRateAdjustmentEffectiveNoticeDate = ;
            // rateOrPaymentChangeOccurrence.RateAdjustmentPercent = ;
            // rateOrPaymentChangeOccurrence.ServicerARMPlanIdentifier = ;
            // rateOrPaymentChangeOccurrence.SequenceNumber = ;

            return rateOrPaymentChangeOccurrence;
        }

        private RateOrPaymentChangeOccurrences CreateRateOrPaymentChangeOccurrences()
        {
            RateOrPaymentChangeOccurrences rateOrPaymentChangeOccurrences = new RateOrPaymentChangeOccurrences();

            // rateOrPaymentChangeOccurrences.RateOrPaymentChangeOccurrenceList.Add(CreateRateOrPaymentChangeOccurrence());

            return rateOrPaymentChangeOccurrences;
        }

        private RealEstateAgent CreateRealEstateAgent()
        {
            RealEstateAgent realEstateAgent = new RealEstateAgent();

            // realEstateAgent.RealEstateAgentType = ;
            // realEstateAgent.RealEstateAgentTypeOtherDescription = ;

            return realEstateAgent;
        }

        private RecordingEndorsement CreateRecordingEndorsement()
        {
            RecordingEndorsement recordingEndorsement = new RecordingEndorsement();

            // recordingEndorsement.RecordingEndorsementExemptions = CreateRecordingEndorsementExemptions();
            // recordingEndorsement.RecordingEndorsementFees = CreateRecordingEndorsementFees();
            // recordingEndorsement.RecordingEndorsementInformation = CreateRecordingEndorsementInformation();
            // recordingEndorsement.SequenceNumber = ;

            return recordingEndorsement;
        }

        private RecordingEndorsements CreateRecordingEndorsements()
        {
            RecordingEndorsements recordingEndorsements = new RecordingEndorsements();

            // recordingEndorsements.RecordingEndorsementList.Add(CreateRecordingEndorsement());

            return recordingEndorsements;
        }

        private RecordingEndorsementExemption CreateRecordingEndorsementExemption()
        {
            RecordingEndorsementExemption recordingEndorsementExemption = new RecordingEndorsementExemption();

            // recordingEndorsementExemption.RecordingEndorsementExemptionAmount = ;
            // recordingEndorsementExemption.RecordingEndorsementExemptionDescription = ;
            // recordingEndorsementExemption.SequenceNumber = ;

            return recordingEndorsementExemption;
        }

        private RecordingEndorsementExemptions CreateRecordingEndorsementExemptions()
        {
            RecordingEndorsementExemptions recordingEndorsementExemptions = new RecordingEndorsementExemptions();

            // recordingEndorsementExemptions.RecordingEndorsementExemptionList.Add(CreateRecordingEndorsementExemption());
            // recordingEndorsementExemptions.RecordingEndorsementExemptionsSummary = CreateRecordingEndorsementExemptionsSummary();

            return recordingEndorsementExemptions;
        }

        private RecordingEndorsementExemptionsSummary CreateRecordingEndorsementExemptionsSummary()
        {
            RecordingEndorsementExemptionsSummary recordingEndorsementExemptionsSummary = new RecordingEndorsementExemptionsSummary();

            // recordingEndorsementExemptionsSummary.RecordingEndorsementExemptionsTotalAmount = ;

            return recordingEndorsementExemptionsSummary;
        }

        private RecordingEndorsementFee CreateRecordingEndorsementFee()
        {
            RecordingEndorsementFee recordingEndorsementFee = new RecordingEndorsementFee();

            // recordingEndorsementFee.RecordingEndorsementFeeAmount = ;
            // recordingEndorsementFee.RecordingEndorsementFeeDescription = ;
            // recordingEndorsementFee.SequenceNumber = ;

            return recordingEndorsementFee;
        }

        private RecordingEndorsementFees CreateRecordingEndorsementFees()
        {
            RecordingEndorsementFees recordingEndorsementFees = new RecordingEndorsementFees();

            // recordingEndorsementFees.RecordingEndorsementFeeList.Add(CreateRecordingEndorsementFee());
            // recordingEndorsementFees.RecordingEndorsementFeesSummary = CreateRecordingEndorsementFeesSummary();

            return recordingEndorsementFees;
        }

        private RecordingEndorsementFeesSummary CreateRecordingEndorsementFeesSummary()
        {
            RecordingEndorsementFeesSummary recordingEndorsementFeesSummary = new RecordingEndorsementFeesSummary();

            // recordingEndorsementFeesSummary.RecordingEndorsementFeesTotalAmount = ;

            return recordingEndorsementFeesSummary;
        }

        private RecordingEndorsementField CreateRecordingEndorsementField()
        {
            RecordingEndorsementField recordingEndorsementField = new RecordingEndorsementField();

            // recordingEndorsementField.RecordingEndorsementFieldReference = CreateRecordingEndorsementFieldReference();

            return recordingEndorsementField;
        }

        private RecordingEndorsementInformation CreateRecordingEndorsementInformation()
        {
            RecordingEndorsementInformation recordingEndorsementInformation = new RecordingEndorsementInformation();

            // recordingEndorsementInformation.CountyOfRecordationName = ;
            // recordingEndorsementInformation.FirstPageNumberIdentifier = ;
            // recordingEndorsementInformation.InstrumentNumberIdentifier = ;
            // recordingEndorsementInformation.LastPageNumberIdentifier = ;
            // recordingEndorsementInformation.OfficeOfRecordationType = ;
            // recordingEndorsementInformation.OfficeOfRecordationTypeOtherDescription = ;
            // recordingEndorsementInformation.RecordedDatetime = ;
            // recordingEndorsementInformation.RecordingEndorsementIdentifier = ;
            // recordingEndorsementInformation.RecordingEndorsementType = ;
            // recordingEndorsementInformation.RecordingEndorsementTypeOtherDescription = ;
            // recordingEndorsementInformation.RecordingOfficersName = ;
            // recordingEndorsementInformation.RecordingPagesCount = ;
            // recordingEndorsementInformation.StateOfRecordationName = ;
            // recordingEndorsementInformation.VolumeIdentifier = ;
            // recordingEndorsementInformation.VolumeType = ;
            // recordingEndorsementInformation.VolumeTypeOtherDescription = ;

            return recordingEndorsementInformation;
        }

        private RecordingError CreateRecordingError()
        {
            RecordingError recordingError = new RecordingError();

            // recordingError.ErrorXPath = ;
            // recordingError.RecordingErrorDescription = ;
            // recordingError.RecordingErrorSeverityType = ;
            // recordingError.RecordingErrorSeverityTypeOtherDescription = ;
            // recordingError.RecordingErrorType = ;
            // recordingError.RecordingErrorTypeOtherDescription = ;
            // recordingError.SequenceNumber = ;

            return recordingError;
        }

        private RecordingErrors CreateRecordingErrors()
        {
            RecordingErrors recordingErrors = new RecordingErrors();

            // recordingErrors.RecordingErrorList.Add(CreateRecordingError());

            return recordingErrors;
        }

        private RecordingTransactionIdentifier CreateRecordingTransactionIdentifier()
        {
            RecordingTransactionIdentifier recordingTransactionIdentifier = new RecordingTransactionIdentifier();

            // recordingTransactionIdentifier.RecordingTransactionIdentifierType = ;
            // recordingTransactionIdentifier.RecordingTransactionIdentifierTypeOtherDescription = ;
            // recordingTransactionIdentifier._RecordingTransactionIdentifier = ;

            return recordingTransactionIdentifier;
        }

        private Reference CreateReference()
        {
            Reference reference = new Reference();

            // reference.LocationURI = ;
            // reference.LocationXPath = ;
            // reference.OriginalCreatorDigestValue = ;

            return reference;
        }

        private Refinance CreateRefinance(CPageData dataLoan)
        {
            Refinance refinance = new Refinance();

            if (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutDebtConsolidation
                || dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutHomeImprovement
                || dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutOther)
            {
                refinance.RefinanceCashOutDeterminationType = RefinanceCashOutDeterminationType.CashOut;
            }
            else if (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance
                || dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutFREOwnedRefinance
                || dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutOther
                || dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutStreamlinedRefinance
                || (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.ChangeInRateTerm && dataLoan.sLT != E_sLT.Conventional))
            {
                refinance.RefinanceCashOutDeterminationType = RefinanceCashOutDeterminationType.NoCashOut;
            }
            else if (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutLimited
                || (dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.ChangeInRateTerm && dataLoan.sLT == E_sLT.Conventional))
            {
                refinance.RefinanceCashOutDeterminationType = RefinanceCashOutDeterminationType.LimitedCashOut;
            }
            // refinance.CurrentFirstMortgageHolderType = ;
            // refinance.CurrentFirstMortgageHolderTypeOtherDescription = ;
            // refinance.EstimatedCashOutAmount = ;
            // refinance.NonStructuralAlterationsConventionalAmount = ;
            // refinance.RefinanceAssetsAcquiredAmount = ;
            if (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout)
            {
                refinance.RefinanceCashOutAmount = dataLoan.sProdCashoutAmt_rep;
            }
            // refinance.RefinanceCashOutDeterminationType = ;
            // refinance.RefinanceCashOutPercent = ;
            // refinance.RefinanceDebtReductionAmount = ;
            // refinance.RefinanceHomeImprovementAmount = ;
            // refinance.RefinancePrimaryPurposeType = ;
            // refinance.RefinancePrimaryPurposeTypeOtherDescription = ;
            // refinance.SecondaryFinancingRefinanceIndicator = ;
            // refinance.StructuralAlterationsConventionalAmount = ;
            // refinance.TotalPriorLienPayoffAmount = ;
            // refinance.TotalPriorLienPrepaymentPenaltyAmount = ;

            return refinance;
        }

        private RegulatoryAgency CreateRegulatoryAgency()
        {
            RegulatoryAgency regulatoryAgency = new RegulatoryAgency();

            // regulatoryAgency.RegulatoryAgencyType = ;
            // regulatoryAgency.RegulatoryAgencyTypeOtherDescription = ;

            return regulatoryAgency;
        }

        private Rehabilitation CreateRehabilitation()
        {
            Rehabilitation rehabilitation = new Rehabilitation();

            // rehabilitation.RehabilitationLoanFundsAmount = ;
            // rehabilitation.RehabilitationCompletionDate = ;
            // rehabilitation.RehabilitationEstimatedPropertyValueAmount = ;
            // rehabilitation.RehabilitationContingencyPercent = ;

            return rehabilitation;
        }

        private Relationship CreateRelationship()
        {
            Relationship relationship = new Relationship();

            // relationship.SequenceNumber = ;

            return relationship;
        }

        private Relationships CreateRelationships()
        {
            Relationships relationships = new Relationships();

            // relationships.RelationshipList.Add(CreateRelationship());

            return relationships;
        }

        private RequestingParty CreateRequestingParty()
        {
            RequestingParty requestingParty = new RequestingParty();

            // requestingParty.InternalAccountIdentifier = ;
            // requestingParty.RequestingPartyBranchIdentifier = ;

            return requestingParty;
        }

        private Residence CreateResidence()
        {
            Residence residence = new Residence();

            // residence.Address = CreateAddress();
            // residence.Landlord = CreateLandlord();
            // residence.ResidenceDetail = CreateResidenceDetail();
            // residence.SequenceNumber = ;

            return residence;
        }

        private Residences CreateResidences()
        {
            Residences residences = new Residences();

            // residences.ResidenceList.Add(CreateResidence());

            return residences;
        }

        private ResidenceDetail CreateResidenceDetail()
        {
            ResidenceDetail residenceDetail = new ResidenceDetail();

            // residenceDetail.BorrowerResidencyBasisType = ;
            // residenceDetail.BorrowerResidencyDurationMonthsCount = ;
            // residenceDetail.BorrowerResidencyDurationYearsCount = ;
            // residenceDetail.BorrowerResidencyEndDate = ;
            // residenceDetail.BorrowerResidencyReportedDate = ;
            // residenceDetail.BorrowerResidencyStartDate = ;
            // residenceDetail.BorrowerResidencyType = ;

            return residenceDetail;
        }

        private Respa CreateRespa()
        {
            Respa respa = new Respa();

            // respa.RespaFees = CreateRespaFees();
            // respa.RespaServicingData = CreateRespaServicingData();
            // respa.RespaSummary = CreateRespaSummary();

            return respa;
        }

        private RespaFee CreateRespaFee()
        {
            RespaFee respaFee = new RespaFee();

            // respaFee.RespaFeeDetail = CreateRespaFeeDetail();
            // respaFee.RespaFeePayments = CreateRespaFeePayments();
            // respaFee.RespaFeeRequiredServiceProvider = CreateRespaFeeRequiredServiceProvider();
            // respaFee.RespaFeePaidTo = CreateRespaFeePaidTo();
            // respaFee.SequenceNumber = ;

            return respaFee;
        }

        private RespaFees CreateRespaFees()
        {
            RespaFees respaFees = new RespaFees();

            // respaFees.RespaFeeList.Add(CreateRespaFee());

            return respaFees;
        }

        private RespaFeeDetail CreateRespaFeeDetail()
        {
            RespaFeeDetail respaFeeDetail = new RespaFeeDetail();

            // respaFeeDetail.RESPAFeeItemDescription = ;
            // respaFeeDetail.RESPAFeePaidToType = ;
            // respaFeeDetail.RESPAFeePaidToTypeOtherDescription = ;
            // respaFeeDetail.RESPAFeePaymentFinancedIndicator = ;
            // respaFeeDetail.RESPAFeePercentBasisType = ;
            // respaFeeDetail.RESPAFeePercentBasisTypeOtherDescription = ;
            // respaFeeDetail.RESPAFeeRequiredProviderOfServiceIndicator = ;
            // respaFeeDetail.RESPAFeeResponsiblePartyType = ;
            // respaFeeDetail.RESPAFeeSpecifiedFixedAmount = ;
            // respaFeeDetail.RESPAFeeSpecifiedHUDLineNumber = ;
            // respaFeeDetail.RESPAFeeTotalAmount = ;
            // respaFeeDetail.RESPAFeeTotalPercent = ;
            // respaFeeDetail.RESPAFeeType = ;
            // respaFeeDetail.RESPAFeeTypeOtherDescription = ;
            // respaFeeDetail.RESPASectionClassificationType = ;

            return respaFeeDetail;
        }

        private RespaFeePaidTo CreateRespaFeePaidTo()
        {
            RespaFeePaidTo respaFeePaidTo = new RespaFeePaidTo();

            // respaFeePaidTo.Individual = CreateIndividual();
            // respaFeePaidTo.LegalEntity = CreateLegalEntity();
            // respaFeePaidTo.Address = CreateAddress();

            return respaFeePaidTo;
        }

        private RespaFeePayment CreateRespaFeePayment()
        {
            RespaFeePayment respaFeePayment = new RespaFeePayment();

            // respaFeePayment.RESPAFeePaymentAllowableFHAClosingCostIndicator = ;
            // respaFeePayment.RESPAFeePaymentAmount = ;
            // respaFeePayment.RESPAFeePaymentCollectedByType = ;
            // respaFeePayment.RESPAFeePaymentIncludedInAPRIndicator = ;
            // respaFeePayment.RESPAFeePaymentIncludedInStateHighCostIndicator = ;
            // respaFeePayment.RESPAFeePaymentNetDueAmount = ;
            // respaFeePayment.RESPAFeePaymentPaidByType = ;
            // respaFeePayment.RESPAFeePaymentPaidByTypeThirdPartyName = ;
            // respaFeePayment.RESPAFeePaymentPaidOutsideOfClosingIndicator = ;
            // respaFeePayment.RESPAFeePaymentPercent = ;
            // respaFeePayment.RESPAFeePaymentProcessType = ;
            // respaFeePayment.RESPAFeePaymentRefundableAmount = ;
            // respaFeePayment.RESPAFeePaymentRefundableDescription = ;
            // respaFeePayment.RESPAFeePaymentRefundableIndicator = ;
            // respaFeePayment.RESPAFeePaymentSection32Indicator = ;
            // respaFeePayment.SequenceNumber = ;

            return respaFeePayment;
        }

        private RespaFeePayments CreateRespaFeePayments()
        {
            RespaFeePayments respaFeePayments = new RespaFeePayments();

            // respaFeePayments.RespaFeePaymentList.Add(CreateRespaFeePayment());

            return respaFeePayments;
        }

        private RespaFeeRequiredServiceProvider CreateRespaFeeRequiredServiceProvider()
        {
            RespaFeeRequiredServiceProvider respaFeeRequiredServiceProvider = new RespaFeeRequiredServiceProvider();

            // respaFeeRequiredServiceProvider.Individual = CreateIndividual();
            // respaFeeRequiredServiceProvider.LegalEntity = CreateLegalEntity();
            // respaFeeRequiredServiceProvider.Address = CreateAddress();
            // respaFeeRequiredServiceProvider.RespaFeeRequiredServiceProviderDetail = CreateRespaFeeRequiredServiceProviderDetail();

            return respaFeeRequiredServiceProvider;
        }

        private RespaFeeRequiredServiceProviderDetail CreateRespaFeeRequiredServiceProviderDetail()
        {
            RespaFeeRequiredServiceProviderDetail respaFeeRequiredServiceProviderDetail = new RespaFeeRequiredServiceProviderDetail();

            // respaFeeRequiredServiceProviderDetail.RESPAFeeRequiredServiceProviderNatureOfRelationshipDescription = ;

            return respaFeeRequiredServiceProviderDetail;
        }

        private RespaServicingData CreateRespaServicingData()
        {
            RespaServicingData respaServicingData = new RespaServicingData();

            // respaServicingData.RESPAServicingDataDoNotServiceIndicator = ;
            // respaServicingData.RESPAServicingDataMayAssignServicingIndicator = ;
            // respaServicingData.RESPAServicingDataWillServiceIndicator = ;

            return respaServicingData;
        }

        private RespaSummary CreateRespaSummary()
        {
            RespaSummary respaSummary = new RespaSummary();

            // respaSummary.RespaSummaryDetail = CreateRespaSummaryDetail();
            // respaSummary.RespaSummaryTotalFeesPaidBy = CreateRespaSummaryTotalFeesPaidBy();
            // respaSummary.RespaSummaryTotalFeesPaidTo = CreateRespaSummaryTotalFeesPaidTo();

            return respaSummary;
        }

        private RespaSummaryDetail CreateRespaSummaryDetail()
        {
            RespaSummaryDetail respaSummaryDetail = new RespaSummaryDetail();

            // respaSummaryDetail.APRPercent = ;
            // respaSummaryDetail.RESPASummaryDisclosedTotalSalesPriceAmount = ;
            // respaSummaryDetail.RESPASummaryTotalAmountFinancedAmount = ;
            // respaSummaryDetail.RESPASummaryTotalAPRFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalDepositedReservesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalFilingRecordingFeeAmount = ;
            // respaSummaryDetail.RESPASummaryTotalFinanceChargeAmount = ;
            // respaSummaryDetail.RESPASummaryTotalLoanOriginationFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalNetBorrowerFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalNetProceedsForFundingAmount = ;
            // respaSummaryDetail.RESPASummaryTotalNetSellerFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalNonAPRFeesAmount = ;
            // respaSummaryDetail.RESPASummaryTotalOfAllPaymentsAmount = ;
            // respaSummaryDetail.RESPASummaryTotalPaidToOthersAmount = ;
            // respaSummaryDetail.RESPASummaryTotalPrepaidFinanceChargeAmount = ;

            return respaSummaryDetail;
        }

        private RespaSummaryTotalFeesPaidBy CreateRespaSummaryTotalFeesPaidBy()
        {
            RespaSummaryTotalFeesPaidBy respaSummaryTotalFeesPaidBy = new RespaSummaryTotalFeesPaidBy();

            // respaSummaryTotalFeesPaidBy.RESPASummaryTotalFeesPaidByType = ;
            // respaSummaryTotalFeesPaidBy.RESPASummaryTotalFeesPaidByTypeAmount = ;
            // respaSummaryTotalFeesPaidBy.RESPASummaryTotalFeesPaidByTypeOtherDescription = ;

            return respaSummaryTotalFeesPaidBy;
        }

        private RespaSummaryTotalFeesPaidTo CreateRespaSummaryTotalFeesPaidTo()
        {
            RespaSummaryTotalFeesPaidTo respaSummaryTotalFeesPaidTo = new RespaSummaryTotalFeesPaidTo();

            // respaSummaryTotalFeesPaidTo.RESPASummaryTotalFeesPaidToType = ;
            // respaSummaryTotalFeesPaidTo.RESPASummaryTotalFeesPaidToTypeAmount = ;
            // respaSummaryTotalFeesPaidTo.RESPASummaryTotalFeesPaidToTypeOtherDescription = ;

            return respaSummaryTotalFeesPaidTo;
        }

        private RespondingParty CreateRespondingParty()
        {
            RespondingParty respondingParty = new RespondingParty();

            // respondingParty.RespondingPartyTransactionIdentifier = ;

            return respondingParty;
        }

        private ReturnTo CreateReturnTo()
        {
            ReturnTo returnTo = new ReturnTo();

            // returnTo.PreferredResponses = CreatePreferredResponses();

            return returnTo;
        }

        private ReviewAppraiser CreateReviewAppraiser()
        {
            ReviewAppraiser reviewAppraiser = new ReviewAppraiser();

            // reviewAppraiser.AppraiserLicense = CreateAppraiserLicense();
            // reviewAppraiser.ReviewAppraiserDetail = CreateReviewAppraiserDetail();

            return reviewAppraiser;
        }

        private ReviewAppraiserDetail CreateReviewAppraiserDetail()
        {
            ReviewAppraiserDetail reviewAppraiserDetail = new ReviewAppraiserDetail();

            // reviewAppraiserDetail.ReviewAppraiserCompanyName = ;

            return reviewAppraiserDetail;
        }

        private Rider CreateRider()
        {
            Rider rider = new Rider();

            // rider.AdjustableRateRiderIndicator = ;
            // rider.BalloonRiderIndicator = ;
            // rider.BiweeklyPaymentRiderIndicator = ;
            // rider.CondominiumPUDDeclarationsDescription = ;
            // rider.CondominiumRiderIndicator = ;
            // rider.GraduatedPaymentRiderIndicator = ;
            // rider.GrowingEquityRiderIndicator = ;
            // rider.NonOwnerOccupancyRiderIndicator = ;
            // rider.OneToFourFamilyRiderIndicator = ;
            // rider.OtherRiderDescription = ;
            // rider.OtherRiderIndicator = ;
            // rider.PUDRiderIndicator = ;
            // rider.RateImprovementRiderIndicator = ;
            // rider.RehabilitationLoanRiderIndicator = ;
            // rider.SecondHomeRiderIndicator = ;
            // rider.VARiderIndicator = ;

            return rider;
        }

        private RightToCancel CreateRightToCancel()
        {
            RightToCancel rightToCancel = new RightToCancel();

            // rightToCancel.RightToCancelDetail = CreateRightToCancelDetail();
            // rightToCancel.RightToCancelNotification = CreateRightToCancelNotification();

            return rightToCancel;
        }

        private RightToCancelDetail CreateRightToCancelDetail()
        {
            RightToCancelDetail rightToCancelDetail = new RightToCancelDetail();

            // rightToCancelDetail.RequestToRescindTransactionDate = ;

            return rightToCancelDetail;
        }

        private RightToCancelNotification CreateRightToCancelNotification()
        {
            RightToCancelNotification rightToCancelNotification = new RightToCancelNotification();

            // rightToCancelNotification.Address = CreateAddress();
            // rightToCancelNotification.Name = CreateName();

            return rightToCancelNotification;
        }

        private Role CreateRole()
        {
            Role role = new Role();

            // role.Appraiser = CreateAppraiser();
            // role.AppraiserSupervisor = CreateAppraiserSupervisor();
            // role.Borrower = CreateBorrower();
            // role.Builder = CreateBuilder();
            // role.ClosingAgent = CreateClosingAgent();
            // role.FulfillmentParty = CreateFulfillmentParty();
            // role.Lender = CreateLender();
            // role.LienHolder = CreateLienHolder();
            // role.LoanOriginator = CreateLoanOriginator();
            // role.LossPayee = CreateLossPayee();
            // role.MortgageBroker = CreateMortgageBroker();
            // role.Notary = CreateNotary();
            // role.Payee = CreatePayee();
            // role.PowerOfAttorney = CreatePowerOfAttorney();
            // role.PropertyOwner = CreatePropertyOwner();
            // role.PropertySeller = CreatePropertySeller();
            // role.RealEstateAgent = CreateRealEstateAgent();
            // role.RegulatoryAgency = CreateRegulatoryAgency();
            // role.RequestingParty = CreateRequestingParty();
            // role.RespondingParty = CreateRespondingParty();
            // role.ReturnTo = CreateReturnTo();
            // role.ReviewAppraiser = CreateReviewAppraiser();
            // role.Servicer = CreateServicer();
            // role.ServicingTransferor = CreateServicingTransferor();
            // role.SubmittingParty = CreateSubmittingParty();
            // role.Trust = CreateTrust();
            // role.Trustee = CreateTrustee();
            // role.RoleDetail = CreateRoleDetail();
            // role.SequenceNumber = ;

            return role;
        }

        private global::ULDD.Roles CreateRoles()
        {
            global::ULDD.Roles roles = new global::ULDD.Roles();

            // roles.PartyRoleIdentifiers = CreatePartyRoleIdentifiers();
            // roles.RoleList.Add(CreateRole());

            return roles;
        }

        private RoleDetail CreateRoleDetail()
        {
            RoleDetail roleDetail = new RoleDetail();

            // roleDetail.PartyRoleType = ;
            // roleDetail.PartyRoleTypeOtherDescription = ;

            return roleDetail;
        }

        private SalesContract CreateSalesContract()
        {
            SalesContract salesContract = new SalesContract();

            // salesContract.SalesConcessionDescription = ;
            // salesContract.SalesConcessionIndicator = ;
            // salesContract.SalesConditionsOfSaleDescription = ;
            // salesContract.SalesContractAmount = ;
            // salesContract.SalesContractDate = ;
            // salesContract.SalesContractInvoiceRevewCommentDescription = ;
            // salesContract.SalesContractInvoiceReviewedIndicator = ;
            // salesContract.SalesContractRetailerName = ;
            // salesContract.SalesContractReviewCommentDescription = ;
            // salesContract.SalesContractReviewedIndicator = ;
            // salesContract.SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator = ;
            // salesContract.SellerIsOwnerIndicator = ;
            // salesContract.SubjectDataSourceDescription = ;

            return salesContract;
        }

        private SalesHistories CreateSalesHistories()
        {
            SalesHistories salesHistories = new SalesHistories();

            // salesHistories.SalesHistoryList.Add(CreateSalesHistory());

            return salesHistories;
        }

        private SalesHistory CreateSalesHistory()
        {
            SalesHistory salesHistory = new SalesHistory();

            // salesHistory.LoanTermDescription = ;
            // salesHistory.PricePerSquareFootAmount = ;
            // salesHistory.PropertySalesAmount = ;
            // salesHistory.PropertySalesDate = ;
            // salesHistory.SalesConcessionAmount = ;
            // salesHistory.SalesConcessionDescription = ;
            // salesHistory.SalesHistoryAnalysisOfAgreementDescription = ;
            // salesHistory.SalesHistoryParcelSplitDescription = ;
            // salesHistory.SalesHistoryPendingRecordIndicator = ;
            // salesHistory.SalesHistoryStampAmount = ;
            // salesHistory.SalesHistoryTransferredOwnershipPercent = ;
            // salesHistory.SalesHistoryType = ;
            // salesHistory.SalesHistoryTypeOtherDescription = ;
            // salesHistory.SequenceNumber = ;

            return salesHistory;
        }

        private SecurityInstrument CreateSecurityInstrument()
        {
            SecurityInstrument securityInstrument = new SecurityInstrument();

            // securityInstrument.Rider = CreateRider();
            // securityInstrument.SecurityInstrumentDetail = CreateSecurityInstrumentDetail();

            return securityInstrument;
        }

        private SecurityInstrumentDetail CreateSecurityInstrumentDetail()
        {
            SecurityInstrumentDetail securityInstrumentDetail = new SecurityInstrumentDetail();

            // securityInstrumentDetail.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = ;
            // securityInstrumentDetail.DefaultApplicationFeesAmount = ;
            // securityInstrumentDetail.DefaultClosingPreparationFeesAmount = ;
            // securityInstrumentDetail.DefaultLendingInstitutionPostOfficeBoxIdentifier = ;
            // securityInstrumentDetail.DocumentDrawnInName = ;
            // securityInstrumentDetail.NameDocumentsDrawnInType = ;
            // securityInstrumentDetail.NameDocumentsDrawnInTypeOtherDescription = ;
            // securityInstrumentDetail.RegistryOperatorName = ;
            // securityInstrumentDetail.SecurityInstrumentAssumptionFeeAmount = ;
            // securityInstrumentDetail.SecurityInstrumentAttorneyFeeMinimumAmount = ;
            // securityInstrumentDetail.SecurityInstrumentAttorneyFeePercent = ;
            // securityInstrumentDetail.SecurityInstrumentCertifyingAttorneyName = ;
            // securityInstrumentDetail.SecurityInstrumentMaximumPrincipalIndebtednessAmount = ;
            // securityInstrumentDetail.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentNoteHolderName = ;
            // securityInstrumentDetail.SecurityInstrumentNoticeOfConfidentialityRightsDescription = ;
            // securityInstrumentDetail.SecurityInstrumentOtherFeesAmount = ;
            // securityInstrumentDetail.SecurityInstrumentOtherFeesDescription = ;
            // securityInstrumentDetail.SecurityInstrumentOweltyOfPartitionIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentPersonAuthorizedToReleaseLienName = ;
            // securityInstrumentDetail.SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue = ;
            // securityInstrumentDetail.SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription = ;
            // securityInstrumentDetail.SecurityInstrumentPurchaseMoneyIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentRecordingRequestedByName = ;
            // securityInstrumentDetail.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentSignerForRegistersOfficeName = ;
            // securityInstrumentDetail.SecurityInstrumentTaxSerialNumberIdentifier = ;
            // securityInstrumentDetail.SecurityInstrumentTrusteeFeePercent = ;
            // securityInstrumentDetail.SecurityInstrumentVendorsLienDescription = ;
            // securityInstrumentDetail.SecurityInstrumentVendorsLienIndicator = ;
            // securityInstrumentDetail.SecurityInstrumentVestingDescription = ;

            return securityInstrumentDetail;
        }

        private Service CreateService()
        {
            Service service = new Service();

            // service.Aus = CreateAus();
            // service.Credit = CreateCredit();
            // service.DataChange = CreateDataChange();
            // service.DocumentManagement = CreateDocumentManagement();
            // service.Flood = CreateFlood();
            // service.Fraud = CreateFraud();
            // service.LoanDelivery = CreateLoanDelivery();
            // service.MersService = CreateMersService();
            // service.Mi = CreateMi();
            // service.Pria = CreatePria();
            // service.Tax = CreateTax();
            // service.Title = CreateTitle();
            // service.Valuation = CreateValuation();
            // service.ServicePayments = CreateServicePayments();
            // service.ServiceProduct = CreateServiceProduct();
            // service.ServiceProductFulfillment = CreateServiceProductFulfillment();
            // service.Statuses = CreateStatuses();
            // service.SequenceNumber = ;

            return service;
        }

        private Servicer CreateServicer()
        {
            Servicer servicer = new Servicer();

            // servicer.ServicerRoleEffectiveDate = ;
            // servicer.ServicerType = ;
            // servicer.ServicerTypeOtherDescription = ;
            // servicer.TransferOfServicingDisclosureType = ;
            // servicer.TransferOfServicingTypeOtherDescription = ;

            return servicer;
        }

        private ServicerQualifiedWrittenRequestMailTo CreateServicerQualifiedWrittenRequestMailTo()
        {
            ServicerQualifiedWrittenRequestMailTo servicerQualifiedWrittenRequestMailTo = new ServicerQualifiedWrittenRequestMailTo();

            // servicerQualifiedWrittenRequestMailTo.Address = CreateAddress();

            return servicerQualifiedWrittenRequestMailTo;
        }

        private ServicerReporting CreateServicerReporting()
        {
            ServicerReporting servicerReporting = new ServicerReporting();

            // servicerReporting.InvestorReportingCycleDate = ;
            // servicerReporting.InvestorReportingCycleDescription = ;
            // servicerReporting.PortfolioDelinquencyIndicator = ;

            return servicerReporting;
        }

        private Services CreateServices()
        {
            Services services = new Services();

            // services.ServiceList.Add(CreateService());

            return services;
        }

        private ServicePayment CreateServicePayment()
        {
            ServicePayment servicePayment = new ServicePayment();

            // servicePayment.Address = CreateAddress();
            // servicePayment.ContactPoints = CreateContactPoints();
            // servicePayment.Name = CreateName();
            // servicePayment.ServicePaymentDetail = CreateServicePaymentDetail();
            // servicePayment.SequenceNumber = ;

            return servicePayment;
        }

        private ServicePayments CreateServicePayments()
        {
            ServicePayments servicePayments = new ServicePayments();

            // servicePayments.ServicePaymentList.Add(CreateServicePayment());

            return servicePayments;
        }

        private ServicePaymentDetail CreateServicePaymentDetail()
        {
            ServicePaymentDetail servicePaymentDetail = new ServicePaymentDetail();

            // servicePaymentDetail.InternalAccountIdentifier = ;
            // servicePaymentDetail.ServicePaymentAccountIdentifier = ;
            // servicePaymentDetail.ServicePaymentAmount = ;
            // servicePaymentDetail.ServicePaymentCreditAccountExpirationDate = ;
            // servicePaymentDetail.ServicePaymentCreditMethodType = ;
            // servicePaymentDetail.ServicePaymentCreditMethodTypeOtherDescription = ;
            // servicePaymentDetail.ServicePaymentMethodType = ;
            // servicePaymentDetail.ServicePaymentMethodTypeOtherDescription = ;
            // servicePaymentDetail.ServicePaymentOnAccountIdentifier = ;
            // servicePaymentDetail.ServicePaymentOnAccountMaximumDebitAmount = ;
            // servicePaymentDetail.ServicePaymentOnAccountMinimumBalanceAmount = ;
            // servicePaymentDetail.ServicePaymentReferenceIdentifier = ;
            // servicePaymentDetail.ServicePaymentSecondaryCreditAccountIdentifier = ;

            return servicePaymentDetail;
        }

        private ServiceProduct CreateServiceProduct()
        {
            ServiceProduct serviceProduct = new ServiceProduct();

            // serviceProduct.ServiceProductRequest = CreateServiceProductRequest();
            // serviceProduct.ServiceProductResponse = CreateServiceProductResponse();

            return serviceProduct;
        }

        private ServiceProductDetail CreateServiceProductDetail()
        {
            ServiceProductDetail serviceProductDetail = new ServiceProductDetail();

            // serviceProductDetail.ServiceProductDescription = ;
            // serviceProductDetail.ServiceProductIdentifier = ;

            return serviceProductDetail;
        }

        private ServiceProductFulfillment CreateServiceProductFulfillment()
        {
            ServiceProductFulfillment serviceProductFulfillment = new ServiceProductFulfillment();

            // serviceProductFulfillment.ContactPoints = CreateContactPoints();
            // serviceProductFulfillment.ProductFulfillmentDetail = CreateProductFulfillmentDetail();

            return serviceProductFulfillment;
        }

        private ServiceProductFulfillmentDetail CreateServiceProductFulfillmentDetail()
        {
            ServiceProductFulfillmentDetail serviceProductFulfillmentDetail = new ServiceProductFulfillmentDetail();

            // serviceProductFulfillmentDetail.ServiceActualCompletionDate = ;
            // serviceProductFulfillmentDetail.ServiceActualPriceAmount = ;
            // serviceProductFulfillmentDetail.ServiceCancellationPriceAmount = ;
            // serviceProductFulfillmentDetail.ServiceEstimatedCompletionDate = ;
            // serviceProductFulfillmentDetail.ServiceEstimatedOffHoldDate = ;
            // serviceProductFulfillmentDetail.ServiceEstimatedPriceAmount = ;
            // serviceProductFulfillmentDetail.VendorOrderIdentifier = ;
            // serviceProductFulfillmentDetail.VendorTransactionIdentifier = ;

            return serviceProductFulfillmentDetail;
        }

        private ServiceProductName CreateServiceProductName()
        {
            ServiceProductName serviceProductName = new ServiceProductName();

            // serviceProductName.ServiceProductNameAddOns = CreateServiceProductNameAddOns();
            // serviceProductName.ServiceProductNameDetail = CreateServiceProductNameDetail();
            // serviceProductName.SequenceNumber = ;

            return serviceProductName;
        }

        private ServiceProductNames CreateServiceProductNames()
        {
            ServiceProductNames serviceProductNames = new ServiceProductNames();

            // serviceProductNames.ServiceProductNameList.Add(CreateServiceProductName());

            return serviceProductNames;
        }

        private ServiceProductNameAddOn CreateServiceProductNameAddOn()
        {
            ServiceProductNameAddOn serviceProductNameAddOn = new ServiceProductNameAddOn();

            // serviceProductNameAddOn.ServiceProductNameAddOnDescription = ;
            // serviceProductNameAddOn.ServiceProductNameAddOnIdentifier = ;
            // serviceProductNameAddOn.SequenceNumber = ;

            return serviceProductNameAddOn;
        }

        private ServiceProductNameAddOns CreateServiceProductNameAddOns()
        {
            ServiceProductNameAddOns serviceProductNameAddOns = new ServiceProductNameAddOns();

            // serviceProductNameAddOns.ServiceProductNameAddOnList.Add(CreateServiceProductNameAddOn());

            return serviceProductNameAddOns;
        }

        private ServiceProductNameDetail CreateServiceProductNameDetail()
        {
            ServiceProductNameDetail serviceProductNameDetail = new ServiceProductNameDetail();

            // serviceProductNameDetail.ServiceProductNameIdentifier = ;
            // serviceProductNameDetail.ServiceProductNameDescription = ;

            return serviceProductNameDetail;
        }

        private ServiceProductRequest CreateServiceProductRequest()
        {
            ServiceProductRequest serviceProductRequest = new ServiceProductRequest();

            // serviceProductRequest.ServiceProductNames = CreateServiceProductNames();
            // serviceProductRequest.ServiceProductDetail = CreateServiceProductDetail();

            return serviceProductRequest;
        }

        private ServiceProductResponse CreateServiceProductResponse()
        {
            ServiceProductResponse serviceProductResponse = new ServiceProductResponse();

            // serviceProductResponse.ServiceProductNames = CreateServiceProductNames();
            // serviceProductResponse.ServiceProductDetail = CreateServiceProductDetail();

            return serviceProductResponse;
        }

        private Servicing CreateServicing()
        {
            Servicing servicing = new Servicing();

            // servicing.CreditBureauReporting = CreateCreditBureauReporting();
            // servicing.DelinquencyNotification = CreateDelinquencyNotification();
            // servicing.DelinquencySummary = CreateDelinquencySummary();
            // servicing.DisclosureOnServicer = CreateDisclosureOnServicer();
            // servicing.MonetaryEvents = CreateMonetaryEvents();
            // servicing.MonetaryEventSummaries = CreateMonetaryEventSummaries();
            // servicing.ServicerQualifiedWrittenRequestMailTo = CreateServicerQualifiedWrittenRequestMailTo();
            // servicing.ServicingComments = CreateServicingComments();
            // servicing.ServicingDetail = CreateServicingDetail();
            // servicing.StopCodes = CreateStopCodes();

            return servicing;
        }

        private ServicingComment CreateServicingComment()
        {
            ServicingComment servicingComment = new ServicingComment();

            // servicingComment.ServicingCommentDatetime = ;
            // servicingComment.ServicingCommentSourceDescription = ;
            // servicingComment.ServicingCommentText = ;
            // servicingComment.ServicingCommentType = ;
            // servicingComment.ServicingCommentTypeOtherDescription = ;
            // servicingComment.SequenceNumber = ;

            return servicingComment;
        }

        private ServicingComments CreateServicingComments()
        {
            ServicingComments servicingComments = new ServicingComments();

            // servicingComments.ServicingCommentList.Add(CreateServicingComment());

            return servicingComments;
        }

        private ServicingDetail CreateServicingDetail()
        {
            ServicingDetail servicingDetail = new ServicingDetail();

            // servicingDetail.DefaultStatusType = ;
            // servicingDetail.ForeclosureStatusType = ;
            // servicingDetail.SFDMSAutomatedDefaultProcessingIdentifier = ;

            return servicingDetail;
        }

        private ServicingTransferor CreateServicingTransferor()
        {
            ServicingTransferor servicingTransferor = new ServicingTransferor();

            // servicingTransferor.ServicingTransferorInactiveIndicator = ;

            return servicingTransferor;
        }

        private Signatories CreateSignatories()
        {
            Signatories signatories = new Signatories();

            // signatories.SignatoryList.Add(CreateSignatory());

            return signatories;
        }

        private Signatory CreateSignatory()
        {
            Signatory signatory = new Signatory();

            // signatory.ElectronicSignature = CreateElectronicSignature();
            // signatory.Execution = CreateExecution();
            // signatory.SequenceNumber = ;

            return signatory;
        }

        private SkipPayment CreateSkipPayment()
        {
            SkipPayment skipPayment = new SkipPayment();

            // skipPayment.SkipPaymentActionType = ;
            // skipPayment.SkipPaymentActionTypeOtherDescription = ;
            // skipPayment.SkipPaymentInitialRestrictionTermMonthsCount = ;
            // skipPayment.SkippedPaymentCount = ;

            return skipPayment;
        }

        private SolicitationPreferences CreateSolicitationPreferences()
        {
            SolicitationPreferences solicitationPreferences = new SolicitationPreferences();

            // solicitationPreferences.CorporateAffiliateSharingAllowedIndicator = ;
            // solicitationPreferences.SolicitationByEmailAllowedIndicator = ;
            // solicitationPreferences.SolicitationByTelephoneAllowedIndicator = ;
            // solicitationPreferences.SolicitationByUSMailAllowedIndicator = ;

            return solicitationPreferences;
        }

        private SpouseInformation CreateSpouseInformation()
        {
            SpouseInformation spouseInformation = new SpouseInformation();

            // spouseInformation.Name = CreateName();

            return spouseInformation;
        }

        private StakeholderSignatureField CreateStakeholderSignatureField()
        {
            StakeholderSignatureField stakeholderSignatureField = new StakeholderSignatureField();

            // stakeholderSignatureField.SignatureAreaFieldReference = CreateSignatureAreaFieldReference();
            // stakeholderSignatureField.SignaturePresentationFieldReference = CreateSignaturePresentationFieldReference();
            // stakeholderSignatureField.SignatureAboveLineFieldReference = CreateSignatureAboveLineFieldReference();
            // stakeholderSignatureField.SignatureBelowLineFieldReference = CreateSignatureBelowLineFieldReference();
            // stakeholderSignatureField.StakeholderSignatureFieldDetail = CreateStakeholderSignatureFieldDetail();

            return stakeholderSignatureField;
        }

        private StakeholderSignatureFieldDetail CreateStakeholderSignatureFieldDetail()
        {
            StakeholderSignatureFieldDetail stakeholderSignatureFieldDetail = new StakeholderSignatureFieldDetail();

            // stakeholderSignatureFieldDetail.MutuallyExclusiveSignatureGroupName = ;
            // stakeholderSignatureFieldDetail.SignatureFieldMarkType = ;
            // stakeholderSignatureFieldDetail.SignatureFieldRequiredIndicator = ;
            // stakeholderSignatureFieldDetail.SignatureType = ;
            // stakeholderSignatureFieldDetail.SignatureTypeOtherDescription = ;

            return stakeholderSignatureFieldDetail;
        }

        private Status CreateStatus()
        {
            Status status = new Status();

            // status.StatusCode = ;
            // status.StatusConditionDescription = ;
            // status.StatusDescription = ;
            // status.StatusName = ;
            // status.SequenceNumber = ;

            return status;
        }

        private Statuses CreateStatuses()
        {
            Statuses statuses = new Statuses();

            // statuses.StatusList.Add(CreateStatus());

            return statuses;
        }

        private StatusChangeEvent CreateStatusChangeEvent()
        {
            StatusChangeEvent statusChangeEvent = new StatusChangeEvent();

            // statusChangeEvent.InvestorReportingActionDate = ;
            // statusChangeEvent.InvestorReportingActionType = ;
            // statusChangeEvent.InvestorReportingActionTypeOtherDescription = ;
            // statusChangeEvent.SequenceNumber = ;

            return statusChangeEvent;
        }

        private StatusChangeEvents CreateStatusChangeEvents()
        {
            StatusChangeEvents statusChangeEvents = new StatusChangeEvents();

            // statusChangeEvents.StatusChangeEventList.Add(CreateStatusChangeEvent());

            return statusChangeEvents;
        }

        private StopCode CreateStopCode()
        {
            StopCode stopCode = new StopCode();

            // stopCode.StopCodeActionType = ;
            // stopCode.StopCodeActionTypeOtherDescription = ;
            // stopCode.StopCodeExpirationDate = ;
            // stopCode.StopCodeConditionType = ;
            // stopCode.StopCodeConditionTypeOtherDescription = ;
            // stopCode.SequenceNumber = ;

            return stopCode;
        }

        private StopCodes CreateStopCodes()
        {
            StopCodes stopCodes = new StopCodes();

            // stopCodes.StopCodeList.Add(CreateStopCode());

            return stopCodes;
        }

        private SubmittingParty CreateSubmittingParty()
        {
            SubmittingParty submittingParty = new SubmittingParty();

            // submittingParty.LoginAccountIdentifier = ;
            // submittingParty.LoginAccountPasswordText = ;

            return submittingParty;
        }

        private Summaries CreateSummaries()
        {
            Summaries summaries = new Summaries();

            // summaries.SummaryList.Add(CreateSummary());

            return summaries;
        }

        private Summary CreateSummary()
        {
            Summary summary = new Summary();

            // summary.SummaryAmount = ;
            // summary.SummaryAmountType = ;
            // summary.SummaryAmountTypeOtherDescription = ;
            // summary.SequenceNumber = ;

            return summary;
        }

        private SystemSignature CreateSystemSignature()
        {
            SystemSignature systemSignature = new SystemSignature();

            // systemSignature.SequenceNumber = ;

            return systemSignature;
        }

        private SystemSignatures CreateSystemSignatures()
        {
            SystemSignatures systemSignatures = new SystemSignatures();

            // systemSignatures.SystemSignatureList.Add(CreateSystemSignature());

            return systemSignatures;
        }

        private Tax CreateTax()
        {
            Tax tax = new Tax();


            return tax;
        }

        private TaxpayerIdentifier CreateTaxpayerIdentifier()
        {
            TaxpayerIdentifier taxpayerIdentifier = new TaxpayerIdentifier();

            // taxpayerIdentifier.TaxpayerIdentifierType = ;
            // taxpayerIdentifier.TaxpayerIdentifierValue = ;
            // taxpayerIdentifier.SequenceNumber = ;

            return taxpayerIdentifier;
        }

        private TaxpayerIdentifiers CreateTaxpayerIdentifiers()
        {
            TaxpayerIdentifiers taxpayerIdentifiers = new TaxpayerIdentifiers();

            // taxpayerIdentifiers.TaxpayerIdentifierList.Add(CreateTaxpayerIdentifier());

            return taxpayerIdentifiers;
        }

        private Template CreateTemplate()
        {
            Template template = new Template();

            // template.TemplatePages = CreateTemplatePages();
            // template.TemplateFiles = CreateTemplateFiles();

            return template;
        }

        private TemplateFile CreateTemplateFile()
        {
            TemplateFile templateFile = new TemplateFile();

            // templateFile.ForeignObject = CreateForeignObject();
            // templateFile.SequenceNumber = ;

            return templateFile;
        }

        private TemplateFiles CreateTemplateFiles()
        {
            TemplateFiles templateFiles = new TemplateFiles();

            // templateFiles.TemplateFileList.Add(CreateTemplateFile());

            return templateFiles;
        }

        private TemplatePage CreateTemplatePage()
        {
            TemplatePage templatePage = new TemplatePage();

            // templatePage.TemplatePageFiles = CreateTemplatePageFiles();
            // templatePage.SequenceNumber = ;

            return templatePage;
        }

        private TemplatePages CreateTemplatePages()
        {
            TemplatePages templatePages = new TemplatePages();

            // templatePages.TemplatePageList.Add(CreateTemplatePage());

            return templatePages;
        }

        private TemplatePageFile CreateTemplatePageFile()
        {
            TemplatePageFile templatePageFile = new TemplatePageFile();

            // templatePageFile.ForeignObject = CreateForeignObject();
            // templatePageFile.SequenceNumber = ;

            return templatePageFile;
        }

        private TemplatePageFiles CreateTemplatePageFiles()
        {
            TemplatePageFiles templatePageFiles = new TemplatePageFiles();

            // templatePageFiles.TemplatePageFileList.Add(CreateTemplatePageFile());

            return templatePageFiles;
        }

        private TermsOfMortgage CreateTermsOfMortgage(CPageData dataLoan)
        {
            TermsOfMortgage termsOfMortgage = new TermsOfMortgage();

            // termsOfMortgage.DisclosedFullyIndexedRatePercent = ;
            if (dataLoan.sGseDeliveryTargetT != E_sGseDeliveryTargetT.FreddieMac || dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                termsOfMortgage.DisclosedIndexRatePercent = dataLoan.sRAdjIndexR_rep;
            }
            // termsOfMortgage.DisclosedMarginRatePercent = ;
            termsOfMortgage.LienPriorityType = ToMismo(dataLoan.sLienPosT);
            // termsOfMortgage.LienPriorityTypeOtherDescription = ;
            termsOfMortgage.LoanPurposeType = ToMismo(dataLoan.sLPurposeT);
            // termsOfMortgage.LoanPurposeTypeOtherDescription = ;
            termsOfMortgage.MortgageType = ToMismo(dataLoan.sLT);
            // termsOfMortgage.MortgageTypeOtherDescription = ;
            termsOfMortgage.NoteAmount = TruncateMoneyAmount(dataLoan.sFinalLAmt_rep);
            termsOfMortgage.NoteDate = dataLoan.sDocumentNoteD_rep;
            termsOfMortgage.NoteRatePercent = dataLoan.sNoteIR_rep;
            // termsOfMortgage.OriginalInterestRateDiscountPercent = ;
            // termsOfMortgage.UPBChangeFrequencyType = ;

            return termsOfMortgage;
        }

        protected MortgageType ToMismo(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.Conventional:
                    return MortgageType.Conventional;
                case E_sLT.FHA:
                    return MortgageType.FHA;
                case E_sLT.VA:
                    return MortgageType.VA;
                case E_sLT.UsdaRural:
                    return MortgageType.USDARuralHousing;
                case E_sLT.Other:
                    return MortgageType.Other;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }

        private LienPriorityType ToMismo(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First:
                    return LienPriorityType.FirstLien;
                case E_sLienPosT.Second:
                    return LienPriorityType.SecondLien;
                default:
                    throw new UnhandledEnumException(sLienPosT);
            }
            throw new NotImplementedException();
        }

        private LoanPurposeType ToMismo(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:

                    return LoanPurposeType.Purchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.HomeEquity:

                    return LoanPurposeType.Refinance;
                case E_sLPurposeT.Other:
                    return LoanPurposeType.Undefined;
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }
        }

        private Title CreateTitle()
        {
            Title title = new Title();

            // title.TitleRequest = CreateTitleRequest();
            // title.TitleResponse = CreateTitleResponse();

            return title;
        }

        private TitleAdditionalException CreateTitleAdditionalException()
        {
            TitleAdditionalException titleAdditionalException = new TitleAdditionalException();

            // titleAdditionalException.TitleAdditionalExceptionIncludedIndicator = ;
            // titleAdditionalException.TitleAdditionalDescription = ;
            // titleAdditionalException.SequenceNumber = ;

            return titleAdditionalException;
        }

        private TitleAdditionalExceptions CreateTitleAdditionalExceptions()
        {
            TitleAdditionalExceptions titleAdditionalExceptions = new TitleAdditionalExceptions();

            // titleAdditionalExceptions.TitleAdditionalExceptionList.Add(CreateTitleAdditionalException());

            return titleAdditionalExceptions;
        }

        private TitleEndorsement CreateTitleEndorsement()
        {
            TitleEndorsement titleEndorsement = new TitleEndorsement();

            // titleEndorsement.TitleEndorsementIncludedIndicator = ;
            // titleEndorsement.TitleEndorsementSourceType = ;
            // titleEndorsement.TitleEndorsementSourceTypeOtherDescription = ;
            // titleEndorsement.TitleEndorsementFormIdentifier = ;
            // titleEndorsement.TitleEndorsementFormName = ;
            // titleEndorsement.TitleEndorsementDescription = ;
            // titleEndorsement.TitleEndorsementStatuteDescription = ;
            // titleEndorsement.TitleEndorsementStatuteIncludedIndicator = ;
            // titleEndorsement.SequenceNumber = ;

            return titleEndorsement;
        }

        private TitleEndorsements CreateTitleEndorsements()
        {
            TitleEndorsements titleEndorsements = new TitleEndorsements();

            // titleEndorsements.TitleEndorsementList.Add(CreateTitleEndorsement());

            return titleEndorsements;
        }

        private TitleRequest CreateTitleRequest()
        {
            TitleRequest titleRequest = new TitleRequest();

            // titleRequest.Executions = CreateExecutions();
            // titleRequest.TitleRequestDetail = CreateTitleRequestDetail();

            return titleRequest;
        }

        private TitleRequestDetail CreateTitleRequestDetail()
        {
            TitleRequestDetail titleRequestDetail = new TitleRequestDetail();

            // titleRequestDetail.InsuredName = ;
            // titleRequestDetail.NamedInsuredType = ;
            // titleRequestDetail.ProcessorIdentifier = ;
            // titleRequestDetail.RequestedClosingDate = ;
            // titleRequestDetail.RequestedClosingTime = ;
            // titleRequestDetail.TitleAssociationType = ;
            // titleRequestDetail.TitleAssociationTypeOtherDescription = ;
            // titleRequestDetail.TitleOfficeIdentifier = ;
            // titleRequestDetail.TitleOwnershipType = ;
            // titleRequestDetail.TitleOwnershipTypeOtherDescription = ;
            // titleRequestDetail.TitleRequestActionType = ;
            // titleRequestDetail.TitleRequestCommentDescription = ;
            // titleRequestDetail.TitleRequestProposedTitleInsuranceCoverageAmount = ;
            // titleRequestDetail.VendorOrderIdentifier = ;
            // titleRequestDetail.VendorTransactionIdentifier = ;

            return titleRequestDetail;
        }

        private TitleResponse CreateTitleResponse()
        {
            TitleResponse titleResponse = new TitleResponse();

            // titleResponse.Executions = CreateExecutions();
            // titleResponse.LegalAndVestings = CreateLegalAndVestings();
            // titleResponse.Loans = CreateLoans();
            // titleResponse.Parties = CreateParties();
            // titleResponse.Property = CreateProperty();
            // titleResponse.RecordingEndorsementInformation = CreateRecordingEndorsementInformation();
            // titleResponse.TitleAdditionalExceptions = CreateTitleAdditionalExceptions();
            // titleResponse.TitleEndorsements = CreateTitleEndorsements();
            // titleResponse.TitleResponseDetail = CreateTitleResponseDetail();

            return titleResponse;
        }

        private TitleResponseDetail CreateTitleResponseDetail()
        {
            TitleResponseDetail titleResponseDetail = new TitleResponseDetail();

            // titleResponseDetail.LoanScheduledClosingDate = ;
            // titleResponseDetail.TitleClearanceGradeIdentifier = ;
            // titleResponseDetail.TitleInsuranceAmount = ;
            // titleResponseDetail.TitlePolicyIdentifier = ;
            // titleResponseDetail.TitlePolicyEffectiveDate = ;
            // titleResponseDetail.TitlePolicyWitnessClauseDescription = ;
            // titleResponseDetail.TitleResponseCommentDescription = ;

            return titleResponseDetail;
        }

        private Transform CreateTransform()
        {
            Transform transform = new Transform();

            // transform.TransformFiles = CreateTransformFiles();
            // transform.TransformPages = CreateTransformPages();
            // transform.SequenceNumber = ;

            return transform;
        }

        private Transforms CreateTransforms()
        {
            Transforms transforms = new Transforms();

            // transforms.TransformList.Add(CreateTransform());

            return transforms;
        }

        private TransformFile CreateTransformFile()
        {
            TransformFile transformFile = new TransformFile();

            // transformFile.ForeignObject = CreateForeignObject();
            // transformFile.SequenceNumber = ;

            return transformFile;
        }

        private TransformFiles CreateTransformFiles()
        {
            TransformFiles transformFiles = new TransformFiles();

            // transformFiles.TransformFileList.Add(CreateTransformFile());

            return transformFiles;
        }

        private TransformPage CreateTransformPage()
        {
            TransformPage transformPage = new TransformPage();

            // transformPage.TransformPageFiles = CreateTransformPageFiles();
            // transformPage.SequenceNumber = ;

            return transformPage;
        }

        private TransformPages CreateTransformPages()
        {
            TransformPages transformPages = new TransformPages();

            // transformPages.TransformPageList.Add(CreateTransformPage());

            return transformPages;
        }

        private TransformPageFile CreateTransformPageFile()
        {
            TransformPageFile transformPageFile = new TransformPageFile();

            // transformPageFile.ForeignObject = CreateForeignObject();
            // transformPageFile.SequenceNumber = ;

            return transformPageFile;
        }

        private TransformPageFiles CreateTransformPageFiles()
        {
            TransformPageFiles transformPageFiles = new TransformPageFiles();

            // transformPageFiles.TransformPageFileList.Add(CreateTransformPageFile());

            return transformPageFiles;
        }

        private Trust CreateTrust(CPageData dataLoan)
        {
            Trust trust = new Trust();

            // trust.TrustClassificationType = ;
            // trust.TrustClassificationTypeOtherDescription = ;
            trust.TrustEstablishedDate = dataLoan.sTrustAgreementD_rep;
            trust.TrustEstablishedStateName = dataLoan.sTrustState;

            return trust;
        }

        private Trustee CreateTrustee()
        {
            Trustee trustee = new Trustee();

            // trustee.TrusteeType = ;
            // trustee.TrusteeTypeOtherDescription = ;

            return trustee;
        }

        private Underwriting CreateUnderwriting(CPageData dataLoan)
        {
            Underwriting underwriting = new Underwriting();

            underwriting.AutomatedUnderwritings = CreateAutomatedUnderwritings(dataLoan);
            underwriting.UnderwritingDetail = CreateUnderwritingDetail(dataLoan);

            return underwriting;
        }

        private UnderwritingDetail CreateUnderwritingDetail(CPageData dataLoan)
        {
            UnderwritingDetail underwritingDetail = new UnderwritingDetail();

            // underwritingDetail.ContractUnderwritingIndicator = ;
            underwritingDetail.LoanManualUnderwritingIndicator = dataLoan.sIsManualUw;
            // underwritingDetail.LoanManualUnderwritingOrganizationName = ;
            // underwritingDetail.LoanUnderwriterName = ;
            // underwritingDetail.LoanUnderwritingInvestorGuidelinesIndicator = ;
            // underwritingDetail.LoanUnderwritingSubmitterType = ;
            // underwritingDetail.LoanUnderwritingSubmitterTypeOtherDescription = ;
            // underwritingDetail.UnderwritingCommentsDescription = ;

            return underwritingDetail;
        }

        private UnknownVersion3Document CreateUnknownVersion3Document()
        {
            UnknownVersion3Document unknownVersion3Document = new UnknownVersion3Document();

            // unknownVersion3Document.ForeignObject = CreateForeignObject();

            return unknownVersion3Document;
        }

        private UnparsedLegalDescription CreateUnparsedLegalDescription()
        {
            UnparsedLegalDescription unparsedLegalDescription = new UnparsedLegalDescription();

            // unparsedLegalDescription._UnparsedLegalDescription = ;
            // unparsedLegalDescription.SequenceNumber = ;

            return unparsedLegalDescription;
        }

        private UnparsedLegalDescriptions CreateUnparsedLegalDescriptions()
        {
            UnparsedLegalDescriptions unparsedLegalDescriptions = new UnparsedLegalDescriptions();

            // unparsedLegalDescriptions.UnparsedLegalDescriptionList.Add(CreateUnparsedLegalDescription());

            return unparsedLegalDescriptions;
        }

        private UnplattedLand CreateUnplattedLand()
        {
            UnplattedLand unplattedLand = new UnplattedLand();

            // unplattedLand.AbstractIdentifier = ;
            // unplattedLand.BaseIdentifier = ;
            // unplattedLand.LegalTractIdentifier = ;
            // unplattedLand.MeridianIdentifier = ;
            // unplattedLand.MetesAndBoundsRemainingDescription = ;
            // unplattedLand.QuarterSectionIdentifier = ;
            // unplattedLand.RangeIdentifier = ;
            // unplattedLand.SectionIdentifier = ;
            // unplattedLand.TownshipIdentifier = ;
            // unplattedLand.UnplattedLandType = ;
            // unplattedLand.UnplattedLandTypeOtherDescription = ;
            // unplattedLand.UnplattedLandTypeIdentifier = ;
            // unplattedLand.SequenceNumber = ;

            return unplattedLand;
        }

        private UnplattedLands CreateUnplattedLands()
        {
            UnplattedLands unplattedLands = new UnplattedLands();

            // unplattedLands.UnplattedLandList.Add(CreateUnplattedLand());

            return unplattedLands;
        }

        private Urla CreateUrla(CPageData dataLoan) // DONE
        {
            Urla urla = new Urla();

            // urla.ApplicationInterviewer = CreateApplicationInterviewer();
            urla.UrlaDetail = CreateUrlaDetail(dataLoan);
            // urla.UrlaTotal = CreateUrlaTotal();
            // urla.UrlaTotalHousingExpenses = CreateUrlaTotalHousingExpenses();

            return urla;
        }

        private UrlaDetail CreateUrlaDetail(CPageData dataLoan)
        {
            UrlaDetail urlaDetail = new UrlaDetail();

            // urlaDetail.AdditionalBorrowerAssetsConsideredIndicator = ;
            // urlaDetail.AdditionalBorrowerAssetsNotConsideredIndicator = ;
            // urlaDetail.AlterationsImprovementsAndRepairsAmount = ;
            // urlaDetail.ARMTypeDescription = ;
            // urlaDetail.BaseLoanAmount = ;
            if (dataLoan.sSettlementDiscountPointF > 0 && LosConvert.GfeItemProps_Payer(dataLoan.sSettlementDiscountPointFProps) == LosConvert.BORR_PAID_OUTOFPOCKET)
            {
                urlaDetail.BorrowerPaidDiscountPointsTotalAmount = dataLoan.sSettlementDiscountPointF_rep;
            }
            else if (dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FannieMae)
            {
                urlaDetail.BorrowerPaidDiscountPointsTotalAmount = dataLoan.m_convertLos.ToMoneyString(0, FormatDirection.ToRep);
            }

            // urlaDetail.BorrowerRequestedInterestRatePercent = ;
            // urlaDetail.BorrowerRequestedLoanAmount = ;
            // urlaDetail.EstimatedClosingCostsAmount = ;
            // urlaDetail.LenderRegistrationIdentifier = ;
            // urlaDetail.MIAndFundingFeeFinancedAmount = ;
            // urlaDetail.MIAndFundingFeeTotalAmount = ;
            // urlaDetail.PrepaidItemsEstimatedAmount = ;
            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase && dataLoan.sLienPosT == E_sLienPosT.First)
            {
                urlaDetail.PurchasePriceAmount = TruncateMoneyAmount(dataLoan.sPurchPrice_rep);
            }
            // urlaDetail.PurchasePriceNetAmount = ;
            // urlaDetail.RefinanceImprovementCostsAmount = ;
            // urlaDetail.RefinanceImprovementsType = ;
            // urlaDetail.RefinanceIncludingDebtsToBePaidOffAmount = ;
            // urlaDetail.RefinanceProposedImprovementsDescription = ;
            // urlaDetail.RefundableApplicationFeeIndicator = ;
            // urlaDetail.RequiredDepositIndicator = ;
            // urlaDetail.SellerPaidClosingCostsAmount = ;

            return urlaDetail;
        }

        private UrlaTotal CreateUrlaTotal()
        {
            UrlaTotal urlaTotal = new UrlaTotal();

            // urlaTotal.URLATotalAssetsAmount = ;
            // urlaTotal.URLATotalBaseIncomeAmount = ;
            // urlaTotal.URLATotalBonusIncomeAmount = ;
            // urlaTotal.URLATotalCashFromToBorrowerAmount = ;
            // urlaTotal.URLATotalCombinedPresentHousingExpenseAmount = ;
            // urlaTotal.URLATotalCombinedProposedHousingExpenseAmount = ;
            // urlaTotal.URLATotalCommissionsIncomeAmount = ;
            // urlaTotal.URLATotalDividendsInterestIncomeAmount = ;
            // urlaTotal.URLATotalLiabilityMonthlyPaymentsAmount = ;
            // urlaTotal.URLATotalLiabilityUPBAmount = ;
            // urlaTotal.URLATotalLotAndImprovementsAmount = ;
            // urlaTotal.URLATotalMonthlyIncomeAmount = ;
            // urlaTotal.URLATotalNetRentalIncomeAmount = ;
            // urlaTotal.URLATotalNetWorthAmount = ;
            // urlaTotal.URLATotalOtherTypesOfIncomeAmount = ;
            // urlaTotal.URLATotalOvertimeIncomeAmount = ;
            // urlaTotal.URLATotalREOLienInstallmentAmount = ;
            // urlaTotal.URLATotalREOLienUPBAmount = ;
            // urlaTotal.URLATotalREOMaintenanceExpenseAmount = ;
            // urlaTotal.URLATotalREOMarketValueAmount = ;
            // urlaTotal.URLATotalREORentalIncomeGrossAmount = ;
            // urlaTotal.URLATotalREORentalIncomeNetAmount = ;
            // urlaTotal.URLATotalTransactionCostAmount = ;

            return urlaTotal;
        }

        private UrlaTotalHousingExpense CreateUrlaTotalHousingExpense()
        {
            UrlaTotalHousingExpense urlaTotalHousingExpense = new UrlaTotalHousingExpense();

            // urlaTotalHousingExpense.URLATotalHousingExpensePaymentAmount = ;
            // urlaTotalHousingExpense.HousingExpenseType = ;
            // urlaTotalHousingExpense.HousingExpenseTypeOtherDescription = ;
            // urlaTotalHousingExpense.SequenceNumber = ;

            return urlaTotalHousingExpense;
        }

        private UrlaTotalHousingExpenses CreateUrlaTotalHousingExpenses()
        {
            UrlaTotalHousingExpenses urlaTotalHousingExpenses = new UrlaTotalHousingExpenses();

            // urlaTotalHousingExpenses.UrlaTotalHousingExpenseList.Add(CreateUrlaTotalHousingExpense());

            return urlaTotalHousingExpenses;
        }

        private Valuation CreateValuation()
        {
            Valuation valuation = new Valuation();


            return valuation;
        }

        private Verification CreateVerification()
        {
            Verification verification = new Verification();

            // verification.VerificationByName = ;
            // verification.VerificationCommentDescription = ;
            // verification.VerificationDate = ;
            // verification.VerificationStatusType = ;
            // verification.VerificationMethodType = ;
            // verification.VerificationMethodTypeOtherDescription = ;

            return verification;
        }

        private VerificationData CreateVerificationData()
        {
            VerificationData verificationData = new VerificationData();

            // verificationData.VerificationDataInvestor = CreateVerificationDataInvestor();
            // verificationData.VerificationDataLoan = CreateVerificationDataLoan();
            // verificationData.VerificationDataPayee = CreateVerificationDataPayee();
            // verificationData.VerificationDataPool = CreateVerificationDataPool();

            return verificationData;
        }

        private VerificationDataInvestor CreateVerificationDataInvestor()
        {
            VerificationDataInvestor verificationDataInvestor = new VerificationDataInvestor();

            // verificationDataInvestor.InvestorTotalCount = ;

            return verificationDataInvestor;
        }

        private VerificationDataLoan CreateVerificationDataLoan()
        {
            VerificationDataLoan verificationDataLoan = new VerificationDataLoan();

            // verificationDataLoan.LoanTotalCount = ;
            // verificationDataLoan.UPBTotalAmount = ;
            // verificationDataLoan.EscrowBalanceTotalAmount = ;
            // verificationDataLoan.RemainingBuydownBalanceAmountTotalAmount = ;
            // verificationDataLoan.DeferredInterestBalanceAmountTotalAmount = ;
            // verificationDataLoan.RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount = ;
            // verificationDataLoan.RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount = ;
            // verificationDataLoan.BankruptcySuspenseBalanceAmountTotalAmount = ;
            // verificationDataLoan.SuspenseBalanceAmountTotalAmount = ;
            // verificationDataLoan.RestrictedEscrowBalanceAmountTotalAmount = ;
            // verificationDataLoan.EscrowCompletionFundsBalanceAmountTotalAmount = ;
            // verificationDataLoan.UncollectedLateChargeBalanceAmountTotalAmount = ;
            // verificationDataLoan.CurrentBuydownSubsidyAmountTotalAmount = ;
            // verificationDataLoan.CurrentTaxAndInsurancePaymentAmountTotalAmount = ;
            // verificationDataLoan.CurrentTotalOptionalProductsPaymentAmountTotalAmount = ;
            // verificationDataLoan.CurrentPrincipalAndInterestPaymentAmountTotalAmount = ;
            // verificationDataLoan.CurrentTotalPaymentAmountTotalAmount = ;
            // verificationDataLoan.CurrentHUD235SubsidyPaymentAmountTotalAmount = ;

            return verificationDataLoan;
        }

        private VerificationDataPayee CreateVerificationDataPayee()
        {
            VerificationDataPayee verificationDataPayee = new VerificationDataPayee();

            // verificationDataPayee.PayeeTotalCount = ;

            return verificationDataPayee;
        }

        private VerificationDataPool CreateVerificationDataPool()
        {
            VerificationDataPool verificationDataPool = new VerificationDataPool();

            // verificationDataPool.PoolTotalCount = ;

            return verificationDataPool;
        }

        private View CreateView()
        {
            View view = new View();

            // view.ViewFiles = CreateViewFiles();
            // view.ViewPages = CreateViewPages();
            // view.ViewFields = CreateViewFields();
            // view.SequenceNumber = ;

            return view;
        }

        private Views CreateViews()
        {
            Views views = new Views();

            // views.ViewList.Add(CreateView());

            return views;
        }

        private ViewField CreateViewField()
        {
            ViewField viewField = new ViewField();

            // viewField.InteractiveField = CreateInteractiveField();
            // viewField.NotarySignatureField = CreateNotarySignatureField();
            // viewField.RecordingEndorsementField = CreateRecordingEndorsementField();
            // viewField.StakeholderSignatureField = CreateStakeholderSignatureField();
            // viewField.WitnessSignatureField = CreateWitnessSignatureField();
            // viewField.SequenceNumber = ;

            return viewField;
        }

        private ViewFields CreateViewFields()
        {
            ViewFields viewFields = new ViewFields();

            // viewFields.ViewFieldList.Add(CreateViewField());

            return viewFields;
        }

        private ViewFile CreateViewFile()
        {
            ViewFile viewFile = new ViewFile();

            // viewFile.ForeignObject = CreateForeignObject();
            // viewFile.SequenceNumber = ;

            return viewFile;
        }

        private ViewFiles CreateViewFiles()
        {
            ViewFiles viewFiles = new ViewFiles();

            // viewFiles.ViewFileList.Add(CreateViewFile());

            return viewFiles;
        }

        private ViewPage CreateViewPage()
        {
            ViewPage viewPage = new ViewPage();

            // viewPage.ViewPageFiles = CreateViewPageFiles();
            // viewPage.SequenceNumber = ;

            return viewPage;
        }

        private ViewPages CreateViewPages()
        {
            ViewPages viewPages = new ViewPages();

            // viewPages.ViewPageList.Add(CreateViewPage());

            return viewPages;
        }

        private ViewPageFile CreateViewPageFile()
        {
            ViewPageFile viewPageFile = new ViewPageFile();

            // viewPageFile.ForeignObject = CreateForeignObject();
            // viewPageFile.SequenceNumber = ;

            return viewPageFile;
        }

        private ViewPageFiles CreateViewPageFiles()
        {
            ViewPageFiles viewPageFiles = new ViewPageFiles();

            // viewPageFiles.ViewPageFileList.Add(CreateViewPageFile());

            return viewPageFiles;
        }

        private WitnessSignatureDetail CreateWitnessSignatureDetail()
        {
            WitnessSignatureDetail witnessSignatureDetail = new WitnessSignatureDetail();

            // witnessSignatureDetail.SignatureFieldRequiredIndicator = ;
            // witnessSignatureDetail.SignatureType = ;
            // witnessSignatureDetail.SignatureTypeOtherDescription = ;

            return witnessSignatureDetail;
        }

        private WitnessSignatureField CreateWitnessSignatureField()
        {
            WitnessSignatureField witnessSignatureField = new WitnessSignatureField();

            // witnessSignatureField.SignatureFieldReference = CreateSignatureFieldReference();
            // witnessSignatureField.WitnessSignatureDetail = CreateWitnessSignatureDetail();

            return witnessSignatureField;
        }

    }

    public class SelectedLoanProduct : LoanProduct
    {
        public override string XmlElementName
        {
            get
            {
                return "SELECTED_LOAN_PRODUCT";
            }
        }
    }
}
