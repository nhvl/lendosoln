﻿namespace LendersOffice.ObjLib.Conversions.LoanProspector
{
    using System;
    using System.Linq;
    using Aus;
    using DataAccess;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// ViewModel data for the Seamless LPA login page.
    /// </summary>
    public class LpaSeamlessLoginModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaSeamlessLoginModel"/> class with no values initialized.
        /// </summary>
        /// <remarks>
        /// Facilitates deserialization.
        /// </remarks>
        public LpaSeamlessLoginModel()
        {
            this.LpaCredentials = new LpaCredentialModel();
            this.LoanInformation = new LpaLoanInformation();
            this.CreditInfo = new AusCreditOrderingInfo();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LpaSeamlessLoginModel"/> class. Creates the model for the LPA Seamless login page.
        /// This class is serialized to JSON for acting as a model for the SeamlessDuLogin.html page.
        /// </summary>
        /// <param name="loanData">The loan data object to use in creating the model.</param>
        /// <param name="user">The user requesting the page.</param>
        public LpaSeamlessLoginModel(CPageData loanData, AbstractUserPrincipal user)
        {
            this.LpaCredentials = new LpaCredentialModel();
            var savedLpaCredentials = ServiceCredential.ListAvailableServiceCredentials(user, loanData.sBranchId, enabledServices: ServiceCredentialService.AusSubmission)
                .Where(credential => credential.ChosenAusOption.HasValue && (credential.ChosenAusOption == AusOption.LpaSellerCredential || credential.ChosenAusOption == AusOption.LpaUserLogin));

            var savedUserCredential = savedLpaCredentials
                .FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaUserLogin);
            this.LpaCredentials.HasLpaAutoLogin = savedUserCredential != null;

            var savedSellerCredential = savedLpaCredentials
                .FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaSellerCredential);
            this.LpaCredentials.HasLpaSellerAutoLogin = savedSellerCredential != null;
            if (!this.LpaCredentials.HasLpaSellerAutoLogin)
            {
                this.LpaCredentials.LpaSellerNumber = loanData.sFreddieSellerNum;
            }

            this.LoanInformation = new LpaLoanInformation
            {
                LpaLoanId = loanData.sFreddieLoanId,
                LpaAusKey = loanData.sLpAusKey,
                CaseStateType = loanData.sFredProcPointT,
                LpaTpoNumber = loanData.sFreddieTpoNum,
                LpaNotpNumber = loanData.sFreddieNotpNum,
                LenderBranchId = loanData.sFreddieLenderBranchId
            };

            this.CreditInfo = new AusCreditOrderingInfo(loanData, user, AUSType.LoanProspector);
        }

        /// <summary>
        /// Gets or sets a container which holds credentials for the user to login to LPA.
        /// </summary>
        /// <value>LPA credentials.</value>
        public LpaCredentialModel LpaCredentials { get; set; }

        /// <summary>
        /// Gets or sets a container which holds information for getting a credit report.
        /// </summary>
        /// <value>Credit Info.</value>
        public AusCreditOrderingInfo CreditInfo { get; set; }

        /// <summary>
        /// Gets or sets a container holding loan data for the LPA login page.
        /// </summary>
        /// <value>Loan Information.</value>
        public LpaLoanInformation LoanInformation { get; set; }

        /// <summary>
        /// Gets a customized credit report type description for LPA.
        /// </summary>
        /// <param name="reportType">The credit report type selected.</param>
        /// <param name="reportExists">Whether there is an existing credit report.</param>
        /// <returns>A description for the report type.</returns>
        public static string CreditReportTypeDescription(CreditReportType reportType, bool reportExists)
        {
            if (reportType == CreditReportType.Reissue)
            {
                return "Reorder a credit report from a credit provider";
            }
            else
            {
                return EnumUtilities.GetDescription(reportType);
            }
        }

        /// <summary>
        /// Saves the data of this object to the loan file.
        /// </summary>
        /// <param name="loanData">A loan data object that has been prepared with the proper dependencies and had <see cref="CPageData.InitSave(int)"/> called.</param>
        public void ApplyToLoanFile(CPageData loanData)
        {
            loanData.sFreddieLoanId = this.LoanInformation.LpaLoanId;
            loanData.sLpAusKey = this.LoanInformation.LpaAusKey;
            loanData.sFredProcPointT = this.LoanInformation.CaseStateType;
            loanData.sFreddieTpoNum = this.LoanInformation.LpaTpoNumber;
            loanData.sFreddieNotpNum = this.LoanInformation.LpaNotpNumber;
            loanData.sFreddieLenderBranchId = this.LoanInformation.LenderBranchId;
            if (!string.IsNullOrEmpty(this.LpaCredentials.LpaSellerNumber))
            {
                loanData.sFreddieSellerNum = this.LpaCredentials.LpaSellerNumber;
            }

            if (!string.IsNullOrEmpty(this.LpaCredentials.LpaSellerPassword))
            {
                loanData.sFreddieLpPassword = this.LpaCredentials.LpaSellerPassword;
            }
        }

        /// <summary>
        /// Holds ViewModel data for LPA info from the loan file.
        /// </summary>
        public class LpaLoanInformation
        {
            /// <summary>
            /// Gets or sets the LPA loan ID (<see cref="CPageBase.sFreddieLoanId"/>).
            /// </summary>
            /// <value>LPA Loan ID.</value>
            public string LpaLoanId { get; set; }

            /// <summary>
            /// Gets or sets the LPA AUS Key (<see cref="CPageBase.sLpAusKey"/>).
            /// </summary>
            /// <value>LPA AUS Key.</value>
            public string LpaAusKey { get; set; }

            /// <summary>
            /// Gets or sets the case state type (<see cref="CPageBase.sFredProcPointT"/>).
            /// </summary>
            /// <value>Case State Type.</value>
            public E_sFredProcPointT CaseStateType { get; set; }

            /// <summary>
            /// Gets or sets the LPA TPO number (<see cref="CPageBase.sFreddieTpoNum"/>).
            /// </summary>
            /// <value>LPA TPO Number.</value>
            public string LpaTpoNumber { get; set; }

            /// <summary>
            /// Gets or sets the LPA NOTP number (<see cref="CPageBase.sFreddieNotpNum"/>).
            /// </summary>
            /// <value>LPA NOTP Number.</value>
            public string LpaNotpNumber { get; set; }

            /// <summary>
            /// Gets or sets the lender branch ID to send to LPA (<see cref="CPageBase.sFreddieLenderBranchId"/>).
            /// </summary>
            /// <value>Lender branch ID.</value>
            public string LenderBranchId { get; set; }
        }

        /// <summary>
        /// Holds ViewModel data which represents the credential information 
        /// necessary to authenticate with Freddie Mac.
        /// </summary>
        /// <remarks>
        /// This view model is used both for sending to the UI to pre-populate the data
        /// and for receiving back from the UI populated with data the user entered.
        /// </remarks>
        public class LpaCredentialModel
        {
            /// <summary>
            /// Gets or sets the LPA user ID used to log the user in to the LPA system.
            /// </summary>
            /// <value>LPA User ID.</value>
            public string LpaUserId { get; set; }

            /// <summary>
            /// Gets or sets the LPA password used to log the user in to the LPA system.
            /// </summary>
            /// <value>LPA Password.</value>
            public string LpaPassword { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the user has configured credentials for the LPA system.
            /// (<see cref="LpaUserId"/> and <see cref="LpaPassword"/>).
            /// </summary>
            /// <value>Has LPA Auto-login.</value>
            public bool HasLpaAutoLogin { get; set; }

            /// <summary>
            /// Gets or sets the LPA seller number sent in the LPA request XML.
            /// </summary>
            /// <value>LPA Seller Number.</value>
            public string LpaSellerNumber { get; set; }

            /// <summary>
            /// Gets or sets the LPA seller password sent in the LPA request XML.
            /// </summary>
            /// <value>LPA Seller Password.</value>
            public string LpaSellerPassword { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the user has configured LPA Seller credentials.
            /// </summary>
            /// <value>Has LPA Seller Auto-login.</value>
            public bool HasLpaSellerAutoLogin { get; set; }
        }
    }
}
