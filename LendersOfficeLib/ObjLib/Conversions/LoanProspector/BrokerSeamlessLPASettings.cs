﻿namespace LendersOffice.ObjLib.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Admin;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Gets the LPA seamless and nonseamless settings for a broker.
    /// </summary>
    public class BrokerSeamlessLPASettings
    {
        /// <summary>
        /// The pml broker ids that are in the designated oc group.
        /// </summary>
        private Lazy<HashSet<Guid>> fallbackEnabledPmlBrokerIds = null;

        /// <summary>
        /// The employee ids that are in the designated employee group.
        /// </summary>
        private Lazy<HashSet<Guid>> fallbackEnabledEmployeeIds = null;

        /// <summary>
        /// The designated oc group name.
        /// </summary>
        private string nonSeamlessEnabledOcGroupName;

        /// <summary>
        /// The designated employee group name.
        /// </summary>
        private string nonSeamlessEnabledEmployeeGroupName;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerSeamlessLPASettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="seamlessLpaEnabled">Whether seamless LPA is enabled.</param>
        /// <param name="nonSeamlessEnabledOcGroup">The designated OC group.</param>
        /// <param name="nonSeamlessEnabledEmployeeGroup">The designated employee group.</param>
        public BrokerSeamlessLPASettings(Guid brokerId, bool seamlessLpaEnabled, string nonSeamlessEnabledOcGroup = null, string nonSeamlessEnabledEmployeeGroup = null)
        {
            this.BrokerId = brokerId;
            this.SeamlessLpaEnabled = seamlessLpaEnabled;
            this.nonSeamlessEnabledOcGroupName = nonSeamlessEnabledOcGroup;
            this.nonSeamlessEnabledEmployeeGroupName = nonSeamlessEnabledEmployeeGroup;
            this.fallbackEnabledPmlBrokerIds = new Lazy<HashSet<Guid>>(this.GetFallbackEnabledPmlBrokers);
            this.fallbackEnabledEmployeeIds = new Lazy<HashSet<Guid>>(this.GetFallbackEnabledEmployees);
        }

        /// <summary>
        /// Gets the associated broker id.
        /// </summary>
        /// <value>The associated broker id.</value>
        public Guid BrokerId
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether seamless LPA is enabled for this broker.
        /// </summary>
        /// <value>Whether seamless LPA is enabled for this broker.</value>
        public bool SeamlessLpaEnabled
        {
            get;
        }

        /// <summary>
        /// Checks if the OC group, if provided, is a valid one.
        /// </summary>
        /// <returns>True if valid or not used, false otherwise.</returns>
        public bool ValidateOcGroup()
        {
            if (string.IsNullOrEmpty(this.nonSeamlessEnabledOcGroupName))
            {
                return true;
            }

            return GroupDB.GetAllGroupIdsAndNames(this.BrokerId, GroupType.PmlBroker).FirstOrDefault(group => group.Item2.Equals(this.nonSeamlessEnabledOcGroupName)) != null;
        }

        /// <summary>
        /// Checks if the employee group, if provided, is a valid one.
        /// </summary>
        /// <returns>True if valid or not used, false otherwise.</returns>
        public bool ValidateEmployeeGroup()
        {
            if (string.IsNullOrEmpty(this.nonSeamlessEnabledEmployeeGroupName))
            {
                return true;
            }

            return GroupDB.GetAllGroupIdsAndNames(this.BrokerId, GroupType.Employee).FirstOrDefault(group => group.Item2.Equals(this.nonSeamlessEnabledEmployeeGroupName)) != null;
        }

        /// <summary>
        /// Checks if a user can use the non-seamless LPA fallback.
        /// </summary>
        /// <param name="principal">The user's principal.</param>
        /// <returns>True if they can, false otherwise.</returns>
        public bool IsNonSeamlessFallbackEnabled(AbstractUserPrincipal principal)
        {
            if (principal.BrokerId != this.BrokerId)
            {
                return false;
            }

            if (!this.SeamlessLpaEnabled)
            {
                // Non-seamless is enabled if seamless is disabled.
                return true;
            }

            bool isPUser = principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase);
            if (isPUser)
            {
                if (string.IsNullOrEmpty(this.nonSeamlessEnabledOcGroupName))
                {
                    return false;
                }

                var pmlBrokerId = principal.PmlBrokerId;
                return this.fallbackEnabledPmlBrokerIds.Value.Contains(pmlBrokerId);
            }
            else
            {
                if (string.IsNullOrEmpty(this.nonSeamlessEnabledEmployeeGroupName))
                {
                    return false;
                }

                var employeeId = principal.EmployeeId;
                return this.fallbackEnabledEmployeeIds.Value.Contains(employeeId);
            }
        }

        /// <summary>
        /// Gets the PML brokers that can use fallback non-seamless LPA.
        /// </summary>
        /// <returns>The brokers that can use fallback non-seamless LPA.</returns>
        private HashSet<Guid> GetFallbackEnabledPmlBrokers()
        {
            HashSet<Guid> enabledPmlBrokerIds = null;
            if (!string.IsNullOrEmpty(this.nonSeamlessEnabledOcGroupName))
            {
                var origCompGroup = GroupDB.GetAllGroupIdsAndNames(this.BrokerId, GroupType.PmlBroker).FirstOrDefault((group) => group.Item2.Equals(this.nonSeamlessEnabledOcGroupName));
                if (origCompGroup != null)
                {
                    var enabledPmlBrokers = GroupDB.GetPmlBrokersWithGroupMembership(this.BrokerId, origCompGroup.Item1).Where(membership => membership.IsInGroup == 1);
                    if (enabledPmlBrokers != null && enabledPmlBrokers.Any())
                    {
                        enabledPmlBrokerIds = new HashSet<Guid>(enabledPmlBrokers.Select((membership) => membership.PmlBrokerId));
                    }
                }
            }

            return enabledPmlBrokerIds;
        }

        /// <summary>
        /// Gets the employees that can use fallback non-seamless LPA.
        /// </summary>
        /// <returns>The brokers taht can use fallback non-seamless LPA.</returns>
        private HashSet<Guid> GetFallbackEnabledEmployees()
        {
            HashSet<Guid> enabledEmployeeIds = null;
            if (!string.IsNullOrEmpty(this.nonSeamlessEnabledEmployeeGroupName))
            {
                var employeeGroup = GroupDB.GetAllGroupIdsAndNames(this.BrokerId, GroupType.Employee).FirstOrDefault((group) => group.Item2.Equals(this.nonSeamlessEnabledEmployeeGroupName));
                if (employeeGroup != null)
                {
                    var enabledEmployees = GroupDB.GetEmployeesWithGroupMembership(this.BrokerId, employeeGroup.Item1).Where(membership => membership.IsInGroup == 1);
                    if (enabledEmployees != null && enabledEmployees.Any())
                    {
                        enabledEmployeeIds = new HashSet<Guid>(enabledEmployees.Select(membership => membership.EmployeeId));
                    }
                }
            }

            return enabledEmployeeIds;
        }
    }
}
