﻿namespace LendersOffice.ObjLib.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using Aus;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    /// <summary>
    /// Stores the data model for the UI of the SeamlessLpaSummary.html page.
    /// </summary>
    public class LpaResponseViewModel
    {
        /// <summary>
        /// Gets or sets the response status to return to the UI.
        /// </summary>
        /// <value>Returned status.</value>
        [JsonConverter(typeof(StringEnumConverter))]
        public AusResponseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the error message to go along with an error status, whether returned from the vendor or our system.
        /// </summary>
        /// <value>Error message.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the list of errors.
        /// </summary>
        public IEnumerable<string> Errors { get; set; }

        /// <summary>
        /// Gets or sets the Fannie Mae - generated ID for identifying an existing request.
        /// </summary>
        /// <value>Fannie ID.</value> 
        public string AsyncIdentifier { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an access denied error was encountered when trying to generate a request.
        /// </summary>
        /// <value>Whether access was denied.</value>
        public bool AccessDenied { get; set; }

        /// <summary>
        /// Gets or sets a URL for a log returned from the Loan Product Advisor system to detail why an error occurred.
        /// </summary>
        /// <value>LPA response log URL.</value>
        public string LpaResponseUrl { get; set; }

        /// <summary>
        /// Gets or sets a mapping of (key, value) pairs to be displayed in the summary table.
        /// </summary>
        /// <value>Summary table contents.</value>
        public Dictionary<string, string> LpaResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the automated underwriting was a success.
        /// </summary>
        /// <value>Whether underwriting was successful.</value>
        public bool UnderwritingSuccess { get; set; }

        /// <summary>
        /// Gets or sets the timeout period that should be between polling requests, in milliseconds.
        /// </summary>
        /// <value>The timeout period in milliseconds.</value>
        public int PollingPeriodMs { get; set; }

        /// <summary>
        /// Gets or sets the public job id for polling.
        /// </summary>
        /// <value>The public job id for polling.</value>
        public Guid PublicJobId { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of retries that the UI should make to retrieve underwriting results from backend.
        /// </summary>
        /// <value>The maximum number of retries from the UI.</value>
        public int MaxRetries { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import the LPA findings to the loan file.
        /// </summary>
        /// <value>Whether to import DU findings.</value>
        public bool? ImportFindings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import the credit report information to the loan file.
        /// </summary>
        /// <value>Whether to import credit report information.</value>
        public bool? ImportCreditReport { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import liabilities from the credit report to the loan file.
        /// </summary>
        /// <value>Whether to import liabilities from the credit report.</value>
        public bool? ImportLiabilities { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a credit report in the response.
        /// </summary>
        public bool HasCreditReport { get; set; }
    }
}
