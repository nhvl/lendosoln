﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using Security;

    /// <summary>
    /// LPA request data class.
    /// </summary>
    public abstract class LpaRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestData"/> class.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="sellerNumber">The seller number.</param>
        /// <param name="sellerPassword">The seller password.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="principal">The principal.</param>
        protected LpaRequestData(string username, string password, string sellerNumber, string sellerPassword, Guid loanId, AbstractUserPrincipal principal)
        {
            this.Username = username;
            this.Password = password;
            this.LoanId = loanId;
            this.Principal = principal;
            this.LpaSellerNumber = sellerNumber;
            this.LpaSellerPassword = sellerPassword;
        }

        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <value>The username.</value>
        public string Username
        {
            get;
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password
        {
            get;
        }

        /// <summary>
        /// Gets or sets the LPA seller number.
        /// </summary>
        /// <value>The LPA seller number.</value>
        public string LpaSellerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the LPA seller password.
        /// </summary>
        /// <value>The LPA seller password.</value>
        public string LpaSellerPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId
        {
            get;
        }

        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <value>The principal.</value>
        public AbstractUserPrincipal Principal
        {
            get;
        }

        /// <summary>
        /// Gets the request type.
        /// </summary>
        /// <value>The reuqest type.</value>
        public abstract LpaRequestType RequestType
        {
            get;
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">Any errors encountered.</param>
        /// <returns>True if valid. False otherwise.</returns>
        public virtual bool Validate(out string error)
        {
            if (this.Principal == null)
            {
                error = "Invalid user principal.";
                return false;
            }

            if (this.LoanId == Guid.Empty && this.RequestType != LpaRequestType.Transfer)
            {
                error = "Invalid loan id.";
                return false;
            }

            if (string.IsNullOrEmpty(this.Username) || string.IsNullOrEmpty(this.Password))
            {
                error = "Invalid LPA credentials.";
                return false;
            }

            error = string.Empty;
            return true;
        }
    }
}
