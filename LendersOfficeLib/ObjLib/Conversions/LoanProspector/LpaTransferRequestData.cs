﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using Security;

    /// <summary>
    /// Request data class for transfer requests.
    /// </summary>
    public class LpaTransferRequestData : LpaRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaTransferRequestData"/> class.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="principal">The principal.</param>
        public LpaTransferRequestData(string username, string password, Guid loanId, AbstractUserPrincipal principal)
            : base(username, password, null, null, loanId, principal)
        {
        }

        /// <summary>
        /// Gets the request type.
        /// </summary>
        /// <value>The request type.</value>
        public override LpaRequestType RequestType
        {
            get
            {
                return LpaRequestType.Transfer;
            }
        }

        /// <summary>
        /// Gets or sets the LPA loan id to pull into the system.
        /// </summary>
        /// <value>The LPA loan id.</value>
        public string LpaLoanId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the LPA AUS key.
        /// </summary>
        /// <value>The LPA AUS key.</value>
        public string LpaAusKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the LPA NOTP number.
        /// </summary>
        /// <value>The LPA NOTP number.</value>
        public string LpaNotpNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the LPA Transaction Id.
        /// </summary>
        /// <value>The LPA Transaction Id.</value>
        public string LpaTransactionId
        {
            get;
            set;
        }
    }
}
