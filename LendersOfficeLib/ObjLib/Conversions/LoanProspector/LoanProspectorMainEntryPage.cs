/// Author: David Dao

using System;

using System.Web.UI;
using System.Text;

using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Audit;
using LendersOffice.Security;
using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOffice.Integration.LoanProductAdvisor;

namespace LendersOffice.Conversions.LoanProspector
{
	public class LoanProspectorMainEntryPage : System.Web.UI.Page
	{
        /*
         * LP requires different parameters to be pass.
         * 
         * version - (Required) - Software version - The software vrsion of the LOS/POS in use. Freddie will use this for internal tracking purpose.
         * lpifileformat - (Required) - LPI Generated File Format - Freddie will use this to format the loan data. Valid values: x = xml.
         * epoint - (Required) - Entry Point - The initial action from the user's website when accessing LP
         *                       Valid values: h = Go to LP Main Menu, s = Send to LP, v = View LP Feedback
         * method - (Required) - Interface Method - Freddie will use this to identify web-to-web vendor. Valid vlues: w = web-to-web.
         * lptid - (Optional) - LP Transaction Identifier
         * lplid - (Required View LP Feedback) - LP Loan Identifier
         * gobackurl - (Required) - Return to Your System URL - The URL Freddie Mac will use to return the user to the originating website.
         * desktopurl - (Required) - Send to Your System URL - The URL Freddie Mac will use to pose the LP Response file back to the originating system.
         * 
         */
        //protected string m_url = "";
        private string m_version = "";
        private string m_lpifileformat = "";
        private string m_epoint = "";
        private string m_method = "";
        private string m_lptid = "";
        private string m_lplid = "";
        private string m_gobackurl = "";
        private string m_desktopurl = "";
        private string m_requestData = "";

        //protected string m_html = "";
        protected bool m_hasAutoLogin = false;
        protected string m_basicAuthentication = string.Empty;

        protected void LoadData() 
        {

            string url = ConstStage.LoanProspectorUrl;


            AbstractUserPrincipal userPrincipal = this.User as AbstractUserPrincipal;
            if (null != userPrincipal) 
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(userPrincipal.BrokerId);

                // 5/3/2012 dd - Per case 34215, we are implement the auto login for LP.
                // However due to cross site domain security restriction, this feature is only
                // work on production (not local nor test). And user must enable "Access data sources across domains" in settings.
                string autoLoginName = brokerDB.AutoLoanProspectorLoginName;
                string autoPassword = brokerDB.AutoLoanProspectorPassword.Value;

                m_hasAutoLogin = string.IsNullOrEmpty(autoLoginName) == false;
                if (m_hasAutoLogin)
                {
                    m_basicAuthentication = "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(autoLoginName + ":" + autoPassword));
                }
            }

            this.Form.Action = url;

            m_version = LoanProspectorHelper.NonSeamless48DataVersionNumber; 
            m_lpifileformat = "x";
            m_method = "w";

            
            m_gobackurl = Request.Url.AbsoluteUri.ToLower().Replace("loanprospectormain.aspx" + Request.Url.Query.ToLower() , "loanprospectorsendtosystem.aspx");
            m_desktopurl = m_gobackurl;

            CPageData dataLoan = null;
            switch (RequestHelper.GetSafeQueryString("entrypoint")) 
            {
                case ConstAppDavid.LoanProspector_EntryPoint_GoToMain:
                    m_epoint = ConstAppDavid.LoanProspector_EntryPoint_GoToMain;
                    break;
                case ConstAppDavid.LoanProspector_EntryPoint_SendLp:
                    m_epoint = ConstAppDavid.LoanProspector_EntryPoint_SendLp;

                    dataLoan = new CLoanProspectorMainData(RequestHelper.LoanID);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    
                    m_lplid = dataLoan.sFreddieLoanId;
                    if (dataLoan.sFreddieLoanId == "") 
                    {
                        dataLoan.sFreddieLoanId = ConstAppDavid.LoanProspector_PendingLoanTransaction;
                    }
                    dataLoan.Save();
                    AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
                    SendToLoanProspectorAuditItem audit = new SendToLoanProspectorAuditItem(principal);
                    AuditManager.RecordAudit(RequestHelper.LoanID, audit);

                    //m_requestData = Server.HtmlEncode(GenerateLPRequestFile());
                    m_requestData = GenerateLPRequestFile();
                    break;
                case ConstAppDavid.LoanProspector_EntryPoint_ViewFeedback:
                    m_epoint = ConstAppDavid.LoanProspector_EntryPoint_ViewFeedback;
                    dataLoan = new CLoanProspectorMainData(RequestHelper.LoanID);
                    dataLoan.InitLoad();
                    if (dataLoan.sFreddieLoanId != ConstAppDavid.LoanProspector_PendingLoanTransaction) 
                    {
                        m_lplid = dataLoan.sFreddieLoanId;
                        m_lptid = dataLoan.sFreddieTransactionId;
                    }
                    break;
                default:
                    Tools.LogErrorWithCriticalTracking("Unsupport LP entrypoint. Value=" + RequestHelper.GetSafeQueryString("entrypoint"));
                    return;
            }

            RegisterHiddenVariables();

        }

        private string GenerateLPRequestFile() 
        {
            LoanProspectorExporter exporter = 
                LoanProspectorExporter.CreateExporterForSubmitRequest(
                    isNonSeamless: true, 
                    loanId: RequestHelper.LoanID, 
                    craInfo: LoanSubmitRequest.GetTemporaryCraInformation(RequestHelper.LoanID), 
                    user: PrincipalFactory.CurrentPrincipal);
            string s = exporter.Export();
            Tools.LogInfo(s);
            return s;
        }

        private void RegisterHiddenVariables() 
        {
            
            string[,] list =  {
                            {"version", m_version}
                            , {"lpifileformat", m_lpifileformat}
                            , {"epoint", m_epoint}
                            , {"method", m_method}
                            , {"lptid", m_lptid}
                            , {"lplid", m_lplid}
                            , {"gobackurl", m_gobackurl}
                            , {"desktopurl", m_desktopurl}
                            , {"requestData", m_requestData}
            };
            for (int i = 0; i < list.Length / 2; i++)
            {
                ClientScript.RegisterHiddenField(list[i,0], list[i,1]);
            }

        }
	}
}
