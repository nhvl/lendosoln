<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:param name="VirtualRoot" ></xsl:param>
  <xsl:template match="SERVICE_ORDER_RESPONSE">
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>

        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/jquery-2.1.4.min.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/js/bootstrap.js</xsl:attribute>
        </script>

        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/css/bootstrap.css</xsl:attribute>
        </link>

        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/style.css</xsl:attribute>
        </link>
        
        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/feedback.css</xsl:attribute>
        </link>

      </head>
      <body class="body-stylesheet bg-greybg" >


        <!-- Main Header -->
        <!-- Fixed navbar -->
        <nav class="main-nav col-12" style="margin-left:0px; position:fixed">
          <ul class="main-nav-list">
            <li>
              <a href="#">
                <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/ic-app-lpa-inv.svg</xsl:attribute></img>loan product advisor<span class="servicemark"></span>
              </a>
            </li>

          </ul>
        </nav>
        <!-- End of Header -->
        <!-- Floating Utility-->
        <ul class="utility-list utility-list-custom-right" data-spy="affix">

          <li>
            <i class="ic-print" onclick="javascript:print()"></i>
          </li>

        </ul>


        <!-- End of Floating Utility-->
        <div class="container">
          <div class="row">

            <div class="col-1">
              <div style="height:500px"></div>
            </div>
            <!-- /col-1-->
            <div class="col-10">
              <p class="floating-header ">
                <i class="ic-certificate "></i>
                <xsl:choose>
                  <xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'">
                    <span class="">FHA TOTAL Scorecard Documentation Checklist</span>
                  </xsl:when>
                  <xsl:otherwise>
                    <span class="">Documentation Checklist</span>
                  </xsl:otherwise>
                </xsl:choose>
              </p>

              <div class="anchor" id="EvalSummary" ></div>
              <div class="panel panel-default">
                <div class="bg-textDarkBlue panel-heading  panel-header-stripe"></div>
                <!--/panel-heading -->
                <div class="panel-body panel-padding">
                  <h1 class="header-contained top-header"  >Evaluation Summary</h1>
                  <hr></hr>
                  <div class="row no-margin-row">

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                      <p class="h4-label">PURCHASE ELIGIBILITY</p>


                      <xsl:choose>
                        <xsl:when test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription">
                          <xsl:choose>
                            <xsl:when test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '000FreddieMacEligible'">
                              <p class="call-to-act" style="white-space:nowrap !important;" >
                                <span>
                                  <i class="ic-shield-green active v-align " ></i>
                                </span> &#160; ELIGIBLE
                              </p>
                            </xsl:when>
                            <xsl:when test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '000FreddieMacIneligible'">
                              <p class="call-to-act" style="white-space:nowrap !important;" >
                                <span>
                                  <i class="ic-shield-red active v-align " ></i>
                                </span> &#160; INELIGIBLE
                              </p>
                            </xsl:when>
                            <xsl:when test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '500FreddieMacEligibleLPAMinusOffering'">
                              <p class="call-to-act" style="white-space:nowrap !important;" >
                                <span>
                                  <i class="ic-shield-green active v-align " ></i>
                                </span> &#160;  Eligible LP A Minus Offering.
                              </p>
                            </xsl:when>
                            <xsl:otherwise>
                              <p class="na-gray na-style-evalSummary">N/A</p>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray na-style-evalSummary">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>





                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                      <p class="h4-label" style="white-space:nowrap !important;">RISK CLASS</p>
                      <p>
                        <xsl:choose>
                          <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription">
                            <xsl:choose>
                              <xsl:when test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Accept' ">
                                <span>
                                  <i class="ic-shield-green active v-align " ></i>
                                </span>&#160;
                              </xsl:when>
                              <xsl:when test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Refer' ">
                                <span>
                                  <i class="ic-shield-yellow active v-align "></i>
                                </span>&#160;
                              </xsl:when>
                              <xsl:when test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Caution' ">
                                <span>
                                  <i class="ic-shield-yellow active v-align "></i>
                                </span>&#160;
                              </xsl:when>
                            </xsl:choose>

                            <span class="call-to-act" style="text-transform: uppercase;white-space:nowrap !important;">
                              <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription"/>
                            </span>
                          </xsl:when>
                          <xsl:otherwise>
                            <p class="na-gray na-style-evalSummary " >N/A</p>
                          </xsl:otherwise>
                        </xsl:choose>
                      </p>

                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                      <p class="h4-label" style="white-space:nowrap !important;">COLLATERAL REP  &#38;  WARRANTY RELIEF</p>

                      <p>
                        <xsl:choose>
                          <xsl:when test = "LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType">
                            <xsl:choose>
                              <xsl:when test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'Eligible' ">
                                <span>
                                  <i class="ic-shield-green active v-align " ></i>
                                </span>&#160;&#160;<span class="call-to-act" style="text-transform: uppercase;white-space:nowrap !important;">Eligible</span>
                              </xsl:when>
                              <xsl:when test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'NotEligible' ">
                                <span>
                                  <i class="ic-shield-yellow active v-align " ></i>
                                </span>&#160;&#160;<span class="call-to-act" style="text-transform: uppercase;white-space:nowrap !important;">Not Eligible</span>
                              </xsl:when>
                              <xsl:when test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'Unavailable' ">
                                <span>
                                  <i class="ic-shield-yellow active v-align " ></i>
                                </span>&#160;&#160;<span class="call-to-act" style="text-transform: uppercase;white-space:nowrap !important;">Unavailable</span>
                              </xsl:when>
                            </xsl:choose>


                          </xsl:when>
                          <xsl:otherwise>
                            <p class="na-gray na-style-evalSummary " >N/A</p>
                          </xsl:otherwise>
                        </xsl:choose>
                      </p>

                    </div>
                  </div>







                </div>
                <!-- /panel-body-->

              </div>
              <!--/ panel -->
              <p  class="floating-header ">Loan Data</p>
              <div class="panel panel-default">

                <div class="panel-body panel-padding">
                  <xsl:for-each select="LOAN_APPLICATION/BORROWER">
                    <div class="col-6 col-6-height" style="">
                      <xsl:choose>
                        <xsl:when test="position() = 1">
                          <p class="h4-label">BORROWER NAME</p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="h4-label">CO-BORROWER NAME(S)</p>
                        </xsl:otherwise>
                      </xsl:choose>




                      <p class="body-navy break-word">
                        <xsl:value-of select="@_FirstName"/>&#160;<xsl:value-of select="@_MiddleName"/>&#160;<xsl:value-of select="@_LastName"/>&#160;<xsl:value-of select="@_NameSuffix"/>
                        <br></br>
                        <xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)"/>
                      </p>

                      <br></br>
                    </div>

                  </xsl:for-each>






                  <div class="row no-margin-rollover-4 ">

                    <xsl:choose>
                      <xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'">
                        <xsl:for-each select="LOAN_APPLICATION/BORROWER">
                          <div class="col-6 col-6-height" style="">
                            <xsl:choose>
                              <xsl:when test="position() = 1">
                                <p class="h4-label">BORROWER NAME</p>
                              </xsl:when>
                              <xsl:otherwise>
                                <p class="h4-label">CO-BORROWER NAME(S)</p>
                              </xsl:otherwise>
                            </xsl:choose>
                            <p class="body-navy break-word">
                              <xsl:value-of select="@_FirstName"/>&#160;<xsl:value-of select="@_MiddleName"/>&#160;<xsl:value-of select="@_LastName"/>&#160;<xsl:value-of select="@_NameSuffix"/>
                              <br></br>
                              <xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)"/>
                            </p>
                          </div>
                        </xsl:for-each>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="KEY/@_Value = '4.0'">
                            <xsl:for-each select="LOAN_APPLICATION/BORROWER">
                              <div class="col-6 col-6-height" style="">
                                <xsl:choose>
                                  <xsl:when test="position() = 1">
                                    <p class="h4-label">BORROWER NAME</p>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <p class="h4-label">CO-BORROWER NAME(S)</p>
                                  </xsl:otherwise>
                                </xsl:choose>




                                <p class="body-navy break-word">
                                  <xsl:value-of select="@_FirstName"/>&#160;<xsl:value-of select="@_MiddleName"/>&#160;<xsl:value-of select="@_LastName"/>&#160;<xsl:value-of select="@_NameSuffix"/>
                                  <br></br>
                                  <xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)"/>
                                </p>

                                <br></br>
                              </div>

                            </xsl:for-each>

                          </xsl:when>
                          <xsl:when test="KEY/@_Value = '4.1'">
                            <xsl:for-each select="LOAN_APPLICATION/BORROWER">
                              <div class="col-6 col-6-height" style="">
                                <xsl:choose>
                                  <xsl:when test="position() = 1">
                                    <p class="h4-label">BORROWER NAME</p>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <p class="h4-label">CO-BORROWER NAME(S)</p>
                                  </xsl:otherwise>
                                </xsl:choose>




                                <p class="body-navy break-word">
                                  <xsl:value-of select="@_FirstName"/>&#160;<xsl:value-of select="@_MiddleName"/>&#160;<xsl:value-of select="@_LastName"/>&#160;<xsl:value-of select="@_NameSuffix"/>
                                  <br></br>
                                  <xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)"/>
                                </p>

                                <br></br>
                              </div>

                            </xsl:for-each>

                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:for-each select="SERVICE_ORDER_APPLICATION/LOAN_APPLICATION/BORROWER">
                              <div class="col-6 col-6-height" style="">
                                <xsl:choose>
                                  <xsl:when test="position() = 1">
                                    <p class="h4-label">BORROWER NAME</p>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <p class="h4-label">CO-BORROWER NAME(S)</p>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <p class="body-navy break-word">
                                  <xsl:value-of select="@_FirstName"/>&#160;<xsl:value-of select="@_MiddleName"/>&#160;<xsl:value-of select="@_LastName"/>&#160;<xsl:value-of select="@_NameSuffix"/>
                                  <br></br>
                                  <xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)"/>-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)"/>
                                </p>

                              </div>

                            </xsl:for-each>

                          </xsl:otherwise>
                        </xsl:choose>

                      </xsl:otherwise>
                    </xsl:choose>

                  </div>
                  <!-- /row-->
                  <hr></hr>
                  <div class="row">
                    <div class="col-4" >
                      <p class="h4-label">APPRAISAL IDENTIFIER</p>
                      <xsl:choose>

                        <xsl:when test = "LOAN_APPLICATION/PROPERTY/_VALUATION/EXTENSION_VALUATION/@AppraisalIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/PROPERTY/_VALUATION/EXTENSION_VALUATION/@AppraisalIdentifier"/>
                          </p>

                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>
                    </div>
                    <!-- /col -->
                    <div class="col-4" >
                      <p class="h4-label">LP AUS KEY</p>

                      <xsl:choose>

                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorKeyIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier"/>
                          </p>

                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <!-- /col -->
                    <div class="col-4" >
                      <p class="h4-label">LOAN APPLICATION NUMBER</p>
                      <xsl:choose>
                        <xsl:when test="LOAN_APPLICATION/MORTGAGE_TERMS/@LenderCaseIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/MORTGAGE_TERMS/@LenderCaseIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>
                    </div>
                    <!-- /col -->

                  </div>
                  <!--/row-->
                  <div class="row">
                    <div class="col-4">
                      <p class="h4-label">AUS Transaction Number:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <div class="col-4">
                      <p class="h4-label">Loan Prospector ID:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorLoanIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <div class="col-4">
                      <p class="h4-label">Transaction ID:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                  </div>
                  <!-- /row-->
                  <hr></hr>
                  <div class="row">
                    <div class="col-4" >
                      <p class="h4-label">PROPERTY ADDRESS</p>

                      <xsl:choose>
                        <xsl:when test="LOAN_APPLICATION/PROPERTY/@_StreetAddress">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_StreetAddress"/>,<br></br><xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_City"/>,<br></br><xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State"/>&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode"/>
                          </p>
                        </xsl:when>
                        <xsl:when test="LOAN_APPLICATION/PROPERTY/@_City">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_City"/>,<br></br><xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State"/>&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode"/>
                          </p>
                        </xsl:when>
                        <xsl:when test="LOAN_APPLICATION/PROPERTY/@_State">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State"/>&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode"/>
                          </p>
                        </xsl:when>
                        <xsl:when test="LOAN_APPLICATION/PROPERTY/@_PostalCode">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>


                    </div>
                    <!-- /col -->
                    <div class="col-4" >
                      <p class="h4-label">MORTGAGE TYPE</p>

                      <xsl:choose>
                        <xsl:when test="LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>


                    </div>
                    <!-- /col -->
                    <div class="col-4" >
                      <p class="h4-label">DOCUMENTATION LEVEL</p>

                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@FREDocumentationLevelDescription">
                          <p class="body-navy" style="">
                            <xsl:value-of select="LOAN_FEEDBACK/@FREDocumentationLevelDescription"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray" style="">N/A</p>
                        </xsl:otherwise>

                      </xsl:choose>


                    </div>
                    <!-- /col -->
                  </div>
                  <!--/row-->
                  <xsl:choose>
                    <xsl:when test = "LOAN_FEEDBACK/@FRECreditScoreLTVFeeLevelDescription">
                      <div class="row">
                        <div class="col-4">
                          <p class="h4-label">CS/LTV Fee Level</p>
                          <p class="body-navy" style="">
                            <xsl:value-of select="LOAN_FEEDBACK/@FRECreditScoreLTVFeeLevelDescription"/>
                          </p>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4"></div>

                      </div>
                    </xsl:when>
                    <xsl:otherwise></xsl:otherwise>

                  </xsl:choose>


                </div>
                <!-- /panel-body -->
              </div>
              <!-- /panel-->

              <h1 class="floating-header"  >Received</h1>
              <div class="panel panel-default">
                <div class="panel-body panel-padding">
                  <p class="na-gray">
                    This checklist reflects those items required for Loan Application Number:
                    <xsl:choose>
                      <xsl:when test="LOAN_FEEDBACK/@LenderCaseIdentifier">
                        <xsl:value-of select="LOAN_FEEDBACK/@LenderCaseIdentifier"/>,
                      </xsl:when>
                      <xsl:otherwise>N/A,</xsl:otherwise>
                    </xsl:choose>

                    Submitting Company:
                    <xsl:choose>
                      <xsl:when test="LOAN_FEEDBACK/@SubmittingCompanyName">
                        <xsl:value-of select="LOAN_FEEDBACK/@SubmittingCompanyName"/>.
                      </xsl:when>
                      <xsl:otherwise>N/A.</xsl:otherwise>
                    </xsl:choose>
                  </p>
                  <b>
                    <p class="na-gray">Please Note: </p>
                  </b>
                  <p class="na-gray" style="font-style:italic">Any changes to loan data could change the Doc. Checklist requirements. If you would like more details on these guidelines, please consult the Full Feedback Certificate.</p>
                  <hr></hr>
                  <xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
                    <xsl:if test="@_Type = 'DocumentationMessages'">

                      <xsl:for-each select="FEEDBACK_MESSAGE">
                        <xsl:if test="@MessageCode != 'TJ' and @MessageCode != 'RZ' and @MessageCode != 'RY'">




                          <div class="row">
                            <div class="col-1">
                              <input id="check" class="form-checkbox" type="checkbox">
                                <xsl:attribute name="id">
                                  <xsl:value-of select="concat('check',position())" />
                                </xsl:attribute>
                              </input>
                              <label for="check" style="font-weight:unset">
                                <xsl:attribute name="for">
                                  <xsl:value-of select="concat('check',position())" />
                                </xsl:attribute>
                              </label>
                            </div>
                            <div class="col-11">
                              <p class="body-navy" >



                                <xsl:value-of select="@MessageText"/>

                              </p>
                            </div>
                          </div>







                        </xsl:if>
                      </xsl:for-each>

                    </xsl:if>
                  </xsl:for-each>

                  <xsl:choose>
                    <xsl:when test="(LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType != 'FHA') and (LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType != 'VA')">
                      <hr></hr>
                      <div class="row">
                        <div class="col-1">
                          <i  class="ic-info"></i>
                        </div>
                        <div class="col-11">
                          <p class="body-mini" style="font-style:italic">

                            <b>Notice:</b>This checklist is not a replacement or substitution for the requirements and information set forth in the Freddie Mac Single-Family Seller/Servicer Guide, LPA Web Interface Online Help, LPA User Agreement, or LPA Functionality Guide.
                          </p>
                        </div>
                      </div>




                    </xsl:when>
                  </xsl:choose>
                </div>
                <!-- /panel-body-->
              </div>
              <!-- /panel -->
            </div>
            <!--/col-10 -->
            <div class="col-1">
              <div style="height:500px"></div>
            </div>
            <!-- /col-1-->
          </div>
        </div>
        <footer class="footer row" style="overflow: auto; margin-bottom:0 !important">
          <section class="col-2">
            <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/lgo-freddiemac-inv.svg</xsl:attribute></img>
          </section>
          <section class="col-4 pull-right" style="height: 40.5px;">
            <div>
              <ul>
                <li id="footer_year" style="font-size:11px;margin-top:15px;color:rgba(170, 183, 194, 0.5) !important;">&#169; Freddie Mac</li>
              </ul>
            </div>
            <ul>
              <li>&#032;</li>
            </ul>
          </section>
        </footer>
        <script>
          var date_year = "<xsl:value-of select="@_Date"/>";
          date_year = date_year.split("-");
          document.getElementById("footer_year").innerHTML = "&#169;"+date_year[2]+" Freddie Mac";
        </script>
      </body>
    </html>
    <xsl:comment>
      This page uses third party software.
      For license information, please see
      https://secure.lendingqb.com/pml_shared/FreddieMac/ThirdPartyLicenses.txt
    </xsl:comment>
  </xsl:template>
</xsl:stylesheet>