﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using Integration.Templates;
    using Drivers.Gateways;
    using LendersOffice.Security;
    using LoanProspectorResponse;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Class to send LPA requests.
    /// </summary>
    public class LoanProspectorServer : AbstractIntegrationServer
    {
        /// <summary>
        /// The user id.
        /// </summary>
        private string userId;

        /// <summary>
        /// The password.
        /// </summary>
        private string password;

        /// <summary>
        /// Any errors encountered.
        /// </summary>
        private List<string> errors = new List<string>();

        /// <summary>
        /// Initializes an instance of the <see cref="LoanProspectorServer"/> class.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        public LoanProspectorServer(string userId, string password)
        { 
            this.userId = userId;
            this.password = password;
        }

        /// <summary>
        /// Gets the errors encountered.
        /// </summary>
        /// <returns>The errors encountered.</returns>
        public override List<string> GetErrors()
        {
            return this.errors;
        }

        /// <summary>
        /// Places the request.
        /// </summary>
        /// <param name="request">The request to submit.</param>
        /// <returns>The string response.</returns>
        public override string PlaceRequest(string request)
        {
            string url = Constants.ConstStage.SeamlessLpaUrl;

            byte[] bytes = UTF8Encoding.UTF8.GetBytes(request);

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false;
            webRequest.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{this.userId}:{this.password}"))}");
            webRequest.ContentType = "text/plain";
            webRequest.Method = "POST";

            webRequest.ContentLength = bytes.Length;
            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            StringBuilder sb = new StringBuilder();
            try
            {
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[60000];
                        int size = stream.Read(buffer, 0, buffer.Length);

                        while (size > 0)
                        {
                            sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                            size = stream.Read(buffer, 0, buffer.Length);
                        }
                    }
                }

                return sb.ToString();
            }
            catch (WebException exc) when ((exc.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.Unauthorized)
            {
                this.errors.Add("Unauthorized. Credentials may be invalid.");
                return null;
            }
            catch (WebException exc) when ((exc.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.Forbidden)
            {
                this.errors.Add("Forbidden. Make sure that your credentials are correct and for system-to-system (S2S).");
                return null;
            }
            catch (InvalidOperationException exc)
            {
                throw new DeveloperException(ErrorMessage.SystemError, exc);
            }
        }

        public static MismoServiceOrderResponse SubmitSynchronous(string lpUserId, string lpPassword, AbstractUserPrincipal principal, Guid sLId, out string rawResponse)
        {
            // TODO SEAMLESS LPA: Either fix this or get rid of this when doing the webservices part.
            rawResponse = null;
            return null;
            /*string asyncId = string.Empty;
            rawResponse = string.Empty;
            int pollingInterval = 0;
            for (int i = 0; i < 20; i++)
            {
                //string lpUserId = "cmgm_lps2sr1";
                //string lpPassword = "Cmg13lqb";	
                // Polling

                string requestXml = string.Empty;
                if (string.IsNullOrEmpty(asyncId) == false)
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(LoanProspectorServer));
                    dataLoan.InitLoad();

                    requestXml = LoanProspectorExporter.CreatePollingRequest(asyncId,
                        dataLoan.sFreddieSellerNum, dataLoan.sFreddieLpPassword.Value,
                        dataLoan.sFreddieNotpNum, dataLoan.sFreddieTpoNum);

                }
                else
                {
                    // New Request
                    LoanProspectorExporter exporter = LoanProspectorExporter.CreateSeamlessExporter(sLId);
                    requestXml = exporter.Export();
                }
                ParseResult<MismoServiceOrderResponse> parsedResponse = Submit(lpUserId, lpPassword, BrokerUserPrincipal.CurrentPrincipal, sLId, requestXml);
                rawResponse = parsedResponse.Input;
                var response = parsedResponse.ValueOrDefault ?? new MismoServiceOrderResponse();

                if (response.Status.Processing == E_MismoStatusProcessing.Partial)
                {
                    if (response.PollingStatus != null)
                    {
                        if (int.TryParse(response.PollingStatus.PollingInterval, out pollingInterval) == false)
                        {
                            // 7/30/2013 dd - Default polling interval to 10 seconds if we cannot parse from LP response.
                            pollingInterval = 10;
                        }
                        asyncId = response.PollingStatus.LoanProspectorAsynchronousIdentifier;
                    }
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(pollingInterval * 1000);
                }
                else if (response.Status.Processing == E_MismoStatusProcessing.Completed)
                {
                    LoanProspectorImporter importer = new LoanProspectorImporter(parsedResponse, isImport1003: true, isImportFeedback: true, isImportCreditReport: false, isImportLiabilitiesFromCreditReport: false);
                    importer.Import();

                    return response;
                }
                else if (response.Status.Processing == E_MismoStatusProcessing.Error)
                {
                    return response;
                }
 
            }
            throw new TimeoutException("Could not get LP response in timely manner.");*/
        }
    }
}
