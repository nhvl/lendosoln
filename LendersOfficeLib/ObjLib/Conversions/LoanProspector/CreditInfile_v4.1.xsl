<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:param name="VirtualRoot" ></xsl:param>
  <xsl:template match="SERVICE_ORDER_RESPONSE">
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>

        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/jquery-2.1.4.min.js</xsl:attribute>
        </script>
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/js/bootstrap.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/d3.v3.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/radialProgress.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/liquidFillGuage.js</xsl:attribute>
        </script>

        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/css/bootstrap.css</xsl:attribute>
        </link>

        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/D3style.css</xsl:attribute>
        </link>
        
        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/style.css</xsl:attribute>
        </link>
        
        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/feedback.css</xsl:attribute>
        </link>

      </head>
      <body class="body-stylesheet bg-greybg">
        <!-- Main Header -->
        <!-- Fixed navbar -->
        <nav class="main-nav col-12" style="margin-left:0px; position:fixed">
          <ul class="main-nav-list">
            <li>
              <a href="#">
                <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/ic-app-lpa-inv.svg</xsl:attribute></img>loan product advisor<span class="servicemark"></span>
              </a>
            </li>

          </ul>
        </nav>


        <!-- End of Header -->
        <!-- Floating Utility-->
        <ul class="utility-list utility-list-custom-right" data-spy="affix">

          <li>
            <i class="ic-print" onclick="javascript:print()" ></i>
          </li>

        </ul>


        <!-- End of Floating Utility-->
        <div class="container bg-greybg">
          <div class="col-1" style="height:500px"></div>
          <div class="col-10">
            <p class="floating-header">Credit Infiles </p>
            <div class="row">
              <div class="panel panel-default">
                <div class="panel-body">
                  <PRE>
                    <xsl:value-of select="LOAN_FEEDBACK/CREDIT_INFILE_REPORT"/>
                  </PRE>
                </div>
              </div>
            </div>
          </div>
          <div class="col-1" style="height:500px"></div>
        </div>





      </body>
      <footer class="footer row" style="overflow: auto; margin-bottom:0 !important">
        <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/lgo-freddiemac-inv.svg</xsl:attribute></img>
        <section class="col-4 pull-right" style="height: 40.5px;">
          <div>
            <ul>
              <li id="footer_year" style="font-size:11px;margin-top:15px;color:rgba(170, 183, 194, 0.5) !important;">&#169; Freddie Mac</li>
            </ul>
          </div>
          <ul>
            <li>&#032;</li>
          </ul>
        </section>
      </footer>
      <script>
        var date_year = "<xsl:value-of select="@_Date"/>";
        date_year = date_year.split("-");
        document.getElementById("footer_year").innerHTML = "&#169;"+date_year[2]+" Freddie Mac";
      </script>
    </html>
    <xsl:comment>
      This page uses third party software.
      For license information, please see
      https://secure.lendingqb.com/pml_shared/FreddieMac/ThirdPartyLicenses.txt
    </xsl:comment>
  </xsl:template>
</xsl:stylesheet>