/// Author: David Dao
namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using DataAccess.Core.Construction;
    using Integration.LoanProductAdvisor;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    using LoanProspectorRequest;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using ObjLib.Conversions.Aus;
    using ObjLib.ServiceCredential;
    using Security;

    public class LoanProspectorExporter
    {
        private const string ServiceRequest_OrderAus = "OrderAUS";
        private const string ServiceRequest_OrderMergedCreditAll = "OrderMergedCreditAll";

        private bool m_hasAtLeastOneCreditReportIdentifier = false;
        private REOCounter m_REOCounter = new REOCounter();

        private Lazy<CLoanProspectorExporterData> dataLoan = null;

        private string pollingAsyncId;
        private AusCreditOrderingInfo craInformation;
        private LpaRequestType requestType;
        private string dataVersionNumber;

        private Lazy<MismoLoanApplication> loanApplicationPayload;
        private LpaRequestData requestData;

        private string sellerNumber;
        private string sellerPassword;

        /// <summary>
        /// Initializes an instance of the <see cref="LoanProspectorExporter"/> class. Uses request data to initialize.
        /// </summary>
        /// <param name="requestData">The request data to initialize from.</param>
        private LoanProspectorExporter(LpaRequestData requestData)
            : this(requestData.RequestType, requestData.LoanId, requestData.Principal)
        {
            if (requestData is LpaPollRequestData)
            {
                var data = requestData as LpaPollRequestData;
                this.pollingAsyncId = data.AsyncId;
            }
            else if (requestData is LpaSubmitRequestData)
            {
                var data = requestData as LpaSubmitRequestData;
                this.craInformation = data.CraInfo;
            }

            this.sellerNumber = requestData.LpaSellerNumber;
            this.sellerPassword = requestData.LpaSellerPassword;

            this.requestData = requestData;
        }

        /// <summary>
        /// Initializes an instance of the <see cref="LoanProspectorExporter"/> class. Only for submit requests.
        /// </summary>
        /// <param name="isNonSeamless">Whether this is a non-seamless export or not.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="craInformation">The CRA information.</param>
        private LoanProspectorExporter(bool isNonSeamless, Guid loanId, AusCreditOrderingInfo craInformation, AbstractUserPrincipal user)
            : this(isNonSeamless ? LpaRequestType.NonSeamlessLpaSubmit : LpaRequestType.SeamlessLpaSubmit, loanId, user)
        {
            var sellerCredential = ServiceCredential.ListAvailableServiceCredentials(user, this.DataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                    .FirstOrDefault(cred => cred.ChosenAusOption == AusOption.LpaSellerCredential);
            this.craInformation = craInformation;
            this.sellerNumber = sellerCredential?.UserName ?? this.DataLoan.sFreddieSellerNum;
            this.sellerPassword = sellerCredential?.UserPassword.Value ?? this.DataLoan.sFreddieLpPassword.Value;
        }

        /// <summary>
        /// Initializes an instance of the <see cref="LoanProspectorExporter"/> class.
        /// </summary>
        /// <param name="requestType">The request type.</param>
        /// <param name="loanId">The loan id.</param>
        private LoanProspectorExporter(LpaRequestType requestType, Guid loanId, AbstractUserPrincipal user)
        {
            this.requestType = requestType;
            this.dataLoan = new Lazy<CLoanProspectorExporterData>(() =>
            {
                var loan = new CLoanProspectorExporterData(loanId);
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.FreddieMacAUS);
                return loan;
            });

            if (this.requestType == LpaRequestType.NonSeamlessLpaSubmit)
            {
                this.dataVersionNumber = LoanProspectorHelper.NonSeamless48DataVersionNumber;
            }
            else if (this.requestType == LpaRequestType.SeamlessLpaSubmit)
            {
                this.dataVersionNumber = LoanProspectorHelper.Seamless48DataVersionNumber;
            }

            this.loanApplicationPayload = new Lazy<MismoLoanApplication>(this.CreateLoanApplication);
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        /// <value>The data loan object.</value>
        protected CPageData DataLoan
        {
            get
            {
                return this.dataLoan.Value;
            }
        }

        /// <summary>
        /// Creates an exporter from the settings set in the request data.
        /// </summary>
        /// <param name="requestData">The data for the LPA request.</param>
        /// <returns>The exporter initialized to export data for the request.</returns>
        public static LoanProspectorExporter CreateExporterFromRequestData(LpaRequestData requestData)
        {
            return new LoanProspectorExporter(requestData);
        }

        /// <summary>
        /// Creates an exporter specifically for LPA submit requests. 
        /// </summary>
        /// <param name="isNonSeamless">Whether we're using LPA non-seamless or not.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="craInfo">The CRA information since it is not saved to the loan.</param>
        /// <returns>The exporter for the submit request.</returns>
        /// <remarks>Only really for non-seamless.</remarks>
        public static LoanProspectorExporter CreateExporterForSubmitRequest(bool isNonSeamless, Guid loanId, AusCreditOrderingInfo craInfo, AbstractUserPrincipal user)
        {
            return new LoanProspectorExporter(isNonSeamless: isNonSeamless, loanId: loanId, craInformation: craInfo, user: user);
        }

        /// <summary>
        /// Creates the XML payload.
        /// </summary>
        /// <returns>The payload.</returns>
        public string Export() 
        {
            return ExportImpl(loanApplicationPayload.Value);
        }

        /// <summary>
        /// Logs the payload. The string payload is passed in in case it is needed.
        /// </summary>
        public void LogPayload()
        {
            var request = this.loanApplicationPayload.Value;

            string stringPayload;
            if (string.IsNullOrEmpty(request.ExtensionLoanApplication.FreRequestingParty.Password))
            {
                stringPayload = this.ExportImpl(request);
            }
            else
            {
                var freRequestingParty = request.ExtensionLoanApplication.FreRequestingParty;
                var oldPassword = freRequestingParty.Password;
                freRequestingParty.Password = ConstAppDavid.FakePasswordDisplay;

                stringPayload = this.ExportImpl(request);
                freRequestingParty.Password = oldPassword;
            }

            LoanProspectorHelper.LogPayload(stringPayload, LoanProspectorHelper.LpaRequestHeader);
        }

        private string ExportImpl(MismoLoanApplication loanApplication)
        {
            byte[] bytes = null;
            int contentLength = 0;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                writer.WriteDocType("LOAN_APPLICATION", null, LoanProspectorHelper.DocType43, null); // !DOCTYPE is required by LP.

                loanApplication.WriteXml(writer);
                writer.Flush();
                contentLength = (int)stream.Position;
                bytes = stream.GetBuffer();
            }

            if (null != bytes)
            {
                return System.Text.Encoding.ASCII.GetString(bytes, 0, contentLength);
            }

            return null;
        }

        /// <summary>
        /// Creates the main LOAN_APPLICATION container for LP exports based on the type of request.
        /// </summary>
        /// <returns>The LOAN_APPLICATION container for LP exports.</returns>
        private MismoLoanApplication CreateLoanApplication()
        {
            MismoLoanApplication payload;
            switch (this.requestType)
            {
                case LpaRequestType.NonSeamlessLpaSubmit:
                case LpaRequestType.SeamlessLpaSubmit:
                    payload = this.CreateSubmitLoanApplication();
                    break;
                case LpaRequestType.Poll:
                    payload = this.CreatePollLoanApplication();
                    break;
                case LpaRequestType.Transfer:
                    payload = this.CreateTransferLoanApplication();
                    break;
                default:
                    throw new UnhandledEnumException(this.requestType);
            }

            return payload;
        }

        /// <summary>
        /// Creates the LOAN_APPLICATION container for transfer requests.
        /// </summary>
        /// <returns>The LOAN_APPLICATION container.</returns>
        private MismoLoanApplication CreateTransferLoanApplication()
        {
            MismoLoanApplication loanApplication = new MismoLoanApplication();

            MismoExtensionLoanApplication extensionLoanApplication = new MismoExtensionLoanApplication();

            extensionLoanApplication.FreLoanProspectorVersionIdentifier = LoanProspectorHelper.FreLoanProspectorVersionIdentifier43;

            extensionLoanApplication.FunctionRequest.Type = E_MismoFunctionRequestType.LoanTransferRequest;

            var transferRequestData = this.requestData as LpaTransferRequestData;
            if (transferRequestData == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("LPA Transfer requests require an LPATransferRequestData"));
            }

            extensionLoanApplication.FreRequestingParty.NonOriginatingThirdPartyIdentifier = transferRequestData.LpaNotpNumber;
            extensionLoanApplication.FreRequestingParty.Password = transferRequestData.LpaSellerPassword;
            extensionLoanApplication.FreRequestingParty.SellerServicerNumber = transferRequestData.LpaSellerNumber;

            extensionLoanApplication.LoanIdentifierData.LoanProspectorKeyIdentifier = transferRequestData.LpaAusKey;
            extensionLoanApplication.LoanIdentifierData.LoanProspectorLoanIdentifier = transferRequestData.LpaLoanId;
            extensionLoanApplication.LoanIdentifierData.FreLoanProspectorTransactionIdentifier = transferRequestData.LpaTransactionId;

            loanApplication.ExtensionLoanApplication = extensionLoanApplication;

            return loanApplication;
        }

        /// <summary>
        /// Creates the LOAN_APPLICATION container for poll requests.
        /// </summary>
        /// <returns>The LOAN_APPLICATION container.</returns>
        private MismoLoanApplication CreatePollLoanApplication()
        {
            MismoLoanApplication loanApplication = new MismoLoanApplication();

            MismoExtensionLoanApplication extensionLoanApplication = new MismoExtensionLoanApplication();
            extensionLoanApplication.FreLoanProspectorVersionIdentifier = LoanProspectorHelper.FreLoanProspectorVersionIdentifier43;
            extensionLoanApplication.FunctionRequest.Type = E_MismoFunctionRequestType.PollingRequest;

            extensionLoanApplication.FreRequestingParty = this.CreateFreRequestingParty();

            extensionLoanApplication.PollingStatus.LoanProspectorAsynchronousIdentifier = this.pollingAsyncId;

            loanApplication.ExtensionLoanApplication = extensionLoanApplication;
            return loanApplication;
        }

        /// <summary>
        /// Creates the LOAN_APPLICATION container for submit requests.
        /// </summary>
        /// <returns>The LOAN_APPLICATION container.</returns>
        private MismoLoanApplication CreateSubmitLoanApplication() 
        {
            MismoLoanApplication loanApplication = new MismoLoanApplication();

            MismoDataVersion dataVersion = new MismoDataVersion();
            dataVersion.Number = this.dataVersionNumber;
            loanApplication.DataInformation.DataVersion.Add(dataVersion);

            for (int borrowerIndex = 0; borrowerIndex < this.DataLoan.nApps; borrowerIndex++) 
            {
                CAppData dataApp = this.DataLoan.GetAppData(borrowerIndex);
                bool hasBorrowerInfo = dataApp.aBLastNm != "" || dataApp.aBFirstNm != "" || dataApp.aBSsn != "";
                bool hasCoborrowerInfo = dataApp.aCLastNm != "" || dataApp.aCFirstNm != "" || dataApp.aCSsn != "";
                
                // OPM 184017. Keep non-purchasing borrowers out of export
                if (dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    if (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly)
                    {
                        hasBorrowerInfo = false;
                    }
                    if (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)
                    {
                        hasCoborrowerInfo = false;
                    }
                }

                if (!hasBorrowerInfo && !hasCoborrowerInfo)
                    continue; // Skip if there is no borrower & no coborrower.

                string borrowerId = "B" + dataApp.aBSsn.Replace("-", "");

                #region Assets
                IAssetCollection assetCollection = dataApp.aAssetCollection;

                loanApplication.Asset.Add(CreateAsset(assetCollection.GetBusinessWorth(false), borrowerId));
                loanApplication.Asset.Add(CreateAsset(assetCollection.GetLifeInsurance(false), borrowerId));
                loanApplication.Asset.Add(CreateAsset(assetCollection.GetRetirement(false), borrowerId));
                loanApplication.Asset.Add(CreateAsset(assetCollection.GetCashDeposit1(false), borrowerId));
                loanApplication.Asset.Add(CreateAsset(assetCollection.GetCashDeposit2(false), borrowerId));

                int assetRecordCount = assetCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < assetRecordCount; recordIndex++) 
                {
                    loanApplication.Asset.Add(CreateAsset(assetCollection.GetRegularRecordAt(recordIndex), borrowerId));
                }
                #endregion

                ////IR - OPM 217424: Evaluate REOs prior to Liabilities so that mortgage tradelines can be associated with appropriate REOs.
                #region REOs
                var reCollection = dataApp.aReCollection;
                int reRecordCount = reCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < reRecordCount; recordIndex++)
                {
                    var reo = reCollection.GetRegularRecordAt(recordIndex);
                    string mismoValue = m_REOCounter.Add(reo.RecordId);
                    loanApplication.ReoProperty.Add(CreateReoProperty(dataApp, reo, mismoValue, borrowerId));
                }
                #endregion

                #region Liabilities
                ILiaCollection liaCollection = dataApp.aLiaCollection;

                loanApplication.Liability.Add(CreateLiability(liaCollection.GetAlimony(false), borrowerId));
                loanApplication.Liability.Add(this.CreateLiability(liaCollection.GetChildSupport(false), borrowerId));
                loanApplication.Liability.Add(CreateLiability(liaCollection.GetJobRelated1(false), borrowerId));
                loanApplication.Liability.Add(CreateLiability(liaCollection.GetJobRelated2(false), borrowerId));

                int liabilityRecordCount = liaCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < liabilityRecordCount; recordIndex++) 
                {
                    loanApplication.Liability.Add(CreateLiability(dataApp, liaCollection.GetRegularRecordAt(recordIndex), borrowerId));
                }

                #endregion

                if (hasBorrowerInfo) 
                {
                    loanApplication.Borrower.Add(CreateBorrower(dataApp, true, hasCoborrowerInfo));
                }

                if (hasCoborrowerInfo) 
                {
                    loanApplication.Borrower.Add(CreateBorrower(dataApp, false, hasBorrowerInfo));
                }

                loanApplication.TitleHolder.Add(CreateTitleHolder(dataApp.aTitleNm1));
                loanApplication.TitleHolder.Add(CreateTitleHolder(dataApp.aTitleNm2));
            }

            if (this.DataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                if (this.DataLoan.sIsUseDUDwnPmtSrc)
                {
                    foreach (var downPayment in this.DataLoan.sDUDwnPmtSrc)
                    {
                        if (downPayment.Amount > 0)
                        {
                            loanApplication.DownPayment.Add(this.CreateDownPayment(downPayment.Amount_rep, downPayment.Explanation, downPayment.Source));
                        }
                    }
                }
                else
                {
                    loanApplication.DownPayment.Add(this.CreateDownPayment(this.DataLoan.sEquityCalc_rep, this.DataLoan.sDwnPmtSrcExplain, this.DataLoan.sDwnPmtSrc));
                }
            }

            #region Proposed Housing Expense
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.MI, this.DataLoan.sProMIns_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.HazardInsurance, this.DataLoan.sProHazIns_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, this.DataLoan.sProSecondMPmt_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.OtherHousingExpense, this.DataLoan.sProOHExp_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.RealEstateTax, this.DataLoan.sProRealETx_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, this.DataLoan.sProHoAssocDues_rep));
            loanApplication.ProposedHousingExpense.Add(CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, this.DataLoan.sProFirstMPmt_rep));

            #endregion
            loanApplication.AdditionalCaseData = CreateAdditionalCaseData();
            loanApplication.MortgageTerms = CreateMortgageTerms();
            loanApplication.Property = CreateProperty();
            loanApplication.ExtensionLoanApplication = CreateExtensionLoanApplication();
            loanApplication.GovernmentLoan = CreateGovernmentLoan();
            loanApplication.InterviewerInformation = CreateInterviewerInformation();

            loanApplication.LoanProductData = CreateLoanProductData();
            loanApplication.LoanPurpose = CreateLoanPurpose();
            loanApplication.TransactionDetail = CreateTransactionDetail();

            loanApplication.ClosingAgent = CreateClosingAgent();
            loanApplication.LoanOriginationSystem = CreateLoanOriginationSystem();

            return loanApplication;
        }

        private MismoReoProperty CreateReoProperty(CAppData dataApp, IRealEstateOwned field, string reoId, string borrowerId) 
        {
            if (null == field)
                return null;

            MismoReoProperty reoProperty = new MismoReoProperty();
            reoProperty.ReoId = reoId;
            reoProperty.BorrowerId = borrowerId;
            Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(field.RecordId);
            string str = "";
            foreach (Guid id in matchedLiabilities) 
            {
                str += "L" + id.ToString("N") + " ";
            }
            if (matchedLiabilities.Length > 0) 
            {
                reoProperty.LiabilityId = str.TrimWhitespaceAndBOM();
            }
            reoProperty.StreetAddress = field.Addr;
            reoProperty.City = field.City;
            reoProperty.State = field.State;
            reoProperty.PostalCode = field.Zip;
            reoProperty.GsePropertyType = ToMismoReoType(field.TypeT);
            reoProperty.DispositionStatusType = ToMismo(field.StatT);
            reoProperty.LienInstallmentAmount = field.MPmt_rep;
            reoProperty.LienUpbAmount = field.MAmt_rep;
            reoProperty.MaintenanceExpenseAmount = field.HExp_rep;
            reoProperty.MarketValueAmount = field.Val_rep;
            reoProperty.RentalIncomeGrossAmount = field.GrossRentI_rep;
            reoProperty.RentalIncomeNetAmount = field.NetRentI_rep;
            reoProperty.SubjectIndicator = ToMismo(field.IsSubjectProp);

            if (field.IsPrimaryResidence)
            {
                reoProperty.CurrentResidenceIndicator = E_YesNoIndicator.Y;
            } 
            else 
            {
                // 5/7/2008 dd - We will try to match the reo property address with borrower current address.
                if (reoProperty.StreetAddress.ToLower() == dataApp.aBAddr.ToLower())
                    reoProperty.CurrentResidenceIndicator = E_YesNoIndicator.Y;
                else
                    reoProperty.CurrentResidenceIndicator = E_YesNoIndicator.N;
            }

            return reoProperty;
        }
        private MismoLiability CreateLiability(ILiabilityJobExpense jobExpense, string borrowerId) 
        {
            if (null == jobExpense)
                return null;

            if (jobExpense.Pmt_rep == "" || jobExpense.Pmt_rep == "0.00")
                return null;

            MismoLiability liability = new MismoLiability();
            liability.Id = "L" + jobExpense.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_MismoLiabilityType.JobRelatedExpenses;
            liability.HolderName = jobExpense.ExpenseDesc;
            liability.MonthlyPaymentAmount = jobExpense.Pmt_rep;
            liability.RemainingTermMonths = "0";

            liability.ExtensionLiability = this.CreateExtensionLiability();

            return liability;
        }
        private MismoLiability CreateLiability(ILiabilityAlimony alimony, string borrowerId) 
        {
            if (null == alimony)
                return null;

            if (alimony.Pmt_rep == "" || alimony.Pmt_rep == "0.00")
                return null;

            MismoLiability liability = new MismoLiability();
            liability.Id = "L" + alimony.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_MismoLiabilityType.Alimony;
            liability.RemainingTermMonths = alimony.RemainMons_rep;
            liability.MonthlyPaymentAmount = alimony.Pmt_rep;
            liability.AlimonyOwedToName = alimony.OwedTo;
            liability.ExclusionIndicator = ToMismo(alimony.NotUsedInRatio);

            liability.ExtensionLiability = this.CreateExtensionLiability();
            return liability;
        }
        private MismoLiability CreateLiability(ILiabilityChildSupport childSupport, string borrowerId)
        {
            if (null == childSupport)
                return null;

            if (childSupport.Pmt_rep == "" || childSupport.Pmt_rep == "0.00")
                return null;

            MismoLiability liability = new MismoLiability();
            liability.Id = "L" + childSupport.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_MismoLiabilityType.ChildSupport;
            liability.RemainingTermMonths = childSupport.RemainMons_rep;
            liability.MonthlyPaymentAmount = childSupport.Pmt_rep;
            liability.ExclusionIndicator = ToMismo(childSupport.NotUsedInRatio);
            return liability;
        }
        private MismoLiability CreateLiability(CAppData dataApp, ILiabilityRegular field, string borrowerId) 
        {
            if (null == field)
                return null;

            MismoLiability liability = new MismoLiability();
            liability.Id = "L" + field.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            if (field.DebtT == E_DebtRegularT.Mortgage && field.MatchedReRecordId != Guid.Empty) 
            {
                liability.ReoId = m_REOCounter.Retrieve(field.MatchedReRecordId);
            }
            if (field.DebtT == E_DebtRegularT.Mortgage)
            {
                if (field.IsPiggyBack)
                {
                    liability.SubjectLoanResubordinationIndicator = E_YesNoIndicator.Y;
                }
                else
                {
                    liability.SubjectLoanResubordinationIndicator = E_YesNoIndicator.N;
                }
            }       
            liability.HolderStreetAddress = field.ComAddr;
            liability.HolderCity = field.ComCity;
            liability.HolderState = field.ComState;
            liability.HolderPostalCode = field.ComZip;
            liability.AccountIdentifier = field.AccNum.Value;
            liability.HolderName = field.ComNm;
            liability.MonthlyPaymentAmount = field.Pmt_rep;
            liability.RemainingTermMonths = field.GetRemainingMonthsForLpa();
            liability.Type = ToMismo(field.DebtT, field.Desc);
            liability.UnpaidBalanceAmount = field.Bal_rep;
            if (field.DebtT == E_DebtRegularT.Mortgage) 
            {
                // 5/7/2008 dd - LendingQB always exclude mortgage liability to be include in DTI calculation. However
                // when we export to LP, we have to turn off ExclusionIndicator if liability is not paid off.
                liability.ExclusionIndicator = ToMismo(field.WillBePdOff);
            } 
            else 
            {
                liability.ExclusionIndicator = ToMismo(field.NotUsedInRatio);
            }

            liability.ExtensionLiability = this.CreateExtensionLiability(field);
            if (liability.ExtensionLiability.LiabilityPartialPayoffIndicator == E_YesNoIndicator.Y)
            {
                liability.PayoffStatusIndicator = E_YesNoIndicator.N;
            }
            else
            {
                liability.PayoffStatusIndicator = ToMismo(field.WillBePdOff);
            }

            return liability;
        }

        private EXTENSION_LIABILITY CreateExtensionLiability()
        {
            var extensionLiability = new EXTENSION_LIABILITY();
            extensionLiability.LiabilityPartialPayoffIndicator = E_YesNoIndicator.N;

            return extensionLiability;
        }

        private EXTENSION_LIABILITY CreateExtensionLiability(ILiabilityRegular liability)
        {
            EXTENSION_LIABILITY extensionLiability = new EXTENSION_LIABILITY();
            if (liability.WillBePdOff && liability.PayoffAmt < liability.Bal)
            {
                extensionLiability.LiabilityPartialPayoffIndicator = E_YesNoIndicator.Y;
                extensionLiability.LiabilityPartialPayoffAmount = liability.PayoffAmt_rep;
            }
            else
            {
                extensionLiability.LiabilityPartialPayoffIndicator = E_YesNoIndicator.N;
            }

            return extensionLiability;
        }

        private MismoAsset CreateAsset(IAssetCashDeposit cashDeposit, string borrowerId) 
        {
            if (null == cashDeposit)
                return null;

            if (cashDeposit.Val_rep == "" || cashDeposit.Val_rep == "0.00")
                return null;

            MismoAsset asset = new MismoAsset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_MismoAssetType.EarnestMoneyCashDepositTowardPurchase;
            asset.CashOrMarketValueAmount = cashDeposit.Val_rep;
            asset.HolderName = cashDeposit.Desc;
            return asset;
        }
        private MismoAsset CreateAsset(IAssetRetirement retirement, string borrowerId) 
        {
            if (null == retirement)
                return null;

            if (retirement.Val_rep == "" || retirement.Val_rep == "0.00")
                return null;

            MismoAsset asset = new MismoAsset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_MismoAssetType.RetirementFund;
            asset.CashOrMarketValueAmount = retirement.Val_rep;
            return asset;
        }
        private MismoAsset CreateAsset(IAssetLifeInsurance lifeIns, string borrowerId) 
        {
            if (null == lifeIns)
                return null;

            if (lifeIns.Val_rep == "" || lifeIns.Val_rep == "0.00")
                return null;

            MismoAsset asset = new MismoAsset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_MismoAssetType.LifeInsurance;
            asset.CashOrMarketValueAmount = lifeIns.Val_rep;
            asset.LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep;

            return asset;
        }
        private MismoAsset CreateAsset(IAssetBusiness business, string borrowerId) 
        {
            if (null == business)
                return null;

            if (business.Val_rep == "" || business.Val_rep == "0.00")
                return null;

            MismoAsset asset = new MismoAsset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_MismoAssetType.NetWorthOfBusinessOwned;
            asset.CashOrMarketValueAmount = business.Val_rep;
            return asset;
        }
        private MismoAsset CreateAsset(IAssetRegular assetField, string borrowerId) 
        {
            if (null == assetField)
                return null;

            if (assetField.Val_rep == "" || assetField.Val_rep == "0.00")
                return null;

            MismoAsset asset = new MismoAsset();
            asset.BorrowerId = borrowerId;
            asset.AccountIdentifier = assetField.AccNum.Value;
            asset.CashOrMarketValueAmount = assetField.Val_rep;
            asset.Type = ToMismo(assetField.AssetT);
            asset.HolderName = assetField.ComNm;
            asset.HolderStreetAddress = assetField.StAddr;
            asset.HolderCity = assetField.City;
            asset.HolderState = assetField.State;
            asset.HolderPostalCode = assetField.Zip;
            asset.OtherAssetTypeDescription = assetField.OtherTypeDesc;
            if (assetField.AssetT == E_AssetRegularT.Auto) 
            {
                asset.AutomobileMakeDescription = assetField.Desc;
            }
            return asset;

        }
        private MismoTitleHolder CreateTitleHolder(string aTitleNm) 
        {
            if (null == aTitleNm || "" == aTitleNm)
                return null;
            MismoTitleHolder titleHolder = new MismoTitleHolder();
            titleHolder.Name = aTitleNm;
            return titleHolder;
        }
        
        private MismoDownPayment CreateDownPayment(string amount, string description, string type) 
        {
            var downPayment = new MismoDownPayment();
            downPayment.Amount = amount;
            downPayment.Type = ToMismoDownPaymentType(type);

            if (this.DataLoan.sLT == E_sLT.FHA && downPayment.Type == E_MismoDownPaymentType.GiftFunds
                && this.HasAtLeastOneAssetWithGiftFunds)
            {
                downPayment.SourceDescription = ToLPDownPaymentSource(type);
            }

            return downPayment;
        }

        /// <summary>
        /// Indicates whether any gift fund assets exist in the loan.
        /// </summary>
        /// <returns>A boolean indicating whether any gift fund assets exist in the loan.</returns>
        private bool HasAtLeastOneAssetWithGiftFunds
        {
            get
            {
                for (int appIndex = 0; appIndex < this.DataLoan.nApps; appIndex++)
                {
                    CAppData app = this.DataLoan.GetAppData(appIndex);
                    IAssetCollection assets = app.aAssetCollection;

                    for (int assetIndex = 0; assetIndex < assets.CountRegular; assetIndex++)
                    {
                        var asset = assets.GetRegularRecordAt(assetIndex);
                        if (asset.AssetT == E_AssetRegularT.GiftFunds && asset.Val > 0)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Derives the LP down payment source from the down payment type.
        /// </summary>
        /// <param name="type">The down payment type.</param>
        /// <returns>The LP down payment source value.</returns>
        private string ToLPDownPaymentSource(string type)
        {
            if (type.Equals("FHA - Gift - Source Relative", StringComparison.OrdinalIgnoreCase))
            {
                return "01"; // Relative
            }
            else if (type.Equals("FHA - Gift - Source Government Assistance", StringComparison.OrdinalIgnoreCase))
            {
                return "03"; // Government Assistance
            }
            else if (type.Equals("FHA - Gift - Source Employer", StringComparison.OrdinalIgnoreCase))
            {
                return "06"; // Employer/Other
            }
            else if (type.Equals("FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded", StringComparison.OrdinalIgnoreCase))
            {
                return "15"; // Nonprofit/Religious/Community - Non-Seller Funded
            }
            else
            {
                return "00"; // N/A
            }
        }

        private MismoLoanOriginationSystem CreateLoanOriginationSystem() 
        {
            MismoLoanOriginationSystem loanOriginationSystem = new MismoLoanOriginationSystem();
            loanOriginationSystem.VendorIdentifier = ConstAppDavid.LoanProspector_VendorIdentifier;
            loanOriginationSystem.VersionIdentifier = "Lender's Office";
            return loanOriginationSystem;
        }

        private MismoClosingAgent CreateClosingAgent() 
        {
            CAgentFields agent = this.DataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid) 
            {
                return null;
            }
            MismoClosingAgent closingAgent = new MismoClosingAgent();
            closingAgent.Type = ToMismo(agent.AgentRoleT);
            closingAgent.UnparsedName = agent.CompanyName;

            return closingAgent;

        }

        private MismoLoanPurpose CreateLoanPurpose() 
        {
            CAppData dataApp = this.DataLoan.GetAppData(0);

            MismoLoanPurpose loanPurpose = new MismoLoanPurpose();
            loanPurpose.GseTitleMannerHeldDescription = dataApp.aManner;
            loanPurpose.Type = ToMismo(this.DataLoan.sLPurposeT, this.DataLoan.sConstructionPurposeT);
            if (this.DataLoan.sLPurposeT == E_sLPurposeT.Other) 
            {
                loanPurpose.OtherLoanPurposeDescription = this.DataLoan.sOLPurposeDesc;
            }
            loanPurpose.PropertyLeaseholdExpirationDate = this.DataLoan.sLeaseHoldExpireD_rep;
            loanPurpose.PropertyRightsType = ToMismo(this.DataLoan.sEstateHeldT);
            loanPurpose.PropertyUsageType = ToMismo(dataApp.aOccT);
            //loanPurpose.PropertyRightsTypeOtherDescription;
            //            loanPurpose.PropertyUsageTypeOtherDescription;

            loanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();
            return loanPurpose;
        }

        private MismoConstructionRefinanceData CreateConstructionRefinanceData() 
        {
            MismoConstructionRefinanceData constructionRefinanceData = null;
            switch (this.DataLoan.sLPurposeT) 
            {
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    constructionRefinanceData = new MismoConstructionRefinanceData();
                    constructionRefinanceData.GseRefinancePurposeType = this.CalculateGseRefinancePurposeType();
                    constructionRefinanceData.PropertyOriginalCostAmount = this.DataLoan.sSpOrigC_rep;
                    constructionRefinanceData.PropertyAcquiredYear = this.DataLoan.sSpAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = this.DataLoan.sSpLien_rep;
                    constructionRefinanceData.RefinanceImprovementCostsAmount = this.DataLoan.sSpImprovC_rep;
                    constructionRefinanceData.RefinanceImprovementsType = ToMismo(this.DataLoan.sSpImprovTimeFrameT);
                    constructionRefinanceData.RefinanceProposedImprovementsDescription = this.DataLoan.sSpImprovDesc;
                    constructionRefinanceData.FreCashOutAmount = this.DataLoan.sProdCashoutAmt_rep;
                    constructionRefinanceData.SecondaryFinancingRefinanceIndicator = ToMismo(this.DataLoan.sPayingOffSubordinate);
//                    constructionRefinanceData.ConstructionImprovementCostsAmount;
//                    constructionRefinanceData.FreCashOutAmount;
//                    constructionRefinanceData.LandEstimatedValueAmount;
                    break;

                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    constructionRefinanceData = new MismoConstructionRefinanceData();
                    constructionRefinanceData.ConstructionImprovementCostsAmount = this.DataLoan.sLotImprovC_rep;
                    constructionRefinanceData.LandEstimatedValueAmount = this.DataLoan.sPresentValOfLot_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = this.DataLoan.sLotOrigC_rep;
                    constructionRefinanceData.PropertyAcquiredYear = this.DataLoan.sLotAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = this.DataLoan.sLotLien_rep;
                    break;

            }
            return constructionRefinanceData;
        }

        private MismoLoanProductData CreateLoanProductData() 
        {
            MismoLoanProductData loanProductData = new MismoLoanProductData();
            loanProductData.Arm = CreateArm();
            loanProductData.LoanFeatures = CreateLoanFeatures();

            if (this.DataLoan.sBuydown) 
            {
                // 4/22/2008 dd - Loan Prospector only use the first buydown sequence
                loanProductData.Buydown.Add(CreateBuydown());
            }

            // 4/29/2008 dd - Based on FreddieMac Specifications, we never send both RateAdjustment and PaymentAdjustment in the same transaction.
            // If both send then LP will only process the first one. We will determine Payment Adjustment if sPmtAdjCapMon_rep and sPmtAdjCapR_rep, sPmtAdjMaxBalPc > 0
            if (this.DataLoan.sPmtAdjCapMon_rep != "" && this.DataLoan.sPmtAdjCapR_rep != "" && this.DataLoan.sPmtAdjMaxBalPc_rep != "") 
            {
                loanProductData.PaymentAdjustment.Add(CreatePaymentAdjustment());
            } 
            else 
            {
                loanProductData.RateAdjustment.Add(CreateRateAdjustment());
            }
            return loanProductData;
        }

        private MismoPaymentAdjustment CreatePaymentAdjustment() 
        {
            MismoPaymentAdjustment paymentAdjustment = null;

            if (this.DataLoan.sFinMethT == E_sFinMethT.ARM || this.DataLoan.sFinMethT == E_sFinMethT.Graduated) 
            {

                paymentAdjustment = new MismoPaymentAdjustment();
                paymentAdjustment.SubsequentPaymentAdjustmentMonths = this.DataLoan.sPmtAdjCapMon_rep;
                paymentAdjustment.PeriodicCapPercent = this.DataLoan.sPmtAdjCapR_rep;
                paymentAdjustment.FirstPaymentAdjustmentMonths = this.DataLoan.sRAdj1stCapMon_rep;
                paymentAdjustment.PeriodNumber = "1"; // 5/15/2008 dd - LP requires this value to be pass.
            }

            return paymentAdjustment;
        }

        private MismoRateAdjustment CreateRateAdjustment() 
        {
            MismoRateAdjustment rateAdjustment = null;
            if (this.DataLoan.sFinMethT == E_sFinMethT.ARM || this.DataLoan.sFinMethT == E_sFinMethT.Graduated) 
            {
                rateAdjustment = new MismoRateAdjustment();
                rateAdjustment.FirstRateAdjustmentMonths = this.DataLoan.sRAdj1stCapMon_rep;
                rateAdjustment.SubsequentCapPercent = this.DataLoan.sRAdjCapR_rep;
                rateAdjustment.SubsequentRateAdjustmentMonths = this.DataLoan.sRAdjCapMon_rep;
            }
            return rateAdjustment;

        }

        private MismoBuydown CreateBuydown() 
        {
            if (this.DataLoan.sBuydwnR1stIncrease_rep == "" || this.DataLoan.sBuydwnR1stIncrease_rep == "0.000" || this.DataLoan.sBuydwnMon1_rep == "" || this.DataLoan.sBuydwnMon1_rep == "0")
                return null;

            MismoBuydown buydown = new MismoBuydown();
            buydown.ChangeFrequencyMonths = this.DataLoan.sBuydwnMon1_rep;
            buydown.DurationMonths = this.DataLoan.sBuydwnTotalMon_rep;
            buydown.IncreaseRatePercent = this.DataLoan.sBuydwnR1stIncrease_rep;
            buydown.ContributorType = ToMismo(this.DataLoan.sBuydownContributorT);
            return buydown;
        }

        private MismoArm CreateArm() 
        {
            if (this.DataLoan.sFinMethT == E_sFinMethT.Fixed)
                return null; // 4/30/2008 dd - Only valid for adjustable mortgage.
            MismoArm arm = new MismoArm();
            arm.IndexMarginPercent = this.DataLoan.sRAdjMarginR_rep;
            arm.QualifyingRatePercent = this.DataLoan.sQualIR_rep;
            arm.RateAdjustmentLifetimeCapPercent = this.DataLoan.sRAdjLifeCapR_rep;
            arm.IndexType = ToMismo(this.DataLoan.sFreddieArmIndexT);

            arm.IndexCurrentValuePercent = this.DataLoan.sRAdjIndexR_rep;
//            arm.IndexTypeOtherDescription;

            return arm;
        }
        private MismoLoanFeatures CreateLoanFeatures() 
        {
            MismoLoanFeatures loanFeatures = new MismoLoanFeatures();
            if (this.DataLoan.sBalloonPmt)
            {
                // 9/1/2010 dd - OPM 56081 - ONly export this field if it is a balloon pmt.
                loanFeatures.BalloonLoanMaturityTermMonths = this.DataLoan.sDue_rep;
            }
//            loanFeatures.GseProjectClassificationType = ToMismo(m_dataLoan.sCondoProjClassT); // 4/11/2008 dd - No longer support base on March 2008 Bulletin.
            if (this.DataLoan.sHelocCreditLimit_rep != "0.00") 
            {
                loanFeatures.HelocMaximumBalanceAmount = this.DataLoan.sHelocCreditLimit_rep;
            }
            loanFeatures.LienPriorityType = ToMismo(this.DataLoan.sLienPosT);
            loanFeatures.NegativeAmortizationLimitPercent = this.DataLoan.sPmtAdjMaxBalPc_rep;
            loanFeatures.NegativeAmortizationType = ToMismo(this.DataLoan.sNegAmortT);
            loanFeatures.ProductDescription = this.DataLoan.sLpTemplateNm;
            loanFeatures.FreOfferingIdentifier = DataAccess.Tools.sFredAffordProdIdToLoanProspectorId(this.DataLoan.sFredAffordProgId);

            loanFeatures.BuydownTemporarySubsidyIndicator = ToMismo(this.DataLoan.sBuydown);
//            loanFeatures.LoanDocumentationType = ToMismo(m_dataLoan.sFreddieDocT); // 11/25/2008 dd - OPM 25843 - No longer support.

//            loanFeatures.LienPriorityTypeOtherDescription;
//            loanFeatures.LoanDocumentationTypeOtherDescription;
//            loanFeatures.ProductName;
            loanFeatures.ProductName = ToMismo(this.DataLoan.sFreddieConstructionT);
            return loanFeatures;
        }
        private string ToMismo(E_sFreddieConstructionT sFreddieConstructionT) 
        {
            switch (sFreddieConstructionT) 
            {
                case E_sFreddieConstructionT.LeaveBlank: return "";
                case E_sFreddieConstructionT.NewlyBuilt: return "NewlyBuilt";
                case E_sFreddieConstructionT.ConstructionConversion: return "ConstructionConversion";
                default:
                    LogInvalidEnum("sFreddieConstructionT", sFreddieConstructionT);
                    return "";
            }
        }

        private static E_MismoValuationAppraisalFormType ToMismo(E_sSpAppraisalFormT sSpAppraisalFormT)
        {
            switch (sSpAppraisalFormT)
            {
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM1004FRE70;
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM1004CFRE70B;
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                    return E_MismoValuationAppraisalFormType.FNM1004DFRE442;
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM1025FRE72;
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM1073FRE465;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM1075FRE466;
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                    return E_MismoValuationAppraisalFormType.FNM2000FRE1032;
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                    return E_MismoValuationAppraisalFormType.FNM2000AFRE1072;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM2055FRE2055;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return E_MismoValuationAppraisalFormType.FNM2075;
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM2090;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return E_MismoValuationAppraisalFormType.FNM2095;
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                    return E_MismoValuationAppraisalFormType.FRE2070;
                case E_sSpAppraisalFormT.Other:
                    return E_MismoValuationAppraisalFormType.Other;
                default:
                    LogInvalidEnum(nameof(E_sSpAppraisalFormT), sSpAppraisalFormT);
                    return E_MismoValuationAppraisalFormType.Other;
            }
        }

        private static E_MismoValuationMethodType ToMismo(E_sSpValuationMethodT sSpValuationMethodT)
        {
            switch (sSpValuationMethodT)
            {
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return E_MismoValuationMethodType.AutomatedValuationModel;
                case E_sSpValuationMethodT.DesktopAppraisal:
                    return E_MismoValuationMethodType.BrokerPriceOpinion;
                case E_sSpValuationMethodT.DriveBy:
                    return E_MismoValuationMethodType.DriveBy;
                case E_sSpValuationMethodT.FullAppraisal:
                    return E_MismoValuationMethodType.FullAppraisal;
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return E_MismoValuationMethodType.PriorAppraisalUsed;
                case E_sSpValuationMethodT.None:
                    return E_MismoValuationMethodType.None;
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.Other:
                    return E_MismoValuationMethodType.Other;
                default:
                    LogInvalidEnum(nameof(E_sSpValuationMethodT), sSpValuationMethodT);
                    return E_MismoValuationMethodType.Other;
            }
        }

        private MismoInterviewerInformation CreateInterviewerInformation() 
        {
            IPreparerFields interviewer = this.DataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            MismoInterviewerInformation interviewerInformation = new MismoInterviewerInformation();
            interviewerInformation.ApplicationTakenMethodType = ToMismo(this.DataLoan.GetAppData(0).aIntrvwrMethodT);
            interviewerInformation.InterviewersEmployerCity = interviewer.City;
            interviewerInformation.InterviewersEmployerName = interviewer.CompanyName;
            interviewerInformation.InterviewersEmployerPostalCode = interviewer.Zip;
            interviewerInformation.InterviewersEmployerState = interviewer.State;
            interviewerInformation.InterviewersEmployerStreetAddress = interviewer.StreetAddr;
            interviewerInformation.InterviewersName = interviewer.PreparerName;
            interviewerInformation.InterviewersTelephoneNumber = interviewer.Phone;
//            interviewerInformation.InterviewerApplicationSignedDate;

            return interviewerInformation;
        }

        private MismoGovernmentLoan CreateGovernmentLoan() 
        {
            if (this.DataLoan.sLT != E_sLT.FHA && this.DataLoan.sLT != E_sLT.VA)
                return null;

            MismoGovernmentLoan governmentLoan = new MismoGovernmentLoan();

            if (this.DataLoan.sLT == E_sLT.FHA) 
            {
                governmentLoan.FhaLoan = CreateFhaLoan();
            } 
            else if (this.DataLoan.sLT == E_sLT.VA) 
            {
                governmentLoan.VaLoan = CreateVaLoan();
            }

            if (this.DataLoan.sLT == E_sLT.FHA) 
            {
                // 5/21/2008 dd - Only create FhaVaLoan for FHA.
                governmentLoan.FhaVaLoan = CreateFhaVaLoan();
            }

            return governmentLoan;

        }

        private MismoFhaVaLoan CreateFhaVaLoan() 
        {
            MismoFhaVaLoan fhaVaLoan = new MismoFhaVaLoan();
            fhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount = this.DataLoan.sFreddieFHABorrPdCc_rep;
            return fhaVaLoan;
        }
        private MismoFhaLoan CreateFhaLoan() 
        {
            MismoFhaLoan fhaLoan = new MismoFhaLoan();
            fhaLoan.BorrowerFinancedFhaDiscountPointsAmount = this.DataLoan.sFHAFinancedDiscPtAmt_rep;
            fhaLoan.LenderIdentifier = this.DataLoan.sFHALenderIdCode;
            fhaLoan.SponsorIdentifier = this.DataLoan.sFHASponsorAgentIdCode;
            return fhaLoan;
        }
        
        private MismoVaLoan CreateVaLoan() 
        {
            CAppData dataApp = this.DataLoan.GetAppData(0);

            MismoVaLoan vaLoan = new MismoVaLoan();
            vaLoan.VaResidualIncomeAmount = dataApp.aVaFamilySupportBal_rep;
            return vaLoan;
        }

        private MismoAdditionalCaseData CreateAdditionalCaseData() 
        {
            MismoAdditionalCaseData additionalCaseData = new MismoAdditionalCaseData();

            additionalCaseData.TransmittalData = CreateTransmittalData();

            return additionalCaseData;
        }
        private MismoTransmittalData CreateTransmittalData() 
        {
            MismoTransmittalData transmittalData = new MismoTransmittalData();

            // LPA 4.8.00 Excel file: "Negative amounts are not permitted. If a value is sent it must be between $9,000.00 and $9,999,999.00."
            if (9000 <= this.DataLoan.sApprVal && this.DataLoan.sApprVal <= 9999999) 
            {
                transmittalData.PropertyAppraisedValueAmount = this.DataLoan.sApprVal_rep;
            }
            if (9000 <= this.DataLoan.sSpMarketVal && this.DataLoan.sSpMarketVal <= 9999999) 
            {
                transmittalData.PropertyEstimatedValueAmount = this.DataLoan.sSpMarketVal_rep;
            }
            transmittalData.CaseStateType = ToMismo(this.DataLoan.sFredProcPointT);
            transmittalData.LenderBranchIdentifier = this.DataLoan.sFreddieLenderBranchId;

//            Tools.LogError("REMOVE HARDCODE LENDER REGISTRATION");
//            transmittalData.LenderRegistrationIdentifier = "123456";

            return transmittalData;
        }

        private MismoTransactionDetail CreateTransactionDetail() 
        {
            MismoTransactionDetail transactionDetail = new MismoTransactionDetail();

            transactionDetail.PurchaseCredit.Add(CreatePurchaseCredit(this.DataLoan.sOCredit1Desc, this.DataLoan.sOCredit1Amt_rep));
            transactionDetail.PurchaseCredit.Add(CreatePurchaseCredit(this.DataLoan.sOCredit2Desc, this.DataLoan.sOCredit2Amt_rep));
            transactionDetail.PurchaseCredit.Add(CreatePurchaseCredit(this.DataLoan.sOCredit3Desc, this.DataLoan.sOCredit3Amt_rep));
            transactionDetail.PurchaseCredit.Add(CreatePurchaseCredit(this.DataLoan.sOCredit4Desc, this.DataLoan.sOCredit4Amt_rep));
            transactionDetail.PurchaseCredit.Add(CreatePurchaseCredit(this.DataLoan.sOCredit5Desc, this.DataLoan.sOCredit5Amt_rep));

            if (this.DataLoan.sAltCost > 0)
            {
                transactionDetail.AlterationsImprovementsAndRepairsAmount = this.DataLoan.sAltCost_rep;
            }
            if (this.DataLoan.sLDiscnt1003 > 0)
            {
                transactionDetail.BorrowerPaidDiscountPointsTotalAmount = this.DataLoan.sLDiscnt1003_rep;
            }
            transactionDetail.EstimatedClosingCostsAmount = this.DataLoan.sTotEstCcNoDiscnt1003_rep;
            transactionDetail.MiAndFundingFeeFinancedAmount = this.DataLoan.sFfUfmipFinanced_rep;
            if (this.DataLoan.sFfUfmip1003 > 0)
            {
                transactionDetail.MiAndFundingFeeTotalAmount = this.DataLoan.sFfUfmip1003_rep;
            }
            if (this.DataLoan.sTotEstPp1003 > 0)
            {
                transactionDetail.PrepaidItemsEstimatedAmount = this.DataLoan.sTotEstPp1003_rep;
            }
            if (this.DataLoan.sPurchPrice > 0)
            {
                transactionDetail.PurchasePriceAmount = this.DataLoan.sPurchPrice_rep;
            }

            if (this.DataLoan.sLPurposeT != E_sLPurposeT.Purchase) 
            {
                transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = this.DataLoan.sRefPdOffAmt1003_rep;
            }
            transactionDetail.SellerPaidClosingCostsAmount = this.DataLoan.sTotCcPbs_rep;
            transactionDetail.SubordinateLienAmount = this.DataLoan.sONewFinBal_rep;
            if (this.DataLoan.sHelocBal > 0) 
            {
                transactionDetail.SubordinateLienHelocAmount = this.DataLoan.sHelocBal_rep;
            }
            transactionDetail.FreReservesAmount = this.DataLoan.sFredieReservesAmt_rep;

//            transactionDetail.FreReservesAmount;
            transactionDetail.SalesConcessionAmount = this.DataLoan.sFHASalesConcessions_rep;
            //            transactionDetail.SubordinateLienHelocAmount;
            //            transactionDetail.SubordinateLienPurposeType;
            //            transactionDetail.SubordinateLienPurposeTypeOtherDescription;

            transactionDetail.ExtensionTransactionDetail = this.CreateExtensionTransactionDetail();
            return transactionDetail;
        }

        private EXTENSION_TRANSACTION_DETAIL CreateExtensionTransactionDetail()
        {
            if (this.DataLoan.sGfeDiscountPointF > 0)
            {
                EXTENSION_TRANSACTION_DETAIL extensionTransactionDetail = new EXTENSION_TRANSACTION_DETAIL();
                extensionTransactionDetail.TotalDiscountPointsAmount = this.DataLoan.sGfeDiscountPointF_rep;

                return extensionTransactionDetail;
            }

            return null;
        }

        private MismoPurchaseCredit CreatePurchaseCredit(string desc, string amount) 
        {
            if (null == amount || "" == amount || "0.00" == amount || null == desc) 
                return null;

            MismoPurchaseCredit purchaseCredit = new MismoPurchaseCredit();
            switch (desc.ToLower()) 
            {
                case "cash deposit on sales contract":
                    purchaseCredit.Type = E_MismoPurchaseCreditType.EarnestMoney;
                    break;
                case "employer assisted housing":
                    purchaseCredit.Type = E_MismoPurchaseCreditType.EmployerAssistedHousing;
                    break;
                case "lease purchase fund":
                    purchaseCredit.Type = E_MismoPurchaseCreditType.LeasePurchaseFund;
                    break;
                case "relocation funds":
                    purchaseCredit.Type = E_MismoPurchaseCreditType.RelocationFunds;
                    break;
                case "seller credit":
                    purchaseCredit.SourceType = E_MismoPurchaseCreditSourceType.PropertySeller;
                    purchaseCredit.Type = E_MismoPurchaseCreditType.Other;
                    break;
                case "lender credit":
                    purchaseCredit.SourceType = E_MismoPurchaseCreditSourceType.Lender;
                    purchaseCredit.Type = E_MismoPurchaseCreditType.Other;
                    break;
                default:
                    purchaseCredit.Type = E_MismoPurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;
            }
            purchaseCredit.Amount = amount;
            return purchaseCredit;

        }
        private MismoProposedHousingExpense CreateProposedHousingExpense(E_MismoProposedHousingExpenseHousingExpenseType type, string amount) 
        {
            if (null == amount || "" == amount || "0.00" == amount)
                return null;
            MismoProposedHousingExpense proposedHousingExpense = new MismoProposedHousingExpense();
            proposedHousingExpense.HousingExpenseType = type;
            proposedHousingExpense.PaymentAmount = amount;
            if (type == E_MismoProposedHousingExpenseHousingExpenseType.OtherHousingExpense) 
            {
                proposedHousingExpense.HousingExpenseTypeOtherDescription = this.DataLoan.sProOHExpDesc;
            }
            return proposedHousingExpense;
        }
        private MismoMortgageTerms CreateMortgageTerms() 
        {
            MismoMortgageTerms mortgageTerms = new MismoMortgageTerms();
            mortgageTerms.AgencyCaseIdentifier = this.DataLoan.sAgencyCaseNum;
            mortgageTerms.BaseLoanAmount = this.DataLoan.sLAmt1003_rep;
            mortgageTerms.LenderCaseIdentifier = this.DataLoan.sLenderCaseNum;
            // 3/12/2008 dd - If Lender Case Number is blank then we use our sLNm when export to LP.
            if (mortgageTerms.LenderCaseIdentifier == "") 
            {
                mortgageTerms.LenderCaseIdentifier = this.DataLoan.sLNm;
            }
            mortgageTerms.LoanAmortizationTermMonths = this.DataLoan.sTerm_rep;
            mortgageTerms.LoanAmortizationType = ToMismo(this.DataLoan.sFinMethT);
            mortgageTerms.MortgageType = ToMismo(this.DataLoan.sLT);
            if (this.DataLoan.sLT == E_sLT.Other) 
            {
                mortgageTerms.OtherMortgageTypeDescription = this.DataLoan.sLTODesc;
            }
            mortgageTerms.RequestedInterestRatePercent = this.DataLoan.sNoteIR_rep;



//            mortgageTerms.ArmTypeDescription;
//            mortgageTerms.BorrowerRequestedLoanAmount;
//            mortgageTerms.Id;
//            mortgageTerms.OtherAmortizationTypeDescription;
            return mortgageTerms;
        }
        private MismoLegalDescription CreateLegalDescription() 
        {
            MismoLegalDescription legalDescription = new MismoLegalDescription();
            legalDescription.TextDescription = this.DataLoan.sSpLegalDesc;
            return legalDescription;
        }
        private MismoProperty CreateProperty() 
        {
            MismoProperty property = new MismoProperty();
            property.City = this.DataLoan.sSpCity;
            property.FinancedNumberOfUnits = this.DataLoan.sUnitsNum_rep;
            property.PostalCode = this.DataLoan.sSpZip;
            property.State = this.DataLoan.sSpState;
            property.StreetAddress = this.DataLoan.sSpAddr;
            property.StructureBuiltYear = this.DataLoan.sYrBuilt;
            property.County = this.DataLoan.sSpCounty;
            property.LegalDescription.Add(CreateLegalDescription());

            property.PlannedUnitDevelopmentIndicator = ToMismo(this.DataLoan.sSpIsInPud);
            property.BuildingStatusType = ToMismo(this.DataLoan.sBuildingStatusT);

            property.Category.Add(CreateCategory());

            // 4/28/2008 dd - Transmit OwnershipType if FinanceUnit = 1 && sGseSpT== Condominium.
            if (this.DataLoan.sUnitsNum_rep == "1") 
            {
                switch (this.DataLoan.sGseSpT) 
                {
                    case E_sGseSpT.Condominium:
                    case E_sGseSpT.DetachedCondominium:
                    case E_sGseSpT.HighRiseCondominium:
                        property.OwnershipType = E_MismoPropertyOwnershipType.Condominium;
                        break;
                    case E_sGseSpT.Cooperative:
                        property.OwnershipType = E_MismoPropertyOwnershipType.Cooperative;
                        break;

                }
            }
            property.Valuation.Add(CreateValuation());
            property.Seller.Add(CreateSeller());

            //            property.BuildingStatusTypeOtherDescription;

//            property.ParsedStreetAddress;
//            property.Project;
//            property.Seller;
//            property.Id;
//            property.CurrentVacancyStatusType;
//            property.GrossLivingAreaSquareFeetCount;
//            property.ManufacturedHomeManufactureYear;
//            property.NativeAmericanLandsType;
//            property.NativeAmericanLandsTypeOtherDescription;
//            property.AcquiredDate;
//            property.AcreageNumber;
//            property.CommunityLandTrustIndicator;
//            property.InclusionaryZoningIndicator;
//            property.NeighborhoodLocationType;
//            property.NeighborhoodLocationTypeOtherDescription;
//            property.OwnershipType;
//            property.OwnershipTypeOtherDescription;
//            property.StreetAddress2;
//            property.ZoningCategoryType;
//            property.ZoningCategoryTypeOtherDescription;
//            property.StoriesCount;
//            property.UniqueDwellingType;
//            property.UniqueDwellingTypeOtherDescription;
            return property;

        }

        private MismoSeller CreateSeller() 
        {
            CAgentFields agent = this.DataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (string.IsNullOrEmpty(agent.AgentName)) //IR - Transmit if [Name] provided by user.
            {
                return null;
            }

            var parser = new CommonLib.Name();
            parser.ParseName(agent.AgentName);

            MismoSeller seller = new MismoSeller();
            seller.FirstName = parser.FirstName;
            seller.LastName = parser.LastName;
            seller.MiddleName = parser.MiddleName;
            seller.NameSuffix = parser.Suffix;
            return seller;
        }
        private MismoValuation CreateValuation() 
        {
            MismoValuation valuation = new MismoValuation();
            if (!string.IsNullOrEmpty(this.DataLoan.sSpAppraisalId))
            {
                valuation.ExtensionValuation.AppraisalIdentifier = this.DataLoan.sSpAppraisalId;
                valuation.AppraisalFormType = ToMismo(this.DataLoan.sSpAppraisalFormT);
                valuation.MethodType = ToMismo(this.DataLoan.sSpValuationMethodT);
            }

            CAgentFields appraiser = this.DataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (appraiser.CompanyName != "" || appraiser.AgentName != "") 
            {
                valuation.Appraiser.Add(CreateAppraiser(appraiser));
            }
            return valuation;
        }
        private MismoAppraiser CreateAppraiser(CAgentFields appraiserAgent) 
        {
            if (null == appraiserAgent)
                return null;

            MismoAppraiser appraiser = new MismoAppraiser();
            appraiser.CompanyName = appraiserAgent.CompanyName;
            appraiser.Name = appraiserAgent.AgentName;

            return appraiser;
        }
        private MismoCategory CreateCategory() 
        {
            MismoCategory category = new MismoCategory();
            category.Type = ToMismo(this.DataLoan.sGseSpT, this.DataLoan.sProdSpStructureT);
            return category;
        }
        private MismoBorrower CreateBorrower(CAppData dataApp, bool isBorrower, bool hasSpouseInfo) 
        {
            MismoBorrower borrower = new MismoBorrower();
            if (isBorrower) 
            {
                borrower.BorrowerId = "B" + dataApp.aBSsn.Replace("-", ""); // 12/21/2007 dd - LP expect borrower id to start with "B"
                // 6/3/2008 dd - Enable the bellow codes to run LP Test Case 13 step 2 & 3.
//                if (borrower.BorrowerId == "B005180001") 
//                {
//                    borrower.JointAssetBorrowerId = "B005170001";
//                }
//                if (borrower.BorrowerId == "B005170001") 
//                {
//                    borrower.JointAssetBorrowerId = "B005180001";
//                }

                if (hasSpouseInfo && dataApp.aCSsn != "") 
                {
                    borrower.JointAssetBorrowerId = "B" + dataApp.aCSsn.Replace("-", "");
                }
            } 
            else 
            {
                borrower.BorrowerId = "B" + dataApp.aCSsn.Replace("-", "");
                if (hasSpouseInfo && dataApp.aBSsn != "") 
                {
                    borrower.JointAssetBorrowerId = "B" + dataApp.aBSsn.Replace("-", "");
                }
            }
            borrower.BirthDate = isBorrower ? dataApp.aBDob_rep : dataApp.aCDob_rep;
            borrower.FirstName = isBorrower ? dataApp.aBFirstNm : dataApp.aCFirstNm;
            borrower.HomeTelephoneNumber = isBorrower ? dataApp.aBHPhone : dataApp.aCHPhone;
            borrower.LastName = isBorrower ? dataApp.aBLastNm : dataApp.aCLastNm;
            borrower.MiddleName = isBorrower ? dataApp.aBMidNm : dataApp.aCMidNm;
            borrower.NameSuffix = isBorrower ? dataApp.aBSuffix : dataApp.aCSuffix;
            borrower.PrintPositionType = isBorrower ? E_MismoBorrowerPrintPositionType.Borrower : E_MismoBorrowerPrintPositionType.CoBorrower;
            borrower.Ssn = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;

            // 4/22/2008 dd - CreditReportIdentifier is pull from temporary cache that user save from Freddie Export screen.
            // In future we might consider store this in the loan file itself.
            if (isBorrower && this.craInformation != null && (this.craInformation.CreditReportOption == CreditReportType.UsePrevious || (this.requestType == LpaRequestType.NonSeamlessLpaSubmit && this.craInformation.CreditReportOption == CreditReportType.OrderNew))) 
            {
                var appInfo = this.craInformation.Applications.SingleOrDefault(appCraInfo => appCraInfo.ApplicationId == dataApp.aAppId);
                if (appInfo != null && !appInfo.ReorderApplicationCredit)
                {
                    // 5/19/2008 dd - For joint borrower, we only need to set credit report identifier for the first borrower information (aBNm) in the pair
                    borrower.CreditReportIdentifier = appInfo?.CraResubmitId;
                    if (!string.IsNullOrEmpty(borrower.CreditReportIdentifier))
                    {
                        this.m_hasAtLeastOneCreditReportIdentifier = true;
                    }
                }
            }

            borrower.DependentCount = isBorrower ? dataApp.aBDependNum_rep : dataApp.aCDependNum_rep;
            borrower.JointAssetLiabilityReportingType = dataApp.aAsstLiaCompletedNotJointly ? E_MismoBorrowerJointAssetLiabilityReportingType.NotJointly : E_MismoBorrowerJointAssetLiabilityReportingType.Jointly;
            borrower.MaritalStatusType = isBorrower ? ToMismo(dataApp.aBMaritalStatT) : ToMismo(dataApp.aCMaritalStatT);
            borrower.SchoolingYears = isBorrower ? dataApp.aBSchoolYrs_rep : dataApp.aCSchoolYrs_rep;
            borrower.MailTo.City = isBorrower ? dataApp.aBCityMail : dataApp.aCCityMail;
            borrower.MailTo.PostalCode = isBorrower ? dataApp.aBZipMail : dataApp.aCZipMail;
            borrower.MailTo.State = isBorrower ? dataApp.aBStateMail : dataApp.aCStateMail;
            borrower.MailTo.StreetAddress = isBorrower ? dataApp.aBAddrMail : dataApp.aCAddrMail;

            if (isBorrower)
            {
                #region Residence
                borrower.Residence.Add(CreateResidence(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip, dataApp.aBAddrT, dataApp.aBAddrYrs, true));
                borrower.Residence.Add(CreateResidence(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip, (E_aBAddrT)dataApp.aBPrev1AddrT, dataApp.aBPrev1AddrYrs, false));
                borrower.Residence.Add(CreateResidence(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip, (E_aBAddrT)dataApp.aBPrev2AddrT, dataApp.aBPrev2AddrYrs, false));
                #endregion
            } 
            else 
            {
                #region Residence
                borrower.Residence.Add(CreateResidence(dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip, (E_aBAddrT)dataApp.aCAddrT, dataApp.aCAddrYrs, true));
                borrower.Residence.Add(CreateResidence(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip, (E_aBAddrT)dataApp.aCPrev1AddrT, dataApp.aCPrev1AddrYrs, false));
                borrower.Residence.Add(CreateResidence(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip, (E_aBAddrT)dataApp.aCPrev2AddrT, dataApp.aCPrev2AddrYrs, false));
                #endregion
            }
            #region Current Income
            var currentIncomeList = new[] {
                Tuple.Create(E_MismoCurrentIncomeIncomeType.Base, isBorrower ? dataApp.aBBaseI : dataApp.aCBaseI),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.Overtime, isBorrower ? dataApp.aBOvertimeI : dataApp.aCOvertimeI),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.Bonus, isBorrower ? dataApp.aBBonusesI : dataApp.aCBonusesI),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.Commissions, isBorrower ? dataApp.aBCommisionI : dataApp.aCCommisionI),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.DividendsInterest, isBorrower ? dataApp.aBDividendI : dataApp.aCDividendI),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.NetRentalIncome, isBorrower ? dataApp.aBNetRentI1003 : dataApp.aCNetRentI1003),
                Tuple.Create(E_MismoCurrentIncomeIncomeType.SubjectPropertyNetCashFlow, isBorrower ? dataApp.aBSpPosCf : dataApp.aCSpPosCf),
            }.ToList();

            Action<E_MismoCurrentIncomeIncomeType, decimal> addIncomeData = (incomeType, amount) =>
            {
                int i = currentIncomeList.FindIndex(t => t.Item1 == incomeType);
                if (i < 0)
                {
                    currentIncomeList.Add(Tuple.Create(incomeType, amount));
                }
                else
                {
                    currentIncomeList[i] = Tuple.Create(incomeType, currentIncomeList[i].Item2 + amount);
                }
            };

            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if ((isBorrower && !otherIncome.IsForCoBorrower) || (!isBorrower && otherIncome.IsForCoBorrower))
                {
                    addIncomeData(ToMismoIncomeType(otherIncome.Desc), otherIncome.Amount);
                }
            }

            if (isBorrower && dataApp.aSpNegCf > 0 && dataLoan.Value.sOccT == E_sOccT.Investment)
            {
                addIncomeData(E_MismoCurrentIncomeIncomeType.SubjectPropertyNetCashFlow, dataApp.aSpNegCf * -1);
            }

            for (int i = 0; i < currentIncomeList.Count; i++) 
            {
                borrower.CurrentIncome.Add(CreateCurrentIncome(currentIncomeList[i].Item1, dataApp.m_convertLos.ToMoneyString(currentIncomeList[i].Item2, FormatDirection.ToRep)));
            }

            #endregion

            borrower.Declaration = CreateDeclaration(dataApp, isBorrower);

            string[] dependentAges = isBorrower ? dataApp.aBDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':') : dataApp.aCDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');
            foreach (string s in dependentAges) 
            {
                borrower.Dependent.Add(CreateDependent(s));
            }

            #region Employer
            IEmpCollection empCollection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
            borrower.Employer.Add(CreateEmployer(empCollection.GetPrimaryEmp(true)));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous)) 
            {
                borrower.Employer.Add(CreateEmployer(record));
            }

            #endregion

            borrower.GovernmentMonitoring = CreateGovernmentMonitoring(dataApp, isBorrower);

            if (isBorrower) 
            {
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.Rent, dataApp.aPresRent_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, dataApp.aPres1stM_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, dataApp.aPresOFin_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.HazardInsurance, dataApp.aPresHazIns_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.RealEstateTax, dataApp.aPresRealETx_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.MI, dataApp.aPresMIns_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, dataApp.aPresHoAssocDues_rep));
                borrower.PresentHousingExpense.Add(CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType.OtherHousingExpense, dataApp.aPresOHExp_rep));
            }

            borrower.FhaVaBorrower = new MismoFhaVaBorrower
            {
                VeteranStatusIndicator = ToMismo(isBorrower ? dataApp.aBIsVeteran : dataApp.aCIsVeteran)
            };
            //borrower.Summary;
            //borrower.VaBorrower;
            //borrower.ContactPoint;

            return borrower;

        }

        private MismoPresentHousingExpense CreatePresentHousingExpense(E_MismoPresentHousingExpenseHousingExpenseType type, string amount) 
        {
            MismoPresentHousingExpense presentHousingExpense = new MismoPresentHousingExpense();
            presentHousingExpense.HousingExpenseType = type;
            presentHousingExpense.PaymentAmount = amount;
            return presentHousingExpense;
        }

        private MismoGovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp, bool isBorrower) 
        {
            MismoGovernmentMonitoring governmentMonitoring = new MismoGovernmentMonitoring();
            governmentMonitoring.GovernmentMonitoringExtension = this.CreateGovernmentMonitoringExtension(dataApp, isBorrower);
            return governmentMonitoring;
        }

        /// <summary>
        /// Creates the GOVERNMENT_MONITORING_EXTENSION container. Holds new HMDA data.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <param name="isBorrower">Whether this is for a borrower or coborrower.</param>
        /// <returns>The container.</returns>
        private GOVERNMENT_MONITORING_EXTENSION CreateGovernmentMonitoringExtension(CAppData dataApp, bool isBorrower)
        {
            var oldBorrowerMode = dataApp.BorrowerModeT;
            dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;

            GOVERNMENT_MONITORING_EXTENSION governmentMonitoringExtension = new GOVERNMENT_MONITORING_EXTENSION();
            governmentMonitoringExtension.GovernmentMonitoringDetail = this.CreateGovernmentMonitoringDetail(dataApp);
            if (governmentMonitoringExtension.GovernmentMonitoringDetail.HMDAEthnicityRefusalIndicator == E_YesNoIndicator.N ||
                (governmentMonitoringExtension.GovernmentMonitoringDetail.HMDAEthnicityRefusalIndicator == E_YesNoIndicator.Y &&
                governmentMonitoringExtension.GovernmentMonitoringDetail.ApplicationTakenMethodType == ApplicationTakenMethodType.FaceToFace))
            {
                governmentMonitoringExtension.HmdaEthnicities = this.CreateHmdaEthnicities(dataApp);
            }

            governmentMonitoringExtension.HmdaEthnicityOrigins = this.CreateHmdaEthnicityOrigins(dataApp);
            governmentMonitoringExtension.HmdaRaces = this.CreateHmdaRaces(dataApp);

            dataApp.BorrowerModeT = oldBorrowerMode;
            return governmentMonitoringExtension;
        }

        /// <summary>
        /// Creates the HMDA_RACES container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <returns>The container.</returns>
        private HMDA_RACES CreateHmdaRaces(CAppData dataApp)
        {
            HMDA_RACES hmdaRaces = new HMDA_RACES();

            if (dataApp.aIsAmericanIndian)
            {
                hmdaRaces.HmdaRaceExtensionList.Add(this.CreateHmdaRaceExtension(dataApp, HmdaRaceDetailHmdaRaceType.AmericanIndianOrAlaskaNative));
            }

            if (dataApp.aIsAsian)
            {
                hmdaRaces.HmdaRaceExtensionList.Add(this.CreateHmdaRaceExtension(dataApp, HmdaRaceDetailHmdaRaceType.Asian));
            }

            if (dataApp.aIsBlack)
            {
                hmdaRaces.HmdaRaceExtensionList.Add(this.CreateHmdaRaceExtension(dataApp, HmdaRaceDetailHmdaRaceType.BlackOrAfricanAmerican));
            }

            if (dataApp.aIsPacificIslander)
            {
                hmdaRaces.HmdaRaceExtensionList.Add(this.CreateHmdaRaceExtension(dataApp, HmdaRaceDetailHmdaRaceType.NativeHawaiianOrOtherPacificIslander));
            }

            if (dataApp.aIsWhite)
            {
                hmdaRaces.HmdaRaceExtensionList.Add(this.CreateHmdaRaceExtension(dataApp, HmdaRaceDetailHmdaRaceType.White));
            }

            return hmdaRaces;
        }

        /// <summary>
        /// Creates a HMDA_RACE_EXTENSION container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <param name="raceType">The race type to to help determine additional HMDA_RACE_DESIGNATIONS.</param>
        /// <returns>The container.</returns>
        private HMDA_RACE_EXTENSION CreateHmdaRaceExtension(CAppData dataApp, HmdaRaceDetailHmdaRaceType raceType)
        {
            HMDA_RACE_EXTENSION hmdaRaceExtension = new HMDA_RACE_EXTENSION();

            hmdaRaceExtension.HmdaRaceDetail = this.CreateHmdaRaceDetail(raceType, raceType == HmdaRaceDetailHmdaRaceType.AmericanIndianOrAlaskaNative ? dataApp.aOtherAmericanIndianDescription : null);
            if (raceType == HmdaRaceDetailHmdaRaceType.Asian || raceType == HmdaRaceDetailHmdaRaceType.NativeHawaiianOrOtherPacificIslander)
            {
                hmdaRaceExtension.HmdaRaceDesignations = this.CreateHmdaRaceDesignations(dataApp, raceType);
            }

            return hmdaRaceExtension;
        }

        /// <summary>
        /// Creates the HMDA_RACE_DESIGNATIONS container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <param name="raceType">The race type. Should only be Asian or NativeHawaiian.</param>
        /// <returns>The container.</returns>
        private HMDA_RACE_DESIGNATIONS CreateHmdaRaceDesignations(CAppData dataApp, HmdaRaceDetailHmdaRaceType raceType)
        {
            HMDA_RACE_DESIGNATIONS hmdaRaceDesignations = new HMDA_RACE_DESIGNATIONS();

            if (raceType == HmdaRaceDetailHmdaRaceType.Asian)
            {
                if (dataApp.aIsAsianIndian)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.AsianIndian));
                }

                if (dataApp.aIsChinese)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Chinese));
                }

                if (dataApp.aIsFilipino)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Filipino));
                }

                if (dataApp.aIsJapanese)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Japanese));
                }

                if (dataApp.aIsKorean)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Korean));
                }

                if (dataApp.aIsVietnamese)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Vietnamese));
                }

                if (dataApp.aIsOtherAsian)
                {
                    var designation = this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.OtherAsian);
                    if (!string.IsNullOrEmpty(dataApp.aOtherAsianDescription))
                    {
                        designation.HMDARaceDesignationOtherAsianDescription = dataApp.aOtherAsianDescription;
                    }

                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(designation);
                }
            }
            else if (raceType == HmdaRaceDetailHmdaRaceType.NativeHawaiianOrOtherPacificIslander)
            {
                if (dataApp.aIsNativeHawaiian)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.NativeHawaiian));
                }

                if (dataApp.aIsGuamanianOrChamorro)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.GuamanianOrChamorro));
                }

                if (dataApp.aIsSamoan)
                {
                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.Samoan));
                }

                if (dataApp.aIsOtherPacificIslander)
                {
                    var designation = this.CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType.OtherPacificIslander);
                    if (!string.IsNullOrEmpty(dataApp.aOtherPacificIslanderDescription))
                    {
                        designation.HMDARaceDesignationOtherPacificIslanderDescription = dataApp.aOtherPacificIslanderDescription;
                    }

                    hmdaRaceDesignations.HmdaRaceDesignationList.Add(designation);
                }
            }

            return hmdaRaceDesignations;
        }

        /// <summary>
        /// Creates a HMDA_RACE_DESIGNATION container.
        /// </summary>
        /// <param name="raceDesignationType">The designation type.</param>
        /// <returns>The container.</returns>
        private HMDA_RACE_DESIGNATION CreateHmdaRaceDesignation(HmdaRaceDesignationHmdaRaceDesignationType raceDesignationType)
        {
            HMDA_RACE_DESIGNATION hmdaRaceDesignation = new HMDA_RACE_DESIGNATION();

            hmdaRaceDesignation.HMDARaceDesignationType = raceDesignationType;

            return hmdaRaceDesignation;
        }

        /// <summary>
        /// Creates a HMDA_RACE_DETAIL container.
        /// </summary>
        /// <param name="raceType">The race type.</param>
        /// <param name="hmdaRaceAdditionalDescription">An additional race description if it has one.</param>
        /// <returns>The container.</returns>
        private HMDA_RACE_DETAIL CreateHmdaRaceDetail(HmdaRaceDetailHmdaRaceType raceType, string hmdaRaceAdditionalDescription = null)
        {
            HMDA_RACE_DETAIL hmdaRaceDetail = new HMDA_RACE_DETAIL();
            hmdaRaceDetail.HMDARaceType = raceType;
            hmdaRaceDetail.HMDARaceTypeAdditionalDescription = hmdaRaceAdditionalDescription;

            return hmdaRaceDetail;
        }

        /// <summary>
        /// Creates a HMDA_ETHNICITY_ORIGINS container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <returns>The container.</returns>
        private HMDA_ETHNICITY_ORIGINS CreateHmdaEthnicityOrigins(CAppData dataApp)
        {
            HMDA_ETHNICITY_ORIGINS hmdaEthnicityOrigins = new HMDA_ETHNICITY_ORIGINS();

            if (dataApp.aIsMexican)
            {
                hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(this.CreateHmdaEthnicityOrigin(HmdaEthnicityOriginHmdaEthnicityOriginType.Mexican));
            }

            if (dataApp.aIsPuertoRican)
            {
                hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(this.CreateHmdaEthnicityOrigin(HmdaEthnicityOriginHmdaEthnicityOriginType.PuertoRican));
            }

            if (dataApp.aIsCuban)
            {
                hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(this.CreateHmdaEthnicityOrigin(HmdaEthnicityOriginHmdaEthnicityOriginType.Cuban));
            }

            if (dataApp.aIsOtherHispanicOrLatino)
            {
                hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(this.CreateHmdaEthnicityOrigin(HmdaEthnicityOriginHmdaEthnicityOriginType.OtherHispanicOrLatino, dataApp.aOtherHispanicOrLatinoDescription));
            }

            return hmdaEthnicityOrigins;
        }

        /// <summary>
        /// Creates a HMDA_ETHNICITY_ORIGIN container.
        /// </summary>
        /// <param name="ethnicityType">The ethnicity type to give it.</param>
        /// <param name="otherEthnicityDescription">The other ethnicity description if it has one.</param>
        /// <returns>The container.</returns>
        private HMDA_ETHNICITY_ORIGIN CreateHmdaEthnicityOrigin(HmdaEthnicityOriginHmdaEthnicityOriginType ethnicityType, string otherEthnicityDescription = null)
        {
            HMDA_ETHNICITY_ORIGIN hmdaEthnicityOrigin = new HMDA_ETHNICITY_ORIGIN();
            hmdaEthnicityOrigin.HMDAEthnicityOriginType = ethnicityType;
            if (!string.IsNullOrEmpty(otherEthnicityDescription))
            {
                hmdaEthnicityOrigin.HMDAEthnicityOriginTypeOtherDescription = otherEthnicityDescription;
            }

            return hmdaEthnicityOrigin;
        }

        /// <summary>
        /// Creates a HMDA_ETHNCITIES container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <returns>The container.</returns>
        private HMDA_ETHNICITIES CreateHmdaEthnicities(CAppData dataApp)
        {
            HMDA_ETHNICITIES hmdaEthnicities = new HMDA_ETHNICITIES();

            if (dataApp.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                HMDA_ETHNICITY hispanic = new HMDA_ETHNICITY();
                hispanic.HMDAEthnicityType = this.GetHmdaEthnicityType(E_aHispanicT.Hispanic);
                hmdaEthnicities.HmdaEthnicityList.Add(hispanic);

                HMDA_ETHNICITY notHispanic = new HMDA_ETHNICITY();
                notHispanic.HMDAEthnicityType = this.GetHmdaEthnicityType(E_aHispanicT.NotHispanic);
                hmdaEthnicities.HmdaEthnicityList.Add(notHispanic);
            }
            else
            {
                hmdaEthnicities.HmdaEthnicityList.Add(this.CreateHmdaEthnicity(dataApp));
            }

            return hmdaEthnicities;
        }

        /// <summary>
        /// Creates a HMDA_ETHNICITY container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <returns>The container.</returns>
        private HMDA_ETHNICITY CreateHmdaEthnicity(CAppData dataApp)
        {
            HMDA_ETHNICITY hmdaEthnicity = new HMDA_ETHNICITY();

            if (dataApp.aHispanicT != E_aHispanicT.LeaveBlank)
            {
                hmdaEthnicity.HMDAEthnicityType = this.GetHmdaEthnicityType(dataApp.aHispanicT);
            }

            return hmdaEthnicity;
        }

        /// <summary>
        /// Converts an E_aHispanicT enum to an enum for the HMDA_ETHNICITY.HmdaEthnicityType attribute.
        /// </summary>
        /// <param name="ethnicity">The LQB ethnicity type.</param>
        /// <returns>The enum for the payload.</returns>
        private HmdaEthnicityHmdaEthnicityType GetHmdaEthnicityType(E_aHispanicT ethnicity)
        {
            if (ethnicity == E_aHispanicT.Hispanic || ethnicity == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                return HmdaEthnicityHmdaEthnicityType.HispanicOrLatino;
            }
            else if (ethnicity == E_aHispanicT.NotHispanic)
            {
                return HmdaEthnicityHmdaEthnicityType.NotHispanicOrLatino;
            }
            else
            {
                return HmdaEthnicityHmdaEthnicityType.Undefined;
            }
        }

        /// <summary>
        /// Creates a GOVERNMENT_MONITORING_DETAIL container.
        /// </summary>
        /// <param name="dataApp">The data app to use.</param>
        /// <returns>The container.</returns>
        private GOVERNMENT_MONITORING_DETAIL CreateGovernmentMonitoringDetail(CAppData dataApp)
        {
            GOVERNMENT_MONITORING_DETAIL governmentMonitoringDetail = new GOVERNMENT_MONITORING_DETAIL();
            governmentMonitoringDetail.ApplicationTakenMethodType = ToMismo(dataApp.aIntrvwrMethodT);
            governmentMonitoringDetail.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator = this.ToMismo(dataApp.aEthnicityCollectedByObservationOrSurname);
            governmentMonitoringDetail.HMDAEthnicityRefusalIndicator = this.ToMismo(dataApp.aDoesNotWishToProvideEthnicity);
            governmentMonitoringDetail.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator = this.ToMismo(dataApp.aSexCollectedByObservationOrSurname);

            var genderTypeValid = dataApp.aGender != E_GenderT.LeaveBlank && dataApp.aGender != E_GenderT.NA;
            if (genderTypeValid)
            {
                governmentMonitoringDetail.HMDAGenderRefusalIndicator = GetGenderRefusalIndicator(dataApp.aGender);
                governmentMonitoringDetail.HMDAGenderType = GetGenderType(dataApp.aGender);
            }

            governmentMonitoringDetail.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator = this.ToMismo(dataApp.aRaceCollectedByObservationOrSurname);
            governmentMonitoringDetail.HMDARaceRefusalIndicator = this.ToMismo(dataApp.aDoesNotWishToProvideRace);

            return governmentMonitoringDetail;
        }

        /// <summary>
        /// Gets an E_YesNoIndiciator for gender refusal based on E_GenderT.
        /// </summary>
        /// <param name="gender">The gender enum.</param>
        /// <returns>The enum indicator.</returns>
        public static E_YesNoIndicator GetGenderRefusalIndicator(E_GenderT gender)
        {
            if (gender == E_GenderT.Unfurnished || gender == E_GenderT.MaleAndNotFurnished || gender == E_GenderT.FemaleAndNotFurnished || gender == E_GenderT.MaleFemaleNotFurnished)
            {
                return E_YesNoIndicator.Y;
            }
            else
            {
                return E_YesNoIndicator.N;
            }
        }

        /// <summary>
        /// Converts the E_GenderT to an enum for the GOVERNMENT_MONITORING_DETAIL.HmdaGenderType attribute.
        /// </summary>
        /// <param name="gender">The gender type.</param>
        /// <returns>The payload enum.</returns>
        public static GovernmentMonitoringDetailHmdaGenderType GetGenderType(E_GenderT gender)
        {
            if (gender == E_GenderT.Male || gender == E_GenderT.MaleAndNotFurnished)
            {
                return GovernmentMonitoringDetailHmdaGenderType.Male;
            }
            else if (gender == E_GenderT.Female || gender == E_GenderT.FemaleAndNotFurnished)
            {
                return GovernmentMonitoringDetailHmdaGenderType.Female;
            }
            else if (gender == E_GenderT.MaleAndFemale || gender == E_GenderT.MaleFemaleNotFurnished)
            {
                return GovernmentMonitoringDetailHmdaGenderType.ApplicantSelectedBothMaleAndFemale;
            }
            else
            {
                return GovernmentMonitoringDetailHmdaGenderType.Undefined;
            }
        }

        /// <summary>
        /// Converts a tri state to an E_YesNoIndicator.
        /// </summary>
        /// <param name="status">The tri state.</param>
        /// <returns>The yes no indicator enum.</returns>
        private E_YesNoIndicator ToMismo(E_TriState status)
        {
            switch (status)
            {
                case E_TriState.No:
                    return E_YesNoIndicator.N;
                case E_TriState.Yes:
                    return E_YesNoIndicator.Y;
                case E_TriState.Blank:
                    return E_YesNoIndicator.Undefined;
                default:
                    throw new UnhandledEnumException(status);
            }
        }

        private MismoHmdaRace CreateHmdaRace(E_MismoHmdaRaceType type) 
        {
            MismoHmdaRace hmdaRace = new MismoHmdaRace();
            hmdaRace.Type = type;
            return hmdaRace;
        }
        private MismoEmployer CreateEmployer(IPrimaryEmploymentRecord record) 
        {
            if (null == record)
                return null;

            MismoEmployer employer = new MismoEmployer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.CurrentEmploymentMonthsOnJob =  record.EmplmtLenInMonths_rep;
            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentCurrentIndicator = E_YesNoIndicator.Y;
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YesNoIndicator.Y;

            if (employer.CurrentEmploymentYearsOnJob == "") 
            {
                employer.CurrentEmploymentYearsOnJob = "0";
            }
            if (employer.CurrentEmploymentMonthsOnJob == "") 
            {
                employer.CurrentEmploymentMonthsOnJob = "0";
            }

            return employer;
        }

        private MismoEmployer CreateEmployer(IRegularEmploymentRecord record) 
        {
            if (null == record)
                return null;
            MismoEmployer employer = new MismoEmployer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YesNoIndicator.N;
            employer.IncomeEmploymentMonthlyAmount = record.MonI_rep;
            employer.PreviousEmploymentEndDate = record.EmplmtEndD_rep;
            employer.PreviousEmploymentStartDate = record.EmplmtStartD_rep;
            employer.EmploymentCurrentIndicator = record.IsCurrent ? E_YesNoIndicator.Y : E_YesNoIndicator.N; 
            //            employer.EmploymentCurrentIndicator = Mismo.Common.E_YesNoIndicator.No;
            //            employer.CurrentEmploymentMonthsOnJob =  record.EmplmtLenInMonths_rep;
            //            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            //            employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;

            return employer;
        }
        private MismoDependent CreateDependent(string s) 
        {
            if (null == s || "" == s)
                return null;

            MismoDependent dependent = new MismoDependent();
            dependent.AgeYears = s;
            return dependent;

        }

        private MismoDeclaration CreateDeclaration(CAppData dataApp, bool isBorrower) 
        {
            MismoDeclaration declaration = new MismoDeclaration();

            declaration.AlimonyChildSupportObligationIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecAlimony : dataApp.aCDecAlimony);
            declaration.BankruptcyIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecBankrupt : dataApp.aCDecBankrupt);
            declaration.BorrowedDownPaymentIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecBorrowing : dataApp.aCDecBorrowing);
            declaration.CoMakerEndorserOfNoteIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecEndorser : dataApp.aCDecEndorser);
            declaration.LoanForeclosureOrJudgementIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecObligated : dataApp.aCDecObligated);
            declaration.OutstandingJudgementsIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecJudgment : dataApp.aCDecJudgment);
            declaration.PartyToLawsuitIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecLawsuit : dataApp.aCDecLawsuit);
            declaration.PresentlyDelinquentIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecDelinquent : dataApp.aCDecDelinquent);
            declaration.PropertyForeclosedPastSevenYearsIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecForeclosure : dataApp.aCDecForeclosure);
            declaration.BorrowerFirstTimeHomebuyerIndicator = ToMismo(this.DataLoan.sHas1stTimeBuyer);

            declaration.PriorPropertyTitleType = ToMismo(isBorrower ? dataApp.aBDecPastOwnedPropTitleT : (E_aBDecPastOwnedPropTitleT) dataApp.aCDecPastOwnedPropTitleT);
            declaration.PriorPropertyUsageType = ToMismo(isBorrower ? dataApp.aBDecPastOwnedPropT : (E_aBDecPastOwnedPropT) dataApp.aCDecPastOwnedPropT);

            string aDecCitizen = isBorrower ? dataApp.aBDecCitizen : dataApp.aCDecCitizen;
            string aDecResidency = isBorrower ? dataApp.aBDecResidency : dataApp.aCDecResidency;
            string aDecForeignNational = isBorrower ? dataApp.aBDecForeignNational : "";

            declaration.CitizenshipResidencyType = ToMismoDeclarationCitizenship(aDecCitizen, aDecResidency, aDecForeignNational);

            string aDecPastOwnership = isBorrower ? dataApp.aBDecPastOwnership : dataApp.aCDecPastOwnership;
            switch (aDecPastOwnership.ToUpper()) 
            {
                case "Y":
                    declaration.HomeownerPastThreeYearsType = E_MismoDeclarationHomeownerPastThreeYearsType.Yes;
                    break;
                case "N":
                    declaration.HomeownerPastThreeYearsType = E_MismoDeclarationHomeownerPastThreeYearsType.No;
                    break;
                default:
                    declaration.HomeownerPastThreeYearsType = E_MismoDeclarationHomeownerPastThreeYearsType.Undefined;
                    break;
            }
            string aDecOcc = isBorrower ? dataApp.aBDecOcc : dataApp.aCDecOcc;
            switch (aDecOcc.ToUpper()) 
            {
                case "Y":
                    declaration.IntentToOccupyType = E_MismoDeclarationIntentToOccupyType.Yes;
                    break;
                case "N":
                    declaration.IntentToOccupyType = E_MismoDeclarationIntentToOccupyType.No;
                    break;
                default:
                    declaration.IntentToOccupyType = E_MismoDeclarationIntentToOccupyType.Undefined;
                    break;

            }

            return declaration;
        }
        private MismoCurrentIncome CreateCurrentIncome(E_MismoCurrentIncomeIncomeType type, string amount) 
        {
            if (null == amount || "" == amount || "0.00" == amount) 
            {
                if (type == E_MismoCurrentIncomeIncomeType.Base) 
                {
                    amount = "0"; // 4/28/2008 dd - Always set base income.
                } 
                else 
                {
                    return null;
                }
            }
            
            MismoCurrentIncome currentIncome = new MismoCurrentIncome();
            currentIncome.IncomeType = type;
            currentIncome.MonthlyTotalAmount = amount;
            return currentIncome;
        }
        private MismoResidence CreateResidence(string addr, string city, string st, string zip, E_aBAddrT addrT, string addrYrs, bool isCurrent) 
        {
            // Incomplete previous addresses should be ignored.
            if (!isCurrent &&
                (addr.TrimWhitespaceAndBOM() == "" || city.TrimWhitespaceAndBOM() == "" || zip.TrimWhitespaceAndBOM() == "" || st.TrimWhitespaceAndBOM() == "" || addrT == E_aBAddrT.LeaveBlank || string.IsNullOrEmpty(addrYrs)))
            {
                return null;
            }

            YearMonthParser parser = new YearMonthParser();
            parser.Parse(addrYrs);

            if (!isCurrent && parser.NumberOfMonths == 0 && parser.NumberOfYears == 0)
            {
                return null;
            }

            MismoResidence residence = new MismoResidence();
            residence.BorrowerResidencyBasisType = ToMismo(addrT);
            residence.BorrowerResidencyDurationMonths = parser.NumberOfMonths.ToString();
            residence.BorrowerResidencyDurationYears = parser.NumberOfYears.ToString();
            residence.BorrowerResidencyType = isCurrent ? E_MismoResidenceBorrowerResidencyType.Current : E_MismoResidenceBorrowerResidencyType.Prior;
            residence.City = city;
            residence.PostalCode = zip;
            residence.State = st;
            residence.StreetAddress = addr;
            if (!isCurrent) 
            {
                // 5/2/2008 dd - LP Required to pass over the country code for prior residency. Since we do not support foreign address we always assume country is US.
                residence.Country = "US";
            }
            return residence;

        }
        private MismoExtensionLoanApplication CreateExtensionLoanApplication() 
        {
            MismoExtensionLoanApplication extensionLoanApplication = new MismoExtensionLoanApplication();
            extensionLoanApplication.FreLoanProspectorVersionIdentifier = LoanProspectorHelper.FreLoanProspectorVersionIdentifier43;

            extensionLoanApplication.FunctionRequest.Type = E_MismoFunctionRequestType.ServiceRequest;
            extensionLoanApplication.ServiceRequest.Add(CreateServiceRequest(ServiceRequest_OrderAus));

            if (this.craInformation != null) 
            {
                // Order merge credit
                if (this.craInformation.CreditReportOption == CreditReportType.OrderNew || (this.craInformation.CreditReportOption == CreditReportType.UsePrevious && this.m_hasAtLeastOneCreditReportIdentifier)) 
                {
                    extensionLoanApplication.ServiceRequest.Add(CreateServiceRequest(ServiceRequest_OrderMergedCreditAll));
                }

                else if (this.craInformation.CreditReportOption == CreditReportType.Reissue) 
                {
                    IEnumerable<CAppData> creditReorderApps = this.craInformation.Applications
                        .Where(appCraInfo => appCraInfo.ReorderApplicationCredit)
                        .SelectMany(appCraInfo => this.DataLoan.Apps.Where(app => appCraInfo.ApplicationId == app.aAppId));

                    foreach (CAppData dataApp in creditReorderApps)
                    {
                        extensionLoanApplication.ServiceRequest.Add(CreateServiceRequest("ReorderCredit_B" + dataApp.aBSsn.Replace("-", "")));
                        if (dataApp.aCSsn.Replace("-", "") != "")
                        {
                            extensionLoanApplication.ServiceRequest.Add(CreateServiceRequest("ReorderCredit_B" + dataApp.aCSsn.Replace("-", "")));
                        }
                    }
                }

                // 4/25/2008 dd - Only create MergedCreditRequest when OrderMergedCredit or CreditReportIdentifier is not null OR mortgage type == FHA

                if (this.craInformation.CreditReportOption == CreditReportType.OrderNew || this.DataLoan.sLT == E_sLT.FHA || m_hasAtLeastOneCreditReportIdentifier) 
                {
                    extensionLoanApplication.MergedCreditRequest = CreateMergedCreditRequest();
                }
            }


            extensionLoanApplication.FreRequestingParty = CreateFreRequestingParty();
            extensionLoanApplication.LoanIdentifierData = CreateLoanIdentifierData();

            return extensionLoanApplication;

        }

        private MismoMergedCreditRequest CreateMergedCreditRequest() 
        {
            MismoMergedCreditRequest mergedCreditRequest = new MismoMergedCreditRequest();
            mergedCreditRequest.CreditBureau = CreateCreditBureau();
            int nApps = this.DataLoan.nApps;

            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = this.DataLoan.GetAppData(i);

                mergedCreditRequest.FreMergedCreditSpecifications.Add(CreateFreMergedCreditSpecifications(dataApp));
            }


            return mergedCreditRequest;
        }

        private MismoFreMergedCreditSpecifications CreateFreMergedCreditSpecifications(CAppData dataApp) 
        {
            MismoFreMergedCreditSpecifications freMergedCreditSpecifications = new MismoFreMergedCreditSpecifications();

            bool hasCoborrowerInfo = dataApp.aCLastNm != "" || dataApp.aCFirstNm != "" || dataApp.aCSsn != "";

            if (dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
            {
                if (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)
                {
                    hasCoborrowerInfo = false;
                }
            }

            string borrowerId = "B" + dataApp.aBSsn.Replace("-", "");
            string coBorrowerId = hasCoborrowerInfo ? "B" + dataApp.aCSsn.Replace("-", "") : borrowerId;

            freMergedCreditSpecifications.Description = borrowerId + ", " + coBorrowerId;

            return freMergedCreditSpecifications;
        }

        private MismoCreditBureau CreateCreditBureau() 
        {
            MismoCreditBureau creditBureau = new MismoCreditBureau();

            creditBureau.Name = craInformation?.CraProviderId ?? LoanSubmitRequest.GetTemporaryCraInformation(this.DataLoan.sLId)?.CraProviderId;
            return creditBureau;
        }

        /// <summary>
        /// Creates an instance of the FRE_REQUESTING_PARTY element to serialize as MISMO.
        /// </summary>
        /// <returns>A new instance of the <see cref="MismoFreRequestingParty"/> class.</returns>
        private MismoFreRequestingParty CreateFreRequestingParty() 
        {
            MismoFreRequestingParty freRequestingParty = new MismoFreRequestingParty();

            freRequestingParty.SellerServicerNumber = this.sellerNumber;
            freRequestingParty.Password = this.sellerPassword;
            freRequestingParty.NonOriginatingThirdPartyIdentifier = this.DataLoan.sFreddieNotpNum;
            freRequestingParty.ThirdPartyOriginatorIdentifier = this.DataLoan.sFreddieTpoNum;

            return freRequestingParty;
        }

        private MismoLoanIdentifierData CreateLoanIdentifierData() 
        {
            MismoLoanIdentifierData loanIdentifierData = new MismoLoanIdentifierData();
            loanIdentifierData.GloballyUniqueIdentifier = this.DataLoan.sLId.ToString();
            loanIdentifierData.LoanProspectorKeyIdentifier = this.DataLoan.sLpAusKey;
            if (this.DataLoan.sFreddieLoanId != ConstAppDavid.LoanProspector_PendingLoanTransaction) 
            {
                loanIdentifierData.LoanProspectorLoanIdentifier = this.DataLoan.sFreddieLoanId;
            }
            loanIdentifierData.FreLoanProspectorTransactionIdentifier = this.DataLoan.sFreddieTransactionId;

            return loanIdentifierData;

        }

        private MismoServiceRequest CreateServiceRequest(string description) 
        {
            MismoServiceRequest serviceRequest = new MismoServiceRequest();
            serviceRequest.Description = description;
            return serviceRequest;
        }

        private E_MismoConstructionRefinanceDataGseRefinancePurposeType CalculateGseRefinancePurposeType()
        {
            if (this.DataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                return E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance;
            }
            else if (this.DataLoan.sRefPurpose.Equals("No Cash-Out Rate/Term", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
            }
            else if (this.DataLoan.sRefPurpose.Equals("Cash-Out/Home Improvement", StringComparison.OrdinalIgnoreCase)
                || this.DataLoan.sRefPurpose.Equals("Cash-Out/Debt Consolidation", StringComparison.OrdinalIgnoreCase)
                || this.DataLoan.sRefPurpose.Equals("Cash-Out/Other", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
            }
            else
            {
                return E_MismoConstructionRefinanceDataGseRefinancePurposeType.Other;
            }
        }

        #region Convert To Mismo enum.
        private E_MismoCategoryType ToMismo(E_sGseSpT sGseSpT, E_sProdSpStructureT structureType) 
        {
            switch (sGseSpT) 
            {
                case E_sGseSpT.Attached:
                    return E_MismoCategoryType.Attached;
                case E_sGseSpT.Condominium:
                    return E_MismoCategoryType.Undefined;
                case E_sGseSpT.Cooperative:
                    return E_MismoCategoryType.Undefined;
                case E_sGseSpT.Detached:
                    return E_MismoCategoryType.Detached;
                case E_sGseSpT.DetachedCondominium:
                    return E_MismoCategoryType.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return E_MismoCategoryType.Attached;
                case E_sGseSpT.LeaveBlank:
                    return E_MismoCategoryType.Undefined;
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return E_MismoCategoryType.Manufactured;
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return E_MismoCategoryType.ManufacturedMultiWide;
                case E_sGseSpT.ManufacturedHousing:
                    return E_MismoCategoryType.Manufactured;
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return E_MismoCategoryType.ManufacturedSingleWide;
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    switch (structureType)
                    {
                        case E_sProdSpStructureT.Attached:
                            return E_MismoCategoryType.Attached;
                        case E_sProdSpStructureT.Detached:
                            return E_MismoCategoryType.Detached;
                        default:
                            LogInvalidEnum("sProdSpStructureT", structureType);
                            return E_MismoCategoryType.Undefined;
                    }
                default:
                    LogInvalidEnum("sGseSpT", sGseSpT);
                    return E_MismoCategoryType.Undefined;
            }
        }
        private E_MismoTransmittalDataCaseStateType ToMismo(E_sFredProcPointT sFredProcPointT) 
        {
            switch (sFredProcPointT) 
            {
                case E_sFredProcPointT.Application: return E_MismoTransmittalDataCaseStateType.Application;
                case E_sFredProcPointT.FinalDisposition: return E_MismoTransmittalDataCaseStateType.FinalDisposition;
                case E_sFredProcPointT.PostClosingQualityControl: return E_MismoTransmittalDataCaseStateType.PostClosingQualityControl;
                case E_sFredProcPointT.Prequalification: return E_MismoTransmittalDataCaseStateType.Prequalification;
                case E_sFredProcPointT.Underwriting: return E_MismoTransmittalDataCaseStateType.Underwriting;
                case E_sFredProcPointT.LeaveBlank: return E_MismoTransmittalDataCaseStateType.Undefined;
                default:
                    LogInvalidEnum("sFredProcPointT", sFredProcPointT);
                    return E_MismoTransmittalDataCaseStateType.Undefined;
            }
        }
        public static E_MismoGovernmentMonitoringGenderType ToMismo(E_GenderT genderT) 
        {
            switch (genderT) 
            {
                case E_GenderT.Female: return E_MismoGovernmentMonitoringGenderType.Female;
                case E_GenderT.LeaveBlank: return E_MismoGovernmentMonitoringGenderType.Undefined;
                case E_GenderT.Male: return E_MismoGovernmentMonitoringGenderType.Male;
                case E_GenderT.NA: return E_MismoGovernmentMonitoringGenderType.NotApplicable;
                case E_GenderT.Unfurnished: return E_MismoGovernmentMonitoringGenderType.InformationNotProvidedUnknown;
                default:
                    LogInvalidEnum("E_GenderT", genderT);
                    return E_MismoGovernmentMonitoringGenderType.Undefined;
            }
        }
        private E_MismoLoanFeaturesLoanDocumentationType ToMismo(E_sFreddieDocT sFreddieDocT) 
        {
            switch (sFreddieDocT) 
            {
                case E_sFreddieDocT.FullDocumentation: return E_MismoLoanFeaturesLoanDocumentationType.FullDocumentation;
                case E_sFreddieDocT.LeaveBlank: return E_MismoLoanFeaturesLoanDocumentationType.Undefined;
                case E_sFreddieDocT.NoDepEmpIncVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
                case E_sFreddieDocT.NoDepositVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerification;
                case E_sFreddieDocT.NoDoc: return E_MismoLoanFeaturesLoanDocumentationType.NoDocumentation;
                case E_sFreddieDocT.NoEmpIncVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification;
                default:
                    LogInvalidEnum("E_sFreddieDocT", sFreddieDocT);
                    return E_MismoLoanFeaturesLoanDocumentationType.Undefined;
            }
        }
        public static E_MismoGovernmentMonitoringHmdaEthnicityType ToMismo(E_aHispanicT aHispanicT) 
        {
            switch (aHispanicT) 
            {
                case E_aHispanicT.Hispanic: return E_MismoGovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
                case E_aHispanicT.LeaveBlank: return E_MismoGovernmentMonitoringHmdaEthnicityType.Undefined;
                case E_aHispanicT.NotHispanic: return E_MismoGovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
                default:
                    LogInvalidEnum("E_aHispanicT", aHispanicT);
                    return E_MismoGovernmentMonitoringHmdaEthnicityType.Undefined;
            }
        }
        private E_MismoDeclarationCitizenshipResidencyType ToMismoDeclarationCitizenship(string aDecCitizen, string aDecResidency, string aDecForeignNational) 
        {
            E_MismoDeclarationCitizenshipResidencyType ret = E_MismoDeclarationCitizenshipResidencyType.Undefined;
            
            if (aDecCitizen.ToUpper() == "Y") 
            {
                ret = E_MismoDeclarationCitizenshipResidencyType.USCitizen;
            } 
            else 
            {
                switch (aDecResidency.ToUpper()) 
                {
                    case "Y": 
                        ret = E_MismoDeclarationCitizenshipResidencyType.PermanentResidentAlien;
                        break;
                    case "N":
                        if (aDecForeignNational.ToUpper() == "Y")
                            ret = E_MismoDeclarationCitizenshipResidencyType.NonResidentAlien;
                        else
                            ret = E_MismoDeclarationCitizenshipResidencyType.NonPermanentResidentAlien;
                        break;
                }
            }
            return ret;

        }
        private E_MismoDeclarationPriorPropertyUsageType ToMismo(E_aBDecPastOwnedPropT aBDecPastOwnedPropT) 
        {
            switch (aBDecPastOwnedPropT) 
            {
                case E_aBDecPastOwnedPropT.Empty: return E_MismoDeclarationPriorPropertyUsageType.Undefined;
                case E_aBDecPastOwnedPropT.IP: return E_MismoDeclarationPriorPropertyUsageType.Investment;
                case E_aBDecPastOwnedPropT.PR: return E_MismoDeclarationPriorPropertyUsageType.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH: return E_MismoDeclarationPriorPropertyUsageType.SecondaryResidence;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropT", aBDecPastOwnedPropT);
                    return E_MismoDeclarationPriorPropertyUsageType.Undefined;
            }
        }
        private E_MismoDeclarationPriorPropertyTitleType ToMismo(E_aBDecPastOwnedPropTitleT aBDecPastOwnedPropTitleT) 
        {
            switch (aBDecPastOwnedPropTitleT) 
            {
                case E_aBDecPastOwnedPropTitleT.O: return E_MismoDeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.S: return E_MismoDeclarationPriorPropertyTitleType.Sole;
                case E_aBDecPastOwnedPropTitleT.SP: return E_MismoDeclarationPriorPropertyTitleType.JointWithSpouse;
                case E_aBDecPastOwnedPropTitleT.Empty: return E_MismoDeclarationPriorPropertyTitleType.Undefined;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropTitleT", aBDecPastOwnedPropTitleT);
                    return E_MismoDeclarationPriorPropertyTitleType.Undefined;
            }
        }
        private E_YesNoIndicator ToMismoFromDeclaration(string str) 
        {
            switch (str.ToUpper()) 
            {
                case "Y": return E_YesNoIndicator.Y;
                case "N": return E_YesNoIndicator.N;
                default:
                    return E_YesNoIndicator.Undefined;
            }
        }
        private E_MismoCurrentIncomeIncomeType ToMismoIncomeType(string desc) 
        {
            if (string.IsNullOrEmpty(desc))
            {
                return E_MismoCurrentIncomeIncomeType.Undefined;
            }
            else if (desc.Equals("subject property net cash flow (two-to-four unit owner-occupied properties)"))
            {
                return E_MismoCurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
            }
            else
            {
                var descriptionType = OtherIncome.Get_aOIDescT(desc);
                switch (descriptionType)
                {
                    case E_aOIDescT.AlimonyChildSupport:
                    case E_aOIDescT.Alimony:
                    case E_aOIDescT.ChildSupport:
                        return E_MismoCurrentIncomeIncomeType.AlimonyChildSupport;
                    case E_aOIDescT.AutomobileExpenseAccount:
                        return E_MismoCurrentIncomeIncomeType.AutomobileExpenseAccount;
                    case E_aOIDescT.FosterCare:
                        return E_MismoCurrentIncomeIncomeType.FosterCare;
                    case E_aOIDescT.MilitaryBasePay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryBasePay;
                    case E_aOIDescT.MilitaryClothesAllowance:
                        return E_MismoCurrentIncomeIncomeType.MilitaryClothesAllowance;
                    case E_aOIDescT.MilitaryCombatPay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryCombatPay;
                    case E_aOIDescT.MilitaryFlightPay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryFlightPay;
                    case E_aOIDescT.MilitaryHazardPay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryHazardPay;
                    case E_aOIDescT.MilitaryOverseasPay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryOverseasPay;
                    case E_aOIDescT.MilitaryPropPay:
                        return E_MismoCurrentIncomeIncomeType.MilitaryPropPay;
                    case E_aOIDescT.MilitaryQuartersAllowance:
                        return E_MismoCurrentIncomeIncomeType.MilitaryQuartersAllowance;
                    case E_aOIDescT.MilitaryRationsAllowance:
                        return E_MismoCurrentIncomeIncomeType.MilitaryRationsAllowance;
                    case E_aOIDescT.MilitaryVariableHousingAllowance:
                    case E_aOIDescT.HousingAllowance:
                        return E_MismoCurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
                    case E_aOIDescT.NotesReceivableInstallment:
                        return E_MismoCurrentIncomeIncomeType.NotesReceivableInstallment;
                    case E_aOIDescT.PensionRetirement:
                        return E_MismoCurrentIncomeIncomeType.Pension;
                    case E_aOIDescT.RealEstateMortgageDifferential:
                        return E_MismoCurrentIncomeIncomeType.MortgageDifferential;
                    case E_aOIDescT.SocialSecurity:
                    case E_aOIDescT.SocialSecurityDisability:
                        return E_MismoCurrentIncomeIncomeType.SocialSecurity;
                    case E_aOIDescT.SubjPropNetCashFlow:
                        return E_MismoCurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
                    case E_aOIDescT.Trust:
                        return E_MismoCurrentIncomeIncomeType.Trust;
                    case E_aOIDescT.UnemploymentWelfare:
                        return E_MismoCurrentIncomeIncomeType.Unemployment;
                    case E_aOIDescT.VABenefitsNonEducation:
                        return E_MismoCurrentIncomeIncomeType.VABenefitsNonEducational;
                    case E_aOIDescT.ContractBasis:
                        return E_MismoCurrentIncomeIncomeType.ContractBasis;
                    case E_aOIDescT.Other:
                    case E_aOIDescT.PublicAssistance:
                    case E_aOIDescT.WorkersCompensation:
                    case E_aOIDescT.MiscellaneousIncome:
                    case E_aOIDescT.DefinedContributionPlan:
                    case E_aOIDescT.AccessoryUnitIncome:
                    case E_aOIDescT.BoarderIncome:
                    case E_aOIDescT.CapitalGains:
                    case E_aOIDescT.EmploymentRelatedAssets:
                    case E_aOIDescT.ForeignIncome:
                    case E_aOIDescT.HousingChoiceVoucher:
                    case E_aOIDescT.MortgageCreditCertificate:
                    case E_aOIDescT.NonBorrowerHouseholdIncome:
                    case E_aOIDescT.RoyaltyPayment:
                    case E_aOIDescT.SeasonalIncome:
                    case E_aOIDescT.TemporaryLeave:
                    case E_aOIDescT.TipIncome:
                    case E_aOIDescT.TrailingCoBorrowerIncome:
                    case E_aOIDescT.Disability:
                        return E_MismoCurrentIncomeIncomeType.OtherTypesOfIncome;
                    default:
                        throw new UnhandledEnumException(descriptionType);
                }
            }
        }

        private E_MismoReoPropertyDispositionStatusType ToMismo(E_ReoStatusT statusT) 
        {
            switch (statusT) 
            {
                case E_ReoStatusT.PendingSale: return E_MismoReoPropertyDispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return E_MismoReoPropertyDispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return E_MismoReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return E_MismoReoPropertyDispositionStatusType.Sold;
                default:
                    LogInvalidEnum("E_ReoStatusT", statusT);
                    return E_MismoReoPropertyDispositionStatusType.Undefined;
                                                               

            }
        }
        private E_MismoReoPropertyGsePropertyType ToMismoReoType(E_ReoTypeT type) 
        {
            switch (type) 
            {
                case  E_ReoTypeT._2_4Plx: return E_MismoReoPropertyGsePropertyType.TwoToFourUnitProperty;
                case  E_ReoTypeT.ComNR: return E_MismoReoPropertyGsePropertyType.CommercialNonResidential;
                case  E_ReoTypeT.ComR: return E_MismoReoPropertyGsePropertyType.HomeAndBusinessCombined;
                case  E_ReoTypeT.Condo: return E_MismoReoPropertyGsePropertyType.Condominium;
                case  E_ReoTypeT.Coop: return E_MismoReoPropertyGsePropertyType.Cooperative;
                case  E_ReoTypeT.Farm: return E_MismoReoPropertyGsePropertyType.Farm;
                case  E_ReoTypeT.Land: return E_MismoReoPropertyGsePropertyType.Land;
                case  E_ReoTypeT.Mixed: return E_MismoReoPropertyGsePropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return E_MismoReoPropertyGsePropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return E_MismoReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: return E_MismoReoPropertyGsePropertyType.SingleFamily;
                case E_ReoTypeT.Town: return E_MismoReoPropertyGsePropertyType.Townhouse;
                case E_ReoTypeT.Other: return E_MismoReoPropertyGsePropertyType.Undefined;
                default:
                    return E_MismoReoPropertyGsePropertyType.Undefined;
            }
        }
        private E_MismoPropertyBuildingStatusType ToMismo(E_sBuildingStatusT sBuildingStatusT) 
        {
            switch (sBuildingStatusT) 
            {
                case E_sBuildingStatusT.Existing: return E_MismoPropertyBuildingStatusType.Existing;
                case E_sBuildingStatusT.LeaveBlank: return E_MismoPropertyBuildingStatusType.Undefined;
                case E_sBuildingStatusT.Proposed: return E_MismoPropertyBuildingStatusType.Proposed;
                case E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab: return E_MismoPropertyBuildingStatusType.SubjectToAlterationImprovementRepairAndRehabilitation;
                case E_sBuildingStatusT.SubstantiallyRehabilitated: return E_MismoPropertyBuildingStatusType.SubstantiallyRehabilitated;
                case E_sBuildingStatusT.UnderConstruction: return E_MismoPropertyBuildingStatusType.UnderConstruction;
                default:
                    LogInvalidEnum("E_sBuildingStatusT", sBuildingStatusT);
                    return E_MismoPropertyBuildingStatusType.Undefined;
            }
        }

        public static E_MismoLiabilityType ToMismo(E_DebtRegularT debtT, string description) 
        {
            switch (debtT) 
            {
                case E_DebtRegularT.Installment:
                    return E_MismoLiabilityType.Installment;
                case E_DebtRegularT.Mortgage:
                    return E_MismoLiabilityType.MortgageLoan;
                case E_DebtRegularT.Open:
                    return E_MismoLiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Revolving:
                    return E_MismoLiabilityType.Revolving;
                case E_DebtRegularT.Other:
                    return GetOtherLiabilityType(description);
                default:
                    LogInvalidEnum("E_DebtRegularT", debtT);
                    return E_MismoLiabilityType.Undefined;
            }
        }

        public static E_MismoLiabilityType GetOtherLiabilityType(string description)
        {
            if (description.Equals("Liens", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.CollectionsJudgementsAndLiens;
            }
            else if (description.Equals("Home Equity Line of Credit", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.HELOC;
            }
            else if (description.Equals("Installment Loan", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.Installment;
            }
            else if (description.Equals("Lease Payments", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.LeasePayments;
            }
            else if (description.Equals("Mortgage", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.MortgageLoan;
            }
            else if (description.Equals("1st Existing Lien", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.MortgageLoan;
            }
            else if (description.Equals("2nd Existing Lien", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.MortgageLoan;
            }
            else if (description.Equals("Open, 30day Charge Acc", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.Open30DayChargeAccount;
            }
            else if (description.Equals("Revolving Charge", StringComparison.OrdinalIgnoreCase))
            {
                return E_MismoLiabilityType.Revolving;
            }
            else
            {
                return E_MismoLiabilityType.OtherLiability;
            }
        }

        private E_YesNoIndicator ToMismo(bool v) 
        {
            return v ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
        }
        public static E_MismoAssetType ToMismo(E_AssetRegularT assetRegularT) 
        {
            switch (assetRegularT) 
            {
                case E_AssetRegularT.Auto: return E_MismoAssetType.Automobile;
                case E_AssetRegularT.Bonds: return E_MismoAssetType.Bond;
                case E_AssetRegularT.Checking: return E_MismoAssetType.CheckingAccount;
                case E_AssetRegularT.GiftFunds: return E_MismoAssetType.GiftsTotal;
                case E_AssetRegularT.OtherIlliquidAsset: return E_MismoAssetType.OtherNonLiquidAssets;
                case E_AssetRegularT.OtherLiquidAsset: return E_MismoAssetType.OtherLiquidAssets;
                case E_AssetRegularT.Savings: return E_MismoAssetType.SavingsAccount;
                case E_AssetRegularT.Stocks: return E_MismoAssetType.Stock;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets: return E_MismoAssetType.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.CertificateOfDeposit: return E_MismoAssetType.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.MoneyMarketFund: return E_MismoAssetType.MoneyMarketFund;
                case E_AssetRegularT.MutualFunds: return E_MismoAssetType.MutualFund;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit: return E_MismoAssetType.SecuredBorrowedFundsNotDeposited;
                case E_AssetRegularT.BridgeLoanNotDeposited: return E_MismoAssetType.BridgeLoanNotDeposited;
                case E_AssetRegularT.TrustFunds: return E_MismoAssetType.TrustAccount;
                default:
                    LogInvalidEnum("E_AssetRegularT", assetRegularT);
                    return E_MismoAssetType.Undefined;
            }

        }
        public static E_MismoDownPaymentType ToMismoDownPaymentType(string src) 
        {
            switch (src.ToLower())
            {
                case "checking/savings":
                    return E_MismoDownPaymentType.CheckingSavings;
                case "gift funds":
                    return E_MismoDownPaymentType.GiftFunds;
                case "stocks & bonds":
                    return E_MismoDownPaymentType.StocksAndBonds;
                case "lot equity":
                    return E_MismoDownPaymentType.LotEquity;
                case "bridge loan":
                    return E_MismoDownPaymentType.BridgeLoan;
                case "trust funds":
                    return E_MismoDownPaymentType.TrustFunds;
                case "retirement funds":
                    return E_MismoDownPaymentType.RetirementFunds;
                case "life insurance cash value":
                    return E_MismoDownPaymentType.LifeInsuranceCashValue;
                case "sale of chattel":
                    return E_MismoDownPaymentType.SaleOfChattel;
                case "trade equity":
                    return E_MismoDownPaymentType.TradeEquity;
                case "sweat equity":
                    return E_MismoDownPaymentType.SweatEquity;
                case "cash on hand":
                    return E_MismoDownPaymentType.CashOnHand;
                case "deposit on sales contract":
                    return E_MismoDownPaymentType.DepositOnSalesContract;
                case "equity from pending sale":
                    return E_MismoDownPaymentType.EquityOnPendingSale;
                case "equity from subject property":
                    return E_MismoDownPaymentType.EquityOnSubjectProperty;
                case "equity on sold property":
                    return E_MismoDownPaymentType.EquityOnSoldProperty;
                case "other type of down payment":
                    return E_MismoDownPaymentType.OtherTypeOfDownPayment;
                case "rent with option to purchase":
                    return E_MismoDownPaymentType.RentWithOptionToPurchase;
                case "secured borrowed funds":
                    return E_MismoDownPaymentType.SecuredBorrowedFunds;
                case "unsecured borrowed funds":
                    return E_MismoDownPaymentType.UnsecuredBorrowedFunds;
                case "fha - gift - source n/a":
                case "fha - gift - source relative":
                case "fha - gift - source government assistance":
                case "fha - gift - source employer":
                case "fha - gift - source nonprofit/religious/community - non-seller funded":
                    return E_MismoDownPaymentType.GiftFunds;
                case "forgivable secured loan":
                    return E_MismoDownPaymentType.ForgivableSecuredLoan;
                default:
                    return string.IsNullOrEmpty(src)
                        ? E_MismoDownPaymentType.Undefined
                        : E_MismoDownPaymentType.OtherTypeOfDownPayment;
            }
        }
        private E_MismoClosingAgentType ToMismo(E_AgentRoleT agentRoleT) 
        {
            switch (agentRoleT) 
            {
                case E_AgentRoleT.SellerAttorney:
                case E_AgentRoleT.BuyerAttorney: return E_MismoClosingAgentType.Attorney;
                case E_AgentRoleT.ClosingAgent: return E_MismoClosingAgentType.ClosingAgent;
                case E_AgentRoleT.Escrow: return E_MismoClosingAgentType.EscrowCompany;
                case E_AgentRoleT.Title: return E_MismoClosingAgentType.TitleCompany;
                default:
                    LogInvalidEnum("E_AgentRoleT", agentRoleT);
                    return E_MismoClosingAgentType.Undefined;
            }
        }
        private E_MismoConstructionRefinanceDataRefinanceImprovementsType ToMismo(E_sSpImprovTimeFrameT sSpImprovTimeFrameT) 
        {
            switch (sSpImprovTimeFrameT) 
            {
                case E_sSpImprovTimeFrameT.LeaveBlank: return E_MismoConstructionRefinanceDataRefinanceImprovementsType.Undefined;
                case E_sSpImprovTimeFrameT.Made: return E_MismoConstructionRefinanceDataRefinanceImprovementsType.Made;
                case E_sSpImprovTimeFrameT.ToBeMade: return E_MismoConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
                default:
                    LogInvalidEnum("E_sSpImprovTimeFrameT", sSpImprovTimeFrameT);
                    return E_MismoConstructionRefinanceDataRefinanceImprovementsType.Undefined;
            }
        }
        private E_MismoConstructionRefinanceDataGseRefinancePurposeType ToMismoGseRefinancePurposeType(string desc) 
        {
            switch (desc.ToLower()) 
            {
                case "no cash-out rate/term":       return E_MismoConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
                case "limited cash-out rate/term":  return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case "cash-out/home improvement":   return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case "cash-out/other":              return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                default:
                    if (this.DataLoan.sLPurposeT == E_sLPurposeT.Refin || this.DataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || this.DataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                        return E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                    else if (this.DataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || this.DataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                        return E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                    else
                        return E_MismoConstructionRefinanceDataGseRefinancePurposeType.Undefined;

            }
        }
        public static E_MismoBuydownContributorType ToMismo(E_sBuydownContributorT sBuydownContributorT) 
        {
            switch (sBuydownContributorT) 
            {
                case E_sBuydownContributorT.Borrower: return E_MismoBuydownContributorType.Borrower;
                case E_sBuydownContributorT.Builder: return E_MismoBuydownContributorType.Builder;
                case E_sBuydownContributorT.Employer: return E_MismoBuydownContributorType.Employer;
                case E_sBuydownContributorT.LeaveBlank: return E_MismoBuydownContributorType.Undefined;
                case E_sBuydownContributorT.LenderPremiumFinanced: return E_MismoBuydownContributorType.LenderPremiumFinanced;
                case E_sBuydownContributorT.NonParentRelative: return E_MismoBuydownContributorType.NonParentRelative;
                case E_sBuydownContributorT.Other: return E_MismoBuydownContributorType.Other;
                case E_sBuydownContributorT.Parent: return E_MismoBuydownContributorType.Parent;
                case E_sBuydownContributorT.Seller: return E_MismoBuydownContributorType.Seller;
                case E_sBuydownContributorT.Unassigned: return E_MismoBuydownContributorType.Unassigned;
                case E_sBuydownContributorT.UnrelatedFriend: return E_MismoBuydownContributorType.UnrelatedFriend;
                default:
                    LogInvalidEnum("E_sBuydownContributorT", sBuydownContributorT);
                    return E_MismoBuydownContributorType.Undefined;
            }
        }
        public static E_MismoLoanPurposePropertyUsageType ToMismo(E_aOccT aOccT) 
        {
            switch (aOccT) 
            {
                case E_aOccT.PrimaryResidence: return E_MismoLoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.Investment: return E_MismoLoanPurposePropertyUsageType.Investment;
                case E_aOccT.SecondaryResidence: return E_MismoLoanPurposePropertyUsageType.SecondHome;
                default:
                    LogInvalidEnum("E_aOccT", aOccT);
                    return E_MismoLoanPurposePropertyUsageType.Undefined;
            }
        }
        private E_MismoLoanPurposePropertyRightsType ToMismo(E_sEstateHeldT sEstateHeldT) 
        {
            switch (sEstateHeldT) 
            {
                case E_sEstateHeldT.FeeSimple: return E_MismoLoanPurposePropertyRightsType.FeeSimple;
                case E_sEstateHeldT.LeaseHold: return E_MismoLoanPurposePropertyRightsType.Leasehold;
                default:
                    LogInvalidEnum("E_sEstateHeldT", sEstateHeldT);
                    return E_MismoLoanPurposePropertyRightsType.Undefined;
            }
        }
        public static E_MismoLoanPurposeType ToMismo(E_sLPurposeT sLPurposeT, ConstructionPurpose constructionPurpose) 
        {
            // If the new construction data is in play (that is, the construction purpose is not blank),
            // both Construct and ConstructPerm will map to Purchase or Refinance depending on that purpose.
            var constructionValue = constructionPurpose == ConstructionPurpose.ConstructionAndLotPurchase
                ? E_MismoLoanPurposeType.Purchase
                : E_MismoLoanPurposeType.Refinance;

            switch (sLPurposeT) 
            {
                // LPA 4.8.x does not permit the use of the ConstructionOnly or ConstructionToPermanent values.
                // They must be mapped to Purchase or Refinance.
                case E_sLPurposeT.Construct:
                    return constructionPurpose == ConstructionPurpose.Blank
                        ? E_MismoLoanPurposeType.ConstructionOnly
                        : constructionValue;
                case E_sLPurposeT.ConstructPerm:
                    return constructionPurpose == ConstructionPurpose.Blank
                        ? E_MismoLoanPurposeType.ConstructionToPermanent
                        : constructionValue;
                case E_sLPurposeT.Other:
                    return E_MismoLoanPurposeType.Other;
                case E_sLPurposeT.Purchase:
                    return E_MismoLoanPurposeType.Purchase;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                    return E_MismoLoanPurposeType.Refinance;
                case E_sLPurposeT.RefinCashout:
                    return E_MismoLoanPurposeType.Refinance;
                case E_sLPurposeT.HomeEquity:
                    return E_MismoLoanPurposeType.Refinance;
                default:
                    LogInvalidEnum("E_sLPurposeT", sLPurposeT);
                    return E_MismoLoanPurposeType.Undefined;
            }
        }
        public static E_MismoLoanFeaturesNegativeAmortizationType ToMismo(E_sNegAmortT sNegAmortT) 
        {
            switch(sNegAmortT) 
            {
                case E_sNegAmortT.NoNegAmort: return E_MismoLoanFeaturesNegativeAmortizationType.NoNegativeAmortization;
                case E_sNegAmortT.PotentialNegAmort: return E_MismoLoanFeaturesNegativeAmortizationType.PotentialNegativeAmortization;
                case E_sNegAmortT.ScheduledNegAmort: return E_MismoLoanFeaturesNegativeAmortizationType.ScheduledNegativeAmortization;
                case E_sNegAmortT.LeaveBlank: return E_MismoLoanFeaturesNegativeAmortizationType.Undefined;
                default:
                    LogInvalidEnum("E_sNegAmortT", sNegAmortT);
                    return E_MismoLoanFeaturesNegativeAmortizationType.Undefined;
            }
        }
        public static E_MismoLoanFeaturesLienPriorityType ToMismo(E_sLienPosT sLienPosT) 
        {
            switch (sLienPosT) 
            {
                case E_sLienPosT.First: return E_MismoLoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second: return E_MismoLoanFeaturesLienPriorityType.SecondLien;
                default:
                    LogInvalidEnum("E_sLienPosT", sLienPosT);
                    return E_MismoLoanFeaturesLienPriorityType.Undefined;
            }

        }
        private E_MismoLoanFeaturesGseProjectClassificationType ToMismo(E_sCondoProjClassT sCondoProjClassT) 
        {
            switch (sCondoProjClassT) 
            {
                case E_sCondoProjClassT.A_IIICondominium: return E_MismoLoanFeaturesGseProjectClassificationType.A_IIICondominium;
                case E_sCondoProjClassT.ApprovedFHA_VACondominiumProjectOrSpotLoan: return E_MismoLoanFeaturesGseProjectClassificationType.ApprovedFHA_VACondominiumProjectOrSpotLoan;
                case E_sCondoProjClassT.B_IICondominium: return E_MismoLoanFeaturesGseProjectClassificationType.B_IICondominium;
                case E_sCondoProjClassT.C_ICondominium: return E_MismoLoanFeaturesGseProjectClassificationType.C_ICondominium;
                case E_sCondoProjClassT.LeaveBlank: return E_MismoLoanFeaturesGseProjectClassificationType.Undefined;
                default:
                    LogInvalidEnum("E_sCondoProjClassT", sCondoProjClassT);
                    return E_MismoLoanFeaturesGseProjectClassificationType.Undefined;
            }
        }
        private E_MismoArmIndexType ToMismo(E_sFreddieArmIndexT sFreddieArmIndexT) 
        {
            switch (sFreddieArmIndexT) 
            {
                case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds: return E_MismoArmIndexType.EleventhDistrictCostOfFunds;
                case E_sFreddieArmIndexT.LeaveBlank: return E_MismoArmIndexType.Undefined;
                case E_sFreddieArmIndexT.LIBOR: return E_MismoArmIndexType.LIBOR;
                case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds: return E_MismoArmIndexType.NationalMonthlyMedianCostOfFunds;
                case E_sFreddieArmIndexT.OneYearTreasury: return E_MismoArmIndexType.OneYearTreasury;
                case E_sFreddieArmIndexT.Other: return E_MismoArmIndexType.Other;
                case E_sFreddieArmIndexT.SixMonthTreasury: return E_MismoArmIndexType.SixMonthTreasury;
                case E_sFreddieArmIndexT.ThreeYearTreasury: return E_MismoArmIndexType.ThreeYearTreasury;
                default:
                    LogInvalidEnum("E_sFreddieArmIndexT", sFreddieArmIndexT);
                    return E_MismoArmIndexType.Undefined;
            }
        }
        public static ApplicationTakenMethodType ToMismo(E_aIntrvwrMethodT aIntrvwrMethodT) 
        {
            switch (aIntrvwrMethodT) 
            {
                case E_aIntrvwrMethodT.ByMail: return ApplicationTakenMethodType.Mail;
                case E_aIntrvwrMethodT.ByTelephone: return ApplicationTakenMethodType.Telephone;
                case E_aIntrvwrMethodT.FaceToFace: return ApplicationTakenMethodType.FaceToFace;
                case E_aIntrvwrMethodT.Internet: return ApplicationTakenMethodType.Internet;
                case E_aIntrvwrMethodT.LeaveBlank: return ApplicationTakenMethodType.Undefined;
                default:
                    LogInvalidEnum("E_aIntrvwrMethodT", aIntrvwrMethodT);
                    return ApplicationTakenMethodType.Undefined;
            }
        }
        public static E_MismoMortgageTermsLoanAmortizationType ToMismo(E_sFinMethT sFinMethT) 
        {
            switch (sFinMethT) 
            {
                case E_sFinMethT.ARM: return E_MismoMortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Fixed: return E_MismoMortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.Graduated: return E_MismoMortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    LogInvalidEnum("E_sFinMethT", sFinMethT);
                    return E_MismoMortgageTermsLoanAmortizationType.Undefined;
            }
        }
        public static E_MismoMortgageTermsMortgageType ToMismo(E_sLT sLT) 
        {
            switch (sLT) 
            {
                case E_sLT.Conventional: return E_MismoMortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return E_MismoMortgageTermsMortgageType.FHA;
                case E_sLT.Other: return E_MismoMortgageTermsMortgageType.Other;
                case E_sLT.UsdaRural: return E_MismoMortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.VA: return E_MismoMortgageTermsMortgageType.VA;
                default:
                    LogInvalidEnum("E_sLT", sLT);
                    return E_MismoMortgageTermsMortgageType.Undefined;
            }
        }
        private E_MismoBorrowerMaritalStatusType ToMismo(E_aBMaritalStatT aBMaritalStatT) 
        {
            switch (aBMaritalStatT) 
            {
                case E_aBMaritalStatT.Married: return E_MismoBorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return E_MismoBorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return E_MismoBorrowerMaritalStatusType.Separated;
                case E_aBMaritalStatT.LeaveBlank: return E_MismoBorrowerMaritalStatusType.NotProvided;
                default:
                    LogInvalidEnum("E_aBMaritalStatT", aBMaritalStatT);
                    return E_MismoBorrowerMaritalStatusType.Unknown;
            }
        }
        private E_MismoBorrowerMaritalStatusType ToMismo(E_aCMaritalStatT aCMaritalStatT) 
        {
            switch (aCMaritalStatT) 
            {
                case E_aCMaritalStatT.Married: return E_MismoBorrowerMaritalStatusType.Married;
                case E_aCMaritalStatT.NotMarried: return E_MismoBorrowerMaritalStatusType.Unmarried;
                case E_aCMaritalStatT.Separated: return E_MismoBorrowerMaritalStatusType.Separated;
                case E_aCMaritalStatT.LeaveBlank: return E_MismoBorrowerMaritalStatusType.NotProvided;
                default:
                    LogInvalidEnum("E_aCMaritalStatT", aCMaritalStatT);
                    return E_MismoBorrowerMaritalStatusType.Unknown;
            }
        }
        private E_MismoResidenceBorrowerResidencyBasisType ToMismo(E_aBAddrT aAddrT) 
        {
            switch (aAddrT) 
            {
                case E_aBAddrT.Own: return E_MismoResidenceBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent: return E_MismoResidenceBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LivingRentFree: return E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree;
                case E_aBAddrT.LeaveBlank: return E_MismoResidenceBorrowerResidencyBasisType.Undefined;
                default:
                    LogInvalidEnum("E_aBAddrT", aAddrT);
                    return E_MismoResidenceBorrowerResidencyBasisType.Undefined;

            }
        }

        #endregion
        private static void LogInvalidEnum(string type, Enum value) 
        {
            Tools.LogBug("Unhandle enum value='" + value + "' for " + type);
        }


    }

    /// <summary>
    /// Class used to link REOs and liabilities on LP Export.
    /// </summary>
    internal class REOCounter
    {
        /// <summary>
        /// Prefix for all REO MISMO identifiers. REO ID must begin with an "R".
        /// </summary>
        private const string REOPREFIX = "R";
        /// <summary>
        /// Dictionary containing lookup between REO identifier in LQB and corresponding MISMO identifier.
        /// </summary>
        private Dictionary<Guid, string> m_ReoIdLookup;
        /// <summary>
        /// REO index counter.
        /// </summary>
        private int m_ReoMismoCounter;

        /// <summary>
        /// Constructs REOCounter object.
        /// </summary>
        internal REOCounter()
        {
            this.m_ReoIdLookup = new Dictionary<Guid, string>();
            ////Counter must be assigned sequentially starting with 1 and up to a maximum of 99.
            this.m_ReoMismoCounter = 1;
        }

        /// <summary>
        /// Adds REO identifier in LQB to lookup. MISMO identifier is auto-populated.
        /// </summary>
        /// <param name="reoId">The REO identifier in LQB.</param>
        /// <returns>MISMO identifier for added REO.</returns>
        internal string Add(Guid reoId)
        {
            string mismoValue = REOPREFIX + m_ReoMismoCounter++;
            this.m_ReoIdLookup.Add(reoId, mismoValue);

            return mismoValue;
        }

        /// <summary>
        /// Retrieves MISMO identifier for given REO identifier in LQB.
        /// </summary>
        /// <param name="reoId">The REO identifier in LQB.</param>
        /// <returns>MISMO identifier for given REO, null if it does not exist in the dictionary.</returns>
        internal string Retrieve(Guid reoId)
        {
            string value;
            this.m_ReoIdLookup.TryGetValue(reoId, out value);
            return value;
        }
    }
}
