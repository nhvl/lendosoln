﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using LoanProspectorResponse;

    /// <summary>
    /// Defines simple parsing of data from the integration with Freddie Mac's Loan Prospector.
    /// </summary>
    public static class LoanProspectorResponseParser
    {
        /// <summary>
        /// Logs the parsed response, masking out any sensitive information.
        /// </summary>
        /// <param name="parsedResponse">The parsed response.</param>
        public static void LogParsedResult(ParseResult<MismoServiceOrderResponse> parsedResponse)
        {
            if (!parsedResponse.IsSuccessful)
            {
                LoanProspectorHelper.LogPayload(parsedResponse.Input, LoanProspectorHelper.LpaResponseHeader);
            }
            else if (string.IsNullOrEmpty(parsedResponse.ValueOrDefault.LoanApplication.ExtensionLoanApplication.FreRequestingParty.Password))
            {
                LoanProspectorHelper.LogPayload(parsedResponse.Input, LoanProspectorHelper.LpaResponseHeader);
            }
            else
            {
                var response = parsedResponse.ValueOrDefault;
                var freRequestingParty = response.LoanApplication.ExtensionLoanApplication.FreRequestingParty;
                string oldPassword = freRequestingParty.Password;
                freRequestingParty.Password = Constants.ConstAppDavid.FakePasswordDisplay;

                string responseString;
                var xmlSettings = new XmlWriterSettings();
                xmlSettings.Encoding = new UTF8Encoding(false);
                using (MemoryStream stream = new MemoryStream(5000))
                {
                    using (XmlWriter writer = XmlWriter.Create(stream, xmlSettings))
                    {
                        response.WriteXml(writer);
                    }

                    byte[] bytes = stream.GetBuffer();
                    responseString = Encoding.UTF8.GetString(bytes, 0, (int)stream.Position);
                }

                freRequestingParty.Password = oldPassword;
                LoanProspectorHelper.LogPayload(responseString, LoanProspectorHelper.LpaResponseHeader);
                LoanProspectorHelper.LogPayload(response?.LoanFeedback?.MergedCreditReport?.InnerText, LoanProspectorHelper.LpaResponseCreditReportHeader);
            }
        }

        /// <summary>
        /// Parses Loan Prospector response xml into MismoServiceOrderResponse.
        /// </summary>
        /// <param name="xml">The xml string from Loan Prospector to be parsed.</param>
        /// <returns>An object containing a complete result of the parsing, including any modifications necessary to the input xml.</returns>
        public static ParseResult<MismoServiceOrderResponse> Parse(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return ParseResult<MismoServiceOrderResponse>.Failure(xml);
            }

            try
            {
                using (XmlReader reader = SetUpReader(xml))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "SERVICE_ORDER_RESPONSE")
                        {
                            var serviceOrderResponse = new MismoServiceOrderResponse();
                            serviceOrderResponse.ReadXml(reader);
                            return ParseResult<MismoServiceOrderResponse>.Success(xml, serviceOrderResponse);
                        }
                    }
                }

                return ParseResult<MismoServiceOrderResponse>.Failure(xml);
            }
            catch (XmlException exc)
            {
                DataAccess.Tools.LogWarning(exc); // Invalid XML is a mostly user input issue, rather than a developer bug
                return ParseResult<MismoServiceOrderResponse>.Failure(xml);
            }
        }

        /// <summary>
        /// Parses the LP xml specifically for ERROR containers.
        /// </summary>
        /// <param name="xml">The xml to parse.</param>
        /// <returns>List of MismoError containers found.</returns>
        public static ParseResult<List<MismoError>> ParseForError(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return ParseResult<List<MismoError>>.Failure(xml);
            }

            try
            {
                List<MismoError> errors = new List<MismoError>();
                using (XmlReader reader = SetUpReader(xml))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "ERROR")
                        {
                            var error = new MismoError();
                            error.ReadXml(reader);

                            errors.Add(error);
                        }
                    }
                }

                return ParseResult<List<MismoError>>.Success(xml, errors);
            }
            catch (XmlException exc)
            {
                // Invalid XML is a mostly user input issue, rather than a developer bug
                DataAccess.Tools.LogWarning(exc);
                return ParseResult<List<MismoError>>.Failure(xml);
            }
        }

        /// <summary>
        /// Sets up the reader to parse the XML.
        /// </summary>
        /// <param name="xml">The xml to parse.</param>
        /// <returns>The set up reader.</returns>
        private static XmlReader SetUpReader(string xml)
        {
            // 7/12/2013 dd - Use XmlReaderSettings so XmlReader will not choke up when parsing the DOCTYPE
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.XmlResolver = null;

            return XmlReader.Create(new System.IO.StringReader(xml), settings);
        }
    }
}
