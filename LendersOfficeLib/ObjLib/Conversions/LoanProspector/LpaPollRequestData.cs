﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using LendersOffice.ObjLib.Conversions.Aus;
    using Security;

    /// <summary>
    /// Request data class for polling requests.
    /// </summary>
    public class LpaPollRequestData : LpaRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaPollRequestData"/> class.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="sellerNumber">The seller number.</param>
        /// <param name="sellerPassword">The seller password.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="asyncId">The asynchronous id returned in the partial response.</param>
        /// <param name="pollingAttemptCount">The number of times we've tried to poll.</param>
        /// <param name="publicJobId">The public job id of the background job.</param>
        /// <param name="reportType">The report type if the initial request was a submission request.</param>
        public LpaPollRequestData(string username, string password, string sellerNumber, string sellerPassword, Guid loanId, AbstractUserPrincipal principal, string asyncId, int pollingAttemptCount, Guid publicJobId, CreditReportType? reportType)
            : base(username, password, sellerNumber, sellerPassword, loanId, principal)
        {
            this.AsyncId = asyncId;
            this.PollingAttemptCount = pollingAttemptCount;
            this.PublicJobId = publicJobId;
            this.CreditReportType = reportType;
        }

        /// <summary>
        /// Gets the request type.
        /// </summary>
        /// <value>The request type.</value>
        public override LpaRequestType RequestType
        {
            get
            {
                return LpaRequestType.Poll;
            }
        }

        /// <summary>
        /// Gets the async id.
        /// </summary>
        /// <value>The async id.</value>
        public string AsyncId
        {
            get;
        }

        /// <summary>
        /// Gets the number of times we've already tried to poll for results.
        /// </summary>
        /// <value>The number of times we've already tried to poll for results.</value>
        public int PollingAttemptCount
        {
            get;
        }

        /// <summary>
        /// Gets the public job id for the background polling job.
        /// </summary>
        /// <value>The public job id for the background polling job.</value>
        public Guid PublicJobId
        {
            get;
        }

        /// <summary>
        /// Gets the credit report type for the initial request.
        /// </summary>
        public CreditReportType? CreditReportType
        {
            get;
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">Any errors encountered.</param>
        /// <returns>True if valid. False otherwise.</returns>
        public override bool Validate(out string error)
        {
            if (!base.Validate(out error))
            {
                return false;
            }

            if (this.RequestType == LpaRequestType.Poll && string.IsNullOrEmpty(this.AsyncId))
            {
                error = "Invalid async id.";
                return false;
            }

            return true;
        }
    }
}
