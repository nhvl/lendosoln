﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using ConfigSystem.Operations;
    using DataAccess;
    using Integration.Underwriting;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LoanProspectorResponse;
    using ObjLib.BackgroundJobs;

    /// <summary>
    /// Request handler for all LPA sys-to-sys requests.
    /// </summary>
    public class LpaRequestHandler
    {
        /// <summary>
        /// A generic message for displaying when LPA returns an error.
        /// </summary>
        private const string GenericLpaErrorMessage = "LPA returned an error.";

        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public LpaRequestHandler(LpaRequestData requestData)
        {
            this.RequestData = requestData;
        }

        /// <summary>
        /// Gets the request data.
        /// </summary>
        /// <value>The request data.</value>
        protected LpaRequestData RequestData
        {
            get;
        }

        /// <summary>
        /// Submits the request based on the data pass in the request data object.
        /// </summary>
        /// <returns>The result wrapper object.</returns>
        public LpaRequestResult SubmitRequest()
        {
            if (this.RequestData.RequestType != LpaRequestType.Transfer)
            {
                // We're timing this since, at least on release on locals, this is taking up a large amount of time due to changes to workflow.
                // On Prod, this shouldn't be a problem but we'll put timings on it anyways.
                Stopwatch sw = new Stopwatch();
                sw.Start();

                var workflowResults = Tools.IsWorkflowOperationAuthorized(this.RequestData.Principal, this.RequestData.LoanId, WorkflowOperations.RunLp);
                if (!workflowResults.Item1)
                {
                    return new LpaRequestErrorResult(new List<string>() { workflowResults.Item2 });
                }

                sw.Stop();
                Tools.LogInfo("LpaRequestHandler", $"Check workflow operation authorization ran in {sw.ElapsedMilliseconds.ToString()} ms.");
            }

            if (this.RequestData is LpaPollRequestData)
            {
                var pollData = this.RequestData as LpaPollRequestData;
                if (pollData.PollingAttemptCount >= Constants.ConstStage.SeamlessLpaPollingRetryLimit)
                {
                    return new LpaRequestErrorResult(new List<string>() { "Unable to retrieve results due to timeout." });
                }
            }

            LoanProspectorExporter exporter = LoanProspectorExporter.CreateExporterFromRequestData(this.RequestData);
            var requestPayload = exporter.Export();
            exporter.LogPayload();

            LoanProspectorServer server = new LoanProspectorServer(this.RequestData.Username, this.RequestData.Password);
            var responsePayload = server.PlaceRequest(requestPayload);
            if (server.GetErrors().Count != 0)
            {
                return new LpaRequestErrorResult(server.GetErrors());
            }

            var parsedResponse = LoanProspectorResponseParser.Parse(responsePayload);
            LoanProspectorResponseParser.LogParsedResult(parsedResponse);

            if (!parsedResponse.IsSuccessful)
            {
                return new LpaRequestErrorResult(new List<string>() { "Unable to parse response." }, parsedResponse);
            }

            if (this.RequestData.RequestType != LpaRequestType.Transfer &&
                parsedResponse.ValueOrDefault.LoanFeedback.ReadFromXml && 
                parsedResponse.ValueOrDefault.Status.Processing == E_MismoStatusProcessing.Completed)
            {
                // When we get a completed order with a LoanFeedback container, we want to add an entry to the AUS dashboard.
                AusResultHandler.CreateSystemLpOrder(
                    parsedResponse.ValueOrDefault?.LoanApplication?.ExtensionLoanApplication?.LoanIdentifierData.LoanProspectorLoanIdentifier ?? parsedResponse.ValueOrDefault?.LoanFeedback?.FreLoanProspectorTransactionIdentifier,
                    this.RequestData.LoanId,
                    parsedResponse.ValueOrDefault.LoanFeedback.FrePurchaseEligibilityDescription,
                    parsedResponse.ValueOrDefault.LoanFeedback.LoanProspectorCreditRiskClassificationDescription,
                    parsedResponse.ValueOrDefault.LoanFeedback.LoanProspectorEvaluationStatusDescription,
                    parsedResponse.ValueOrDefault.LoanFeedback.FreCreditScoreLtvFeeLevelDescription,
                    this.RequestData.Principal);
            }

            return this.ConstructResults(parsedResponse);
        }

        /// <summary>
        /// Constructs the results wrapper from the parsed response.
        /// </summary>
        /// <param name="response">The parsed response.</param>
        /// <returns>The results wrapper.</returns>
        private LpaRequestResult ConstructResults(ParseResult<MismoServiceOrderResponse> response)
        {
            if (response.ValueOrDefault.Status.Processing == E_MismoStatusProcessing.Completed)
            {
                CreditReportType? reportType = null;
                if (this.RequestData is LpaSubmitRequestData)
                {
                    reportType = (this.RequestData as LpaSubmitRequestData)?.CraInfo?.CreditReportOption;
                }
                else if (this.RequestData is LpaPollRequestData)
                {
                    reportType = (this.RequestData as LpaPollRequestData)?.CreditReportType;
                }

                return new LpaRequestCompleteResult(response, reportType);
            }
            else if (response.ValueOrDefault.Status.Processing == E_MismoStatusProcessing.Partial)
            {
                int pollingInterval;
                if (!int.TryParse(response.ValueOrDefault.PollingStatus.PollingInterval, out pollingInterval))
                {
                    return new LpaRequestErrorResult(new List<string>() { "Invalid polling interval." }, response);
                }

                var asyncId = response.ValueOrDefault.PollingStatus.LoanProspectorAsynchronousIdentifier;

                Guid publicJobId;
                if (!(this.RequestData is LpaPollRequestData))
                {
                    publicJobId = SeamlessLpaPollJob.AddSeamlessLpaPollJob(asyncId, pollingInterval, this.RequestData);
                }
                else
                {
                    publicJobId = ((LpaPollRequestData)this.RequestData).PublicJobId;
                }

                return new LpaRequestPartialResult(asyncId, publicJobId, pollingInterval, response);
            }
            else
            {
                var errors = this.ConstructErrorMessages(response);
                return new LpaRequestErrorResult(errors, response);
            }
        }

        /// <summary>
        /// Constructs the error messages for error responses.
        /// </summary>
        /// <param name="response">The error response.</param>
        /// <returns>The list of errors in the response.</returns>
        private List<string> ConstructErrorMessages(ParseResult<MismoServiceOrderResponse> response)
        {
            List<string> errors = new List<string>();
            foreach (MismoErrorInformation errorInformation in response.ValueOrDefault.Status.ErrorInformation)
            {
                if (errorInformation.ErrorCategory == E_MismoErrorInformationErrorCategory.Processing)
                {
                    errors.Add(errorInformation.ErrorDescription);
                }
                else
                {
                    errors.Add(GenericLpaErrorMessage);
                }
            }

            if (this.RequestData.RequestType != LpaRequestType.Poll && this.RequestData.RequestType != LpaRequestType.Transfer)
            {
                // In initial submission cases, error response can have ERROR containers scattered throughout. 
                // It's hard to find all of them using the data classes, so we'll parse again and specifically look for the ERROR elements.
                var errorContainers = LoanProspectorResponseParser.ParseForError(response.Input);
                if (errorContainers.IsSuccessful)
                {
                    foreach (var errorContainer in errorContainers.ValueOrDefault)
                    {
                        errors.Add($"{errorContainer.Type}:{errorContainer.Code}; \"{errorContainer.Message}\"");
                    }
                }
            }

            return errors;
        }
    }
}
