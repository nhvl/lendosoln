﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System.Collections.Generic;
    using LoanProspectorResponse;

    /// <summary>
    /// Wrapper class for LPA error results.
    /// </summary>
    public class LpaRequestErrorResult : LpaRequestResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestErrorResult"/> class.
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <param name="response">The response.</param>
        public LpaRequestErrorResult(IEnumerable<string> errors, ParseResult<MismoServiceOrderResponse> response = null)
            : base(response)
        {
            this.Errors = errors;
        }

        /// <summary>
        /// Gets the status of the response.
        /// </summary>
        /// <value>Thes status of the response.</value>
        public override LpaRequestResultStatus Status
        {
            get
            {
                return LpaRequestResultStatus.Error;
            }
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>The errors.</value>
        public IEnumerable<string> Errors
        {
            get;
        }
    }
}
