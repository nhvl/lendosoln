﻿namespace LendersOffice.Conversions.LoanProspector
{
    using LoanProspectorResponse;

    /// <summary>
    /// Wrapper for LPA response.
    /// </summary>
    public abstract class LpaRequestResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestResult"/> class.
        /// </summary>
        /// <param name="response">The response from Freddie.</param>
        protected LpaRequestResult(ParseResult<MismoServiceOrderResponse> response)
        {
            this.Response = response;
        }

        /// <summary>
        /// Gets the status of the response.
        /// </summary>
        /// <value>The status of the response.</value>
        public abstract LpaRequestResultStatus Status
        {
            get;
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <value>The response.</value>
        public ParseResult<MismoServiceOrderResponse> Response
        {
            get;
        }
    }
}
