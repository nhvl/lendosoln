<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:param name="VirtualRoot" />
	<xsl:template match="SERVICE_ORDER_RESPONSE">
		<html>
			<head>
				<meta charset="utf-8"></meta>
				<meta name="viewport" content="width=device-width, initial-scale=1"></meta>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
				<link rel="stylesheet" href="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/bootstrap-3.3.5/css/bootstrap.css')}"></link>
				<link rel="stylesheet" type="text/css" href="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/css/D3style.css')}"></link>
				<link rel="stylesheet" type="text/css" href="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/css/style.css')}"></link>
				<link rel="stylesheet" type="text/css" href="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/css/feedback.css')}"></link>
			</head>
			<body class="body-stylesheet bg-greybg">
				<!-- Main Header -->
				<!-- Fixed navbar -->
				<nav class="main-nav col-12" style="margin-left:0px; position:fixed">
					<ul class="main-nav-list">
						<li>
							<a href="#">
								<img src="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/img/ic-app-lpa-inv.svg')}" />loan product advisor<span class="servicemark"></span>
							</a>
						</li>

					</ul>
				</nav>
				<!-- End of Header -->
				<div class="container bg-greybg">
					<div class="row" style="">
						<div class="col-12">
							<p class="floating-header">Errors</p>
							<div class="panel panel-default " style="margin-bottom:0px">
								<div class="bg-textDarkBlue panel-heading  panel-header-stripe"></div>
								<div class="panel-body panel-padding">
									<p class="header-contained top-header">
										<xsl:choose>
											<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator  = 'Y'">
												<span class="feedbackH1Bold">FHA TOTAL Scorecard</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="feedbackH1Bold">Loan Product Advisor</span>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:choose>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Incomplete'">
												<span class="feedbackH1Bold"> Incomplete Messages</span>
											</xsl:when>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Invalid'">
												<span class="feedbackH1Bold"> Invalid Messages</span>
											</xsl:when>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Ineligible'">
												<span class="feedbackH1Bold"> Ineligible Messages</span>
											</xsl:when>
										</xsl:choose>
										<xsl:if test="STATUS/ERROR_INFORMATION/@ErrorCategory='Processing'">
											<span class="feedbackH1Bold"> Processing Error Messages</span>
										</xsl:if>
									</p>
									<hr></hr>
									<div class="row" style="text-align:center; ">
										<xsl:choose>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Ineligible'">
												<xsl:for-each
													select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE">
													<xsl:if test="@MessageTag = 'IneligibleMessage'">
														<p class="alert-text" style="font-size:20px">
															&#160;
															<xsl:value-of select="@MessageText" />
															&#160;&#160;(<xsl:value-of select="@MessageCode" />)
														</p>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Invalid'">
												<xsl:for-each
													select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE">
													<xsl:if test="@MessageTag='InvalidMessage'">
														<p class="alert-text">
															<xsl:value-of select="@MessageText" />
															&#160;(<xsl:value-of select="@MessageCode" />)
														</p>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Incomplete'">
												<xsl:for-each
													select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE">
													<xsl:if test="@MessageTag = 'IncompleteMessage'">
														<xsl:if test="@MessageCode = 'GA'">
															<p class="alert-text">
																&#160;
																<xsl:value-of select="@MessageText" />
																&#160;(<xsl:value-of select="@MessageCode" />)
															</p>
														</xsl:if>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
										</xsl:choose>
										<xsl:choose>
											<xsl:when
												test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Incomplete'">
												<xsl:for-each
													select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE">
													<xsl:if test="@MessageTag = 'IncompleteMessage'">
														<xsl:if test="@MessageCode != 'GA'">
															<p class="alert-text">
																&#160;
																<xsl:value-of select="@MessageText" />
																&#160;(<xsl:value-of select="@MessageCode" />)
															</p>
														</xsl:if>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
										</xsl:choose>
										<xsl:choose>
											<xsl:when
												test="STATUS/ERROR_INFORMATION/@ErrorCategory='Processing'">
												<p class="body-navy">
													<xsl:value-of select="STATUS/ERROR_INFORMATION/@ErrorDescription" />
												</p>
											</xsl:when>
											<xsl:otherwise>
												<p class="body-navy">
													Self-help tools and resources for this error can be found here:
													<br></br>
													<a
														href="http://www.freddiemac.com/help/lpa/index.htm#determine_error_types.htm"
														target="_blank">http://www.freddiemac.com/help/lpa/index.htm#determine_error_types.htm</a>.
												</p>
												<p class="body-navy">
													For live agent assistance, please click the following link
													for our Customer Service Center's contact information and
													hours of operation:
													<a href="https://las.freddiemac.com/passch/contactus.html"
														target="_blank">https://las.freddiemac.com/passch/contactus.html</a>.
												</p>
											</xsl:otherwise>
										</xsl:choose>
										<hr></hr>
										<div class="row row_middle center">
											<div class="col-4">
												<p class="h4-label">Borrower Name:</p>
												<p class="body-navy">
													<xsl:choose>
														<xsl:when
															test="STATUS/ERROR_INFORMATION/@ErrorCategory='Processing' or LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription='Invalid' or LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription='Ineligible' or LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription='Incomplete'">
															<xsl:value-of select="LOAN_APPLICATION/BORROWER/@_FirstName" />
															&#160;
															<xsl:value-of select="LOAN_APPLICATION/BORROWER/@_LastName" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="LOAN_FEEDBACK/@ScoredBorrowerName" />
														</xsl:otherwise>
													</xsl:choose>
												</p>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Loan Application Number:</p>
												<p class="body-navy">
													<xsl:choose>
														<xsl:when
															test="STATUS/ERROR_INFORMATION/@ErrorCategory='Processing'">
															<xsl:value-of
																select="LOAN_APPLICATION/MORTGAGE_TERMS/@LenderCaseIdentifier" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="LOAN_FEEDBACK/@LenderCaseIdentifier" />
														</xsl:otherwise>
													</xsl:choose>
												</p>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">LP AUS Key:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
										</div><!-- /row -->
										<div class="row row_bot center">
											<div class="col-4">
												<p class="h4-label">AUS Transaction Number:</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Loan Prospector ID:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Transaction ID:</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
										</div><!-- /row -->
									</div><!-- /row -->
								</div><!--/panel-body -->
							</div><!--/panel -->
						</div><!-- /col-12 -->
					</div><!-- /row -->
				</div><!--/container -->
				<footer class="footer row" style="overflow: auto; margin-bottom:0 !important">
					<section class="col-2">
						<img src="{concat($VirtualRoot, '/pml_shared/FreddieMac/lib/img/lgo-freddiemac-inv.svg')}"></img>
					</section>
					<section class="col-4 pull-right" style="height: 40.5px;">
						<div>
							<ul>
								<li id="footer_year"
									style="font-size:11px;margin-top:15px;color:rgba(170, 183, 194, 0.5) !important;">
									&#169;
									<xsl:value-of select="substring(@_Date,7,4)"></xsl:value-of>
									Freddie Mac
								</li>
							</ul>
						</div>
						<ul>
							<li>&#032;
							</li>
						</ul>
					</section>
				</footer>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
<!-- end -->