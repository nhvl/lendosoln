<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:param name="VirtualRoot" ></xsl:param>
  <xsl:template name="printimage">

    <xsl:variable name="string1" select="substring-after(//MERGED_CREDIT_REPORT,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string2" select="substring-before($string1,']]&gt;&lt;/DOCUMENT&gt;')"/>
    <xsl:variable name="string3" select="substring-after($string1,$string2)"/>
    <xsl:variable name="string4" select="substring-after($string3,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string5" select="substring-before($string4,']]&gt;&lt;/DOCUMENT&gt;')"/>


    <xsl:variable name="string6" select="substring-after($string1,$string5)"/>
    <xsl:variable name="string7" select="substring-after($string6,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string8" select="substring-before($string7,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string9" select="substring-after($string6,$string8)"/>
    <xsl:variable name="string10" select="substring-after($string9,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string11" select="substring-before($string10,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string12" select="substring-after($string9,$string11)"/>
    <xsl:variable name="string13" select="substring-after($string12,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string14" select="substring-before($string13,']]&gt;&lt;/DOCUMENT&gt;')"/>


    <xsl:variable name="string15" select="substring-after($string12,$string14)"/>
    <xsl:variable name="string16" select="substring-after($string15,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string17" select="substring-before($string16,']]&gt;&lt;/DOCUMENT&gt;')"/>



    <xsl:variable name="string18" select="substring-after($string15,$string17)"/>
    <xsl:variable name="string19" select="substring-after($string18,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string20" select="substring-before($string19,']]&gt;&lt;/DOCUMENT&gt;')"/>


    <xsl:variable name="string21" select="substring-after($string18,$string20)"/>
    <xsl:variable name="string22" select="substring-after($string21,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string23" select="substring-before($string22,']]&gt;&lt;/DOCUMENT&gt;')"/>


    <xsl:variable name="string24" select="substring-after($string21,$string23)"/>
    <xsl:variable name="string25" select="substring-after($string24,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string26" select="substring-before($string25,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string27" select="substring-after($string24,$string26)"/>
    <xsl:variable name="string28" select="substring-after($string27,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string29" select="substring-before($string28,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string30" select="substring-after($string27,$string29)"/>
    <xsl:variable name="string31" select="substring-after($string30,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string32" select="substring-before($string31,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string33" select="substring-after($string30,$string32)"/>
    <xsl:variable name="string34" select="substring-after($string33,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string35" select="substring-before($string34,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string36" select="substring-after($string33,$string35)"/>
    <xsl:variable name="string37" select="substring-after($string36,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string38" select="substring-before($string37,']]&gt;&lt;/DOCUMENT&gt;')"/>

    <xsl:variable name="string39" select="substring-after($string36,$string38)"/>
    <xsl:variable name="string40" select="substring-after($string39,'&lt;DOCUMENT&gt;&lt;![CDATA[')"/>
    <xsl:variable name="string41" select="substring-before($string40,']]&gt;&lt;/DOCUMENT&gt;')"/>
    <xsl:choose>


      <xsl:when test="//MERGED_CREDIT_REPORT" >

        <xsl:value-of select="concat($string2,$string5,$string8,$string11,$string14,$string17,$string20,$string23,$string26,$string29,$string32,$string35,$string38,$string41)"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>


  <xsl:template match="SERVICE_ORDER_RESPONSE">

    <html>
      <head>
        <meta charset="utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>

        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/jquery-2.1.4.min.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/js/bootstrap.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/d3.v3.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/radialProgress.js</xsl:attribute>
        </script>
        
        <script type="text/javascript">
          <xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/js/liquidFillGuage.js</xsl:attribute>
        </script>

        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/css/bootstrap.css</xsl:attribute>
        </link>

        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/D3style.css</xsl:attribute>
        </link>
        
        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/style.css</xsl:attribute>
        </link>
        
        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/feedback.css</xsl:attribute>
        </link>

      </head>
      <body class="body-stylesheet bg-greybg">

        <!-- Main Header -->
        <nav class="main-nav col-12" style="margin-left:0px; position:fixed">
          <ul class="main-nav-list">
            <li>
              <a href="#">
                <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/ic-app-lpa-inv.svg</xsl:attribute></img>loan product advisor<span class="servicemark"></span>
              </a>
            </li>

          </ul>
        </nav>


        <!-- End of Header -->
        <!-- Floating Utility-->
        <ul class="utility-list utility-list-custom-right" data-spy="affix">

          <li>
            <i class="ic-print" onclick="javascript:print()"></i>
          </li>

        </ul>


        <!-- End of Floating Utility-->
        <div class="container bg-greybg" >
          <div class="row" style="">
            <div class="col-1" style="height:500px"></div>
            <!-- /col-1-->
            <div class="col-10">
              <p class="floating-header">Merged Credit Report</p>
              <div  class="panel panel-default "  >
                <div class="bg-textDarkBlue panel-heading  panel-header-stripe" ></div>
                <div class="panel-body panel-padding">

                  <div class="alert alert-danger alert-dismissible" role="alert">

                    <p class="center">***** Please retain the merged credit reference number(s) for future submissions.***** </p>
                  </div>

                  <p class="header-contained top-header">Loan Information:</p>
                  <hr></hr>
                  <div class="row">
                    <div class="col-4">
                      <p class="h4-label">LP AUS Key:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorKeyIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <div class="col-4">
                      <p class="h4-label">Loan Prospector ID:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorLoanIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <div class="col-4">
                      <p class="h4-label">AUS Transaction Number:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-4">
                      <p class="h4-label">Transaction ID:</p>
                      <xsl:choose>
                        <xsl:when test = "LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier">
                          <p class="body-navy">
                            <xsl:value-of select="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier"/>
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p class="na-gray">N/A</p>
                        </xsl:otherwise>
                      </xsl:choose>

                    </div>
                    <div class="col-4">

                    </div>
                    <div class="col-4">

                    </div>
                  </div>
                  <hr></hr>
                  <p class="header-contained">Report:</p>


                  <PRE>
                    <xsl:choose>
                      <xsl:when test = "substring-after(//MERGED_CREDIT_REPORT,'&lt;DOCUMENT&gt;&lt;![CDATA[') = ''">
                        <xsl:value-of select="LOAN_FEEDBACK/MERGED_CREDIT_REPORT"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:for-each select="//MERGED_CREDIT_REPORT">
                          <xsl:call-template name="printimage"/>
                        </xsl:for-each>
                      </xsl:otherwise>
                    </xsl:choose>
                  </PRE>

                </div>
                <!-- /panel-body-->
              </div>
              <!-- /panel-->

            </div>
            <!-- col-10-->
            <div class="col-1" style="height:500px"></div>
            <!-- /col-1-->
          </div>
          <!-- /row-->
        </div>
        <!-- /container-->
        <footer class="footer row" style="overflow: auto; margin-bottom:0 !important">
          <section class="col-2">
            <img><xsl:attribute name="src"><xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/lgo-freddiemac-inv.svg</xsl:attribute></img>
          </section>
          <section class="col-4 pull-right" style="height: 40.5px;">
            <div>
              <ul>
                <li id="footer_year" style="font-size:11px;margin-top:15px;color:rgba(170, 183, 194, 0.5) !important;">&#169; Freddie Mac</li>
              </ul>
            </div>
            <ul>
              <li>&#032;</li>
            </ul>
          </section>
        </footer>
        <script>
          var date_year = "<xsl:value-of select="@_Date"/>";
          date_year = date_year.split("-");
          document.getElementById("footer_year").innerHTML = "&#169;"+date_year[2]+" Freddie Mac";
        </script>

      </body>
    </html>
    <xsl:comment>
      This page uses third party software.
      For license information, please see
      https://secure.lendingqb.com/pml_shared/FreddieMac/ThirdPartyLicenses.txt
    </xsl:comment>
  </xsl:template>
</xsl:stylesheet>