﻿namespace LendersOffice.Conversions.LoanProspector
{
    using LendersOffice.ObjLib.Conversions.Aus;
    using LoanProspectorResponse;

    /// <summary>
    /// Wrapper for LPA completed response.
    /// </summary>
    public class LpaRequestCompleteResult : LpaRequestResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestCompleteResult"/> class.
        /// </summary>
        /// <param name="response">The parsed response.</param>
        /// <param name="reportType">The report type if the initial request was a submission request.</param>
        public LpaRequestCompleteResult(ParseResult<MismoServiceOrderResponse> response, CreditReportType? reportType)
            : base(response)
        {
            this.CreditReportType = reportType;
        }

        /// <summary>
        /// Gets the status of the response.
        /// </summary>
        /// <value>The status of the response.</value>
        public override LpaRequestResultStatus Status
        {
            get
            {
                return LpaRequestResultStatus.Completed;
            }
        }

        /// <summary>
        /// Gets the credit report type if the request was a submit request.
        /// </summary>
        [Newtonsoft.Json.JsonProperty]
        public CreditReportType? CreditReportType
        {
            get;
            private set;
        }
    }
}
