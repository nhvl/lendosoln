﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Generic;
    using Admin;
    using LendersOffice.Security;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "This code was copied from LoanProspectorExporter to achieve better separation of legacy classes.")]
    public class LoanProspectorCreditReportingCompanyMapping
    {
        //// ir - iOPM 190789: Refer to \\megatron\LendersOffice\Integration\Freddie Mac\CRA ULDD MAP 8-27-2014.xlsx
        private static Dictionary<Guid, string> lpCrcCodeForUlddExportDictionary = new Dictionary<Guid, string>()
        {
            { new Guid("74DCF4FF-FB64-49A9-8088-E0A7365185B1"), "5025" }, // 1 Source Data
            { new Guid("9A927536-ED89-4F54-AAB3-3B429703F677"), "5031" }, // ACRAnet
            { new Guid("E87AD0C4-8939-4130-8F1A-B4E353CCB9BE"), "5031" }, // ACRAnet
            { new Guid("BB271325-A79A-446E-B419-7EF5AFF3B399"), "5031" }, // ACRAnet
            { new Guid("ED0B86E2-E2FF-46FF-8D8C-8945B258A497"), "5031" }, // ACRAnet
            { new Guid("003D94DE-BF5E-4A09-B69F-CBC800314740"), "5031" }, // ACRAnet
            { new Guid("260AA6DB-B4DF-4C7B-BCE1-29AC58BC3236"), "5031" }, // ACRAnet
            { new Guid("4D1CCE98-C1CB-4748-A6D6-7E821A8A6866"), "5031" }, // ACRAnet
            { new Guid("E0B2BA82-9347-4EBA-82D6-7EC04D3B4A52"), "5031" }, // ACRAnet
            { new Guid("C027BBE9-A099-4BFC-AC12-E69919FA6276"), "5031" }, // ACRAnet
            { new Guid("4235E681-5ACE-4BAB-AE66-2DEC53E8D2A8"), "5031" }, // ACRAnet
            { new Guid("6C030BA4-CE63-4DBD-A2A6-02CB04E6B04C"), "5031" }, // ACRAnet
            { new Guid("EB228885-B484-404A-99FF-B28511DD3E38"), "5226" }, // Advantage Credit Inc. 
            { new Guid("FD0081A0-0384-4B06-B6B6-08E091A115C5"), "5138" }, // Advantage Plus Credit Reporting, Inc. 
            { new Guid("23C60165-1DA3-4CFB-A33D-31CCDEEA54B6"), "5275" }, // Alliance 2020, Inc. 
            { new Guid("3C5E0CA9-1981-4AFB-8988-EA0AA9E24712"), "5016" }, // American Reporting Company, LLC
            { new Guid("9B05738C-5718-4D2A-A097-41A611755797"), "5016" }, // American Reporting Company, LLC
            { new Guid("AA41ABFD-8B64-4504-83CD-CFAF8D16710E"), "5091" }, // Avantus LLC
            { new Guid("C9945176-FEF9-4F0F-8138-C58169FEE228"), "5091" }, // Avantus LLC
            { new Guid("855F23F4-B45F-4CDC-B699-3C5946396605"), "5076" }, // Birchwood Credit Services, Inc. 
            { new Guid("DA8759E4-1607-422F-8838-B3282FE4FF35"), "1000" }, // CBCInnovis
            { new Guid("52D2DED3-7025-43FB-89C1-36593FA9B303"), "5071" }, // Certified Credit Reporting via Meridian Link
            { new Guid("FEA25F7C-3E56-40F3-A5AF-809505A5AE83"), "5128" }, // CIC Credit
            { new Guid("39BB11F0-1E2A-4B03-B444-F811ADE65D62"), "5022" }, // CIS, Inc. 
            { new Guid("69AC6CBC-235F-4E8B-A613-DE7A95391DAD"), "5086" }, // Cisco
            { new Guid("44BD6469-4330-4891-87C2-9AB82787D6B6"), "8000" }, // Corelogic CREDCO / Credstar
            { new Guid("F27CC198-78F9-4059-9B06-D6C94318C803"), "8000" }, // Corelogic CREDCO / Credstar
            { new Guid("58F3794D-C2E7-4B29-97C9-154DD1BA7A46"), "5084" }, // Credit Communications, Inc. 
            { new Guid("C7EE2B84-783F-4207-9981-08A43BE05598"), "5298" }, // Credit Information Systems
            { new Guid("E849190A-823E-4548-9963-6F25303BC2C0"), "5024" }, // Credit Link LLC
            { new Guid("4874D304-FF41-4D10-8AD7-9555785085B7"), "5017" }, // Credit Plus, Inc. MD
            { new Guid("03DD85E6-A885-4FE0-9EA2-E3BEADD10C5E"), "5166" }, // Credit Service Company
            { new Guid("F384B8C7-CCC6-4DC8-B270-36F9AA7DC648"), "5181" }, // Credit Technologies, Inc.
            { new Guid("1B5EBBB3-9B89-48AB-A82F-9CF9C3F5CFD1"), "5161" }, // CTI - credit Technology, Inc. 
            { new Guid("12B32351-B891-4E34-85BE-EFA361A53BBF"), "5224" }, // Credit Verifiers
            { new Guid("41B7BD42-F346-4E4D-A3FD-97D85F2D12A1"), "8000" }, // Corelogic CREDCO / Credstar
            { new Guid("7E7AF93E-EEC6-4BA2-83E4-BEED5E3F311F"), "8000" }, // Corelogic CREDCO / Credstar
            { new Guid("DE5C07DA-43E7-4687-87F0-4A43727C1A3A"), "5007" }, // CSC Mortgage Services
            { new Guid("37E358E5-45C6-4EC6-B6A9-8F9E1D26AEEF"), "5306" }, // Data Facts Inc.
            { new Guid("8775A1F2-682E-42FC-A835-6B29A718535E"), "5311" }, // DataQuick
            { new Guid("4F974B71-90BD-499F-9757-53BB19DC0DA8"), "5311" }, // DataQuick
            { new Guid("3ADAB50C-FE58-47C2-B76A-269599307FB7"), "5062" }, // Equidata
            { new Guid("67DED324-E3C8-4391-AFAE-9D72280C14D6"), "5062" }, // Equidata
            { new Guid("AE23B898-B527-4EBE-B139-830D11D6B4BD"), "5000" }, // Equifax Mortgage Solutions
            { new Guid("740EE93D-7FE4-47E3-847E-FD57A7417570"), "5211" }, // Financial Data Reports
            { new Guid("E9FA1733-EF79-4C4E-81BA-05480CB90937"), "5130" }, // Funding Suite
            { new Guid("108C37DC-950C-4C84-8AD6-955B0EFFB67D"), "5058" }, // Information Searching Company
            { new Guid("7C75A248-9932-430A-8BCE-1471279A5FF8"), "5308" }, // Informative Research
            { new Guid("5972F865-81B7-4427-AE40-05E9C8F7E813"), "5308" }, // Informative Research
            { new Guid("DA66612F-D574-408A-9785-6EAFBF1A57DD"), "6000" }, // Kroll Factual Data Corp.
            { new Guid("D772E794-F627-47F9-9CA1-B26767729D08"), "6000" }, // Kroll Factual Data Corp.
            { new Guid("937E8D90-CEDB-4B46-BFC3-0A884B4E7741"), "5010" }, // MCR of America
            { new Guid("116858B5-6863-4857-93F2-65326697BFD6"), "5078" }, // Merchangs Credit Bureau/MCB
            { new Guid("2D488F6D-E698-4E17-9E67-56DD26D3937F"), "5078" }, // Merchangs Credit Bureau/MCB
            { new Guid("C6842CF2-A7CE-4E18-A770-8DA3DF72FFF0"), "5277" }, // MFI Credit Solutions
            { new Guid("FA253DF5-FCD8-4A5F-8613-C8706F7F1CE8"), "5029" }, // Midwest Mortgage Credit Services, Inc. 
            { new Guid("23EC185E-D112-4F29-BE4E-035C2CC58C82"), "5313" }, // NCO Credit Services via Sharper Lending
            { new Guid("E17E0A7B-7018-44AA-835F-943834760464"), "5251" }, // Old Republic Credit Services
            { new Guid("5590C0CF-4212-4027-A2CE-CD4DD35D4486"), "5040" }, // One Source Credit Reporting
            { new Guid("6EFA366C-CDBD-42CB-94FC-2DFE9CA3DC0A"), "5314" }, // Online Information Services, Inc.
            { new Guid("05903B7B-3711-4D8A-A08A-95BBF36011B5"), "5258" }, // Platinum Credit Services, Inc. 
            { new Guid("92C4E3C0-8B12-4161-B012-4B045E2428C3"), "5294" }, // Premium Credit Bureau/PCB1
            { new Guid("582D8F1D-C3CE-4191-B29D-880CC4C7B094"), "5294" }, // Premium Credit Bureau/PCB1
            { new Guid("4127ADB7-2A3A-46E4-9845-71BD87CB89BB"), "5073" }, // Rapid Credit Reports Inc.
            { new Guid("36AB1595-5FDD-4647-A2EF-1DB2C2AB54FB"), "5073" }, // Rapid Credit Reports Inc.
            { new Guid("A22DA6C6-7CC7-4645-B5A9-5358FA7D3BB6"), "5087" }, // Royal Mortgage Credit Reporting
            { new Guid("0AC80267-EA42-456E-B75E-F0CBB88790E6"), "5043" }, // SARMA
            { new Guid("BCC5F38A-F5D8-4510-A430-EA0FDB823DDA"), "5229" }, // Service 1st Information Systems
            { new Guid("0CD873B1-4661-4172-9AEF-7DFC06086263"), "5290" }, // Settlement One via MeridianLink
            { new Guid("EBCB031F-D7D3-4B55-8553-C3513E3A257C"), "5119" }, // Southwest Credit Services
            { new Guid("5208025D-50DE-4E5C-94CE-BD0143C2EAB2"), "5075" }, // Strategic Information resources, Inc. 
            { new Guid("DE4D9062-3C54-4E9C-BE52-BE026341068F"), "5225" }, // Total Credit Services
            { new Guid("6D8E98F2-6471-4295-8BA3-A805A3AB2254"), "5063" }, // United One Resource, Inc. 
            { new Guid("AD64E198-34A5-4929-9D5D-7F5B528F5707"), "5127" }, // Universal Credit Services via Meridian Link
            { new Guid("14668871-CC0A-443C-88D4-47DE1E58D1B0"), "5127" }  // Universal Credit Services via Meridian Link
        };

        public static bool TryGetLpCrcCodeForUlddExport(Guid craId, out string lpCrcCode)
        {
            return lpCrcCodeForUlddExportDictionary.TryGetValue(craId, out lpCrcCode);
        }

        public static string GetLpCrcCode(string lendersOfficeCraId)
        {
            if (lendersOfficeCraId == null)
            {
                return string.Empty;
            }

            string cra = string.Empty;
            lendersOfficeCraId = lendersOfficeCraId.ToLower();

            // CBC Through FNMA Connection
            // CBC Direct
            if (lendersOfficeCraId == "5eb349da-4a9b-4348-86e4-bc213945c936" || 
               lendersOfficeCraId == "da8759e4-1607-422f-8838-b3282fe4ff35") 
            {
                cra = "1"; // CBC Innovis
            }
            else if (lendersOfficeCraId == "ae23b898-b527-4ebe-b139-830d11d6b4bd")
            {
                cra = "5"; // Equifax Mortgage Services     
            }
            else if (lendersOfficeCraId == "da66612f-d574-408a-9785-6eafbf1a57dd" ||
                lendersOfficeCraId == "d772e794-f627-47f9-9ca1-b26767729d08")
            {
                cra = "6"; // Kroll Factual Data
            }
            else if (lendersOfficeCraId == "f27cc198-78f9-4059-9b06-d6c94318c803")
            {
                cra = "8"; // First American Credco
            }
            else if (lendersOfficeCraId == "7c6e1bdd-a963-48aa-8eca-2a4c0b355132")
            {
                cra = "A"; // LandAmerica Credit Info1
            }
            else if (lendersOfficeCraId == "07dc117a-f9e8-4ddf-8f65-9ed4ae9a7a60" ||
                lendersOfficeCraId == "5da6fa05-c7e2-4120-83d0-e37c4bd02568")
            {
                cra = "B"; // LandSafe
            }
            else if (lendersOfficeCraId == "58a078b5-839b-4153-bc49-c236683193b3")
            {
                cra = "4"; // FIS Credit Services
            }

            return cra;
        }

        public static Guid GetLendersOfficeCraId(string lpCrcCode)
        {
            Guid cra = Guid.Empty;
            if (lpCrcCode == "1")
            {
                // cra = new Guid("5eb349da-4a9b-4348-86e4-bc213945c936"); //  CBC Innovis -- See OPM 19664, we are no longer use CBC through FNMA connection
                cra = new Guid("da8759e4-1607-422f-8838-b3282fe4ff35"); // CBC Innovis Direct
            }
            else if (lpCrcCode == "5")
            {
                cra = new Guid("ae23b898-b527-4ebe-b139-830d11d6b4bd"); // Equifax Mortgage Services
            }
            else if (lpCrcCode == "6") 
            {
                // Kroll Factual Data
                AbstractUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
                BrokerDB broker = BrokerDB.RetrieveById(brokerUser.BrokerId);

                if (broker.IsAllowKrollFactualDataDirect)
                {
                    cra = new Guid("d772e794-f627-47f9-9ca1-b26767729d08");
                }
                else
                {
                    cra = new Guid("da66612f-d574-408a-9785-6eafbf1a57dd");
                }
            }
            else if (lpCrcCode == "8")
            {
                cra = new Guid("f27cc198-78f9-4059-9b06-d6c94318c803"); // First American Credco
            }
            else if (lpCrcCode == "A")
            {
                cra = new Guid("7c6e1bdd-a963-48aa-8eca-2a4c0b355132"); // LandAmerica Credit Info1
            }
            else if (lpCrcCode == "B")
            {
                AbstractUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
                BrokerDB broker = BrokerDB.RetrieveById(brokerUser.BrokerId);
                if (broker.IsAllowLandsafeDirect)
                {
                    cra = new Guid("5da6fa05-c7e2-4120-83d0-e37c4bd02568");
                }
                else
                {
                    cra = new Guid("07dc117a-f9e8-4ddf-8f65-9ed4ae9a7a60");
                }
            }
            else if (lpCrcCode == "4")
            {
                cra = new Guid("58a078b5-839b-4153-bc49-c236683193b3"); // FIS Credit Services
            }

            return cra;
        }
    }
}
