﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using LoanProspectorResponse;

    /// <summary>
    /// Wrapper for LPA partial response.
    /// </summary>
    public class LpaRequestPartialResult : LpaRequestResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaRequestPartialResult"/> class.
        /// </summary>
        /// <param name="asyncId">The asynchronous id used to retrieve results.</param>
        /// <param name="publicJobId">The public id of the background polling job.</param>
        /// <param name="pollingInterval">The polling interval.</param>
        /// <param name="response">The response.</param>
        public LpaRequestPartialResult(string asyncId, Guid publicJobId, int pollingInterval, ParseResult<MismoServiceOrderResponse> response)
            : base(response)
        {
            this.AsyncId = asyncId;
            this.PollingInterval = pollingInterval;
            this.PublicJobid = publicJobId;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
        public override LpaRequestResultStatus Status
        {
            get
            {
                return LpaRequestResultStatus.Partial;
            }
        }

        /// <summary>
        /// Gets the async id.
        /// </summary>
        /// <value>The async id.</value>
        public string AsyncId
        {
            get;
        }

        /// <summary>
        /// Gets the polling interval.
        /// </summary>
        /// <value>The polling interval.</value>
        public int PollingInterval
        {
            get;
        }

        /// <summary>
        /// Gets or sets the job id of the polling job queued up in the background job processor.
        /// </summary>
        /// <value>The job id of the polling job queued up in the background job processor.</value>
        public Guid PublicJobid
        {
            get;
            set;
        }
    }
}
