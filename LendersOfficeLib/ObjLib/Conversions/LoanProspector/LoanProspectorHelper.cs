﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.Conversions.Aus;
using LoanProspectorResponse;

namespace LendersOffice.Conversions.LoanProspector
{
    public static class LoanProspectorHelper
    {
        /// <summary>
        /// The DataVersionNumber for software version 4.8.00 for non-seamless LPA.
        /// </summary>
        public const string NonSeamless48DataVersionNumber = "WW4.8.00";

        /// <summary>
        /// The DataVersionNumber for software version 4.8.00 for seamless LPA.
        /// </summary>
        public const string Seamless48DataVersionNumber = "S4.8.00";

        /// <summary>
        /// VersionIdentifier to pass in for version 4.3 of the LPA DTD.
        /// </summary>
        public const string FreLoanProspectorVersionIdentifier43 = "4.3";

        /// <summary>
        /// Doctype for version 4.3 of the LPA DTD.
        /// </summary>
        public const string DocType43 = "17_LOANPROSPECTOR_REQUEST_4.3.DTD";

        public const string LpaRequestHeader = "=== LOAN PRODUCT ADVISOR REQUEST ===";

        public const string LpaResponseHeader = "=== LOAN PRODUCT ADVISOR RESPONSE ===";

        public const string LpaResponseCreditReportHeader = "=== LOAN PRODUCT ADVISOR CREDIT REPORT ===";

        /// <summary>
        /// Logs the payload.
        /// </summary>
        /// <param name="payload">The payload.</param>
        /// <param name="header">The header to use.</param>
        public static void LogPayload(string payload, string header)
        {
            Tools.LogInfo(header + Environment.NewLine + Environment.NewLine + payload ?? "[No payload]");
        }

        public static E_sProd3rdPartyUwResultT Parse3rdPartyUwResult(MismoLoanFeedback loanFeedback)
        {
            if (null == loanFeedback)
                return E_sProd3rdPartyUwResultT.NA;

            switch (loanFeedback.FrePurchaseEligibilityDescription.ToLower())
            {
                case "000freddiemaceligible":
                    switch (loanFeedback.LoanProspectorCreditRiskClassificationDescription.ToLower())
                    {
                        case "accept":
                            return E_sProd3rdPartyUwResultT.LP_AcceptEligible;
                        case "caution":
                            return E_sProd3rdPartyUwResultT.LP_CautionEligible;
                    }

                    break;
                case "000freddiemacineligible":
                    switch (loanFeedback.LoanProspectorCreditRiskClassificationDescription.ToLower())
                    {
                        case "accept":
                            return E_sProd3rdPartyUwResultT.LP_AcceptIneligible;
                        case "caution":
                            return E_sProd3rdPartyUwResultT.LP_CautionIneligible;
                    }
                    break;
                case "500freddiemaceligiblelpaminusoffering":
                    string level = loanFeedback.FreCreditScoreLtvFeeLevelDescription;
                    if (level == "" || level == "01")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level1;
                    }
                    else if (level == "02")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level2;
                    }
                    else if (level == "03")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level3;
                    }
                    else if (level == "04")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level4;
                    }
                    else if (level == "05")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level5;
                    }
                    else
                    {
                        // 7/23/2008 dd - Default to level 1.
                        Tools.LogBug("Unhandle FreCreditScoreLtvFeeLevelDescription=" + level + " in LoanProspectorImporter.");
                        return E_sProd3rdPartyUwResultT.Lp_AMinus_Level1;

                    }
                default:
                    if (loanFeedback.LoanProspectorCreditRiskClassificationDescription.ToLower() == "refer")
                    {
                        return E_sProd3rdPartyUwResultT.Lp_Refer;
                    }
                    break;
            }
            return E_sProd3rdPartyUwResultT.NA;
        }

        public static LpLoadedAssessmentSummaryData LoadAssessmentData(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return LpLoadedAssessmentSummaryData.Failure("No data to import.");
            }

            Tools.LogInfo("LoanProspectorHelper", "Loading Assessment Data: " + xml);
            var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);
            if (!parsedServiceOrderResponse.IsSuccessful)
            {
                return LpLoadedAssessmentSummaryData.Failure("An unexpected error occurred. Please contact your LendingQB system administrator.");
            }

            var serviceOrderResponse = parsedServiceOrderResponse.ValueOrDefault;
            string inputLoanId = serviceOrderResponse.LoanApplication.ExtensionLoanApplication.LoanIdentifierData.GloballyUniqueIdentifier;

            var loanId = inputLoanId.ToNullable<Guid>(Guid.TryParse);
            if (!loanId.HasValue)
            {
                return LpLoadedAssessmentSummaryData.Failure("Unable to determine import target loan.");
            }

            return LpLoadedAssessmentSummaryData.Successful(GetAssessmentSummaryData(serviceOrderResponse));
        }

        private static List<KeyValuePair<string, string>> GetAssessmentSummaryData(MismoServiceOrderResponse serviceOrderResponse)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("Loan Application Number", serviceOrderResponse?.LoanFeedback?.LenderCaseIdentifier));
            data.AddRange(serviceOrderResponse?.LoanApplication?.Borrower?.Cast<MismoBorrower>().Select(
                borrower => new KeyValuePair<string, string>("Borrower", Tools.ComposeFullName(borrower.FirstName, borrower.MiddleName, borrower.LastName, borrower.NameSuffix))));

            var assessmentDateString = serviceOrderResponse?.LoanFeedback?.LoanProspectorEvaluationDate;
            var assessmentDate = assessmentDateString.ToNullable<DateTime>(DateTime.TryParse);
            data.Add(new KeyValuePair<string, string>("Date/Time Assessed", assessmentDate.HasValue ? Tools.GetEasternTimeDescription(assessmentDate.Value) : assessmentDateString));
            data.Add(new KeyValuePair<string, string>("AUS Status", serviceOrderResponse?.LoanFeedback.LoanProspectorEvaluationStatusDescription));
            data.Add(new KeyValuePair<string, string>("Risk Class", serviceOrderResponse?.LoanFeedback.LoanProspectorCreditRiskClassificationDescription));
            data.Add(new KeyValuePair<string, string>("Documentation Level", serviceOrderResponse?.LoanFeedback.FreDocumentationLevelDescription));

            string lpAusKey = "LPA AUS Key";

            data.Add(new KeyValuePair<string, string>(lpAusKey, serviceOrderResponse?.LoanFeedback.LoanProspectorKeyIdentifier));

            var fredProcPoint = FromLoanProspectorFormat(serviceOrderResponse?.LoanApplication?.AdditionalCaseData?.TransmittalData?.CaseStateType);
            data.Add(new KeyValuePair<string, string>("Loan Processing Stage", fredProcPoint.HasValue ? Tools.sFredProcPointT_map[fredProcPoint.Value] : string.Empty));
            return data;
        }

        public static E_sFredProcPointT? FromLoanProspectorFormat(E_MismoTransmittalDataCaseStateType? caseStateType)
        {
            switch (caseStateType)
            {
                case E_MismoTransmittalDataCaseStateType.Application:
                    return E_sFredProcPointT.Application;
                case E_MismoTransmittalDataCaseStateType.FinalDisposition:
                    return E_sFredProcPointT.FinalDisposition;
                case E_MismoTransmittalDataCaseStateType.PostClosingQualityControl:
                    return E_sFredProcPointT.PostClosingQualityControl;
                case E_MismoTransmittalDataCaseStateType.Prequalification:
                    return E_sFredProcPointT.Prequalification;
                case E_MismoTransmittalDataCaseStateType.Underwriting:
                    return E_sFredProcPointT.Underwriting;
            }

            return null;
        }
    }
}
