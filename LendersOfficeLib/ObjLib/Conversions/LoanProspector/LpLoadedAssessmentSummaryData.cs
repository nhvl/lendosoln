﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents the assessment summary data received from LP to display before import.
    /// </summary>
    public class LpLoadedAssessmentSummaryData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpLoadedAssessmentSummaryData"/> class.
        /// </summary>
        /// <param name="isSuccessful">A value indicating whether the data was loaded successfully.</param>
        /// <param name="userErrorMessage">An error message to display to the user.</param>
        /// <param name="assessmentSummaryData">The assessment summary data to display to the user.</param>
        private LpLoadedAssessmentSummaryData(bool isSuccessful, string userErrorMessage, List<KeyValuePair<string, string>> assessmentSummaryData)
        {
            this.IsSuccessful = isSuccessful;
            this.UserErrorMessage = userErrorMessage ?? string.Empty;
            this.AssessmentSummaryData = assessmentSummaryData ?? new List<KeyValuePair<string, string>>();
        }

        /// <summary>
        /// Gets a value indicating whether the data was loaded successfully.
        /// </summary>
        /// <value>A value indicating whether the data was loaded successfully.</value>
        public bool IsSuccessful { get; }

        /// <summary>
        /// Gets an error message to display to the user.
        /// </summary>
        /// <value>An error message to display to the user.</value>
        public string UserErrorMessage { get; }

        /// <summary>
        /// Gets the assessment summary data to display to the user.
        /// </summary>
        /// <value>The assessment summary data to display to the user.</value>
        public List<KeyValuePair<string, string>> AssessmentSummaryData { get; }

        /// <summary>
        /// Creates an instance of <see cref="LpLoadedAssessmentSummaryData"/> representing a successful load.
        /// </summary>
        /// <param name="assessmentSummaryData">The assessment summary data to display to the user.</param>
        /// <returns>An instance of <see cref="LpLoadedAssessmentSummaryData"/> representing a successful load.</returns>
        public static LpLoadedAssessmentSummaryData Successful(List<KeyValuePair<string, string>> assessmentSummaryData)
        {
            return new LpLoadedAssessmentSummaryData(true, null, assessmentSummaryData);
        }

        /// <summary>
        /// Creates an instance of <see cref="LpLoadedAssessmentSummaryData"/> representing an unsuccessful load.
        /// </summary>
        /// <param name="userErrorMessage">An error message to display to the user.</param>
        /// <returns>An instance of <see cref="LpLoadedAssessmentSummaryData"/> representing an unsuccessful load.</returns>
        public static LpLoadedAssessmentSummaryData Failure(string userErrorMessage)
        {
            return new LpLoadedAssessmentSummaryData(false, userErrorMessage, null);
        }
    }
}
