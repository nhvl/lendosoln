﻿namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections.Specialized;
    using ObjLib.Conversions.Aus;
    using Security;

    /// <summary>
    /// LPA submit request data.
    /// </summary>
    public class LpaSubmitRequestData : LpaRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LpaSubmitRequestData"/> class.
        /// </summary>
        /// <param name="requestType">The request type.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="sellerNumber">The seller number.</param>
        /// <param name="sellerPassword">The seller password.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="craInfo">The CRA info.</param>
        public LpaSubmitRequestData(LpaRequestType requestType, string username, string password, string sellerNumber, string sellerPassword, Guid loanId, AbstractUserPrincipal principal, AusCreditOrderingInfo craInfo)
            : base(username, password, sellerNumber, sellerPassword, loanId, principal)
        {
            this.CraInfo = craInfo;
            this.RequestType = requestType;
        }

        /// <summary>
        /// Gets the request type.
        /// </summary>
        /// <value>The request type.</value>
        public override LpaRequestType RequestType
        {
            get;
        }

        /// <summary>
        /// Gets the CRA information to pass into the request.
        /// </summary>
        /// <value>The CRA information to pass into the request.</value>
        public AusCreditOrderingInfo CraInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">Any errors.</param>
        /// <returns>True if valid. False otherwise.</returns>
        public override bool Validate(out string error)
        {
            if (!base.Validate(out error))
            {
                return false;
            }

            if (this.RequestType != LpaRequestType.NonSeamlessLpaSubmit && this.RequestType != LpaRequestType.SeamlessLpaSubmit)
            {
                error = "Invalid request type.";
                return false;
            }

            if (this.RequestType == LpaRequestType.SeamlessLpaSubmit && this.CraInfo == null)
            {
                error = "CRA information not provided.";
                return false;
            }

            return true;
        }
    }
}
