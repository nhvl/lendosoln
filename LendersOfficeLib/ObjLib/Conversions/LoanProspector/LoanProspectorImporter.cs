/// Author: David Dao
namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.Integration.Underwriting;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    using LoanProspectorResponse;
    using LqbGrammar.DataTypes;

    public class LoanProspectorImporter
    {
        private CPageData m_dataLoan = null;
        private readonly MismoServiceOrderResponse serviceOrderResponse;
        private readonly string rawServiceOrderResponseXml;

        private bool isImporting1003Data;
        private bool isImportingFeedback;
        private bool isImportingCreditReport;
        private bool isImportingLiabilitiesFromCreditReport;
        private bool addToAusDashboard;
        private bool hasSubjectPropertyNetCashFlow;
        private decimal totalSubjectPropertyNetCashFlow;

        private LoanProspectorImporter(
            ParseResult<MismoServiceOrderResponse> parsedServiceOrderResponse, 
            bool isImport1003, 
            bool isImportFeedback, 
            bool isImportCreditReport, 
            bool isImportLiabilitiesFromCreditReport, 
            bool addToAusDashboard,
            Guid? loanId)
        {
            if (!parsedServiceOrderResponse.IsSuccessful)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            this.rawServiceOrderResponseXml = parsedServiceOrderResponse.Input;          
            this.serviceOrderResponse = parsedServiceOrderResponse.ValueOrDefault;
            string inputLoanId = this.serviceOrderResponse.LoanApplication.ExtensionLoanApplication.LoanIdentifierData.GloballyUniqueIdentifier;

            if (loanId.HasValue)
            {
                this.LoanId = loanId.Value;
            }
            else
            {
                this.LoanId = inputLoanId.ToNullable<Guid>(Guid.TryParse);
                if (this.LoanId == Guid.Empty)
                {
                    // avoid the confusion between Guid.Empty and default(Guid?) from the start; neither is a valid loan id
                    this.LoanId = null;
                }
            }

            this.isImporting1003Data = isImport1003;
            this.isImportingFeedback = isImportFeedback;
            this.isImportingCreditReport = isImportCreditReport;
            this.isImportingLiabilitiesFromCreditReport = isImportLiabilitiesFromCreditReport;
            this.addToAusDashboard = addToAusDashboard;
        }

        public Guid? LoanId { get; }

        public Guid? FeedbackEdocId
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates an importer for Nonseamless LPA.
        /// </summary>
        /// <param name="parsedServiceOrderResponse">The parsed response.</param>
        /// <param name="isImport1003">Whether to import 1003 data.</param>
        /// <param name="isImportFeedback">Whether to import the feedback report.</param>
        /// <param name="isImportCreditReport">Whether to import the credit report.</param>
        /// <param name="loanId">The loan to import to. If not provided, will attempt to pull from the response.</param>
        /// <returns></returns>
        public static LoanProspectorImporter CreateImporterForNonseamless(ParseResult<MismoServiceOrderResponse> parsedServiceOrderResponse, bool isImport1003, bool isImportFeedback, bool isImportCreditReport, Guid? loanId)
        {
            return new LoanProspectorImporter(parsedServiceOrderResponse, isImport1003: isImport1003, isImportFeedback: isImportFeedback, isImportCreditReport: isImportCreditReport, isImportLiabilitiesFromCreditReport: false, addToAusDashboard: true, loanId: loanId);
        }

        /// <summary>
        /// Creates an importer for seamless LPA.
        /// </summary>
        /// <param name="parsedServiceOrderResponse">The parsed response.</param>
        /// <param name="isImportFeedback">Whether to import the feedback report.</param>
        /// <param name="isImportCreditReport">Whether to import the credit report.</param>
        /// <param name="isImportLiabilitiesFromCreditReport">Whether to import in the liabilities from the credit report.</param>
        /// <param name="loanId">The loan id to import to.</param>
        /// <returns></returns>
        public static LoanProspectorImporter CreateImporterForSeamless(ParseResult<MismoServiceOrderResponse> parsedServiceOrderResponse, bool isImportFeedback, bool isImportCreditReport, bool isImportLiabilitiesFromCreditReport, Guid? loanId)
        {
            return new LoanProspectorImporter(parsedServiceOrderResponse, isImport1003: false, isImportFeedback: isImportFeedback, isImportCreditReport: isImportCreditReport, isImportLiabilitiesFromCreditReport: isImportLiabilitiesFromCreditReport, addToAusDashboard: false, loanId: loanId);
        }

        /// <summary>
        /// Creates an importer for LPA transfer requests.
        /// </summary>
        /// <param name="parsedServiceOrderResponse">The parsed response.</param>
        /// <param name="isImport1003">Whether to import in 1003 loan data.</param>
        /// <param name="isImportCreditReport">Whether to import the credit report.</param>
        /// <param name="loanId">The loan id to import to.</param>
        /// <returns></returns>
        public static LoanProspectorImporter CreateImporterForTransfer(ParseResult<MismoServiceOrderResponse> parsedServiceOrderResponse, bool isImport1003, bool isImportCreditReport, Guid? loanId)
        {
            return new LoanProspectorImporter(parsedServiceOrderResponse, isImport1003: isImport1003, isImportFeedback: false, isImportCreditReport: isImportCreditReport, isImportLiabilitiesFromCreditReport: false, addToAusDashboard: false, loanId: loanId);
        }

        public void Import()
        {
            if (!this.isImporting1003Data && !this.isImportingFeedback && !this.isImportingCreditReport)
            {
                return;
            }

            this.m_dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.LoanId.Value, typeof(LoanProspectorImporter)); // Access controls will always be on the entire LP interaction, rather than the import itself.
            this.m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.m_dataLoan.SetFormatTarget(FormatTarget.FreddieMacAUS);
            List<string> importOptionsApplied = new List<string>(3);

            ApplyAlwaysImportedDataToLoanObject();
            if (this.isImporting1003Data)
            {
                Apply1003DataToLoanObject();
                importOptionsApplied.Add("1003 Data");
            }

            if (this.isImportingFeedback)
            {
                ApplyLpFeedbackToLoanObject();
                importOptionsApplied.Add("LPA Feedback");                
            }

            if (this.isImportingCreditReport)
            {
                ApplyCreditOrderToLoanObject();
                importOptionsApplied.Add("Credit Report");
            }
            var audit = new UpdateFromLoanProspectorAuditItem(
                PrincipalFactory.CurrentPrincipal,
                this.m_dataLoan.sFreddieLoanId,
                this.m_dataLoan.sFreddieTransactionId,
                this.m_dataLoan.sLpAusKey,
                importOptionsApplied);
            this.m_dataLoan.RecordAuditOnSave(audit);
            this.m_dataLoan.Save();
        }

        private void Apply1003DataToLoanObject()
        {
            Parse(this.serviceOrderResponse.LoanApplication);
        }

        private void ApplyLpFeedbackToLoanObject()
        {
            Parse(this.serviceOrderResponse.LoanFeedback);
            this.FeedbackEdocId = m_dataLoan.SaveFreddieFeedbackResponseXml(this.rawServiceOrderResponseXml);
        }

        private void ApplyCreditOrderToLoanObject()
        {
            var mergedCreditReport = this.serviceOrderResponse.LoanFeedback.MergedCreditReport;
            var freBorrowerResponse = this.serviceOrderResponse.LoanFeedback.FreBorrowerResponse;
            var loanApplication = this.serviceOrderResponse.LoanApplication;

            Parse(mergedCreditReport, freBorrowerResponse, loanApplication);
        }

        private void ApplyAlwaysImportedDataToLoanObject()
        {
            var loanIdentifierData = this.serviceOrderResponse?.LoanApplication?.ExtensionLoanApplication?.LoanIdentifierData;
            var freRequestingParty = this.serviceOrderResponse?.LoanApplication?.ExtensionLoanApplication?.FreRequestingParty;
            var transmittalData = this.serviceOrderResponse?.LoanApplication?.AdditionalCaseData?.TransmittalData;

            this.m_dataLoan.sFreddieLoanId = EmptyStringCoalesce(
                loanIdentifierData?.LoanProspectorLoanIdentifier,
                this.serviceOrderResponse?.LoanFeedback?.LoanProspectorLoanIdentifier);

            this.m_dataLoan.sFreddieTransactionId = EmptyStringCoalesce(
                loanIdentifierData?.FreLoanProspectorTransactionIdentifier,
                this.serviceOrderResponse?.LoanFeedback?.FreLoanProspectorTransactionIdentifier);

            this.m_dataLoan.sLpAusKey = EmptyStringCoalesce(
                loanIdentifierData?.LoanProspectorKeyIdentifier,
                this.serviceOrderResponse?.LoanFeedback?.LoanProspectorKeyIdentifier);

            this.m_dataLoan.sFreddieSellerNum = freRequestingParty?.SellerServicerNumber;

            var fredProcProint = LoanProspectorHelper.FromLoanProspectorFormat(transmittalData?.CaseStateType);
            if (fredProcProint.HasValue)
            {
                m_dataLoan.sFredProcPointT = fredProcProint.Value;
            }
        }

        /// <summary>
        /// Returns the first non-empty string.
        /// </summary>
        /// <param name="one">The first string.</param>
        /// <param name="two">The second string.</param>
        /// <returns>The first string, if it has a value; otherwise, the second.</returns>
        private static string EmptyStringCoalesce(string one, string two)
        {
            return !string.IsNullOrEmpty(one) ? one : two;
        }

        private void Parse(MismoLoanFeedback loanFeedback) 
        {
            if (null == loanFeedback)
                return;

            m_dataLoan.sLpDocClass = loanFeedback.FreDocumentationLevelDescription;
            E_sProd3rdPartyUwResultT _3rdPartyResultT = LoanProspectorHelper.Parse3rdPartyUwResult(loanFeedback);
            if (_3rdPartyResultT != E_sProd3rdPartyUwResultT.NA)
            {
                m_dataLoan.sProd3rdPartyUwResultT = _3rdPartyResultT;
            }

            foreach (MismoLoanProcessingMessage loanProcessingMessage in loanFeedback.LoanProcessingMessage) 
            {
                Parse(loanProcessingMessage);
            }

            m_dataLoan.sAusFindingsPull = true; // OPM 90052: the certificate is received and valid, so set sAusFindingsPull to true
            m_dataLoan.sIsLpUw = true;
            if (this.addToAusDashboard)
            {
                AusResultHandler.CreateSystemLpOrder(
                    this.m_dataLoan.sFreddieLoanId,
                    m_dataLoan.sLId,
                    loanFeedback.FrePurchaseEligibilityDescription,
                    loanFeedback.LoanProspectorCreditRiskClassificationDescription,
                    loanFeedback.LoanProspectorEvaluationStatusDescription,
                    loanFeedback.FreCreditScoreLtvFeeLevelDescription,
                    PrincipalFactory.CurrentPrincipal);
            }
        }

        private void Parse(MismoFreCreditScoreCollection creditScoreCollection, CAppData dataApp, bool isBorrower) 
        {
            foreach (MismoFreCreditScore creditScore in creditScoreCollection) 
            {
                switch (creditScore.RepositorySourceType) 
                {
                    case E_MismoFreCreditScoreRepositorySourceType.TRW: // Experian
                        if (isBorrower)
                            dataApp.aBExperianScore_rep = creditScore.RepositoryScoreValue;
                        else
                            dataApp.aCExperianScore_rep = creditScore.RepositoryScoreValue;
                        break;
                    case E_MismoFreCreditScoreRepositorySourceType.CCC: // TransUnion
                        if (isBorrower) 
                            dataApp.aBTransUnionScore_rep = creditScore.RepositoryScoreValue;
                        else
                            dataApp.aCTransUnionScore_rep = creditScore.RepositoryScoreValue;
                        break;
                    case E_MismoFreCreditScoreRepositorySourceType.CBI: // Equifax
                        if (isBorrower)
                            dataApp.aBEquifaxScore_rep = creditScore.RepositoryScoreValue;
                        else
                            dataApp.aCEquifaxScore_rep = creditScore.RepositoryScoreValue;
                        break;

                }
            }
        }
        private void Parse(MismoCreditInfileReport creditInfileReport, MismoFreBorrowerResponseCollection freBorrowerResponseCollection) 
        {
            if (null == creditInfileReport ||creditInfileReport.InnerText == null)
                return;

            // 1/25/2012 dd - Per OPM 76917 We are no longer import LP viewable only credit report.
            // We need XML credit report for accurate pricing.
            /*
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (null == principal)
                return;

            Hashtable hash = new Hashtable(5);

            foreach (MismoFreBorrowerResponse o in freBorrowerResponseCollection) 
            {
                string ssn = o.BorrowerSsn.Replace("-", "");
                CAppData dataApp = FindDataAppBySsn(ssn);
                if (null != dataApp) 
                {
                    Parse(o.FreCreditScore, dataApp, dataApp.aBSsn.Replace("-","") == ssn);
                }
                hash[ssn] = o.CreditReportIdentifier;
                
            }

            string xml = creditInfileReport.InnerText.TrimWhitespaceAndBOM();
            xml = xml.Replace("<![CDATA[", "").Replace("]]>", "");
            for (int appIndex = 0; appIndex < m_dataLoan.nApps; appIndex++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(appIndex);
                string creditReportIdentifier = (string) hash[dataApp.aBSsn.Replace("-", "")];
                XmlDocument doc = CreateUserViewableCredit(xml, creditReportIdentifier);

                SaveCreditReport(doc, m_dataLoan.GetAppData(0).aAppId, E_CreditReportAuditSourceT.ImportInfileFromLp, ConstAppDavid.DummyFannieMaeServiceCompany);
            }
            */
        }
        private XmlDocument CreateUserViewableCredit(string data, string creditReportIdentifier) 
        {
            string templateXml = @"
<RESPONSE_GROUP MISMOVersionID=""2.3.1"">
<RESPONSE>
<RESPONSE_DATA>
<CREDIT_RESPONSE MISMOVersionID=""2.3.1"" CreditReportIdentifier=""" + creditReportIdentifier + @""">
<EMBEDDED_FILE _Type=""TEXT"" _Description=""EmbeddedFile"">
<DOCUMENT>
</DOCUMENT>
</EMBEDDED_FILE>
</CREDIT_RESPONSE>
</RESPONSE_DATA>
</RESPONSE>
</RESPONSE_GROUP>
";
            XmlDocument doc = Tools.CreateXmlDoc(templateXml);
            XmlElement el = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE/DOCUMENT");
            if (null != el) 
            {
                XmlCDataSection cdata = doc.CreateCDataSection(data);
                el.AppendChild(cdata);
            }
            return doc;
        }
        private void SaveCreditReport(XmlDocument doc, Guid aAppId, Guid crcCompanyId) 
        {
            if (null == doc)
                return;

            

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (null == principal)
                return;

            LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse creditReport = new LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse(doc);
            CreditReportUtilities.SaveXmlCreditReport(creditReport, principal, m_dataLoan.sLId, aAppId, crcCompanyId, Guid.Empty, E_CreditReportAuditSourceT.ImportMergedFromLp);
        }
        private void Parse(MismoMergedCreditReport mergedCreditReport, MismoFreBorrowerResponseCollection freBorrowerResponseCollection, MismoLoanApplication loanApplication) 
        {
            if (null == mergedCreditReport || mergedCreditReport.InnerText == null)
                return;

            Hashtable hash = new Hashtable(5);

            Guid crcCompanyId = LoanProspectorCreditReportingCompanyMapping.GetLendersOfficeCraId(loanApplication.ExtensionLoanApplication.MergedCreditRequest.CreditBureau.Name);
            if (Guid.Empty == crcCompanyId)
                crcCompanyId = ConstAppDavid.DummyFannieMaeServiceCompany; // Default to dummy company

            foreach (MismoFreBorrowerResponse o in freBorrowerResponseCollection) 
            {
                string ssn = o.BorrowerSsn.Replace("-", "");
                CAppData dataApp = FindDataAppBySsn(ssn);
                if (null != dataApp) 
                {
                    Parse(o.FreCreditScore, dataApp, dataApp.aBSsn.Replace("-","") == ssn);
                }

                hash[ssn] = o.CreditReportIdentifier;
            }

            string xml = mergedCreditReport.InnerText.TrimWhitespaceAndBOM();
            XmlDocument doc = null;
            if (xml.StartsWith("<![CDATA[MERGED CREDIT**")) 
            {
                // 1/25/2012 dd - Per OPM 76917 We are no longer import LP viewable only credit report.
                // We need XML credit report for accurate pricing.
                /*
                xml = xml.Replace("<![CDATA[MERGED CREDIT**", "").Replace("**MERGED CREDIT]]>", "");
                for (int appIndex = 0; appIndex < m_dataLoan.nApps; appIndex++) 
                {
                    CAppData dataApp = m_dataLoan.GetAppData(appIndex);
                    string creditReportIdentifier = (string) hash[dataApp.aBSsn.Replace("-", "")];
                    doc = CreateUserViewableCredit(xml, creditReportIdentifier);
                    // 4/30/2008 dd - Regardless number of borrowers with credit report. LP return one big txt credit that contains all borrower credit report. However
                    // there is no easy way to break each credit report apart, I have to always insert to all applicant.

                    SaveCreditReport(doc, dataApp.aAppId, E_CreditReportAuditSourceT.ImportMergedFromLp, crcCompanyId);

                }
                */
            } 
            else 
            {

                xml = xml.Replace("&gt;", ">").Replace("&lt;", "<");
                // 4/30/2008 dd - Need to embed whole xml under one single root, otherwise you will get Xml parsing error if LP contains multiple credit reports.
                doc = Tools.CreateXmlDoc("<root>" + xml + "</root>");
                

                XmlNodeList nodeList = doc.SelectNodes("root/RESPONSE_GROUP");
                foreach (XmlElement el in nodeList) 
                {
                    XmlElement _borrowerEl = (XmlElement) el.SelectSingleNode("RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/BORROWER[@_PrintPositionType='Borrower']");
                    Guid aAppId = m_dataLoan.GetAppData(0).aAppId; // Default to primary borrower.
                    if (null != _borrowerEl) 
                    {
                        string ssn = _borrowerEl.GetAttribute("_SSN").Replace("-", "");
                        for (int i = 0; i < m_dataLoan.nApps; i++) 
                        {
                            if (m_dataLoan.GetAppData(i).aBSsn.Replace("-", "") == ssn) 
                            {
                                aAppId = m_dataLoan.GetAppData(i).aAppId;
                                break;
                            }
                        }

                    }

                    XmlDocument _doc = Tools.CreateXmlDoc(el.OuterXml);
                    SaveCreditReport(_doc, aAppId, crcCompanyId);

                    CAppData dataApp = m_dataLoan.GetAppData(aAppId);
                    if (this.isImportingLiabilitiesFromCreditReport)
                    {
                        dataApp.ImportLiabilitiesFromCreditReport(true, true);
                    }

                    // Parse credit score.
                    CreditReportProxy creditReportProxy = CreditReportFactory.ConstructCreditReportFromXmlDoc(_doc, new CreditReportDebugInfo(m_dataLoan.sLId, ""));
                    dataApp.aBEquifaxScore = creditReportProxy.BorrowerScore.Equifax;
                    dataApp.aBExperianScore = creditReportProxy.BorrowerScore.Experian;
                    dataApp.aBTransUnionScore = creditReportProxy.BorrowerScore.TransUnion;

                    dataApp.aCEquifaxScore = creditReportProxy.CoborrowerScore.Equifax;
                    dataApp.aCExperianScore = creditReportProxy.CoborrowerScore.Experian;
                    dataApp.aCTransUnionScore = creditReportProxy.CoborrowerScore.TransUnion;

                    dataApp.ImportPublicRecordsFromCreditReport(); // 6/5/2009 dd - OPM 31159
                }
            }
            
        }
        private void Parse(MismoLoanApplication loanApplication) 
        {
            if (null == loanApplication)
                return;

            bool? isPropertyUnderConstruction = loanApplication?.Property?.BuildingStatusType == E_MismoPropertyBuildingStatusType.Proposed
                || loanApplication?.Property?.BuildingStatusType == E_MismoPropertyBuildingStatusType.UnderConstruction;
            Parse(loanApplication.LoanPurpose, isPropertyUnderConstruction); // Need to be parse first in order for sLPurposeT set correctly.
            this.hasSubjectPropertyNetCashFlow = false;
            this.totalSubjectPropertyNetCashFlow = 0;
            
            Parse(loanApplication.LoanProductData);

            Parse(loanApplication.GovernmentLoan);
            Parse(loanApplication.AdditionalCaseData);
            Parse(loanApplication.Property);
            Parse(loanApplication.MortgageTerms);
            Parse(loanApplication.InterviewerInformation);
            Parse(loanApplication.TransactionDetail);
            Parse(loanApplication.ClosingAgent);

            // Clear out other income for existing applicant.
            if (!m_dataLoan.sIsIncomeCollectionEnabled)
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData dataApp = m_dataLoan.GetAppData(i);
                    dataApp.aOtherIncomeList = new List<OtherIncome>();
                }
            }
            else
            {
                m_dataLoan.ClearIncomeSources();
            }

            bool isPrimaryApp = true;
            Hashtable borrowerHash = new Hashtable();

            foreach (MismoBorrower borrower in loanApplication.Borrower) 
            {
                if (null != borrower) 
                {
                    bool isBorrower = true;
                    CAppData dataApp = FindDataAppBySsn(borrower.Ssn, out isBorrower);
                    if (null == dataApp) 
                    {
                        if (isPrimaryApp) 
                        {
                            dataApp = m_dataLoan.GetAppData(0);
                            isPrimaryApp = false;
                        } 
                        else 
                        {
                            m_dataLoan.Save();
                            int iApp = m_dataLoan.AddNewApp();
                            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            dataApp = m_dataLoan.GetAppData(iApp);
                        }
                    } 
                    else 
                    {
                        isPrimaryApp = false;
                    }
                    if (!borrowerHash.Contains(borrower.BorrowerId)) 
                    {
                        borrowerHash.Add(borrower.BorrowerId, dataApp);
                    }
                    Parse(borrower, dataApp, isBorrower);
                }
            }

            if (this.m_dataLoan.sIsIncomeCollectionEnabled)
            {
                this.m_dataLoan.ImportGrossRent(this.hasSubjectPropertyNetCashFlow, this.totalSubjectPropertyNetCashFlow);
            }

            // Clear out existing collection: asset, liabilities, REOs
            for (int i = 0; i < m_dataLoan.nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                dataApp.aAssetCollection.ClearAll();
                dataApp.aReCollection.ClearAll();
                dataApp.aLiaCollection.ClearAll();
            }

            foreach (MismoAsset asset in loanApplication.Asset) 
            {
                if (null != asset) 
                {
                    CAppData dataApp = (CAppData) borrowerHash[asset.BorrowerId];
                    if (null != dataApp) 
                    {
                        Parse(asset, dataApp.aAssetCollection);
                    } 
                    else 
                    {
                        Tools.LogBug("Unable to import asset with borrowerId=" + asset.BorrowerId);
                    }

                }
            }

            Hashtable liabilityHash = new Hashtable();

            foreach (MismoLiability liability in loanApplication.Liability) 
            {
                if (null != liability) 
                {
                    CAppData dataApp = (CAppData) borrowerHash[liability.BorrowerId];
                    if (null != dataApp) 
                    {
                        ILiability field = Parse(liability, dataApp.aLiaCollection);
                        if (null != liability.Id && "" != liability.Id && null != field) 
                        {
                            liabilityHash.Add(liability.Id, field);
                        }
                    } 
                    else 
                    {
                        Tools.LogBug("Unable to import liability with borrowerId=" + liability.BorrowerId);
                    }

                }
            }

            foreach (MismoReoProperty reoProperty in loanApplication.ReoProperty) 
            {
                if (null != reoProperty) 
                {
                    CAppData dataApp = (CAppData) borrowerHash[reoProperty.BorrowerId];
                    if (null != dataApp) 
                    {
                        Parse(reoProperty, dataApp.aReCollection, liabilityHash);
                    } 
                    else 
                    {
                        Tools.LogBug("Unable to import reo with borrowerId=" + reoProperty.BorrowerId);
                    }
                }
            }

            foreach (MismoProposedHousingExpense proposedHousingExpense in loanApplication.ProposedHousingExpense) 
            {
                if (null != proposedHousingExpense) 
                {
                    switch (proposedHousingExpense.HousingExpenseType) 
                    {
                        case E_MismoProposedHousingExpenseHousingExpenseType.MI:
                            if (m_dataLoan.sProMIns_rep != proposedHousingExpense.PaymentAmount)
                            {
                                m_dataLoan.sProMInsLckd = true;
                                m_dataLoan.sProMIns_rep = proposedHousingExpense.PaymentAmount;
                            }
                            break;
                        case E_MismoProposedHousingExpenseHousingExpenseType.HazardInsurance:
                            m_dataLoan.sProHazInsR_rep = "0";
                            m_dataLoan.sProHazInsMb_rep = proposedHousingExpense.PaymentAmount;
                            break;
//                        case E_MismoProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                        case E_MismoProposedHousingExpenseHousingExpenseType.OtherHousingExpense:
                            m_dataLoan.sProOHExp_rep = proposedHousingExpense.PaymentAmount;
                            m_dataLoan.sProOHExpLckd = true;
                            m_dataLoan.sProOHExpDesc = proposedHousingExpense.HousingExpenseTypeOtherDescription;
                            break;
                        case E_MismoProposedHousingExpenseHousingExpenseType.RealEstateTax:
                            m_dataLoan.sProRealETxMb_rep = proposedHousingExpense.PaymentAmount;
                            m_dataLoan.sProRealETxR_rep = "0";
                            break;
                        case E_MismoProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                            m_dataLoan.sProHoAssocDues_rep = proposedHousingExpense.PaymentAmount;
                            break;
//                        case E_MismoProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:

                    }
                }
            }

            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                m_dataLoan.GetAppData(i).Flush(); // Save all liabilities, assets and reos to db.
            }
            foreach (MismoDownPayment downPayment in loanApplication.DownPayment) 
            {
                Parse(downPayment);
            }

        }

        private void Parse(MismoReoProperty reoProperty, IReCollection aReCollection, Hashtable liabilityHash) 
        {
            if (null == reoProperty || null == aReCollection)
                return;

            var reFields = aReCollection.AddRegularRecord();

            string[] matchedLiabilities = reoProperty.LiabilityId.Split(' ');
            foreach (string liaId in matchedLiabilities) 
            {
                ILiabilityRegular liaFields = liabilityHash[liaId] as ILiabilityRegular;
                if (null != liaFields) 
                {
                    liaFields.MatchedReRecordId = reFields.RecordId;
                    liaFields.Update();
                }
            }
            reFields.OccR_rep = "100";
            reFields.Addr = reoProperty.StreetAddress;
            reFields.City = reoProperty.City;
            reFields.State = reoProperty.State;
            reFields.Zip = reoProperty.PostalCode;
            reFields.TypeT = Parse(reoProperty.GsePropertyType);
            reFields.StatT = Parse(reoProperty.DispositionStatusType); 
            reFields.MPmt_rep = reoProperty.LienInstallmentAmount;
            reFields.MAmt_rep = reoProperty.LienUpbAmount;
            reFields.HExp_rep = reoProperty.MaintenanceExpenseAmount;
            reFields.Val_rep = reoProperty.MarketValueAmount;
            reFields.GrossRentI_rep = reoProperty.RentalIncomeGrossAmount;
            reFields.IsSubjectProp = reoProperty.SubjectIndicator == E_YesNoIndicator.Y;
            
            reFields.Update();

        }

        private E_ReoStatusT Parse(E_MismoReoPropertyDispositionStatusType type) 
        {
            switch (type) 
            {
                case E_MismoReoPropertyDispositionStatusType.PendingSale: return E_ReoStatusT.PendingSale;
                case E_MismoReoPropertyDispositionStatusType.RetainForRental: return E_ReoStatusT.Rental;
                case E_MismoReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence: return E_ReoStatusT.Residence;
                case E_MismoReoPropertyDispositionStatusType.Sold: return E_ReoStatusT.Sale;
                default:
                    return E_ReoStatusT.Residence;
            }
        }
        private E_ReoTypeT Parse(E_MismoReoPropertyGsePropertyType type) 
        {
            switch (type) 
            {
                case E_MismoReoPropertyGsePropertyType.TwoToFourUnitProperty: return E_ReoTypeT._2_4Plx;
                case E_MismoReoPropertyGsePropertyType.CommercialNonResidential: return E_ReoTypeT.ComNR;
                case E_MismoReoPropertyGsePropertyType.HomeAndBusinessCombined: return E_ReoTypeT.ComR;
                case E_MismoReoPropertyGsePropertyType.Condominium: return E_ReoTypeT.Condo;
                case E_MismoReoPropertyGsePropertyType.Cooperative: return E_ReoTypeT.Coop;
                case E_MismoReoPropertyGsePropertyType.Farm: return E_ReoTypeT.Farm;
                case E_MismoReoPropertyGsePropertyType.Land: return E_ReoTypeT.Land;
                case E_MismoReoPropertyGsePropertyType.MixedUseResidential: return E_ReoTypeT.Mixed;
                case E_MismoReoPropertyGsePropertyType.ManufacturedMobileHome: return E_ReoTypeT.Mobil;
                case E_MismoReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits: return E_ReoTypeT.Multi;
                case E_MismoReoPropertyGsePropertyType.SingleFamily: return E_ReoTypeT.SFR;
                case E_MismoReoPropertyGsePropertyType.Townhouse: return E_ReoTypeT.Town;
                case E_MismoReoPropertyGsePropertyType.Undefined: return E_ReoTypeT.LeaveBlank;
                default:
                    return E_ReoTypeT.LeaveBlank;
            }
        }

        private ILiability Parse(MismoLiability liability, ILiaCollection aLiaCollection) 
        {
            ILiability field = null;

            if (null == liability || null == aLiaCollection)
                return field;

            if (liability.Type == E_MismoLiabilityType.JobRelatedExpenses) 
            {
                var jobExpense = aLiaCollection.GetJobRelated1(true);
                if (jobExpense.Pmt_rep != "" && jobExpense.Pmt_rep != "0.00") 
                {
                    jobExpense = aLiaCollection.GetJobRelated2(true);
                }
                jobExpense.ExpenseDesc = liability.HolderName;
                jobExpense.Pmt_rep = liability.MonthlyPaymentAmount;

                field = jobExpense;
            } 
            else if (liability.Type == E_MismoLiabilityType.Alimony) 
            {
                var alimony = aLiaCollection.GetAlimony(true);
                alimony.RemainMons_rep = liability.RemainingTermMonths;
                alimony.Pmt_rep = liability.MonthlyPaymentAmount;
                alimony.OwedTo = liability.AlimonyOwedToName;
                alimony.NotUsedInRatio = liability.ExclusionIndicator == E_YesNoIndicator.Y;

                field = alimony;
            }
            else if (liability.Type == E_MismoLiabilityType.ChildSupport)
            {
                var childSupport = aLiaCollection.GetChildSupport(true);
                childSupport.RemainMons_rep = liability.RemainingTermMonths;
                childSupport.Pmt_rep = liability.MonthlyPaymentAmount;
                childSupport.NotUsedInRatio = liability.ExclusionIndicator == E_YesNoIndicator.Y;

                field = childSupport;
            }
            else 
            {
                var liaRegular = aLiaCollection.AddRegularRecord();
                liaRegular.ComAddr = liability.HolderStreetAddress;
                liaRegular.ComCity = liability.HolderCity;
                liaRegular.ComState = liability.HolderState;
                liaRegular.ComZip = liability.HolderPostalCode;
                liaRegular.AccNum = liability.AccountIdentifier;
                liaRegular.ComNm = liability.HolderName;
                liaRegular.Pmt_rep = liability.MonthlyPaymentAmount;
                liaRegular.RemainMons_rep = liability.RemainingTermMonths;
                liaRegular.Bal_rep = liability.UnpaidBalanceAmount;
                liaRegular.NotUsedInRatio = liability.ExclusionIndicator == E_YesNoIndicator.Y;
                liaRegular.DebtT = Parse(liability.Type);

                if (liaRegular.DebtT == E_DebtRegularT.Other)
                {
                    liaRegular.Desc = ParseOtherLiabilityDescription(liability.Type);
                }

                this.Parse(liability.ExtensionLiability, liaRegular);
                liaRegular.WillBePdOff |= liability.PayoffStatusIndicator == E_YesNoIndicator.Y;

                field = liaRegular;
            }

            if (null != field) 
            {
                field.Update();
            }

            return field;
        }

        private void Parse(EXTENSION_LIABILITY extensionLiability, ILiabilityRegular liability)
        {
            if (extensionLiability.ReadFromXml)
            {
                liability.WillBePdOff = extensionLiability.LiabilityPartialPayoffIndicator == E_YesNoIndicator.Y;
                if (extensionLiability.LiabilityPartialPayoffIndicator == E_YesNoIndicator.Y)
                {
                    liability.PayoffAmtLckd = true;
                    liability.PayoffAmt_rep = extensionLiability.LiabilityPartialPayoffAmount;
                }
            }
        }

        private E_DebtRegularT Parse(E_MismoLiabilityType type) 
        {
            switch (type) 
            {
                case E_MismoLiabilityType.Installment: 
                    return E_DebtRegularT.Installment;
                case E_MismoLiabilityType.MortgageLoan:
                    return E_DebtRegularT.Mortgage;
                case E_MismoLiabilityType.Revolving:
                    return E_DebtRegularT.Revolving;
                case E_MismoLiabilityType.Open30DayChargeAccount:
                    return E_DebtRegularT.Open;
                default:
                    return E_DebtRegularT.Other;
            }
        }

        private string ParseOtherLiabilityDescription(E_MismoLiabilityType type)
        {
            if (type == E_MismoLiabilityType.CollectionsJudgementsAndLiens)
            {
                return "Liens";
            }
            else if (type == E_MismoLiabilityType.HELOC)
            {
                return "Home Equity Line of Credit";
            }
            else if (type == E_MismoLiabilityType.Installment)
            {
                return "Installment Loan";
            }
            else if (type == E_MismoLiabilityType.LeasePayments)
            {
                return "Lease Payments";
            }
            else if (type == E_MismoLiabilityType.MortgageLoan)
            {
                return "Mortgage";
            }
            else if (type == E_MismoLiabilityType.Open30DayChargeAccount)
            {
                return "Open, 30day Charge Acc";
            }
            else if (type == E_MismoLiabilityType.Revolving)
            {
                return "Revolving Charge";
            }
            else
            {
                return "Other Liability";
            }
        }

        private void Parse(MismoAsset asset, IAssetCollection aAssetCollection) 
        {
            if (null == asset || null == aAssetCollection)
                return;

            if (asset.Type == E_MismoAssetType.NetWorthOfBusinessOwned) 
            {
                var assetBusiness = aAssetCollection.GetBusinessWorth(true);
                assetBusiness.Val_rep = asset.CashOrMarketValueAmount;
                assetBusiness.Update();
            } 
            else if (asset.Type == E_MismoAssetType.EarnestMoneyCashDepositTowardPurchase) 
            {
                var cashDeposit = aAssetCollection.GetCashDeposit1(true);
                if (cashDeposit.Val_rep != "" && cashDeposit.Val_rep != "0.00") 
                {
                    // First entry already populate, use the second field.
                    cashDeposit = aAssetCollection.GetCashDeposit2(true);
                }
                cashDeposit.Val_rep = asset.CashOrMarketValueAmount;
                cashDeposit.Desc = asset.HolderName;
                cashDeposit.Update();
            } 
            else if (asset.Type == E_MismoAssetType.RetirementFund) 
            {
                var assetRetirement = aAssetCollection.GetRetirement(true);
                assetRetirement.Val_rep = asset.CashOrMarketValueAmount;
                assetRetirement.Update();
            } 
            else if (asset.Type == E_MismoAssetType.LifeInsurance) 
            {
                var assetLifeIns = aAssetCollection.GetLifeInsurance(true);
                assetLifeIns.Val_rep = asset.CashOrMarketValueAmount;
                assetLifeIns.FaceVal_rep = asset.LifeInsuranceFaceValueAmount;
                assetLifeIns.Update();
            } 
            else 
            {
                var assetRegular = aAssetCollection.AddRegularRecord();
                assetRegular.AccNum = asset.AccountIdentifier;
                assetRegular.Val_rep = asset.CashOrMarketValueAmount;
                assetRegular.ComNm = asset.HolderName;
                assetRegular.StAddr = asset.HolderStreetAddress;
                assetRegular.City = asset.HolderCity;
                assetRegular.State = asset.HolderState;
                assetRegular.Zip = asset.HolderPostalCode;
                assetRegular.OtherTypeDesc = asset.OtherAssetTypeDescription;
                assetRegular.AssetT = Parse(asset.Type);
                if (asset.Type == E_MismoAssetType.Automobile) 
                {
                    assetRegular.Desc = asset.AutomobileMakeDescription;
                }
                assetRegular.Update();
            }


        }
        private E_AssetRegularT Parse(E_MismoAssetType type) 
        {
            switch (type) 
            {
                case E_MismoAssetType.Automobile: return E_AssetRegularT.Auto;
                case E_MismoAssetType.Bond: return E_AssetRegularT.Bonds;
                case E_MismoAssetType.CheckingAccount: return E_AssetRegularT.Checking;
                case E_MismoAssetType.GiftsTotal: return E_AssetRegularT.GiftFunds;
                case E_MismoAssetType.OtherNonLiquidAssets: return E_AssetRegularT.OtherIlliquidAsset;
                case E_MismoAssetType.OtherLiquidAssets: return E_AssetRegularT.OtherLiquidAsset;
                case E_MismoAssetType.SavingsAccount: return E_AssetRegularT.Savings;
                case E_MismoAssetType.Stock: return E_AssetRegularT.Stocks;
                case E_MismoAssetType.PendingNetSaleProceedsFromRealEstateAssets: return E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets;
                case E_MismoAssetType.CertificateOfDepositTimeDeposit: return E_AssetRegularT.CertificateOfDeposit;
                case E_MismoAssetType.MoneyMarketFund: return E_AssetRegularT.MoneyMarketFund;
                case E_MismoAssetType.MutualFund: return E_AssetRegularT.MutualFunds;
                case E_MismoAssetType.SecuredBorrowedFundsNotDeposited: return E_AssetRegularT.SecuredBorrowedFundsNotDeposit;
                case E_MismoAssetType.BridgeLoanNotDeposited: return E_AssetRegularT.BridgeLoanNotDeposited;
                case E_MismoAssetType.TrustAccount: return E_AssetRegularT.TrustFunds;
                default:
                    return E_AssetRegularT.OtherLiquidAsset;
            }
        }
        private void Parse(MismoBorrower borrower, CAppData dataApp, bool isBorrower) 
        {
            //bool isBorrower = borrower.PrintPositionType == E_MismoBorrowerPrintPositionType.Borrower;
             if (isBorrower) 
            {
                dataApp.aBDob_rep = borrower.BirthDate;
                dataApp.aBFirstNm = borrower.FirstName;
                dataApp.aBHPhone = borrower.HomeTelephoneNumber;
                dataApp.aBLastNm = borrower.LastName;
                dataApp.aBMidNm = borrower.MiddleName;
                dataApp.aBSuffix = borrower.NameSuffix;
                dataApp.aBSsn = borrower.Ssn;
                dataApp.aBDependNum_rep = borrower.DependentCount;
                if (borrower.JointAssetLiabilityReportingType != E_MismoBorrowerJointAssetLiabilityReportingType.Undefined) 
                {
                    dataApp.aAsstLiaCompletedNotJointly = borrower.JointAssetLiabilityReportingType == E_MismoBorrowerJointAssetLiabilityReportingType.NotJointly;
                }
                switch (borrower.MaritalStatusType) 
                {
                    case E_MismoBorrowerMaritalStatusType.Married:
                        dataApp.aBMaritalStatT = E_aBMaritalStatT.Married;
                        break;
                    case E_MismoBorrowerMaritalStatusType.Separated:
                        dataApp.aBMaritalStatT = E_aBMaritalStatT.Separated;
                        break;
                    case E_MismoBorrowerMaritalStatusType.Unmarried:
                        dataApp.aBMaritalStatT = E_aBMaritalStatT.NotMarried;
                        break;
                    default:
                        dataApp.aBMaritalStatT = E_aBMaritalStatT.LeaveBlank;
                        break;

                }

                #region Addresses
                dataApp.aBSchoolYrs_rep = borrower.SchoolingYears;
                dataApp.aBCityMail = borrower.MailTo.City;
                dataApp.aBZipMail = borrower.MailTo.PostalCode;
                dataApp.aBStateMail = borrower.MailTo.State;
                dataApp.aBAddrMail = borrower.MailTo.StreetAddress;
                dataApp.aBAddrMailUsePresentAddr = false;
                int aPrevAddrIndex = 0;
                YearMonthParser yearMonthParser = new YearMonthParser();

                foreach (MismoResidence residence in borrower.Residence) 
                {
                    if (null != residence) 
                    {
                        yearMonthParser.ParseStr(residence.BorrowerResidencyDurationYears, residence.BorrowerResidencyDurationMonths);

                        if (residence.BorrowerResidencyType == E_MismoResidenceBorrowerResidencyType.Current) 
                        {
                            dataApp.aBAddr = residence.StreetAddress;
                            dataApp.aBCity = residence.City;
                            dataApp.aBState = residence.State;
                            dataApp.aBZip = residence.PostalCode;
                            dataApp.aBAddrYrs = yearMonthParser.YearsInDecimal.ToString();
                            switch (residence.BorrowerResidencyBasisType) 
                            {
                                case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                    dataApp.aBAddrT = E_aBAddrT.Own;
                                    break;
                                case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                    dataApp.aBAddrT = E_aBAddrT.Rent;
                                    break;
                                case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                    dataApp.aBAddrT = E_aBAddrT.LivingRentFree;
                                    break;
                                default:
                                    dataApp.aBAddrT = E_aBAddrT.LeaveBlank;
                                    break;

                            }
                        } 
                        else if (residence.BorrowerResidencyType == E_MismoResidenceBorrowerResidencyType.Prior) 
                        {
                            if (aPrevAddrIndex == 0) 
                            {
                                dataApp.aBPrev1Addr = residence.StreetAddress;
                                dataApp.aBPrev1City = residence.City;
                                dataApp.aBPrev1State = residence.State;
                                dataApp.aBPrev1Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType) 
                                {
                                    case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aBPrev1AddrT = E_aBPrev1AddrT.Own;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aBPrev1AddrT = E_aBPrev1AddrT.Rent;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aBPrev1AddrT = E_aBPrev1AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aBPrev1AddrT = E_aBPrev1AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aBPrev1AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            } 
                            else if (aPrevAddrIndex == 1) 
                            {
                                dataApp.aBPrev2Addr = residence.StreetAddress;
                                dataApp.aBPrev2City = residence.City;
                                dataApp.aBPrev2State = residence.State;
                                dataApp.aBPrev2Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType) 
                                {
                                    case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aBPrev2AddrT = E_aBPrev2AddrT.Own;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aBPrev2AddrT = E_aBPrev2AddrT.Rent;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aBPrev2AddrT = E_aBPrev2AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aBPrev2AddrT = E_aBPrev2AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aBPrev2AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            }
                            aPrevAddrIndex++;

                        }
                    }
                }
                #endregion

                #region Current Income

                var borrowerId = dataApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                foreach (MismoCurrentIncome currentIncome in borrower.CurrentIncome) 
                {
                    if (null != currentIncome && currentIncome.MonthlyTotalAmount != "") 
                    {
                        var monthlyTotalAmount = Money.Create(m_dataLoan.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount));
                        switch (currentIncome.IncomeType) 
                        {
                            case E_MismoCurrentIncomeIncomeType.Base:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aBBaseI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else {
                                    m_dataLoan.AddIncomeSource(
                                        borrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.BaseIncome,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Overtime:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                    {
                                dataApp.aBOvertimeI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else{
                                    m_dataLoan.AddIncomeSource(
                                        borrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Overtime,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Bonus:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                    {
                                dataApp.aBBonusesI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else{
                                    m_dataLoan.AddIncomeSource(
                                        borrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Bonuses,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Commissions:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                    {
                                dataApp.aBCommisionI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else{
                                    m_dataLoan.AddIncomeSource(
                                        borrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Commission,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.DividendsInterest:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                    {
                                dataApp.aBDividendI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else{
                                    m_dataLoan.AddIncomeSource(
                                        borrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.DividendsOrInterest,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.NetRentalIncome:
                                dataApp.aBNetRentI1003_rep = currentIncome.MonthlyTotalAmount;
                                break;
                            default:
                                var item = new OtherIncome()
                                {
                                    Amount = m_dataLoan.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount),
                                    Desc = ParseOtherIncome(currentIncome.IncomeType),
                                    IsForCoBorrower = false
                                };

                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    var items = dataApp.aOtherIncomeList;
                                    items.Add(item);
                                    dataApp.aOtherIncomeList = items;
                                }
                                else
                                {
                                    if (item.Desc.Equals(OtherIncome.GetDescription(E_aOIDescT.SubjPropNetCashFlow), StringComparison.OrdinalIgnoreCase))
                                    {
                                        this.m_dataLoan.sSpCountRentalIForPrimaryResidToo = true;
                                        this.hasSubjectPropertyNetCashFlow = true;
                                        this.totalSubjectPropertyNetCashFlow += item.Amount;
                                    }
                                    else
                                    {
                                        var migratedOtherIncome = IncomeCollectionMigration.GetMigratedOtherIncome(new[] { item }, dataApp.aBConsumerId, dataApp.aCConsumerId);
                                        foreach (var consumerIdIncomeSourcePair in migratedOtherIncome)
                                        {
                                            m_dataLoan.AddIncomeSource(consumerIdIncomeSourcePair.Item1, consumerIdIncomeSourcePair.Item2);
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }

                #endregion

                dataApp.aBDependAges = "";
                foreach (MismoDependent dependent in borrower.Dependent) 
                {
                    if (null != dependent && dependent.AgeYears != "") 
                    {
                        dataApp.aBDependAges += dependent.AgeYears + ",";
                    }
                }

                #region Present Housing Expense
                foreach (MismoPresentHousingExpense presentHousingExpense in borrower.PresentHousingExpense) 
                {
                    if (null != presentHousingExpense) 
                    {
                        switch (presentHousingExpense.HousingExpenseType) 
                        {
                            case E_MismoPresentHousingExpenseHousingExpenseType.Rent:
                                dataApp.aPresRent_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                                dataApp.aPres1stM_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                                dataApp.aPresOFin_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.HazardInsurance:
                                dataApp.aPresHazIns_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.RealEstateTax:
                                dataApp.aPresRealETx_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.MI:
                                dataApp.aPresMIns_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                                dataApp.aPresHoAssocDues_rep = presentHousingExpense.PaymentAmount;
                                break;
                            case E_MismoPresentHousingExpenseHousingExpenseType.OtherHousingExpense:
                                dataApp.aPresOHExp_rep = presentHousingExpense.PaymentAmount;
                                dataApp.aPresOHExpDesc = presentHousingExpense.HousingExpenseTypeOtherDescription;
                                break;
                        }
                    }
                }
                #endregion
                
            } 
            else 
            {
                dataApp.aCDob_rep = borrower.BirthDate;
                dataApp.aCFirstNm = borrower.FirstName;
                dataApp.aCHPhone = borrower.HomeTelephoneNumber;
                dataApp.aCLastNm = borrower.LastName;
                dataApp.aCMidNm = borrower.MiddleName;
                dataApp.aCSuffix = borrower.NameSuffix;
                dataApp.aCSsn = borrower.Ssn;
                dataApp.aCDependNum_rep = borrower.DependentCount;
                switch (borrower.MaritalStatusType) 
                {
                    case E_MismoBorrowerMaritalStatusType.Married:
                        dataApp.aCMaritalStatT = E_aCMaritalStatT.Married;
                        break;
                    case E_MismoBorrowerMaritalStatusType.Separated:
                        dataApp.aCMaritalStatT = E_aCMaritalStatT.Separated;
                        break;
                    case E_MismoBorrowerMaritalStatusType.Unmarried:
                        dataApp.aCMaritalStatT = E_aCMaritalStatT.NotMarried;
                        break;
                    default:
                        dataApp.aCMaritalStatT = E_aCMaritalStatT.LeaveBlank;
                        break;

                }

                #region Addresses
                dataApp.aCSchoolYrs_rep = borrower.SchoolingYears;
                dataApp.aCCityMail = borrower.MailTo.City;
                dataApp.aCZipMail = borrower.MailTo.PostalCode;
                dataApp.aCStateMail = borrower.MailTo.State;
                dataApp.aCAddrMail = borrower.MailTo.StreetAddress;
                dataApp.aCAddrMailUsePresentAddr = false;
                int aPrevAddrIndex = 0;
                YearMonthParser yearMonthParser = new YearMonthParser();

                foreach (MismoResidence residence in borrower.Residence) 
                {
                    if (null != residence) 
                    {
                        yearMonthParser.ParseStr(residence.BorrowerResidencyDurationYears, residence.BorrowerResidencyDurationMonths);

                        if (residence.BorrowerResidencyType == E_MismoResidenceBorrowerResidencyType.Current) 
                        {
                            dataApp.aCAddr = residence.StreetAddress;
                            dataApp.aCCity = residence.City;
                            dataApp.aCState = residence.State;
                            dataApp.aCZip = residence.PostalCode;
                            dataApp.aCAddrYrs = yearMonthParser.YearsInDecimal.ToString();
                            switch (residence.BorrowerResidencyBasisType) 
                            {
                                case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                    dataApp.aCAddrT = E_aCAddrT.Own;
                                    break;
                                case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                    dataApp.aCAddrT = E_aCAddrT.Rent;
                                    break;
                                case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                    dataApp.aCAddrT = E_aCAddrT.LivingRentFree;
                                    break;
                                default:
                                    dataApp.aCAddrT = E_aCAddrT.LeaveBlank;
                                    break;
                            }
                        } 
                        else if (residence.BorrowerResidencyType == E_MismoResidenceBorrowerResidencyType.Prior) 
                        {
                            if (aPrevAddrIndex == 0) 
                            {
                                dataApp.aCPrev1Addr = residence.StreetAddress;
                                dataApp.aCPrev1City = residence.City;
                                dataApp.aCPrev1State = residence.State;
                                dataApp.aCPrev1Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType) 
                                {
                                    case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aCPrev1AddrT = E_aCPrev1AddrT.Own;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aCPrev1AddrT = E_aCPrev1AddrT.Rent;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aCPrev1AddrT = E_aCPrev1AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aCPrev1AddrT = E_aCPrev1AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aCPrev1AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            } 
                            else if (aPrevAddrIndex == 1) 
                            {
                                dataApp.aCPrev2Addr = residence.StreetAddress;
                                dataApp.aCPrev2City = residence.City;
                                dataApp.aCPrev2State = residence.State;
                                dataApp.aCPrev2Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType) 
                                {
                                    case E_MismoResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aCPrev2AddrT = E_aCPrev2AddrT.Own;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aCPrev2AddrT = E_aCPrev2AddrT.Rent;
                                        break;
                                    case E_MismoResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aCPrev2AddrT = E_aCPrev2AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aCPrev2AddrT = E_aCPrev2AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aCPrev2AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            }
                            aPrevAddrIndex++;

                        }
                    }
                }
                #endregion

                #region Current Income
                var coborrowerId = dataApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                foreach (MismoCurrentIncome currentIncome in borrower.CurrentIncome) 
                {
                    if (null != currentIncome && currentIncome.MonthlyTotalAmount != "") 
                    {
                        var monthlyTotalAmount = Money.Create(m_dataLoan.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount));

                        switch (currentIncome.IncomeType)
                        {
                            case E_MismoCurrentIncomeIncomeType.Base:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aCBaseI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else
                                {
                                    m_dataLoan.AddIncomeSource(
                                        coborrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.BaseIncome,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Overtime:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aCOvertimeI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else
                                {
                                    m_dataLoan.AddIncomeSource(
                                        coborrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Overtime,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Bonus:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aCBonusesI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else
                                {
                                    m_dataLoan.AddIncomeSource(
                                        coborrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Bonuses,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.Commissions:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aCCommisionI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else
                                {
                                    m_dataLoan.AddIncomeSource(
                                        coborrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.Commission,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.DividendsInterest:
                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    dataApp.aCDividendI_rep = currentIncome.MonthlyTotalAmount;
                                }
                                else
                                {
                                    m_dataLoan.AddIncomeSource(
                                        coborrowerId,
                                        new IncomeSource
                                        {
                                            IncomeType = IncomeType.DividendsOrInterest,
                                            MonthlyAmountData = monthlyTotalAmount
                                        });
                                }
                                break;
                            case E_MismoCurrentIncomeIncomeType.NetRentalIncome:
                                dataApp.aCNetRentI1003_rep = currentIncome.MonthlyTotalAmount;
                                break;
                            default:
                                var item = new OtherIncome()
                                {
                                    Amount = m_dataLoan.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount),
                                    Desc = ParseOtherIncome(currentIncome.IncomeType),
                                    IsForCoBorrower = true
                                };

                                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    var items = dataApp.aOtherIncomeList;
                                    items.Add(item);
                                    dataApp.aOtherIncomeList = items;
                                }
                                else
                                {
                                    if (item.Desc.Equals(OtherIncome.GetDescription(E_aOIDescT.SubjPropNetCashFlow), StringComparison.OrdinalIgnoreCase))
                                    {
                                        this.m_dataLoan.sSpCountRentalIForPrimaryResidToo = true;
                                        this.hasSubjectPropertyNetCashFlow = true;
                                        this.totalSubjectPropertyNetCashFlow += item.Amount;
                                    }
                                    else
                                    {
                                        var migratedOtherIncome = IncomeCollectionMigration.GetMigratedOtherIncome(new[] { item }, dataApp.aBConsumerId, dataApp.aCConsumerId);
                                        foreach (var consumerIdIncomeSourcePair in migratedOtherIncome)
                                        {
                                            m_dataLoan.AddIncomeSource(consumerIdIncomeSourcePair.Item1, consumerIdIncomeSourcePair.Item2);
                                        }
                                    }
                                }
                                break;

                        }
                    }
                }
                #endregion

                dataApp.aCDependAges = "";
                foreach (MismoDependent dependent in borrower.Dependent) 
                {
                    if (null != dependent && dependent.AgeYears != "") 
                    {
                        dataApp.aCDependAges += dependent.AgeYears + ",";
                    }
                }
            }

            #region Employer
            IEmpCollection empCollection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
            empCollection.ClearAll();

            foreach (MismoEmployer employer in borrower.Employer) 
            {
                if (null != employer) 
                {
                    if (employer.EmploymentPrimaryIndicator == E_YesNoIndicator.Y) 
                    {
                        IPrimaryEmploymentRecord record = empCollection.GetPrimaryEmp(true);
                        record.EmplrNm = employer.Name;
                        record.EmplrAddr = employer.StreetAddress;
                        record.EmplrCity = employer.City;
                        record.EmplrState = employer.State;
                        record.EmplrZip = employer.PostalCode;
                        record.EmplrBusPhone = employer.TelephoneNumber;
                        record.IsCurrent = employer.EmploymentCurrentIndicator == E_YesNoIndicator.Y;
                        YearMonthParser yearMonthParser = new YearMonthParser();
                        yearMonthParser.ParseStr(employer.CurrentEmploymentYearsOnJob, employer.CurrentEmploymentMonthsOnJob);
                        record.EmplmtLen_rep = yearMonthParser.YearsInDecimal.ToString();

                        record.ProfLen_rep = employer.CurrentEmploymentTimeInLineOfWorkYears;
                        record.IsSelfEmplmt = employer.EmploymentBorrowerSelfEmployedIndicator == E_YesNoIndicator.Y;
                        record.JobTitle = employer.EmploymentPositionDescription;
                        record.Update();
                    } 
                    else 
                    {
                        IRegularEmploymentRecord record = empCollection.AddRegularRecord();
                        record.EmplrNm = employer.Name;
                        record.EmplrAddr = employer.StreetAddress;
                        record.EmplrCity = employer.City;
                        record.EmplrState = employer.State;
                        record.EmplrZip = employer.PostalCode;
                        record.EmplrBusPhone = employer.TelephoneNumber;
                        record.IsCurrent = employer.EmploymentCurrentIndicator == E_YesNoIndicator.Y;
                        record.IsSelfEmplmt = employer.EmploymentBorrowerSelfEmployedIndicator == E_YesNoIndicator.Y;
                        record.JobTitle = employer.EmploymentPositionDescription;
                        record.MonI_rep = employer.IncomeEmploymentMonthlyAmount;
                        record.EmplmtEndD_rep = employer.PreviousEmploymentEndDate;
                        record.EmplmtStartD_rep = employer.PreviousEmploymentStartDate;
                        record.Update();
                    }
                }
            }
            #endregion
            Parse(borrower.GovernmentMonitoring, dataApp, isBorrower);
            Parse(borrower.Declaration, dataApp, isBorrower);
        }

        private void Parse(MismoGovernmentMonitoring governmentMonitoring, CAppData dataApp, bool isBorrower) 
        {
            if (null == governmentMonitoring)
                return;

            if (isBorrower) 
            {
                dataApp.aBNoFurnishLckd = true;
                dataApp.aBNoFurnish = governmentMonitoring.RaceNationalOriginRefusalIndicator == E_YesNoIndicator.Y;
                if (governmentMonitoring.GovernmentMonitoringExtension.WasReadFromXml)
                {
                    this.Parse(governmentMonitoring.GovernmentMonitoringExtension, dataApp, isBorrower);
                }
                else
                {
                    dataApp.aBHispanicT = Parse(governmentMonitoring.HmdaEthnicityType);
                    dataApp.aBGender = Parse(governmentMonitoring.GenderType);
                    dataApp.aBIsAmericanIndian = false;
                    dataApp.aBIsAsian = false;
                    dataApp.aBIsBlack = false;
                    dataApp.aBIsPacificIslander = false;
                    dataApp.aBIsWhite = false;

                    foreach (MismoHmdaRace hmdaRace in governmentMonitoring.HmdaRace)
                    {
                        if (null != hmdaRace)
                        {
                            switch (hmdaRace.Type)
                            {
                                case E_MismoHmdaRaceType.AmericanIndianOrAlaskaNative:
                                    dataApp.aBIsAmericanIndian = true;
                                    break;
                                case E_MismoHmdaRaceType.Asian:
                                    dataApp.aBIsAsian = true;
                                    break;
                                case E_MismoHmdaRaceType.BlackOrAfricanAmerican:
                                    dataApp.aBIsBlack = true;
                                    break;
                                case E_MismoHmdaRaceType.NativeHawaiianOrOtherPacificIslander:
                                    dataApp.aBIsPacificIslander = true;
                                    break;
                                case E_MismoHmdaRaceType.White:
                                    dataApp.aBIsWhite = true;
                                    break;
                            }
                        }
                    }
                }
            } 
            else 
            {
                dataApp.aCNoFurnishLckd = true;
                dataApp.aCNoFurnish = governmentMonitoring.RaceNationalOriginRefusalIndicator == E_YesNoIndicator.Y;
                if (governmentMonitoring.GovernmentMonitoringExtension.WasReadFromXml)
                {
                    this.Parse(governmentMonitoring.GovernmentMonitoringExtension, dataApp, isBorrower);
                }
                else
                {
                    dataApp.aCHispanicT = Parse(governmentMonitoring.HmdaEthnicityType);
                    dataApp.aCGender = Parse(governmentMonitoring.GenderType);
                    dataApp.aCIsAmericanIndian = false;
                    dataApp.aCIsAsian = false;
                    dataApp.aCIsBlack = false;
                    dataApp.aCIsPacificIslander = false;
                    dataApp.aCIsWhite = false;

                    foreach (MismoHmdaRace hmdaRace in governmentMonitoring.HmdaRace)
                    {
                        if (null != hmdaRace)
                        {
                            switch (hmdaRace.Type)
                            {
                                case E_MismoHmdaRaceType.AmericanIndianOrAlaskaNative:
                                    dataApp.aCIsAmericanIndian = true;
                                    break;
                                case E_MismoHmdaRaceType.Asian:
                                    dataApp.aCIsAsian = true;
                                    break;
                                case E_MismoHmdaRaceType.BlackOrAfricanAmerican:
                                    dataApp.aCIsBlack = true;
                                    break;
                                case E_MismoHmdaRaceType.NativeHawaiianOrOtherPacificIslander:
                                    dataApp.aCIsPacificIslander = true;
                                    break;
                                case E_MismoHmdaRaceType.White:
                                    dataApp.aCIsWhite = true;
                                    break;
                            }
                        }
                    }
                }
            }
        }

        private void Parse(GOVERNMENT_MONITORING_EXTENSION governmentMonitoringExtension, CAppData dataApp, bool isBorrower)
        {
            E_BorrowerModeT oldBorrowerMode = dataApp.BorrowerModeT;
            dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;

            // None of the objects should be null since the getters will always create a new object.
            this.Parse(governmentMonitoringExtension.GovernmentMonitoringDetail, dataApp);
            this.Parse(governmentMonitoringExtension.HmdaEthnicities, dataApp);
            this.Parse(governmentMonitoringExtension.HmdaEthnicityOrigins, dataApp);
            this.Parse(governmentMonitoringExtension.HmdaRaces, dataApp);

            dataApp.BorrowerModeT = oldBorrowerMode;
        }

        private void Parse(HMDA_RACES hmdaRaces, CAppData dataApp)
        {
            HashSet<HmdaRaceDetailHmdaRaceType> hmdaRaceTypes = new HashSet<HmdaRaceDetailHmdaRaceType>();
            HashSet<HmdaRaceDesignationHmdaRaceDesignationType> hmdaRaceDesignationTypes = new HashSet<HmdaRaceDesignationHmdaRaceDesignationType>();
            string otherAsianDescription = null;
            string otherPacificIslanderDescription = null;
            foreach (var hmdaRaceExtension in hmdaRaces.HmdaRaceExtensionList)
            {
                hmdaRaceTypes.Add(hmdaRaceExtension.HmdaRaceDetail.HMDARaceType);

                if (hmdaRaceExtension.HmdaRaceDesignations.HmdaRaceDesignationList.Count != 0)
                {
                    hmdaRaceExtension.HmdaRaceDesignations.HmdaRaceDesignationList.ForEach(designation =>
                    {
                        if (designation.HMDARaceDesignationType != HmdaRaceDesignationHmdaRaceDesignationType.Undefined)
                        {
                            hmdaRaceDesignationTypes.Add(designation.HMDARaceDesignationType);
                        }
                    });

                    // Find the first HMDA_RACE_DESIGNATION container for the description properties.
                    if (string.IsNullOrEmpty(otherAsianDescription))
                    {
                        otherAsianDescription = hmdaRaceExtension.HmdaRaceDesignations.HmdaRaceDesignationList
                                                .FirstOrDefault(designation => !string.IsNullOrEmpty(designation.HMDARaceDesignationOtherAsianDescription))?.HMDARaceDesignationOtherAsianDescription ?? string.Empty;
                    }

                    if (string.IsNullOrEmpty(otherPacificIslanderDescription))
                    {
                        otherPacificIslanderDescription = hmdaRaceExtension.HmdaRaceDesignations.HmdaRaceDesignationList
                                                            .FirstOrDefault(designation => !string.IsNullOrEmpty(designation.HMDARaceDesignationOtherPacificIslanderDescription))?.HMDARaceDesignationOtherPacificIslanderDescription ?? string.Empty;
                    }
                }
            }

            dataApp.aIsAmericanIndian = hmdaRaceTypes.Contains(HmdaRaceDetailHmdaRaceType.AmericanIndianOrAlaskaNative);
            dataApp.aIsAsian = hmdaRaceTypes.Contains(HmdaRaceDetailHmdaRaceType.Asian);
            dataApp.aIsBlack = hmdaRaceTypes.Contains(HmdaRaceDetailHmdaRaceType.BlackOrAfricanAmerican);
            dataApp.aIsPacificIslander = hmdaRaceTypes.Contains(HmdaRaceDetailHmdaRaceType.NativeHawaiianOrOtherPacificIslander);
            dataApp.aIsWhite = hmdaRaceTypes.Contains(HmdaRaceDetailHmdaRaceType.White);
            // TODO SEAMLESS: Figure out how to set AdditionalRaceDescription

            dataApp.aIsAsianIndian = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.AsianIndian);
            dataApp.aIsChinese = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Chinese);
            dataApp.aIsFilipino = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Filipino);
            dataApp.aIsGuamanianOrChamorro = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.GuamanianOrChamorro);
            dataApp.aIsJapanese = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Japanese);
            dataApp.aIsKorean = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Korean);
            dataApp.aIsNativeHawaiian = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.NativeHawaiian);
            dataApp.aIsSamoan = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Samoan);
            dataApp.aIsVietnamese = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.Vietnamese);
            dataApp.aIsOtherAsian = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.OtherAsian);
            dataApp.aIsOtherPacificIslander = hmdaRaceDesignationTypes.Contains(HmdaRaceDesignationHmdaRaceDesignationType.OtherPacificIslander);

            dataApp.aOtherAsianDescription = otherAsianDescription;
            dataApp.aOtherPacificIslanderDescription = otherPacificIslanderDescription;
        }

        private void Parse(HMDA_ETHNICITY_ORIGINS hmdaEthnicityOrigins, CAppData dataApp)
        {
            dataApp.aIsMexican = hmdaEthnicityOrigins.HmdaEthnicityOriginList.Any(origin => origin.HMDAEthnicityOriginType == HmdaEthnicityOriginHmdaEthnicityOriginType.Mexican);
            dataApp.aIsPuertoRican = hmdaEthnicityOrigins.HmdaEthnicityOriginList.Any(origin => origin.HMDAEthnicityOriginType == HmdaEthnicityOriginHmdaEthnicityOriginType.PuertoRican);
            dataApp.aIsCuban = hmdaEthnicityOrigins.HmdaEthnicityOriginList.Any(origin => origin.HMDAEthnicityOriginType == HmdaEthnicityOriginHmdaEthnicityOriginType.Cuban);
            dataApp.aIsOtherHispanicOrLatino = hmdaEthnicityOrigins.HmdaEthnicityOriginList.Any(origin => origin.HMDAEthnicityOriginType == HmdaEthnicityOriginHmdaEthnicityOriginType.OtherHispanicOrLatino);

            var descriptionContainer = hmdaEthnicityOrigins.HmdaEthnicityOriginList.FirstOrDefault(origin => !string.IsNullOrEmpty(origin.HMDAEthnicityOriginTypeOtherDescription));
            dataApp.aOtherHispanicOrLatinoDescription = descriptionContainer?.HMDAEthnicityOriginTypeOtherDescription ?? string.Empty;
        }

        private void Parse(HMDA_ETHNICITIES hmdaEthnicities, CAppData dataApp)
        {
            var firstHmdaEthnicity = hmdaEthnicities.HmdaEthnicityList.ElementAtOrDefault(0);
            E_aHispanicT? firstEthnicity = null;
            if (firstHmdaEthnicity != null && firstHmdaEthnicity.HMDAEthnicityType != HmdaEthnicityHmdaEthnicityType.Undefined)
            {
                firstEthnicity = firstHmdaEthnicity.HMDAEthnicityType == HmdaEthnicityHmdaEthnicityType.HispanicOrLatino ? E_aHispanicT.Hispanic : E_aHispanicT.NotHispanic;
            }

            var secondHmdaEthnicty = hmdaEthnicities.HmdaEthnicityList.ElementAtOrDefault(1);
            E_aHispanicT? secondEthnicity = null;
            if (secondHmdaEthnicty != null && secondHmdaEthnicty.HMDAEthnicityType != HmdaEthnicityHmdaEthnicityType.Undefined)
            {
                secondEthnicity = secondHmdaEthnicty.HMDAEthnicityType == HmdaEthnicityHmdaEthnicityType.HispanicOrLatino ? E_aHispanicT.Hispanic : E_aHispanicT.NotHispanic;
            }

            if (firstEthnicity.HasValue && !secondEthnicity.HasValue ||
                (firstEthnicity.HasValue && secondEthnicity.HasValue && firstEthnicity == secondEthnicity))
            {
                dataApp.aHispanicT = firstEthnicity.Value;
            }
            else if (!firstEthnicity.HasValue && secondEthnicity.HasValue)
            {
                dataApp.aHispanicT = secondEthnicity.Value;
            }
            else if (firstEthnicity.HasValue && secondEthnicity.HasValue && firstEthnicity != secondEthnicity)
            {
                dataApp.aHispanicT = E_aHispanicT.BothHispanicAndNotHispanic;
            }
            else
            {
                dataApp.aHispanicT = E_aHispanicT.LeaveBlank;
            }
        }

        private void Parse(GOVERNMENT_MONITORING_DETAIL governmentMonitoringDetail, CAppData dataApp)
        {
            E_aIntrvwrMethodT interviewMethod;
            if (InterviewMethodToLo.TryGetValue(governmentMonitoringDetail.ApplicationTakenMethodType, out interviewMethod))
            {
                dataApp.aInterviewMethodT = interviewMethod;
            }
            else
            {
                dataApp.aInterviewMethodT = E_aIntrvwrMethodT.LeaveBlank;
            }

            dataApp.aEthnicityCollectedByObservationOrSurname = this.YesNoToTriState(governmentMonitoringDetail.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator);
            dataApp.aDoesNotWishToProvideEthnicity = governmentMonitoringDetail.HMDAEthnicityRefusalIndicator == E_YesNoIndicator.Y ? true : false;
            dataApp.aSexCollectedByObservationOrSurname = this.YesNoToTriState(governmentMonitoringDetail.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator);
            dataApp.aGender = this.Parse(governmentMonitoringDetail.HMDAGenderType, governmentMonitoringDetail.HMDAGenderRefusalIndicator);
            dataApp.aRaceCollectedByObservationOrSurname = this.YesNoToTriState(governmentMonitoringDetail.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator);
            dataApp.aDoesNotWishToProvideRace = governmentMonitoringDetail.HMDARaceRefusalIndicator == E_YesNoIndicator.Y ? true : false;
        }

        private E_GenderT Parse(GovernmentMonitoringDetailHmdaGenderType genderType, E_YesNoIndicator refusalIndicator)
        {
            switch (genderType)
            {
                case GovernmentMonitoringDetailHmdaGenderType.ApplicantSelectedBothMaleAndFemale:
                    return refusalIndicator == E_YesNoIndicator.Y ? E_GenderT.MaleFemaleNotFurnished : E_GenderT.MaleAndFemale;
                case GovernmentMonitoringDetailHmdaGenderType.Female:
                    return refusalIndicator == E_YesNoIndicator.Y ? E_GenderT.FemaleAndNotFurnished : E_GenderT.Female;
                case GovernmentMonitoringDetailHmdaGenderType.Male:
                    return refusalIndicator == E_YesNoIndicator.Y ? E_GenderT.MaleAndNotFurnished : E_GenderT.Male;
                case GovernmentMonitoringDetailHmdaGenderType.Undefined:
                    return refusalIndicator == E_YesNoIndicator.Y ? E_GenderT.Unfurnished : E_GenderT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(genderType);
            }
        }

        private E_TriState YesNoToTriState(E_YesNoIndicator yesNoIndicator)
        {
            switch (yesNoIndicator)
            {
                case E_YesNoIndicator.N:
                    return E_TriState.No;
                case E_YesNoIndicator.Y:
                    return E_TriState.Yes;
                case E_YesNoIndicator.Undefined:
                    return E_TriState.Blank;
                default:
                    throw new UnhandledEnumException(yesNoIndicator);
            }
        }

        private E_GenderT Parse(E_MismoGovernmentMonitoringGenderType type) 
        {
            switch (type) 
            {
                case E_MismoGovernmentMonitoringGenderType.Female: return E_GenderT.Female;
                case E_MismoGovernmentMonitoringGenderType.Male: return E_GenderT.Male;
                case E_MismoGovernmentMonitoringGenderType.InformationNotProvidedUnknown: return E_GenderT.Unfurnished;
                default: return E_GenderT.LeaveBlank;

            }
        }
        private E_aHispanicT Parse(E_MismoGovernmentMonitoringHmdaEthnicityType type) 
        {
            switch (type) 
            {
                case E_MismoGovernmentMonitoringHmdaEthnicityType.HispanicOrLatino: return E_aHispanicT.Hispanic;
                case E_MismoGovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino: return E_aHispanicT.NotHispanic;
                default:
                    return E_aHispanicT.LeaveBlank;

            }
        }
        private void Parse(MismoDeclaration declaration, CAppData dataApp, bool isBorrower) 
        {
            if (null == declaration)
                return;

            if (isBorrower) 
            {
                dataApp.aBDecAlimony = ParseDeclaration(declaration.AlimonyChildSupportObligationIndicator);
                dataApp.aBDecBankrupt = ParseDeclaration(declaration.BankruptcyIndicator);
                dataApp.aBDecBorrowing = ParseDeclaration(declaration.BorrowedDownPaymentIndicator);
                dataApp.aBDecEndorser = ParseDeclaration(declaration.CoMakerEndorserOfNoteIndicator);
                dataApp.aBDecObligated = ParseDeclaration(declaration.LoanForeclosureOrJudgementIndicator);
                dataApp.aBDecJudgment = ParseDeclaration(declaration.OutstandingJudgementsIndicator);
                dataApp.aBDecLawsuit = ParseDeclaration(declaration.PartyToLawsuitIndicator);
                dataApp.aBDecDelinquent = ParseDeclaration(declaration.PresentlyDelinquentIndicator);
                dataApp.aBDecForeclosure = ParseDeclaration(declaration.PropertyForeclosedPastSevenYearsIndicator);
                switch (declaration.PriorPropertyTitleType) 
                {
                    case E_MismoDeclarationPriorPropertyTitleType.Sole:
                        dataApp.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.S;
                        break;
                    case E_MismoDeclarationPriorPropertyTitleType.JointWithOtherThanSpouse:
                        dataApp.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.O;
                        break;
                    case E_MismoDeclarationPriorPropertyTitleType.JointWithSpouse:
                        dataApp.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.SP;
                        break;
                    default:
                        dataApp.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.Empty;
                        break;
                }
                switch (declaration.PriorPropertyUsageType) 
                {
                    case E_MismoDeclarationPriorPropertyUsageType.Investment:
                        dataApp.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.IP;
                        break;
                    case E_MismoDeclarationPriorPropertyUsageType.PrimaryResidence:
                        dataApp.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.PR;
                        break;
                    case E_MismoDeclarationPriorPropertyUsageType.SecondaryResidence:
                        dataApp.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.SH;
                        break;
                    default:
                        dataApp.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.Empty;
                        break;
                }
                switch (declaration.HomeownerPastThreeYearsType) 
                {
                    case E_MismoDeclarationHomeownerPastThreeYearsType.Yes:
                        dataApp.aBDecPastOwnership = "Y";
                        break;
                    case E_MismoDeclarationHomeownerPastThreeYearsType.No:
                        dataApp.aBDecPastOwnership = "N";
                        break;
                    default:
                        dataApp.aBDecPastOwnership = "";
                        break;
                }
                switch (declaration.IntentToOccupyType) 
                {
                    case E_MismoDeclarationIntentToOccupyType.Yes:
                        dataApp.aBDecOcc = "Y";
                        break;
                    case E_MismoDeclarationIntentToOccupyType.No:
                        dataApp.aBDecOcc = "N";
                        break;
                    default:
                        dataApp.aBDecOcc = "";
                        break;
                }
                switch (declaration.CitizenshipResidencyType) 
                {
                    case E_MismoDeclarationCitizenshipResidencyType.NonPermanentResidentAlien:
                        dataApp.aBDecCitizen = "N";
                        dataApp.aBDecResidency = "N";
                        dataApp.aBDecForeignNational = "N";
                        break;
                    case E_MismoDeclarationCitizenshipResidencyType.NonResidentAlien:
                        dataApp.aBDecCitizen = "N";
                        dataApp.aBDecResidency = "N";
                        dataApp.aBDecForeignNational = "Y";
                        break;
                    case E_MismoDeclarationCitizenshipResidencyType.PermanentResidentAlien:
                        dataApp.aBDecCitizen = "N";
                        dataApp.aBDecResidency = "Y";
                        break;

                    case E_MismoDeclarationCitizenshipResidencyType.USCitizen:
                        dataApp.aBDecCitizen = "Y";
                        dataApp.aBDecResidency = "N";
                        break;
                    default:
                        dataApp.aBDecCitizen = "";
                        dataApp.aBDecResidency = "";
                        dataApp.aBDecForeignNational = "";
                        break;
                }

            } 
            else 
            {
                dataApp.aCDecAlimony = ParseDeclaration(declaration.AlimonyChildSupportObligationIndicator);
                dataApp.aCDecBankrupt = ParseDeclaration(declaration.BankruptcyIndicator);
                dataApp.aCDecBorrowing = ParseDeclaration(declaration.BorrowedDownPaymentIndicator);
                dataApp.aCDecEndorser = ParseDeclaration(declaration.CoMakerEndorserOfNoteIndicator);
                dataApp.aCDecObligated = ParseDeclaration(declaration.LoanForeclosureOrJudgementIndicator);
                dataApp.aCDecJudgment = ParseDeclaration(declaration.OutstandingJudgementsIndicator);
                dataApp.aCDecLawsuit = ParseDeclaration(declaration.PartyToLawsuitIndicator);
                dataApp.aCDecDelinquent = ParseDeclaration(declaration.PresentlyDelinquentIndicator);
                dataApp.aCDecForeclosure = ParseDeclaration(declaration.PropertyForeclosedPastSevenYearsIndicator);
                switch (declaration.PriorPropertyTitleType) 
                {
                    case E_MismoDeclarationPriorPropertyTitleType.Sole:
                        dataApp.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.S;
                        break;
                    case E_MismoDeclarationPriorPropertyTitleType.JointWithOtherThanSpouse:
                        dataApp.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.O;
                        break;
                    case E_MismoDeclarationPriorPropertyTitleType.JointWithSpouse:
                        dataApp.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.SP;
                        break;
                    default:
                        dataApp.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.Empty;
                        break;
                }
                switch (declaration.PriorPropertyUsageType) 
                {
                    case E_MismoDeclarationPriorPropertyUsageType.Investment:
                        dataApp.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.IP;
                        break;
                    case E_MismoDeclarationPriorPropertyUsageType.PrimaryResidence:
                        dataApp.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.PR;
                        break;
                    case E_MismoDeclarationPriorPropertyUsageType.SecondaryResidence:
                        dataApp.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.SH;
                        break;
                    default:
                        dataApp.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.Empty;
                        break;
                }
                switch (declaration.HomeownerPastThreeYearsType) 
                {
                    case E_MismoDeclarationHomeownerPastThreeYearsType.Yes:
                        dataApp.aCDecPastOwnership = "Y";
                        break;
                    case E_MismoDeclarationHomeownerPastThreeYearsType.No:
                        dataApp.aCDecPastOwnership = "N";
                        break;
                    default:
                        dataApp.aCDecPastOwnership = "";
                        break;
                }
                switch (declaration.IntentToOccupyType) 
                {
                    case E_MismoDeclarationIntentToOccupyType.Yes:
                        dataApp.aCDecOcc = "Y";
                        break;
                    case E_MismoDeclarationIntentToOccupyType.No:
                        dataApp.aCDecOcc = "N";
                        break;
                    default:
                        dataApp.aCDecOcc = "";
                        break;
                }
                switch (declaration.CitizenshipResidencyType) 
                {
                    case E_MismoDeclarationCitizenshipResidencyType.NonPermanentResidentAlien:
                    case E_MismoDeclarationCitizenshipResidencyType.NonResidentAlien:
                        dataApp.aCDecCitizen = "N";
                        dataApp.aCDecResidency = "N";
                        break;
                    case E_MismoDeclarationCitizenshipResidencyType.PermanentResidentAlien:
                        dataApp.aCDecCitizen = "N";
                        dataApp.aCDecResidency = "Y";
                        break;
                    case E_MismoDeclarationCitizenshipResidencyType.USCitizen:
                        dataApp.aCDecCitizen = "Y";
                        dataApp.aCDecResidency = "N";
                        break;
                    default:
                        dataApp.aCDecCitizen = "";
                        dataApp.aCDecResidency = "";
                        break;
                }

            }
        }

        private void Parse(MismoLoanProductData loanProductData) 
        {
            if (null == loanProductData)
                return;

            Parse(loanProductData.Arm);
            Parse(loanProductData.LoanFeatures);

            foreach (MismoBuydown buydown in loanProductData.Buydown) 
            {
                if (null != buydown) 
                {
                    // 5/19/2008 dd - LP only return one occurence of BuyDown element. TODO.
                }
            }

            foreach (MismoRateAdjustment rateAdjustment in loanProductData.RateAdjustment) 
            {
                if (null != rateAdjustment) 
                {
                    Parse(rateAdjustment);
                    break;
                }
            }

            foreach (MismoPaymentAdjustment paymentAdjustment in loanProductData.PaymentAdjustment) 
            {
                if (null != paymentAdjustment) 
                {
                    Parse(paymentAdjustment);
                    break;
                }
            }


        }

        private void Parse(MismoLoanFeatures loanFeatures) 
        {
            if (null == loanFeatures)
                return;

            m_dataLoan.sDue_rep = loanFeatures.BalloonLoanMaturityTermMonths;
             // 4/11/2008 dd - GseProjectClassificationType No longer support base on March 2008 Bulletin.
//            switch (loanFeatures.GseProjectClassificationType) 
//            {
//                case E_MismoLoanFeaturesGseProjectClassificationType.A_IIICondominium:
//                    m_dataLoan.sCondoProjClassT = E_sCondoProjClassT.A_IIICondominium;
//                    break;
//                case E_MismoLoanFeaturesGseProjectClassificationType.ApprovedFHA_VACondominiumProjectOrSpotLoan:
//                    m_dataLoan.sCondoProjClassT = E_sCondoProjClassT.ApprovedFHA_VACondominiumProjectOrSpotLoan;
//                    break;
//
//                case E_MismoLoanFeaturesGseProjectClassificationType.B_IICondominium:
//                    m_dataLoan.sCondoProjClassT = E_sCondoProjClassT.B_IICondominium;
//                    break;
//
//                case E_MismoLoanFeaturesGseProjectClassificationType.C_ICondominium:
//                    m_dataLoan.sCondoProjClassT = E_sCondoProjClassT.C_ICondominium;
//                    break;
//                default:
//                    m_dataLoan.sCondoProjClassT = E_sCondoProjClassT.LeaveBlank;
//                    break;
//            }

            m_dataLoan.sHelocCreditLimit_rep = loanFeatures.HelocMaximumBalanceAmount;
            switch (loanFeatures.LienPriorityType) 
            {
                case E_MismoLoanFeaturesLienPriorityType.FirstLien:
                    m_dataLoan.sLienPosT = E_sLienPosT.First;
                    break;
                case E_MismoLoanFeaturesLienPriorityType.SecondLien:
                    m_dataLoan.sLienPosT = E_sLienPosT.Second;
                    break;
            }
            m_dataLoan.sPmtAdjMaxBalPc_rep = loanFeatures.NegativeAmortizationLimitPercent;
            switch (loanFeatures.NegativeAmortizationType) 
            {
                case E_MismoLoanFeaturesNegativeAmortizationType.NoNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.NoNegAmort;
                    break;
                case E_MismoLoanFeaturesNegativeAmortizationType.PotentialNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.PotentialNegAmort;
                    break;

                case E_MismoLoanFeaturesNegativeAmortizationType.ScheduledNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.ScheduledNegAmort;
                    break;
                case E_MismoLoanFeaturesNegativeAmortizationType.Undefined:
                default:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.LeaveBlank;
                    break;

            }
            m_dataLoan.sBuydown = loanFeatures.BuydownTemporarySubsidyIndicator == E_YesNoIndicator.Y;
            if (!string.IsNullOrEmpty(loanFeatures.ProductDescription))
            {
                // 1/27/2010 dd - OPM 45219 Only override sLpTemplateNm when ProductDescription is not blank.
                m_dataLoan.sLpTemplateNm = loanFeatures.ProductDescription;
            }
            m_dataLoan.sFredAffordProgId = DataAccess.Tools.LoanProspectorIdTosFredAffordProdId(loanFeatures.FreOfferingIdentifier);
//            m_dataLoan.sFreddieDocT = Parse(loanFeatures.LoanDocumentationType);// 11/25/2008 dd - No longer support.
            if (loanFeatures.ProductName == null) 
            {
                m_dataLoan.sFreddieConstructionT = E_sFreddieConstructionT.LeaveBlank;
            } 
            else 
            {
                switch (loanFeatures.ProductName.ToLower()) 
                {
                    case "newlybuilt":
                        m_dataLoan.sFreddieConstructionT = E_sFreddieConstructionT.NewlyBuilt;
                        break;
                    case "constructionconversion":
                        m_dataLoan.sFreddieConstructionT = E_sFreddieConstructionT.ConstructionConversion;
                        break;
                    default:
                        m_dataLoan.sFreddieConstructionT = E_sFreddieConstructionT.LeaveBlank;
                        break;
                }
            }

        }
        private E_sFreddieDocT Parse(E_MismoLoanFeaturesLoanDocumentationType type) 
        {
//                            case E_sFreddieDocT.FullDocumentation: return E_MismoLoanFeaturesLoanDocumentationType.FullDocumentation;
//                case E_sFreddieDocT.LeaveBlank: return E_MismoLoanFeaturesLoanDocumentationType.Undefined;
//                case E_sFreddieDocT.NoDepEmpIncVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
//                case E_sFreddieDocT.NoDepositVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerification;
//                case E_sFreddieDocT.NoDoc: return E_MismoLoanFeaturesLoanDocumentationType.NoDocumentation;
//                case E_sFreddieDocT.NoEmpIncVerif: return E_MismoLoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification;

            switch (type) 
            {
                case E_MismoLoanFeaturesLoanDocumentationType.FullDocumentation: return E_sFreddieDocT.FullDocumentation;
                case E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification: return E_sFreddieDocT.NoDepEmpIncVerif;
                case E_MismoLoanFeaturesLoanDocumentationType.NoDepositVerification: return E_sFreddieDocT.NoDepositVerif;
                case E_MismoLoanFeaturesLoanDocumentationType.NoDocumentation: return E_sFreddieDocT.NoDoc;
                case E_MismoLoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification: return E_sFreddieDocT.NoEmpIncVerif;
                default:
                    return E_sFreddieDocT.LeaveBlank;

            }
        }
        private void Parse(MismoRateAdjustment rateAdjustment) 
        {
            if (null == rateAdjustment)
                return;

            m_dataLoan.sRAdj1stCapMon_rep = rateAdjustment.FirstRateAdjustmentMonths;
            m_dataLoan.sRAdjCapR_rep = rateAdjustment.SubsequentCapPercent;
            m_dataLoan.sRAdjCapMon_rep = rateAdjustment.SubsequentRateAdjustmentMonths;
        }
        private void Parse(MismoPaymentAdjustment paymentAdjustment) 
        {
            if (null == paymentAdjustment)
                return;

            m_dataLoan.sPmtAdjCapMon_rep = paymentAdjustment.SubsequentPaymentAdjustmentMonths;
            m_dataLoan.sPmtAdjCapR_rep = paymentAdjustment.PeriodicCapPercent;
        }

        private void Parse(MismoClosingAgent closingAgent) 
        {
            // 10/9/2014 AV - 194182 Escrow company name cleared out when exporting back to LP
            if (null == closingAgent || String.IsNullOrEmpty(closingAgent.UnparsedName))
            {
                return;
            }
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = closingAgent.UnparsedName;

            agent.Update();


        }

        private void Parse(MismoTransactionDetail transactionDetail) 
        {
            if (null == transactionDetail)
                return;

            m_dataLoan.sAltCost_rep = transactionDetail.AlterationsImprovementsAndRepairsAmount;
            m_dataLoan.sLDiscnt1003Lckd = true;
            m_dataLoan.sLDiscnt1003_rep = transactionDetail.BorrowerPaidDiscountPointsTotalAmount;
            m_dataLoan.sTotEstCcNoDiscnt1003_rep = transactionDetail.EstimatedClosingCostsAmount;
            //m_dataLoan.sFfUfmipFinanced_rep = transactionDetail.MiAndFundingFeeFinancedAmount;
            m_dataLoan.sFfUfmip1003Lckd = true;
            m_dataLoan.sFfUfmip1003_rep = transactionDetail.MiAndFundingFeeTotalAmount;
            m_dataLoan.sTotEstPp1003_rep = transactionDetail.PrepaidItemsEstimatedAmount;
            m_dataLoan.sTotEstPp1003Lckd = true;
            m_dataLoan.sPurchPrice_rep = transactionDetail.PurchasePriceAmount;
            m_dataLoan.sRefPdOffAmt1003Lckd = true;
            m_dataLoan.sRefPdOffAmt1003_rep = transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount;
            m_dataLoan.sTotCcPbsLocked = true;
            m_dataLoan.sTotCcPbs_rep = transactionDetail.SellerPaidClosingCostsAmount;
            m_dataLoan.sFredieReservesAmt_rep = transactionDetail.FreReservesAmount;
            m_dataLoan.sFHASalesConcessions_rep = transactionDetail.SalesConcessionAmount;

            if (transactionDetail.SubordinateLienAmount != "") 
            {
                m_dataLoan.sIsOFinNew = true;
                if (m_dataLoan.sLienPosT == E_sLienPosT.First) 
                {
                    m_dataLoan.sConcurSubFin_rep = transactionDetail.SubordinateLienAmount;
                } 
                else if (m_dataLoan.sLienPosT == E_sLienPosT.Second) 
                {
                    m_dataLoan.s1stMtgOrigLAmt_rep = transactionDetail.SubordinateLienAmount;
                }
            }
            m_dataLoan.sHelocBal_rep = transactionDetail.SubordinateLienHelocAmount;

            if (m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                ImportIntoAdjustments(transactionDetail);
            }
            else
            {
                ImportInto1003LineL(transactionDetail);
            }
        }

        private void ImportIntoAdjustments(MismoTransactionDetail transactionDetail)
        {
            if (false == m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming Error. Should not call ImportIntoAdjustments unless sLoads1003LineLFromAdjustments");
            }

            m_dataLoan.sAdjustmentList.ClearAdjustmentsThatPopulateTo1003();
            m_dataLoan.sProrationList.ClearProrationsIfPopulatingTo1003();

            foreach (MismoPurchaseCredit purchaseCredit in transactionDetail.PurchaseCredit)
            {
                m_dataLoan.sAdjustmentList.AddWithLoanVersion(FromPurchaseCreditType(purchaseCredit.Type), purchaseCredit.Amount);
            }

            m_dataLoan.sAdjustmentList = m_dataLoan.sAdjustmentList;
        }

        private void ImportInto1003LineL(MismoTransactionDetail transactionDetail)
        {
            if (m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming Error. Should not call ImportInto1003LineL when sLoads1003LineLFromAdjustments");
            }

            int purchaseCreditIndex = 0;
            foreach (MismoPurchaseCredit purchaseCredit in transactionDetail.PurchaseCredit)
            {
                if (null != purchaseCredit)
                {
                    if (purchaseCreditIndex == 0)
                    {
                        m_dataLoan.sOCredit1Lckd = true;
                        m_dataLoan.sOCredit1Desc = FromPurchaseCreditType(purchaseCredit.Type);
                        m_dataLoan.sOCredit1Amt_rep = purchaseCredit.Amount;
                    }
                    else if (purchaseCreditIndex == 1)
                    {
                        //m_dataLoan.sOCredit2Lckd = true;
                        m_dataLoan.sOCredit2Desc = FromPurchaseCreditType(purchaseCredit.Type);
                        m_dataLoan.sOCredit2Amt_rep = purchaseCredit.Amount;
                    }
                    else if (purchaseCreditIndex == 2)
                    {
                        m_dataLoan.sOCredit3Desc = FromPurchaseCreditType(purchaseCredit.Type);
                        m_dataLoan.sOCredit3Amt_rep = purchaseCredit.Amount;
                    }
                    else if (purchaseCreditIndex == 3)
                    {
                        m_dataLoan.sOCredit4Desc = FromPurchaseCreditType(purchaseCredit.Type);
                        m_dataLoan.sOCredit4Amt_rep = purchaseCredit.Amount;
                    }
                    purchaseCreditIndex++;
                }
            }

            // opm 202414 sk - wipe any not in the import.
            // The reason to do this is to avoid duplicates since the export/import doesn't label the credit.
            for (; purchaseCreditIndex < 4; purchaseCreditIndex++)
            {
                if (purchaseCreditIndex == 0)
                {
                    m_dataLoan.sOCredit1Lckd = true;
                    m_dataLoan.sOCredit1Desc = string.Empty;
                    m_dataLoan.sOCredit1Amt = 0.00M;
                }
                else if (purchaseCreditIndex == 1)
                {
                    m_dataLoan.sOCredit2Desc = string.Empty;
                    m_dataLoan.sOCredit2Amt = 0.00M;
                }
                else if (purchaseCreditIndex == 2)
                {
                    m_dataLoan.sOCredit3Desc = string.Empty;
                    m_dataLoan.sOCredit3Amt = 0.00M;
                }
                else if (purchaseCreditIndex == 3)
                {
                    m_dataLoan.sOCredit4Desc = string.Empty;
                    m_dataLoan.sOCredit4Amt = 0.00M;
                }
            }
        }

        private string FromPurchaseCreditType(E_MismoPurchaseCreditType type) 
        {
            switch (type) 
            {
                case E_MismoPurchaseCreditType.EarnestMoney:
                    return "cash deposit on sales contract";
                case E_MismoPurchaseCreditType.EmployerAssistedHousing:
                    return "employer assisted housing";
                case E_MismoPurchaseCreditType.LeasePurchaseFund:
                    return "lease purchase fund";
                case E_MismoPurchaseCreditType.RelocationFunds:
                    return "relocation funds";
                default:
                    return "";
            }

        }
        private void Parse(MismoInterviewerInformation interviewerInformation) 
        {
            if (null == interviewerInformation)
                return;

            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);

            interviewer.City = interviewerInformation.InterviewersEmployerCity;
            interviewer.CompanyName = interviewerInformation.InterviewersEmployerName;
            interviewer.Zip = interviewerInformation.InterviewersEmployerPostalCode;
            interviewer.State = interviewerInformation.InterviewersEmployerState;
            interviewer.StreetAddr = interviewerInformation.InterviewersEmployerStreetAddress;
            interviewer.PreparerName = interviewerInformation.InterviewersEmployerName;
            interviewer.Phone = interviewerInformation.InterviewersTelephoneNumber;

            // 2/6/2012 dd - OPM 76420 - There may be scenario where LP will not return
            // interviewer method type to us. In this case just ignore instead of override the existing
            // value.
            E_aIntrvwrMethodT interviewMethod;
            if (InterviewMethodToLo.TryGetValue(interviewerInformation.ApplicationTakenMethodType, out interviewMethod))
            {
                // This import only supports one interview method per applicant, so we set both.
                m_dataLoan.GetAppData(0).aBInterviewMethodT = interviewMethod;
                m_dataLoan.GetAppData(0).aCInterviewMethodT = interviewMethod;
            }
                    
            interviewer.Update();
        }

        /// <summary>
        /// Maps LP's interview method types to the LQB equivalent.
        /// </summary>
        private static Dictionary<ApplicationTakenMethodType, E_aIntrvwrMethodT> InterviewMethodToLo =
            new Dictionary<ApplicationTakenMethodType, E_aIntrvwrMethodT>
            {
                { ApplicationTakenMethodType.FaceToFace, E_aIntrvwrMethodT.FaceToFace },
                { ApplicationTakenMethodType.Internet, E_aIntrvwrMethodT.Internet },
                { ApplicationTakenMethodType.Mail, E_aIntrvwrMethodT.ByMail },
                { ApplicationTakenMethodType.Telephone, E_aIntrvwrMethodT.ByTelephone }
            };

        private void Parse(MismoDownPayment downPayment) 
        {
            if (null == downPayment)
                return;

            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase) 
            {
                m_dataLoan.sEquityCalc_rep = downPayment.Amount;
                m_dataLoan.sDwnPmtSrcExplain = downPayment.SourceDescription;
                switch (downPayment.Type) 
                {
                    case E_MismoDownPaymentType.CheckingSavings:
                        m_dataLoan.sDwnPmtSrc = "checking/savings";
                        break;
                    case E_MismoDownPaymentType.GiftFunds:
                        m_dataLoan.sDwnPmtSrc = "gift funds";
                        break;
                    case E_MismoDownPaymentType.StocksAndBonds:
                        m_dataLoan.sDwnPmtSrc = "stocks & bonds";
                        break;
                    case E_MismoDownPaymentType.LotEquity:
                        m_dataLoan.sDwnPmtSrc = "lot equity";
                        break;
                    case E_MismoDownPaymentType.BridgeLoan:
                        m_dataLoan.sDwnPmtSrc = "bridge loan";
                        break;
                    case E_MismoDownPaymentType.TrustFunds:
                        m_dataLoan.sDwnPmtSrc = "trust funds";
                        break;
                    case E_MismoDownPaymentType.RetirementFunds:
                        m_dataLoan.sDwnPmtSrc = "retirement funds";
                        break;
                    case E_MismoDownPaymentType.LifeInsuranceCashValue:
                        m_dataLoan.sDwnPmtSrc = "life insurance cash value";
                        break;
                    case E_MismoDownPaymentType.SaleOfChattel:
                        m_dataLoan.sDwnPmtSrc = "sale of chattel";
                        break;
                    case E_MismoDownPaymentType.TradeEquity:
                        m_dataLoan.sDwnPmtSrc = "trade equity";
                        break;
                    case E_MismoDownPaymentType.SweatEquity:
                        m_dataLoan.sDwnPmtSrc = "sweat equity";
                        break;
                    case E_MismoDownPaymentType.CashOnHand:
                        m_dataLoan.sDwnPmtSrc = "cash on hand";
                        break;
                    case E_MismoDownPaymentType.DepositOnSalesContract:
                        m_dataLoan.sDwnPmtSrc = "deposit on sales contract";
                        break;
                    case E_MismoDownPaymentType.EquityOnPendingSale:
                        m_dataLoan.sDwnPmtSrc = "equity from pending sale";
                        break;
                    case E_MismoDownPaymentType.EquityOnSubjectProperty:
                        m_dataLoan.sDwnPmtSrc = "equity from subject property";
                        break;
                    case E_MismoDownPaymentType.EquityOnSoldProperty:
                        m_dataLoan.sDwnPmtSrc = "equity on sold property";
                        break;
                    case E_MismoDownPaymentType.OtherTypeOfDownPayment:
                        m_dataLoan.sDwnPmtSrc = "other type of down payment";
                        break;
                    case E_MismoDownPaymentType.RentWithOptionToPurchase:
                        m_dataLoan.sDwnPmtSrc = "rent with option to purchase";
                        break;
                    case E_MismoDownPaymentType.SecuredBorrowedFunds:
                        m_dataLoan.sDwnPmtSrc = "secured borrowed funds";
                        break;
                    case E_MismoDownPaymentType.UnsecuredBorrowedFunds:
                        m_dataLoan.sDwnPmtSrc = "unsecured borrowed funds";
                        break;
                    case E_MismoDownPaymentType.ForgivableSecuredLoan:
                        m_dataLoan.sDwnPmtSrc = "forgivable secured loan";
                        break;
                    default:
                        m_dataLoan.sDwnPmtSrc = "";
                        break;

                }
            }
        }
        private void Parse(MismoLoanPurpose loanPurpose, bool? isPropertyUnderConstruction) 
        {
            if (null == loanPurpose)
                return;
            
            m_dataLoan.GetAppData(0).aManner = loanPurpose.GseTitleMannerHeldDescription;
            m_dataLoan.sLeaseHoldExpireD_rep = loanPurpose.PropertyLeaseholdExpirationDate;
            switch (loanPurpose.Type)
            {
                case E_MismoLoanPurposeType.ConstructionOnly:
                    m_dataLoan.sLPurposeT = E_sLPurposeT.Construct;
                    break;
                case E_MismoLoanPurposeType.ConstructionToPermanent:
                    m_dataLoan.sLPurposeT = E_sLPurposeT.ConstructPerm;
                    break;
                case E_MismoLoanPurposeType.Other:
                    m_dataLoan.sLPurposeT = E_sLPurposeT.Other;
                    m_dataLoan.sOLPurposeDesc = loanPurpose.OtherLoanPurposeDescription;
                    break;
                case E_MismoLoanPurposeType.Purchase:
                    m_dataLoan.sLPurposeT = (isPropertyUnderConstruction.HasValue && isPropertyUnderConstruction.Value)
                        ? E_sLPurposeT.ConstructPerm
                        : E_sLPurposeT.Purchase;
                    break;
                case E_MismoLoanPurposeType.Refinance:
                    m_dataLoan.sLPurposeT = (isPropertyUnderConstruction.HasValue && isPropertyUnderConstruction.Value)
                        ? E_sLPurposeT.ConstructPerm
                        : E_sLPurposeT.Refin;
                    break;
                case E_MismoLoanPurposeType.Undefined:
                case E_MismoLoanPurposeType.Unknown:
                default:
                    m_dataLoan.sLPurposeT = E_sLPurposeT.Other;
                    break;
            }

            switch (loanPurpose.PropertyRightsType) 
            {
                case E_MismoLoanPurposePropertyRightsType.FeeSimple:
                    m_dataLoan.sEstateHeldT = E_sEstateHeldT.FeeSimple;
                    break;
                case E_MismoLoanPurposePropertyRightsType.Leasehold:
                    m_dataLoan.sEstateHeldT = E_sEstateHeldT.LeaseHold;
                    break;
            }
            switch (loanPurpose.PropertyUsageType) 
            {
                case E_MismoLoanPurposePropertyUsageType.PrimaryResidence:
                    m_dataLoan.GetAppData(0).aOccT = E_aOccT.PrimaryResidence;
                    break;
                case E_MismoLoanPurposePropertyUsageType.SecondHome:
                    m_dataLoan.GetAppData(0).aOccT = E_aOccT.SecondaryResidence;
                    break;
                case E_MismoLoanPurposePropertyUsageType.Investment:
                    m_dataLoan.GetAppData(0).aOccT = E_aOccT.Investment;
                    break;
            }

            Parse(loanPurpose.ConstructionRefinanceData);
        }
        private void Parse(MismoConstructionRefinanceData constructionRefinanceData) 
        {
            if (null == constructionRefinanceData)
                return;
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                m_dataLoan.sSpOrigC_rep = constructionRefinanceData.PropertyOriginalCostAmount;
                m_dataLoan.sSpAcqYr = constructionRefinanceData.PropertyAcquiredYear;
                m_dataLoan.sSpLien_rep = constructionRefinanceData.PropertyExistingLienAmount;
                m_dataLoan.sSpImprovC_rep = constructionRefinanceData.RefinanceImprovementCostsAmount;
                m_dataLoan.sSpImprovDesc = constructionRefinanceData.RefinanceProposedImprovementsDescription;
                if (constructionRefinanceData.GseRefinancePurposeType == E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation ||
                    constructionRefinanceData.GseRefinancePurposeType == E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement  ||
                    constructionRefinanceData.GseRefinancePurposeType == E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited ||
                    constructionRefinanceData.GseRefinancePurposeType == E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther ) 
                {
                    m_dataLoan.sLPurposeT = E_sLPurposeT.RefinCashout;
                }
                m_dataLoan.sProdCashoutAmt_rep = constructionRefinanceData.FreCashOutAmount;

                m_dataLoan.sPayingOffSubordinate = constructionRefinanceData.SecondaryFinancingRefinanceIndicator == E_YesNoIndicator.Y;
                switch (constructionRefinanceData.RefinanceImprovementsType) 
                {
                    case E_MismoConstructionRefinanceDataRefinanceImprovementsType.Made:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.Made;
                        break;
                    case E_MismoConstructionRefinanceDataRefinanceImprovementsType.ToBeMade:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.ToBeMade;
                        break;
                    default:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.LeaveBlank;
                        break;
                }
                m_dataLoan.sGseRefPurposeT = Parse(constructionRefinanceData.GseRefinancePurposeType);
                switch (constructionRefinanceData.GseRefinancePurposeType) 
                {
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm:
                        m_dataLoan.sRefPurpose = "no cash-out rate/term";
                        break;
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited:
                        m_dataLoan.sRefPurpose = "limited cash-out rate/term";
                        break;
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement:
                        m_dataLoan.sRefPurpose = "cash-out/home improvement";
                        break;
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation:
                        m_dataLoan.sRefPurpose = "cash-out/debt consolidation";
                        break;
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther:
                        m_dataLoan.sRefPurpose = "cash-out/other";
                        break;
                    case E_MismoConstructionRefinanceDataGseRefinancePurposeType.Other:
                        m_dataLoan.sRefPurpose = constructionRefinanceData.GseRefinancePurposeTypeOtherDescription;
                        break;

                }

            } 
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm) 
            {
                m_dataLoan.sLotImprovC_rep = constructionRefinanceData.ConstructionImprovementCostsAmount;
                m_dataLoan.sPresentValOfLot_rep = constructionRefinanceData.LandEstimatedValueAmount;
                m_dataLoan.sLotOrigC_rep = constructionRefinanceData.PropertyOriginalCostAmount;
                m_dataLoan.sLotAcqYr = constructionRefinanceData.PropertyAcquiredYear;
                m_dataLoan.sLotLien_rep = constructionRefinanceData.PropertyExistingLienAmount;
            }
        }
        private void Parse(MismoArm arm) 
        {
            if (null == arm)
                return;

            m_dataLoan.sRAdjMarginR_rep = arm.IndexMarginPercent;
            m_dataLoan.sQualIRLckd = true;
            m_dataLoan.sQualIR_rep = arm.QualifyingRatePercent;
            m_dataLoan.sRAdjLifeCapR_rep = arm.RateAdjustmentLifetimeCapPercent;
            switch (arm.IndexType) 
            {
                case E_MismoArmIndexType.EleventhDistrictCostOfFunds:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.EleventhDistrictCostOfFunds;
                    break;
                case E_MismoArmIndexType.LIBOR:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.LIBOR;
                    break;
                case E_MismoArmIndexType.NationalMonthlyMedianCostOfFunds:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds;
                    break;
                case E_MismoArmIndexType.OneYearTreasury:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.OneYearTreasury;
                    break;
                case E_MismoArmIndexType.Other:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.Other;
                    break;
                case E_MismoArmIndexType.SixMonthTreasury:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.SixMonthTreasury;
                    break;
                case E_MismoArmIndexType.ThreeYearTreasury:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.ThreeYearTreasury;
                    break;
                default:
                    m_dataLoan.sFreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
                    break;


            }
            m_dataLoan.sFreddieArmIndexTLckd = true;

            if (arm.IndexCurrentValuePercent != null && arm.IndexCurrentValuePercent != "" && arm.IndexCurrentValuePercent != "0") 
            {
                m_dataLoan.sRAdjWorstIndex = false;
                m_dataLoan.sRAdjIndexR_rep = arm.IndexCurrentValuePercent;
            }
        }
        private E_sGseRefPurposeT Parse(E_MismoConstructionRefinanceDataGseRefinancePurposeType type) 
        {
            switch (type) 
            {
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation: return E_sGseRefPurposeT.CashOutDebtConsolidation;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement: return E_sGseRefPurposeT.CashOutHomeImprovement;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited: return E_sGseRefPurposeT.CashOutLimited;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.CashOutOther: return E_sGseRefPurposeT.CashOutOther;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm: return E_sGseRefPurposeT.ChangeInRateTerm;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.Undefined: return E_sGseRefPurposeT.LeaveBlank;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance: return E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFREOwnedRefinance: return E_sGseRefPurposeT.NoCashOutFREOwnedRefinance;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther: return E_sGseRefPurposeT.NoCashOutOther;
                case E_MismoConstructionRefinanceDataGseRefinancePurposeType.NoCashOutStreamlinedRefinance: return E_sGseRefPurposeT.NoCashOutStreamlinedRefinance;
                default:
                    return E_sGseRefPurposeT.LeaveBlank;
            }
        }


        private void Parse(MismoLoanProcessingMessage loanProcessingMessage) 
        {
            if (null == loanProcessingMessage) 
                return;

            // 3/13/2008 dd - Only convert documentation messages to condition.
            if (loanProcessingMessage.Type == E_MismoLoanProcessingMessageType.DocumentationMessages)
            {
                BrokerDB db = BrokerDB.RetrieveById(m_dataLoan.sBrokerId);
                if (!db.IsImportDoDuLpFindingsAsConditions)
                {
                    return;
                }

                if (db.IsUseNewTaskSystem)
                {

                    IConditionImporter importer = m_dataLoan.GetConditionImporter("LP");
                    importer.IsSkipCategoryCheck = true; // 9/7/2011 dd - OPM 70845 don't check category for condition uniqueness.

                    foreach (MismoFeedbackMessage feedbackMessage in loanProcessingMessage.FeedbackMessage)
                    {
                        if (null != feedbackMessage)
                        {
                            importer.AddUniqueAus(feedbackMessage.MessageText);
                        }
                    }
                }
                else
                {
                    LoanConditionSetObsolete conditions = new LoanConditionSetObsolete();
                    conditions.RetrieveAll(m_dataLoan.sLId, true);

                    foreach (MismoFeedbackMessage feedbackMessage in loanProcessingMessage.FeedbackMessage)
                    {
                        if (null != feedbackMessage)
                        {
                            conditions.AddUniqueCondition("LP", feedbackMessage.MessageText);
                        }
                    }
                    conditions.Save(m_dataLoan.sBrokerId);
                }
            }

        }

        private void Parse(MismoProperty property) 
        {
            if (null == property)
                return;

            m_dataLoan.sSpCity = property.City;
            m_dataLoan.sUnitsNum_rep = property.FinancedNumberOfUnits;
            m_dataLoan.sSpZip = property.PostalCode;
            m_dataLoan.sSpState = property.State;
            m_dataLoan.sSpAddr = property.StreetAddress;
            m_dataLoan.sYrBuilt = property.StructureBuiltYear;
            m_dataLoan.sSpCounty = property.County;
            string legalDesc = "";
            foreach (MismoLegalDescription legalDescription in property.LegalDescription) 
            {
                if (null != legalDescription)
                    legalDesc += legalDescription.TextDescription;
            }
            m_dataLoan.sSpLegalDesc = legalDesc;
            bool bIsPUD = m_dataLoan.sSpIsInPud;
            if (property.PlannedUnitDevelopmentIndicator != E_YesNoIndicator.Undefined) 
            {
                m_dataLoan.sSpIsInPud = bIsPUD = property.PlannedUnitDevelopmentIndicator == E_YesNoIndicator.Y;
            }
            if (property.BuildingStatusType != E_MismoPropertyBuildingStatusType.Undefined) 
            {
                m_dataLoan.sBuildingStatusT = FromMismo(property.BuildingStatusType);
            }
            MismoCategory lastCategory = null;
            foreach (MismoCategory category in property.Category) 
            {
                if (null != category) 
                {
                    m_dataLoan.sGseSpT = Parse(category.Type);
                    lastCategory = category;
                }
            }
            // sk 105419; 02/15/13 BB: bIsPUD is used rather than m_dataLoan.sSpIsInPud here because the set statement for sGseSpT above may revert sSpIsInPud
            if (lastCategory != null && bIsPUD)
            {
                switch (lastCategory.Type)
                {
                    case E_MismoCategoryType.Undefined:
                    case E_MismoCategoryType.Attached:
                    case E_MismoCategoryType.TownhouseRowhouse:
                    case E_MismoCategoryType.Other:
                    case E_MismoCategoryType.Detached:
                        m_dataLoan.sProdSpT = E_sProdSpT.PUD;
                        m_dataLoan.sGseSpT = E_sGseSpT.PUD;
                        m_dataLoan.sFannieSpT = E_sFannieSpT.PUD;
                        break;
                    default:
                        break;
                }
            }

            m_dataLoan.sSpAppraisalFormT = this.Parse(property.Valuation.AppraisalFormType);
            m_dataLoan.sSpValuationMethodT = this.Parse(property.Valuation.MethodType);
        }

        private E_sSpValuationMethodT Parse(E_MismoValuationMethodType methodType)
        {
            switch (methodType)
            {
                case E_MismoValuationMethodType.AutomatedValuationModel:
                    return E_sSpValuationMethodT.AutomatedValuationModel;
                case E_MismoValuationMethodType.BrokerPriceOpinion:
                    return E_sSpValuationMethodT.DesktopAppraisal;
                case E_MismoValuationMethodType.DriveBy:
                    return E_sSpValuationMethodT.DriveBy;
                case E_MismoValuationMethodType.FullAppraisal:
                    return E_sSpValuationMethodT.FullAppraisal;
                case E_MismoValuationMethodType.PriorAppraisalUsed:
                    return E_sSpValuationMethodT.PriorAppraisalUsed;
                case E_MismoValuationMethodType.Undefined:
                case E_MismoValuationMethodType.None:
                    return E_sSpValuationMethodT.None;
                case E_MismoValuationMethodType.Other:
                    return E_sSpValuationMethodT.Other;
                default:
                    throw new UnhandledEnumException(methodType);
            }
        }

        private E_sSpAppraisalFormT Parse(E_MismoValuationAppraisalFormType appraisalFormType)
        {
            switch (appraisalFormType)
            {
                case E_MismoValuationAppraisalFormType.FNM1004FRE70:
                    return E_sSpAppraisalFormT.UniformResidentialAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM1004CFRE70B:
                    return E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM1004DFRE442:
                    return E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport;
                case E_MismoValuationAppraisalFormType.FNM1025FRE72:
                    return E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM1073FRE465:
                    return E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM1075FRE466:
                    return E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM2000FRE1032:
                    return E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport;
                case E_MismoValuationAppraisalFormType.FNM2000AFRE1072:
                    return E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal;
                case E_MismoValuationAppraisalFormType.FNM2055FRE2055:
                    return E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM2075:
                    return E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport;
                case E_MismoValuationAppraisalFormType.FNM2090:
                    return E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport;
                case E_MismoValuationAppraisalFormType.FNM2095:
                    return E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport;
                case E_MismoValuationAppraisalFormType.FRE2070:
                    return E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability;
                case E_MismoValuationAppraisalFormType.Other:
                    return E_sSpAppraisalFormT.Other;
                case E_MismoValuationAppraisalFormType.Undefined:
                    return E_sSpAppraisalFormT.Blank;
                default:
                    throw new UnhandledEnumException(appraisalFormType);
            }
        }

        private E_sBuydownContributorT Parse(E_MismoBuydownContributorType type) 
        {
            switch (type) 
            {
                case E_MismoBuydownContributorType.Borrower: return E_sBuydownContributorT.Borrower;
                case E_MismoBuydownContributorType.Builder: return E_sBuydownContributorT.Builder;
                case E_MismoBuydownContributorType.Employer: return E_sBuydownContributorT.Employer;
                case E_MismoBuydownContributorType.Financed: return E_sBuydownContributorT.LenderPremiumFinanced;
                case E_MismoBuydownContributorType.LenderPremiumFinanced: return E_sBuydownContributorT.LenderPremiumFinanced;
                case E_MismoBuydownContributorType.NonParentRelative: return E_sBuydownContributorT.NonParentRelative;
                case E_MismoBuydownContributorType.Other: return E_sBuydownContributorT.Other;
                case E_MismoBuydownContributorType.Parent: return E_sBuydownContributorT.Parent;
                case E_MismoBuydownContributorType.Seller: return E_sBuydownContributorT.Seller;
                case E_MismoBuydownContributorType.Unassigned: return E_sBuydownContributorT.Unassigned;
                case E_MismoBuydownContributorType.UnrelatedFriend: return E_sBuydownContributorT.UnrelatedFriend;
                case E_MismoBuydownContributorType.Undefined:
                default:
                    return E_sBuydownContributorT.LeaveBlank;
            }
        }
        private E_sGseSpT Parse(E_MismoCategoryType type) 
        {
            switch (type) 
            {
                case E_MismoCategoryType.Attached: return E_sGseSpT.Attached;
                case E_MismoCategoryType.Detached: return E_sGseSpT.Detached;
                case E_MismoCategoryType.LowRise: return E_sGseSpT.Condominium;
                case E_MismoCategoryType.HighRise: return E_sGseSpT.HighRiseCondominium;
                case E_MismoCategoryType.Manufactured: return E_sGseSpT.ManufacturedHousing;
                case E_MismoCategoryType.ManufacturedMultiWide: return E_sGseSpT.ManufacturedHomeMultiwide;
                case E_MismoCategoryType.ManufacturedSingleWide: return E_sGseSpT.ManufacturedHousingSingleWide;
                case E_MismoCategoryType.Modular: return E_sGseSpT.Modular;
                default:
                    return E_sGseSpT.LeaveBlank;
            }
        }

        private void Parse(MismoMortgageTerms mortgageTerms) 
        {
            if (null == mortgageTerms)
                return;
            m_dataLoan.sAgencyCaseNum = mortgageTerms.AgencyCaseIdentifier;
            m_dataLoan.sLAmtLckd = true;
            m_dataLoan.sLAmtCalc_rep = mortgageTerms.BaseLoanAmount;
            m_dataLoan.sLenderCaseNum = mortgageTerms.LenderCaseIdentifier;
            m_dataLoan.sTerm_rep = mortgageTerms.LoanAmortizationTermMonths;
            m_dataLoan.sNoteIR_rep = mortgageTerms.RequestedInterestRatePercent;

            switch (mortgageTerms.LoanAmortizationType) 
            {
                case E_MismoMortgageTermsLoanAmortizationType.Fixed:
                    m_dataLoan.sFinMethT = E_sFinMethT.Fixed;
                    break;
                case E_MismoMortgageTermsLoanAmortizationType.AdjustableRate:
                    m_dataLoan.sFinMethT = E_sFinMethT.ARM;
                    break;
                case E_MismoMortgageTermsLoanAmortizationType.GraduatedPaymentMortgage:
                    m_dataLoan.sFinMethT = E_sFinMethT.Graduated;
                    break;

            }
            switch (mortgageTerms.MortgageType) 
            {
                case E_MismoMortgageTermsMortgageType.Conventional:
                    m_dataLoan.sLT = E_sLT.Conventional;
                    break;
                case E_MismoMortgageTermsMortgageType.FHA:
                    m_dataLoan.sLT = E_sLT.FHA;
                    break;
                case E_MismoMortgageTermsMortgageType.VA:
                    m_dataLoan.sLT = E_sLT.VA;
                    break;
                case E_MismoMortgageTermsMortgageType.FarmersHomeAdministration:
                    m_dataLoan.sLT = E_sLT.UsdaRural;
                    break;
                default:
                    m_dataLoan.sLT = E_sLT.Other;
                    m_dataLoan.sLTODesc = mortgageTerms.OtherMortgageTypeDescription;
                    break;

            }
        }

        private void Parse(MismoAdditionalCaseData additionalCaseData) 
        {
            if (null == additionalCaseData)
                return;
            Parse(additionalCaseData.TransmittalData);
        }
        private void Parse(MismoTransmittalData transmittalData)
        {
            if (null == transmittalData)
                return;

            m_dataLoan.sApprVal_rep = transmittalData.PropertyAppraisedValueAmount;
            m_dataLoan.sSpMarketVal_rep = transmittalData.PropertyEstimatedValueAmount;
        }

        

        private void Parse(MismoGovernmentLoan governmentLoan) 
        {
            if (null == governmentLoan)
                return;
            Parse(governmentLoan.FhaLoan);
            Parse(governmentLoan.VaLoan);
        }
        private void Parse(MismoFhaLoan fhaLoan) 
        {
            if (null == fhaLoan)
                return;

            m_dataLoan.sFHALenderIdCode = fhaLoan.LenderIdentifier;
            m_dataLoan.sFHASponsorAgentIdCode = fhaLoan.SponsorIdentifier;
            m_dataLoan.sFHAFinancedDiscPtAmt_rep = fhaLoan.BorrowerFinancedFhaDiscountPointsAmount;
            
        }
        private void Parse(MismoVaLoan vaLoan) 
        {
            if (null == vaLoan)
                return;
            //m_dataLoan.GetAppData(0).aVaFamilySupportBal_rep = vaLoan.VaResidualIncomeAmount;
        }
        private CAppData FindDataAppBySsn(string ssn) 
        {
            if (null == ssn || ssn == "")
                return null;

            ssn = ssn.Replace("-", "");
            CAppData dataApp = null;
            bool isFound = false;
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++) 
            {
                dataApp = m_dataLoan.GetAppData(i);
                isFound = (dataApp.aBSsn.Replace("-", "") == ssn) || (dataApp.aCSsn.Replace("-", "") == ssn);
                if (isFound)
                    break;
            }
            return isFound ? dataApp : null;
        }
        private CAppData FindDataAppBySsn(string ssn, out bool isBorrower) 
        {
            isBorrower = true;
            if (null == ssn || ssn == "")
                return null;

            ssn = ssn.Replace("-", "");

            CAppData dataApp = null;
            bool isFound = false;
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++) 
            {
                dataApp = m_dataLoan.GetAppData(i);
                // 2/22/2011 dd - Fix for OPM 45961. We only search for ssn only. We assume there is no duplicate ssn between 
                // borrower and spouse.
                if (dataApp.aBSsn.Replace("-", "") == ssn) 
                {
                    isBorrower = true;
                    isFound = true;
                }
                else if (dataApp.aCSsn.Replace("-", "") == ssn)
                {
                    isBorrower = false;
                    isFound = true;
                }
                //isFound = (dataApp.aBSsn.Replace("-", "") == ssn && !isCoborrower) || (dataApp.aCSsn.Replace("-", "") == ssn && isCoborrower);
                if (isFound) 
                    break;

            }
            return isFound ? dataApp : null;
        }

        private string ParseOtherIncome(E_MismoCurrentIncomeIncomeType type)
        {
            E_aOIDescT descriptionType;
            switch (type)
            {
                case E_MismoCurrentIncomeIncomeType.AlimonyChildSupport:
                    descriptionType = E_aOIDescT.AlimonyChildSupport;
                    break;
                case E_MismoCurrentIncomeIncomeType.AutomobileExpenseAccount:
                    descriptionType = E_aOIDescT.AutomobileExpenseAccount;
                    break;
                case E_MismoCurrentIncomeIncomeType.FosterCare:
                    descriptionType = E_aOIDescT.FosterCare;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryBasePay:
                    descriptionType = E_aOIDescT.MilitaryBasePay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryClothesAllowance:
                    descriptionType = E_aOIDescT.MilitaryClothesAllowance;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryCombatPay:
                    descriptionType = E_aOIDescT.MilitaryCombatPay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryFlightPay:
                    descriptionType = E_aOIDescT.MilitaryFlightPay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryHazardPay:
                    descriptionType = E_aOIDescT.MilitaryHazardPay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryOverseasPay:
                    descriptionType = E_aOIDescT.MilitaryOverseasPay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryPropPay:
                    descriptionType = E_aOIDescT.MilitaryPropPay;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryQuartersAllowance:
                    descriptionType = E_aOIDescT.MilitaryQuartersAllowance;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryRationsAllowance:
                    descriptionType = E_aOIDescT.MilitaryRationsAllowance;
                    break;
                case E_MismoCurrentIncomeIncomeType.MilitaryVariableHousingAllowance:
                    descriptionType = E_aOIDescT.MilitaryVariableHousingAllowance;
                    break;
                case E_MismoCurrentIncomeIncomeType.NotesReceivableInstallment:
                    descriptionType = E_aOIDescT.NotesReceivableInstallment;
                    break;
                case E_MismoCurrentIncomeIncomeType.Pension:
                    descriptionType = E_aOIDescT.PensionRetirement;
                    break;
                case E_MismoCurrentIncomeIncomeType.MortgageDifferential:
                    descriptionType = E_aOIDescT.RealEstateMortgageDifferential;
                    break;
                case E_MismoCurrentIncomeIncomeType.SocialSecurity:
                    descriptionType = E_aOIDescT.SocialSecurityDisability;
                    break;
                case E_MismoCurrentIncomeIncomeType.SubjectPropertyNetCashFlow:
                    descriptionType = E_aOIDescT.SubjPropNetCashFlow;
                    break;
                case E_MismoCurrentIncomeIncomeType.Trust:
                    descriptionType = E_aOIDescT.Trust;
                    break;
                case E_MismoCurrentIncomeIncomeType.Unemployment:
                    descriptionType = E_aOIDescT.UnemploymentWelfare;
                    break;
                case E_MismoCurrentIncomeIncomeType.VABenefitsNonEducational:
                    descriptionType = E_aOIDescT.VABenefitsNonEducation;
                    break;
                case E_MismoCurrentIncomeIncomeType.OtherTypesOfIncome:
                    descriptionType = E_aOIDescT.Other;
                    break;
                default:
                    return string.Empty;
            }

            return OtherIncome.GetDescription(descriptionType).ToLower();
        }
        
        private string ParseDeclaration(E_YesNoIndicator indicator) 
        {
            switch (indicator) 
            {
                case E_YesNoIndicator.Y: return "Y";
                case E_YesNoIndicator.N: return "N";
                default: return "";
            }
        }
        private E_sBuildingStatusT FromMismo(E_MismoPropertyBuildingStatusType type) 
        {
            switch (type) 
            {
                case E_MismoPropertyBuildingStatusType.Existing:
                    return E_sBuildingStatusT.Existing;
                case E_MismoPropertyBuildingStatusType.Proposed:
                    return E_sBuildingStatusT.Proposed;
                case E_MismoPropertyBuildingStatusType.SubjectToAlterationImprovementRepairAndRehabilitation:
                    return E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab;
                case E_MismoPropertyBuildingStatusType.UnderConstruction:
                    return E_sBuildingStatusT.UnderConstruction;
                default:
                    return E_sBuildingStatusT.LeaveBlank;
            }

        }
        private void Debug(string msg) 
        {
            Tools.LogError(msg);
        }
	}
}
