﻿namespace LendersOffice.Conversions.LoanProspector
{
    /// <summary>
    /// Status of LPA requests.
    /// </summary>
    public enum LpaRequestResultStatus
    {
        /// <summary>
        /// Error status.
        /// </summary>
        Error = 0,

        /// <summary>
        /// Partial status. Need to poll to pull loan info.
        /// </summary>
        Partial = 1,

        /// <summary>
        /// Completed status. Should have received full response.
        /// </summary>
        Completed = 2
    }
}
