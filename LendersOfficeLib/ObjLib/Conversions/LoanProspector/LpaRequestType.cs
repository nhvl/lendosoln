﻿namespace LendersOffice.Conversions.LoanProspector
{
    /// <summary>
    /// The type of request that we're submitting to the LpaRequestHandler.
    /// </summary>
    public enum LpaRequestType
    {
        /// <summary>
        /// Submit request for non-seamless LPA.
        /// </summary>
        NonSeamlessLpaSubmit,

        /// <summary>
        /// Initial submit request for seamless LPA.
        /// </summary>
        SeamlessLpaSubmit,

        /// <summary>
        /// A poll request for seamless LPA.
        /// </summary>
        Poll,

        /// <summary>
        /// A transfer request to pull a file from Freddie.
        /// </summary>
        Transfer
    }
}
