<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!-- 
		Freddie Mac FullFeedback, Non JavaScript Stylesheet
		version: 4.7.20 
	-->
  <xsl:param name="VirtualRoot" ></xsl:param>
	<xsl:template name="expandRepositoryCode">
		<xsl:param name="code" />
		<xsl:choose>
			<xsl:when test="$code = 'TRW'">
				Experian
			</xsl:when>
			<xsl:when test="$code = 'CCC'">
				Trans Union
			</xsl:when>
			<xsl:when test="$code = 'CBI'">
				Equifax
			</xsl:when>
			<xsl:otherwise>
				$code
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
    <xsl:key name="verOrderBorrName" match="VERIFICATION_ASSET" use="@VerOrderBorrName" />
    <xsl:template match="VERIFIED_ASSETS">
        <xsl:apply-templates select="VERIFICATION_ASSET[generate-id(.)=generate-id(key('verOrderBorrName',@VerOrderBorrName)[1])]"/>
    </xsl:template>
	<xsl:template match="SERVICE_ORDER_RESPONSE">
		<html>
			<head>
				<meta charset="utf-8"></meta>
				<meta name="viewport" content="width=device-width, initial-scale=1"></meta>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
        <link rel="stylesheet">
          <xsl:attribute name="href">
            <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/bootstrap-3.3.5/css/bootstrap.css
          </xsl:attribute>
        </link>
        <link rel="stylesheet" type="text/css">
          <xsl:attribute name="href">
            <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/D3style.css
          </xsl:attribute>
        </link>
        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href">
            <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/style.css
          </xsl:attribute>
        </link>
        <link rel="stylesheet" type="text/css" >
          <xsl:attribute name="href">
            <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/css/feedback.css
          </xsl:attribute>
        </link>
			</head>
			<body class="body-stylesheet bg-greybg" data-spy="scroll"
				data-target=".myScrollspy">
				<!-- Main Header -->
				<!-- Fixed navbar -->
				<nav class="main-nav col-12" style="margin-left:0px; position:fixed">
					<ul class="main-nav-list">
            <li>
              <a href="#">
                <img>
                  <xsl:attribute name="src">
                    <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/ic-app-lpa-inv.svg
                  </xsl:attribute>
                </img>loan product advisor<span class="servicemark"></span>
              </a>
            </li>
					</ul>
				</nav>
				<!-- End of Header -->
				<div class="container bg-greybg">
					<div class="row" style="">
						<!-- Start of the Sidebar -->
						<div class="SB">
							<div class=" col-2 " style="height:500px">
								<nav class="myScrollspy">
									<ul class="sidebar-list " data-spy="affix">
										<li>
											<div style="height:75px;"></div>
										</li>
										<li>
											<a href="#">Feedback Certificate</a>
											<ul class="navCustom  sidebar-sublist">
												<li>
													<a href="#EvalSummary">Evaluation Summary</a>
												</li>
												<li>
													<a href="#loanData">Loan Data</a>
												</li>
												<li>
													<a href="#Results">Results</a>
												</li>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'"></xsl:when>
													<xsl:otherwise>
														<li>
															<a href="#CreditReportInformation">Credit Report
																Information</a>
														</li>
													</xsl:otherwise>
												</xsl:choose>
												<li>
													<a href="#MortgageInformation">Mortgage Information</a>
												</li>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS[@MortgageType='VA']"></xsl:when>
													<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'"></xsl:when>
													<xsl:otherwise>
														<li>
															<a href="#AssetInformation">Asset Information</a>
														</li>
													</xsl:otherwise>
												</xsl:choose>
												<li>
													<a href="#CalculatedValues">Calculated Values</a>
												</li>
												<li>
													<a href="#BorrowerInformation">Borrower Information</a>
												</li>
												<li>
													<a href="#TransactionInformation">Transaction Information</a>
												</li>
												<li>
													<a href="#FeedbackSummary">
														Feedback Summary&#160;
														<span class="badge badge-custom">
															<xsl:value-of
																select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE[@_Type='LoanProcessingMessages' or @_Type='CreditMessages']/FEEDBACK_MESSAGE )"></xsl:value-of>
														</span>
													</a>
												</li>
												<li></li>
											</ul>
										</li>
									</ul>
								</nav>
							</div>
							<!-- End of the Sidebar -->
						</div> <!--/Print Wrapper -->
						<div class="col-10  ">
							<div class="HH">
								<p class="floating-header " style="margin-bottom:15px">
									<i class="ic-certificate "></i>
									<xsl:choose>
										<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'">
											<span class="floating-header">FHA TOTAL Scorecard Full Feedback
												Certificate</span>
										</xsl:when>
										<xsl:otherwise>
											<span class="floating-header">Loan Product Advisor Full Feedback
												Certificate</span>
										</xsl:otherwise>
									</xsl:choose>
								</p>
							</div>
							<div class="ES">
								<div class="anchor" id="EvalSummary"></div>
								<div class="panel panel-default">
									<div class="bg-textDarkBlue panel-heading  panel-header-stripe"></div>

									<div class="panel-body panel-padding">
										<h1 class="header-contained top-header">Evaluation Summary</h1>
										<hr></hr>
										<div class="row row_bot">
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<p class="h4-label">PURCHASE ELIGIBILITY</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription">
														<xsl:choose>
															<xsl:when
																test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '000FreddieMacEligible'">
																<p class="call-to-act" style="white-space:nowrap !important;">
																	<span>
																		<i class="ic-shield-green active v-align " alt=""></i>
																	</span>
																	&#160; ELIGIBLE
																</p>
															</xsl:when>
															<xsl:when
																test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '000FreddieMacIneligible'">
																<p class="call-to-act" style="white-space:nowrap !important;">
																	<span>
																		<i class="ic-shield-red active v-align" alt=""></i>
																	</span>
																	&#160; INELIGIBLE
																</p>
															</xsl:when>
															<xsl:when
																test="LOAN_FEEDBACK/@FREPurchaseEligibilityDescription = '500FreddieMacEligibleLPAMinusOffering'">
																<p class="call-to-act" style="white-space:nowrap !important;">
																	<span>
																		<i class="ic-shield-green active v-align" alt=""></i>
																	</span>
																	&#160; Eligible LP A Minus Offering.
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray na-style-evalSummary">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray na-style-evalSummary">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<p class="h4-label">RISK CLASS</p>
												<p>
													<xsl:choose>
														<xsl:when
															test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription">
															<xsl:choose>
																<xsl:when
																	test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Accept' ">
																	<span>
																		<i class="ic-shield-green active v-align" alt=""></i>
																	</span>
																	&#160;
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Refer' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription = 'Caution' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;
																</xsl:when>
															</xsl:choose>
															<span class="call-to-act"
																style="text-transform: uppercase; white-space:nowrap !important;">
																<xsl:value-of
																	select="LOAN_FEEDBACK/@LoanProspectorCreditRiskClassificationDescription" />
															</span>
														</xsl:when>
														<xsl:otherwise>
															<p class="na-gray na-style-evalSummary ">N/A</p>
														</xsl:otherwise>
													</xsl:choose>
												</p>
											</div>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<p class="h4-label" style="white-space:nowrap !important;">COLLATERAL
													R&#38;W* RELIEF</p>
												<p>
													<xsl:choose>
														<xsl:when
															test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType">
															<xsl:choose>
																<xsl:when
																	test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'Eligible' ">
																	<span>
																		<i class="ic-shield-green active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Eligible</span>
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'NotEligible' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Not Eligible</span>
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@CollateralRepresentationAndWarrantyReliefEligibilityType = 'Unavailable' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Unavailable</span>
																</xsl:when>
															</xsl:choose>
														</xsl:when>
														<xsl:otherwise>
															<p class="na-gray na-style-evalSummary ">N/A</p>
														</xsl:otherwise>
													</xsl:choose>
												</p>
											</div>
										</div>
										<div class="row row_bot">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<p class="h4-label" style="white-space:nowrap !important;">ASSET
													R&#38;W* RELIEF</p>
												<p>
													<xsl:choose>
														<xsl:when
															test="LOAN_FEEDBACK/@AssetRepresentationAndWarrantyReliefEligibilityType ">
															<xsl:choose>
																<xsl:when
																	test="LOAN_FEEDBACK/@AssetRepresentationAndWarrantyReliefEligibilityType  = 'Eligible' ">
																	<span>
																		<i class="ic-shield-green active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Eligible</span>
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@AssetRepresentationAndWarrantyReliefEligibilityType  = 'NotEligible' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Not Eligible</span>
																</xsl:when>
																<xsl:when
																	test="LOAN_FEEDBACK/@AssetRepresentationAndWarrantyReliefEligibilityType  = 'Unavailable' ">
																	<span>
																		<i class="ic-shield-yellow active v-align" alt=""></i>
																	</span>
																	&#160;&#160;
																	<span class="call-to-act"
																		style="text-transform: uppercase; white-space:nowrap !important;">Unavailable</span>
																</xsl:when>
															</xsl:choose>
														</xsl:when>
														<xsl:otherwise>
															<span>
																<i class="ic-shield-yellow active v-align" alt=""></i>
															</span>
															&#160;&#160;
															<span class="call-to-act"
																style="text-transform: uppercase; white-space:nowrap !important;">Unavailable</span>
														</xsl:otherwise>
													</xsl:choose>
												</p>
											</div>
										</div>
										<xsl:if
											test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription = 'Complete'">
											<xsl:if
												test="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE/@MessageTag='PurchaseRestrictionMessage'">
												<div class="row_bot no-margin row_top">
													<table class="table">
														<thead class=" ">
															<tr>
																<th class="table-row-header-padding" style="white-space:nowrap !important;">PURCHASE
																	RESTRICTIONS</th>
																<th></th>
															</tr>
														</thead>
														<tbody>
															<xsl:for-each
																select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE">
																<xsl:if test="@MessageTag= 'PurchaseRestrictionMessage'">
																	<tr>
																		<td>
																			<xsl:value-of select="@MessageCode" />
																		</td>
																		<td>
																			<xsl:value-of select="@MessageText" />
																		</td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
														</tbody>
													</table>
												</div>
											</xsl:if>
										</xsl:if>
									</div> <!-- /panel-body -->
								</div> <!--/ panel -->
							</div><!-- /Print Wrapper -->
							<div class="LD">
								<div id="loanData" class="anchor"></div>
								<p class="floating-header">Loan Data</p>
								<div class="panel panel-default  " style="margin-bottom:0px">
									<div class="panel-body panel-padding">
										<div class="row row_bot no-margin-rollover-4">
											<xsl:for-each select="LOAN_APPLICATION/BORROWER">
												<div class="col-4" style="">
													<xsl:choose>
														<xsl:when test="position() = 1">
															<p class="h4-label">BORROWER NAME</p>
														</xsl:when>
														<xsl:otherwise>
															<p class="h4-label">CO-BORROWER NAME(S)</p>
														</xsl:otherwise>
													</xsl:choose>
													<p class="body-navy break-word">
														<xsl:value-of select="@_FirstName" />&#160;<xsl:value-of select="@_MiddleName" />&#160;<xsl:value-of select="@_LastName" />&#160;<xsl:value-of select="@_NameSuffix" />
														<br></br>
														<xsl:value-of select="substring(format-number(@_SSN,'000000000'),1,3)" />-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),4,2)" />-<xsl:value-of select="substring(format-number(@_SSN,'000000000'),6,4)" />
													</p>
												</div>
											</xsl:for-each>
										</div> <!-- /row -->
										<hr class="LINE"></hr>
										<div class="row row_bot">
											<div class="col-4">
												<p class="h4-label">APPRAISAL IDENTIFIER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/PROPERTY/_VALUATION/EXTENSION_VALUATION/@AppraisalIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/PROPERTY/_VALUATION/EXTENSION_VALUATION/@AppraisalIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">LP AUS KEY</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorKeyIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">LOAN APPLICATION NUMBER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS/@LenderCaseIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/MORTGAGE_TERMS/@LenderCaseIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!--/row -->
										<hr class="LINE"></hr>
										<div class="row row_bot">
											<div class="col-4">
												<p class="h4-label">PROPERTY ADDRESS</p>
												<xsl:choose>
													<xsl:when test="LOAN_APPLICATION/PROPERTY/@_StreetAddress">
														<p class="body-navy">
															<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_StreetAddress" />, <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_City" />, <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State" />&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode" />
														</p>
													</xsl:when>
													<xsl:when test="LOAN_APPLICATION/PROPERTY/@_City">
														<p class="body-navy">
															<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_City" />, <xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State" />&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode" />
														</p>
													</xsl:when>
													<xsl:when test="LOAN_APPLICATION/PROPERTY/@_State">
														<p class="body-navy">
															<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_State" />&#160;<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode" />
														</p>
													</xsl:when>
													<xsl:when test="LOAN_APPLICATION/PROPERTY/@_PostalCode">
														<p class="body-navy">
															<xsl:value-of select="LOAN_APPLICATION/PROPERTY/@_PostalCode" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">MORTGAGE TYPE</p>
												<xsl:choose>
													<xsl:when test="LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">DOCUMENTATION LEVEL</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FREDocumentationLevelDescription">
														<p class="body-navy" style="">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FREDocumentationLevelDescription" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray" style="">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!--/row -->
										<xsl:choose>
											<xsl:when
												test="LOAN_FEEDBACK/@FRECreditScoreLTVFeeLevelDescription">
												<div class="row row_bot">
													<div class="col-4">
														<p class="h4-label">CS/LTV Fee Level</p>
														<p class="body-navy" style="">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRECreditScoreLTVFeeLevelDescription" />
														</p>
													</div>
													<div class="col-4"></div>
													<div class="col-4"></div>
												</div>
											</xsl:when>
											<xsl:otherwise></xsl:otherwise>
										</xsl:choose>
									</div><!-- /Print Wrapper -->
								</div>
							</div>
							<div class="RS">
								<a class="anchor" id="Results"></a>
								<h1 class="floating-header">Results</h1>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<div class="row row_middle">
											<div class="col-4">
												<p class="h4-label">AUS STATUS</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorEvaluationStatusDescription" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">APPRAISAL TYPE</p>
												<p id="appraisalType" class="body-navy">
													<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
														<xsl:if test="@_Type = 'LoanProcessingMessages'">
															<xsl:for-each select="FEEDBACK_MESSAGE">
																<xsl:if test="@MessageCode='50'">
																	<xsl:text>Form 72</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='51'">
																	<xsl:text>Form 70/465)</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='52'">
																	<xsl:text>Form 2055 interior</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='64'">
																	<xsl:text>Form 2070 interior</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='53'">
																	<xsl:text>Form 2055 exterior</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='65'">
																	<xsl:text>Form 2070 exterior</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='L3'">
																	<xsl:text>Form 465</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='L4'">
																	<xsl:text>Form 466</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='L5'">
																	<xsl:text>Form 70</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='L6'">
																	<xsl:text>Form 2055</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='L2'">
																	<xsl:text>Form 70B</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='B1'">
																	<xsl:text>No Appraisal Required</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='B3'">
																	<xsl:text>No Appraisal Required</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='RT'">
																	<xsl:text>Prop Inspect Alt</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='MQ'">
																	<xsl:text>Form 2070</xsl:text>
																</xsl:if>
																<xsl:if test="@MessageCode='LU'">
																	<xsl:text>Form 2090</xsl:text>
																</xsl:if>
															</xsl:for-each>
														</xsl:if>
													</xsl:for-each>
												</p>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">LOAN PROCESSING STAGE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@CaseStateType">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@CaseStateType" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!--/row -->
										<div class="row row_bot">
											<div class="col-4">
												<p class="h4-label">ASSESSMENT TYPE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorServiceEvaluationDescription = 'Credit'">
														<p class="body-navy">Credit Only</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorServiceEvaluationDescription = 'CreditAndMI'">
														<p class="body-navy">Credit And MI</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorServiceEvaluationDescription = 'MergedCredit'">
														<p class="body-navy">Merged Credit</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">LPA VERSION</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/_DATA_INFORMATION/DATA_VERSION/@_Number">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/_DATA_INFORMATION/DATA_VERSION/@_Number" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4">
												<p class="h4-label">LPA ASSESSMENT EXP. DATE</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FRECreditExpirationDate">
														<p class="body-navy">
															<xsl:value-of select="LOAN_FEEDBACK/@FRECreditExpirationDate" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!--/row -->
									</div><!-- /panel body -->
								</div> <!-- /panel -->
							</div><!-- /Print Wrapper -->
							<xsl:choose>
								<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'"></xsl:when>
								<xsl:otherwise>
									<div class="CR">
										<a class="anchor" id="CreditReportInformation"></a>
										<h1 class="floating-header">Credit Report Information</h1>
										<div class="panel panel-default">
											<div class="panel-body panel-padding">
												<div class="row row_middle">
													<div class="col-4">
														<p class="h4-label">SELECTED BORROWER</p>
														<xsl:choose>
															<xsl:when test="LOAN_FEEDBACK/@ScoredBorrowerName">
																<p class="body-navy">
																	<xsl:value-of select="LOAN_FEEDBACK/@ScoredBorrowerName" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</div><!--/col -->
													<div class="col-4">
														<p class="h4-label">SELECTED REPOSITORY</p>
														<xsl:choose>
															<xsl:when
																test="LOAN_FEEDBACK/@CreditScoreDataRepositorySourceType">
																<p class="body-navy">
																	<xsl:value-of
																		select="LOAN_FEEDBACK/@CreditScoreDataRepositorySourceType" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</div><!--/col -->
													<div class="col-4">
														<p class="h4-label">INDICATOR SCORE</p>
														<xsl:choose>
															<xsl:when test="LOAN_FEEDBACK/@FRESelectedCreditScoreValue">
																<p class="body-navy">
																	<xsl:value-of
																		select="LOAN_FEEDBACK/@FRESelectedCreditScoreValue" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</div><!--/col -->
												</div><!-- /row -->
												<div class="row row_bot">
													<table class="table">
														<thead class="table-bg table-header">
															<tr>
																<th>BORROWER</th>
																<th>REPOSITORY</th>
																<th>CREDIT SCORE</th>
																<th style="white-space:nowrap !important;">MERGED CREDIT REFERENCE #</th>
															</tr>
														</thead><!-- /thead -->
														<tbody>
															<xsl:for-each select="LOAN_FEEDBACK/FRE_BORROWER_RESPONSE">
																<tr>
																	<td>
																		<p class="body-navy" style="word-break:normal">
																			&#160;
																			<xsl:value-of select="@BorrowerUnparsedName" />
																		</p>
																	</td>
																	<td>
																		<xsl:for-each select="FRE_CREDIT_SCORE">
																			<xsl:choose>
																				<xsl:when
																					test="@_RepositorySourceType=../@_SelectedRepositorySourceType">
																					<p class="body-navy no-margin print-selected">
																						<b>
																							<xsl:call-template name="expandRepositoryCode">
																								<xsl:with-param name="code"
																									select="@_RepositorySourceType" />
																							</xsl:call-template>
																						</b>
																					</p>
																				</xsl:when>
																				<xsl:otherwise>
																					<p class="na-gray no-margin print-selected">
																						<xsl:call-template name="expandRepositoryCode">
																							<xsl:with-param name="code"
																								select="@_RepositorySourceType" />
																						</xsl:call-template>
																					</p>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:for-each>
																	</td>
																	<td>
																		<xsl:for-each select="FRE_CREDIT_SCORE">
																			<xsl:choose>
																				<xsl:when
																					test="@_RepositorySourceType=../@_SelectedRepositorySourceType">
																					<p class="no-margin ">
																						<b>
																							<xsl:value-of select="@_RepositoryScoreValue" />
																						</b>
																					</p>
																				</xsl:when>
																				<xsl:otherwise>
																					<p class="na-gray no-margin">
																						<xsl:value-of select="@_RepositoryScoreValue" />
																						&#160;&#160;
																					</p>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:for-each>
																	</td>
																	<td>
																		<xsl:choose>
																			<xsl:when test="@CreditReportIdentifier">
																				<p class="body-navy">
																					&#160;
																					<xsl:value-of select="@CreditReportIdentifier" />
																				</p>
																			</xsl:when>
																			<xsl:otherwise>
																				<p class="na-gray">N/A</p>
																			</xsl:otherwise>
																		</xsl:choose>
																	</td>
																</tr>
															</xsl:for-each>
														</tbody><!-- -->
													</table><!--/table -->
												</div><!--/row -->
												<xsl:if
													test="((LOAN_FEEDBACK/FRE_BORROWER_RESPONSE[@_CreditRepulledIndicator!='N']) or (LOAN_FEEDBACK/FRE_BORROWER_RESPONSE[@CreditReaccessIndicator!='N']))">
													<div class="row row_bot">
														<xsl:for-each select="LOAN_FEEDBACK/FRE_BORROWER_RESPONSE">
															<xsl:if test="position()=1">
																<p class="h4-label ">Credit Reordered:</p>
																<div id="repulled-label"></div>
															</xsl:if>
															<xsl:choose>
																<xsl:when test="@_CreditRepulledIndicator = 'y'">
																	<p class="body-navy repulled">
																		Credit was reordered for
																		<xsl:value-of select="@BorrowerUnparsedName" />
																		.
																	</p>
																</xsl:when>
																<xsl:when test="@_CreditRepulledIndicator = 'Y'">
																	<p class="body-navy repulled">
																		Credit was reordered for
																		<xsl:value-of select="@BorrowerUnparsedName" />
																		.
																	</p>
																</xsl:when>
															</xsl:choose>
														</xsl:for-each>
														<br></br>
														<xsl:for-each select="LOAN_FEEDBACK/FRE_BORROWER_RESPONSE">
															<xsl:if test="position()=1">
																<p class="h4-label ">Credit Reaccessed:</p>
																<div id="reaccessed-label"></div>
															</xsl:if>
															<xsl:if test="@CreditReaccessIndicator = 'Y'">
																<p class="body-navy reaccessed">
																	Credit was reaccessed for
																	<xsl:value-of select="@BorrowerUnparsedName" />
																	.
																</p>
															</xsl:if>
														</xsl:for-each>
													</div><!--/row -->
												</xsl:if>
											</div><!-- /panel-body -->
										</div><!-- /panel -->
									</div><!-- /Print Wrapper -->
								</xsl:otherwise>
							</xsl:choose>
							<div class="MI">
								<a class="anchor" id="MortgageInformation"></a>
								<h1 class="floating-header">Mortgage Information</h1>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">PRODUCT TYPE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = 'F40'">
														<p class="body-navy">40 Year Fixed Rate</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = 'F30'">
														<p class="body-navy">30 Year Fixed Rate</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = 'F20'">
														<p class="body-navy">20 Year Fixed Rate</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = 'F15'">
														<p class="body-navy">15 Year Fixed Rate</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = 'ARM'">
														<p class="body-navy">Adjustable Rate Mortgage</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = '525'">
														<p class="body-navy">5 Year Balloon</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREProductEvaluationDescription = '723'">
														<p class="body-navy">7 Year Balloon</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">AMORTIZATION TYPE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS/@LoanAmortizationType = 'AdjustableRate'">
														<p class="body-navy">Adjustable</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS/@LoanAmortizationType = 'Fixed'">
														<p class="body-navy">Fixed</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">AMORTIZATION MONTHS</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS/@LoanAmortizationTermMonths">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/MORTGAGE_TERMS/@LoanAmortizationTermMonths" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">INTEREST RATE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/MORTGAGE_TERMS/@RequestedInterestRatePercent">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/MORTGAGE_TERMS/@RequestedInterestRatePercent" />%
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">PURPOSE OF LOAN</p>
												<xsl:choose>
													<xsl:when test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type">
														<p class="body-navy">
															<xsl:value-of select="LOAN_APPLICATION/LOAN_PURPOSE/@_Type" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">PURCHASE PRICE</p>
												<xsl:choose>
													<xsl:when
														test="not(LOAN_APPLICATION/TRANSACTION_DETAIL/@PurchasePriceAmount)">
														<p class="na-gray">N/A</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/TRANSACTION_DETAIL/@PurchasePriceAmount,',##0.00')" />
														</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">LOAN AMOUNT</p>
												<xsl:choose>
													<xsl:when
														test="CALCULATIONS[@_Name='BorrowerRequestedLoanAmount']/@_Value">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name='BorrowerRequestedLoanAmount']/@_Value,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">ESTIMATED VALUE OF PROPERTY</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@PropertyEstimatedValueAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@PropertyEstimatedValueAmount,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4   ">
												<p class="h4-label">APPRAISED VALUE OF PROPERTY</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@PropertyAppraisedValueAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA/@PropertyAppraisedValueAmount,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">PROPERTY TYPE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'Condominium'">
														<p class="body-navy" style="word-break:normal">Condominium</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'SingleFamilyDetached'">
														<p class="body-navy" style="word-break:normal">Single Family Detached</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'SingleFamilyAttached'">
														<p class="body-navy" style="word-break:normal">Single
															Family Attached</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'SingleFamilyDetachedPUD'">
														<p class="body-navy" style="word-break:normal">Single
															Family Detached PUD</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'SingleFamilyAttachedPUD'">
														<p class="body-navy" style="word-break:normal">Single
															Family Attached PUD</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'TwoToFourFamily'">
														<p class="body-navy" style="word-break:normal">Two to
															Four Family</p>
													</xsl:when>
													<xsl:when
														test="LOAN_FEEDBACK/@FREPropertyCategoryDescription = 'Cooperative'">
														<p class="body-navy" style="word-break:normal">Cooperative</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/PROPERTY/_CATEGORY/@_Type = 'ManufacturedSingleWide'">
														<p class="body-navy" style="word-break:normal">Manufactured
															Housing SingleWide</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/PROPERTY/_CATEGORY/@_Type = 'ManufacturedMultiWide'">
														<p class="body-navy" style="word-break:normal">Manufactured
															Home MultiWide</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4   ">
												<p class="h4-label">INTENDED USE OF PROPERTY</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/@PropertyUsageType = 'Investment'">
														<p class="body-navy">Investment Property</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/@PropertyUsageType = 'PrimaryResidence'">
														<p class="body-navy">Primary Residence</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/@PropertyUsageType = 'SecondHome'">
														<p class="body-navy">Secondary Residence</p>
													</xsl:when>
													<xsl:otherwise>
														N/A
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4   ">
												<p class="h4-label">NUMBER OF UNITS</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/PROPERTY/@_FinancedNumberOfUnits">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/PROPERTY/@_FinancedNumberOfUnits" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</div><!--/row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">AFFORDABLE PRODUCT TYPE</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/AFFORDABLE_LENDING/@FREAffordableProgramIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/AFFORDABLE_LENDING/@FREAffordableProgramIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">CASH OUT AMOUNT</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@FRECashOutAmount">
														<xsl:if
															test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type !='Purchase'">
															<xsl:if
																test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@FRECashOutAmount = 'N/A'">
																<p class="na-gray">N/A</p>
															</xsl:if>
															<xsl:if
																test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@FRECashOutAmount != 'N/A'">
																<p class="body-navy">$<xsl:value-of
																	select="format-number(LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@FRECashOutAmount,',##0.00')" />
																</p>
															</xsl:if>
														</xsl:if>
														<xsl:if test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type ='Purchase'">
															<p class="na-gray">N/A</p>
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label" style="white-space:nowrap !important;">TEMPORARY SUBSIDY BUYDOWN</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@BuydownTemporarySubsidyIndicator = 'Y'">
														<p class="body-navy">Yes</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@BuydownTemporarySubsidyIndicator = 'N'">
														<p class="body-navy">No</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div> <!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">OFFERING IDENTIFIER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '230'">
														<p class="body-navy">Initial Int 10/20 FIXED</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '231'">
														<p class="body-navy">Initial Int 15/15 FIXED</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '232'">
														<p class="body-navy">Initial Int ARM</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '210'">
														<p class="body-navy">Alt 97</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '220'">
														<p class="body-navy">Freddie Mac 100</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '233'">
														<p class="body-navy">Initial Int ARM - 10 Year IO</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '240'">
														<p class="body-navy">Home Possible 100</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '241'">
														<p class="body-navy">Home Possible</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '244'">
														<p class="body-navy">Home Possible 3% Cash</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '242'">
														<p class="body-navy">Home Possible NH Solution 100</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '243'">
														<p class="body-navy">Home Possible NH Solution</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '245'">
														<p class="body-navy">Home Possible NH Solution 3% Cash</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '250'">
														<p class="body-navy">Home Possible Advantage</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '251'">
														<p class="body-navy">Home Possible Advantage for HFAs</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '310'">
														<p class="body-navy">Relief Refinance - Open Access</p>
													</xsl:when>
													<xsl:when 
														test = "LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@FREOfferingIdentifier = '320'"><p class="body-navy">Enhanced Relief Refinance</p>
												   	</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">SUBORDINATE AMOUNT</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/TRANSACTION_DETAIL/@SubordinateLienAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/TRANSACTION_DETAIL/@SubordinateLienAmount,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">SALES CONCESSIONS</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/TRANSACTION_DETAIL/@SalesConcessionAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/TRANSACTION_DETAIL/@SalesConcessionAmount, ',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">NEW CONSTRUCTION</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@ConstructionToPermanentClosingType = 'OneClosing'">
														<p class="body-navy">1 Close Const To Perm</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@ConstructionToPermanentClosingType = 'TwoClosing'">
														<p class="body-navy">2 Close Const To Perm</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@ConstructionToPermanentClosingType = 'Other'">
														<p class="body-navy">Const Compl After Appl</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@ProductName = 'NewlyBuilt'">
														<p class="body-navy">Newly Built</p>
													</xsl:when>
													<xsl:when
														test="LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@ProductName = 'ConstructionConversion'">
														<p class="body-navy">Construction Conversion</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4   ">
												<p class="h4-label">PURPOSE OF REFINANCE</p>
												<xsl:if test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type != 'Purchase'">
													<xsl:choose>
														<xsl:when
															test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@GSERefinancePurposeType = 'CashOutOther'">
															<p class="body-navy">Cash-Out Refi</p>
														</xsl:when>
														<xsl:when
															test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@GSERefinancePurposeType = 'NoCashOutFHAStreamlinedRefinance'">
															<p class="body-navy">No Cash-Out FHA Streamlined Refi</p>
														</xsl:when>
														<xsl:when
															test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@GSERefinancePurposeType = 'NoCashOutFREOwnedRefinance'">
															<p class="body-navy">No Cash-Out Freddie-owned Refi</p>
														</xsl:when>
														<xsl:when
															test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@GSERefinancePurposeType = 'NoCashOutOther'">
															<p class="body-navy">No Cash-Out Refi</p>
														</xsl:when>
														<xsl:when
															test="LOAN_APPLICATION/LOAN_PURPOSE/CONSTRUCTION_REFINANCE_DATA/@GSERefinancePurposeType = 'NoCashOutStreamlinedRefinance'">
															<p class="body-navy">No Cash-Out Streamlined Refi</p>
														</xsl:when>
														<xsl:otherwise>
															<p class="na-gray">N/A</p>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
												<xsl:if test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type = 'Purchase'">
													<p class="na-gray">N/A</p>
												</xsl:if>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">LENDER SUBMITTED RESERVES</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/TRANSACTION_DETAIL/@FREReservesAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/TRANSACTION_DETAIL/@FREReservesAmount,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														N/A
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<hr></hr>
										<div class="row row_bot">
											<div class="col-4   ">
												<p class="header-contained Header_bot print-arm">ARM Related Details:</p>
											</div> <!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">ARM QUALIFYING RATE</p>
												<xsl:choose>
													<xsl:when test="CALCULATIONS[@_Name='ARMQualifyingRate']">
														<p class="body-navy">
															<xsl:value-of
																select="CALCULATIONS[@_Name='ARMQualifyingRate']/@_Value" />%
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label ">ARM QUALIFYING PITI</p>
												<xsl:choose>
													<xsl:when test="CALCULATIONS[@_Name = 'ARMQualifyingPITI']">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name = 'ARMQualifyingPITI']/@_Value,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
											</div><!-- /col -->
										</div><!-- /row -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
							</div><!-- /Print Wrapper -->
							<xsl:choose>
								<xsl:when test="LOAN_APPLICATION/MORTGAGE_TERMS[@MortgageType='VA']"></xsl:when>
								<xsl:when test="LOAN_FEEDBACK/@FRE_HUDScoredIndicator = 'Y'"></xsl:when>
								<xsl:otherwise>
									<div class="AI">
										<a class="anchor" id="AssetInformation"></a>
										<h1 class="floating-header">Asset Information</h1>
										<div class="panel panel-default">
											<div class="panel-body panel-padding">
												<div class="row" style="margin-bottom:0px">
													<div class="col-4">
														<p class="h4-label print-selected1">TOTAL ELIGIBLE ASSETS</p>
														<xsl:choose>
															<xsl:when test="CALCULATIONS[@_Name = 'TotalEligibleAssets']">
																<p class="body-navy">$<xsl:value-of
																	select="format-number(CALCULATIONS[@_Name = 'TotalEligibleAssets']/@_Value,'###,##0.00')" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</div><!-- /col -->
													<div class="col-4 ">
														<p class="h4-label print-selected1">TOTAL FUNDS TO BE VERIFIED</p>
														<xsl:choose>
															<xsl:when
																test="CALCULATIONS[@_Name = 'TotalFundsToBeVerified']">
																<p class="body-navy" style="color:#009CE0">$<xsl:value-of
																	select="format-number(CALCULATIONS[@_Name = 'TotalFundsToBeVerified']/@_Value,'###,##0.00')" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</div><!-- /col -->
													<div class="col-4   ">
														<xsl:choose>
															<xsl:when
																test="CALCULATIONS[@_Name = 'ThirdPartyTotalAmounts']">
																<p class="h4-label print-selected1">THIRD PARTY ASSET VALIDATION</p>
																<b style="color:#009CE0;"
																	class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'ThirdPartyTotalAmounts']/@_Value,'###,##0.00')" />
																</b>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if test="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA">
																	<p class="h4-label print-selected1">THIRD PARTY ASSET VALIDATION</p>
																	<b class="alert-text">Error(s) (See details)</b>
																</xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</div><!-- /col -->
												</div><!-- /row -->
												<xsl:if test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type != 'Purchase'">
													<div class="row">
														<div class="col-12 ">
															<div class="img-bracket">
                                <img>
                                  <xsl:attribute name="style">width:100%</xsl:attribute>
                                  <xsl:attribute name="src">
                                    <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/bracket_icon_sym.svg
                                  </xsl:attribute>
                                </img>
															</div>
														</div>
													</div>
													<div class="row row_bot">
														<div class="col-5">
															<p class="h4-label print-selected1" style="white-space:nowrap !important;">REQUIRED
																BORROWER FUNDS</p>
															<xsl:choose>
																<xsl:when
																	test="CALCULATIONS[@_Name = 'RequiredBorrowerFunds']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'RequiredBorrowerFunds']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="na-gray">N/A</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
														<div class="col-2" style="margin-left:0;">
															<div style="text-align: center;">
                                <img>
                                  <xsl:attribute name="style">height:45px;width:28px;</xsl:attribute>
                                  <xsl:attribute name="src">
                                    <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/plus_icon.svg
                                  </xsl:attribute>
                                </img>
															</div>
														</div>
														<div class="col-3">&#160;</div>
														<div class="col-2">
															<p class="h4-label print-selected1">REQUIRED RESERVES</p>
															<xsl:choose>
																<xsl:when test="CALCULATIONS[@_Name = 'RequiredReserves']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'RequiredReserves']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="na-gray">N/A</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
													</div><!-- /row -->
												</xsl:if>
												<xsl:if test="LOAN_APPLICATION/LOAN_PURPOSE/@_Type = 'Purchase'">
													<div class="row">
														<div class="img-bracket">
                              <img>
                                <xsl:attribute name="style">width:100%</xsl:attribute>
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/bracket_icon.svg
                                </xsl:attribute>
                              </img>
														</div>
													</div>
													<div class="row row_bot">
														<div class="col-3">
															<p class="h4-label print-selected1" style="white-space:nowrap !important;">REQUIRED
																BORROWER FUNDS</p>
															<xsl:choose>
																<xsl:when
																	test="CALCULATIONS[@_Name = 'RequiredBorrowerFunds']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'RequiredBorrowerFunds']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="na-gray">N/A</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
														<div class="col-1">
                              <img>
                                <xsl:attribute name="style">height:45px;width:28px</xsl:attribute>
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/plus_icon.svg
                                </xsl:attribute>
                              </img>
														</div>
														<div class="col-2">
															<p class="h4-label print-selected1">REQUIRED RESERVES</p>
															<xsl:choose>
																<xsl:when test="CALCULATIONS[@_Name = 'RequiredReserves']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'RequiredReserves']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="na-gray">N/A</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
														<div class="col-1">
                              <img>
                                <xsl:attribute name="style">height:45px;width:28px</xsl:attribute>
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/plus_icon.svg
                                </xsl:attribute>
                              </img>
														</div>
														<div class="col-2">
															<p class="h4-label print-selected1">PAID DOWN DEBTS</p>
															
															<xsl:choose>
																<xsl:when test="CALCULATIONS[@_Name = 'PaidDownDept']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'PaidDownDept']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="body-navy">$0.00</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
														<div class="col-1">
                              <img>
                                <xsl:attribute name="style">height:45px;width:28px;</xsl:attribute>
                                <xsl:attribute name="src">
                                  <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/plus_icon.svg
                                </xsl:attribute>
                              </img>
														</div>
														<div class="col-2">
															<p class="h4-label print-selected1">PAID OFF DEBTS</p>
															<xsl:choose>
																<xsl:when test="CALCULATIONS[@_Name = 'PaidOffDebt']">
																	<p class="body-navy">$<xsl:value-of
																		select="format-number(CALCULATIONS[@_Name = 'PaidOffDebt']/@_Value,'###,##0.00')" />
																	</p>
																</xsl:when>
																<xsl:otherwise>
																	<p class="na-gray">N/A</p>
																</xsl:otherwise>
															</xsl:choose>
														</div><!-- /col -->
													</div><!-- /row -->
												</xsl:if>
												<!-- Asset table -->
												<xsl:if test="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA">
												<div id="assetverificationtable">
													<div class="assetdetails">
														<p class="header-contained">Asset Details</p>
													</div>
													<xsl:if
														test="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFICATION_MESSAGES/VRF_MSG_GROUP/VRF_MESSAGE">
														<xsl:for-each
															select="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFICATION_MESSAGES/VRF_MSG_GROUP/VRF_MESSAGE">
															<xsl:if test="@MessageSection='AssetsAndReserves'">
																<p style="color: #DC3232; padding-bottom: 5px;">
																	<img src="lib/img/ic-loanapplicationalert.svg"
																		style="height: 18px; padding-right: 20px"></img>
																	<xsl:value-of select="@MessageText" />
																</p>
															</xsl:if>
														</xsl:for-each>
													</xsl:if>
													<xsl:for-each select="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFIED_ASSETS/VERIFICATION_ASSET[generate-id(.)=generate-id(key('verOrderBorrName',@VerOrderBorrName)[1])]">
														<p class="section-header">
															<xsl:value-of select="@VerOrderBorrName"/>
														</p><br/>
														
															<table class="table">
																<thead class="table-bg table-header">
																	<tr>
																		<th class="asset_detail_1">
																			ACCOUNT
																			<p>OWNER(S)</p>
																		</th>
																		<th class="asset_detail_1">
																			FINANCIAL
																			<p> INSTITUTION</p>
																		</th>
																		<th class="asset_detail_1">
																			ACCOUNT NUMBER
																			<p> AND TYPE</p>
																		</th>
																		<th class="asset_detail_2">
																			BALANCE
																			<p>&#160;</p>
																		</th>
																		<th class="asset_detail_3">
																			STATEMENT DATE/
																			<p>EXPIRATION DATE</p>
																		</th>
																		<th class="asset_detail_3">
																			DUPLICATE
																			<p>&#160;</p>
																		</th>
																	</tr>
																</thead><!-- /thead -->
												    			<xsl:for-each select="key('verOrderBorrName', @VerOrderBorrName)">
																<tbody>
																	<tr>
																		<td>
																			
																				<xsl:value-of select="@AccountHolder" />
																			
																		</td>
																		<td>
																			
																				<xsl:value-of select="@FinInstitution" />
																			
																			<p class="body-mini">
																				FormFree:
																				<xsl:value-of select="@ReportIdentifier" />
																			</p>
																		</td>
																		<td>
																			
																				<xsl:value-of select="@AcctNumber" />
																			
																			<p class="body-mini">
																				
																				<xsl:value-of select="@AcctType" />
																			</p>
																		</td>
																		<td>
																			
																				<xsl:value-of select="@Balance" />
																			
																		</td>
																		<td>
																			
																				<xsl:value-of select="@StatementDate" />
																			
																			<p>
																				<xsl:value-of select="@ExpirationDate" />
																			</p>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="@DuplFlag = 'Y'">
																					<p class="body-navy">Yes</p>
																				</xsl:when>
																				<xsl:otherwise>
																					<p class="body-navy">No</p>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																	</tr>
																</tbody><!-- -->
																</xsl:for-each>
															</table><!--/table -->
															
													</xsl:for-each>

												<xsl:if test="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFICATION_MESSAGES/VRF_MSG_GROUP/VRF_MESSAGE/@MessageCode='85'">
														<div class="row_asset">
															<div class="col-12 header-contained">
																<p>Large Deposits</p>
																<hr class="LINE"></hr>
															</div>
														</div>
													<p class="body-gray large-deposit-msg">
														<xsl:for-each select="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFICATION_MESSAGES/VRF_MSG_GROUP/VRF_MESSAGE">
															<xsl:if test="@MessageCode='85'">
																<xsl:value-of select='@MessageText'/>
															</xsl:if>
														</xsl:for-each>
													</p>
													<xsl:for-each 
														select="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFICATION_MESSAGES/VRF_MSG_GROUP/VRF_MESSAGE/MSG_CONTEXT_DATA/CTX_ROW" >
														<div class="row_asset">
															<xsl:for-each select="CTX_COL_ITEM" >							
																<xsl:choose>
																											
																	<xsl:when test="@ColNum='2' ">
																		<span style="margin-top: 0px; margin-bottom: 0px; "><span  class="h4-label">ACCOUNT:  </span><span class="body-navy"><xsl:value-of select="@Value"/></span></span>
																		
																	</xsl:when>										
																	<xsl:when test="@ColNum='4'">
																		<p style="margin-top: 0px; margin-bottom: 0px"><span class="h4-label">DESCRIPTION:  </span><span class="body-navy"><xsl:value-of select="@Value"/></span></p>
																	</xsl:when>
																	<xsl:when test="@ColNum='5'">
																		<p style="margin-top: 0px; margin-bottom: 0px"><span class="h4-label">AMOUNT:</span><span class="body-navy"> <xsl:value-of select="@Value"/></span></p>
																	</xsl:when>
																	<xsl:when test="@ColNum='6'">
																		<p style="margin-top: 0px; margin-bottom: 0px"><span class="h4-label">DATE: </span><span class="body-navy"><xsl:value-of select="@Value"/></span></p>
																	</xsl:when>
																	<xsl:when test="@ColNum='7'">
																		<p style="margin-top: 0px; margin-bottom: 0px"><span class="h4-label">REISSUE KEY: </span><span class="body-navy"><xsl:value-of select="@Value"/></span></p>
																	</xsl:when>
																	<xsl:when test="@ColNum='0'">
																		<p class="section-header" style="margin-top: 0px; margin-bottom: 0px; "><xsl:value-of select="@Value"/></p>
																	</xsl:when>
																	<xsl:when test="@ColNum='1'">
																		<p class="body-navy" style="margin-top: 0px; margin-bottom: 0px;  display: block "><xsl:value-of select="@Value"/></p>
																	</xsl:when>
																	<xsl:otherwise>
																		<span class="body-navy" style="margin-top: 0px; margin-bottom: 0px;"><xsl:value-of select="@Value"/></span>
																	</xsl:otherwise>
																
																</xsl:choose>								
															</xsl:for-each>
														</div>
													</xsl:for-each>
													</xsl:if>
												</div>
											</xsl:if>
												<!-- end Asset table -->
												<xsl:for-each
													select="LOAN_FEEDBACK/ASSET_VERIFICATION_DATA/VERIFIED_ASSETS/VERIFICATION_ASSET">
												</xsl:for-each>
											</div><!-- /panel-body -->
										</div><!-- /panel -->
									</div><!-- /Print Wrapper -->
								</xsl:otherwise>
							</xsl:choose>
							<div class="CV">
								<a class="anchor" id="CalculatedValues"></a>
								<h1 class="floating-header">Calculated Values</h1>
								<div class="panel panel-default pretty-pagebreak">
									<div class="panel-body panel-padding">
										<div style="text-align:center">
											<div class="col-4   ">
												<p class="h4-label ">LTV</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorInitialLTVRatio">
														<div class="CVS">
															<p>
																<xsl:value-of
																	select="LOAN_FEEDBACK/@LoanProspectorInitialLTVRatio" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="CVS">
															<p>N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">TLTV</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorInitialTotalLTVRatio">
														<div class="CVS">
															<p>
																<xsl:value-of
																	select="LOAN_FEEDBACK/@LoanProspectorInitialTotalLTVRatio" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="CVS">
															<p>N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">HTLTV</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FREHigherTotalLTV">
														<div class="CVS">
															<p>
																<xsl:value-of select="LOAN_FEEDBACK/@FREHigherTotalLTV" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="CVS">
															<p>N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
							</div><!-- /Print Wrapper -->
							<div class="BS">
								<a class="anchor" id="BorrowerInformation"></a>
								<h1 class="floating-header">Borrower Information</h1>
								<div class="panel panel-default">
									<div class="panel-body panel-padding pretty-pagebreak">
										<div class="row row_bot">
											<div class="col-4">
												<p class="header-contained top-header">Present Address:</p>
											</div>
											<div class="col-4">
												<xsl:for-each select="LOAN_APPLICATION/BORROWER/_RESIDENCE">
													<xsl:sort order="ascending" />
													<!-- Only want first BORROWER/RESIDENCE -->
													<xsl:if test="position() = 1">
														<xsl:choose>
															<xsl:when test="@_StreetAddress">
																<p class="body-navy">
																	<xsl:value-of select="@_StreetAddress" />, <xsl:value-of select="@_City" />,&#160;<xsl:value-of select="@_State" />&#160;<xsl:value-of select="@_PostalCode" />
																</p>
															</xsl:when>
															<xsl:when test="@_City">
																<p class="body-navy">
																	<xsl:value-of select="@_City" />,&#160;<xsl:value-of select="@_State" />&#160;<xsl:value-of select="@_PostalCode" />
																</p>
															</xsl:when>
															<xsl:when test="@_State">
																<p class="body-navy">
																	<xsl:value-of select="@_State" />&#160;<xsl:value-of select="@_PostalCode" />
																</p>
															</xsl:when>
															<xsl:when test="@_PostalCode">
																<p class="body-navy">
																	<xsl:value-of select="@_PostalCode" />
																</p>
															</xsl:when>
															<xsl:otherwise>
																<p class="na-gray">N/A</p>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:if>
												</xsl:for-each>
											</div>
										</div>
										<hr class="LINE_2"></hr>
										<div>
											<div class="col-4   ">
												<p class="h4-label">PROPOSED HOUSING (PITI)</p>
												<xsl:choose>
													<xsl:when test="CALCULATIONS[@_Name = 'ProposedHousingExpense']">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name = 'ProposedHousingExpense']/@_Value,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">PRESENT HOUSING EXPENSE</p>
												<xsl:choose>
													<xsl:when
														test="CALCULATIONS[@_Name='TotalPresentHousingExpense']">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name='TotalPresentHousingExpense']/@_Value,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">HOUSING RATIO</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@HousingRatioPercent">
														<div class="BI">
															<p>
																<xsl:value-of select="LOAN_FEEDBACK/@HousingRatioPercent" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="BI">
															<p class="na-gray">N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div>
											<div class="col-4   ">
												<p class="h4-label">DEBT RATIO</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@DebtRatioPercent">
														<div class="BI">
															<p>
																<xsl:value-of select="LOAN_FEEDBACK/@DebtRatioPercent" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="BI">
															<p class="na-gray">N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">OCCUPANT HOUSING RATIO</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FREOccupantHousingRatio">
														<div class="BI">
															<p>
																<xsl:value-of
																	select="format-number(LOAN_FEEDBACK/@FREOccupantHousingRatio, ',###')" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="BI">
															<p class="na-gray">N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">OCCUPANT DEBT RATIO</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FREOccupantDebtRatio">
														<div class="BI">
															<p>
																<xsl:value-of
																	select="format-number(LOAN_FEEDBACK/@FREOccupantDebtRatio, ',###')" />%
															</p>
														</div>
													</xsl:when>
													<xsl:otherwise>
														<div class="BI">
															<p class="na-gray">N/A</p>
														</div>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div>
											<div class="col-4   ">
												<p class="h4-label">MAX MORTGAGE LIMIT</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@MortgageLoanLimit">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_FEEDBACK/@MortgageLoanLimit,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4    ">
												<p class="h4-label">TOTAL MONTHLY INCOME</p>
												<xsl:choose>
													<xsl:when test="CALCULATIONS[@_Name = 'TotalIncome']">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name = 'TotalIncome']/@_Value,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">TOTAL MONTHLY DEBT</p>
												<xsl:choose>
													<xsl:when test="CALCULATIONS[@_Name = 'TotalMonthlyDebt']">
														<p class="body-navy">$<xsl:value-of
															select="format-number(CALCULATIONS[@_Name = 'TotalMonthlyDebt']/@_Value,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row hide">
											<div class="col-4 hide ">
												<p class="h4-label">RESERVES</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/TRANSACTION_DETAIL/@FREReservesAmount">
														<p class="body-navy">$<xsl:value-of
															select="format-number(LOAN_APPLICATION/TRANSACTION_DETAIL/@FREReservesAmount,'###,##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4  hide ">
												<p class="h4-label">NON-OCCUPANT INCOME</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@MortgageLoanLimit">
														<p class="na-gray">$<xsl:value-of
															select="format-number(LOAN_FEEDBACK/@MortgageLoanLimit,',##0.00')" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   hide">
												<p class="h4-label">NON-OCCUPANT DEBT</p>
											</div><!-- /col -->
										</div><!-- /row -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
							</div><!-- /Print Wrapper -->
							<div class="TI">
								<a class="anchor" id="TransactionInformation"></a>
								<h1 class="floating-header">Transaction Information</h1>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">ORIGINATING COMPANY</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@OriginatingCompanyName">
														<p class="body-navy">
															<xsl:value-of select="LOAN_FEEDBACK/@OriginatingCompanyName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray"> N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">SUBMITTING COMPANY</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@SubmittingCompanyName">
														<p class="body-navy">
															<xsl:value-of select="LOAN_FEEDBACK/@SubmittingCompanyName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">NUMBER OF SUBMISSIONS</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorSubmissionNumber &lt; 10">
														<p class="body-navy">
															0
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorSubmissionNumber" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorSubmissionNumber" />
														</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">DATE/TIME REQUESTED</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorSubmissionDate">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorSubmissionDate" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">DATE/TIME ASSESSED</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorEvaluationDate">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorEvaluationDate" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">SELLER NUMBER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@SellerServicerNumber">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@SellerServicerNumber" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4">
												<p class="h4-label">AUS Transaction Number:</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorTransactionIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4">
												<p class="h4-label">Loan Prospector ID:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@LoanProspectorLoanIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
											<div class="col-4">
												<p class="h4-label">Transaction ID:</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRELoanProspectorTransactionIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</div><!-- /row -->
										<div class="row row_middle">
											<div class="col-4   ">
												<p class="h4-label">TPO NUMBER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@ThirdPartyOriginatorIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@ThirdPartyOriginatorIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
											<div class="col-4   ">
												<p class="h4-label">NOTP NUMBER</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@NonOriginatingThirdPartyIdentifier">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_APPLICATION/EXTENSION_LOAN_APPLICATION/FRE_REQUESTING_PARTY/@NonOriginatingThirdPartyIdentifier" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col -->
										</div><!-- /row -->
										<hr></hr>
										<div class="row row_middle">
											<div class="col-12">
												<p class="header-contained Header_mid">Assignment Information</p>
											</div>
										</div>
										<div class="row row_middle">
											<div class="col-4">
												<p class="h4-label">Assignment Status:</p>
												<p class="body-navy">
													<xsl:value-of
														select="LOAN_FEEDBACK/@FRELoanAssignmentStatusDescription" />
												</p>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Assigned By:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@AssignFromUnparsedName">
														<p class="body-navy">
															<xsl:value-of select="LOAN_FEEDBACK/@AssignFromUnparsedName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Released By:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FRELoanReleaseFromUnparsedName">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRELoanReleaseFromUnparsedName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
										</div><!-- /row -->
										<div class="row row_bot">
											<div class="col-4">
												<p class="h4-label">Assigned MSP:</p>
												<xsl:choose>
													<xsl:when
														test="LOAN_FEEDBACK/@ FRELoanMSPAssignmentUnparsedName">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRELoanMSPAssignmentUnparsedName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Assigned To:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@AssignToUnparsedName">
														<p class="body-navy">
															<xsl:value-of select="LOAN_FEEDBACK/@AssignToUnparsedName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
											<div class="col-4">
												<p class="h4-label">Released To:</p>
												<xsl:choose>
													<xsl:when test="LOAN_FEEDBACK/@FRELoanReleaseToUnparsedName">
														<p class="body-navy">
															<xsl:value-of
																select="LOAN_FEEDBACK/@FRELoanReleaseToUnparsedName" />
														</p>
													</xsl:when>
													<xsl:otherwise>
														<p class="na-gray">N/A</p>
													</xsl:otherwise>
												</xsl:choose>
											</div><!-- /col-4 -->
										</div><!-- /row -->
									</div><!-- /panel-body -->
									<div id="feedback"></div>
								</div><!-- /panel -->
							</div><!-- /Print Wrapper -->
							<div class="FB">
								<a class="anchor" id="FeedbackSummary"></a>
								<h1 class="floating-header">
									Feedback Summary&#160;
									<span class="badge badge-custom">
										<xsl:value-of
											select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE[@_Type='LoanProcessingMessages' or @_Type='CreditMessages']/FEEDBACK_MESSAGE )"></xsl:value-of>
									</span>
								</h1>
								<div class="panel panel-default pretty-pagebreak">
									<div class="panel-body panel-padding">
										<div class="row no-margin-row">
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label">
													EMPLOYMENT
													<br></br>
													AND INCOME
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='EmploymentAndIncome'])"></xsl:value-of>
													</p>
												</div>
												<a href="#EmploymentAndIncome" class="btn-secondary-md no-print"
													style="margin:auto">RESULTS</a>
											</div>
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label">
													ASSETS
													<br></br>
													AND RESERVES
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='AssetsAndReserves'])"></xsl:value-of>
													</p>
												</div>
												<a href="#AssetsAndReserves" class="btn-secondary-md btn-specs no-print"
													style="margin:auto">RESULTS</a>
											</div>
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label" style="white-space:nowrap !important;">
													CREDIT
													<br></br>
													AND LIABILITIES
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count( LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='CreditAndLiabilities'] | LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageTag='CreditRiskComment'] | LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageTag='RepositoryReasonMessage'] )"></xsl:value-of>
													</p>
												</div>
												<a href="#CreditAndLiabilities" class="btn-secondary-md btn-specs no-print"
													style="margin:auto">RESULTS</a>
											</div>
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label" style="white-space:nowrap !important;">
													PROPERTY
													<br></br>
													AND APPRAISAL
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='PropertyAndAppraisal'])"></xsl:value-of>
													</p>
												</div>
												<a href="#PropertyAndAppraisal" class="btn-secondary-md no-print"
													style="margin:auto">RESULTS</a>
											</div>
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label">
													GENERAL
													<br></br>
													MESSAGES
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='GeneralMessages'])"></xsl:value-of>
													</p>
												</div>
												<a href="#GeneralMessages" class="btn-secondary-md no-print"
													style="margin:auto">RESULTS</a>
											</div>
											<div class="col-2 resultsBtn" style="text-align:center">
												<p class="h4-label" style="white-space:nowrap !important;">
													Mortgage
													<br></br>
													Insurance &#38; Fees
												</p>
												<div class="FS">
													<p>
														<xsl:value-of
															select="count(LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE/FEEDBACK_MESSAGE[@MessageSection='MortgageInsuranceAndFees'])"></xsl:value-of>
													</p>
												</div>
												<a href="#MortgageInsuranceAndFees" class="btn-secondary-md no-print"
													style="margin:auto">RESULTS</a>
											</div>
										</div>
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">Employment &#38; Income</h1>
								<div id="EmploymentAndIncome" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='EmploymentAndIncome']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">Assets &#38; Reserves</h1>
								<div id="AssetsAndReserves" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='AssetsAndReserves']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">Credit &#38; Liabilities</h1>
								<div id="CreditAndLiabilities" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'CreditMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageTag='CreditRiskComment']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageTag='RepositoryReasonMessage']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='CreditAndLiabilities']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">Property &#38; Appraisal </h1>
								<div id="PropertyAndAppraisal" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='PropertyAndAppraisal']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">General Messages</h1>
								<div id="GeneralMessages" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding">
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='GeneralMessages']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /panel-body -->
								</div><!-- /panel -->
								<h1 class="floating-header">Mortgage Insurance &#38; Fees  </h1>
								<div id="MortgageInsuranceAndFees" class="anchor"></div>
								<div class="panel panel-default">
									<div class="panel-body panel-padding ">
									
										<table class="table">
											<thead class="table-bg table-header">
												<tr>
													<th>Code</th>
													<th>Messages</th>
												</tr>
											</thead><!-- /thead -->
											<tbody>
												<xsl:for-each select="LOAN_FEEDBACK/LOAN_PROCESSING_MESSAGE">
													<xsl:if test="@_Type = 'LoanProcessingMessages'">
														<xsl:for-each
															select="FEEDBACK_MESSAGE[@MessageSection='MortgageInsuranceAndFees']">
															<tr>
																<td>
																	<xsl:value-of select="@MessageCode"></xsl:value-of>
																</td>
																<td>
																	<xsl:value-of select="@MessageText"></xsl:value-of>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</tbody> <!-- /tbody -->
										</table><!-- /table -->
										<p class="body-gray" style="font-size : 12px">* Representation and Warranty</p>
																		
										
									</div><!-- /panel-body -->
									
								</div><!-- /panel -->
								<xsl:choose>
									<xsl:when
										test="(LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType != 'FHA') and (LOAN_APPLICATION/MORTGAGE_TERMS/@MortgageType != 'VA')">
										<div class="row">
											<div class="col-1">
												<i class="ic-info"></i>
											</div>
											<div class="col-11">
												<p class="body-mini" style="font-style:italic">
													<b>Notice:</b>
													This feedback certificate is not a replacement or
													substitution for the requirements and information set forth
													in the Freddie Mac Single-Family Seller/Servicer Guide
													and/or Master Agreement
												</p>
												<p class="body-mini" style="font-style:italic">
													<b>Please Note:</b>
													N/A indicates that the particular value does not apply for
													this loan.
												</p>
											</div>
										</div>
									</xsl:when>
									<xsl:otherwise>
										<div class="row">
											<div class="col-1">
												<i class="ic-info"></i>
											</div>
											<div class="col-11">
												<p class="body-mini" style="font-style:italic">
													<b>Please Note:</b>
													N/A indicates that the particular value does not apply for
													this loan.
												</p>
											</div>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</div><!-- /Print Wrapper -->
							<!-- Serge new code -->
						</div> <!-- / col-10 -->
					</div> <!-- / row -->
				</div> <!--/ container -->
				<footer class="footer row" style="overflow: auto; margin-bottom:0 !important">
					<section class="col-2">
            <img>
              <xsl:attribute name="src">
                <xsl:value-of select="$VirtualRoot" />/pml_shared/FreddieMac/lib/img/lgo-freddiemac-inv.svg
              </xsl:attribute>
            </img>
					</section>
					<section class="col-4 pull-right" style="height: 40.5px;">
						<div>
							<ul>
								<li id="footer_year"
									style="font-size:11px;margin-top:15px;color:rgba(170, 183, 194, 0.5) !important;">
									&#169;
									<xsl:value-of select="substring(@_Date,7,4)"></xsl:value-of>
									Freddie Mac
								</li>
							</ul>
						</div>
						<ul>
							<li>&#032;
							</li>
						</ul>
					</section>
				</footer>
			</body>
		</html>
    <xsl:comment>
      This page uses third party software.
      For license information, please see
      https://secure.lendingqb.com/pml_shared/FreddieMac/ThirdPartyLicenses.txt
    </xsl:comment>
	</xsl:template>
</xsl:stylesheet>
<!-- end -->
