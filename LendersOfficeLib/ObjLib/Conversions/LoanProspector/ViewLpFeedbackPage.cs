/// Author: David Dao
namespace LendersOffice.Conversions.LoanProspector
{
    using System;
    using LendersOffice.Common;
    using DataAccess;

    public class ViewLpFeedbackPage : BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.Load += new EventHandler(this.PageLoad);
            base.OnInit(e);
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            string html = string.Empty;
            string errorKey = RequestHelper.GetQueryString("error");
            if (!string.IsNullOrEmpty(errorKey))
            {
                string xml = AutoExpiredTextCache.GetUserString(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, errorKey);
                html = FreddieMacXmlTransform.LoanProductAdvisorErrorsXslt.TransformXmlToHtml(xml);
                WriteResponse(html);
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(ViewLpFeedbackPage));
            dataLoan.InitLoad();

            string type = RequestHelper.GetSafeQueryString("type");

            switch (type)
            {
                case "checklist":
                    html = dataLoan.sFreddieFeedbackChecklistHtml.Value;
                    break;
                case "merged":
                    html = dataLoan.sFreddieFeedbackMergedCreditHtml.Value;
                    break;
                case "infile":
                    html = dataLoan.sFreddieFeedbackInfileHtml.Value;
                    break;
                default:
                    html = dataLoan.sFreddieFeedbackHtml.Value;
                    break;
            }

            WriteResponse(html);
        }

        private void WriteResponse(string html)
        {
            this.Response.Clear();
            this.Response.ClearContent();
            this.Response.Write(html);
            this.Context.ApplicationInstance.CompleteRequest();
        }
    }
}
