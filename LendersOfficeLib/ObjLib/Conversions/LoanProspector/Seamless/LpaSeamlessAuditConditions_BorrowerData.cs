﻿namespace LendersOffice.ObjLib.Conversions.LoanProspector.Seamless
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Conversions.LoanProspector;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    using LoanProspectorRequest;
    using Templates;
    using static LendersOffice.Conversions.LoanProspector.LoanProspectorExporter;

    /// <summary>
    /// Defines a static class for retrieving predefined sets of LPA audit conditions.
    /// </summary>
    /// <content>
    /// This partial class implementation contains conditions for borrower data auditing.
    /// </content>
    public partial class LpaSeamlessAuditConditions : ILoanAuditConditions
    {
        /// <summary>
        /// Gets the loan data audit conditions needed for LPA export.
        /// </summary>
        /// <returns>
        /// All of the conditions needed to validate a <see cref="CAppData"/> 
        /// for LPA audit validation.
        /// </returns>
        public IEnumerable<IAuditCondition<CAppData>> GetBorrowerDataConditions()
        {
            return new List<IAuditCondition<CAppData>>
            {
                new AuditCondition<CAppData>(
                    "Residence City",
                    valueToDisplay: app => app.aCity,
                    lqbFieldId: nameof(CAppData.aBAddrYrs),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residence Postal Code",
                    valueToDisplay: app => app.aZip,
                    lqbFieldId: nameof(CAppData.aBZip),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residence State",
                    valueToDisplay: app => app.aState,
                    lqbFieldId: nameof(CAppData.aBState),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residence Address",
                    valueToDisplay: app => app.aAddr,
                    lqbFieldId: nameof(CAppData.aBAddr),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residency Basis Type",
                    valueToDisplay: app => app.aAddrT.GetDescription(),
                    lqbFieldId: nameof(CAppData.aBAddrT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residency Duration Months",
                    valueToDisplay: app => YearMonthParser.Create(app.aAddrYrs).NumberOfMonths.ToString(),
                    lqbFieldId: nameof(CAppData.aBAddrYrs),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Residency Duration Years",
                    valueToDisplay: app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears.ToString(),
                    lqbFieldId: nameof(CAppData.aBAddrYrs),
                    page: LoanEditorPage.Application1003Page1),
                new CollectionAuditCondition<CAppData, OtherIncome>(
                    "Income Type Other Description",
                    collectionToAudit: app => app.aOtherIncomeList,
                    displayMessage: "Other Income Description is required for LPA submission.",
                    collectionItemValueToDisplay: otherIncome => otherIncome.Desc,
                    collectionItemIsValid: (app, otherIncome) => otherIncome.IsForCoBorrower != (app.BorrowerModeT == E_BorrowerModeT.Coborrower) || otherIncome.Amount <= 0 || !string.IsNullOrEmpty(otherIncome.Desc),
                    page: LoanEditorPage.MonthlyIncome),
                new AuditCondition<CAppData>(
                    dataPointName: "Assets and Liabilities Completed Jointly",
                    requiredIf: (app) => false,
                    valueToDisplay: (app) => app.aAsstLiaCompletedNotJointly ? "Not Jointly" : "Jointly",
                    valueIsValid: (app) => app.aHasSpouse || app.aAsstLiaCompletedNotJointly,
                    displayMessage: "If there is only one borrower on an application, the application must be completed \"Not Jointly\"",
                    lqbFieldId: "aAsstLiaCompletedJointly",
                    page: LoanEditorPage.Assets),
                new CollectionAuditCondition<CAppData, IAssetRegular>(
                    "Asset Cash or Market Value Amount",
                    collectionToAudit: app => app.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Regular).Cast<IAssetRegular>()
                                .Where(asset => (asset.OwnerT.EqualsOneOf(E_AssetOwnerT.Borrower, E_AssetOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Borrower)
                                                || (asset.OwnerT.EqualsOneOf(E_AssetOwnerT.CoBorrower, E_AssetOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Coborrower)),
                    collectionItemValueToDisplay: asset => $"{asset.AssetT.GetDescription()}{(asset.AssetT == E_AssetRegularT.OtherIlliquidAsset || asset.AssetT == E_AssetRegularT.OtherLiquidAsset ? " - " + asset.OtherTypeDesc : string.Empty)}: {asset.Val_rep}",
                    collectionItemIsValid: (loan, asset) => ToMismo(asset.AssetT) == E_MismoAssetType.Undefined || asset.Val >= 0,
                    displayMessage: "Asset Value must be greater than or equal to $0.00.",
                    lqbFieldId: nameof(IAsset.Val),
                    page: LoanEditorPage.Assets),
                new CollectionAuditCondition<CAppData, IAssetRegular>(
                    "Other Asset Type Description",
                    collectionToAudit: app => app.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Regular).Cast<IAssetRegular>()
                                .Where(asset => (asset.OwnerT.EqualsOneOf(E_AssetOwnerT.Borrower, E_AssetOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Borrower)
                                                || (asset.OwnerT.EqualsOneOf(E_AssetOwnerT.CoBorrower, E_AssetOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Coborrower)),
                    collectionItemValueToDisplay: asset => asset.OtherTypeDesc,
                    collectionItemIsValid: (loan, asset) => !(asset.AssetT == E_AssetRegularT.OtherIlliquidAsset || asset.AssetT == E_AssetRegularT.OtherLiquidAsset) || !string.IsNullOrEmpty(asset.OtherTypeDesc),
                    displayMessage: "Asset Type Description is required for Other Asset Type.",
                    lqbFieldId: nameof(IAsset.AssetT),
                    page: LoanEditorPage.Assets),
                new CollectionAuditCondition<CAppData, ILiabilityRegular>(
                    "Liability Monthly Payment Amount",
                    collectionToAudit: app => GetLiabilitiesToAudit(app, includeLiabilitiesExcludedFromUnderwriting: true),
                    collectionItemValueToDisplay: liability => $"{liability.ComNm} ({ToMismo(liability.DebtT, liability.Desc).GetDescription()}): {liability.Pmt_rep}",
                    collectionItemIsValid: (loan, liability) => (ToMismo(liability.DebtT, liability.Desc) == E_MismoLiabilityType.Undefined)
                                                                || liability.Pmt >= 0,
                    displayMessage: "Liability Payment Amount is required to be positive for LPA submission.",
                    lqbFieldId: "liability." + nameof(ILiabilityRegular.Pmt),
                    tpoPage: LoanEditorPage.Application1003Page2,
                    page: LoanEditorPage.Liabilities),
                new CollectionAuditCondition<CAppData, ILiability>(
                    "Liability Remaining Term Months",
                    collectionToAudit: app => app.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Other | E_DebtGroupT.Alimony | E_DebtGroupT.ChildSupport).Cast<ILiability>().Where(lia => LiabilityBelongsToBorrower(lia, app)),
                    collectionItemValueToDisplay: liability => liability is ILiabilityRegular ?
                        $"{((ILiabilityRegular)liability).ComNm} ({ToMismo(((ILiabilityRegular)liability).DebtT, ((ILiabilityRegular)liability).Desc).GetDescription()}): {liability.RemainMons_rep}"
                        : $"{EnumUtilities.GetDescription(liability.DebtT)}: {liability.RemainMons_rep}",
                    collectionItemIsValid: (loan, liability) => liability == null || liability.Pmt == 0 || liability.RemainMons_raw.Equals("(R)", StringComparison.OrdinalIgnoreCase) || loan.m_convertLos.ToCount(liability.RemainMons_raw) > 0,
                    displayMessage: "Liability Remaining Months is required for LPA submission.",
                    lqbFieldId: "liability." + nameof(ILiability.RemainMons_raw),
                    tpoPage: LoanEditorPage.Application1003Page2,
                    page: LoanEditorPage.Liabilities),
                new CollectionAuditCondition<CAppData, ILiabilityRegular>(
                    "Liability Unpaid Balance Amount",
                    collectionToAudit: app => GetLiabilitiesToAudit(app, includeLiabilitiesExcludedFromUnderwriting: true),
                    collectionItemValueToDisplay: liability => $"{liability.ComNm} ({ToMismo(liability.DebtT, liability.Desc).GetDescription()}): {liability.Bal_rep}",
                    collectionItemIsValid: (loan, liability) => (ToMismo(liability.DebtT, liability.Desc) == E_MismoLiabilityType.Undefined)
                                                                || liability.Bal > 0,
                    displayMessage: "Liability Unpaid Balance is required to be positive for LPA submission.",
                    lqbFieldId: "liability." + nameof(ILiabilityRegular.Bal),
                    tpoPage: LoanEditorPage.Application1003Page2,
                    page: LoanEditorPage.Liabilities),

                // For HMDA fields, Some entry is required (including "declined to specify") for non-Prequalification loans.
                // Further, for Face-to-Face interviews, just "declined to specify" isn't enough; some guess needs to be made.
                new AuditCondition<CAppData>(
                    "HMDA Gender Type",
                    requiredIf: app => false,
                    valueToDisplay: app => string.Empty,
                    valueIsValid: app => app.LoanData.sFredProcPointT == E_sFredProcPointT.Prequalification 
                                        || (GetGenderRefusalIndicator(app.aGender) == E_YesNoIndicator.Y && app.aIntrvwrMethodT != E_aIntrvwrMethodT.FaceToFace)
                                        || GetGenderType(app.aGender) != GovernmentMonitoringDetailHmdaGenderType.Undefined,
                    displayMessage: "At least one Gender option must be selected for LPA export.",
                    lqbFieldId: nameof(CAppData.aBGender),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "HMDA Ethnicity Type",
                    requiredIf: app => false,
                    valueToDisplay: app => string.Empty,
                    valueIsValid: app => app.LoanData.sFredProcPointT == E_sFredProcPointT.Prequalification 
                                        || (app.aDoesNotWishToProvideEthnicity && app.aIntrvwrMethodT != E_aIntrvwrMethodT.FaceToFace)
                                        || app.aHispanicT != E_aHispanicT.LeaveBlank,
                    displayMessage: "At least one Ethnicity option must be selected for LPA export.",
                    lqbFieldId: nameof(CAppData.aBHispanicT),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "HMDA Race Type",
                    requiredIf: app => false,
                    valueToDisplay: app => string.Empty,
                    valueIsValid: app => app.LoanData.sFredProcPointT == E_sFredProcPointT.Prequalification 
                                        || (app.aDoesNotWishToProvideRace && app.aIntrvwrMethodT != E_aIntrvwrMethodT.FaceToFace)
                                        || (app.aIsAmericanIndian || app.aIsAsian || app.aIsBlack || app.aIsPacificIslander || app.aIsWhite),
                    displayMessage: "At least one Race option must be selected for LPA export.",
                    lqbFieldId: "aBRace",
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Borrower Birth Date",
                    valueToDisplay: app => app.aDob_rep,
                    requiredMessage: "\"Date Of Birth\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CAppData.aBDob),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Borrower First Name",
                    valueToDisplay: app => app.aFirstNm,
                    requiredMessage: "\"First Name\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CAppData.aBFirstNm),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Borrower Last Name",
                    valueToDisplay: app => app.aLastNm,
                    requiredMessage: "\"Last Name\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CAppData.aBLastNm),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Borrower Social Security Number",
                    valueToDisplay: app => app.aSsn,
                    requiredMessage: "\"Social Security Number\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CAppData.aBSsn),
                    page: LoanEditorPage.Application1003Page1),

                new AuditCondition<CAppData>(
                    "Former Residence City",
                    valueToDisplay: app => app.aPrev1City,
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residence City\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1City),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Former Residence Postal Code",
                    valueToDisplay: app => app.aPrev1Zip,
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residence Postal Code\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1Zip),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Former Residence State",
                    valueToDisplay: app => app.aPrev1State,
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residence State\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1State),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Former Residence Address",
                    valueToDisplay: app => app.aPrev1Addr,
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residence Address\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1Addr),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Former Residency Basis Type",
                    valueToDisplay: app => ((E_aBAddrT)app.aPrev1AddrT).GetDescription(),
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residency Basis Type\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1AddrT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CAppData>(
                    "Former Residency Duration Years",
                    valueToDisplay: app => YearMonthParser.Create(app.aPrev1AddrYrs).NumberOfYears.ToString(),
                    requiredIf:
                        app => YearMonthParser.Create(app.aAddrYrs).NumberOfYears < 2,
                    requiredMessage: "\"Former Residency Duration Years\" is a required field if current residency less than 2 years.",
                    lqbFieldId: nameof(CAppData.aBPrev1AddrYrs),
                    page: LoanEditorPage.Application1003Page1),

                new AuditCondition<CAppData>(
                    "Bankruptcy Past Seven Years Indicator",
                    valueToDisplay: app => app.aDecBankrupt,
                    requiredMessage: "\"b. Have you been declared bankrupt within the past 7 years? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecBankrupt),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Property Foreclosure Past Seven Years Indicator",
                    valueToDisplay: app => app.aDecForeclosure,
                    requiredMessage: "\"c. Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecForeclosure),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Presently Delinquent Indicator",
                    valueToDisplay: app => app.aDecDelinquent,
                    requiredIf: app => app.LoanData.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA),
                    requiredMessage: "\"f. Are you presently delinquent or in default on any Federal debt or any other loan, mortgage, financial obligation bond, or loan guarantee?\" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecDelinquent),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Citizenship Indicator",
                    valueToDisplay: app => app.aDecCitizen,
                    requiredMessage: "\"j. Are you a U.S. citizen? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecCitizen),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Permanent Residency Indicator",
                    valueToDisplay: app => app.aDecResidency,
                    requiredMessage: "\"k. Are you a permanent resident alien? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecResidency),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Intent To Occupy Indicator",
                    valueToDisplay: app => app.aDecOcc,
                    requiredMessage: "\"l. Do you intend to occupy the property as your primary residence? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecOcc),
                    page: LoanEditorPage.Application1003Page3),
                new AuditCondition<CAppData>(
                    "Homeowner Past Three Years Indicator",
                    valueToDisplay: app => app.aDecPastOwnership,
                    requiredMessage: "\"m. Have you had an ownership interest in a property in the last three years? \" is a required question.",
                    lqbFieldId: nameof(CAppData.aBDecPastOwnership),
                    page: LoanEditorPage.Application1003Page3),
            };
        }

        /// <summary>
        /// Gets the collection of liabilities that are to be audited by this borrower data auditor.
        /// </summary>
        /// <param name="app">The application from which the liabilities are retrieved.</param>
        /// <param name="includeLiabilitiesExcludedFromUnderwriting">Specifies whether to include liabilities that are excluded from underwriting.</param>
        /// <returns>An enumerable list of liability objects.</returns>
        private static IEnumerable<ILiabilityRegular> GetLiabilitiesToAudit(CAppData app, bool includeLiabilitiesExcludedFromUnderwriting)
        {
            return app.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular | E_DebtGroupT.Other).Cast<ILiabilityRegular>()
                                .Where(lia => (includeLiabilitiesExcludedFromUnderwriting || !lia.ExcFromUnderwriting) && LiabilityBelongsToBorrower(lia, app));
        }

        /// <summary>
        /// Determines whether the given liability belongs to the given borrower, using the application's <see cref="CAppData.BorrowerModeT"/>.
        /// </summary>
        /// <param name="lia">The liability to check.</param>
        /// <param name="app">The app to check, representing a borrower.</param>
        /// <returns>A boolean indicating whether the liability belongs to the borrower.</returns>
        private static bool LiabilityBelongsToBorrower(ILiability lia, CAppData app) =>
            (lia.OwnerT.EqualsOneOf(E_LiaOwnerT.Borrower, E_LiaOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Borrower)
            || (lia.OwnerT.EqualsOneOf(E_LiaOwnerT.CoBorrower, E_LiaOwnerT.Joint) && app.BorrowerModeT == E_BorrowerModeT.Coborrower);
    }
}
