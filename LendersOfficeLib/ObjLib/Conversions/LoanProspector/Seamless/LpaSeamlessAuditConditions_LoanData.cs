﻿namespace LendersOffice.ObjLib.Conversions.LoanProspector.Seamless
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.Migration;
    using LoanProspectorRequest;
    using Templates;

    /// <summary>
    /// Defines a singleton class for retrieving predefined sets of LPA audit conditions.
    /// </summary>
    /// <content>
    /// This partial class implementation contains conditions for loan data auditing.
    /// </content>
    public partial class LpaSeamlessAuditConditions : ILoanAuditConditions
    {
        /// <summary>
        /// Initializes static members of the <see cref="LpaSeamlessAuditConditions"/> class.
        /// Initializes the singleton instance.
        /// </summary>
        static LpaSeamlessAuditConditions()
        {
            Singleton = new LpaSeamlessAuditConditions();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="LpaSeamlessAuditConditions"/> class from being created.
        /// </summary>
        private LpaSeamlessAuditConditions()
        {
        }

        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        /// <value>A singleton.</value>
        public static LpaSeamlessAuditConditions Singleton { get; }

        /// <summary>
        /// Gets the loan data audit conditions needed for LPA export.
        /// </summary>
        /// <returns>
        /// All of the conditions needed to validate a <see cref="CPageData"/> 
        /// for LPA audit validation.
        /// </returns>
        public IEnumerable<IAuditCondition<CPageData>> GetLoanDataConditions()
        {
            return new List<IAuditCondition<CPageData>>
            {
                // One of Appraised Value and Estimated Value Amount are required for all Refinance loans and for non-Prequalification, non-Refinance loans.
                // Neither is required for Prequalification, non-Refinance loans.
                new AuditCondition<CPageData>(
                    "Property Appraised Value Amount",
                    loan => loan.sApprVal_rep,
                    requiredIf: loan => false,
                    valueIsValid:
                        loan =>
                            (loan.sLPurposeT != E_sLPurposeT.Refin
                                && loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.LeaveBlank, E_sFredProcPointT.Prequalification))
                            || (loan.sSpMarketVal >= 9000 && loan.sSpMarketVal <= 9999999) || (loan.sApprVal >= 9000 && loan.sApprVal <= 9999999),
                    displayMessage: "Appraised Value or Estimated Value must be between $9,000.00 and $9,999,999.00.",
                    lqbFieldId: nameof(CPageData.sApprVal),
                    page: LoanEditorPage.ThisLoanInfo),
                new AuditCondition<CPageData>(
                    "Property Estimated Value Amount",
                    loan => loan.sSpMarketVal_rep,
                    requiredIf: loan => false,
                    valueIsValid:
                        loan =>
                            (loan.sLPurposeT != E_sLPurposeT.Refin
                                && loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.LeaveBlank, E_sFredProcPointT.Prequalification))
                            || (loan.sSpMarketVal >= 9000 && loan.sSpMarketVal <= 9999999) || (loan.sApprVal >= 9000 && loan.sApprVal <= 9999999),
                    displayMessage: "Estimated Value or Appraised Value must be between $9,000.00 and $9,999,999.00.",
                    lqbFieldId: nameof(CPageData.sSpMarketVal),
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "Down Payments",
                    valueToDisplay: loan => string.Empty,
                    requiredIf: loan => false,
                    valueIsValid:
                        loan => !(loan.sLPurposeT == E_sLPurposeT.Purchase && loan.sIsUseDUDwnPmtSrc) || loan.sDUDwnPmtSrc.Any(downpayment => downpayment.Amount > 0 && !string.IsNullOrEmpty(downpayment.Source)),
                    displayMessage: "When using custom down payment sources, at least one down payment amount and source must be provided.",
                    lqbFieldId: "sDUDownPmtSrcs",
                    page: LoanEditorPage.SubmitToLpa),
                new CollectionAuditCondition<CPageData, DownPaymentGift>(
                    "Down Payment Amount",
                    collectionToAudit: loan => loan.sDUDwnPmtSrc,
                    collectionItemValueToDisplay: downPayment => $"{downPayment.Source}: {downPayment.Amount_rep}",
                    collectionItemIsValid: (loan, downPayment) =>
                            (downPayment.Amount == 0 && downPayment.Source == string.Empty)
                            || !(loan.sLPurposeT == E_sLPurposeT.Purchase || LoanProspectorExporter.ToMismoDownPaymentType(downPayment.Source) == E_MismoDownPaymentType.Undefined) || downPayment.Amount >= 0,
                    displayMessage: "Positive down payment amount is required for Purchases.",
                    lqbFieldId: "sDUDownPmtSrcs",
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "Lender Identifier",
                    valueToDisplay: loan => loan.sFHALenderIdCode,
                    requiredIf: loan => loan.sLT == E_sLT.FHA,
                    requiredMessage: "FHA Lender ID required for FHA loans.",
                    lqbFieldId: nameof(CPageData.sFHALenderIdCode),
                    page: LoanEditorPage.FhaAddendum),
                new AuditCondition<CPageData>(
                    "Sponsor Identifier",
                    valueToDisplay: loan => loan.sFHASponsorAgentIdCode,
                    requiredIf: loan => loan.sLT == E_sLT.FHA && loan.sFHALenderIdCode.Equals("loan correspondent", StringComparison.OrdinalIgnoreCase),
                    requiredMessage: "FHA Sponsor/Agent ID required for FHA loans.",
                    lqbFieldId: nameof(CPageData.sFHASponsorAgentIdCode),
                    page: LoanEditorPage.FhaAddendum),
                new AuditCondition<CPageData>(
                    "ARM Index Current Value Percent",
                    valueToDisplay: loan => loan.sRAdjIndexR_rep,
                    requiredIf: loan => loan.sFinMethT == E_sFinMethT.ARM,
                    valueIsValid: loan => loan.sRAdjIndexR >= 0,
                    requiredMessage: "Index value is required for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdjIndexR),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Buydown Period To First Adjustment Months",
                    valueToDisplay: loan => loan.sBuydwnMon1_rep,
                    requiredIf: loan => loan.sBuydown,
                    valueIsValid: loan => !loan.sBuydown || loan.sBuydwnMon1 > 0,
                    displayMessage: "Buydown Months is required for loans with a Buydown.",
                    lqbFieldId: nameof(CPageData.sBuydwnMon1),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Buydown Term Months",
                    valueToDisplay: loan => loan.sBuydwnTotalMon_rep,
                    requiredIf: loan => loan.sBuydown,
                    valueIsValid: loan => !loan.sBuydown || loan.sBuydwnTotalMon > 0,
                    displayMessage: "Total Buydown Months is required for loans with a Buydown.",
                    lqbFieldId: nameof(CPageData.sBuydwnMon1),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Buydown Initial Adjustment Percent",
                    valueToDisplay: loan => loan.sBuydwnR1stIncrease_rep,
                    requiredIf: loan => loan.sBuydown,
                    requiredMessage: "Rate Increase required for loans with a Buydown.",
                    lqbFieldId: nameof(CPageData.sBuydwnR1stIncrease),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Negative Amortization Limit Percent",
                    valueToDisplay: loan => loan.sPmtAdjMaxBalPc_rep,
                    requiredIf: loan => false,
                    valueIsValid: loan => !(loan.sNegAmortT == E_sNegAmortT.PotentialNegAmort || loan.sNegAmortT == E_sNegAmortT.ScheduledNegAmort) || loan.sPmtAdjMaxBalPc > 0,
                    displayMessage: "Negative Amortization Limit is required for loans with Negative Amortization.",
                    lqbFieldId: nameof(CPageData.sPmtAdjMaxBalPc),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Negative Amortization Type",
                    valueToDisplay: loan => string.Empty,
                    requiredIf: loan => false,
                    valueIsValid: loan => loan.sFinMethT != E_sFinMethT.ARM || loan.sNegAmortT != E_sNegAmortT.LeaveBlank,
                    displayMessage: "Negative Amortization Type is required for ARM loans.",
                    lqbFieldId: nameof(CPageData.sNegAmortT),
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "Subsequent Rate Adjustment Months",
                    valueToDisplay: loan => loan.sRAdjCapMon_rep,
                    valueIsValid: loan => !(loan.sFinMethT == E_sFinMethT.ARM && loan.sRAdjCapMon < 0),
                    displayMessage: "ARM Period is required for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdjCapMon),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Property Original Cost Amount (Refinance)",
                    valueToDisplay: loan => loan.sIsRefinancing ? loan.sSpOrigC_rep : loan.sLotOrigC_rep,
                    requiredIf: loan => false,
                    valueIsValid: loan => !loan.sIsRefinancing || loan.sSpOrigC > 0,
                    displayMessage: "Original Cost is a required field for Refinances.",
                    lqbFieldId: nameof(CPageData.sSpOrigC),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property City",
                    valueToDisplay: loan => loan.sSpCity,
                    requiredIf: loan => !loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.Prequalification, E_sFredProcPointT.LeaveBlank),
                    requiredMessage: "Property city is required for LPA submission.",
                    lqbFieldId: nameof(CPageData.sSpCity),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property County",
                    valueToDisplay: loan => loan.sSpCounty,
                    requiredIf: loan => !loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.Prequalification, E_sFredProcPointT.LeaveBlank)
                                        || loan.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA),
                    requiredMessage: "Property county is required for LPA submission.",
                    lqbFieldId: nameof(CPageData.sSpCounty),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Postal Code",
                    valueToDisplay: loan => loan.sSpZip,
                    requiredIf: loan => !loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.Prequalification, E_sFredProcPointT.LeaveBlank)
                                        || loan.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA),
                    requiredMessage: "Property ZIP Code is required for LPA submission.",
                    lqbFieldId: nameof(CPageData.sSpZip),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property State",
                    valueToDisplay: loan => loan.sSpState,
                    requiredIf: loan => !loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.Prequalification, E_sFredProcPointT.LeaveBlank)
                                        || loan.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA),
                    requiredMessage: "Property State is required for LPA submission.",
                    lqbFieldId: nameof(CPageData.sSpState),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Address",
                    valueToDisplay: loan => loan.sSpAddr,
                    requiredIf: loan => !loan.sFredProcPointT.EqualsOneOf(E_sFredProcPointT.Prequalification, E_sFredProcPointT.LeaveBlank)
                                        || loan.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA),
                    requiredMessage: "Property Address is required for LPA submission.",
                    lqbFieldId: nameof(CPageData.sSpAddr),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Type",
                    valueToDisplay: loan => Tools.Get_sGseSptFriendlyDisplay(loan.sGseSpT),
                    valueIsValid: loan => loan.sUnitsNum != 1 || loan.sGseSpT != E_sGseSpT.LeaveBlank,
                    displayMessage: "\"Property Type\" is a required field for submission.",
                    lqbFieldId: nameof(CPageData.sGseSpT),
                    page: LoanEditorPage.ThisLoanInfo),
                new AuditCondition<CPageData>(
                    "Property Type",
                    valueToDisplay: loan => Tools.Get_sGseSptFriendlyDisplay(loan.sGseSpT),
                    requiredIf: loan => false,
                    valueIsValid: loan => !loan.sGseSpT.EqualsOneOf(E_sGseSpT.ManufacturedHomeCondominium, E_sGseSpT.ManufacturedHousing),
                    displayMessage: "Not a valid value for LPA submission. Please use \"Manufactued Home Single Wide\" or \"Manufactured Home Multiwide\"",
                    lqbFieldId: nameof(CPageData.sGseSpT),
                    page: LoanEditorPage.ThisLoanInfo),
                new AuditCondition<CPageData>(
                    "Purchase Price Amount",
                    valueToDisplay: loan => loan.sPurchPrice_rep,
                    valueIsValid: loan => loan.sLPurposeT != E_sLPurposeT.Purchase || loan.sPurchPrice > 0,
                    displayMessage: "Purchase Price must be greater than $0.00.",
                    lqbFieldId: nameof(CPageData.sPurchPrice),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "ARM Index Margin Percent",
                    valueToDisplay: loan => loan.sRAdjMarginR_rep,
                    requiredIf: loan => loan.sFinMethT == E_sFinMethT.ARM,
                    requiredMessage: "ARM Index Margin is a requied field for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdjMarginR),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "ARM Index Type",
                    valueToDisplay: loan => string.Empty,
                    requiredIf: loan => false,
                    valueIsValid: loan => loan.sFinMethT != E_sFinMethT.ARM || loan.sFreddieArmIndexT != E_sFreddieArmIndexT.LeaveBlank,
                    displayMessage: "ARM Index Type is a required field for ARM loans.",
                    lqbFieldId: nameof(CPageData.sFreddieArmIndexT),
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "ARM Qualifying Rate Percent",
                    valueToDisplay: loan => loan.sQualIR_rep,
                    valueIsValid: loan => loan.sFinMethT != E_sFinMethT.ARM || loan.sQualIR > 0,
                    displayMessage: "Qualifying Rate must be greater than 0% for ARM loans.",
                    lqbFieldId: "LoanInfoUC_" + nameof(CPageData.sQualIR), // Without the prefix, another invisible ...sQualIR element gets highlighted.
                    page: LoanEditorPage.ThisLoanInfo),
                new AuditCondition<CPageData>(
                    "Payment Adjustment Periodic Cap Percent",
                    valueToDisplay: loan => loan.sPmtAdjCapR_rep,
                    requiredIf: loan => loan.sFinMethT == E_sFinMethT.ARM,
                    requiredMessage: "Payment Adjustment Cap is a required field for ARM loans.",
                    lqbFieldId: nameof(CPageData.sPmtAdjCapR),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "First Rate Adjustment Months",
                    valueToDisplay: loan => loan.sRAdj1stCapMon_rep,
                    valueIsValid: loan => loan.sFinMethT != E_sFinMethT.ARM || loan.sRAdj1stCapMon > 0,
                    displayMessage: "First Rate Adjustment Months must be greater than 0 for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdj1stCapMon),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Subsequent Rate Adjustment Months",
                    valueToDisplay: loan => loan.sRAdjCapMon_rep,
                    valueIsValid: loan => loan.sFinMethT != E_sFinMethT.ARM || loan.sRAdjCapMon > 0,
                    displayMessage: "Subsequent Rate Adjustment Months must be greater than 0 for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdjCapMon),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Rate Adjustment Periodic Cap Percent",
                    valueToDisplay: loan => loan.sRAdjCapR_rep,
                    requiredIf: loan => loan.sFinMethT == E_sFinMethT.ARM,
                    requiredMessage: "Payment Adjustment Cap is a required field for ARM loans.",
                    lqbFieldId: nameof(CPageData.sRAdjCapR),
                    page: LoanEditorPage.LoanTerms),
                new AuditCondition<CPageData>(
                    "Purpose of Refinance",
                    valueToDisplay: loan => loan.sRefPurpose,
                    requiredIf: loan => LoanProspectorExporter.ToMismo(loan.sLPurposeT, loan.sConstructionPurposeT) == E_MismoLoanPurposeType.Refinance && !loan.sIsConstructionLoan,
                    valueIsValid: loan => !(loan.sLT == E_sLT.VA && loan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance),
                    requiredMessage: "Purpose of Refinance is required for all Refinance loans.",
                    displayMessage: "Invalid Purpose of Refinance for a VA loan.",
                    lqbFieldId: nameof(CPageData.sRefPurpose),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Acquired Year",
                    valueToDisplay: loan => loan.sSpAcqYr,
                    requiredIf: loan => LoanProspectorExporter.ToMismo(loan.sLPurposeT, loan.sConstructionPurposeT) == E_MismoLoanPurposeType.Refinance && !loan.sIsConstructionLoan,
                    requiredMessage: "Property Acquired Year is required for all Refinance loans.",
                    lqbFieldId: nameof(CPageData.sSpAcqYr),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Amount of Existing Lien",
                    valueToDisplay: loan => loan.sSpLien_rep,
                    requiredIf: loan => LoanProspectorExporter.ToMismo(loan.sLPurposeT, loan.sConstructionPurposeT) == E_MismoLoanPurposeType.Refinance,
                    requiredMessage: "Existing Lien Amount is required for all Refinance loans.",
                    lqbFieldId: nameof(CPageData.sSpLien),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Building Status Type",
                    valueToDisplay: loan => EnumUtilities.GetDescription(loan.sBuildingStatusT),
                    requiredIf: loan => 
                          (loan.sFreddieConstructionT == E_sFreddieConstructionT.NewlyBuilt
                        || loan.sFreddieConstructionT == E_sFreddieConstructionT.ConstructionConversion)
                        && (loan.sLT == E_sLT.FHA || loan.sLT == E_sLT.VA),
                    requiredMessage: "Building Status Type is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sBuildingStatusT),
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "Lien Priority Type",
                    valueToDisplay: loan => loan.sLienPosT.GetDescription(),
                    valueIsValid: loan => LoanProspectorExporter.ToMismo(loan.sLienPosT) != E_MismoLoanFeaturesLienPriorityType.Undefined,
                    displayMessage: "\"Lien Position\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sFredProcPointT),
                    page: LoanEditorPage.SubmitToLpa),
                new AuditCondition<CPageData>(
                    "Loan Purpose Type",
                    valueToDisplay: loan => loan.sLPurposeT.GetDescription(),
                    valueIsValid: loan => !LoanProspectorExporter.ToMismo(loan.sLPurposeT, loan.sConstructionPurposeT).EqualsOneOf(E_MismoLoanPurposeType.Undefined, E_MismoLoanPurposeType.Unknown),
                    displayMessage: "\"Loan Purpose Type\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sLPurposeT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Loan Purpose Type",
                    valueToDisplay: loan => loan.sLPurposeT.GetDescription(),
                    valueIsValid: loan => LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges)
                        || (loan.sLPurposeT != E_sLPurposeT.Construct && loan.sLPurposeT != E_sLPurposeT.ConstructPerm),
                    displayMessage: "\"Construction\" and \"Construction-to-Permanent\" are not supported Loan Purposes in LPA",
                    lqbFieldId: nameof(CPageData.sLPurposeT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Usage Type",
                    valueToDisplay: loan => loan.GetAppData(0).aOccT.GetDescription(),
                    valueIsValid: loan => LoanProspectorExporter.ToMismo(loan.GetAppData(0).aOccT) != E_MismoLoanPurposePropertyUsageType.Undefined,
                    displayMessage: "\"Occupancy Type\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CAppData.aOccT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Base Loan Amount",
                    valueToDisplay: loan => loan.sLAmt1003_rep,
                    requiredMessage: "\"Loan Amount\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sLAmt1003),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Loan Amortization Type",
                    valueToDisplay: loan => loan.sFinMethT.GetDescription(),
                    valueIsValid: loan => LoanProspectorExporter.ToMismo(loan.sFinMethT) != E_MismoMortgageTermsLoanAmortizationType.Undefined,
                    displayMessage: "\"Amortization Type\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sFinMethT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Mortgage Type",
                    valueToDisplay: loan => loan.sLT.GetDescription(),
                    valueIsValid: loan => LoanProspectorExporter.ToMismo(loan.sLT) != E_MismoMortgageTermsMortgageType.Undefined,
                    displayMessage: "\"Loan Type\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sLT),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Requested Interest Rate Percent",
                    valueToDisplay: loan => loan.sNoteIR_rep,
                    requiredMessage: "\"Intrest Rate\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sNoteIR),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Property Financed Number of Units",
                    valueToDisplay: loan => loan.sUnitsNum_rep,
                    requiredMessage: "\"Number of Units\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sUnitsNum),
                    page: LoanEditorPage.Application1003Page1),
                new AuditCondition<CPageData>(
                    "Loan Amortization Term Months",
                    valueToDisplay: loan => loan.sTerm_rep,
                    requiredMessage: "\"Term\" is a required field for LPA submission.",
                    lqbFieldId: nameof(CPageData.sTerm),
                    page: LoanEditorPage.Application1003Page1),
            };
        }
    }
}
