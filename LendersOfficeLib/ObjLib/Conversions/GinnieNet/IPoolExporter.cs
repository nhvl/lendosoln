﻿using System;
using System.Collections.Generic;
namespace LendersOffice.ObjLib.Conversions
{
    public interface IPoolExporter
    {
        HashSet<string> Errors
        {
            get;
        }
        HashSet<string> Warnings
        {
            get;
        }
        string OutputFileName
        {
            get;
        }
        bool TryExport(out byte[] output);
    }
}
