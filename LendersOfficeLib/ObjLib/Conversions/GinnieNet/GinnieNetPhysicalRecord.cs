﻿using System;
using System.Linq;
using DataAccess;

namespace LendersOffice.ObjLib.Conversions.GinnieNet
{
    public class GinnieNetPhysicalRecord
    {
        private static string s_ExportFailMessage = "GinnieNet Export failed.";
        private static int s_recordTypeIdentifierLength = 3;
        private static int s_recordTotalLength = 80;


        private char[] m_data;
        private int[] m_startIndices;


        public GinnieNetPhysicalRecord(string type, int[] startIndices)
        {
            if (type.Length != s_recordTypeIdentifierLength)
            {
                throw new CBaseException(s_ExportFailMessage, "Physical Record \"type\" must be " + s_recordTypeIdentifierLength + " characters; Was: \"" + type + "\" (" + type.Length + " characters)");
            }
            m_data = new char[s_recordTotalLength];
            m_startIndices = startIndices;
            for (int index = s_recordTypeIdentifierLength; index < s_recordTotalLength; index++)
            {
                m_data[index] = ' ';
            }
            this[1] = type;
        }

        /* Start-End mapping based on GinnieNET_Single_Family_File_Layout-new.pdf
         * See: \\megatron\LendersOffice\Integration\Ginnie Mae\
         * 
         * Use fieldIndex = [1, 2, ...] (no zero value)
         * in order to match specification PDF
         * 
         */
        public string this[int fieldIndex]
        {
            get
            {
                int start; //INDEX: First character of field
                int end; //INDEX: Last character of field

                if (fieldIndex > 0 && fieldIndex < m_startIndices.Length)
                {
                    start = m_startIndices[fieldIndex - 1];
                    end = m_startIndices[fieldIndex] - 1;
                }
                else if (fieldIndex == m_startIndices.Length)
                {
                    start = m_startIndices[fieldIndex - 1];
                    end = 79;
                }
                else
                {
                    throw new CBaseException(s_ExportFailMessage, string.Format("Field index not in valid range for record [{0}]; index={1}", this[1], fieldIndex));
                }

                return new String(m_data.Skip(start).Take(end - start + 1).ToArray());
            }
            set
            {
                int start; //INDEX: First character of field
                int end; //INDEX: Last character of field
                if (fieldIndex > 0 && fieldIndex < m_startIndices.Length)
                {
                    start = m_startIndices[fieldIndex - 1];
                    end = m_startIndices[fieldIndex] - 1;
                }
                else if (fieldIndex == m_startIndices.Length)
                {
                    start = m_startIndices[fieldIndex - 1];
                    end = 79;
                }
                else
                {
                    throw new CBaseException(s_ExportFailMessage, string.Format("Field index not in valid range for record [{0}]; index={1}", this[1], fieldIndex));
                }

                string newValue = value;
                int fieldMaxLength = end - start + 1;
                if (value.Length > fieldMaxLength)
                {
                    newValue = value.Substring(0, fieldMaxLength);
                    Tools.LogWarning(
                        string.Format("[{0}] exceeds Ginnie Net length of {1}. Exported value will be truncated to [{2}].",
                        value, fieldMaxLength, newValue));
                }
                newValue.PadRight(fieldMaxLength, ' ').CopyTo(0, m_data, start, fieldMaxLength); //Pad right to fill field length with whitespace
            }
        }
        public override string ToString()
        {
            return new String(m_data);
        }
    }
}
