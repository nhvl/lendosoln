﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Conversions.GinnieNet
{
    public class GinnieNetExporter : IPoolExporter
    {

        // Record file specification: 
        // \\megatron\LendersOffice\Integration\Ginnie Mae\GinnieNET_Single_Family_File_Layout-new.pdf

        #region Segment Definitions

        //- Start indices for each Field in each Logical Record
        //- Corresponds to (start - 1) for "Start" in GinnieNET_Single_Family_File_Layout-new.pdf        
        
        private static int[] P01 = new int[]{0, 3, 4, 10, 11, 13, 17, 23, 31, 39, 53, 59, 65, 71, 73};
        private static int[] P02 = new int[] { 0, 3, 11, 19, 27, 29, 38, 43, 49, 57, 58, 59, 60, 61, 62 };
        private static int[] P06 = new int[] { 0, 3, 43, 63, 72 };
        private static int[] M01 = new int[] { 0, 3, 4, 10, 11, 13, 28, 43, 44, 45, 51, 59, 69, 79 };
        private static int[] M02 = new int[] { 0, 3, 11, 19, 28, 34, 40, 42, 43, 44, 62 };
        private static int[] M03 = new int[] { 0, 3, 43, 64, 66, 75 };
        private static int[] M04 = new int[] { 0, 3, 28, 53, 62, 68, 76, 77 };
        private static int[] M05_M08 = new int[] { 0, 3, 28, 53, 62 };
        private static int[] M10 = new int[] { 0, 3, 12, 13, 16, 17, 18, 19, 20, 23, 24, 32, 40, 43, 51, 56, 63, 77, 78, 79 };
        private static int[] M11 = new int[] { 0, 3, 9, 15, 16, 24, 32, 43, 49, 57, 58, 64, 70, 78 };

        #endregion

        private MortgagePool.MortgagePool m_pool;
        private LosConvert m_losConvert;
        private HashSet<string> m_errors;

        public GinnieNetExporter(long poolid)
        {
            m_pool = new MortgagePool.MortgagePool(poolid);
            m_pool.SetFormatTarget(FormatTarget.GinnieNet);

            m_losConvert = new LosConvert(FormatTarget.GinnieNet);

            m_errors = new HashSet<string>();   // OPM 175885 - Instantiate m_errors on construction to prevent errors with CSV export
        }
        public bool TryExport(out byte[] output)
        {
            m_errors.Clear();
            output = System.Text.UTF8Encoding.UTF8.GetBytes(ExportToString());
            if (m_errors.Count == 0)
                return true;
            else
            {
                output = null;
                StringBuilder sb = new StringBuilder("GinnieNetExporter.TryExport() had the following errors:");
                sb.AppendLine().AppendLine();
                foreach(string s in m_errors)
                {
                    sb.AppendLine(s);
                }
                Tools.LogInfo(sb.ToString());
                return false;
            }
        }
        private string ExportToString()
        {
            StringBuilder sb = new StringBuilder();
            
            List<GinnieNetPhysicalRecord> segments = BuildSegments();
            
            foreach (GinnieNetPhysicalRecord seg in segments)
            {
                sb.AppendLine(seg.ToString());
            }

            return sb.ToString();
        }
        private List<GinnieNetPhysicalRecord> BuildSegments()
        {
            decimal sumsServicingUnpaidPrincipalBalance = 0;
            List<GinnieNetPhysicalRecord> segments = new List<GinnieNetPhysicalRecord>();
            GinnieNetPhysicalRecord record;

            #region P02
            record = new GinnieNetPhysicalRecord("P02", P02);

            record[2] = m_pool.InitialPmtD_rep;
            record[3] = m_pool.MaturityD_rep;
            record[4] = m_pool.UnpaidBalanceD_rep;
            record[5] = m_pool.Term.ToString();
            record[6] = m_pool.TaxID;
            record[7] = m_pool.LoanCount.ToString();
            record[8] = m_pool.SecurityRMargin_rep;
            record[9] = m_pool.SecurityChangeD_rep;
            switch(m_pool.IndexT)
            {
                case E_MortgagePoolIndexT.CMT:
                    record[11] = "C";
                    break;
                case E_MortgagePoolIndexT.LIBOR:
                    record[11] = "L";
                    break;
            }
            switch (m_pool.CertAndAgreementT)
            {
                case E_MortgagePoolCertAndAgreementT.LimitedSecurityAgreement:
                    record[13] = "1";
                    break;
                case E_MortgagePoolCertAndAgreementT.NoSecurityAgreement:
                    record[13] = "2";
                    break;
            }
            record[14] = m_pool.IsFormHUD11711ASentToDocumentCustodian ? "1" : "2";

            segments.Add(record);
            #endregion

            #region P06
            record = new GinnieNetPhysicalRecord("P06", P06);

            record[3] = m_pool.PIAccountNum.Value;
            record[4] = m_pool.PIRoutingNum.Value;

            segments.Add(record);
            #endregion

            List<Guid> loans = m_pool.FetchLoansInPool();
            foreach (Guid sLId in loans)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(GinnieNetExporter));
                dataLoan.InitLoad();
                dataLoan.SetFormatTarget(FormatTarget.GinnieNet);

                sumsServicingUnpaidPrincipalBalance += dataLoan.sServicingUnpaidPrincipalBalance;

                List<GinnieNetPhysicalRecord> MSegments = BuildMSegments(dataLoan);
                segments.AddRange(MSegments);
            }

            #region P01
            record = new GinnieNetPhysicalRecord("P01", P01);

            record[3] = m_pool.BasePoolNumber;
            record[4] = m_pool.IssueTypeCode;
            record[5] = m_pool.PoolTypeCode;
            record[6] = m_pool.IssuerID;
            record[7] = m_pool.CustodianID;
            record[8] = m_pool.IssueD_rep;
            record[9] = m_pool.SettlementD_rep;
            record[10] = m_pool.ToRep(sumsServicingUnpaidPrincipalBalance);
            record[11] = m_pool.SecurityR_rep;
            record[12] = m_pool.LowR_rep;
            record[13] = m_pool.HighR_rep;
            switch (m_pool.AmortizationMethodT)
            {
                case E_MortgagePoolAmortMethT.ConcurrentDate:
                    record[14] = "CD";
                    break;
                case E_MortgagePoolAmortMethT.InternalReserve:
                    record[14] = "IR";
                    break;
            }

            segments.Insert(0,record);
            #endregion

            return segments;
        }

        private List<GinnieNetPhysicalRecord> BuildMSegments(CPageData dataLoan)
        {
            List<GinnieNetPhysicalRecord> segments = new List<GinnieNetPhysicalRecord>();
            GinnieNetPhysicalRecord record;

            #region M01
            record = new GinnieNetPhysicalRecord("M01", M01);

            record[3] = m_pool.BasePoolNumber;
            record[4] = m_pool.IssueTypeCode;
            record[5] = m_pool.PoolTypeCode;
            record[6] = dataLoan.sLenderCaseNum;
            record[7] = this.ProcessAgencyCaseNum(dataLoan.sAgencyCaseNum, dataLoan.sFHAADPCode, dataLoan.sLNm, dataLoan.sLT);

            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    record[8] = "F";
                    break;
                case E_sLT.VA:
                    record[8] = "V";
                    break;
                case E_sLT.UsdaRural:
                    record[8] = "M";
                    break;
                default:
                    m_errors.Add("M01-8: Illegal Loan Type for GinnieNet Export - Loan # " + dataLoan.sLNm);
                    break;
            }
            record[10] = dataLoan.sNoteIR_rep;
            record[11] = dataLoan.sProThisMPmt_rep;
            record[12] = dataLoan.sFinalLAmt_rep;
            record[13] = dataLoan.sServicingUnpaidPrincipalBalance_rep;

            segments.Add(record);
            #endregion

            #region M02
            record = new GinnieNetPhysicalRecord("M02", M02);

            record[2] = dataLoan.sSchedDueD1_rep;
            if (dataLoan.sSchedDueD1.IsValid)
            {
                DateTime dt = dataLoan.sSchedDueD1.DateTimeForComputation;
                dt = dt.AddMonths(dataLoan.sDue - 1);
                record[3] = m_losConvert.ToDateTimeString(dt);
            }
            decimal amount = m_losConvert.ToMoney(dataLoan.sServicingCollectedFunds_Principal_rep);
            int intAmount = decimal.ToInt32(decimal.Round(amount));
            record[4] = m_losConvert.ToMoneyString(intAmount, FormatDirection.ToRep);
            record[6] = dataLoan.sRAdjMarginR_rep;
            record[9] = dataLoan.sMersIsOriginalMortgagee?"Y":"N";
            record[10] = dataLoan.sMersMin;

            segments.Add(record);
            #endregion

            #region M03
            record = new GinnieNetPhysicalRecord("M03", M03);

            record[2] = dataLoan.sSpAddr;
            record[3] = dataLoan.sSpCity;
            record[4] = dataLoan.sSpState;
            record[5] = dataLoan.sSpZip;

            segments.Add(record);
            #endregion

            #region M04
            record = new GinnieNetPhysicalRecord("M04", M04);
            CAppData primApp = dataLoan.GetAppData(0);
            
            record[2] = primApp.aBFirstNm + (string.IsNullOrEmpty(primApp.aBMidNm) ? string.Empty : " " + primApp.aBMidNm);
            record[3] = primApp.aBLastNm + (string.IsNullOrEmpty(primApp.aBSuffix) ? string.Empty : " " + primApp.aBSuffix);
            record[4] = primApp.aBSsn;
            if (dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                record[5] = "0.00";
            }
            else
            {
                if (dataLoan.sHouseVal != 0)
                {
                    decimal percent = (dataLoan.sFinalLAmt / dataLoan.sHouseVal * 100);
                    int result = decimal.ToInt32(percent * 100);
                    if (result % 100 > 0)
                        result += 100;
                    result /= 100;
                    record[5] = m_losConvert.ToDecimalString(result, FormatDirection.ToRep);
                }
            }
            record[6] = dataLoan.sAppSubmittedD_rep;
            record[7] = dataLoan.sHas1stTimeBuyer ? "Y" : "N";
            

            segments.Add(record);
            #endregion

            #region M05-M08
            int recordIndex = 5;
            CAppData app;

            if (primApp.aCIsValidNameSsn)
            {
                record = new GinnieNetPhysicalRecord("M0" + (recordIndex), M05_M08);
                recordIndex++;

                record[2] = primApp.aCFirstNm + (string.IsNullOrEmpty(primApp.aCMidNm) ? string.Empty : " " + primApp.aCMidNm);
                record[3] = primApp.aCLastNm + (string.IsNullOrEmpty(primApp.aCSuffix) ? string.Empty : " " + primApp.aCSuffix);
                record[4] = primApp.aCSsn;

                segments.Add(record);
            }
            for (int appIndex = 1; appIndex < dataLoan.nApps; appIndex++)
            {
                app = dataLoan.GetAppData(appIndex);

                if (app.aBIsValidNameSsn)
                {
                    if (recordIndex > 8) //8 = maximum record index for coborrower records
                    {
                        m_errors.Add("Too many coborrowers for GinnieNet Export; Only 5 or fewer total borrowers permitted (M04-M08) - Loan # " + dataLoan.sLNm);
                    }
                    record = new GinnieNetPhysicalRecord("M0" + (recordIndex), M05_M08);
                    recordIndex++;

                    record[2] = app.aBFirstNm;
                    record[3] = app.aBLastNm;
                    record[4] = app.aBSsn;

                    segments.Add(record);
                }
                if (app.aCIsValidNameSsn)
                {
                    if (recordIndex > 8) //8 = maximum record index for coborrower records
                    {
                        m_errors.Add("Too many coborrowers for GinnieNet Export; Only 5 or fewer total borrowers permitted (M04-M08) - Loan # " + dataLoan.sLNm);
                    }
                    record = new GinnieNetPhysicalRecord("M0" + (recordIndex), M05_M08);
                    recordIndex++;

                    record[2] = app.aCFirstNm;
                    record[3] = app.aCLastNm;
                    record[4] = app.aCSsn;

                    segments.Add(record);
                }
            }
            #endregion

            #region M10
            record = new GinnieNetPhysicalRecord("M10", M10);

            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    record[3] = "1";
                    break;
                case E_sLT.VA:
                    record[3] = "2";
                    break;
                case E_sLT.UsdaRural:
                    record[3] = "3";
                    break;
                default:
                    m_errors.Add("M10-3: Illegal Loan Type for GinnieNet Export - Loan # " + dataLoan.sLNm);
                    break;
            }
            record[5] = (dataLoan.sIsRefinancing ? "2" : "1");
            record[6] = dataLoan.sUnitsNum.ToString();
            if (dataLoan.sFHAGift1gAmt > 0 || decimal.Parse(dataLoan.sTotGiftFundAsset_rep) > 0)
            {
                record[8] = "1";
            }
            else
            {
                record[8] = "2";
            }
            try
            {
                record[9] = dataLoan.sCreditScoreType2Soft.ToString();
            }
            catch (CBaseException e)
            {
                m_errors.Add("M10-9: " + e.DeveloperMessage + " - Loan # " + dataLoan.sLNm);
            }
            record[10] = (dataLoan.sHasTempBuydown ? "1" : "2");
            if (E_sLT.FHA == dataLoan.sLT)
            {
                record[11] = dataLoan.sFfUfmip1003_rep;
                record[12] = dataLoan.m_convertLos.ToMoneyString(dataLoan.sProMIns * 12, FormatDirection.ToRep);
            }
            record[14] = dataLoan.sRAdj1stD_rep;
            record[15] = m_pool.IndexT.ToString();

            if(dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                string range = "";
                int x = dataLoan.sRAdjCapMon;
                if (x >= 12 && x <= 18)
                    range = "012-018";
                else if (x >= 36 && x <= 42)
                    range = "036-042";
                else if (x >= 60 && x <= 66)
                    range = "060-066";
                else if (x >= 84 && x <= 90)
                    range = "084-090";
                else if (x >= 120 && x <= 126)
                    range = "120-126";
                else
                    m_errors.Add("M10-16: Not in any valid month range for export - Loan # " + dataLoan.sLNm);
                record[16] = range;
            }

            if (dataLoan.sRAdj1stCapR % 1 != 0 || dataLoan.sRAdj1stCapR < 0)
                m_errors.Add("M10-18: Initial Interest Rate Cap not valid integer value - Loan # " + dataLoan.sLNm);
            if (dataLoan.sRAdjCapR % 1 != 0 || dataLoan.sRAdjCapR < 0)
                m_errors.Add("M10-19: Subsequent Interest Rate Cap not valid integer value - Loan # " + dataLoan.sLNm);
            if (dataLoan.sRAdjLifeCapR % 1 != 0 || dataLoan.sRAdjLifeCapR < 0)
                m_errors.Add("M10-20: Lifetime Interest Rate Cap not valid integer value - Loan # " + dataLoan.sLNm);
            record[18] = dataLoan.sRAdj1stCapR.ToString("0");
            record[19] = dataLoan.sRAdjCapR.ToString("0");
            record[20] = dataLoan.sRAdjLifeCapR.ToString("0");
            

            segments.Add(record);
            #endregion

            #region M11
            record = new GinnieNetPhysicalRecord("M11", M11);
            
            if (dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                record[2] = "0.00";
            }
            else
            {
                if (dataLoan.sHouseVal != 0)
                {
                    decimal percent = (dataLoan.sFinalLAmt / dataLoan.sHouseVal * 100);
                    int result = decimal.ToInt32(percent * 100);
                    if (result % 100 > 0)
                        result += 100;
                    result /= 100;
                    record[2] = m_losConvert.ToDecimalString(result, FormatDirection.ToRep);
                }
            }

            decimal r3;
            if (decimal.TryParse(dataLoan.sQualBottomR_rep, out r3))
            {
                record[3] = m_losConvert.ToDecimalString(r3, FormatDirection.ToRep);
            }
            else
            {
                record[3] = "000.00";
            }

            switch (dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Refin:
                    record[4] = "1";
                    break;
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    record[4] = "2";
                    break;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                    record[4] = "3";
                    break;
            }

            record[5] = dataLoan.sServicingLastPaymentMadeDueD_rep;

            switch (dataLoan.sBranchChannelT)
            {
                case E_BranchChannelT.Broker:
                case E_BranchChannelT.Wholesale:
                    record[10] = "1";
                    break;
                case E_BranchChannelT.Correspondent:
                    record[10] = "2";
                    break;
                case E_BranchChannelT.Retail:
                    record[10] = "3";
                    break;
                default:
                    m_errors.Add("M11-10: Third Party Origination Type cannot be blank for GinnieNet Export - Loan # " + dataLoan.sLNm);
                    break;
            }
            if (dataLoan.sLT == E_sLT.FHA)
            {
                decimal r11, r12;
                if(decimal.TryParse(dataLoan.sFfUfmipR_rep, out r11))
                {
                    record[11] = m_losConvert.ToRateString(r11);
                }
                if(decimal.TryParse(dataLoan.sProMInsR_rep, out r12))
                {                        
                    record[12] = m_losConvert.ToRateString(r12);
                }
            }

            record[13] = dataLoan.sDocumentNoteD_rep; // 3/5/2015 BB - 199092

            segments.Add(record);
            #endregion

            return segments;
        }

        /// <summary>
        /// Processes the agency case number before exporting. If the ADP code was not included by the user
        /// in the UI, it should be appended here.
        /// </summary>
        /// <param name="caseNum">The agency case number.</param>
        /// <param name="adpCode">The ADP code.</param>
        /// <param name="loanType">The loan type.</param>
        /// <returns>The processed agency case number.</returns>
        private string ProcessAgencyCaseNum(string caseNum, string adpCode, string loanNumber, E_sLT loanType)
        {
            caseNum = caseNum.Replace("-", "");

            if (loanType == E_sLT.FHA && caseNum.Length == 10)
            {
                caseNum += adpCode;
            }

            caseNum = caseNum.PadLeft(15, '0');

            if (caseNum.Length != 15)
            {
                m_errors.Add("M01-7: Case Number length different than expected 15 characters - Loan # " + loanNumber);
            }

            return caseNum;
        }

        /// <summary>
        /// Return segments M01-M11 in CSV format for the provided loan IDs
        /// </summary>
        /// <param name="sLIdList">List of Loan IDs</param>
        /// <param name="errors">HashSet of errors output on failure</param>
        /// <returns>NULL if errors occured.</returns>
        public static string ExportMSegmentsToCsv(IEnumerable<Guid> sLIdList, out HashSet<string> errors)
        {
            errors = new HashSet<string>();

            // Create CSV
            string headerRow = "M01_RecordType|M01_Filler1|Pool Number|Issue Type|Pool Type|Mort Number|Case Number|Mort Type|M01_Filler2|Interest Rate|"
                + "P&I|OPB|UPB|M01_Filler3|M02_RecordType|First Pay Date|Last Pay Date|Unscheduled Principal Curtailment|% of increase|Mort Margin|MH Type|"
                + "M02_Filler1|MOM|MIN|M02_Filler2|M03_RecordType|Mort Address|Mort City|Mort State|Mort Zip|M03_Filler1|M04_RecordType|Borrower First Name|"
                + "Borrower Last Name|Borrower SSN|LTV|Loan Application Date|First Time Homebuyer Indicator|M04_Filler1|M05_RecordType|BorrowerSpouse First Name|"
                + "BorrowerSpouse Last Name|BorrowerSpouse SSN|M05_Filler1|M06_RecordType|Co-Borrower First Name1|Co-Borrower Last Name1|Co-Borrower SSN1|M06_Filler1|"
                + "M07_RecordType|Co-BorrowerSpouse First Name1|Co-BorrowerSpouse Last Name1|Co-BorrowerSpouse SSN1|M07_Filler1|M08_RecordType|Co-Borrower First Name2|"
                + "Co-Borrower Last Name2|Co-Borrower SSN2|M08_Filler1|M10_RecordType|Loan Key|Loan Type Code|M10_Filler1|Loan Purpose|living_area Units|M10_Filler2|"
                + "Down payment Assistance Flag|CREDIT Score|Loan Buydown Code|Upfront MIP Amount|Annual MIP Amount|M10_Filler3|Interest Rate Change Date|Index Type|"
                + "Acceptable Range (Months)|Type of ARM Note|Initial (+/-) Interest Rate Cap|Subsequent (+/-) Interest Rate Cap|Lifetime (+/-) Interest Rate Cap|"
                + "M11_RecordType|Combined LTV Ratio Percent|Total Debt Expense Ratio Percent|Refinance Type|Last Paid Installment Due Date|"
                + "Pre-Modification First Installment Due Date|Pre-Modification Original Principal Balance (OPB) Amount|Pre-Modification Interest Rate Percent|"
                + "PreModification Loan Maturity Date|Third Party Origination Type|Upfront MIP Rate|Annual MIP Rate|Loan Origination Date|M11_Filler1";


            StringBuilder sbCsv = new StringBuilder();
            sbCsv.AppendLine(headerRow);    // CSV header row

            // Loop Through Loans
            foreach (Guid sLId in sLIdList)
            {
                // Create Loan
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(GinnieNetExporter));
                dataLoan.InitLoad();
                dataLoan.SetFormatTarget(FormatTarget.GinnieNet);

                // If loan not associated with Mortgage Pool, log error and skip
                if (!dataLoan.sMortgagePoolId.HasValue)
                {
                    errors.Add("Loan is not assigned to a Mortgage Pool - Loan # " + dataLoan.sLNm);
                    continue;   // Add nothing to CSV
                }

                // Mortgage Pool found! Build M01-M11 segments and convert to CSV
                GinnieNetExporter exporter = new GinnieNetExporter(dataLoan.sMortgagePoolId.Value);
                List<GinnieNetPhysicalRecord> lSegments = exporter.BuildMSegments(dataLoan);
                errors.UnionWith(exporter.m_errors);    // Add build errors to error set

                // Put segments in Dictionary for easier lookup
                Dictionary<string, GinnieNetPhysicalRecord> dictSegments = new Dictionary<string, GinnieNetPhysicalRecord>();
                foreach (GinnieNetPhysicalRecord segment in lSegments)
                {
                    //Tools.LogInfo(string.Format("Adding Segment: {0}\n Type: {1}", segment.ToString(), segment[1]));
                    dictSegments.Add(segment[1], segment);
                }

                // Convert Segments to CSV
                StringBuilder sb = new StringBuilder();
                AppendMSegmentAsCSV(dictSegments["M01"], M01.Length, ref sb);
                AppendMSegmentAsCSV(dictSegments["M02"], M02.Length, ref sb);
                AppendMSegmentAsCSV(dictSegments["M03"], M03.Length, ref sb);
                AppendMSegmentAsCSV(dictSegments["M04"], M04.Length, ref sb);

                for (int i = 5; i <= 8; i++)
                {
                    string key = string.Format("M{0:D2}", i);
                    if (!dictSegments.ContainsKey(key))
                    {
                        sb.Append("|||||");
                    }
                    else
                    {
                        AppendMSegmentAsCSV(dictSegments[key], M05_M08.Length, ref sb);
                    }
                }

                AppendMSegmentAsCSV(dictSegments["M10"], M10.Length, ref sb);
                AppendMSegmentAsCSV(dictSegments["M11"], M11.Length, ref sb);

                // Remove , at end
                sb.Remove(sb.Length - 1, 1);

                // Add line to CSV
                sbCsv.AppendLine(sb.ToString()); 
            }

            if (errors.Count == 0)
                return sbCsv.ToString();
            else
            {
                // Allow caller to handle errors
                //// Log errors
                //StringBuilder sb = new StringBuilder("GinnieNetExporter.ExportMSegmentsToCsv() had the following errors:");
                //sb.AppendLine().AppendLine();
                //foreach (string s in errors)
                //{
                //    sb.AppendLine(s);
                //}
                //Tools.LogInfo(sb.ToString());

                // Return null if errors found
                return null;
            }
        }

        private static void AppendMSegmentAsCSV(GinnieNetPhysicalRecord segment, int nSubSegments, ref StringBuilder sb)
        {
            for (int i = 1; i <= nSubSegments; i++)
            {
                string s = segment[i].TrimWhitespaceAndBOM();
                if (string.IsNullOrEmpty(s))
                {
                    sb.Append("|");
                }
                else
                {
                    // Escape and append value
                    sb.Append("\"" + s.Replace("\"", "\"\"") + "\"|");
                } 
            }
        }

        public string OutputFileName
        {
            get 
            {
                DateTime date = DateTime.Now;
                return date.ToString("yyyy-MM-dd") + "_" + m_pool.PoolNumberByAgency;
            }
        }
        public HashSet<string> Errors
        {
            get 
            {
                return m_errors;
            }
        }

        public HashSet<string> Warnings
        {
            get
            {
                return new HashSet<string>();
            }
        }
    }
}
