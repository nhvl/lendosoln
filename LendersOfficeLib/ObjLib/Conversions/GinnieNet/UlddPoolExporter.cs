﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions;
using LendersOffice.ObjLib.MortgagePool;
using DataAccess;
using ULDD;
using LendersOffice.ObjLib.Conversions;

namespace LendersOffice.Conversions
{
    public class UlddPoolExporter : UlddExporter, IPoolExporter
    {

        public static IEnumerable<Guid> GetLoansInPool(long poolId)
        {
            return new MortgagePool(poolId).FetchLoansInPool();
        }

        MortgagePool pool;
        public UlddPoolExporter(long poolId) : base(GetLoansInPool(poolId), null)
        {
            pool = new MortgagePool(poolId);
            pool.SetFormatTarget(DataAccess.FormatTarget.MismoClosing);
        }

        protected override DealSet CreateDealSet()
        {
            var baseDeals = base.CreateDealSet();

            baseDeals.Pool = CreatePool();
            baseDeals.Parties = CreateParties();
            baseDeals.InvestorFeatures = CreateFeatures();

            return baseDeals;
        }

        protected override AboutVersion CreateAboutVersion()
        {
            var about = base.CreateAboutVersion();
            var firstLoan = CPageData.CreateUsingSmartDependencyForLoad(GetLoansInPool(pool.PoolId).FirstOrDefault(), typeof(UlddPoolExporter));
            UlddPhase3Part part = firstLoan.BrokerDB.GetUlddPhase3Settings.GetPart(firstLoan.sGseDeliveryTargetT, firstLoan.sLoanFileT == E_sLoanFileT.Test);
            about.AboutVersionIdentifier = part == UlddPhase3Part.UsePhase2Instead ? AboutVersion_Fnma_Phase2 : AboutVersion_Fnma_Phase3;
            return about;
        }

        private InvestorFeatures CreateFeatures()
        {
            var features = new InvestorFeatures();
            var codes = from f in pool.FeatureCodes select new InvestorFeature(){
                 InvestorFeatureIdentifier = f
            };
            features.InvestorFeatureList.AddRange(codes);

            return features;
        }

        private Parties CreateParties()
        {
            Parties p = new Parties();
            p.PartyList.Add(new Party()
            {
                Roles = new ULDD.Roles()
                {
                    PartyRoleIdentifiers = new PartyRoleIdentifiers()
                }
            });

            p.PartyList[0].Roles.PartyRoleIdentifiers.PartyRoleIdentifierList.AddRange(
                          new[]{
                              new PartyRoleIdentifier(){
                                  _PartyRoleIdentifier = pool.SellerID
                              },
                              new PartyRoleIdentifier(){
                                  _PartyRoleIdentifier = pool.ServicerID
                              },
                              new PartyRoleIdentifier(){
                                  _PartyRoleIdentifier = pool.DocumentCustodianID
                              }
                          });

            return p;
        }

        private ULDD.Pool CreatePool()
        {
            var pool = new ULDD.Pool();
            pool.PoolDetail = CreatePoolDetails();

            return pool;
        }

        private ULDD.PoolDetail CreatePoolDetails()
        {
            var detail = new ULDD.PoolDetail();

            detail.PoolIssueDate = pool.IssueD_rep;
            detail.SecurityTradeBookEntryDate = pool.BookEntryD_rep;
            detail.PoolIdentifier = pool.BasePoolNumber;
            detail.PoolSuffixIdentifier = pool.PoolSuffix;
            detail.PoolMortgageType = ToMismo(pool.MortgageT);
            detail.PoolSecurityIssueDateInterestRatePercent = pool.SecurityR_rep;
            detail.PoolScheduledRemittancePaymentDay = pool.RemittanceD_rep;
            detail.PoolStructureType = ToMismo(pool.StructureT);
            detail.PoolOwnershipPercent = pool.OwnershipPc_rep;
            detail.PoolInterestOnlyIndicator = pool.IsInterestOnly;
            detail.PoolBalloonIndicator = pool.IsBalloon;
            detail.PoolAssumabilityIndicator = pool.IsAssumable;
            detail.PoolAmortizationType = ToMismoPool(pool.AmortizationT);
            detail.PoolInvestorProductPlanIdentifier = pool.ARMPlanNum;
            detail.PoolInterestRateRoundingType = ToMismo(pool.RoundingT);
            detail.PoolInterestRateRoundingPercent = pool.RoundingPc_rep;
            detail.PoolInterestAndPaymentAdjustmentIndexLeadDaysCount = pool.InterestRateChangeLookbackDays_rep;
            detail.PoolMarginRatePercent = pool.MBSMarginPc_rep;
            detail.PoolAccrualRateStructureType = ToMismo(pool.AccrualRateStructureT);
            detail.PoolFixedServicingFeePercent = pool.FixedServicingFeePc_rep;
            detail.PoolMinimumAccrualRatePercent = pool.MinAccrualR_rep;
            detail.PoolMaximumAccrualRatePercent = pool.MaxAccrualR_rep;	


            return detail;
        }

        private AmortizationType ToMismoPool(E_sFinMethT method)
        {
            switch(method)
            {
                case E_sFinMethT.ARM: return AmortizationType.AdjustableRate;
                case E_sFinMethT.Fixed: return AmortizationType.Fixed;
                case E_sFinMethT.Graduated: return AmortizationType.GraduatedPaymentMortgage;
                default: return AmortizationType.Undefined;
            }
        }

        private PoolAccrualRateStructureType ToMismo(E_PoolAccrualRateStructT structure)
        {
            switch(structure)
            {
                case E_PoolAccrualRateStructT.Stated: return PoolAccrualRateStructureType.StatedStructure;
                case E_PoolAccrualRateStructT.WeightedAverage: return PoolAccrualRateStructureType.WeightedAverageStructure;
                default: return PoolAccrualRateStructureType.Undefined;
            }
        }

        private PoolInterestRateRoundingType ToMismo(E_PoolRoundingT rounding)
        {
            switch(rounding)
            {
                case E_PoolRoundingT.Down01:
                case E_PoolRoundingT.Down125:
                case E_PoolRoundingT.Down25:
                    return PoolInterestRateRoundingType.Down;
                case E_PoolRoundingT.Nearest125:
                case E_PoolRoundingT.Nearest25:
                    return PoolInterestRateRoundingType.Nearest;
                case E_PoolRoundingT.Up125:
                case E_PoolRoundingT.Up25:
                    return PoolInterestRateRoundingType.Up;
                case E_PoolRoundingT.NoRounding:
                    return PoolInterestRateRoundingType.NoRounding;
                case E_PoolRoundingT.LeaveBlank:
                default:
                    return PoolInterestRateRoundingType.Undefined;
            }
        }

        private PoolStructureType ToMismo(E_PoolStructureT structure)
        {
            switch(structure)
            {
                case E_PoolStructureT.InvestorDefinedMultipleLender: return PoolStructureType.InvestorDefinedMultipleLender;
                case E_PoolStructureT.LenderInitiatedMultipleLender: return PoolStructureType.LenderInitiatedMultipleLender;
                case E_PoolStructureT.SingleLender: return PoolStructureType.SingleLender;
                default: return PoolStructureType.Undefined;
            }
        }

        #region IPoolExporter Members

        public HashSet<string> Errors
        {
            get
            {
                List<UlddExportError> allErrors;
                this.Verify(out allErrors);

                HashSet<string> ret = new HashSet<string>();

                foreach (var errors in allErrors)
                {
                    foreach (var error in errors.ErrorList)
                    {
                        ret.Add(errors.sLNm + ": " + error.ErrorMessage);
                    }
                }

                return ret;
            }
        }

        public string OutputFileName
        {
            get
            {
                DateTime date = DateTime.Now;
                return date.ToString("yyyy-MM-dd") + "_" + pool.PoolNumberByAgency;
            }
        }

        public HashSet<string> Warnings
        {
            get
            {
                return new HashSet<string>();
            }
        }

        public bool TryExport(out byte[] output)
        {
            output = null;

            List<UlddExportError> allErrors;
            if (!this.Verify(out allErrors)) return false;

            output = Encoding.UTF8.GetBytes(this.Export());
            return true;
        }

        #endregion
    }
}
