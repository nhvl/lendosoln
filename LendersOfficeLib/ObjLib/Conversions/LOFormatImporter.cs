namespace LendersOffice.Conversions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using CommonLib;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.CreditReport.Mcl;
    using LendersOffice.DU;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.ObjLib.LoXml;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using ObjLib.Appraisal;

    public class LOFormatImporter
    {
        private static readonly string BORROWERCLOSINGCOSTSET = "sClosingCostSet";
        private static readonly string SELLERCLOSINGCOSTSET = "sSellerResponsibleClosingCostSet";

        /// <summary>
        /// Imports the data into the specified loan.
        /// </summary>
        /// <param name="sLId">The loan to import into.</param>
        /// <param name="principal">The user doing the importing.</param>
        /// <param name="data">The LOXml</param>
        /// <param name="fieldsToOutput">The fields to output after save.</param>
        /// <returns>True if successful import. False if there was a hardstop in the import preventing a save.</returns>
        public static LoFormatImporterResult Import(Guid sLId, AbstractUserPrincipal principal, string data, IEnumerable<string> fieldsToOutput = null)
        {
            return Import(sLId, principal, data, false, fieldsToOutput);
        }

        /// <summary>
        /// Imports the data into the specified loan.
        /// </summary>
        /// <param name="sLId">The loan to import into.</param>
        /// <param name="principal">The user doing the importing.</param>
        /// <param name="data">The LOXml</param>
        /// <param name="bypassPermissions">Whether import should bypass permission checks.</param>
        /// <param name="fieldsToOutput">The fields to output after save.</param>
        /// <returns>True if successful import. False if there was a hardstop in the import preventing a save.</returns>
        public static LoFormatImporterResult Import(Guid sLId, AbstractUserPrincipal principal, string data, bool bypassPermissions, IEnumerable<string> fieldsToOutput = null)
        {
            E_CalcModeT oldCalcMode;
            StringBuilder warningMessages = new StringBuilder();

            LoXmlInputParser parser;
            bool isQp2LoanFile = Tools.IsQP2LoanFile(sLId, principal.BrokerId);

            if (isQp2LoanFile)
            {
                parser = new QuickPricerLoXmlInputParser(data, GetOriginatorCompensationSettings(principal, sLId));
            }
            else
            {
                parser = new LoXmlInputParser(data);
            }

            XmlDocument doc = parser.GetValidatedDocument();

            if (parser.GetErrors().Any())
            {
                throw new InvalidLoXmlException(parser.GetErrors());
            }

            foreach (var warning in parser.GetWarnings())
            {
                warningMessages.AppendLine(warning);
            }

            // Gather all field id in loan and applicant element.
            List<string> loanFieldList = new List<string>();
            List<string> applicantFieldList = new List<string>();

            foreach (XmlElement el in doc.SelectNodes("//loan/field"))
            {
                loanFieldList.Add(el.GetAttribute("id").Trim());
            }
            foreach (XmlElement el in doc.SelectNodes("//loan/applicant/field"))
            {
                applicantFieldList.Add(el.GetAttribute("id").Trim());
            }

            List<string> dependencyFieldList = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LOFormatImporter)).ToList();

            dependencyFieldList.AddRange(loanFieldList);
            dependencyFieldList.AddRange(applicantFieldList);

            CSelectStatementProvider stmtProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, dependencyFieldList);

            CPageData dataLoan;

            if (bypassPermissions)
            {
                // Creating a sandbox file is a system operation and thus all permission checks should be bypassed.
                dataLoan = new CFullAccessPageData(sLId, "LOFormatExporterBypass", stmtProvider);
                dataLoan.ByPassFieldSecurityCheck = true;
            }
            else
            {
                dataLoan = new CPageData(sLId, "LOFormatExporter", stmtProvider);
            }

            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            LoFormatImporterResult result = new LoFormatImporterResult();
            result.ResultStatus = LoFormatImporterResultStatus.Ok;
            List<Action<CPageData>> postBindLoanPreSaveActions = new List<Action<CPageData>>();

            #region //loan/field
            XmlNodeList list = doc.SelectNodes("//loan/field");
            foreach (XmlElement el in list)
            {
                string fieldId = el.GetAttribute("id").TrimWhitespaceAndBOM();
                string value = el.InnerText;


                switch (fieldId.ToLower())
                {
                    case "sprodisfhamipfinanced":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sProdIsFhaMipFinanced = bool.Parse(value);
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "ssubfint":
                        // OPM 477222. To get correct flow into sIsOFinCreditLineInDrawPeriod,
                        // this is another pe-type field.
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sSubFinT = (E_sSubFinT)Enum.Parse(typeof(E_sSubFinT), value);
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "slamtcalc": dataLoan.sLAmtLckd = true; dataLoan.sLAmtCalc_rep = value; break;
                    case "sproohexp": dataLoan.sProOHExp_rep = value; dataLoan.sProOHExpLckd = true; break;
                    case "sotherlfinmetht":
                        try
                        {
                            dataLoan.sOtherLFinMethT = (E_sFinMethT)Enum.Parse(typeof(E_sFinMethT), value);
                        }
                        catch (ArgumentException)
                        {
                            InvalidEnumValue(fieldId, value, warningMessages);
                        }
                        catch (OverflowException)
                        {
                            InvalidEnumValue(fieldId, value, warningMessages);
                        }
                        break;
                    case "srlckdexpiredd": dataLoan.Set_sRLckdExpiredD_Manually(value); break;
                    case "sestclosed":
                        dataLoan.sEstCloseD_rep = value;
                        dataLoan.sEstCloseDLckd = true;
                        break;
                    case "sinvestorlocklpinvestornm":
                        warningMessages.AppendLine("sInvestorLockLpInvestorNm is no longer writable. Use sInvestorRolodexId id");
                        break;
                    case "sinvestorlockrlckexpiredd":
                        dataLoan.sInvestorLockRLckExpiredDLckd = true;
                        dataLoan.sInvestorLockRLckExpiredD_rep = value;
                        break;
                    case "sinvestorlockdeliveryexpiredd":
                        dataLoan.sInvestorLockDeliveryExpiredDLckd = true;
                        dataLoan.sInvestorLockDeliveryExpiredD_rep = value;
                        break;
                    case "authorizecreditcheckforallborrowers":
                        // This relies on the borrowers bound during the application processing
                        postBindLoanPreSaveActions.Add(loan => ObjLib.CreditReportAuthorization.ConsumerPortalCreditReportAuthorization.AuthorizeCreditCheckForAllBorrowers(loan, value, principal));
                        break;
                    case "sspzip":
                        dataLoan.sSpZip = value;
                        postBindLoanPreSaveActions.Add(UpdateSubjectPropertyCountyOnModifiedZip);
                        break;
                    case "sprodspt":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        switch (value)
                        {
                            case "0": dataLoan.sProdSpT = E_sProdSpT.SFR; break;
                            case "1": dataLoan.sProdSpT = E_sProdSpT.PUD; break;
                            case "2": dataLoan.sProdSpT = E_sProdSpT.Condo; break;
                            case "3": dataLoan.sProdSpT = E_sProdSpT.CoOp; break;
                            case "4": dataLoan.sProdSpT = E_sProdSpT.Manufactured; break;
                            case "5": dataLoan.sProdSpT = E_sProdSpT.Townhouse; break;
                            case "6": dataLoan.sProdSpT = E_sProdSpT.Commercial; break;
                            case "7": dataLoan.sProdSpT = E_sProdSpT.MixedUse; break;
                            case "8": dataLoan.sProdSpT = E_sProdSpT.TwoUnits; break;
                            case "9": dataLoan.sProdSpT = E_sProdSpT.ThreeUnits; break;
                            case "10": dataLoan.sProdSpT = E_sProdSpT.FourUnits; break;
                            case "11": dataLoan.sProdSpT = E_sProdSpT.Modular; break;
                            case "12": dataLoan.sProdSpT = E_sProdSpT.Rowhouse; break;
                            default:
                                InvalidEnumValue("sProdSpT", value, warningMessages);
                                break;
                        };
                        dataLoan.CalcModeT = oldCalcMode;


                        break;
                    case "sprodcashoutamt":
                        dataLoan.sProdCashoutAmt_rep = value;
                        if (dataLoan.sProdCashoutAmt < 0)
                        {
                            warningMessages.AppendFormat("Cashout amount cannot be negative.{0}", Environment.NewLine);
                            dataLoan.sProdCashoutAmt = 0; // 4/12/2007 dd - Reset amount to 0.
                        }
                        break;
                    case "sstatust":
                        try
                        {
                            dataLoan.sStatusT = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), value);
                            //2-13-09 OPM 22741 jk - When the status of a loan changes, sStatusLckd should be set to true
                            dataLoan.sStatusLckd = true;

                        }
                        catch (ArgumentException)
                        {
                            InvalidEnumValue(fieldId, value, warningMessages);
                        }
                        catch (OverflowException)
                        {
                            InvalidEnumValue(fieldId, value, warningMessages);
                        }

                        break;
                    case "soccr":
                        dataLoan.sOccRLckd = true;
                        dataLoan.sOccR_rep = value;
                        break;
                    case "sspgrossrent":
                        dataLoan.sSpGrossRentLckd = true;
                        dataLoan.sSpGrossRent_rep = value;
                        break;
                    case "sprorealetxpe":
                        // 3/27/2007 dd - Need to set to PriceMyLoan calc mode when set Pe field.
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sProRealETxPe_rep = value;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;

                    case "sproohexppe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sProOHExpPe_rep = value;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "sisionlype":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sIsIOnlyPe = value.ToLower() == "true";
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "sprimapptotnonspipe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sPrimAppTotNonspIPe_rep = value;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "shousevalpe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sHouseValPe_rep = value;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "sltvrotherfinpe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sLtvROtherFinPe_rep = value;
                        dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.LtvTargetOtherFin;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "sproofinbalpe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sProOFinBalPe_rep = value;
                        dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.LAmtOtherFin;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "sspstatepe":
                    case "slpurposetpe":
                    case "shasnonoccupantcoborrowerpe":
                    case "sprominspe":
                    case "sspgrossrentpe":
                    case "soccrpe":
                    case "sproofinpmtpe":
                    case "screditscoreestimatepe":
                    case "spresohexppe":
                    case "sownertitleinsfpe":
                        SetPmlField(dataLoan, fieldId, value);
                        break;
                    case "sapprvalpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.HouseVal);
                        break;
                    case "sdownpmtpcpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.DwnPmtPc);
                        break;
                    case "sequitype":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.Equity);
                        break;
                    case "sltvrpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.LtvTarget);
                        break;
                    case "slamtcalcpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.LAmt);
                        break;
                    case "scltvrpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.CltvTarget);
                        break;
                    case "ssubfinpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.LtvTargetOtherFin);
                        break;
                    case "shcltvrpe":
                        SetPmlField(dataLoan, fieldId, value, E_sProdCalcEntryT.HcltvTarget);
                        break;
                    case "semployeeprocessorexternallogin":
                        AssignRole(warningMessages, el, E_RoleT.Pml_BrokerProcessor, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeecreditauditorlogin":
                        AssignRole(warningMessages, el, E_RoleT.CreditAuditor, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeesecondarylogin":
                        AssignRole(warningMessages, el, E_RoleT.Secondary, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeloanofficerassistantlogin":
                        AssignRole(warningMessages, el, E_RoleT.LoanOfficerAssistant, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeejuniorprocessorlogin":
                        AssignRole(warningMessages, el, E_RoleT.JuniorProcessor, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeejuniorunderwriterlogin":
                        AssignRole(warningMessages, el, E_RoleT.JuniorUnderwriter, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeecollateralagentlogin":
                        AssignRole(warningMessages, el, E_RoleT.CollateralAgent, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeedisclosuredesklogin":
                        AssignRole(warningMessages, el, E_RoleT.DisclosureDesk, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeelegalauditorlogin":
                        AssignRole(warningMessages, el, E_RoleT.LegalAuditor, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeepostcloserlogin":
                        AssignRole(warningMessages, el, E_RoleT.PostCloser, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeepostcloserexternallogin":
                        AssignRole(warningMessages, el, E_RoleT.Pml_PostCloser, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeqccompliancelogin":
                        AssignRole(warningMessages, el, E_RoleT.QCCompliance, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeepurchaserlogin":
                        AssignRole(warningMessages, el, E_RoleT.Purchaser, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeservicinglogin":
                        AssignRole(warningMessages, el, E_RoleT.Servicing, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeecloserlogin":
                        AssignRole(warningMessages, el, E_RoleT.Closer, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeesecondaryexternallogin":
                        if (value.TrimWhitespaceAndBOM() == "")
                        {
                            dataLoan.sEmployeeExternalSecondaryId = Guid.Empty;
                            dataLoan.RecalculateTpoValue();
                        }
                        else
                        {
                            bool sendNotification = true;

                            if (el.GetAttribute("sendNotification") == "false")
                            {
                                sendNotification = false;
                            }

                            string userType = el.GetAttribute("usertype");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("userType");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("UserType");

                            if (userType == string.Empty || userType.Equals("B") || userType.Equals("b"))
                            {
                                warningMessages.AppendFormat("Non-PML users not applicable for external secondary role.{0}", Environment.NewLine);
                                break;
                            }

                            Guid newEmployeeExtSecondaryRepId = GetEmployeeIdByLogin(value, userType, principal.BrokerId, E_RoleT.Pml_Secondary, warningMessages);
                            if (newEmployeeExtSecondaryRepId != Guid.Empty)
                            {
                                Guid oldEmployeeExtSecondaryRepId = dataLoan.sEmployeeExternalSecondaryId;

                                var loanAssignmentContactTable = new LoanAssignmentContactTable(dataLoan.sBrokerId, dataLoan.sLId);
                                var loanOfficer = loanAssignmentContactTable.FindByRoleT(E_RoleT.LoanOfficer);
                                if (loanOfficer != null && !loanOfficer.IsPmlUser)
                                {
                                    warningMessages.AppendFormat("Unable to assign external secondary when internal loan officer is assigned.{0}", Environment.NewLine);
                                    break;
                                }

                                EmployeeDB emp = new EmployeeDB(newEmployeeExtSecondaryRepId, dataLoan.sBrokerId);
                                emp.Retrieve();

                                // We want to reassign the branch when there is no LO assigned.
                                if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
                                {
                                    Guid branchToAssign = Guid.Empty;

                                    if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                                    {
                                        branchToAssign = emp.MiniCorrespondentBranchID;
                                    }
                                    else
                                    {
                                        branchToAssign = emp.CorrespondentBranchID;
                                    }

                                    if (branchToAssign != Guid.Empty && branchToAssign != dataLoan.sBranchId)
                                    {
                                        dataLoan.AssignBranch(branchToAssign);
                                    }
                                }

                                dataLoan.sEmployeeExternalSecondaryId = newEmployeeExtSecondaryRepId;

                                CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.CreateNew);
                                agent.ClearExistingData();

                                bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(newEmployeeExtSecondaryRepId, dataLoan.sLId) == E_AgentRoleT.Other;
                                CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, newEmployeeExtSecondaryRepId, bCopyCommissionToAgent);
                                agent.Update();

                                MarkForAssignmentNotification(sLId, principal, oldEmployeeExtSecondaryRepId, newEmployeeExtSecondaryRepId, CEmployeeFields.s_ExternalSecondaryId, sendNotification, isQp2LoanFile: isQp2LoanFile);

                                dataLoan.RecalculateTpoValue();
                            }
                        }
                        break;
                    //OPM 22735 1-21-09 jk - Inserting additional cases into the switch to allow loan reassignments
                    // to other users. They will allow reassigning loan officer, manager, real estate agent, underwriter,
                    //lock desk, processor, call center agent, loan opener, and lender account executive.
                    //
                    //Each user must have permission to edit a loan to reassign it. This check is handled by the system.
                    // Conditions checked here: The user being assigned to a loan must be in the same brokerId as the 
                    // user doing the assigning. The user being assigned to must "have" the role that they are being 
                    // assigned to.
                    case "semployeeloanreplogin":

                        if (value.TrimWhitespaceAndBOM() == "")
                        {
                            dataLoan.sEmployeeLoanRepId = Guid.Empty;
                            dataLoan.RecalculateTpoValue();
                        }
                        else
                        {
                            bool sendNotification = true;

                            if (el.GetAttribute("sendNotification") == "false")
                            {
                                sendNotification = false;
                            }

                            //Setting to check for pml user if the user value contains usertype='P', otherwise, if the value contains
                            // usertype='L' or if neither of these is included it will check for LO user
                            string userType = el.GetAttribute("usertype");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("userType");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("UserType");

                            Guid newEmployeeLoanRepId = GetEmployeeIdByLogin(value, userType, principal.BrokerId, E_RoleT.LoanOfficer, warningMessages);
                            if (newEmployeeLoanRepId != Guid.Empty)
                            {
                                Guid oldEmployeeLoanRepId = dataLoan.sEmployeeLoanRepId;
                                dataLoan.sEmployeeLoanRepId = newEmployeeLoanRepId;

                                if (dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                                {
                                    EmployeeDB emp = new EmployeeDB(newEmployeeLoanRepId, dataLoan.sBrokerId);
                                    emp.Retrieve();
                                    if (dataLoan.sBranchId != emp.BranchID)
                                    {
                                        dataLoan.AssignBranch(emp.BranchID);
                                    }
                                }

                                // 2/7/2011 dd - eOPM 249861. When assign LO through webservice we auto-populate official contact.
                                CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                                agent.ClearExistingData();

                                bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(newEmployeeLoanRepId, dataLoan.sLId) == E_AgentRoleT.Other;
                                CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, newEmployeeLoanRepId, bCopyCommissionToAgent);
                                agent.Update();

                                MarkForAssignmentNotification(sLId, principal, oldEmployeeLoanRepId, newEmployeeLoanRepId, CEmployeeFields.s_LoanRepRoleId, sendNotification, isQp2LoanFile: isQp2LoanFile);

                                dataLoan.RecalculateTpoValue();
                            }
                        }

                        break;

                    case "semployeemanagerlogin":
                        AssignRole(warningMessages, el, E_RoleT.Manager, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeerealestateagentlogin":
                        AssignRole(warningMessages, el, E_RoleT.RealEstateAgent, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeunderwriterlogin":
                        AssignRole(warningMessages, el, E_RoleT.Underwriter, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;

                    //OPM 22735 1-21-09 jk
                    case "semployeelockdesklogin":
                        if (value.TrimWhitespaceAndBOM() == "")
                        {
                            dataLoan.sEmployeeLockDeskId = Guid.Empty;
                        }
                        else
                        {
                            bool sendNotification = true;

                            if (el.GetAttribute("sendNotification") == "false")
                            {
                                sendNotification = false;
                            }
                            string userType = el.GetAttribute("usertype");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("userType");
                            if (userType.Equals(""))
                                userType = el.GetAttribute("UserType");


                            if (userType.Equals("P") || userType.Equals("p"))
                            {
                                userType = "B";
                                warningMessages.AppendFormat("PML users not applicable for lock desk role, defaulting to LO user{0}", Environment.NewLine);
                            }

                            Guid reassignemployeeID2 = GetEmployeeIdByLogin(value, userType, principal.BrokerId, E_RoleT.LockDesk, warningMessages);
                            if (reassignemployeeID2 != Guid.Empty)
                            {
                                Guid sEmployeeLockDeskId = dataLoan.sEmployeeLockDeskId;
                                dataLoan.sEmployeeLockDeskId = reassignemployeeID2;
                                MarkForAssignmentNotification(sLId, principal, sEmployeeLockDeskId, reassignemployeeID2, CEmployeeFields.s_LockDeskRoleId, sendNotification, isQp2LoanFile: isQp2LoanFile);
                            }

                        }

                        break;

                    case "semployeeprocessorlogin":
                        AssignRole(warningMessages, el, E_RoleT.Processor, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeecallcenteragentlogin":
                        AssignRole(warningMessages, el, E_RoleT.CallCenterAgent, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeloanopenerlogin":
                        AssignRole(warningMessages, el, E_RoleT.LoanOpener, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "socctpe":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        switch (value)
                        {
                            case "0": dataLoan.sOccTPe = E_sOccT.PrimaryResidence; break;
                            case "1": dataLoan.sOccTPe = E_sOccT.SecondaryResidence; break;
                            case "2": dataLoan.sOccTPe = E_sOccT.Investment; break;
                            default:
                                InvalidEnumValue("E_sOccT", value, warningMessages);
                                break;
                        }
                        dataLoan.CalcModeT = oldCalcMode;
                        break;
                    case "semployeelenderaccexeclogin":
                        AssignRole(warningMessages, el, E_RoleT.LenderAccountExecutive, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeshipperlogin":
                        AssignRole(warningMessages, el, E_RoleT.Shipper, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeefunderlogin":
                        AssignRole(warningMessages, el, E_RoleT.Funder, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeedocdrawerlogin":
                        AssignRole(warningMessages, el, E_RoleT.DocDrawer, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "semployeeinsuringlogin":
                        AssignRole(warningMessages, el, E_RoleT.Insuring, principal, dataLoan, isQp2LoanFile: isQp2LoanFile);
                        break;
                    case "sprodfilterterm10yrs": // Warning Field Mismatch
                        dataLoan.sProdFilterDue10Yrs = bool.Parse(value);
                        break;
                    case "sprodfilterterm15yrs": // Warning Field Mismatch
                        dataLoan.sProdFilterDue15Yrs = bool.Parse(value);
                        break;
                    case "sprodfilterterm20yrs": // Warning Field Mismatch
                        dataLoan.sProdFilterDue20Yrs = bool.Parse(value);
                        break;
                    case "sprodfilterterm25yrs": // Warning Field Mismatch
                        dataLoan.sProdFilterDue25Yrs = bool.Parse(value);
                        break;
                    case "sprodfilterterm30yrs": // Warning Field Mismatch
                        dataLoan.sProdFilterDue30Yrs = bool.Parse(value);
                        break;
                    case "sprodfilterpmttpi":
                        dataLoan.sProdFilterPmtTPI = bool.Parse(value);
                        break;
                    case "sprodfilterpmttionly":
                        dataLoan.sProdFilterPmtTIOnly = bool.Parse(value);
                        break;
                    case "sprodfiltertermother": // Warning Field Mismatch
                        dataLoan.sProdFilterDueOther = bool.Parse(value);
                        break;
                    case "soriginatorcompensationpaymentsourcet":
                        E_sOriginatorCompensationPaymentSourceT sourceType;
                        if (Enum.TryParse<E_sOriginatorCompensationPaymentSourceT>(value, out sourceType) &&
                            Enum.IsDefined(typeof(E_sOriginatorCompensationPaymentSourceT), sourceType))
                        {
                            dataLoan.sOriginatorCompensationPaymentSourceT = sourceType;
                            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                            {
                                                                                                           // 3/8/2012 dd - Per OPM 75381 - Remove hardcode for PML0180
                                if (principal.BrokerId == new Guid("0a15ba94-d49e-40ef-9175-d6e0d3139f0d")) // Universal Lending (PML0174)
                                {
                                    // 6/27/2011 dd - Based on Becca @ Universal Lending (PML0174) when user choose Lender Paid from
                                    // Encompass TPO then set it to "compensation is in addition of lender price".
                                    // 12/7/2011 dd - Added Nations Direct Mortgage (PML0180)
                                    dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                                }
                            }
                        }
                        else
                        {
                            InvalidEnumValue(fieldId, value, warningMessages);
                        }
                        break;
                    case "sffufmip1003":
                        dataLoan.sFfUfmip1003_rep = value;
                        dataLoan.sFfUfmip1003Lckd = true;
                        break;
                    case "sbranchid":
                        SetBranch(dataLoan, principal, warningMessages, value);
                        break;
                    case "sufcashpd":
                        dataLoan.sUfCashPdLckd = true;
                        dataLoan.sUfCashPd_rep = value;
                        break;

                    case "sprodfhaufmip":
                        oldCalcMode = dataLoan.CalcModeT;
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.sProdFhaUfmip_rep = value;
                        dataLoan.CalcModeT = oldCalcMode;
                        break;

                    case "breakratelock":
                        // 6/11/2014 dd - OPM 184058 - Support Break Broker Rate Lock through LO Format Importer.
                        int dependencyPlaceholder = dataLoan.sfBreakRateLock; ////SmartDependency does not capture methods assigned to delegates.
                        bool isRateLocked = dataLoan.sRateLockStatusT == E_sRateLockStatusT.Locked;
                        Action<string, string> lockAction = dataLoan.BreakRateLock;
                        TryBreakOrSuspendLock(principal, fieldId, el, isRateLocked, lockAction, warningMessages);
                        break;
                    case "suspendratelock":
                        dependencyPlaceholder = dataLoan.sfBreakRateLock;
                        isRateLocked = dataLoan.sRateLockStatusT == E_sRateLockStatusT.Locked;
                        lockAction = dataLoan.SuspendLock;
                        TryBreakOrSuspendLock(principal, fieldId, el, isRateLocked, lockAction, warningMessages);
                        break;
                    case "breakinvestorratelock":
                        // 6/11/2014 dd - OPM 184058 - Support Break Investor Rate Lock through LO Format Importer.
                        dependencyPlaceholder = dataLoan.sfBreakInvestorRateLock;
                        isRateLocked = dataLoan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.Locked;
                        lockAction = dataLoan.BreakInvestorRateLock;
                        TryBreakOrSuspendLock(principal, fieldId, el, isRateLocked, lockAction, warningMessages);
                        break;
                    case "suspendinvestorratelock":
                        dependencyPlaceholder = dataLoan.sfBreakInvestorRateLock;
                        isRateLocked = dataLoan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.Locked;
                        lockAction = dataLoan.SuspendInvestorRateLock;
                        TryBreakOrSuspendLock(principal, fieldId, el, isRateLocked, lockAction, warningMessages);
                        break;


                    case "updatestatusofarchiveinunknownstatus":
                        ClosingCostArchive.E_ClosingCostArchiveStatus status;
                        if (dataLoan.sClosingCostArchiveInUnknownStatus != null && Enum.TryParse(value, out status))
                        {
                            dataLoan.UpdateStatusOfArchiveInUnknownStatus(status);
                        }
                        break;
                    case "updatependingloanestimatearchivestatus":
                        if (dataLoan.sLoanEstimateArchiveInPendingStatus != null && Enum.TryParse(value, out status))
                        {
                            dataLoan.UpdateLoanEstimateArchiveWithPendingStatus(status);
                        }
                        break;
                    case "smicompanynmt":
                        if (value.Equals("UGI", StringComparison.OrdinalIgnoreCase))
                        {
                            value = nameof(E_sMiCompanyNmT.UnitedGuaranty);
                        }

                        PageDataUtilities.SetValue(dataLoan, null, fieldId, value);
                        break;
                    // BEGIN compatibility shim for special FHA preparers
                    // Uncomment the logging once the doc vendors are migrated. If no logs are generated afterwards,
                    // remove the shim.
                    case "getpreparerofform[fhaddendummortgagee].preparername":
                        dataLoan.sFHAAddendumMortgageeName = value;
                        ////Tools.LogWarning("Used Save shim for sFHAAddendumMortgageeName");
                        break;
                    case "getpreparerofform[fhaddendummortgagee].title":
                        dataLoan.sFHAAddendumMortgageeTitle = value;
                        ////Tools.LogWarning("Used Save shim for sFHAAddendumMortgageeTitle");
                        break;
                    case "getpreparerofform[fhaaddendumunderwriter].preparername":
                        dataLoan.sFHAAddendumUnderwriterName = value;
                        ////Tools.LogWarning("Used Save shim for sFHAAddendumUnderwriterName");
                        break;
                    case "getpreparerofform[fhaaddendumunderwriter].licensenumofagent":
                        dataLoan.sFHAAddendumUnderwriterChumsId = value;
                        ////Tools.LogWarning("Used Save shim for sFHAAddendumUnderwriterChumsId");
                        break;
                    case "getpreparerofform[fha92900ltunderwriter].licensenumofagent":
                        dataLoan.sFHA92900LtUnderwriterChumsId = value;
                        ////Tools.LogWarning("Used Save shim for sFHA92900LtUnderwriterChumsId");
                        break;
                    // END compatibility shim for special FHA preparers
                    default:
                        if (PageDataUtilities.ContainsField(fieldId))
                        {
                            try
                            {
                                PageDataUtilities.SetValue(dataLoan, null, fieldId, value);
                            }
                            catch (NotFoundException)
                            {
                                warningMessages.AppendFormat("WARNING: {0} is invalid field id. Skip.{1}", fieldId, Environment.NewLine);
                            }
                            catch (FieldInvalidValueException)
                            {
                                InvalidEnumValue(fieldId, value, warningMessages);
                            }
                            catch (TargetInvocationException ex) when(ex.InnerException is CBaseException)
                            {
                                Tools.LogWarning(ex);
                                result.ResultStatus = LoFormatImporterResultStatus.Error;
                                result.ErrorMessages = $"{fieldId} could not be set. Error Message: {((CBaseException)ex.InnerException).UserMessage}";
                                return result;
                            }
                        }
                        else
                        {
                            warningMessages.AppendFormat("WARNING: {0} is invalid field id. Skip.{1}", fieldId, Environment.NewLine);
                        }
                        break;
                }

            }
            #endregion
            bool isImportLiabilitiesFromCreditReport = true;
            bool deleteExistingLiabilities = true; // 9/19/2013 dd - Be default wipe out existing liabilities when order credit.
            List<CreditRequestData> creditRequestDataList = new List<CreditRequestData>();
            List<KeyValuePair<Guid, ICreditReportResponse>> credit_mismoList = new List<KeyValuePair<Guid, ICreditReportResponse>>();
            #region //loan/applicant
            XmlNodeList applicantList = doc.SelectNodes("//loan/applicant");
            foreach (XmlElement applicant in applicantList)
            {
                string id = applicant.GetAttribute("id");

                if ("" == id)
                {
                    warningMessages.AppendFormat("Applicant id cannot be empty. Must be a valid ssn.{0}", Environment.NewLine);
                    continue;
                }

                // 8/25/2008 dd - Look for 2 special attributes that will automatically reissue mcl credit.
                string mclInstantViewId = applicant.GetAttribute("mcl_instant_view_id");
                string mclReportId = applicant.GetAttribute("mcl_report_id");


                CAppData dataApp = FindDataAppById(dataLoan, id);

                if ("" != mclInstantViewId && null != mclInstantViewId && "" != mclReportId && null != mclReportId)
                {
                    PullMclInstantViewCreditReport(dataLoan, dataApp, mclInstantViewId, mclReportId, warningMessages, principal);
                }

                bool isUpdateLiaSpecialRecord = false;
                bool isUpdateAssetSpecialRecord = false;
                var alimony = dataApp.aLiaCollection.GetAlimony(true);
                var childSupport = dataApp.aLiaCollection.GetChildSupport(true);
                var jobExpense1 = dataApp.aLiaCollection.GetJobRelated1(true);
                var jobExpense2 = dataApp.aLiaCollection.GetJobRelated2(true);
                Lazy<IPrimaryEmploymentRecord> aBPrimaryEmployment = new Lazy<IPrimaryEmploymentRecord>(() => dataApp.aBEmpCollection.GetPrimaryEmp(true));
                Lazy<IPrimaryEmploymentRecord> aCPrimaryEmployment = new Lazy<IPrimaryEmploymentRecord>(() => dataApp.aCEmpCollection.GetPrimaryEmp(true));

                Func<CAppData, IAssetCashDeposit> cashDeposit1Func = a => a.aAssetCollection.GetCashDeposit1(true);
                Func<CAppData, IAssetCashDeposit> cashDeposit2Func = a => a.aAssetCollection.GetCashDeposit2(true);
                Func<CAppData, IAssetLifeInsurance> lifeInsuranceFunc = a => a.aAssetCollection.GetLifeInsurance(true);
                Func<CAppData, IAssetRetirement> retirementFunc = a => a.aAssetCollection.GetRetirement(true);
                Func<CAppData, IAssetBusiness> businessFunc = a => a.aAssetCollection.GetBusinessWorth(true);

                List<Action<CAppData>> postBindAppPreSaveActions = new List<Action<CAppData>>();

                #region //loan/applicant/credit
                XmlNodeList creditList = applicant.SelectNodes("credit");
                if (null != creditList)
                {
                    if (creditList.Count > 1)
                        warningMessages.AppendLine("WARNING: Too many credit requests for this applicant. Only processing the first request.");

                    if (creditList.Count > 0)
                    {
                        XmlElement creditNode = (XmlElement)creditList[0];
                        try
                        {
                            Guid craId = new Guid(creditNode.GetAttribute("craid"));
                            string requestType = creditNode.GetAttribute("requestType");
                            bool includeEquifax = creditNode.GetAttribute("equifax").ToUpper() == "Y";
                            bool includeExperian = creditNode.GetAttribute("experian").ToUpper() == "Y";
                            bool includeTransUnion = creditNode.GetAttribute("transunion").ToUpper() == "Y";
                            string reportId = creditNode.GetAttribute("reportId");
                            string instantViewId = creditNode.GetAttribute("instantViewId");
                            string loginName = creditNode.GetAttribute("loginName");
                            string password = creditNode.GetAttribute("password");
                            string accountId = creditNode.GetAttribute("accountId");

                            XmlElement creditBilling = creditNode.GetElementsByTagName("creditcard").Cast<XmlElement>().FirstOrDefault();

                            // OPM 450477. If credentials are passed in, use them, otherwise
                            // try to use applicable saved credentials if possible.
                            // If nothing there, let it fail due to missing credentials.
                            var allowOrder = true;
                            if (string.IsNullOrEmpty(loginName)
                                || string.IsNullOrEmpty(password))
                            {
                                var storedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.CreditReports)
                                    .FirstOrDefault(credential => credential.ServiceProviderId.HasValue && credential.ServiceProviderId == craId);

                                if (storedCredential != null)
                                {
                                    loginName = storedCredential.UserName;
                                    password = storedCredential.UserPassword.Value;
                                    accountId = storedCredential.AccountId;
                                }
                                else
                                {
                                    // OPM 450477.
                                    warningMessages.AppendLine("Error message: Credit cannot be ordered. Please provide a loginName and password.");
                                    allowOrder = false;
                                }
                            }

                            if (allowOrder)
                            {
                                string fannieMaeMORNETUserID = creditNode.GetAttribute("fannieMaeMORNETUserID");
                                string fannieMaeMORNETPassword = creditNode.GetAttribute("fannieMaeMORNETPassword");
                                if (creditNode.GetAttribute("isImportLiabilitiesFromCreditReport").ToUpper() == "N")
                                {
                                    isImportLiabilitiesFromCreditReport = false;
                                }

                                // 9/19/2013 dd - If deleteExistingLiabilities attribute is pass then do not delete
                                // existing liabilities when order credit report.
                                if (creditNode.GetAttribute("deleteExistingLiabilities").Equals("N", StringComparison.OrdinalIgnoreCase))
                                {
                                    deleteExistingLiabilities = false;
                                }

                                CreditRequestData _creditRequestData = CreditRequestData.Create(sLId, dataApp.aAppId);
                                _creditRequestData.ReferenceNumber = dataLoan.sLNm;
                                CRA cra = _creditRequestData.UpdateCRAInfoFrom(craId, principal.BrokerId, true);
                                switch (requestType.ToUpper())
                                {
                                    case "NEW":
                                        _creditRequestData.MclRequestType = MclRequestType.New;
                                        _creditRequestData.RequestActionType = CreditReport.Mismo.E_CreditReportRequestActionType.Submit;
                                        break;
                                    case "GET":
                                        _creditRequestData.MclRequestType = MclRequestType.Get;
                                        _creditRequestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Reissue;
                                        break;
                                    case "UPGRADE":
                                        _creditRequestData.MclRequestType = MclRequestType.Upgrade;
                                        _creditRequestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                                        break;
                                    case "REFRESH":
                                        _creditRequestData.MclRequestType = MclRequestType.Refresh;
                                        _creditRequestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Other;
                                        _creditRequestData.RequestActionTypeOtherDetail = CreditReport.Mismo.CreditReportRequestActionTypeOtherDetail.LqiNew;
                                        _creditRequestData.IsRequestLqiReport = true;
                                        break;
                                    // 2017-05 247780 re-issue LQI report is not yet supported via web services
                                    default:
                                        warningMessages.AppendLine("ERROR: Unsupport requestType=" + requestType + ". Credit will not be pull.");
                                        throw new CBaseException("ERROR: Unsupport requestType=" + requestType + ". Credit will not be pull.", "ERROR: Unsupport requestType=" + requestType + ". Credit will not be pull.");
                                }

                                _creditRequestData.IncludeEquifax = includeEquifax;
                                _creditRequestData.IncludeExperian = includeExperian;
                                _creditRequestData.IncludeTransUnion = includeTransUnion;

                                _creditRequestData.ReportID = reportId;
                                _creditRequestData.InstantViewID = instantViewId;
                                _creditRequestData.LoginInfo.AccountIdentifier = accountId;
                                _creditRequestData.LoginInfo.UserName = loginName;
                                _creditRequestData.LoginInfo.Password = password;
                                _creditRequestData.LoginInfo.FannieMaeMORNETUserID = fannieMaeMORNETUserID;
                                _creditRequestData.LoginInfo.FannieMaeMORNETPassword = fannieMaeMORNETPassword;

                                if (creditBilling != null)
                                {
                                    _creditRequestData.BillingCardNumber = creditBilling.GetAttribute("cardNumber");
                                    _creditRequestData.BillingCVV = creditBilling.GetAttribute("cvv");
                                    _creditRequestData.BillingExpirationMonth = creditBilling.GetAttribute("expirationMonth").ToNullable<int>(int.TryParse) ?? 0;
                                    _creditRequestData.BillingExpirationYear = creditBilling.GetAttribute("expirationYear").ToNullable<int>(int.TryParse) ?? 0;
                                    _creditRequestData.BillingStreetAddress = creditBilling.GetAttribute("streetAddress");
                                    _creditRequestData.BillingCity = creditBilling.GetAttribute("addressCity");
                                    _creditRequestData.BillingState = creditBilling.GetAttribute("addressState");
                                    _creditRequestData.BillingZipcode = creditBilling.GetAttribute("addressZipcode");
                                    _creditRequestData.BillingFirstName = creditBilling.GetAttribute("cardholderFirstName");
                                    _creditRequestData.BillingLastName = creditBilling.GetAttribute("cardholderLastName");
                                    _creditRequestData.UseCreditCardPayment = true;
                                }

                                if (null != cra)
                                {
                                    _creditRequestData.LoginInfo.FannieMaeCreditProvider = cra.Url;
                                }
                                creditRequestDataList.Add(_creditRequestData);
                            }
                        }
                        catch (Exception exc)
                        {
                            Tools.LogError(exc);
                            warningMessages.AppendLine("ERROR: Unable to parse credit node. Credit Report will not be pull.");
                        }
                    }
                }
                #endregion

                #region //loan/applicant/credit_mismo
                // 1/19/2011 dd - "<![CDATA[ ]]> section must be a child of credit_mismo element.
                XmlElement credit_mismo = (XmlElement)applicant.SelectSingleNode("credit_mismo");
                if (null != credit_mismo)
                {
                    string mismoXml = credit_mismo.FirstChild.Value;

                    XmlDocument mismoDoc = Tools.CreateXmlDoc(mismoXml);

                    ICreditReportResponse mismoCreditResponse = new LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse(mismoDoc);


                    if (mismoCreditResponse.HasError == false && mismoCreditResponse.IsReady)
                    {
                        // 1/13/2015 ir - defer credit import to after dataLoan.Save(). Otherwise, liabilities may be overwritten.
                        credit_mismoList.Add(new KeyValuePair<Guid, ICreditReportResponse>(dataApp.aAppId, mismoCreditResponse));
                    }
                }
                #endregion
                #region //loan/applicant/field
                XmlNodeList fieldList = applicant.SelectNodes("field");
                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id").TrimWhitespaceAndBOM();
                    string value = el.InnerText;
                    switch (fieldId.ToLower())
                    {
                        case "aalimonynm":
                            alimony.OwedTo = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "aalimonyexcl":
                            alimony.NotUsedInRatio = value.ToLower() == "true";
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "aalimonypmt":
                            alimony.Pmt_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "aalimonyremainmons":
                            alimony.RemainMons_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "achildsupportnm":
                            childSupport.OwedTo = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "achildsupportexcl":
                            childSupport.NotUsedInRatio = value.ToLower() == "true";
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "achildsupportpmt":
                            childSupport.Pmt_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "achildsupportremainmons":
                            childSupport.RemainMons_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "ajobrelated1excl":
                            jobExpense1.NotUsedInRatio = value.ToLower() == "true";
                            break;
                        case "ajobrelated1expensedesc":
                            jobExpense1.ExpenseDesc = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "ajobrelated1pmt":
                            jobExpense1.Pmt_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "ajobrelated2excl":
                            jobExpense2.NotUsedInRatio = value.ToLower() == "true";
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "ajobrelated2expensedesc":
                            jobExpense2.ExpenseDesc = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "ajobrelated2pmt":
                            jobExpense2.Pmt_rep = value;
                            isUpdateLiaSpecialRecord = true;
                            break;
                        case "aprodbcitizent":
                            // 3/26/2007 dd - Need to switch to PriceMyLoan calc mode so that aProdBCitizenT can be set correctly.
                            oldCalcMode = dataLoan.CalcModeT;
                            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                            switch (value)
                            {
                                case "0": dataApp.aProdBCitizenT = E_aProdCitizenT.USCitizen; break;
                                case "1": dataApp.aProdBCitizenT = E_aProdCitizenT.PermanentResident; break;
                                case "2": dataApp.aProdBCitizenT = E_aProdCitizenT.NonpermanentResident; break;
                                case "3": dataApp.aProdBCitizenT = E_aProdCitizenT.ForeignNational; break;
                                default:
                                    InvalidEnumValue("aProdBCitizenT", value, warningMessages);
                                    break;

                            }
                            dataLoan.CalcModeT = oldCalcMode;
                            break;
                        case "aprodccitizent":
                            oldCalcMode = dataLoan.CalcModeT;
                            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                            switch (value)
                            {
                                case "0": dataApp.aProdCCitizenT = E_aProdCitizenT.USCitizen; break;
                                case "1": dataApp.aProdCCitizenT = E_aProdCitizenT.PermanentResident; break;
                                case "2": dataApp.aProdCCitizenT = E_aProdCitizenT.NonpermanentResident; break;
                                case "3": dataApp.aProdCCitizenT = E_aProdCitizenT.ForeignNational; break;
                                default:
                                    InvalidEnumValue("aProdCCitizenT", value, warningMessages);
                                    break;
                            }
                            dataLoan.CalcModeT = oldCalcMode;
                            break;
                        case "acisselfemplmt":
                            oldCalcMode = dataLoan.CalcModeT;
                            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                            dataApp.aCIsSelfEmplmt = value.ToLower() == "true";
                            dataLoan.CalcModeT = oldCalcMode;
                            break;
                        case "atransmomonpmtpe":
                            oldCalcMode = dataLoan.CalcModeT;
                            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                            dataApp.aTransmOMonPmtPe_rep = value;
                            dataLoan.CalcModeT = oldCalcMode;
                            break;
                        case "aopnegcf":
                            dataApp.aOpNegCf_rep = value;
                            dataApp.aOpNegCfLckd = true;
                            break;
                        case "apresohexppe":
                            // 3/27/2007 dd - Need to switch to PriceMyLoanCalcMode when set Pe field.
                            oldCalcMode = dataLoan.CalcModeT;
                            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                            dataApp.aPresOHExpPe_rep = value;
                            dataLoan.CalcModeT = oldCalcMode;
                            break;
                        case "abprimaryisselfemplmt":
                            aBPrimaryEmployment.Value.IsSelfEmplmt = value.ToLower() == "true";
                            break;
                        case "abprimaryemplrnm":
                            aBPrimaryEmployment.Value.EmplrNm = value;
                            break;
                        case "abprimaryemplraddr":
                            aBPrimaryEmployment.Value.EmplrAddr = value;
                            break;
                        case "abprimaryemplrcity":
                            aBPrimaryEmployment.Value.EmplrCity = value;
                            break;
                        case "abprimaryemplrstate":
                            aBPrimaryEmployment.Value.EmplrState = value;
                            break;
                        case "abprimaryemplrzip":
                            aBPrimaryEmployment.Value.EmplrZip = value;
                            break;
                        case "abprimaryemplmtlen":
                            aBPrimaryEmployment.Value.EmplmtLen_rep = value;
                            break;
                        case "abprimaryproflen":
                            aBPrimaryEmployment.Value.ProfLen_rep = value;
                            break;
                        case "abprimaryjobtitle":
                            aBPrimaryEmployment.Value.JobTitle = value;
                            break;
                        case "abprimaryemplrbusphone":
                            aBPrimaryEmployment.Value.EmplrBusPhone = value;
                            dataApp.aBEmplrBusPhoneLckd = true;
                            break;
                        case "abprimaryemplrfax":
                            aBPrimaryEmployment.Value.EmplrFax = value;
                            break;
                        case "abprimaryverifsentd":
                            aBPrimaryEmployment.Value.VerifSentD_rep = value;
                            break;
                        case "abprimaryverifreorderedd":
                            aBPrimaryEmployment.Value.VerifReorderedD_rep = value;
                            break;
                        case "abprimaryverifrecvd":
                            aBPrimaryEmployment.Value.VerifRecvD_rep = value;
                            break;
                        case "abprimaryverifexpd":
                            aBPrimaryEmployment.Value.VerifExpD_rep = value;
                            break;
                        case "abprimaryempltstartd":
                            aBPrimaryEmployment.Value.SetEmploymentStartDate(value);
                            break;
                        case "abprimaryprofstartd":
                            aBPrimaryEmployment.Value.SetProfessionStartDate(value);
                            break;

                        case "acprimaryisselfemplmt":
                            aCPrimaryEmployment.Value.IsSelfEmplmt = value.ToLower() == "true";
                            break;
                        case "acprimaryemplrnm":
                            aCPrimaryEmployment.Value.EmplrNm = value;
                            break;
                        case "acprimaryemplraddr":
                            aCPrimaryEmployment.Value.EmplrAddr = value;
                            break;
                        case "acprimaryemplrcity":
                            aCPrimaryEmployment.Value.EmplrCity = value;
                            break;
                        case "acprimaryemplrstate":
                            aCPrimaryEmployment.Value.EmplrState = value;
                            break;
                        case "acprimaryemplrzip":
                            aCPrimaryEmployment.Value.EmplrZip = value;
                            break;
                        case "acprimaryemplmtlen":
                            aCPrimaryEmployment.Value.EmplmtLen_rep = value;
                            break;
                        case "acprimaryproflen":
                            aCPrimaryEmployment.Value.ProfLen_rep = value;
                            break;
                        case "acprimaryjobtitle":
                            aCPrimaryEmployment.Value.JobTitle = value;
                            break;
                        case "acprimaryemplrbusphone":
                            aCPrimaryEmployment.Value.EmplrBusPhone = value;
                            dataApp.aCEmplrBusPhoneLckd = true;
                            break;
                        case "acprimaryemplrfax":
                            aCPrimaryEmployment.Value.EmplrFax = value;
                            break;
                        case "acprimaryverifsentd":
                            aCPrimaryEmployment.Value.VerifSentD_rep = value;
                            break;
                        case "acprimaryverifreorderedd":
                            aCPrimaryEmployment.Value.VerifReorderedD_rep = value;
                            break;
                        case "acprimaryverifrecvd":
                            aCPrimaryEmployment.Value.VerifRecvD_rep = value;
                            break;
                        case "acprimaryverifexpd":
                            aCPrimaryEmployment.Value.VerifExpD_rep = value;
                            break;
                        case "acprimaryempltstartd":
                            aCPrimaryEmployment.Value.SetEmploymentStartDate(value);
                            break;
                        case "acprimaryprofstartd":
                            aCPrimaryEmployment.Value.SetProfessionStartDate(value);
                            break;
                        case "acashdeposit1_desc":
                            postBindAppPreSaveActions.Add(a => cashDeposit1Func(a).Desc = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "acashdeposit1_val":
                            postBindAppPreSaveActions.Add(a => cashDeposit1Func(a).Val_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "acashdeposit2_desc":
                            postBindAppPreSaveActions.Add(a => cashDeposit2Func(a).Desc = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "acashdeposit2_val":
                            postBindAppPreSaveActions.Add(a => cashDeposit2Func(a).Val_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "aassetlifeins_faceval":
                            postBindAppPreSaveActions.Add(a => lifeInsuranceFunc(a).FaceVal_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "aassetlifeins_val":
                            postBindAppPreSaveActions.Add(a => lifeInsuranceFunc(a).Val_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "aassetretirement_val":
                            postBindAppPreSaveActions.Add(a => retirementFunc(a).Val_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "aassetbusiness_val":
                            postBindAppPreSaveActions.Add(a => businessFunc(a).Val_rep = value);
                            isUpdateAssetSpecialRecord = true;
                            break;
                        case "ao1iforc":
                        case "ao1idesc":
                        case "ao1i":
                        case "ao2iforc":
                        case "ao2idesc":
                        case "ao2i":
                        case "ao3iforc":
                        case "ao3idesc":
                        case "ao3i":
                            {
                                if (dataLoan.sIsIncomeCollectionEnabled)
                                {
                                    warningMessages.AppendLine("WARNING: '" + fieldId + "' is not supported for collection-based income. Please use aOtherIncomeCollection instead. Skip.");
                                    break;
                                }

                                warningMessages.AppendLine("WARNING: '" + fieldId + "' has been deprecated and will no longer be supported. Please use aOtherIncomeCollection instead.");
                                List<OtherIncome> otherIncome = dataApp.aOtherIncomeList;
                                int index = int.Parse(fieldId.Substring(2, 1)) - 1;
                                string suffix = fieldId.Substring(4);
                                if (StringComparer.OrdinalIgnoreCase.Equals(suffix, "ForC"))
                                {
                                    otherIncome[index].IsForCoBorrower = bool.Parse(value);
                                }
                                else if (StringComparer.OrdinalIgnoreCase.Equals(suffix, "Desc"))
                                {
                                    otherIncome[index].Desc = value;
                                }
                                else if (suffix == string.Empty)
                                {
                                    otherIncome[index].Amount = dataApp.m_convertLos.ToMoney(value);
                                }

                                dataApp.aOtherIncomeList = otherIncome;
                                break;
                            }
                        case "abbasei":
                        case "abovertimei":
                        case "abbonusesi":
                        case "abcommisioni":
                        case "abdividendi":
                        case "acbasei":
                        case "acovertimei":
                        case "acbonusesi":
                        case "accommisioni":
                        case "acdividendi":
                            string incomeError;
                            bool success = SetIncomeField(dataLoan, dataApp, fieldId, value, out incomeError);
                            if (!success)
                            {
                                warningMessages.AppendLine("ERROR: " + incomeError + " Skip.");
                            }
                            break;
                        default:
                            if (PageDataUtilities.ContainsField(fieldId))
                            {
                                try
                                {
                                    PageDataUtilities.SetValue(dataLoan, dataApp, fieldId, value);
                                }
                                catch (NotFoundException)
                                {
                                    warningMessages.AppendFormat("WARNING: {0} is invalid field id. Skip.{1}", fieldId, Environment.NewLine);
                                }
                                catch (FieldInvalidValueException)
                                {
                                    InvalidEnumValue(fieldId, value, warningMessages);
                                }
                            }
                            else
                            {
                                warningMessages.AppendFormat("WARNING: {0} is invalid field id. Skip.{1}", fieldId, Environment.NewLine);
                            }
                            break;
                    }
                }

                if (isUpdateLiaSpecialRecord)
                {
                    dataApp.aLiaCollection.Flush();
                }

                if (aBPrimaryEmployment.IsValueCreated)
                {
                    dataApp.aBEmpCollection.Flush();
                }
                if (aCPrimaryEmployment.IsValueCreated)
                {
                    dataApp.aCEmpCollection.Flush();
                }

                #endregion

                #region //loan/applicant/collection - Liabilities

                XmlNodeList applicantCollectionList = applicant.SelectNodes("collection");
                foreach (XmlElement collection in applicantCollectionList)
                {
                    string collectionId = collection.GetAttribute("id");
                    bool isDeleteExisting = collection.GetAttribute("DeleteExisting").Equals("True", StringComparison.OrdinalIgnoreCase);
                    switch (collectionId.ToLower())
                    {
                        case "aliacollection":
                            ImportLiabilities(dataApp, principal, collection, warningMessages);
                            break;
                        case "aassetcollection":
                            ImportAssets(dataApp, principal, collection, isDeleteExisting, warningMessages);
                            break;
                        case "arecollection":
                            ImportReos(dataApp, principal, collection, isDeleteExisting, warningMessages);
                            break;
                        case "abempcollection":
                            ImportEmploymentRecord(dataApp, principal, collection, isDeleteExisting, warningMessages, true);
                            break;
                        case "acempcollection":
                            ImportEmploymentRecord(dataApp, principal, collection, isDeleteExisting, warningMessages, false);
                            break;
                        case "aotherincomecollection":
                            ImportOtherIncome(dataApp, principal, collection, warningMessages);
                            break;
                        default:
                            warningMessages.AppendFormat("WARNING: {0} is invalid collection id. Skip.{1}", collectionId, Environment.NewLine);
                            break;
                    }
                }

                #endregion

                if (isUpdateAssetSpecialRecord)
                {
                    postBindAppPreSaveActions.Add(a => a.aAssetCollection.Flush());
                }

                foreach (var action in postBindAppPreSaveActions)
                {
                    action(dataApp);
                }
            }
            #endregion
            #region //loan/collection
            XmlNodeList collectionList = doc.SelectNodes("//loan/collection");
            foreach (XmlElement collection in collectionList)
            {
                LoFormatImporterResult collectionResult = SetLoanCollection(collection, dataLoan, principal, result, warningMessages);
                if (collectionResult != null)
                {
                    return collectionResult; // Usually this means a fatal error occurred
                }
            }
            #endregion

            foreach (var action in postBindLoanPreSaveActions)
            {
                action(dataLoan);
            }

            // 6/18/2013 dd - Save Method must invoke before order credit report.
            // If Save method is call after order credit then it will override the new liabilities
            // that were import from credit report.
            dataLoan.Save();

            if (fieldsToOutput != null)
            {
                foreach (var field in fieldsToOutput)
                {
                    switch (field.ToLower())
                    {
                        case "sselectedproductcodefilter":
                            if (!result.FieldsToOutput.ContainsKey("sSelectedProductCodeFilter"))
                            {
                                result.FieldsToOutput.Add("sSelectedProductCodeFilter", CreateProductCodeLoXml(principal, dataLoan.BrokerDB, dataLoan.sSelectedProductCodeFilter, "sSelectedProductCodeFilter"));
                            }
                            break;
                        case "savailableproductcodefilter":
                            if (!result.FieldsToOutput.ContainsKey("sAvailableProductCodeFilter"))
                            {
                                result.FieldsToOutput.Add("sAvailableProductCodeFilter", CreateProductCodeLoXml(principal, dataLoan.BrokerDB, dataLoan.sAvailableProductCodeFilter, "sAvailableProductCodeFilter"));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            // 1/13/2015 ir - Import credit_mismo here
            foreach (KeyValuePair<Guid, ICreditReportResponse> credit_mismo in credit_mismoList)
            {
                Guid appId = credit_mismo.Key;
                ICreditReportResponse creditResponse = credit_mismo.Value;
                Guid comId = ConstAppDavid.DummyFannieMaeServiceCompany;
                CreditReportUtilities.SaveXmlCreditReport(creditResponse, principal, dataLoan.sLId, appId, comId, Guid.Empty, E_CreditReportAuditSourceT.ImportFromDoDu);
                CreditReportUtilities.ImportLiabilities(dataLoan.sLId, appId, true /* skipZeroBalance */, true /* deleteExistingLiabilities */, true /* importBorrowerInfo */);
            }

            // 6/25/2009 dd - OPM 31849 - Allow user to order credit report through API.
            foreach (CreditRequestData creditRequestData in creditRequestDataList)
            {
                try
                {
                    Guid aAppId = creditRequestData.ApplicationId;
                    Guid comId = creditRequestData.CraId;

                    if (principal.Type == "B")
                    {
                        BrokerDB requestingParty = principal.BrokerDB;
                        creditRequestData.RequestingPartyName = requestingParty.Name;
                        creditRequestData.RequestingPartyStreetAddress = requestingParty.Address.StreetAddress;
                        creditRequestData.RequestingPartyCity = requestingParty.Address.City;
                        creditRequestData.RequestingPartyState = requestingParty.Address.State;
                        creditRequestData.RequestingPartyZipcode = requestingParty.Address.Zipcode;
                    }
                    else if (principal.Type == "P")
                    {
                        creditRequestData.RequestingPartyName = principal.DisplayName;
                    }

                    CAppData dataApp = dataLoan.GetAppData(aAppId);
                    creditRequestData.Borrower.FirstName = dataApp.aBFirstNm;
                    creditRequestData.Borrower.LastName = dataApp.aBLastNm;
                    creditRequestData.Borrower.MiddleName = dataApp.aBMidNm;
                    creditRequestData.Borrower.Suffix = dataApp.aBSuffix;
                    creditRequestData.Borrower.Ssn = dataApp.aBSsn;
                    creditRequestData.Borrower.DOB = dataApp.aBDob_rep;
                    creditRequestData.Borrower.MaritalStatus = dataApp.aBMaritalStatT;
                    try
                    {
                        Address address = new Address();
                        address.ParseStreetAddress(dataApp.aBAddr);
                        address.City = dataApp.aBCity;
                        address.State = dataApp.aBState;
                        address.Zipcode = dataApp.aBZip;

                        creditRequestData.Borrower.CurrentAddress = address;
                        creditRequestData.Borrower.YearsAtCurrentAddress = dataApp.aBAddrYrs;
                    }
                    catch (ApplicationException)
                    {
                        Address address = new Address();
                        address.StreetAddress = dataApp.aBAddr;
                        address.City = dataApp.aBCity;
                        address.State = dataApp.aBState;
                        address.Zipcode = dataApp.aBZip;
                        creditRequestData.Borrower.CurrentAddress = address;

                        Tools.LogWarning("Unable to parse borrower address: " + dataApp.aBAddr);
                    }

                    Address mailingAddress = new Address();
                    mailingAddress.StreetAddress = dataApp.aAddrMail;
                    mailingAddress.City = dataApp.aCityMail;
                    mailingAddress.State = dataApp.aStateMail;
                    mailingAddress.Zipcode = dataApp.aZipMail;
                    creditRequestData.Borrower.MailingAddress = mailingAddress;

                    // Co-borrower
                    creditRequestData.Coborrower.FirstName = dataApp.aCFirstNm;
                    creditRequestData.Coborrower.LastName = dataApp.aCLastNm;
                    creditRequestData.Coborrower.MiddleName = dataApp.aCMidNm;
                    creditRequestData.Coborrower.Suffix = dataApp.aCSuffix;
                    creditRequestData.Coborrower.Ssn = dataApp.aCSsn;
                    creditRequestData.Coborrower.DOB = dataApp.aCDob_rep;
                    creditRequestData.Coborrower.MaritalStatus = (E_aBMaritalStatT)dataApp.aCMaritalStatT;

                    if (creditRequestData.CreditProtocol == CreditReportProtocol.CSD || creditRequestData.CreditProtocol == CreditReportProtocol.InformativeResearch)
                    {
                        if (!string.IsNullOrEmpty(dataApp.aCAddrMail) || !string.IsNullOrEmpty(dataApp.aCCityMail)
                            || !string.IsNullOrEmpty(dataApp.aCStateMail) || !string.IsNullOrEmpty(dataApp.aCZipMail))
                        {
                            Address coMailingAddress = new Address();
                            coMailingAddress.StreetAddress = dataApp.aCAddrMail;
                            coMailingAddress.City = dataApp.aCCityMail;
                            coMailingAddress.State = dataApp.aCStateMail;
                            coMailingAddress.Zipcode = dataApp.aCZipMail;
                            creditRequestData.Borrower.MailingAddress = coMailingAddress;
                        }
                    }

                    var maxAttemptCount = 20;
                    var pollingIntervalInSeconds = 5;
                    var principalSubmittingCreditRequest = principal is ConsumerPortalUserPrincipal ? ((ConsumerPortalUserPrincipal)principal).GetImpersonatePrincipal() : principal;
                    var requestHandler = new CreditReportRequestHandler(creditRequestData, principalSubmittingCreditRequest, shouldReissueOnResponseNotReady: true, retryIntervalInSeconds: pollingIntervalInSeconds, maxRetryCount: maxAttemptCount, loanId: sLId);
                    if (ConstStage.DisableCreditOrderingViaBJP)
                    {
                        // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                        OrderCreditSynchronously(requestHandler, maxAttemptCount, warningMessages, dataLoan, dataApp, creditRequestData, principal, comId, isImportLiabilitiesFromCreditReport, deleteExistingLiabilities);
                    }
                    else
                    {
                        var initialResult = requestHandler.SubmitToProcessor();
                        if (initialResult.Status == CreditReportRequestResultStatus.Failure)
                        {
                            foreach (var error in initialResult.ErrorMessages.CoalesceWithEmpty())
                            {
                                warningMessages.AppendLine(error);
                            }
                        }
                        else
                        {
                            var publicJobId = initialResult.PublicJobId;
                            var hasReceivedResults = false;
                            for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
                            {
                                var finalResult = CreditReportRequestHandler.CheckForRequestResults(publicJobId.Value);
                                if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                                {
                                    foreach (var error in finalResult.ErrorMessages.CoalesceWithEmpty())
                                    {
                                        warningMessages.AppendLine(error);
                                    }

                                    hasReceivedResults = true;
                                    break;
                                }
                                else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                                {
                                    var creditResponse = finalResult.CreditReportResponse;
                                    var finalCreditRequestData = finalResult.CreditRequestData;

                                    if (null == creditResponse)
                                    {
                                        warningMessages.AppendLine("ERROR: Unable to reissue credit report.");
                                    }
                                    else if (creditResponse.HasError)
                                    {
                                        warningMessages.AppendLine("ERROR: Unable to reissue credit report. " + creditResponse.ErrorMessage);
                                    }
                                    else
                                    {
                                        // The credit response should be ready at this point.
                                        if (CreditReportUtilities.IsBorrowerInformationMatch(dataLoan.sLId, dataApp.aAppId, finalCreditRequestData, creditResponse))
                                        {
                                            CreditReportUtilities.SaveXmlCreditReport(creditResponse, principal, dataLoan.sLId, dataApp.aAppId, comId, finalCreditRequestData.ProxyId, E_CreditReportAuditSourceT.OrderNew);
                                            if (isImportLiabilitiesFromCreditReport)
                                            {
                                                CreditReportUtilities.ImportLiabilities(dataLoan.sLId, dataApp.aAppId, true /* skipZeroBalance */, deleteExistingLiabilities, true /* importBorrowerInfo */);
                                            }
                                            else
                                            {

                                                dataApp.ImportCreditScoresFromCreditReport(); // Only Import Credit Score.
                                            }
                                        }
                                        else
                                        {
                                            warningMessages.Append("ERROR: Unable to import credit report. " + ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                                        }
                                    }

                                    hasReceivedResults = true;
                                    break;
                                }
                                else
                                {
                                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(pollingIntervalInSeconds * 1000);
                                }
                            }

                            if (!hasReceivedResults)
                            {
                                warningMessages.AppendLine(ErrorMessages.AsyncReportTakingTooLongToProcess);
                            }
                        }
                    }
                }
                catch (CBaseException exc)
                {
                    warningMessages.Append("ERROR: Unable to reissue credit report. " + exc.UserMessage);
                }
                catch (ApplicationException aExc) // OPM 40879
                {
                    warningMessages.Append("ERROR: Unable to reissue credit report. " + aExc.Message);
                }
            }

            result.ErrorMessages = warningMessages.ToString();
            if (string.IsNullOrEmpty(result.ErrorMessages))
            {
                result.ResultStatus = LoFormatImporterResultStatus.Ok;
            }
            else
            {
                result.ResultStatus = LoFormatImporterResultStatus.OkWithWarning;
            }

            return result;
        }

        private static bool SetIncomeField(CPageData dataLoan, CAppData dataApp, string fieldId, string value, out string error)
        {
            error = null;
            if (dataLoan.sIsIncomeCollectionEnabled)
            {
                var consumerId = GetConsumerIdForIncomeField(dataApp, fieldId);
                var incomeType = GetIncomeTypeForIncomeField(fieldId);

                if (dataLoan.HasIncomeSourceOfType(consumerId, incomeType))
                {
                    error = $"{fieldId} could not be set because the file is using the income collection and the consumer already has an income of type {incomeType}.";
                    return false;
                }
                else
                {
                    dataLoan.AddIncomeSource(
                        consumerId,
                        new LendingQB.Core.Data.IncomeSource
                        {
                            IncomeType = incomeType,
                            MonthlyAmountData = Money.Create(dataLoan.m_convertLos.ToMoney(value))
                        });
                }
            }
            else
            {
                PageDataUtilities.SetValue(dataLoan, dataApp, fieldId, value);
            }

            return true;
        }

        private static DataObjectIdentifier<DataObjectKind.Consumer, Guid> GetConsumerIdForIncomeField(CAppData dataApp, string fieldId)
        {
            switch (fieldId.ToLower())
            {
                case "abbasei":
                case "abovertimei":
                case "abbonusesi":
                case "abcommisioni":
                case "abdividendi":
                    return dataApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                case "acbasei":
                case "acovertimei":
                case "acbonusesi":
                case "accommisioni":
                case "acdividendi":
                    return dataApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled income field id: " + fieldId);
            }
        }

        private static IncomeType GetIncomeTypeForIncomeField(string fieldId)
        {
            switch (fieldId.ToLower())
            {
                case "abbasei":
                case "acbasei":
                    return IncomeType.BaseIncome;
                case "abovertimei":
                case "acovertimei":
                    return IncomeType.Overtime;
                case "abbonusesi":
                case "acbonusesi":
                    return IncomeType.Bonuses;
                case "abcommisioni":
                case "accommisioni":
                    return IncomeType.Commission;
                case "abdividendi":
                case "acdividendi":
                    return IncomeType.DividendsOrInterest;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled income field id: " + fieldId);
            }
        }

        private static void OrderCreditSynchronously(CreditReportRequestHandler requestHandler, int maxAttemptCount, StringBuilder warningMessages, CPageData dataLoan, CAppData dataApp, CreditRequestData creditRequestData, AbstractUserPrincipal principal,
            Guid comId, bool isImportLiabilitiesFromCreditReport, bool deleteExistingLiabilities)
        {
            var hasReceivedResults = false;
            for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
            {
                var finalResult = requestHandler.SubmitSynchronously();
                if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                {
                    foreach (var error in finalResult.ErrorMessages.CoalesceWithEmpty())
                    {
                        warningMessages.AppendLine(error);
                    }

                    hasReceivedResults = true;
                    break;
                }
                else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                {
                    var creditResponse = finalResult.CreditReportResponse;
                    if (null == creditResponse)
                    {
                        warningMessages.AppendLine("ERROR: Unable to reissue credit report.");
                    }
                    else if (creditResponse.HasError)
                    {
                        warningMessages.AppendLine("ERROR: Unable to reissue credit report. " + creditResponse.ErrorMessage);
                    }
                    else if (creditResponse.IsReady)
                    {
                        if (CreditReportUtilities.IsBorrowerInformationMatch(dataLoan.sLId, dataApp.aAppId, creditRequestData, creditResponse))
                        {
                            CreditReportUtilities.SaveXmlCreditReport(creditResponse, principal, dataLoan.sLId, dataApp.aAppId, comId, creditRequestData.ProxyId, E_CreditReportAuditSourceT.OrderNew);
                            if (isImportLiabilitiesFromCreditReport)
                            {
                                CreditReportUtilities.ImportLiabilities(dataLoan.sLId, dataApp.aAppId, true /* skipZeroBalance */, deleteExistingLiabilities, true /* importBorrowerInfo */);
                            }
                            else
                            {
                                dataApp.ImportCreditScoresFromCreditReport(); // Only Import Credit Score.
                            }
                        }
                        else
                        {
                            warningMessages.Append("ERROR: Unable to import credit report. " + ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else
                    {
                        if (creditRequestData.CreditProtocol == CreditReportProtocol.Mcl)
                        {
                            creditRequestData.ReportID = creditResponse.ReportID;
                            creditRequestData.MclRequestType = MclRequestType.Get;
                        }
                        else if (creditRequestData.CreditProtocol == CreditReportProtocol.InfoNetwork)
                        {
                            creditRequestData.ReportID = creditResponse.ReportID;
                            creditRequestData.RequestActionType = CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                        }

                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000);
                    }
                }
            }

            if (!hasReceivedResults)
            {
                warningMessages.AppendLine(ErrorMessages.AsyncReportTakingTooLongToProcess);
            }
        }

        /// <summary>
        /// Sets the loan collection for an element specified by <param name="collection"/>.  Non-null return values are reserved for early
        /// exit to the import process; a successful import will return null.
        /// </summary>
        private static LoFormatImporterResult SetLoanCollection(XmlElement collection, CPageData dataLoan, AbstractUserPrincipal principal, LoFormatImporterResult result, StringBuilder warningMessages)
        {
            string id = collection.GetAttribute("id");
            bool isDeleteExisting = collection.GetAttribute("DeleteExisting").Equals("True", StringComparison.OrdinalIgnoreCase);
            switch (id.ToLower())
            {
                case "sconddataset":
                    {
                        string conditionImportErrors;
                        if (!ImportConditions(dataLoan, principal, collection, warningMessages, out conditionImportErrors))
                        {
                            result.ResultStatus = LoFormatImporterResultStatus.Error;
                            result.ErrorMessages = conditionImportErrors;
                            return result;
                        }

                        break;
                    }
                case "sagentdataset":
                    AddAgents(dataLoan, collection, warningMessages);
                    break;
                case "sbrokerlockadjustments":
                    AddBrokerLockAdjustments(dataLoan, principal, collection, warningMessages);
                    break;
                case "sinvestorlockadjustments":
                    AddInvestorLockAdjustments(dataLoan, principal, collection, warningMessages);
                    break;
                case "brokerratelocktable":
                    SetBrokerRateLockTable(dataLoan, principal, collection, warningMessages);
                    break;
                case "sinvestorratelocktable":
                    SetInvestorRateLockTable(dataLoan, principal, collection, warningMessages);
                    break;
                case "spurchaseadvicefees":
                    SetPurchaseAdviceFees(dataLoan, principal, collection, warningMessages);
                    break;
                case "spurchaseadviceadjustments":
                    SetPurchaseAdviceAdjustments(dataLoan, principal, collection, warningMessages);
                    break;
                case "sdudwnpmtsrccollection":
                    SetDUDownPaymentSources(dataLoan, collection, isDeleteExisting);
                    break;
                case "sduthirdpartyproviders":
                    SetDuThirdPartyProviders(dataLoan, collection, warningMessages);
                    break;
                case "sclosingcostset":
                    SetClosingCostSet(dataLoan, BORROWERCLOSINGCOSTSET, principal, collection, warningMessages);
                    break;
                case "ssellerresponsibleclosingcostset":
                    SetClosingCostSet(dataLoan, SELLERCLOSINGCOSTSET, principal, collection, warningMessages);
                    break;
                case "sadjustmentlist":
                    SetAdjustmentList(dataLoan, principal, collection);
                    break;
                case "sprorationlist":
                    SetProrations(dataLoan, principal, collection);
                    break;
                case "shousingexpenses":
                    var housingExpenseImporter = new LoFormatHousingExpenseImporter(dataLoan, principal, collection, warningMessages);
                    housingExpenseImporter.SetHousingExpenses();
                    break;
                case "sselectedproductcodefilter":
                    string errors;
                    if (!SetSelectedProductCodeSet(principal, dataLoan, collection, warningMessages, out errors))
                    {
                        result.ResultStatus = LoFormatImporterResultStatus.Error;
                        result.ErrorMessages = errors;
                        return result;
                    }
                    break;
                case "sloanestimatedatesinfo":
                    string leErrors;
                    if (!SetLoanEstimateDates(dataLoan, collection, out leErrors))
                    {
                        result.ResultStatus = LoFormatImporterResultStatus.Error;
                        result.ErrorMessages = leErrors;
                        return result;
                    }
                    break;
                case "sclosingdisclosuredatesinfo":
                    string cdErrors;
                    if (!SetClosingDisclosureDates(dataLoan, collection, out cdErrors))
                    {
                        result.ResultStatus = LoFormatImporterResultStatus.Error;
                        result.ErrorMessages = cdErrors;
                        return result;
                    }
                    break;
                case "settlementserviceproviders":
                    SetSettlementServiceProviders(principal, dataLoan, collection, warningMessages);
                    break;
                case "shomeownercounselingorganizationcollection":
                    SetHomeownerCounselingOrgs(principal, dataLoan, collection, warningMessages);
                    break;
                case "strackingappraisalorders":
                    string appraisalErrors; 
                    if (!SetAppraisalOrderTracking(principal, dataLoan, collection, out appraisalErrors))
                    {
                        result.ResultStatus = LoFormatImporterResultStatus.Error;
                        result.ErrorMessages = appraisalErrors;
                        return result;
                    }
                    break;
                case "stasklist":
                    string taskErrors;
                    if (!SetTaskList(principal, dataLoan, collection, out taskErrors))
                    {
                        result.ResultStatus = LoFormatImporterResultStatus.Error;
                        result.ErrorMessages = taskErrors;
                        return result;
                    }
                    break;
                default:
                    warningMessages.AppendFormat("WARNING: {0} is invalid collection id. Skip.{1}", id, Environment.NewLine);
                    break;
            }

            return null;
        }

        private static void UpdateSubjectPropertyCountyOnModifiedZip(CPageData dataLoan)
        {
            if (string.IsNullOrEmpty(dataLoan.sSpCounty) && string.IsNullOrEmpty(dataLoan.sSpState))
            {
                dataLoan.sSpCounty = Tools.GetCountyFromZipCode(dataLoan.sSpZip);
            }
            else
            {
                HashSet<string> counties = new HashSet<string>(StateInfo.Instance.GetCountiesIn(dataLoan.sSpState), StringComparer.OrdinalIgnoreCase);
                if (!string.IsNullOrEmpty(dataLoan.sSpState) && !counties.Contains(dataLoan.sSpCounty))
                {
                    string newCounty = StateInfo.Instance.GetFirstCountyByStateAndZip(dataLoan.sSpState, dataLoan.sSpZip);
                    dataLoan.sSpCounty = newCounty;
                }
            }
        }

        private static void SetPmlField(CPageData dataLoan, string fieldId, string value, E_sProdCalcEntryT? prodCalcEntry = null)
        {
            var oldCalcMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            PageDataUtilities.SetValue(dataLoan, null, fieldId, value);
            dataLoan.CalcModeT = oldCalcMode;

            if (prodCalcEntry.HasValue)
            {
                dataLoan.sProdCalcEntryT = prodCalcEntry.Value;
            }
        }

        /// <summary>
        /// Parses creditXml for credit information inputs for DU Submission.
        /// </summary>
        /// <param name="creditXml">Credit xml string to be parsed.</param>
        /// <param name="useCreditReportOnFileForAllApplicants">Boolean indicating whether to fetch all credit information from existing credit in loan file.</param>
        /// <param name="useExistingCreditForApplicantList">Collection of applicants for which credit information will be created by existing credit in loan file.</param>
        /// <param name="creditInformationList">Collection of credit information for DU submission as specified in the credit xml.</param>
        public static void ParseCreditLOXmlForDUSeamless(string creditXml, out bool useCreditReportOnFileForAllApplicants, out IEnumerable<string> useExistingCreditForApplicantList, out IEnumerable<XisCreditInformation> creditInformationList)
        {
            useCreditReportOnFileForAllApplicants = false;
            useExistingCreditForApplicantList = new List<string>();
            creditInformationList = new List<XisCreditInformation>();

            XmlDocument doc = Tools.CreateXmlDoc(creditXml);

            XmlNode useCreditReportOnFileForAllApplicantsNode = doc.SelectSingleNode("//loan/credit[@useCreditReportOnFile]");
            if (useCreditReportOnFileForAllApplicantsNode != null)
            {
                bool.TryParse(useCreditReportOnFileForAllApplicantsNode.Attributes["useCreditReportOnFile"].Value
                    , out useCreditReportOnFileForAllApplicants);
                if (useCreditReportOnFileForAllApplicants == true)
                {
                    return;
                }
            }

            XmlNodeList applicants = doc.SelectNodes("//loan/applicant[@id]");
            foreach (XmlNode applicant in applicants)
            {
                string ssn = ((XmlElement)applicant).GetAttribute("id").TrimWhitespaceAndBOM();

                bool useCreditReportOnFile = false;
                string reportId = string.Empty;
                bool isJointCredit = false;

                XmlElement creditNode = applicant["credit"];
                if (creditNode != null)
                {
                    bool.TryParse(creditNode.GetAttribute("useCreditReportOnFile"), out useCreditReportOnFile);
                    reportId = creditNode.GetAttribute("reportId").TrimWhitespaceAndBOM();
                    bool.TryParse(creditNode.GetAttribute("isJointCredit"), out isJointCredit);
                }

                if (useCreditReportOnFile == true)
                {
                    ((List<string>)useExistingCreditForApplicantList).Add(ssn);
                }
                else
                {
                    E_XisCreditInformationRequestType creditRequestType = isJointCredit ? E_XisCreditInformationRequestType.Joint : E_XisCreditInformationRequestType.Individual;
                    XisCreditInformation creditInformation = new XisCreditInformation(ssn, reportId, creditRequestType);
                    ((List<XisCreditInformation>)creditInformationList).Add(creditInformation);
                }


            }
        }

        private static Guid GetEmployeeIdByLogin(string loginNm, string userType, Guid brokerId, E_RoleT roleT, StringBuilder warningMessages)
        {
            Guid employeeId = Guid.Empty;
            bool isActive = false;

            Role role = null;

            try
            {
                role = Role.Get(roleT);
            }
            catch (UnhandledEnumException)
            {
                warningMessages.AppendLine("Role " + roleT + " is invalid.");
                return employeeId;
            }

            SqlParameter[] parameters = {
                                           new SqlParameter("@LoginName", loginNm),
                                           new SqlParameter("@BrokerId", brokerId)
                                       };
            string storedProcedureName = "GetEmployeeIdByLoginName";
            if (userType.Equals("P", StringComparison.OrdinalIgnoreCase))
            {
                storedProcedureName = "RetrieveEmployeeIdByPmlLoginName";
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, storedProcedureName, parameters))
            {
                if (reader.Read())
                {
                    employeeId = (Guid)reader["EmployeeId"];
                    isActive = (bool)reader["IsActive"];
                }
            }

            if (employeeId == Guid.Empty)
            {
                warningMessages.AppendFormat("{0} reassign user name not found.{1}", role.ModifiableDesc, Environment.NewLine);
            }
            else if (isActive == false)
            {
                warningMessages.AppendFormat("Loan cannot be reassigned to an inactive {0}.{1}", role.ModifiableDesc, Environment.NewLine);
            }
            else
            {
                bool hasRole = false;
                parameters = new SqlParameter[] {
                    new SqlParameter("EmployeeId", employeeId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveEmployeeRole", parameters))
                {
                    while (reader.Read() && hasRole == false)
                    {
                        if ((string)reader["RoleDesc"] == role.Desc)
                        {
                            hasRole = true;
                        }
                    }
                }

                if (hasRole == false)
                {
                    employeeId = Guid.Empty;
                    warningMessages.AppendFormat("The user doesn't have the role to take the reassignment{0}", Environment.NewLine);
                }
            }

            return employeeId;
        }

        /// <summary>
        /// Gets the getter and setter for the role id using the passed in data loan.
        /// </summary>
        /// <returns>A tuple with the first item being the getter and the second item being the setter.</returns>
        public static Tuple<Func<Guid>, Action<Guid>> GetGetterSetterViaRole(E_RoleT role, CPageData dataLoan)
        {
            switch (role)
            {
                case E_RoleT.Pml_BrokerProcessor:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeBrokerProcessorId, (id) => dataLoan.sEmployeeBrokerProcessorId = id);
                case E_RoleT.CreditAuditor:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeCreditAuditorId, id => dataLoan.sEmployeeCreditAuditorId = id);
                case E_RoleT.Secondary:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeSecondaryId, id => dataLoan.sEmployeeSecondaryId = id);
                case E_RoleT.LoanOfficerAssistant:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeLoanOfficerAssistantId, id => dataLoan.sEmployeeLoanOfficerAssistantId = id);
                case E_RoleT.JuniorProcessor:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeJuniorProcessorId, id => dataLoan.sEmployeeJuniorProcessorId = id);
                case E_RoleT.JuniorUnderwriter:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeJuniorUnderwriterId, id => dataLoan.sEmployeeJuniorUnderwriterId = id);
                case E_RoleT.CollateralAgent:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeCollateralAgentId, id => dataLoan.sEmployeeCollateralAgentId = id);
                case E_RoleT.DisclosureDesk:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeDisclosureDeskId, id => dataLoan.sEmployeeDisclosureDeskId = id);
                case E_RoleT.LegalAuditor:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeLegalAuditorId, id => dataLoan.sEmployeeLegalAuditorId = id);
                case E_RoleT.PostCloser:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeePostCloserId, id => dataLoan.sEmployeePostCloserId = id);
                case E_RoleT.Pml_PostCloser:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeExternalPostCloserId, id => dataLoan.sEmployeeExternalPostCloserId = id);
                case E_RoleT.QCCompliance:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeQCComplianceId, id => dataLoan.sEmployeeQCComplianceId = id);
                case E_RoleT.Purchaser:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeePurchaserId, id => dataLoan.sEmployeePurchaserId = id);
                case E_RoleT.Closer:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeCloserId, id => dataLoan.sEmployeeCloserId = id);
                case E_RoleT.Servicing:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeServicingId, id => dataLoan.sEmployeeServicingId = id);
                case E_RoleT.Manager:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeManagerId, id => dataLoan.sEmployeeManagerId = id);
                case E_RoleT.RealEstateAgent:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeRealEstateAgentId, id => dataLoan.sEmployeeRealEstateAgentId = id);
                case E_RoleT.Underwriter:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeUnderwriterId, id => dataLoan.sEmployeeUnderwriterId = id);
                case E_RoleT.Processor:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeProcessorId, id => dataLoan.sEmployeeProcessorId = id);
                case E_RoleT.CallCenterAgent:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeCallCenterAgentId, id => dataLoan.sEmployeeCallCenterAgentId = id);
                case E_RoleT.LoanOpener:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeLoanOpenerId, id => dataLoan.sEmployeeLoanOpenerId = id);
                case E_RoleT.LenderAccountExecutive:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeLenderAccExecId, id => dataLoan.sEmployeeLenderAccExecId = id);
                case E_RoleT.Shipper:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeShipperId, id => dataLoan.sEmployeeShipperId = id);
                case E_RoleT.Funder:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeFunderId, id => dataLoan.sEmployeeFunderId = id);
                case E_RoleT.DocDrawer:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeDocDrawerId, id => dataLoan.sEmployeeDocDrawerId = id);
                case E_RoleT.Insuring:
                    return new Tuple<Func<Guid>, Action<Guid>>(() => dataLoan.sEmployeeInsuringId, id => dataLoan.sEmployeeInsuringId = id);
                default:
                    throw new UnhandledEnumException(role);
            }
        }

        /// <summary>
        /// Assigns the user to the specified role. This can add an official contact and will send out a notification that a role has changed.
        /// </summary>
        /// <param name="warnings">The string builder holding warning messages.</param>
        /// <param name="roleAssignElement">The xml element from the LOXml.</param>
        /// <param name="role">The role to assign to the user.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="dataLoan">The data loan to modify.</param>
        /// <param name="isQp2LoanFile">Whether the loan is a QP2 file.</param>
        private static void AssignRole(StringBuilder warnings, XmlElement roleAssignElement, E_RoleT role, AbstractUserPrincipal principal, CPageData dataLoan, bool isQp2LoanFile)
        {
            var getterSetter = GetGetterSetterViaRole(role, dataLoan);

            string loginName = roleAssignElement.InnerText;
            if (string.IsNullOrEmpty(loginName.TrimWhitespaceAndBOM()))
            {
                getterSetter.Item2(Guid.Empty);
                return;
            }

            bool sendNotification = true;
            string userType = string.Empty;

            if (string.Compare(roleAssignElement.GetAttribute("sendNotification"), "false", true) == 0 ||
                string.Compare(roleAssignElement.GetAttribute("sendnotification"), "false", true) == 0)
            {
                sendNotification = false;
            }

            userType = roleAssignElement.GetAttribute("usertype");
            if (userType.Equals(""))
            {
                userType = roleAssignElement.GetAttribute("userType");
            }

            if (userType.Equals(""))
            {
                userType = roleAssignElement.GetAttribute("UserType");
            }

            Role foundRole = Role.Get(role);
            string roleName = foundRole.ModifiableDesc;
            Guid roleId = foundRole.Id;
            E_AgentRoleT agentRole = Role.GetAgentRoleT(role);
            string expectedType = Role.LendingQBRoles.Any(lqbRole => lqbRole.Id == roleId) ? "b" : "p";

            if (string.IsNullOrEmpty(userType) || string.Compare(userType, expectedType, true) != 0)
            {
                warnings.AppendFormat("User is not applicable for the " + roleName + " role.");
                return;
            }

            Guid newId = GetEmployeeIdByLogin(loginName, userType, principal.BrokerId, role, warnings);
            if (newId != Guid.Empty)
            {
                Guid oldId = getterSetter.Item1();
                getterSetter.Item2(newId);

                var updateAgent = true;
                if (string.Compare(roleAssignElement.GetAttribute("createOfficialContact"), "false", ignoreCase: true) == 0 ||
                    string.Compare(roleAssignElement.GetAttribute("createofficialcontact"), "false", ignoreCase: true) == 0)
                {
                    updateAgent = false;
                }

                if (updateAgent)
                {
                    CAgentFields agent = dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.CreateNew);
                    agent.ClearExistingData();

                    bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(newId, dataLoan.sLId) == E_AgentRoleT.Other;
                    CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, newId, bCopyCommissionToAgent);
                    agent.Update();
                }

                MarkForAssignmentNotification(dataLoan.sLId, principal, oldId, newId, roleId, sendNotification, isQp2LoanFile);
            }
        }

        private static void MarkForAssignmentNotification(Guid sLId, AbstractUserPrincipal principal, Guid prevId, Guid newId, Guid roleId, bool sendAssignmentNotifcation, bool isQp2LoanFile)
        {
            if (sendAssignmentNotifcation == false || isQp2LoanFile)
            {
                return;
            }
            RoleChange.Mark(sLId, principal.BrokerId, prevId, newId, roleId, E_RoleChangeT.Update);
        }

        private static void PullMclInstantViewCreditReport(CPageData dataLoan, CAppData dataApp, string instantViewId, string reportId, StringBuilder warningMessages, AbstractUserPrincipal principal)
        {
            CreditRequestData requestData = CreditRequestData.Create(dataLoan.sLId, dataApp.aAppId);
            requestData.MclRequestType = MclRequestType.Get;
            requestData.InstantViewID = instantViewId;
            requestData.ReportID = reportId;

            var retryIntervalInSeconds = 5;
            var maxRetryCount = 20;
            var principalSubmittingCreditRequest = principal is ConsumerPortalUserPrincipal ? ((ConsumerPortalUserPrincipal)principal).GetImpersonatePrincipal() : principal;
            CreditReportRequestHandler requestHandler = new CreditReportRequestHandler(requestData, principalSubmittingCreditRequest, shouldReissueOnResponseNotReady: true, retryIntervalInSeconds: retryIntervalInSeconds, maxRetryCount: maxRetryCount, loanId: dataLoan.sLId);
            if (ConstStage.DisableCreditOrderingViaBJP)
            {
                // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                SubmitMclCreditReportSynchronously(requestHandler, maxRetryCount, warningMessages, dataLoan, dataApp, instantViewId);
                return;
            }
            var initialResult = requestHandler.SubmitToProcessor();
            if (initialResult.Status == CreditReportRequestResultStatus.Failure)
            {
                foreach (var error in initialResult.ErrorMessages)
                {
                    warningMessages.AppendLine(error);
                }

                return;
            }
            else
            {
                var publicJobId = initialResult.PublicJobId.Value;
                for (int attemptCount = 0; attemptCount < maxRetryCount; attemptCount++)
                {
                    var finalResult = CreditReportRequestHandler.CheckForRequestResults(publicJobId);
                    if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                    {
                        foreach (var error in finalResult.ErrorMessages)
                        {
                            warningMessages.AppendLine(error);
                        }

                        return;
                    }
                    else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                    {
                        ICreditReportResponse creditResponse = finalResult.CreditReportResponse;

                        if (null == creditResponse)
                        {
                            warningMessages.AppendFormat("WARNING: Unable to reissue credit report. Skip Credit Report.{0}", Environment.NewLine);
                            return;
                        }
                        if (creditResponse.HasError)
                        {
                            warningMessages.AppendFormat("WARNING: Unable to reissue credit report. {0}. Skip Credit Report.{1}", creditResponse.ErrorMessage, Environment.NewLine);
                            return;
                        }

                        SaveMclCreditReport(dataLoan, dataApp, creditResponse, instantViewId);
                        return;
                    }
                    else
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(retryIntervalInSeconds * 1000);
                    }
                }

                warningMessages.AppendFormat("WARNING: Unable to reissue credit report. Skip Credit Report.{0}", Environment.NewLine);
            }
        }

        private static void SubmitMclCreditReportSynchronously(CreditReportRequestHandler requestHandler, int maxRetryCount, StringBuilder warningMessages, CPageData dataLoan, CAppData dataApp, string instantViewId)
        {
            for (int attemptCount = 0; attemptCount < maxRetryCount; attemptCount++)
            {
                var finalResult = requestHandler.SubmitSynchronously();
                if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                {
                    foreach (var error in finalResult.ErrorMessages)
                    {
                        warningMessages.AppendLine(error);
                    }

                    return;
                }
                else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                {
                    ICreditReportResponse creditResponse = finalResult.CreditReportResponse;
                    if (null == creditResponse)
                    {
                        warningMessages.AppendFormat("WARNING: Unable to reissue credit report. Skip Credit Report.{0}", Environment.NewLine);
                        return;
                    }
                    if (creditResponse.HasError)
                    {
                        warningMessages.AppendFormat("WARNING: Unable to reissue credit report. {0}. Skip Credit Report.{1}", creditResponse.ErrorMessage, Environment.NewLine);
                        return;
                    }

                    if (creditResponse.IsReady)
                    {
                        SaveMclCreditReport(dataLoan, dataApp, creditResponse, instantViewId);
                        return;
                    }

                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5 * 1000);
                }
            }

            warningMessages.AppendFormat("WARNING: Unable to reissue credit report. Skip Credit Report.{0}", Environment.NewLine);
            return;
        }

        private static void SaveMclCreditReport(CPageData dataLoan, CAppData dataApp, ICreditReportResponse creditResponse, string instantViewId)
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;

            Guid comId = new Guid("74dcf4ff-fb64-49a9-8088-e0a7365185b1"); // Default: DataSource CRA. Will use this if we cannot locate MclCraCode, i.e: beta MCL.

            SqlParameter[] parameters = {
                                            new SqlParameter("@MclCraCode", instantViewId.Substring(0, 2))
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveComIdByMclCraCode", parameters))
            {
                if (reader.Read())
                {
                    comId = (Guid)reader["ComId"];
                }
            }

            CreditReportUtilities.SaveXmlCreditReport(creditResponse, principal, dataLoan.sLId, dataApp.aAppId, comId, Guid.Empty, E_CreditReportAuditSourceT.Reissue);

            dataApp = dataLoan.GetAppData(0);
            dataApp.ImportLiabilitiesFromCreditReport(true, true);
            dataApp.ImportCreditScoresFromCreditReport();
        }

        private static CAppData FindDataAppById(CPageData dataLoan, string id)
        {
            // Id must be aBSsn. If Id does not match with any aBSsn then use following algorithm to determine whether to use dataLoan.GetAppData(0) or
            // create new dataApp.
            // 1) If Id does not match with ssn.
            //     a) Does dataLoan.GetAppData(0).aBSsn non-empty.
            //          i) Yes - Use dataApp(0).
            //          ii) No - Create new dataApp.
            CAppData returnDataApp = null;
            int nApps = dataLoan.nApps;
            bool isFound = false;
            id = id.Replace("-", "");
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                if (id == dataApp.aBSsn.Replace("-", ""))
                {
                    isFound = true;
                    returnDataApp = dataApp;
                    break;
                }
            }

            if (!isFound)
            {
                if (dataLoan.GetAppData(0).aBSsn.TrimWhitespaceAndBOM() == "")
                {
                    returnDataApp = dataLoan.GetAppData(0);
                }
                else
                {
                    // Create new dataApp.
                    dataLoan.Save();
                    int index = dataLoan.AddNewApp();
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    returnDataApp = dataLoan.GetAppData(index);
                }
            }

            return returnDataApp;
        }

        private static void UpdateReoField(NameValueCollection nv, IRealEstateOwned field, StringBuilder warningMessages)
        {
            foreach (string key in nv.Keys)
            {
                string value = nv[key];
                switch (key.ToLower())
                {
                    case "addr":
                        field.Addr = value;
                        break;
                    case "city":
                        field.City = value;
                        break;
                    case "grossrenti":
                        field.GrossRentI_rep = value;
                        break;
                    case "hexp":
                        field.HExp_rep = value;
                        break;
                    case "issubjectprop":
                        field.IsSubjectProp = value.ToLower() == "true";
                        break;
                    case "mpmt":
                        field.MPmt_rep = value;
                        break;
                    case "mamt":
                        field.MAmt_rep = value;
                        break;
                    case "state":
                        field.State = value;
                        break;
                    case "statt":
                        switch (value.ToLower())
                        {
                            case "0": field.StatT = E_ReoStatusT.Residence; break;
                            case "1": field.StatT = E_ReoStatusT.Sale; break;
                            case "2": field.StatT = E_ReoStatusT.PendingSale; break;
                            case "3": field.StatT = E_ReoStatusT.Rental; break;
                            default:
                                InvalidEnumValue(key, value, warningMessages);
                                break;
                        }
                        break;
                    case "type":
                        if (IsReoTypeValid(value))
                        {
                            field.Type = value;
                        }
                        else
                        {
                            InvalidEnumValue(key, value, warningMessages);
                        }
                        break;
                    case "val":
                        field.Val_rep = value;
                        break;
                    case "zip":
                        field.Zip = value;
                        break;
                    case "occr":
                        field.OccR_rep = value;
                        break;
                    case "reownert":
                        switch (value)
                        {
                            case "0": field.ReOwnerT = E_ReOwnerT.Borrower; break;
                            case "1": field.ReOwnerT = E_ReOwnerT.CoBorrower; break;
                            case "2": field.ReOwnerT = E_ReOwnerT.Joint; break;
                            default:
                                InvalidEnumValue(key, value, warningMessages);
                                break;
                        }
                        break;
                    case "netrentilckd":
                        field.NetRentILckd = value.ToLower() == "true";
                        break;
                    case "netrenti":
                        field.NetRentI_rep = value;
                        break;
                    case "isforcecalcnetrentali":
                        field.IsForceCalcNetRentalI = value.ToLower() == "true";
                        break;
                    case "isprimaryresidence":
                        field.IsPrimaryResidence = value.ToLower() == "true";
                        break;
                    case "recordid":
                        break; // NOOP.
                    default:
                        warningMessages.AppendFormat("WARNING: {0} is invalid id for aReCollection. Skip.{1}", key, Environment.NewLine);
                        break;
                }
            }
        }

        /// <summary>
        /// Validates an REO type code passed in by the user to ensure that it maps to a valid enum value.
        /// </summary>
        /// <param name="type">The REO type code</param>
        /// <returns>A boolean indicating whether the code is valid.</returns>
        private static bool IsReoTypeValid(string type)
        {
            try
            {
                var mappedType = CalculatedFields.RealProperty.TypeFromCode(type);
                return true;
            }
            catch (CBaseException)
            {
                return false;
            }
        }

        private static void UpdateEmploymentRecord(NameValueCollection nv, IRegularEmploymentRecord field, StringBuilder warningMessages)
        {
            foreach (string key in nv.Keys)
            {
                string value = nv[key];
                switch (key.ToLower())
                {
                    case "emplrnm":
                        field.EmplrNm = value.ToLower();
                        break;
                    case "isselfemplmt":
                        field.IsSelfEmplmt = value.ToLower() == "true";
                        break;
                    case "iscurrent":
                        field.IsCurrent = value.ToLower() == "true";
                        break;
                    case "moni":
                        field.MonI_rep = value;
                        break;
                    case "emplmtstartd":
                        field.EmplmtStartD_rep = value;
                        break;
                    case "emplmtendd":
                        field.EmplmtEndD_rep = value;
                        break;
                    case "emplraddr":
                        field.EmplrAddr = value;
                        break;
                    case "emplrcity":
                        field.EmplrCity = value;
                        break;
                    case "emplrstate":
                        field.EmplrState = value;
                        break;
                    case "emplrzip":
                        field.EmplrZip = value;
                        break;
                    case "emplrbusphone":
                        field.EmplrBusPhone = value;
                        break;
                    case "emplrfax":
                        field.EmplrFax = value;
                        break;
                    case "jobtitle":
                        field.JobTitle = value;
                        break;
                    case "verifsentd":
                        field.VerifSentD_rep = value;
                        break;
                    case "verifreorderedd":
                        field.VerifReorderedD_rep = value;
                        break;
                    case "verifrecvd":
                        field.VerifRecvD_rep = value;
                        break;
                    case "verifexpd":
                        field.VerifExpD_rep = value;
                        break;
                    default:
                        warningMessages.AppendFormat("WARNING: {0} is invalid id for aEmpCollection. Skip.{1}", key, Environment.NewLine);
                        break;
                }
            }
        }

        private static void ImportEmploymentRecord(CAppData dataApp, AbstractUserPrincipal principal, XmlElement parentNode, bool isDeleteExisting, StringBuilder warningMessages, bool isBorrower)
        {
            List<NameValueCollection> employmentFromXmlList = new List<NameValueCollection>();
            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                NameValueCollection nv = new NameValueCollection();
                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    nv.Add(id, value);
                }
                employmentFromXmlList.Add(nv);
            }

            if (isDeleteExisting == false)
            {
                int count = isBorrower ? dataApp.aBEmpCollection.CountRegular : dataApp.aCEmpCollection.CountRegular;

                for (int i = 0; i < count; i++)
                {
                    IRegularEmploymentRecord field = isBorrower ? dataApp.aBEmpCollection.GetRegularRecordAt(i) : dataApp.aCEmpCollection.GetRegularRecordAt(i);
                    int matchIndex = -1;
                    for (int j = 0; j < employmentFromXmlList.Count; j++)
                    {
                        NameValueCollection nv = employmentFromXmlList[j];
                        string emplrAddr = nv["EmplrAddr"] == null ? "" : nv["EmplrAddr"];
                        if (!String.IsNullOrEmpty(emplrAddr) && emplrAddr.ToLower() == field.EmplrAddr.ToLower())
                        {
                            matchIndex = j;
                            UpdateEmploymentRecord(nv, field, warningMessages);
                            break;
                        }
                    }
                    if (matchIndex != -1)
                    {
                        employmentFromXmlList.RemoveAt(matchIndex);
                    }
                }
            }
            else
            {
                // Delete existing record.
                int count = isBorrower ? dataApp.aBEmpCollection.CountRegular : dataApp.aCEmpCollection.CountRegular;
                for (int i = 0; i < count; i++)
                {
                    IRegularEmploymentRecord field = isBorrower ? dataApp.aBEmpCollection.GetRegularRecordAt(i) : dataApp.aCEmpCollection.GetRegularRecordAt(i);
                    field.IsOnDeathRow = true;
                }
            }
            foreach (var nv in employmentFromXmlList)
            {
                IRegularEmploymentRecord field = isBorrower ? dataApp.aBEmpCollection.AddRegularRecord() : dataApp.aCEmpCollection.AddRegularRecord();
                UpdateEmploymentRecord(nv, field, warningMessages);
            }

            if (isBorrower)
                dataApp.aBEmpCollection.Flush();
            else
                dataApp.aCEmpCollection.Flush();
        }

        private static bool GetEmpRecOwner(NameValueCollection nv)
        {
            string owner = nv["OwnerT"];
            if (!String.IsNullOrEmpty(owner))
            {
                if (Convert.ToInt32(owner) == 0)
                    return true;
                else if (Convert.ToInt32(owner) == 1)
                    return false;
                else
                    throw new CBaseException(string.Format("{0} is an invalid value for OwnerT", owner),
                        string.Format("{0} is an invalid value for OwnerT", owner));
            }
            else
            {
                throw new CBaseException("OwnerT value missing", "OwnerT value missing");
            }
        }
        private static void ImportOtherIncome(CAppData dataApp, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            List<NameValueCollection> incomeFromXmlList = new List<NameValueCollection>();

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");

                NameValueCollection nv = new NameValueCollection();
                foreach (XmlElement el in fieldList)
                {

                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    nv.Add(id, value);
                }

                incomeFromXmlList.Add(nv);
            }

            // OPM 60794. Per PDE, other income is stomped with the incoming list.
            // We do not need to try to match and merge the lists for this feature.
            List<OtherIncome> otherIncome = new List<OtherIncome>();
            foreach (var nv in incomeFromXmlList)
            {
                OtherIncome newIncome = new OtherIncome();
                foreach (string key in nv.Keys)
                {
                    string value = nv[key];
                    switch (key.ToLower())
                    {
                        case "isforcoborrower": newIncome.IsForCoBorrower = value.ToLower() == "true"; break;
                        case "incomedesc": newIncome.Desc = value; break;
                        case "monthlyamt": newIncome.Amount = dataApp.m_convertLos.ToMoney(value); break;
                        default: warningMessages.AppendFormat("WARNING: {0} is invalid id for aOtherIncomeCollection. Skip.{1}", key, Environment.NewLine); break;
                    }
                }

                otherIncome.Add(newIncome);
            }

            if (!dataApp.LoanData.sIsIncomeCollectionEnabled)
            {
                dataApp.aOtherIncomeList = otherIncome;
                return;
            }

            // The following behavior is a shim so that setting the legacy application other income collection will still clear the existing entries and replace them
            if (!dataApp.aHasCoborrower && otherIncome.Any(o => o.IsForCoBorrower))
            {
                warningMessages.AppendLine("WARNING: aOtherIncomeCollection specified an income for a co-bororwer who does not exist, so the entire set is not being used. Skip.");
                return;
            }

            string errorMessage = IncomeCollectionMigration.GetErrorMessageForOtherIncome(otherIncome);
            if (errorMessage != null)
            {
                warningMessages.AppendLine("WARNING: " + errorMessage + " Skip.");
                return;
            }

            dataApp.ClearOtherIncomeSources();
            foreach (var ownerAndIncomeSource in IncomeCollectionMigration.GetMigratedOtherIncome(otherIncome, dataApp.aBConsumerId, dataApp.aCConsumerId))
            {
                dataApp.LoanData.AddIncomeSource(ownerAndIncomeSource.Item1, ownerAndIncomeSource.Item2);
            }
        }

        private static void ImportReos(CAppData dataApp, AbstractUserPrincipal principal, XmlElement parentNode, bool isDeleteExisting, StringBuilder warningMessages)
        {
            List<NameValueCollection> reoFromXmlList = new List<NameValueCollection>();

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                NameValueCollection nv = new NameValueCollection();


                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    nv.Add(id, value);
                }
                reoFromXmlList.Add(nv);

            }

            if (isDeleteExisting)
            {
                dataApp.aReCollection.ClearAll();
                dataApp.aReCollection.Flush();
            }
            int count = dataApp.aReCollection.CountRegular;

            for (int i = 0; i < count; i++)
            {
                var field = dataApp.aReCollection.GetRegularRecordAt(i);


                int matchIndex = -1;
                for (int j = 0; j < reoFromXmlList.Count; j++)
                {
                    NameValueCollection nv = reoFromXmlList[j];

                    string addr = nv["Addr"] == null ? "" : nv["Addr"];

                    string accNum = nv["AccNum"] == null ? "" : nv["AccNum"];

                    if (addr.ToLower() == field.Addr.ToLower())
                    {
                        matchIndex = j;
                        UpdateReoField(nv, field, warningMessages);
                        break;
                    }
                }

                if (matchIndex != -1)
                {
                    reoFromXmlList.RemoveAt(matchIndex);
                }

            }
            foreach (var nv in reoFromXmlList)
            {
                Guid recordId = Guid.Empty;

                if (string.IsNullOrEmpty(nv["RecordId"]) == false)
                {
                    try
                    {
                        recordId = new Guid(nv["RecordId"]);
                    }
                    catch (FormatException) { }
                }

                IRealEstateOwned field = null;

                if (recordId == Guid.Empty)
                {
                    field = dataApp.aReCollection.AddRegularRecord();
                }
                else
                {
                    // 6/26/2013 dd - In the consumer portal, we allowed a reo to create with a specific recordid.
                    field = dataApp.aReCollection.EnsureRegularRecordOf(recordId);
                }

                UpdateReoField(nv, field, warningMessages);
            }
            dataApp.aReCollection.Flush();

        }
        private static void ImportAssets(CAppData dataApp, AbstractUserPrincipal principal, XmlElement parentNode, bool isDeleteExisting, StringBuilder warningMessages)
        {
            List<NameValueCollection> assetFromXmlList = new List<NameValueCollection>();

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                NameValueCollection nv = new NameValueCollection();
                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    nv.Add(id, value);
                }
                assetFromXmlList.Add(nv);

            }

            if (isDeleteExisting)
            {
                dataApp.aAssetCollection.ClearAll();
                dataApp.aAssetCollection.Flush();
            }

            int count = dataApp.aAssetCollection.CountRegular;
            for (int i = 0; i < count; i++)
            {
                var field = dataApp.aAssetCollection.GetRegularRecordAt(i);


                int matchIndex = -1;
                for (int j = 0; j < assetFromXmlList.Count; j++)
                {
                    NameValueCollection nv = assetFromXmlList[j];

                    string comNm = nv["ComNm"] == null ? "" : nv["ComNm"];

                    string accNum = nv["AccNum"] == null ? "" : nv["AccNum"];

                    if (IsSameAsset(field, comNm, accNum))
                    {
                        matchIndex = j;
                        UpdateAssetField(nv, field, warningMessages);
                        break;
                    }
                }

                if (matchIndex != -1)
                {
                    assetFromXmlList.RemoveAt(matchIndex);
                }

            }
            foreach (var nv in assetFromXmlList)
            {
                var field = dataApp.aAssetCollection.AddRegularRecord();
                UpdateAssetField(nv, field, warningMessages);
            }
            dataApp.aAssetCollection.Flush();
        }
        private static bool IsSameAsset(IAssetRegular field, string comNm, string accNum)
        {
            // Definition: 2 tradeline is the same when at least one of these is true
            // 1) Both account numbers are the same and not empty string
            // 2) If account numbers are both empty string and creditor names are the same.
            if (accNum == field.AccNum.Value)
            {
                if ("" == accNum)
                {
                    string creditorNm1 = field.ComNm.TrimWhitespaceAndBOM().ToUpper();
                    string creditorNm2 = comNm.TrimWhitespaceAndBOM().ToUpper();
                    return creditorNm1 == creditorNm2;
                }

                return true;
            }
            return false;
        }
        private static void UpdateAssetField(NameValueCollection nv, IAssetRegular field, StringBuilder warningMessages)
        {
            foreach (string key in nv.Keys)
            {
                string value = nv[key];
                switch (key.ToLower())
                {
                    case "ownert":
                        switch (value)
                        {
                            case "0": field.OwnerT = E_AssetOwnerT.Borrower; break;
                            case "1": field.OwnerT = E_AssetOwnerT.CoBorrower; break;
                            case "2": field.OwnerT = E_AssetOwnerT.Joint; break;
                            default:
                                InvalidEnumValue(key, value, warningMessages);
                                break;
                        }
                        break;
                    case "assett":
                        switch (value)
                        {
                            case "0": field.AssetT = E_AssetRegularT.Auto; break;
                            case "1": field.AssetT = E_AssetRegularT.Bonds; break;
                            case "3": field.AssetT = E_AssetRegularT.Checking; break;
                            case "4":
                                {
                                    field.AssetT = E_AssetRegularT.GiftFunds;
                                    value = nv["GiftSource"];
                                    if (value != null)
                                    {
                                        field.GiftSource = (E_GiftFundSourceT)Enum.Parse(typeof(E_GiftFundSourceT), value, true);
                                    }
                                    break;
                                }
                            case "7": field.AssetT = E_AssetRegularT.Savings; break;
                            case "8": field.AssetT = E_AssetRegularT.Stocks; break;
                            case "9": field.AssetT = E_AssetRegularT.OtherIlliquidAsset; break;
                            case "11": field.AssetT = E_AssetRegularT.OtherLiquidAsset; break;
                            case "12": field.AssetT = E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets; break;
                            case "13": field.AssetT = E_AssetRegularT.GiftEquity; break;
                            case "14": field.AssetT = E_AssetRegularT.CertificateOfDeposit; break;
                            case "15": field.AssetT = E_AssetRegularT.MoneyMarketFund; break;
                            case "16": field.AssetT = E_AssetRegularT.MutualFunds; break;
                            case "17": field.AssetT = E_AssetRegularT.SecuredBorrowedFundsNotDeposit; break;
                            case "18": field.AssetT = E_AssetRegularT.BridgeLoanNotDeposited; break;
                            case "19": field.AssetT = E_AssetRegularT.TrustFunds; break;
                            default:
                                InvalidEnumValue(key, value, warningMessages);
                                break;
                        }
                        break;
                    case "attention": field.Attention = value; break;
                    case "departmentname": field.DepartmentName = value; break;
                    case "desc": field.Desc = value; break;
                    case "comnm": field.ComNm = value; break;
                    case "staddr": field.StAddr = value; break;
                    case "city": field.City = value; break;
                    case "state": field.State = value; break;
                    case "zip": field.Zip = value; break;
                    case "accnum": field.AccNum = value; break;
                    case "accnm": field.AccNm = value; break;
                    case "val": field.Val_rep = value; break;
                    case "verifsentd": field.VerifSentD_rep = value; break;
                    case "verifreorderedd": field.VerifReorderedD_rep = value; break;
                    case "verifrecvd": field.VerifRecvD_rep = value; break;
                    case "verifexpd": field.VerifExpD_rep = value; break;
                    case "giftsource": break; //// Skip, handled in assett "4" Gift Funds.
                    default:
                        warningMessages.AppendFormat("WARNING: {0} is invalid id for aAssetCollection. Skip.{1}", key, Environment.NewLine);
                        break;
                }
            }
        }
        private static void ImportLiabilities(CAppData dataApp, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            ArrayList liabilityFromXmlList = new ArrayList();

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                NameValueCollection nv = new NameValueCollection(15);
                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    nv.Add(id, value);

                }
                liabilityFromXmlList.Add(nv);
            }

            int count = dataApp.aLiaCollection.CountRegular;
            for (int i = 0; i < count; i++)
            {
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegularRecordAt(i);
                int matchIndex = -1;


                for (int j = 0; j < liabilityFromXmlList.Count; j++)
                {
                    NameValueCollection nv = (NameValueCollection)liabilityFromXmlList[j];
                    string comNm = nv["ComNm"] == null ? "" : nv["ComNm"];
                    string accNum = nv["AccNum"] == null ? "" : nv["AccNum"];

                    if (field.IsSameTradeline(accNum, comNm))
                    {
                        matchIndex = j;
                        UpdateLiaField(nv, field, warningMessages);
                        break;
                    }
                }
                if (matchIndex != -1)
                    liabilityFromXmlList.RemoveAt(matchIndex);

            }
            foreach (NameValueCollection nv in liabilityFromXmlList)
            {
                ILiabilityRegular field = dataApp.aLiaCollection.AddRegularRecord();
                UpdateLiaField(nv, field, warningMessages);
            }
            dataApp.aLiaCollection.Flush();
        }

        private static void UpdateLiaField(NameValueCollection nv, ILiabilityRegular field, StringBuilder warningMessages)
        {
            foreach (string key in nv.Keys)
            {
                string value = nv[key];
                switch (key.ToLower())
                {
                    case "ownert":
                        switch (value.ToLower())
                        {
                            case "0": field.OwnerT = E_LiaOwnerT.Borrower; break;
                            case "1": field.OwnerT = E_LiaOwnerT.CoBorrower; break;
                            case "2": field.OwnerT = E_LiaOwnerT.Joint; break;
                            default:
                                InvalidEnumValue("OwnerT", value, warningMessages);
                                break;
                        }
                        break;
                    case "debtt":
                        switch (value.ToLower())
                        {

                            case "1": field.DebtT = E_DebtRegularT.Installment; break;
                            case "3": field.DebtT = E_DebtRegularT.Mortgage; break;
                            case "4": field.DebtT = E_DebtRegularT.Open; break;
                            case "5": field.DebtT = E_DebtRegularT.Revolving; break;
                            case "6": field.DebtT = E_DebtRegularT.Other; break;
                            default:
                                InvalidEnumValue("DebtT", value, warningMessages);
                                break;
                        }
                        break;
                    case "comnm":
                        field.ComNm = value;
                        break;
                    case "comaddr":
                        field.ComAddr = value;
                        break;
                    case "comcity":
                        field.ComCity = value;
                        break;
                    case "comstate":
                        field.ComState = value;
                        break;
                    case "comzip":
                        field.ComZip = value;
                        break;
                    case "comphone":
                        field.ComPhone = value;
                        break;
                    case "comfax":
                        field.ComFax = value;
                        break;
                    case "desc":
                        field.Desc = value;
                        break;
                    case "accnm":
                        field.AccNm = value;
                        break;
                    case "accnum":
                        field.AccNum = value;
                        break;
                    case "bal":
                        field.Bal_rep = value;
                        break;
                    case "pmt":
                        field.Pmt_rep = value;
                        break;
                    case "remainmons":
                        field.RemainMons_rep = value;
                        break;
                    case "rate":
                        field.R_rep = value;
                        break;
                    case "origterm":
                        field.OrigTerm_rep = value;
                        break;
                    case "due":
                        field.Due_rep = value;
                        break;
                    case "late30":
                        field.Late30_rep = value;
                        break;
                    case "late60":
                        field.Late60_rep = value;
                        break;
                    case "late90plus":
                        field.Late90Plus_rep = value;
                        break;
                    case "matchedrerecordid":
                        try
                        {
                            field.MatchedReRecordId = new Guid(value);
                        }
                        catch (FormatException)
                        {
                            warningMessages.AppendFormat("WARNING: {0} is an invalid id for MatchedReRecordId", value);
                        }
                        break;
                    case "willbepdoff":
                        field.WillBePdOff = value.ToLower() == "true";
                        break;
                    case "usedinratio":
                        field.NotUsedInRatio = value.ToLower() != "true";
                        break;
                    case "ispiggyback":
                        field.IsPiggyBack = value.ToLower() == "true";
                        break;
                    case "incinreposession":
                        field.IncInReposession = value.ToLower() == "true";
                        break;
                    case "incinbankruptcy":
                        field.IncInBankruptcy = value.ToLower() == "true";
                        break;
                    case "incinforeclosure":
                        field.IncInForeclosure = value.ToLower() == "true";
                        break;
                    case "excfromunderwriting":
                        field.ExcFromUnderwriting = value.ToLower() == "true";
                        break;
                    case "verifsentd":
                        field.VerifSentD_rep = value;
                        break;
                    case "verifreorderedd":
                        field.VerifReorderedD_rep = value;
                        break;
                    case "verifrecvd":
                        field.VerifRecvD_rep = value;
                        break;
                    case "verifexpd":
                        field.VerifExpD_rep = value;
                        break;
                    case "issubjectproperty1stmortgage":
                        field.IsSubjectProperty1stMortgage = value.ToLower() == "true";
                        break;
                    case "issubjectpropertymortgage":
                        field.IsSubjectPropertyMortgage = value.ToLower() == "true";
                        break;
                    case "payoffamt":
                        field.PayoffAmt_rep = value;
                        break;
                    case "payoffamtlckd":
                        field.PayoffAmtLckd = value.ToLower() == "true";
                        break;
                    case "payofftiming":
                        switch (value)
                        {
                            case "0":
                                field.PayoffTiming = E_Timing.Blank;
                                break;
                            case "1":
                                field.PayoffTiming = E_Timing.Before_Closing;
                                break;
                            case "2":
                                field.PayoffTiming = E_Timing.At_Closing;
                                break;
                            case "3":
                                field.PayoffTiming = E_Timing.After_Closing;
                                break;
                            default:
                                InvalidEnumValue("PayoffTiming", value, warningMessages);
                                break;
                        }
                        break;
                    case "payofftiminglckd":
                        field.PayoffTimingLckd = value.ToLower() == "true";
                        break;
                    case "autoyearmake":
                        field.AutoYearMake = value;
                        break;
                    case "isforauto":
                        field.IsForAuto = value.ToLower() == "true";
                        break;
                    case "ismortfhainsured":
                        field.IsMortFHAInsured = value.ToLower() == "true";
                        break;
                    case "origdebtamt":
                        field.OrigDebtAmt_rep = value;
                        break;
                    default:
                        warningMessages.AppendFormat("WARNING: {0} is invalid id for aLiaCollection. Skip.{1}", key, Environment.NewLine);
                        break;
                }

            }
            field.Update();
        }
        private static void InvalidEnumValue(string fieldId, string value, StringBuilder warningMessages)
        {

            warningMessages.AppendFormat("WARNING: {0} is invalid value for {1}. Skip. {2}", value, fieldId, Environment.NewLine);
        }

        private static void AddAgents(CPageData dataLoan, XmlElement parentNode, StringBuilder warningMessages)
        {
            XmlNodeList recordList = parentNode.SelectNodes("record");
            HashSet<Guid> agentsToDelete = new HashSet<Guid>();
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                NameValueCollection nv = new NameValueCollection(20);

                string action = record.GetAttribute("action");
                if (!string.IsNullOrEmpty(action))
                {
                    if (action == "1" || action.Equals("delete", StringComparison.OrdinalIgnoreCase))
                    {
                        string idString = record.SelectSingleNode("field[@id='RecordId']")?.InnerText;
                        Guid recordId;
                        if (!Guid.TryParse(idString, out recordId))
                        {
                            warningMessages.AppendLine($"Invalid record id {idString}.");
                            return;
                        }

                        agentsToDelete.Add(recordId);
                        continue;
                    }
                    else
                    {
                        nv.Add("action", action);
                    }
                }

                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id").ToLower();
                    string value = el.InnerText;
                    nv.Add(id, value);
                }

                AddAgent(dataLoan, nv, warningMessages);
            }

            HashSet<Guid> agentsNotDeleted;
            if (!dataLoan.DeleteAgents(agentsToDelete, out agentsNotDeleted))
            {
                foreach (Guid id in agentsNotDeleted)
                {
                    warningMessages.AppendLine($"Unable to delete agent with RecordId {id.ToString()}");
                }

                return;
            }
        }
        private static void AddAgent(CPageData dataLoan, NameValueCollection nv, StringBuilder warningMessage)
        {
            try
            {
                E_AgentRoleT agentRoleT = E_AgentRoleT.Other;
                bool agentRoleIsValid = true;
                if (!Enum.TryParse(nv["agentrolet"], out agentRoleT) || !Enum.IsDefined(typeof(E_AgentRoleT), agentRoleT))
                {
                    agentRoleIsValid = false;
                }

                CAgentFields agent = null;
                bool canChangeRole = false;
                string action = nv["action"];
                if (!string.IsNullOrEmpty(action))
                {
                    string recordIdString = nv["recordid"];
                    Guid recordId;
                    if (string.IsNullOrEmpty(recordIdString))
                    {
                        if (!agentRoleIsValid)
                        {
                            InvalidEnumValue("AgentRoleT", nv["agentrolet"], warningMessage);
                            return;
                        }

                        // Make a new record and assign the agent role to it.
                        agent = dataLoan.GetAgentFields(-1);
                        agent.AgentRoleT = agentRoleT;
                    }
                    else if (Guid.TryParse(recordIdString, out recordId))
                    {
                        agent = dataLoan.GetAgentFields(recordId);
                        if (agent.IsNewRecord)
                        {
                            warningMessage.AppendLine($"Record with record id {recordIdString} not found.");
                            return;
                        }

                        canChangeRole = true;
                    }
                    else
                    {
                        // Not empty and could not be parsed. It's invalid.
                        warningMessage.AppendLine($"Invalid record id {recordIdString}.");
                        return;
                    }
                }
                else
                {
                    if (!agentRoleIsValid)
                    {
                        InvalidEnumValue("AgentRoleT", nv["agentrolet"], warningMessage);
                        return;
                    }

                    agent = dataLoan.GetAgentOfRole(agentRoleT, E_ReturnOptionIfNotExist.CreateNew);
                }

                foreach (string key in nv)
                {
                    string value = nv[key];
                    switch (key)
                    {
                        case "agentrolet":
                            if (canChangeRole)
                            {
                                if (agentRoleIsValid)
                                {
                                    agent.AgentRoleT = agentRoleT;
                                }
                                else
                                {
                                    InvalidEnumValue("AgentRoleT", key, warningMessage);
                                }
                            }
                            break;
                        case "agentname":
                            agent.AgentName = value;
                            break;
                        case "licensenumofagent":
                            agent.LicenseNumOfAgent = value;
                            break;
                        case "licensenumofcompany":
                            agent.LicenseNumOfCompany = value;
                            break;
                        case "companyname":
                            agent.CompanyName = value;
                            break;
                        case "streetaddr":
                            agent.StreetAddr = value;
                            break;
                        case "city":
                            agent.City = value;
                            break;
                        case "state":
                            agent.State = value;
                            break;
                        case "zip":
                            agent.Zip = value;
                            break;
                        case "county":
                            agent.County = value;
                            break;
                        case "phone":
                            agent.Phone = value;
                            break;
                        case "cellphone":
                            agent.CellPhone = value;
                            break;
                        case "pagernum":
                            agent.PagerNum = value;
                            break;
                        case "faxnum":
                            agent.FaxNum = value;
                            break;
                        case "emailaddr":
                            agent.EmailAddr = value;
                            break;
                        case "phoneofcompany":
                            agent.PhoneOfCompany = value;
                            break;
                        case "faxofcompany":
                            agent.FaxOfCompany = value;
                            break;
                        case "companyloanoriginatoridentifier":
                            agent.CompanyLoanOriginatorIdentifier = value;
                            break;
                        case "loanoriginatoridentifier":
                            agent.LoanOriginatorIdentifier = value;
                            break;
                        case "otheragentroletdesc":
                            agent.OtherAgentRoleTDesc = value;
                            break;
                        case "employeeidincompany":
                            agent.EmployeeIDInCompany = value;
                            break;
                        case "companyid":
                            agent.CompanyId = value;
                            break;
                        case "taxid":
                            TrySetTaxId(agent, value, warningMessage);
                            break;
                        case "chumsid":
                            agent.ChumsId = value;
                            break;
                        case "departmentname":
                            agent.DepartmentName = value;
                            break;
                        case "casenum":
                            agent.CaseNum = value;
                            break;
                        case "notes":
                            agent.Notes = value;
                            break;
                        case "commissionminbase":
                            agent.CommissionMinBase_rep = value;
                            break;
                        case "commissionpointofloanamount":
                            agent.CommissionPointOfLoanAmount_rep = value;
                            break;
                        case "commissionpointofgrossprofit":
                            agent.CommissionPointOfGrossProfit_rep = value;
                            break;
                        case "investorsolddate":
                            agent.InvestorSoldDate_rep = value;
                            break;
                        case "investorbasispoints":
                            agent.InvestorBasisPoints_rep = value;
                            break;
                        case "islistedingfeproviderform":
                            TrySetBoolProperty("IsListedInGFEProviderForm", v => agent.IsListedInGFEProviderForm = v, value, warningMessage);
                            break;
                        case "islenderassociation":
                            TrySetBoolProperty("IsLenderAssociation", v => agent.IsLenderAssociation = v, value, warningMessage);
                            break;
                        case "islenderaffiliate":
                            TrySetBoolProperty("IsLenderAffiliate", v => agent.IsLenderAffiliate = v, value, warningMessage);
                            break;
                        case "islenderrelative":
                            TrySetBoolProperty("IsLenderRelative", v => agent.IsLenderRelative = v, value, warningMessage);
                            break;
                        case "haslenderrelationship":
                            TrySetBoolProperty("HasLenderRelationship", v => agent.HasLenderRelationship = v, value, warningMessage);
                            break;
                        case "haslenderaccountlast12months":
                            TrySetBoolProperty("HasLenderAccountLast12Months", v => agent.HasLenderAccountLast12Months = v, value, warningMessage);
                            break;
                        case "isusedrepeatlybylenderlast12months":
                            TrySetBoolProperty("IsUsedRepeatlyByLenderLast12Months", v => agent.IsUsedRepeatlyByLenderLast12Months = v, value, warningMessage);
                            break;
                        case "provideritemnumber":
                            agent.ProviderItemNumber = value;
                            break;
                        case "isnotifywhenloanstatuschange":
                            TrySetBoolProperty("IsNotifyWhenLoanStatusChange", v => agent.IsNotifyWhenLoanStatusChange = v, value, warningMessage);
                            break;
                        case "branchname":
                            agent.BranchName = value;
                            break;
                        case "paytobankname":
                            agent.PayToBankName = value;
                            break;
                        case "paytobankcitystate":
                            agent.PayToBankCityState = value;
                            break;
                        case "paytoabanumber":
                            agent.PayToABANumber = value;
                            break;
                        case "paytoaccountnumber":
                            agent.PayToAccountNumber = value;
                            break;
                        case "paytoaccountname":
                            agent.PayToAccountName = value;
                            break;
                        case "furthercredittoaccountnumber":
                            agent.FurtherCreditToAccountNumber = value;
                            break;
                        case "furthercredittoaccountname":
                            agent.FurtherCreditToAccountName = value;
                            break;
                        case "islender":
                            TrySetBoolProperty("IsLender", v => agent.IsLender = v, value, warningMessage);
                            break;
                        case "isoriginator":
                            TrySetBoolProperty("IsOriginator", v => agent.IsOriginator = v, value, warningMessage);
                            break;
                        case "isoriginatoraffiliate":
                            TrySetBoolProperty("IsOriginatorAffiliate", v => agent.IsOriginatorAffiliate = v, value, warningMessage);
                            break;
                    }
                }

                agent.Update(updateAffiliateValuesFromLoan: false); // 07/2017 Per OPM 459142, we are assuming that the caller will specify intended values for the affiliate options
            }
            catch
            {
                warningMessage.Append("WARNING: Unable to update agent information for record.sAgentRoleT=" + nv["agentrolet"]).Append(Environment.NewLine);
            }
        }

        private static void TrySetTaxId(CAgentFields agent, string value, StringBuilder warningMessage)
        {
            try
            {
                agent.TaxId = value;
            }
            catch (FieldInvalidValueException)
            {
                warningMessage.AppendLine($"WARNING: Unable to update TaxId for sAgentRoleT= {agent.AgentRoleT}. "
                    + "The provided value is incorrectly formatted. Please update it a 9 digit number formatted as either 123456789 or 12-456789.");
            }
        }

        private static void SetPurchaseAdviceAdjustments(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowLockDeskWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Lock Desk write permission. SKIP modify purchase advice adjustment");
                return;

            }
            XmlNodeList recordList = parentNode.SelectNodes("record");

            var list = dataLoan.sPurchaseAdviceAdjustments;

            foreach (XmlElement record in recordList)
            {
                string adjDesc = string.Empty;
                string adjPc = string.Empty;
                string adjAmt = string.Empty;
                bool isSRP = false;
                int lastColumnUpdate = 0;

                XmlNodeList fieldList = record.SelectNodes("field");

                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;
                    if (fieldId.Equals("AdjDesc", StringComparison.OrdinalIgnoreCase))
                    {
                        adjDesc = value;
                    }
                    else if (fieldId.Equals("AdjPc", StringComparison.OrdinalIgnoreCase))
                    {
                        adjPc = value;
                        lastColumnUpdate = 2;
                    }
                    else if (fieldId.Equals("AdjAmt", StringComparison.OrdinalIgnoreCase))
                    {
                        adjAmt = value;
                        lastColumnUpdate = 1;
                    }
                    else if (fieldId.Equals("IsSRP", StringComparison.OrdinalIgnoreCase))
                    {
                        isSRP = value.Equals("True", StringComparison.OrdinalIgnoreCase);
                    }
                }

                list.Add(new CPurchaseAdviceAdjustmentsFields() { AdjDesc = adjDesc, AdjPc_rep = adjPc, IsSRP = isSRP, LastColumnUpdated = lastColumnUpdate, AdjAmt_rep = adjAmt, RowNum = list.Count });
            }

            dataLoan.sPurchaseAdviceAdjustments = list;
        }
        private static void SetPurchaseAdviceFees(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowAccountantWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Accountant write permission. SKIP modify Purchase Advice table");
                return;
            }
            XmlNodeList recordList = parentNode.SelectNodes("record");
            var list = dataLoan.sPurchaseAdviceFees;

            foreach (XmlElement record in recordList)
            {
                string feeDesc = string.Empty;
                string feeAmt = string.Empty;

                XmlNodeList fieldList = record.SelectNodes("field");

                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;
                    if (fieldId.Equals("FeeDesc", StringComparison.OrdinalIgnoreCase))
                    {
                        feeDesc = value;
                    }
                    else if (fieldId.Equals("FeeAmt", StringComparison.OrdinalIgnoreCase))
                    {
                        feeAmt = value;
                    }
                }
                list.Add(new CPurchaseAdviceFeesFields() { FeeDesc = feeDesc, FeeAmt_rep = feeAmt, RowNum = list.Count });
            }

            dataLoan.sPurchaseAdviceFees = list;
        }

        /// <summary>
        /// Imports down payment sources for DO/DU.
        /// </summary>
        /// <param name="dataLoan">The loan object to which down payment sources will be imported.</param>
        /// <param name="parentNode">The xml containing down payment information.</param>
        /// <param name="isDeleteExisting">Boolean indicating whether to overwrite existing down payment sources.</param>
        private static void SetDUDownPaymentSources(CPageData dataLoan, XmlElement parentNode, bool isDeleteExisting)
        {
            List<DownPaymentGift> downPaymentList = new List<DownPaymentGift>();
            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                DownPaymentGift downPayment = new DownPaymentGift();
                XmlNodeList fieldList = record.SelectNodes("field");
                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;
                    if (fieldId.Equals("Amount", StringComparison.OrdinalIgnoreCase))
                    {
                        downPayment.Amount = dataLoan.m_convertLos.ToMoney(value);
                        downPayment.Amount_rep = dataLoan.m_convertLos.ToMoneyString(downPayment.Amount, FormatDirection.ToRep);
                    }
                    else if (fieldId.Equals("Source", StringComparison.OrdinalIgnoreCase))
                    {
                        downPayment.Source = value;
                    }
                    else if (fieldId.Equals("Explanation", StringComparison.OrdinalIgnoreCase))
                    {
                        downPayment.Explanation = value;
                    }
                }

                downPaymentList.Add(downPayment);
            }

            DownPaymentGiftSet downPaymentSet = isDeleteExisting ? new DownPaymentGiftSet(dataLoan.m_convertLos, string.Empty) : dataLoan.sDUDwnPmtSrc;
            downPaymentSet.Import(downPaymentList);
            dataLoan.sDUDwnPmtSrc = downPaymentSet;
        }

        internal class CollectionProperties
        {
            public CollectionProperties(XmlElement record, string collectionName, Dictionary<string, string> permittedActionsToHelpText)
            {
                string action = record.GetAttribute("action");
                if (string.IsNullOrEmpty(action) || !permittedActionsToHelpText.ContainsKey(action))
                {
                    IEnumerable<string> validActionDescriptions = permittedActionsToHelpText.Select(pair => $"[{pair.Key}]" + (pair.Value == null ? null : $"({pair.Value})"));
                    throw new CBaseException(
                        collectionName + " cannot be saved without a valid action attribute value: " + string.Join(", ", validActionDescriptions),
                        "Invalid action specified: " + action);
                }

                this.ShouldDelete = (action == "1" || action.Equals("delete", StringComparison.OrdinalIgnoreCase));
                foreach (XmlElement field in record.SelectNodes("field"))
                {
                    if (string.Equals(field.GetAttribute("id"), "ID", StringComparison.OrdinalIgnoreCase))
                    {
                        Guid id = Guid.Empty;
                        string idString = field.InnerText;
                        if (!Guid.TryParse(idString, out id))
                        {
                            string errorMessage = $"Invalid {collectionName} id: {idString}";
                            throw new CBaseException(errorMessage, errorMessage);
                        }

                        this.Id = id;
                    }
                    else
                    {
                        string key = field.GetAttribute("id");
                        string value = field.InnerText;

                        this.AddProperty(key, value);
                    }
                }

                foreach (XmlElement collection in record.SelectNodes("collection"))
                {
                    var id = collection.GetAttribute("id");
                    if (string.IsNullOrWhiteSpace(id))
                    {
                        string errorMessage = $"Invalid {collectionName} sub-collection id: {id}";
                        throw new CBaseException(errorMessage, errorMessage);
                    }

                    var subcollectionProperties = LOFormatImporter.GatherCollectionProperties(collection, id);
                    this.Subcollections.Add(id, subcollectionProperties);
                }
            }

            public bool ShouldDelete { get; set; }

            public Guid? Id { get; set; }

            public Dictionary<string, string> Properties { get; } = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            public Dictionary<string, IEnumerable<CollectionProperties>> Subcollections { get; } = new Dictionary<string, IEnumerable<CollectionProperties>>(StringComparer.OrdinalIgnoreCase);

            private void AddProperty(string key, string value)
            {
                this.Properties.Add(key, value);
            }
        }

        /// <summary>
        /// Represents a single imported task.
        /// </summary>
        private class TaskProperties : CollectionProperties
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TaskProperties"/> class.
            /// </summary>
            /// <param name="record">The XML record holding task data.</param>
            /// <param name="collectionName">The collection name.</param>
            /// <param name="permittedActionsToHelpText">A dictionary of permitted actions mapped to their help text.</param>
            public TaskProperties(XmlElement record, string collectionName, Dictionary<string, string> permittedActionsToHelpText)
                : base(record, collectionName, permittedActionsToHelpText)
            {
                string taskIdKey = "TaskId";
                if (this.Properties.ContainsKey(taskIdKey))
                {
                    // The base CollectionProperties is hardcoded to expect Guid IDs, so this is
                    // a bit of a hack to allow similar usage of the string task IDs.
                    this.Id = this.Properties[taskIdKey];
                    this.Properties.Remove(taskIdKey);
                }

                XmlNodeList associatedDocs = record.SelectNodes("collection[@id='AssociatedDoc']/record");
                foreach (XmlNode associatedDoc in associatedDocs)
                {
                    var docIdNode = associatedDoc.SelectSingleNode("field[@id='AssociatedDocId']");

                    if (docIdNode == null)
                    {
                        string message = "An AssociatedDocId field must be passed in each record under the AssociatedDoc collection.";
                        throw new CBaseException(message, message);
                    }

                    this.AssociatedDocs.Add(docIdNode.InnerText);
                }

                XmlNodeList requiredDocTypes = record.SelectNodes("collection[@id='RequiredDoc']/record");
                if (requiredDocTypes.Count > 1)
                {
                    // Count check should be done on the raw XML so that even duplicate records can trigger it.
                    string message = "You are only allowed to set one document requirement.";
                    throw new CBaseException(message, message);
                }

                foreach (XmlNode requiredDocType in requiredDocTypes)
                {
                    var requiredDocTypeNode = requiredDocType.SelectSingleNode("field[@id='ReqDocTypeId']");

                    if (requiredDocTypeNode == null)
                    {
                        string message = "A ReqDocTypeId field must be passed in each record under the RequiredDoc collection.";
                        throw new CBaseException(message, message);
                    }

                    this.RequiredDocTypes.Add(requiredDocTypeNode.InnerText);
                }
            }

            /// <summary>
            /// The task ID.
            /// </summary>
            public new string Id { get; }

            /// <summary>
            /// A collection of required doctypes for associated docs.
            /// </summary>
            public List<string> RequiredDocTypes { get; } = new List<string>();

            /// <summary>
            /// A collection of associated documents.
            /// </summary>
            public List<string> AssociatedDocs { get; } = new List<string>();
        }

        internal static IEnumerable<CollectionProperties> GatherCollectionProperties(XmlElement parentNode, string collectionName, string[] permittedActions = null)
        {
            Dictionary<string, string> actionsToHelpText = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "add", null },
                { "edit", null },
                { "delete", null },
                { "0", "Adds/modifies depending on presence of ID" },
                { "1", "Delete" },
            };

            if (permittedActions != null)
            {
                foreach (string action in actionsToHelpText.Keys.Where(action => !permittedActions.Contains(action)).ToList()) // ToList means we finish enumerating before deletion - this is vital
                {
                    actionsToHelpText.Remove(action);
                }
            }

            List<CollectionProperties> collectionItems = new List<CollectionProperties>();
            foreach (XmlElement record in parentNode.SelectNodes("record"))
            {
                CollectionProperties xmlProps;
                if (parentNode.GetAttributeNode("id").Value.Equals("sTaskList", StringComparison.OrdinalIgnoreCase))
                {
                    xmlProps = new TaskProperties(record, collectionName, actionsToHelpText);
                }
                else
                {
                    xmlProps = new CollectionProperties(record, collectionName, actionsToHelpText);
                }

                collectionItems.Add(xmlProps);
            }

            return collectionItems;
        }

        private static bool SetAppraisalOrderTracking(AbstractUserPrincipal principal, CPageData dataLoan, XmlElement parentNode, out string error)
        {
            StringBuilder errorMessages = new StringBuilder();
            error = string.Empty;
            IEnumerable<CollectionProperties> appraisalProperties = GatherCollectionProperties(parentNode, "Appraisal Order Tracking", new string[] { "edit" });

            var loader = new AppraisalOrderViewLoader(dataLoan.sLId, dataLoan.sBrokerId, principal);
            Dictionary<Guid, AppraisalOrderView> availableAppraisals = loader.Retrieve().ToDictionary(view => view.AppraisalOrderId);
            List<AppraisalOrderView> ordersToSave = new List<AppraisalOrderView>();
            bool hasError = false;

            foreach (CollectionProperties xmlProperties in appraisalProperties)
            {
                AppraisalOrderView order;
                if (!xmlProperties.Id.HasValue || !availableAppraisals.TryGetValue(xmlProperties.Id.Value, out order))
                {
                    hasError = true;
                    errorMessages.AppendLine("Could not find appraisal order id.");
                    continue;
                }

                hasError = SetAppraisalOrderProperties(order, xmlProperties, errorMessages, dataLoan) || hasError;
                ordersToSave.Add(order);
            }

            if (hasError)
            {
                error = $"Invalid appraisal order xml: {Environment.NewLine} {errorMessages.ToString()}";
                return false;
            }

            var result = AppraisalOrderView.Save(ordersToSave, principal);
            if (result.HasError)
            {
                error = ErrorMessage.SystemError.ToString();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Modifies the task list based on the input XML.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <param name="dataLoan">The loan object.</param>
        /// <param name="collection">The input LOXml.</param>
        /// <param name="errorMessage">Any error messages occurring during the import process.</param>
        /// <returns>A boolean indicating whether the import was successful.</returns>
        private static bool SetTaskList(AbstractUserPrincipal principal, CPageData dataLoan, XmlElement collection, out string errorMessage)
        {
            try
            {
                if (!principal.BrokerDB.IsUseNewTaskSystem)
                {
                    errorMessage = "The LOXml collection sTaskList is only available under the new task system.";
                    return false;
                }

                errorMessage = string.Empty;
                var taskProperties = GatherCollectionProperties(collection, "Tasks");
                var existingTasks = Task.GetTasksByEmployeeAccess(principal.BrokerId, dataLoan.sLId, principal.UserId);

                if (!Task.CanUserViewHiddenInformation(principal))
                {
                    existingTasks = existingTasks.Where(t => !t.CondIsHidden).ToList();
                }

                var tasksToSave = new List<Task>();
                var conditionsToDelete = new List<Task>();

                EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                Lazy<Dictionary<Guid, EDocument>> readableEdocs = new Lazy<Dictionary<Guid, EDocument>>(() => repo.GetDocumentsByLoanId(dataLoan.sLId).ToDictionary((edoc) => edoc.DocumentId));
                Lazy<Dictionary<int, DocType>> readableDocTypes = new Lazy<Dictionary<int, DocType>>(() => EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, EnforcePermissions: true).ToDictionary((docType) => docType.Id));

                foreach (TaskProperties record in taskProperties)
                {
                    var task = existingTasks.FirstOrDefault(t => t.TaskId == record.Id);

                    if (record.ShouldDelete)
                    {
                        if (task == null)
                        {
                            errorMessage = "A condition was marked for deletion but did not contain an valid ID.";
                            return false;
                        }
                        else if (!task.TaskIsCondition)
                        {
                            errorMessage = $"Task {record.Id} cannot be deleted because it is not a condition.";
                            return false;
                        }
                        else
                        {
                            conditionsToDelete.Add(task);
                        }
                    }
                    else
                    {
                        bool isNew = string.IsNullOrEmpty(record.Id);
                        if (isNew)
                        {
                            task = new Task(dataLoan.sLId, principal.BrokerId);
                        }
                        else if (task == null)
                        {
                            errorMessage = $"Could not find task with ID {record.Id}.";
                            return false;
                        }

                        if (!SetTaskProperties(task, record, principal, readableEdocs, readableDocTypes, out errorMessage))
                        {
                            return false;
                        }

                        tasksToSave.Add(task);
                    }
                }

                foreach (var task in tasksToSave)
                {
                    try
                    {
                        task.Save(isUpdateLoanStatistics: false);
                    }
                    catch (CBaseException exc)
                    {
                        string taskDescriptor = string.Empty;
                        if (!task.IsNew)
                        {
                            taskDescriptor = $"Task {task.TaskId} - ";
                        }

                        errorMessage = taskDescriptor + exc.UserMessage;
                        return false;
                    }
                }

                foreach (var condition in conditionsToDelete)
                {
                    try
                    {
                        condition.Delete(principal.DisplayName, updateLoanStatistics: false);
                    }
                    catch (CBaseException exc)
                    {
                        string conditionDescriptor = $"Condition {condition.TaskId} - ";

                        errorMessage = conditionDescriptor + exc.UserMessage;
                        return false;
                    }
                    
                }
            }
            catch (CBaseException exc)
            {
                errorMessage = exc.UserMessage;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reads in an LOXml task record and applies it to a <see cref="Task"/>  object.
        /// </summary>
        /// <param name="task">The task object to update.</param>
        /// <param name="record">The LOXml record.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="readableEdocs">A collection of documents readable by the user.</param>
        /// <param name="readableDocTypes">A collection of doctypes.</param>
        /// <param name="errorMessage">Holds any error that occurs during the process.</param>
        /// <returns>A boolean indicating whether the import was successful.</returns>
        /// <remarks>
        /// <see cref="Dictionary{TKey, TValue}.ContainsKey()"/> uses a hash code rather than linear search to estimate
        /// the location of the data, so there's no major performance loss from not iterating over the properties. This allows
        /// us to continue to take advantage of the case insensitive dictionary keys.
        /// </remarks>
        private static bool SetTaskProperties(Task task, TaskProperties record, AbstractUserPrincipal principal, Lazy<Dictionary<Guid, EDocument>> readableEdocs, Lazy<Dictionary<int, DocType>> readableDocTypes, out string errorMessage)
        {
            var properties = record.Properties;

            // Tasks aren't given an ID until the CreateTask sproc is called, so we can't uniquely identify a new task.
            string taskDescriptor = task.IsNew ? "A newly created task" : $"Task {task.TaskId}";
            Func<string, string, string> generateErrorForProperty = (key, value) => $"{taskDescriptor} has an invalid value {value} for key {key}.";

            string isConditionKey = "TaskIsCondition";
            if (properties.ContainsKey(isConditionKey))
            {
                bool value;
                if (task.TaskIsCondition)
                {
                    // Users can freely change tasks into conditions, but not back.
                    errorMessage = $"{taskDescriptor} is a condition. The field {nameof(Task.TaskIsCondition)} cannot be changed for conditions.";
                    return false;
                }
                else if (bool.TryParse(properties[isConditionKey], out value))
                {
                    task.TaskIsCondition = value;
                }
                else
                {
                    errorMessage = generateErrorForProperty(isConditionKey, properties[isConditionKey]);
                    return false;
                }
                
                properties.Remove(isConditionKey);
            }

            string subjectKey = "TaskSubject";
            if (properties.ContainsKey(subjectKey))
            {
                task.TaskSubject = properties[subjectKey];
                properties.Remove(subjectKey);
            }
            else if (task.IsNew)
            {
                errorMessage = "New tasks and conditions must specify a TaskSubject.";
                return false;
            }

            string ownerUsernameKey = "TaskOwnerUsername";
            string ownerUserTypeKey = "TaskOwnerUserType";
            if (properties.ContainsKey(ownerUsernameKey) || properties.ContainsKey(ownerUserTypeKey))
            {
                if (!properties.ContainsKey(ownerUsernameKey))
                {
                    errorMessage = $"{taskDescriptor} indicates an owner user type but not a username. The username must also be included to set a different task owner.";
                    return false;
                }

                if (!properties.ContainsKey(ownerUserTypeKey))
                {
                    errorMessage = $"{taskDescriptor} contains an owner username but does not indicate a user type. The user type must also be included to set a different task owner.";
                    return false;
                }

                var ownerUserId = Tools.GetUserIdByUsername(properties[ownerUsernameKey], properties[ownerUserTypeKey], principal.BrokerId);

                if (ownerUserId == Guid.Empty)
                {
                    errorMessage = $"{taskDescriptor} attempted to set ownership to nonexistent user {properties[ownerUsernameKey]}.";
                    return false;
                }

                task.TaskOwnerUserId = ownerUserId;
                properties.Remove(ownerUsernameKey);
                properties.Remove(ownerUserTypeKey);
            }
            else if (task.IsNew)
            {
                // The creator is the owner by default.
                task.TaskOwnerUserId = principal.UserId;
            }

            string assignedToUserNameKey = "TaskAssignedUserUsername";
            string assignedToUserTypeKey = "TaskAssignedUserUserType";
            if (properties.ContainsKey(assignedToUserNameKey) || properties.ContainsKey(assignedToUserTypeKey))
            {
                if (!properties.ContainsKey(assignedToUserNameKey))
                {
                    errorMessage = $"{taskDescriptor} indicates an assigned user type but not a username. The username must also be included to assign the task to a different user.";
                    return false;
                }

                if (!properties.ContainsKey(assignedToUserTypeKey))
                {
                    errorMessage = $"{taskDescriptor} contains an assigned username but does not indicate a user type. The user type must also be included to assign the task to a different user.";
                    return false;
                }

                var assignedToUserId = Tools.GetUserIdByUsername(properties[assignedToUserNameKey], properties[assignedToUserTypeKey], principal.BrokerId);

                if (assignedToUserId == Guid.Empty)
                {
                    errorMessage = $"{taskDescriptor} attempted to set assigned user to nonexistent user {properties[assignedToUserNameKey]}.";
                    return false;
                }

                task.TaskAssignedUserId = assignedToUserId;
                properties.Remove(assignedToUserNameKey);
                properties.Remove(assignedToUserTypeKey);
            }
            else if (task.IsNew)
            {
                // Assign to self by default.
                task.TaskAssignedUserId = principal.UserId;
            }

            string statusKey = "TaskStatus";
            if (properties.ContainsKey(statusKey))
            {
                E_TaskStatus value;
                if (properties[statusKey].TryParseDefine<E_TaskStatus>(out value))
                {
                    task.TaskStatus = value;
                }
                else
                {
                    errorMessage = generateErrorForProperty(statusKey, properties[statusKey]);
                    return false;
                }
                
                properties.Remove(statusKey);
            }

            string dueDateKey = "TaskDueDate";
            bool dueDateSetManually = false;
            if (properties.ContainsKey(dueDateKey))
            {
                DateTime value;
                if (DateTime.TryParse(properties[dueDateKey], out value))
                {
                    task.TaskDueDate = value;
                    task.TaskDueDateLocked = true;
                    dueDateSetManually = true;
                }
                else
                {
                    errorMessage = generateErrorForProperty(dueDateKey, properties[dueDateKey]);
                    return false;
                }
                
                properties.Remove(dueDateKey);
            }
            else if (task.IsNew)
            {
                errorMessage = $"New tasks and conditions must set a due date.";
                return false;
            }

            string dueDateLockedKey = "TaskDueDateLocked";
            if (properties.ContainsKey(dueDateLockedKey))
            {
                if (!dueDateSetManually)
                {
                    bool value;
                    if (bool.TryParse(properties[dueDateLockedKey], out value))
                    {
                        task.TaskDueDateLocked = value;
                    }
                    else
                    {
                        errorMessage = generateErrorForProperty(dueDateLockedKey, properties[dueDateLockedKey]);
                        return false;
                    }
                }

                properties.Remove(dueDateLockedKey);
            }

            string followUpDateKey = "TaskFollowUpDate";
            if (properties.ContainsKey(followUpDateKey))
            {
                DateTime value;
                if (DateTime.TryParse(properties[followUpDateKey], out value))
                {
                    task.TaskFollowUpDate = value;
                }
                else
                {
                    errorMessage = generateErrorForProperty(followUpDateKey, properties[followUpDateKey]);
                    return false;
                }
                
                properties.Remove(followUpDateKey);
            }

            string permissionLevelIdKey = "TaskPermissionLevelId";
            if (properties.ContainsKey(permissionLevelIdKey))
            {
                int value;
                if (int.TryParse(properties[permissionLevelIdKey], out value))
                {
                    try
                    {
                        task.TaskPermissionLevelId = value;
                    }
                    catch (KeyNotFoundException)
                    {
                        errorMessage = $"The value {value} is not a valid task permission level ID.";
                        return false;
                    }
                }
                else
                {
                    errorMessage = generateErrorForProperty(permissionLevelIdKey, properties[permissionLevelIdKey]);
                    return false;
                }

                properties.Remove(permissionLevelIdKey);
            }
            else if (task.IsNew)
            {
                try
                {
                    task.TaskPermissionLevelId = Task.GetDefaultPermissionLevel(principal.BrokerId);
                }
                catch (CBaseException)
                {
                    errorMessage = "New tasks and conditions must set the TaskPermissionLevelId if no default is defined at the lender level.";
                    return false;
                }
            }

            string commentsKey = "Comments";
            if (properties.ContainsKey(commentsKey))
            {
                task.Comments = properties[commentsKey];
                properties.Remove(commentsKey);
            }

            if (properties.Keys.Any(k => k.StartsWith("Cond")) || record.RequiredDocTypes.Any() || record.AssociatedDocs.Any())
            {
                if (!task.TaskIsCondition)
                {
                    errorMessage = $"{taskDescriptor} cannot set condition fields unless {nameof(Task.TaskIsCondition)} is set to true";
                    return false;
                }

                string conditionIsHiddenKey = "CondIsHidden";
                if (properties.ContainsKey(conditionIsHiddenKey))
                {
                    bool value;
                    if (!Task.CanUserViewHiddenInformation(principal))
                    {
                        errorMessage = $"User does not have permission to modify hidden conditions (cannot edit {nameof(Task.CondIsHidden)}).";
                        return false;
                    }
                    else if (bool.TryParse(properties[conditionIsHiddenKey], out value))
                    {
                        task.CondIsHidden = value;
                    }
                    else
                    {
                        errorMessage = generateErrorForProperty(conditionIsHiddenKey, properties[conditionIsHiddenKey]);
                        return false;
                    }

                    properties.Remove(conditionIsHiddenKey);
                }

                string conditionCategoryIdKey = "CondCategoryId";
                string conditionCategoryKey = "CondCategory";
                var categories = new Lazy<IEnumerable<ConditionCategory>>(() => ConditionCategory.GetCategories(principal.BrokerId));
                if (properties.ContainsKey(conditionCategoryKey) && properties.ContainsKey(conditionCategoryIdKey))
                {
                    errorMessage = $"{taskDescriptor} cannot set both {nameof(Task.CondCategoryId)} and CondCategory. Please set only one or the other.";
                    return false;
                }
                else if (properties.ContainsKey(conditionCategoryKey))
                {
                    string categoryName = properties[conditionCategoryKey];
                    ConditionCategory category = categories.Value.FirstOrDefault(c => c.Category.Equals(categoryName, StringComparison.OrdinalIgnoreCase));

                    if (category == null)
                    {
                        if (!principal.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.HasValue)
                        {
                            errorMessage = $"{taskDescriptor} cannot generate new category {categoryName} because no default task permission level for imported categories is set at the lender level.";
                            return false;
                        }

                        category = new ConditionCategory(principal.BrokerId);
                        category.Category = categoryName;
                        category.DefaultTaskPermissionLevelId = principal.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.Value;
                        category.Save();
                    }

                    task.CondCategoryId = category.Id;
                    task.TaskPermissionLevelId = category.DefaultTaskPermissionLevelId;
                    properties.Remove(conditionCategoryKey);
                }
                else if (properties.ContainsKey(conditionCategoryIdKey))
                { 
                    int value;
                    if (string.IsNullOrEmpty(properties[conditionCategoryIdKey]))
                    {
                        task.CondCategoryId = null;
                    }
                    else if (int.TryParse(properties[conditionCategoryIdKey], out value))
                    {
                        var category = categories.Value.FirstOrDefault(c => c.Id == value);
                        if (category != null)
                        {
                            task.CondCategoryId = value;
                            task.TaskPermissionLevelId = category.DefaultTaskPermissionLevelId;
                        }
                        else
                        {
                            errorMessage = $"The category ID {value} is not a valid condition category ID.";
                            return false;
                        }
                    }
                    else
                    {
                        errorMessage = generateErrorForProperty(conditionCategoryIdKey, properties[conditionCategoryIdKey]);
                        return false;
                    }

                    properties.Remove(conditionCategoryIdKey);
                }
                else if (task.IsNew)
                {
                    errorMessage = "New conditions must set a condition category.";
                    return false;
                }

                string conditionInternalNotesKey = "CondInternalNotes";
                if (properties.ContainsKey(conditionInternalNotesKey))
                {
                    task.CondInternalNotes = properties[conditionInternalNotesKey];
                    properties.Remove(conditionInternalNotesKey);
                }
                
                if (record.RequiredDocTypes.Any())
                {
                    if (!SetRequiredDocTypeWithCondition(task, principal, record.RequiredDocTypes, readableDocTypes.Value, out errorMessage))
                    {
                        return false;
                    }
                }

                if (record.AssociatedDocs.Any())
                {
                    if (!AssociateDocumentsWithCondition(task, principal, record.AssociatedDocs, readableEdocs.Value, out errorMessage))
                    {
                        return false;
                    }
                }
            }

            // At this point, any remaining properties are not valid for import.
            if (properties.Any())
            {
                string propertyList = string.Join(", ", properties.Select(p => p.Key));
                errorMessage = $"The following fields are not valid for import to the task list: {propertyList}";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool SetDateOrError(string errorDesc, string newDate, StringBuilder warningMessages, Action<CDateTime> setter)
        {
            if (string.IsNullOrWhiteSpace(newDate))
            {
                setter(CDateTime.InvalidWrapValue);
                return true;
            }

            CDateTime dt = CDateTime.Create(newDate, null);

            if (dt == CDateTime.InvalidWrapValue || !dt.IsValid)
            {
                warningMessages.AppendLine($"Invalid value in {errorDesc}");
                return false;
            }

            setter(dt);
            return true;
        }

        /// <summary>
        /// Attempts to set the specified enum value.
        /// </summary>
        /// <typeparam name="T">The enum type.</typeparam>
        /// <param name="errorDesc">The value description, for reporting to be an invalid value.</param>
        /// <param name="newEnum">The enum's string value.  Whitespace will be replaced with "Blank".</param>
        /// <param name="warningMessages">The warning messages to append our error message to.</param>
        /// <param name="setAction">The action to actually set an enum value.</param>
        /// <returns>True if the enum is successfully parsed and set; false otherwise.</returns>
        private static bool SetEnumOrError<T>(string errorDesc, string newEnum, StringBuilder warningMessages, Action<T> setAction) where T: struct, IConvertible
        {
            if (string.IsNullOrWhiteSpace(newEnum))
            {
                newEnum = "Blank";
            }

            T enumValue;
            if (!Enum.TryParse(newEnum, out enumValue) || !Enum.IsDefined(typeof(T), enumValue))
            {
                warningMessages.AppendLine($"Invalid value in {errorDesc}");
                return false;
            }

            setAction(enumValue);
            return true;
        }

        private static bool SetAppraisalOrderProperties(AppraisalOrderView order, CollectionProperties properties, StringBuilder warningMessage, CPageData dataLoan)
        { 
            bool hasError = false;
            foreach (var property in properties.Properties)
            {
                switch (property.Key.ToLower())
                {
                    case "valuationmethod":
                        hasError = !SetEnumOrError<AppraisalValuationMethod>($"Appraisal Order {order.AppraisalOrderId} ValuationMethod", property.Value, warningMessage, p => order.ValuationMethod = p) || hasError;
                        break;
                    case "expirationdate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} ExpirationDate", property.Value, warningMessage, p => order.ExpirationDate = p) || hasError;
                        break;
                    case "receiveddate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} ReceivedDate", property.Value, warningMessage, p => order.ReceivedDate = p) || hasError;
                        break;
                    case "senttoborrowerdate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} SentToBorrowerDate", property.Value, warningMessage, p => order.SentToBorrowerDate = p) || hasError;
                        break;
                    case "deliverymethod":
                        hasError = !SetEnumOrError<AppraisalDeliveryMethod>($"Appraisal Order {order.AppraisalOrderId} DeliveryMethod", property.Value, warningMessage, p => order.DeliveryMethod = p) || hasError;
                        break;
                    case "borrowerreceiveddate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} BorrowerReceivedDate", property.Value, warningMessage, p => order.BorrowerReceivedDate = p) || hasError;
                        break;
                    case "revisiondate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} RevisionDate", property.Value, warningMessage, p => order.RevisionDate = p) || hasError;
                        break;
                    case "valuationeffectivedate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} ValuationEffectiveDate", property.Value, warningMessage, p => order.ValuationEffectiveDate = p) || hasError;
                        break;
                    case "appraisalformtype":
                        hasError = !SetEnumOrError<E_sSpAppraisalFormT>($"Appraisal Order {order.AppraisalOrderId} AppraisalFormType", property.Value, warningMessage, p => order.AppraisalFormType = p) || hasError;
                        break;
                    case "fhadocfileid":
                        order.FhaDocFileId = property.Value;
                        break;
                    case "ucdpappraisalid":
                        order.UcdpAppraisalId = property.Value;
                        break;
                    case "submittedtofhadate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} SubmittedToFhaDate", property.Value, warningMessage, p => order.SubmittedToFhaDate  = p) || hasError;
                        break;
                    case "submittedtoucdpdate":
                        hasError = !SetDateOrError($"Appraisal Order {order.AppraisalOrderId} SubmittedToUcdpDate", property.Value, warningMessage, p => order.SubmittedToUcdpDate = p) || hasError;
                        break;
                    case "curiskscore":
                        order.CuRiskScore_rep = property.Value;
                        break;
                    case "overvaluationriskt":
                        hasError = !SetEnumOrError<E_CuRiskT>($"Appraisal Order {order.AppraisalOrderId} OvervaluationRiskT", property.Value, warningMessage, p => order.OvervaluationRiskT = p) || hasError;
                        break;
                    case "propertyeligibilityriskt":
                        hasError = !SetEnumOrError<E_CuRiskT>($"Appraisal Order {order.AppraisalOrderId} PropertyEligibilityRiskT", property.Value, warningMessage, p => order.PropertyEligibilityRiskT = p) || hasError;
                        break;
                    case "appraisalqualityriskt":
                        hasError = !SetEnumOrError<E_CuRiskT>($"Appraisal Order {order.AppraisalOrderId} AppraisalQualityRiskT", property.Value, warningMessage, p => order.AppraisalQualityRiskT = p) || hasError;
                        break;
                    case "notes":
                        order.Notes = property.Value;
                        break;
                    case "reoid":
                        if (order.AppraisalOrderType != AppraisalOrderType.Manual)
                        {
                            warningMessage.AppendLine($"Unable to set field reoid for Appraisal Order {order.AppraisalOrderId} because it is not a Manual order.");
                            hasError = true;
                            break;
                        }

                        if (string.IsNullOrEmpty(property.Value))
                        {
                            var primaryApp = dataLoan.GetAppData(0);
                            order.SetSubjectProperty(primaryApp.aAppId, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
                            break;
                        }

                        Guid? reoId = property.Value.ToNullable<Guid>(Guid.TryParse);
                        if(!reoId.HasValue)
                        {
                            warningMessage.AppendLine($"Appraisal Order {order.AppraisalOrderId} record has invalid field {property.Key} with value {property.Value}");
                            hasError = true;
                            break;
                        }

                        IRealEstateOwned linkedReo = null;
                        for (var i = 0; i < dataLoan.nApps; i++)
                        {
                            var app = dataLoan.GetAppData(i);
                            linkedReo = app.aReCollection.GetRecordOf(reoId.Value) as IRealEstateOwned;
                            if (linkedReo != null)
                            {
                                order.SetReo(linkedReo, app.aAppId);
                                break;
                            }
                        }

                        if (linkedReo == null)
                        {
                            warningMessage.AppendLine($"Unable to find REO with id {property.Value}.");
                            hasError = true;
                            break;
                        }

                        break;
                    default:
                        warningMessage.AppendLine($"Appraisal Order {order.AppraisalOrderId} record has invalid field {property.Key} with value {property.Value}");
                        hasError = true;
                        break;
                }
            }

            return hasError;
        }

        private static void SetHomeownerCounselingOrgs(AbstractUserPrincipal principal, CPageData dataLoan, XmlElement parentNode, StringBuilder warningMessages)
        {
            IEnumerable<CollectionProperties> hcXmlProperties = GatherCollectionProperties(parentNode, "Homeowner Counseling Organizations");
            var currentHcCollection = dataLoan.sHomeownerCounselingOrganizationCollection;

            foreach (CollectionProperties xmlProperty in hcXmlProperties)
            {
                if (xmlProperty.ShouldDelete)
                {
                    if (!xmlProperty.Id.HasValue || xmlProperty.Id.Value == Guid.Empty)
                    {
                        warningMessages.AppendLine("No id found for Homeowner Counseling Organization marked for deletion.");
                        return;
                    }

                    if (!currentHcCollection.RemoveById(xmlProperty.Id.Value))
                    {
                        warningMessages.AppendLine($"No Homeowner Counseling Organization found with id {xmlProperty.Id.Value.ToString()}.");
                        return;
                    }
                }
                else
                {
                    DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization hcToModify = null;
                    if (!xmlProperty.Id.HasValue || xmlProperty.Id.Value == Guid.Empty)
                    {
                        if (currentHcCollection.Count >= 10)
                        {
                            warningMessages.AppendLine("Number of Homeowner Counseling Organization cannot exceed 10.");
                            return;
                        }

                        hcToModify = new DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization(shouldSetNewId: true);
                        currentHcCollection.Add(hcToModify);
                    }
                    else
                    {
                        hcToModify = currentHcCollection.RetrieveById(xmlProperty.Id.Value);
                        if (hcToModify == null)
                        {
                            warningMessages.AppendLine($"No Homeowner Counseling Organization found with id {xmlProperty.Id.Value.ToString()}.");
                            return;
                        }
                    }

                    if (!SetHomeownerCounselingOrgProperties(hcToModify, xmlProperty, warningMessages))
                    {
                        return;
                    }
                }
            }

            dataLoan.sHomeownerCounselingOrganizationCollection = currentHcCollection;
        }

        private static bool SetHomeownerCounselingOrgProperties(DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization hcToModify, CollectionProperties properties, StringBuilder warningMessage)
        {
            string invalidValueError = "Invalid value in {0} Field: {1}.";
            bool hasError = false;
            foreach (var property in properties.Properties)
            {
                if (property.Key.Equals("Name", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Name = property.Value;
                }
                else if (property.Key.Equals("StreetAddress", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Address.StreetAddress = property.Value;
                }
                else if (property.Key.Equals("StreetAddress2", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Address.StreetAddress2 = property.Value;
                }
                else if (property.Key.Equals("City", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Address.City = property.Value;
                }
                else if (property.Key.Equals("State", StringComparison.OrdinalIgnoreCase))
                {
                    string safeState = Tools.SafeStateCode(property.Value);
                    if (string.IsNullOrEmpty(safeState))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    hcToModify.Address.State = safeState;
                }
                else if (property.Key.Equals("Zipcode", StringComparison.OrdinalIgnoreCase))
                {
                    Zipcode? validatedZipcode = Zipcode.CreateWithValidation(property.Value);
                    if (!validatedZipcode.HasValue)
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    hcToModify.Address.Zipcode = validatedZipcode.Value.ToString();
                }
                else if (property.Key.Equals("Phone", StringComparison.OrdinalIgnoreCase))
                {
                    string formattedPhone;
                    if (!Tools.ToPhoneFormat(property.Value, out formattedPhone))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    hcToModify.Phone = formattedPhone;
                }
                else if (property.Key.Equals("Email", StringComparison.OrdinalIgnoreCase))
                {
                    EmailAddress? email = EmailAddress.Create(property.Value);
                    if (!email.HasValue)
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    hcToModify.Email = email.Value.ToString();
                }
                else if (property.Key.Equals("Website", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Website = property.Value;
                }
                else if (property.Key.Equals("Services", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Services = property.Value;
                }
                else if (property.Key.Equals("Languages", StringComparison.OrdinalIgnoreCase))
                {
                    hcToModify.Languages = property.Value;
                }
                else if (property.Key.Equals("Distance", StringComparison.OrdinalIgnoreCase))
                {
                    int distance;
                    if (!int.TryParse(property.Value, out distance) || distance < 0)
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    hcToModify.Distance = distance.ToString();
                }
                else
                {
                    warningMessage.AppendLine($"Unable to modify field {property.Key}.");
                    hasError = true;
                    continue;
                }
            }

            return !hasError;
        }

        private static void SetDuThirdPartyProviders(CPageData loanData, XmlElement collectionElement, StringBuilder warningMessages)
        {
            var thirdPartyProviders = loanData.sDuThirdPartyProviders;
            foreach (CollectionProperties recordItem in GatherCollectionProperties(collectionElement, "DU Third Party Providers", permittedActions: new[] { "add", "delete" }))
            {
                string serviceProviderName = recordItem.Properties.GetValueOrNull("FNMAServiceProviderName");
                string referenceNumber = recordItem.Properties.GetValueOrNull("ReferenceNumber");
                var serviceProvider = DataAccess.FannieMae.DuServiceProviders.RetrieveByProviderName(serviceProviderName);
                if (!serviceProvider.HasValue)
                {
                    warningMessages.AppendLine($"{nameof(CPageData.sDuThirdPartyProviders)}: Unable to recognize FNMAServiceProviderName '{serviceProviderName}'; entry will be skipped.");
                    continue;
                }
                else if (string.IsNullOrEmpty(referenceNumber))
                {
                    warningMessages.AppendLine($"{nameof(CPageData.sDuThirdPartyProviders)}: Unable to save empty reference number for provider with FNMAServiceProviderName '{serviceProviderName}'; entry will be skipped.");
                    continue;
                }

                var provider = new DataAccess.FannieMae.FannieMaeThirdPartyProvider(serviceProvider.Value, referenceNumber);
                if (recordItem.ShouldDelete)
                {
                    thirdPartyProviders.RemoveProvider(provider);
                }
                else
                {
                    thirdPartyProviders.AddProviders(new[] { provider });
                }
            }
        }

        private static void SetSettlementServiceProviders(AbstractUserPrincipal principal, CPageData dataLoan, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (dataLoan.sIsLegacyClosingCostVersion)
            {
                warningMessages.AppendLine("Unable to modify legacy settlement service providers.");
                return;
            }

            IEnumerable<CollectionProperties> sspXmlProperties = GatherCollectionProperties(parentNode, "Available Settlement Service Providers");
            AvailableSettlementServiceProviders currentAvailSSPs = dataLoan.sAvailableSettlementServiceProviders;

            Dictionary<Guid, CAgentFields> agentIdToEntry = new Dictionary<Guid, CAgentFields>();
            foreach (CollectionProperties xmlProperty in sspXmlProperties)
            {
                if (xmlProperty.ShouldDelete)
                {
                    if (!xmlProperty.Id.HasValue || xmlProperty.Id.Value == Guid.Empty)
                    {
                        warningMessages.AppendLine("No id found for Settlement Service Provider marked for deletion.");
                        return;
                    }

                    if (!currentAvailSSPs.DeleteProvider(xmlProperty.Id.Value))
                    {
                        warningMessages.AppendLine($"Could not find Settlement Service Provider with ID {xmlProperty.Id.Value.ToString()}.");
                        return;
                    }
                }
                else
                {
                    SettlementServiceProvider providerToModify = null;
                    bool isNew = false;
                    Guid feeTypeId = Guid.Empty;
                    if (!xmlProperty.Id.HasValue || xmlProperty.Id.Value == Guid.Empty)
                    {
                        providerToModify = new SettlementServiceProvider(shouldMakeNewId: true);

                        // Only new SSPs need a fee type id. And need to do it here anyways since FeeTypeId technically isn't an SSP property but a key used in the dictionary.
                        if (!xmlProperty.Properties.ContainsKey("FeeTypeId") || !Guid.TryParse(xmlProperty.Properties["FeeTypeId"], out feeTypeId))
                        {
                            warningMessages.AppendLine($"New Settlement Service Provider records must contain a valid FeeTypeId.");
                            return;
                        }

                        if (!currentAvailSSPs.IsFeeTypeAvailableInProviderList(feeTypeId))
                        {
                            warningMessages.AppendLine($"New Settlement Service Provider record specifies an invalid fee type id. FeeTypeId: {feeTypeId.ToString()}.");
                            return;
                        }

                        xmlProperty.Properties.Remove("FeeTypeId");
                        isNew = true;
                    }
                    else
                    {
                        providerToModify = currentAvailSSPs.RetrieveProvider(xmlProperty.Id.Value);
                        if (providerToModify == null)
                        {
                            warningMessages.AppendLine($"Could not find Settlement Service Provider with ID {xmlProperty.Id.Value.ToString()}.");
                            return;
                        }

                        isNew = false;
                    }

                    CAgentFields agent = null;
                    if (xmlProperty.Properties.ContainsKey("RecordId"))
                    {
                        Guid agentId;
                        if (!Guid.TryParse(xmlProperty.Properties["RecordId"], out agentId))
                        {
                            warningMessages.AppendLine($"Invalid value {xmlProperty.Properties["RecordId"]} in Field: RecordId.");
                            return;
                        }

                        if (!agentIdToEntry.TryGetValue(agentId, out agent))
                        {
                            agent = dataLoan.GetAgentFields(agentId);
                            agentIdToEntry.Add(agentId, agent);
                        }

                        xmlProperty.Properties.Remove("RecordId");
                    }

                    bool success = SetSettlementServiceProviderProperties(agent, providerToModify, xmlProperty, warningMessages);
                    if (!success)
                    {
                        return;
                    }

                    if (success && isNew)
                    {
                        currentAvailSSPs.AddProviderForFeeType(feeTypeId, providerToModify);
                    }
                }
            }

            string validationError;
            if (currentAvailSSPs.ValidateSSPs(out validationError))
            {
                dataLoan.SetFeeTypesAndECAforAvailableSSPs(currentAvailSSPs);
                dataLoan.sAvailableSettlementServiceProviders = currentAvailSSPs;
            }
            else
            {
                warningMessages.AppendLine(validationError);
            }
        }

        private static bool SetSettlementServiceProviderProperties(CAgentFields chosenAgent, SettlementServiceProvider ssp, CollectionProperties properties, StringBuilder warningMessage)
        {
            if (chosenAgent != null)
            {
                ssp.CompanyName = chosenAgent.CompanyName;
                ssp.StreetAddress = chosenAgent.StreetAddr;
                ssp.City = chosenAgent.City;
                ssp.State = chosenAgent.State;
                ssp.Zip = chosenAgent.Zip;
                ssp.ContactName = chosenAgent.AgentName;
                ssp.Email = chosenAgent.EmailAddr;
                ssp.Phone = chosenAgent.Phone;
                ssp.Fax = chosenAgent.FaxNum;
                ssp.AgentId = chosenAgent.RecordId;
                ssp.ServiceT = chosenAgent.AgentRoleT;
            }

            string invalidValueError = "Invalid value in {0} Field: {1}.";
            bool hasError = false;
            foreach (var property in properties.Properties)
            {
                if(property.Key.Equals("ServiceT", StringComparison.OrdinalIgnoreCase))
                {
                    E_AgentRoleT serviceType;
                    if (!Enum.TryParse(property.Value, out serviceType) || !Enum.IsDefined(typeof(E_AgentRoleT), serviceType))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.ServiceT = serviceType;
                }
                else if (property.Key.Equals("CompanyName", StringComparison.OrdinalIgnoreCase))
                {
                    ssp.CompanyName = property.Value;
                }
                else if (property.Key.Equals("StreetAddress", StringComparison.OrdinalIgnoreCase))
                {
                    ssp.StreetAddress = property.Value;
                }
                else if (property.Key.Equals("City", StringComparison.OrdinalIgnoreCase))
                {
                    ssp.City = property.Value;
                }
                else if (property.Key.Equals("State", StringComparison.OrdinalIgnoreCase))
                {
                    string safeState = Tools.SafeStateCode(property.Value);
                    if (string.IsNullOrEmpty(safeState))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.State = safeState;
                }
                else if (property.Key.Equals("Zip", StringComparison.OrdinalIgnoreCase))
                {
                    Zipcode? validatedZipcode = Zipcode.CreateWithValidation(property.Value);
                    if (!validatedZipcode.HasValue)
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.Zip = validatedZipcode.ToString();
                }
                else if (property.Key.Equals("ContactName"))
                {
                    ssp.ContactName = property.Value;
                }
                else if (property.Key.Equals("Phone"))
                {
                    string formattedPhone;
                    if (!Tools.ToPhoneFormat(property.Value, out formattedPhone))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.Phone = formattedPhone;
                }
                else if (property.Key.Equals("Fax"))
                {
                    string formattedPhone;
                    if (!Tools.ToPhoneFormat(property.Value, out formattedPhone))
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.Fax = formattedPhone;
                }
                else if (property.Key.Equals("Email", StringComparison.OrdinalIgnoreCase))
                {
                    EmailAddress? email = EmailAddress.Create(property.Value);
                    if (!email.HasValue)
                    {
                        warningMessage.AppendLine(string.Format(invalidValueError, property.Key, property.Value));
                        hasError = true;
                        continue;
                    }

                    ssp.Email = email.Value.ToString();
                }
                else
                {
                    warningMessage.AppendLine($"Unable to modify field {property.Key}.");
                    hasError = true;
                    continue;
                }
            }

            return !hasError;
        }

        private static XElement CreateProductCodeLoXml(AbstractUserPrincipal principal, BrokerDB brokerDb, Dictionary<string, bool> productCodes, string id)
        {
            if (brokerDb.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                XElement collectionElement = new XElement("collection", new XAttribute("id", id));

                for (int i = 0; i < productCodes.Count; i++)
                {
                    var pair = productCodes.ElementAt(i);
                    XElement recordElement = new XElement("record", new XAttribute("index", i + 1));
                    collectionElement.Add(recordElement);

                    XElement productNameElement = new XElement("field", new XAttribute("id", "ProductCodeName"), pair.Key);
                    recordElement.Add(productNameElement);

                    XElement visiblityElement = new XElement("field", new XAttribute("id", "IsVisible"), pair.Value);
                    recordElement.Add(visiblityElement);
                }

                return collectionElement;
            }

            return null;
        }

        private static bool SetSelectedProductCodeSet(AbstractUserPrincipal principal, CPageData dataLoan, XmlElement parentNode, StringBuilder warningMessages, out string errorMessage)
        {
            errorMessage = string.Empty;
            if (!dataLoan.BrokerDB.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                warningMessages.AppendLine("Advanced filtering options are not enabled.");
                return true;
            }
            else
            {
                Dictionary<string, bool> selectedProductCodes = new Dictionary<string, bool>();
                foreach (XmlElement field in parentNode.SelectNodes("record/field"))
                {
                    if (field.GetAttribute("id").Equals("ProductCodeName", StringComparison.OrdinalIgnoreCase))
                    {
                        string productCode = field.InnerText;
                        if (!selectedProductCodes.ContainsKey(productCode))
                        {
                            // The visiblity bool doesn't really matter since that gets recalculated anyways.
                            selectedProductCodes.Add(productCode.ToUpper(), true);
                        }
                    }
                }

                string invalidCodes;
                if (dataLoan.ValidateIncomingProductCodeList(selectedProductCodes, out invalidCodes))
                {
                    dataLoan.sSelectedProductCodeFilter = selectedProductCodes;
                    return true;
                }
                else
                {
                    errorMessage = invalidCodes;
                    return false;
                }
            }
        }

        private static bool SetLoanEstimateDates(CPageData dataLoan, XmlElement parentNode, out string errorMessage)
        {
            IEnumerable<CollectionProperties> leDatesXml = GatherCollectionProperties(parentNode, "Loan Estimate Dates");

            LoanEstimateDatesInfo leDates = dataLoan.sLoanEstimateDatesInfo;
            LoanEstimateDatesInfo oldLeDates = leDates.DuplicatePreservingIds();
            Dictionary <Guid, LoanEstimateDates> idToDates = leDates.LoanEstimateDatesList.ToDictionary((date) => date.UniqueId);
            Dictionary<Guid, ClosingCostArchive> validLeArchives = dataLoan.GetValidArchivesForLoanEstimateDates();

            HashSet<Guid> initialModifiedIds = new HashSet<Guid>();
            bool shoulClearInitialCheckboxes = leDatesXml.Any((property) => !property.ShouldDelete && (!property.Id.HasValue || property.Id.Value == Guid.Empty));
            if (dataLoan.sCalculateInitialLoanEstimate && shoulClearInitialCheckboxes)
            {
                // The UI clears all initial checkboxes when a new date is manually entered. We'll do the same here.
                foreach (var date in leDates.LoanEstimateDatesList)
                {
                    date.IsInitial = false;
                }
            }

            foreach (CollectionProperties leProperties in leDatesXml)
            {
                if (leProperties.ShouldDelete)
                {
                    if (!leProperties.Id.HasValue)
                    {
                        errorMessage = "A LoanEstimateDates record was marked for deletion but contained no ID.";
                        return false;
                    }
                    else if(leProperties.Id.Value == Guid.Empty || !idToDates.ContainsKey(leProperties.Id.Value))
                    {
                        errorMessage = $"LoanEstimateDates id not found: {leProperties.Id.Value}.";
                        return false;
                    }

                    LoanEstimateDates dateToDelete = idToDates[leProperties.Id.Value];
                    if (!leDates.DeleteDate(dateToDelete, out errorMessage))
                    {
                        return false;
                    }

                    idToDates.Remove(dateToDelete.UniqueId);
                }
                else
                {
                    LoanEstimateDates dateToModify;
                    if (!leProperties.Id.HasValue || leProperties.Id.Value == Guid.Empty)
                    {
                        var metadata = new LoanEstimateDatesMetadata(
                            dataLoan.GetClosingCostArchiveById,
                            dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                            dataLoan.sLoanRescindableT);

                        dateToModify = LoanEstimateDates.Create(metadata);
                        dateToModify.IsManual = true;
                        dateToModify.DisableManualArchiveAssociation = false;
                        leDates.AddDates(dateToModify);
                        idToDates.Add(dateToModify.UniqueId, dateToModify);
                    }
                    else
                    {
                        if (!idToDates.TryGetValue(leProperties.Id.Value, out dateToModify))
                        {
                            errorMessage = $"LoanEstimateDates id not found: {leProperties.Id.Value}.";
                            return false;
                        }
                    }

                    bool oldIsInitial = dateToModify.IsInitial;
                    if (!SetLoanEstimateProperties(dateToModify, leProperties, validLeArchives, out errorMessage))
                    {
                        return false;
                    }

                    if (!SetLoanEstimateSubcollections(dateToModify, leProperties.Subcollections, out errorMessage))
                    {
                        return false;
                    }

                    if (oldIsInitial != dateToModify.IsInitial)
                    {
                        initialModifiedIds.Add(dateToModify.UniqueId);
                    }
                }
            }

            // Need to run this first so that all the Initial calculations get ran so I can validate them.
            leDates.CalculateInitialLoanEstimate(dataLoan.sCalculateInitialLoanEstimate, dataLoan.sClosingCostArchive);
            if (!leDates.ValidateDatesToOldList(oldLeDates, initialModifiedIds, validLeArchives, dataLoan.sCalculateInitialLoanEstimate, out errorMessage))
            {
                return false;
            }

            dataLoan.sLoanEstimateDatesInfo = leDates;
            return true;
        }

        private static bool SetLoanEstimateProperties(LoanEstimateDates date, CollectionProperties leDateProperties, Dictionary<Guid, ClosingCostArchive> validArchives, out string errorMessage)
        {
            string invalidValueError = "Invalid value in {0} Field: {1}.";

            bool issuedDateLockedSet = false, deliveryMethodLockedSet = false, receivedDateLockedSet = false, signedDateLockedSet = false;
            var postSetActions = new List<Action>();

            foreach (var property in leDateProperties.Properties)
            {
                if (string.Equals(property.Key, "CreatedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime createdDate;
                    if (!DateTime.TryParse(property.Value, out createdDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.CreatedDate = createdDate;
                }
                else if (string.Equals(property.Key, "IssuedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime issuedDate;
                    if (!DateTime.TryParse(property.Value, out issuedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IssuedDate = issuedDate;
                    postSetActions.Add(() =>
                    {
                        if (!issuedDateLockedSet)    
                        {
                            date.IssuedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "DeliveryMethod", StringComparison.OrdinalIgnoreCase))
                {
                    E_DeliveryMethodT deliveryMethod;
                    if (!Enum.TryParse(property.Value, out deliveryMethod) || !Enum.IsDefined(typeof(E_DeliveryMethodT), deliveryMethod))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DeliveryMethod = deliveryMethod;
                    postSetActions.Add(() =>
                    {
                        if (!deliveryMethodLockedSet)
                        {
                            date.DeliveryMethodLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "ReceivedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime receivedDate;
                    if (!DateTime.TryParse(property.Value, out receivedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.ReceivedDate = receivedDate;
                    postSetActions.Add(() =>
                    {
                        if (!receivedDateLockedSet)
                        {
                            date.ReceivedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "SignedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime signedDate;
                    if (!DateTime.TryParse(property.Value, out signedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.SignedDate = signedDate;
                    postSetActions.Add(() =>
                    {
                        if (!signedDateLockedSet)
                        {
                            date.SignedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "ArchiveId", StringComparison.OrdinalIgnoreCase))
                {
                    Guid archiveId;
                    if (!Guid.TryParse(property.Value, out archiveId))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    ClosingCostArchive chosenArchive = null;
                    if (archiveId != Guid.Empty && !validArchives.TryGetValue(archiveId, out chosenArchive))
                    {
                        errorMessage = $"Associated archive not found: Id {archiveId}";
                        return false;
                    }

                    date.ArchiveId = archiveId;
                    if (chosenArchive != null)
                    {
                        date.ArchiveDate = DateTime.Parse(chosenArchive.DateArchived);
                    }
                }
                else if (string.Equals(property.Key, "IsInitial", StringComparison.OrdinalIgnoreCase))
                {
                    bool initial;
                    if (!bool.TryParse(property.Value, out initial))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsInitial = initial;
                }
                else if (string.Equals(property.Key, "LastDisclosedTRIDLoanProductDescription", StringComparison.OrdinalIgnoreCase))
                {
                    date.LastDisclosedTRIDLoanProductDescription = property.Value;
                }
                else if (string.Equals(property.Key, "DisclosedAprLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool locked;
                    if (!bool.TryParse(property.Value, out locked))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DisclosedAprLckd = locked;
                }
                else if (string.Equals(property.Key, "DisclosedApr", StringComparison.OrdinalIgnoreCase))
                {
                    date.DisclosedApr_rep = property.Value;
                }
                else if (string.Equals(property.Key, "IssuedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IssuedDateLckd = value;
                    issuedDateLockedSet = true;
                }
                else if (string.Equals(property.Key, "DeliveryMethodLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DeliveryMethodLckd = value;
                    deliveryMethodLockedSet = true;
                }
                else if (string.Equals(property.Key, "ReceivedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.ReceivedDateLckd = value;
                    receivedDateLockedSet = true;
                }
                else if (string.Equals(property.Key, "SignedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.SignedDateLckd = value;
                    signedDateLockedSet = true;
                }
                else
                {
                    errorMessage = $"{property.Key} Field cannot be modified.";
                    return false;
                }
            }

            postSetActions.ForEach(a => a());
            errorMessage = string.Empty;
            return true;
        }

        private static bool SetLoanEstimateSubcollections(LoanEstimateDates dateToModify, Dictionary<string, IEnumerable<CollectionProperties>> subcollections, out string errorMessage)
        {
            foreach (var subcollection in subcollections)
            {
                switch (subcollection.Key.ToLower())
                {
                    case "disclosuredatesbyconsumerid":
                        if (!SetDisclosureDatesByConsumerId(dateToModify.DisclosureDatesByConsumerId, subcollection.Value, out errorMessage))
                        {
                            return false;
                        }
                        break;
                    default:
                        errorMessage = $"{subcollection.Key} does not exist.";
                        return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool SetClosingDisclosureDates(CPageData dataLoan, XmlElement parentNode, out string errorMessage)
        {
            IEnumerable<CollectionProperties> closingDateXmlProperties = GatherCollectionProperties(parentNode, "Closing Disclosure Dates");

            ClosingDisclosureDatesInfo cdDates = dataLoan.sClosingDisclosureDatesInfo;
            ClosingDisclosureDatesInfo oldCdDates = cdDates.DuplicatePreservingIds();
            Dictionary<Guid, ClosingDisclosureDates> idToDates = cdDates.ClosingDisclosureDatesList.ToDictionary((date) => date.UniqueId);
            Dictionary<Guid, ClosingCostArchive> validArchives = dataLoan.GetValidArchivesForClosingDisclosureDates();
            HashSet<Guid> initialChangedDates = new HashSet<Guid>();

            bool shoulClearInitialCheckboxes = closingDateXmlProperties.Any((property) => !property.ShouldDelete && (!property.Id.HasValue || property.Id.Value == Guid.Empty));
            if (dataLoan.sCalculateInitialLoanEstimate && shoulClearInitialCheckboxes)
            {
                // The UI clears all initial checkboxes when a new date is manually entered. We'll do the same here.
                foreach (var date in cdDates.ClosingDisclosureDatesList)
                {
                    date.IsInitial = false;
                }
            }

            foreach (var cdProperty in closingDateXmlProperties)
            {
                if (cdProperty.ShouldDelete)
                {
                    if (!cdProperty.Id.HasValue)
                    {
                        errorMessage = "A ClosingDisclosureDates record was marked for deletion but contained no ID.";
                        return false;
                    }
                    else if (cdProperty.Id.Value == Guid.Empty || !idToDates.ContainsKey(cdProperty.Id.Value))
                    {
                        errorMessage = $"ClosingDisclosureDates id not found: {cdProperty.Id.Value}.";
                        return false;
                    }

                    ClosingDisclosureDates dateToDelete = idToDates[cdProperty.Id.Value];
                    if (!cdDates.DeleteDate(dateToDelete, out errorMessage))
                    {
                        return false;
                    }

                    idToDates.Remove(dateToDelete.UniqueId);
                }
                else
                {
                    ClosingDisclosureDates dateToModify;
                    if (!cdProperty.Id.HasValue || cdProperty.Id.Value == Guid.Empty)
                    {
                        var metadata = new ClosingDisclosureDatesMetadata(
                            dataLoan.GetClosingCostArchiveById,
                            dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                            dataLoan.sLoanRescindableT,
                            dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

                        dateToModify = ClosingDisclosureDates.Create(metadata);
                        dateToModify.IsManual = true;
                        dateToModify.DisableManualArchiveAssociation = false;
                        cdDates.AddDates(dateToModify);
                        idToDates.Add(dateToModify.UniqueId, dateToModify);
                    }
                    else
                    {
                        if (!idToDates.TryGetValue(cdProperty.Id.Value, out dateToModify))
                        {
                            errorMessage = $"ClosingDisclousreDates id not found: {cdProperty.Id.Value}.";
                            return false;
                        }
                    }

                    bool oldIsInitialValue = dateToModify.IsInitial;
                    if (!SetClosingDisclosureProperties(dateToModify, cdProperty, validArchives, dataLoan.BrokerDB.BrokerID, out errorMessage))
                    {
                        return false;
                    }

                    if (!SetClosingDisclosureSubcollections(dateToModify, cdProperty.Subcollections, out errorMessage))
                    {
                        return false;
                    }

                    if (oldIsInitialValue != dateToModify.IsInitial)
                    {
                        initialChangedDates.Add(dateToModify.UniqueId);
                    }
                }
            }

            cdDates.CalculateInitialClosingDisclosure(dataLoan.sCalculateInitialClosingDisclosure, dataLoan.sClosingCostArchive);
            if (!cdDates.ValidateDatesToOldList(oldCdDates, initialChangedDates, validArchives, dataLoan.sCalculateInitialClosingDisclosure, out errorMessage))
            {
                return false;
            }

            dataLoan.sClosingDisclosureDatesInfo = cdDates;

            errorMessage = string.Empty;
            return true;
        }

        private static bool SetClosingDisclosureProperties(ClosingDisclosureDates date, CollectionProperties cdDateProperties, Dictionary<Guid, ClosingCostArchive> validArchives, Guid brokerId, out string errorMessage)
        {
            string invalidValueError = "Invalid value in {0} Field: {1}.";

            bool issuedDateLockedSet = false, deliveryMethodLockedSet = false, receivedDateLockedSet = false, signedDateLockedSet = false;
            var postSetActions = new List<Action>();

            foreach (var property in cdDateProperties.Properties)
            {
                if (string.Equals(property.Key, "CreatedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime createdDate;
                    if (!DateTime.TryParse(property.Value, out createdDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.CreatedDate = createdDate;
                }
                else if (string.Equals(property.Key, "IssuedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime issuedDate;
                    if (!DateTime.TryParse(property.Value, out issuedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IssuedDate = issuedDate;
                    postSetActions.Add(() =>
                    {
                        if (!issuedDateLockedSet)
                        {
                            date.IssuedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "DeliveryMethod", StringComparison.OrdinalIgnoreCase))
                {
                    E_DeliveryMethodT deliveryMethod;
                    if (!Enum.TryParse(property.Value, out deliveryMethod) || !Enum.IsDefined(typeof(E_DeliveryMethodT), deliveryMethod))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DeliveryMethod = deliveryMethod;
                    postSetActions.Add(() =>
                    {
                        if (!deliveryMethodLockedSet)
                        {
                            date.DeliveryMethodLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "ReceivedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime receivedDate;
                    if (!DateTime.TryParse(property.Value, out receivedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.ReceivedDate = receivedDate;
                    postSetActions.Add(() =>
                    {
                        if (!receivedDateLockedSet)
                        {
                            date.ReceivedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "SignedDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime signedDate;
                    if (!DateTime.TryParse(property.Value, out signedDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.SignedDate = signedDate;
                    postSetActions.Add(() =>
                    {
                        if (!signedDateLockedSet)
                        {
                            date.SignedDateLckd = true;
                        }
                    });
                }
                else if (string.Equals(property.Key, "ArchiveId", StringComparison.OrdinalIgnoreCase))
                {
                    Guid archiveId;
                    if (!Guid.TryParse(property.Value, out archiveId))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    ClosingCostArchive chosenArchive = null;
                    if (archiveId != Guid.Empty && !validArchives.TryGetValue(archiveId, out chosenArchive))
                    {
                        errorMessage = $"Associated archive not found: Id {archiveId}";
                        return false;
                    }

                    date.ArchiveId = archiveId;
                    if (chosenArchive != null)
                    {
                        date.ArchiveDate = DateTime.Parse(chosenArchive.DateArchived);
                    }
                }
                else if (string.Equals(property.Key, "IsInitial", StringComparison.OrdinalIgnoreCase))
                {
                    bool initial;
                    if (!bool.TryParse(property.Value, out initial))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsInitial = initial;
                }
                else if (string.Equals(property.Key, "IsPreview", StringComparison.OrdinalIgnoreCase))
                {
                    bool isPreview;
                    if (!bool.TryParse(property.Value, out isPreview))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsPreview = isPreview;
                }
                else if (string.Equals(property.Key, "IsFinal", StringComparison.OrdinalIgnoreCase))
                {
                    bool isFinal;
                    if (!bool.TryParse(property.Value, out isFinal))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsFinal = isFinal;
                }
                else if (string.Equals(property.Key, "UcdDocument", StringComparison.OrdinalIgnoreCase))
                {
                    Guid ucdDocument;
                    if (!Guid.TryParse(property.Value, out ucdDocument))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    if (!UcdGenericEDocument.IsUcdEdocValid(ucdDocument, brokerId))
                    {
                        errorMessage = "Only UCD EDocs can be associated with a Closing Disclosure.";
                        return false;
                    }

                    date.UcdDocument = ucdDocument;
                }
                else if (string.Equals(property.Key, "IsDisclosurePostClosingDueToCureForToleranceViolation", StringComparison.OrdinalIgnoreCase))
                {
                    bool isDisclosurePostClosingDueToCureForToleranceViolation;
                    if (!bool.TryParse(property.Value, out isDisclosurePostClosingDueToCureForToleranceViolation))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation;
                }
                else if (string.Equals(property.Key, "IsDisclosurePostClosingDueToNonNumericalClericalError", StringComparison.OrdinalIgnoreCase))
                {
                    bool isDisclosurePostClosingDueToNonNumericalClericalError;
                    if (!bool.TryParse(property.Value, out isDisclosurePostClosingDueToNonNumericalClericalError))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError;
                }
                else if (string.Equals(property.Key, "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller", StringComparison.OrdinalIgnoreCase))
                {
                    bool isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
                    if (!bool.TryParse(property.Value, out isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
                }
                else if (string.Equals(property.Key, "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower", StringComparison.OrdinalIgnoreCase))
                {
                    bool isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
                    if (!bool.TryParse(property.Value, out isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
                }
                else if (string.Equals(property.Key, "PostConsummationKnowledgeOfEventDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime postConsummationKnowledgeOfEventDate;
                    if (!DateTime.TryParse(property.Value, out postConsummationKnowledgeOfEventDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate;
                }
                else if (string.Equals(property.Key, "PostConsummationRedisclosureReasonDate", StringComparison.OrdinalIgnoreCase))
                {
                    DateTime postConsummationRedisclosureReasonDate;
                    if (!DateTime.TryParse(property.Value, out postConsummationRedisclosureReasonDate))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate;
                }
                else if (string.Equals(property.Key, "LastDisclosedTRIDLoanProductDescription", StringComparison.OrdinalIgnoreCase))
                {
                    date.LastDisclosedTRIDLoanProductDescription = property.Value;
                }
                else if (string.Equals(property.Key, "DisclosedAprLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool locked;
                    if (!bool.TryParse(property.Value, out locked))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DisclosedAprLckd = locked;
                }
                else if (string.Equals(property.Key, "DisclosedApr", StringComparison.OrdinalIgnoreCase))
                {
                    date.DisclosedApr_rep = property.Value;
                }
                else if (string.Equals(property.Key, "IssuedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.IssuedDateLckd = value;
                    issuedDateLockedSet = true;
                }
                else if (string.Equals(property.Key, "DeliveryMethodLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.DeliveryMethodLckd = value;
                    deliveryMethodLockedSet = true;
                }
                else if (string.Equals(property.Key, "ReceivedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.ReceivedDateLckd = value;
                    receivedDateLockedSet = true;
                }
                else if (string.Equals(property.Key, "SignedDateLckd", StringComparison.OrdinalIgnoreCase))
                {
                    bool value;
                    if (!bool.TryParse(property.Value, out value))
                    {
                        errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                        return false;
                    }

                    date.SignedDateLckd = value;
                    signedDateLockedSet = true;
                }
                else
                {
                    errorMessage = $"{property.Key} Field cannot be modified.";
                    return false;
                }
            }

            postSetActions.ForEach(a => a());
            errorMessage = string.Empty;
            return true;
        }

        private static bool SetClosingDisclosureSubcollections(ClosingDisclosureDates dateToModify, Dictionary<string, IEnumerable<CollectionProperties>> subcollections, out string errorMessage)
        {
            foreach (var subcollection in subcollections)
            {
                switch (subcollection.Key.ToLower())
                {
                    case "disclosuredatesbyconsumerid":
                        if (!SetDisclosureDatesByConsumerId(dateToModify.DisclosureDatesByConsumerId, subcollection.Value, out errorMessage))
                        {
                            return false;
                        }
                        break;
                    default:
                        errorMessage = $"{subcollection.Key} does not exist.";
                        return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool SetDisclosureDatesByConsumerId(
            ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId, 
            IEnumerable<CollectionProperties> records, 
            out string errorMessage)
        {
            foreach (var record in records)
            {
                var consumerIdStr = record.Properties.FirstOrDefault(property => string.Equals("ConsumerId", property.Key, StringComparison.OrdinalIgnoreCase)).Value;
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>? consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(consumerIdStr);

                if (!consumerId.HasValue)
                {
                    errorMessage = "Consumer ID must be specified for DisclosureDatesByConsumerId.";
                    return false;
                }

                var borrowerLevelDates = disclosureDatesByConsumerId.FirstOrDefault(disclosureDates => disclosureDates.Key == consumerId.Value.Value);
                if (default(KeyValuePair<Guid, BorrowerDisclosureDates>).Equals(borrowerLevelDates))
                {
                    errorMessage = $"Consumer with ID {consumerId.Value.Value} not found.";
                    return false;
                }

                if (!SetBorrowerLevelDisclosureDateProperties(borrowerLevelDates.Value, record.Properties, out errorMessage))
                {
                    return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool SetBorrowerLevelDisclosureDateProperties(
            BorrowerDisclosureDates borrowerLevelDates, 
            Dictionary<string, string> properties, 
            out string errorMessage)
        {
            var invalidValueError = "Invalid value in {0} Field: {1}.";

            foreach (var property in properties)
            {
                switch (property.Key.ToLower())
                {
                    case "consumerid":
                        continue;
                    case "excludefromcalculations":
                        {
                            bool temp;
                            if (!bool.TryParse(property.Value, out temp))
                            {
                                errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                                return false;
                            }

                            borrowerLevelDates.ExcludeFromCalculations = temp;
                            break;
                        }
                    case "issueddate":
                        {
                            DateTime temp;
                            if (!DateTime.TryParse(property.Value, out temp))
                            {
                                errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                                return false;
                            }

                            borrowerLevelDates.IssuedDate = temp;
                            break;
                        }
                    case "deliverymethod":
                        {
                            E_DeliveryMethodT result;
                            if (!property.Value.TryParseDefine(out result))
                            {
                                errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                                return false;
                            }

                            borrowerLevelDates.DeliveryMethod = result;
                            break;
                        }
                    case "receiveddate":
                        {
                            DateTime temp;
                            if (!DateTime.TryParse(property.Value, out temp))
                            {
                                errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                                return false;
                            }

                            borrowerLevelDates.ReceivedDate = temp;
                            break;
                        }
                    case "signeddate":
                        {
                            DateTime temp;
                            if (!DateTime.TryParse(property.Value, out temp))
                            {
                                errorMessage = string.Format(invalidValueError, property.Key, property.Value);
                                return false;
                            }

                            borrowerLevelDates.SignedDate = temp;
                            break;
                        }
                    default:
                        errorMessage = $"{property.Key} Field cannot be modified.";
                        return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        private static void SetProrations(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode)
        {
            if (dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                throw new CBaseException("Prorations can only be modified in TRID 2015 mode.", "Prorations can only be modified in TRID 2015 mode.");
            }

            string isPopulateToLineLOn1003String = parentNode.GetAttribute("IsPopulatetoLineLOn1003");
            bool? isPopulateToLineLOn1003 = null;
            if (!string.IsNullOrEmpty(isPopulateToLineLOn1003String))
            {
                bool tempBool;
                if (!bool.TryParse(isPopulateToLineLOn1003String, out tempBool))
                {
                    string error = $"Invalid value in IsPopulatetoLineLOn1003 Field: {isPopulateToLineLOn1003String}.";
                    throw new CBaseException(error, error);
                }

                isPopulateToLineLOn1003 = tempBool;
            }

            IEnumerable<CollectionProperties> prorationsXml = GatherCollectionProperties(parentNode, "Prorations");
            var prorationList = dataLoan.sProrationList.GetUnlinkedCopy(IdAssignment.Copy);
            Dictionary<Guid, Proration> prorationMap = prorationList.ToDictionary((proration) => proration.Id.Value);
            foreach (CollectionProperties xmlProration in prorationsXml)
            {
                if (xmlProration.ShouldDelete)
                {
                    if (!xmlProration.Id.HasValue || xmlProration.Id == Guid.Empty || !prorationMap.ContainsKey(xmlProration.Id.Value))
                    {
                        string error = $"Proration ID not found. ID: {xmlProration.Id.Value.ToString()}";
                        throw new CBaseException(error, error);
                    }

                    Proration prorationToDelete = prorationMap[xmlProration.Id.Value];
                    if (prorationToDelete.IsSystemProration)
                    {
                        string error = "Prorations of type City/Town Taxes, County Taxes, and Assessments can only be modified, not deleted.";
                        throw new CBaseException(error, error);
                    }

                    prorationList.Remove(prorationToDelete);
                    prorationMap.Remove(xmlProration.Id.Value);
                }
                else
                {
                    Proration prorationToModfiy;
                    if (!xmlProration.Id.HasValue || xmlProration.Id == Guid.Empty)
                    {
                        // If the description corresponds to a system proration, set that.
                        string description = xmlProration.Properties.GetValueOrNull("description");
                        E_ProrationT? prorationType = xmlProration.Properties.GetValueOrNull("ProrationType")?.ToNullableEnum<E_ProrationT>();
                        if (Proration.SystemProrationDescriptions[E_ProrationT.Assessments].Equals(description, StringComparison.OrdinalIgnoreCase)
                            || prorationType == E_ProrationT.Assessments)
                        {
                            prorationToModfiy = prorationList.Assessments;
                        }
                        else if (Proration.SystemProrationDescriptions[E_ProrationT.CityTownTaxes].Equals(description, StringComparison.OrdinalIgnoreCase)
                            || prorationType == E_ProrationT.CityTownTaxes)
                        {
                            prorationToModfiy = prorationList.CityTownTaxes;
                        }
                        else if (Proration.SystemProrationDescriptions[E_ProrationT.CountyTaxes].Equals(description, StringComparison.OrdinalIgnoreCase)
                            || prorationType == E_ProrationT.CountyTaxes)
                        {
                            prorationToModfiy = prorationList.CountyTaxes;
                        }
                        else
                        {
                            prorationToModfiy = new Proration();
                            prorationList.Add(prorationToModfiy);
                        }
                    }
                    else
                    {
                        if (!prorationMap.TryGetValue(xmlProration.Id.Value, out prorationToModfiy))
                        {
                            string error = $"Proration ID not found. Id: {xmlProration.Id.Value.ToString()}";
                            throw new CBaseException(error, error);
                        }
                    }

                    SetProrationProperties(principal, prorationToModfiy, xmlProration, prorationToModfiy.IsSystemProration);
                }
            }

            if (isPopulateToLineLOn1003.HasValue)
            {
                prorationList.IsPopulateToLineLOn1003 = isPopulateToLineLOn1003.Value;
            }

            string validationError;
            if (!prorationList.ValidateToOldProrationList(dataLoan.sProrationList, dataLoan.sLoads1003LineLFromAdjustments, out validationError))
            {
                throw new CBaseException(validationError, validationError);
            }

            dataLoan.sProrationList = prorationList;
        }

        private static void SetProrationProperties(AbstractUserPrincipal principal, Proration prorationToModify, CollectionProperties prorationProps, bool isSystemProration)
        {
            string invalidValueError = "Invalid value in {0} Field: {1}.";
            string errorMsg = null;
            foreach (var property in prorationProps.Properties)
            {
                if (string.Equals(property.Key, "amount", StringComparison.OrdinalIgnoreCase))
                {
                    decimal amount;
                    if (!decimal.TryParse(property.Value, out amount))
                    {
                        errorMsg = string.Format(invalidValueError, property.Key, property.Value);
                        break;
                    }

                    prorationToModify.Amount = amount;
                }
                else if (string.Equals(property.Key, "description", StringComparison.OrdinalIgnoreCase))
                {
                    // There's a check in the validation function but that one doesn't check if the user actually attempted to modify the description.
                    if (isSystemProration && !string.Equals(property.Value, prorationToModify.Description))
                    {
                        errorMsg = "Descriptions for Prorations of type City/Town Taxes, County Taxes, or Assessments cannot be modified";
                        break;
                    }

                    prorationToModify.Description = property.Value;
                }
                else if (string.Equals(property.Key, "paidthroughdate", StringComparison.OrdinalIgnoreCase))
                {
                    CDateTime paidThroughDate;
                    if (string.IsNullOrEmpty(property.Value))
                    {
                        paidThroughDate = CDateTime.InvalidWrapValue;
                    }
                    else
                    {
                        paidThroughDate = CDateTime.Create(property.Value, new LosConvert());

                        if (!paidThroughDate.IsValid)
                        {
                            errorMsg = string.Format(invalidValueError, property.Key, property.Value);
                            break;
                        }
                    }

                    prorationToModify.PaidThroughDate = paidThroughDate;
                }
                else if (!string.Equals(property.Key, "prorationtype", StringComparison.OrdinalIgnoreCase))
                {
                    errorMsg = $"{property.Key} Field cannot be modified.";
                    break;
                }
            }

            if (!string.IsNullOrEmpty(errorMsg))
            {
                throw new CBaseException(errorMsg, errorMsg);
            }
        }

        private static void SetAdjustmentList(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode)
        {
            if (dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                throw new CBaseException("Adjustments can only be modified in TRID 2015 mode.", "Adjustments can only be modified in TRID 2015 mode.");
            }

            IEnumerable<CollectionProperties> adjustmentsXml = GatherCollectionProperties(parentNode, "Adjustments");

            var adjustmentsList = dataLoan.sAdjustmentList.GetUnlinkedCopy(IdAssignment.Copy);
            Dictionary<Guid, Adjustment> adjustmentsMap = adjustmentsList.ToDictionary((adj) => adj.Id.Value);
            foreach (CollectionProperties xmlAdjustment in adjustmentsXml)
            {
                if (xmlAdjustment.ShouldDelete)
                {
                    if (!xmlAdjustment.Id.HasValue || xmlAdjustment.Id == Guid.Empty || !adjustmentsMap.ContainsKey(xmlAdjustment.Id.Value))
                    {
                        string error = $"Adjustment ID not found. Id: {xmlAdjustment.Id.Value.ToString()}";
                        throw new CBaseException(error, error);
                    }

                    Adjustment adjustmentToDelete = adjustmentsMap[xmlAdjustment.Id.Value];
                    if (adjustmentToDelete == adjustmentsList.SellerCredit)
                    {
                        string error = "The default Seller Credit Adjustment cannot be deleted";
                        throw new CBaseException(error, error);
                    }

                    adjustmentsList.Remove(adjustmentToDelete);
                    adjustmentsMap.Remove(xmlAdjustment.Id.Value);
                }
                else
                {
                    Adjustment adjustmentToModify;
                    if (!xmlAdjustment.Id.HasValue || xmlAdjustment.Id == Guid.Empty)
                    {
                        // We can look for the Seller Credit through just the description as well.
                        string description = null;
                        if (xmlAdjustment.Properties.TryGetValue("description", out description) && description.Equals(Adjustment.SellerCreditDesc, StringComparison.OrdinalIgnoreCase))
                        {
                            adjustmentToModify = adjustmentsList.SellerCredit;
                        }
                        else
                        {
                            adjustmentToModify = new Adjustment();
                            adjustmentsList.Add(adjustmentToModify);
                        }
                    }
                    else
                    {
                        if (!adjustmentsMap.TryGetValue(xmlAdjustment.Id.Value, out adjustmentToModify))
                        {
                            string error = $"Adjustment ID not found. Id: {xmlAdjustment.Id.Value.ToString()}";
                            throw new CBaseException(error, error);
                        }
                    }

                    SetAdjustmentProperties(principal, adjustmentToModify, xmlAdjustment, adjustmentToModify == adjustmentsList.SellerCredit);
                }
            }

            string validationError;
            if (!adjustmentsList.ValidateToOldAdjustmentList(principal, dataLoan.sAdjustmentList, dataLoan.sLoads1003LineLFromAdjustments, out validationError))
            {
                throw new CBaseException(validationError, validationError);
            }

            dataLoan.sAdjustmentList = adjustmentsList;
        }

        private static void SetAdjustmentProperties(AbstractUserPrincipal principal, Adjustment adjustmentToModify, CollectionProperties adjustmentProps, bool isSellerCredit)
        {
            string invalidValueError = "Invalid value in {0} Field: {1}.";
            string invalidEnumError = "Invalid enumeration in {0} Field: {1}. Valid options are {2}.";
            string errorMsg = null;
            foreach (var property in adjustmentProps.Properties)
            {
                if (string.Equals(property.Key, "amount", StringComparison.OrdinalIgnoreCase))
                {
                    decimal amount;
                    if (!decimal.TryParse(property.Value, out amount))
                    {
                        errorMsg = string.Format(invalidValueError, property.Key, property.Value);
                        break;
                    }

                    adjustmentToModify.Amount = amount;
                }
                else if (string.Equals(property.Key, "description", StringComparison.OrdinalIgnoreCase))
                {
                    // Have to prevent setting here since the Adjustments calculate Adjustment type based on Description.
                    // Don't want the adjustment type to change in case a check will use the Adjustment type to look for the seller credit.
                    if (isSellerCredit && !string.Equals(Adjustment.SellerCreditDesc, property.Value, StringComparison.OrdinalIgnoreCase))
                    {
                        errorMsg = "The Description field of the default Seller Credit Adjustment cannot be modified";
                        break;
                    }

                    adjustmentToModify.Description = property.Value;
                }
                else if (string.Equals(property.Key, "ispopulatetolinelon1003", StringComparison.OrdinalIgnoreCase))
                {
                    bool isPopulateToLineLOn1003;
                    if (!bool.TryParse(property.Value, out isPopulateToLineLOn1003))
                    {
                        errorMsg = string.Format(invalidValueError, property.Key, property.Value);
                        break;
                    }

                    adjustmentToModify.SetIsPopulateToLineLOn1003(isPopulateToLineLOn1003);
                }
                else if (string.Equals(property.Key, "paidfromparty", StringComparison.OrdinalIgnoreCase))
                {
                    E_PartyT paidFromParty;
                    if (!Enum.TryParse(property.Value, out paidFromParty) ||
                        !Enum.IsDefined(typeof(E_PartyT), paidFromParty))
                    {
                        string validValues = string.Join(",", Enum.GetValues(typeof(E_PartyT)).Cast<int>());
                        errorMsg = string.Format(invalidEnumError, property.Key, property.Value, validValues);
                        break;
                    }

                    adjustmentToModify.PaidFromParty = paidFromParty;
                }
                else if (string.Equals(property.Key, "paidtoparty", StringComparison.OrdinalIgnoreCase))
                {
                    E_PartyT paidToParty;
                    if (!Enum.TryParse(property.Value, out paidToParty) ||
                        !Enum.IsDefined(typeof(E_PartyT), paidToParty))
                    {
                        string validValues = string.Join(",", Enum.GetValues(typeof(E_PartyT)).Cast<int>());
                        errorMsg = string.Format(invalidEnumError, property.Key, property.Value, validValues);
                        break;
                    }

                    adjustmentToModify.PaidToParty = paidToParty;
                }
                else if (string.Equals(property.Key, "pocdflp", StringComparison.OrdinalIgnoreCase))
                {
                    PocDflp pocDflp;
                    if (!Enum.TryParse(property.Value, out pocDflp) ||
                        !Enum.IsDefined(typeof(PocDflp), pocDflp))
                    {
                        string validValues = string.Join(",", Enum.GetValues(typeof(PocDflp)).Cast<int>());
                        errorMsg = string.Format(invalidEnumError, property.Key, property.Value, validValues);
                        break;
                    }

                    bool oldDflp = adjustmentToModify.DFLP;
                    adjustmentToModify.SetPocDflp(pocDflp);
                }
                else
                {
                    errorMsg = $"{property.Key} Field cannot be modified.";
                    break;
                }
            }

            if (!string.IsNullOrEmpty(errorMsg))
            {
                throw new CBaseException(errorMsg, errorMsg);
            }
        }

        /// <summary>
        /// Sets closing cost set from specified LOXml.
        /// </summary>
        /// <param name="dataLoan">The loan object containing the closing cost set to be modified.</param>
        /// <param name="collectionId">The name of the closting cost set being modified.</param>
        /// <param name="principal">The user performing this action.</param>
        /// <param name="parentNode">The LOXml to be imported into the closing cost set.</param>
        /// <param name="warningMessage">Stringbuilder to which failure messages are appended if closing cost set cannot be updated.</param>
        private static void SetClosingCostSet(CPageData dataLoan, string collectionId, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessage)
        {
            ////Verify record(s) are present, Action="0" or Action="1" attribute is present for all fee and feePayment records.
            if (IsClosingCostSetMissingRecordsOrActions(parentNode, collectionId, warningMessage))
            {
                return;
            }

            ////Identify whether borrower closing cost set is being modified.
            bool isBorrowerClosingCostFee = collectionId.Equals(BORROWERCLOSINGCOSTSET);
            LoanClosingCostSet closingCostSet = isBorrowerClosingCostFee ? (LoanClosingCostSet)dataLoan.sClosingCostSet : (LoanClosingCostSet)dataLoan.sSellerResponsibleClosingCostSet;

            Func<BaseClosingCostFee, bool> setFilter = null;
            if (isBorrowerClosingCostFee)
            {
                setFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostSet)closingCostSet,
                                                                                     false,
                                                                                     dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                     dataLoan.sDisclosureRegulationT,
                                                                                     dataLoan.sGfeIsTPOTransaction);
            }

            ////Transform LOXml to wrapper objects.
            List<ClosingCostFeeWrapper> closingCostFeeWrapperList = new List<ClosingCostFeeWrapper>();
            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                ////Parse fee information from record
                NameValueCollection feeProperties = ParseFromRecord(record);

                ////Parse payment information from record
                IEnumerable<ClosingCostFeePaymentWrapper> paymentRecords = ParsePaymentInformation(record);

                ////Action to add or delete closing cost fee will be an attribute.
                feeProperties.Add("action", record.GetAttribute("Action"));

                closingCostFeeWrapperList.Add(new ClosingCostFeeWrapper(feeProperties, paymentRecords, isBorrowerClosingCostFee));
            }

            ////Try matching with new or existing closing cost fee as well as associated fee payments to coresponding wrapper objects.
            ////Exit method if unsuccessful with any record.
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            FeeSetupClosingCostSet brokerBaseClosingCostFeeCollection = db.GetUnlinkedClosingCostSet();
            IEnumerable<BaseClosingCostFee> availableFees = brokerBaseClosingCostFeeCollection.GetFees(c => c.CanManuallyAddToEditor);
            foreach (ClosingCostFeeWrapper closingCostFeeWrapper in closingCostFeeWrapperList)
            {
                if (!closingCostFeeWrapper.TryMatchClosingCostFee(closingCostSet, setFilter, availableFees, collectionId, warningMessage))
                {
                    return;
                }
            }

            ////Apply changes
            foreach (ClosingCostFeeWrapper closingCostFeeWrapper in closingCostFeeWrapperList)
            {
                if (closingCostFeeWrapper.ClosingCostFeeMarkedForDeletion)
                {
                    closingCostSet.Remove(closingCostFeeWrapper.ClosingCostFee);
                }
                else
                {
                    closingCostFeeWrapper.UpdateClosingCostFee(dataLoan);
                    closingCostSet.AddOrUpdate(closingCostFeeWrapper.ClosingCostFee);
                }
            }
        }

        /// <summary>
        /// Inspects LOXml collection element for missing records or actions needed for modifying closing cost sets.
        /// </summary>
        /// <param name="parentNode">LOXml collection element containing information to modify closing cost set.</param>
        /// <param name="collectionId">The name of the closing cost set to be modified.</param>
        /// <param name="warningMessage">Stringbuilder to which failure messages are appended if LOXml sub-element are missing record elements or valid Action attribute.</param>
        /// <returns>Returns a boolean indicating whether LOXml collection element is missing records or actions needed for modifying closing cost sets.</returns>
        private static bool IsClosingCostSetMissingRecordsOrActions(XmlElement parentNode, string collectionId, StringBuilder warningMessage)
        {
            if (!StaticMethodsAndExtensions.DoesXPathExist(parentNode, "record"))
            {
                warningMessage.Append("WARNING: No records in " + collectionId + " are present. This collection will not be modified.");
                return true;
            }
            if (StaticMethodsAndExtensions.DoesXPathExist(parentNode, "record[not(@Action='" + ClosingCostFeeWrapper.FEEACTIONADD + "' or @Action='" + ClosingCostFeeWrapper.FEEACTIONDELETE + "')]"))
            {
                warningMessage.Append("WARNING: A record in " + collectionId + " does not contain a valid Action attribute. This collection will not be modified.").Append(Environment.NewLine);
                return true;
            }

            if (StaticMethodsAndExtensions.DoesXPathExist(parentNode, "record/collection[@id='Payments']/record[not(@Action='" + ClosingCostFeePaymentWrapper.FEEPAYMENTACTIONADD + "' or @Action='" + ClosingCostFeePaymentWrapper.FEEPAYMENTACTIONDELETE + "')]"))
            {
                warningMessage.Append("WARNING: A record in " + collectionId + " does not contain a payment record with valid Action attribute. This collection will not be modified.").Append(Environment.NewLine);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Parses fee payment LOXml into a collection of wrapper objects.
        /// </summary>
        /// <param name="feeRecord">LOXml containing fee record and associated payments.</param>        
        /// <returns>A collection of fee payment wrapper objects.</returns>
        private static IEnumerable<ClosingCostFeePaymentWrapper> ParsePaymentInformation(XmlElement feeRecord)
        {
            List<ClosingCostFeePaymentWrapper> paymentList = new List<ClosingCostFeePaymentWrapper>();
            XmlNodeList recordList = feeRecord.SelectNodes("collection[@id='Payments']/record");
            foreach (XmlElement record in recordList)
            {
                NameValueCollection nv = ParseFromRecord(record);

                ////Action to add or delete closing cost fee payment will be an attribute.
                nv.Add("action", record.GetAttribute("Action"));
                paymentList.Add(new ClosingCostFeePaymentWrapper(nv));
            }

            return paymentList;
        }

        /// <summary>
        /// Parses LOXml collection record into NameValueCollection object.
        /// </summary>
        /// <param name="record">LOXml collection record to be parsed.</param>
        /// <returns>Name value collection object with field id as name and its corresponding xml value as value.</returns>
        private static NameValueCollection ParseFromRecord(XmlElement record)
        {
            XmlNodeList fieldList = record.SelectNodes("field");
            NameValueCollection nv = new NameValueCollection();
            foreach (XmlElement el in fieldList)
            {
                string id = el.GetAttribute("id").ToLower();
                string value = el.InnerText;
                nv.Add(id, value);
            }

            return nv;
        }

        private static void SetBranch(CPageData dataLoan, AbstractUserPrincipal principal, StringBuilder warningMessages, string value)
        {
            Guid branchId;
            try
            {
                branchId = new Guid(value);
            }
            catch (FormatException)
            {
                throw new FieldInvalidValueException("sBranchId", value);
            }

            Dictionary<string, bool> requiredUserProperties = new Dictionary<string, bool>();
            requiredUserProperties.Add("be LQB User", principal.Type.Equals("B", StringComparison.OrdinalIgnoreCase));
            requiredUserProperties.Add("have Manager Role", principal.IsInRole(ConstApp.ROLE_MANAGER));
            requiredUserProperties.Add("have Corporate Access", principal.HasPermission(Permission.BrokerLevelAccess));
            IEnumerable<string> missingrequiredUserProperties = requiredUserProperties
                                                                .Where(p => p.Value == false)
                                                                .Select(p => p.Key);
            if (missingrequiredUserProperties.Count() > 0)
            {
                warningMessages.AppendLine(string.Format("WARNING: User must {0} in order to set sBranchId. Skip.{1}",
                                                          string.Join(", ", missingrequiredUserProperties.ToArray()),
                                                          Environment.NewLine));
                return;
            }

            BranchDB branch = new BranchDB(branchId, principal.BrokerId);
            if (branch.Retrieve() == false)
            {
                warningMessages.AppendLine(string.Format("WARNING: sBranchId {0} does not exist in lender account. Skip.{1}",
                                                          branchId.ToString(),
                                                          Environment.NewLine));
                return;
            }

            dataLoan.AssignBranch(branchId);
        }
        private static void SetBrokerRateLockTable(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowLockDeskWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Lock Desk write permission. SKIP modify broker rate table");
                return;
            }
            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                E_sBrokerLockPriceRowLockedT sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                string rate = string.Empty;
                string fee = string.Empty;
                string margin = string.Empty;
                string trate = string.Empty;
                XmlNodeList fieldList = record.SelectNodes("field");

                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;

                    if (fieldId.Equals("sBrokerLockPriceRowLockedT", StringComparison.OrdinalIgnoreCase))
                    {
                        sBrokerLockPriceRowLockedT = (E_sBrokerLockPriceRowLockedT)Enum.Parse(typeof(E_sBrokerLockPriceRowLockedT), value);
                    }
                    else if (fieldId.Equals("Rate", StringComparison.OrdinalIgnoreCase))
                    {
                        rate = value;
                    }
                    else if (fieldId.Equals("Fee", StringComparison.OrdinalIgnoreCase))
                    {
                        fee = value;
                    }
                    else if (fieldId.Equals("Margin", StringComparison.OrdinalIgnoreCase))
                    {
                        margin = value;
                    }
                    else if (fieldId.Equals("TRate", StringComparison.OrdinalIgnoreCase))
                    {
                        trate = value;
                    }
                }
                if (string.IsNullOrEmpty(rate) == false || string.IsNullOrEmpty(fee) == false)
                {
                    dataLoan.sBrokerLockPriceRowLockedT = sBrokerLockPriceRowLockedT;
                    dataLoan.LoadBrokerLockData(rate, fee, margin, trate);
                }
                return; // Only Parse First one.
            }

        }
        private static void AddInvestorLockAdjustments(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowLockDeskWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Lock Desk write permission. SKIP modify lock adjustment");
                return;
            }

            List<PricingAdjustment> list = dataLoan.sInvestorLockAdjustments;
            XmlNodeList recordList = parentNode.SelectNodes("record");

            bool isBasePriceRowLocked = dataLoan.sInvestorLockPriceRowLockedT == E_sInvestorLockPriceRowLockedT.Base;
            var cachedInvestorAdjustments = new CachedLockPrice(
                interestRate: isBasePriceRowLocked ? dataLoan.sInvestorLockBaseNoteIR_rep : dataLoan.sInvestorLockNoteIR_rep,
                fee: isBasePriceRowLocked ? dataLoan.sInvestorLockBaseBrokComp1PcFee_rep : dataLoan.sInvestorLockBrokComp1Pc_rep,
                margin: isBasePriceRowLocked ? dataLoan.sInvestorLockBaseRAdjMarginR_rep : dataLoan.sInvestorLockRAdjMarginR_rep,
                armTeaserRate: isBasePriceRowLocked ? dataLoan.sInvestorLockBaseOptionArmTeaserR_rep : dataLoan.sInvestorLockOptionArmTeaserR_rep);

            if (parentNode.GetAttribute("DeleteExisting").Equals("True", StringComparison.OrdinalIgnoreCase))
            {
                list = new List<PricingAdjustment>();
            }

            foreach (XmlElement record in recordList)
            {
                PricingAdjustment adjustment = new PricingAdjustment();
                adjustment.IsHidden = false;
                adjustment.IsLenderAdjustment = false;



                XmlNodeList fieldList = record.SelectNodes("field");



                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;

                    if (fieldId.Equals("Description", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Description = value;
                    }
                    else if (fieldId.Equals("Point", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Fee = value;
                    }
                    else if (fieldId.Equals("Rate", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Rate = value;
                    }
                    else if (fieldId.Equals("Margin", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Margin = value;
                    }
                }
                list.Add(adjustment);
            }
            dataLoan.sInvestorLockAdjustments = list;
            dataLoan.LoadInvestorLockData(cachedInvestorAdjustments.InterestRate, cachedInvestorAdjustments.Fee, cachedInvestorAdjustments.Margin, cachedInvestorAdjustments.ARMTeaserRate);
        }

        private static void AddBrokerLockAdjustments(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowLockDeskWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Lock Desk write permission. SKIP modify lock adjustment");
                return;
            }

            var cachedBrokerPrice = GetCachedBrokerPrice(dataLoan);
            List<PricingAdjustment> list = dataLoan.sBrokerLockAdjustments;
            XmlNodeList recordList = parentNode.SelectNodes("record");

            if (parentNode.GetAttribute("DeleteExisting").Equals("True", StringComparison.OrdinalIgnoreCase))
            {
                List<PricingAdjustment> temp_list = new List<PricingAdjustment>();
                foreach (var o in list)
                {
                    if (o.IsHidden == true)
                    {
                        temp_list.Add(o);
                    }
                }
                list = temp_list;
            }

            string invalidValueMessage = "Invalid value for field {0}: {1}";
            foreach (XmlElement record in recordList)
            {
                PricingAdjustment adjustment = new PricingAdjustment();
                adjustment.IsHidden = false;
                adjustment.IsLenderAdjustment = false;

                XmlNodeList fieldList = record.SelectNodes("field");

                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;

                    if (fieldId.Equals("Description", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Description = value;
                    }
                    else if (fieldId.Equals("Point", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Fee = value;
                    }
                    else if (fieldId.Equals("Rate", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Rate = value;
                    }
                    else if (fieldId.Equals("Margin", StringComparison.OrdinalIgnoreCase))
                    {
                        adjustment.Margin = value;
                    }
                    else if (fieldId.Equals("IsHidden", StringComparison.OrdinalIgnoreCase))
                    {
                        bool isHidden;
                        if (!bool.TryParse(value, out isHidden))
                        {
                            warningMessages.AppendLine(string.Format(invalidValueMessage, fieldId, value));
                            return;
                        }

                        adjustment.IsHidden = isHidden;
                    }
                    else if (fieldId.Equals("IsLender", StringComparison.OrdinalIgnoreCase))
                    {
                        bool isLenderAdjustment;
                        if (!bool.TryParse(value, out isLenderAdjustment))
                        {
                            warningMessages.AppendLine(string.Format(invalidValueMessage, fieldId, value));
                            return;
                        }

                        adjustment.IsLenderAdjustment = isLenderAdjustment;
                    }
                    else if (fieldId.Equals("IsPersist", StringComparison.OrdinalIgnoreCase))
                    {
                        bool isPersist;
                        if (!bool.TryParse(value, out isPersist))
                        {
                            warningMessages.AppendLine(string.Format(invalidValueMessage, fieldId, value));
                            return;
                        }

                        adjustment.IsPersist = isPersist;
                    }
                    else
                    {
                        warningMessages.AppendLine($"Invalid field {fieldId} specified.");
                        return;
                    }

                }
                list.Add(adjustment);
            }
            dataLoan.sBrokerLockAdjustments = list;
            dataLoan.LoadBrokerLockData(cachedBrokerPrice.InterestRate, cachedBrokerPrice.Fee, cachedBrokerPrice.Margin, cachedBrokerPrice.ARMTeaserRate);
        }

        private static CachedLockPrice GetCachedBrokerPrice(CPageData loan)
        {
            switch (loan.sBrokerLockPriceRowLockedT)
            {
                case E_sBrokerLockPriceRowLockedT.Base:
                    return new CachedLockPrice(
                        interestRate: loan.sBrokerLockBaseNoteIR_rep,
                        fee: loan.sBrokerLockBaseBrokComp1PcFee_rep,
                        margin: loan.sBrokerLockBaseRAdjMarginR_rep,
                        armTeaserRate: loan.sBrokerLockBaseOptionArmTeaserR_rep);
                case E_sBrokerLockPriceRowLockedT.BrokerBase:
                    return new CachedLockPrice(
                        interestRate: loan.sBrokerLockBrokerBaseNoteIR_rep,
                        fee: loan.sBrokerLockBrokerBaseBrokComp1PcFee_rep,
                        margin: loan.sBrokerLockBrokerBaseRAdjMarginR_rep,
                        armTeaserRate: loan.sBrokerLockBrokerBaseOptionArmTeaserR_rep);
                case E_sBrokerLockPriceRowLockedT.BrokerFinal:
                    return new CachedLockPrice(
                        interestRate: loan.sNoteIR_rep,
                        fee: loan.sBrokComp1Pc_rep,
                        margin: loan.sRAdjMarginR_rep,
                        armTeaserRate: loan.sOptionArmTeaserR_rep);
                case E_sBrokerLockPriceRowLockedT.OriginatorPrice:
                    return new CachedLockPrice(
                        interestRate: loan.sBrokerLockOriginatorPriceNoteIR_rep,
                        fee: loan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep,
                        margin: loan.sBrokerLockOriginatorPriceRAdjMarginR_rep,
                        armTeaserRate: loan.sBrokerLockOriginatorPriceOptionArmTeaserR_rep);
                default:
                    throw new UnhandledEnumException(loan.sBrokerLockPriceRowLockedT);
            }
        }

        /// <summary>
        /// Simple DTO for lock adjustments, to save us from anonymous types/tuple madness.
        /// </summary>
        private class CachedLockPrice
        {
            public CachedLockPrice(string interestRate, string fee, string margin, string armTeaserRate)
            {
                this.InterestRate = interestRate;
                this.Fee = fee;
                this.Margin = margin;
                this.ARMTeaserRate = armTeaserRate;
            }

            public string InterestRate { get; }
            public string Fee { get; }
            public string Margin { get; }
            public string ARMTeaserRate { get; }
        }

        private static void SetInvestorRateLockTable(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages)
        {
            if (principal.HasPermission(Permission.AllowLockDeskWrite) == false)
            {
                warningMessages.AppendLine("WARNING: Lack of Lock Desk write permission. The investor rate table was not modified.");
                return;
            }

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                E_sInvestorLockPriceRowLockedT sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
                string sRate = string.Empty;
                string sFee = string.Empty;
                string sMargin = string.Empty;
                string sTrate = string.Empty;
                string sLockReason = dataLoan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.LockSuspended ?
                    "Lock resumed via web services" : "Locked via web services";
                bool bLockRate = false;
                XmlNodeList fieldList = record.SelectNodes("field");

                foreach (XmlElement el in fieldList)
                {
                    string fieldId = el.GetAttribute("id");
                    string value = el.InnerText;

                    if (fieldId.Equals("sInvestorLockPriceRowLockedT", StringComparison.OrdinalIgnoreCase))
                    {
                        sInvestorLockPriceRowLockedT = (E_sInvestorLockPriceRowLockedT)Enum.Parse(typeof(E_sInvestorLockPriceRowLockedT), value);
                    }
                    else if (fieldId.Equals("Rate", StringComparison.OrdinalIgnoreCase))
                    {
                        sRate = value;
                    }
                    else if (fieldId.Equals("Fee", StringComparison.OrdinalIgnoreCase))
                    {
                        sFee = value;
                    }
                    else if (fieldId.Equals("Margin", StringComparison.OrdinalIgnoreCase))
                    {
                        sMargin = value;
                    }
                    else if (fieldId.Equals("TRate", StringComparison.OrdinalIgnoreCase))
                    {
                        sTrate = value;
                    }
                    else if (fieldId.Equals("Lock", StringComparison.OrdinalIgnoreCase))
                    {
                        bLockRate = value.Equals("True", StringComparison.OrdinalIgnoreCase);
                    }
                    else if (fieldId.Equals("LockReason", StringComparison.OrdinalIgnoreCase))
                    {
                        sLockReason = value;
                    }
                }

                bool isBasePriceRowLocked = sInvestorLockPriceRowLockedT == E_sInvestorLockPriceRowLockedT.Base;
                if (string.IsNullOrEmpty(sRate))
                {
                    sRate = isBasePriceRowLocked ? dataLoan.sInvestorLockBaseNoteIR_rep : dataLoan.sInvestorLockNoteIR_rep;
                }

                if (string.IsNullOrEmpty(sFee))
                {
                    sFee = isBasePriceRowLocked ? dataLoan.sInvestorLockBaseBrokComp1PcFee_rep : dataLoan.sInvestorLockBrokComp1Pc_rep;
                }

                if (string.IsNullOrEmpty(sMargin))
                {
                    sMargin = isBasePriceRowLocked ? dataLoan.sInvestorLockBaseRAdjMarginR_rep : dataLoan.sInvestorLockRAdjMarginR_rep;
                }

                if (string.IsNullOrEmpty(sTrate))
                {
                    sTrate = isBasePriceRowLocked ? dataLoan.sInvestorLockBaseOptionArmTeaserR_rep : dataLoan.sInvestorLockOptionArmTeaserR_rep;
                }

                dataLoan.sInvestorLockPriceRowLockedT = sInvestorLockPriceRowLockedT;
                dataLoan.LoadInvestorLockData(sRate, sFee, sMargin, sTrate);
                if (bLockRate)
                {
                    if (dataLoan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.LockSuspended)
                    {
                        dataLoan.ResumeInvestorRateLock(principal.LoginNm, sLockReason);
                    }
                    else
                    {
                        dataLoan.LockInvestorRate(principal.LoginNm, sLockReason);
                    }
                }
                return; // Only Parse First one.
            }
        }

        private class ConditionProperties
        {
            public NameValueCollection FieldList { get; } = new NameValueCollection(6);

            public HashSet<string> AssociatedDocIds { get; } = new HashSet<string>();

            public HashSet<string> RequiredDocTypeIds { get; } = new HashSet<string>();
            
            public void AddToFieldList(string id, string value)
            {
                this.FieldList.Add(id, value);
            }

            public void AddAssocDocId(string id)
            {
                this.AssociatedDocIds.Add(id);
            }

            public void AddRequiredDocTypeId(string id)
            {
                this.RequiredDocTypeIds.Add(id);
            }
        }

        private static bool ImportConditions(CPageData dataLoan, AbstractUserPrincipal principal, XmlElement parentNode, StringBuilder warningMessages, out string errorMessage)
        {
            // Condition will compare base on the subject. If subject are identical then update the IsRequired, DoneDate, Category and Notes.
            // If subject is not match then create new conditions.

            // Retrieve a list of conditions from XML
            List<ConditionProperties> conditionFromXmlList = new List<ConditionProperties>();

            XmlNodeList recordList = parentNode.SelectNodes("record");
            foreach (XmlElement record in recordList)
            {
                XmlNodeList fieldList = record.SelectNodes("field");
                ConditionProperties properties = new ConditionProperties();
                foreach (XmlElement el in fieldList)
                {
                    string id = el.GetAttribute("id");
                    string value = el.InnerText;
                    properties.AddToFieldList(id, value);
                    if (id.ToLower() == "conddesc")
                    {
                        properties.AddToFieldList("key", value.ToLower());
                    }
                }

                XmlNodeList associatedDocs = record.SelectNodes("collection[@id='AssociatedDoc']/record");
                foreach (XmlNode associatedDoc in associatedDocs)
                {
                    properties.AddAssocDocId(associatedDoc.SelectSingleNode("field[@id='AssociatedDocId']").InnerText);
                }

                XmlNodeList requiredDocTypes = record.SelectNodes("collection[@id='RequiredDoc']/record");
                if (requiredDocTypes.Count > 1)
                {
                    // Count check should be done on the raw XML so that even duplicate records can trigger it.
                    errorMessage = "You are only allowed to set one document requirement.";
                    return false;
                }

                foreach (XmlNode requiredDocType in requiredDocTypes)
                {
                    properties.AddRequiredDocTypeId(requiredDocType.SelectSingleNode("field[@id='ReqDocTypeId']").InnerText);
                }

                conditionFromXmlList.Add(properties);
            }

            bool isDeleteExisting = false;
            if (parentNode.GetAttribute("DeleteExisting") != null)
            {
                isDeleteExisting = parentNode.GetAttribute("DeleteExisting").ToLower() == "true";
            }
            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
            if (brokerDB.IsUseNewTaskSystem)
            {
                if (!ImportConditionsNewTaskSystem(dataLoan, principal, conditionFromXmlList, warningMessages, isDeleteExisting, out errorMessage))
                {
                    return false;
                }
            }
            else
            {
                if (brokerDB.HasLenderDefaultFeatures)
                {
                    ImportTaskConditions(dataLoan, principal, conditionFromXmlList, warningMessages, isDeleteExisting);
                }
                else
                {
                    ImportXmlConditions(dataLoan, principal, conditionFromXmlList, warningMessages, isDeleteExisting);
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        private static void ImportXmlConditions(CPageData dataLoan, AbstractUserPrincipal principal, IEnumerable<ConditionProperties> conditionFromXmlList, StringBuilder warningMessages, bool isDeleteExisting)
        {
            try
            {
                if (isDeleteExisting)
                {
                    dataLoan.sCondDataSetClear();
                }
                else
                {
                    int count = dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete field = dataLoan.GetCondFieldsObsolete(i);
                        string subject = field.CondDesc.ToLower();
                        foreach (var condition in conditionFromXmlList)
                        {
                            var nv = condition.FieldList;
                            if (null != nv["key"])
                            {
                                if (nv["key"] == subject)
                                {
                                    nv["id"] = field.RecordId.ToString();
                                    break;


                                }
                            }
                        }
                    }
                }

                foreach (var condition in conditionFromXmlList)
                {
                    var nv = condition.FieldList;
                    CCondFieldsObsolete field = null;

                    if (null != nv["id"])
                    {
                        field = dataLoan.GetCondFieldsObsolete(new Guid(nv["id"]));
                    }
                    else
                    {
                        field = dataLoan.GetCondFieldsObsolete(-1);
                    }

                    if (null != nv["IsRequired"])
                    {
                        field.IsRequired = nv["IsRequired"].ToLower() == "true";
                    }
                    if (null != nv["DoneDate"])
                    {
                        field.DoneDate_rep = nv["DoneDate"];
                        if (field.DoneDate_rep != "")
                            field.IsDone = true;
                    }
                    if (null != nv["Category"])
                    {
                        field.PriorToEventDesc = nv["Category"];
                    }
                    if (null != nv["CondDesc"])
                    {
                        field.CondDesc = nv["CondDesc"];
                    }

                    field.Update();
                }
            }
            catch (Exception exc)
            {
                warningMessages.Append("WARNING: There is error while saving conditions.").Append(Environment.NewLine);
                Tools.LogError("Unable to save condition", exc);
            }
        }
        private static void ImportTaskConditions(CPageData dataLoan, AbstractUserPrincipal principal, IEnumerable<ConditionProperties> conditionFromXmlList, StringBuilder warningMessages, bool isDeleteExisting)
        {
            // Retrieve a list of conditions from system. Need to determine base one broker permission and use either condition from sCondDataSet
            // or Task Conditions.

            LoanConditionSetObsolete conditionList = new LoanConditionSetObsolete();
            conditionList.Retrieve(dataLoan.sLId, true, false, false);

            if (isDeleteExisting)
            {
                try
                {
                    using (CStoredProcedureExec spExec = new CStoredProcedureExec(dataLoan.sBrokerId))
                    {
                        try
                        {
                            spExec.BeginTransactionForWrite();
                            foreach (CLoanConditionObsolete condition in conditionList)
                            {
                                CLoanConditionObsolete.DeleteLoanCondition(spExec, condition.CondId, dataLoan.sLId);
                            }

                            spExec.CommitTransaction();
                        }
                        catch (Exception exc)
                        {
                            spExec.RollbackTransaction();
                            warningMessages.Append("ERROR: There is error while delete existing conditions.").Append(Environment.NewLine);
                            Tools.LogError("Unable to delete existing condition", exc);
                            return;
                        }
                    }
                }
                catch { }
            }
            else
            {
                foreach (CLoanConditionObsolete condition in conditionList)
                {
                    // 2/24/2006 dd - This code is extremely poor performance, because I re-loop the list of conditions everytime.
                    string discSubject = condition.CondDesc.ToLower();
                    foreach (var conditionProperties in conditionFromXmlList)
                    {
                        var nv = conditionProperties.FieldList;
                        if (null != nv["key"])
                        {
                            if (nv["key"] == discSubject)
                            {
                                nv["id"] = condition.CondId.ToString();
                                break;
                            }
                        }
                    }
                }
            }

            try
            {
                using (CStoredProcedureExec spExec = new CStoredProcedureExec(dataLoan.sBrokerId))
                {
                    try
                    {
                        spExec.BeginTransactionForWrite();
                        foreach (var conditionProperties in conditionFromXmlList)
                        {
                            var nv = conditionProperties.FieldList;
                            CLoanConditionObsolete condition = null;

                            if (null != nv["id"])
                            {
                                condition = new CLoanConditionObsolete(dataLoan.sBrokerId, new Guid(nv["id"]));
                                condition.Retrieve(spExec);
                            }
                            else
                            {
                                condition = new CLoanConditionObsolete(dataLoan.sBrokerId);
                                condition.LoanId = dataLoan.sLId;
                            }

                            if (null != nv["DoneDate"])
                            {
                                try
                                {
                                    condition.CondStatusDate = DateTime.Parse(nv["DoneDate"]);
                                    condition.CondStatus = E_CondStatus.Done;
                                }
                                catch { }
                            }
                            if (null != nv["Category"])
                            {
                                condition.CondCategoryDesc = nv["Category"];
                            }
                            if (null != nv["CondDesc"])
                            {
                                condition.CondDesc = nv["CondDesc"];
                            }
                            if (null != nv["Notes"])
                            {
                                condition.CondNotes = nv["Notes"];
                            }

                            condition.Save(spExec);
                        }

                        spExec.CommitTransaction();
                    }
                    catch (Exception exc)
                    {
                        spExec.RollbackTransaction();
                        warningMessages.Append("WARNING: There is error while saving conditions.").Append(Environment.NewLine);
                        Tools.LogError("Unable to save condition", exc);
                    }
                }
            }
            catch { }
        }

        private static bool ImportConditionsNewTaskSystem(CPageData dataLoan, AbstractUserPrincipal principal, IEnumerable<ConditionProperties> conditionFromXmlList, StringBuilder warningMessages, bool isDeleteExisting, out string errorMessage)
        {
            if (isDeleteExisting)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(principal.BrokerId, dataLoan.sLId, true);
                TaskPermissionProcessor taskPermissions = new TaskPermissionProcessor(principal);
                foreach (Task task in tasks)
                {
                    task.PermissionProcessor = taskPermissions;
                    if (task.GetPermissionLevelFor(principal) == E_UserTaskPermissionLevel.Manage)
                    {
                        task.Delete(principal.DisplayName, false);
                    }
                }
            }

            Dictionary<string, ConditionCategory> categories = ConditionCategory.GetCategories(principal.BrokerId).ToDictionary(category => category.Category, StringComparer.OrdinalIgnoreCase);
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Lazy<Dictionary<Guid, EDocument>> readableEdocs = new Lazy<Dictionary<Guid, EDocument>>(() => repo.GetDocumentsByLoanId(dataLoan.sLId).ToDictionary((edoc) => edoc.DocumentId));
            Lazy<Dictionary<int, DocType>> readableDocTypes = new Lazy<Dictionary<int, DocType>>(() => EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, EnforcePermissions: true).ToDictionary((doctype) => doctype.Id));

            foreach (var condition in conditionFromXmlList)
            {
                NameValueCollection nv = condition.FieldList;
                HashSet<string> associatedDoc = condition.AssociatedDocIds;
                HashSet<string> requiredDoc = condition.RequiredDocTypeIds;
                try
                {
                    if (!UpdateTask(nv, principal, dataLoan, categories, warningMessages, associatedDoc, requiredDoc, readableEdocs, readableDocTypes, out errorMessage))
                    {
                        return false;
                    }
                }
                catch (CBaseException e)
                {
                    warningMessages.AppendLine("Could not update a condition: " + e.UserMessage);
                    Tools.LogError(e);
                }
                catch (Exception e)
                {
                    warningMessages.AppendLine("Could not update a condition");
                    Tools.LogError(e);
                }
            }

            TaskUtilities.EnqueueTasksDueDateUpdate(principal.BrokerId, dataLoan.sLId);

            errorMessage = string.Empty;
            return true;
        }

        private static bool UpdateTask(NameValueCollection nv, AbstractUserPrincipal principal, CPageData dataLoan, Dictionary<string, ConditionCategory> categories, StringBuilder warningMessages,
                                        HashSet<string> associatedDocIds, HashSet<string> requiredDocTypeIds, Lazy<Dictionary<Guid, EDocument>> readableEdocs, Lazy<Dictionary<int, DocType>> readableDocTypes, out string errorMessage)
        {
            errorMessage = string.Empty;
            Task condition = null;
            if (null != nv["id"])
            {
                try
                {
                    condition = Task.Retrieve(principal.BrokerId, nv["id"]);
                }
                catch (TaskNotFoundException)
                {
                    warningMessages.AppendLine("Condition " + nv["id"] + " was not found.");
                    return true;
                }
                catch (TaskPermissionException) 
                {
                    // The user is trying to associate docs but they can't load the task. We'll show the doc association message
                    // instead and kill the save process.
                    if (associatedDocIds != null && associatedDocIds.Count > 0)
                    {
                        errorMessage = "You are not the task owner and you do not have any task permissions to associate documents.";
                        return false;
                    }

                    // Same here except this is for setting the required document.
                    if (requiredDocTypeIds != null && requiredDocTypeIds.Count > 0)
                    {
                        errorMessage = "You do not have permission to set the required document.";
                        return false;
                    }

                    throw;
                }
            }
            else
            {
                //make sure the required fields are there if its a new task 
                condition = Task.Create(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sPrimaryNm, dataLoan.sLNm);
                condition.TaskCreatedByUserId = principal.UserId;
                condition.TaskIsCondition = true;
                string[] requiredFields = new string[] {
                    "CondDesc",
                    "Category",
                    "AssignedTo",
                    "OwnedBy",
                    "DueDate"
                };

                foreach (string requiredField in requiredFields)
                {
                    if (string.IsNullOrEmpty(nv[requiredField]))
                    {
                        warningMessages.AppendLine("A condition did not have one of the required fields (CondDesc, Category, AssignedTo, OwnedBy, DueDate).");
                        return true;
                    }
                }
            }

            #region set category if its passed in 
            if (string.IsNullOrEmpty(nv["Category"]) == false)
            {
                ConditionCategory conditionCategory;
                //if the category does not exist add it
                if (false == categories.TryGetValue(nv["Category"], out conditionCategory))
                {
                    if (dataLoan.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.HasValue == false)
                    {
                        warningMessages.AppendLine("Condition could not be imported please contact Support.");
                        return true;
                    }

                    conditionCategory = new ConditionCategory(principal.BrokerId);
                    conditionCategory.Category = nv["Category"];
                    conditionCategory.DefaultTaskPermissionLevelId = dataLoan.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.Value;
                    conditionCategory.Save();
                    categories.Add(nv["Category"], conditionCategory);
                }

                condition.CondCategoryId = conditionCategory.Id;
                condition.TaskPermissionLevelId = conditionCategory.DefaultTaskPermissionLevelId;
            }
            #endregion

            #region figure out assigned to if the data is passed in 
            if (false == string.IsNullOrEmpty(nv["AssignedTo"]))
            {
                if (nv["AssignedTo"].StartsWith("ROLE_", StringComparison.OrdinalIgnoreCase))
                {
                    string roleError;
                    Guid roleId = GetRoleIdFromString(nv["AssignedTo"], out roleError);
                    if (roleId == Guid.Empty)
                    {
                        warningMessages.Append(roleError);
                        return true;
                    }
                    condition.TaskToBeAssignedRoleId = roleId;
                }
                else
                {
                    string userType = nv["AssignedToUserType"] ?? "B"; //asume B
                    switch (userType.ToLower())
                    {
                        case "b":
                        case "p":
                            break;
                        default:
                            warningMessages.AppendLine("Invalid AssignedToUserType - Valid Values : P OR B ");
                            return true;
                    }

                    Guid userId = GetUserId(principal, userType, nv["AssignedTo"]);
                    if (userId == Guid.Empty)
                    {
                        warningMessages.AppendLine("Could not save changes to task - could not find user  by login name of : " + nv["AssignedTo"]);
                        return true;
                    }

                    condition.TaskAssignedUserId = userId;
                }
            }
            #endregion 

            #region figure out owned by if its passed in 
            if (false == string.IsNullOrEmpty(nv["OwnedBy"]) && nv["id"] == null)
            {
                if (nv["OwnedBy"].StartsWith("ROLE_", StringComparison.OrdinalIgnoreCase))
                {
                    string roleError;
                    Guid roleId = GetRoleIdFromString(nv["OwnedBy"], out roleError);
                    if (roleId == Guid.Empty)
                    {
                        warningMessages.Append(roleError);
                        return true;
                    }
                    condition.TaskToBeOwnerRoleId = roleId;
                }
                else
                {
                    string userType = nv["OwnedByUserType"] ?? "B"; //asume B
                    switch (userType.ToLower())
                    {
                        case "b":
                        case "p":
                            break;
                        default:
                            warningMessages.AppendLine("Invalid OwnedByUserType - Valid Values : P OR B ");
                            return true;
                    }

                    Guid userId = GetUserId(principal, userType, nv["OwnedBy"]);
                    if (userId == Guid.Empty)
                    {
                        warningMessages.AppendLine("Could not save changes to task - could not find user  by login name of : " + nv["OwnedBy"]);
                        return true;
                    }
                    condition.TaskOwnerUserId = userId;
                }

            }
            #endregion 

            Task.SetTaskRoles(principal.BrokerId, condition, new LoanAssignmentContactTable(principal.BrokerId, dataLoan.sLId), new Dictionary<Guid, Guid>());

            if (false == string.IsNullOrEmpty(nv["DueDate"]))
            {
                DateTime dt;
                if (DateTime.TryParse(nv["DueDate"], out dt))
                {
                    condition.TaskDueDateLocked = true;
                    condition.TaskDueDate = dt;
                }
                else
                {
                    warningMessages.AppendLine("Invalid Due date for condition.");
                    return true;
                }
            }

            string value;

            if (IsNotNull(nv["CondDesc"], out value)) condition.TaskSubject = value;
            if (IsNotNull(nv["Notes"], out value)) condition.CondInternalNotes = value;
            if (IsNotNull(nv["Comments"], out value)) condition.Comments = value;

            try
            {
                int taskPermissionLevelId;
                if (int.TryParse(nv["TaskPermissionLevelId"], out taskPermissionLevelId)) condition.TaskPermissionLevelId = taskPermissionLevelId;
            }
            catch (KeyNotFoundException)
            {
                warningMessages.Append("Invalid TaskPermissionLevelId: ");
                throw;
            }

            bool condIsHidden;
            if (bool.TryParse(nv["CondIsHidden"], out condIsHidden)) condition.CondIsHidden = condIsHidden;

            if (associatedDocIds != null && associatedDocIds.Count > 0)
            {
                if (!AssociateDocumentsWithCondition(condition, principal, associatedDocIds, readableEdocs.Value, out errorMessage))
                {
                    return false;
                }    
            }

            if (requiredDocTypeIds != null && requiredDocTypeIds.Count > 0)
            {
                if (!SetRequiredDocTypeWithCondition(condition, principal, requiredDocTypeIds, readableDocTypes.Value, out errorMessage))
                {
                    return false;
                }
            }

            condition.Save(false);

            if (false == string.IsNullOrEmpty(nv["DoneDate"]))
            {
                condition = Task.Retrieve(principal.BrokerId, condition.TaskId);
                DateTime closeD;

                if (false == DateTime.TryParse(nv["DoneDate"], out closeD))
                {
                    warningMessages.AppendLine("Skiping condition Invalid DoneDate: " + nv["DoneDate"]);
                    return true;
                }

                condition.Close(principal.BrokerId, closeD, principal.DisplayName);
            }

            return true;
        }

        private static bool SetRequiredDocTypeWithCondition(Task condition, AbstractUserPrincipal principal, IEnumerable<string> requiredDocTypeIds, Dictionary<int, DocType> readableDocTypes, out string errorMessage)
        {
            if (string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase))
            {
                errorMessage = "As a PML user, you cannot set the required doc.";
                return false;
            }

            //  We don't support setting multiple required doc types yet.
            if (requiredDocTypeIds.Count() > 1)
            {
                errorMessage = "You are only allowed to set one document requirement.";
                return false;
            }

            int parsedDocTypeId;
            if (!int.TryParse(requiredDocTypeIds.First(), out parsedDocTypeId))
            {
                errorMessage = $"Invalid required doc type id: {parsedDocTypeId}.";
                return false;
            }

            if (principal.UserId == condition.TaskOwnerUserId)
            {
                condition.CondRequiredDocTypeId = parsedDocTypeId;
            }
            else if (condition.GetPermissionLevelFor(principal) == E_UserTaskPermissionLevel.Manage)
            {
                if (!readableDocTypes.ContainsKey(parsedDocTypeId))
                {
                    errorMessage = "You are not the task owner and you do not have access to the doc folder of the document you are setting.";
                    return false;
                }

                condition.CondRequiredDocTypeId = parsedDocTypeId;
            }
            else
            {
                errorMessage = "You are not the task owner and you do not have the Manage task permission.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool AssociateDocumentsWithCondition(Task condition, AbstractUserPrincipal principal, IEnumerable<string> associatedDocs, Dictionary<Guid, EDocument> readableEdocs, out string errorMessage)
        {
            if (condition.TaskOwnerUserId == principal.UserId ||
                condition.GetPermissionLevelFor(principal) != E_UserTaskPermissionLevel.None)
            {
                HashSet<Guid> currentAssociatedDocs = new HashSet<Guid>(condition.AssociatedDocs.Select((assoc) => assoc.DocumentId));

                foreach (var id in associatedDocs)
                {
                    Guid docId;
                    if (!Guid.TryParse(id, out docId))
                    {
                        errorMessage = $"Invalid associated doc id: {id}.";
                        return false;
                    }

                    // This dictionary should only contain the documents that are readable by this user.
                    if (!readableEdocs.ContainsKey(docId))
                    {
                        errorMessage = $"You do not have access to the doc folder of the document you are setting. Doc folder id: {docId}.";
                        return false;
                    }

                    currentAssociatedDocs.Add(docId);
                }

                condition.UpdateDocumentAssociations(currentAssociatedDocs);
            }
            else
            {
                errorMessage = "You are not the task owner and you do not have any task permissions to associate documents.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool IsNotNull(string input, out string output)
        {
            output = input;
            return input != null;
        }

        private static Guid GetUserId(AbstractUserPrincipal p, string userType, string loginname)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", p.BrokerId),
                                            new SqlParameter("@UserType", userType),
                                            new SqlParameter("@LoginName", loginname)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(p.BrokerId, "RetrieveUserIdByLoginNameAndBrokerId", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["UserId"];
                }
            }
            return Guid.Empty;
        }

        private static Guid GetRoleIdFromString(string roleDesc, out string warningMessages)
        {
            Guid roleId = Guid.Empty;
            warningMessages = string.Empty;
            switch (roleDesc.ToLower())
            {
                case "role_manager":
                    roleId = CEmployeeFields.s_ManagerRoleId;
                    break;
                case "role_underwriter":
                    roleId = CEmployeeFields.s_UnderwriterRoleId;
                    break;
                case "role_lock_desk":
                    roleId = CEmployeeFields.s_LockDeskRoleId;
                    break;
                case "role_processor":
                    roleId = CEmployeeFields.s_ProcessorRoleId;
                    break;
                case "role_loan_opener":
                    roleId = CEmployeeFields.s_LoanOpenerId;
                    break;
                case "role_loan_officer":
                    roleId = CEmployeeFields.s_LoanRepRoleId;
                    break;
                case "role_broker_processor":
                    roleId = CEmployeeFields.s_BrokerProcessorId;
                    break;
                case "role_lender_account_executive":
                    roleId = CEmployeeFields.s_LenderAccountExecRoleId;
                    break;
                case "role_real_estate_agent":
                    roleId = CEmployeeFields.s_RealEstateAgentRoleId;
                    break;
                case "role_call_center_agent":
                    roleId = CEmployeeFields.s_CallCenterAgentRoleId;
                    break;
                case "role_closer":
                    roleId = CEmployeeFields.s_CloserId;
                    break;
                case "role_external_secondary":
                    roleId = CEmployeeFields.s_ExternalSecondaryId;
                    break;
                case "role_external_post_closer":
                    roleId = CEmployeeFields.s_ExternalPostCloserId;
                    break;


                default:
                    warningMessages = string.Format("{0} not a valid role identifier - skipping condition import.", roleDesc);
                    Tools.LogError("Invalid role " + roleDesc);
                    break;
            }

            return roleId;
        }

        /// <summary>
        /// Attempts to parse a string to bool and apply to bool property.
        /// </summary>
        /// <param name="propertyName">The name of property to be set.</param>
        /// <param name="propertySetter">The property setter lambda to be invoked if inputString can be parsed to bool.</param>
        /// <param name="inputString">The string to be parsed to bool and applied to the bool property.</param>
        /// <param name="warningMessage">The warning message to be appended if inputString cannot be parsed.</param>
        private static void TrySetBoolProperty(string propertyName, Action<bool> propertySetter, string inputString, StringBuilder warningMessage)
        {
            bool b;
            if (bool.TryParse(inputString, out b))
            {
                propertySetter(b);
            }
            else
            {
                warningMessage.Append("WARNING: " + propertyName + " cannot be set. " + inputString + " is not a valid boolean.").Append(Environment.NewLine);
            }
        }

        /// <summary>
        /// Attempt to break or suspend lock, whether it is a front-end or back-end rate lock.
        /// </summary>
        /// <param name="principal">The user performing this action.</param>
        /// <param name="fieldId">The field id invoking the lock suspension/break.</param>
        /// <param name="fieldElement">The xml element containing the field id representing and reason for lock suspension/break.</param>
        /// <param name="isRateLocked">Boolean indicating whether rate is currently locked.</param>
        /// <param name="lockAction">The lock suspension/break action to be applied to loan.</param>
        /// <param name="warningMessage">Stringbuilder to which failure messages are appended if reason for lock suspension/break is not specified.</param>
        private static void TryBreakOrSuspendLock(AbstractUserPrincipal principal, string fieldId, XmlElement fieldElement, bool isRateLocked, Action<string, string> lockAction, StringBuilder warningMessages)
        {
            string reason = fieldElement.GetAttribute("reason").TrimWhitespaceAndBOM();
            if (string.IsNullOrEmpty(reason))
            {
                warningMessages.AppendLine("reason attribute is required for " + fieldId);
                return;
            }

            if (!isRateLocked)
            {
                warningMessages.AppendLine(fieldId + " is ignored. Rate is not locked.");
                return;
            }

            lockAction(principal.LoginNm, reason);
        }

        /// <summary>
        /// Retrieves the originator compensation settings needed to validate QuickPricer2 LOXml input.
        /// </summary>
        /// <param name="principal">The current principal.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns></returns>
        private static QuickPricerLoXmlInputParser.OriginatorCompensationSettings GetOriginatorCompensationSettings(
            AbstractUserPrincipal principal,
            Guid loanId)
        {
            var loan = new CFullAccessPageData(loanId, new[] { nameof(CPageBase.sOriginatorCompensationPaymentSourceT) });
            loan.AllowLoadWhileQP2Sandboxed = true;
            loan.ByPassFieldSecurityCheck = true;
            loan.InitLoad();
            return new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(
                principal.BrokerDB.IsDisplayCompensationChoiceInPml,
                loan.sOriginatorCompensationPaymentSourceT);
        }
    }
}
