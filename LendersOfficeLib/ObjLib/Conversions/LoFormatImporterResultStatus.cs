﻿namespace LendersOffice.Conversions
{
    /// <summary>
    /// Whether the import was successful or not.
    /// </summary>
    public enum LoFormatImporterResultStatus
    {
        /// <summary>
        /// The importer saved everything successfully.
        /// </summary>
        Ok = 0,

        /// <summary>
        /// The importer saved but may have encoutered non-fatal errors with some fields.
        /// </summary>
        OkWithWarning = 1,

        /// <summary>
        /// The importer ran and ran into errors. This should have resulted in a hardstop.
        /// </summary>
        Error = 2
    }
}
