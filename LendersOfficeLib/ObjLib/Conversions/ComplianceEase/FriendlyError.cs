﻿// <copyright file="FriendlyError.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   7/11/2014 3:43:01 PM 
// </summary>
namespace LendersOffice.ObjLib.Conversions.ComplianceEase
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// FriendlyError represents an error message, field and page where you the
    /// error can be corrected.
    /// </summary>
    public class FriendlyError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FriendlyError"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="field">The field causing the error.</param>
        /// <param name="page">The page the field is located when in Legacy mode.</param>
        /// <param name="pageGfe2010">The page the field is located when in Gfe 2010 mode.</param>
        /// <param name="pageTrid2015">The page the field is located when in Trid 2015 mode.</param>
        public FriendlyError(string message, string field, string page, string pageGfe2010, string pageTrid2015)
            : this(message, field, page)
        {
            this.PageGfe2010 = pageGfe2010;
            this.PageTrid2015 = pageTrid2015;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FriendlyError"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="field">The field causing the error.</param>
        /// <param name="page">The page the field is located.</param>
        public FriendlyError(string message, string field, string page)
        {
            this.Message = message;
            this.Field = field;
            this.Page = page;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FriendlyError"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        public FriendlyError(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string Message
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets the field causing the error.
        /// </summary>
        /// <value>The field.</value>
        public string Field
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the page the field can be found.
        /// </summary>
        /// <value>The page the field can be found.</value>
        public string Page
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the page the field can be found.  This only applies for loans that are no longer in legacy and aren't Trid yet.
        /// </summary>
        /// <value>The page the field can be found.</value>
        public string PageGfe2010
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the page the field can be found.  This only applies for Trid loans.
        /// </summary>
        /// <value>The page the field can be found.</value>
        public string PageTrid2015
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether a field was specified.
        /// </summary>
        /// <value>Whether a field was specified.</value>
        public bool HasField
        {
            get
            {
                string field = this.Field ?? string.Empty; 
                return !string.IsNullOrEmpty(field.TrimWhitespaceAndBOM());
            }
        }
    }
}