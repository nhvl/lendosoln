﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Security;
using System.Threading;
using ComplianceEase;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.ObjLib.DatabaseMessageQueue;

namespace LendersOffice.Conversions.ComplianceEase
{
    public class ComplianceEaseProcessorBackup : CommonProjectLib.Runnable.IRunnable
    {
        public string Description
        {
            get
            {
                return "Runs compliance ease on loans queued in ComplianceEaseAutoSubmitSecondaryQueue.";
            }
        }


        public void Run()
        {
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.ComplianceEaseAutoSubmitQueue);
            DBMessageQueue mQSecondary = new DBMessageQueue(ConstMsg.ComplianceEaseAutoSubmitSecondaryQueue);
            DateTime emailCutoff = DateTime.Now.Subtract(new TimeSpan(0, 30, 0));

            foreach (DBMessage msg in mQSecondary.Entries)
            {
                if (DateTime.Compare(msg.InsertionTime, emailCutoff) < 0)
                {
                    mQ.Send(msg.Subject1, msg.Data, msg.Subject2);
                }
            }
        }
    }
}
