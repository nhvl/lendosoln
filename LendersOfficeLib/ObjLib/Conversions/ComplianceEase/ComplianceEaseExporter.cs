﻿namespace LendersOffice.Conversions.ComplianceEase
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Services.Protocols;
    using System.Xml;
    using System.Xml.Serialization;
    using Common.SerializationTypes;
    using DataAccess;
    using DataAccess.Core.Construction;
    using EDocs;
    using global::ComplianceEase;
    using LendersOffice.Admin;
    using LendersOffice.com.complianceease.www;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Conversions.ComplianceEase;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.Security;

    public class ComplianceEaseExporter
    {
        public static FriendlyError GetFriendlyErrorMessage(Error e)
        {
            if (FriendlyErrorMappings.ContainsKey(e.ErrorCode))
            {
                return FriendlyErrorMappings[e.ErrorCode];
            }
            else
            {
                Tools.LogWarning("Unmapped Compliance Ease Error!" + Environment.NewLine + " Code: " + e.ErrorCode + Environment.NewLine + " Description: " + e.Description);
                return new FriendlyError(e.ErrorCode + " - " + e.Description);
            }
        }

        public static IEnumerable<FriendlyError> GetAllFriendlyErrors()
        {
            return from kvp in FriendlyErrorMappings select kvp.Value;
        }

        #region FriendlyErrorMappings
        private static readonly Dictionary<string, FriendlyError> FriendlyErrorMappings = new Dictionary<string, FriendlyError>()
        {
            {"200000", new FriendlyError(@"Audit Type must be either PreClose or PostClose.")},
            {"200001", new FriendlyError(@"Lender Name cannot exceed 50 characters.", @"Agents Page", "Status/Agents.aspx")},
            {"200002", new FriendlyError(@"Loan Number cannot exceed 20 characters.", @"sLNm", "LoanInfo.aspx?pg=0")},
            {"200003", new FriendlyError(@"Lender License Type is not a recognized valid type. For a complete list of supported license types, please contact ComplianceEase Integration.")},
            {"200004", new FriendlyError(@"HUD Approval Status Type must be either ""Approved"", ""Default"", ""NotApproved"", or ""NotConfigured"".")},
            {"200005", new FriendlyError(@"MERS MIN Number cannot exceed 18 characters.", @"sMersMin", "Status/General.aspx")},
            {"200006", new FriendlyError(@"Loan Originator Name cannot exceed 50 characters.", @"1003 Pg 3 (E_PreparerFormT.App1003Interviewer).PreparerName", "Forms/Loan1003.aspx?pg=2")},
            {"200007", new FriendlyError(@"Seller ID is not a valid ID. For a list of available seller IDs please contact ComplianceEase Integration.")},
            {"200008", new FriendlyError(@"Borrower First Name cannot exceed 30 characters.", @"aBFirstNm", "PmlDefaultValues.aspx")},
            {"200009", new FriendlyError(@"Borrower Last Name cannot exceed 30 characters.", @"aBLastNm", "PmlDefaultValues.aspx")},
            {"200010", new FriendlyError(@"Monthly Income must be between $0 and $15000000.")},
            {"200011", new FriendlyError(@"DTI Ratio must be between 0 and 999.999.", @"sQualBottomR", "LoanInfo.aspx?pg=0")},
            {"200012", new FriendlyError(@"Building and House number cannot exceed 10 characters.")},
            {"200013", new FriendlyError(@"Street Name cannot exceed 30 characters.")},
            {"200014", new FriendlyError(@"Street Suffix cannot exceed 10 characters.")},
            {"200015", new FriendlyError(@"Street Address Direction must be either ""E"", ""N"", ""NE"", ""NW"", ""S"", ""SE"", ""SW"", or ""W"".")},
            {"200016", new FriendlyError(@"Apartment or Unit Number cannot exceed 10 characters.")},
            {"200017", new FriendlyError(@"Property City cannot exceed 30 characters.", @"sSpCity", "Underwriting/PmlReviewInfo.aspx")},
            {"200018", new FriendlyError(@"Property County cannot exceed 30 characters.", @"sSpCounty", "Underwriting/PmlReviewInfo.aspx")},
            {"200019", new FriendlyError(@"Property State not recognized.", @"sSpState", "Underwriting/PmlReviewInfo.aspx")},
            {"200020", new FriendlyError(@"Property ZIP code is not valid.", @"sSpZip", "Underwriting/PmlReviewInfo.aspx")},
            {"200021", new FriendlyError(@"Subject Property Type must be either ""Attached"", ""Condominium"",  ""Cooperative"", ""Detached"", ""HighRiseCondominium"", ""ManufacturedHousing"", ""Other"", or ""PUD"".", @"sGseSpT", "LoanInfo.aspx?pg=0")},
            {"200022", new FriendlyError(@"Financed Number of Units must be between 1 and 5.", @"sUnitsNum", "Underwriting/PmlReviewInfo.aspx")},
            {"200023", new FriendlyError(@"Property Usage Type must be either ""Investment"", ""PrimaryResidence"", or ""SecondHome"".", @"aOccT", "LoanInfo.aspx?pg=0")},
            {"200024", new FriendlyError(@"""Other"" fee descriptions cannot exceed 255 characters.")},
            {"200029", new FriendlyError(@"Loan Amortization must be ""AdjustableRate"" or ""Fixed"" for HELOC loans.", @"sFinMethT", "Underwriting/PmlReviewInfo.aspx")},
            {"200030", new FriendlyError(@"Loan Purpose must be ""HomeImprovement"", ""Other"", ""Purchase"", or  ""Refinance"" for HELOC loans.", @"sLPurposeT", "Underwriting/PmlReviewInfo.aspx")},
            {"200037", new FriendlyError(@"Prepayment Penalty Term must be between 0 and 1080 months.", @"sPrepmtPeriodMonths", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200040", new FriendlyError(@"Mortgage Type must be either ""Conventional"", ""FarmersHomeAdministration"", ""FHA"", ""HELOC"", ""VA"", or ""Other"".", @"sLT", "Status/NMLSCallReport.aspx")},
            {"200041", new FriendlyError(@"Loan Purpose must be ""Bridge"", ""ConstructionOnly"", ""ConstructionToPermanent"", ""HomeImprovement"", ""Purchase"", ""Refinance"", or ""Other"".", @"sLPurposeT", "Underwriting/PmlReviewInfo.aspx")},
            {"200042", new FriendlyError(@"Base Loan Amount must be between $5000 and $20000000.", @"sLAmtCalc", "Underwriting/PmlReviewInfo.aspx")},
            {"200043", new FriendlyError(@"Note Rate must be between 0% and 30%.", @"sNoteIR", "Underwriting/PmlReviewInfo.aspx")},
            {"200045", new FriendlyError(@"Disclosed APR must be between 0% and 35%.", @"sApr", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200046", new FriendlyError(@"Disclosed Finance Charge must be between $0 and $325000000.", @"sFinCharge", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200047", new FriendlyError(@"Loan Original Maturity Term must be between 6 and 1080 months.", @"sDue", "LoanInfo.aspx?pg=0")},
            {"200048", new FriendlyError(@"Loan Amortization Term must be between 6 and 1080 months.", @"sTerm", "LoanInfo.aspx?pg=0")},
            {"200049", new FriendlyError(@"Loan Amortization Type must be either ""AdjustableRate"", ""Fixed"", or ""GraduatedPaymentMortgage"".", @"sFinMethT", "Underwriting/PmlReviewInfo.aspx")},
            {"200050", new FriendlyError(@"GSE Refinance Purpose must be either ""CashOutDebtConsolidation"", ""CashOutHomeImprovement"", ""CashOutLimited"", ""CashOutOther"", or ""ChangeInRateTerm"".", @"sGseRefPurposeT", "FreddieExport.aspx")},
            {"200051", new FriendlyError(@"Original LTV Ratio must be between 0% and 150%.", @"sLtvR", "Underwriting/PmlReviewInfo.aspx")},
            {"200052", new FriendlyError(@"Original CLTV Ratio must be between 0% and 150%.", @"sCltvR", "Underwriting/PmlReviewInfo.aspx")},
            {"200053", new FriendlyError(@"Lien Priority must be either ""FirstLien"", ""SecondLien"", or ""Other"".", @"sLienPosT", "Status/NMLSCallReport.aspx")},
            {"200054", new FriendlyError(@"Loan Documentation Type must be either ""Alternative"", ""FullDocumentation"", ""NoDocumentation"", ""NoVerificationOfStatedIncome"", ""NoVerificationOfStatedIncomeOrAssets"", ""NoVerificationOfStatedAssets"", ""Reduced"", or ""StreamlineRefinance"".")},
            {"200055", new FriendlyError(@"Late Charge Rate must be between 0% and .99999%", @"sLateChargePc", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200056", new FriendlyError(@"Late Charge Grace Period must be between 1 and 45 months.", @"sLateDays", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200057", new FriendlyError(@"Prepayment Penalty Term must be between 0 and 1080 months.", @"sPrepmtPeriodMonths", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200058", new FriendlyError(@"Maximum Prepayment Penalty Amount must be between $0 and $20000000.", @"sGfeMaxPpmtPenaltyAmt", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/LoanEstimate.aspx")},
            {"200065", new FriendlyError(@"Index Margin must be between -10% and 20%.", @"sRAdjMarginR", "Status/ViewBrokerRateLock.aspx")},
            {"200066", new FriendlyError(@"Index Current Value must be between 0% and 20%", @"sRAdjIndexR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200067", new FriendlyError(@"ARM Lifetime Cap Rate must be between 0% and .35%.", @"sRLifeCapR", "Forms/GoodFaithEstimate2010.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200068", new FriendlyError(@"ARM Lifetime Floor must be between 0% and 35%.", @"sRAdjFloorR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200069", new FriendlyError(@"Rate Adjustment Initial Cap must be between 0% and 20%.", @"sRAdj1stCapR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200070", new FriendlyError(@"First Rate Adjustment must be between 0 and 1080 months.", @"sRAdj1stCapMon", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200071", new FriendlyError(@"Rate Adjustment Subsequent Cap must be between 0% and 20%.", @"sRAdjCapR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200072", new FriendlyError(@"Subsequent Rate Adjustment Months must be between 0 and 1080.", @"sRAdjCapMon", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200073", new FriendlyError(@"ARM Interest Rate Rounding Type must be either ""Down"", ""Nearest"", ""Up"", or ""None"".", @"sRAdjRoundT", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200076", new FriendlyError(@"Graduated Payment Rate must be between 0% and 99.999%.", @"sGradPmtR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200077", new FriendlyError(@"Graduated Payment Term must be between 0 and 90 years.", @"sGradPmtYrs", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"200117", new FriendlyError(@"Interest Only Term must be between 0 and 1080 months.", @"sIOnlyMon", "Underwriting/PmlReviewInfo.aspx")},
            {"200122", new FriendlyError(@"FHA Upfront MI Premium must be between 0% and 10%.", @"sFfUfmipR", "LoanInfo.aspx?pg=1")},
            {"200123", new FriendlyError(@"Borrower Funding Fee must be between 0% and 10%.", @"sFfUfmipR", "LoanInfo.aspx?pg=1")},
            {"200125", new FriendlyError(@"MI and Funding Fee Total must be between $0 and $120000.", @"sFfUfmip1003", "LoanInfo.aspx?pg=1")},
            {"200126", new FriendlyError(@"MI and Funding Fee Financed Amount must be between $0 and $120000.", @"sFfUfmipFinanced", "LoanInfo.aspx?pg=0")},
            {"200127", new FriendlyError(@"MI and Funding Fee Cash Credit Amount must be between $0 and $120000.", @"sUfCashPd", "LoanInfo.aspx?pg=1")},
            {"200128", new FriendlyError(@"MI Renewal Premium Rate must be between 0% and 0.1%.", @"sProMInsR2", "LoanInfo.aspx?pg=1")},
            {"200129", new FriendlyError(@"MI Renewal Premium Rate must be between 0% and 0.1%.", @"sProMInsR2", "LoanInfo.aspx?pg=1")},
            {"200132", new FriendlyError(@"MI Renewal Premium Rate Duration must be between 0 and 1080 months.", @"sProMIns2Mon", "LoanInfo.aspx?pg=1")},
            {"200133", new FriendlyError(@"MI Renewal Premium Rate must be between 0% and 0.1%.", @"sProMInsR2", "LoanInfo.aspx?pg=1")},
            {"200135", new FriendlyError(@"MI Renewal Premium Rate Duration must be between 0 and 1080 months.", @"sProMIns2Mon", "LoanInfo.aspx?pg=1")},
            {"200137", new FriendlyError(@"MI LTV Cutoff must be between 0% and 150%.", @"sProMInsCancelLtv", "LoanInfo.aspx?pg=1")},
            {"200143", new FriendlyError(@"NMLS ID must be no more than 27 digits.")},
            {"200145", new FriendlyError(@"Company NMLS ID must be no more than 27 digits.")},
            {"200209", new FriendlyError(@"Proposed Principal & Interest payment amount must be between $0 and $15000000.")},
            {"200210", new FriendlyError(@"Proposed Monthly MIP be between $0 and $15000000.")},
            {"200211", new FriendlyError(@"Total Proposed P&I + Monthly MIP must be between $0 and $15000000.")},
            {"200212", new FriendlyError(@"Monthly Liability Payment must be between $0 and $15000000.")},
            {"202003", new FriendlyError(@"Appraisal Fee must be between 0 and 200,000.")},
            {"202047", new FriendlyError(@"Hazard Insurance Premium must be between 0 and 200,000.")},
            {"202054", new FriendlyError(@"Hazard Insurance Reserve must be between 0 and 200,000.")},
            {"210000", new FriendlyError(@"Compliance Audit Type required.")},
            {"210001", new FriendlyError(@"Lender Loan ID required.", @"sLNm", "LoanInfo.aspx?pg=0")},
            {"210002", new FriendlyError(@"Borrower DTI Ratio required.", @"sQualBottomR", "LoanInfo.aspx?pg=0")},
            {"210003", new FriendlyError(@"Lender Name required.")},
            {"210004", new FriendlyError(@"Property City required.", @"sSpCity", "Underwriting/PmlReviewInfo.aspx")},
            {"210005", new FriendlyError(@"Property State required.", @"sSpState", "Underwriting/PmlReviewInfo.aspx")},
            {"210006", new FriendlyError(@"Property ZIP code required.", @"sSpZip", "Underwriting/PmlReviewInfo.aspx")},
            {"210007", new FriendlyError(@"Subject Property Type required.", @"sGseSpT", "LoanInfo.aspx?pg=0")},
            {"210008", new FriendlyError(@"Financed number of units required.", @"sUnitsNum", "Underwriting/PmlReviewInfo.aspx")},
            {"210009", new FriendlyError(@"Property Usage Type required.", @"aOccT", "LoanInfo.aspx?pg=0")},
            {"210010", new FriendlyError(@"App Received by Lender Date is required", @"sAppReceivedByLenderD", "Status/General.aspx")},
            {"210011", new FriendlyError(@"Closing Date required.", @"sEstCloseD", "Status/General.aspx")},
            {"210012", new FriendlyError(@"Disbursement Date required.", @"sSchedFundD", "Status/General.aspx")},
            {"210013", new FriendlyError(@"Paid To required for ""Other"" RESPA Fees.", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"210014", new FriendlyError(@"Description is required for ""Other"" RESPA Fees.", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"210015", new FriendlyError(@"Interim Interest Paid Number of Days is required.", @"sIPiaDy", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"210016", new FriendlyError(@"Loan Amortization Type is required.", @"sFinMethT", "Underwriting/PmlReviewInfo.aspx")},
            {"210022", new FriendlyError(@"Prepayment Penalty Term Months is required.", @"sPrepmtPeriodMonths", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210024", new FriendlyError(@"Late Charge Rate is required.", @"sLateChargePc", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210025", new FriendlyError(@"Late Charge Grace Period is required.", @"sLateDays", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210026", new FriendlyError(@"Note Rate is required.", @"sNoteIR", "Underwriting/PmlReviewInfo.aspx")},
            {"210027", new FriendlyError(@"Loan Amortization Term is required.", @"sTerm", "LoanInfo.aspx?pg=0")},
            {"210028", new FriendlyError(@"Loan Original Maturity Term is required.", @"sDue", "LoanInfo.aspx?pg=0")},
            {"210029", new FriendlyError(@"Mortgage Type is required.", @"sLT", "Status/NMLSCallReport.aspx")},
            {"210030", new FriendlyError(@"Base Loan Amount is required.", @"sLAmtCalc", "Underwriting/PmlReviewInfo.aspx")},
            {"210031", new FriendlyError(@"Disclosed APR is required.", @"sApr", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210032", new FriendlyError(@"Prepayment Penalty Program Name or Prepayment Penalty Term is required.", @"sPrepmtPeriodMonths", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210033", new FriendlyError(@"Late Charge Rate is required.", @"sLateChargePc", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210034", new FriendlyError(@"Late Charge Grace Period is required.", @"sLateDays", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210035", new FriendlyError(@"ARM Lifetime Cap Rate is required for ARMs made by Colorado Licenses.", @"sRLifeCapR", "Forms/GoodFaithEstimate2010.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210036", new FriendlyError(@"Estimated Closing Date must be specified when when the subject property state is GA", @"sEstCloseD", "Status/General.aspx")},
            {"210037", new FriendlyError(@"GSE Refinance Purpose is required for Texas Refinance Loans.", @"sGseRefPurposeT", "FreddieExport.aspx")},
            {"210038", new FriendlyError(@"Original CLTV Ratio is required for first mortgages made by Kansas licensees using Interest section 16-207.", @"sCltvR", "Underwriting/PmlReviewInfo.aspx")},
            {"210039", new FriendlyError(@"Original CLTV Ratio is required when the Standard & Poors investor Policies are selected with a Kansas loan.", @"sCltvR", "Underwriting/PmlReviewInfo.aspx")},
            {"210040", new FriendlyError(@"Loan Purpose is required.", @"sLPurposeT", "Underwriting/PmlReviewInfo.aspx")},
            {"210041", new FriendlyError(@"Lien Priority is required.", @"sLienPosT", "Status/NMLSCallReport.aspx")},
            {"210042", new FriendlyError(@"Maximum Prepayment Penalty Amount is required.", @"sGfeMaxPpmtPenaltyAmt", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/LoanEstimate.aspx")},
            {"210046", new FriendlyError(@"If neither Index Current Value nor Index Margin is provided, then Lifetime Cap is required.", @"sRLifeCapR", "Forms/GoodFaithEstimate2010.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210048", new FriendlyError(@"Graduated Payment Rate is required.", @"sGradPmtR", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210049", new FriendlyError(@"Graduated Payment Term is required.", @"sGradPmtYrs", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"210070", new FriendlyError(@"FHA Upfront MI Premium is required when MI and Funding Fee Cash Credit Amount is provided.", @"sFfUfmipR", "LoanInfo.aspx?pg=1")},
            {"210071", new FriendlyError(@"Either Borrower Funding Fee or MI and Funding Fee Total is required when MI and Funding Fee Cash Credit Amount is provided.", @"sFfUfmipR", "LoanInfo.aspx?pg=1")},
            {"210072", new FriendlyError(@"Either MI Initial Premium Rate OR MI And Funding Fee Total is required when MI And Funding Fee Cash Credit Amount is provided.", @"sFfUfmip1003", "LoanInfo.aspx?pg=1")},
            {"210073", new FriendlyError(@"MI Initial Premium Rate Duration Months must be provided.", @"sProMInsMon", "LoanInfo.aspx?pg=1")},
            {"250000", new FriendlyError(@"The property zipcode did not match any known zipcodes for the specified state.", @"sSpZip", "Underwriting/PmlReviewInfo.aspx")},
            {"250001", new FriendlyError(@"Subject property type must be either ""Attached"", ""Detached"", or ""Other"" when number of units is greater than 1.", @"sGseSpT", "LoanInfo.aspx?pg=0")},
            {"250002", new FriendlyError(@"If subject property type is either ""Attached"" or ""Detached"" then number of units cannot exceed 4.", @"sUnitsNum", "Underwriting/PmlReviewInfo.aspx")},
            {"250003", new FriendlyError(@"If number of units is 5 or more, then subject property type must be ""Other""", @"sGseSpT", "LoanInfo.aspx?pg=0")},
            {"250005", new FriendlyError(@"The system cannot perform audits on loans created 90 days or more in the future. For more information, please contact ComplianceEase Client Support.")},
            {"250006", new FriendlyError(@"Estimated Closing Date cannot occur before Loan Application Date", @"sEstCloseD", "Status/General.aspx")},
            {"250007", new FriendlyError(@"Closing Date cannot be before Loan Application Date", @"sEstCloseD", "Status/General.aspx")},
            {"250008", new FriendlyError(@"Estimated Closing Date cannot occur before GFE Disclosure Date", @"sEstCloseD", "Status/General.aspx")},
            {"250009", new FriendlyError(@"Closing Date cannot be before GFE Disclosure Date", @"sEstCloseD", "Status/General.aspx")},
            {"250010", new FriendlyError(@"Estimated Closing Date cannot occur before TIL Disclosure Date", @"sEstCloseD", "Status/General.aspx")},
            {"250011", new FriendlyError(@"Closing Date cannot occur before TIL Disclosure Date.", @"sEstCloseD", "Status/General.aspx")},
            {"250015", new FriendlyError(@"Disbursement Date cannot be before Loan Application Date", @"sSchedFundD", "Status/General.aspx")},
            {"250016", new FriendlyError(@"Disbursement Date cannot be before GFE Disclosure Date", @"sSchedFundD", "Status/General.aspx")},
            {"250017", new FriendlyError(@"Disbursement Date cannot be before TIL Disclosure Date", @"sSchedFundD", "Status/General.aspx")},
            {"250019", new FriendlyError(@"Disbursement Date cannot occur before Estimated Closing Date", @"sSchedFundD", "Status/General.aspx")},
            {"250020", new FriendlyError(@"Disbursement Date cannot be before Closing Date", @"sSchedFundD", "Status/General.aspx")},
            {"250022", new FriendlyError(@"TIL Prepared Date cannot occur before App Received by Lender Date")},
            {"250023", new FriendlyError(@"Estimated Closing Date cannot occur before Rate Lock Date", @"sEstCloseD", "Status/General.aspx")},
            {"250024", new FriendlyError(@"Rate Lock Date cannot be after Closing Date", @"sRLckdD", "Status/General.aspx")},
            {"250025", new FriendlyError(@"Rate Lock Date cannot be after Disbursement Date", @"sRLckdD", "Status/General.aspx")},
            {"250026", new FriendlyError(@"Mortgage Broker Discount Fee is not permitted with loans within the current jurisdiction.", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250027", new FriendlyError(@"Interim Interest Days must be greater than zero ", @"sIPiaDy", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250028", new FriendlyError(@"Interim Interest Days must be less than zero ", @"sIPiaDy", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250031", new FriendlyError(@"The sum of the Draw period and the Repay period cannot exceed 1080", @"sHelocDraw_rep", "AdditionalHELOC.aspx")},
            {"250035", new FriendlyError(@"The Termination Fee cannot exceed the Line Amount", @"sHelocTerminationFee_rep", "AdditionalHELOC.aspx")},
            {"250037", new FriendlyError(@"The total amount of fees provided exceeds Base Loan Amount")},
            {"250038", new FriendlyError(@"Base Loan Amount, less any financed fees, cannot be lower than $300", @"sLAmtCalc", "Underwriting/PmlReviewInfo.aspx")},
            {"250039", new FriendlyError(@"Loan Original Maturity Term cannot exceed Loan Amortization Term", @"sDue", "LoanInfo.aspx?pg=0")},
            {"250040", new FriendlyError(@"The QM Par Rate cannot be less than the Note Rate Percent", @"sQMParR", "Underwriting/QM/Main.aspx?pg=0")},
            {"250042", new FriendlyError(@"Lender License Type is set to ""Default"", however no license type has been configured in your company profile for the specified state and lien priority type.")},
            {"250043", new FriendlyError(@"Lender License Type is not applicable to loans made in the specified state.")},
            {"250044", new FriendlyError(@"The licensing state has opted out of DIDMCA. The value of Lender DIDMCA Exemption Indicator must be ""N"".")},
            {"250045", new FriendlyError(@"If Lender DIDMCA Exemption Indicator is ""Y"", then the  Lien Priority must be ""FirstLien"".", @"sLienPosT", "Status/NMLSCallReport.aspx")},
            {"250047", new FriendlyError(@"Original LTV Ration cannot exceed Original CLTV Ratio", @"sLtvR", "Underwriting/PmlReviewInfo.aspx")},
            {"250050", new FriendlyError(@"Prepayment Penalty Term cannot exceed Loan Term", @"sPrepmtPeriodMonths", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"250052", new FriendlyError(@"Maximum Prepayment Penalty cannot exceed the value of Base Loan amount minus any financed fees", @"sGfeMaxPpmtPenaltyAmt", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/LoanEstimate.aspx")},
            {"250053", new FriendlyError(@"Construction to Permanent loans are not supported for FHA ", @"sLPurposeT", "Underwriting/PmlReviewInfo.aspx")},
            {"250054", new FriendlyError(@"Interim Interest fees are prohibited on Construction to Permanent loans", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250055", new FriendlyError(@"The total of all prepaid finance charges plus the estimated construction interest must be less than the base loan amount", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250056", new FriendlyError(@"Construction Only loans are not supported for FHA or VA", @"sLPurposeT", "Underwriting/PmlReviewInfo.aspx")},
            {"250057", new FriendlyError(@"Interim Interest fees are prohibited on Construction Only loans", @"GFE Page", "Forms/GoodFaithEstimate2010.aspx", "Test/RActiveGFE.aspx", "Disclosure/BorrowerClosingCosts.aspx")},
            {"250058", new FriendlyError(@"The total of all prepaid finance charges must be less than one-half of the Base Loan Amount")},
            {"250059", new FriendlyError(@"The sum of  ARM IndexMarginPercent and ARM IndexCurrentValuePercent cannot be less than 0.", @"sRAdjMarginR", "Status/ViewBrokerRateLock.aspx")},
            {"250060", new FriendlyError(@"Lifetime Cap Rate cannot be less than Lifetime Floor Percent.", @"sRLifeCapR", "Forms/GoodFaithEstimate2010.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"250065", new FriendlyError(@"Graduated Payment Term cannot exceed Loan Term", @"sGradPmtYrs", "Forms/TruthInLending.aspx", "Forms/TruthInLending.aspx", "Forms/LoanTerms.aspx")},
            {"250085", new FriendlyError(@"Interest Only Term cannot exceed Loan Term", @"sIOnlyMon", "Underwriting/PmlReviewInfo.aspx")},
            {"250103", new FriendlyError(@"MI and Funding Fee Cash Credit Amount cannot exceed MI and Funding Fee Total Amount.", @"sUfCashPd", "LoanInfo.aspx?pg=1")},
            {"250104", new FriendlyError(@"MI and Funding Fee Financed Amount cannot exceed MI and Funding Fee Total Amount.", @"sFfUfmipFinanced", "LoanInfo.aspx?pg=0")},
        };
        #endregion FriendlyErrorMappings

        private static object fieldLock = new object();
        private static CSelectStatementProvider selectStatementProvider;

        public static CSelectStatementProvider SelectStatementProvider
        {
            get
            {
                lock (fieldLock)
                {
                    if (selectStatementProvider != null)
                    {
                        return selectStatementProvider;
                    }

                    HashSet<string> fields = new HashSet<string>(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ComplianceEaseExporter)));
                    fields.UnionWith(CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release);
                    selectStatementProvider = CSelectStatementProvider.GetProviderForTargets(fields, expandTriggersIfNeeded: false);

                    return selectStatementProvider;
                }
            }
        }

        /// <summary>
        /// goal is to log additional info, but not bog down any export queue with logging.
        /// </summary>
        private static void LogProvisionally(string info, string earlyExitEvent)
        {
            IEnumerable<string> eventsToBeLogged = ConstStage.ComplianceEaseEarlyExitEventsToLog;
            if (eventsToBeLogged.Contains(earlyExitEvent,StringComparer.OrdinalIgnoreCase))
            {
                Tools.LogInfo(info);
            }
        }

        public static bool Export(Guid sLId, string userName, string password, E_TransmittalDataComplianceAuditType auditType, out ServiceFault serviceFault, bool asynchronous, int expectedLoanVersion)
        {
            bool didUpdate = false;
            return Export(sLId, userName, password, auditType, out serviceFault, asynchronous, expectedLoanVersion, false, out didUpdate);
        }

        public static bool Export(Guid sLId, string userName, string password, E_TransmittalDataComplianceAuditType auditType, 
            out ServiceFault serviceFault, bool asynchronous, int expectedLoanVersion, bool ignorePreviousChecksum, out bool didUpdate)
        {
            serviceFault = null;

            bool hasError = false;
            ComplianceAuditService service = new ComplianceAuditService();

            TransactionType transaction = new TransactionType();
            transaction.Source = "LendingQB";

            SecurityType securityType = new SecurityType();
            securityType.Username = userName;
            securityType.Password = password;

            PreferredResponseType preferredResponse = new PreferredResponseType();
            preferredResponse.Type = PreferredResponseTypeType.Full;
            preferredResponse.Format = new PreferredResponseTypeFormat[] { PreferredResponseTypeFormat.PDF };

            service.PreferredResponse = preferredResponse;
            service.Security = securityType;
            service.Transaction = transaction;

            string m_sComplianceEaseErrorMessage = "";
            string m_sComplianceEaseChecksumValue = "";
            string m_sComplianceEaseChecksum = "";
            string m_sComplianceEaseStatus;
            string m_sComplianceEaseId = null;

            didUpdate = false;
            Guid brokerId = Guid.Empty;
            string existingComplianceEaseId = null;
            try
            {
                ComplianceEaseExporter exporter = new ComplianceEaseExporter(sLId, auditType);
                brokerId = exporter.m_dataLoan.sBrokerId;
                existingComplianceEaseId = exporter.m_dataLoan.sComplianceEaseId;

                //IR - OPM 225746, this is needed to allow loan updating if CE is ran manually.
                if (expectedLoanVersion == -1 && !asynchronous)
                {
                    expectedLoanVersion = exporter.m_dataLoan.sFileVersion;
                }

                //OPM 225746: Use BETA URL for CE test user and/or test loan files.
                if (exporter.m_dataLoan.sLoanFileT == E_sLoanFileT.Test || userName == ConstStage.ComplianceEaseTestUserAccount)
                {
                    service.Url = ConstStage.ComplianceEaseBetaUrl;
                }
                else
                {
                    service.Url = ConstStage.ComplianceEaseProductionUrl;
                }

                if (asynchronous)
                {
                  
                    BrokerDB brokerDb = BrokerDB.RetrieveById(exporter.m_dataLoan.sBrokerId);

                    if (String.IsNullOrEmpty(brokerDb.ComplianceEaseUserName))
                    {
                        return false;
                    }

                    if (!exporter.m_dataLoan.BrokerDB.IsEnableComplianceEaseIntegration)
                    {
                        Tools.LogError($"[CEQUEUE] The ComplianceEase integration is not enabled for broker {exporter.m_dataLoan.sBrokerId.ToString()}.");
                        clearEntry(sLId, expectedLoanVersion);
                        return false;
                    }

                    if (!brokerDb.ComplianceEaseUserName.Equals(userName) &&
                        !userName.Equals(ConstStage.ComplianceEasePTMTestLogin))
                    {
                        Tools.LogError(
                            String.Concat("[CEQUEUE] CE Username does not match loan broker settings ", 
                            userName, " ", ConstStage.ComplianceEasePTMLogin, " brokerid ", exporter.m_dataLoan.sBrokerId.ToString(), " loan id ", sLId.ToString()));
                        clearEntry(sLId, expectedLoanVersion);
                        return false;
                    }

                    if (exporter.m_dataLoan.BrokerDB.BillingVersion != E_BrokerBillingVersion.PerTransaction ||
                        !exporter.m_dataLoan.BrokerDB.IsEnablePTMComplianceEaseIndicator)
                    {
                        Tools.LogError(" [CEQUEUE] Ignoring CE request for broker " + exporter.m_dataLoan.BrokerDB.CustomerCode);
                        //they no longer have ptm enabled dont bother.
                        clearEntry(sLId, expectedLoanVersion);
                        return false;
                    }

                    var reasonNotSending = "not sending slid " + sLId + "  to ComplianceEase because ";
                    if (exporter.m_dataLoan.IsTemplate)
                    {
                        reasonNotSending += " the loan is a template";
                        LogProvisionally(reasonNotSending, ConstAppDavid.ComplianceEaseEarlyExitEvent_IsTemplate);

                        clearEntry(sLId, expectedLoanVersion);
                        return false;
                    }

                    if (exporter.m_dataLoan.BrokerDB.ComplianceEaseExportConfig.IgnoredLoanStatuses.Contains(exporter.m_dataLoan.sStatusT))
                    {
                        reasonNotSending += " the loan is an ignored status.";
                        LogProvisionally(reasonNotSending, ConstAppDavid.ComplianceEaseEarlyExitEvent_IsIgnoreLoanStatus);

                        clearEntry(sLId, expectedLoanVersion);
                        return false;
                    }

                    if (exporter.m_dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
                    {
                        reasonNotSending += " the initial loan estimate date is associated with a non-disclosed archive.";
                        LogProvisionally(reasonNotSending, ConstAppDavid.ComplianceEaseEarlyExitEvent_InvalidInitialLoanEstimate);

                        clearEntry(sLId, expectedLoanVersion);
                        updateLoan(
                            brokerId,
                            sLId,
                            expectedLoanVersion,
                            exporter.m_dataLoan.sComplianceEaseErrorMessage,
                            exporter.m_dataLoan.sComplianceEaseChecksum,
                            "ERRORS",
                            null,
                            existingComplianceEaseId);
                        return false;
                    }

                    if (exporter.m_dataLoan.sDocMagicHaveClosingDocsBeenGenerated)
                    {
                        exporter.m_auditType = E_TransmittalDataComplianceAuditType.PostClose;
                    }
                    else
                    {
                        exporter.m_auditType = E_TransmittalDataComplianceAuditType.PreClose;
                    }
                }

                AuditLoanDataType auditLoanData = new AuditLoanDataType();
                auditLoanData.RequestXML = exporter.Export();
                //auditLoanData.RequestXML = TextFileHelper.ReadFile(@"c:\LOTemp\testCE.xml"); // use this if you want to fiddle with the xml.

                if (!exporter.m_dataLoan.sBlockComplianceEaseIdFromRequest && !string.IsNullOrEmpty(exporter.m_dataLoan.sComplianceEaseId))
                {
                    auditLoanData.CEID = exporter.m_dataLoan.sComplianceEaseId;
                }

                SHA256 sha = new SHA256Managed();

                var bytes = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(auditLoanData.RequestXML));
                StringBuilder sb = new StringBuilder();
                foreach (byte b in bytes)
                {
                    sb.Append(b.ToString("X2"));
                }

                if (asynchronous 
                    && exporter.m_dataLoan.sComplianceEaseChecksumValue.Equals(sb.ToString())
                    && false == ignorePreviousChecksum)
                {
                    m_sComplianceEaseStatus = exporter.m_dataLoan.sComplianceEaseLastStatus;
                    m_sComplianceEaseChecksum = exporter.m_dataLoan.sComplianceEaseChecksum;
                    m_sComplianceEaseErrorMessage = exporter.m_dataLoan.sComplianceEaseErrorMessage;

                    updateLoan(exporter.m_dataLoan.sBrokerId,  sLId, expectedLoanVersion, m_sComplianceEaseErrorMessage, m_sComplianceEaseChecksum, m_sComplianceEaseStatus, null, existingComplianceEaseId);

                    var reasonNotSending = string.Format("not sending slid {0} to ComplianceEase because the checksum didn't change. Current request: {1} dataLoan.sComplianceEaseChecksumValue: {2}", sLId, sb.ToString(), exporter.m_dataLoan.sComplianceEaseChecksumValue);
                    LogProvisionally(reasonNotSending, ConstAppDavid.ComplianceEaseEarlyExitEvent_IsChecksumValueIdentical);
                    return false;
                }

                m_sComplianceEaseChecksumValue = sb.ToString();

                Tools.LogInfo("Sending ComplianceEase audit data for loan [" + sLId +
                              "] at " + DateTime.Now + ", sent xml is: " +
                              Environment.NewLine + auditLoanData.RequestXML);

                AuditLoanDataResponseType auditLoanDataResponse = AuditLoanDataWithRetry(service, auditLoanData);

                //AuditLoanDataResponseType is already deserialized at this point, so we have to serialize it again.
                using (StringWriter sw = new StringWriter())
                {
                    new XmlSerializer(typeof(AuditSummaryType)).Serialize(sw, auditLoanDataResponse.AuditSummary);
                    Tools.LogInfo("Received ComplianceEase audit response for loan [" + sLId +
                                  "] at " + DateTime.Now + ", response xml is: " +
                                  Environment.NewLine + sw.ToString());
                }

                // OPM 240571 - Set compliance ease ID on loan if it has not yet been set.
                if (string.IsNullOrEmpty(exporter.m_dataLoan.sComplianceEaseId))
                {
                    m_sComplianceEaseId = auditLoanDataResponse.AuditSummary.CEID;
                }

                m_sComplianceEaseStatus = auditLoanDataResponse.AuditSummary.RiskIndicator;

                // Save PDF  auditLoanDataResponse.AuditReport.EmbeddedFile.Document
                var repository = EDocumentRepository.GetSystemRepository(exporter.m_dataLoan.BrokerDB.BrokerID);
                foreach (var auditReport in auditLoanDataResponse.AuditReport)
                {
                    byte[] buffer = auditReport.EmbeddedFile.Document;

                    if (asynchronous)
                    {
                        string pdfPath = TempFileUtils.NewTempFilePath() + "docmagic.pdf";
                        BinaryFileHelper.WriteAllBytes(pdfPath, buffer);
                        buffer = null;

                        int docTypeId = 0;
                        var key = GeneratedKeyForRequestOrThreadCache.CEDocTypeKey(exporter.m_dataLoan.sBrokerId);
                        object cacheDocTypeId = CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key);

                        if (cacheDocTypeId == null || ((int)cacheDocTypeId) == 0)
                        {
                            IEnumerable<DocType> docTypes = EDocumentDocType.GetDocTypesByBroker(exporter.m_dataLoan.BrokerDB.BrokerID, E_EnforceFolderPermissions.True);
                            foreach (DocType dt in docTypes)
                            {
                                if (dt.DocTypeName.ToUpper().Equals("COMPLIANCEREPORT"))
                                {
                                    docTypeId = int.Parse(dt.DocTypeId);
                                    break;
                                }
                            }

                            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, docTypeId);
                        }
                        else
                        {
                            docTypeId = (int)cacheDocTypeId;
                        }

                        if (docTypeId == 0)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "COMPLIANCEREPORT doc type missing.");
                        }

                        IList<EDocument> docList = repository.GetDocumentsByLoanId(sLId);
                        string nonDestructivePdfDocumentId = string.Empty;
                        EDocument edoc;
                        try
                        {
                            edoc = docList.First(d => d.DocTypeName == "COMPLIANCEREPORT");
                            nonDestructivePdfDocumentId = edoc.OverwritePDFContentForExistingDocs(pdfPath);
                        }
                        catch (InvalidOperationException)
                        {
                            try
                            {
                                docList = repository.GetDeletedDocumentsByLoanId(sLId, exporter.m_dataLoan.BrokerDB.BrokerID);
                                edoc = docList.First(d => d.DocTypeName == "COMPLIANCEREPORT");
                                EDocument.RestoreDocument(exporter.m_dataLoan.BrokerDB.BrokerID, edoc.DocumentId, new Guid(ConstStage.ComplianceEasePTMEditorUserId), "Report update");
                                edoc = repository.GetDocumentById(edoc.DocumentId);
                                nonDestructivePdfDocumentId = edoc.OverwritePDFContentForExistingDocs(pdfPath);
                            }
                            catch (InvalidOperationException)
                            {
                                edoc = repository.CreateDocument(E_EDocumentSource.GeneratedDocs);
                                edoc.UpdatePDFContentOnSave(pdfPath);
                            }
                        }

                        edoc.AppId = exporter.m_dataLoan.GetAppData(0).aAppId;

                        edoc.LoanId = sLId;
                        edoc.DocumentTypeId = docTypeId;
                        edoc.IsUploadedByPmlUser = PrincipalFactory.CurrentPrincipal.Type == "P";
                        edoc.PublicDescription = "Autosaved: " + DateTime.Now.ToShortTimeString();
                        edoc.Save(new Guid(ConstStage.ComplianceEasePTMEditorUserId));

                        if (!string.IsNullOrEmpty(nonDestructivePdfDocumentId))
                        {
                            // 7/17/2017 - dd - OPM 453261 - Enqueue the document for pdf rasterizer after save. Otherwise the meta data from pdf rasterizer will get override in save.
                            EDocument.EnqueueNonDestructivePdfRasterizeRequest(edoc.BrokerId, edoc.DocumentId, nonDestructivePdfDocumentId, EDocument.E_NonDestructiveSourceT.OverwriteExisting, EDocument.POST_ACTION_UPDATE_METADATA_EXCLUDE_ORIGINAL);
                            EDocument.UpdateImageStatus(edoc.BrokerId, edoc.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
                        }
                    }
                    else
                    {
                        var fileDbKey = sLId.ToString("N") + "_COMPLIANCE_EASE_REPORT_PDF";
                        FileDBTools.WriteData(E_FileDB.Normal, fileDbKey, buffer);
                        AutoSaveDocTypeFactory.SavePdfDoc(E_AutoSavePage.ComplianceEase, fileDbKey, E_FileDB.Normal,
                            sLId, exporter.m_dataLoan.GetAppData(0).aAppId, PrincipalFactory.CurrentPrincipal);
                    }
                }

                didUpdate = true;
            }
            catch (SoapException exc)
            {
                serviceFault = new ServiceFault(exc.Detail.FirstChild.OuterXml);
                m_sComplianceEaseErrorMessage = exc.Detail.FirstChild.OuterXml;
                Tools.LogInfo("Received an error during ComplianceEase audit for loan [" + sLId +
                                  "] at " + DateTime.Now + ", error xml is: " +
                                  Environment.NewLine + m_sComplianceEaseErrorMessage);
                m_sComplianceEaseStatus = "ERRORS";
                hasError = true;
            }

            m_sComplianceEaseChecksum = m_sComplianceEaseStatus + ":" + m_sComplianceEaseChecksumValue;
            updateLoan(brokerId, sLId, expectedLoanVersion, m_sComplianceEaseErrorMessage, m_sComplianceEaseChecksum, m_sComplianceEaseStatus, m_sComplianceEaseId, existingComplianceEaseId);

            return hasError;
        }

        private static AuditLoanDataResponseType AuditLoanDataWithRetry(ComplianceAuditService service, AuditLoanDataType auditLoanData)
        {
            int retryCount = 3;
            while (true)
            {
                try
                {
                    return service.AuditLoanData(auditLoanData);
                }
                catch (System.Net.WebException exc)
                {
                    if (--retryCount <= 0)
                        throw exc;
                    Tools.SleepAndWakeupRandomlyWithin(2000, 3000);
                }
            }
        }

        private static void updateLoan(Guid brokerId, Guid sLId, int expectedLoanVersion, string m_sComplianceEaseErrorMessage, string m_sComplianceEaseChecksum, string m_sComplianceEaseStatus, string m_sComplianceEaseId, string existingComplianceEaseId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@sLId", sLId));
            parameters.Add(new SqlParameter("@sFileVersion", expectedLoanVersion));
            parameters.Add(new SqlParameter("@sComplianceEaseErrorMessage", m_sComplianceEaseErrorMessage));
            parameters.Add(new SqlParameter("@sComplianceEaseChecksum", m_sComplianceEaseChecksum));
            parameters.Add(new SqlParameter("@sComplianceEaseStatus", m_sComplianceEaseStatus));

            bool updatingComplianceEaseId = false;
            if (!string.IsNullOrEmpty(m_sComplianceEaseId))
            {
                parameters.Add(new SqlParameter("@sComplianceEaseId", m_sComplianceEaseId));
                updatingComplianceEaseId = true;
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateComplianceEaseData", 1, parameters);

            Audit.TrackedField trackedField;
            if (updatingComplianceEaseId
                && m_sComplianceEaseId != existingComplianceEaseId
                && Audit.TrackedField.Retrieve().TryGetValue(nameof(CPageData.sComplianceEaseId), out trackedField))
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                Audit.AbstractAuditItem audit = new Audit.TrackedFieldModifiedAuditItem(
                        trackedField,
                        principal?.UserId ?? Guid.Empty,
                        principal?.DisplayName ?? "System",
                        oldValue: existingComplianceEaseId,
                        newValue: m_sComplianceEaseId,
                        changedAt: typeof(ComplianceEaseExporter).FullName,
                        isautomatic: false);
                Audit.AuditManager.RecordAudit(sLId, audit);
            }

            clearEntry(sLId, expectedLoanVersion);
        }

        private static void clearEntry(Guid sLId, int expectedLoanVersion)
        {
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.ComplianceEaseAutoSubmitSecondaryQueue);
            foreach (DBMessage msg in mQ.Entries)
            {
                if (msg.Subject1.Equals(sLId.ToString()) && msg.Subject2.Equals(expectedLoanVersion.ToString()))
                {
                    mQ.ReceiveByIdSynchronized(msg.Id);
                    return;
                }
            }
        }

        private CPageData GetPageData(Guid loanid)
        {
            return new CFullAccessPageData(loanid, nameof(ComplianceEaseExporter), SelectStatementProvider);
        }

        private CPageData m_dataLoan = null;
        private E_TransmittalDataComplianceAuditType m_auditType;

        /// <summary>
        /// Indicates whether to use the old or new datalayer for exporting. Dependent on sClosingCostFeeVersionT.
        /// </summary>
        private bool use2015DataLayer = false;

        /// <summary>
        /// Indicates whether to override Section G and Hud 902-903 fee paid to Lender/Broker and set to Default instead. New Data layer only.
        /// </summary>
        private bool allowOverridePaidToForHOEPATest = false;

        /// <summary>
        /// Indicates whether to export GFE or TRID data. Dependent on sDisclosureRegulationT.
        /// </summary>
        private bool exportIntegratedDisclosureData = false;

        public ComplianceEaseExporter(Guid sLId, E_TransmittalDataComplianceAuditType auditType)
        {
            m_dataLoan = GetPageData(sLId);

            m_dataLoan.InitLoad();
            //if (m_dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && m_dataLoan.LastDisclosedGFEArchive != null)  // disable opm 142081
            //{
            //    m_dataLoan.ApplyGFEArchive(m_dataLoan.LastDisclosedGFEArchive);
            //}
            
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            
            m_auditType = auditType;

            exportIntegratedDisclosureData = m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            use2015DataLayer = exportIntegratedDisclosureData || m_dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015;
            this.allowOverridePaidToForHOEPATest = use2015DataLayer && m_dataLoan.BrokerDB.ComplianceEaseAllowOverridePaidToForHOEPAFeeTest;
        }

        public string Export()
        {
            AcsRequest acsRequest = CreateAcsRequest();

            StringBuilder sb = new StringBuilder();
            byte[] bytes = null;
            int contentLength = 0;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII))
                {
                    acsRequest.WriteXml(writer);
                    writer.Flush();
                    contentLength = (int)stream.Position;
                }

                bytes = stream.GetBuffer();
            }

            return System.Text.Encoding.ASCII.GetString(bytes, 0, contentLength);
        }

        private AcsRequest CreateAcsRequest()
        {
            AcsRequest _AcsRequest = new AcsRequest();

            _AcsRequest.Loan = CreateLoan();
            _AcsRequest.AcsExtension = CreateAcsExtension();
            return _AcsRequest;
        }

        private ComplianceEaseExportConfiguration m_ExportConfig
        {
            get
            {
                return m_dataLoan.BrokerDB.ComplianceEaseExportConfig;
            }
        }

        private AcsExtension CreateAcsExtension()
        {
            AcsExtension _AcsExtension = new AcsExtension();

            _AcsExtension.AdditionalCaseData = CreateAdditionalCaseData();

            if (this.m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || this.m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                _AcsExtension.ConstructionLoanDetails = CreateConstructionLoanDetails();
            }

            if (m_dataLoan.BrokerDB.BillingVersion != E_BrokerBillingVersion.PerTransaction
                || (m_dataLoan.BrokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction
                && !m_dataLoan.BrokerDB.IsEnablePTMComplianceEaseIndicator))
            {
                _AcsExtension.InvestorList.Add(CreateInvestor());
            }
            else
            {
                foreach (string name in m_dataLoan.BrokerDB.ComplianceEaseExportConfig.LendingPolicies)
                {
                    _AcsExtension.InvestorList.Add(CreateInvestor(name));
                }
            }

            _AcsExtension.LenderDetails = CreateLenderDetails();
            _AcsExtension.LoanOriginator = CreateLoanOriginator();
            _AcsExtension.LoanProductDetails = CreateLoanProductDetails();
            _AcsExtension.LoanPurposeDetails = CreateLoanPurposeDetails();
            _AcsExtension.Mi = CreateMi();
            _AcsExtension.MortgageLoanDetails = CreateMortgageLoanDetails();
            // _AcsExtension.PrepaymentPenaltyProgram = CreatePrepaymentPenaltyProgram();

            if (exportIntegratedDisclosureData)
            {
                _AcsExtension.IntegratedDisclosure = CreateIntegratedDisclosure();
            }

            if (use2015DataLayer)
            {
                PopulateRespaFeeList_2015DataLayer(_AcsExtension.RespaFeeList);
            }
            else
            {
                PopulateRespaFeeList(_AcsExtension.RespaFeeList);
            }
           
            _AcsExtension.SubjectProperty = CreateSubjectProperty();

            return _AcsExtension;
        }

        /// <summary>
        /// Populates the RESPA fee list with data using the pre-TRID format.
        /// </summary>
        /// <param name="respaFeeList">The fee list to be populated.</param>
        private void PopulateRespaFeeList(List<RespaFee> respaFeeList)
        {
            ComplianceEaseExportConfiguration ExportConfig = m_dataLoan.BrokerDB.ComplianceEaseExportConfig;

            #region 800 Fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LoanOriginationFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sLOrigF_rep, m_dataLoan.sLOrigFProps, string.Empty, ExportConfig.sLOrigFPaidTo, true));

            E_RespaFeeType originatorRespa;
            E_RespaFeeGfeAggregationType originatorAggreg = E_RespaFeeGfeAggregationType.Undefined;
            string originatorTypeOtherDescription = string.Empty;

            if (m_dataLoan.sGfeOriginatorCompFProps_PdByT == LosConvert.BORR_PAID_OUTOFPOCKET
             || m_dataLoan.sGfeOriginatorCompFProps_PdByT == LosConvert.BORR_PAID_FINANCED)
            {
                if (m_dataLoan.sGfeOriginatorCompFProps_ToBroker)
                {
                    originatorRespa = E_RespaFeeType.MortgageBrokerFee;
                }
                else
                {
                    originatorRespa = E_RespaFeeType.Other;
                    originatorAggreg = E_RespaFeeGfeAggregationType.OurOriginationCharge;
                    originatorTypeOtherDescription = "ORIGINATOR COMPENSATION";
                }
            }
            else
            {
                originatorRespa = E_RespaFeeType.MortgageBrokerFeeIndirectPOC;
            }

            var originatorComp = CreateRespaFee(
                originatorRespa,
                E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                originatorAggreg,
                originatorTypeOtherDescription,
                m_dataLoan.sGfeOriginatorCompF_rep,
                m_dataLoan.sGfeOriginatorCompFProps,
                "",
                m_dataLoan.sGfeOriginatorCompFProps_ToBroker ? E_RespaFeePaidToType.Broker : E_RespaFeePaidToType.Lender,
                false);

            if (originatorComp != null)
            {
                if (m_dataLoan.sGfeOriginatorCompFProps_ToBroker)
                {
                    originatorComp.PaidToType = E_RespaFeePaidToType.Broker;
                }
                else
                {
                    originatorComp.PaidToType = E_RespaFeePaidToType.Lender;
                }

                respaFeeList.Add(originatorComp);
            }

            // No longer necessary according to case 75841
            /*if (m_dataLoan.sLDiscnt < 0)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LenderCreditForTheInterestRateChosen, "", m_dataLoan.sLDiscnt_rep, m_dataLoan.sLDiscntProps, string.Empty));
            }
            else
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LoanDiscountPoints, "", m_dataLoan.sLDiscnt_rep, m_dataLoan.sLDiscntProps, string.Empty));
            }*/
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.AppraisalFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sApprF_rep, m_dataLoan.sApprFProps, m_dataLoan.sApprFPaidTo, ExportConfig.sApprFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.CreditReportFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sCrF_rep, m_dataLoan.sCrFProps, m_dataLoan.sCrFPaidTo, ExportConfig.sCrFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LenderInspectionFeePriorToClosing, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sInspectF_rep, m_dataLoan.sInspectFProps, m_dataLoan.sInspectFPaidTo, ExportConfig.sInspectFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.MortgageBrokerFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sMBrokF_rep, m_dataLoan.sMBrokFProps, string.Empty, ExportConfig.sMBrokFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.ProcessingFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sProcF_rep, m_dataLoan.sProcFProps, m_dataLoan.sProcFPaidTo, ExportConfig.sProcFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.UnderwritingFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sUwF_rep, m_dataLoan.sUwFProps, m_dataLoan.sUwFPaidTo, ExportConfig.sUwFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.WireTransferFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sWireF_rep, m_dataLoan.sWireFProps, m_dataLoan.sWireFPaidTo, ExportConfig.sWireFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.TaxRelatedServiceFee, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sTxServF_rep, m_dataLoan.sTxServFProps, m_dataLoan.sTxServFPaidTo, ExportConfig.sTxServFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._800_LoanFees, Convert(m_dataLoan.s800U1FGfeSection), m_dataLoan.s800U1FDesc, m_dataLoan.s800U1F_rep, m_dataLoan.s800U1FProps, m_dataLoan.s800U1FPaidTo, ExportConfig.s800RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._800_LoanFees, Convert(m_dataLoan.s800U2FGfeSection), m_dataLoan.s800U2FDesc, m_dataLoan.s800U2F_rep, m_dataLoan.s800U2FProps, m_dataLoan.s800U2FPaidTo, ExportConfig.s800RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._800_LoanFees, Convert(m_dataLoan.s800U3FGfeSection), m_dataLoan.s800U3FDesc, m_dataLoan.s800U3F_rep, m_dataLoan.s800U3FProps, m_dataLoan.s800U3FPaidTo, ExportConfig.s800RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._800_LoanFees, Convert(m_dataLoan.s800U4FGfeSection), m_dataLoan.s800U4FDesc, m_dataLoan.s800U4F_rep, m_dataLoan.s800U4FProps, m_dataLoan.s800U4FPaidTo, ExportConfig.s800RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._800_LoanFees, Convert(m_dataLoan.s800U5FGfeSection), m_dataLoan.s800U5FDesc, m_dataLoan.s800U5F_rep, m_dataLoan.s800U5FProps, m_dataLoan.s800U5FPaidTo, ExportConfig.s800RestPaidTo, false));

            /*RespaFee sLDiscntRespaFee = CreateRespaFee(E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal, E_RespaFeeRespaSectionClassificationType._800_LoanFees, E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, "", m_dataLoan.sLDiscnt_rep, m_dataLoan.sLDiscntProps, string.Empty, ExportConfig.sLDiscntPaidTo);
            if (sLDiscntRespaFee != null)
            {
                // As per case 81170, the paid by for this field the same as sGfeCreditLenderPaidItemF, sGfeLenderCreditF and sGfeDiscountPointF
                // if they are the same. If not, the value should be Buyer. sGfeCreditLenderPaidItemF and sGfeLenderCreditF are both always lender paid.
                if (LosConvert.GfeItemProps_Payer(m_dataLoan.sGfeDiscountPointFProps) == LosConvert.LENDER_PAID)
                {
                    sLDiscntRespaFee.Payment.PaidByType = E_RespaFeePaymentPaidByType.Lender;
                }
                else
                {
                    sLDiscntRespaFee.Payment.PaidByType = E_RespaFeePaymentPaidByType.Buyer;
                }
                respaFeeList.Add(sLDiscntRespaFee);
            }*/
            RespaFee sGfeCreditLenderPaidItemRespaFee = CreateRespaFee(E_RespaFeeType.LenderCreditNotForTheInterestRateChosen, E_RespaFeeRespaSectionClassificationType._800_LoanFees, E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, "", m_dataLoan.sGfeCreditLenderPaidItemFComplianceEase_rep, 0, string.Empty, ExportConfig.sGfeCreditLenderPaidItemFPaidTo, false);
            if (sGfeCreditLenderPaidItemRespaFee != null)
            {
                sGfeCreditLenderPaidItemRespaFee.PaymentList[0].PaidByType = E_RespaFeePaymentPaidByType.Lender;
                respaFeeList.Add(sGfeCreditLenderPaidItemRespaFee);
            }

            RespaFee sGfeLenderCreditRespaFee = CreateRespaFee(E_RespaFeeType.LenderCreditForTheInterestRateChosen, E_RespaFeeRespaSectionClassificationType._800_LoanFees, E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, "", m_dataLoan.sGfeLenderCreditFComplianceEase_rep, m_dataLoan.sGfeLenderCreditFProps, string.Empty, ExportConfig.sGfeLenderCreditFPaidTo, false);
            if (sGfeLenderCreditRespaFee != null)
            {
                sGfeLenderCreditRespaFee.PaymentList[0].PaidByType = E_RespaFeePaymentPaidByType.Lender;
                respaFeeList.Add(sGfeLenderCreditRespaFee);
            }

            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LoanDiscountPoints, E_RespaFeeRespaSectionClassificationType._800_LoanFees, E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, "", m_dataLoan.sGfeDiscountPointFComplianceEase_rep, m_dataLoan.sGfeDiscountPointFProps, string.Empty, ExportConfig.sGfeDiscountPointFPaidTo, true));

            E_RespaFeeType floodDeterminationType;
            if (m_dataLoan.sFloodCertificationDeterminationT == E_FloodCertificationDeterminationT.SingleChargeOrLifeOfLoan)
            {
                floodDeterminationType = E_RespaFeeType.FloodDeterminationLifeOfLoanFee;
            }
            else if (m_dataLoan.sFloodCertificationDeterminationT == E_FloodCertificationDeterminationT.InitialFee)
            {
                floodDeterminationType = E_RespaFeeType.FloodDeterminationInitialFee;
            }
            else
            {
                throw new CBaseException("developer error.",
                    string.Format("compliance ease exporter is out of sync with E_FloodCertificationDeterminationT" +
                    "type {0} was not expected",
                    m_dataLoan.sFloodCertificationDeterminationT.ToString()));
            }

            respaFeeList.Add(CreateRespaFee(floodDeterminationType, E_RespaFeeRespaSectionClassificationType._800_LoanFees, "", m_dataLoan.sFloodCertificationF_rep, m_dataLoan.sFloodCertificationFProps, m_dataLoan.sFloodCertificationFPaidTo, ExportConfig.sFloodCertificationFPaidTo));
            #endregion

            #region 900 Fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.InterimInterest, E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, "", m_dataLoan.sIPia_rep, m_dataLoan.sIPiaProps, string.Empty, ExportConfig.sIPiaPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.HazardInsurancePremium, E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, "", m_dataLoan.sHazInsPia_rep, m_dataLoan.sHazInsPiaProps, m_dataLoan.sHazInsPiaPaidTo, ExportConfig.sHazInsPiaPaidTo));
            //respaFeeList.Add(CreateRespaFee(E_RespaFeeType.MortgageInsurancePremium, E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, "", m_dataLoan.sMipPia_rep, m_dataLoan.sMipPiaProps, m_dataLoan.sMipPiaPaidTo, ExportConfig.sMipPiaPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, E_RespaFeeGfeAggregationType.Undefined, m_dataLoan.s904PiaDesc, m_dataLoan.s904Pia_rep, m_dataLoan.s904PiaProps, string.Empty, ExportConfig.s900RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, E_RespaFeeGfeAggregationType.Undefined, m_dataLoan.s900U1PiaDesc, m_dataLoan.s900U1Pia_rep, m_dataLoan.s900U1PiaProps, string.Empty, ExportConfig.s900RestPaidTo, false));
            #endregion

            #region 1000 Fees
            //string section1000PaidToType = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName.TrimWhitespaceAndBOM(); // opm 79196, set paid to type to lender, as this is assumed.
            // 4/16/2012 dd - OPM 82340 - We will always send PaidTo="Other" for 1000 sections.
            if (m_dataLoan.sHazInsRsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.HazardInsuranceReserve, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, "", m_dataLoan.sHazInsRsrv_rep, m_dataLoan.sHazInsRsrvProps, "OTHER", ExportConfig.sHazInsRsrvPaidTo));
            }
            if (m_dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.MortgageInsuranceReserve, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, "", m_dataLoan.sMInsRsrv_rep, m_dataLoan.sMInsRsrvProps, "OTHER", ExportConfig.sMInsRsrvPaidTo));
            }
            if (m_dataLoan.sRealETxRsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.CountyPropertyTaxesReserve, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, "", m_dataLoan.sRealETxRsrv_rep, m_dataLoan.sRealETxRsrvProps, "OTHER", ExportConfig.sRealETxRsrvPaidTo));
            }
            //respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, "SCHOOL TAXES", m_dataLoan.sSchoolTxRsrv_rep, m_dataLoan.sSchoolTxRsrvProps, "OTHER", ExportConfig.sSchoolTxRsrvPaidTo, false));
            if (m_dataLoan.sFloodInsRsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, "FLOOD INSURANCE RESERVE", m_dataLoan.sFloodInsRsrv_rep, m_dataLoan.sFloodInsRsrvProps, "OTHER", ExportConfig.sFloodInsRsrvPaidTo, false));
            }
            if (m_dataLoan.s1006RsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, m_dataLoan.s1006ProHExpDesc, m_dataLoan.s1006Rsrv_rep, m_dataLoan.s1006RsrvProps, "OTHER", ExportConfig.s1000RestPaidTo, false));
            }
            if (m_dataLoan.s1007RsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, m_dataLoan.s1007ProHExpDesc, m_dataLoan.s1007Rsrv_rep, m_dataLoan.s1007RsrvProps, "OTHER", ExportConfig.s1000RestPaidTo, false));
            }
            if (m_dataLoan.sU3RsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, m_dataLoan.sU3RsrvDesc, m_dataLoan.sU3Rsrv_rep, m_dataLoan.sU3RsrvProps, "OTHER", ExportConfig.s1000RestPaidTo, false));
            }
            if (m_dataLoan.sU4RsrvEscrowedTri == E_TriState.Yes)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, m_dataLoan.sU4RsrvDesc, m_dataLoan.sU4Rsrv_rep, m_dataLoan.sU4RsrvProps, "OTHER", ExportConfig.s1000RestPaidTo, false));
            }
            // 2/28/14 gf - opm 126772, ComplianceEase doesn't seem to support more than 5 "Other" type fees for 1000 section.
            // If all 5 are taken up, send school taxes as property taxes reserve. Otherwise, maintain old behavior.
            Func<RespaFee, bool> isTypeOtherSection1000Fee = fee => fee != null && fee.Type == E_RespaFeeType.Other
                    && fee.RespaSectionClassificationType == E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender
                    && fee.GfeAggregationType == E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount;
            bool usedAllAvailableTypeOther1000Fees = respaFeeList.Count(isTypeOtherSection1000Fee) == 5;
            int indexOfFirstTypeOther1000Fee = respaFeeList.FindIndex(fee => isTypeOtherSection1000Fee(fee));
            var schoolTaxesFeeType = usedAllAvailableTypeOther1000Fees ? E_RespaFeeType.PropertyTaxesReserve : E_RespaFeeType.Other;
            var schoolTaxesFee = CreateRespaFee(schoolTaxesFeeType, E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount, "SCHOOL TAXES", m_dataLoan.sSchoolTxRsrv_rep, m_dataLoan.sSchoolTxRsrvProps, "OTHER", ExportConfig.sSchoolTxRsrvPaidTo, false);
            if (m_dataLoan.sSchoolTxRsrvEscrowedTri == E_TriState.Yes)
            {
                if (indexOfFirstTypeOther1000Fee == -1)
                {
                    respaFeeList.Add(schoolTaxesFee);
                }
                else
                {
                    respaFeeList.Insert(indexOfFirstTypeOther1000Fee, schoolTaxesFee);
                }
            }
            #endregion

            #region 1100 Fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.SettlementOrClosingFee, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sEscrowF_rep, m_dataLoan.sEscrowFProps, m_dataLoan.sEscrowFTable, ExportConfig.sEscrowFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.TitleDocumentPreparationFee, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sDocPrepF_rep, m_dataLoan.sDocPrepFProps, m_dataLoan.sDocPrepFPaidTo, ExportConfig.sDocPrepFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.OwnersCoverage, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sOwnerTitleInsF_rep, m_dataLoan.sOwnerTitleInsProps, m_dataLoan.sOwnerTitleInsPaidTo, ExportConfig.sOwnerTitleInsPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.LendersCoverage, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sTitleInsF_rep, m_dataLoan.sTitleInsFProps, m_dataLoan.sTitleInsFTable, ExportConfig.sTitleInsFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.NotaryFee, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sNotaryF_rep, m_dataLoan.sNotaryFProps, m_dataLoan.sNotaryFPaidTo, ExportConfig.sNotaryFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.AttorneyFee, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, "", m_dataLoan.sAttorneyF_rep, m_dataLoan.sAttorneyFProps, m_dataLoan.sAttorneyFPaidTo, ExportConfig.sAttorneyFPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, E_RespaFeeGfeAggregationType.TitleServices, m_dataLoan.sU1TcDesc, m_dataLoan.sU1Tc_rep, m_dataLoan.sU1TcProps, m_dataLoan.sU1TcPaidTo, ExportConfig.s1100RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, E_RespaFeeGfeAggregationType.TitleServices, m_dataLoan.sU2TcDesc, m_dataLoan.sU2Tc_rep, m_dataLoan.sU2TcProps, m_dataLoan.sU2TcPaidTo, ExportConfig.s1100RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, E_RespaFeeGfeAggregationType.TitleServices, m_dataLoan.sU3TcDesc, m_dataLoan.sU3Tc_rep, m_dataLoan.sU3TcProps, m_dataLoan.sU3TcPaidTo, ExportConfig.s1100RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, E_RespaFeeGfeAggregationType.TitleServices, m_dataLoan.sU4TcDesc, m_dataLoan.sU4Tc_rep, m_dataLoan.sU4TcProps, m_dataLoan.sU4TcPaidTo, ExportConfig.s1100RestPaidTo, false));
            #endregion

            #region 1200 Fees
            // 1201/1202 Recording Fee
            if (m_dataLoan.sRecFLckd)
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.RecordingFee, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sRecF_rep, m_dataLoan.sRecFProps, m_dataLoan.sRecFDesc, ExportConfig.sRecFPaidTo));
            }
            else
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.DeedRecordingFee, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sRecDeed_rep, m_dataLoan.sRecFProps, m_dataLoan.sRecFDesc, ExportConfig.sRecFPaidTo));
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.MortgageRecordingFee, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sRecMortgage_rep, m_dataLoan.sRecFProps, m_dataLoan.sRecFDesc, ExportConfig.sRecFPaidTo));
                respaFeeList.Add(CreateRespaFee(E_RespaFeeType.ReleaseRecordingFee, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sRecRelease_rep, m_dataLoan.sRecFProps, m_dataLoan.sRecFDesc, ExportConfig.sRecFPaidTo));
            }

            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.CityCountyTaxStamps, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sCountyRtc_rep, m_dataLoan.sCountyRtcProps, m_dataLoan.sCountyRtcDesc, ExportConfig.sCountyRtcPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.StateTaxStamps, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, "", m_dataLoan.sStateRtc_rep, m_dataLoan.sStateRtcProps, m_dataLoan.sStateRtcDesc, ExportConfig.sStateRtcPaidTo));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Convert1200(m_dataLoan.sU1GovRtcGfeSection), m_dataLoan.sU1GovRtcDesc, m_dataLoan.sU1GovRtc_rep, m_dataLoan.sU1GovRtcProps, m_dataLoan.sU1GovRtcPaidTo, ExportConfig.s1200RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Convert1200(m_dataLoan.sU1GovRtcGfeSection), m_dataLoan.sU2GovRtcDesc, m_dataLoan.sU2GovRtc_rep, m_dataLoan.sU2GovRtcProps, m_dataLoan.sU2GovRtcPaidTo, ExportConfig.s1200RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Convert1200(m_dataLoan.sU1GovRtcGfeSection), m_dataLoan.sU3GovRtcDesc, m_dataLoan.sU3GovRtc_rep, m_dataLoan.sU3GovRtcProps, m_dataLoan.sU3GovRtcPaidTo, ExportConfig.s1200RestPaidTo, false));

            #endregion

            #region 1300 Fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeType.PestInspectionFee, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, "", m_dataLoan.sPestInspectF_rep, m_dataLoan.sPestInspectFProps, m_dataLoan.sPestInspectPaidTo, ExportConfig.sPestInspectFPaidTo));
            respaFeeList.Add(CreateRespaFee(m_dataLoan.sU1ScDesc.Contains("survey", StringComparison.OrdinalIgnoreCase) ? E_RespaFeeType.SurveyFee : E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, m_dataLoan.sU1ScDesc, m_dataLoan.sU1Sc_rep, m_dataLoan.sU1ScProps, m_dataLoan.sU1ScPaidTo, ExportConfig.s1300RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(m_dataLoan.sU2ScDesc.Contains("survey", StringComparison.OrdinalIgnoreCase) ? E_RespaFeeType.SurveyFee : E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, m_dataLoan.sU2ScDesc, m_dataLoan.sU2Sc_rep, m_dataLoan.sU2ScProps, m_dataLoan.sU2ScPaidTo, ExportConfig.s1300RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(m_dataLoan.sU3ScDesc.Contains("survey", StringComparison.OrdinalIgnoreCase) ? E_RespaFeeType.SurveyFee : E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, m_dataLoan.sU3ScDesc, m_dataLoan.sU3Sc_rep, m_dataLoan.sU3ScProps, m_dataLoan.sU3ScPaidTo, ExportConfig.s1300RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(m_dataLoan.sU4ScDesc.Contains("survey", StringComparison.OrdinalIgnoreCase) ? E_RespaFeeType.SurveyFee : E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, m_dataLoan.sU4ScDesc, m_dataLoan.sU4Sc_rep, m_dataLoan.sU4ScProps, m_dataLoan.sU4ScPaidTo, ExportConfig.s1300RestPaidTo, false));
            respaFeeList.Add(CreateRespaFee(m_dataLoan.sU5ScDesc.Contains("survey", StringComparison.OrdinalIgnoreCase) ? E_RespaFeeType.SurveyFee : E_RespaFeeType.Other, E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, m_dataLoan.sU5ScDesc, m_dataLoan.sU5Sc_rep, m_dataLoan.sU5ScProps, m_dataLoan.sU5ScPaidTo, ExportConfig.s1300RestPaidTo, false));
            #endregion
        }

        /// <summary>
        /// Populates the RESPA fee list with data based on the TRID format.
        /// </summary>
        /// <param name="respaFeeList">The fee list to be populated.</param>
        private void PopulateRespaFeeList_2015DataLayer(List<RespaFee> respaFeeList)
        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter =
                ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(
                    m_dataLoan.sClosingCostSet,
                    false,
                    m_dataLoan.sOriginatorCompensationPaymentSourceT,
                    m_dataLoan.sDisclosureRegulationT,
                    m_dataLoan.sGfeIsTPOTransaction);

            foreach (BorrowerClosingCostFee closingCost in m_dataLoan.sClosingCostSet.GetFees(borrowerSetFilter))
            {
                CAgentFields beneficiaryContact = null;
                if (closingCost.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = m_dataLoan.GetAgentFields(closingCost.BeneficiaryAgentId);
                }

                respaFeeList.Add(CreateRespaFee_2015DataLayer(closingCost, beneficiaryContact));
            }

            //OPM 228554: Lender Paid Originator Compensation will not be part of the closing cost set if TRID 2015. Borrowing from BorrowerClosingCostSet::Initialize.
            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && m_dataLoan.sGfeIsTPOTransaction && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
            {
                respaFeeList.Add(CreateRespaFee_TRID2015LenderPaidLOComp());
            }
        }

        /// <summary>
        /// Creates a RESPA fee element with data based on the TRID format.
        /// </summary>
        /// <param name="closingCost">The closing cost fee data.</param>
        /// <returns>A RespaFee element.</returns>
        private RespaFee CreateRespaFee_2015DataLayer(BorrowerClosingCostFee closingCost, CAgentFields beneficiaryContact)
        {
            //// IR - OPM 228164. Also exclude VA Funding Fee.
            //// IR - OPM 226349. Exclude Mortgage Insurance Premium per CE. This is already reported elsewhere:
            //// sLT is Conventional OR Other: /ACS_REQUEST/ACS_EXTENSION/MI/@_UpfrontPremiumPercent
            //// sLT is FHA OR USDA: /ACS_REQUEST/LOAN/_APPLICATION/GOVERNMENT_LOAN/FHA_LOAN/@FHAUpfrontMIPremiumPercent
            //// sLT is VA: /ACS_REQUEST/LOAN/_APPLICATION/GOVERNMENT_LOAN/VA_LOAN/@BorrowerFundingFeePercent
            if (closingCost.TotalAmount == 0
                || closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId
                || closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                || closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId
                || closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                return null;
            }

            var respaFee = new RespaFee();
            respaFee.SpecifiedHudLineNumber = closingCost.HudLine_rep;
            respaFee.Identifier = closingCost.UniqueId.ToString();
            respaFee.RespaSectionClassificationType = GetRespaSectionClassificationType(closingCost.HudLine);
            if (StaticMethodsAndExtensions.IsInRange(closingCost.HudLine, 1000, 1099))
            {
                respaFee.GfeAggregationType = E_RespaFeeGfeAggregationType.InitialDepositForYourEscrowAccount;
            }

            bool disclosureisCD = this.m_dataLoan.sHasClosingDisclosureArchive;
            respaFee.PaidToType = GetRespaPaidToType(
                beneficiaryContact, 
                closingCost.IsAffiliate, 
                closingCost.BeneficiaryType, 
                closingCost.Beneficiary, 
                closingCost.BeneficiaryDescription,
                closingCost.HudLine,
                closingCost.GetTRIDSectionBasedOnDisclosure(disclosureisCD));

            if (exportIntegratedDisclosureData)
            {
                respaFee.IntegratedDisclosureSectionType = Convert(closingCost.GetTRIDSectionBasedOnDisclosure(disclosureisCD));
            }

            respaFee.Type = GetClosingCostFeeType(closingCost.ClosingCostFeeTypeId, closingCost.MismoFeeT, closingCost.Description, closingCost.HudLine, closingCost.IsTitleFee, closingCost.Beneficiary, closingCost.IsThirdParty);

            if (respaFee.Type == E_RespaFeeType.Other)
            {
                respaFee.TypeOtherDescription = closingCost.Description;
                respaFee.ClassificationType = GetClassificationType(closingCost);

                if (this.m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE
                    && closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    respaFee.GfeAggregationType = E_RespaFeeGfeAggregationType.OurOriginationCharge;
                }
            }

            foreach (BorrowerClosingCostFeePayment payment in closingCost.Payments)
            {
                respaFee.PaymentList.Add(CreateRespaFeePayment_2015DataLayer(payment));
            }

            if (closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                bool isBonaFide = closingCost.IsBonaFide;
                respaFee.PaymentList[0].FederalBonaFideIndicator = isBonaFide ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
                respaFee.PaymentList[0].GSEBonaFideIndicator = isBonaFide ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
                respaFee.PaymentList[0].StateBonaFideIndicator = isBonaFide ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            }

            return respaFee;
        }

        /// <summary>
        /// Creates a RESPA fee element specifically for lender paid compensation on loans following TRID regulations.
        /// </summary>
        /// <returns>A RespaFee element representing lender paid originator compensation.</returns>
        private RespaFee CreateRespaFee_TRID2015LenderPaidLOComp()
        {
            if (this.m_dataLoan.sOriginatorCompensationTotalAmount == 0.0M)
            {
                return null;
            }

            var respaFee = new RespaFee();

            respaFee.SpecifiedHudLineNumber = "801";
            respaFee.Identifier = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId.ToString();
            respaFee.RespaSectionClassificationType = E_RespaFeeRespaSectionClassificationType._800_LoanFees;

            bool disclosureisCD = this.m_dataLoan.sHasClosingDisclosureArchive;
            respaFee.PaidToType = E_RespaFeePaidToType.Broker;

            if (exportIntegratedDisclosureData)
            {
                respaFee.IntegratedDisclosureSectionType = E_RespaFeeIntegratedDisclosureSectionType.OriginationCharges;
            }

            respaFee.Type = E_RespaFeeType.MortgageBrokerFeeIndirectPOC;

            respaFee.PaymentList.Add(CreateRespaFeePayment_TRID2015LenderPaidLOComp());

            return respaFee;
        }

        /// <summary>
        /// Retrieve the section classification type corresponding to the HUD-1 line number.
        /// </summary>
        /// <param name="hudLine">The HUD-1 line number.</param>
        /// <returns>The corresponding RESPA section classification type.</returns>
        public E_RespaFeeRespaSectionClassificationType GetRespaSectionClassificationType(int hudLine)
        {
            if (StaticMethodsAndExtensions.IsInRange(hudLine, 800, 899))
            {
                return E_RespaFeeRespaSectionClassificationType._800_LoanFees;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 900, 999))
            {
                return E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1000, 1099))
            {
                return E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1100, 1199))
            {
                return E_RespaFeeRespaSectionClassificationType._1100_TitleCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1200, 1299))
            {
                return E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1300, 1399))
            {
                return E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges;
            }
            else
            {
                return E_RespaFeeRespaSectionClassificationType.Undefined;
            }
        }

        /// <summary>
        /// Identifies whether ComplianceEase allows the fee to be retained by broker or lender as part of their HOEPA test.
        /// </summary>
        /// <param name="hudLine">The fee's hudline.</param>
        /// <param name="integratedDisclosureSectionT">The integrated disclosure section type.</param>
        /// <returns>Returns a boolean indicating whether ComplianceEase asserts that fee cannot be retained by lender or broker.</returns>
        private static bool CanBrokerLenderRetainRespaFeeForHOEPATest(int hudLine, E_IntegratedDisclosureSectionT integratedDisclosureSectionT)
        {
            //// Per CE: All items in the 1000, 1100, and 1200 series should not be retained by the lender or broker.
            //// Additionally, fees for third party services that the lender requires and selects (lines 804 - 807)
            //// mortgage and homeowner's insurance premiums (lines 902 - 903)
            //// and required services that you can shop for in the 1301 section should not be retained by the lender or broker.

            //// GFE Section 1000 corresponds to Integrated Disclosures Section G. These are paid to the lender
            //// GFE Section 1100 corresponds to Integrated Disclosures Section C. These are not paid to the lender.
            //// GFE Section 1200 corresponds to Integrated Disclosures Section E. These are not paid to the lender.
            //// GFE lines 804 - 807 corresponds to Integrated Disclosures Section A. These are not paid to the lender.
            //// GFE lines 902-903 are premiums which may be paid to the lender.
            //// GFE line 1302 corresponds to Integrated Disclosures Section C. These are not paid to the lender.

            if (integratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG
                || StaticMethodsAndExtensions.IsInRange(hudLine, 902, 903))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the corresponding Integrated Disclosure section type value.
        /// </summary>
        /// <param name="type">The Integrated Disclosure section type.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeIntegratedDisclosureSectionType"/> value.</returns>
        public E_RespaFeeIntegratedDisclosureSectionType Convert(E_IntegratedDisclosureSectionT type)
        {
            switch (type)
            {
                case E_IntegratedDisclosureSectionT.SectionA:
                    return E_RespaFeeIntegratedDisclosureSectionType.OriginationCharges;
                case E_IntegratedDisclosureSectionT.SectionB:
                    return E_RespaFeeIntegratedDisclosureSectionType.ServicesYouCannotShopFor;
                case E_IntegratedDisclosureSectionT.SectionC:
                    return E_RespaFeeIntegratedDisclosureSectionType.ServicesYouCanShopFor;
                case E_IntegratedDisclosureSectionT.SectionE:
                    return E_RespaFeeIntegratedDisclosureSectionType.TaxesAndOtherGovernmentFees;
                case E_IntegratedDisclosureSectionT.SectionF:
                    return E_RespaFeeIntegratedDisclosureSectionType.Prepaids;
                case E_IntegratedDisclosureSectionT.SectionG:
                    return E_RespaFeeIntegratedDisclosureSectionType.InitialEscrowPaymentAtClosing;
                case E_IntegratedDisclosureSectionT.SectionD:
                case E_IntegratedDisclosureSectionT.SectionH:
                case E_IntegratedDisclosureSectionT.SectionBorC:
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return E_RespaFeeIntegratedDisclosureSectionType.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Gets the general classification type for a fee.
        /// </summary>
        /// <param name="fee">The closing cost fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeClassificationType" /> value.</returns>
        public E_RespaFeeClassificationType GetClassificationType(BorrowerClosingCostFee fee)
        {
            if (fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionA)
            {
                return E_RespaFeeClassificationType.OriginationCharges;
            }
            else if (fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium
                || (fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.TitleEndorsementFee
                    && fee.GfeSectionT == E_GfeSectionT.B4))
            {
                return E_RespaFeeClassificationType.Title_LendersTitleInsurance;
            }
            else if (fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.TitleEndorsementFee)
            {
                return E_RespaFeeClassificationType.Title_OwnersTitleInsurance;
            }
            else if (fee.IsTitleFee || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.EscrowServiceFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee)
            {
                return E_RespaFeeClassificationType.Title_InsuranceAndServices;
            }
            else if (fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionB
                || fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionBorC
                || fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC)
            {
                return E_RespaFeeClassificationType.LoanCosts;
            }
            else if (fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionF)
            {
                return E_RespaFeeClassificationType.Prepaids;
            }
            else if (fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG)
            {
                return E_RespaFeeClassificationType.EscrowOrReserves;
            }
            else if (fee.GfeSectionT == E_GfeSectionT.B7
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.DeedRecordingFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.MortgageRecordingFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination
                || fee.MismoFeeT == E_ClosingCostFeeMismoFeeT.RecordingFeeTotal)
            {
                return E_RespaFeeClassificationType.RecordingFees;
            }
            else if (fee.GfeSectionT == E_GfeSectionT.B8)
            {
                return E_RespaFeeClassificationType.TransferTaxes;
            }
            else if (fee.IsOptional)
            {
                return E_RespaFeeClassificationType.OtherFormerHUD1Custom1301;
            }
            else
            {
                return E_RespaFeeClassificationType.OtherFormerHUD1Custom1300;
            }
        }

        /// <summary>
        /// Retrieves the type for fees based on either their feeTypeId or their MISMO fee type.
        /// </summary>
        /// <param name="feeTypeId">The Guid corresponding to the <code>ClosingCostFeeTypeId</code> of the fee.</param>
        /// <param name="feeType">The mismo type associated with the fee, from the fee setup.</param>
        /// <param name="description">The fee description.</param>
        /// <param name="hudLine">The HUD-1 line number.</param>
        /// <param name="isTitleFee">Boolean indicating whether fee is a title fee.</param>
        /// <param name="beneficiary">The role of the beneficiary entity associated with the fee.</param>
        /// <param name="isThirdParty">Boolean indicating whether fee is paid to third party.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeType" />.</returns>
        public E_RespaFeeType GetClosingCostFeeType(Guid feeTypeId, E_ClosingCostFeeMismoFeeT feeType, string description, int hudLine, bool isTitleFee, E_AgentRoleT beneficiary, bool isThirdParty)
        {
            if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId))
            {
                return E_RespaFeeType.AppraisalFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId))
            {
                return E_RespaFeeType.LenderCreditForTheInterestRateChosen;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId))
            {
                return E_RespaFeeType.CreditReportFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId))
            {
                return E_RespaFeeType.LoanDiscountPoints;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId))
            {
                return this.m_dataLoan.sFloodCertificationDeterminationT == E_FloodCertificationDeterminationT.SingleChargeOrLifeOfLoan ? 
                    E_RespaFeeType.FloodDeterminationLifeOfLoanFee : 
                    E_RespaFeeType.FloodDeterminationInitialFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId))
            {
                // OPM 243743, 6/22/2016, ML
                // The lender inspection fee has a customizable MISMO type. Per the PDE, we
                // want to use the lender-specified MISMO type unless the conversion results
                // in "Other". In that case, we will use the hardcoded RESPA fee type.
                var respaTypeFromMismoType = this.Convert(feeType, hudLine, isTitleFee);

                if (respaTypeFromMismoType != E_RespaFeeType.Other)
                {
                    return respaTypeFromMismoType;
                }

                return E_RespaFeeType.LenderInspectionFeePriorToClosing;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId))
            {
                return E_RespaFeeType.LoanOriginationFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId))
            {
                return E_RespaFeeType.MortgageBrokerFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId))
            {
                if (this.m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE
                    && (beneficiary == E_AgentRoleT.Broker || isThirdParty || this.m_dataLoan.sGfeIsTPOTransaction))
                {
                    if (this.m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                    {
                        return E_RespaFeeType.MortgageBrokerFee;
                    }
                    else
                    {
                        return E_RespaFeeType.MortgageBrokerFeeIndirectPOC;
                    }
                }
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId))
            {
                return E_RespaFeeType.ProcessingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId))
            {
                return E_RespaFeeType.TaxRelatedServiceFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId))
            {
                return E_RespaFeeType.UnderwritingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId))
            {
                return E_RespaFeeType.WireTransferFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId))
            {
                return E_RespaFeeType.HazardInsurancePremium;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId))
            {
                return E_RespaFeeType.InterimInterest;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId))
            {
                return E_RespaFeeType.FloodInsurancePremium;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId))
            {
                return E_RespaFeeType.HazardInsurancePremium;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId))
            {
                //// The aggregate is not included in the payload, so this is a don't care.
            }
            //// OPM 228227: ComplianceEase Advised mapping Prepaid Property Taxes to be mapped to CountyPropertyTaxes.
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId))
            {
                return E_RespaFeeType.CountyPropertyTaxes;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId))
            {
                return E_RespaFeeType.HazardInsurancePremium;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId))
            {
                //// The aggregate is not included in the payload, so this is a don't care.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId))
            {
                return E_RespaFeeType.HazardInsuranceReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId))
            {
                return E_RespaFeeType.HazardInsuranceReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId))
            {
                return E_RespaFeeType.MortgageInsuranceReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId))
            {
                return E_RespaFeeType.PropertyTaxesReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId))
            {
                return E_RespaFeeType.CountyPropertyTaxesReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId))
            {
                return E_RespaFeeType.HazardInsuranceReserve;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId))
            {
                return E_RespaFeeType.AttorneyFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId))
            {
                return E_RespaFeeType.SettlementOrClosingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId))
            {
                return E_RespaFeeType.TitleDocumentPreparationFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId))
            {
                return E_RespaFeeType.LendersCoverage;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId))
            {
                return E_RespaFeeType.NotaryFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId))
            {
                return E_RespaFeeType.OwnersCoverage;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId))
            {
                return E_RespaFeeType.CityCountyTaxStamps;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId))
            {
                //// Fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId))
            {
                return E_RespaFeeType.DeedRecordingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId))
            {
                return E_RespaFeeType.MortgageRecordingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId))
            {
                return E_RespaFeeType.RecordingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId))
            {
                return E_RespaFeeType.ReleaseRecordingFee;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId))
            {
                return E_RespaFeeType.StateTaxStamps;
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId)
                || feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId))
            {
                if (description.Contains("survey", StringComparison.OrdinalIgnoreCase))
                {
                    return E_RespaFeeType.SurveyFee;
                }

                //// Otherwise fall back on the mismo type.
            }
            else if (feeTypeId.Equals(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId))
            {
                return E_RespaFeeType.PestInspectionFee;
            }

            return Convert(feeType, hudLine, isTitleFee);
        }

        /// <summary>
        /// Creates a RESPA fee payment element with data based on the TRID format.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>A PAYMENT element.</returns>
        private Payment CreateRespaFeePayment_2015DataLayer(BorrowerClosingCostFeePayment payment)
        {
            var respaFeePayment = new Payment();
            respaFeePayment.Amount = payment.Amount.ToString();
            respaFeePayment.FinancedIndicator = payment.IsFinanced ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            respaFeePayment.IncludedInAprIndicator = payment.IsIncludedInApr(m_dataLoan.GetLiveLoanVersion()) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            respaFeePayment.PaidByType = Convert(payment.PaidByT);

            return respaFeePayment;
        }

        /// <summary>
        /// Creates a RESPA fee payment element with data based on the TRID format for Lender Paid Originator Compensation.
        /// </summary>
        /// <returns>The payment data.</returns>
        private Payment CreateRespaFeePayment_TRID2015LenderPaidLOComp()
        {
            var respaFeePayment = new Payment();
            respaFeePayment.Amount = m_dataLoan.sOriginatorCompensationTotalAmount_rep;
            respaFeePayment.FinancedIndicator = E_DefaultAbleIndicator.N;
            respaFeePayment.IncludedInAprIndicator = E_DefaultAbleIndicator.N;
            respaFeePayment.PaidByType = E_RespaFeePaymentPaidByType.Lender;
            return respaFeePayment;
        }

        /// <summary>
        /// Converts an entity into a ComplianceEase PaidBy value.
        /// </summary>
        /// <param name="paidBy">The entity who is making the payment.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePaymentPaidByType" /> value.</returns>
        private E_RespaFeePaymentPaidByType Convert(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_RespaFeePaymentPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return E_RespaFeePaymentPaidByType.Broker;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_RespaFeePaymentPaidByType.Undefined;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_RespaFeePaymentPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_RespaFeePaymentPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_RespaFeePaymentPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }



        /// <summary>
        /// Converts a closing cost fee type to a ComplianceEase fee type value.
        /// </summary>
        /// <param name="type">The closing cost fee type.</param>
        /// <param name="hudLine">The HUD-1 line number.</param>
        /// <param name="isTitleFee">Boolean indicating whether fee is a title fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeType" /> value.</returns>
        private E_RespaFeeType Convert(E_ClosingCostFeeMismoFeeT type, int hudLine, bool isTitleFee)
        {
            switch (type)
            {
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return E_RespaFeeType.ArchitecturalAndEngineeringFee;
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                    return E_RespaFeeType.AbstractOrTitleSearchFee;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return E_RespaFeeType.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                    return E_RespaFeeType.AppraisalReviewFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return E_RespaFeeType.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return E_RespaFeeType.AssignmentRecordingFee;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return E_RespaFeeType.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return E_RespaFeeType.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return E_RespaFeeType.CityCountyTaxStamps;
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                    return E_RespaFeeType.CLOAccessFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return E_RespaFeeType.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return (isTitleFee || StaticMethodsAndExtensions.IsInRange(hudLine, 1100, 1199)) ? E_RespaFeeType.TitleCourierFee : E_RespaFeeType.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return E_RespaFeeType.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return E_RespaFeeType.DeedRecordingFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return E_RespaFeeType.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return E_RespaFeeType.FloodDeterminationInitialFee;
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return E_RespaFeeType.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return E_RespaFeeType.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return E_RespaFeeType.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return E_RespaFeeType.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return E_RespaFeeType.MortgageRecordingFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return E_RespaFeeType.RecordingFee;
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return E_RespaFeeType.NewLoanAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return E_RespaFeeType.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return E_RespaFeeType.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return E_RespaFeeType.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return E_RespaFeeType.ReleaseRecordingFee;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return E_RespaFeeType.SettlementOrClosingFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return E_RespaFeeType.StateTaxStamps;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return E_RespaFeeType.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return E_RespaFeeType.TaxRelatedServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                    return E_RespaFeeType.TitleEndorsementFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return E_RespaFeeType.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return E_RespaFeeType.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                    return E_RespaFeeType.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                    return E_RespaFeeType.LendersCoverage;
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return E_RespaFeeType.OwnersCoverage;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return E_RespaFeeType.UnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                    return E_RespaFeeType.WireTransferFee;
                // OPM 227381
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal;
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return E_RespaFeeType.OurOriginationChargeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return E_RespaFeeType.TitleServicesFeeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                    return E_RespaFeeType.TitleDocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                    return E_RespaFeeType.ReconveyanceFee;
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                    return E_RespaFeeType.SubordinationRecordingFee;
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                    return E_RespaFeeType.TransferTaxesTotal;
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                case E_ClosingCostFeeMismoFeeT.BondFee:
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                case E_ClosingCostFeeMismoFeeT.Undefined:
                case E_ClosingCostFeeMismoFeeT.Other:
                // OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return E_RespaFeeType.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// For sU1GovRtcDesc to sU3GovRtcDesc
        /// </summary>
        private E_RespaFeeGfeAggregationType Convert1200(E_GfeSectionT gfeSection)
        {
            if (gfeSection == E_GfeSectionT.B7)
                return E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
            else if (gfeSection == E_GfeSectionT.B8)
                return E_RespaFeeGfeAggregationType.TransferTaxes;

            return E_RespaFeeGfeAggregationType.Undefined;
        }

        /// <summary>
        /// For s800U1F to s800U5F
        /// </summary>
        private E_RespaFeeGfeAggregationType Convert(E_GfeSectionT gfeSection)
        {
            if (gfeSection == E_GfeSectionT.B1)
                return E_RespaFeeGfeAggregationType.OurOriginationCharge;

            return E_RespaFeeGfeAggregationType.Undefined;
        }

        private SubjectProperty CreateSubjectProperty()
        {
            SubjectProperty _SubjectProperty = new SubjectProperty();

            _SubjectProperty.Type = Convert(m_dataLoan.sGseSpT);

            // _SubjectProperty.JurisdictionList.Add(CreateJurisdiction());
            return _SubjectProperty;
        }

        private Jurisdiction CreateJurisdiction()
        {
            Jurisdiction _Jurisdiction = new Jurisdiction();

            // _Jurisdiction.Name = ;
            // _Jurisdiction.PropertySituatedInJurisdictionIndicator = ;

            return _Jurisdiction;
        }

        public E_SubjectPropertyType Convert(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                    return E_SubjectPropertyType.Undefined;
                case E_sGseSpT.Attached:
                    return E_SubjectPropertyType.Attached;
                case E_sGseSpT.Condominium:
                    return E_SubjectPropertyType.Condominium;
                case E_sGseSpT.Cooperative:
                    return E_SubjectPropertyType.Cooperative;
                case E_sGseSpT.Detached:
                    return E_SubjectPropertyType.Detached;
                case E_sGseSpT.DetachedCondominium:
                    return E_SubjectPropertyType.Condominium;
                case E_sGseSpT.HighRiseCondominium:
                    return E_SubjectPropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return E_SubjectPropertyType.ManufacturedHousing;
                case E_sGseSpT.Modular:
                    return E_SubjectPropertyType.Other;
                case E_sGseSpT.PUD:
                    return E_SubjectPropertyType.PUD;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        private RespaFee CreateSellerPaidFee()
        {
            if (m_dataLoan.sTotCcPbs < 0)
            {
                return null;
            }

            if (m_dataLoan.sTotCcPbsLocked)
            {
                RespaFee _RespaFee = new RespaFee();
                _RespaFee.Type = E_RespaFeeType.SellerPaidPointsAndFees;
                string feeStr = m_dataLoan.sTotCcPbs_rep;
                _RespaFee.PaymentList.Add(new Payment(){Amount = (feeStr == "") ? "0.00" : feeStr});
                return _RespaFee;
            }
            else
            {
                return null;
            }
        }

        private RespaFee CreateRespaFee(E_RespaFeeType feeType, E_RespaFeeRespaSectionClassificationType respaSectionClassificationType, string typeOtherDescription, string amount, int props, string paidTo, E_RespaFeePaidToType complianceEaseFeePaidTo, bool includeBF)
        {
            return CreateRespaFee(feeType, respaSectionClassificationType, E_RespaFeeGfeAggregationType.Undefined, typeOtherDescription, amount, props, paidTo, complianceEaseFeePaidTo, includeBF);
        }

        private RespaFee CreateRespaFee(E_RespaFeeType feeType, E_RespaFeeRespaSectionClassificationType respaSectionClassificationType, string typeOtherDescription, string amount, int props, string paidTo, E_RespaFeePaidToType complianceEaseFeePaidTo)
        {
            return CreateRespaFee(feeType, respaSectionClassificationType, E_RespaFeeGfeAggregationType.Undefined, typeOtherDescription, amount, props, paidTo, complianceEaseFeePaidTo, false);
        }

        private RespaFee CreateRespaFee(E_RespaFeeType feeType, E_RespaFeeRespaSectionClassificationType classificationType, E_RespaFeeGfeAggregationType gfeAggregationType, string typeOtherDescription, string amount, int props, string paidTo, E_RespaFeePaidToType complianceEaseFeePaidTo, bool includeBF)
        {
            if (amount == "" || amount == "0.00")
            {
                return null;
            }

            if (feeType == E_RespaFeeType.Other)
            {
                if (gfeAggregationType == E_RespaFeeGfeAggregationType.OurOriginationCharge && string.Equals(typeOtherDescription, "Application Fee", StringComparison.InvariantCultureIgnoreCase))
                {
                    feeType = E_RespaFeeType.ApplicationFee;
                    typeOtherDescription = string.Empty;
                }
                else if (gfeAggregationType == E_RespaFeeGfeAggregationType.OurOriginationCharge && string.Equals(typeOtherDescription, "Commitment Fee", StringComparison.InvariantCultureIgnoreCase))
                {
                    feeType = E_RespaFeeType.CommitmentFee;
                    typeOtherDescription = string.Empty;
                }
            }

            RespaFee _RespaFee = new RespaFee();

            _RespaFee.RespaSectionClassificationType = classificationType;
            _RespaFee.GfeAggregationType = gfeAggregationType;
            // _RespaFee.SpecifiedHudLineNumber = ;
            _RespaFee.Type = feeType;

            if (feeType == E_RespaFeeType.Other)
            {
                _RespaFee.TypeOtherDescription = typeOtherDescription;
            }

            _RespaFee.PaidToType = GetRespaPaidToType(paidTo, props, complianceEaseFeePaidTo);

            if (feeType == E_RespaFeeType.LoanDiscountPoints && LosConvert.GfeItemProps_BF(props)
                && m_dataLoan.sQMNonExcludableDiscountPointsPc != 0)
            {
                _RespaFee.PaymentList.Add(CreatePayment(m_dataLoan.sQMDiscountPointFComplianceEase_SansNonExcludable_rep, props, includeBF));
                _RespaFee.PaymentList.Add(CreatePayment(m_dataLoan.sQMNonExcludableDiscountPointsFComplianceEase_rep, LosConvert.GfeItemProps_UpdateBF(props, false), includeBF));
            }
            else
            {
                _RespaFee.PaymentList.Add(CreatePayment(amount, props, includeBF));
            }

            return _RespaFee;
        }

        /// <summary>
        /// Retrieves the entity who will receive payment for a fee.
        /// </summary>
        /// <param name="agent">The contact for the entity, if any.</param>
        /// <param name="isAffiliate">Indicates whether the entity is an affiliate of the loan originator.</param>
        /// <param name="beneficiaryType">The entity type, expressed as a string.</param>
        /// <param name="beneficiary">The role of the entity.</param>
        /// <param name="beneficiaryDescription">A description of the entity.</param>
        /// <returns></returns>
        private E_RespaFeePaidToType GetRespaPaidToType(
            CAgentFields agent, 
            bool isAffiliate, 
            string beneficiaryType, 
            E_AgentRoleT beneficiary, 
            string beneficiaryDescription,
            int hudLine,
            E_IntegratedDisclosureSectionT integratedDisclosureSectionT)
        {
            E_RespaFeePaidToType feePaidToType = E_RespaFeePaidToType.Other;

            if (agent != null)
            {
                if (agent.IsValid && agent.RecordId != Guid.Empty && !agent.IsNewRecord)
                {
                    bool isAffiliateOfBroker = agent.IsOriginatorAffiliate && this.m_dataLoan.sGfeIsTPOTransaction;
                    feePaidToType = Convert(agent.AgentRoleT, agent.IsLender, agent.IsOriginator, isAffiliate, isAffiliateOfBroker);
                }
            }

            //// If the fee PaidTo is set w/o having an associated agent, such as set from the fee setup, then map it.
            if (feePaidToType == E_RespaFeePaidToType.Other)
            {
                feePaidToType = Convert(beneficiary, false, false, isAffiliate, false);
            }

            //// OPM 228634: CAgentFields.IsOriginator is not reliable. If Paid To Loan Officer and TPO transaction, assume broker.
            if (feePaidToType == E_RespaFeePaidToType.Other && beneficiary == E_AgentRoleT.LoanOfficer && m_dataLoan.sGfeIsTPOTransaction)
            {
                feePaidToType = E_RespaFeePaidToType.Broker;
            }

            if (this.allowOverridePaidToForHOEPATest && !CanBrokerLenderRetainRespaFeeForHOEPATest(hudLine, integratedDisclosureSectionT)
                && (feePaidToType == E_RespaFeePaidToType.Lender || feePaidToType == E_RespaFeePaidToType.Broker))
            {
                feePaidToType = E_RespaFeePaidToType.Default;
            }

            return feePaidToType;
        }

        /// <summary>
        /// Converts an agent role into a ComplianceEase PaidTo value.
        /// </summary>
        /// <param name="role">The role of the entity to whom the fee is paid.</param>
        /// <param name="isLender">Indicates whether the entity is the lender.</param>
        /// <param name="isOriginator">Indicates whether the entity is the loan originator.</param>
        /// <param name="isAffiliate">Indicates whether the entity is an affiliate of the lender or broker.</param>
        /// <param name="isAffiliateOfBroker">Should be True if the entity is an affiliate of the broker, and false if the
        /// entity is an affiliate of the lender or not an affiliate at all.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePaidToType" /> value.</returns>
        private E_RespaFeePaidToType Convert(E_AgentRoleT role, bool isLender, bool isOriginator, bool isAffiliate, bool isAffiliateOfBroker)
        {
            if (role == E_AgentRoleT.Broker)
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (role == E_AgentRoleT.Lender)
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (isLender)
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (isOriginator)
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (isAffiliate)
            {
                return isAffiliateOfBroker ? E_RespaFeePaidToType.AffiliateOfBroker : E_RespaFeePaidToType.AffiliateOfLender;
            }
            else
            {
                return E_RespaFeePaidToType.Other;
            }
        }

        private E_RespaFeePaidToType GetRespaPaidToType(string paidTo, int props, E_RespaFeePaidToType complianceEaseFeePaidTo)
        {
            E_RespaFeePaidToType paidToType;
            if (paidTo == "OTHER")
            {
                // 4/16/2012 dd - OPM 82340 - This allow section 1000 to export as OTHER.
                paidToType = E_RespaFeePaidToType.Other;
            }
            //OPM 69450 - If PaidTo description is not blank AND paidTo EQUAL to m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender).CompanyName (case insensitive) THEN PaidToType = Lender.
            else if ((!string.IsNullOrEmpty(paidTo)) && String.Equals(paidTo.TrimWhitespaceAndBOM(), m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase))
            {
                paidToType = E_RespaFeePaidToType.Lender;
            }
            else if (LosConvert.GfeItemProps_ToBr(props))
            {
                paidToType = E_RespaFeePaidToType.Broker;
            }
            else if (LosConvert.GfeItemProps_ThisPartyIsAffiliate(props))
            {
                paidToType = E_RespaFeePaidToType.AffiliateOfLender;
            }
            else if (string.IsNullOrEmpty(paidTo) == false)
            {
                // 7/29/2011 dd - If user fill out the paid to section then set PaidToType to Other.
                paidToType = E_RespaFeePaidToType.Other;
            }
            else if (m_dataLoan.BrokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction && m_dataLoan.BrokerDB.IsEnablePTMComplianceEaseIndicator)
            {
                paidToType = complianceEaseFeePaidTo == E_RespaFeePaidToType.Undefined ? E_RespaFeePaidToType.Other : complianceEaseFeePaidTo;
            }
            else
            {
                // 10/19/2011 dd - OPM 72351 - When Paid To description is blank send over Default to ComplianceEase.
                paidToType = E_RespaFeePaidToType.Default;
            }

            return paidToType;
        }

        private E_RespaFeePaymentPaidByType GetPaidByTypeFromProps(int props)
        {
            switch (LosConvert.GfeItemProps_Payer(props))
            {
                case LosConvert.BORR_PAID_FINANCED:
                    return E_RespaFeePaymentPaidByType.Buyer; 
                case LosConvert.BORR_PAID_OUTOFPOCKET:
                    return E_RespaFeePaymentPaidByType.Buyer;
                case LosConvert.SELLER_PAID:
                    return E_RespaFeePaymentPaidByType.Seller;
                case LosConvert.LENDER_PAID:
                    return E_RespaFeePaymentPaidByType.Lender;
                case LosConvert.BROKER_PAID:
                    return E_RespaFeePaymentPaidByType.Broker;
                default:
                    Tools.LogError("unhandled value for gfe payer prop: " + LosConvert.GfeItemProps_Payer(props), new Exception());
                    return E_RespaFeePaymentPaidByType.Undefined;
            }
        }

        private Payment CreatePayment(string amount, int props)
        {
            return CreatePayment(amount, props, false);
        }

        private Payment CreatePayment(string amount, int props, bool includeBF)
        {
            Payment _Payment = new Payment();

            _Payment.Amount = amount;

            bool isApr = LosConvert.GfeItemProps_AprAndPdByBorrower(props);
            _Payment.FinancedIndicator = E_DefaultAbleIndicator.N;
            _Payment.IncludedInAprIndicator = isApr ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;

            if (LosConvert.GfeItemProps_Payer(props) == LosConvert.BORR_PAID_FINANCED)
            {
                _Payment.FinancedIndicator = E_DefaultAbleIndicator.Y;
            }
            _Payment.PaidByType = GetPaidByTypeFromProps(props);
            
            bool isBorr = LosConvert.GfeItemProps_Borr(props);
            _Payment.ExcludableByBorrowerChoiceIndicator = isBorr ? E_YesNoIndicator.Y : E_YesNoIndicator.N;

            if (includeBF)
            {
                _Payment.FederalBonaFideIndicator = LosConvert.GfeItemProps_BF(props) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
                _Payment.GSEBonaFideIndicator = LosConvert.GfeItemProps_BF(props) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
                _Payment.StateBonaFideIndicator = LosConvert.GfeItemProps_BF(props) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            }

            return _Payment;
        }

        private PrepaymentPenaltyProgram CreatePrepaymentPenaltyProgram()
        {
            PrepaymentPenaltyProgram _PrepaymentPenaltyProgram = new PrepaymentPenaltyProgram();

            // _PrepaymentPenaltyProgram.Name = ;
            // _PrepaymentPenaltyProgram.Type = ;

            // _PrepaymentPenaltyProgram.PeriodList.Add(CreatePeriod());
            return _PrepaymentPenaltyProgram;
        }

        private Period CreatePeriod()
        {
            Period _Period = new Period();

            // _Period.Number = ;
            // _Period.DurationMonths = ;
            // _Period.TriggerFraction = ;
            // _Period.TriggerPercent = ;
            // _Period.PenaltyPercent = ;
            // _Period.PenaltyBasisType = ;
            // _Period.PenaltyDays = ;
            // _Period.PenaltyMonths = ;
            // _Period.PenaltyMonthsBasisPercent = ;

            return _Period;
        }

        private MortgageLoanDetails CreateMortgageLoanDetails()
        {
            MortgageLoanDetails _MortgageLoanDetails = new MortgageLoanDetails();

            // _MortgageLoanDetails.InitialPaymentOptionPeriodMonths = ;
            // _MortgageLoanDetails.InterestOnlyDuringOptionPeriodIndicator = ;
            // _MortgageLoanDetails.PostOptionPeriodInterestOnlyTermMonths = ;
            // _MortgageLoanDetails.MinimumPaymentRateMarginPercent = ;
            // _MortgageLoanDetails.MinimumPaymentRateFloorPercent = ;
            // _MortgageLoanDetails.InitialPaymentDiscountPercent = ;
            // _MortgageLoanDetails.InitialPaymentDiscountPeriodMonths = ;
            // _MortgageLoanDetails.SubsequentPaymentDiscountPercent = ;
            // _MortgageLoanDetails.TotalPaymentDiscountPeriodMonths = ;
            _MortgageLoanDetails.DisclosedApr = m_dataLoan.sApr_rep;
            _MortgageLoanDetails.DisclosedFinanceChargeAmount = m_dataLoan.sFinCharge_rep;
            _MortgageLoanDetails.MaximumPrepaymentPenaltyAmount = m_dataLoan.sGfeMaxPpmtPenaltyAmt_rep;
            // _MortgageLoanDetails.Section32DisclosureDate = ;
            // _MortgageLoanDetails.UndiscountedInterestRatePercent = ;

            return _MortgageLoanDetails;
        }

        private Mi CreateMi()
        {
            Mi _Mi = new Mi();

            //opm 117759 av - Due to ComplianceEase giving us incorrect information several months ago.
            //if(m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.UsdaRural)
            //_Mi.MiAndFundingFeeCashCreditAmount = m_dataLoan.sUfCashPd_rep;

            if (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other)
                _Mi.CancelAtMidpointIndicator = m_dataLoan.sProMInsMidptCancel ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            // _Mi.PaymentShiftMonths = ;
            // _Mi.RenewalCalculationType = ;
            if (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other)
                _Mi.UpfrontPremiumPercent = m_dataLoan.sFfUfmipR_rep;

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                _Mi.PaidToType = GetRespaPaidToType(String.Empty, m_dataLoan.sVaFfProps, E_RespaFeePaidToType.Other);
            }
            else if (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other || m_dataLoan.sLT == E_sLT.UsdaRural || m_dataLoan.sLT == E_sLT.FHA)
            {
                _Mi.PaidToType = GetRespaPaidToType(String.Empty, m_dataLoan.sMipPiaProps, m_ExportConfig.sMipPiaPaidTo);
            }
            else
            {
                Tools.LogError("unhandled enum value for sLT: " + m_dataLoan.sLT, new Exception());
            }

            _Mi.MiAndFundingFeeTotalAmountRefundableIndicator = m_dataLoan.sUfmipIsRefundableOnProRataBasis ? E_YesNoIndicator.Y : E_YesNoIndicator.N;

            if (m_dataLoan.sMipPiaProps_Apr)
            {
                _Mi.MiAndFundingFeeFinancedAmountIncludedInAPRIndicator = E_DefaultAbleIndicator.Y;
            }
            
            _Mi.MiAndFundingFeeCashCreditList.Add(
                this.m_dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy
                ? CreateMiAndFundingFeeCashCredit()
                : CreateMiAndFundingFeeCashCredit_2015DataLayer());
            
            return _Mi;
        }

        /// <summary>
        /// Creates an MI_AND_FUNDING_FEE_CASH_CREDIT element using the 2015 datalayer.
        /// </summary>
        /// <returns>An MI_AND_FUNDING_FEE_CASH_CREDIT element.</returns>
        private MiAndFundingFeeCashCredit CreateMiAndFundingFeeCashCredit_2015DataLayer()
        {
            var miFee = (BorrowerClosingCostFee)this.m_dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId);

            if (miFee.TotalAmount == 0m)
            {
                miFee = (BorrowerClosingCostFee)this.m_dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId);
            }

            if (miFee.TotalAmount == 0m && this.m_dataLoan.sLT == E_sLT.VA)
            {
                miFee = (BorrowerClosingCostFee)this.m_dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId);
            }

            var cashPayment = miFee.Payments.FirstOrDefault(p => ((BorrowerClosingCostFeePayment)p).IsCashPdPayment);

            if (miFee.TotalAmount == 0m || cashPayment == null || cashPayment.Amount == 0m)
            {
                return null;
            }

            var cashCredit = new MiAndFundingFeeCashCredit();

            cashCredit.Amount = cashPayment.Amount_rep;
            cashCredit.IncludedInAPRIndicator = cashPayment.IsIncludedInApr(m_dataLoan.GetLiveLoanVersion()) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            cashCredit.PaidByType = Convert(cashPayment.PaidByT);

            return cashCredit;
        }

        private MiAndFundingFeeCashCredit CreateMiAndFundingFeeCashCredit()
        {
            if (m_dataLoan.sUfCashPd == 0.0M)
            {
                return null;
            }

            var miffcc = new MiAndFundingFeeCashCredit();
            miffcc.Amount = m_dataLoan.sUfCashPd_rep;

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                miffcc.IncludedInAPRIndicator = LosConvert.GfeItemProps_Apr(m_dataLoan.sVaFfProps) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            }
            else if (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other || m_dataLoan.sLT == E_sLT.UsdaRural || m_dataLoan.sLT == E_sLT.FHA)
            {
                miffcc.IncludedInAPRIndicator = LosConvert.GfeItemProps_Apr(m_dataLoan.sMipPiaProps) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N;
            }

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                miffcc.PaidByType = GetPaidByTypeFromProps(m_dataLoan.sVaFfProps);
            }
            else if (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other || m_dataLoan.sLT == E_sLT.UsdaRural || m_dataLoan.sLT == E_sLT.FHA)
            {
                miffcc.PaidByType = GetPaidByTypeFromProps(m_dataLoan.sMipPiaProps);
            }

            return miffcc;
        }

        private LoanPurposeDetails CreateLoanPurposeDetails()
        {
            LoanPurposeDetails _LoanPurposeDetails = new LoanPurposeDetails();

            _LoanPurposeDetails.LoanPurposeType = Convert(m_dataLoan.sLPurposeT);

            return _LoanPurposeDetails;
        }

        private E_LoanPurposeDetailsLoanPurposeType Convert(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                    return E_LoanPurposeDetailsLoanPurposeType.Purchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                    return E_LoanPurposeDetailsLoanPurposeType.Refinance;
                case E_sLPurposeT.Construct:
                    return E_LoanPurposeDetailsLoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm:
                    return E_LoanPurposeDetailsLoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.Other:
                    return E_LoanPurposeDetailsLoanPurposeType.Other;
                case E_sLPurposeT.HomeEquity:
                    return E_LoanPurposeDetailsLoanPurposeType.Refinance;
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }
        }

        private LoanProductDetails CreateLoanProductDetails()
        {
            LoanProductDetails _LoanProductDetails = new LoanProductDetails();

            if (m_dataLoan.sSpState == "NY" || m_dataLoan.sSpState == "RI")
            {
                _LoanProductDetails.ArmDetails = CreateArmDetails();
            }
            // _LoanProductDetails.BuydownList.Add(CreateBuydown());
            // _LoanProductDetails.DualAmortization = CreateDualAmortization();
            _LoanProductDetails.Gpm = CreateGpm();
            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536
                _LoanProductDetails.HelocTerms = CreateHelocTerms();
                _LoanProductDetails.CreditType = E_LoanProductDetailsCreditType.HELOC;
            }
            // _LoanProductDetails.NegativeAmortization = CreateNegativeAmortization();
            return _LoanProductDetails;
        }

        private NegativeAmortization CreateNegativeAmortization()
        {
            NegativeAmortization _NegativeAmortization = new NegativeAmortization();

            // _NegativeAmortization.ProgramType = ;
            // _NegativeAmortization.PrincipalAndInterestFinalRecastMonthsCount = ;
            // _NegativeAmortization.PrincipalAndInterestInitialRecastMonthsCount = ;
            // _NegativeAmortization.PrincipalAndInterestRecastAtMaximumBalanceIndicator = ;
            // _NegativeAmortization.PrincipalAndInterestRecastMonthsCount = ;
            // _NegativeAmortization.MaximumBalanceCanBeExceededByOnePaymentIndicator = ;

            return _NegativeAmortization;
        }

        private HelocTerms CreateHelocTerms()
        {
            HelocTerms _HelocTerms = new HelocTerms();

            _HelocTerms.HelocInitialRatePercent = m_dataLoan.sNoteIR_rep;
            _HelocTerms.FullyIndexedApr = m_dataLoan.sFullyIndexedR_rep;
            _HelocTerms.HELOCInitialDisclosureDate = m_dataLoan.sTilInitialDisclosureD_rep;
            _HelocTerms.HELOCDrawFeeAmount = m_dataLoan.sHelocDrawFee_rep;
            return _HelocTerms;
        }

        private Gpm CreateGpm()
        {
            if (m_dataLoan.sFinMethT != E_sFinMethT.Graduated)
            {
                return null;
            }
            Gpm _Gpm = new Gpm();

            _Gpm.GraduatedPaymentRatePercent = m_dataLoan.sGradPmtR_rep;
            _Gpm.GraduatedPaymentTermYears = m_dataLoan.sGradPmtYrs_rep;

            return _Gpm;
        }

        private DualAmortization CreateDualAmortization()
        {
            DualAmortization _DualAmortization = new DualAmortization();

            // _DualAmortization.InitialLoanAmortizationTermMonths = ;
            // _DualAmortization.InitialLoanAmortizationPeriodMonths = ;
            // _DualAmortization.SubsequentLoanAmortizationTermMonths = ;
            // _DualAmortization.SubsequentLoanAmortizationPeriodMonths = ;

            return _DualAmortization;
        }

        private Buydown CreateBuydown()
        {
            Buydown _Buydown = new Buydown();

            // _Buydown.PeriodIdentifier = ;
            // _Buydown.PeriodMonths = ;
            // _Buydown.AdjustmentPercent = ;

            return _Buydown;
        }

        private ArmDetails CreateArmDetails()
        {
            ArmDetails _ArmDetails = new ArmDetails();

            _ArmDetails.ArmIndexType = E_ArmDetailsArmIndexType.UserProvidedIndexValue;
            _ArmDetails.AlternateArmIndexValuePercent = m_dataLoan.sRAdjIndexR_rep;

            return _ArmDetails;
        }

        private LoanOriginator CreateLoanOriginator()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            LoanOriginator _LoanOriginator = new LoanOriginator();

            _LoanOriginator.Name = preparer.PreparerName;
            _LoanOriginator.BranchNmlsIdentifier = m_dataLoan.sBranchNmlsIdentifier;
            _LoanOriginator.CompanyNmlsIdentifier = preparer.CompanyLoanOriginatorIdentifier;
            _LoanOriginator.NmlsIdentifier = preparer.LoanOriginatorIdentifier;

            return _LoanOriginator;
        }

        private LenderDetails CreateLenderDetails()
        {
            LenderDetails _LenderDetails = new LenderDetails();

            if (!(m_dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale && m_dataLoan.BrokerDB.ComplianceEaseSuppressLenderNMLSForWholesaleChannel))
            {
                _LenderDetails.LenderCompanyNMLSIdentifier = m_dataLoan.BrokerDB.NmLsIdentifier;
            }

            if (m_dataLoan.BrokerDB.BillingVersion != E_BrokerBillingVersion.PerTransaction
                || (m_dataLoan.BrokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction
                && !m_dataLoan.BrokerDB.IsEnablePTMComplianceEaseIndicator))
            {
                _LenderDetails.LenderHudApprovalStatusType = E_LenderDetailsLenderHudApprovalStatusType.Default;
                _LenderDetails.LenderLicenseType = "Default";

                if (m_dataLoan.sLienPosT == E_sLienPosT.First)
                {
                    _LenderDetails.LenderDidmcaExemptionIndicator = E_DefaultAbleIndicator.Default;
                }
            }
            else
            {
                _LenderDetails.LenderHudApprovalStatusType = m_dataLoan.BrokerDB.ComplianceEaseExportConfig.HUDApproval;
                _LenderDetails.LenderLicenseType = m_dataLoan.BrokerDB.ComplianceEaseExportConfig.GetLicenseForState(m_dataLoan.sSpState);

                if (m_dataLoan.sLienPosT == E_sLienPosT.First)
                {
                    if (m_dataLoan.BrokerDB.ComplianceEaseExportConfig.DIDMCAExemptions.Contains(m_dataLoan.sSpState))
                    {
                        _LenderDetails.LenderDidmcaExemptionIndicator = E_DefaultAbleIndicator.Y;
                    }
                    else
                    {
                        _LenderDetails.LenderDidmcaExemptionIndicator = E_DefaultAbleIndicator.N;
                    }
                }
            }

            return _LenderDetails;
        }

        private Investor CreateInvestor()
        {
            Investor _Investor = new Investor();

            _Investor.Name = "Default"; // 10/7/2011 dd - OPM 69055

            return _Investor;
        }

        private Investor CreateInvestor(string name)
        {
            Investor _Investor = new Investor();
            _Investor.Name = name;
            return _Investor;
        }

        private ConstructionLoanDetails CreateConstructionLoanDetails()
        {
            ConstructionLoanDetails _ConstructionLoanDetails = new ConstructionLoanDetails();

            _ConstructionLoanDetails.ConstructionInterestEstimationType = this.Convert(this.m_dataLoan.sConstructionIntCalcT);
            _ConstructionLoanDetails.ConstructionLoanInterestReserveAmount = this.m_dataLoan.sIntReserveAmt_rep;

            return _ConstructionLoanDetails;
        }

        private E_ConstructionLoanDetailsConstructionInterestEstimationType Convert(ConstructionIntCalcType calcType)
        {
            switch (calcType)
            {
                case ConstructionIntCalcType.Blank:
                    return E_ConstructionLoanDetailsConstructionInterestEstimationType.Undefined;
                case ConstructionIntCalcType.FullCommitment:
                    return E_ConstructionLoanDetailsConstructionInterestEstimationType.EntireCommitment;
                case ConstructionIntCalcType.HalfCommitment:
                    return E_ConstructionLoanDetailsConstructionInterestEstimationType.AmountAdvanced;
                default:
                    throw new UnhandledEnumException(calcType);
            }
        }

        private AdditionalCaseData CreateAdditionalCaseData()
        {
            AdditionalCaseData _AdditionalCaseData = new AdditionalCaseData();

            // _AdditionalCaseData.DatumList.Add(CreateDatum());
            _AdditionalCaseData.InvestorData = CreateInvestorData();
            _AdditionalCaseData.TransmittalData = CreateTransmittalData();
            return _AdditionalCaseData;
        }

        private TransmittalData CreateTransmittalData()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            TransmittalData _TransmittalData = new TransmittalData();

            _TransmittalData.ApplicationReceivedByCreditorDate = m_dataLoan.sAppReceivedByLenderD_rep;
            _TransmittalData.ApplicationReceivedByOriginatorDate = m_dataLoan.sAppSubmittedD_rep;
            // _TransmittalData.AttorneyFeeExcludableByBorrowerChoiceIndicator = ;
            // _TransmittalData.AttorneyFeeOtherExcludableByBorrowerChoiceIndicator = ;
            _TransmittalData.BorrowerDebtAndLiabilitiesAccurateAndVerifiedIndicator = Convert(m_dataLoan.sQMIsVerifiedIncomeAndAssets);
            _TransmittalData.BorrowerDtiRatioPercent = m_dataLoan.sQualBottomR_rep;
            _TransmittalData.BorrowerIncomeAndAssetsAccurateAndVerifiedIndicator = Convert(m_dataLoan.sQMIsVerifiedIncomeAndAssets);

            if (m_dataLoan.BrokerDB.CustomerCode.StartsWith("PML0213"))
            {
                // 10/8/2014 dd - Per OPM 193287 - We are exporting two custom fields for CMG.
                _TransmittalData.ClientDefinedField1Value = Tools.GetDescription(m_dataLoan.Branch.BranchChannelT);
                _TransmittalData.ClientDefinedField2Value = m_dataLoan.Branch.Division;
            }
            else if (m_dataLoan.BrokerDB.CustomerCode.StartsWith("PML0260"))
            {
                // 7/21/2015 jl - Exporting branch name in custom field for HMAC (OPM 219696)
                _TransmittalData.ClientDefinedField1Value = m_dataLoan.Branch.Name;
            }

            // _TransmittalData.ClientDefinedProductField1Value = ;
            // _TransmittalData.ClientDefinedProductField2Value = ;
            _TransmittalData.ComplianceAuditType = m_auditType;
            //_TransmittalData.DiscountPointsGSEBonaFideIndicator = E_DefaultAbleIndicator.Default;// 10/7/2011 dd - OPM 69055
            //_TransmittalData.DiscountPointsStateBonaFideIndicator = LosConvert.GfeItemProps_BF(m_dataLoan.sGfeDiscountPointFProps) ? E_DefaultAbleIndicator.Y : E_DefaultAbleIndicator.N; // 5/10/12 vm - OPM 77851

            _TransmittalData.DisbursementDateDocumentSourceTypeName = this.m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE
                ? "Final HUD-1 Settlement Statement"
                : "Closing Disclosure Documentation";

            // _TransmittalData.IrregularPaymentTransactionIndicator = ;
            _TransmittalData.FinanceChargeCreditAmount = m_dataLoan.sComplianceEaseFinanceChargeCreditAmount_rep; // opm 143384.
            _TransmittalData.GSEOrFederalAgencyEligibleIndicator = Convert(m_dataLoan.sQMIsEligibleByLoanPurchaseAgency);
            //_TransmittalData.HighCostMortgageDisclosureDate = m_dataLoan.
            _TransmittalData.HomeownershipCounselingOrganizationsDisclosureDate = m_dataLoan.sHomeownerCounselingOrganizationDisclosureD_rep;
            //_TransmittalData.LoanApplicationDate = preparer.PrepareDate_rep;
            _TransmittalData.NMLSVerificationType = E_TransmittalDataNMLSVerificationType.Default;
            _TransmittalData.OriginalCltvRatioPercent = m_dataLoan.sCltvR_rep;
            _TransmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprVal_rep;
            _TransmittalData.ProposedHousingExpenseMortgageInsurancePaymentAmount = m_dataLoan.sProMIns_rep;
            _TransmittalData.ProposedHousingExpenseMortgagePrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            _TransmittalData.QualifiedMortgageType = m_dataLoan.sQMIsEligibleByLoanPurchaseAgency ? E_TransmittalDataQualifiedMortgageType.AgencyQualifiedMortgage : E_TransmittalDataQualifiedMortgageType.GeneralQualifiedMortgage;
            _TransmittalData.RateLockDate = m_dataLoan.sRLckdD_rep != string.Empty ? m_dataLoan.sRLckdD_rep : m_dataLoan.sSubmitD_rep;
            // _TransmittalData.RefinancingPortfolioLoanIndicator = ;
            // _TransmittalData.SellerIdentifier = ;
            if (m_dataLoan.sQMParR != 0
                && m_dataLoan.sGfeDiscountPointF > 0                              // opm 147836
                && LosConvert.GfeItemProps_BF(m_dataLoan.sGfeDiscountPointFProps) // opm 147836
                )
            {
                _TransmittalData.UndiscountedInterestRatePercent = m_dataLoan.sQMParR_rep;
            }

            if (use2015DataLayer)
            {
                _TransmittalData.ConsummationDate = m_dataLoan.sDocMagicClosingD_rep;
            }

            _TransmittalData.IncludeInvestmentPropertiesInQualifiedMortgageTestsIndicator
                = m_dataLoan.sIsExemptFromAtr ? E_DefaultAbleIndicator.N : E_DefaultAbleIndicator.Y;
            
            return _TransmittalData;
        }

        private E_YesNoIndicator Convert(bool value)
        {
            return value ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
        }

        private InvestorData CreateInvestorData()
        {
            InvestorData _InvestorData = new InvestorData();

            return _InvestorData;
        }

        private Datum CreateDatum()
        {
            Datum _Datum = new Datum();

            // _Datum.Name = ;
            // _Datum.Value = ;

            return _Datum;
        }

        private Loan CreateLoan()
        {
            Loan _Loan = new Loan();

            _Loan.Application = CreateApplication();
            _Loan.ClosingDocuments = CreateClosingDocuments();
            return _Loan;
        }

        private ClosingDocuments CreateClosingDocuments()
        {
            ClosingDocuments _ClosingDocuments = new ClosingDocuments();

            _ClosingDocuments.Lender = CreateLender();
            _ClosingDocuments.LoanDetails = CreateLoanDetails();
            return _ClosingDocuments;
        }

        private LoanDetails CreateLoanDetails()
        {
            LoanDetails _LoanDetails = new LoanDetails();
            _LoanDetails.ClosingDate = m_dataLoan.sDocMagicClosingD_rep;            // opm 141713
            _LoanDetails.DisbursementDate = m_dataLoan.sDocMagicDisbursementD_rep;  // opm 141713
            _LoanDetails.OriginalLtvRatioPercent = m_dataLoan.sLtvR_rep;
            _LoanDetails.TruthInLendingDisclosureDate = m_dataLoan.sTilInitialDisclosureD_rep;
            _LoanDetails.InterimInterest = CreateInterimInterest();
            _LoanDetails.GfeDetail = CreateGfeDetail();
            return _LoanDetails;
        }

        private GfeDetail CreateGfeDetail()
        {
            GfeDetail _GfeDetail = new GfeDetail();
            _GfeDetail.GfeDisclosureDate = m_dataLoan.sGfeInitialDisclosureD_rep;
            return _GfeDetail;
        }

        private InterimInterest CreateInterimInterest()
        {
            InterimInterest _InterimInterest = new InterimInterest();

            _InterimInterest.PaidNumberOfDays = m_dataLoan.sIPiaDy_rep;
            _InterimInterest.PerDiemCalculationScaleType = E_PerDiemCalculationScaleType._4;
            _InterimInterest.AdditionalInterestDaysRequiringConsentCount = this.m_dataLoan.sAdditionalInterestDaysRequiringConsent == 0 ? "0" : this.m_dataLoan.sAdditionalInterestDaysRequiringConsent_rep;

            E_PerDiemCalculationMethodType? perDiemCalcMethod = GetPerDiemCalculationMethod(m_dataLoan.sDaysInYr);
            if (perDiemCalcMethod.HasValue)
            {
                _InterimInterest.PerDiemCalculationMethodType = perDiemCalcMethod.Value;
            }

            return _InterimInterest;
        }

        private E_PerDiemCalculationMethodType? GetPerDiemCalculationMethod(int daysInYear)
        {
            if (daysInYear == 360)
            {
                return E_PerDiemCalculationMethodType._360;
            }
            else if (daysInYear == 364)
            {
                return E_PerDiemCalculationMethodType._364;
            }
            else if (daysInYear == 365)
            {
                return E_PerDiemCalculationMethodType._365;
            }
            else if (daysInYear == 366)
            {
                return E_PerDiemCalculationMethodType._366;
            }

            return null;
        }

        private Lender CreateLender()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            Lender _Lender = new Lender();
            // 4/20/2012 dd - OPM 82629 - If Lender name is not available in official contact list then use name define in Broker General settings.
            if (string.IsNullOrEmpty(agent.CompanyName) == false)
            {

                _Lender.UnparsedName = agent.CompanyName;
            }
            else
            {
                _Lender.UnparsedName = m_dataLoan.BrokerDB.Name;
            }

            return _Lender;
        }

        private Application CreateApplication()
        {
            Application _Application = new Application();

            _Application.GovernmentLoan = CreateGovernmentLoan();
            _Application.LoanProductData = CreateLoanProductData();
            _Application.LoanPurpose = CreateLoanPurpose();
            _Application.Mers = CreateMers();
            _Application.MiData = CreateMiData();
            _Application.MortgageTerms = CreateMortgageTerms();
            _Application.Property = CreateProperty();
            _Application.TransactionDetail = CreateTransactionDetail();
            _Application.Borrower = CreateBorrower();
            _Application.UrlaTotal = CreateUrlaTotal();
            return _Application;
        }

        private UrlaTotal CreateUrlaTotal()
        {
            UrlaTotal _UrlaTotal = new UrlaTotal();

            _UrlaTotal.CombinedProposedHousingExpenseAmount = m_dataLoan.sTransmProTotHExp_rep;
            _UrlaTotal.LiabilityMonthlyPaymentsAmount = m_dataLoan.sTotNonHExpMonPmt_rep;
            _UrlaTotal.MonthlyIncomeAmount = m_dataLoan.sLTotI_rep;

            return _UrlaTotal;
        }

        private Borrower CreateBorrower()
        {
            Borrower _Borrower = new Borrower();

            CAppData dataApp = m_dataLoan.GetAppData(0);
            _Borrower.FirstName = dataApp.aBFirstNm;
            _Borrower.LastName = dataApp.aBLastNm;

            return _Borrower;
        }

        private TransactionDetail CreateTransactionDetail()
        {
            TransactionDetail _TransactionDetail = new TransactionDetail();

            if (m_dataLoan.sFfUfmipFinanced != 0)
            {
                _TransactionDetail.MiAndFundingFeeFinancedAmount = m_dataLoan.sFfUfmipFinanced_rep;
            }

            if ((m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other || m_dataLoan.sLT == E_sLT.VA) &&
                m_dataLoan.sFfUfmip1003 != 0)
            {
                _TransactionDetail.MiAndFundingFeeTotalAmount = m_dataLoan.sFfUfmip1003_rep;
            }

            return _TransactionDetail;
        }
        
        /// <summary>
        /// Remove paranthetical expressions before exporting. opm 135514
        /// </summary>
        private string ConvertCity(string city)
        {
            return System.Text.RegularExpressions.Regex.Replace(city, @"\(.*\)", "").TrimWhitespaceAndBOM();
        }

        private Property CreateProperty()
        {
            Property _Property = new Property();

            _Property.City = ConvertCity(m_dataLoan.sSpCity);  // opm 135514
            _Property.County = m_dataLoan.sSpCounty;
            _Property.State = ConvertState(m_dataLoan.sSpState);
            _Property.PostalCode = m_dataLoan.sSpZip;
            _Property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;

            _Property.ParsedStreetAddress = CreateParsedStreetAddress(m_dataLoan.sSpAddr);
            return _Property;
        }

        private E_PropertyState ConvertState(string state)
        {
            if (string.IsNullOrEmpty(state))
            {
                return E_PropertyState.Undefined;
            }

            switch (state.ToUpper())
            {
                case "AK":
                    return E_PropertyState.AK;
                case "AL":
                    return E_PropertyState.AL;
                case "AR":
                    return E_PropertyState.AR;
                case "AZ":
                    return E_PropertyState.AZ;
                case "CA":
                    return E_PropertyState.CA;
                case "CO":
                    return E_PropertyState.CO;
                case "CT":
                    return E_PropertyState.CT;
                case "DC":
                    return E_PropertyState.DC;
                case "DE":
                    return E_PropertyState.DE;
                case "FL":
                    return E_PropertyState.FL;
                case "GA":
                    return E_PropertyState.GA;
                case "HI":
                    return E_PropertyState.HI;
                case "IA":
                    return E_PropertyState.IA;
                case "ID":
                    return E_PropertyState.ID;
                case "IL":
                    return E_PropertyState.IL;
                case "IN":
                    return E_PropertyState.IN;
                case "KS":
                    return E_PropertyState.KS;
                case "KY":
                    return E_PropertyState.KY;
                case "LA":
                    return E_PropertyState.LA;
                case "MA":
                    return E_PropertyState.MA;
                case "MD":
                    return E_PropertyState.MD;
                case "ME":
                    return E_PropertyState.ME;
                case "MI":
                    return E_PropertyState.MI;
                case "MN":
                    return E_PropertyState.MN;
                case "MO":
                    return E_PropertyState.MO;
                case "MS":
                    return E_PropertyState.MS;
                case "MT":
                    return E_PropertyState.MT;
                case "NC":
                    return E_PropertyState.NC;
                case "ND":
                    return E_PropertyState.ND;
                case "NE":
                    return E_PropertyState.NE;
                case "NH":
                    return E_PropertyState.NH;
                case "NJ":
                    return E_PropertyState.NJ;
                case "NM":
                    return E_PropertyState.NM;
                case "NV":
                    return E_PropertyState.NV;
                case "NY":
                    return E_PropertyState.NY;
                case "OH":
                    return E_PropertyState.OH;
                case "OK":
                    return E_PropertyState.OK;
                case "OR":
                    return E_PropertyState.OR;
                case "PA":
                    return E_PropertyState.PA;
                case "PR":
                    return E_PropertyState.PR;
                case "RI":
                    return E_PropertyState.RI;
                case "SC":
                    return E_PropertyState.SC;
                case "SD":
                    return E_PropertyState.SD;
                case "TN":
                    return E_PropertyState.TN;
                case "TX":
                    return E_PropertyState.TX;
                case "UT":
                    return E_PropertyState.UT;
                case "VA":
                    return E_PropertyState.VA;
                case "VT":
                    return E_PropertyState.VT;
                case "WA":
                    return E_PropertyState.WA;
                case "WI":
                    return E_PropertyState.WI;
                case "WV":
                    return E_PropertyState.WV;
                case "WY":
                    return E_PropertyState.WY;
                default:
                    return E_PropertyState.Undefined;
            }
        }

        private ParsedStreetAddress CreateParsedStreetAddress(string address)
        {
            ParsedStreetAddress _ParsedStreetAddress = new ParsedStreetAddress();
            CommonLib.Address addr = new CommonLib.Address();
            try
            {
                addr.ParseStreetAddress(address);
            }
            catch (Exception e)
            {
                if (!"To Be Determined".Equals(address, StringComparison.InvariantCultureIgnoreCase)
                    && !"TBD".Equals(address, StringComparison.InvariantCultureIgnoreCase))
                {
                    string msg = string.Format("ComplianceEaseExport could not parse address for loan '{0}' and address '{1}'. Parse Error Message={2}", m_dataLoan.sLId, address, e.Message);
                    Tools.LogInfo("ComplianceEaseExport", msg);
                }
                return null;
            }

            _ParsedStreetAddress.ApartmentOrUnit = addr.AptNum;

            _ParsedStreetAddress.DirectionSuffix = ConvertToParsedStreetAddressDirectionSuffix(addr.StreetDir);
            if (!string.IsNullOrEmpty(_ParsedStreetAddress.ApartmentOrUnit)) _ParsedStreetAddress.BuildingNumber = addr.StreetNumber;
            else _ParsedStreetAddress.HouseNumber = addr.StreetNumber;
            _ParsedStreetAddress.StreetName = addr.StreetName;
            _ParsedStreetAddress.StreetSuffix = addr.StreetType;

            return _ParsedStreetAddress;
        }

        private static E_ParsedStreetAddressDirectionSuffix ConvertToParsedStreetAddressDirectionSuffix(CommonLib.Address.STREET_DIR street_dir)
        {
            switch (street_dir)
            {
                case CommonLib.Address.STREET_DIR.E:
                    return E_ParsedStreetAddressDirectionSuffix.E;
                case CommonLib.Address.STREET_DIR.N:
                    return E_ParsedStreetAddressDirectionSuffix.N;
                case CommonLib.Address.STREET_DIR.NE:
                    return E_ParsedStreetAddressDirectionSuffix.NE;
                case CommonLib.Address.STREET_DIR.NW:
                    return E_ParsedStreetAddressDirectionSuffix.NW;
                case CommonLib.Address.STREET_DIR.S:
                    return E_ParsedStreetAddressDirectionSuffix.S;
                case CommonLib.Address.STREET_DIR.SE:
                    return E_ParsedStreetAddressDirectionSuffix.SE;
                case CommonLib.Address.STREET_DIR.SW:
                    return E_ParsedStreetAddressDirectionSuffix.SW;
                case CommonLib.Address.STREET_DIR.W:
                    return E_ParsedStreetAddressDirectionSuffix.W;
                case CommonLib.Address.STREET_DIR.blank:
                    return E_ParsedStreetAddressDirectionSuffix.Undefined;
                default:
                    throw new UnhandledEnumException(street_dir);
            }
        }

        private MortgageTerms CreateMortgageTerms()
        {
            MortgageTerms _MortgageTerms = new MortgageTerms();

            _MortgageTerms.BaseLoanAmount = m_dataLoan.sLAmtCalc_rep;
            _MortgageTerms.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            _MortgageTerms.LoanAmortizationType = Convert(m_dataLoan.sFinMethT);
            _MortgageTerms.LoanEstimatedClosingDate = m_dataLoan.sEstCloseD_rep;
            _MortgageTerms.MortgageType = Convert(m_dataLoan.sLT);
            _MortgageTerms.NoteRatePercent = m_dataLoan.sNoteIR_rep;
            _MortgageTerms.LenderLoanIdentifier = m_dataLoan.sLNm;

            return _MortgageTerms;
        }

        private E_MortgageTermsMortgageType Convert(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.Conventional:
                    return E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA:
                    return E_MortgageTermsMortgageType.FHA;
                case E_sLT.VA:
                    return E_MortgageTermsMortgageType.VA;
                case E_sLT.UsdaRural:
                    return E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.Other:
                    return E_MortgageTermsMortgageType.Other;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }

        private E_MortgageTermsLoanAmortizationType Convert(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed:
                    return E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.ARM:
                    return E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Graduated:
                    return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    throw new UnhandledEnumException(sFinMethT);
            }
        }

        private bool SendMiPremiumData
        {
            get
            {
                return m_dataLoan.sProMIns > 0.0M && (m_dataLoan.sLT == E_sLT.Conventional || m_dataLoan.sLT == E_sLT.Other);
            }
        }

        private MiData CreateMiData()
        {
            MiData _MiData = new MiData();

            if (E_sLT.Conventional == m_dataLoan.sLT || E_sLT.FHA == m_dataLoan.sLT || E_sLT.UsdaRural == m_dataLoan.sLT) // 108217
            {
                _MiData.MiLtvCutoffPercent = m_dataLoan.sProMInsCancelLtv_rep;
            }

            if (SendMiPremiumData)
            {
                //av opm 116293 we will need to change the export such that we ALWAYS send sProMIns
                //to _MiData.MiInitialPremiumAmount regardless if it is locked or not, and never send a
                //value for _MiData.MiInitialPremiumRatePercent.
                _MiData.MiInitialPremiumAmount = m_dataLoan.sProMIns_rep;
                _MiData.MiRenewalPremium = CreateMiRenewalPremium();
                // _MiData.MiInitialPremiumRatePercent = m_dataLoan.sProMInsR_rep;
                _MiData.MiInitialPremiumRateDurationMonths = m_dataLoan.sProMInsMon_rep;
            }

            if (E_sLT.FHA == m_dataLoan.sLT ||
                E_sLT.UsdaRural == m_dataLoan.sLT)
            {
                _MiData.MiInitialPremiumRateDurationMonths = m_dataLoan.sProMInsMon_rep;
                _MiData.MiInitialPremiumRatePercent = m_dataLoan.sProMInsR_rep;
            }

            return _MiData;
        }

        private MiRenewalPremium CreateMiRenewalPremium()
        {
            MiRenewalPremium _MiRenewalPremium = new MiRenewalPremium();

            //_MiRenewalPremium.MonthlyPaymentAmount = m_dataLoan.sProMIns2_rep;
            _MiRenewalPremium.Rate = m_dataLoan.sCompEaseExpsProMInsR2_rep;
            _MiRenewalPremium.RateDurationMonths = m_dataLoan.sProMIns2Mon_rep;

            return _MiRenewalPremium;
        }

        private Mers CreateMers()
        {
            if (string.IsNullOrEmpty(m_dataLoan.sMersMin))
            {
                return null;
            }

            Mers _Mers = new Mers();

            _Mers.MersMinNumber = m_dataLoan.sMersMin;

            return _Mers;
        }

        private LoanPurpose CreateLoanPurpose()
        {
            LoanPurpose _LoanPurpose = new LoanPurpose();

            _LoanPurpose.PropertyUsageType = Convert(m_dataLoan.GetAppData(0).aOccT);

            _LoanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();
            return _LoanPurpose;
        }

        private E_LoanPurposePropertyUsageType Convert(E_aOccT aOccT)
        {
            switch (aOccT)
            {
                case E_aOccT.PrimaryResidence:
                    return E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence:
                    return E_LoanPurposePropertyUsageType.SecondHome;
                case E_aOccT.Investment:
                    return E_LoanPurposePropertyUsageType.Investment;
                default:
                    throw new UnhandledEnumException(aOccT);
            }
        }

        private ConstructionRefinanceData CreateConstructionRefinanceData()
        {
            ConstructionRefinanceData _ConstructionRefinanceData = new ConstructionRefinanceData();

            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || 
                m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl ||
                m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                _ConstructionRefinanceData.GseRefinancePurposeType = GseRefinancePurposeType(m_dataLoan.sGseRefPurposeT);
                if (m_dataLoan.sSpState == "TX" && !m_dataLoan.sProdIsTexas50a6Loan)
                {
                    _ConstructionRefinanceData.GseRefinancePurposeType = E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther; // 2/7/2014 tj - opm 150582
                }
            }
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                _ConstructionRefinanceData.ConstructionPeriodInterestRatePercent = this.m_dataLoan.sConstructionPeriodIR_rep;
                _ConstructionRefinanceData.ConstructionPeriodNumberOfMonthsCount = this.m_dataLoan.sConstructionPeriodMon_rep;
            }

            return _ConstructionRefinanceData;
        }

        private E_ConstructionRefinanceDataGseRefinancePurposeType GseRefinancePurposeType(E_sGseRefPurposeT eGSERefiPurpose)
        {
            switch (eGSERefiPurpose)
            {
                case E_sGseRefPurposeT.CashOutDebtConsolidation: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case E_sGseRefPurposeT.CashOutHomeImprovement: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case E_sGseRefPurposeT.CashOutLimited: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case E_sGseRefPurposeT.CashOutOther: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                case E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance:
                case E_sGseRefPurposeT.NoCashOutFREOwnedRefinance:
                case E_sGseRefPurposeT.NoCashOutOther:
                case E_sGseRefPurposeT.NoCashOutStreamlinedRefinance:
                case E_sGseRefPurposeT.ChangeInRateTerm: return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                default: return E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;
            }
        }

        private LoanProductData CreateLoanProductData()
        {
            LoanProductData _LoanProductData = new LoanProductData();

            _LoanProductData.Arm = CreateArm();
            _LoanProductData.LoanFeatures = CreateLoanFeatures();
            // _LoanProductData.PaymentAdjustmentList.Add(CreatePaymentAdjustment());
            _LoanProductData.RateAdjustment = CreateRateAdjustment();

            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536
                _LoanProductData.Heloc = CreateHeloc();
            }

            _LoanProductData.InterestOnly = CreateInterestOnly();
            _LoanProductData.PrepaymentPenalty = CreatePrepaymentPenalty();
            return _LoanProductData;
        }

        private PrepaymentPenalty CreatePrepaymentPenalty()
        {
            PrepaymentPenalty _PrepaymentPenalty = new PrepaymentPenalty();

            var termMonthsRep = m_dataLoan.sProdPpmtPenaltyMonKeyword_rep;
            _PrepaymentPenalty.TermMonths = termMonthsRep == "" ? "0" : termMonthsRep;
            
            return _PrepaymentPenalty;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <returns>An INTEGRATED_DISCLOSURE element.</returns>
        private IntegratedDisclosure CreateIntegratedDisclosure()
        {
            IntegratedDisclosure _IntegratedDisclosure = new IntegratedDisclosure();
            bool singleDisclosureFormat = true; // Currently we only support the single-disclosure module.
            var losConvert = m_dataLoan.m_convertLos;

            if (singleDisclosureFormat)
            {
                var initialLoanEstimate = m_dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate;
                if (initialLoanEstimate != null)
                {
                    _IntegratedDisclosure.DocumentType = E_DocumentType.LoanEstimate;
                    _IntegratedDisclosure.InitialLoanEstimateDeliveryDate = losConvert.ToDateTimeString(initialLoanEstimate.IssuedDate);

                    int numDisclosedLoanEstimates = m_dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Count(
                        le => m_dataLoan.sClosingCostArchive.Any(
                            archive => archive.Id == le.ArchiveId && archive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed));
                    bool loanEstimateHasBeenRevised = numDisclosedLoanEstimates > 1;

                    if (loanEstimateHasBeenRevised)
                    {
                        _IntegratedDisclosure.RevisedLoanEstimateDeliveryDate = m_dataLoan.sLastDisclosedLoanEstimateIssuedD_rep;
                    }
                }

                var initialClosingDisclosure = m_dataLoan.sClosingDisclosureDatesInfo.InitialClosingDisclosure;
                if (initialClosingDisclosure != null)
                {
                    _IntegratedDisclosure.DocumentType = E_DocumentType.ClosingDisclosure;
                    _IntegratedDisclosure.InitialClosingDisclosureDeliveryDate = losConvert.ToDateTimeString(initialClosingDisclosure.IssuedDate);
                    _IntegratedDisclosure.InitialClosingDisclosureDeliveryMethodType = Convert(initialClosingDisclosure.DeliveryMethod);
                    _IntegratedDisclosure.InitialClosingDisclosureReceiptDate = losConvert.ToDateTimeString(initialClosingDisclosure.ReceivedDate);
                }

                var latestClosingDisclosure = m_dataLoan.sLastRevisedClosingDisclosure;
                if (latestClosingDisclosure != null && latestClosingDisclosure.IsPostClosing)
                {
                    _IntegratedDisclosure.DocumentType = E_DocumentType.PostConsummationRevisedClosingDisclosure;
                    _IntegratedDisclosure.PostConsummationClosingDisclosureDeliveryDate = losConvert.ToDateTimeString(latestClosingDisclosure.IssuedDate);
                    _IntegratedDisclosure.PostConsummationRedisclosureReasonType = this.GetPostConsummationRedisclosureReason(latestClosingDisclosure);
                    _IntegratedDisclosure.PostConsummationRedisclosureReasonDate = losConvert.ToDateTimeString(latestClosingDisclosure.PostConsummationRedisclosureReasonDate);
                    _IntegratedDisclosure.PostConsummationKnowledgeOfEventDate = losConvert.ToDateTimeString(latestClosingDisclosure.PostConsummationKnowledgeOfEventDate);
                }
            }
            else
            {
                // Determine whether the latest disclosure is a Loan Estimate or Closing Disclosure.
                if (m_dataLoan.sClosingDisclosureDatesInfo.InitialClosingDisclosure != null)
                {
                    var latestDisclosure = m_dataLoan.sLastRevisedClosingDisclosure ?? m_dataLoan.sClosingDisclosureDatesInfo.InitialClosingDisclosure;
                    _IntegratedDisclosure.DocumentType = E_DocumentType.ClosingDisclosure;
                    _IntegratedDisclosure.DeliveryDate = losConvert.ToDateTimeString(latestDisclosure.IssuedDate);
                    _IntegratedDisclosure.DeliveryMethodType = Convert(latestDisclosure.DeliveryMethod);
                    _IntegratedDisclosure.DocumentIssuanceType = latestDisclosure.IsInitial ? E_DocumentIssuanceType.Initial : E_DocumentIssuanceType.Revised;
                    _IntegratedDisclosure.ReceiptDate = losConvert.ToDateTimeString(latestDisclosure.ReceivedDate);

                    // SequenceNumber distinguishes between multiple disclosures provided on the same day.
                    int sequenceNumber = 0;
                    foreach (var closingDisclosure in m_dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
                    {
                        if (latestDisclosure.IssuedDate.Date.Equals(closingDisclosure.IssuedDate.Date))
                        {
                            sequenceNumber++;
                        }
                    }

                    _IntegratedDisclosure.SequenceNumber = sequenceNumber.ToString();
                }
                else
                {
                    var latestDisclosure = m_dataLoan.sLoanEstimateDatesInfo.LastRevisedLoanEstimate ?? m_dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate;
                    _IntegratedDisclosure.DocumentType = E_DocumentType.LoanEstimate;
                    _IntegratedDisclosure.DeliveryDate = losConvert.ToDateTimeString(latestDisclosure.IssuedDate);
                    _IntegratedDisclosure.DeliveryMethodType = Convert(latestDisclosure.DeliveryMethod);
                    _IntegratedDisclosure.DocumentIssuanceType = latestDisclosure.IsInitial ? E_DocumentIssuanceType.Initial : E_DocumentIssuanceType.Revised;
                    _IntegratedDisclosure.ReceiptDate = losConvert.ToDateTimeString(latestDisclosure.ReceivedDate);

                    // SequenceNumber distinguishes between multiple disclosures provided on the same day.
                    int sequenceNumber = 0;
                    foreach (var loanEstimate in m_dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
                    {
                        if (latestDisclosure.IssuedDate.Date.Equals(loanEstimate.IssuedDate.Date))
                        {
                            sequenceNumber++;
                        }
                    }

                    _IntegratedDisclosure.SequenceNumber = sequenceNumber.ToString();
                }
            }

            _IntegratedDisclosure.LenderCredits = this.CreateLenderCredits();
            if (!_IntegratedDisclosure.DocumentType.HasValue)
            {
                _IntegratedDisclosure = null; // OPM 227660 Attribute "_DocumentType" on element "INTEGRATED_DISCLOSURE" is required and must be specified
            }

            return _IntegratedDisclosure;
        }

        /// <summary>
        /// Creates a LENDER_CREDITS element.
        /// </summary>
        /// <returns>A LENDER_CREDITS element.</returns>
        private LenderCredits CreateLenderCredits()
        {
            var lenderCredits = new LenderCredits();
            lenderCredits.Amount = this.m_dataLoan.sTRIDClosingDisclosureGeneralLenderCreditsLessToleranceCures_rep;

            return lenderCredits;
        }

        /// <summary>
        /// Determines the post-consummation redisclosure reason from a <see cref="ClosingDisclosureDates"/> instance.
        /// </summary>
        /// <param name="cd">The closing disclosure dates object.</param>
        /// <returns>The post-consummation redisclosure reason, if any.</returns>
        private E_PostConsummationRedisclosureReasonType? GetPostConsummationRedisclosureReason(ClosingDisclosureDates cd)
        {
            if (cd == null || !cd.IsPostClosing)
            {
                return null;
            }

            // We have 4 reasons but only map 2 of them. See case 235989 for details.
            if (cd.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower)
            {
                return E_PostConsummationRedisclosureReasonType.ChangeToAmountPaidByBorrower;
            }
            else if (cd.IsDisclosurePostClosingDueToNonNumericalClericalError)
            {
                return E_PostConsummationRedisclosureReasonType.NonNumericClericalError;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Converts the given delivery method to a ComplianceEase delivery method type.
        /// </summary>
        /// <param name="deliveryMethod">The delivery method to convert.</param>
        /// <returns>The corresponding ComplianceEase <see cref="E_DeliveryMethodType" /> value.</returns>
        private E_DeliveryMethodType? Convert(E_DeliveryMethodT deliveryMethod)
        {
            switch (deliveryMethod)
            {
                case E_DeliveryMethodT.LeaveEmpty:
                    return null;
                case E_DeliveryMethodT.Email:
                case E_DeliveryMethodT.Fax:
                    return E_DeliveryMethodType.ElectronicDelivery;
                case E_DeliveryMethodT.InPerson:
                    return E_DeliveryMethodType.InPerson;
                case E_DeliveryMethodT.Mail:
                case E_DeliveryMethodT.Overnight:
                    return E_DeliveryMethodType.USPSFirstClassMail;
                default:
                    throw new UnhandledEnumException(deliveryMethod);
            }
        }

        private InterestOnly CreateInterestOnly()
        {
            if (m_dataLoan.sIOnlyMon_rep == "")
            {
                return null;
            }

            InterestOnly _InterestOnly = new InterestOnly();

            _InterestOnly.TermMonthsCount = m_dataLoan.sIOnlyMon_rep;

            return _InterestOnly;
        }

        private Heloc CreateHeloc()
        {
            Heloc _Heloc = new Heloc();

            _Heloc.AnnualFeeAmount = m_dataLoan.sHelocAnnualFee_rep;
            _Heloc.DrawPeriodMonthsCount = m_dataLoan.sHelocDraw_rep;
            _Heloc.MinimumAdvanceAmount = m_dataLoan.sHelocMinimumAdvanceAmt_rep;
            _Heloc.RepayPeriodMonthsCount = m_dataLoan.sHelocRepay_rep;
            _Heloc.TerminationFeeAmount = m_dataLoan.sHelocTerminationFee_rep;

            return _Heloc;
        }

        private RateAdjustment CreateRateAdjustment()
        {
            if (m_dataLoan.sFinMethT != E_sFinMethT.ARM)
            {
                return null;
            }

            RateAdjustment _RateAdjustment = new RateAdjustment();

            _RateAdjustment.FirstRateAdjustmentMonths = m_dataLoan.sRAdj1stCapMon_rep;
            _RateAdjustment.SubsequentCapPercent = m_dataLoan.sRAdjCapR_rep;
            _RateAdjustment.SubsequentRateAdjustmentMonths = m_dataLoan.sRAdjCapMon_rep;
            _RateAdjustment.InitialCapPercent = m_dataLoan.sRAdj1stCapR_rep;

            return _RateAdjustment;
        }

        private PaymentAdjustment CreatePaymentAdjustment()
        {
            PaymentAdjustment _PaymentAdjustment = new PaymentAdjustment();

            // _PaymentAdjustment.DurationMonths = ;
            // _PaymentAdjustment.PeriodNumber = ;
            // _PaymentAdjustment.PeriodicCapPercent = ;

            return _PaymentAdjustment;
        }

        private LoanFeatures CreateLoanFeatures()
        {
            LoanFeatures _LoanFeatures = new LoanFeatures();

            // _LoanFeatures.BalloonIndicator = ;
            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                _LoanFeatures.HelocMaximumBalanceAmount = m_dataLoan.sCreditLineAmt_rep;
            }
            _LoanFeatures.LienPriorityType = Convert(m_dataLoan.sLienPosT);


            // _LoanFeatures.NegativeAmortizationLimitPercent = ;
            _LoanFeatures.LoanOriginalMaturityTermMonths = m_dataLoan.sDue_rep;
            _LoanFeatures.ScheduledFirstPaymentDate = this.m_dataLoan.sSchedDueD1_rep;
            
            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // OPM 228217: Per ComplianceEase: "Investment Properties and Co-Ops should have the RESPAConformingYearType removed if the user wishes to audit these loans in our TRID form."
                if (this.Convert(this.m_dataLoan.sGseSpT) != E_SubjectPropertyType.Cooperative && this.Convert(this.m_dataLoan.GetAppData(0).aOccT) != E_LoanPurposePropertyUsageType.Investment)
                {
                    _LoanFeatures.RespaConformingYearType = E_LoanFeaturesRespaConformingYearType.October2015;
                }
            }
            else 
            {
                _LoanFeatures.RespaConformingYearType = E_LoanFeaturesRespaConformingYearType.January2010;
            }

            // _LoanFeatures.InitialPaymentRatePercent = ;
            _LoanFeatures.LateCharge = CreateLateCharge();
            _LoanFeatures.LoanDocumentationType = GetLoanDocumentationType();
            return _LoanFeatures;
        }

        /// <summary>
        /// per opm 79332
        /// </summary>
        /// <returns></returns>
        private E_LoanFeaturesLoanDocumentationType GetLoanDocumentationType()
        {
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
            }
            else
            {
                switch (m_dataLoan.sProdDocT)
                {
                    case E_sProdDocT.Full:
                        return E_LoanFeaturesLoanDocumentationType.FullDocumentation;
                    case E_sProdDocT.Alt:
                        return E_LoanFeaturesLoanDocumentationType.Alternative;
                    case E_sProdDocT.Light:
                        return E_LoanFeaturesLoanDocumentationType.Reduced;
                    case E_sProdDocT.NINA:
                        return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                    case E_sProdDocT.NISA:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                    case E_sProdDocT.NINANE:
                        return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                    case E_sProdDocT.NIVA:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                    case E_sProdDocT.SISA:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                    case E_sProdDocT.SIVA:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome;
                    case E_sProdDocT.VISA:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
                    case E_sProdDocT.NIVANE:
                        return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                    case E_sProdDocT.VINA:
                        return E_LoanFeaturesLoanDocumentationType.Reduced;
                    case E_sProdDocT.Streamline:
                        return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
                    case E_sProdDocT._12MoPersonalBankStatements:
                    case E_sProdDocT._24MoPersonalBankStatements:
                    case E_sProdDocT._12MoBusinessBankStatements:
                    case E_sProdDocT._24MoBusinessBankStatements:
                    case E_sProdDocT.OtherBankStatements:
                    case E_sProdDocT._1YrTaxReturns:
                    case E_sProdDocT.Voe:
                    case E_sProdDocT.AssetUtilization:
                        return E_LoanFeaturesLoanDocumentationType.Alternative;
                    case E_sProdDocT.DebtServiceCoverage:
                    case E_sProdDocT.NoIncome:
                        return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                    default:
                        throw new CBaseException("sProdDocT " + m_dataLoan.sProdDocT + " is unmapped", "bad sProdDocT sLPurposeT combo");
                    //case E_sProdDocT.Streamline;
                    //    return E_LoanFeaturesLoanDocumentationType.
                    //    throw new NotImplementedException();
                }
            }
        }

        private E_LoanFeaturesLienPriorityType Convert(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First:
                    return E_LoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second:
                    return E_LoanFeaturesLienPriorityType.SecondLien;
                default:
                    throw new UnhandledEnumException(sLienPosT);
            }
        }

        private LateCharge CreateLateCharge()
        {
            LateCharge _LateCharge = new LateCharge();

            _LateCharge.GracePeriod = m_dataLoan.sLateDays;
            string rate = m_dataLoan.sLateChargePc.Replace("%", ""); // 11/18/2010 dd - Remove percent sign.
            decimal v;

            if (decimal.TryParse(rate, out v))
            {
                v = v / 100; // ComplianceEase use rate instead of percent.
            }
            else
            {
                v = 0;
            }

            _LateCharge.Rate = v.ToString();

            return _LateCharge;
        }

        private Arm CreateArm()
        {
            if (m_dataLoan.sFinMethT != E_sFinMethT.ARM)
            {
                return null;
            }

            Arm _Arm = new Arm();

            _Arm.IndexCurrentValuePercent = m_dataLoan.sRAdjIndexR_rep;
            _Arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            decimal lifeRate = 0;
            if (decimal.TryParse(m_dataLoan.sRLifeCapR_rep, out lifeRate))
            {
                lifeRate = lifeRate / 100; // 11/18/2010 dd - ComplianceEase use rate instead of percent here.
            }
            _Arm.LifetimeCapRate = lifeRate.ToString();
            _Arm.LifetimeFloorPercent = m_dataLoan.sRAdjFloorR_rep;
            // _Arm.InterestRateRoundingFactor = ;
            _Arm.InterestRateRoundingType = Convert(m_dataLoan.sRAdjRoundT);

            return _Arm;
        }

        private E_ArmInterestRateRoundingType Convert(E_sRAdjRoundT sRAdjRoundT)
        {
            switch (sRAdjRoundT)
            {
                case E_sRAdjRoundT.Normal:
                    return E_ArmInterestRateRoundingType.Nearest;
                case E_sRAdjRoundT.Up:
                    return E_ArmInterestRateRoundingType.Up;
                case E_sRAdjRoundT.Down:
                    return E_ArmInterestRateRoundingType.Down;
                default:
                    throw new UnhandledEnumException(sRAdjRoundT);
            }
        }

        private GovernmentLoan CreateGovernmentLoan()
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.VA && m_dataLoan.sLT != E_sLT.UsdaRural)
            {
                return null;
            }

            GovernmentLoan _GovernmentLoan = new GovernmentLoan();

            _GovernmentLoan.FhaLoan = CreateFhaLoan();
            _GovernmentLoan.VaLoan = CreateVaLoan();
            return _GovernmentLoan;
        }

        private VaLoan CreateVaLoan()
        {
            if (m_dataLoan.sLT != E_sLT.VA)
            {
                return null;
            }

            VaLoan _VaLoan = new VaLoan();

            _VaLoan.BorrowerFundingFeePercent = m_dataLoan.sFfUfmipR_rep;

            return _VaLoan;
        }

        private FhaLoan CreateFhaLoan()
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.UsdaRural)
            {
                return null;
            }

            FhaLoan _FhaLoan = new FhaLoan();

            _FhaLoan.FhaUpfrontMiPremiumPercent = m_dataLoan.sFfUfmipR_rep;

            return _FhaLoan;
        }
    }
}
