﻿using ComplianceEase;

namespace LendersOffice.Conversions.ComplianceEase
{
    using System;
    using LendersOffice.Security;
    using System.Threading;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using DataAccess;
    using LendersOffice.ObjLib.DatabaseMessageQueue;

    public class AsynchronousComplianceEaseProcessor : CommonProjectLib.Runnable.IRunnable
    {
        protected virtual DBQueue Queue
        {
            get{ return ConstMsg.ComplianceEaseAutoSubmitQueue; }
        }

        protected virtual string LogId
        {
            get { return "CEQUEUE"; }
        }

        public static bool TryExecute(int timeoutInSeconds, Action func)
        {
            var thread = new Thread(() => func());
            thread.IsBackground = true;
            thread.Start();
            var completed = thread.Join(TimeSpan.FromSeconds(timeoutInSeconds));
            if (!completed) thread.Abort();
            return completed;
        }

        public string Description
        {
            get
            {
                return $"Runs compliance ease on loans queued in {this.Queue.Name} queue.";
            }
        }

        public void Run()
        {
            Tools.SetupServicePointManager();
            DBMessageQueue mQ = new DBMessageQueue(this.Queue);

            while (true)
            {
                Tools.ResetLogCorrelationId();
                bool success = false;
                DBMessage message = null;
                Credentials creds = null;

                try
                {
                    message = mQ.Receive();

                    if (message == null)
                    {
                        return; // Empty Queue.
                    }

                    ServiceFault serviceFault = null;
                    Thread.CurrentPrincipal = SystemUserPrincipal.CESystemUser;
                    bool useProduction;

                    if (Boolean.TryParse(message.Data, out useProduction))
                    {
                        if (useProduction)
                        {
                            creds.Username = ConstStage.ComplianceEasePTMLogin;
                            creds.Password = ConstStage.ComplianceEasePTMPassword;
                        }
                        else
                        {
                            creds.Username = ConstStage.ComplianceEasePTMTestLogin;
                            creds.Password = ConstStage.ComplianceEasePTMTestPassword;
                        }

                        creds.AttemptCount = 0;
                    }
                    else
                    {
                        creds = ObsoleteSerializationHelper.JsonDeserialize<Credentials>(message.Data);
                    }

                    // Remove any test request that managed to make their way into production queue.
                    if (this.Queue == ConstMsg.ComplianceEaseAutoSubmitQueue && creds.Username == ConstStage.ComplianceEasePTMTestLogin)
                    {
                        // Send to test queue.
                        Tools.LogWarning($"[{this.LogId}] Received message meant for test queue. Will send to correct queue. Message slid=[{message.Subject1 }], fileVersion=[{message.Subject2}]");
                        DBMessageQueue testQ = new DBMessageQueue(ConstMsg.ComplianceEaseAutoSubmitTestQueue);
                        testQ.Send(message.Subject1, ObsoleteSerializationHelper.JsonSerialize(creds), message.Subject2);

                        // Drop message from this queue.
                        success = true;
                        continue;
                    }

                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    Tools.LogInfo($"[{this.LogId}]  Running {message.Subject1 } for fileVersion {message.Subject2}");
                    sw.Start();
                    ComplianceEaseExporter.Export(new Guid(message.Subject1), creds.Username, creds.Password, E_TransmittalDataComplianceAuditType.PreClose, out serviceFault, true, Convert.ToInt32(message.Subject2));
                    sw.Stop();
                    Tools.LogInfo($"[{this.LogId}] Processed slid {message.Subject1} in {sw.ElapsedMilliseconds}ms");
                    success = true;
                }
                catch (DBMessageQueueException)
                {
                    return;
                }
                catch (System.Net.WebException we)
                {
                    // Log error.
                    string slid = message?.Subject1 ?? "n/a";
                    Tools.LogError($"[{this.LogId}] WEB EXCEPTION. Loan id=[{slid}]", we);

                    continue;   // NOTE: Message added back to queue in finally statement.
                }
                catch (CBaseException e)
                {
                    string slid = "n/a";
                    if (message != null)
                    {
                        slid = message.Subject1;
                    }
                    Tools.LogError($"[{this.LogId}] ERROR with loan {slid}", e);
                    throw;
                }
                catch (Exception e)
                {
                    if (message != null)
                    {
                        Tools.LogError($"[{this.LogId}] ERROR slid: {message.Subject1}", e);
                    }
                    throw;
                }
                finally
                {
                    if ((message != null) && !success && (creds != null))
                    {
                        creds.AttemptCount++;

                        //// Add it back to the queue for retry.
                        if (creds.AttemptCount < 5)
                        {
                            mQ.Send(message.Subject1, ObsoleteSerializationHelper.JsonSerialize(creds), message.Subject2);
                        }
                    }
                }
            }
        }
    }
}
