﻿namespace LendersOffice.ObjLib.Conversions.ComplianceEase
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml;
    using DataAccess;
    using global::ComplianceEase;
    using System;
    using LendersOffice.Common;

    /// <summary>
    /// This is a plain data object for serializing/deserializing xml to and from the database.
    /// </summary>
    [DataContract]
    public class ComplianceEaseExportConfiguration
    {
        [Serializable]
        public class KVP<T1, T2>
        {
            public T1 Key { get; set; }
            public T2 Value { get; set; }
        }

        [DataMember(Name = "ExcludedStatuses")]
        IEnumerable<E_sStatusT> _excludedStatuses;

        //ILookups can't be serialized or deserialized :(
        [DataMember(Name = "LenderLicenses")]
        IEnumerable<KVP<string, string>> _licensesRaw;

        [DataMember(Name = "LendingPolicies")]
        IEnumerable<string> _policies;

        [DataMember(Name = "DIDMCAExemptions")]
        IEnumerable<string> _exemptions;

        [DataMember(Name = "BeginRunningAuditsAt")]
        E_sStatusT _beginRunningAuditsStatus;

        [DataMember(Name = "HUDApproval")]
        E_LenderDetailsLenderHudApprovalStatusType _approval;

        // 800 Fees
        [DataMember(Name = "sLOrigFPaidTo")]
        public E_RespaFeePaidToType sLOrigFPaidTo;
        [DataMember(Name = "sApprFPaidTo")]
        public E_RespaFeePaidToType sApprFPaidTo;
        [DataMember(Name = "sCrFPaidTo")]
        public E_RespaFeePaidToType sCrFPaidTo;
        [DataMember(Name = "sInspectFPaidTo")]
        public E_RespaFeePaidToType sInspectFPaidTo;
        [DataMember(Name = "sMBrokFPaidTo")]
        public E_RespaFeePaidToType sMBrokFPaidTo;
        [DataMember(Name = "sProcFPaidTo")]
        public E_RespaFeePaidToType sProcFPaidTo;
        [DataMember(Name = "sUwFPaidTo")]
        public E_RespaFeePaidToType sUwFPaidTo;
        [DataMember(Name = "sWireFPaidTo")]
        public E_RespaFeePaidToType sWireFPaidTo;
        [DataMember(Name = "sTxServFPaidTo")]
        public E_RespaFeePaidToType sTxServFPaidTo;
        [DataMember(Name = "sFloodCertificationFPaidTo")]
        public E_RespaFeePaidToType sFloodCertificationFPaidTo;
        [DataMember(Name = "sLDiscntPaidTo")]
        public E_RespaFeePaidToType sLDiscntPaidTo;
        [DataMember(Name = "sGfeCreditLenderPaidItemFPaidTo")]
        public E_RespaFeePaidToType sGfeCreditLenderPaidItemFPaidTo;
        [DataMember(Name = "sGfeLenderCreditFPaidTo")]
        public E_RespaFeePaidToType sGfeLenderCreditFPaidTo;
        [DataMember(Name = "sGfeDiscountPointFPaidTo")]
        public E_RespaFeePaidToType sGfeDiscountPointFPaidTo;
        [DataMember(Name = "s800RestPaidTo")]
        public E_RespaFeePaidToType s800RestPaidTo;

        // 900 Fees
        [DataMember(Name = "sIPiaPaidTo")]
        public E_RespaFeePaidToType sIPiaPaidTo;
        [DataMember(Name = "sHazInsPiaPaidTo")]
        public E_RespaFeePaidToType sHazInsPiaPaidTo;
        [DataMember(Name = "sMipPiaPaidTo")]
        public E_RespaFeePaidToType sMipPiaPaidTo;
        [DataMember(Name = "s904PiaPaidTo")]
        public E_RespaFeePaidToType s900RestPaidTo;

        // 1000 Fees
        [DataMember(Name = "sHazInsRsrvPaidTo")]
        public E_RespaFeePaidToType sHazInsRsrvPaidTo;
        [DataMember(Name = "sMInsRsrvPaidTo")]
        public E_RespaFeePaidToType sMInsRsrvPaidTo;
        [DataMember(Name = "sRealETxRsrvPaidTo")]
        public E_RespaFeePaidToType sRealETxRsrvPaidTo;
        [DataMember(Name = "sSchoolTxRsrvPaidTo")]
        public E_RespaFeePaidToType sSchoolTxRsrvPaidTo;
        [DataMember(Name = "sFloodInsRsrvPaidTo")]
        public E_RespaFeePaidToType sFloodInsRsrvPaidTo;
        [DataMember(Name = "s1000RestPaidTo")]
        public E_RespaFeePaidToType s1000RestPaidTo;

        // 1100 Fees
        [DataMember(Name = "sEscrowFPaidTo")]
        public E_RespaFeePaidToType sEscrowFPaidTo;
        [DataMember(Name = "sDocPrepFPaidTo")]
        public E_RespaFeePaidToType sDocPrepFPaidTo;
        [DataMember(Name = "sOwnerTitleInsPaidTo")]
        public E_RespaFeePaidToType sOwnerTitleInsPaidTo;
        [DataMember(Name = "sTitleInsFPaidTo")]
        public E_RespaFeePaidToType sTitleInsFPaidTo;
        [DataMember(Name = "sNotaryFPaidTo")]
        public E_RespaFeePaidToType sNotaryFPaidTo;
        [DataMember(Name = "sAttorneyFPaidTo")]
        public E_RespaFeePaidToType sAttorneyFPaidTo;
        [DataMember(Name = "s1100RestPaidTo")]
        public E_RespaFeePaidToType s1100RestPaidTo;

        // 1200 Fees
        [DataMember(Name = "sCountyRtcPaidTo")]
        public E_RespaFeePaidToType sCountyRtcPaidTo;
        [DataMember(Name = "sStateRtcPaidTo")]
        public E_RespaFeePaidToType sStateRtcPaidTo;
        [DataMember(Name = "sRecFPaidTo")]
        public E_RespaFeePaidToType sRecFPaidTo;
        [DataMember(Name = "s1200RestPaidTo")]
        public E_RespaFeePaidToType s1200RestPaidTo;

        // 1300 Fees
        [DataMember(Name = "sPestInspectFPaidTo")]
        public E_RespaFeePaidToType sPestInspectFPaidTo;
        [DataMember(Name = "s1300RestPaidTo")]
        public E_RespaFeePaidToType s1300RestPaidTo;

        /// <summary>
        /// Statuses that should not trigger compliance analysis.
        /// </summary>
        public IEnumerable<E_sStatusT> ExcludedStatuses { get { return _excludedStatuses ?? Enumerable.Empty<E_sStatusT>(); } }

        public ILookup<string, string> LenderLicensesByState { get { return _licensesRaw.ToLookup(a => a.Key, a => a.Value); } }

        public IEnumerable<string> LenderLicenses { get { return _licensesRaw.Select((a) => a.Value); } }

        public IEnumerable<string> LendingPolicies { get { return _policies; } }

        /// <summary>
        /// List of state abbreviations for which this lender has DIDMCAExemptions.
        /// </summary>
        public IEnumerable<string> DIDMCAExemptions { get { return _exemptions; } }

        public E_sStatusT BeginRunningAuditsAt { get { return _beginRunningAuditsStatus; } }

        public E_LenderDetailsLenderHudApprovalStatusType HUDApproval { get { return _approval; } }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext context) //set default values not yet in xml on the db
        {
            _beginRunningAuditsStatus = E_sStatusT.Loan_Processing;
        }

        public static ComplianceEaseExportConfiguration Parse(string xml)
        {
            if (string.IsNullOrEmpty(xml.TrimWhitespaceAndBOM()))
            {
                return new ComplianceEaseExportConfiguration();
            }
            ComplianceEaseExportConfiguration ret;
            var deserializer = new DataContractSerializer(typeof(ComplianceEaseExportConfiguration));
            using (StringReader sr = new StringReader(xml))
            {
                using (XmlReader xr = XmlReader.Create(sr))
                {
                    try
                    {
                        ret = (ComplianceEaseExportConfiguration)deserializer.ReadObject(xr);
                    }
                    catch (InvalidCastException)
                    {
                        ret = new ComplianceEaseExportConfiguration();
                    }
                    catch (SerializationException)
                    {
                        ret = new ComplianceEaseExportConfiguration();
                    }
                }
            }

            return ret;
        }

        public string Serialize()
        {
            string xml;
            var serializer = new DataContractSerializer(typeof(ComplianceEaseExportConfiguration));
            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter xw = XmlWriter.Create(sw))
                {
                    serializer.WriteObject(xw, this);
                    xw.Flush();
                    xml = sw.ToString();
                }
            }

            return xml;
        }

        /// <summary>
        /// Returns the license string for a given state, by two letter state abbreviation.
        /// </summary>
        /// <param name="state">The two letter state abbreviation; case is ignored</param>
        /// <returns>The license string this lender has set up; in the case of multiple for a given state, the first will be returned.</returns>
        public string GetLicenseForState(string state)
        {
            // If we get no state, return nothing.
            if (string.IsNullOrEmpty(state.TrimWhitespaceAndBOM()))
            {
                return string.Empty;
            }

            // Otherwise do an argument check.
            if (state.Length != 2)
            {
                throw new ArgumentException("State must be a two letter state abbreviation!", "state");
            }

            // If there's no configuration at all, we want to say not configured
            if (!_licensesRaw.Any())
            {
                return "Not Configured";
            }

            var ret = (from a in _licensesRaw
                       where a.Key.Equals(state, StringComparison.InvariantCultureIgnoreCase)
                       select a.Value).FirstOrDefault();

            // If there is some configuration, but not for this state, we want to return a blank string
            if (ret == default(string))
            {
                ret = "";
            }

            return ret;
        }

        public ComplianceEaseExportConfiguration(
            IEnumerable<E_sStatusT> excludedStatuses,
            ILookup<string, string> licenses,
            IEnumerable<string> policies,
            IEnumerable<string> exemptions,
            E_LenderDetailsLenderHudApprovalStatusType approval,
            E_sStatusT beginRunningAuditsStatus)
        {
            var temp = new List<KVP<string, string>>(licenses.Count);
            foreach (var licensesPerState in licenses)
            {
                foreach (var license in licensesPerState)
                {
                    temp.Add(new KVP<string, string>() { Key = licensesPerState.Key, Value = license });
                }
            }

            _excludedStatuses = excludedStatuses;
            _licensesRaw = temp;
            _policies = policies;
            _exemptions = exemptions;
            _approval = approval;
            _beginRunningAuditsStatus = beginRunningAuditsStatus;
        }

        private ComplianceEaseExportConfiguration()
        {
            _excludedStatuses = new List<E_sStatusT>();
            _licensesRaw = new List<KVP<string, string>>();
            _policies = new List<string>();
            _exemptions = new List<string>();
            _approval = E_LenderDetailsLenderHudApprovalStatusType.NotConfigured;
            _beginRunningAuditsStatus = E_sStatusT.Loan_Processing;
        }

        public static readonly string[] Policies = new[]
        {
            "Broker Compensation (Using Paid By)",
            "Broker Compensation (Prohibit Simultaneous Direct and Indirect Broker Fees)",
            "Broker Compensation (Prohibit Credit Insufficient to Pay All Broker Charges)",
            "Broker Compensation (Borrower-Paid Broker Compensation)",
            "CA Higher-Priced Mortgage Loan",
            "CT Nonprime Home Loan",
            "FHA",
            "GSE (Fannie Mae public guidelines)",
            "GSE (Freddie Mac public guidelines)",
            "HOEPA Higher-Priced Mortgage Loan",
            "MA Higher-Priced Mortgage Loan",
            "MD COMAR Higher Priced Mortgage Loan",
            "ME 2011 Higher-Priced Mortgage Loan",
            "ME Rate Spread Home Loan",
            "NC Rate Spread Home Loan",
            "NY Subprime Home Loan",
            "OK HOEPA Higher-Priced Mortgage Loan",
            "Qualified Mortgage",
            "Qualified Mortgage Safe Harbor",
            "Standard & Poor's",
            "VA SB 797 High Risk Mortgage Loan",
            "Veterans Affairs",
            "VT High Rate Loans",
            "Higher-Priced Mortgage Loan (Amended)",
            "Qualified Mortgage DTI",
            "Default",
            "FHA QM Safe Harbor"
        };

        public static readonly string[] Licenses = new[]
        {
            "Alaska Small Loans Act",
            "Alaska Unlicensed",
            "Alaska Mortgage Lending Regulation License",
            "Alaska Exemption Letter",
            "Alabama Consumer Credit License",
            "Alabama Exemption Letter",
            "Arkansas Mortgage Loan Company",
            "Arkansas Exemption Letter",
            "Arkansas Mortgage Banker, Broker, or Servicer License",
            "Arizona Mortgage Banker and Broker License",
            "Arizona Exemption Letter",
            "California Finance Lender License",
            "California Real Estate Corporation License",
            "California Real Estate Broker License",
            "California Residential Mortgage Lender License",
            "California Exemption Letter",
            "Colorado Mortgage Company Registration",
            "Colorado Supervised Lender License",
            "Colorado Unlicensed",
            "Connecticut First Mortgage Lender License (1st Lien)",
            "Connecticut Second Mortgage Lender License (Subordinate Lien)",
            "Connecticut Exemption Letter",
            "Connecticut Mortgage Lender License",
            "District of Columbia Mortgage Lender License",
            "District of Columbia Exemption Letter",
            "Delaware Licensed Lender License",
            "Delaware Exemption Letter",
            "Florida Correspondent Mortgage Lender License",
            "Florida Mortgage Lender License",
            "Florida Exemption Letter",
            "Georgia Mortgage Lender License",
            "Georgia Exemption Letter",
            "Hawaii Financial Services Loan Company License",
            "Hawaii Mortgage Brokers and Solicitors License",
            "Hawaii Mortgage Loan Originator Company License",
            "Hawaii Foreign Lender",
            "Hawaii Exemption Letter",
            "Iowa Mortgage Banker License",
            "Iowa Regulated Loan License",
            "Iowa Industrial Loan License",
            "Iowa Exemption Letter",
            "Idaho Mortgage Banker License (1st Lien)",
            "Idaho Regulated Lender License (Subordinate Lien)",
            "Idaho Exemption Letter",
            "Illinois Residential Mortgage License",
            "Illinois Exemption Company Registration License",
            "Illinois Exemption Letter",
            "Indiana First Lien Mortgage Lender License",
            "Indiana Subordinate Lien Mortgage Lender License",
            "Indiana Unregulated (1st Lien)",
            "Indiana Consumer Loan License (Subordinate Lien)",
            "Indiana Unregulated UCCC (First Lien)",
            "Indiana Consumer Loan License UCCC (Subordinate Lien)",
            "Indiana Exemption Letter",
            "Indiana Exemption Letter UCCC",
            "Kansas Supervised Lender License (using UCCC)",
            "Kansas Mortgage Company License (using Interest section 16-207)",
            "Kansas Exemption Letter",
            "Kansas Supervised Lender License (using Interest section 16-207)",
            "Kansas Mortgage Company License (using UCCC)",
            "Kansas Exemption Letter (using Interest section 16-207)",
            "Kentucky Mortgage Loan Company License",
            "Kentucky Exemption Letter",
            "Louisiana Residential Mortgage Lending License",
            "Louisiana Consumer Loan License (Subordinate Lien)",
            "Louisiana Exemption Letter",
            "Massachusetts Mortgage Lender License",
            "Massachusetts Exemption Letter",
            "Maryland Exemption Letter",
            "Maryland Mortgage Lender License (using 12-100 Interest and Usury)",
            "Maryland Mortgage Lender License (using 12-400 Secondary Mortgage Loans)",
            "Maryland Mortgage Lender License (using 12-900 and 12-1000 Credit Grantor)",
            "Maine Supervised Lender License",
            "Michigan First Mortgage Lender License/Registration",
            "Michigan Secondary Mortgage Lender License/Registration",
            "Michigan Exemption Letter",
            "Minnesota Residential Mortgage Originator or Servicer License",
            "Minnesota Regulated Loan License",
            "Minnesota Industrial Loan Company",
            "Minnesota Exemption Letter",
            "Mississippi Residential Mortgage Company License",
            "Mississippi Exemption Letter",
            "Missouri Residential Mortgage Broker License",
            "Missouri Exemption Letter",
            "Montana Residential Mortgage Lender License",
            "Montana Consumer Loan License",
            "Montana Unlicensed (Subordinate Lien)",
            "Montana Exemption Letter (Residential Mortgage Lender Act)",
            "Montana Exemption Letter",
            "North Carolina Mortgage Lender License",
            "North Carolina Exemption Letter",
            "North Dakota Money Broker License",
            "North Dakota Consumer Finance License",
            "North Dakota Exemption Letter",
            "Nebraska Mortgage Banker License",
            "Nebraska Exemption Letter",
            "New Hampshire First Mortgage Banker License",
            "New Hampshire Second Mortgage Home Loan License",
            "New Hampshire Exemption Letter",
            "New Jersey Mortgage Banker or Correspondent Mortgage Banker License (1st Lien)",
            "New Jersey Secondary Mortgage Lender License",
            "New Jersey Exemption Letter",
            "New Mexico Mortgage Loan Company Registration License",
            "New Mexico Exemption Letter",
            "Nevada Mortgage Banker License",
            "Nevada Exemption Letter",
            "Nevada Mortgage Broker License",
            "New York Mortgage Banker License",
            "New York Exemption Letter",
            "Ohio Exemption Letter",
            "Ohio Mortgage Broker Act Registration Certificate (1st Lien)",
            "Ohio Second Mortgage Act Registration Certificate",
            "Oklahoma Mortgage Broker License",
            "Oklahoma Supervised Lender License",
            "Oklahoma Exemption Letter",
            "Oregon Consumer Finance License",
            "Oregon Mortgage Lender License",
            "Oregon Exemption Letter",
            "Pennsylvania Mortgage Lender License",
            "Pennsylvania Exemption Letter (Mortgage Act)",
            "Pennsylvania First Mortgage Banker License",
            "Pennsylvania Secondary Mortgage Loan License",
            "Pennsylvania Consumer Discount Company License",
            "Pennsylvania Unlicensed (Subordinate Lien)",
            "Pennsylvania Exemption Letter",
            "Rhode Island Lender License",
            "Rhode Island Exemption Letter",
            "South Carolina Mortgage Lender / Servicer License",
            "South Carolina Exemption Letter",
            "South Carolina Unregulated (1st Lien)",
            "South Carolina Supervised Lender License (Subordinate Lien)",
            "South Dakota Mortgage Lender License",
            "South Dakota Exemption Letter",
            "Tennessee Mortgage Lender License",
            "Tennessee Mortgage Lender Registration (Exemption Letter)",
            "Tennessee Industrial Loan and Thrift Company Registration",
            "Texas Regulated Loan License (Subordinate Lien)",
            "Texas Mortgage Broker License (1st Lien)",
            "Texas Exemption Letter",
            "Texas Mortgage Banker Registration (1st Lien)",
            "Utah Mortgage Entity (1st Lien)",
            "Utah Consumer Credit Code Notification (Subordinate Lien)",
            "Utah Exemption Letter",
            "Virginia Mortgage Lender License",
            "Virginia Exemption Letter",
            "Vermont Mortgage Lender License",
            "Vermont Exemption Letter",
            "Washington Consumer Loan Company License",
            "Washington Mortgage Broker License",
            "Washington Exemption Letter",
            "Wisconsin Loan Company License (Subordinate Lien)",
            "Wisconsin Mortgage Banker License",
            "Wisconsin Exemption Letter",
            "Wisconsin Consumer Act Registration (Subordinate Lien)",
            "West Virginia Regulated Consumer Lending License",
            "West Virginia Mortgage Lender License",
            "West Virginia Exemption Letter",
            "Wyoming Supervised Lender License",
            "Wyoming Unlicensed Lender (Expired License)",
            "Wyoming Registered Consumer Loan Lender",
            "Wyoming Mortgage Lender Broker License"
        };

        /// <summary>
        /// Map of state abbreviation => state name
        /// </summary>
        public static readonly Dictionary<string, string> States = new Dictionary<string, string>()
        {
            {"AL", "Alabama"},
            {"AK", "Alaska"},
            {"AZ", "Arizona"},
            {"AR", "Arkansas"},
            {"CA", "California"},
            {"CO", "Colorado"},
            {"CT", "Connecticut"},
            {"DC", "District of Columbia"},
            {"DE", "Delaware"},
            {"FL", "Florida"},
            {"GA", "Georgia"},
            {"HI", "Hawaii"},
            {"ID", "Idaho"},
            {"IL", "Illinois"},
            {"IN", "Indiana"},
            {"IA", "Iowa"},
            {"KS", "Kansas"},
            {"KY", "Kentucky"},
            {"LA", "Louisiana"},
            {"ME", "Maine"},
            {"MD", "Maryland"},
            {"MA", "Massachusetts"},
            {"MI", "Michigan"},
            {"MN", "Minnesota"},
            {"MS", "Mississippi"},
            {"MO", "Missouri"},
            {"MT", "Montana"},
            {"NE", "Nebraska"},
            {"NV", "Nevada"},
            {"NH", "New Hampshire"},
            {"NJ", "New Jersey"},
            {"NM", "New Mexico"},
            {"NY", "New York"},
            {"NC", "North Carolina"},
            {"ND", "North Dakota"},
            {"OH", "Ohio"},
            {"OK", "Oklahoma"},
            {"OR", "Oregon"},
            {"PA", "Pennsylvania"},
            {"RI", "Rhode Island"},
            {"SC", "South Carolina"},
            {"SD", "South Dakota"},
            {"TN", "Tennessee"},
            {"TX", "Texas"},
            {"UT", "Utah"},
            {"VT", "Vermont"},
            {"VA", "Virginia"},
            {"WA", "Washington"},
            {"WV", "West Virginia"},
            {"WI", "Wisconsin"},
            {"WY", "Wyoming"}
        };

        /// <summary>
        /// Returns the list of loan statuses that should be ignored. This list is comprised
        /// of statuses that should always be ignored, any statuses that have been excluded
        /// by the lender, and any statuses occurring chronologically before the first status
        /// that should trigger audits.
        /// </summary>
        /// <returns>A set of statuses that should not trigger ComplianceEase.</returns>
        public IEnumerable<E_sStatusT> IgnoredLoanStatuses
        {
            get
            {
                return ComplianceExportStatusConfig.GetIgnoredLoanStatuses(
                    this.BeginRunningAuditsAt,
                    this.ExcludedStatuses);
            }
        }
    }
}
