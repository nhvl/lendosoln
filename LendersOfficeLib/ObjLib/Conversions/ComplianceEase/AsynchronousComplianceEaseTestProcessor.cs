﻿/// <copyright file="ContinuousServiceApplicationInitializer.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
///     Author: Jhairo Erazo
///     Date: 12/13/2016
/// </summary>
namespace LendersOffice.Conversions.ComplianceEase
{
    using System.Threading;

    using DataAccess;
    using LendersOffice.ObjLib.DatabaseMessageQueue;

    /// <summary>
    /// Compliance Ease test queue.
    /// </summary>
    public class AsynchronousComplianceEaseTestProcessor : AsynchronousComplianceEaseProcessor
    {
        /// <summary>
        /// Gets the queue to use.
        /// </summary>
        /// <value>The queue to use.</value>
        protected override DBQueue Queue
        {
            get { return ConstMsg.ComplianceEaseAutoSubmitTestQueue; }
        }

        /// <summary>
        /// Gets string ID used in logs.
        /// </summary>
        /// <value>String ID used in logs.</value>
        protected override string LogId
        {
            get { return "CEQUEUE TEST"; }
        }
    }
}
