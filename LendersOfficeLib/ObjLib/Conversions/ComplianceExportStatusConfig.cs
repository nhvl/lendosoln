﻿namespace LendersOffice.ObjLib.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Utility methods for status handling in compliance exports.
    /// </summary>
    public static class ComplianceExportStatusConfig
    {
        /// <summary>
        /// Gets statuses that should always be ignored by automated compliance exports
        /// regardless of user settings. This includes lead statuses and any statuses
        /// we no longer use.
        /// </summary>
        /// <value>The statuses that are always ignored regardless of user settings.</value>
        internal static IEnumerable<E_sStatusT> AlwaysIgnoredStatuses
        {
            get
            {
                return CPageBase.s_sStatusT_Order
                    .Where(s => Tools.IsStatusLead(s) || Tools.IsDefunctStatus(s));
            }
        }

        /// <summary>
        /// Retrieves a set of loan statuses that could trigger a compliance audit.
        /// The user can exclude certain items from this list.
        /// </summary>
        /// <param name="enabledForBroker">
        /// A method that determines if a status is enabled for a broker.
        /// </param>
        /// <returns>A list of possible starting point statuses.</returns>
        public static IEnumerable<E_sStatusT> StatusesAvailableForAudit(Func<E_sStatusT, bool> enabledForBroker)
        {
            return CPageBase.s_sStatusT_Order
                .Where(s => enabledForBroker(s)
                    && !Tools.IsStatusLead(s)
                    && !Tools.IsDefunctStatus(s));
        }

        /// <summary>
        /// Gets the statuses that occur prior to the given status.
        /// </summary>
        /// <param name="status">The base status from which all prior statuses will be calculated.</param>
        /// <returns>The statuses that occur prior to the given status.</returns>
        public static IEnumerable<E_sStatusT> PriorLoanStatuses(E_sStatusT status)
        {
            return CPageBase.s_sStatusT_Order
                .TakeWhile(s => s != status)
                .Except(AlwaysIgnoredStatuses);
        }

        /// <summary>
        /// Returns the list of loan statuses that should be ignored. This list is comprised
        /// of statuses that should always be ignored, any statuses that have been excluded
        /// by the lender, and any statuses occurring chronologically before the first status
        /// that should trigger audits.
        /// </summary>
        /// <param name="beginRunningAuditsAt">The status at which compliance audits begin to run.</param>
        /// <param name="excludedStatuses">The statuses the lender has chosen to exclude.</param>
        /// <returns>A set of statuses that should not trigger compliance exports.</returns>
        internal static IEnumerable<E_sStatusT> GetIgnoredLoanStatuses(E_sStatusT beginRunningAuditsAt, IEnumerable<E_sStatusT> excludedStatuses)
        {
            return AlwaysIgnoredStatuses
                .Union(PriorLoanStatuses(beginRunningAuditsAt))
                .Union(excludedStatuses);
        }
    }
}
