﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class GDMSMessageBehavior : IEndpointBehavior
    {
        protected GDMSClient Client;
        public GDMSMessageBehavior(GDMSClient client)
            : base()
        {
            this.Client = client;
        }

        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new GDMSMessageInspector(this.Client));
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new GDMSMessageInspector(this.Client));
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion
    }
}
