﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.GDMS.LookupMethods;
using System.Runtime.Serialization;
using System.IO;
using DataAccess;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    /// <summary>
    /// Facade to simplify calls to Global DMS. Mainly deserializes for us.
    /// </summary>
    public class LookupMethodsClient : GDMSClient, IDisposable
    {
        private GDMS.LookupMethods.LookupMethodsClient m_client;
        private GDMS.LookupMethods.CredentialsObject m_credentials;

        public override System.ServiceModel.Description.ServiceEndpoint Endpoint
        {
            get { return m_client.Endpoint; }
        }

        public LookupMethodsClient(GDMSCredentials credentials)
            : base()
        {
            string clientEndpoint = "BasicHttpBinding_ILookupMethodsProduction";

            if (credentials.ExportToStage || credentials.UserName == "LQBtestuser" || credentials.UserName == "LQBUser")
            {
                clientEndpoint = "BasicHttpBinding_ILookupMethods";
            }

            m_client = new GDMS.LookupMethods.LookupMethodsClient(clientEndpoint);
            m_client.Endpoint.Behaviors.Add(new GDMSMessageBehavior(this));

            m_credentials = new GDMS.LookupMethods.CredentialsObject();
            m_credentials.CompanyId = credentials.CompanyId;
            m_credentials.UserName = credentials.UserName;
            m_credentials.Password = credentials.Password;
            m_credentials.UserType = credentials.UserType;
        }

        public GDMS.LookupMethods.Product[] GetProducts()
        {
            return LookupMethodsClient.GetProducts(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.Product[] GetProducts(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetProducts(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.ProductListResponse), "ProductListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.ProductListResponse;

            var items = responseObj.items;
            return items;
        }

        public GDMS.LookupMethods.PropertyType[] GetPropertyTypes()
        {
            return GetPropertyTypes(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.PropertyType[] GetPropertyTypes(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetPropertyTypes(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.PropertyTypeListResponse), "PropertyTypeListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.PropertyTypeListResponse;

            var items = responseObj.items as GDMS.LookupMethods.PropertyType[];
            return items;
        }

        public GDMS.LookupMethods.LoanType[] GetIntendedUses()
        {
            return GetIntendedUses(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.LoanType[] GetIntendedUses(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetIntendedUses(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.LoanTypeListResponse), "LoanTypeListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.LoanTypeListResponse;

            var items = responseObj.items as GDMS.LookupMethods.LoanType[];
            return items;
        }

        public GDMS.LookupMethods.BillingMethod[] GetBillingMethods()
        {
            return GetBillingMethods(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.BillingMethod[] GetBillingMethods(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetBillingMethods(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.BillingMethodListResponse), "BillingMethodListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.BillingMethodListResponse;

            var items = responseObj.items as GDMS.LookupMethods.BillingMethod[];
            return items;
        }

        /// <summary>
        /// The client ID should not matter since we're only sending client-type credentials
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public GDMS.LookupMethods.clsClient GetClientInfo(int clientId)
        {
            return GetClientInfo(m_client, m_credentials, clientId);
        }

        public static GDMS.LookupMethods.clsClient GetClientInfo(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials, int clientId)
        {
            var response = client.GetClientInfo(credentials, clientId, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleDataObject), "SingleDataObject", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleDataObject;

            var item = responseObj.data as GDMS.LookupMethods.clsClient;
            return item;
        }

        /// <summary>
        /// The client ID should not matter since we're only sending client-type credentials
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public GDMS.LookupMethods.clsClientUser[] GetClientUsers(int clientId)
        {
            return GetClientUsers(m_client, m_credentials, clientId);
        }

        public static GDMS.LookupMethods.clsClientUser[] GetClientUsers(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials, int clientId)
        {
            var response = client.GetClientUsers(credentials, clientId, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.ClientUserListResponse), "ClientUserListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.ClientUserListResponse;

            if (responseObj == null)
            {
                return new GDMS.LookupMethods.clsClientUser[0];
            }

            var items = responseObj.items as GDMS.LookupMethods.clsClientUser[];
            return items;
        }

        public GDMS.LookupMethods.clsClientForDropDown[] GetClient2Users()
        {
            return GetClient2Users(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.clsClientForDropDown[] GetClient2Users(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetClient2Users(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.ClientForDropDownListResponse), "ClientForDropDownListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.ClientForDropDownListResponse;

            if (responseObj == null)
            {
                return new GDMS.LookupMethods.clsClientForDropDown[0];
            }

            var items = responseObj.items as GDMS.LookupMethods.clsClientForDropDown[];
            return items;
        }

        public GDMS.LookupMethods.LoanType[] GetLoanTypes()
        {
            return GetLoanTypes(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.LoanType[] GetLoanTypes(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetLoanTypes(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.LoanTypeListResponse), "LoanTypeListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.LoanTypeListResponse;

            var items = responseObj.items as GDMS.LookupMethods.LoanType[];
            return items;
        }

        public GDMS.LookupMethods.OccupancyType[] GetOccupancyTypes()
        {
            return GetOccupancyTypes(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.OccupancyType[] GetOccupancyTypes(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetOccupancyTypes(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.OccupancyTypeListResponse), "OccupancyTypeListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.OccupancyTypeListResponse;

            var items = responseObj.items as GDMS.LookupMethods.OccupancyType[];
            return items;
        }

        public GDMS.LookupMethods.OrderFileUploadFileType[] GetOrderFileUploadFileTypes()
        {
            return GetOrderFileUploadFileTypes(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.OrderFileUploadFileType[] GetOrderFileUploadFileTypes(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials)
        {
            var response = client.GetOrderFileUploadFileTypes(credentials, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.OrderFileUploadFileTypesListResponse), "OrderFileUploadFileTypesListResponse", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.OrderFileUploadFileTypesListResponse;

            var items = responseObj.items as GDMS.LookupMethods.OrderFileUploadFileType[];
            return items;
        }

        public GDMS.LookupMethods.clsAppraiserForListing GetAppraiserById(int appraiserId)
        {
            return GetAppraiserById(m_client, m_credentials, appraiserId);
        }
        public static GDMS.LookupMethods.clsAppraiserForListing GetAppraiserById(GDMS.LookupMethods.LookupMethodsClient client, GDMS.LookupMethods.CredentialsObject credentials, int appraiserId)
        {
            var response = client.GetAppraiserById(credentials, appraiserId, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleDataObject), "SingleDataObject", LookupMethodsClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleDataObject;

            var item = responseObj.data as GDMS.LookupMethods.clsAppraiserForListing;
            return item;
        }


        public override void Dispose()
        {
            m_client.Close();
        }
    }
}
