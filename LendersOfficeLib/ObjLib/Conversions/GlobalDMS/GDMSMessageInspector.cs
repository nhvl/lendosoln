﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using DataAccess;
using System.ServiceModel;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class GDMSMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        protected GDMSClient Client;

        public GDMSMessageInspector(GDMSClient client)
            : base()
        {
            this.Client = client;
        }

        

        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            this.Client.StoreMessage(ref reply, "GDMS: Received reply");
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            this.Client.StoreMessage(ref request, "GDMS: Sending request to " + this.Client.Endpoint.Address.Uri.ToString());
            return null;
        }

        #endregion

        #region IDispatchMessageInspector Members

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            this.Client.StoreMessage(ref request, "GDMS: Received request");
            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            this.Client.StoreMessage(ref reply, "GDMS: Sending reply");
        }

        #endregion
    }
}
