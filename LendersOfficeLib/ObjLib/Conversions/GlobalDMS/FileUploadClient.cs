﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.GDMS.FileUpload;
using System.Runtime.Serialization;
using System.IO;
using DataAccess;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    /// <summary>
    /// Facade to simplify calls to Global DMS. Mainly deserializes for us.
    /// </summary>
    public class FileUploadClient : GDMSClient, IDisposable
    {
        private GDMS.FileUpload.FileUploadClient m_client;
        private GDMS.FileUpload.FileUploadMessage m_fileUploadMessage;

        public override System.ServiceModel.Description.ServiceEndpoint Endpoint
        {
            get { return m_client.Endpoint; }
        }

        public FileUploadClient(GDMSCredentials credentials, string fileName, int recordKeyToAttachTo, int fileTypeId, System.IO.Stream fileData)
            : base()
        {
            string clientEndpoint = "BasicHttpBinding_IFileUploadProduction";

            if (credentials.ExportToStage || credentials.UserName == "LQBtestuser" || credentials.UserName == "LQBUser")
            {
                clientEndpoint = "BasicHttpBinding_IFileUpload";
            }

            m_client = new GDMS.FileUpload.FileUploadClient(clientEndpoint);
            m_client.Endpoint.Behaviors.Add(new GDMSMessageBehavior(this));

            m_fileUploadMessage = new FileUploadMessage();
            m_fileUploadMessage.FileInfo = new FileTransferInfo();
            m_fileUploadMessage.FileInfo.CompanyId = credentials.CompanyId;
            m_fileUploadMessage.FileInfo.UserName = credentials.UserName;
            m_fileUploadMessage.FileInfo.Password = credentials.Password;
            m_fileUploadMessage.FileInfo.UserType = credentials.UserType;

            m_fileUploadMessage.FileInfo.ViewableByClient = true;
            m_fileUploadMessage.FileInfo.ViewableByVendor = true;

            m_fileUploadMessage.FileInfo.FileName = fileName;
            m_fileUploadMessage.FileInfo.RecordKeyToAttachTo = recordKeyToAttachTo;
            m_fileUploadMessage.FileInfo.FileTypeId = fileTypeId;

            m_fileUploadMessage.FileData = fileData;
        }

        public GDMS.FileUpload.UploadResults SubmitReport()
        {
            return FileUploadClient.SubmitReport(m_client, m_fileUploadMessage);
        }

        public static GDMS.FileUpload.UploadResults SubmitReport(GDMS.FileUpload.FileUploadClient client, GDMS.FileUpload.FileUploadMessage fileUploadMessage)
        {
            var uploadResults = client.SubmitReport(fileUploadMessage.FileInfo, fileUploadMessage.FileData);
            return uploadResults;
        }

        public override void Dispose()
        {
            m_client.Close();
        }
    }
}
