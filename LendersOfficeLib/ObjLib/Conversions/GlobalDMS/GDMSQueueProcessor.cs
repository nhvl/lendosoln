﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CommonProjectLib.Logging;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class GDMSQueueProcessor: CommonProjectLib.Runnable.IRunnable
    {
        static GDMSQueueProcessor()
        {
            // This code requires the call to GDMS to go through a proxy. In order for it to succeed we need to whitelist
            // the URL cert since the proxy will return its own SSL cert instead of GDMS.
            Tools.SetupServicePointCallback();
            Tools.SetupServicePointManager();

            string msmqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msmqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msmqLogConnectStr, ConstStage.EnvironmentName));
            }

            string server = ConstStage.SmtpServer;
            int port = ConstStage.SmtpServerPortNumber;
            string notifyOnErrorEmail = ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"];
            string notifyFromErrorEmail = ConfigurationManager.AppSettings["NotifyFromOnError"];
            if (false == string.IsNullOrEmpty(server) && false == string.IsNullOrEmpty(notifyOnErrorEmail) && false == string.IsNullOrEmpty(notifyFromErrorEmail))
            {
                LogManager.Add(new EmailLogger(server, port, notifyOnErrorEmail, notifyFromErrorEmail));
            }
        }

        public string Description
        {
            get
            {
                return "Auto-save support for both pdf and xml files from appraisal framework. OPM 111224";
            }
        }

        private static string[] x_commaSplit = new string[] { "," };
        private static string[] x_equalSplit = new string[] { "=" };

        private Dictionary<string, string> ParseData(string data, string[] kvpSplit, string[] kvSplit)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            Dictionary<string, string> keysAndValues = new Dictionary<string, string>();
            string[] keyValuePairs = data.Split(kvpSplit, StringSplitOptions.RemoveEmptyEntries);

            foreach (string keyValuePair in keyValuePairs)
            {
                string[] keyAndValue = keyValuePair.Split(kvSplit, StringSplitOptions.RemoveEmptyEntries);
                if (keyAndValue.Length != 2 || String.IsNullOrEmpty(keyAndValue[0]) || String.IsNullOrEmpty(keyAndValue[1]))
                {
                    var msg = "Key-value pair of wrong format.";
                    throw new CBaseException(msg, msg);
                }

                keysAndValues.Add(keyAndValue[0], keyAndValue[1]);
            }

            return keysAndValues;
        }

        private Action expireCachedFiles = null;
        public void HandleMessage(DBMessage msg)
        {
            if (msg == null)
            {
                return;
            }
            var parsedUserData = ParseData(msg.Subject2, x_commaSplit, x_equalSplit);
            if (parsedUserData == null)
            {
                Tools.LogError("[GDMSQueueProcessor] Message enqueued without any data.");
                return;
            }

            var loanId = new Guid(msg.Subject1);
            var brokerId = new Guid(parsedUserData["BrokerId"]);
            var userId = new Guid(parsedUserData["UserId"]);
            var employeeId = new Guid(parsedUserData["EmployeeId"]);

            var uploadedFiles = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<AppraisalFileDocUploadInfo>>(msg.Data);

            var filesToProcess = uploadedFiles.ToList();
            foreach (var uploadedFile in uploadedFiles)
            {
                var cacheKey = $"{loanId.ToString("N")}_{uploadedFile.AppraisalFileNumber.ToString()}_{uploadedFile.FileId.ToString()}";
                if (AutoExpiredTextCache.GetFromCache(cacheKey) != null)
                {
                    filesToProcess.Remove(uploadedFile);
                }
                else
                {
                    AutoExpiredTextCache.AddToCache("Processing", TimeSpan.FromMinutes(10), cacheKey);
                }
            }

            this.expireCachedFiles = new Action(() =>
            {
                foreach (var processedFile in filesToProcess)
                {
                    var cacheKey = $"{loanId.ToString("N")}_{processedFile.AppraisalFileNumber.ToString()}_{processedFile.FileId.ToString()}";
                    AutoExpiredTextCache.RemoveFromCacheImmediately(cacheKey);
                }
            });

            AppraisalFileDocUploadInfo.UploadFiles(loanId, brokerId, filesToProcess);
        }

        #region IRunnable Members

        public void Run()
        {
            Tools.SetupServicePointManager();
            DBMessageQueue gdmsQueue = new DBMessageQueue(ConstMsg.GdmsAutoSaveDocsQueue);
            DBMessage msg;

            do
            {
                try
                {
                    msg = gdmsQueue.Receive();

                    if (msg == null) //means no msg.
                    {
                        return;
                    }

                    HandleMessage(msg);
                }
                catch (DBMessageQueueException dbe) when (dbe.MessageQueueErrorCode == DBMessageQueueErrorCode.IOTimeout || dbe.MessageQueueErrorCode == DBMessageQueueErrorCode.EmptyQueue)
                {
                    return; // Just means the queue is empty.
                }
                catch (Exception e)
                {
                    // Log error.
                    Tools.LogError("[GDMSQueueProcessor] Exception", e);

                    // Continue on web exception (not all messages hit the same URL).
                    if (e is System.Net.WebException)
                    {
                        continue;
                    }

                    throw;
                }
                finally
                {
                    if (this.expireCachedFiles != null)
                    {
                        this.expireCachedFiles();
                        this.expireCachedFiles = null;
                    }
                }
            } while (true);
        }

        #endregion
    }
}
