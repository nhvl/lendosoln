﻿namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Appraisal;
    using DataAccess;
    using DatabaseMessageQueue;
    using Edocs.AutoSaveDocType;
    using GDMS.LookupMethods;
    using Integration.Appraisals;
    using LendersOffice.Security;

    /// <summary>
    /// Loads GlobalDMS orders.
    /// </summary>
    public class GlobalDMSOrderLoader
    {
        /// <summary>
        /// The GlobalDMS orders.
        /// </summary>
        private Lazy<List<OrderContainer>> globalDMSOrders;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalDMSOrderLoader"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="loadDocAttachmentTypes">Whether the doc attachment types should be loaded. This is a separate call to GDMS.</param>
        /// <param name="shouldQueueNewFiles">Whether newly uploaded files when loading orders from GDMS should be queued for upload to EDocs.</param>
        public GlobalDMSOrderLoader(Guid loanId, AbstractUserPrincipal principal, bool loadDocAttachmentTypes, bool shouldQueueNewFiles)
        {
            this.LoanId = loanId;
            this.Principal = principal;
            this.LoadDocAttachmentTypes = loadDocAttachmentTypes;
            this.ShouldQueueNewFiles = shouldQueueNewFiles;

            this.globalDMSOrders = new Lazy<List<OrderContainer>>(this.LoadGlobalDMSOrders);
        }

        /// <summary>
        /// Gets the loan id used to load orders.
        /// </summary>
        public Guid LoanId
        {
            get;
        }

        /// <summary>
        /// Gets the principal used to load orders.
        /// </summary>
        public AbstractUserPrincipal Principal
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether the loader should also load doc attachment types.
        /// </summary>
        public bool LoadDocAttachmentTypes
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this loaded will queue up new files to upload to Edocs.
        /// </summary>
        public bool ShouldQueueNewFiles
        {
            get;
        }

        /// <summary>
        /// Gets the new files uploaded to Edocs as part of the load.
        /// </summary>
        public List<AppraisalFileDocUploadInfo> NewFilesToEdocs
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets any errors encountered during the load.
        /// </summary>
        public List<string> Errors
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the user has a valid GDMS login.
        /// </summary>
        public bool HasValidLogin
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a list of doc attachment types, if they were to be loaded.
        /// </summary>
        public IEnumerable<Tuple<int, string>> DocAttachmentTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Retrieves the orders.
        /// </summary>
        /// <returns>The list of orders.</returns>
        public List<OrderContainer> Retrieve()
        {
            return this.globalDMSOrders.Value;
        }

        /// <summary>
        /// Loads the GlobalDMS orders.
        /// </summary>
        /// <returns>The list of orders.</returns>
        private List<OrderContainer> LoadGlobalDMSOrders()
        {
            List<AppraisalFileDocUploadInfo> newFilesToUploadToEdocs = new List<AppraisalFileDocUploadInfo>();
            List<string> errorMessages = new List<string>();
            List<OrderContainer> orderContainers = new List<OrderContainer>();

            var ordersByVendorId = GDMSAppraisalOrderInfo.GetAllGdmsAppraisalOrdersByLoanId(this.LoanId, this.Principal.BrokerId).ToLookup(order => order.VendorId);
            foreach (AppraisalVendorConfig vendor in AppraisalVendorFactory.GetAvailableVendorsForBroker(this.Principal.BrokerId).Where(v => v.UsesGlobalDMS))
            {
                var orders = ordersByVendorId[vendor.VendorId];
                if (ordersByVendorId.Contains(Guid.Empty) && Regex.IsMatch(vendor.VendorName, "Global ?DMS", RegexOptions.IgnoreCase))
                {
                    orders = orders.Concat(ordersByVendorId[Guid.Empty]);
                }

                if (!orders.Any())
                {
                    continue; // No orders with this GlobalDMS vendor
                }

                string errorMessage;
                GDMSCredentials credentials = vendor.GetGDMSCredentials(this.Principal.BrokerId, this.Principal.EmployeeId, out errorMessage);
                if (credentials == null)
                {
                    this.HasValidLogin = false;
                    if (errorMessages != null && errorMessage == "Invalid Company ID.")
                    {
                        errorMessages.Add("Error loading credentials for " + vendor.VendorName + ". Company ID was invalid.");
                    }

                    foreach (var appraisalOrder in orders)
                    {
                        orderContainers.Add(this.GDMSErrorLoadingOrder(appraisalOrder, vendor.VendorName));
                    }

                    continue;
                }

                this.HasValidLogin = true;
                if (this.LoadDocAttachmentTypes)
                {
                    using (var lookupMethodsClient = new LookupMethodsClient(credentials))
                    {
                        try
                        {
                            var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                            this.DocAttachmentTypes = orderFileUploadFileTypes.Select(o => new Tuple<int, string>(o.FileTypesId, o.FileTypeDescription));
                        }
                        catch (GDMSErrorResponseException exc)
                        {
                            this.HasValidLogin = false;
                            Tools.LogError(exc);
                        }
                    }
                }

                orderContainers.AddRange(this.LoadOrdersClient(credentials, vendor, orders, newFilesToUploadToEdocs));
            }

            if (this.ShouldQueueNewFiles)
            {
                this.EnqueueNewlyUploadedFiles(newFilesToUploadToEdocs);
            }

            this.NewFilesToEdocs = newFilesToUploadToEdocs;
            this.Errors = errorMessages;

            return orderContainers;
        }

        /// <summary>
        /// Creates an order container for orders that errored while loading.
        /// </summary>
        /// <param name="appraisalOrder">The GDMS appraisal order.</param>
        /// <param name="vendorName">The vendor name.</param>
        /// <returns>The error order container.</returns>
        private OrderContainer GDMSErrorLoadingOrder(GDMSAppraisalOrderInfo appraisalOrder, string vendorName)
        {
            var orderContainer = new OrderContainer();
            orderContainer.GdmsAppraisalOrder = appraisalOrder;
            orderContainer.VendorID = appraisalOrder.VendorId;
            orderContainer.VendorName = vendorName;
            orderContainer.ClientOrder = new ClientOrder();
            orderContainer.ClientOrder.FileNumber = appraisalOrder.FileNumber;
            orderContainer.ClientOrder.CreationDate_String = string.Empty;
            orderContainer.ClientOrder.StatusName = "Error Accessing Order";
            orderContainer.ClientOrder.AppraiserLastModifiedDate_String = string.Empty;
            return orderContainer;
        }

        /// <summary>
        /// Loads client orders from GDMS.
        /// </summary>
        /// <param name="credentials">The credentials.</param>
        /// <param name="vendor">The vendor.</param>
        /// <param name="orders">The GDMS appraisal orders.</param>
        /// <param name="newFilesToUploadToEdocs">List to add new edocs to.</param>
        /// <returns>The client orders.</returns>
        private IEnumerable<OrderContainer> LoadOrdersClient(GDMSCredentials credentials, AppraisalVendorConfig vendor, IEnumerable<GDMSAppraisalOrderInfo> orders, List<AppraisalFileDocUploadInfo> newFilesToUploadToEdocs)
        {
            List<OrderContainer> processedOrders = new List<OrderContainer>();
            using (var ordersClient = new OrdersClient(credentials))
            {
                foreach (var appraisalOrder in orders)
                {
                    // Load the appraisal and add it to the list
                    var orderDetail = new OrderContainer() { GdmsAppraisalOrder = appraisalOrder, VendorID = appraisalOrder.VendorId, VendorName = vendor.VendorName };
                    try
                    {
                        orderDetail.ClientOrder = ordersClient.GetOrder(appraisalOrder.FileNumber);
                        orderDetail.UploadedFiles = ordersClient.GetOrderUploadedFilesList(appraisalOrder.FileNumber);
                        newFilesToUploadToEdocs.AddRange(
                            orderDetail.UploadedFiles
                                .Where(file => !appraisalOrder.UploadedDocumentIds.Contains(file.id) && file.viewableByClient)
                                .Select(file => new AppraisalFileDocUploadInfo(appraisalOrder.FileNumber, file)));
                    }
                    catch (GDMSErrorResponseException exc)
                    {
                        string logMsg = string.Format(
                            "GDMS::Error retrieving order info for file #{0}.{4}LoanID: {1}{4}GDMS Company Id: {2}{4}GDMS Username: {3}{4}{4}{5}",
                            appraisalOrder.FileNumber,
                            this.LoanId,
                            credentials.CompanyId,
                            credentials.UserName,
                            Environment.NewLine,
                            exc);
                        Tools.LogWarning(logMsg, exc);
                        orderDetail = this.GDMSErrorLoadingOrder(appraisalOrder, vendor.VendorName);
                    }

                    processedOrders.Add(orderDetail);
                }
            }

            return processedOrders;
        }

        /// <summary>
        /// Adds new files from Global DMS to a DB Message queue to auto-save into EDocs.
        /// </summary>
        /// <param name="newlyUploadedFiles">The new files detected from Global DMS.</param>
        private void EnqueueNewlyUploadedFiles(List<AppraisalFileDocUploadInfo> newlyUploadedFiles)
        {
            // If the broker is configured to autosave appraisal docs, submit
            // a messsage to the queue. The docs and loan file will be
            // updated as necessary. This is done to avoid hanging the page
            // while the files are downloaded/generated.
            // If the serialized list is greater than the maximum length of
            // the message queue data, we serialize chunks and enqueue multiple
            // messages per file. It will start at chunks of size 5 and go 
            // down to 1. The chunk size/decrement can probably be fine tuned
            // if necessary.
            if (newlyUploadedFiles.Count == 0)
            {
                return; // no files
            }

            var brokerAutoSavesAppraisalDocs = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.AppraisalDocs, this.Principal.BrokerId, E_EnforceFolderPermissions.True) != null;
            if (!brokerAutoSavesAppraisalDocs)
            {
                return; // not autosaved
            }

            var messages = new List<string>();
            var serializedUploadInfo = LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(newlyUploadedFiles);
            messages.Add(serializedUploadInfo);

            var chunkSize = 5;
            while (messages.Any(str => str.Length > ConstMsg.DATA_MAX_LENGTH) && --chunkSize > 0)
            {
                messages.Clear();
                var chunks = newlyUploadedFiles.Select((file, index) => new { File = file, Index = index })
                    .GroupBy(obj => obj.Index / chunkSize)
                    .Select(group => group.Select(obj => obj.File));

                foreach (var chunk in chunks)
                {
                    serializedUploadInfo = LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(chunk.ToList());
                    messages.Add(serializedUploadInfo);
                }
            }

            if (chunkSize == 0)
            {
                Tools.LogError("GDMS:: Could not insert newly uploaded files into message queue.");
            }
            else
            {
                var gdmsQueue = new DBMessageQueue(ConstMsg.GdmsAutoSaveDocsQueue);
                var brokerUserInfo = string.Format("BrokerId={0},UserId={1},EmployeeId={2}", this.Principal.BrokerId, this.Principal.UserId, this.Principal.EmployeeId);

                foreach (var msgData in messages)
                {
                    gdmsQueue.Send(this.LoanId.ToString("d"), msgData, brokerUserInfo);
                }
            }
        }
    }
}
