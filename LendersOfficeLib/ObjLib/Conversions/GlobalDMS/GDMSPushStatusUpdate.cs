﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class GDMSPushStatusUpdateResponse
    {
        public enum StatusType
        {
            OK,
            ERROR
        }

        public byte[] ToXml()
        {
            // Serialize to XML
            XmlSerializer serializer = new XmlSerializer(typeof(GDMSPushStatusUpdateResponse));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;
                using (XmlWriter tw = XmlWriter.Create(stream, writerSettings))
                {
                    serializer.Serialize(tw, this, ns);
                }
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public StatusType Status;
        public bool successFlag
        {
            get { return this.Status == StatusType.OK; }
            set { this.Status = value ? StatusType.OK : StatusType.ERROR; }
        }
        public string Message = "";
    }

    [XmlRoot("PushStatusUpdateMessage", Namespace = "http://schemas.datacontract.org/2004/07/GDMSWS.StatusPush.library")]
    public class GDMSPushStatusUpdateMessage
    {
        public static GDMSPushStatusUpdateMessage FromXml(XmlReader reader)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(GDMSPushStatusUpdateMessage));
            var response = (GDMSPushStatusUpdateMessage)deserializer.Deserialize(reader);

            return response;
        }

        public string Address1;
        public string Address2;
        public string ApprCompanyName;
        public string AppraiserEstCompleteDate;
        public int AppraiserId;
        public string AppraiserName;
        public string BorrowerName;
        public string CaseNumber;
        public string City;
        public string CurrentStatusComments;
        public string CustomStatusUpdateFieldsList;
        public string DateInspectionScheduled;
        public string DateStatusUpdated;
        public string EAD_DocID;
        public string EAD_Status;
        public int EngagerId;
        public string EngagerName;
        public string FNM_Status;
        public string FRE_Status;
        public string FileIdsUploaded;
        public int FileNumber;
        public string InspectionTime;
        public string LoanNumber;
        public int OrderStatusId;
        public string RRRNumber;
        public string State;
        public string StatusName;
        public string UCDP_DocID;
        public string Zip;
    }
}
