﻿// <copyright file="AppraisalFileDocUploadInfo.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   8/5/2014 1:57:53 PM
// </summary>
namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;

    /// <summary>
    /// Represents a file uploaded to GlobalDMS. We use this data to download the file into EDocs.
    /// </summary>
    [DataContract]
    public class AppraisalFileDocUploadInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalFileDocUploadInfo"/> class.
        /// </summary>
        /// <param name="appraisalFileNumber">The appraisal number to which the file belongs.</param>
        /// <param name="file">The file object as represented by GlobalDMS.</param>
        public AppraisalFileDocUploadInfo(int appraisalFileNumber, GDMS.LookupMethods.OrderFileObject file)
            : this(appraisalFileNumber, file.id, file.fileName, file.url, file.viewableByClient)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalFileDocUploadInfo"/> class.
        /// </summary>
        /// <param name="appraisalFileNumber">The appraisal number to which the file belongs.</param>
        /// <param name="fileId">The identifier of the file in GlobalDMS's system.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="url">The url where the file is hosted by GlobalDMS.</param>
        /// <param name="viewableByClient">The GlobalDMS permission of whether the file may be viewed by the client.</param>
        private AppraisalFileDocUploadInfo(int appraisalFileNumber, int fileId, string fileName, string url, bool viewableByClient)
        {
            this.AppraisalFileNumber = appraisalFileNumber;
            this.FileId = fileId;
            this.FileName = fileName;
            this.Url = url;
            this.ViewableByClient = viewableByClient;
        }

        /// <summary>
        /// Gets the appraisal order number given by GlobalDMS's system.
        /// </summary>
        /// <value>The unique appraisal order number given by GlobalDMS's system.</value>
        [DataMember]
        public int AppraisalFileNumber { get; private set; }

        /// <summary>
        /// Gets the identifier GlobalDMS uses for the file on the appraisal order.
        /// </summary>
        /// <value>The identifier GlobalDMS uses for the file on the appraisal order.  At least unique within the appraisal order; possibly unique to the user level.</value>
        [DataMember]
        public int FileId { get; private set; }

        /// <summary>
        /// Gets the name of the file that is was uploaded to GlobalDMS.
        /// </summary>
        /// <value>The name of the file that is was uploaded to GlobalDMS.</value>
        [DataMember]
        public string FileName { get; private set; }

        /// <summary>
        /// Gets the url where GlobalDMS uploaded the file.
        /// </summary>
        /// <value>The url where GlobalDMS uploaded the file.</value>
        [DataMember]
        public string Url { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the file is viewable by the client, a permission defined by GlobalDMS.
        /// </summary>
        /// <value>The permission defined by GlobalDMS of whether the file is viewable by the client.</value>
        [DataMember]
        public bool ViewableByClient { get; private set; } // Include this just to be safe.

        /// <summary>
        /// Upload a collection of files into EDocs from GlobalDMS.
        /// </summary>
        /// <param name="loanId">The loan ID of the order receiving the files.</param>
        /// <param name="brokerId">The broker ID of the order receiving the files.</param>
        /// <param name="uploadedFiles">The files being uploaded from GlobalDMS.</param>
        public static void UploadFiles(Guid loanId, Guid brokerId, IEnumerable<AppraisalFileDocUploadInfo> uploadedFiles)
        {
            if (uploadedFiles == null || !uploadedFiles.Any())
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AppraisalFileDocUploadInfo));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var loanUpdated = false;
            var appraisalsToSave = new HashSet<GDMSAppraisalOrderInfo>();
            var orders = GDMSAppraisalOrderInfo.GetAllGdmsAppraisalOrdersByLoanId(loanId, brokerId);
            foreach (AppraisalFileDocUploadInfo uploadedFile in uploadedFiles)
            {
                GDMSAppraisalOrderInfo appraisalOrder = orders.FirstOrDefault(order => order.FileNumber == uploadedFile.AppraisalFileNumber);
                bool? updatedLoan = uploadedFile.UploadFileToEdocs(appraisalOrder, dataLoan);
                if (updatedLoan.HasValue)
                {
                    appraisalsToSave.Add(appraisalOrder);
                    loanUpdated |= updatedLoan.Value;
                }
            }

            foreach (var appraisalOrder in appraisalsToSave)
            {
                appraisalOrder.Save();
            }

            if (loanUpdated)
            {
                dataLoan.Save();
            }
        }

        /// <summary>
        /// Uploads the individual file to EDocs, updating the loan and appraisals if necessary and indicating whether
        /// a save is needed by returning a boolean.
        /// </summary>
        /// <param name="appraisalOrder">The appraisal order to upload to.</param>
        /// <param name="dataLoan">The loan to save data from the appraisal xml.</param>
        /// <returns><see langword="null"/> if no action was performed on the order; <see langword="true"/> if <paramref name="dataLoan"/> has been updated during the upload; false otherwise.</returns>
        public bool? UploadFileToEdocs(GDMSAppraisalOrderInfo appraisalOrder, CPageData dataLoan)
        {
            Tools.LogInfo(
                "[GDMSFileUpload] - Processing file:" + Environment.NewLine +
                "File Name: " + this.FileName + Environment.NewLine +
                "File ID: " + this.FileId + Environment.NewLine +
                "Appraisal File Number: " + this.AppraisalFileNumber + Environment.NewLine +
                "URL: " + this.Url + Environment.NewLine +
                "LoanID: " + dataLoan.sLId + Environment.NewLine +
                "LoanNumber: " + dataLoan.sLNm);

            if (appraisalOrder == null || !this.ViewableByClient || appraisalOrder.FileNumber != this.AppraisalFileNumber)
            {
                Tools.LogError("[GDMSFileUpload] File not viewable by client or appraisal order not found for loan.");
                return null;
            }

            if (appraisalOrder.UploadedDocumentIds.Contains(this.FileId))
            {
                Tools.LogInfo("[GDMSFileUpload] File ID already uploaded.");
                return null; // 2/6/2014 tj - opm 168702 prevent appraisal docs from getting duplicated in edocs
            }

            bool loanUpdated = false;
            Guid? edocId = null;
            try
            {
                if (this.FileName.EndsWith("pdf", StringComparison.OrdinalIgnoreCase))
                {
                    edocId = AutoSaveDocTypeFactory.SavePdfDocFromUrlAsSystem(
                        E_AutoSavePage.AppraisalDocs,
                        this.Url,
                        dataLoan.sBrokerId,
                        dataLoan.sLId,
                        dataLoan.GetAppData(0).aAppId,
                        this.FileName);
                }
                else if (this.FileName.EndsWith("xml", StringComparison.OrdinalIgnoreCase))
                {
                    // 10/7/2013 gf - opm 115878 update loan file based on info in UAD appraisal xml.
                    var xmlPath = TempFileUtils.NewTempFilePath();
                    using (var webClient = new WebClient())
                    {
                        webClient.DownloadFile(this.Url, xmlPath);
                    }

                    var xml = TextFileHelper.ReadFile(xmlPath);
                    loanUpdated = dataLoan.PopulateDataFromAppraisalXml(xml, appraisalOrder);

                    edocId = AutoSaveDocTypeFactory.SaveXmlDoc(
                        E_AutoSavePage.AppraisalDocs,
                        () => xmlPath,
                        dataLoan.sBrokerId,
                        dataLoan.sLId,
                        dataLoan.GetAppData(0).aAppId,
                        null /* UserId */,
                        this.FileName);
                }
                else
                {
                    Tools.LogWarning("[GDMSFileUpload] Unexpected file type for file " + this.FileName + ". LoanId: " + dataLoan.sLId);
                }
            }
            catch (WebException e)
            {
                Tools.LogError("[GDMSFileUpload] Error autosaving remote file.", e);
                return loanUpdated;
            }
            catch (NotSupportedException e)
            {
                Tools.LogError("[GDMSFileUpload] Error autosaving remote file.", e);
                return loanUpdated;
            }

            appraisalOrder.UploadedDocumentIds.Add(this.FileId);
            if (edocId.HasValue)
            {
                appraisalOrder.AddEdocRecord(this.FileId, edocId.Value, this.FileName);
            }

            return loanUpdated;
        }
    }
}
