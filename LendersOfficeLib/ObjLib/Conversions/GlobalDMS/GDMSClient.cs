﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using DataAccess;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.ServiceModel;
using System.Xml.Linq;
using LendersOffice.GDMS.FileUpload;
using System.ServiceModel.Description;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class GDMSCredentials
    {
        public int CompanyId;
        public string UserName;
        public string Password;
        public int UserType;
        public bool ExportToStage;
    }

    /// <summary>
    /// This exception will contain GDMS's ErrorResponse object.
    /// You can check Errors which will, as far as I've seen, contain a list of strings describing the errors.
    /// 
    /// If this throws a SerializationException, then that means the serializer can neither read the xml as
    /// the expected object nor as an ErrorResponse.
    /// Perhaps we got html back instead of xml.
    /// In that case, there's probably an issue with a proxy somewhere between us and the GDMS server.
    /// Either that, or there's a programming error in the deserialization code.
    /// </summary>
    [Serializable()]
    public class GDMSErrorResponseException : System.Exception
    {
        public GDMS.LookupMethods.ErrorResponse ErrorResponse;
        public List<string> Errors = new List<string>();
        public string ErrorMessage;

        public GDMSErrorResponseException() : base() { }
        public GDMSErrorResponseException(string xml)
            : base("Received error response from GDMS:" + Environment.NewLine + xml)
        {
            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.ErrorResponse), "ErrorResponse", LookupMethodsClient.NAMESPACE);

            this.ErrorResponse = (GDMS.LookupMethods.ErrorResponse)dcs.ReadObject(new MemoryStream(Encoding.ASCII.GetBytes(xml)));

            foreach (var error in ErrorResponse.errors)
            {
                this.Errors.Add(error as string);
            }

            this.ErrorMessage = this.ErrorResponse.message;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("GDMSErrorResponseException:");
            sb.AppendLine(Environment.NewLine);
            sb.Append("ErrorMessage: ").AppendLine(this.ErrorMessage);
            sb.AppendLine("Errors:");
            for (int i = 0; i < this.Errors.Count; ++i)
            {
                sb.AppendLine((i + 1) + ". " + this.Errors[i]);
            }

            return sb.ToString() + Environment.NewLine + base.ToString();
        }
    }

    /// <summary>
    /// This exception will contain GDMS's ConnectionResponseBase object.
    /// 
    /// This is not an error, but it only happens in exceptional circumstances.
    /// Usually, GDMS will return a ConnectionResponseBase when there are no results.
    /// </summary>
    [Serializable()]
    public class GDMSConnectionResponseBaseException : System.Exception
    {
        public GDMS.LookupMethods.ConnectionResponseBase ConnectionResponseBase;

        public GDMSConnectionResponseBaseException() : base() { }
        public GDMSConnectionResponseBaseException(string xml)
            : base("Received ConnectionResponseBase from GDMS: " + Environment.NewLine + xml)
        {
            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.ConnectionResponseBase), "ConnectionResponseBase", GDMSClient.NAMESPACE);

            ConnectionResponseBase = (GDMS.LookupMethods.ConnectionResponseBase)dcs.ReadObject(new MemoryStream(Encoding.ASCII.GetBytes(xml)));
        }
    }

    public abstract class GDMSClient : IDisposable
    {
        public abstract System.ServiceModel.Description.ServiceEndpoint Endpoint { get; } // The service endpoint that we're talking to

        public void StoreMessage(ref Message message, string name)
        {
            Message msgCopy = null;
            using (MessageBuffer buffer = message.CreateBufferedCopy(int.MaxValue))
            {
                // These messages are finicky - accessing it seems to make it invalid for sending.
                message = buffer.CreateMessage();
                msgCopy = buffer.CreateMessage(); // We need a deep copy of the message for logging - we'll remove the Base64 file data, for example.
            }

            LogMessage(msgCopy, name);
        }

        private void LogMessage(Message msg, string name)
        {
            string messageText;
            try
            {
                StringBuilder sb = new StringBuilder();
                if (msg.Headers.Action == "http://tempuri.org/IFileUpload/SubmitReport")
                {
                    TypedMessageConverter tmc = TypedMessageConverter.Create(typeof(FileUploadMessage), "http://tempuri.org/IFileUpload/SubmitReport");
                    FileUploadMessage fileMsg = (FileUploadMessage)tmc.FromMessage(msg);
                    fileMsg.FileData = new MemoryStream();//Clear FileData to prevent OutOfMemoryException in XmlWriter
                    msg = tmc.ToMessage(fileMsg);
                    sb.AppendLine("FileData was removed from this log.");
                }

                using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(sb))
                {
                    msg.WriteMessage(xw);
                }

                sb.Replace("&lt;", "<");
                sb.Replace("&gt;", ">");
                messageText = Regex.Replace(sb.ToString(), "<([A-Za-z0-9]+:)?Password( .*)?>.+?</\\1?Password>", "<$1Password$2>*****</$1Password>");
            }
            catch (OutOfMemoryException oomExc)
            {
                Tools.LogWarning("Ran out of memory while recording message to/from GDMS", oomExc);
                messageText = "Unable to record message sent to/from GDMS - an Out of Memory Exception occurred.";
            }

            Tools.LogInfo(name + ":" + Environment.NewLine + messageText);
        }

        public static string NAMESPACE = "http://schemas.datacontract.org/2004/07/GDMSWS.Service.library.DataContracts";

        public static object Process(string xml, DataContractSerializer dcs)
        {
            string tagName = string.Empty;

            try
            {
                XElement response = XElement.Parse(xml);
                tagName = response.Name.LocalName;
                XName name = XName.Get("count", NAMESPACE);
                XElement count = response.Element(name);

                if (tagName.Equals("ConnectionResponseBase", StringComparison.OrdinalIgnoreCase)
                    && count != null && count.Value  == "0" )
                {
                    return null;
                }
                
            }
            catch (XmlException e)
            {
                throw new SerializationException("XmlReader could not read the following string:" + Environment.NewLine + xml, e);
            }

            using (MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(xml)))
            {
                switch (tagName)
                {
                    case "ErrorResponse":
                        throw new GDMSErrorResponseException(xml);
                    case "ConnectionResponseBase":
                        throw new GDMSConnectionResponseBaseException(xml);
                    default: // will throw serializationexception if the object is not recognized
                        return dcs.ReadObject(ms);
                }
            }
        }

        public abstract void Dispose();
    }
}
