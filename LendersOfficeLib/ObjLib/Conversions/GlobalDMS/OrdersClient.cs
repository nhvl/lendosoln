﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;

using LendersOffice.GDMS.LookupMethods;
using LendersOffice.GDMS.Orders;
using DataAccess;
using System.Reflection;

namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    public class OrdersClient : GDMSClient, IDisposable
    {
        private GDMS.Orders.OrdersClient m_client;
        private GDMS.Orders.CredentialsObject m_credentials;

        public override System.ServiceModel.Description.ServiceEndpoint Endpoint
        {
            get { return m_client.Endpoint; }
        }

        public OrdersClient(GDMSCredentials credentials)
            : base()
        {
            string clientEndpoint = "BasicHttpBinding_IOrdersProduction";

            if (credentials.ExportToStage || credentials.UserName == "LQBtestuser" || credentials.UserName == "LQBUser")
            {
                clientEndpoint = "BasicHttpBinding_IOrders";
            }

            m_client = new GDMS.Orders.OrdersClient(clientEndpoint);
            m_client.Endpoint.Behaviors.Add(new GDMSMessageBehavior(this));
            
            m_credentials = new GDMS.Orders.CredentialsObject();
            m_credentials.CompanyId = credentials.CompanyId;
            m_credentials.UserName = credentials.UserName;
            m_credentials.Password = credentials.Password;
            m_credentials.UserType = credentials.UserType;
        }

        public GDMS.LookupMethods.SearchItem[] SearchOrders(GDMS.Orders.SearchOrdersRequestParams searchParams)
        {
            return SearchOrders(m_client, m_credentials, searchParams);
        }

        public static GDMS.LookupMethods.SearchItem[] SearchOrders(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, GDMS.Orders.SearchOrdersRequestParams searchParams)
        {
            var response = client.SearchOrders(credentials, searchParams, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SearchItemsListResponse), "SearchItemsListResponse", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SearchItemsListResponse;

            return responseObj.items;
        }

        public GDMS.LookupMethods.ClientOrder GetOrder(int fileNumber)
        {
            return GetOrder(m_client, m_credentials, fileNumber);
        }

        /// <summary>
        /// Throws an exception with error "Access check failed" or "Invalid or missing credentials" if GDMS could not find the fileNumber.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="credentials"></param>
        /// <param name="fileNumber"></param>
        public static GDMS.LookupMethods.ClientOrder GetOrder(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, int fileNumber)
        {
            var response = client.GetOrder(credentials, fileNumber, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleDataObject), "SingleDataObject", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleDataObject;
            var item = responseObj.data as GDMS.LookupMethods.ClientOrder;
            return item;
        }

        public GDMS.LookupMethods.clsEmailLog[] GetOrderEmailLog(int fileNumber)
        {
            return GetOrderEmailLog(m_client, m_credentials, fileNumber);
        }

        public static GDMS.LookupMethods.clsEmailLog[] GetOrderEmailLog(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, int fileNumber)
        {
            var response = client.GetOrderEmailLog(credentials, fileNumber, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.OrderEmailLogListResponse), "OrderEmailLogListResponse", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.OrderEmailLogListResponse;
            if (responseObj == null)
            {
                return new GDMS.LookupMethods.clsEmailLog[0];
            }

            return responseObj.items;
        }

        public GDMS.LookupMethods.OrderStatusHistory[] GetOrderStatusHistory(int fileNumber)
        {
            return GetOrderStatusHistory(m_client, m_credentials, fileNumber);
        }

        /// <summary>
        /// Throws an exception with error "Access check failed" or "Invalid or missing credentials" if GDMS could not find the fileNumber.
        /// </summary>
        public static GDMS.LookupMethods.OrderStatusHistory[] GetOrderStatusHistory(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, int fileNumber)
        {
            var response = client.GetOrderStatusHistory(credentials, fileNumber, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.OrderStatusHistoryListResponse), "OrderStatusHistoryListResponse", OrdersClient.NAMESPACE);

            try
            {
                var responseObj = Process(response, dcs) as GDMS.LookupMethods.OrderStatusHistoryListResponse;
                if (responseObj == null)
                {
                    return new GDMS.LookupMethods.OrderStatusHistory[0];
                }

                return responseObj.items;
            }
            catch (GDMSConnectionResponseBaseException) // There are no items, so return an empty list
            {
                return new GDMS.LookupMethods.OrderStatusHistory[0];
            }
        }

        public GDMS.LookupMethods.ListOrderItemBase[] GetOrders()
        {
            return GetOrders(m_client, m_credentials);
        }

        public static GDMS.LookupMethods.ListOrderItemBase[] GetOrders(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials)
        {
            var requestParams = new GDMS.Orders.ListOrdersRequestParams();
            requestParams.ListOrderType = GDMS.Orders.ListOrdersRequestParams.OrderTypes.All;

            var response = client.GetOrders(credentials, requestParams, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleDataObject), "SingleDataObject", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleDataObject;

            var data = responseObj.data as GDMS.LookupMethods.ListOrdersWrapper;

            return data.items;
        }

        public GDMS.LookupMethods.OrderFileObject[] GetOrderUploadedFilesList(int fileNumber)
        {
            return GetOrderUploadedFilesList(m_client, m_credentials, fileNumber);
        }

        public static GDMS.LookupMethods.OrderFileObject[] GetOrderUploadedFilesList(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, int fileNumber)
        {
            var response = client.GetOrderUploadedFilesList(credentials, fileNumber, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.OrderFileObjectListResponse), "OrderFileObjectListResponse", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.OrderFileObjectListResponse;

            if (responseObj == null)
            {
                return new GDMS.LookupMethods.OrderFileObject[0];
            }

            return responseObj.items;
        }

        public clsCUDetail GetUcdpCuSummaryData(int fileNumber)
        {
            return GetUcdpCuSummaryData(this.m_client, this.m_credentials, fileNumber);
        }

        public static clsCUDetail GetUcdpCuSummaryData(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, int fileNumber)
        {
            string response = client.GetUCDPCUSummaryData(credentials, fileNumber, blnLatestOnly: true, sReturnFormat: "xml");

            var dcs = new DataContractSerializer(typeof(UCDPCUSummaryResponse), "UCDPCUSummaryResponse", NAMESPACE);

            var responseObj = Process(response, dcs) as UCDPCUSummaryResponse;

            return responseObj?.items?.FirstOrDefault();
        }

        public string SendEmailCommunication(GDMS.Orders.SendEmailMessageObject emailToSend, GDMS.Orders.OrderIdentifier orderIdentification)
        {
            return SendEmailCommunication(m_client, m_credentials, emailToSend, orderIdentification);
        }

        public static string SendEmailCommunication(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, GDMS.Orders.SendEmailMessageObject emailToSend, GDMS.Orders.OrderIdentifier orderIdentification)
        {
            var response = client.SendEmailCommunication(credentials, emailToSend, orderIdentification, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleStringDataResponse), "SingleStringDataResponse", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleStringDataResponse;

            return responseObj.data;
        }

        public string AddClientOrder(GDMS.Orders.ClientOrder orderObject)
        {
            return AddClientOrder(m_client, m_credentials, orderObject);
        }

        public static string AddClientOrder(GDMS.Orders.OrdersClient client, GDMS.Orders.CredentialsObject credentials, GDMS.Orders.ClientOrder orderObject)
        {
            var response = client.AddClientOrder(credentials, orderObject, "xml");

            var dcs = new DataContractSerializer(typeof(GDMS.LookupMethods.SingleStringDataResponse), "SingleStringDataResponse", OrdersClient.NAMESPACE);

            var responseObj = Process(response, dcs) as GDMS.LookupMethods.SingleStringDataResponse;

            return responseObj.data;
        }

        public override void Dispose()
        {
            m_client.Close();
        }

        /// <summary>
        /// Converts string name for Enum SendEmailMessageObject.SendEmailTo to value
        /// </summary>
        public static SendEmailMessageObject.SendEmailTo sendEmailToWho(string name)
        {
            string[] NamesSendEmailTo = System.Enum.GetNames(typeof(SendEmailMessageObject.SendEmailTo));
            Array ValuesSendEmailTo = System.Enum.GetValues(typeof(SendEmailMessageObject.SendEmailTo));

            for (int i = 0; i < NamesSendEmailTo.Length; i++)
            {
                if (name == NamesSendEmailTo[i])
                    return (SendEmailMessageObject.SendEmailTo)ValuesSendEmailTo.GetValue(i);
            }
            throw new ArgumentOutOfRangeException("name", name, "Must be a valid name for Enum value.");
        }
    }

}
