﻿namespace LendersOffice.ObjLib.Conversions.GlobalDMS
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using Integration.Appraisals;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains appraisal order file numbers.
    /// </summary>
    public class GDMSAppraisalOrderInfo : AppraisalOrderBase
    {
        /// <summary>
        /// An xml serializer, for use in serializing uploadedDocumentIds for DB storage.
        /// </summary>
        private static XmlSerializer uploadedDocSerializer = new XmlSerializer(typeof(HashSet<int>));

        /// <summary>
        /// A value indicating whether this is a new appraisal order.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// The docs that have been saved to edocs.
        /// </summary>
        private Dictionary<int, Tuple<Guid, string>> uploadedDocsSavedToEDocs;

        /// <summary>
        /// The docs that have just been uploaded to edocs.
        /// </summary>
        private Dictionary<int, Tuple<Guid, string>> uploadedEdocsToSave = new Dictionary<int, Tuple<Guid, string>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GDMSAppraisalOrderInfo"/> class.
        /// </summary>
        /// <param name="fileNumber">The file number.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="requestDetails">The request details.</param>
        public GDMSAppraisalOrderInfo(int fileNumber, Guid loanId, Guid brokerId, Guid userId, Guid employeeId, Guid vendorId, GdmsOrderRequestDetails requestDetails)
            : base(loanId, brokerId)
        {
            // Set class specific properties.
            this.isNew = true;
            this.FileNumber = fileNumber;
            this.UserId = userId;
            this.EmployeeId = employeeId;
            this.VendorId = vendorId;
            this.UploadedDocumentIds = new HashSet<int>();
            this.uploadedDocsSavedToEDocs = new Dictionary<int, Tuple<Guid, string>>();

            this.SetPropertyInfo(requestDetails);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GDMSAppraisalOrderInfo"/> class.
        /// </summary>
        /// <param name="reader">Dictionary turned data reader.</param>
        /// <param name="edocRecords">List of edoc records.</param>
        private GDMSAppraisalOrderInfo(IReadOnlyDictionary<string, object> reader, List<IReadOnlyDictionary<string, object>> edocRecords)
            : base(reader)
        {
            this.isNew = false;
            this.FileNumber = (int)reader["FileNumber"];
            this.UserId = (Guid)reader["UserId"];
            this.EmployeeId = (Guid)reader["EmployeeId"];
            this.VendorId = (Guid)reader["VendorId"];

            var serializedDocIds = (string)reader["UploadedFileIdXmlContent"];
            if (string.IsNullOrEmpty(serializedDocIds))
            {
                this.UploadedDocumentIds = new HashSet<int>();
            }
            else
            {
                using (var sr = new StringReader(serializedDocIds))
                {
                    this.UploadedDocumentIds = (HashSet<int>)uploadedDocSerializer.Deserialize(sr);
                }
            }

            this.uploadedDocsSavedToEDocs = new Dictionary<int, Tuple<Guid, string>>();
            foreach (var edocRecord in edocRecords.CoalesceWithEmpty())
            {
                int id = (int)edocRecord["GdmsFileId"];
                if (this.uploadedDocsSavedToEDocs.ContainsKey(id))
                {
                    /* ejm opm 469778
                     * On rare occasions, the same set of un-saved documents will get pushed to the GDMS doc processor queue at the same time (due to simultaneous pushes for example).
                     * Different processors will take one set each. 
                     * Since the associated appraisal order that is cached in each processor hasn't been updated with the saved edocs yet, the processors proceed with making an edoc and assigning
                     *  the doc to the appraisal order.
                     * This results in the same set of edocs being saved to a single order.
                     * So this will skip the duplicate edocs.
                     * 
                     * Ideal fix would be to either check for existing docs in the queue before adding to the queue 
                     * or to have a table that holds all the file ids that all the queues are processing. If it exists in that table, don't process the file.
                     */
                    continue;
                }

                Guid edocId = (Guid)edocRecord["EdocId"];
                string fileName = (string)edocRecord["FileName"];
                this.uploadedDocsSavedToEDocs.Add(id, new Tuple<Guid, string>(edocId, fileName));
            }
        }

        /// <summary>
        /// Gets or sets the GDMS appraisal order file number.
        /// </summary>
        /// <value>File number as int value.</value>
        public int FileNumber
        {
            get
            {
                // Note: we want this to throw if OrderNumber is not an int.
                // Hence why I use int.Parse instead of int.TryParse.
                return int.Parse(this.OrderNumber);
            }

            set
            {
                this.OrderNumber = value.ToString();
            }
        }

        /// <summary>
        /// Gets the user ID of the user who submitted this appraisal order.
        /// </summary>
        /// <value>User ID of the user who submitted this appraisal order.</value>
        public Guid UserId { get; private set; }

        /// <summary>
        /// Gets the employee ID of the user who submitted this appraisal order.
        /// </summary>
        /// <value>Employee ID of the user who submitted this appraisal order.</value>
        public Guid EmployeeId { get; private set; }

        /// <summary>
        /// Gets the Vendor ID.
        /// </summary>
        /// <value>Vendor ID.</value>
        public Guid VendorId { get; private set; }

        /// <summary>
        /// Gets the hash set of uploaded document IDs.
        /// </summary>
        /// <value>HashSet of uploaded document IDs.</value>
        public HashSet<int> UploadedDocumentIds { get; }

        /// <summary>
        /// Gets the uploaded documents that have been uploaded to edocs.
        /// </summary>
        public IReadOnlyDictionary<int, Tuple<Guid, string>> UploadedDocsSavedToEdocs
        {
            get
            {
                return this.uploadedDocsSavedToEDocs;
            }
        }

        /// <summary>
        /// Gets the type of appraisal order this object is.
        /// </summary>
        /// <value>Appraisal order type.</value>
        public override AppraisalOrderType AppraisalOrderType
        {
            get
            {
                return AppraisalOrderType.Gdms;
            }

            protected set
            {
            }
        }

        /// <summary>
        /// Gets the ids of all the associated edocs.
        /// Note that this only includes documents that have already been uploaded successfully to edocs. There may be more in the queue.
        /// </summary>
        protected override IEnumerable<Guid> AssociatedEdocIds
        {
            get
            {
                return this.UploadedDocsSavedToEdocs?.Values?.Select(pair => pair.Item1);
            }
        }

        /// <summary>
        /// Retrieves GDMS appraisal order by file number.
        /// </summary>
        /// <param name="fileNumber">Appraisal order file number.</param>
        /// <returns>A GDMSAppraisalOrderInfo object, or null if order is not found.</returns>
        public static GDMSAppraisalOrderInfo Load(int fileNumber)
        {
            // 4/30/2015 dd - The file number is generate from Global DMS. From the look of it, this file number is unique across all brokers.
            // Therefore when searching for fileNumber across multiple database, I will stop at the first record.
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@FileNumber", fileNumber)
                    };

                IReadOnlyDictionary<string, object> orderProperties = null;
                List<IReadOnlyDictionary<string, object>> orderEdocs = new List<IReadOnlyDictionary<string, object>>();
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GDMS_GetOrderByFileNumber", parameters))
                {
                    if (reader.Read())
                    {
                        orderProperties = reader.ToDictionary();
                    }

                    if (orderProperties != null && reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            orderEdocs.Add(reader.ToDictionary());
                        }

                        return new GDMSAppraisalOrderInfo(orderProperties, orderEdocs);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves all GDMS appraisal orders for a loan.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>A GDMSAppraisalOrderInfo object, or null if order is not found.</returns>
        public static IEnumerable<GDMSAppraisalOrderInfo> GetAllGdmsAppraisalOrdersByLoanId(Guid loanId, Guid brokerId)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@LoanId", loanId),
                    new SqlParameter("@BrokerId", brokerId)
                };

            List<IReadOnlyDictionary<string, object>> ordersInDb = new List<IReadOnlyDictionary<string, object>>();
            List<IReadOnlyDictionary<string, object>> orderDocsInDb = new List<IReadOnlyDictionary<string, object>>();

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GDMS_GetAllOrdersByLoanId", parameters))
            {
                while (reader.Read())
                {
                    ordersInDb.Add(reader.ToDictionary());
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        orderDocsInDb.Add(reader.ToDictionary());
                    }
                }
            }

            List<GDMSAppraisalOrderInfo> orders = new List<GDMSAppraisalOrderInfo>();

            Dictionary<int, List<IReadOnlyDictionary<string, object>>> docsInDbForOrders = new Dictionary<int, List<IReadOnlyDictionary<string, object>>>();

            foreach (var docsInDb in orderDocsInDb)
            {
                var fileNumber = (int)docsInDb["FileNumber"];

                if (!docsInDbForOrders.ContainsKey(fileNumber))
                {
                    docsInDbForOrders[fileNumber] = new List<IReadOnlyDictionary<string, object>>();
                }

                docsInDbForOrders[fileNumber].Add(docsInDb);
            }

            foreach (var orderInDb in ordersInDb)
            {
                var fileNumber = (int)orderInDb["FileNumber"];
                var docs = docsInDbForOrders.ContainsKey(fileNumber) ? docsInDbForOrders[fileNumber] : null;

                orders.Add(new GDMSAppraisalOrderInfo(orderInDb, docs));
            }

            return orders;
        }

        /// <summary>
        /// Records the document as having been saved to edocs.
        /// </summary>
        /// <param name="gdmsDocId">The GDMS file id.</param>
        /// <param name="edocId">The edoc id.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>True if the edoc was added. False otherwise.</returns>
        public bool AddEdocRecord(int gdmsDocId, Guid edocId, string fileName)
        {
            if (this.uploadedDocsSavedToEDocs.ContainsKey(gdmsDocId))
            {
                return false;
            }

            var value = new Tuple<Guid, string>(edocId, fileName);
            this.uploadedDocsSavedToEDocs.Add(gdmsDocId, value);
            this.uploadedEdocsToSave.Add(gdmsDocId, value);

            return true;
        }

        /// <summary>
        /// Save GDMS order to DB.
        /// </summary>
        public void Save()
        {
            using (var storedProcedureExecutionContext = new CStoredProcedureExec(this.BrokerId))
            {
                try
                {
                    storedProcedureExecutionContext.BeginTransactionForWrite();

                    var orderParameters = this.GetOrderParameters();
                    if (this.isNew)
                    {
                        storedProcedureExecutionContext.ExecuteNonQuery("GDMS_CreateOrder", orderParameters.ToArray());
                    }
                    else
                    {
                        storedProcedureExecutionContext.ExecuteNonQuery("GDMS_UpdateOrder", orderParameters.ToArray());
                    }

                    foreach (var edoc in this.uploadedEdocsToSave)
                    {
                        var parameters = new SqlParameter[]
                        {
                                new SqlParameter("@FileNumber", this.FileNumber),
                                new SqlParameter("@BrokerId", this.BrokerId),
                                new SqlParameter("@LoanId", this.LoanId),
                                new SqlParameter("@GdmsFileId", edoc.Key),
                                new SqlParameter("@EdocId", edoc.Value.Item1),
                                new SqlParameter("@FileName", edoc.Value.Item2),
                        };

                        storedProcedureExecutionContext.ExecuteNonQuery("GDMS_ORDER_EDOCS_CreateEdocRecord", parameters);
                    }

                    storedProcedureExecutionContext.CommitTransaction();
                    this.uploadedEdocsToSave.Clear();
                }
                catch (Exception e)
                {
                    storedProcedureExecutionContext.RollbackTransaction();
                    Tools.LogError(e);
                    throw;
                }
            }
        }

        /// <summary>
        /// Converts this appraisal order into an appraisal order view.
        /// </summary>
        /// <param name="principal">The principal of the user creating the views.</param>
        /// <returns>Appraisal order view representation of the order.</returns>
        public override AppraisalOrderView ToView(AbstractUserPrincipal principal)
        {
            AppraisalOrderView view = new AppraisalOrderView(this);

            view.VendorId = this.VendorId;
            view.LoadingId = this.FileNumber.ToString();
            return view;
        }

        /// <summary>
        /// Gets a list of parameters for saving order to DB.
        /// </summary>
        /// <returns>List of parameter.</returns>
        private List<SqlParameter> GetOrderParameters()
        {
            List<SqlParameter> parameters = this.GetBaseParameters();

            parameters.Add(new SqlParameter("@FileNumber", this.FileNumber));
            parameters.Add(new SqlParameter("@UserId", this.UserId));
            parameters.Add(new SqlParameter("@EmployeeId", this.EmployeeId));
            parameters.Add(new SqlParameter("@VendorId", this.VendorId));

            // Serialize and add the uploaded doc ids.
            string serializedDocIds = string.Empty;
            using (var sw = new StringWriter())
            {
                uploadedDocSerializer.Serialize(sw, this.UploadedDocumentIds);
                serializedDocIds = sw.ToString();
            }

            parameters.Add(new SqlParameter("@UploadedFileIdXmlContent", serializedDocIds));

            return parameters;
        }
    }
}