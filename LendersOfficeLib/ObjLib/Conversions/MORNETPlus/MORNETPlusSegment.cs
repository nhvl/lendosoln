using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataAccess;
namespace LendersOffice.Conversions.MORNETPlus
{
    
    public class MORNETPlusSegment 
    {
        private string m_line;
        private string[] m_parts;
        private SegmentDefinition m_definition;

        private List<string> m_errorMessageList = new List<string>();

        public IEnumerable<string> ErrorMessages
        {
            get { return m_errorMessageList; }
        }

        internal MORNETPlusSegment(SegmentDefinition def) : this(null, def)
        {
        }
        internal MORNETPlusSegment(string line, SegmentDefinition def) 
        {
            m_line = line;
            m_definition = def;
            m_parts = new string[def.PartCount];
        }
        public string Get(string name) 
        {
            return Get(name, true);
        }
        /// <summary>
        /// Name has following format XXX-ZZZ. XXX represents a segment record ID.
        /// The ZZZ represents the field number within that segment, starting with 010 and incrementing by 10.
        /// 
        /// Example: In segment "00A", the four fields in the segment are numbered: 00A-010, 00A-020, 00A-030, 00A-040
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isTrim"></param>
        /// <returns></returns>
        public string Get(string name, bool isTrim) 
        {
            return m_definition.Get(m_line, name, isTrim);
        }

        public bool GetBool(string name) 
        {
            return m_definition.GetBool(m_line, name);
        }


        public override string ToString() 
        {
            return m_definition.BuildSegment(m_parts);
        }

        public string this[string name]
        {
            get
            {
                return Get(name, true);
            }
            set
            {
                int index = m_definition.NameToIndex(name);

                if (index < m_parts.Length)
                {
                    if (value.IndexOfAny(new char[] { '<', '>' }) >= 0)
                    {
                        m_errorMessageList.Add(string.Format("[{0}] contains invalid character '<', '>'. Value='{1}'", name, value));
                    }
                    m_parts[index] = value;
                }
                else
                    Tools.LogError("Programming Error. " + name + " is exceeding segment definition");
            }
        }
        public ArrayList WarningMessages 
        {
            get { return m_definition.WarningMessages; }
        }


    }

}
