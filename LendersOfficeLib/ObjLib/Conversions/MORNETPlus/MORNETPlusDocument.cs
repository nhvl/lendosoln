namespace LendersOffice.Conversions.MORNETPlus
{
    using System;
    using System.Collections;
    using System.IO;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    public abstract class MORNETPlusDocument 
    {

        private string m_sDuCaseId = "";
        private int m_index;
        private string[] m_lines;

        public string sDuCaseId 
        {
            get { return m_sDuCaseId; }
            set { m_sDuCaseId = value; }
        }

        private string m_sFannieInstitutionId = "";
        public string sFannieInstitutionId
        {
            get { return m_sFannieInstitutionId; }
            set { m_sFannieInstitutionId = value; }
        }

        public void Load(string fileName) 
        {
            Action<TextFileHelper.LqbTextReader> readHandler = delegate(TextFileHelper.LqbTextReader reader)
            {
                Load(reader.Reader);
            };

            TextFileHelper.OpenRead(fileName, readHandler);
        }

        public void Load(StreamReader stream) 
        {
            ArrayList list = new ArrayList();

            string line = null;
            while ((line = stream.ReadLine()) != null) 
            {
                if (line.TrimWhitespaceAndBOM() == "")
                    continue; // Skip blank line.
                list.Add(line);
            }

            m_lines = (string[]) list.ToArray(typeof(string));
            m_index = -1;

            Process();
        }
        public void Load(string[] lines) 
        {
            // 6/28/2006 dd - Need to filter out blank line.
            ArrayList list = new ArrayList();
            foreach (string s in lines) 
            {
                if (s.TrimWhitespaceAndBOM() != "")
                {
                    if (s.EndsWith("\r"))
                    {
                        list.Add(s.Substring(0, s.Length - 1));
                    }
                    else
                    {
                        list.Add(s);
                    }
                }
            }
            m_lines = (string[]) list.ToArray(typeof(string));
            m_index = -1;
            Process();
        }
        protected string GetNextLine() 
        {
            if (++m_index >= m_lines.Length)
                return null;

            Log("GetNextLine: Line=" + m_index + ", " + m_lines[m_index]);
            return m_lines[m_index];
        }

        protected void PushLineBack() 
        {
            m_index--;
        }

        public abstract void Initialize();
        protected abstract void Process();

        private void Parse(SegmentDefinition header, string fileType, string fileVersion, MORNETPlusDataset ds) 
        {
            Log("*** Start Parse 000 segment. FileType = " + fileType);
            string line = GetNextLine();
            if (header.IsValid(line)) 
            {
                string _fileType = header.Get(line, "000-020");
                string _fileVersion = header.Get(line, "000-030");
                if (_fileType == fileType && _fileVersion == fileVersion) 
                {
                    while((line = GetNextLine()) != null) 
                    {
                        if (!ds.Add(line))
                            break;
                    }
                } 
            }
            PushLineBack();
            Log("*** End Parse 000 segment. FileType = " + fileType);
        }


        public abstract MORNETPlusDataset _1003 { get; }
        public abstract MORNETPlusDataset AdditionalCaseData { get; }
        public abstract MORNETPlusDataset LoanProductData { get; }
        public abstract MORNETPlusDataset GovernmentLoanData { get; }
        public abstract MORNETPlusDataset CommunityLendingData { get; }
        public abstract MORNETPlusDataset AppraisalData { get; }
        public abstract MORNETPlusDataset FloodData { get; }

        protected void Log(string msg) 
        {
            // 4/27/2005 dd - To avoid poluting PB Log, I disable this logging.

        }

        protected CBaseException ConstructError(string msg, string line) 
        {
            if (null != line) 
            {
                return new InvalidFannieFileFormatException("Unrecognized FNMA file format.", msg + " Line: [" + line + "] Length = "  + line.Length);
            } else
                return new InvalidFannieFileFormatException("Unrecognized FNMA file format.", msg + " Line is null.");
        }

    }
}
