using System;
using System.Collections;
using LendersOffice.Common;

namespace LendersOffice.Conversions.MORNETPlus
{
    public class MORNETPlusDataset 
    {
        private SegmentDefinition[] m_definitions;

        private Hashtable m_hash;

        private ArrayList m_segmentList; // Usefull in exporting only.

		private bool m_isBeingSkipped = false;

        internal MORNETPlusDataset(SegmentDefinition[] definitions) 
        {
            m_definitions = definitions;
            m_hash = new Hashtable();
            m_segmentList = new ArrayList();

        }

		public bool Skipped 
		{
			get { return m_isBeingSkipped; } 
			set { m_isBeingSkipped = true; } 
		}
		public bool IsPartOfDataSet( string line ) 
		{
			bool isPart = false;
			foreach ( SegmentDefinition def in m_definitions ) 
			{
				if ( line.StartsWith( def.ID ) )  
				{
					isPart  = true; 
					break;
				}
			}
			return isPart; 
		}

        /// <summary>
        /// Return false if line does not belong to this dataset
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool Add(string line) 
        {
            bool isValid = false;
            foreach (SegmentDefinition def in m_definitions) 
            {
                if (line.StartsWith(def.ID)) 
                {
                    if (line.Length < def.Length) 
                    {
                        // 7/13/2004 dd - Some LOS ie Virtual Lender does not export FNM conform file.
                        // Since POINT, Pipeline will forgive on minor error, I need to make the import engine to autocorrect error also.

                        line = line.PadRight(def.Length, ' ');
                    } 
                    else if (line.Length > def.Length) 
                    {
                        // 7/13/2004 dd - Some LOS ie Virtual Lender does not export FNM conform file.
                        // Since POINT, Pipeline will forgive on minor error, I need to make the import engine to autocorrect error also.
                        line = line.Substring(0, def.Length);
                    }
                    // 5/5/2004 dd - Match with definition.
                    ArrayList list = (ArrayList) m_hash[def.ID];
                    if (null == list) 
                    {
                        list = new ArrayList();
                        m_hash[def.ID] = list;
                    }
                    list.Add(new MORNETPlusSegment(line, def));
                    isValid = true;
                    break;
                }
            }
            return isValid;
        }
        #region Importing functions.
        public ArrayList GetSegment(string id) 
        {
            ArrayList ret = (ArrayList) m_hash[id];
            if (null != ret)
                return (ArrayList) m_hash[id];
            else 
                return new ArrayList();
        }
        #endregion

        #region Exporting functions. Use these function for exporting.
        public MORNETPlusSegment CreateSegment(string id) 
        {
            MORNETPlusSegment segment = null;
            foreach (SegmentDefinition def in m_definitions) 
            {
                if (def.ID == id) 
                {
                    segment = new MORNETPlusSegment(def);
                    break;
                }

            }

            return segment;
        }
        public void Add(MORNETPlusSegment segment) 
        {
            m_segmentList.Add(segment);
        }
        public ArrayList SegmentList 
        {
            get { return m_segmentList; }
        }
        #endregion


    }

}
