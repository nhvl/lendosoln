using System;
using System.Collections;
using System.Text.RegularExpressions;

using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Conversions.MORNETPlus
{
    /// <summary>
    /// Summary description for SegmentDefinition.
    /// </summary>
    public class SegmentDefinition 
    {
        private static Regex s_fieldNamePattern = new Regex(@"(\w{1,4})-(\d{2})0", RegexOptions.Compiled);

        private string m_id;
        private int m_length;
        private int m_partCount;
        private int[] m_pos;
        private ArrayList m_warningMessages = new ArrayList();

        public ArrayList WarningMessages 
        {
            get { return m_warningMessages; }
        }

        public String ID 
        {
            get { return m_id; }
        }
        public int Length 
        {
            get { return m_length; }
        }


        public int PartCount 
        {
            get { return m_partCount; }
        }
        /// <summary>
        /// args contains record layout of this datastream.
        /// args[0] = starting position of field 0
        /// args[1] = starting position of field 1
        /// args[n] = starting position of field n
        /// n &lt; args.Length - 1, last integer in args represents the record length
        /// </summary>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public SegmentDefinition(string id, params int[] args) 
        {
            m_id = id;
            m_length = args[args.Length - 1];

            m_pos = args;
            m_pos[args.Length - 1] = m_length + 1;
            m_partCount = args.Length;
        }

        public int NameToIndex(string name) 
        {
            Match m = s_fieldNamePattern.Match(name);
            if (!m.Success)
                throw new ArgumentException(name + " is not a valid Field ID. Field ID has XXX-ZZZ format.");

            
            if (m.Groups[1].Value != m_id)
                throw new GenericUserErrorMessageException( m.Groups[1].Value + " does not belong to this segment.  Segment ID = " + m_id );

            int index = 0;
            try 
            {
                index = int.Parse(m.Groups[2].Value) - 1;
            } 
            catch {}

            return index;
        }

        /// <summary>
        /// Name has following format XXX-ZZZ. XXX represents a segment record ID.
        /// The ZZZ represents the field number within that segment, starting with 010 and incrementing by 10.
        /// 
        /// Example: In segment "00A", the four fields in the segment are numbered: 00A-010, 00A-020, 00A-030, 00A-040
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isTrim"></param>
        /// <returns></returns>
        public string Get(string line, string name) 
        {
            return Get(line, name, true);
        }
        public string Get(string line, string name, bool isTrim) 
        {
            int index = NameToIndex(name);
            return Get(line, index, isTrim);
        }
        private string Get(string line, int index) 
        {
            return Get(line, index, true);
        }
        private string Get(string line, int index, bool isTrim) 
        {
            if (index < 0 || index > m_pos.Length - 1)
                throw new IndexOutOfRangeException(index + " out of range in SegmentDefinition.Get()");

            int startIndex = m_pos[index] - 1;
            int length = m_pos[index + 1] - m_pos[index];

            if (startIndex + length > line.Length)
                return "";

            string ret = line.Substring(startIndex, length);
            if (isTrim)
                ret = ret.TrimWhitespaceAndBOM();

            return ret;
        }

        public bool GetBool(string line, string name) 
        {
            string val = Get(line, name);

            return val == "Y";
        }

        public bool IsValid(string line) 
        {
            if (null == line)
                return false;

            if (line.StartsWith(m_id) && line.Length != m_length)
                Tools.LogWarning(string.Format(@"[{0}] LENGTH DOES NOT MATCH WITH FNM SPECIFICATION. EXPECTED {1} chars", line, m_length));

            // 7/13/2004 dd - Only compare the ID Segment AND NOT LENGTH. Some program might either trim or have extra character to each line.
            return line.StartsWith(m_id);
        }

        public string BuildSegment(string[] parts) 
        {
            m_warningMessages = new ArrayList();
            string str = "";
            for (int i = 0; i < parts.Length - 1; i++) 
            {
                string s = parts[i];
                if (i == 0)
                    s = m_id;
                else if (null == s)
                    s = "";
                else
                    s = s.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " "); // Don't trim 's' because numeric value in FNM is left padding with empty space.

                int length = m_pos[i + 1] - m_pos[i];
                if (s.Length > length) 
                {
                    string original = s;
                    
                    s = s.Substring(0, m_pos[i + 1] - m_pos[i]);

					// 2/8/2007 nw - OPM 5813 - Display better warning message for Fannie Export
					// m_warningMessages.Add(string.Format("[{0}] exceeding length of {1}. Truncate to [{2}].", original, length, s));
					m_warningMessages.Add(string.Format("[{0}] exceeds Fannie Mae max length of {1}. Exported value will be truncated to [{2}].", original, length, s));
                } 
                else 
                {
                    s = s.PadRight(m_pos[i + 1] - m_pos[i], ' ');
                }
                str += s;
            }

            return str;
        }
        public override string ToString()
        {
            string str = "";
            foreach (int i in m_pos)
                str += i + ", ";
            return string.Format("SegmentDefinition: {0}, {1}", m_id, str);
        }
    }
}
