namespace LendersOffice.Conversions.GenericMismoClosing26
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;
    using LendersOffice.Common.SerializationTypes;
    using Mismo.Closing2_6;
    using XmlSerializableCommon;

    /// <summary>
    /// This class will serve as starting point for generic Mismo Closing 2.6.
    /// Any specific customization for 3rd party to Mismo Closing should clone from this class.
    /// </summary>
    public class GenericMismoClosing26Exporter
    {
        /// <summary>
        /// This is used to lock the thread when generating the list of field dependencies for the export.
        /// </summary>
        private static object fieldLock = new object();

        /// <summary>
        /// The select statement provider of the field dependencies for the export, stored here so that it only needs to be loaded once.
        /// </summary>
        private static CSelectStatementProvider selectStatementProvider;

        private CPageData m_dataLoan = null;
        private CPageData m_dataLoanWithGfeArchiveApplied = null;
        private bool m_useGfeFee; //export the GFE vs the settlement 
        private bool m_includeGfe; //include the gfe data along with the settlement x_gfe has to be false 
        private bool vendorIsDocuTech = false;
        private bool vendorIsIDS = false;
        private bool vendorIsDOD = false;
        private List<string> m_feeDiscrepancies = null; // 12/17/2013 gf - opm 146376 keep list of settlement charge/GFE fee discrepancies.
        private bool use2015DataLayer = false;
        private bool exportTridData = false;
        private bool isClosingPackage = false;
        private int liabilityPayoffHudlineCounter = 1501;

        public static string Export(Guid sLId, AbstractUserPrincipal principal)
        {
            return ExportCustom(sLId, string.Empty, principal);
        }

        public static string ExportCustom(Guid sLId, string vendorName, AbstractUserPrincipal principal)
        {
            List<string> feeDiscrepancies;
            return ExportCustom(sLId, vendorName, null, out feeDiscrepancies, principal);
        }

        public static string ExportCustom(Guid sLId, string vendorName, VendorConfig.DocumentPackage package, out List<string> feeDiscrepancies, AbstractUserPrincipal principal, bool useTemporaryArchive = false)
        {
            Loan loan = GenericMismoClosing26Exporter.ExportCustomLoan(sLId, vendorName, package, out feeDiscrepancies, useTemporaryArchive, principal);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UTF8Encoding(false); // 7/13/2015 - skip BOM marker.
            settings.OmitXmlDeclaration = true;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    loan.WriteXml(writer);
                }

                byte[] bytes = stream.GetBuffer();
                return Encoding.UTF8.GetString(bytes, 0, (int)stream.Position);
            }
        }

        private static Loan ExportCustomLoan(Guid sLId, string vendorName, VendorConfig.DocumentPackage package, out List<string> feeDiscrepancies, bool useTemporaryArchive, AbstractUserPrincipal principal)
        {
            vendorName = vendorName ?? string.Empty;
            bool loadFeesFromGFEArchive = true;
            bool vendorIsComplianceEagle = false;

            if (vendorName.Equals("ComplianceEagle", StringComparison.OrdinalIgnoreCase))
            {
                loadFeesFromGFEArchive = false;
                vendorIsComplianceEagle = true;
            }

            GenericMismoClosing26Exporter exporter = new GenericMismoClosing26Exporter(sLId, loadFeesFromGFEArchive, vendorIsComplianceEagle, package, useTemporaryArchive, principal);

            if (vendorName.Contains("DocuTech"))
            {
                exporter.vendorIsDocuTech = true;
                exporter.m_HUDLineOriginatorCompensation = "895"; //OPM 145507: Permanent
            }
            else if (vendorName.Contains("IDS"))
            {
                exporter.vendorIsIDS = true;
            }
            else if (vendorName.Contains("Docs on Demand"))
            {
                exporter.vendorIsDOD = true;
            }

            Loan loan = exporter.CreateLoan();
            feeDiscrepancies = new List<string>(exporter.m_feeDiscrepancies);

            if (vendorName.Contains("DocuTech") && (loan.Application != null) && (loan.ClosingDocuments != null))
            {
                loan = LoadDocutechInfo(loan, exporter);
            }

            return loan;
        }

        private static Loan LoadDocutechInfo(Loan loan, GenericMismoClosing26Exporter exporter)
        {
            if (loan.Application.Property != null)
            {
                if (loan.Application.Property.Details != null)
                {
                    loan.Application.Property.Details.NfipCommunityIdentifier = null; //OPM 145195: Temporary
                }
            }

            //OPM 228540: for TRID non-closing package, use sTRIDLoanEstimateLenderCredits
            if (!exporter.isClosingPackage && exporter.m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                loan.Application.TransactionDetail.PurchaseCreditList.Add(exporter.CreatePurchaseCredit(exporter.m_dataLoan.sOCredit5Desc, exporter.m_dataLoan.sTRIDLoanEstimateLenderCredits_rep));
            }
            else
            {
                loan.Application.TransactionDetail.PurchaseCreditList.Add(exporter.CreatePurchaseCredit(exporter.m_dataLoan.sOCredit5Desc, exporter.m_dataLoan.sOCredit5Amt_rep));//OPM 168877: Only add for DocuTech
            }

            loan.Application.RespaFeeList.RemoveAll(respaFee => respaFee != null && respaFee.SpecifiedHudLineNumber == "894"); //OPM 171667: Temporary

            //OPM 144962: Temporary (added for DoD)
            loan.Application.BorrowerList.ForEach(borrower =>
            {
                if (borrower != null)
                {
                    borrower.SequenceIdentifier = null;
                    if (borrower.FhaVaBorrower != null)
                    {
                        borrower.FhaVaBorrower.CaivrsIdentifier = null;
                    }

                    if (borrower.VaBorrower != null)
                    {
                        borrower.VaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount = null;
                        borrower.VaBorrower.VaCoBorrowerNonTaxableIncomeAmount = null;
                    }
                }
            });

            if (loan.Application.GovernmentLoan != null && loan.Application.GovernmentLoan.FhaLoan != null)
            {
                loan.Application.GovernmentLoan.FhaLoan.Sponsor = null;
            }

            loan.ClosingDocuments.ClosingInstructionsList.ForEach(closingInstruct => { if (closingInstruct != null) closingInstruct.LeadBasedPaintCertificationRequiredIndicator = E_YNIndicator.Undefined; });
            loan.ClosingDocuments.EscrowAccountDetailList.ForEach(escrowDetail => { if (escrowDetail != null) escrowDetail.InitialEscrowDepositIncludesOtherDescription = null; });

            if (loan.ClosingDocuments.LoanDetails != null)
            {
                loan.ClosingDocuments.LoanDetails.DisbursementDate = null;

                if (loan.ClosingDocuments.LoanDetails.GfeDetail != null)
                {
                    if (!string.IsNullOrEmpty(loan.ClosingDocuments.LoanDetails.GfeDetail.GfeInterestRateAvailableThroughDate))
                    {
                        loan.ClosingDocuments.LoanDetails.GfeDetail.GfeInterestRateAvailableThroughDate = exporter.m_dataLoan.sGfeNoteIRAvailTillD_Time + " on " + loan.ClosingDocuments.LoanDetails.GfeDetail.GfeInterestRateAvailableThroughDate;
                        loan.ClosingDocuments.LoanDetails.GfeDetail.GfeLoanOriginatorFeePaymentCreditType = E_GfeDetailGfeLoanOriginatorFeePaymentCreditType.Undefined;
                        loan.ClosingDocuments.LoanDetails.GfeDetail.GfeComparisonList.RemoveAll(gfeComp => gfeComp != null &&
                            (gfeComp.Type == E_GfeComparisonType.SameLoanWithLowerSettlementCharges || gfeComp.Type == E_GfeComparisonType.SameLoanWithLowerInterestRate)
                        );
                    }
                }
            }

            foreach (var recordableDoc in loan.ClosingDocuments.RecordableDocumentList)
            {
                if (recordableDoc != null)
                {
                    recordableDoc.SecurityInstrument.VestingDescription = null;
                    recordableDoc.TrusteeList.Clear();
                }
            }

            loan.ClosingDocuments.ClosingAgentList.RemoveAll(closingAgent => closingAgent != null &&
                (closingAgent.Type == E_ClosingAgentType.Other &&
                    (closingAgent.TypeOtherDescription == FLOOD_INSURANCE_AGENT || closingAgent.TypeOtherDescription == WINDSTORM_INSURANCE_AGENT)
                )
            );

            loan.ClosingDocuments.TrustList.Clear();
            loan.ClosingDocuments.Investor = null;
            loan.ClosingDocuments.ServicerList.Clear();
            loan.ClosingDocuments.LenderBranch = null;

            return loan;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMismoClosing26Exporter"/> class.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="loadFeesFromGFEArchive">True if fees & escrow data should be loaded from the GFE archive. False if not.</param>
        /// <param name="vendorIsComplianceEagle">True if the target vendor is ComplianceEagle. False if not.</param>
        /// <param name="isLoanEstimatePackage">Indicates whether we're exporting a TRID Loan Estimate package.</param>
        /// <param name="useTemporaryArchive">Indicates whether the export should use the temporary closing cost archive. Only expected to be true for seamless.</param>
        private GenericMismoClosing26Exporter(Guid loanID, bool loadFeesFromGFEArchive, bool vendorIsComplianceEagle, VendorConfig.DocumentPackage package, bool useTemporaryArchive, AbstractUserPrincipal principal)
        {
            Initialize(loanID, loadFeesFromGFEArchive, vendorIsComplianceEagle, package, useTemporaryArchive, principal);
        }

        /// <summary>
        /// Gets the field dependencies required to load the data included in the export.
        /// </summary>
        /// <value>A hash of the fields required for the export.</value>
        public static CSelectStatementProvider SelectStatementProvider
        {
            get
            {
                lock (fieldLock)
                {
                    if (selectStatementProvider != null)
                    {
                        return selectStatementProvider;
                    }

                    IEnumerable<string> mismoFields = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(GenericMismoClosing26Exporter));
                    IEnumerable<string> gfearchiveFields = CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release;

                    HashSet<string> fields = new HashSet<string>(mismoFields);

                    if (gfearchiveFields != null)
                    {
                        fields.UnionWith(gfearchiveFields);
                    }

                    selectStatementProvider = CSelectStatementProvider.GetProviderForTargets(fields, expandTriggersIfNeeded: false);
                    return selectStatementProvider;
                }
            }
        }

        /// <summary>
        /// Gets a <see cref="CPageData"/> object with the required data dependencies.
        /// </summary>
        /// <param name="loanID">The loan identifier for the loan file.</param>
        /// <returns>A <see cref="CPageData"/> object.</returns>
        private CPageData GetPageData(Guid loanID)
        {
            return new CFullAccessPageData(loanID, nameof(GenericMismoClosing26Exporter), SelectStatementProvider);
        }

        /// <summary>
        /// Initializes the loan data objects used by the exporter.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="loadFeesFromArchive">True if fees & escrow data should be loaded from the GFE archive. False if not.</param>
        /// <param name="vendorIsComplianceEagle">True if the target vendor is ComplianceEagle. False if not.</param>
        /// <param name="package">The document package. Will be null if the caller is not a doc vendor.</param>
        /// <param name="useTemporaryArchive">Indicates whether the export should use the temporary closing cost archive. Only expected to be true for seamless.</param>
        private void Initialize(Guid loanID, bool loadFeesFromGFEArchive, bool vendorIsComplianceEagle, VendorConfig.DocumentPackage package, bool useTemporaryArchive, AbstractUserPrincipal principal)
        {
            m_dataLoan = GetPageData(loanID);
            m_dataLoan.ByPassFieldSecurityCheck = true; // opm 147459
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            m_dataLoanWithGfeArchiveApplied = GetPageData(loanID);
            m_dataLoanWithGfeArchiveApplied.ByPassFieldSecurityCheck = true;  // opm 147459
            m_dataLoanWithGfeArchiveApplied.InitLoad();

            this.isClosingPackage = package == null || package.IsOfType(VendorConfig.PackageType.Closing) || package.IsUndefinedType;

            exportTridData = m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            use2015DataLayer = m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            if (m_dataLoanWithGfeArchiveApplied.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (use2015DataLayer)
                {
                    if (useTemporaryArchive)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyTemporaryArchive(principal);
                    }
                    else if (!this.isClosingPackage && m_dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive != null)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyClosingCostArchive(m_dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive);
                    }
                }
                else if (loadFeesFromGFEArchive && m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive != null)
                {
                    m_dataLoanWithGfeArchiveApplied.ApplyGFEArchiveForExport(m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive);
                }
            }

            m_dataLoanWithGfeArchiveApplied.SetFormatTarget(FormatTarget.MismoClosing);

            m_useGfeFee = use2015DataLayer ? true : m_dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.GFE;
            // 9/10/2013 AV - 137986 [Document Framework] GFE Data not being included in export?
            m_includeGfe = use2015DataLayer ? false : !m_useGfeFee;

            m_feeDiscrepancies = new List<string>();
        }

        public Loan CreateMismoLoan()
        {
            Loan loan = CreateLoan();
            return loan;
        }

        private Loan CreateLoan()
        {
            Loan loan = new Loan();
            loan.MismoVersionIdentifier = E_LoanMismoVersionIdentifier._2_6;
            loan.Application = CreateApplication();
            loan.ClosingDocuments = CreateClosingDocuments();

            return loan;
        }

        private string m_HUDLineOriginatorCompensation = HUDLines.LOAN_ORIGINATION_FEE;
        private Application CreateApplication()
        {
            Application application = new Application();
            application.AdditionalCaseData = CreateAdditionalCaseData();
            application.InterviewerInformation = CreateInterviewerInformation();
            application.LoanProductData = CreateLoanProductData();
            application.MortgageTerms = CreateMortgageTerms();
            application.Property = CreateProperty();
            application.TransactionDetail = CreateTransactionDetail();
            application.UrlaTotalList.Add(CreateUrlaTotal());

            int nApps = m_dataLoan.nApps;
            for (int borrowerIndex = 0; borrowerIndex < nApps; borrowerIndex++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(borrowerIndex);

                if (!dataApp.aBIsValidNameSsn && !dataApp.aCIsValidNameSsn)
                    continue; // Skip if there is no borrower & no coborrower.

                PopulateAssetList(application.AssetList, dataApp);
                PopulateLiabilityList(application.LiabilityList, dataApp, borrowerIndex);
                PopulateReoPropertyList(application.ReoPropertyList, dataApp, borrowerIndex);

                string sequenceIdentifier;
                if (dataApp.aBIsValidNameSsn)
                {
                    sequenceIdentifier = (borrowerIndex + 1).ToString() + "A";
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    Borrower borr = CreateBorrower(dataApp, sequenceIdentifier);

                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift, Amount = dataApp.aAsstLiqTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalNonLiquidAssets, Amount = dataApp.aAsstNonReSolidTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalLiabilitesMonthlyPayment, Amount = dataApp.aLiaMonTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.TotalLiabilitiesBalance, Amount = dataApp.aLiaBalTot_rep });
                    if (m_dataLoan.sApp1003InterviewerPrepareDate.IsValid)
                    {
                        borr.ApplicationSignedDate = m_dataLoan.sApp1003InterviewerPrepareDate_rep;
                    }

                    application.BorrowerList.Add(borr);
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    sequenceIdentifier = (borrowerIndex + 1).ToString() + "B";
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    application.BorrowerList.Add(CreateBorrower(dataApp, sequenceIdentifier));
                }
            }

            if (nApps > 0)
            {
                application.GovernmentLoan = CreateGovernmentLoan(m_dataLoan.GetAppData(0));
                application.LoanPurpose = CreateLoanPurpose(m_dataLoan.GetAppData(0));
            }

            int tIndex = 0;
            foreach (TitleBorrower borrower in m_dataLoan.sTitleBorrowers)
            {
                if (borrower.FullName.TrimWhitespaceAndBOM().Length > 0 || borrower.SSN.Length > 0)
                {
                    string sequenceIdentifier = String.Concat(m_dataLoan.nApps + tIndex) + "T";
                    application.BorrowerList.Add(CreateBorrower_TitleOnly(sequenceIdentifier, borrower, tIndex));
                    tIndex += 1;
                }
            }

            application.GovernmentReporting = CreateGovernmentReporting();
            application.LoanQualification = CreateLoanQualification();
            application.DownPaymentList.Add(CreateDownPayment());
            application.EscrowAccountSummary = CreateEscrowAccountSummary();

            if (use2015DataLayer)
            {
                PopulateRespaFeeList_2015DataLayer(application.RespaFeeList);
                PopulateEscrowList_2015DataLayer(application.EscrowList);
                PopulateProposedHousingExpenseList_2015DataLayer(application.ProposedHousingExpenseList);
            }
            else
            {
                PopulateRespaFeeList(application.RespaFeeList);
                PopulateEscrowList(application.EscrowList);
                PopulateProposedHousingExpenseList(application.ProposedHousingExpenseList);
            }

            if (m_dataLoan.sLT != E_sLT.VA)
            {
                application.MiData = CreateMiData();
            }
            //application.Id = null;
            //application.DataInformation = CreateDataInformation();
            application.AffordableLending = CreateAffordableLending(); // 8/12/2014 BB - This is needed for ComplianceEagle since it's the only location of the MSA number for the property. However, this block is also specific to MyCommunity / Home Possible product type. If it breaks another vendor then we'll revert this and add the MSA number to the CEagle data wrapper instead.

            if (!string.IsNullOrEmpty(m_dataLoan.sMersMin))
            {
                application.Mers = CreateMers();
            }

            PopulateLoanOriginatorList(application.LoanOriginatorList);
            //application.TitleHolderList.Add(CreateTitleHolder());
            //application.InvestorFeatureList.Add(CreateInvestorFeature());
            //application.LoanOriginationSystem = CreateLoanOriginationSystem();
            application.LoanUnderwritingList.Add(CreateLoanUnderwriting(this.m_dataLoan.GetAppData(0).aVaRatio_rep));

            if (ToMismo(this.m_dataLoan.sLPurposeT) == E_LoanPurposeType.Refinance)
            {
                application.RelatedLoanList.Add(this.CreateRelatedLoan(E_RelatedLoanRelatedLoanRelationshipType.RefinancedBySubjectLoan));
            }

            return application;
        }

        /// <summary>
        /// Populates the proposed housing expense list with TRID data.
        /// </summary>
        /// <param name="proposedHousingExpenseList">The housing expense list to populate.</param>
        private void PopulateProposedHousingExpenseList_2015DataLayer(List<ProposedHousingExpense> proposedHousingExpenseList)
        {

            // 8/14/2015 bs - Add P&I Mortgage Payment and MI to proposed housing expense. OPM 219295 

            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, m_dataLoan.sProFirstMPmt_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.MI, m_dataLoan.sProMIns_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, m_dataLoan.sProSecondMPmt_rep));

            if (this.vendorIsIDS)
            {
                var combinedHousingExpenseList = m_dataLoanWithGfeArchiveApplied.sHousingExpenses.ExpensesToUse;
                IEnumerable<IGrouping<E_ProposedHousingExpenseHousingExpenseType, BaseHousingExpense>> groups = combinedHousingExpenseList.GroupBy(expense => Convert(expense.HousingExpenseType));
                foreach (IGrouping<E_ProposedHousingExpenseHousingExpenseType, BaseHousingExpense> group in groups)
                {
                    string monthlyAmt_rep = this.m_dataLoan.m_convertLos.ToMoneyStringRoundAwayFromZero(group.Sum(expense => expense.MonthlyAmtTotal), FormatDirection.ToRep);
                    proposedHousingExpenseList.Add(CreateProposedHousingExpense(group.Key, monthlyAmt_rep));
                }
            }
            else
            {
                foreach (var housingExpense in m_dataLoanWithGfeArchiveApplied.sHousingExpenses.ExpensesToUse)
                {
                    proposedHousingExpenseList.Add(CreateProposedHousingExpense_2015DataLayer(housingExpense));
                }
            }
        }

        /// <summary>
        /// Populates the proposed housing expense list with data.
        /// </summary>
        /// <param name="proposedHousingExpenseList">The housing expense list to populate.</param>
        private void PopulateProposedHousingExpenseList(List<ProposedHousingExpense> proposedHousingExpenseList)
        {
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.MI, m_dataLoan.sProMIns_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HazardInsurance, m_dataLoan.sProHazIns_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, m_dataLoan.sProSecondMPmt_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense, m_dataLoan.sProOHExp_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.RealEstateTax, m_dataLoan.sProRealETx_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, m_dataLoan.sProHoAssocDues_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, m_dataLoan.sProFirstMPmt_rep));
        }

        private void PopulateRespaFeeList(List<RespaFee> respaFeeList)
        {
            // 801 Loan Origination Fee

            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.LoanOriginationFee, HUDLines.LOAN_ORIGINATION_FEE, "", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sLOrigF_rep : m_dataLoan.sSettlementLOrigF_rep,
                m_useGfeFee ? m_dataLoan.sLOrigFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sLOrigFProps, m_dataLoan.sSettlementLOrigFProps),
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sLOrigFPc_rep : m_dataLoan.sSettlementLOrigFPc_rep,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sLOrigFMb_rep : m_dataLoan.sSettlementLOrigFMb_rep,
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sLOrigF_rep : null));

            //// 801 Broker Commission
            //respaFeeList.Add(
            //    CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
            //    E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.LoanOriginationFee, "801", "", false,
            //    mxGfe ? m_dataLoan.sGfeBrokerComp_rep : m_dataLoan.sSettlementBrokerComp_rep,
            //    mxGfe ? m_dataLoan.sLOrigFProps : LosConvert.GfeItemProps_CopyAprFrom(m_dataLoan.sLOrigFProps, m_dataLoan.sSettlementLOrigFProps),
            //    miGfe ? m_dataLoan.sGfeBrokerComp_rep : null, ""));

            // 801 Originator Compensation
            string brokerCompanyName = m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName
                : m_dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName;

            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.LoanOriginationFee, m_HUDLineOriginatorCompensation,
                string.IsNullOrEmpty(brokerCompanyName) ? "Originator Compensation" : "Originator Compensation paid to " + brokerCompanyName, false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep : m_dataLoan.sGfeOriginatorCompF_rep,
                m_useGfeFee ? m_dataLoan.sGfeOriginatorCompFProps : m_dataLoan.sGfeOriginatorCompFProps,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFPc_rep : m_dataLoan.sGfeOriginatorCompFPc_rep,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFMb_rep : m_dataLoan.sGfeOriginatorCompFMb_rep,
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep : null));
            #region 802 Credit or Charge
            // 802 Discount Points
            respaFeeList.Add(CreateRespaFee(
                E_RespaFeeRespaSectionClassificationType._800_LoanFees, // SectionClassificationType
                E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, // AggregationType
                E_RespaFeeType.LoanDiscountPoints, // FeeType
                "802", // hudLineNumber
                "Discount Point", // description
                false, // isPaid
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep : m_dataLoan.sSettlementDiscountPointF_rep, // amount
                m_useGfeFee ? m_dataLoan.sGfeDiscountPointFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sGfeDiscountPointFProps, m_dataLoan.sSettlementDiscountPointFProps), // properties containing Paid by, APR, FHA allowable, etc.
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointFPc_rep : m_dataLoan.sSettlementDiscountPointFPc_rep, // point
                null, // fixed amount
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep : null // gfeDisclosedAmount
            )); // If it's null, it will add a null entry
            // 894 (802)
            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemF_rep) < 0 ||
                m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sSettlementCreditLenderPaidItemF_rep) < 0) //OPM 150540
            {
                string desc;
                E_CreditLenderPaidItemT CreditLenderPaidItemT = m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemT : m_dataLoan.sSettlementCreditLenderPaidItemT;
                switch (CreditLenderPaidItemT)
                {
                    case E_CreditLenderPaidItemT.OriginatorCompensationOnly:
                        desc = "Credit for Originator Compensation paid by lender";
                        break;
                    case E_CreditLenderPaidItemT.AllLenderPaidItems:
                        desc = "Credit for lender paid fees";
                        break;
                    default:
                        desc = "Origination Credit";
                        break;
                }
                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                            E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal, "894", desc, false,
                            m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemF_rep : m_dataLoan.sSettlementCreditLenderPaidItemF_rep,
                            LosConvert.GfeItemProps_Pack(false, false, LosConvert.BORR_PAID_OUTOFPOCKET, false, false), //Sets all props to "N" and _PaidByType="Buyer"
                            null, //_TotalPercent
                            null, //_SpecifiedFixedAmount
                            m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemF_rep : null)); //_GFEDisclosedAmount
            }
            // 896 (802) General Lender Credit
            if (m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditF < 0 || m_dataLoan.sSettlementLenderCreditF < 0) //OPM 145507
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                            E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal, "896", "General Lender Credit", false,
                            m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditF_rep : m_dataLoan.sSettlementLenderCreditF_rep,
                            m_useGfeFee ? m_dataLoan.sGfeLenderCreditFProps : m_dataLoan.sGfeLenderCreditFProps,
                            m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditFPc_rep : m_dataLoan.sSettlementLenderCreditFPc_rep,
                            null,
                            m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditF_rep : null));
            }
            #endregion 802 Credit or Charge
            // 804 Appraisal fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, E_RespaFeeType.AppraisalFee, HUDLines.APPRAISAL_FEE, "Appraisal fee",
                m_useGfeFee ? m_dataLoan.sApprFPaid : m_dataLoan.sSettlementApprFPaid,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sApprF_rep : m_dataLoan.sSettlementApprF_rep,
                m_useGfeFee ? m_dataLoan.sApprFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sApprFProps, m_dataLoan.sSettlementApprFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sApprF_rep : null, m_dataLoan.sApprFPaidTo));

            // 805 Credit report
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, E_RespaFeeType.CreditReportFee, HUDLines.CREDIT_REPORT_FEE, "Credit report",
                m_useGfeFee ? m_dataLoan.sCrFPaid : m_dataLoan.sSettlementCrFPaid,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sCrF_rep : m_dataLoan.sSettlementCrF_rep,
                m_useGfeFee ? m_dataLoan.sCrFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCrFProps, m_dataLoan.sSettlementCrFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sCrF_rep : null, m_dataLoan.sCrFPaidTo));

            // 806 Tax service fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, E_RespaFeeType.TaxRelatedServiceFee, HUDLines.TAX_SERVICE_FEE, "Tax service fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sTxServF_rep : m_dataLoan.sSettlementTxServF_rep,
                m_useGfeFee ? m_dataLoan.sTxServFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTxServFProps, m_dataLoan.sSettlementTxServFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sTxServF_rep : null, m_dataLoan.sTxServFPaidTo));


            // 807 Flood Certification
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, E_RespaFeeType.FloodCertification, "807", "Flood Certification", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sFloodCertificationF_rep : m_dataLoan.sSettlementFloodCertificationF_rep,
                m_useGfeFee ? m_dataLoan.sFloodCertificationFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sFloodCertificationFProps, m_dataLoan.sSettlementFloodCertificationFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sFloodCertificationF_rep : null, m_dataLoan.sFloodCertificationFPaidTo));

            // 808 Mortgage broker fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.MortgageBrokerFee, HUDLines.MORTGAGE_BROKER_FEE, "Mortgage broker fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sMBrokF_rep : m_dataLoan.sSettlementMBrokF_rep,
                m_useGfeFee ? m_dataLoan.sMBrokFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sMBrokFProps, m_dataLoan.sSettlementMBrokFProps),
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sMBrokFPc_rep : m_dataLoan.sSettlementMBrokFPc_rep,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sMBrokFMb_rep : m_dataLoan.sSettlementMBrokFMb_rep,
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sMBrokF_rep : null));

            // 809 Lender's inspection fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.InspectionFee, HUDLines.LENDERS_INSPECTION_FEE, "Lender's inspection fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sInspectF_rep : m_dataLoan.sSettlementInspectF_rep,
                m_useGfeFee ? m_dataLoan.sInspectFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sInspectFProps, m_dataLoan.sSettlementInspectFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sInspectF_rep : null, m_dataLoan.sInspectFPaidTo));

            // 810 Processing fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.ProcessingFee, "810", "Processing fee",
                m_useGfeFee ? m_dataLoan.sProcFPaid : m_dataLoan.sSettlementProcFPaid,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sProcF_rep : m_dataLoan.sSettlementProcF_rep,
                m_useGfeFee ? m_dataLoan.sProcFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sProcFProps, m_dataLoan.sSettlementProcFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sProcF_rep : null, m_dataLoan.sProcFPaidTo));

            // 811 Underwriting fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.UnderwritingFee, "811", "Underwriting fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sUwF_rep : m_dataLoan.sSettlementUwF_rep,
                m_useGfeFee ? m_dataLoan.sUwFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sUwFProps, m_dataLoan.sSettlementUwFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sUwF_rep : null, m_dataLoan.sUwFPaidTo));

            // 812 Wire transfer
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.WireTransferFee, "812", "Wire transfer", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sWireF_rep : m_dataLoan.sSettlementWireF_rep,
                m_useGfeFee ? m_dataLoan.sWireFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sWireFProps, m_dataLoan.sSettlementWireFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sWireF_rep : null, m_dataLoan.sWireFPaidTo));

            // 813 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("813", m_useGfeFee));

            // 814 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("814", m_useGfeFee));

            // 815 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("815", m_useGfeFee));

            // 816 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("816", m_useGfeFee));

            // 817 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("817", m_useGfeFee));

            respaFeeList.Add(CreateRespaFee(
                E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                ToMismo(E_GfeSectionT.B10),
                E_RespaFeeType.Other,
                "901",
                "Prepaid Interest",
                false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sIPia_rep : m_dataLoan.sSettlementIPia_rep,
                m_useGfeFee ? m_dataLoan.sIPiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sIPiaProps, m_dataLoan.sSettlementIPiaProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sIPia_rep : null,
                ""));

            // 902 is pass in MI DATA
            //respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, E_RespaFeeType.Other, 
            //    "902", "UPFRONT MIP", false, m_dataLoan.sMipPia_rep, m_dataLoan.sMipPiaProps));
            RespaFee mi902Fee = new RespaFee()
            {
                TotalAmount = m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep : m_dataLoan.sFfUfmip1003_rep,
                GfeDisclosedAmount = m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep : null
            };

            mi902Fee.Type = GetRespaFeeType_mi902Fee();

            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance
                , E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected
                , mi902Fee.Type,
                "902", "Mortgage Insurance Premium", false,
                    mi902Fee.TotalAmount,
                    m_dataLoan.sLT == E_sLT.VA ? m_dataLoan.sVaFfProps : m_dataLoan.sMipPiaProps,
                    mi902Fee.GfeDisclosedAmount,
                    m_dataLoan.sLT == E_sLT.VA ? m_dataLoan.sVaFfPaidTo : m_dataLoan.sMipPiaPaidTo));

            //This stuff needs to be refactored
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, E_RespaFeeGfeAggregationType.None, E_RespaFeeType.Other,
                "903", "Hazard Insurance Premium", false,
                    m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sHazInsPia_rep : m_dataLoan.sSettlementHazInsPia_rep,
                    m_useGfeFee ? m_dataLoan.sHazInsPiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sHazInsPiaProps, m_dataLoan.sSettlementHazInsPiaProps),
                    m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsPia_rep : null,
                    m_dataLoan.sHazInsPiaPaidTo));

            respaFeeList.Add(GetRespaFeeForHudLine("904", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("906", m_useGfeFee));

            // 1102 Closing / Escrow Fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                ToMismo(m_dataLoanWithGfeArchiveApplied.sEscrowFGfeSection), E_RespaFeeType.SettlementOrClosingFee, HUDLines.CLOSING_ESCROW_FEE, "Closing/Escrow Fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sEscrowF_rep : m_dataLoan.sSettlementEscrowF_rep,
                m_useGfeFee ? m_dataLoan.sEscrowFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sEscrowFProps, m_dataLoan.sSettlementEscrowFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sEscrowF_rep : null, m_dataLoan.sEscrowFTable));

            // 1103 Owners Title Insurance
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                E_RespaFeeGfeAggregationType.OwnersTitleInsurance, E_RespaFeeType.Other, "1103", "Owners Title Insurance", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sOwnerTitleInsF_rep : m_dataLoan.sSettlementOwnerTitleInsF_rep,
                m_useGfeFee ? m_dataLoan.sOwnerTitleInsProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sOwnerTitleInsProps, m_dataLoan.sSettlementOwnerTitleInsFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sOwnerTitleInsF_rep : null, m_dataLoan.sOwnerTitleInsPaidTo));

            // 1104 Lenders Title Insurance
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                E_RespaFeeGfeAggregationType.TitleServices, E_RespaFeeType.Other, "1104", "Lenders Title Insurance", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep : m_dataLoan.sSettlementTitleInsF_rep,
                m_useGfeFee ? m_dataLoan.sTitleInsFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTitleInsFProps, m_dataLoan.sSettlementTitleInsFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep : null, m_dataLoan.sTitleInsFTable));

            // 1109 Doc preparation fee
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                ToMismo(m_dataLoanWithGfeArchiveApplied.sDocPrepFGfeSection), E_RespaFeeType.DocumentPreparationFee, HUDLines.DOC_PREP_FEE, "Doc preparation fee", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep : m_dataLoan.sSettlementDocPrepF_rep,
                m_useGfeFee ? m_dataLoan.sDocPrepFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sDocPrepFProps, m_dataLoan.sSettlementDocPrepFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep : null, m_dataLoan.sDocPrepFPaidTo));

            // 1110 Notary fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                ToMismo(m_dataLoanWithGfeArchiveApplied.sNotaryFGfeSection), E_RespaFeeType.NotaryFee, HUDLines.NOTARY_FEES, "Notary fees", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sNotaryF_rep : m_dataLoan.sSettlementNotaryF_rep,
                m_useGfeFee ? m_dataLoan.sNotaryFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sNotaryFProps, m_dataLoan.sSettlementNotaryFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sNotaryF_rep : null, m_dataLoan.sNotaryFPaidTo));

            // 1111 Attorney fees
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                ToMismo(m_dataLoanWithGfeArchiveApplied.sAttorneyFGfeSection), E_RespaFeeType.AttorneyFee, HUDLines.ATTORNEY_FEES, "Attorney fees", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep : m_dataLoan.sSettlementAttorneyF_rep,
                m_useGfeFee ? m_dataLoan.sAttorneyFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sAttorneyFProps, m_dataLoan.sSettlementAttorneyFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep : null, m_dataLoan.sAttorneyFPaidTo));

            //1112 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("1112", m_useGfeFee));

            //1113 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("1113", m_useGfeFee));

            //1114 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("1114", m_useGfeFee));

            //1115 - Others
            respaFeeList.Add(GetRespaFeeForHudLine("1115", m_useGfeFee));

            // respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, E_RespaFeeType.Other,"1201", "", false, m_dataLoanWithGfeArchiveApplied.sRecF_rep, m_dataLoan.sRecFProps));
            // 1201/1202 Recording Fee

            bool use1201 = m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRecFLckd : m_dataLoan.sSettlementRecFLckd;
            if (use1201)
            {
                RespaFee fee = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                    E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.DeedRecordingFee, "1202", "Recording Fee", false,
                    m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRecF_rep : m_dataLoan.sSettlementRecF_rep,
                    m_useGfeFee ? m_dataLoan.sRecFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                    m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sRecF_rep : null, m_dataLoan.sRecFDesc);

                respaFeeList.Add(fee);
            }
            else
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                   E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.DeedRecordingFee, "1202", "Deed Recording Fee", false,
                   m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRecDeed_rep : m_dataLoan.sSettlementRecDeed_rep,
                   m_useGfeFee ? m_dataLoan.sRecFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                   m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sRecDeed_rep : null, m_dataLoan.sRecFDesc));

                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                    E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.MortgageRecordingFee, "1202", "Mortgage Recording Fee", false,
                    m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep : m_dataLoan.sSettlementRecMortgage_rep,
                    m_useGfeFee ? m_dataLoan.sRecFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                    m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep : null, m_dataLoan.sRecFDesc));

                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                     E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.ReleaseRecordingFee, "1202", "Release Recording Fee", false,
                     m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRecRelease_rep : m_dataLoan.sSettlementRecRelease_rep,
                     m_useGfeFee ? m_dataLoan.sRecFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                     m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sRecRelease_rep : null, m_dataLoan.sRecFDesc));
            }

            // 1204 City/County tax stamps
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                E_RespaFeeGfeAggregationType.TransferTaxes, E_RespaFeeType.CityCountyMortgageTaxStampFee,
                HUDLines.CITY_COUNTY_TAX_STAMPS, "City/County tax stamps", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep : m_dataLoan.sSettlementCountyRtc_rep,
                m_useGfeFee ? m_dataLoan.sCountyRtcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCountyRtcProps, m_dataLoan.sSettlementCountyRtcProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep : null, m_dataLoan.sCountyRtcDesc));

            // 1205 State tax/stamps
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                E_RespaFeeGfeAggregationType.TransferTaxes, E_RespaFeeType.StateMortgageTaxStampFee,
                HUDLines.STATE_TAX_STAMPS, "State tax/stamps", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sStateRtc_rep : m_dataLoan.sSettlementStateRtc_rep,
                m_useGfeFee ? m_dataLoan.sStateRtcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sStateRtcProps, m_dataLoan.sSettlementStateRtcProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sStateRtc_rep : null, m_dataLoan.sStateRtcDesc));

            respaFeeList.Add(GetRespaFeeForHudLine("1206", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1207", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1208", m_useGfeFee));

            // 1302 Pest Inspection
            respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, E_RespaFeeType.PestInspectionFee,
                "1302", "Pest Inspection", false,
                m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep : m_dataLoan.sSettlementPestInspectF_rep,
                m_useGfeFee ? m_dataLoan.sPestInspectFProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sPestInspectFProps, m_dataLoan.sSettlementPestInspectFProps),
                m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep : null, m_dataLoan.sPestInspectPaidTo));

            respaFeeList.Add(GetRespaFeeForHudLine("1303", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1304", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1305", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1306", m_useGfeFee));

            respaFeeList.Add(GetRespaFeeForHudLine("1307", m_useGfeFee));

            #region Seller Charges
            if (m_dataLoan.sSellerSettlementChargesU01F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU01FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU01FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU01F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU02F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU02FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU02FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU02F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU03F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU03FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU03FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU03F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU04F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU04FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU04FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU04F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU05F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU05FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU05FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU05F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU06F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU06FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU06FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU06F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU07F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU07FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU07FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU07F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU08F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU08FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU08FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU08F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU09F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU09FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU09FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU09F_rep));
            }
            if (m_dataLoan.sSellerSettlementChargesU10F_rep != "0.00")
            {
                respaFeeList.Add(
                    CreateRespaFeeFromSellerCharges(m_dataLoan.sSellerSettlementChargesU10FHudline_rep,
                                                        m_dataLoan.sSellerSettlementChargesU10FDesc,
                                                        m_dataLoan.sSellerSettlementChargesU10F_rep));
            }
            #endregion
        }

        /// <summary>
        /// Exports the Integrated Disclosure closing costs in lieu of RESPA fees.
        /// </summary>
        /// <param name="respaFeeList">The list of Respa Fees to populate.</param>
        private void PopulateRespaFeeList_2015DataLayer(List<RespaFee> respaFeeList)

        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoanWithGfeArchiveApplied.sClosingCostSet,
                                                                                                                        true,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sDisclosureRegulationT,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sGfeIsTPOTransaction);

            AddBorrowerClosingCostFees(respaFeeList, m_dataLoanWithGfeArchiveApplied.sClosingCostSet.GetFees(borrowerSetFilter));
            AddSellerClosingCostFees(respaFeeList, m_dataLoanWithGfeArchiveApplied.sSellerResponsibleClosingCostSet.GetFees((BaseClosingCostFee b) => true));

            // per OPM 227316: Lender Credits are not closing costs, but still need to be exported as RESPA fees. Taken from existing PopulateRespaFeeList code.
            // Only checking GFE values, not settlement b/c data should be merged during migration process.
            // 894 (802)
            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemF_rep) < 0) //OPM 150540
            {
                string desc;
                E_CreditLenderPaidItemT CreditLenderPaidItemT = m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemT;
                switch (CreditLenderPaidItemT)
                {
                    case E_CreditLenderPaidItemT.OriginatorCompensationOnly:
                        desc = "Credit for Originator Compensation paid by lender";
                        break;
                    case E_CreditLenderPaidItemT.AllLenderPaidItems:
                        desc = "Credit for lender paid fees";
                        break;
                    default:
                        desc = "Origination Credit";
                        break;
                }
                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                            E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal, "894", desc, false,
                            m_dataLoanWithGfeArchiveApplied.sGfeCreditLenderPaidItemF_rep,
                            LosConvert.GfeItemProps_Pack(false, false, LosConvert.BORR_PAID_OUTOFPOCKET, false, false), //Sets all props to "N" and _PaidByType="Buyer"
                            null, //_TotalPercent
                            null, //_SpecifiedFixedAmount
                            null)); //_GFEDisclosedAmount
            }

            if (vendorIsDocuTech && m_dataLoan.sGfeOriginatorCompF > 0)
            {
                string brokerCompanyName = m_dataLoanWithGfeArchiveApplied.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName;

                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                    E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.LoanOriginationFee, "895",
                    string.IsNullOrEmpty(brokerCompanyName) ? "Originator Compensation" : "Originator Compensation paid to " + brokerCompanyName, false,
                    m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep,
                    m_dataLoan.sGfeOriginatorCompFProps,
                    m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFPc_rep,
                    m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFMb_rep,
                    null));
            }

            // General Lender Credit
            if (m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditF < 0) //OPM 145507
            {
                respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                            E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal, "896", "General Lender Credit", false,
                            m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditF_rep,
                            m_dataLoan.sGfeLenderCreditFProps,
                            m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditFPc_rep,
                            null,
                            null));
            }
        }

        /// <summary>
        /// Adds Borrower-responsible closing costs to the given RESPA fee list. Borrower-specific modifications should go in here.
        /// </summary>
        /// <param name="respaFeeList">The list to which the fees will be added.</param>
        /// <param name="closingCostSet">The closing cost set from which the fees are taken.</param>
        private void AddBorrowerClosingCostFees(List<RespaFee> respaFeeList, IEnumerable<BaseClosingCostFee> closingCostSet)
        {
            foreach (BorrowerClosingCostFee closingCost in closingCostSet)
            {
                RespaFee fee = CreateRespaFee_2015DataLayer_Common(closingCost);

                if (fee != null)
                {
                    // Put borrower-specific modifications to the fee here
                    SetLastDisclosedAmount_Borrower(closingCost, fee);
                }

                respaFeeList.Add(fee);
            }
        }

        /// <summary>
        /// Adds a last-disclosed amount to a RESPA fee, from the given closing cost.
        /// </summary>
        /// <param name="closingCost">The LQB closing cost item.</param>
        /// <param name="fee">The <see cref="RespaFee"/> to which the last disclosed amount is to be added.</param>
        private void SetLastDisclosedAmount_Borrower(BorrowerClosingCostFee closingCost, RespaFee fee)
        {
            if (this.isClosingPackage)
            {
                // We're using live data and need to pull the GfeDisclosedAmount value from the last archive.
                ClosingCostArchive lastDisclosedArchive = m_dataLoan.sLastDisclosedClosingCostArchive;
                    
                if (lastDisclosedArchive != null)
                {
                    // This filter is used with a ClosingCostSet made from an archive AND it doesn't want to include the Orig Comp Fee.
                    Func<BaseClosingCostFee, bool> archivedFeeFilter = feeToFilter => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)feeToFilter) &&
                                                                                      feeToFilter.ClosingCostFeeTypeId == closingCost.ClosingCostFeeTypeId;
                    BorrowerClosingCostFee archivedFee = (BorrowerClosingCostFee)lastDisclosedArchive.GetClosingCostSet().GetFees(archivedFeeFilter).FirstOrDefault();
                    if (archivedFee != null)
                    {
                        fee.GfeDisclosedAmount = m_dataLoan.m_convertLos.ToDecimalString(archivedFee.TotalAmount, FormatDirection.ToRep);
                    }
                }

                fee.TotalAmount = closingCost.TotalAmount_rep;
            }
            else
            {
                // We're using archived data and need to pull the TotalAmount value from the live loan object.
                fee.GfeDisclosedAmount = closingCost.TotalAmount_rep;
                Func<BaseClosingCostFee, bool> liveFeeFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoan.sClosingCostSet,
                                                                                                                        false,
                                                                                                                        m_dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoan.sDisclosureRegulationT,
                                                                                                                        m_dataLoan.sGfeIsTPOTransaction);

                Func<BaseClosingCostFee, bool> trueFilter = feeToFilter => liveFeeFilter(feeToFilter) && feeToFilter.ClosingCostFeeTypeId == closingCost.ClosingCostFeeTypeId;

                BorrowerClosingCostFee liveFee = (BorrowerClosingCostFee)m_dataLoan.sClosingCostSet.GetFees(trueFilter).FirstOrDefault();
                fee.TotalAmount = liveFee != null ? liveFee.TotalAmount_rep : closingCost.TotalAmount_rep;
            }
        }

        /// <summary>
        /// Adds Seller-responsible closing costs to the given RESPA fee list. Seller-specific modifications should go in here.
        /// </summary>
        /// <param name="respaFeeList">The list to which the fees will be added.</param>
        /// <param name="closingCostSet">The closing cost set from which the fees are taken.</param>
        private void AddSellerClosingCostFees(List<RespaFee> respaFeeList, IEnumerable<BaseClosingCostFee> closingCostSet)
        {
            // Add seller fees only to the Closing package
            if (this.isClosingPackage)
            {
                foreach (SellerClosingCostFee closingCost in closingCostSet)
                {
                    RespaFee fee = CreateRespaFee_2015DataLayer_Common(closingCost);
                    // Put seller-specific modifications to the fee here
                    respaFeeList.Add(fee);
                }
            }
        }

        /// <summary>
        /// Helper function for the CreateRespaFee_2015DataLayer methods which does the common work for both types of LoanClosingCostFees.
        /// </summary>
        /// <param name="closingCost">The closing cost to export.</param>
        /// <returns>A <see cref="RespaFee" /> object.</returns>
        private RespaFee CreateRespaFee_2015DataLayer_Common(LoanClosingCostFee closingCost)
        {
            if (vendorIsDocuTech && closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
            {
                return null;
            }

            var fee = new RespaFee();
            fee.RespaSectionClassificationType = GetRespaSectionClassificationType(closingCost.HudLine_rep);
            fee.Type = ToMismo(closingCost.MismoFeeT);
            fee.GfeAggregationType = Convert(closingCost.GfeSectionT);
            fee.SpecifiedHudLineNumber = closingCost.HudLine_rep == "905" ? "902" : closingCost.HudLine_rep;
            fee.BorrowerChosenProviderIndicator = closingCost.DidShop ? E_YNIndicator.Y : E_YNIndicator.N;
            fee.PaidToType = ToMismo_2015DataLayerFeePaidTo(closingCost.Beneficiary);
            fee.PaidToTypeOtherDescription = fee.PaidToType == E_RespaFeePaidToType.Other ? closingCost.Beneficiary.ToString() : null;
            fee.PaidTo = CreatePaidTo(closingCost.BeneficiaryDescription);
            fee.TotalPercent = GetTotalPercent(closingCost);

            if (exportTridData)
            {
                fee.Id = "_" + closingCost.UniqueId.ToString();
                fee.ResponsiblePartyType = ToMismo(closingCost.GfeResponsiblePartyT);
                fee.PercentBasisType = ToMismo_2015DataLayer(closingCost.PercentBaseT);
                fee.RequiredProviderOfServiceIndicator = closingCost.CanShop ? E_YNIndicator.N : E_YNIndicator.Y;

                if (closingCost.IsTitleFee)
                {
                    closingCost.Description = "TITLE - " + closingCost.Description;
                }

                if (closingCost.IsOptional)
                {
                    closingCost.Description += " (optional)";
                }
            }

            fee.TypeOtherDescription = closingCost.Description;

            foreach (LoanClosingCostFeePayment payment in closingCost.Payments)
            {
                fee.PaymentList.Add(CreateRespaFeePayment_2015DataLayer(payment, closingCost.IsFhaAllowable));
            }

            return fee;
        }

        /// <summary>
        /// Exports a payment for an Integrated Disclosure closing cost.
        /// </summary>
        /// <param name="payment">The payment to be exported.</param>
        /// <param name="isFhaAllowable">Indicates whether the fee is an allowable FHA closing cost.</param>
        /// <param name="sequenceNumber">A sequence number assigned to each payment.</param>
        /// <returns>A <see cref="Payment" /> object.</returns>
        private Payment CreateRespaFeePayment_2015DataLayer(LoanClosingCostFeePayment payment, bool isFhaAllowable)
        {
            var respaPayment = new Payment();
            respaPayment.Amount = payment.Amount_rep;
            respaPayment.IncludedInAprIndicator = payment.IsIncludedInApr(m_dataLoan.GetLiveLoanVersion()) ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.PaidOutsideOfClosingIndicator = payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.FinancedIndicator = payment.IsFinanced ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.PaidByType = ToMismo(payment.GetPaidByType(m_dataLoan.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
            respaPayment.AllowableFhaClosingCostIndicator = isFhaAllowable ? E_YNIndicator.Y : E_YNIndicator.N;

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                respaPayment.Id = "_" + payment.Id.ToString();
            }

            return respaPayment;
        }

        /// <summary>
        /// Gets the total percent of a fee either from the fee itself or from loan fields for certain fees.
        /// </summary>
        /// <param name="closingCost">The fee to obtain the total percent for.</param>
        /// <returns>The amount of a fee in percent.</returns>
        private string GetTotalPercent(LoanClosingCostFee closingCost)
        {
            if (closingCost.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointFPc_rep;
            }
            else
            {
                return closingCost.Percent_rep;
            }
        }

        /// <summary>
        /// Retrieves the RESPA section classification type based on a fee's HUD line.
        /// </summary>
        /// <param name="hudLine">The HUD line for a fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeRespaSectionClassificationType"/> value.</returns>
        private E_RespaFeeRespaSectionClassificationType GetRespaSectionClassificationType(string hudLine)
        {
            if (StaticMethodsAndExtensions.IsInRange(hudLine, 100, 199))
            {
                return E_RespaFeeRespaSectionClassificationType._100_GrossAmountDueFromBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 200, 299))
            {
                return E_RespaFeeRespaSectionClassificationType._200_AmountsPaidByOrInBehalfOfBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 300, 399))
            {
                return E_RespaFeeRespaSectionClassificationType._300_CashAtSettlementFromToBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 400, 499))
            {
                return E_RespaFeeRespaSectionClassificationType._400_GrossAmountDueToSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 500, 599))
            {
                return E_RespaFeeRespaSectionClassificationType._500_ReductionsInAmountDueSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 600, 699))
            {
                return E_RespaFeeRespaSectionClassificationType._600_CashAtSettlementToFromSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 700, 799))
            {
                return E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 800, 899))
            {
                return E_RespaFeeRespaSectionClassificationType._800_LoanFees;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 900, 999))
            {
                return E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1000, 1099))
            {
                return E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1100, 1199))
            {
                return E_RespaFeeRespaSectionClassificationType._1100_TitleCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1200, 1299))
            {
                return E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1300, 1399))
            {
                return E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges;
            }
            else
            {
                return E_RespaFeeRespaSectionClassificationType.Undefined;
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_RespaFeeGfeAggregationType"/> corresponding to the GFE section
        /// type of a closing cost fee.
        /// </summary>
        /// <param name="gfeSectionT">The GFE section of a fee.</param>
        /// <returns>The corresponding GFE aggregation type.</returns>
        private E_RespaFeeGfeAggregationType Convert(E_GfeSectionT gfeSectionT)
        {
            switch (gfeSectionT)
            {
                case E_GfeSectionT.NotApplicable:
                    return E_RespaFeeGfeAggregationType.None;
                case E_GfeSectionT.B1:
                    return E_RespaFeeGfeAggregationType.OurOriginationCharge;
                case E_GfeSectionT.B2:
                    return E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge;
                case E_GfeSectionT.B3:
                    return E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;
                case E_GfeSectionT.B4:
                    return E_RespaFeeGfeAggregationType.TitleServices;
                case E_GfeSectionT.B5:
                    return E_RespaFeeGfeAggregationType.OwnersTitleInsurance;
                case E_GfeSectionT.B6:
                    return E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor;
                case E_GfeSectionT.B7:
                    return E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
                case E_GfeSectionT.B8:
                    return E_RespaFeeGfeAggregationType.TransferTaxes;
                case E_GfeSectionT.B9:
                case E_GfeSectionT.B10:
                case E_GfeSectionT.B11:
                case E_GfeSectionT.LeaveBlank:
                    return E_RespaFeeGfeAggregationType.Undefined;
                default:
                    throw new UnhandledEnumException(gfeSectionT);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_PaymentPaidByType" /> corresponding to the entity who paid the fee.
        /// </summary>
        /// <param name="paidBy">The entity who paid the fee.</param>
        /// <returns>The corresponding <see cref="E_PaymentPaidByType" /> value.</returns>
        private E_PaymentPaidByType ToMismo(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_PaymentPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return E_PaymentPaidByType.Broker;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_PaymentPaidByType.Undefined;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_PaymentPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_PaymentPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_PaymentPaidByType.ThirdParty;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_RespaFeePaidToType" /> corresponding to the entity who will receive the fee payment.
        /// </summary>
        /// <param name="role">The role of the entity who will receive the fee payment.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePaidToType" /> value.</returns>
        private E_RespaFeePaidToType ToMismo_2015DataLayerFeePaidTo(E_AgentRoleT role)
        {
            var agent = m_dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (Enum.Equals(role, E_AgentRoleT.Broker))
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Investor))
            {
                return E_RespaFeePaidToType.Investor;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Lender))
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsLender)
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsOriginator)
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (this.vendorIsIDS && agent.IsOriginatorAffiliate)
            {
                return E_RespaFeePaidToType.AffiliateOfLender;
            }
            else
            {
                return E_RespaFeePaidToType.Other;
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_RespaFeePercentBasisType" /> corresponding to the given percent basis type.
        /// </summary>
        /// <param name="basisType">The percent basis type.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePercentBasisType" /> value.</returns>
        private E_RespaFeePercentBasisType ToMismo_2015DataLayer(E_PercentBaseT basisType)
        {
            switch (basisType)
            {
                case E_PercentBaseT.AppraisalValue:
                    return E_RespaFeePercentBasisType.PropertyAppraisedValueAmount;
                case E_PercentBaseT.LoanAmount:
                    return E_RespaFeePercentBasisType.BaseLoanAmount;
                case E_PercentBaseT.SalesPrice:
                    return E_RespaFeePercentBasisType.PurchasePriceAmount;
                case E_PercentBaseT.TotalLoanAmount:
                    return E_RespaFeePercentBasisType.OriginalLoanAmount;
                case E_PercentBaseT.AllYSP:
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                case E_PercentBaseT.OriginalCost:
                    return E_RespaFeePercentBasisType.Other;
                default:
                    throw new UnhandledEnumException(basisType);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_RespaFeeType" /> corresponding to the given fee type.
        /// </summary>
        /// <param name="feeType">The fee type.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeType" /> value.</returns>
        private E_RespaFeeType ToMismo(E_ClosingCostFeeMismoFeeT feeType)
        {
            switch (feeType)
            {
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return E_RespaFeeType._203KArchitecturalAndEngineeringFee;
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return E_RespaFeeType._203KConsultantFee;
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return E_RespaFeeType._203KDiscountOnRepairs;
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return E_RespaFeeType._203KInspectionFee;
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return E_RespaFeeType._203KPermits;
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return E_RespaFeeType._203KSupplementalOriginationFee;
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return E_RespaFeeType._203KTitleUpdate;
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                    return E_RespaFeeType.AbstractOrTitleSearchFee;
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                    return E_RespaFeeType.AmortizationFee;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return E_RespaFeeType.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                    return E_RespaFeeType.AppraisalDeskReviewFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return E_RespaFeeType.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                    return E_RespaFeeType.AppraisalFieldReviewFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return E_RespaFeeType.AssignmentFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return E_RespaFeeType.AssignmentRecordingFee;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return E_RespaFeeType.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return E_RespaFeeType.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                    return E_RespaFeeType.BankruptcyMonitoringFee;
                case E_ClosingCostFeeMismoFeeT.BondFee:
                    return E_RespaFeeType.BondFee;
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                    return E_RespaFeeType.BondReviewFee;
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                    return E_RespaFeeType.CertificationFee;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return E_RespaFeeType.CityCountyDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return E_RespaFeeType.CityCountyMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                    return E_RespaFeeType.CLOAccessFee;
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                    return E_RespaFeeType.ClosingProtectionLetterFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return E_RespaFeeType.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                    return E_RespaFeeType.CopyFaxFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return E_RespaFeeType.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return E_RespaFeeType.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return E_RespaFeeType.DeedRecordingFee;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return E_RespaFeeType.DocumentaryStampFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return E_RespaFeeType.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                    return E_RespaFeeType.ElectronicDocumentDeliveryFee;
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                    return E_RespaFeeType.EscrowServiceFee;
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return E_RespaFeeType.EscrowWaiverFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return E_RespaFeeType.FloodCertification;
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                    return E_RespaFeeType.GeneralCounselFee;
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return E_RespaFeeType.InspectionFee;
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return E_RespaFeeType.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return E_RespaFeeType.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                    return E_RespaFeeType.MERSRegistrationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return E_RespaFeeType.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return E_RespaFeeType.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return E_RespaFeeType.MortgageRecordingFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return E_RespaFeeType.MunicipalLienCertificateFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return E_RespaFeeType.MunicipalLienCertificateRecordingFee;
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return E_RespaFeeType.NewLoanAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return E_RespaFeeType.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                    return E_RespaFeeType.PayoffRequestFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return E_RespaFeeType.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return E_RespaFeeType.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return E_RespaFeeType.RealEstateCommission;
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return E_RespaFeeType.RedrawFee;
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return E_RespaFeeType.ReinspectionFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return E_RespaFeeType.ReleaseRecordingFee;
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return E_RespaFeeType.RuralHousingFee;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return E_RespaFeeType.SettlementOrClosingFee;
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                    return E_RespaFeeType.SigningAgentFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return E_RespaFeeType.StateDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return E_RespaFeeType.StateMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                    return E_RespaFeeType.SubordinationFee;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return E_RespaFeeType.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return E_RespaFeeType.TaxRelatedServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                    return E_RespaFeeType.TitleEndorsementFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return E_RespaFeeType.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return E_RespaFeeType.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return E_RespaFeeType.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return E_RespaFeeType.UnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                    return E_RespaFeeType.WireTransferFee;
                // OPM 227381 - Both MISMO 2.6 and 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                    return E_RespaFeeType.VAFundingFee;
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                    return E_RespaFeeType.MIInitialPremium;
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal;
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return E_RespaFeeType.OurOriginationChargeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return E_RespaFeeType.TitleServicesFeeTotal;
                // OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                case E_ClosingCostFeeMismoFeeT.Undefined:
                case E_ClosingCostFeeMismoFeeT.Other:
                    return E_RespaFeeType.Other;
                default:
                    throw new UnhandledEnumException(feeType);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_RespaFeeResponsiblePartyType" /> value corresponding to the responsible party for the fee.
        /// </summary>
        /// <param name="responsibleParty">The responsible party for the fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeResponsiblePartyType" /> value.</returns>
        private E_RespaFeeResponsiblePartyType ToMismo(E_GfeResponsiblePartyT responsibleParty)
        {
            switch (responsibleParty)
            {
                case E_GfeResponsiblePartyT.Buyer:
                    return E_RespaFeeResponsiblePartyType.Buyer;
                case E_GfeResponsiblePartyT.Seller:
                    return E_RespaFeeResponsiblePartyType.Seller;
                case E_GfeResponsiblePartyT.LeaveBlank:
                case E_GfeResponsiblePartyT.Lender:
                case E_GfeResponsiblePartyT.Broker:
                    return E_RespaFeeResponsiblePartyType.Undefined;
                default:
                    throw new UnhandledEnumException(responsibleParty);
            }
        }

        private void PopulateEscrowList(List<Escrow> escrowList)
        {
            if (m_dataLoan.sHazInsRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.HAZ_INS_RESERVE,
                        "",
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProHazIns_rep, // this is the same in both pages
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProHazIns,     // this too
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sHazInsRsrvMon_rep : m_dataLoan.sSettlementHazInsRsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sHazInsRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sHazInsRsrvProps, m_dataLoan.sSettlementHazInsRsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.HazardInsurance)
                        ));    //dont need this for 1000's      
            }
            if (m_dataLoan.sSchoolTxRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.SCHOOL_TAXES,
                        "",
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProSchoolTx_rep, // this amount is the same in both the GFE and settlement pages
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProSchoolTx,     // same as above
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sSchoolTxRsrvMon_rep : m_dataLoan.sSettlementSchoolTxRsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sSchoolTxRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sSchoolTxRsrvProps, m_dataLoan.sSettlementSchoolTxRsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.SchoolTax)
                        ));
            }

            if (m_dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes)
            {
                // OPM 182290
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.MTG_INS_RESERVE,
                        "",
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProMIns_rep, // this amount is the same in both the GFE and settlement pages
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProMIns,     // same as above
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sMInsRsrvMon_rep : m_dataLoan.sSettlementMInsRsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sMInsRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sMInsRsrvProps, m_dataLoan.sSettlementMInsRsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.MortgageInsurance)
                        ));
            }

            if (m_dataLoan.sRealETxRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.REAL_ESTATE_TAX_RESERVE,
                        "",
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProRealETx_rep, // same on both pages linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProRealETx,
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sRealETxRsrvMon_rep : m_dataLoan.sSettlementRealETxRsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sRealETxRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRealETxRsrvProps, m_dataLoan.sSettlementRealETxRsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.RealEstateTax)
                        ));
            }

            if (m_dataLoan.sFloodInsRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.FLOOD_INS_RESERVE,
                        "",
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProFloodIns_rep, // this field is used by both pages
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProFloodIns,     // this field is used by both pages
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sFloodInsRsrvMon_rep : m_dataLoan.sSettlementFloodInsRsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sFloodInsRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sFloodInsRsrvProps, m_dataLoan.sSettlementFloodInsRsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.FloodInsurance)
                        ));   //include the total 
            }

            if (m_dataLoan.s1006RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.CUSTOM_1008,
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExpDesc, // fields are linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExp_rep, // fields are linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExp,     // fields are linked
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.s1006RsrvMon_rep : m_dataLoan.sSettlement1008RsrvMon_rep, //not quit esure why its 1006 and 1008 in the different pages, but then again this mismo doc says 1006
                        "",
                        m_useGfeFee ? m_dataLoan.s1006RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s1006RsrvProps, m_dataLoan.sSettlement1008RsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.UserDefine1)
                        ));
            }

            if (m_dataLoan.s1007RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.CUSTOM_1009,
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExpDesc, // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExp_rep, // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExp,     // linked
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.s1007RsrvMon_rep : m_dataLoan.sSettlement1009RsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.s1007RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s1007RsrvProps, m_dataLoan.sSettlement1009RsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.UserDefine2)
                        ));
            }
            if (m_dataLoan.sU3RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.CUSTOM_1010,
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sU3RsrvDesc,    // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU3Rsrv_rep, // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU3Rsrv,     // linked
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3RsrvMon_rep : m_dataLoan.sSettlementU3RsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sU3RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3RsrvProps, m_dataLoan.sSettlementU3RsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.UserDefine3)
                        ));
            }

            if (m_dataLoan.sU4RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        HUDLines.CUSTOM_1011,
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sU4RsrvDesc,    // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU4Rsrv_rep, // linked
                        (m_useGfeFee ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU4Rsrv,     // linked
                        m_useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4RsrvMon_rep : m_dataLoan.sSettlementU4RsrvMon_rep,
                        "",
                        m_useGfeFee ? m_dataLoan.sU4RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4RsrvProps, m_dataLoan.sSettlementU4RsrvProps),
                        null,
                        "",
                        m_dataLoan.GetCushionMonths(E_EscrowItemT.UserDefine4)
                        ));
            }
            escrowList.Add(new Escrow()
            {
                ItemType = E_EscrowItemType.WindstormInsurance,
                AnnualPaymentAmount = m_dataLoan.sWindInsPaymentDueAmt_rep,
                PaidTo = new PaidTo() { Name = m_dataLoan.sWindInsCompanyNm }
            });
        }

        /// <summary>
        /// Populates the Escrow list with values from the TRID housing expenses.
        /// </summary>
        /// <param name="escrowList">The list to populate.</param>
        private void PopulateEscrowList_2015DataLayer(List<Escrow> escrowList)
        {
            // MI Escrow
            escrowList.Add(
                CreateEscrow(
                HUDLines.MTG_INS_RESERVE,
                "",
                m_dataLoanWithGfeArchiveApplied.sProMIns_rep,
                m_dataLoanWithGfeArchiveApplied.sProMIns,
                m_dataLoanWithGfeArchiveApplied.sMInsRsrvMon_rep,
                "",
                m_dataLoanWithGfeArchiveApplied.sMInsRsrvProps,
                null,
                "",
                m_dataLoan.GetCushionMonths(E_EscrowItemT.MortgageInsurance)
            ));

            foreach (var expense in m_dataLoanWithGfeArchiveApplied.sHousingExpenses.ExpensesToUse)
            {
                escrowList.Add(CreateEscrow_2015DataLayer(expense, m_dataLoan.sClosingCostSet));
            }
        }

        /// <summary>
        /// Creates an Escrow container representing a housing expense.
        /// </summary>
        /// <param name="expense">The housing expense to export.</param>
        /// <returns>An Escrow container.</returns>
        private Escrow CreateEscrow_2015DataLayer(BaseHousingExpense expense, BorrowerClosingCostSet closingCostSet)
        {
            // 9/25/2015 sg - Case 227053 Skip $0.00 expenses when exporting to ESCROW element
            // Exporter skips over null elements
            if (skipExpenseToEscrow(expense))
            {
                return null;
            }

            var escrow = new Escrow();

            escrow.ItemType = ToMismo(expense.HousingExpenseType);

            if (escrow.ItemType == E_EscrowItemType.Other)
            {
                escrow.ItemTypeOtherDescription = this.GetEscrowItemTypeOtherDescription(expense);
            }

            // 8/24/2015 bs - Case 222479 Get the correct HUDLine from fee type
            Guid feeTypeId = ClosingCostSetUtils.Get1000FeeTypeIdFromAssignedHousingExpense(expense);
            BorrowerClosingCostFee fee = (BorrowerClosingCostFee)closingCostSet.FindFeeByTypeId(feeTypeId);
            if (expense.HousingExpenseType != E_HousingExpenseTypeT.Unassigned)
            {
                if (fee != null)
                {
                    escrow.SpecifiedHud1LineNumber = fee.HudLine.ToString();
                }
            }
            else
            {
                escrow.SpecifiedHud1LineNumber = expense.LineNumAsString;
            }

            escrow.AccountSummary = CreateAccountSummary(expense.CushionMonths);

            if (fee != null)
            {
                escrow.PaidTo = CreatePaidTo(fee.Beneficiary);
            }

            if (vendorIsIDS || vendorIsDocuTech)
            {
                escrow.PaidToType = ToMismo_2015DataLayerEscrowPaidTo(expense.PaidTo);
            }

            BorrowerClosingCostFee prepaidFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionF);
            if (prepaidFee != null)
            {
                escrow.PremiumDurationMonthsCount = expense.PrepaidMonths_rep;
                escrow.GfeDisclosedPremiumAmount = expense.PrepaidAmt_rep;
                // Get live data for premium amount field
                BaseHousingExpense liveExpense = m_dataLoan.sHousingExpenses.AssignedExpenses.Where(e => e.HousingExpenseType == expense.HousingExpenseType).FirstOrDefault();
                escrow.PremiumAmount = liveExpense != null ? liveExpense.PrepaidAmt_rep : expense.PrepaidAmt_rep;

                var firstPayment = prepaidFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PremiumPaidByType = ToMismo_2015DataLayerEscrowPremiumPaidBy(firstPayment.GetPaidByType(m_dataLoan.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
                }
            }

            escrow.MonthlyPaymentAmount = expense.MonthlyAmtTotal_rep;
            escrow.CollectedNumberOfMonthsCount = expense.ReserveMonths_rep;
            escrow.AnnualPaymentAmount = expense.AnnualAmt_rep;
            escrow.PremiumPaymentType = GetPremiumPaymentType(expense);

            BorrowerClosingCostFee reserveFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            if (reserveFee != null)
            {
                var firstPayment = reserveFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PaidByType = ToMismo_2015DataLayerEscrowPaidBy(firstPayment.PaidByT);
                }
            }

            int numberOfPayments = 1;
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                foreach (var disbursement in expense.ActualDisbursementsList.Where(d => d.PaidBy == E_DisbursementPaidByT.EscrowImpounds))
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }
            else
            {
                foreach (var disbursement in expense.ProjectedDisbursements.Where(d => d.PaidBy == E_DisbursementPaidByT.EscrowImpounds).OrderBy(o => o.DueDate))
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }

            return escrow;
        }

        /// <summary>
        /// Returns a description for a housing expense with an ItemType of Other.
        /// </summary>
        /// <param name="expense">The housing expense.</param>
        /// <returns>The housing expense description.</returns>
        private string GetEscrowItemTypeOtherDescription(BaseHousingExpense expense)
        {
            if (!string.IsNullOrEmpty(expense.ExpenseDescription))
            {
                return expense.ExpenseDescription;
            }
            else
            {
                return expense.HousingExpenseType.ToString();
            }
        }

        /// <summary>
        /// Returns a value indicating whether the expense should be skipped when adding ESCROW elements.
        /// </summary>
        /// <param name="expense">the expense to be evaluated</param>
        /// <returns>whether the expense should be skipped</returns>
        /// <remarks>"If the expense's AnnualAmt, MonthlyAmtTotal, ReserveAmt, and PrepaidAmt_rep are all zero then it follows that there is no such expense,
        ///  and therefore neither an associated prepaid fee (TRID section F) nor reserves (section G)." - bb, opm 227053</remarks>
        private bool skipExpenseToEscrow(BaseHousingExpense expense)
        {
            if (expense.AnnualAmt == 0 && expense.MonthlyAmtTotal == 0 && expense.ReserveAmt == 0 && expense.PrepaidAmt == 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the prepaid fee associated with a housing expense. The fee will either be from 
        /// section 900 (GFE) or section F (TRID).
        /// Retrieves the premium payment type based on whether an expense was escrowed and paid at closing.
        /// </summary>
        /// <param name="expense">The housing expense.</param>
        /// <returns>The corresponding <see cref="E_EscrowPremiumPaymentType"/> value.</returns>
        private E_EscrowPremiumPaymentType GetPremiumPaymentType(BaseHousingExpense expense)
        {
            if (expense.IsEscrowedAtClosing == E_TriState.No)
            {
                return E_EscrowPremiumPaymentType.Waived;
            }

            BorrowerClosingCostFee reserveFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            if (reserveFee != null)
            {
                var firstPayment = reserveFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    if (firstPayment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing)
                    {
                        return E_EscrowPremiumPaymentType.CollectedAtClosing;
                    }
                    else if (firstPayment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing)
                    {
                        return E_EscrowPremiumPaymentType.PaidOutsideOfClosing;
                    }
                }
            }

            return E_EscrowPremiumPaymentType.Undefined;
        }

        /// <summary>
        /// Creates a disbursement for a housing expense.
        /// </summary>
        /// <param name="disbursement">The disbursement to export.</param>
        /// <param name="sequenceIdentifier">Indicates where this payment falls in the sequence of payments.</param>
        /// <returns>A Payments object.</returns>
        private Payments CreateEscrowPayments_2015DataLayer(SingleDisbursement disbursement, int sequenceIdentifier)
        {
            var payments = new Payments();
            payments.DueDate = disbursement.DueDate_rep;
            payments.PaymentAmount = disbursement.DisbursementAmt_rep;
            payments.SequenceIdentifier = sequenceIdentifier.ToString();

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                payments.Id = "_" + disbursement.DisbId.ToString();
            }

            return payments;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow premium.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPremiumPaidByType ToMismo_2015DataLayerEscrowPremiumPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPremiumPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPremiumPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPremiumPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_EscrowPremiumPaidByType.Other;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPremiumPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow reserve.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow reserve.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPaidByType ToMismo_2015DataLayerEscrowPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPaidByType.LenderPremium;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_EscrowPaidByType.Other;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_EscrowPaidToType" /> corresponding to the given role.
        /// </summary>
        /// <param name="role">The role of the entity who will receive a housing expense payment.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidToType" /> value.</returns>
        private E_EscrowPaidToType ToMismo_2015DataLayerEscrowPaidTo(E_AgentRoleT role)
        {
            var agent = m_dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (Enum.Equals(role, E_AgentRoleT.Broker))
            {
                return E_EscrowPaidToType.Broker;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Investor))
            {
                return E_EscrowPaidToType.Investor;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Lender))
            {
                return E_EscrowPaidToType.Lender;
            }
            else if (agent.IsLender)
            {
                return E_EscrowPaidToType.Lender;
            }
            else if (agent.IsOriginator)
            {
                return E_EscrowPaidToType.Broker;
            }
            else if (agent.IsOriginatorAffiliate)
            {
                return E_EscrowPaidToType.AffiliateOfLender;
            }
            else
            {
                return E_EscrowPaidToType.Other;
            }
        }

        /// <summary>
        /// Retrieves the <see cref="E_EscrowItemType" /> corresponding to the expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="E_EscrowItemType" /> value.</returns>
        private E_EscrowItemType ToMismo(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return E_EscrowItemType.HazardInsurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return E_EscrowItemType.FloodInsurance;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return E_EscrowItemType.WindstormInsurance;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return E_EscrowItemType.SchoolPropertyTax;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                case E_HousingExpenseTypeT.GroundRent:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.Unassigned:
                    return E_EscrowItemType.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private void PopulateAssetList(List<Asset> assetList, CAppData dataApp)
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            assetList.Add(CreateAsset(assetCollection.GetBusinessWorth(false), dataApp.aBMismoId));
            assetList.Add(CreateAsset(assetCollection.GetLifeInsurance(false), dataApp.aBMismoId));
            assetList.Add(CreateAsset(assetCollection.GetRetirement(false), dataApp.aBMismoId));
            assetList.Add(CreateAsset(assetCollection.GetCashDeposit1(false), dataApp.aBMismoId));
            assetList.Add(CreateAsset(assetCollection.GetCashDeposit2(false), dataApp.aBMismoId));
            for (int i = 0; i < assetCollection.CountRegular; i++)
            {
                assetList.Add(CreateAsset(assetCollection.GetRegularRecordAt(i), dataApp));
            }
        }

        private void PopulateLiabilityList(List<Liability> liabilityList, CAppData dataApp, int borrowerIndex)
        {
            ILiaCollection liaCollection = dataApp.aLiaCollection;

            liabilityList.Add(CreateLiability(liaCollection.GetAlimony(false), dataApp.aBMismoId, borrowerIndex));
            liabilityList.Add(CreateLiability(liaCollection.GetChildSupport(false), dataApp.aBMismoId, borrowerIndex));
            liabilityList.Add(CreateLiability(liaCollection.GetJobRelated1(false), dataApp.aBMismoId, borrowerIndex));
            liabilityList.Add(CreateLiability(liaCollection.GetJobRelated2(false), dataApp.aBMismoId, borrowerIndex));
            for (int recordIndex = 0; recordIndex < liaCollection.CountRegular; recordIndex++)
            {
                liabilityList.Add(CreateLiability(liaCollection.GetRegularRecordAt(recordIndex), dataApp, borrowerIndex));
            }
        }

        private void PopulateReoPropertyList(List<ReoProperty> reoPropertyList, CAppData dataApp, int borrowerIndex)
        {
            var reCollection = dataApp.aReCollection;

            for (int recordIndex = 0; recordIndex < reCollection.CountRegular; recordIndex++)
            {
                reoPropertyList.Add(CreateReoProperty(reCollection.GetRegularRecordAt(recordIndex), dataApp, borrowerIndex));
            }
        }

        /// <summary>
        /// Get the RespaFee associated with the HUD line.
        /// Only supports lines 813-817, 904, 906, 1112-1115, 1206-1208, and 1303-1307.
        /// </summary>
        /// <remarks>
        /// This should be used to retrieve the fee items with custom descriptions and multiple section options.
        /// </remarks>
        /// <param name="lineNumber">The HUD line of the fee.</param>
        /// <param name="useGfeFee">If true, returns GFE fee. Otherwise, returns SC fee</param>
        /// <returns>The fee associated with the HUD line.</returns>
        private RespaFee GetRespaFeeForHudLine(string lineNumber, bool useGfeFee)
        {
            return GetRespaFeeForHudLine(lineNumber, useGfeFee, false);
        }

        /// <summary>
        /// Get the RespaFee associated with the line number.
        /// Only supports lines 813-817, 904, 906, 1112-1115, 1206-1208, and 1303-1307.
        /// </summary>
        /// <remarks>
        /// This should be used to retrieve the fee items with custom descriptions and multiple section options.
        /// </remarks>
        /// <param name="lineNumber">The HUD line of the fee.</param>
        /// <param name="useGfeFee">If true, returns GFE fee. Otherwise, returns SC fee</param>
        /// <param name="bypassGfeDiscAmtPmtAmtCheck">If true, return fee even when the amount and GFE disclosed amount are 0. Otherwise, return null in this case.</param>
        /// <returns>The fee associated with the HUD line.</returns>
        private RespaFee GetRespaFeeForHudLine(string lineNumber, bool useGfeFee, bool bypassGfeDiscAmtPmtAmtCheck)
        {
            RespaFee ret = null;
            bool requireGfeDisclosedAmt = false;
            switch (lineNumber)
            {
                case "813":
                    // 813 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U1FGfeSection : m_dataLoan.s800U1FSettlementSection), E_RespaFeeType.Other,
                        "813" /* m_dataLoan.s800U1FCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U1FDesc : m_dataLoan.sSettlement800U1FDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U1F_rep : m_dataLoan.sSettlement800U1F_rep,
                        useGfeFee ? m_dataLoan.s800U1FProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U1FProps, m_dataLoan.sSettlement800U1FProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s800U1F_rep : null, m_dataLoan.s800U1FPaidTo, requireGfeDisclosedAmt);
                    break;
                case "814":
                    // 814 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U2FGfeSection : m_dataLoan.s800U2FSettlementSection), E_RespaFeeType.Other,
                        "814" /* m_dataLoan.s800U2FCode */,
                         useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U2FDesc : m_dataLoan.sSettlement800U2FDesc,
                         false,
                         useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U2F_rep : m_dataLoan.sSettlement800U2F_rep,
                         useGfeFee ? m_dataLoan.s800U2FProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U2FProps, m_dataLoan.sSettlement800U2FProps),
                         m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s800U2F_rep : null, m_dataLoan.s800U2FPaidTo, requireGfeDisclosedAmt);
                    break;
                case "815":
                    // 815 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U3FGfeSection : m_dataLoan.s800U3FSettlementSection), E_RespaFeeType.Other,
                        "815" /* m_dataLoan.s800U3FCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U3FDesc : m_dataLoan.sSettlement800U3FDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U3F_rep : m_dataLoan.sSettlement800U3F_rep,
                        useGfeFee ? m_dataLoan.s800U3FProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U3FProps, m_dataLoan.sSettlement800U3FProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s800U3F_rep : null, m_dataLoan.s800U3FPaidTo, requireGfeDisclosedAmt);
                    break;
                case "816":
                    // 816 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U4FGfeSection : m_dataLoan.s800U4FSettlementSection), E_RespaFeeType.Other,
                        "816" /* m_dataLoan.s800U4FCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U4FDesc : m_dataLoan.sSettlement800U4FDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U4F_rep : m_dataLoan.sSettlement800U4F_rep,
                        useGfeFee ? m_dataLoan.s800U4FProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U4FProps, m_dataLoan.sSettlement800U4FProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s800U4F_rep : null, m_dataLoan.s800U4FPaidTo, requireGfeDisclosedAmt);
                    break;
                case "817":
                    // 817 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U5FGfeSection : m_dataLoan.s800U5FSettlementSection), E_RespaFeeType.Other,
                        "817" /* m_dataLoan.s800U5FCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U5FDesc : m_dataLoan.sSettlement800U5FDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.s800U5F_rep : m_dataLoan.sSettlement800U5F_rep,
                        useGfeFee ? m_dataLoan.s800U5FProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U5FProps, m_dataLoan.sSettlement800U5FProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s800U5F_rep : null, m_dataLoan.s800U5FPaidTo, requireGfeDisclosedAmt);
                    break;
                case "904":
                    E_GfeSectionT section = useGfeFee ? m_dataLoanWithGfeArchiveApplied.s904PiaGfeSection : m_dataLoan.s904PiaSettlementSection;
                    var aggregationType904 = E_RespaFeeGfeAggregationType.None;
                    if (section == E_GfeSectionT.B3) aggregationType904 = E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;

                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, aggregationType904, E_RespaFeeType.Other,
                        "904", useGfeFee ? m_dataLoanWithGfeArchiveApplied.s904PiaDesc : m_dataLoan.sSettlement904PiaDesc, false,
                            useGfeFee ? m_dataLoanWithGfeArchiveApplied.s904Pia_rep : m_dataLoan.sSettlement904Pia_rep,
                            useGfeFee ? m_dataLoan.s904PiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s904PiaProps, m_dataLoan.sSettlement904PiaProps),
                            m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s904Pia_rep : null,
                            "", requireGfeDisclosedAmt);
                    break;
                case "906":
                    section = useGfeFee ? m_dataLoanWithGfeArchiveApplied.s900U1PiaGfeSection : m_dataLoan.s900U1PiaSettlementSection;
                    var aggregationType906 = E_RespaFeeGfeAggregationType.None;
                    if (section == E_GfeSectionT.B3) aggregationType906 = E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;

                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, aggregationType906, E_RespaFeeType.Other,
                        "906", useGfeFee ? m_dataLoanWithGfeArchiveApplied.s900U1PiaDesc : m_dataLoan.sSettlement900U1PiaDesc, false,
                            useGfeFee ? m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep : m_dataLoan.sSettlement900U1Pia_rep,
                            useGfeFee ? m_dataLoan.s900U1PiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s900U1PiaProps, m_dataLoan.sSettlement900U1PiaProps),
                            m_includeGfe ? m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep : null,
                            "", requireGfeDisclosedAmt);
                    break;
                case "1112":
                    //1112 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1TcGfeSection : m_dataLoan.sU1TcSettlementSection), E_RespaFeeType.Other,
                        "1112" /* m_dataLoan.sU1TcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1TcDesc : m_dataLoan.sSettlementU1TcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1Tc_rep : m_dataLoan.sSettlementU1Tc_rep,
                        useGfeFee ? m_dataLoan.sU1TcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1TcProps, m_dataLoan.sSettlementU1TcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU1Tc_rep : null, m_dataLoan.sU1TcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1113":
                    //1113 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2TcGfeSection : m_dataLoan.sU2TcSettlementSection), E_RespaFeeType.Other,
                        "1113" /* m_dataLoan.sU2TcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2TcDesc : m_dataLoan.sSettlementU2TcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2Tc_rep : m_dataLoan.sSettlementU2Tc_rep,
                        useGfeFee ? m_dataLoan.sU2TcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2TcProps, m_dataLoan.sSettlementU2TcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU2Tc_rep : null, m_dataLoan.sU2TcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1114":
                    //1114 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3TcGfeSection : m_dataLoan.sU3TcSettlementSection), E_RespaFeeType.Other,
                        "1114" /*m_dataLoan.sU3TcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3TcDesc : m_dataLoan.sSettlementU3TcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3Tc_rep : m_dataLoan.sSettlementU3Tc_rep,
                        useGfeFee ? m_dataLoan.sU3TcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3TcProps, m_dataLoan.sSettlementU3TcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU3Tc_rep : null, m_dataLoan.sU3TcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1115":
                    //1115 - Others
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4TcGfeSection : m_dataLoan.sU4TcSettlementSection), E_RespaFeeType.Other,
                        "1115" /* m_dataLoan.sU4TcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4TcDesc : m_dataLoan.sSettlementU4TcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4Tc_rep : m_dataLoan.sSettlementU4Tc_rep,
                        useGfeFee ? m_dataLoan.sU4TcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4TcProps, m_dataLoan.sSettlementU4TcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU4Tc_rep : null, m_dataLoan.sU4TcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1206":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1GovRtcGfeSection : m_dataLoan.sU1GovRtcSettlementSection), E_RespaFeeType.Other,
                        "1206" /*m_dataLoan.sU1GovRtcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1GovRtcDesc : m_dataLoan.sSettlementU1GovRtcDesc, false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep : m_dataLoan.sSettlementU1GovRtc_rep,
                        useGfeFee ? m_dataLoan.sU1GovRtcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1GovRtcProps, m_dataLoan.sSettlementU1GovRtcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep : null, m_dataLoan.sU1GovRtcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1207":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2GovRtcGfeSection : m_dataLoan.sU2GovRtcSettlementSection), E_RespaFeeType.Other,
                        "1207" /* m_dataLoan.sU2GovRtcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2GovRtcDesc : m_dataLoan.sSettlementU2GovRtcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2GovRtc_rep : m_dataLoan.sSettlementU2GovRtc_rep,
                        useGfeFee ? m_dataLoan.sU2GovRtcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2GovRtcProps, m_dataLoan.sSettlementU2GovRtcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU2GovRtc_rep : null, m_dataLoan.sU2GovRtcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1208":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3GovRtcGfeSection : m_dataLoan.sU3GovRtcSettlementSection), E_RespaFeeType.Other,
                        "1208" /* m_dataLoan.sU3GovRtcCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3GovRtcDesc : m_dataLoan.sSettlementU3GovRtcDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3GovRtc_rep : m_dataLoan.sSettlementU3GovRtc_rep,
                        useGfeFee ? m_dataLoan.sU3GovRtcProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3GovRtcProps, m_dataLoan.sSettlementU3GovRtcProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU3GovRtc_rep : null, m_dataLoan.sU3GovRtcPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1303":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1ScGfeSection : m_dataLoan.sU1ScSettlementSection), E_RespaFeeType.Other,
                        "1303" /* m_dataLoan.sU1ScCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1ScDesc : m_dataLoan.sSettlementU1ScDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU1Sc_rep : m_dataLoan.sSettlementU1Sc_rep,
                        useGfeFee ? m_dataLoan.sU1ScProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1ScProps, m_dataLoan.sSettlementU1ScProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU1Sc_rep : null, m_dataLoan.sU1ScPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1304":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2ScGfeSection : m_dataLoan.sU2ScSettlementSection), E_RespaFeeType.Other,
                        "1304" /* m_dataLoan.sU2ScCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2ScDesc : m_dataLoan.sSettlementU2ScDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU2Sc_rep : m_dataLoan.sSettlementU2Sc_rep,
                        useGfeFee ? m_dataLoan.sU2ScProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2ScProps, m_dataLoan.sSettlementU2ScProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU2Sc_rep : null, m_dataLoan.sU2ScPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1305":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3ScGfeSection : m_dataLoan.sU3ScSettlementSection), E_RespaFeeType.Other,
                        "1305" /* m_dataLoan.sU3ScCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3ScDesc : m_dataLoan.sSettlementU3ScDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU3Sc_rep : m_dataLoan.sSettlementU3Sc_rep,
                        useGfeFee ? m_dataLoan.sU3ScProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3ScProps, m_dataLoan.sSettlementU3ScProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU3Sc_rep : null, m_dataLoan.sU3ScPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1306":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4ScGfeSection : m_dataLoan.sU4ScSettlementSection), E_RespaFeeType.Other,
                        "1306" /* m_dataLoan.sU4ScCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4ScDesc : m_dataLoan.sSettlementU4ScDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU4Sc_rep : m_dataLoan.sSettlementU4Sc_rep,
                        useGfeFee ? m_dataLoan.sU4ScProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4ScProps, m_dataLoan.sSettlementU4ScProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU4Sc_rep : null, m_dataLoan.sU4ScPaidTo, requireGfeDisclosedAmt);
                    break;
                case "1307":
                    ret = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU5ScGfeSection : m_dataLoan.sU5ScSettlementSection), E_RespaFeeType.Other,
                        "1307" /* m_dataLoan.sU5ScCode */,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU5ScDesc : m_dataLoan.sSettlementU5ScDesc,
                        false,
                        useGfeFee ? m_dataLoanWithGfeArchiveApplied.sU5Sc_rep : m_dataLoan.sSettlementU5Sc_rep,
                        useGfeFee ? m_dataLoan.sU5ScProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU5ScProps, m_dataLoan.sSettlementU5ScProps),
                        m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sU5Sc_rep : null, m_dataLoan.sU5ScPaidTo, requireGfeDisclosedAmt);
                    break;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic,
                        "Line " + lineNumber + " is not unsupported.");
            }

            if (!useGfeFee)
            {
                // Bypass the check here. We want to get the GFE version even if the amount is 0 and GFE disclosed amount is 0.
                var gfeFee = GetRespaFeeForHudLine(lineNumber, true, true);

                var gfeFeeAmtStr = gfeFee.PaymentList.First().Amount;
                var scFeeAmtStr = ret.PaymentList.First().Amount;

                var gfeFeeAmt = m_dataLoan.m_convertLos.ToMoney(gfeFeeAmtStr);
                var scFeeAmt = m_dataLoan.m_convertLos.ToMoney(scFeeAmtStr);

                if (gfeFeeAmt != 0 && scFeeAmt != 0)
                {
                    if (gfeFee.TypeOtherDescription != ret.TypeOtherDescription)
                    {
                        m_feeDiscrepancies.Add(string.Format("The description for the fee on line {0} does not"
                            + " match between the GFE and Settlement Charges.", lineNumber));
                    }
                    if (gfeFee.GfeAggregationType != ret.GfeAggregationType)
                    {
                        m_feeDiscrepancies.Add(string.Format("The GFE box for the fee on line {0} does not"
                            + " match between the GFE and Settlement Charges.", lineNumber));
                    }
                }
                else if (gfeFeeAmt != 0 && scFeeAmt == 0)
                {
                    ret.TypeOtherDescription = gfeFee.TypeOtherDescription;
                    ret.GfeAggregationType = gfeFee.GfeAggregationType;
                }
                else if (gfeFeeAmt == 0 && scFeeAmt != 0)
                {
                    ret.GfeDisclosedAmount = m_dataLoan.m_convertLos.ToMoneyString(0, FormatDirection.ToRep);
                }
            }

            // 12/17/2013 gf - opm 146376 This logic is taken from CreateRespaFee,
            // if it is updated there it should most likely be updated here, too.
            if (!bypassGfeDiscAmtPmtAmtCheck)
            {
                var paymentAmt = ret.PaymentList.FirstOrDefault().Amount;
                if ((string.IsNullOrEmpty(paymentAmt) || paymentAmt == "0.00") &&
                     ((string.IsNullOrEmpty(ret.GfeDisclosedAmount) || ret.GfeDisclosedAmount == "0.00")))
                {
                    return null;
                }
            }

            return ret;
        }

        private E_RespaFeeType GetRespaFeeType_mi902Fee()
        {
            switch (m_dataLoan.sLT)
            {
                case E_sLT.FHA: return E_RespaFeeType.MI_FHAUpfrontPremium;
                case E_sLT.VA: return E_RespaFeeType.VAFundingFee;
                case E_sLT.UsdaRural: return E_RespaFeeType.RuralHousingFee;
                case E_sLT.Other:
                case E_sLT.Conventional: return E_RespaFeeType.MIInitialPremium;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sLT);
            }
        }

        private void PopulateLoanOriginatorList(List<LoanOriginator> loanOriginatorList)
        {
            // 11/11/2010 dd - OPM 59574 - Support nationwide mortgage licensing system.
            // 11/11/2010 dd - Order of these two objects are important. The first record must be company information.
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            LoanOriginator originatingCompany = new LoanOriginator();
            originatingCompany.UnparsedName = preparer.CompanyName;
            originatingCompany.NonPersonEntityIndicator = E_YNIndicator.Y;
            originatingCompany.NationwideMortgageLicensingSystemAssignedIdentifier = preparer.CompanyLoanOriginatorIdentifier;
            originatingCompany.StreetAddress = preparer.StreetAddr;
            originatingCompany.City = preparer.City;
            originatingCompany.State = preparer.State;
            originatingCompany.PostalCode = preparer.Zip;
            originatingCompany.ContactDetail.Name = preparer.PreparerName;
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.PhoneOfCompany));
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxOfCompany));
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));
            originatingCompany.LicenseNumberIdentifier = IgnoreNotApplicable(preparer.LicenseNumOfCompany);

            loanOriginatorList.Add(originatingCompany);

            LoanOriginator loanOriginator = new LoanOriginator();
            loanOriginator.UnparsedName = preparer.PreparerName;
            loanOriginator.NonPersonEntityIndicator = E_YNIndicator.N;
            loanOriginator.NationwideMortgageLicensingSystemAssignedIdentifier = preparer.LoanOriginatorIdentifier;
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.Phone));
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxNum));
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));
            loanOriginator.LicenseNumberIdentifier = IgnoreNotApplicable(preparer.LicenseNumOfAgent);

            loanOriginatorList.Add(loanOriginator);
        }

        /// <summary>
        /// Returns empty string for "n/a" or "na", case insensitive; value otherwise
        /// </summary>
        private static string IgnoreNotApplicable(string value)
        {
            if (string.IsNullOrEmpty(value)
                || value.Equals("n/a", StringComparison.OrdinalIgnoreCase)
                || value.Equals("na", StringComparison.OrdinalIgnoreCase))
            {
                return string.Empty;
            }

            return value;
        }

        private DataInformation CreateDataInformation()
        {
            DataInformation dataInformation = new DataInformation();
            //dataInformation.Id = null;
            //dataInformation.DataVersionList.Add(CreateDataVersion());
            return dataInformation;
        }

        private DataVersion CreateDataVersion()
        {
            DataVersion dataVersion = new DataVersion();
            //dataVersion.Id = null;
            //dataVersion.Name = null;
            //dataVersion.Number = null;
            return dataVersion;
        }

        private AdditionalCaseData CreateAdditionalCaseData()
        {
            AdditionalCaseData additionalCaseData = new AdditionalCaseData();
            //additionalCaseData.Id = null;
            //additionalCaseData.MortgageScoreList.Add(CreateMortgageScore());
            additionalCaseData.TransmittalData = CreateTransmittalData();
            return additionalCaseData;
        }

        private MortgageScore CreateMortgageScore()
        {
            MortgageScore mortgageScore = new MortgageScore();
            //mortgageScore.Id = null;
            //mortgageScore.Date = null;
            //mortgageScore.Type = null;
            //mortgageScore.TypeOtherDescription = null;
            //mortgageScore.Value = null;
            return mortgageScore;
        }

        private TransmittalData CreateTransmittalData()
        {
            TransmittalData transmittalData = new TransmittalData();
            transmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprVal_rep;
            transmittalData.RateLockPeriodDays = m_dataLoan.sRLckdDays_rep;
            // 7/14/2011 dd - Based from Request for DocMagic

            switch (m_dataLoan.sBranchChannelT)
            {
                case E_BranchChannelT.Blank:
                case E_BranchChannelT.Retail:
                case E_BranchChannelT.Wholesale:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Lender;
                    break;
                case E_BranchChannelT.Correspondent:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Correspondent;
                    break;
                case E_BranchChannelT.Broker:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Broker;
                    break;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sBranchChannelT);
            }

            //transmittalData.Id = null;
            //transmittalData.ArmsLengthIndicator = null;
            //transmittalData.BelowMarketSubordinateFinancingIndicator = null;
            //transmittalData.BuydownRatePercent = null;
            //transmittalData.CaseStateType = null;
            //transmittalData.CaseStateTypeOtherDescription = null;
            //transmittalData.CommitmentReferenceIdentifier = null;
            //transmittalData.ConcurrentOriginationIndicator = null;
            //transmittalData.ConcurrentOriginationLenderIndicator = null;
            //transmittalData.CreditReportAuthorizationIndicator = null;
            //transmittalData.CurrentFirstMortgageHolderType = null;
            //transmittalData.CurrentFirstMortgageHolderTypeOtherDescription = null;
            //transmittalData.InvestorInstitutionIdentifier = null;
            //transmittalData.InvestorLoanIdentifier = null;
            //transmittalData.LenderBranchIdentifier = null;
            //transmittalData.LenderRegistrationIdentifier = null;
            //transmittalData.LoanOriginationSystemLoanIdentifier = null;
            //transmittalData.LoanOriginatorTypeOtherDescription = null;
            //transmittalData.PropertiesFinancedByLenderCount = null;
            //transmittalData.PropertyEstimatedValueAmount = null;
            //transmittalData.RateLockRequestedExtensionDays = null;
            //transmittalData.RateLockType = null;
            return transmittalData;
        }

        private AffordableLending CreateAffordableLending()
        {
            AffordableLending affordableLending = new AffordableLending();
            //affordableLending.Id = null;
            //affordableLending.FnmCommunityLendingProductName = null;
            //affordableLending.FnmCommunityLendingProductType = null;
            //affordableLending.FnmCommunityLendingProductTypeOtherDescription = null;
            //affordableLending.FnmCommunitySecondsIndicator = null;
            //affordableLending.FnmNeighborsMortgageEligibilityIndicator = null;
            //affordableLending.FreAffordableProgramIdentifier = null;
            //affordableLending.HudIncomeLimitAdjustmentFactor = null;
            //affordableLending.HudLendingIncomeLimitAmount = null;
            //affordableLending.HudMedianIncomeAmount = null;
            affordableLending.MsaIdentifier = this.m_dataLoan.sHmdaMsaNum;
            return affordableLending;
        }

        private Asset CreateAsset(IAssetRetirement retirement, string borrowerId)
        {
            if (retirement == null) { return null; }
            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.RetirementFund,
                CashOrMarketValueAmount = retirement.Val_rep
            };
            return asset;
        }
        private Asset CreateAsset(IAssetBusiness business, string borrowerId)
        {
            if (null == business) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.NetWorthOfBusinessOwned,
                CashOrMarketValueAmount = business.Val_rep,

            };
            return asset;
        }
        private Asset CreateAsset(IAssetLifeInsurance lifeIns, string borrowerId)
        {
            if (null == lifeIns) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.LifeInsurance,
                CashOrMarketValueAmount = lifeIns.Val_rep,
                LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep
            };
            return asset;
        }
        private Asset CreateAsset(IAssetCashDeposit cashDeposit, string borrowerId)
        {
            if (null == cashDeposit) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.EarnestMoneyCashDepositTowardPurchase,
                CashOrMarketValueAmount = cashDeposit.Val_rep,
                HolderName = cashDeposit.Desc
            };
            return asset;
        }
        private Asset CreateAsset(IAssetRegular assetField, CAppData dataApp)
        {
            if (null == assetField) return null;
            Asset asset = new Asset()
            {
                AccountIdentifier = assetField.AccNum.Value,
                Type = ToMismo(assetField.AssetT),
                CashOrMarketValueAmount = assetField.Val_rep,
                HolderName = assetField.ComNm,
                HolderStreetAddress = assetField.StAddr,
                HolderCity = assetField.City,
                HolderState = assetField.State,
                HolderPostalCode = assetField.Zip,
                OtherAssetTypeDescription = assetField.OtherTypeDesc
            };
            if (assetField.OwnerT == E_AssetOwnerT.Borrower)
            {
                asset.BorrowerId = dataApp.aBMismoId;
            }
            else if (assetField.OwnerT == E_AssetOwnerT.CoBorrower)
            {
                asset.BorrowerId = dataApp.aCMismoId;
            }
            else if (assetField.OwnerT == E_AssetOwnerT.Joint)
            {
                if (dataApp.aCIsValidNameSsn)
                {
                    asset.BorrowerId = dataApp.aBMismoId + " " + dataApp.aCMismoId;
                }
                else
                {
                    asset.BorrowerId = dataApp.aBMismoId;
                }
            }

            if (assetField.AssetT == E_AssetRegularT.Auto)
            {
                asset.AutomobileMakeDescription = assetField.Desc;
            }


            return asset;
        }

        private E_AssetType ToMismo(E_AssetRegularT assetRegularT)
        {
            switch (assetRegularT)
            {
                case E_AssetRegularT.Auto: return E_AssetType.Automobile;
                case E_AssetRegularT.Bonds: return E_AssetType.Bond;
                case E_AssetRegularT.Checking: return E_AssetType.CheckingAccount;
                case E_AssetRegularT.GiftFunds: return E_AssetType.GiftsNotDeposited;
                case E_AssetRegularT.Savings: return E_AssetType.SavingsAccount;
                case E_AssetRegularT.Stocks: return E_AssetType.Stock;
                case E_AssetRegularT.OtherIlliquidAsset: return E_AssetType.OtherNonLiquidAssets;
                case E_AssetRegularT.OtherLiquidAsset: return E_AssetType.OtherLiquidAssets;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets: return E_AssetType.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.GiftEquity: return E_AssetType.GiftsTotal;
                case E_AssetRegularT.CertificateOfDeposit: return E_AssetType.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.MoneyMarketFund: return E_AssetType.MoneyMarketFund;
                case E_AssetRegularT.MutualFunds: return E_AssetType.MutualFund;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit: return E_AssetType.SecuredBorrowedFundsNotDeposited;
                case E_AssetRegularT.BridgeLoanNotDeposited: return E_AssetType.BridgeLoanNotDeposited;
                case E_AssetRegularT.TrustFunds: return E_AssetType.TrustAccount;
                default:
                    return E_AssetType.Undefined;
            }
        }
        private DownPayment CreateDownPayment()
        {
            if (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
                return null;

            DownPayment downPayment = new DownPayment();
            //downPayment.Id = null;
            downPayment.Amount = m_dataLoan.sEquityCalc_rep;
            downPayment.SourceDescription = m_dataLoan.sDwnPmtSrcExplain;
            downPayment.Type = ToMismoDownPaymentType(m_dataLoan.sDwnPmtSrc);
            //downPayment.TypeOtherDescription = null;
            return downPayment;
        }

        private E_DownPaymentType ToMismoDownPaymentType(string src)
        {
            switch (src.ToLower())
            {
                case "checking/savings": return E_DownPaymentType.CheckingSavings;
                case "gift funds": return E_DownPaymentType.GiftFunds;
                case "stocks & bonds": return E_DownPaymentType.StocksAndBonds;
                case "lot equity": return E_DownPaymentType.LotEquity;
                case "bridge loan": return E_DownPaymentType.BridgeLoan;
                case "trust funds": return E_DownPaymentType.TrustFunds;
                case "retirement funds": return E_DownPaymentType.RetirementFunds;
                case "life insurance cash value": return E_DownPaymentType.LifeInsuranceCashValue;
                case "sale of chattel": return E_DownPaymentType.SaleOfChattel;
                case "trade equity": return E_DownPaymentType.TradeEquity;
                case "sweat equity": return E_DownPaymentType.SweatEquity;
                case "cash on hand": return E_DownPaymentType.CashOnHand;
                case "deposit on sales contract": return E_DownPaymentType.DepositOnSalesContract;
                case "equity from pending sale": return E_DownPaymentType.EquityOnPendingSale;
                case "equity from subject property": return E_DownPaymentType.EquityOnSubjectProperty;
                case "equity on sold property": return E_DownPaymentType.EquityOnSoldProperty;
                case "other type of down payment": return E_DownPaymentType.OtherTypeOfDownPayment;
                case "rent with option to purchase": return E_DownPaymentType.RentWithOptionToPurchase;
                case "secured borrowed funds": return E_DownPaymentType.SecuredBorrowedFunds;
                case "unsecured borrowed funds": return E_DownPaymentType.UnsecuredBorrowedFunds;
                case "forgivable secured loan": return E_DownPaymentType.ForgivableSecuredLoan;

                default:
                    return E_DownPaymentType.Undefined;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="props">An integer (?!) representing the Paid By and checkboxes on the rightmost columns of the GFE</param>
        /// <returns></returns>
        private Escrow CreateEscrow(string hudLineNumber, string otherDesc, string monthlyPaymentAmount_rep, decimal monthlyPaymentAmount, string numOfMonths, string premiumAmount, int props, string gfeDisclosedAmount, string paidToOverrideString, int cushionNumberOfMonths)
        {
            if (monthlyPaymentAmount == 0 && (string.IsNullOrEmpty(gfeDisclosedAmount) || gfeDisclosedAmount == "0.00"))
                return null;

            int colIndex = -1;
            Escrow escrow = new Escrow();
            escrow.AccountSummary = CreateAccountSummary(cushionNumberOfMonths);
            bool isPremium = false;
            E_EscrowItemType itemType = E_EscrowItemType.Undefined;
            switch (hudLineNumber)
            {
                case "903":
                    itemType = E_EscrowItemType.HazardInsurance;
                    isPremium = true;
                    if (string.IsNullOrEmpty(paidToOverrideString) == false)
                    {
                        escrow.PaidTo = CreatePaidTo(paidToOverrideString);
                    }
                    else
                    {
                        escrow.PaidTo = CreatePaidTo(E_AgentRoleT.HazardInsurance);
                    }
                    break;
                case "904":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case "905":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case "906":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case HUDLines.HAZ_INS_RESERVE:
                    itemType = E_EscrowItemType.HazardInsurance;
                    escrow.PaidTo = CreatePaidTo(E_AgentRoleT.HazardInsurance);
                    colIndex = 1; // HAZARD
                    break;
                case HUDLines.SCHOOL_TAXES:
                    itemType = E_EscrowItemType.SchoolPropertyTax;
                    colIndex = 4; // SCHOOL TAX
                    break;
                case HUDLines.MTG_INS_RESERVE:
                    itemType = E_EscrowItemType.MortgageInsurance;
                    colIndex = 2; // MORTGAGE INSURANCE
                    break;
                case HUDLines.REAL_ESTATE_TAX_RESERVE:
                    itemType = E_EscrowItemType.CountyPropertyTax;
                    colIndex = 0; // REAL ESTATE TAX
                    break;
                case HUDLines.FLOOD_INS_RESERVE:
                    itemType = E_EscrowItemType.FloodInsurance;
                    colIndex = 3; // FLOOD
                    escrow.PaidTo = CreatePaidTo(E_AgentRoleT.FloodProvider);
                    break;
                case HUDLines.CUSTOM_1008:
                    itemType = E_EscrowItemType.Other;
                    colIndex = 5; // OTHER 1
                    break;
                case HUDLines.CUSTOM_1009:
                    itemType = E_EscrowItemType.Other;
                    colIndex = 6; // OTHER 2
                    break;
                case HUDLines.CUSTOM_1010:
                    itemType = E_EscrowItemType.Other;
                    colIndex = 7; // OTHER 3
                    break;
                case HUDLines.CUSTOM_1011:
                    itemType = E_EscrowItemType.Other;
                    colIndex = 8; // OTHER 4
                    break;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "CreateEscrow does not have well-defined behavior for hudline '" + hudLineNumber + "'");
            }

            escrow.SpecifiedHud1LineNumber = hudLineNumber;
            escrow.ItemType = itemType;
            escrow.ItemTypeOtherDescription = otherDesc;

            if (isPremium)
            {
                escrow.PremiumAmount = premiumAmount;
                escrow.PremiumDurationMonthsCount = numOfMonths;

                if (string.IsNullOrEmpty(gfeDisclosedAmount) == false)
                {
                    escrow.GfeDisclosedPremiumAmount = gfeDisclosedAmount;
                }

                int paidBy = LosConvert.GfeItemProps_Payer(props);
                switch (paidBy)
                {
                    case LosConvert.BORR_PAID_OUTOFPOCKET:
                    case LosConvert.BORR_PAID_FINANCED:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Buyer;
                        break;
                    case LosConvert.SELLER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Seller;
                        break;
                    case LosConvert.LENDER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Lender;
                        break;
                    case LosConvert.BROKER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Other;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                escrow.MonthlyPaymentAmount = monthlyPaymentAmount_rep;
                escrow.CollectedNumberOfMonthsCount = numOfMonths;
                escrow.AnnualPaymentAmount = m_dataLoan.m_convertLos.ToMoneyString(monthlyPaymentAmount * 12, FormatDirection.ToRep);

                bool hasSchedDueD1 = false;
                DateTime sSchedDueD1 = DateTime.MinValue;

                try
                {
                    sSchedDueD1 = m_dataLoan.sSchedDueD1.DateTimeForComputation;
                    hasSchedDueD1 = true;
                }
                catch (CBaseException)
                {
                    // It is okay to swallow because sSchedDueD1 is missing.
                }
                catch (FormatException)
                {
                    // It is okay to swallow because sSchedDueD1 is missing.
                }
                catch (Exception e)
                {
                    Tools.LogError("DocMagicMismoExporter:: Error trying to get sSchedDueD1 - previously was being swallowed on purpose.", e);
                    throw;
                }

                if (hasSchedDueD1)
                {
                    int[,] sInitialEscrowAcc = m_dataLoan.sInitialEscrowAcc;
                    int index = 1;
                    for (int offset = 0; offset < 12; offset++)
                    {
                        DateTime dueDate = sSchedDueD1.AddMonths(offset);
                        int nMonths = sInitialEscrowAcc[dueDate.Month, colIndex];
                        if (nMonths > 0)
                        {
                            decimal amount = (decimal)nMonths * monthlyPaymentAmount;
                            escrow.PaymentsList.Add(CreatePayments(m_dataLoan.m_convertLos.ToDateTimeString(dueDate), m_dataLoan.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep), index.ToString()));
                            index++;
                        }
                    }
                }

                int paidBy = LosConvert.GfeItemProps_Payer(props);
                switch (paidBy)
                {
                    case LosConvert.BORR_PAID_OUTOFPOCKET:
                    case LosConvert.BORR_PAID_FINANCED:
                        escrow.PaidByType = E_EscrowPaidByType.Buyer;
                        break;
                    case LosConvert.SELLER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.Seller;
                        break;
                    case LosConvert.LENDER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.LenderPremium;
                        break;
                    case LosConvert.BROKER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.Other;
                        break;
                    default:
                        break;
                }

                if (vendorIsIDS)
                {
                    bool thisPartyIsAffiliate = LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
                    bool paidToThirdParty = LosConvert.GfeItemProps_PaidToThirdParty(props);
                    if (thisPartyIsAffiliate) escrow.PaidToType = E_EscrowPaidToType.AffiliateOfLender;
                    else if (paidToThirdParty) escrow.PaidToType = E_EscrowPaidToType.Other;
                    else escrow.PaidToType = E_EscrowPaidToType.Lender;
                }
            }

            return escrow;
        }

        private AccountSummary CreateAccountSummary(int cushionNumberOfMonths)
        {
            if (cushionNumberOfMonths <= 0)
            {
                return null;
            }
            AccountSummary accountSummary = new AccountSummary();
            //accountSummary.Id = null;
            //accountSummary.EscrowAggregateAccountingAdjustmentAmount = null;
            accountSummary.EscrowCushionNumberOfMonthsCount = cushionNumberOfMonths.ToString();
            return accountSummary;
        }

        private Payments CreatePayments(string dueDate, string paymentAmount, string sequenceIdentifier)
        {
            Payments payments = new Payments();

            payments.DueDate = dueDate;
            payments.PaymentAmount = paymentAmount;
            payments.SequenceIdentifier = sequenceIdentifier;

            //payments.Id = null;
            return payments;
        }

        // OPM 43776
        private PaidTo CreatePaidTo(string paidToName)
        {
            PaidTo paidTo = new PaidTo();
            paidTo.Name = paidToName;
            return paidTo;
        }

        private PaidTo CreatePaidTo(E_AgentRoleT agentRole)
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            PaidTo paidTo = new PaidTo();
            //paidTo.Id = null;
            //paidTo.NonPersonEntityIndicator = null;
            paidTo.City = agent.City;
            //paidTo.Country = null;
            //paidTo.County = null;
            paidTo.Name = agent.CompanyName;
            paidTo.PostalCode = agent.Zip;
            paidTo.State = agent.State;
            paidTo.StreetAddress = agent.StreetAddr;
            //paidTo.StreetAddress2 = null;
            paidTo.ContactDetail = new ContactDetail();

            paidTo.ContactDetail.Name = agent.AgentName;
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Phone, Value = agent.Phone });
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Fax, Value = agent.FaxNum });
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Email, Value = agent.EmailAddr });


            //paidTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return paidTo;
        }
        private ContactDetail CreateContactDetail(IPreparerFields preparer)
        {
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = preparer.PreparerName;

            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.Phone));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxNum));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));

            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            return contactDetail;
        }
        private ContactDetail CreateContactDetail(CAgentFields agent)
        {
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = agent.AgentName;

            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, agent.Phone));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, agent.FaxNum));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, agent.EmailAddr));

            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            return contactDetail;
        }
        private ContactDetail CreateContactDetailAssumeRoleTypeWork(CAgentFields agent)
        {
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = agent.AgentName;

            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Phone, agent.Phone));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Fax, agent.FaxNum));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Email, agent.EmailAddr));

            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            return contactDetail;
        }

        private Mismo.Closing2_6.ContactPoint CreateContactPoint(E_ContactPointRoleType roleType, Mismo.Closing2_6.E_ContactPointType type, string value)
        {
            if (null == value || "" == value)
                return null;
            Mismo.Closing2_6.ContactPoint contactPoint = new Mismo.Closing2_6.ContactPoint();
            contactPoint.RoleType = roleType;
            contactPoint.Type = type;
            contactPoint.Value = value;

            //contactPoint.Id = null;
            //contactPoint.PreferenceIndicator = null;
            //contactPoint.RoleTypeOtherDescription = null;
            //contactPoint.TypeOtherDescription = null;
            return contactPoint;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail()
        {
            NonPersonEntityDetail nonPersonEntityDetail = new NonPersonEntityDetail();
            //nonPersonEntityDetail.Id = null;
            //nonPersonEntityDetail.MersOrganizationIdentifier = null;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription = null;
            //nonPersonEntityDetail.OrganizationType = null;
            //nonPersonEntityDetail.OrganizationTypeOtherDescription = null;
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = null;
            //nonPersonEntityDetail.SuccessorClauseTextDescription = null;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier = null;
            //nonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative());
            return nonPersonEntityDetail;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail(CAgentFields agentFields)
        {
            NonPersonEntityDetail nonPersonEntityDetail = new NonPersonEntityDetail();
            //nonPersonEntityDetail.Id = null;
            //nonPersonEntityDetail.MersOrganizationIdentifier = null;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription = null;
            //nonPersonEntityDetail.OrganizationType = null;
            //nonPersonEntityDetail.OrganizationTypeOtherDescription = null;
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = null;
            //nonPersonEntityDetail.SuccessorClauseTextDescription = null;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier = null;
            nonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative(agentFields));
            return nonPersonEntityDetail;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail(E_SellerEntityType sellerEntityType)
        {
            if (sellerEntityType == E_SellerEntityType.Undefined) return null;
            NonPersonEntityDetail detail = new NonPersonEntityDetail();
            detail.OrganizationType = ToMismo(sellerEntityType);
            if (detail.OrganizationType == E_NonPersonEntityDetailOrganizationType.Other)
            {
                detail.OrganizationTypeOtherDescription = sellerEntityType.ToString();
            }
            return detail;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetailBorrower()
        {
            NonPersonEntityDetail nonPersonEntityDetail = new NonPersonEntityDetail();
            //nonPersonEntityDetail.Id = null;
            //nonPersonEntityDetail.MersOrganizationIdentifier = null;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription = null;
            nonPersonEntityDetail.OrganizationType = E_NonPersonEntityDetailOrganizationType.Other;
            nonPersonEntityDetail.OrganizationTypeOtherDescription = "Individual";
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = null;
            //nonPersonEntityDetail.SuccessorClauseTextDescription = null;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier = null;
            //nonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative());
            return nonPersonEntityDetail;
        }

        private AuthorizedRepresentative CreateAuthorizedRepresentative(CAgentFields agentFields)
        {
            AuthorizedRepresentative authorizedRepresentative = new AuthorizedRepresentative();
            //authorizedRepresentative.Id = null;
            //authorizedRepresentative.AuthorizedToSignIndicator = null;
            //authorizedRepresentative.TitleDescription = null;
            //authorizedRepresentative.UnparsedName = null;
            authorizedRepresentative.ContactDetail = CreateContactDetail(agentFields);
            return authorizedRepresentative;
        }

        private GovernmentLoan CreateGovernmentLoan(CAppData pageData)
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.VA)
                return null;

            GovernmentLoan governmentLoan = new GovernmentLoan();

            governmentLoan.FhaLoan = CreateFhaLoan();
            if (m_dataLoan.sLT == E_sLT.VA)
            {
                governmentLoan.VaLoan = CreateVaLoan(pageData);
            }
            governmentLoan.FhaVaLoan = CreateFhaVaLoan();


            //governmentLoan.Id = null;

            return governmentLoan;
        }

        private FhaLoan CreateFhaLoan()
        {
            FhaLoan fhaLoan = new FhaLoan();

            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                fhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount = m_dataLoan.sFHAEnergyEffImprov_rep;
                fhaLoan.FhaUpfrontMiPremiumPercent = m_dataLoanWithGfeArchiveApplied.sFfUfmipR_rep;
                fhaLoan.FhaMiPremiumRefundAmount = m_dataLoan.sFHA203kFHAMipRefund_rep;
                fhaLoan.SectionOfActType = ToMismoLoanSectionOfAct(m_dataLoan.sFHAHousingActSection);
                fhaLoan.FhaCoverageRenewalRatePercent = m_dataLoan.sProMInsR_rep;
            }

            fhaLoan.LenderIdentifier = m_dataLoan.sFHALenderIdCode;
            fhaLoan.SponsorIdentifier = m_dataLoan.sFHASponsorAgentIdCode;

            //fhaLoan.Id = null;
            //fhaLoan.BorrowerFinancedFhaDiscountPointsAmount = null;
            //fhaLoan.BorrowerHomeInspectionChosenIndicator = null;
            //fhaLoan.DaysToFhaMiEligibilityCount = null;
            //fhaLoan.FhaAlimonyLiabilityTreatmentType = null;

            //fhaLoan.FhaGeneralServicesAdministrationCodeIdentifier = null;
            //fhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier = null;
            //fhaLoan.FhaLimitedDenialParticipationIdentifier = null;
            //fhaLoan.FhaNonOwnerOccupancyRiderRule248Indicator = null;
            //fhaLoan.FhaRefinanceInterestOnExistingLienAmount = null;
            //fhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier = null;
            //fhaLoan.FhaRefinanceOriginalExistingUpFrontMipAmount = null;
            //fhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier = null;
            //fhaLoan.FhaUpfrontMiPremiumPercent = null;

            //fhaLoan.HudAdequateAvailableAssetsIndicator = null;
            //fhaLoan.HudAdequateEffectiveIncomeIndicator = null;
            //fhaLoan.HudCreditCharacteristicsIndicator = null;
            //fhaLoan.HudStableEffectiveIncomeIndicator = null;

            //fhaLoan.SectionOfActTypeOtherDescription = null;
            //fhaLoan.SoldUnderHudSingleFamilyPropertyDispositionProgramIndicator = null;
            IPreparerFields fhaAddendumSponsor = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (fhaAddendumSponsor.IsValid)
            {
                fhaLoan.Sponsor = CreateSponsor(fhaAddendumSponsor);
            }
            return fhaLoan;
        }

        private Sponsor CreateSponsor(IPreparerFields preparerFields)
        {
            Sponsor sponsor = new Sponsor();
            sponsor.UnparsedName = preparerFields.CompanyName;
            sponsor.StreetAddress = preparerFields.StreetAddr;
            sponsor.City = preparerFields.City;
            sponsor.State = preparerFields.State;
            sponsor.PostalCode = preparerFields.Zip;
            sponsor.ContactDetail = CreateContactDetail(preparerFields);
            return sponsor;
        }

        private E_FhaLoanSectionOfActType ToMismoLoanSectionOfAct(string type)
        {
            switch (type.ToLower())
            {
                case "184": return E_FhaLoanSectionOfActType._184;
                case "203(b)": return E_FhaLoanSectionOfActType._203B;
                case "203(b)2": return E_FhaLoanSectionOfActType._203B2;
                case "203(b)/251": return E_FhaLoanSectionOfActType._203B251;
                case "203(h)": return E_FhaLoanSectionOfActType._203H;
                case "203(k)": return E_FhaLoanSectionOfActType._203K;
                case "203(k)/251": return E_FhaLoanSectionOfActType._203K251;
                case "234(c)": return E_FhaLoanSectionOfActType._234C;
                case "234(c)/251": return E_FhaLoanSectionOfActType._234C251;
                case "248": return E_FhaLoanSectionOfActType._248;
                case "251": return E_FhaLoanSectionOfActType._251;
                case "255": return E_FhaLoanSectionOfActType._255;
                default: return E_FhaLoanSectionOfActType.Undefined;
            }
        }
        //todo THIS IS NULL in Closing231
        private FhaVaLoan CreateFhaVaLoan()
        {
            FhaVaLoan fhaVaLoan = new FhaVaLoan();

            //fhaVaLoan.Id = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.GovernmentMortgageCreditCertificateAmount = null;
            //fhaVaLoan.GovernmentRefinanceTypeOtherDescription = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.PropertyEnergyEfficientHomeIndicator = null;
            //fhaVaLoan.SellerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.OriginatorIdentifier = null;

            if (m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                fhaVaLoan.GovernmentRefinanceType = E_FhaVaLoanGovernmentRefinanceType.InterestRateReductionRefinanceLoan;
            }
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                if (m_dataLoan.sHasAppraisal)
                {
                    fhaVaLoan.GovernmentRefinanceType = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithAppraisal;
                }
                else
                {
                    fhaVaLoan.GovernmentRefinanceType = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithoutAppraisal;
                }
            }
            else
            {
                fhaVaLoan.GovernmentRefinanceType = E_FhaVaLoanGovernmentRefinanceType.FullDocumentation;
            }

            return fhaVaLoan;
        }

        private VaLoan CreateVaLoan(CAppData dataApp)
        {
            VaLoan vaLoan = new VaLoan();

            vaLoan.VaEntitlementAmount = dataApp.aVaEntitleAmt_rep;
            vaLoan.VaEntitlementCodeIdentifier = dataApp.aVaEntitleCode;
            vaLoan.VaMaintenanceExpenseMonthlyAmount = m_dataLoan.sVaProMaintenancePmt_rep;
            vaLoan.VaResidualIncomeAmount = dataApp.aVaFamilySupportBal_rep;
            vaLoan.VaUtilityExpenseMonthlyAmount = m_dataLoan.sVaProUtilityPmt_rep;
            //vaLoan.Id = null;
            vaLoan.BorrowerFundingFeePercent = (m_useGfeFee || use2015DataLayer ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sFfUfmipR_rep;
            //vaLoan.VaBorrowerCoBorrowerMarriedIndicator = null;
            //vaLoan.VaHouseholdSizeCount = null;
            //vaLoan.VaResidualIncomeAmount = null;
            //vaLoan.VaUtilityExpenseMonthlyAmount = null;
            return vaLoan;
        }

        private GovernmentReporting CreateGovernmentReporting()
        {
            GovernmentReporting governmentReporting = new GovernmentReporting();
            //governmentReporting.Id = null;


            governmentReporting.HmdaHoepaLoanStatusIndicator = ToMismo(m_dataLoan.sHmdaReportAsHoepaLoan);
            governmentReporting.HmdaPreapprovalType = ToMismo(m_dataLoan.sHmdaPreapprovalT);
            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                    governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.HomePurchase;
                    break;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    if (m_dataLoan.sHmdaReportAsHomeImprov)
                    {
                        governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.HomeImprovement;
                    }
                    else
                    {
                        governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing;
                    }
                    break;
                default:
                    governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing;
                    break;
            }


            governmentReporting.HmdaRateSpreadPercent = m_dataLoan.sHmdaAprRateSpread;


            return governmentReporting;
        }

        private E_GovernmentReportingHmdaPreapprovalType ToMismo(E_sHmdaPreapprovalT hmdaPreapprovalT)
        {
            switch (hmdaPreapprovalT)
            {
                case E_sHmdaPreapprovalT.LeaveBlank: return E_GovernmentReportingHmdaPreapprovalType.Undefined;
                case E_sHmdaPreapprovalT.NotApplicable: return E_GovernmentReportingHmdaPreapprovalType.NotApplicable;
                case E_sHmdaPreapprovalT.PreapprovalNotRequested: return E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasNotRequested;
                case E_sHmdaPreapprovalT.PreapprovalRequested: return E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasRequested;
                default:
                    LogInvalidEnum("E_sHmdaPreapprovalT", hmdaPreapprovalT);
                    return E_GovernmentReportingHmdaPreapprovalType.Undefined;
            }
        }

        private InterviewerInformation CreateInterviewerInformation()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            InterviewerInformation interviewerInformation = new InterviewerInformation();
            //interviewerInformation.Id = null;
            interviewerInformation.ApplicationTakenMethodType = ToMismo(m_dataLoan.GetAppData(0).aIntrvwrMethodT);
            interviewerInformation.InterviewerApplicationSignedDate = m_dataLoan.GetAppData(0).a1003InterviewD_rep;
            interviewerInformation.InterviewersEmployerCity = preparer.City;
            interviewerInformation.InterviewersEmployerName = preparer.CompanyName;
            interviewerInformation.InterviewersEmployerPostalCode = preparer.Zip;
            interviewerInformation.InterviewersEmployerState = preparer.State;
            interviewerInformation.InterviewersEmployerStreetAddress = preparer.StreetAddr;
            //interviewerInformation.InterviewersEmployerStreetAddress2 = null;
            interviewerInformation.InterviewersName = preparer.PreparerName;
            interviewerInformation.InterviewersTelephoneNumber = preparer.Phone;
            return interviewerInformation;
        }

        private string GenerateLiabilityId(string basicId, int appNumber)
        {
            return basicId + appNumber;
        }

        private Liability CreateLiability(ILiabilityAlimony alimony, string borrowerId, int appNumber)
        {
            if (null == alimony)
                return null;

            Liability liability = new Liability()
            {
                Id = GenerateLiabilityId("LIA_" + alimony.RecordId.ToString("N"), appNumber),
                BorrowerId = borrowerId,
                Type = E_LiabilityType.Alimony,
                AlimonyOwedToName = alimony.OwedTo,
                RemainingTermMonths = alimony.RemainMons_rep,
                MonthlyPaymentAmount = alimony.Pmt_rep
            };
            return liability;
        }
        private Liability CreateLiability(ILiabilityChildSupport childSupport, string borrowerId, int appNumber)
        {
            if (null == childSupport)
                return null;

            Liability liability = new Liability()
            {
                Id = GenerateLiabilityId("LIA_" + childSupport.RecordId.ToString("N"), appNumber),
                BorrowerId = borrowerId,
                Type = E_LiabilityType.ChildSupport,
                RemainingTermMonths = childSupport.RemainMons_rep,
                MonthlyPaymentAmount = childSupport.Pmt_rep
            };
            return liability;
        }
        private Liability CreateLiability(ILiabilityJobExpense jobExpense, string borrowerId, int appNumber)
        {
            if (null == jobExpense)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + jobExpense.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.JobRelatedExpenses;
            liability.HolderName = jobExpense.ExpenseDesc;
            liability.MonthlyPaymentAmount = jobExpense.Pmt_rep;
            return liability;
        }
        private Liability CreateLiability(ILiabilityRegular field, CAppData dataApp, int appNumber)
        {
            if (null == field)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + field.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = field.OwnerT == E_LiaOwnerT.CoBorrower ? dataApp.aCMismoId : dataApp.aBMismoId;
            if (field.DebtT == E_DebtRegularT.Mortgage && field.MatchedReRecordId != Guid.Empty)
            {
                liability.ReoId = "REO_" + field.MatchedReRecordId.ToString("N");
            }
            liability.HolderStreetAddress = field.ComAddr;
            liability.HolderCity = field.ComCity;
            liability.HolderState = field.ComState;
            liability.HolderPostalCode = field.ComZip;
            liability.AccountIdentifier = field.AccNum.Value;
            liability.HolderName = field.ComNm;
            liability.MonthlyPaymentAmount = field.Pmt_rep;
            liability.PayoffStatusIndicator = ToMismo(field.WillBePdOff);
            liability.RemainingTermMonths = field.RemainMons_rep;
            liability.Type = ToMismo(field.DebtT);
            liability.UnpaidBalanceAmount = field.Bal_rep;
            liability.PayoffWithCurrentAssetsIndicator = ToMismo(field.PayoffTiming);

            if (field.DebtT == E_DebtRegularT.Mortgage)
            {
                // 5/7/2008 dd - LendingQB always exclude mortgage liability to be include in DTI calculation. However
                // when we export to LP, we have to turn off ExclusionIndicator if liability is not paid off.
                liability.ExclusionIndicator = ToMismo(field.WillBePdOff);
            }
            else
            {
                liability.ExclusionIndicator = ToMismo(field.NotUsedInRatio);
            }


            return liability;
        }

        /// <summary>
        /// Creates an indicator specifying whether a buyer will be paying off a liability before closing
        /// with current assets.
        /// </summary>
        /// <param name="timing">The timing of the payoff.</param>
        /// <returns>A Y/N indicator.</returns>
        private E_YNIndicator ToMismo(E_Timing timing)
        {
            switch (timing)
            {
                case E_Timing.Blank:
                    return E_YNIndicator.Undefined;
                case E_Timing.After_Closing:
                case E_Timing.At_Closing:
                    return E_YNIndicator.N;
                case E_Timing.Before_Closing:
                    return E_YNIndicator.Y;
                default:
                    throw new UnhandledEnumException(timing);
            }
        }

        private E_LiabilityType ToMismo(E_DebtRegularT e_DebtRegularT)
        {
            switch (e_DebtRegularT)
            {
                case E_DebtRegularT.Installment: return E_LiabilityType.Installment;
                case E_DebtRegularT.Mortgage: return E_LiabilityType.MortgageLoan;
                case E_DebtRegularT.Open: return E_LiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Other: return E_LiabilityType.OtherLiability;
                case E_DebtRegularT.Revolving: return E_LiabilityType.Revolving;
                default:
                    LogInvalidEnum("E_DebtRegularT", e_DebtRegularT);
                    return E_LiabilityType.Undefined;
            }
        }
        private void LogInvalidEnum(string type, Enum value)
        {
            Tools.LogBug("Unhandle enum value='" + value + "' for " + type);
        }
        private static E_YNIndicator ToMismo(bool p)
        {
            return p ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        private Liability CreateLiability()
        {
            Liability liability = new Liability();
            //liability.Id = null;
            //liability.ReoId = null;
            //liability.BorrowerId = null;
            //liability.AlimonyOwedToName = null;
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            //liability.AccountIdentifier = null;
            //liability.ExclusionIndicator = null;
            //liability.HolderCity = null;
            //liability.HolderName = null;
            //liability.HolderPostalCode = null;
            //liability.HolderState = null;
            //liability.HolderStreetAddress = null;
            //liability.HolderStreetAddress2 = null;
            //liability.MonthlyPaymentAmount = null;
            //liability.PayoffStatusIndicator = null;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            //liability.RemainingTermMonths = null;
            //liability.Type = null;
            //liability.TypeOtherDescription = null;
            //liability.UnpaidBalanceAmount = null;
            return liability;
        }

        private LoanProductData CreateLoanProductData()
        {
            LoanProductData loanProductData = new LoanProductData();
            loanProductData.Arm = CreateArm();
            loanProductData.LoanFeatures = CreateLoanFeatures();
            loanProductData.PaymentAdjustmentList.Add(CreatePaymentAdjustment());
            loanProductData.RateAdjustmentList.Add(CreateRateAdjustment());
            loanProductData.InterestOnly = CreateInterestOnly();

            //loanProductData.Id = null;
            loanProductData.BuydownList.Add(this.CreateBuydown());

            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536
                loanProductData.Heloc = CreateHeloc();
            }

            loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty());
            //loanProductData.InterestCalculationRuleList.Add(CreateInterestCalculationRule());
            return loanProductData;
        }

        private Arm CreateArm()
        {
            Arm arm = new Arm();
            arm.RateAdjustmentLifetimeCapPercent = m_dataLoan.sRAdjLifeCapR_rep;
            arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR == 0m ? ZERO_DOLLARS : m_dataLoan.sRAdjMarginR_rep;
            arm.LifetimeFloorPercent = m_dataLoan.sRAdjFloorR_rep;
            arm.QualifyingRatePercent = m_dataLoan.sQualIR_rep;
            arm.LifetimeCapRate = m_dataLoan.sRLifeCapR_rep;
            arm.IndexCurrentValuePercent = m_dataLoan.sRAdjIndexR_rep;
            arm.PrincipalAndInterestPaymentMaximumAmount = m_dataLoan.sTRIDPIPmtMaxEver_rep;

            //arm.Id = null;
            //arm.FnmTreasuryYieldForCurrentIndexDivisorNumber = null;
            //arm.FnmTreasuryYieldForIndexDivisorNumber = null;
            //arm.PaymentAdjustmentLifetimeCapAmount = null;
            //arm.PaymentAdjustmentLifetimeCapPercent = null;
            //arm.ConversionOptionIndicator = null;
            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                arm.IndexType = E_ArmIndexType.Other;
                arm.IndexTypeOtherDescription = m_dataLoan.sArmIndexNameVstr;
            }

            arm.InterestRateRoundingFactor = this.m_dataLoan.sRAdjRoundToR_rep;
            arm.InterestRateRoundingType = ToMismo(this.m_dataLoan.sRAdjRoundT);
            //arm.ConversionOption = CreateConversionOption();
            return arm;
        }

        private E_ArmInterestRateRoundingType ToMismo(E_sRAdjRoundT roundType)
        {
            switch (roundType)
            {
                case E_sRAdjRoundT.Down: return E_ArmInterestRateRoundingType.Down;
                case E_sRAdjRoundT.Normal: return E_ArmInterestRateRoundingType.Nearest;
                case E_sRAdjRoundT.Up: return E_ArmInterestRateRoundingType.Up;
                default:
                    throw new UnhandledEnumException(roundType);
            }
        }

        private ConversionOption CreateConversionOption()
        {
            ConversionOption conversionOption = new ConversionOption();
            //conversionOption.Id = null;
            //conversionOption.ConversionOptionPeriodFeePercent = null;
            //conversionOption.EndingChangeDatePeriodDescription = null;
            //conversionOption.FeeAmount = null;
            //conversionOption.NoteTermGreaterThanFifteenYearsAdditionalPercent = null;
            //conversionOption.NoteTermLessThanFifteenYearsAdditionalPercent = null;
            //conversionOption.PeriodEndDate = null;
            //conversionOption.PeriodStartDate = null;
            //conversionOption.StartingChangeDatePeriodDescription = null;
            return conversionOption;
        }

        /// <summary>
        /// Creates a Buydown object. Copied from the DocMagicMismoExporter class; OPM 61315.
        /// </summary>
        /// <returns>A Buydown object populated with the buydowns declared in the TIL.</returns>
        private Buydown CreateBuydown()
        {
            Buydown buydown = new Buydown();

            var buydownRows = new[]
            {
                //// R = Rate Reduction, Mon = Term (mths)
                new { R = this.m_dataLoan.sBuydwnR1, R_rep = this.m_dataLoan.sBuydwnR1_rep, Mon = this.m_dataLoan.sBuydwnMon1, Mon_rep = this.m_dataLoan.sBuydwnMon1_rep, Num = "1"},
                new { R = this.m_dataLoan.sBuydwnR2, R_rep = this.m_dataLoan.sBuydwnR2_rep, Mon = this.m_dataLoan.sBuydwnMon2, Mon_rep = this.m_dataLoan.sBuydwnMon2_rep, Num = "2"},
                new { R = this.m_dataLoan.sBuydwnR3, R_rep = this.m_dataLoan.sBuydwnR3_rep, Mon = this.m_dataLoan.sBuydwnMon3, Mon_rep = this.m_dataLoan.sBuydwnMon3_rep, Num = "3"},
                new { R = this.m_dataLoan.sBuydwnR4, R_rep = this.m_dataLoan.sBuydwnR4_rep, Mon = this.m_dataLoan.sBuydwnMon4, Mon_rep = this.m_dataLoan.sBuydwnMon4_rep, Num = "4"},
                new { R = this.m_dataLoan.sBuydwnR5, R_rep = this.m_dataLoan.sBuydwnR5_rep, Mon = this.m_dataLoan.sBuydwnMon5, Mon_rep = this.m_dataLoan.sBuydwnMon5_rep, Num = "5"},
            };

            //// Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);

            bool isIncreaseRatePercentConstant = true;
            decimal increaseRatePercent = 0.0M;
            bool isChangeFrequencyMonthsConstant = true;
            int changeFrequencyMonths = 0;
            int durationMonths = 0;

            if (buydownRowsFiltered.Count() == 0)
            {
                return null;
            }
            else
            {
                increaseRatePercent = buydownRowsFiltered.First().R;
                changeFrequencyMonths = buydownRowsFiltered.First().Mon;
            }

            foreach (var row in buydownRowsFiltered)
            {
                buydown.SubsidyScheduleList.Add(this.CreateSubsidySchedule(row.R_rep, row.Num, row.Mon_rep));

                durationMonths += row.Mon;

                //// Detect increase rate percent: iff the value in sBuydwnR1 through sBuydwnR5 drops at the same rate
                if (row.R != increaseRatePercent)
                {
                    isIncreaseRatePercentConstant = false;
                }

                //// Detect change frequency months: iff the value in sBuydwnMon1 through sBuydwnMon5 remains constant
                if (row.Mon != changeFrequencyMonths)
                {
                    isChangeFrequencyMonthsConstant = false;
                }
            }

            ////buydown.Id = null;
            buydown.BaseDateType = E_BuydownBaseDateType.FirstPaymentDate;
            ////buydown.BaseDateTypeOtherDescription = null;

            if (isChangeFrequencyMonthsConstant)
            {
                buydown.ChangeFrequencyMonths = this.m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths);
            }

            ////buydown.ContributorType = null;
            ////buydown.ContributorTypeOtherDescription = null;
            buydown.DurationMonths = this.m_dataLoan.m_convertLos.ToCountString(durationMonths);

            if (isIncreaseRatePercentConstant)
            {
                buydown.IncreaseRatePercent = this.m_dataLoan.m_convertLos.ToRateString(increaseRatePercent);
            }

            ////buydown.LenderFundingIndicator = null;
            ////buydown.OriginalBalanceAmount = null;
            ////buydown.PermanentIndicator = null;
            ////buydown.SubsidyCalculationType = null;
            ////buydown.TotalSubsidyAmount = null;
            ////buydown.ContributorList.Add(CreateContributor());

            return buydown;
        }

        private Contributor CreateContributor()
        {
            Contributor contributor = new Contributor();
            //contributor.Id = null;
            //contributor.Amount = null;
            //contributor.Percent = null;
            //contributor.RoleType = null;
            //contributor.RoleTypeOtherDescription = null;
            //contributor.UnparsedName = null;
            return contributor;
        }

        /// <summary>
        /// Creates a SubsidySchedule object.
        /// </summary>
        /// <param name="adjustmentPercent">The percentage adjustment of the subsidy.</param>
        /// <param name="periodIdentifier">An identifier for the subsidy period.</param>
        /// <param name="periodicTerm">The subsidy term in months.</param>
        /// <returns>A SubsidySchedule object populated with the given parameters.</returns>
        private SubsidySchedule CreateSubsidySchedule(string adjustmentPercent, string periodIdentifier, string periodicTerm)
        {
            SubsidySchedule subsidySchedule = new SubsidySchedule();

            ////subsidySchedule.Id = null;
            subsidySchedule.AdjustmentPercent = adjustmentPercent;
            subsidySchedule.PeriodIdentifier = periodIdentifier;
            ////subsidySchedule.PeriodicPaymentEffectiveDate = null;
            ////subsidySchedule.PeriodicPaymentSubsidyAmount = null;
            subsidySchedule.PeriodicTerm = periodicTerm;

            return subsidySchedule;
        }

        private LoanFeatures CreateLoanFeatures()
        {
            LoanFeatures loanFeatures = new LoanFeatures();
            //loanFeatures.Id = null;

            loanFeatures.BalloonIndicator = ToMismo(m_dataLoan.sBalloonPmt);
            loanFeatures.BalloonLoanMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.DemandFeatureIndicator = ToMismo(m_dataLoan.sHasDemandFeature);
            loanFeatures.EscrowWaiverIndicator = ToMismo(m_dataLoan.sWillEscrowBeWaived);
            loanFeatures.EstimatedPrepaidDays = m_dataLoan.sIPiaDy_rep;
            loanFeatures.GsePropertyType = ToMismo(m_dataLoan.sGseSpT);
            loanFeatures.InterestOnlyTerm = m_dataLoan.sIOnlyMon_rep;
            loanFeatures.LienPriorityType = ToMismo(m_dataLoan.sLienPosT);
            loanFeatures.LoanOriginalMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.LoanScheduledClosingDate = m_dataLoan.sDocMagicClosingD_rep;
            loanFeatures.NegativeAmortizationLimitPercent = m_dataLoan.sPmtAdjMaxBalPc_rep;
            loanFeatures.OriginalBalloonTermMonths = m_dataLoan.sTerm_rep;
            loanFeatures.OriginalPrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            loanFeatures.PaymentFrequencyType = E_LoanFeaturesPaymentFrequencyType.Monthly;
            loanFeatures.ProductName = m_dataLoan.sLpTemplateNm;
            loanFeatures.ProductIdentifier = m_dataLoan.sLoanProductIdentifier;

            loanFeatures.RequiredDepositIndicator = ToMismo(m_dataLoan.sAprIncludesReqDeposit);
            loanFeatures.ScheduledFirstPaymentDate = m_dataLoan.sSchedDueD1_rep;


            switch (m_dataLoan.sAssumeLT)
            {
                case E_sAssumeLT.May:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.Y;
                    loanFeatures.ConditionsToAssumabilityIndicator = E_YNIndicator.N;
                    break;
                case E_sAssumeLT.MayNot:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.N;
                    break;
                case E_sAssumeLT.MaySubjectToCondition:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.Y;
                    loanFeatures.ConditionsToAssumabilityIndicator = E_YNIndicator.Y;
                    break;
            }

            switch (m_dataLoan.sPrepmtPenaltyT)
            {
                case E_sPrepmtPenaltyT.May:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Y;
                    break;
                case E_sPrepmtPenaltyT.WillNot:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.N;
                    break;
                default:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Undefined;
                    break;
            }
            switch (m_dataLoan.sPrepmtRefundT)
            {
                case E_sPrepmtRefundT.May:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.Y;
                    break;
                case E_sPrepmtRefundT.WillNot:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.N;
                    break;
                default:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.Undefined;
                    break;

            }

            loanFeatures.BuydownTemporarySubsidyIndicator = m_dataLoan.sHasTempBuydown ? E_YNIndicator.Y : E_YNIndicator.N;
            //loanFeatures.ConformingIndicator = null;
            //loanFeatures.CounselingConfirmationIndicator = null;
            //loanFeatures.CounselingConfirmationType = null;
            //loanFeatures.DownPaymentOptionType = null;
            //loanFeatures.DownPaymentOptionTypeOtherDescription = null;
            //loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = null;
            E_LoanFeaturesEstimatedPrepaidDaysPaidByType paidBy;
            switch (LosConvert.GfeItemProps_Payer(m_dataLoan.sIPiaProps))
            {
                case LosConvert.BORR_PAID_OUTOFPOCKET:
                case LosConvert.BORR_PAID_FINANCED:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Buyer;
                    break;
                case LosConvert.SELLER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Seller;
                    break;
                case LosConvert.LENDER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Lender;
                    break;
                case LosConvert.BROKER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Other;
                    loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = "Broker";
                    break;
                default:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Undefined;
                    break;
            }
            loanFeatures.EstimatedPrepaidDaysPaidByType = paidBy;
            //loanFeatures.FnmProductPlanIdentifier = null;
            //loanFeatures.FnmProductPlanIndentifier = null;
            //loanFeatures.FnmProjectClassificationType = null;
            //loanFeatures.FnmProjectClassificationTypeOtherDescription = null;
            //loanFeatures.FreOfferingIdentifier = null;
            //loanFeatures.FreProjectClassificationType = null;
            //loanFeatures.FreProjectClassificationTypeOtherDescription = null;
            //loanFeatures.GraduatedPaymentMultiplierFactor = null;
            //loanFeatures.GrowingEquityLoanPayoffYearsCount = null;
            loanFeatures.FullPrepaymentPenaltyOptionType = ToMismo(this.m_dataLoan.sPrepmtPeriodMonths, this.m_dataLoan.sSoftPrepmtPeriodMonths);

            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536
                loanFeatures.HelocInitialAdvanceAmount = m_dataLoan.sLAmtCalc_rep;
                loanFeatures.HelocMaximumBalanceAmount = m_dataLoan.sCreditLineAmt_rep;
            }
            //loanFeatures.InitialPaymentRatePercent = null;
            //loanFeatures.InitialPaymentDiscountPercent = null;
            //loanFeatures.LenderSelfInsuredIndicator = null;
            //loanFeatures.LienPriorityTypeOtherDescription = null;
            //loanFeatures.LoanClosingStatusType = null;
            loanFeatures.LoanDocumentationType = ToMismo(this.m_dataLoan.sProdDocT, this.m_dataLoan.sLPurposeT);

            if (loanFeatures.LoanDocumentationType == E_LoanFeaturesLoanDocumentationType.Other)
            {
                loanFeatures.LoanDocumentationTypeOtherDescription = this.m_dataLoan.sProdDocT_rep;
            }
            //loanFeatures.LoanMaturityDate = null;
            //loanFeatures.LoanRepaymentType = null;
            //loanFeatures.LoanRepaymentTypeOtherDescription = null;
            //loanFeatures.MiCertificationStatusType = null;
            //loanFeatures.MiCertificationStatusTypeOtherDescription = null;
            loanFeatures.MiCompanyNameType = ToMismo(this.m_dataLoan.sMiCompanyNmT);

            if (loanFeatures.MiCompanyNameType == E_LoanFeaturesMiCompanyNameType.Other)
            {
                loanFeatures.MiCompanyNameTypeOtherDescription = this.m_dataLoan.sMiCompanyNmT_rep;
            }
            //loanFeatures.MiCoveragePercent = null;
            //loanFeatures.NameDocumentsDrawnInType = null;
            //loanFeatures.NameDocumentsDrawnInTypeOtherDescription = null;
            loanFeatures.NegativeAmortizationLimitMonthsCount = this.m_dataLoan.sPmtAdjRecastPeriodMon_rep;
            //loanFeatures.NegativeAmortizationType = null;
            //loanFeatures.PaymentFrequencyTypeOtherDescription = null;
            loanFeatures.PrepaymentPenaltyTermMonths = this.m_dataLoan.sPrepmtPeriodMonths_rep;
            //loanFeatures.PrepaymentRestrictionIndicator = null;
            //loanFeatures.ProductDescription = null;
            //loanFeatures.ProductIdentifier = null;
            //loanFeatures.RefundableApplicationFeeIndicator = null;
            //loanFeatures.ServicingTransferStatusType = null;
            //loanFeatures.TimelyPaymentRateReductionIndicator = null;
            //loanFeatures.TimelyPaymentRateReductionPercent = null;
            loanFeatures.LateCharge = CreateLateCharge();
            //loanFeatures.NotePayTo = CreateNotePayTo();

            return loanFeatures;
        }

        /// <summary>
        /// Determines whether there is a prepayment penalty option on the loan.
        /// </summary>
        /// <param name="prepaymentPeriodMonths">The duration of any hard prepayment penalty.</param>
        /// <param name="softPrepaymentPeriodMonths">The duration of any soft prepayment penalty.</param>
        /// <returns>The prepayment penalty option on the loan, if any.</returns>
        private E_LoanFeaturesFullPrepaymentPenaltyOptionType ToMismo(int prepaymentPeriodMonths, int softPrepaymentPeriodMonths)
        {
            if (prepaymentPeriodMonths > 0)
            {
                return E_LoanFeaturesFullPrepaymentPenaltyOptionType.Hard;
            }
            else if (softPrepaymentPeriodMonths > 0)
            {
                return E_LoanFeaturesFullPrepaymentPenaltyOptionType.Soft;
            }

            return E_LoanFeaturesFullPrepaymentPenaltyOptionType.Undefined;
        }

        /// <summary>
        /// Converts the loan doc type to the MISMO format.
        /// </summary>
        /// <param name="documentationType">The loan documentation type [sProdDocT].</param>
        /// <param name="transactionType">The loan transaction/purpose type [sLPurposeT].</param>
        /// <returns>The MISMO formatted loan documentation type.</returns>
        private E_LoanFeaturesLoanDocumentationType ToMismo(E_sProdDocT documentationType, E_sLPurposeT transactionType)
        {
            if (transactionType == E_sLPurposeT.FhaStreamlinedRefinance || transactionType == E_sLPurposeT.VaIrrrl)
            {
                return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
            }
            switch (documentationType)
            {
                case E_sProdDocT.Full:
                    return E_LoanFeaturesLoanDocumentationType.FullDocumentation;
                case E_sProdDocT.Alt:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.Light:
                    return E_LoanFeaturesLoanDocumentationType.Reduced;
                case E_sProdDocT.NINA:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                case E_sProdDocT.NISA:
                    return E_LoanFeaturesLoanDocumentationType.Other;
                case E_sProdDocT.NINANE:
                    return E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
                case E_sProdDocT.NIVA:
                    return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.SISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                case E_sProdDocT.SIVA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome;
                case E_sProdDocT.VISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
                case E_sProdDocT.NIVANE:
                    return E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
                case E_sProdDocT.VINA:
                    return E_LoanFeaturesLoanDocumentationType.NoDepositVerification;
                case E_sProdDocT.Streamline:
                    return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                    return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.NoIncome:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                default:
                    throw new UnhandledEnumException(documentationType);
            }
        }

        private E_LoanFeaturesMiCompanyNameType ToMismo(E_sMiCompanyNmT mortgageInsuranceCoNameType)
        {
            switch (mortgageInsuranceCoNameType)
            {
                case E_sMiCompanyNmT.Amerin: return E_LoanFeaturesMiCompanyNameType.AmerinGuaranteeCorporation;
                case E_sMiCompanyNmT.Arch: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.CAHLIF: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.CMG: return E_LoanFeaturesMiCompanyNameType.CMG_MICompany;
                case E_sMiCompanyNmT.CMGPre94: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.Commonwealth: return E_LoanFeaturesMiCompanyNameType.CommonwealthMortgageAssuranceCompany;
                case E_sMiCompanyNmT.Essent: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.FHA: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.Genworth: return E_LoanFeaturesMiCompanyNameType.GenworthMortgageInsuranceCorporation;
                case E_sMiCompanyNmT.LeaveBlank: return E_LoanFeaturesMiCompanyNameType.Undefined;
                case E_sMiCompanyNmT.MDHousing: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.MGIC: return E_LoanFeaturesMiCompanyNameType.MortgageGuarantyInsuranceCorporation;
                case E_sMiCompanyNmT.MIF: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.NationalMI: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.PMI: return E_LoanFeaturesMiCompanyNameType.PMI_MICorporation;
                case E_sMiCompanyNmT.Radian: return E_LoanFeaturesMiCompanyNameType.RadianGuarantyIncorporated;
                case E_sMiCompanyNmT.RMIC: return E_LoanFeaturesMiCompanyNameType.RepublicMICompany;
                case E_sMiCompanyNmT.RMICNC: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.SONYMA: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.Triad: return E_LoanFeaturesMiCompanyNameType.TriadGuarantyInsuranceCorporation;
                case E_sMiCompanyNmT.UnitedGuaranty: return E_LoanFeaturesMiCompanyNameType.UnitedGuarantyCorporation;
                case E_sMiCompanyNmT.USDA: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.VA: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.Verex: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.WiscMtgAssr: return E_LoanFeaturesMiCompanyNameType.Other;
                case E_sMiCompanyNmT.MassHousing: return E_LoanFeaturesMiCompanyNameType.Other;
                default:
                    throw new UnhandledEnumException(mortgageInsuranceCoNameType);
            }
        }

        private LateCharge CreateLateCharge()
        {
            LateCharge lateCharge = new LateCharge();
            //lateCharge.Id = null;
            //lateCharge.Amount;
            lateCharge.GracePeriod = this.m_dataLoan.sLateDays;
            //lateCharge.LoanPaymentAmount = null;
            //lateCharge.MaximumAmount = null;
            //lateCharge.MinimumAmount = null;
            lateCharge.Rate = this.m_dataLoan.sLateChargePc.Replace("%", ""); // 08/12/14 BB - Per the MISMO 2.6 spec, this is meant to be the percentage value. For example 2% => "2" and not ".02".
            //lateCharge.Type = null;
            return lateCharge;
        }

        private NotePayTo CreateNotePayTo()
        {
            NotePayTo notePayTo = new NotePayTo();
            //notePayTo.Id = null;
            //notePayTo.City = null;
            //notePayTo.Country = null;
            //notePayTo.PostalCode = null;
            //notePayTo.State = null;
            //notePayTo.StreetAddress = null;
            //notePayTo.StreetAddress2 = null;
            //notePayTo.UnparsedName = null;
            return notePayTo;
        }

        private PaymentAdjustment CreatePaymentAdjustment()
        {
            PaymentAdjustment paymentAdjustment = new PaymentAdjustment();
            paymentAdjustment.PeriodNumber = m_dataLoan.sPmtAdjCapMon_rep;
            paymentAdjustment.PeriodicCapPercent = m_dataLoan.sPmtAdjCapR_rep;

            //paymentAdjustment.Id = null;
            //paymentAdjustment.FirstPaymentAdjustmentDate = null;
            //paymentAdjustment.FirstPaymentAdjustmentMonths = null;
            //paymentAdjustment.LastPaymentAdjustmentDate = null;
            //paymentAdjustment.SubsequentPaymentAdjustmentMonths = null;
            //paymentAdjustment.Amount = null;
            //paymentAdjustment.CalculationType = null;
            //paymentAdjustment.CalculationTypeOtherDescription = null;
            //paymentAdjustment.DurationMonths = null;
            //paymentAdjustment.Percent = null;
            //paymentAdjustment.PeriodicCapAmount = null;
            return paymentAdjustment;
        }

        private RateAdjustment CreateRateAdjustment()
        {
            RateAdjustment rateAdjustment = new RateAdjustment();
            rateAdjustment.FirstRateAdjustmentMonths = m_dataLoan.sRAdj1stCapMon_rep;
            rateAdjustment.SubsequentRateAdjustmentMonths = m_dataLoan.sRAdjCapMon_rep;
            rateAdjustment.FirstChangeCapRate = m_dataLoan.sR1stCapR_rep;
            rateAdjustment.FirstChangeFloorRate = m_dataLoan.m_convertLos.ToRateString(Math.Max((m_dataLoan.sNoteIR - m_dataLoan.sRAdj1stCapR), m_dataLoan.sRAdjFloorR));
            rateAdjustment.InitialCapPercent = m_dataLoan.sRAdj1stCapR_rep;
            rateAdjustment.SubsequentCapPercent = m_dataLoan.sRAdjCapR_rep;

            //rateAdjustment.Id = null;
            //rateAdjustment.FirstRateAdjustmentDate = null;
            //rateAdjustment.CalculationType = null;
            //rateAdjustment.CalculationTypeOtherDescription = null;
            //rateAdjustment.DurationMonths = null;
            //rateAdjustment.FirstChangeFloorPercent = null;
            //rateAdjustment.Percent = null;
            //rateAdjustment.PeriodNumber = null;
            return rateAdjustment;
        }

        private Heloc CreateHeloc()
        {
            Heloc heloc = new Heloc();
            //heloc.Id = null;
            heloc.AnnualFeeAmount = m_dataLoan.sHelocAnnualFee_rep;
            //heloc.CreditCardAccountIdentifier = null;
            //heloc.CreditCardIndicator = null;
            if (m_dataLoan.sDaysInYr_rep == "360")
            {
                heloc.DailyPeriodicInterestRateCalculationType = E_HelocDailyPeriodicInterestRateCalculationType._360;
            }
            else if (m_dataLoan.sDaysInYr_rep == "365")
            {
                heloc.DailyPeriodicInterestRateCalculationType = E_HelocDailyPeriodicInterestRateCalculationType._365;

            }
            heloc.DailyPeriodicInterestRatePercent = m_dataLoan.sDailyPeriodicR_rep;
            heloc.DrawPeriodMonthsCount = m_dataLoan.sHelocDraw_rep;
            heloc.FirstLienBookNumber = m_dataLoan.sFirstLienRecordingBookNum;
            //heloc.FirstLienDate = null;
            heloc.FirstLienHolderName = m_dataLoan.sFirstLienHolderNm;
            heloc.FirstLienIndicator = m_dataLoan.sLienPosT == E_sLienPosT.First ? E_YNIndicator.Y : E_YNIndicator.N;
            heloc.FirstLienInstrumentNumber = m_dataLoan.sFirstLienRecordingInstrumentNum;
            heloc.FirstLienPageNumber = m_dataLoan.sFirstLienRecordingPageNum;
            heloc.FirstLienPrincipalBalanceAmount = m_dataLoan.sRemain1stMBal_rep;
            heloc.FirstLienRecordedDate = m_dataLoan.sFirstLienRecordingD_rep;
            heloc.InitialAdvanceAmount = m_dataLoan.sLAmtCalc_rep;
            heloc.MaximumAprRate = m_dataLoan.sRLifeCapR_rep;
            heloc.MinimumAdvanceAmount = m_dataLoan.sHelocMinimumAdvanceAmt_rep;
            heloc.MinimumPaymentAmount = m_dataLoan.sHelocMinimumPayment_rep;
            heloc.MinimumPaymentPercent = m_dataLoan.sHelocMinimumPaymentPc_rep;
            heloc.RepayPeriodMonthsCount = m_dataLoan.sHelocRepay_rep;
            heloc.ReturnedCheckChargeAmount = m_dataLoan.sHelocReturnedCheckFee_rep;
            heloc.StopPaymentChargeAmount = m_dataLoan.sHelocStopPaymentFee_rep;
            //heloc.TeaserTermEndDate = null;
            heloc.TeaserTermMonthsCount = m_dataLoan.sRAdj1stCapMon_rep;
            heloc.TerminationFeeAmount = m_dataLoan.sHelocTerminationFee_rep;
            heloc.TerminationPeriodMonthsCount = m_dataLoan.sHelocTerminationFeePeriod_rep;
            return heloc;
        }

        private InterestOnly CreateInterestOnly()
        {
            InterestOnly interestOnly = new InterestOnly();
            //interestOnly.Id = null;
            interestOnly.MonthlyPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            interestOnly.TermMonthsCount = m_dataLoan.sIOnlyMon_rep;
            return interestOnly;
        }

        private PrepaymentPenalty CreatePrepaymentPenalty()
        {
            PrepaymentPenalty prepaymentPenalty = new PrepaymentPenalty();

            //prepaymentPenalty.Id = null;
            //prepaymentPenalty.PenaltyFixedAmount = null;
            //prepaymentPenalty.Percent = null;
            //prepaymentPenalty.PeriodSequenceIdentifier = null;
            prepaymentPenalty.TermMonths = m_dataLoan.sPpmtPenaltyMon_rep;
            prepaymentPenalty.GfePrepaymentPenaltyMaximumAmount = m_dataLoan.sGfeMaxPpmtPenaltyAmt_rep;

            return prepaymentPenalty;
        }

        private InterestCalculationRule CreateInterestCalculationRule()
        {
            InterestCalculationRule interestCalculationRule = new InterestCalculationRule();
            //interestCalculationRule.Id = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodType = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationBasisDaysInYearCount = null;
            //interestCalculationRule.InterestCalculationBasisType = null;
            //interestCalculationRule.InterestCalculationBasisTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationEffectiveDate = null;
            //interestCalculationRule.InterestCalculationEffectiveMonthsCount = null;
            //interestCalculationRule.InterestCalculationExpirationDate = null;
            //interestCalculationRule.InterestCalculationPeriodAdjustmentIndicator = null;
            //interestCalculationRule.InterestCalculationPeriodType = null;
            //interestCalculationRule.InterestCalculationPurposeType = null;
            //interestCalculationRule.InterestCalculationPurposeTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationType = null;
            //interestCalculationRule.InterestCalculationTypeOtherDescription = null;
            //interestCalculationRule.InterestInAdvanceIndicator = null;
            //interestCalculationRule.LoanInterestAccrualStartDate = null;
            return interestCalculationRule;
        }

        private LoanPurpose CreateLoanPurpose(CAppData dataApp)
        {
            LoanPurpose loanPurpose = new LoanPurpose();
            //loanPurpose.Id = null;
            //loanPurpose.OtherLoanPurposeDescription = null;
            //loanPurpose.PropertyRightsTypeOtherDescription = null;
            //loanPurpose.PropertyUsageTypeOtherDescription = null;

            loanPurpose.GseTitleMannerHeldDescription = dataApp.aManner;
            loanPurpose.PropertyLeaseholdExpirationDate = m_dataLoan.sLeaseHoldExpireD_rep;
            loanPurpose.PropertyRightsType = ToMismo(m_dataLoan.sEstateHeldT);
            loanPurpose.PropertyUsageType = ToMismo(dataApp.aOccT);

            if (this.vendorIsDocuTech && m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                loanPurpose.Type = E_LoanPurposeType.Other;
                loanPurpose.OtherLoanPurposeDescription = "HELOC";
            }
            else
            {
                loanPurpose.Type = ToMismo(m_dataLoan.sLPurposeT);
                loanPurpose.OtherLoanPurposeDescription = m_dataLoan.sOLPurposeDesc;
            }

            loanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();

            return loanPurpose;
        }

        private E_LoanPurposeType ToMismo(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Construct: return E_LoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm: return E_LoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.Other: return E_LoanPurposeType.Other;
                case E_sLPurposeT.Purchase: return E_LoanPurposeType.Purchase;
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                    return E_LoanPurposeType.Refinance;
                default:
                    LogInvalidEnum("sLPurposeT", sLPurposeT);
                    return E_LoanPurposeType.Undefined;
            }
        }

        private E_LoanPurposePropertyUsageType ToMismo(E_aOccT aOccT)
        {
            switch (aOccT)
            {
                case E_aOccT.PrimaryResidence: return E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return E_LoanPurposePropertyUsageType.SecondHome;
                case E_aOccT.Investment: return E_LoanPurposePropertyUsageType.Investor;
                default:
                    LogInvalidEnum("aOccT", aOccT);
                    return E_LoanPurposePropertyUsageType.Undefined;
            }
        }

        private E_LoanPurposePropertyRightsType ToMismo(E_sEstateHeldT sEstateHeldT)
        {
            switch (sEstateHeldT)
            {
                case E_sEstateHeldT.FeeSimple: return E_LoanPurposePropertyRightsType.FeeSimple;
                case E_sEstateHeldT.LeaseHold: return E_LoanPurposePropertyRightsType.Leasehold;
                default:
                    LogInvalidEnum("sEstateHeldT", sEstateHeldT);
                    return E_LoanPurposePropertyRightsType.Undefined;
            }
        }

        private ConstructionRefinanceData CreateConstructionRefinanceData()
        {
            ConstructionRefinanceData constructionRefinanceData = new ConstructionRefinanceData();

            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    constructionRefinanceData.GseRefinancePurposeType = ToMismoGseRefinancePurposeType(m_dataLoan.sRefPurpose);
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sSpAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sSpLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sSpOrigC_rep;
                    constructionRefinanceData.RefinanceImprovementCostsAmount = m_dataLoan.sSpImprovC_rep;
                    constructionRefinanceData.RefinanceImprovementsType = ToMismo(m_dataLoan.sSpImprovTimeFrameT);
                    constructionRefinanceData.FreCashOutAmount = m_dataLoan.sProdCashoutAmt_rep;
                    constructionRefinanceData.RefinanceProposedImprovementsDescription = m_dataLoan.sSpImprovDesc;
                    constructionRefinanceData.SecondaryFinancingRefinanceIndicator = ToMismo(m_dataLoan.sPayingOffSubordinate);
                    break;
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    constructionRefinanceData.ConstructionImprovementCostsAmount = m_dataLoan.sLotImprovC_rep;
                    constructionRefinanceData.ConstructionPurposeType = m_dataLoan.sLPurposeT == E_sLPurposeT.Construct ? E_ConstructionRefinanceDataConstructionPurposeType.ConstructionOnly : E_ConstructionRefinanceDataConstructionPurposeType.ConstructionToPermanent;
                    constructionRefinanceData.LandEstimatedValueAmount = m_dataLoan.sLotVal_rep;
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sLotAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sLotLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sLotOrigC_rep;
                    constructionRefinanceData.ConstructionPeriodInterestRatePercent = this.m_dataLoan.sConstructionPeriodIR_rep;
                    constructionRefinanceData.ConstructionPeriodNumberOfMonthsCount = this.m_dataLoan.sConstructionPeriodMon_rep;
                    break;
                case E_sLPurposeT.Other:
                case E_sLPurposeT.Purchase:
                    break;
                    throw new UnhandledEnumException(this.m_dataLoan.sLPurposeT);
            };

            //constructionRefinanceData.Id = null;

            //constructionRefinanceData.ConstructionToPermanentClosingFeatureType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingFeatureTypeOtherDescription = null;
            //constructionRefinanceData.ConstructionToPermanentClosingType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingTypeOtherDescription = null;
            //constructionRefinanceData.FnmSecondMortgageFinancingOriginalPropertyIndicator = null;
            //constructionRefinanceData.FreCashOutAmount = null;
            //constructionRefinanceData.GseRefinancePurposeType = null;
            //constructionRefinanceData.GseRefinancePurposeTypeOtherDescription = null;
            //constructionRefinanceData.LandEstimatedValueAmount = null;
            //constructionRefinanceData.LandOriginalCostAmount = null;
            //constructionRefinanceData.NonStructuralAlterationsConventionalAmount = null;
            //constructionRefinanceData.PropertyAcquiredYear = null;
            //constructionRefinanceData.PropertyExistingLienAmount = null;
            //constructionRefinanceData.PropertyOriginalCostAmount = null;
            //constructionRefinanceData.RefinanceImprovementCostsAmount = null;
            //constructionRefinanceData.RefinanceImprovementsType = null;
            //constructionRefinanceData.RefinanceProposedImprovementsDescription = null;
            //constructionRefinanceData.SecondaryFinancingRefinanceIndicator = null;
            //constructionRefinanceData.StructuralAlterationsConventionalAmount = null;
            return constructionRefinanceData;
        }

        private E_ConstructionRefinanceDataRefinanceImprovementsType ToMismo(E_sSpImprovTimeFrameT sSpImprovTimeFrameT)
        {
            switch (sSpImprovTimeFrameT)
            {
                case E_sSpImprovTimeFrameT.LeaveBlank: return E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown;
                case E_sSpImprovTimeFrameT.Made: return E_ConstructionRefinanceDataRefinanceImprovementsType.Made;
                case E_sSpImprovTimeFrameT.ToBeMade: return E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
                default:
                    LogInvalidEnum("sSpImprovTimeFrameT", sSpImprovTimeFrameT);
                    return E_ConstructionRefinanceDataRefinanceImprovementsType.Undefined;
            }
        }

        private E_ConstructionRefinanceDataGseRefinancePurposeType ToMismoGseRefinancePurposeType(string desc)
        {
            switch (desc.ToLower())
            {
                case "no cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
                case "limited cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case "cash-out/home improvement": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case "cash-out/other": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                default:
                    if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance
                        || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                    else if (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                    else
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;

            }
        }

        private LoanQualification CreateLoanQualification()
        {
            LoanQualification loanQualification = new LoanQualification();
            //loanQualification.Id = null;
            loanQualification.AdditionalBorrowerAssetsConsideredIndicator = ToMismo(m_dataLoan.sMultiApps);
            loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismo(m_dataLoan.GetAppData(0).aSpouseIExcl);
            return loanQualification;
        }

        private Mers CreateMers()
        {
            Mers mers = new Mers();
            //mers.Id = null;
            //mers.MersMortgageeOfRecordIndicator = null;
            mers.MersOriginalMortgageeOfRecordIndicator = ToMismo(m_dataLoan.sMersIsOriginalMortgagee);
            //mers.MersRegistrationDate = null;
            //mers.MersRegistrationIndicator = null;
            //mers.MersRegistrationStatusType = null;
            //mers.MersRegistrationStatusTypeOtherDescription = null;
            //mers.MersTaxNumberIdentifier = null;
            mers.MersMinNumber = m_dataLoan.sMersMin;
            return mers;
        }

        private MiData CreateMiData()
        {
            MiData miData = new MiData();
            var miDataSourceDataLoan = m_useGfeFee || exportTridData ? m_dataLoanWithGfeArchiveApplied : m_dataLoan; // OPM 195882 - On closing docs, use live MI data instead of archived/CoCed values.
            miData.MiPremiumFinancedIndicator = miDataSourceDataLoan.sFfUfmipFinanced > 0 ? E_YNIndicator.Y : E_YNIndicator.N;
            miData.MiPremiumFromClosingAmount = miDataSourceDataLoan.sUfCashPd_rep;
            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                // For FHA transaction, only need to pass initial "Upfront" Premium in this property.
                miData.MiFhaUpfrontPremiumAmount = miDataSourceDataLoan.sFfUfmip1003_rep;
                miData.MiSourceType = E_MiDataMiSourceType.FHA;
            }
            else //m_dataLoan.sLT != E_sLT.FHA
            {
                if (m_dataLoanWithGfeArchiveApplied.sMiInsuranceT == E_sMiInsuranceT.BorrowerPaid)
                {
                    miData.MiInitialPremiumAmount = miDataSourceDataLoan.sFfUfmip1003_rep;
                    miData.MiInitialPremiumRatePercent = miDataSourceDataLoan.sFfUfmipR_rep;
                    if (
                           (miDataSourceDataLoan.sFfUfmip1003 != 0
                            || miDataSourceDataLoan.sFfUfmipR != 0
                           )
                        && miDataSourceDataLoan.sMipFrequency != E_MipFrequency.SinglePayment
                        )
                    {
                        miData.MiInitialPremiumRateDurationMonths = miDataSourceDataLoan.sMipPiaMon_rep;
                    }
                }
                miData.MiSourceType = E_MiDataMiSourceType.PMI;

                if (this.m_dataLoan.sProdConvMIOptionT != E_sProdConvMIOptionT.NoMI && this.m_dataLoan.sProdConvMIOptionT != E_sProdConvMIOptionT.Blank)
                {
                    miData.MiDurationType = this.ToMismo(this.m_dataLoan.sProdConvMIOptionT);
                }
            }
            //miData.Id = null;
            //miData.BorrowerMiTerminationDate = null;
            miData.MiCertificateIdentifier = m_dataLoan.sMiCertId;
            if (m_dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes)
            {
                miData.MiCollectedNumberOfMonthsCount = (this.m_dataLoan.sMInsRsrvMon == 0) ? "0" : this.m_dataLoan.sMInsRsrvMon_rep; //OPM 175437 - Need to support "0"
                miData.MiCushionNumberOfMonthsCount = m_dataLoan.GetCushionMonths(E_EscrowItemT.MortgageInsurance).ToString();
            }

            //miData.MiCompanyName = null;
            //miData.MiEscrowIncludedInAggregateIndicator = null;
            //miData.MiInitialPremiumAtClosingType = null;

            if (m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid || m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.InvestorPaid)
            {
                miData.MiPremiumPaymentType = E_MiDataMiPremiumPaymentType.LenderPaid;
            }
            else if (m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.BorrowerPaid)
            {
                miData.MiPremiumPaymentType = E_MiDataMiPremiumPaymentType.BorrowerPaid;
            }

            //miData.MiPremiumRefundableType = null;

            if (miDataSourceDataLoan.sMipFrequency == E_MipFrequency.AnnualRateXMonths)
            {
                miData.MiPremiumTermMonths = (miDataSourceDataLoan.sMipPiaMon == 0 || miDataSourceDataLoan.sFfUfmip1003 == 0) ? "0" : miDataSourceDataLoan.sMipPiaMon_rep;
            }

            if (m_dataLoan.sProMIns == 0)
            {
                miData.MiRenewalCalculationType = E_MiDataMiRenewalCalculationType.NoRenewals;
            }
            else
            {
                miData.MiRenewalCalculationType = ToMismo(miDataSourceDataLoan.sProMInsT);
            }

            //miData.MiScheduledTerminationDate = null;
            miData.MiLtvCutoffPercent = string.IsNullOrEmpty(m_dataLoan.sProMInsCancelLtv_rep) ? "0.000" : m_dataLoan.sProMInsCancelLtv_rep;
            //miData.MiLtvCutoffType = null;
            //miData.ScheduledAmortizationMidpointDate = null;
            //miData.MiPremiumTax = CreateMiPremiumTax();

            miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(
                E_MiRenewalPremiumSequence.First,
                m_dataLoan.sProMInsR_rep,
                m_dataLoan.sProMInsMon == 0 ? m_dataLoan.sDue_rep : m_dataLoan.sProMInsMon_rep,// OPM 146716
                m_dataLoan.sProMIns_rep));

            miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(
                E_MiRenewalPremiumSequence.Second,
                m_dataLoan.sProMInsR2_rep,
                m_dataLoan.sProMIns2Mon_rep,
                m_dataLoan.sProMIns2_rep));

            miData.PaidTo = CreatePaidTo(E_AgentRoleT.MortgageInsurance);
            return miData;
        }

        private E_MiDataMiRenewalCalculationType ToMismo(E_PercentBaseT sProMInsT)
        {
            switch (sProMInsT)
            {
                case E_PercentBaseT.AppraisalValue:
                case E_PercentBaseT.LoanAmount:
                case E_PercentBaseT.SalesPrice://"Purchase Price"
                case E_PercentBaseT.TotalLoanAmount:
                    return E_MiDataMiRenewalCalculationType.Constant;
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return E_MiDataMiRenewalCalculationType.Declining;
                case E_PercentBaseT.AllYSP:
                case E_PercentBaseT.OriginalCost:
                default:
                    return E_MiDataMiRenewalCalculationType.Undefined;
            }
        }

        private MiPremiumTax CreateMiPremiumTax()
        {
            MiPremiumTax miPremiumTax = new MiPremiumTax();
            //miPremiumTax.Id = null;
            //miPremiumTax.CodeAmount = null;
            //miPremiumTax.CodePercent = null;
            //miPremiumTax.CodeType = null;
            return miPremiumTax;
        }

        private const string ZERO_DOLLARS = "0.00";
        private const string ZERO_RATE_SIX_DIGITS = "0.000000";
        private const string ZERO_INT = "0";

        private MiRenewalPremium CreateMiRenewalPremium(E_MiRenewalPremiumSequence sequence, string rate, string term, string monthlyPayment)
        {
            if (monthlyPayment == ZERO_DOLLARS)
            {
                return new MiRenewalPremium() { MonthlyPaymentAmount = ZERO_DOLLARS, Rate = ZERO_RATE_SIX_DIGITS, RateDurationMonths = ZERO_INT, Sequence = sequence };
            }
            if (rate == "")
            {
                return null;
            }
            MiRenewalPremium miRenewalPremium = new MiRenewalPremium();
            //miRenewalPremium.Id = null;
            miRenewalPremium.MonthlyPaymentAmount = monthlyPayment;
            //miRenewalPremium.MonthlyPaymentRoundingType = null;
            miRenewalPremium.Rate = rate;
            miRenewalPremium.RateDurationMonths = term;
            miRenewalPremium.Sequence = sequence;
            return miRenewalPremium;
        }

        private MortgageTerms CreateMortgageTerms()
        {
            MortgageTerms mortgageTerms = new MortgageTerms();
            mortgageTerms.AgencyCaseIdentifier = m_dataLoan.sAgencyCaseNum;
            mortgageTerms.BaseLoanAmount = (m_dataLoan.sIsLineOfCredit ? m_dataLoan.sCreditLineAmt_rep : m_dataLoan.sLAmtCalc_rep);
            mortgageTerms.BorrowerRequestedLoanAmount = m_dataLoan.sFinalLAmt_rep;
            mortgageTerms.LenderCaseIdentifier = m_dataLoan.sLenderCaseNum;
            mortgageTerms.LenderLoanIdentifier = m_dataLoan.sLNm;
            mortgageTerms.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            mortgageTerms.LoanAmortizationType = ToMismo(m_dataLoan.sFinMethT);
            mortgageTerms.LoanEstimatedClosingDate = m_dataLoan.sDocMagicClosingD_rep;
            mortgageTerms.MortgageType = ToMismo(m_dataLoan.sLT);
            mortgageTerms.NoteRatePercent = string.IsNullOrEmpty(m_dataLoan.sNoteIR_rep) ? "0.00" : m_dataLoan.sNoteIR_rep;
            mortgageTerms.OriginalLoanAmount = m_dataLoan.sFinalLAmt_rep;
            mortgageTerms.OtherAmortizationTypeDescription = m_dataLoan.sFinMethDesc;
            mortgageTerms.OtherMortgageTypeDescription = m_dataLoan.sLTODesc;
            mortgageTerms.RequestedInterestRatePercent = m_dataLoan.sNoteIR_rep;
            //mortgageTerms.Id = null;
            //mortgageTerms.ArmTypeDescription = null;

            //mortgageTerms.LendersContactPrimaryTelephoneNumber = null;

            //mortgageTerms.PaymentRemittanceDay = null;

            return mortgageTerms;
        }


        private Mismo.Closing2_6.Property CreateProperty()
        {
            Mismo.Closing2_6.Property property = new Mismo.Closing2_6.Property();
            property.City = m_dataLoan.sSpCity;
            property.County = m_dataLoan.sSpCounty;
            property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;

            property.PostalCode = m_dataLoan.sSpZip;

            property.State = m_dataLoan.sSpState;
            property.StreetAddress = m_dataLoan.sSpAddr;

            property.StructureBuiltYear = m_dataLoan.sYrBuilt;

            property.LegalDescriptionList.Add(CreateLegalDescription());

            //property.Id = null;
            property.AssessorsParcelIdentifier = m_dataLoan.sAssessorsParcelId;
            //property.AssessorsSecondParcelIdentifier = null;
            //property.BuildingStatusType = null;
            //property.BuildingStatusTypeOtherDescription = null;
            //property.CurrentVacancyStatusType = null;
            //property.GrossLivingAreaSquareFeetCount = null;
            //property.ManufacturedHomeManufactureYear = null;
            //property.NativeAmericanLandsType = null;
            //property.NativeAmericanLandsTypeOtherDescription = null;
            //property.PlannedUnitDevelopmentIndicator = null;

            if (this.m_dataLoan.sGseSpT == E_sGseSpT.Condominium ||
                this.m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium ||
                this.m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                this.m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                property.StoriesCount = m_dataLoan.sProdCondoStories_rep;
            }

            //property.UniqueDwellingType = null;
            //property.UniqueDwellingTypeOtherDescription = null;
            //property.AcquiredDate = null;
            //property.AcreageNumber = null;
            //property.CommunityLandTrustIndicator = null;
            //property.ConditionDescription = null;
            //property.Country = null;
            //property.InclusionaryZoningIndicator = null;
            //property.NeighborhoodLocationType = null;
            //property.NeighborhoodLocationTypeOtherDescription = null;
            //property.OwnershipType = null;
            //property.OwnershipTypeOtherDescription = null;
            switch (m_dataLoan.sFHAConstructionT)
            {
                case E_sFHAConstructionT.Blank:
                    break;
                case E_sFHAConstructionT.Existing:
                    property.PreviouslyOccupiedIndicator = E_YNIndicator.Y;
                    break;
                case E_sFHAConstructionT.Proposed:
                case E_sFHAConstructionT.New:
                    property.PreviouslyOccupiedIndicator = E_YNIndicator.N;
                    break;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sFHAConstructionT);
            }
            //property.StreetAddress2 = null;
            //property.ZoningCategoryType = null;
            //property.ZoningCategoryTypeOtherDescription = null;
            //property.ParsedStreetAddress = CreateParsedStreetAddress();
            property.ValuationList.Add(CreateValuation());
            property.Details = CreateDetails();
            //property.HomeownersAssociation = CreateHomeownersAssociation();
            //property.Project = CreateProject();
            //property.CategoryList.Add(CreateCategory());
            //property.DwellingUnitList.Add(CreateDwellingUnit());
            //property.ManufacturedHomeList.Add(CreateManufacturedHome());
            //property.PlattedLandList.Add(CreatePlattedLand());
            //property.UnplattedLand = CreateUnplattedLand();
            property.FloodDeterminationList.Add(CreateFloodDetermination());
            return property;
        }

        private LegalDescription CreateLegalDescription()
        {
            LegalDescription legalDescription = new LegalDescription();
            //legalDescription.Id = null;
            legalDescription.TextDescription = m_dataLoan.sSpLegalDesc;
            legalDescription.Type = E_LegalDescriptionType.LongLegal;
            //legalDescription.TypeOtherDescription = null;
            //legalDescription.PlattedLand = CreatePlattedLand();
            return legalDescription;
        }

        private ParsedStreetAddress CreateParsedStreetAddress()
        {
            ParsedStreetAddress parsedStreetAddress = new ParsedStreetAddress();
            //parsedStreetAddress.Id = null;
            //parsedStreetAddress.ApartmentOrUnit = null;
            //parsedStreetAddress.BuildingNumber = null;
            //parsedStreetAddress.DirectionPrefix = null;
            //parsedStreetAddress.DirectionSuffix = null;
            //parsedStreetAddress.HouseNumber = null;
            //parsedStreetAddress.MilitaryApoFpo = null;
            //parsedStreetAddress.PostOfficeBox = null;
            //parsedStreetAddress.RuralRoute = null;
            //parsedStreetAddress.StreetName = null;
            //parsedStreetAddress.StreetSuffix = null;
            return parsedStreetAddress;
        }

        private Valuation CreateValuation()
        {
            Valuation valuation = new Valuation();
            //valuation.Id = null;
            valuation.AppraisalFormType = ToMismo(m_dataLoan.sSpAppraisalFormT);
            //valuation.AppraisalFormTypeOtherDescription = null;
            //valuation.AppraisalFormVersionIdentifier = null;
            valuation.AppraisalInspectionType = ToMismo(m_dataLoan.sPropertyInspectionT);
            //valuation.AppraisalInspectionTypeOtherDescription = null;
            valuation.MethodType = ToMismo(m_dataLoan.sSpValuationMethodT);

            if (m_dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FieldReview)
            {
                valuation.MethodTypeOtherDescription = "FieldReview-FNM2000";
            }

            valuation.AppraiserList.Add(CreateAppraiser());
            return valuation;
        }

        /// <summary>
        /// Maps the valuation method to MISMO.
        /// </summary>
        /// <param name="valuationMethod">The valuation method as represented in LQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private E_ValuationMethodType ToMismo(E_sSpValuationMethodT valuationMethod)
        {
            switch (valuationMethod)
            {
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return E_ValuationMethodType.AutomatedValuationModel;
                case E_sSpValuationMethodT.DesktopAppraisal:
                    return E_ValuationMethodType.FNM2075;
                case E_sSpValuationMethodT.DriveBy:
                    return E_ValuationMethodType.DriveBy;
                case E_sSpValuationMethodT.FullAppraisal:
                    return E_ValuationMethodType.FullAppraisal;
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return E_ValuationMethodType.PriorAppraisalUsed;
                case E_sSpValuationMethodT.None:
                    return E_ValuationMethodType.None;
                case E_sSpValuationMethodT.FieldReview:
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.LeaveBlank:
                case E_sSpValuationMethodT.Other:
                    return E_ValuationMethodType.Other;
                default:
                    throw new UnhandledEnumException(valuationMethod);
            }
        }

        /// <summary>
        /// Maps the appraisal inspection type to MISMO.
        /// </summary>
        /// <param name="inspectionType">The inspection type as represented in LQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private E_ValuationAppraisalInspectionType ToMismo(E_sPropertyInspectionT inspectionType)
        {
            switch (inspectionType)
            {
                case E_sPropertyInspectionT.ExteriorAndInterior:
                    return E_ValuationAppraisalInspectionType.ExteriorAndInterior;
                case E_sPropertyInspectionT.ExteriorOnly:
                    return E_ValuationAppraisalInspectionType.ExteriorOnly;
                case E_sPropertyInspectionT.None:
                    return E_ValuationAppraisalInspectionType.None;
                case E_sPropertyInspectionT.Other:
                    return E_ValuationAppraisalInspectionType.Other;
                default:
                    throw new UnhandledEnumException(inspectionType);
            }
        }

        /// <summary>
        /// Maps the appraisal form type to MISMO.
        /// </summary>
        /// <param name="appraisalForm">The appraisal form as represented in LQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private E_ValuationAppraisalFormType ToMismo(E_sSpAppraisalFormT appraisalForm)
        {
            switch (appraisalForm)
            {
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                    return E_ValuationAppraisalFormType.FNM1004DFRE442;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return E_ValuationAppraisalFormType.FNM2075;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM1075FRE466;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM2095;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM2055FRE2055;
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM1073FRE465;
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM2090;
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                    return E_ValuationAppraisalFormType.FRE2070;
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM1004CFRE70B;
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                    return E_ValuationAppraisalFormType.FNM2000FRE1032;
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM1025FRE72;
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                    return E_ValuationAppraisalFormType.FNM2000AFRE1072;
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return E_ValuationAppraisalFormType.FNM1004FRE70;
                case E_sSpAppraisalFormT.Blank:
                case E_sSpAppraisalFormT.Other:
                    return E_ValuationAppraisalFormType.Other;
                default:
                    throw new UnhandledEnumException(appraisalForm);
            }
        }

        private Appraiser CreateAppraiser()
        {
            Appraiser appraiser = new Appraiser();
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            //appraiser.Id = null;
            appraiser.CompanyName = agent.CompanyName;
            appraiser.LicenseIdentifier = agent.LicenseNumOfAgent;
            //appraiser.LicenseState = null;            //the state where they live? 
            appraiser.Name = agent.AgentName;
            return appraiser;
        }

        private Details CreateDetails()
        {
            Details details = new Details();
            //details.Id = null;
            //details.CondominiumIndicator = null;
            //details.CondominiumPudDeclarationsDescription = null;
            //details.JudicialDistrictName = null;
            //details.JudicialDivisionName = null;
            details.NfipCommunityIdentifier = m_dataLoan.sFloodCertificationCommunityNum;
            //details.NfipCommunityName = null;
            //details.NfipCommunityParticipationStatusType = null;
            //details.NfipCommunityParticipationStatusTypeOtherDescription = null;
            details.NfipFloodZoneIdentifier = m_dataLoan.sNfipFloodZoneId;
            if ((m_dataLoan.sGseSpT == E_sGseSpT.Attached || m_dataLoan.sGseSpT == E_sGseSpT.Detached)
                    && m_dataLoan.sUnitsNum > 1)
                details.OneToFourFamilyIndicator = E_YNIndicator.Y;
            details.ProjectName = m_dataLoan.sProjNm;
            //details.ProjectTotalSharesCount = null;
            //details.PropertyUnincorporatedAreaName = null;
            //details.RecordingJurisdictionName = null;
            //details.RecordingJurisdictionType = null;
            //details.RecordingJurisdictionTypeOtherDescription = null;

            if (this.m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium || 
                this.m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide ||
                this.m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousing ||
                this.m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide)
            {
                details.ManufacturedHomeIndicator = E_YNIndicator.Y;
            }
            else
            {
                details.ManufacturedHomeIndicator = E_YNIndicator.N;
            }

            return details;
        }

        private HomeownersAssociation CreateHomeownersAssociation()
        {
            HomeownersAssociation homeownersAssociation = new HomeownersAssociation();
            //homeownersAssociation.Id = null;
            //homeownersAssociation.City = null;
            //homeownersAssociation.Country = null;
            //homeownersAssociation.County = null;
            //homeownersAssociation.Name = null;
            //homeownersAssociation.PostalCode = null;
            //homeownersAssociation.State = null;
            //homeownersAssociation.StreetAddress = null;
            //homeownersAssociation.StreetAddress2 = null;
            //homeownersAssociation.ContactDetail = CreateContactDetail();
            return homeownersAssociation;
        }

        private Project CreateProject()
        {
            Project project = new Project();
            //project.Id = null;
            //project.LivingUnitCount = null;
            //project.ClassificationType = null;
            //project.ClassificationTypeOtherDescription = null;
            //project.DesignType = null;
            //project.DesignTypeOtherDescription = null;
            return project;
        }

        private Category CreateCategory()
        {
            Category category = new Category();
            //category.Id = null;
            //category.Type = null;
            //category.TypeOtherDescription = null;
            return category;
        }

        private DwellingUnit CreateDwellingUnit()
        {
            DwellingUnit dwellingUnit = new DwellingUnit();
            //dwellingUnit.Id = null;
            //dwellingUnit.BedroomCount = null;
            //dwellingUnit.PropertyRehabilitationCompletionDate = null;
            //dwellingUnit.EligibleRentAmount = null;
            //dwellingUnit.LeaseProvidedIndicator = null;
            return dwellingUnit;
        }

        private Mismo.Closing2_6.ManufacturedHome CreateManufacturedHome()
        {
            Mismo.Closing2_6.ManufacturedHome manufacturedHome = new Mismo.Closing2_6.ManufacturedHome();
            //manufacturedHome.Id = null;
            //manufacturedHome.LengthFeetCount = null;
            //manufacturedHome.WidthFeetCount = null;
            //manufacturedHome.AttachedToFoundationIndicator = null;
            //manufacturedHome.ConditionDescriptionType = null;
            //manufacturedHome.HudCertificationLabelIdentifier = null;
            //manufacturedHome.MakeIdentifier = null;
            //manufacturedHome.ModelIdentifier = null;
            //manufacturedHome.SerialNumberIdentifier = null;
            //manufacturedHome.WidthType = null;
            return manufacturedHome;
        }

        private PlattedLand CreatePlattedLand()
        {
            PlattedLand plattedLand = new PlattedLand();
            //plattedLand.Id = null;
            //plattedLand.PlatName = null;
            //plattedLand.PropertyBlockIdentifier = null;
            //plattedLand.PropertyLotIdentifier = null;
            //plattedLand.PropertySectionIdentifier = null;
            //plattedLand.PropertySubdivisionIdentifier = null;
            //plattedLand.PropertyTractIdentifier = null;
            //plattedLand.RecordedDocumentBook = null;
            //plattedLand.RecordedDocumentPage = null;
            //plattedLand.AdditionalParcelDescription = null;
            //plattedLand.AdditionalParcelIdentifier = null;
            //plattedLand.AppurtenanceDescription = null;
            //plattedLand.AppurtenanceIdentifier = null;
            //plattedLand.BuildingIdentifier = null;
            //plattedLand.PlatCodeIdentifier = null;
            //plattedLand.PlatInstrumentIdentifier = null;
            //plattedLand.SequenceIdentifier = null;
            //plattedLand.Type = null;
            //plattedLand.TypeOtherDescription = null;
            //plattedLand.UnitNumberIdentifier = null;
            return plattedLand;
        }

        private UnplattedLand CreateUnplattedLand()
        {
            UnplattedLand unplattedLand = new UnplattedLand();
            //unplattedLand.Id = null;
            //unplattedLand.PropertyRangeIdentifier = null;
            //unplattedLand.PropertySectionIdentifier = null;
            //unplattedLand.PropertyTownshipIdentifier = null;
            //unplattedLand.AbstractNumberIdentifier = null;
            //unplattedLand.BaseIdentifier = null;
            //unplattedLand.DescriptionType = null;
            //unplattedLand.DescriptionTypeOtherDescription = null;
            //unplattedLand.LandGrantIdentifier = null;
            //unplattedLand.MeridianIdentifier = null;
            //unplattedLand.MetesAndBoundsRemainingDescription = null;
            //unplattedLand.QuarterSectionIdentifier = null;
            //unplattedLand.SequenceIdentifier = null;
            return unplattedLand;
        }

        private FloodDetermination CreateFloodDetermination()
        {
            FloodDetermination floodDetermination = new FloodDetermination();

            if (vendorIsDOD)
            {
                if (m_dataLoan.sFloodHazardBuilding || m_dataLoan.sFloodHazardMobileHome)
                {
                    if (m_dataLoan.sFloodHazardFedInsAvail)
                        floodDetermination.FloodDeterminationId = "Flood Zone With Insurance";
                    else if (m_dataLoan.sFloodHazardFedInsNotAvail)
                        floodDetermination.FloodDeterminationId = "Flood Zone Without Insurance";
                }
                else
                {
                    floodDetermination.FloodDeterminationId = "Not in Flood Zone";
                }
            }

            floodDetermination.FloodCertificationIdentifier = this.m_dataLoan.sFloodCertId;
            //floodDetermination.FloodContractFeeAmount = null;
            floodDetermination.FloodDeterminationLifeofLoanIndicator = ToMismo(this.m_dataLoan.sFloodCertificationIsLOLUpgraded);
            floodDetermination.FloodPartialIndicator = ToMismo(this.m_dataLoan.sNfipIsPartialZone);
            floodDetermination.FloodProductCertifyDate = this.m_dataLoan.sFloodCertificationDeterminationD_rep;
            floodDetermination.NfipCommunityIdentifier = this.m_dataLoan.sFloodCertificationCommunityNum;
            floodDetermination.NfipCommunityName = this.m_dataLoan.sFloodHazardCommunityDesc;
            floodDetermination.NfipCommunityParticipationStatusType = ToMismoNfipCommunityParticipationStatusType(this.m_dataLoan.sFloodCertificationParticipationStatus);
            //floodDetermination.NfipCommunityParticipationStatusTypeOtherDescription = null;
            floodDetermination.NfipFloodZoneIdentifier = this.m_dataLoan.sNfipFloodZoneId;
            floodDetermination.NfipMapIdentifier = this.m_dataLoan.sFloodCertificationMapNum + this.m_dataLoan.sFloodCertificationPanelNums + this.m_dataLoan.sFloodCertificationPanelSuffix;
            floodDetermination.NfipMapPanelDate = this.m_dataLoan.sFloodCertificationMapD_rep;
            floodDetermination.NfipMapPanelIdentifier = this.m_dataLoan.sFloodCertificationPanelNums;
            floodDetermination.SpecialFloodHazardAreaIndicator = ToMismo(this.m_dataLoan.sFloodCertificationIsInSpecialArea);
            return floodDetermination;
        }

        /// <summary>
        /// Converts a housing expense type into a MISMO 2.6 enumeration.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="E_ProposedHousingExpenseHousingExpenseType" /> value.</returns>
        private E_ProposedHousingExpenseHousingExpenseType Convert(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.GroundRent:
                    return E_ProposedHousingExpenseHousingExpenseType.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return E_ProposedHousingExpenseHousingExpenseType.HazardInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return E_ProposedHousingExpenseHousingExpenseType.RealEstateTax;
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.FloodInsurance:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.SchoolTaxes:
                case E_HousingExpenseTypeT.Unassigned:
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense;
                default:
                    throw new ArgumentException("Unhandled value '" + type + "' for HousingExpenseType.");
            }
        }

        /// <summary>
        /// Creates a <see cref="ProposedHousingExpense" /> using TRID data.
        /// </summary>
        /// <param name="expense">The housing expense data.</param>
        /// <returns>A <see cref="ProposedHousingExpense" /> object.</returns>
        private ProposedHousingExpense CreateProposedHousingExpense_2015DataLayer(BaseHousingExpense expense)
        {
            var proposedHousingExpense = new ProposedHousingExpense();
            proposedHousingExpense.PaymentAmount = expense.MonthlyAmtTotal_rep;
            proposedHousingExpense.HousingExpenseType = Convert(expense.HousingExpenseType);

            if (proposedHousingExpense.HousingExpenseType == E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense)
            {
                proposedHousingExpense.HousingExpenseTypeOtherDescription = expense.HousingExpenseType.ToString();
            }

            return proposedHousingExpense;
        }

        private ProposedHousingExpense CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType type, string amount)
        {
            ProposedHousingExpense proposedHousingExpense = new ProposedHousingExpense();
            //proposedHousingExpense.Id = null;
            //proposedHousingExpense.HousingExpenseTypeOtherDescription = null;
            proposedHousingExpense.HousingExpenseType = type;
            proposedHousingExpense.PaymentAmount = amount;

            return proposedHousingExpense;
        }

        private ReoProperty CreateReoProperty(IRealEstateOwned field, CAppData dataApp, int appNumber)
        {
            ReoProperty reoProperty = new ReoProperty();
            reoProperty.ReoId = "REO_" + field.RecordId.ToString("N");
            reoProperty.BorrowerId = field.ReOwnerT == E_ReOwnerT.CoBorrower ? dataApp.aCMismoId : dataApp.aBMismoId;

            Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(field.RecordId);
            string str = "";
            foreach (Guid id in matchedLiabilities)
            {
                str += GenerateLiabilityId("LIA_" + id.ToString("N"), appNumber) + " ";
            }
            if (matchedLiabilities.Length > 0)
            {
                reoProperty.LiabilityId = str.TrimWhitespaceAndBOM();
            }
            reoProperty.StreetAddress = field.Addr;
            reoProperty.City = field.City;
            reoProperty.State = field.State;
            reoProperty.PostalCode = field.Zip;
            reoProperty.GsePropertyType = ToMismoReoType(field.TypeT);
            reoProperty.DispositionStatusType = ToMismo(field.StatT);
            reoProperty.LienInstallmentAmount = field.MPmt_rep;
            reoProperty.LienUpbAmount = field.MAmt_rep;
            reoProperty.MaintenanceExpenseAmount = field.HExp_rep;
            reoProperty.MarketValueAmount = field.Val_rep;
            reoProperty.RentalIncomeGrossAmount = field.GrossRentI_rep;
            reoProperty.RentalIncomeNetAmount = field.NetRentI_rep;
            reoProperty.SubjectIndicator = ToMismo(field.IsSubjectProp);

            return reoProperty;
        }

        private E_ReoPropertyDispositionStatusType ToMismo(E_ReoStatusT e_ReoStatusT)
        {
            switch (e_ReoStatusT)
            {
                case E_ReoStatusT.PendingSale: return E_ReoPropertyDispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return E_ReoPropertyDispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return E_ReoPropertyDispositionStatusType.Sold;
                default:
                    LogInvalidEnum("E_ReoStatusT", e_ReoStatusT);
                    return E_ReoPropertyDispositionStatusType.Undefined;

            }
        }

        private E_ReoPropertyGsePropertyType ToMismoReoType(E_ReoTypeT type)
        {
            switch (type)
            {
                case E_ReoTypeT._2_4Plx: return E_ReoPropertyGsePropertyType.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: return E_ReoPropertyGsePropertyType.CommercialNonResidential;
                case E_ReoTypeT.ComR: return E_ReoPropertyGsePropertyType.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: return E_ReoPropertyGsePropertyType.Condominium;
                case E_ReoTypeT.Coop: return E_ReoPropertyGsePropertyType.Cooperative;
                case E_ReoTypeT.Farm: return E_ReoPropertyGsePropertyType.Farm;
                case E_ReoTypeT.Land: return E_ReoPropertyGsePropertyType.Land;
                case E_ReoTypeT.Mixed: return E_ReoPropertyGsePropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return E_ReoPropertyGsePropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: return E_ReoPropertyGsePropertyType.SingleFamily;
                case E_ReoTypeT.Town: return E_ReoPropertyGsePropertyType.Townhouse;
                case E_ReoTypeT.Other: return E_ReoPropertyGsePropertyType.Undefined;
                default:
                    return E_ReoPropertyGsePropertyType.Undefined;
            }
        }

        private E_RespaFeeRespaSectionClassificationType GetSectionClassificationTypeFromHudline(string hudLineNumber)
        {
            int hudLineInt;
            if (int.TryParse(hudLineNumber, out hudLineInt))
            {
                if (hudLineInt < 200 && hudLineInt >= 100)
                    return E_RespaFeeRespaSectionClassificationType._100_GrossAmountDueFromBorrower;
                else if (hudLineInt < 300)
                    return E_RespaFeeRespaSectionClassificationType._200_AmountsPaidByOrInBehalfOfBorrower;
                else if (hudLineInt < 400)
                    return E_RespaFeeRespaSectionClassificationType._300_CashAtSettlementFromToBorrower;
                else if (hudLineInt < 500)
                    return E_RespaFeeRespaSectionClassificationType._400_GrossAmountDueToSeller;
                else if (hudLineInt < 600)
                    return E_RespaFeeRespaSectionClassificationType._500_ReductionsInAmountDueSeller;
                else if (hudLineInt < 700)
                    return E_RespaFeeRespaSectionClassificationType._600_CashAtSettlementToFromSeller;
                else if (hudLineInt < 800)
                    return E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions;
                else if (hudLineInt < 900)
                    return E_RespaFeeRespaSectionClassificationType._800_LoanFees;
                else if (hudLineInt < 1000)
                    return E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance;
                else if (hudLineInt < 1100)
                    return E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender;
                else if (hudLineInt < 1200)
                    return E_RespaFeeRespaSectionClassificationType._1100_TitleCharges;
                else if (hudLineInt < 1300)
                    return E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges;
                else if (hudLineInt < 1400)
                    return E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges;
            }
            return E_RespaFeeRespaSectionClassificationType.Undefined;
        }

        private E_RespaFeeType GetRespaFeeTypeFromHudline(string hudLineNumber)
        {
            switch (hudLineNumber)
            {
                case "801": return E_RespaFeeType.LoanOriginationFee;
                case "802": return E_RespaFeeType.LoanDiscountPoints;
                case "804": return E_RespaFeeType.AppraisalFee;
                case "805": return E_RespaFeeType.CreditReportFee;
                case "806": return E_RespaFeeType.TaxRelatedServiceFee;
                case "807": return E_RespaFeeType.FloodCertification;
                case "808": return E_RespaFeeType.MortgageBrokerFee;
                case "809": return E_RespaFeeType.InspectionFee;
                case "810": return E_RespaFeeType.ProcessingFee;
                case "811": return E_RespaFeeType.UnderwritingFee;
                case "812": return E_RespaFeeType.WireTransferFee;
                case "902": return GetRespaFeeType_mi902Fee();
                case "903": return E_RespaFeeType.Other;
                case "1102": return E_RespaFeeType.SettlementOrClosingFee;
                case "1109": return E_RespaFeeType.DocumentPreparationFee;
                case "1110": return E_RespaFeeType.NotaryFee;
                case "1111": return E_RespaFeeType.AttorneyFee;
                case "1201": return E_RespaFeeType.DeedRecordingFee;
                case "1204": return E_RespaFeeType.CityCountyMortgageTaxStampFee;
                case "1205": return E_RespaFeeType.StateMortgageTaxStampFee;
                case "1302": return E_RespaFeeType.PestInspectionFee;
                default:
                    return E_RespaFeeType.Other;
            }
        }

        private RespaFee CreateRespaFeeFromSellerCharges(string hudLineNumber, string description, string amount)
        {
            E_RespaFeeRespaSectionClassificationType sectionClassificationType = GetSectionClassificationTypeFromHudline(hudLineNumber);
            E_RespaFeeGfeAggregationType aggregationType = E_RespaFeeGfeAggregationType.Undefined;
            E_RespaFeeType feeType = GetRespaFeeTypeFromHudline(hudLineNumber);
            bool isPaid = false;
            int props = 512; //Sets all props to "N" and _PaidByType="Seller"
            string gfeDisclosedAmount = null;
            string paidToOverrideString = "";
            bool requireGFEDisclosedAmount = false;
            RespaFee respaFee = CreateRespaFee(sectionClassificationType,
                aggregationType, feeType, hudLineNumber,
                description, isPaid, amount, props, gfeDisclosedAmount, paidToOverrideString, requireGFEDisclosedAmount);

            respaFee.ResponsiblePartyType = E_RespaFeeResponsiblePartyType.Seller;
            return respaFee;
        }

        private PaidTo CreateRespaFeePaidTo(string hudline)
        {
            E_AgentRoleT agentRole;
            switch (hudline)
            {
                case HUDLines.APPRAISAL_FEE:
                    agentRole = E_AgentRoleT.Appraiser;
                    break;
                case HUDLines.CREDIT_REPORT_FEE:
                    agentRole = E_AgentRoleT.CreditReport;
                    break;
                default:
                    return null;
            }
            return CreatePaidTo(agentRole);
        }

        private RespaFee CreateRespaFee(E_RespaFeeRespaSectionClassificationType sectionClassificationType,
            E_RespaFeeGfeAggregationType gfeAggregationType,
            E_RespaFeeType type, string specifiedHudLineNumber, string typeOtherdescription, bool isPaid,
            string amount, int props, string totalPercent, string specifiedFixedAmount, string gfeDisclosedAmount)
        {
            const string PaidToOverrideString = "";
            RespaFee fee = CreateRespaFee(sectionClassificationType, gfeAggregationType, type, specifiedHudLineNumber,
                typeOtherdescription, isPaid, amount, props, gfeDisclosedAmount, PaidToOverrideString);
            if (fee == null) return null;
            fee.TotalPercent = totalPercent;
            fee.SpecifiedFixedAmount = specifiedFixedAmount;

            return fee;
        }

        private RespaFee CreateRespaFee(E_RespaFeeRespaSectionClassificationType sectionClassificationType,
            E_RespaFeeGfeAggregationType gfeAggregationType, E_RespaFeeType type, string specifiedHudLineNumber,
            string typeOtherdescription, bool isPaid, string amount, int props, string gfeDisclosedAmount, string paidToOverrideString)
        {
            const bool requireGFEDisclosedAmount = true;
            return CreateRespaFee(sectionClassificationType,
                gfeAggregationType, type, specifiedHudLineNumber,
                typeOtherdescription, isPaid, amount, props, gfeDisclosedAmount, paidToOverrideString, requireGFEDisclosedAmount);
        }

        private RespaFee CreateRespaFee(E_RespaFeeRespaSectionClassificationType sectionClassificationType,
            E_RespaFeeGfeAggregationType gfeAggregationType, E_RespaFeeType type, string specifiedHudLineNumber,
            string typeOtherdescription, bool isPaid, string amount, int props, string gfeDisclosedAmount,
            string paidToOverrideString, bool requireGFEDisclosedAmount)
        {
            // if ((string.IsNullOrEmpty(amount) || "0.00" == amount) && ) { return null; }
            // 12/17/2013 gf - opm 146376 This logic is mimicked in GetRespaFeeForHudLineNumber
            // If it is updated here, it should most likely be updated there as well.
            if ((string.IsNullOrEmpty(amount) || amount == "0.00") &&
                 (requireGFEDisclosedAmount && (string.IsNullOrEmpty(gfeDisclosedAmount) || gfeDisclosedAmount == "0.00")))
            { return null; }

            RespaFee respaFee = new RespaFee();
            respaFee.RespaSectionClassificationType = sectionClassificationType;
            respaFee.SpecifiedHudLineNumber = specifiedHudLineNumber;
            respaFee.Type = type;
            respaFee.TypeOtherDescription = typeOtherdescription;
            respaFee.GfeAggregationType = gfeAggregationType;

            if (LendersOffice.Common.StaticMethodsAndExtensions.IsInRange(specifiedHudLineNumber, 1100, 1199)
                || LendersOffice.Common.StaticMethodsAndExtensions.IsInRange(specifiedHudLineNumber, 1300, 1399)
                )
            {
                respaFee.BorrowerChosenProviderIndicator = ToMismo(LosConvert.GfeItemProps_Borr(props));
            }

            respaFee.PaymentList.Add(CreatePayment(amount, isPaid, props));
            //respaFee.Id = null;
            //respaFee.ItemDescription = null;
            //respaFee.PercentBasisType = null;
            //respaFee.PercentBasisTypeOtherDescription = null;
            //respaFee.RequiredProviderOfServiceIndicator = null;
            //respaFee.ResponsiblePartyType = null;
            //respaFee.SpecifiedFixedAmount = null;
            //respaFee.TotalAmount = null;
            //respaFee.TotalPercent = null;

            if (string.IsNullOrEmpty(gfeDisclosedAmount) == false)
            {
                respaFee.GfeDisclosedAmount = gfeDisclosedAmount;
            }

            respaFee.PaidToType = this.ToMismo(props, specifiedHudLineNumber, sectionClassificationType, paidToOverrideString);

            // 11/25/2014 BB - This was implemented in case 191849.
            // The additions to E_RespaFeePaidToType are non-compliant, but this will slide for now as the impact is confined to IDS and I don't have time to force them to conform.
            if (vendorIsIDS && LosConvert.GfeItemProps_ThisPartyIsAffiliate(props))
            {
                respaFee.PaidToType = E_RespaFeePaidToType.AffiliateOfLender;
            }

            respaFee.PaidToTypeOtherDescription = respaFee.PaidToType == E_RespaFeePaidToType.Other ? this.ToMismoPaidToTypeOtherDescription(LosConvert.GfeItemProps_ThisPartyIsAffiliate(props), paidToOverrideString) : string.Empty;
            //respaFee.RequiredServiceProvider = CreateRequiredServiceProvider();

            if (paidToOverrideString.Length > 0)
            {
                respaFee.PaidTo = CreatePaidTo(paidToOverrideString);
            }
            else
            {
                var paidTo = CreateRespaFeePaidTo(specifiedHudLineNumber);

                if (paidTo != null)
                {
                    respaFee.PaidTo = paidTo;
                }
            }

            /*if (respaFee.PaidTo != null)
            {
                respaFee.RequiredProviderOfServiceIndicator = E_YNIndicator.Y; 
                respaFee.RequiredServiceProvider = new RequiredServiceProvider() { Name = respaFee.PaidTo.Name, City = respaFee.PaidTo.City, PostalCode = respaFee.PaidTo.PostalCode, State = respaFee.PaidTo.State, StreetAddress = respaFee.PaidTo.StreetAddress };
            } */

            return respaFee;
        }

        private Payment CreatePayment(string amount, bool isPaid, int props)
        {
            Payment payment = new Payment();
            payment.AllowableFhaClosingCostIndicator = ToMismo(LosConvert.GfeItemProps_FhaAllow(props));
            payment.Amount = amount ?? "0.00";
            payment.IncludedInAprIndicator = ToMismo(LosConvert.GfeItemProps_AprAndPdByBorrower(props));
            payment.PaidOutsideOfClosingIndicator = ToMismo(LosConvert.GfeItemProps_Poc(props));

            int paidBy = LosConvert.GfeItemProps_Payer(props);
            if (paidBy == LosConvert.BORR_PAID_OUTOFPOCKET || paidBy == LosConvert.BORR_PAID_FINANCED)
            {
                payment.PaidByType = E_PaymentPaidByType.Buyer;
                if (paidBy == LosConvert.BORR_PAID_FINANCED)
                    payment.FinancedIndicator = E_YNIndicator.Y;
            }
            else if (paidBy == LosConvert.SELLER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Seller;
            }
            else if (paidBy == LosConvert.LENDER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Lender;
            }
            else if (paidBy == LosConvert.BROKER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Broker;
            }
            //payment.Id = null;
            //payment.CollectedByType = null;
            //payment.IncludedInStateHighCostIndicator = null;
            //payment.NetDueAmount = null;
            //payment.PaidByType = null;
            //payment.PaidByTypeThirdPartyName = null;
            //payment.Percent = null;
            //payment.ProcessType = null;
            //payment.Section32Indicator = null;
            return payment;
        }

        private RequiredServiceProvider CreateRequiredServiceProvider()
        {
            RequiredServiceProvider requiredServiceProvider = new RequiredServiceProvider();
            //requiredServiceProvider.Id = null;
            //requiredServiceProvider.City = null;
            //requiredServiceProvider.Name = null;
            //requiredServiceProvider.NatureOfRelationshipDescription = null;
            //requiredServiceProvider.PostalCode = null;
            //requiredServiceProvider.ReferenceIdentifier = null;
            //requiredServiceProvider.State = null;
            //requiredServiceProvider.StreetAddress = null;
            //requiredServiceProvider.StreetAddress2 = null;
            //requiredServiceProvider.TelephoneNumber = null;
            return requiredServiceProvider;
        }

        private TitleHolder CreateTitleHolder()
        {
            TitleHolder titleHolder = new TitleHolder();
            //titleHolder.Id = null;
            //titleHolder.LandTrustType = null;
            //titleHolder.LandTrustTypeOtherDescription = null;
            //titleHolder.Name = null;
            return titleHolder;
        }

        private TransactionDetail CreateTransactionDetail()
        {
            TransactionDetail transactionDetail = new TransactionDetail();
            transactionDetail.BorrowerPaidDiscountPointsTotalAmount = m_dataLoan.sLDiscnt1003_rep;
            transactionDetail.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            transactionDetail.EstimatedClosingCostsAmount = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            transactionDetail.MiAndFundingFeeFinancedAmount = (m_useGfeFee || exportTridData ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sFfUfmipFinanced_rep;
            transactionDetail.MiAndFundingFeeTotalAmount = (m_useGfeFee || exportTridData ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sFfUfmip1003_rep;
            transactionDetail.PrepaidItemsEstimatedAmount = m_dataLoan.sTotEstPp1003_rep;
            transactionDetail.PurchasePriceAmount = m_dataLoan.sPurchPrice_rep;

            if (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
            {
                transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            }

            //transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            transactionDetail.SellerPaidClosingCostsAmount = m_dataLoan.sTotCcPbs_rep;

            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit1Desc, m_dataLoan.sOCredit1Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit2Desc, m_dataLoan.sOCredit2Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit3Desc, m_dataLoan.sOCredit3Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit4Desc, m_dataLoan.sOCredit4Amt_rep));
            //transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit5Desc, m_dataLoan.sOCredit5Amt_rep));//168877 - will now be only in DocuTech
            transactionDetail.SalesConcessionAmount = m_dataLoan.sFHASalesConcessions_rep;
            //transactionDetail.Id = null;


            //transactionDetail.FreReserveAmount = null;
            transactionDetail.FreReservesAmount = m_dataLoan.sFredieReservesAmt_rep;

            //transactionDetail.SalesConcessionAmount = null;

            if (m_dataLoan.sLienPosT == E_sLienPosT.First)
            {
                if (m_dataLoan.sIsOFinCreditLineInDrawPeriod)
                {
                    transactionDetail.SubordinateLienAmount = m_dataLoan.sSubFin_rep;
                    transactionDetail.SubordinateLienHelocAmount = m_dataLoan.sConcurSubFin_rep;
                }
                else
                {
                    transactionDetail.SubordinateLienAmount = m_dataLoan.sConcurSubFin_rep;
                }
            }
            //transactionDetail.SubordinateLienPurposeType = null;
            //transactionDetail.SubordinateLienPurposeTypeOtherDescription = null;

            return transactionDetail;
        }

        private PurchaseCredit CreatePurchaseCredit(string desc, string amount)
        {
            if (null == amount || "" == amount || "0.00" == amount || null == desc)
                return null;

            PurchaseCredit purchaseCredit = new PurchaseCredit();
            switch (desc.ToLower())
            {
                case "cash deposit on sales contract":
                    purchaseCredit.Type = E_PurchaseCreditType.EarnestMoney;
                    break;
                case "employer assisted housing":
                    purchaseCredit.Type = E_PurchaseCreditType.EmployerAssistedHousing;
                    break;
                case "lease purchase fund":
                    purchaseCredit.Type = E_PurchaseCreditType.LeasePurchaseFund;
                    break;
                case "relocation funds":
                    purchaseCredit.Type = E_PurchaseCreditType.RelocationFunds;
                    break;
                case "seller credit":
                    purchaseCredit.SourceType = E_PurchaseCreditSourceType.PropertySeller;
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;
                case "lender credit":
                    purchaseCredit.SourceType = E_PurchaseCreditSourceType.Lender;
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;

                default:
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;
            }
            purchaseCredit.Amount = amount;
            //purchaseCredit.Id = null;
            return purchaseCredit;
        }

        private Borrower CreateBorrower_TitleOnly(string sequenceIdentifier, TitleBorrower titleBorrower, int index)
        {
            Borrower borrower = new Borrower();
            borrower.BorrowerId = string.Format("T{0}", index);
            borrower.FirstName = titleBorrower.FirstNm;
            borrower.MiddleName = titleBorrower.MidNm;
            borrower.LastName = titleBorrower.LastNm;
            borrower.Ssn = titleBorrower.SSNForExport;
            borrower.UnparsedName = titleBorrower.FullName;

            if (!string.IsNullOrEmpty(titleBorrower.POA))
            {
                var borrPOA = new PowerOfAttorney();
                borrPOA.UnparsedName = titleBorrower.POA;
                borrower.PowerOfAttorney = borrPOA;
            }

            foreach (string alias in titleBorrower.Aliases)
            {
                if (string.IsNullOrEmpty(alias))
                    continue;
                else
                    borrower.AliasList.Add(CreateAlias(alias));
            }


            if (vendorIsDocuTech)
            {
                // These fields are custom for DocuTech
                borrower.City = titleBorrower.City;
                borrower.PostalCode = titleBorrower.Zip;
                borrower.State = titleBorrower.State;
                borrower.StreetAddress = titleBorrower.Address;
            }

            borrower.MailTo = CreateMailTo(titleBorrower);

            var aRelationshipTitleT = titleBorrower.RelationshipTitleT;
            var dmRelationshipTitleT = ToMismo(aRelationshipTitleT);
            borrower.RelationshipTitleType = dmRelationshipTitleT;

            // If the value in our system doesn't have a docmagic equivalent,
            //   then send it as an Other description.
            if (aRelationshipTitleT != E_aRelationshipTitleT.Other && dmRelationshipTitleT == E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);
            else
                borrower.RelationshipTitleTypeOtherDescription = titleBorrower.RelationshipTitleTOtherDesc;

            borrower.ResidenceList.Add(CreateResidence(titleBorrower.Address, titleBorrower.City, titleBorrower.State, titleBorrower.Zip, E_aBAddrT.LeaveBlank, "", true));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Email, titleBorrower.Email));

            borrower.BorrowerIsCosignerIndicator = ToMismo(false);
            borrower.BorrowerNonObligatedIndicator = ToMismo(true);
            borrower.BorrowerNonTitleSpouseIndicator = ToMismo(false);

            borrower.NonPersonEntityIndicator = ToMismo(false);
            borrower.NonPersonEntityDetail = CreateNonPersonEntityDetailBorrower();

            return borrower;
        }

        private Borrower CreateBorrower(CAppData dataApp, string sequenceIdentifier)
        {
            Borrower borrower = new Borrower();
            borrower.BorrowerId = dataApp.aMismoId;
            borrower.JointAssetBorrowerId = dataApp.aSpouseMismoId;
            borrower.DependentCount = string.IsNullOrEmpty(dataApp.aDependNum_rep) ? "0" : dataApp.aDependNum_rep;
            borrower.JointAssetLiabilityReportingType = dataApp.aAsstLiaCompletedNotJointly ? E_BorrowerJointAssetLiabilityReportingType.NotJointly : E_BorrowerJointAssetLiabilityReportingType.Jointly;
            borrower.MaritalStatusType = ToMismo(dataApp.aMaritalStatT);
            borrower.SchoolingYears = dataApp.aSchoolYrs_rep;
            borrower.AgeAtApplicationYears = dataApp.aAge_rep;
            borrower.BirthDate = dataApp.aDob_rep;
            borrower.FirstName = dataApp.aFirstNm;
            borrower.HomeTelephoneNumber = dataApp.aHPhone;
            borrower.LastName = dataApp.aLastNm;
            borrower.MiddleName = dataApp.aMidNm;
            borrower.NameSuffix = dataApp.aSuffix;
            borrower.PrintPositionType = ToMismo(dataApp.BorrowerModeT);
            borrower.Ssn = dataApp.aSsn;
            borrower.UnparsedName = dataApp.aNm;
            borrower.MailTo = CreateMailTo(dataApp);

            var aRelationshipTitleT = dataApp.aRelationshipTitleT;
            var dmRelationshipTitleT = ToMismo(aRelationshipTitleT);
            borrower.RelationshipTitleType = dmRelationshipTitleT;

            // If the value in our system doesn't have a docmagic equivalent,
            //   then send it as an Other description.
            if (aRelationshipTitleT != E_aRelationshipTitleT.Other && dmRelationshipTitleT == E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);
            else
                borrower.RelationshipTitleTypeOtherDescription = dataApp.aRelationshipTitleOtherDesc;

            borrower.ResidenceList.Add(CreateResidence(dataApp.aAddr, dataApp.aCity, dataApp.aState, dataApp.aZip, dataApp.aAddrT, dataApp.aAddrYrs, true));
            borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev1Addr, dataApp.aPrev1City, dataApp.aPrev1State, dataApp.aPrev1Zip, (E_aBAddrT)dataApp.aPrev1AddrT, dataApp.aPrev1AddrYrs, false));
            borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev2Addr, dataApp.aPrev2City, dataApp.aPrev2State, dataApp.aPrev2Zip, (E_aBAddrT)dataApp.aPrev2AddrT, dataApp.aPrev2AddrYrs, false));

            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Base, dataApp.aBaseI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Overtime, dataApp.aOvertimeI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Bonus, dataApp.aBonusesI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Commissions, dataApp.aCommisionI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.DividendsInterest, dataApp.aDividendI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.NetRentalIncome, dataApp.aNetRentI1003_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow, dataApp.aSpPosCf_rep));
            borrower.Declaration = CreateDeclaration(dataApp);

            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    borrower.CurrentIncomeList.Add(CreateCurrentIncome(ToMismo(OtherIncome.Get_aOIDescT(otherIncome.Desc)), dataApp.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep), otherIncome.Desc));
                }
            }

            string[] dependentAges = dataApp.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');
            dependentAges.Where(p => !string.IsNullOrEmpty(p)).ToList().ForEach(p => borrower.DependentList.Add(CreateDependent(p)));


            IEmpCollection empCollection = dataApp.aEmpCollection;
            borrower.EmployerList.Add(CreateEmployer(empCollection.GetPrimaryEmp(false)));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                borrower.EmployerList.Add(CreateEmployer(record));
            }

            borrower.GovernmentMonitoring = CreateGovernmentMonitoring(dataApp);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                // Only add present housing for Borrower.
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.Rent, dataApp.aPresRent_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, dataApp.aPres1stM_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, dataApp.aPresOFin_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HazardInsurance, dataApp.aPresHazIns_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.RealEstateTax, dataApp.aPresRealETx_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.MI, dataApp.aPresMIns_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, dataApp.aPresHoAssocDues_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense, dataApp.aPresOHExp_rep));
            }


            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aBusPhone));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Mobile, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aCellPhone));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Fax, dataApp.aFax));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Email, dataApp.aEmail));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aHPhone));

            borrower.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Equifax, dataApp.aEquifaxScore_rep, dataApp.aEquifaxCreatedD_rep, dataApp.aEquifaxFactors));
            borrower.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Experian, dataApp.aExperianScore_rep, dataApp.aExperianCreatedD_rep, dataApp.aExperianFactors));
            borrower.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.TransUnion, dataApp.aTransUnionScore_rep, dataApp.aTransUnionCreatedD_rep, dataApp.aTransUnionFactors));

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower && m_dataLoan.sLT == E_sLT.FHA)
            {
                borrower.FhaBorrower = CreateFhaBorrower(dataApp);
            }

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                borrower.BorrowerIsCosignerIndicator = ToMismo(dataApp.aBTypeT == E_aTypeT.CoSigner);
                borrower.BorrowerNonObligatedIndicator = ToMismo(dataApp.aBTypeT == E_aTypeT.TitleOnly);
                borrower.BorrowerNonTitleSpouseIndicator = ToMismo(dataApp.aBTypeT == E_aTypeT.NonTitleSpouse);
            }
            else if (dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                borrower.BorrowerIsCosignerIndicator = ToMismo(dataApp.aCTypeT == E_aTypeT.CoSigner);
                borrower.BorrowerNonObligatedIndicator = ToMismo(dataApp.aCTypeT == E_aTypeT.TitleOnly);
                borrower.BorrowerNonTitleSpouseIndicator = ToMismo(dataApp.aCTypeT == E_aTypeT.NonTitleSpouse);
            }

            //borrower.CreditReportIdentifier = null;
            borrower.NonPersonEntityIndicator = ToMismo(false);
            //borrower.UrlaBorrowerTotalMonthlyIncomeAmount = null;
            //borrower.UrlaBorrowerTotalOtherIncomeAmount = null;
            borrower.ApplicationSignedDate = m_dataLoan.sAppSubmittedD_rep;
            //borrower.RelationshipTitleType = null;
            //borrower.RelationshipTitleTypeOtherDescription = null;
            borrower.SequenceIdentifier = sequenceIdentifier;
            //borrower.AliasList.Add(CreateAlias());

            PopulateBorrowerAliasList(borrower.AliasList, dataApp);
            if (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)
            {
                borrower.FhaVaBorrower = CreateFhaVaBorrower(dataApp);
            }

            //borrower.SummaryList.Add(CreateSummary());
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower && m_dataLoan.sLT == E_sLT.VA)
            {
                borrower.VaBorrower = CreateVaBorrower(dataApp);
            }
            //borrower.NearestLivingRelative = CreateNearestLivingRelative();
            borrower.NonPersonEntityDetail = CreateNonPersonEntityDetailBorrower();
            borrower.PowerOfAttorney = CreatePowerOfAttorney(dataApp);
            return borrower;
        }
        private void PopulateBorrowerAliasList(List<Alias> aliasList, CAppData dataApp)
        {
            string[] appAliases;

            // Decide whether we're using borrower or coborrower aliases
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                appAliases = dataApp.aBAliases.ToArray();

            }
            else
            {
                appAliases = dataApp.aCAliases.ToArray();
            }

            foreach (string alias in appAliases)
            {
                if (string.IsNullOrEmpty(alias)) // empty strings get no names
                {
                    continue;
                }

                aliasList.Add(CreateAlias(alias));
            }
        }

        private E_BorrowerRelationshipTitleType ToMismo(E_aRelationshipTitleT e_aRelationshipTitleT)
        {
            switch (e_aRelationshipTitleT)
            {
                case E_aRelationshipTitleT.LeaveBlank:
                    return E_BorrowerRelationshipTitleType.Undefined;
                case E_aRelationshipTitleT.AHusbandAndWife:
                    return E_BorrowerRelationshipTitleType.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                    return E_BorrowerRelationshipTitleType.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return E_BorrowerRelationshipTitleType.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                    return E_BorrowerRelationshipTitleType.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedWoman;
                case E_aRelationshipTitleT.ASingleMan:
                    return E_BorrowerRelationshipTitleType.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return E_BorrowerRelationshipTitleType.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                    return E_BorrowerRelationshipTitleType.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return E_BorrowerRelationshipTitleType.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return E_BorrowerRelationshipTitleType.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                    return E_BorrowerRelationshipTitleType.AWifeAndHusband;
                case E_aRelationshipTitleT.HerHusband:
                    return E_BorrowerRelationshipTitleType.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return E_BorrowerRelationshipTitleType.HisWife;
                case E_aRelationshipTitleT.HusbandAndWife:
                    return E_BorrowerRelationshipTitleType.HusbandAndWife;
                case E_aRelationshipTitleT.WifeAndHusband:
                    return E_BorrowerRelationshipTitleType.WifeAndHusband;
                case E_aRelationshipTitleT.NotApplicable:
                    return E_BorrowerRelationshipTitleType.NotApplicable;
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty: // fields in our system that don't completely map to MISMO fields
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.GeneralPartner:
                case E_aRelationshipTitleT.DomesticPartners:
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                case E_aRelationshipTitleT.Other:
                    return E_BorrowerRelationshipTitleType.Other;
                default:
                    throw new UnhandledEnumException(e_aRelationshipTitleT);
            }
        }

        private E_CurrentIncomeIncomeType ToMismo(E_aOIDescT e_aOIDescT)
        {
            E_CurrentIncomeIncomeType type = E_CurrentIncomeIncomeType.Undefined;
            switch (e_aOIDescT)
            {
                case E_aOIDescT.Other:
                case E_aOIDescT.CapitalGains:
                case E_aOIDescT.EmploymentRelatedAssets:
                case E_aOIDescT.ForeignIncome:
                case E_aOIDescT.RoyaltyPayment:
                case E_aOIDescT.SeasonalIncome:
                case E_aOIDescT.TemporaryLeave:
                case E_aOIDescT.TipIncome:
                case E_aOIDescT.AccessoryUnitIncome:
                case E_aOIDescT.NonBorrowerHouseholdIncome:
                case E_aOIDescT.DefinedContributionPlan:
                case E_aOIDescT.HousingAllowance:
                case E_aOIDescT.MiscellaneousIncome:
                    type = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
                    break;
                case E_aOIDescT.PublicAssistance:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                case E_aOIDescT.WorkersCompensation:
                    type = E_CurrentIncomeIncomeType.WorkersCompensation;
                    break;
                case E_aOIDescT.ContractBasis:
                    type = E_CurrentIncomeIncomeType.ContractBasis;
                    break;
                case E_aOIDescT.MilitaryBasePay:
                    type = E_CurrentIncomeIncomeType.MilitaryBasePay;
                    break;
                case E_aOIDescT.MilitaryRationsAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryRationsAllowance;
                    break;
                case E_aOIDescT.MilitaryFlightPay:
                    type = E_CurrentIncomeIncomeType.MilitaryFlightPay;
                    break;
                case E_aOIDescT.MilitaryHazardPay:
                    type = E_CurrentIncomeIncomeType.MilitaryHazardPay;
                    break;
                case E_aOIDescT.MilitaryClothesAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryClothesAllowance;
                    break;
                case E_aOIDescT.MilitaryQuartersAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryQuartersAllowance;
                    break;
                case E_aOIDescT.MilitaryPropPay:
                    type = E_CurrentIncomeIncomeType.MilitaryPropPay;
                    break;
                case E_aOIDescT.MilitaryOverseasPay:
                    type = E_CurrentIncomeIncomeType.MilitaryOverseasPay;
                    break;
                case E_aOIDescT.MilitaryCombatPay:
                    type = E_CurrentIncomeIncomeType.MilitaryCombatPay;
                    break;
                case E_aOIDescT.MilitaryVariableHousingAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
                    break;
                case E_aOIDescT.AlimonyChildSupport:
                case E_aOIDescT.Alimony:
                case E_aOIDescT.ChildSupport:
                    type = E_CurrentIncomeIncomeType.AlimonyChildSupport;
                    break;
                case E_aOIDescT.NotesReceivableInstallment:
                    type = E_CurrentIncomeIncomeType.NotesReceivableInstallment;
                    break;
                case E_aOIDescT.PensionRetirement:
                    type = E_CurrentIncomeIncomeType.Pension;
                    break;
                case E_aOIDescT.SocialSecurityDisability:
                case E_aOIDescT.SocialSecurity:
                case E_aOIDescT.Disability:
                    type = E_CurrentIncomeIncomeType.SocialSecurity;
                    break;
                case E_aOIDescT.RealEstateMortgageDifferential:
                    type = E_CurrentIncomeIncomeType.MortgageDifferential;
                    break;
                case E_aOIDescT.Trust:
                    type = E_CurrentIncomeIncomeType.Trust;
                    break;
                case E_aOIDescT.UnemploymentWelfare:
                    type = E_CurrentIncomeIncomeType.Unemployment;
                    break;
                case E_aOIDescT.AutomobileExpenseAccount:
                    type = E_CurrentIncomeIncomeType.AutomobileExpenseAccount;
                    break;
                case E_aOIDescT.FosterCare:
                    type = E_CurrentIncomeIncomeType.FosterCare;
                    break;
                case E_aOIDescT.VABenefitsNonEducation:
                    type = E_CurrentIncomeIncomeType.VABenefitsNonEducational;
                    break;
                case E_aOIDescT.SubjPropNetCashFlow:
                    type = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
                    break;
                case E_aOIDescT.BoarderIncome:
                    type = E_CurrentIncomeIncomeType.BoarderIncome;
                    break;
                case E_aOIDescT.MortgageCreditCertificate:
                    type = E_CurrentIncomeIncomeType.MortgageCreditCertificate;
                    break;
                case E_aOIDescT.TrailingCoBorrowerIncome:
                    type = E_CurrentIncomeIncomeType.TrailingCoBorrowerIncome;
                    break;
                case E_aOIDescT.HousingChoiceVoucher:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                default:
                    LogInvalidEnum("Current income type", e_aOIDescT);
                    break;
            }
            return type;
        }

        private Alias CreateAlias(string unparsedName)
        {
            Alias alias = new Alias();
            //alias.Id = null;
            //alias.AccountIdentifier = null;
            //alias.CreditorName = null;
            //alias.FirstName = null;
            //alias.LastName = null;
            //alias.MiddleName = null;
            //alias.NameSuffix = null;
            //alias.SequenceIdentifier = null;
            //alias.Type = null;
            //alias.TypeOtherDescription = null;
            alias.UnparsedName = unparsedName;
            return alias;
        }

        private MailTo CreateMailTo(TitleBorrower borrower)
        {
            MailTo mailTo = new MailTo();
            mailTo.City = borrower.City;
            mailTo.PostalCode = borrower.Zip;
            mailTo.State = borrower.State;
            mailTo.StreetAddress = borrower.Address;
            return mailTo;
        }

        private MailTo CreateMailTo(CAppData appData)
        {
            MailTo mailTo = new MailTo();
            //mailTo.AddressSameAsPropertyIndicator = ToMismo(appData.aAddrMailUsePresentAddr); 
            mailTo.City = appData.aCityMail;
            //mailTo.Country = null;
            mailTo.PostalCode = appData.aZipMail;
            mailTo.State = appData.aStateMail;
            mailTo.StreetAddress = appData.aAddrMail;
            return mailTo;
        }

        private Residence CreateResidence(string address, string city, string state, string zip, E_aBAddrT type, string years, bool current)
        {
            if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(city) && string.IsNullOrEmpty(zip) && string.IsNullOrEmpty(state))
            {
                return null;
            }
            Residence residence = new Residence();
            YearMonthParser parser = new YearMonthParser();
            parser.Parse(years);

            //residence.Id = null;
            residence.BorrowerResidencyBasisType = ToMismo(type);
            residence.BorrowerResidencyDurationMonths = parser.NumberOfMonths.ToString();
            residence.BorrowerResidencyDurationYears = parser.NumberOfYears.ToString();
            residence.BorrowerResidencyType = current ? E_ResidenceBorrowerResidencyType.Current : E_ResidenceBorrowerResidencyType.Prior;
            residence.City = city;
            //residence.Country = null;
            //residence.County = null;
            residence.PostalCode = zip;
            residence.State = state;
            residence.StreetAddress = address;
            //residence.StreetAddress2 = null;
            //residence.Landlord = CreateLandlord();
            return residence;
        }
        private E_RespaFeeGfeAggregationType ToMismo(E_GfeSectionT type)
        {
            switch (type)
            {
                case E_GfeSectionT.LeaveBlank:
                    return E_RespaFeeGfeAggregationType.Undefined;
                case E_GfeSectionT.B1:
                    return E_RespaFeeGfeAggregationType.OurOriginationCharge;
                case E_GfeSectionT.B2:
                    return E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge;
                case E_GfeSectionT.B3:
                    return E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;
                case E_GfeSectionT.B4:
                    return E_RespaFeeGfeAggregationType.TitleServices;
                case E_GfeSectionT.B5:
                    return E_RespaFeeGfeAggregationType.OwnersTitleInsurance;
                case E_GfeSectionT.B6:
                    return E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor;
                case E_GfeSectionT.B7:
                    return E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
                case E_GfeSectionT.B8:
                    return E_RespaFeeGfeAggregationType.TransferTaxes;
                case E_GfeSectionT.B9:
                case E_GfeSectionT.B10:
                case E_GfeSectionT.B11:
                    return E_RespaFeeGfeAggregationType.Undefined;
                case E_GfeSectionT.NotApplicable:
                    return E_RespaFeeGfeAggregationType.None;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_ResidenceBorrowerResidencyBasisType ToMismo(E_aBAddrT type)
        {
            switch (type)
            {
                case E_aBAddrT.Own: return E_ResidenceBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent: return E_ResidenceBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LeaveBlank: return E_ResidenceBorrowerResidencyBasisType.Undefined;
                case E_aBAddrT.LivingRentFree: return E_ResidenceBorrowerResidencyBasisType.LivingRentFree; // OPM 46198
                default:
                    LogInvalidEnum("E_aBAddrT", type);
                    return E_ResidenceBorrowerResidencyBasisType.Undefined;
            }
        }

        private Landlord CreateLandlord()
        {
            Landlord landlord = new Landlord();
            //landlord.Id = null;
            //landlord.City = null;
            //landlord.Name = null;
            //landlord.PostalCode = null;
            //landlord.State = null;
            //landlord.StreetAddress = null;
            //landlord.StreetAddress2 = null;
            //landlord.ContactDetail = CreateContactDetail();
            return landlord;
        }

        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType type, string monthlyTotalAmount)
        {
            return CreateCurrentIncome(type, monthlyTotalAmount, "");
        }

        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType type, string monthlyTotalAmount, string otherDesc)
        {
            if (monthlyTotalAmount == "" || monthlyTotalAmount == "0.00")
            {
                return null;
            }
            CurrentIncome currentIncome = new CurrentIncome();
            currentIncome.IncomeType = type;
            if (currentIncome.IncomeType == E_CurrentIncomeIncomeType.OtherTypesOfIncome)
            {
                currentIncome.IncomeTypeOtherDescription = otherDesc;
            }
            currentIncome.MonthlyTotalAmount = monthlyTotalAmount;
            //currentIncome.Id = null;
            //currentIncome.IncomeFederalTaxExemptIndicator = null;
            //currentIncome.IncomeType = null;
            //currentIncome.IncomeTypeOtherDescription = null;
            //currentIncome.MonthlyTotalAmount = null;
            return currentIncome;
        }

        // OPM 41047
        private bool DetermineFirstTimeHomeBuyerValue(CAppData dataApp)
        {
            if (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)
            {
                return dataApp.aHas1stTimeBuyerTri == E_TriState.Yes;
            }
            else
            {
                return m_dataLoan.sHas1stTimeBuyer;
            }
        }

        private Declaration CreateDeclaration(CAppData dataApp)
        {
            Declaration declaration = new Declaration();
            declaration.AlimonyChildSupportObligationIndicator = ToMismoFromDeclaration(dataApp.aDecAlimony);
            declaration.BankruptcyIndicator = ToMismoFromDeclaration(dataApp.aDecBankrupt);
            declaration.BorrowedDownPaymentIndicator = ToMismoFromDeclaration(dataApp.aDecBorrowing);
            declaration.CoMakerEndorserOfNoteIndicator = ToMismoFromDeclaration(dataApp.aDecEndorser);
            declaration.LoanForeclosureOrJudgementIndicator = ToMismoFromDeclaration(dataApp.aDecObligated);
            declaration.OutstandingJudgementsIndicator = ToMismoFromDeclaration(dataApp.aDecJudgment);
            declaration.PartyToLawsuitIndicator = ToMismoFromDeclaration(dataApp.aDecLawsuit);
            declaration.PresentlyDelinquentIndicator = ToMismoFromDeclaration(dataApp.aDecDelinquent);
            declaration.PropertyForeclosedPastSevenYearsIndicator = ToMismoFromDeclaration(dataApp.aDecForeclosure);
            declaration.BorrowerFirstTimeHomebuyerIndicator = ToMismo(DetermineFirstTimeHomeBuyerValue(dataApp));

            declaration.PriorPropertyTitleType = ToMismo(dataApp.aDecPastOwnedPropTitleT);
            declaration.PriorPropertyUsageType = ToMismo(dataApp.aDecPastOwnedPropT);

            switch (dataApp.aDecPastOwnership)
            {
                case "Y":
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Yes;
                    break;
                case "N":
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.No;
                    break;
                default:
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Undefined;
                    break;

            }
            
            switch (dataApp.aDecOcc)
            {
                case "Y":
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Yes;
                    break;
                case "N":
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.No;
                    break;
                default:
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Undefined;
                    break;
            }

            if (string.IsNullOrWhiteSpace(dataApp.aDecCitizen) && string.IsNullOrWhiteSpace(dataApp.aDecResidency))
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.Undefined;
            }
            else if (dataApp.aDecCitizen.ToUpper() == "Y")
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.USCitizen;
            }
            else if (dataApp.aDecResidency.ToUpper() == "Y")
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.PermanentResidentAlien;
            }
            else
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.NonResidentAlien;
            }
            
            return declaration;
        }

        private E_DeclarationPriorPropertyUsageType ToMismo(E_aBDecPastOwnedPropT e_aBDecPastOwnedPropT)
        {
            switch (e_aBDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.Empty: return E_DeclarationPriorPropertyUsageType.Undefined;
                case E_aBDecPastOwnedPropT.IP: return E_DeclarationPriorPropertyUsageType.Investment;
                case E_aBDecPastOwnedPropT.PR: return E_DeclarationPriorPropertyUsageType.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH: return E_DeclarationPriorPropertyUsageType.SecondaryResidence;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropT", e_aBDecPastOwnedPropT);
                    return E_DeclarationPriorPropertyUsageType.Undefined;
            }
        }

        private E_DeclarationPriorPropertyTitleType ToMismo(E_aBDecPastOwnedPropTitleT e_aBDecPastOwnedPropTitleT)
        {
            switch (e_aBDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.Empty: return E_DeclarationPriorPropertyTitleType.Undefined;
                case E_aBDecPastOwnedPropTitleT.O: return E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.S: return E_DeclarationPriorPropertyTitleType.Sole;
                case E_aBDecPastOwnedPropTitleT.SP: return E_DeclarationPriorPropertyTitleType.JointWithSpouse;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropTitleT", e_aBDecPastOwnedPropTitleT);
                    return E_DeclarationPriorPropertyTitleType.Undefined;
            }
        }

        private E_YNIndicator ToMismoFromDeclaration(string p)
        {
            switch (p.ToUpper())
            {
                case "Y": return E_YNIndicator.Y;
                case "N": return E_YNIndicator.N;
                default: return E_YNIndicator.Undefined;
            }
        }

        private E_FloodDeterminationNfipCommunityParticipationStatusType ToMismoNfipCommunityParticipationStatusType(string participationType)
        {
            if (string.IsNullOrEmpty(participationType))
            {
                return E_FloodDeterminationNfipCommunityParticipationStatusType.Undefined;
            }

            switch (participationType.ToUpper())
            {
                case "E": return E_FloodDeterminationNfipCommunityParticipationStatusType.Emergency;
                case "N": return E_FloodDeterminationNfipCommunityParticipationStatusType.NonParticipating;
                case "P": return E_FloodDeterminationNfipCommunityParticipationStatusType.Probation;
                case "R": return E_FloodDeterminationNfipCommunityParticipationStatusType.Regular;
                case "S": return E_FloodDeterminationNfipCommunityParticipationStatusType.Suspended;
                default:
                    return E_FloodDeterminationNfipCommunityParticipationStatusType.Undefined;
            }
        }

        private Explanation CreateExplanation()
        {
            Explanation explanation = new Explanation();
            //explanation.Id = null;
            //explanation.Type = E_ExplanationType.
            //explanation.Type = E_ExplanationType.R
            return explanation;
        }

        private Dependent CreateDependent(string years)
        {
            Dependent dependent = new Dependent();
            //dependent.Id = null;
            dependent.AgeYears = years;
            return dependent;
        }
        private Employer CreateEmployer(IRegularEmploymentRecord record)
        {
            if (null == record)
                return null;

            Employer employer = new Employer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = ToMismo(record.IsPrimaryEmp);
            employer.IncomeEmploymentMonthlyAmount = record.MonI_rep;
            employer.PreviousEmploymentEndDate = record.EmplmtEndD_rep;
            employer.PreviousEmploymentStartDate = record.EmplmtStartD_rep;
            employer.EmploymentCurrentIndicator = record.IsCurrent ? E_YNIndicator.Y : E_YNIndicator.N;
            return employer;

        }
        private Employer CreateEmployer(IPrimaryEmploymentRecord record)
        {
            if (record == null)
            {
                return null;
            }

            Employer employer = new Employer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.CurrentEmploymentMonthsOnJob = record.EmplmtLenInMonths_rep;
            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            if (vendorIsIDS)
            {
                employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLenYears;
                employer.CurrentEmploymentTimeInLineOfWorkMonths = record.ProfLenRemainderMonths;
            }
            else
            {
                employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;
            }

            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentCurrentIndicator = E_YNIndicator.Y;
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YNIndicator.Y;
            //            employer.IncomeEmploymentMonthlyAmount;
            //            employer.PreviousEmploymentEndDate;
            //            employer.PreviousEmploymentStartDate;
            //employer.Id = null;

            //employer.CurrentEmploymentStartDate = null;
            //employer.EmployedAbroadIndicator = null;
            //employer.EmploymentBorrowerSelfEmployedIndicator = null;
            //employer.EmploymentCurrentIndicator = null;
            //employer.EmploymentPositionDescription = null;
            //employer.EmploymentPrimaryIndicator = null;
            //employer.IncomeEmploymentMonthlyAmount = null;
            //employer.PreviousEmploymentEndDate = null;
            //employer.PreviousEmploymentStartDate = null;
            //employer.SpecialBorrowerEmployerRelationshipType = null;
            //employer.SpecialBorrowerEmployerRelationshipTypeOtherDescription = null;

            //employer.Country = null;

            return employer;
        }

        private FhaVaBorrower CreateFhaVaBorrower(CAppData dataApp)
        {
            FhaVaBorrower fhaVaBorrower = new FhaVaBorrower();
            //fhaVaBorrower.Id = null;
            fhaVaBorrower.CaivrsIdentifier = (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower) ? dataApp.aFHABCaivrsNum : dataApp.aFHACCaivrsNum;
            //fhaVaBorrower.FnmBankruptcyCount = null;
            //fhaVaBorrower.FnmBorrowerCreditRating = null;
            //fhaVaBorrower.FnmCreditReportScoreType = null;
            //fhaVaBorrower.FnmForeclosureCount = null;
            //fhaVaBorrower.VeteranStatusIndicator = null;
            //fhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType = null;
            return fhaVaBorrower;
        }

        private GovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp)
        {
            GovernmentMonitoring governmentMonitoring = new GovernmentMonitoring();
            governmentMonitoring.HmdaEthnicityType = ToMismo(dataApp.aHispanicTFallback);
            governmentMonitoring.GenderType = ToMismo(dataApp.aGenderFallback);
            governmentMonitoring.RaceNationalOriginRefusalIndicator = ToMismo(dataApp.aNoFurnish);

            if ((dataApp.aIsAmericanIndian))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.AmericanIndianOrAlaskaNative));

            if ((dataApp.aIsAsian))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.Asian));

            if ((dataApp.aIsBlack))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.BlackOrAfricanAmerican));

            if ((dataApp.aIsPacificIslander))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander));

            if ((dataApp.aIsWhite))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.White));

            //governmentMonitoring.Id = null;
            //governmentMonitoring.OtherRaceNationalOriginDescription = null;
            //governmentMonitoring.RaceNationalOriginType = null;

            return governmentMonitoring;
        }

        private E_GovernmentMonitoringGenderType ToMismo(E_GenderT e_GenderT)
        {
            switch (e_GenderT)
            {
                case E_GenderT.Female: return E_GovernmentMonitoringGenderType.Female;
                case E_GenderT.Male: return E_GovernmentMonitoringGenderType.Male;
                case E_GenderT.LeaveBlank: return E_GovernmentMonitoringGenderType.Undefined;
                case E_GenderT.NA: return E_GovernmentMonitoringGenderType.NotApplicable;
                case E_GenderT.Unfurnished: return E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown;
                default:
                    LogInvalidEnum("E_GenderT", e_GenderT);
                    return E_GovernmentMonitoringGenderType.Undefined;
            }
        }

        private E_GovernmentMonitoringHmdaEthnicityType ToMismo(E_aHispanicT e_aHispanicT)
        {
            switch (e_aHispanicT)
            {
                case E_aHispanicT.Hispanic: return E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
                case E_aHispanicT.LeaveBlank: return E_GovernmentMonitoringHmdaEthnicityType.Undefined;
                case E_aHispanicT.NotHispanic: return E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
                default:
                    LogInvalidEnum("E_aHispanicT", e_aHispanicT);
                    return E_GovernmentMonitoringHmdaEthnicityType.Undefined;
            }
        }

        private HmdaRace CreateHmdaRace(E_HmdaRaceType type)
        {
            HmdaRace hmdaRace = new HmdaRace();
            //hmdaRace.Id = null;
            hmdaRace.Type = type;
            return hmdaRace;
        }

        private PresentHousingExpense CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType type, string amount)
        {
            if ("" == amount || "0.00" == amount || "$0.00" == amount)
                return null;

            PresentHousingExpense presentHousingExpense = new PresentHousingExpense();
            //presentHousingExpense.Id = null;
            presentHousingExpense.HousingExpenseType = type;
            //presentHousingExpense.HousingExpenseTypeOtherDescription = null;
            presentHousingExpense.PaymentAmount = amount;
            return presentHousingExpense;
        }

        private Summary CreateSummary()
        {
            Summary summary = new Summary();
            //summary.Id = null;
            //summary.Amount = null;
            //summary.AmountType = null;
            //summary.AmountTypeOtherDescription = null;
            return summary;
        }

        private VaBorrower CreateVaBorrower(CAppData dataApp)
        {
            VaBorrower vaBorrower = new VaBorrower();
            //vaBorrower.Id = null;
            vaBorrower.VaCoBorrowerNonTaxableIncomeAmount = dataApp.aVaCONetI_rep;
            //vaBorrower.VaCoBorrowerTaxableIncomeAmount = null;
            vaBorrower.VaFederalTaxAmount = this.m_dataLoan.m_convertLos.ToMoneyString(dataApp.aVaBFedITax + dataApp.aVaCFedITax, FormatDirection.ToRep);
            vaBorrower.VaLocalTaxAmount = this.m_dataLoan.m_convertLos.ToMoneyString(dataApp.aVaBOITax + dataApp.aVaCOITax, FormatDirection.ToRep);
            vaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount = dataApp.aVaBONetI_rep;
            //vaBorrower.VaPrimaryBorrowerTaxableIncomeAmount = null;
            vaBorrower.VaSocialSecurityTaxAmount = this.m_dataLoan.m_convertLos.ToMoneyString(dataApp.aVaBSsnTax + dataApp.aVaCSsnTax, FormatDirection.ToRep);
            vaBorrower.VaStateTaxAmount = this.m_dataLoan.m_convertLos.ToMoneyString(dataApp.aVaBStateITax + dataApp.aVaCStateITax, FormatDirection.ToRep);
            if (dataApp.aFHABorrCertOccIsAsHome)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.A;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.B;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomePrev)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.C;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.D;
            }

            return vaBorrower;
        }

        private FhaBorrower CreateFhaBorrower(CAppData dataApp)
        {
            FhaBorrower fhaBorrower = new FhaBorrower();
            //fhaBorrower.Id = null;
            //fhaBorrower.CertificationLeadPaintIndicator = null;
            fhaBorrower.CertificationOriginalMortgageAmount = dataApp.aFHABorrCertOtherPropOrigMAmt_rep;
            fhaBorrower.CertificationOwn4OrMoreDwellingsIndicator = ToMismo(dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
            fhaBorrower.CertificationOwnOtherPropertyIndicator = ToMismo(dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            fhaBorrower.CertificationPropertySoldCity = dataApp.aFHABorrCertOtherPropCity;
            fhaBorrower.CertificationPropertySoldPostalCode = dataApp.aFHABorrCertOtherPropZip;
            fhaBorrower.CertificationPropertySoldState = dataApp.aFHABorrCertOtherPropState;
            fhaBorrower.CertificationPropertySoldStreetAddress = dataApp.aFHABorrCertOtherPropStAddr;
            fhaBorrower.CertificationPropertyToBeSoldIndicator = ToMismo(dataApp.aFHABorrCertOtherPropToBeSoldTri);
            //fhaBorrower.CertificationRentalIndicator = null;
            fhaBorrower.CertificationSalesPriceAmount = dataApp.aFHABorrCertOtherPropSalesPrice_rep;
            return fhaBorrower;
        }

        private E_YNIndicator ToMismo(E_TriState e_TriState)
        {
            switch (e_TriState)
            {
                case E_TriState.Blank: return E_YNIndicator.Undefined;
                case E_TriState.Yes: return E_YNIndicator.Y;
                case E_TriState.No: return E_YNIndicator.N;
                default:
                    return E_YNIndicator.Undefined;
            }
        }

        private NearestLivingRelative CreateNearestLivingRelative()
        {
            NearestLivingRelative nearestLivingRelative = new NearestLivingRelative();
            //nearestLivingRelative.Id = null;
            //nearestLivingRelative.City = null;
            //nearestLivingRelative.Name = null;
            //nearestLivingRelative.PostalCode = null;
            //nearestLivingRelative.RelationshipDescription = null;
            //nearestLivingRelative.State = null;
            //nearestLivingRelative.StreetAddress = null;
            //nearestLivingRelative.StreetAddress2 = null;
            //nearestLivingRelative.TelephoneNumber = null;
            return nearestLivingRelative;
        }

        private PowerOfAttorney CreatePowerOfAttorney(CAppData dataApp)
        {
            string name = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? dataApp.aBPowerOfAttorneyNm : dataApp.aCPowerOfAttorneyNm;
            if (string.IsNullOrEmpty(name))
                return null;

            PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
            //powerOfAttorney.Id = null;
            //powerOfAttorney.SigningCapacityTextDescription = null;
            //powerOfAttorney.TitleDescription = null;
            powerOfAttorney.UnparsedName = name;
            return powerOfAttorney;
        }

        private Mismo.Closing2_6.CreditScore CreateCreditScore(string borrowerId, E_CreditScoreCreditRepositorySourceType respository, string score, string creditScoreCreatedDate, string factors)
        {
            if (string.IsNullOrEmpty(score))
                return null;

            Mismo.Closing2_6.CreditScore creditScore = new Mismo.Closing2_6.CreditScore();
            creditScore.CreditScoreId = borrowerId + "_" + respository.ToString();
            //creditScore.CreditReportIdentifier = null;
            creditScore.CreditRepositorySourceType = respository;
            //creditScore.CreditRepositorySourceTypeOtherDescription = null;
            creditScore.Date = creditScoreCreatedDate;
            //creditScore.ExclusionReasonType = null;
            //creditScore.FactaInquiriesIndicator = null;
            //creditScore.ModelNameType = null;
            //creditScore.ModelNameTypeOtherDescription = null;
            creditScore.Value = score;
            if ("" != factors)
            {
                string[] parts = factors.Split('\n');
                foreach (string o in parts)
                {
                    creditScore.FactorList.Add(CreateFactor(o));
                }
            }
            return creditScore;
        }

        private Factor CreateFactor(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            Factor factor = new Factor();
            //factor.Id = null;
            //factor.Code = null;
            factor.Text = text;
            return factor;
        }

        private InvestorFeature CreateInvestorFeature()
        {
            InvestorFeature investorFeature = new InvestorFeature();
            //investorFeature.Id = null;
            //investorFeature.CategoryName = null;
            //investorFeature.Description = null;
            //investorFeature.Identifier = null;
            //investorFeature.Name = null;
            return investorFeature;
        }

        private LoanOriginationSystem CreateLoanOriginationSystem()
        {
            LoanOriginationSystem loanOriginationSystem = new LoanOriginationSystem();
            //loanOriginationSystem.Id = null;
            //loanOriginationSystem.LoanIdentifier = null;
            //loanOriginationSystem.Name = null;
            //loanOriginationSystem.VendorIdentifier = null;
            //loanOriginationSystem.VersionIdentifier = null;
            return loanOriginationSystem;
        }

        private LoanUnderwriting CreateLoanUnderwriting(string veteransAffairsDebtRatio)
        {
            LoanUnderwriting loanUnderwriting = new LoanUnderwriting();
            //loanUnderwriting.LoanUnderwritingId = null;
            //loanUnderwriting.AgencyProgramDescription = null;
            //loanUnderwriting.AutomatedUnderwritingEvaluationStatusDescription = null;
            //loanUnderwriting.AutomatedUnderwritingProcessDescription = null;
            loanUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismo(this.m_dataLoan.sProd3rdPartyUwResultT);
            loanUnderwriting.AutomatedUnderwritingSystemName = this.GetAUSName();
            //loanUnderwriting.ContractUnderwritingIndicator = null;
            loanUnderwriting.HousingExpenseRatioPercent = this.m_dataLoan.sQualTopR_rep;
            loanUnderwriting.LoanManualUnderwritingIndicator = ToMismo(this.m_dataLoan.sIsManualUw);
            //loanUnderwriting.LoanProspectorCreditRiskClassificationDescription = null;
            //loanUnderwriting.LoanProspectorDocumentationClassificationDescription = null;
            //loanUnderwriting.LoanProspectorRiskGradeAssignedDescription = null;
            loanUnderwriting.LoanUnderwriterName = this.m_dataLoan.sEmployeeUnderwriterName;
            loanUnderwriting.TotalDebtExpenseRatioPercent = this.m_dataLoan.sQualBottomR_rep;
            loanUnderwriting.CaseIdentifier = string.IsNullOrEmpty(this.m_dataLoan.sDuCaseId) ? this.m_dataLoan.sLpAusKey : this.m_dataLoan.sDuCaseId;
            loanUnderwriting.DecisionDatetime = this.GetUWDecisionDate();
            //loanUnderwriting.InvestorGuidelinesIndicator = null;
            //loanUnderwriting.MethodVersionIdentifier = null;
            //loanUnderwriting.OrganizationName = null;
            //loanUnderwriting.SubmitterType = null;
            //loanUnderwriting.SubmitterTypeOtherDescription = null;

            if (this.m_dataLoan.sLT == E_sLT.VA)
            {
                loanUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismo(this.m_dataLoan.sVaRiskT);
                loanUnderwriting.TotalDebtExpenseRatioPercent = veteransAffairsDebtRatio;
            }

            if (this.m_dataLoan.sFHARatedAcceptedByTotalScorecard)
            {
                loanUnderwriting.AutomatedUnderwritingSystemResultValue = "AA";
                loanUnderwriting.LoanUnderwriterName = this.m_dataLoan.sFHAAddendumMortgageeName;
            }
            else if (m_dataLoan.sFHARatedReferByTotalScorecard)
            {
                loanUnderwriting.AutomatedUnderwritingSystemResultValue = "Refer";
                loanUnderwriting.LoanUnderwriterName = this.m_dataLoan.sFHAAddendumUnderwriterName;
            }

            return loanUnderwriting;
        }

        /// <summary>
        /// Determines the name of the automated underwriting system that was used to produce the underwriting recommendation.
        /// </summary>
        /// <returns>The name of the automated underwriting system.</returns>
        private string GetAUSName()
        {
            string name = string.Empty;

            if (this.m_dataLoan.sIsDuUw)
            {
                name = "Desktop Underwriter";
            }
            else if (this.m_dataLoan.sIsLpUw)
            {
                name = "Loan Prospector";
            }
            else if (this.m_dataLoan.sIsOtherUw)
            {
                name = this.m_dataLoan.sOtherUwDesc;
            }

            return name;
        }

        private string ToMismo(E_sProd3rdPartyUwResultT automatedUWResult)
        {
            switch (automatedUWResult)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible: return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible: return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.DU_EAIEligible: return "EAIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible: return "EAIIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible: return "EAIIIEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferEligible: return "ReferEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible: return "ReferIneligible";
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return "ReferWithCautionIV";
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                    return "Refer";
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return "ReferWCaution";
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return "CautionEligibleForAMinus";
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return "C1Caution";
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return "Unknown";
                case E_sProd3rdPartyUwResultT.NA:
                    return string.Empty;
                case E_sProd3rdPartyUwResultT.OutOfScope:
                    return "OutofScope";
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible: return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible: return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.Total_ReferEligible: return "ReferEligible";
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible: return "ReferIneligible";
                default:
                    throw new UnhandledEnumException(automatedUWResult);
            }
        }

        private string ToMismo(E_sVaRiskT sVaRiskT)
        {
            switch (sVaRiskT)
            {
                case E_sVaRiskT.LeaveBlank: return string.Empty;
                case E_sVaRiskT.Approve: return "Approve";
                case E_sVaRiskT.Refer: return "Refer";
                default:
                    throw new UnhandledEnumException(sVaRiskT);
            }
        }

        /// <summary>
        /// Determines the underwriting decision date, which should be the date of either the approval or the denial.
        /// </summary>
        /// <returns>Either the approval date or the denial date.</returns>
        private string GetUWDecisionDate()
        {
            string decisionDate = string.Empty;

            if (this.m_dataLoan.sApprovD.IsValid)
            {
                decisionDate = (this.m_dataLoan.sApprovD.CompareTo(this.m_dataLoan.sRejectD, true) >= 0) ? this.m_dataLoan.sApprovD_rep : this.m_dataLoan.sRejectD_rep;
            }
            else if (this.m_dataLoan.sRejectD.IsValid)
            {
                decisionDate = this.m_dataLoan.sRejectD_rep;
            }

            return decisionDate;
        }

        private RelatedLoan CreateRelatedLoan(E_RelatedLoanRelatedLoanRelationshipType relationship)
        {
            RelatedLoan relatedLoan = new RelatedLoan();

            //relatedLoan.RelatedLoanId = null;
            //relatedLoan.BalloonIndicator = null;
            //relatedLoan.HelocMaximumBalanceAmount = null;
            //relatedLoan.InvestorReoPropertyIdentifier = null;
            //relatedLoan.LienHolderCity = null;
            //relatedLoan.LienHolderCountry = null;
            //relatedLoan.LienHolderPostalCode = null;
            //relatedLoan.LienHolderSameAsSubjectLoanIndicator = null;
            //relatedLoan.LienHolderState = null;
            //relatedLoan.LienHolderStreetAddress = null;
            //relatedLoan.LienHolderStreetAddress2 = null;
            //relatedLoan.LienHolderType = null;
            //relatedLoan.LienHolderTypeOtherDescription = null;
            //relatedLoan.LienHolderUnparsedName = null;
            //relatedLoan.LienPriorityType = null;
            //relatedLoan.LienPriorityTypeOtherDescription = null;
            //relatedLoan.LoanAllInPricePercent = null;
            //relatedLoan.LoanOriginalMaturityTermMonths = null;
            //relatedLoan.NegativeAmortizationType = null;
            //relatedLoan.RelatedInvestorLoanIdentifier = null;
            //relatedLoan.RelatedLoanFinancingSourceType = null;
            //relatedLoan.RelatedLoanFinancingSourceTypeOtherDescription = null;
            //relatedLoan.RelatedLoanInvestorType = null;
            //relatedLoan.RelatedLoanInvestorTypeOtherDescription = null;
            relatedLoan.RelatedLoanRelationshipType = relationship;
            //relatedLoan.RelatedLoanRelationshipTypeOtherDescription = null;
            //relatedLoan.ScheduledFirstPaymentDate = null;

            if (relationship == E_RelatedLoanRelatedLoanRelationshipType.RefinancedBySubjectLoan)
            {
                relatedLoan.OriginalLoanAmount = this.m_dataLoan.sSpOrigC_rep;
                relatedLoan.RelatedLoanUpbAmount = this.m_dataLoan.sSpLien_rep;
            }

            if ((this.m_dataLoan.sLT == E_sLT.VA) && (relationship == E_RelatedLoanRelatedLoanRelationshipType.RefinancedBySubjectLoan))
            {
                CAppData applicationZero = this.m_dataLoan.GetAppData(0);
                string noteDate = string.Empty;

                if (applicationZero.aVaApplyRegularRefiCashoutTri == E_TriState.Yes)
                {
                    noteDate = this.ToMismoDate(applicationZero.aVaApplyRegularRefiCashoutDateOfLoan);

                }
                else if (applicationZero.aVaApplyRefiNoCashoutTri == E_TriState.Yes)
                {
                    noteDate = this.ToMismoDate(applicationZero.aVaApplyRefiNoCashoutDateOfLoan);
                }

                if (!string.IsNullOrEmpty(noteDate))
                {
                    relatedLoan.NoteDate = noteDate;
                }

                switch (this.m_dataLoan.sVaPriorLoanT)
                {
                    case E_sVaPriorLoanT.ConventionalArm:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                        break;
                    case E_sVaPriorLoanT.ConventionalFixed:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                        break;
                    case E_sVaPriorLoanT.ConventionalInterestOnly:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.OtherAmortizationType;
                        relatedLoan.OtherAmortizationTypeDescription = "Interest Only";
                        break;
                    case E_sVaPriorLoanT.FhaArm:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.FHA;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                        break;
                    case E_sVaPriorLoanT.FhaFixed:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.FHA;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                        break;
                    case E_sVaPriorLoanT.LeaveBlank:
                        break;
                    case E_sVaPriorLoanT.Other:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.Other;
                        relatedLoan.OtherMortgageTypeDescription = "Other";
                        break;
                    case E_sVaPriorLoanT.VaArm:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.VA;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                        break;
                    case E_sVaPriorLoanT.VaFixed:
                        relatedLoan.MortgageType = E_RelatedLoanMortgageType.VA;
                        relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                        break;
                    default:
                        throw new UnhandledEnumException(this.m_dataLoan.sVaPriorLoanT);
                }
            }

            return relatedLoan;
        }

        /// <summary>
        /// Converts the given date string to a MISMO-formatted date string.
        /// <para>Should only be used if the LQB date field is free-form text. If the field is implemented as a date field then use the corresponding _rep field instead of this method.</para>
        /// <para>Consider migrating any such free-form text field to an actual date field instead.</para>
        /// </summary>
        /// <param name="freeformDate">The free-form text to convert to MISMO-formatted date string.</param>
        /// <returns>A MISMO-formatted date string. If the date cannot be parsed then an empty string is returned.</returns>
        private string ToMismoDate(string freeformDate)
        {
            DateTime parsedDate = DateTime.MinValue;
            DateTime.TryParse(freeformDate.TrimWhitespaceAndBOM(), out parsedDate);

            return this.m_dataLoan.m_convertLos.ToDateTimeString(parsedDate);
        }

        private UrlaTotal CreateUrlaTotal()
        {
            UrlaTotal urlaTotal = new UrlaTotal();
            //urlaTotal.Id = null;
            //urlaTotal.BorrowerId = null;
            //urlaTotal.UrlaSubtotalLiquidAssetsAmount = null;
            //urlaTotal.AssetsAmount = null;
            urlaTotal.BaseIncomeAmount = this.m_dataLoan.sLTotBaseI_rep;
            //urlaTotal.BonusIncomeAmount = null;
            urlaTotal.CashFromToBorrowerAmount = this.m_dataLoan.sTransNetCash_rep;
            //urlaTotal.CombinedPresentHousingExpenseAmount = null;
            //urlaTotal.CombinedProposedHousingExpenseAmount = null;
            //urlaTotal.CommissionsIncomeAmount = null;
            //urlaTotal.DividendsInterestIncomeAmount = null;
            urlaTotal.LiabilityMonthlyPaymentsAmount = this.m_dataLoan.sLiaMonLTot_rep;
            //urlaTotal.LiabilityUnpaidBalanceAmount = null;
            //urlaTotal.LotAndImprovementsAmount = null;
            urlaTotal.MonthlyIncomeAmount = this.m_dataLoan.sLTotI_rep;
            //urlaTotal.NetRentalIncomeAmount = null;
            //urlaTotal.NetWorthAmount = null;
            urlaTotal.OtherTypesOfIncomeAmount = this.m_dataLoan.sLTotOI_rep;
            //urlaTotal.OvertimeIncomeAmount = null;
            //urlaTotal.ReoLienInstallmentAmount = null;
            //urlaTotal.ReoLienUpbAmount = null;
            //urlaTotal.ReoMaintenanceExpenseAmount = null;
            //urlaTotal.ReoMarketValueAmount = null;
            //urlaTotal.ReoRentalIncomeGrossAmount = null;
            //urlaTotal.ReoRentalIncomeNetAmount = null;
            urlaTotal.TransactionCostAmount = m_dataLoan.sTotTransC_rep;
            //urlaTotal.HousingExpenseList.Add(CreateHousingExpense());
            return urlaTotal;
        }

        private HousingExpense CreateHousingExpense()
        {
            HousingExpense housingExpense = new HousingExpense();
            //housingExpense.Id = null;
            //housingExpense.HousingExpenseType = null;
            //housingExpense.HousingExpenseTypeOtherDescription = null;
            //housingExpense.PaymentAmount = null;
            return housingExpense;
        }

        private const string FLOOD_INSURANCE_AGENT = "FloodInsuranceAgent";
        private const string WINDSTORM_INSURANCE_AGENT = "WindstormInsuranceAgent";
        private ClosingDocuments CreateClosingDocuments()
        {
            ClosingDocuments closingDocuments = new ClosingDocuments();
            //closingDocuments.AllongeToNote = CreateAllongeToNote();
            //closingDocuments.Beneficiary = CreateBeneficiary();
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.ClosingAgent));
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Escrow));
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Title));
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.FloodProvider));
            if (!string.IsNullOrEmpty(m_dataLoan.sWindInsCompanyNm))
            {
                closingDocuments.ClosingAgentList.Add(new ClosingAgent()
                {
                    Type = E_ClosingAgentType.Other,
                    TypeOtherDescription = WINDSTORM_INSURANCE_AGENT,
                    ContactDetail = new ContactDetail() { Name = m_dataLoan.sWindInsCompanyNm }
                });
            }

            closingDocuments.ClosingInstructionsList.Add(CreateClosingInstructions());


            #region Hudline 802
            ////OPM 69510 av 8/26
            //if (m_useGfeFee) // Exporting GFE
            //{
            //    // OPM 70657 mp 11/17. Adding compensation elements for line 802.

            //    // BrokerCompensation
            //    if (m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF != 0
            //        && m_dataLoan.sGfeCreditLenderPaidItemT != E_CreditLenderPaidItemT.None
            //        && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid) // checks if originator compensation is lender paid
            //    {
            //        string brokerCompensationAmount = m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep;
            //        closingDocuments.CompensationList.Add(new Compensation()
            //        {
            //            Amount = brokerCompensationAmount,
            //            Type = E_CompensationType.BrokerCompensation,
            //            TypeOtherDescription = null,
            //            GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
            //            GfeDisclosedAmount = null,
            //            PaidToType = E_CompensationPaidToType.Lender,
            //            PaidByType = E_CompensationPaidByType.Lender, // This will always be lender for the 802
            //            Percent = m_dataLoan.sGfeDiscountPointFPc_rep
            //        });
            //    }
            //    // YieldSpreadDifferential
            //    string yieldSpreadDiffAmount;
            //    if (m_dataLoan.sGfeLenderCreditYieldSpreadDifferentialAmount != 0)
            //    {
            //        yieldSpreadDiffAmount = m_dataLoan.sGfeLenderCreditYieldSpreadDifferentialAmount_rep;
            //        closingDocuments.CompensationList.Add(new Compensation()
            //        {
            //            Amount = yieldSpreadDiffAmount,
            //            Type = E_CompensationType.YieldSpreadDifferential,
            //            TypeOtherDescription = null,
            //            GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
            //            GfeDisclosedAmount = null,
            //            PaidToType = E_CompensationPaidToType.Lender,
            //            PaidByType = E_CompensationPaidByType.Lender,
            //            Percent = m_dataLoan.sGfeDiscountPointFPc_rep
            //        });
            //    }
            //}
            //else // Exporting settlement charges
            //{
            //    decimal sLDiscnt = m_dataLoan.sSettlementLDiscnt;

            //    if (sLDiscnt < 0)
            //    {
            //        closingDocuments.CompensationList.Add(new Compensation()
            //        {
            //            Amount = (sLDiscnt * -1).ToString(),
            //            Type = E_CompensationType.Other,
            //            TypeOtherDescription = "YIELD SPREAD PREMIUM",
            //            GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
            //            GfeDisclosedAmount = m_includeGfe ? m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep : null,
            //            PaidToType = E_CompensationPaidToType.Lender,
            //            PaidByType = E_CompensationPaidByType.Lender,
            //            Percent = m_dataLoan.sSettlementLDiscntPc_rep
            //        });


            //    }
            //}

            #endregion
            //closingDocuments.CosignerList.Add(CreateCosigner());

            if (m_includeGfe)
            {
                closingDocuments.EscrowAccountDetailList.Add(CreateEscrowAccountDetail());
            }

            closingDocuments.Execution = CreateExecution();
            closingDocuments.Investor = CreateInvestor();
            closingDocuments.Lender = CreateLender();
            closingDocuments.LenderBranch = CreateLenderBranch();
            closingDocuments.LoanDetails = CreateLoanDetails();
            //closingDocuments.LossPayeeList.Add(CreateLossPayee());

            //have to use create new because fail get agent of role returns the loan officer automatically =/ 
            var mortgageBroker = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);
            //if it doesnt exist and  sGfeIsTPOTransaction is set look for a agent loan officer

            if (mortgageBroker.IsNewRecord && m_dataLoan.sGfeIsTPOTransaction)
            {
                mortgageBroker = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }

            //if somehow mortgageBroker was set by a valid object always export it.
            if (mortgageBroker.IsValid && mortgageBroker.IsNewRecord == false)
            {
                closingDocuments.MortgageBroker = CreateMortgageBroker(mortgageBroker);
            }

            //closingDocuments.PaymentDetails = CreatePaymentDetails();
            if (m_dataLoan.sRefPdOffAmt1003Lckd)
            {
                if (m_dataLoan.sRefPdOffAmt1003_rep != "")
                {
                    closingDocuments.PayoffList.Add(CreatePayoff(m_dataLoan.sRefPdOffAmt1003_rep, ""));
                }
            }
            else
            {
                foreach (CAppData application in m_dataLoan.Apps)
                {
                    ILiaCollection aLiaCollection = application.aLiaCollection;
                    int count = aLiaCollection.CountRegular;
                    for (int i = 0; i < count; i++)
                    {
                        ILiabilityRegular record = aLiaCollection.GetRegularRecordAt(i);
                        if (record.PayoffTiming != E_Timing.Before_Closing && record.WillBePdOff && record.PayoffAmt_rep != "0.00")
                        {
                            closingDocuments.PayoffList.Add(CreatePayoff(record.PayoffAmt_rep, record.ComNm));
                        }
                    }
                }
            }

            closingDocuments.RecordableDocumentList.Add(CreateRecordableDocument());

            //if (m_dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.SETTLEMENT) // opm 142911
            PopulateRespaHudDetailList(closingDocuments.RespaHudDetailList);

            closingDocuments.RespaServicingData = CreateRespaServicingData();
            closingDocuments.RespaSummary = CreateRespaSummary();
            PopulateClosingDocumentsSellerList(closingDocuments.SellerList);
            CAgentFields servicing = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (servicing.IsValid)
            {
                closingDocuments.ServicerList.Add(CreateServicer(servicing));
            }
            closingDocuments.BuilderList.Add(CreateBuilder());
            //closingDocuments.ClosingCostList.Add(CreateClosingCost());
            closingDocuments.TrustList.Add(CreateTrust());
            return closingDocuments;
        }

        private void PopulateClosingDocumentsSellerList(List<Seller> sellerList)
        {
            if (m_dataLoan.sSellerCollection.IsEmpty()) return;
            foreach (var seller in m_dataLoan.sSellerCollection.ListOfSellers)
            {
                sellerList.Add(CreateSeller(seller));
            }
        }

        private void PopulateRespaHudDetailList(List<RespaHudDetail> ls)
        {
            // Gross amount due
            // 101 Contract sales price
            // Unmapped

            // 102 Personal property
            ls.Add(CreateRespaHudDetail(
                "102",
                "Personal property",
                m_dataLoan.sGrossDueFromBorrPersonalProperty_rep,
                null,
                null,
                true,
                false));

            // 103 Settlement charges to borrower
            // Unmapped

            // 104 - Do not send 104: reserved for payoffs OPM 80612, m.p.
            //ls.Add(CreateRespaHudDetail(
            //    "104",
            //    m_dataLoan.sGrossDueFromBorrU1FDesc,
            //    m_dataLoan.sGrossDueFromBorrU1F_rep,
            //    null,
            //    null,
            //    true,
            //    false));

            // 105
            ls.Add(CreateRespaHudDetail(
                "105",
                m_dataLoan.sGrossDueFromBorrU2FDesc,
                m_dataLoan.sGrossDueFromBorrU2F_rep,
                null,
                null,
                true,
                false));

            // 401 Contract sales price
            // Unmapped

            // 402 Personal property
            ls.Add(CreateRespaHudDetail(
                "402",
                "Personal property",
                m_dataLoan.sGrossDueToSellerPersonalProperty_rep,
                null,
                null,
                false,
                true));

            // 403
            ls.Add(CreateRespaHudDetail(
                "403",
                m_dataLoan.sGrossDueToSellerU1FDesc,
                m_dataLoan.sGrossDueToSellerU1F_rep,
                null,
                null,
                false,
                true));

            // 404
            ls.Add(CreateRespaHudDetail(
                "404",
                m_dataLoan.sGrossDueToSellerU2FDesc,
                m_dataLoan.sGrossDueToSellerU2F_rep,
                null,
                null,
                false,
                true));

            // 405
            ls.Add(CreateRespaHudDetail(
                "405",
                m_dataLoan.sGrossDueToSellerU3FDesc,
                m_dataLoan.sGrossDueToSellerU3F_rep,
                null,
                null,
                false,
                true));

            // Adjustments for items paid by seller in advance
            // 106/406 City/town taxes
            ls.Add(CreateRespaHudDetail(
                "106",
                "City/town taxes",
                m_dataLoan.sPdBySellerCityTaxF_rep,
                m_dataLoan.sPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sPdBySellerCityTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "406",
                "City/town taxes",
                m_dataLoan.sPdBySellerCityTaxF_rep,
                m_dataLoan.sPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sPdBySellerCityTaxFEndD_rep,
                false,
                true));

            // 107/407 County taxes
            ls.Add(CreateRespaHudDetail(
                "107",
                "County taxes",
                m_dataLoan.sPdBySellerCountyTaxF_rep,
                m_dataLoan.sPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sPdBySellerCountyTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "407",
                "County taxes",
                m_dataLoan.sPdBySellerCountyTaxF_rep,
                m_dataLoan.sPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sPdBySellerCountyTaxFEndD_rep,
                false,
                true));

            // 108/408 Assessments
            ls.Add(CreateRespaHudDetail(
                "108",
                "Assessments",
                m_dataLoan.sPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "408",
                "Assessments",
                m_dataLoan.sPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFEndD_rep,
                false,
                true));

            // For 109-112/409-412 and 213-219/513-519, append "{startDate} to {endDate}" as part of the description,
            // and do not include the HUD1LineItemFromDate and HUD1LineItemToDate attributes for now
            // This function will take in the start date and the end date and return a string: "{startDate} to {endDate}" if
            // both are nonempty, "{date}" if only one is nonempty, "" if neither are nonempty.
            Func<CDateTime, CDateTime, string> JoinDates = (start, end) => string.Join(" to ", (new string[] { start.ToString("MM/dd/yy"), end.ToString("MM/dd/yy") }).Where((x) => !string.IsNullOrEmpty(x)).ToArray());

            // 109/409
            var itemsPdBySellerInAdvance = new[]
            {
                new { Hudline = "09", Desc = m_dataLoan.sPdBySellerU1FDesc, Amt = m_dataLoan.sPdBySellerU1F_rep, StartD = m_dataLoan.sPdBySellerU1FStartD, EndD = m_dataLoan.sPdBySellerU1FEndD },
                new { Hudline = "10", Desc = m_dataLoan.sPdBySellerU2FDesc, Amt = m_dataLoan.sPdBySellerU2F_rep, StartD = m_dataLoan.sPdBySellerU2FStartD, EndD = m_dataLoan.sPdBySellerU2FEndD },
                new { Hudline = "11", Desc = m_dataLoan.sPdBySellerU3FDesc, Amt = m_dataLoan.sPdBySellerU3F_rep, StartD = m_dataLoan.sPdBySellerU3FStartD, EndD = m_dataLoan.sPdBySellerU3FEndD },
                new { Hudline = "12", Desc = m_dataLoan.sPdBySellerU4FDesc, Amt = m_dataLoan.sPdBySellerU4F_rep, StartD = m_dataLoan.sPdBySellerU4FStartD, EndD = m_dataLoan.sPdBySellerU4FEndD },
            };

            // Add the lines which have a description
            foreach (var item in itemsPdBySellerInAdvance)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    "1" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    true,
                    false));

                ls.Add(CreateRespaHudDetail(
                    "4" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

            // 200. Amount Paid by or in Behalf of Borrower
            // Unmapped

            // 201 Deposit or earnest money
            ls.Add(CreateRespaHudDetail(
                "201",
                "Deposit or Earnest money",
                m_dataLoan.sTotCashDeposit_rep,
                null,
                null,
                true,
                false));

            // 202 Principal amount of new loans
            // Unmapped

            // 20X Custom lines
            var itemsPdByBorrower = new[]
            {
                new { Hudline = m_dataLoan.sPdByBorrowerU1FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU1FDesc, Amt = m_dataLoan.sPdByBorrowerU1F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU2FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU2FDesc, Amt = m_dataLoan.sPdByBorrowerU2F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU3FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU3FDesc, Amt = m_dataLoan.sPdByBorrowerU3F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU4FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU4FDesc, Amt = m_dataLoan.sPdByBorrowerU4F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU5FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU5FDesc, Amt = m_dataLoan.sPdByBorrowerU5F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU6FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU6FDesc, Amt = m_dataLoan.sPdByBorrowerU6F_rep },
            };

            // Add the lines which have a description
            foreach (var item in itemsPdByBorrower)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    item.Hudline,
                    item.Desc,
                    item.Amt,
                    null,
                    null,
                    true,
                    false));
            }

            // 500 Reductions in Amount due to Seller
            // 501 Excess deposit
            ls.Add(CreateRespaHudDetail(
                "501",
                "Excess deposit",
                m_dataLoan.sReductionsDueToSellerExcessDeposit_rep,
                null,
                null,
                false,
                true));

            // 502 Settlement charges to seller (line 1400)
            // Unmapped

            // 504 Payoff of first mortgage loan
            ls.Add(CreateRespaHudDetail(
                "504",
                "Payoff of first mortgage loan",
                m_dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                null,
                null,
                false,
                true));

            // 505 Payoff of second mortgage loan
            ls.Add(CreateRespaHudDetail(
                "505",
                "Payoff of second mortgage loan",
                m_dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                null,
                null,
                false,
                true));

            // 50X Custom lines
            var itemsReducedDueToSeller = new[]
            {
                new { Hudline = m_dataLoan.sReductionsDueToSellerU1FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU1FDesc, Amt = m_dataLoan.sReductionsDueToSellerU1F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU2FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU2FDesc, Amt = m_dataLoan.sReductionsDueToSellerU2F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU3FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU3FDesc, Amt = m_dataLoan.sReductionsDueToSellerU3F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU4FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU4FDesc, Amt = m_dataLoan.sReductionsDueToSellerU4F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU5FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU5FDesc, Amt = m_dataLoan.sReductionsDueToSellerU5F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU6FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU6FDesc, Amt = m_dataLoan.sReductionsDueToSellerU6F_rep },
            };

            // Add the lines which have a description
            foreach (var item in itemsReducedDueToSeller)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    item.Hudline,
                    item.Desc,
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

            // Adjustments for items unpaid by seller
            // 210/510 City/town taxes
            ls.Add(CreateRespaHudDetail(
                "210",
                "City/town taxes",
                m_dataLoan.sUnPdBySellerCityTaxF_rep,
                m_dataLoan.sUnPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCityTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "510",
                "City/town taxes",
                m_dataLoan.sUnPdBySellerCityTaxF_rep,
                m_dataLoan.sUnPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCityTaxFEndD_rep,
                false,
                true));

            // 211/511 County taxes
            ls.Add(CreateRespaHudDetail(
                "211",
                "County taxes",
                m_dataLoan.sUnPdBySellerCountyTaxF_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "511",
                "County taxes",
                m_dataLoan.sUnPdBySellerCountyTaxF_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFEndD_rep,
                false,
                true));

            // 212/512 Assessments
            ls.Add(CreateRespaHudDetail(
                "212",
                "Assessments",
                m_dataLoan.sUnPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "512",
                "Assessments",
                m_dataLoan.sUnPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep,
                false,
                true));

            // 213/513
            // 214/514
            // 215/515
            // 216/516
            // 217/517
            // 218/518
            // 219/519
            var itemsUnPdBySeller = new[]
            {
                new { Hudline = "13", Desc = m_dataLoan.sUnPdBySellerU1FDesc, Amt = m_dataLoan.sUnPdBySellerU1F_rep, StartD = m_dataLoan.sUnPdBySellerU1FStartD, EndD = m_dataLoan.sUnPdBySellerU1FEndD },
                new { Hudline = "14", Desc = m_dataLoan.sUnPdBySellerU2FDesc, Amt = m_dataLoan.sUnPdBySellerU2F_rep, StartD = m_dataLoan.sUnPdBySellerU2FStartD, EndD = m_dataLoan.sUnPdBySellerU2FEndD },
                new { Hudline = "15", Desc = m_dataLoan.sUnPdBySellerU3FDesc, Amt = m_dataLoan.sUnPdBySellerU3F_rep, StartD = m_dataLoan.sUnPdBySellerU3FStartD, EndD = m_dataLoan.sUnPdBySellerU3FEndD },
                new { Hudline = "16", Desc = m_dataLoan.sUnPdBySellerU4FDesc, Amt = m_dataLoan.sUnPdBySellerU4F_rep, StartD = m_dataLoan.sUnPdBySellerU4FStartD, EndD = m_dataLoan.sUnPdBySellerU4FEndD },
                new { Hudline = "17", Desc = m_dataLoan.sUnPdBySellerU5FDesc, Amt = m_dataLoan.sUnPdBySellerU5F_rep, StartD = m_dataLoan.sUnPdBySellerU5FStartD, EndD = m_dataLoan.sUnPdBySellerU5FEndD },
                new { Hudline = "18", Desc = m_dataLoan.sUnPdBySellerU6FDesc, Amt = m_dataLoan.sUnPdBySellerU6F_rep, StartD = m_dataLoan.sUnPdBySellerU6FStartD, EndD = m_dataLoan.sUnPdBySellerU6FEndD },
                new { Hudline = "19", Desc = m_dataLoan.sUnPdBySellerU7FDesc, Amt = m_dataLoan.sUnPdBySellerU7F_rep, StartD = m_dataLoan.sUnPdBySellerU7FStartD, EndD = m_dataLoan.sUnPdBySellerU7FEndD },
            };

            // Add the lines which have a description
            foreach (var item in itemsUnPdBySeller)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    "2" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    true,
                    false));

                ls.Add(CreateRespaHudDetail(
                    "5" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

            // 701
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "701",
                LineItemDescription = m_dataLoan.sRealEstateBrokerFees1FAgentName,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFees1F_rep
            });

            // 702
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "702",
                LineItemDescription = m_dataLoan.sRealEstateBrokerFees2FAgentName,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFees2F_rep
            });

            // 703
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "703",
                LineItemPaidByType = E_RespaHudDetailLineItemPaidByType.Buyer,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep
            });
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "703",
                LineItemPaidByType = E_RespaHudDetailLineItemPaidByType.Seller,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep
            });
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "703",
                LineItemDescription = "Commission paid at settlement"
            });

            // 704
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "704",
                LineItemPaidByType = E_RespaHudDetailLineItemPaidByType.Buyer,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFeesU1FFromBorr_rep
            });
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "704",
                LineItemPaidByType = E_RespaHudDetailLineItemPaidByType.Seller,
                LineItemAmount = m_dataLoan.sRealEstateBrokerFeesU1FFromSeller_rep
            });
            ls.Add(new RespaHudDetail()
            {
                SpecifiedHudLineNumber = "704",
                LineItemDescription = m_dataLoan.sRealEstateBrokerFeesU1FDesc
            });
        }

        private RespaHudDetail CreateRespaHudDetail(
            string _specifiedHudLineNumber,
            string _lineItemDescription,
            string _lineItemAmount,
            string _HUD1LineItemFromDate,
            string _HUD1LineItemToDate,
            bool _HUD1CashToOrFromBorrowerIndicator,
            bool _HUD1CashToOrFromSellerIndicator)
        {
            if (string.IsNullOrEmpty(_lineItemAmount) || _lineItemAmount == "0.00")
                return null;

            RespaHudDetail respaHudDetail = new RespaHudDetail();
            respaHudDetail.SpecifiedHudLineNumber = _specifiedHudLineNumber;
            respaHudDetail.LineItemDescription = _lineItemDescription;
            respaHudDetail.LineItemAmount = _lineItemAmount;
            //respaHudDetail.LineItemPaidByType = null;
            //respaHudDetail.LineItemPaidByTypeOtherDescription = null;
            respaHudDetail.Hud1LineItemFromDate = _HUD1LineItemFromDate;
            respaHudDetail.Hud1LineItemToDate = _HUD1LineItemToDate;
            respaHudDetail.Hud1CashToOrFromBorrowerIndicator = ToYNIndicator(_HUD1CashToOrFromBorrowerIndicator);
            respaHudDetail.Hud1CashToOrFromSellerIndicator = ToYNIndicator(_HUD1CashToOrFromSellerIndicator);

            //respaHudDetail.Id = null;
            //respaHudDetail.Hud1ConventionalInsuredIndicator = null;
            //respaHudDetail.Hud1FileNumberIdentifier = null;
            //respaHudDetail.Hud1LenderUnparsedAddress = null;
            //respaHudDetail.Hud1LenderUnparsedName = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedAddress = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedName = null;
            //respaHudDetail.Hud1SettlementDate = null;

            return respaHudDetail;
        }



        private AllongeToNote CreateAllongeToNote()
        {
            AllongeToNote allongeToNote = new AllongeToNote();
            //allongeToNote.Id = null;
            //allongeToNote.Date = null;
            //allongeToNote.ExecutedByDescription = null;
            //allongeToNote.InFavorOfDescription = null;
            //allongeToNote.PayToTheOrderOfDescription = null;
            //allongeToNote.Type = null;
            //allongeToNote.TypeOtherDescription = null;
            //allongeToNote.WithoutRecourseDescription = null;
            //allongeToNote.AuthorizedRepresentative = CreateAuthorizedRepresentative();
            return allongeToNote;
        }

        private Beneficiary CreateBeneficiary()
        {
            Beneficiary beneficiary = new Beneficiary();
            //beneficiary.Id = null;
            //beneficiary.NonPersonEntityIndicator = null;
            //beneficiary.City = null;
            //beneficiary.Country = null;
            //beneficiary.County = null;
            //beneficiary.PostalCode = null;
            //beneficiary.State = null;
            //beneficiary.StreetAddress = null;
            //beneficiary.StreetAddress2 = null;
            //beneficiary.UnparsedName = null;
            //beneficiary.ContactDetail = CreateContactDetail();
            //beneficiary.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return beneficiary;
        }

        private ClosingAgent CreateClosingAgent(E_AgentRoleT agentRole)
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }
            ClosingAgent closingAgent = new ClosingAgent();
            //closingAgent.Id = null;
            //closingAgent.AuthorizedToSignIndicator = null;
            //closingAgent.NonPersonEntityIndicator = null;
            closingAgent.City = agent.City;
            closingAgent.State = agent.State;
            //closingAgent.Country = null;
            //closingAgent.County = null;
            //closingAgent.Identifier = null;
            closingAgent.OrderNumberIdentifier = agent.CaseNum;
            closingAgent.PostalCode = agent.Zip;
            closingAgent.StreetAddress = agent.StreetAddr;
            //closingAgent.StreetAddress2 = null;
            //closingAgent.TitleDescription = null;
            closingAgent.Type = ToMismoClosingAgentType(agentRole);
            if (agentRole == E_AgentRoleT.FloodProvider)
            {
                closingAgent.TypeOtherDescription = FLOOD_INSURANCE_AGENT;
            }
            //closingAgent.TypeOtherDescription = null;
            closingAgent.UnparsedName = agent.CompanyName;
            ContactDetail contactDetail = new ContactDetail();
            try
            {
                var parser = new CommonLib.Name();
                parser.ParseName(agent.AgentName);

                contactDetail.FirstName = parser.FirstName;
                contactDetail.MiddleName = parser.MiddleName;
                contactDetail.LastName = parser.LastName;
                contactDetail.NameSuffix = parser.Suffix;
            }
            catch (NullReferenceException)
            {
                // Unable to parse the name, let the default behavior occur instead.
            }

            contactDetail.Name = agent.AgentName;
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { RoleType = E_ContactPointRoleType.Work, Type = Mismo.Closing2_6.E_ContactPointType.Phone, Value = agent.Phone });
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { RoleType = E_ContactPointRoleType.Work, Type = Mismo.Closing2_6.E_ContactPointType.Fax, Value = agent.FaxNum });
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { RoleType = E_ContactPointRoleType.Work, Type = Mismo.Closing2_6.E_ContactPointType.Email, Value = agent.EmailAddr });

            closingAgent.ContactDetail = contactDetail;

            //closingAgent.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return closingAgent;
        }

        private E_ClosingAgentType ToMismoClosingAgentType(E_AgentRoleT agentRole)
        {
            switch (agentRole)
            {
                case E_AgentRoleT.ClosingAgent: return E_ClosingAgentType.ClosingAgent;
                case E_AgentRoleT.Escrow: return E_ClosingAgentType.EscrowCompany;
                case E_AgentRoleT.Title: return E_ClosingAgentType.TitleCompany;
                case E_AgentRoleT.FloodProvider: return E_ClosingAgentType.Other;
                default:
                    return E_ClosingAgentType.Undefined;

            }
        }

        private ClosingInstructions CreateClosingInstructions()
        {
            ClosingInstructions closingInstructions = new ClosingInstructions();

            #region Conditions
            if (this.m_dataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(this.m_dataLoan.sBrokerId, m_dataLoan.sLId, false);
                foreach (Task task in tasks)
                {
                    if (task.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue; //skip complete condition
                    }
                    closingInstructions.ConditionList.Add(CreateCondition(task.TaskSubject));
                }
            }
            else
            {
                LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();

                if (this.m_dataLoan.BrokerDB.HasLenderDefaultFeatures)
                {
                    ldSet.Retrieve(m_dataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in ldSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                            continue; // Skip complete condition.

                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
                else
                {
                    int count = m_dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = m_dataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                            continue;
                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
            }

            #endregion
            //closingInstructions.Id = null;
            //closingInstructions.FundingCutoffTime = null;
            //closingInstructions.HoursDocumentsNeededPriorToDisbursementCount = null;
            CAppData primaryApp = m_dataLoan.GetAppData(0);
            if (primaryApp != null) closingInstructions.LeadBasedPaintCertificationRequiredIndicator = ToMismo(primaryApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            closingInstructions.PreliminaryTitleReportDate = m_dataLoan.sPrelimRprtDocumentD_rep;
            closingInstructions.SpecialFloodHazardAreaIndicator = ToMismo(m_dataLoan.sFloodCertificationIsInSpecialArea);
            //closingInstructions.SpecialFloodHazardAreaIndictor = null;
            closingInstructions.TitleReportItemsDescription = m_dataLoan.sTitleReportItemsDescription;
            closingInstructions.TitleReportRequiredEndorsementsDescription = m_dataLoan.sRequiredEndorsements;
            //closingInstructions.ConsolidatedClosingConditionsDescription = null;
            closingInstructions.PropertyTaxMessageDescription = m_dataLoan.sPropertyTaxMessageDescription;
            //closingInstructions.TermiteReportRequiredIndicator = null;
            return closingInstructions;
        }

        private Condition CreateCondition(string desc)
        {
            if (string.IsNullOrEmpty(desc))
                return null;

            Condition condition = new Condition();
            //condition.Id = null;
            condition.Description = desc;
            //condition.MetIndicator = null;
            //condition.SatisfactionTimeframeType = null;
            //condition.SatisfactionTimeframeTypeOtherDescription = null;
            //condition.SatisfactionApprovedByName = null;
            //condition.SatisfactionDate = null;
            //condition.SatisfactionResponsiblePartyType = null;
            //condition.SatisfactionResponsiblePartyTypeOtherDescription = null;
            //condition.SequenceIdentifier = null;
            //condition.WaivedIndicator = null;
            return condition;
        }

        private Compensation CreateCompensation(
            string amount,
            E_CompensationPaidByType paidBy,
            E_CompensationPaidToType paidTo,
            string percent,
            E_CompensationGfeAggregationType gfeAggregationType,
            string gfeDisclosedAmount,
            E_CompensationType type,
            string typeDescription,
            string typeOtherDescription)
        {
            Compensation compensation = new Compensation();
            //compensation.Id = null;
            compensation.Amount = amount;
            compensation.PaidByType = paidBy;
            compensation.PaidToType = paidTo;
            compensation.Percent = percent;
            compensation.GfeAggregationType = gfeAggregationType;
            compensation.GfeDisclosedAmount = gfeDisclosedAmount;
            compensation.Type = type;
            compensation.TypeOtherDescription = typeOtherDescription;
            return compensation;
        }

        private Cosigner CreateCosigner()
        {
            Cosigner cosigner = new Cosigner();
            //cosigner.Id = null;
            //cosigner.TitleDescription = null;
            //cosigner.UnparsedName = null;
            return cosigner;
        }

        private EscrowAccountDetail CreateEscrowAccountDetail()
        {
            EscrowAccountDetail escrowAccountDetail = new EscrowAccountDetail();
            //escrowAccountDetail.Id = null;
            //escrowAccountDetail.TotalMonthlyPitiAmount = null;
            //escrowAccountDetail.InitialBalanceAmount = null;
            //escrowAccountDetail.MinimumBalanceAmount = null;
            //escrowAccountDetail.EscrowAccountActivityList.Add(CreateEscrowAccountActivity());
            escrowAccountDetail.GfeDisclosedInitialEscrowBalanceAmount = m_dataLoanWithGfeArchiveApplied.sGfeInitialImpoundDeposit_rep;
            escrowAccountDetail.InitialEscrowDepositIncludesOtherDescription = m_dataLoan.sGfeHasImpoundDepositDescription;
            return escrowAccountDetail;
        }

        private EscrowAccountActivity CreateEscrowAccountActivity()
        {
            EscrowAccountActivity escrowAccountActivity = new EscrowAccountActivity();
            //escrowAccountActivity.Id = null;
            //escrowAccountActivity.CurrentBalanceAmount = null;
            //escrowAccountActivity.DisbursementMonth = null;
            //escrowAccountActivity.DisbursementSequenceIdentifier = null;
            //escrowAccountActivity.DisbursementYear = null;
            //escrowAccountActivity.PaymentDescriptionType = null;
            //escrowAccountActivity.PaymentDescriptionTypeOtherDescription = null;
            //escrowAccountActivity.PaymentFromEscrowAccountAmount = null;
            //escrowAccountActivity.PaymentToEscrowAccountAmount = null;
            return escrowAccountActivity;
        }

        private Execution CreateExecution()
        {
            Execution execution = new Execution();
            //execution.Id = null;
            execution.City = m_dataLoan.sExecutionLocationCity;
            execution.County = m_dataLoan.sExecutionLocationCounty;
            //execution.Date = null;
            execution.State = m_dataLoan.sExecutionLocationState;
            return execution;
        }

        private Investor CreateInvestor()
        {
            Investor investor = new Investor();
            //investor.Id = null;
            //investor.NonPersonEntityIndicator = null;
            //investor.City = null;
            //investor.Country = null;
            //investor.County = null;
            //investor.PostalCode = null;
            //investor.State = null;
            //investor.StreetAddress = null;
            //investor.StreetAddress2 = null;
            investor.UnparsedName = m_dataLoan.sCalcInvestorNm;
            //investor.ContactDetail = CreateContactDetail();
            //investor.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //investor.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            return investor;
        }

        private RegulatoryAgency CreateRegulatoryAgency()
        {
            RegulatoryAgency regulatoryAgency = new RegulatoryAgency();
            //regulatoryAgency.Id = null;
            //regulatoryAgency.City = null;
            //regulatoryAgency.Country = null;
            //regulatoryAgency.County = null;
            //regulatoryAgency.PostalCode = null;
            //regulatoryAgency.State = null;
            //regulatoryAgency.StreetAddress = null;
            //regulatoryAgency.StreetAddress2 = null;
            //regulatoryAgency.Type = null;
            //regulatoryAgency.TypeOtherDescription = null;
            //regulatoryAgency.UnparsedName = null;
            //regulatoryAgency.ContactDetail = CreateContactDetail();
            return regulatoryAgency;
        }

        private Lender CreateLender()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (agent.IsValid == false)
            {
                return null;
            }

            Lender lender = new Lender();
            //lender.Id = null;
            lender.NonPersonEntityIndicator = XmlSerializableCommon.E_YNIndicator.Y;
            lender.City = agent.City;
            //lender.Country = null;
            //lender.County = null;
            //lender.DocumentsOrderedByName = null;
            //lender.FunderName = null;
            lender.PostalCode = agent.Zip;
            lender.State = agent.State;
            lender.StreetAddress = agent.StreetAddr;
            //lender.StreetAddress2 = null;
            lender.UnparsedName = agent.CompanyName;
            lender.ContactDetail = new ContactDetail() { Name = m_dataLoan.sEmployeeLoanRepName };
            //lender.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //lender.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            lender.LicenseNumberIdentifier = agent.LicenseNumOfCompany;

            return lender;
        }

        private LenderBranch CreateLenderBranch()
        {
            LenderBranch lenderBranch = new LenderBranch();
            BranchDB branchDB = new BranchDB(m_dataLoan.sBranchId, m_dataLoan.sBrokerId);
            branchDB.Retrieve();
            //lenderBranch.Id = null;
            lenderBranch.City = branchDB.Address.City;
            //lenderBranch.Country = null;
            //lenderBranch.County = null;
            lenderBranch.PostalCode = branchDB.Address.Zipcode;
            lenderBranch.State = branchDB.Address.State;
            lenderBranch.StreetAddress = branchDB.Address.StreetAddress;
            //lenderBranch.StreetAddress2 = null;
            //lenderBranch.UnparsedName = null;
            //lenderBranch.ContactDetail = CreateContactDetail();
            return lenderBranch;
        }

        private LoanDetails CreateLoanDetails()
        {
            LoanDetails loanDetails = new LoanDetails();
            //loanDetails.Id = null;
            loanDetails.ClosingDate = this.m_dataLoan.sDocMagicClosingD_rep;
            loanDetails.DisbursementDate = this.m_dataLoan.sDocMagicDisbursementD_rep;
            //loanDetails.DocumentOrderClassificationType = null;
            loanDetails.DocumentPreparationDate = this.m_dataLoan.sDocsD_rep;
            //loanDetails.FundByDate = null;
            loanDetails.LockExpirationDate = this.m_dataLoan.sRLckdExpiredD_rep;
            loanDetails.OriginalLtvRatioPercent = this.m_dataLoan.sLtvR_rep;
            loanDetails.RescissionDate = this.m_dataLoan.sDocMagicCancelD_rep;
            if (this.m_useGfeFee || this.m_includeGfe)
            {
                loanDetails.InterimInterestList.Add(CreateGfeInterimInterest());
            }
            if (!m_useGfeFee)
            {
                loanDetails.InterimInterestList.Add(CreateHud1InterimInterest());
            }

            //loanDetails.RequestToRescind = CreateRequestToRescind();
            loanDetails.GfeDetail = CreateGfeDetail();
            loanDetails.TruthInLendingDisclosureDate = this.m_dataLoan.sTilPrepareDate_rep;
            return loanDetails;
        }

        private GfeDetail CreateGfeDetail()
        {
            GfeDetail gfeDetail = new GfeDetail();
            gfeDetail.GfeInterestRateAvailableThroughDate = m_dataLoan.sIsRateLocked ? m_dataLoan.sRLckdExpiredD_rep : m_dataLoan.sGfeNoteIRAvailTillD_Date;
            // sg - OPM 227308: Block GFESettlementChargesAvailableThroughDate for DocuTech TRID Files
            gfeDetail.GfeSettlementChargesAvailableThroughDate = m_dataLoan.sDisclosureRegulationT.Equals(E_sDisclosureRegulationT.TRID) ? null : m_dataLoan.sGfeEstScAvailTillD_Date;
            gfeDetail.GfeRateLockPeriodDaysCount = m_dataLoan.sGfeRateLockPeriod_rep;
            gfeDetail.GfeRateLockMinimumDaysPriorToSettlementCount = m_dataLoan.sGfeLockPeriodBeforeSettlement_rep;
            gfeDetail.GfeDisclosureDate = m_dataLoan.sGfeTilPrepareDate_rep;
            gfeDetail.GfeLoanOriginatorFeePaymentCreditType = (E_GfeDetailGfeLoanOriginatorFeePaymentCreditType)Enum.Parse(typeof(E_GfeDetailGfeLoanOriginatorFeePaymentCreditType), m_dataLoan.sGFELoanOriginatorFeePaymentCreditType);//Should really change this Loan File field into an enum...
            PopulateGfeComparisonList(gfeDetail.GfeComparisonList);
            PopulateGfeSectionSummaryList(gfeDetail.GfeSectionSummaryList);
            return gfeDetail;
        }

        private void PopulateGfeComparisonList(List<GfeComparison> ls)
        {
            ls.Add(CreateGfeComparison(
                E_GfeComparisonType.SameLoanWithLowerSettlementCharges,
                m_dataLoan.sGfeTradeOffLowerCCNoteIR_rep,
                m_dataLoan.sGfeTradeOffLowerCCClosingCostDiff_rep
            ));
            ls.Add(CreateGfeComparison(
                E_GfeComparisonType.SameLoanWithLowerInterestRate,
                m_dataLoan.sGfeTradeOffLowerRateNoteIR_rep,
                m_dataLoan.sGfeTradeOffLowerRateClosingCostDiff_rep
            ));
        }

        private GfeComparison CreateGfeComparison(E_GfeComparisonType type, string interestRatePercent, string settlementChargesAmount)
        {
            GfeComparison gfeComparison = new GfeComparison();
            gfeComparison.Type = type;
            gfeComparison.InterestRatePercent = interestRatePercent;
            gfeComparison.SettlementChargesAmount = settlementChargesAmount;
            return gfeComparison;
        }

        private void PopulateGfeSectionSummaryList(List<GfeSectionSummary> ls)
        {
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.One, m_dataLoanWithGfeArchiveApplied.sGfeOriginationF_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Two, m_dataLoanWithGfeArchiveApplied.sLDiscnt_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.A, m_dataLoanWithGfeArchiveApplied.sGfeAdjOriginationCharge_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Three, m_dataLoanWithGfeArchiveApplied.sGfeRequiredServicesTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Four, m_dataLoanWithGfeArchiveApplied.sGfeLenderTitleTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Five, m_dataLoanWithGfeArchiveApplied.sGfeOwnerTitleTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Six, m_dataLoanWithGfeArchiveApplied.sGfeServicesYouShopTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Seven, m_dataLoanWithGfeArchiveApplied.sGfeGovtRecTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Eight, m_dataLoanWithGfeArchiveApplied.sGfeTransferTaxTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Nine, m_dataLoanWithGfeArchiveApplied.sGfeInitialImpoundDeposit_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Ten, m_dataLoanWithGfeArchiveApplied.sGfeDailyInterestTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Eleven, m_dataLoanWithGfeArchiveApplied.sGfeHomeOwnerInsuranceTotalFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.B, m_dataLoanWithGfeArchiveApplied.sGfeTotalOtherSettlementServiceFee_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.AB, m_dataLoanWithGfeArchiveApplied.sGfeTotalEstimateSettlementCharge_rep));
            ls.Add(CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType.Other, m_dataLoanWithGfeArchiveApplied.sGfeNotApplicableF_rep));
        }
        private GfeSectionSummary CreateGfeSectionSummary(E_GfeSectionSummaryGfeSectionIdentifierType gfeSectionIdentifierType, string gfeDisclosedTotalAmount)
        {
            GfeSectionSummary gfeSectionSummary = new GfeSectionSummary();
            gfeSectionSummary.GfeSectionIdentifierType = gfeSectionIdentifierType;
            gfeSectionSummary.GfeDisclosedTotalAmount = gfeDisclosedTotalAmount;
            return gfeSectionSummary;
        }

        private InterimInterest CreateGfeInterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;

            if (use2015DataLayer)
            {
                BorrowerClosingCostFee interimInterestFee = (BorrowerClosingCostFee)m_dataLoanWithGfeArchiveApplied.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sIPia, PrincipalFactory.CurrentPrincipal.BrokerId, false);
                if (interimInterestFee.IsValidFee())
                {
                    interimInterest.TotalPerDiemAmount = interimInterestFee.TotalAmount_rep;
                }
            }
            else
            {
                interimInterest.TotalPerDiemAmount = m_dataLoanWithGfeArchiveApplied.sIPia_rep;
            }

            if (this.m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE)
            {
                interimInterest.DisclosureType = E_InterimInterestDisclosureType.GFE;
            }

            interimInterest.PaidFromDate = this.m_dataLoan.sConsummationD_rep;
            interimInterest.PaidNumberOfDays = string.IsNullOrEmpty(m_dataLoanWithGfeArchiveApplied.sIPiaDy_rep) ? "0" : m_dataLoanWithGfeArchiveApplied.sIPiaDy_rep; // OPM 192839 Export zeros for Days of Interest paid in advance.
            interimInterest.PaidThroughDate = this.InterimInterestPaidThroughDate();

            if (m_dataLoan.sDaysInYr_rep == "365")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (m_dataLoan.sDaysInYr_rep == "360")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }

            interimInterest.PerDiemPaymentOptionType = E_InterimInterestPerDiemPaymentOptionType.Closing;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            interimInterest.SinglePerDiemAmount = m_dataLoanWithGfeArchiveApplied.sIPerDay_rep;

            return interimInterest;
        }

        private InterimInterest CreateHud1InterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;
            interimInterest.PaidFromDate = this.m_dataLoan.sUseGFEDataForSCFields ? this.m_dataLoan.sConsummationD_rep : this.m_dataLoan.sSchedFundD_rep;
            interimInterest.PaidNumberOfDays = string.IsNullOrEmpty(m_dataLoan.sSettlementIPiaDy_rep) ? "0" : m_dataLoan.sSettlementIPiaDy_rep; // OPM 192839 Export zeros for Days of Interest paid in advance.

            interimInterest.PaidThroughDate = this.InterimInterestPaidThroughDate();

            if (m_dataLoan.sDaysInYr_rep == "365")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (m_dataLoan.sDaysInYr_rep == "360")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }
            interimInterest.DisclosureType = E_InterimInterestDisclosureType.HUD1;

            interimInterest.PerDiemPaymentOptionType = E_InterimInterestPerDiemPaymentOptionType.Closing;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            interimInterest.SinglePerDiemAmount = m_dataLoan.sSettlementIPerDay_rep;
            //interimInterest.TotalPerDiemAmount = m_dataLoan.sSettlementIPia_rep;   this is ignored in hud1 
            return interimInterest;
        }

        private string InterimInterestPaidThroughDate()
        {
            if (this.m_dataLoan.sSchedDueD1.IsValid)
            {
                return this.m_dataLoan.m_convertLos.ToDateTimeString(this.m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(-1));
            }

            return string.Empty;
        }

        private RequestToRescind CreateRequestToRescind()
        {
            RequestToRescind requestToRescind = new RequestToRescind();
            //requestToRescind.Id = null;
            //requestToRescind.NotificationCity = null;
            //requestToRescind.NotificationCountry = null;
            //requestToRescind.NotificationPostalCode = null;
            //requestToRescind.NotificationState = null;
            //requestToRescind.NotificationStreetAddress = null;
            //requestToRescind.NotificationStreetAddress2 = null;
            //requestToRescind.NotificationUnparsedName = null;
            //requestToRescind.TransactionDate = null;
            return requestToRescind;
        }

        private LossPayee CreateLossPayee()
        {
            LossPayee lossPayee = new LossPayee();
            //lossPayee.Id = null;
            //lossPayee.NonPersonEntityIndicator = null;
            //lossPayee.City = null;
            //lossPayee.Country = null;
            //lossPayee.County = null;
            //lossPayee.PostalCode = null;
            //lossPayee.State = null;
            //lossPayee.StreetAddress = null;
            //lossPayee.StreetAddress2 = null;
            //lossPayee.Type = null;
            //lossPayee.TypeOtherDescription = null;
            //lossPayee.UnparsedName = null;
            //lossPayee.ContactDetail = CreateContactDetail();
            //lossPayee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return lossPayee;
        }

        private MortgageBroker CreateMortgageBroker(CAgentFields agent)
        {
            if (agent.IsValid == false)
            {
                return null;
            }
            MortgageBroker mortgageBroker = new MortgageBroker();
            //mortgageBroker.Id = null;
            //mortgageBroker.NonPersonEntityIndicator = null;
            mortgageBroker.City = agent.City;
            //mortgageBroker.Country = null;
            //mortgageBroker.County = null;
            mortgageBroker.LicenseNumberIdentifier = agent.LicenseNumOfCompany;
            mortgageBroker.PostalCode = agent.Zip;
            mortgageBroker.State = agent.State;
            mortgageBroker.StreetAddress = agent.StreetAddr;
            //mortgageBroker.StreetAddress2 = null;
            mortgageBroker.UnparsedName = agent.CompanyName;
            mortgageBroker.ContactDetail = CreateContactDetailAssumeRoleTypeWork(agent);
            //mortgageBroker.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //mortgageBroker.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            return mortgageBroker;
        }

        private PaymentDetails CreatePaymentDetails()
        {
            PaymentDetails paymentDetails = new PaymentDetails();
            //paymentDetails.AmortizationScheduleList.Add(CreateAmortizationSchedule());
            //paymentDetails.PaymentScheduleList.Add(CreatePaymentSchedule());
            return paymentDetails;
        }

        private AmortizationSchedule CreateAmortizationSchedule()
        {
            AmortizationSchedule amortizationSchedule = new AmortizationSchedule();
            //amortizationSchedule.Id = null;
            //amortizationSchedule.EndingBalanceAmount = null;
            //amortizationSchedule.InterestRatePercent = null;
            //amortizationSchedule.LoanToValuePercent = null;
            //amortizationSchedule.MiPaymentAmount = null;
            //amortizationSchedule.PaymentAmount = null;
            //amortizationSchedule.PaymentDueDate = null;
            //amortizationSchedule.PaymentNumber = null;
            //amortizationSchedule.PortionOfPaymentDistributedToInterestAmount = null;
            //amortizationSchedule.PortionOfPaymentDistributedToPrincipalAmount = null;
            return amortizationSchedule;
        }

        private PaymentSchedule CreatePaymentSchedule()
        {
            PaymentSchedule paymentSchedule = new PaymentSchedule();
            //paymentSchedule.Id = null;
            //paymentSchedule.PaymentAmount = null;
            //paymentSchedule.PaymentSequenceIdentifier = null;
            //paymentSchedule.PaymentStartDate = null;
            //paymentSchedule.PaymentVaryingToAmount = null;
            //paymentSchedule.TotalNumberOfPaymentsCount = null;
            return paymentSchedule;
        }

        private Payoff CreatePayoff(string amount, string companyName)
        {
            Payoff payoff = new Payoff();
            //payoff.Id = null;
            //payoff.AccountNumberIdentifier = null;
            payoff.Amount = amount;
            //payoff.PerDiemAmount = null;
            //payoff.SequenceIdentifier = null;
            payoff.SpecifiedHudLineNumber = liabilityPayoffHudlineCounter.ToString();
            liabilityPayoffHudlineCounter++;
            //payoff.ThroughDate = null;
            payoff.Payee = new Payee() { UnparsedName = companyName };
            return payoff;
        }

        private Payee CreatePayee()
        {
            Payee payee = new Payee();
            //payee.Id = null;
            //payee.NonPersonEntityIndicator = null;
            //payee.City = null;
            //payee.Country = null;
            //payee.County = null;
            //payee.PostalCode = null;
            //payee.State = null;
            //payee.StreetAddress = null;
            //payee.StreetAddress2 = null;
            //payee.UnparsedName = null;
            //payee.ContactDetail = CreateContactDetail();
            //payee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return payee;
        }

        private RecordableDocument CreateRecordableDocument()
        {
            RecordableDocument recordableDocument = new RecordableDocument();
            //recordableDocument.Id = null;
            //recordableDocument.SequenceIdentifier = null;
            //recordableDocument.Type = null;
            //recordableDocument.TypeOtherDescription = null;
            //recordableDocument.AssignFrom = CreateAssignFrom();
            //recordableDocument.AssignTo = CreateAssignTo();
            recordableDocument.Default = CreateDefault();
            //recordableDocument.NotaryList.Add(CreateNotary());
            //recordableDocument.PreparedBy = CreatePreparedBy();
            //recordableDocument.AssociatedDocument = CreateAssociatedDocument();
            //recordableDocument.ReturnToList.Add(CreateReturnTo());
            //recordableDocument.Riders = CreateRiders();
            recordableDocument.SecurityInstrument = CreateSecurityInstrument();

            CAgentFields trustee = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (trustee.IsValid)
            {
                recordableDocument.TrusteeList.Add(CreateTrustee(trustee));
            }
            //recordableDocument.WitnessList.Add(CreateWitness());

            return recordableDocument;
        }

        private AssignFrom CreateAssignFrom()
        {
            AssignFrom assignFrom = new AssignFrom();
            //assignFrom.Id = null;
            //assignFrom.NonPersonEntityIndicator = null;
            //assignFrom.City = null;
            //assignFrom.Country = null;
            //assignFrom.County = null;
            //assignFrom.CountyFipsCode = null;
            //assignFrom.PostalCode = null;
            //assignFrom.SigningOfficialName = null;
            //assignFrom.SigningOfficialTitleDescription = null;
            //assignFrom.State = null;
            //assignFrom.StreetAddress = null;
            //assignFrom.StreetAddress2 = null;
            //assignFrom.UnparsedName = null;
            //assignFrom.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignFrom;
        }

        private AssignTo CreateAssignTo()
        {
            AssignTo assignTo = new AssignTo();
            //assignTo.Id = null;
            //assignTo.NonPersonEntityIndicator = null;
            //assignTo.City = null;
            //assignTo.Country = null;
            //assignTo.County = null;
            //assignTo.CountyFipsCode = null;
            //assignTo.PostalCode = null;
            //assignTo.State = null;
            //assignTo.StreetAddress = null;
            //assignTo.StreetAddress2 = null;
            //assignTo.UnparsedName = null;
            //assignTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignTo;
        }

        private Default CreateDefault()
        {
            Default _default = new Default();
            //_default.Id = null;
            //_default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = null;
            //_default.ApplicationFeesAmount = null;
            //_default.ClosingPreparationFeesAmount = null;
            //_default.LendingInstitutionPostOfficeBoxAddress = null;
            _default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = ToMismo(m_dataLoan.sAcknowledgeCashAdvanceForNonHomestead);
            return _default;
        }

        private Notary CreateNotary()
        {
            Notary notary = new Notary();
            //notary.Id = null;
            //notary.AppearanceDate = null;
            //notary.AppearedBeforeNamesDescription = null;
            //notary.AppearedBeforeTitlesDescription = null;
            //notary.City = null;
            //notary.CommissionBondNumberIdentifier = null;
            //notary.CommissionCity = null;
            //notary.CommissionCounty = null;
            //notary.CommissionExpirationDate = null;
            //notary.CommissionNumberIdentifier = null;
            //notary.CommissionState = null;
            //notary.County = null;
            //notary.PostalCode = null;
            //notary.State = null;
            //notary.StreetAddress = null;
            //notary.StreetAddress2 = null;
            //notary.TitleDescription = null;
            //notary.UnparsedName = null;
            //notary.CertificateList.Add(CreateCertificate());
            //notary.DataVersionList.Add(CreateDataVersion());
            return notary;
        }

        private Certificate CreateCertificate()
        {
            Certificate certificate = new Certificate();
            //certificate.Id = null;
            //certificate.SignerCompanyName = null;
            //certificate.SignerFirstName = null;
            //certificate.SignerLastName = null;
            //certificate.SignerMiddleName = null;
            //certificate.SignerNameSuffix = null;
            //certificate.SignerTitleDescription = null;
            //certificate.SignerUnparsedName = null;
            //certificate.SigningCounty = null;
            //certificate.SigningDate = null;
            //certificate.SigningState = null;
            //certificate.SignerIdentification = CreateSignerIdentification();
            return certificate;
        }

        private SignerIdentification CreateSignerIdentification()
        {
            SignerIdentification signerIdentification = new SignerIdentification();
            //signerIdentification.Id = null;
            //signerIdentification.Description = null;
            //signerIdentification.Type = null;
            return signerIdentification;
        }

        private PreparedBy CreatePreparedBy()
        {
            PreparedBy preparedBy = new PreparedBy();
            //preparedBy.Id = null;
            //preparedBy.NonPersonEntityIndicator = null;
            //preparedBy.City = null;
            //preparedBy.Country = null;
            //preparedBy.CountryCode = null;
            //preparedBy.County = null;
            //preparedBy.CountyFipsCode = null;
            //preparedBy.ElectronicRoutingAddress = null;
            //preparedBy.ElectronicRoutingMethodType = null;
            //preparedBy.PostalCode = null;
            //preparedBy.State = null;
            //preparedBy.StateFipsCode = null;
            //preparedBy.StreetAddress = null;
            //preparedBy.StreetAddress2 = null;
            //preparedBy.TelephoneNumber = null;
            //preparedBy.TitleDescription = null;
            //preparedBy.UnparsedName = null;
            //preparedBy.ContactDetail = CreateContactDetail();
            return preparedBy;
        }

        private AssociatedDocument CreateAssociatedDocument()
        {
            AssociatedDocument associatedDocument = new AssociatedDocument();
            //associatedDocument.Id = null;
            //associatedDocument.BookNumber = null;
            //associatedDocument.BookType = null;
            //associatedDocument.BookTypeOtherDescription = null;
            //associatedDocument.CountyOfRecordationName = null;
            //associatedDocument.InstrumentNumber = null;
            //associatedDocument.Number = null;
            //associatedDocument.OfficeOfRecordationName = null;
            //associatedDocument.PageNumber = null;
            //associatedDocument.RecordingDate = null;
            //associatedDocument.RecordingJurisdictionName = null;
            //associatedDocument.StateOfRecordationName = null;
            //associatedDocument.TitleDescription = null;
            //associatedDocument.Type = null;
            //associatedDocument.TypeOtherDescription = null;
            //associatedDocument.VolumeNumber = null;
            return associatedDocument;
        }

        private ReturnTo CreateReturnTo()
        {
            ReturnTo returnTo = new ReturnTo();
            //returnTo.Id = null;
            //returnTo.NonPersonEntityIndicator = null;
            //returnTo.City = null;
            //returnTo.Country = null;
            //returnTo.CountryCode = null;
            //returnTo.County = null;
            //returnTo.CountyFipsCode = null;
            //returnTo.ElectronicRoutingAddress = null;
            //returnTo.ElectronicRoutingMethodType = null;
            //returnTo.PostalCode = null;
            //returnTo.State = null;
            //returnTo.StateFipsCode = null;
            //returnTo.StreetAddress = null;
            //returnTo.StreetAddress2 = null;
            //returnTo.TitleDescription = null;
            //returnTo.UnparsedName = null;
            //returnTo.ContactDetail = CreateContactDetail();
            //returnTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return returnTo;
        }

        private Riders CreateRiders()
        {
            Riders riders = new Riders();
            //riders.Id = null;
            //riders.AdjustableRateRiderIndicator = null;
            //riders.BalloonRiderIndicator = null;
            //riders.BiweeklyPaymentRiderIndicator = null;
            //riders.CondominiumRiderIndicator = null;
            //riders.GraduatedPaymentRiderIndicator = null;
            //riders.GrowingEquityRiderIndicator = null;
            //riders.NonOwnerOccupancyRiderIndicator = null;
            //riders.OneToFourFamilyRiderIndicator = null;
            //riders.OtherRiderDescription = null;
            //riders.OtherRiderIndicator = null;
            //riders.PlannedUnitDevelopmentRiderIndicator = null;
            //riders.RateImprovementRiderIndicator = null;
            //riders.RehabilitationLoanRiderIndicator = null;
            //riders.SecondHomeRiderIndicator = null;
            //riders.VaRiderIndicator = null;
            return riders;
        }

        private SecurityInstrument CreateSecurityInstrument()
        {
            SecurityInstrument securityInstrument = new SecurityInstrument();
            //securityInstrument.Id = null;
            //securityInstrument.PropertyLongLegalDescriptionPageNumberDescription = null;
            //securityInstrument.PropertyLongLegalPageNumberDescription = null;
            //securityInstrument.RecordedDocumentIdentifier = null;
            //securityInstrument.AssumptionFeeAmount = null;
            //securityInstrument.AttorneyFeeMinimumAmount = null;
            securityInstrument.AttorneyFeePercent = m_dataLoan.sSecInstrAttorneyFeesPc_rep;
            //securityInstrument.CertifyingAttorneyName = null;
            //securityInstrument.MaximumPrincipalIndebtednessAmount = null;
            //securityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.NoteHolderName = null;
            //securityInstrument.NoticeOfConfidentialtyRightsDescription = null;
            //securityInstrument.OtherFeesAmount = null;
            //securityInstrument.OtherFeesDescription = null;
            securityInstrument.OweltyOfPartitionIndicator = ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.OweltyOfPartition);
            //securityInstrument.PersonAuthorizedToReleaseLienName = null;
            //securityInstrument.PersonAuthorizedToReleaseLienPhoneNumber = null;
            //securityInstrument.PersonAuthorizedToReleaseLienTitle = null;
            securityInstrument.PurchaseMoneyIndicator = ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.PurchaseMoney);
            //securityInstrument.RealPropertyImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.RealPropertyImprovementsNotCoveredIndicator = null;
            //securityInstrument.RecordingRequestedByName = null;
            securityInstrument.RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator =
                ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.RenewalAndExtensionOfLiens);
            securityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = ToMismo(m_dataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator);
            securityInstrument.RealPropertyImprovedOrToBeImprovedIndicator = ToMismo(m_dataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovedOrToBeImprovedIndicator);
            securityInstrument.RealPropertyImprovementsNotCoveredIndicator = ToMismo(m_dataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovementsNotCoveredIndicator);
            //securityInstrument.SignerForRegistersOfficeName = null;
            //securityInstrument.TaxSerialNumberIdentifier = null;
            securityInstrument.TrusteeFeePercent = m_dataLoan.sTrusteeFeePc_rep;
            //securityInstrument.VendorsLienDescription = null;
            //securityInstrument.VendorsLienIndicator = null;
            securityInstrument.VestingDescription = m_dataLoan.sVestingToRead;
            //securityInstrument.TaxablePartyList.Add(CreateTaxableParty());
            return securityInstrument;
        }

        private TaxableParty CreateTaxableParty()
        {
            TaxableParty taxableParty = new TaxableParty();
            //taxableParty.Id = null;
            //taxableParty.City = null;
            //taxableParty.Country = null;
            //taxableParty.CountryCode = null;
            //taxableParty.County = null;
            //taxableParty.PostalCode = null;
            //taxableParty.SequenceIdentifier = null;
            //taxableParty.State = null;
            //taxableParty.StreetAddress = null;
            //taxableParty.StreetAddress2 = null;
            //taxableParty.TitleDescription = null;
            //taxableParty.UnparsedName = null;
            //taxableParty.PreferredResponseList.Add(CreatePreferredResponse());
            //taxableParty.ContactDetail = CreateContactDetail();
            return taxableParty;
        }

        private PreferredResponse CreatePreferredResponse()
        {
            PreferredResponse preferredResponse = new PreferredResponse();
            //preferredResponse.Id = null;
            //preferredResponse.MimeType = null;
            //preferredResponse.Destination = null;
            //preferredResponse.Format = null;
            //preferredResponse.FormatOtherDescription = null;
            //preferredResponse.Method = null;
            //preferredResponse.MethodOther = null;
            //preferredResponse.UseEmbeddedFileIndicator = null;
            //preferredResponse.VersionIdentifier = null;
            return preferredResponse;
        }

        private Trustee CreateTrustee(CAgentFields agentFields)
        {
            Trustee trustee = new Trustee();
            //trustee.Id = null;
            //trustee.NonPersonEntityIndicator = null;
            trustee.City = agentFields.City;
            //trustee.Country = null;
            //trustee.County = null;
            trustee.PostalCode = agentFields.Zip;
            trustee.State = agentFields.State;
            trustee.StreetAddress = agentFields.StreetAddr;
            //trustee.StreetAddress2 = null;
            //trustee.Type = null;
            //trustee.TypeOtherDescription = null;
            trustee.UnparsedName = agentFields.CompanyName;
            trustee.ContactDetail = CreateContactDetail(agentFields);
            //trustee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return trustee;
        }

        private Witness CreateWitness()
        {
            Witness witness = new Witness();
            //witness.Id = null;
            //witness.SequenceIdentifier = null;
            //witness.UnparsedName = null;
            return witness;
        }

        private RespaServicingData CreateRespaServicingData()
        {
            RespaServicingData respaServicingData = new RespaServicingData();
            //respaServicingData.Id = null;
            //respaServicingData.AreAbleToServiceIndicator = null;
            respaServicingData.AssignSellOrTransferSomeServicingIndicator = ToYNIndicator(m_dataLoan.sAppliedProgramTransfered);
            //respaServicingData.ExpectToAssignSellOrTransferPercent = null;
            //respaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator = null;
            //respaServicingData.ExpectToRetainAllServicingIndicator = null;
            //respaServicingData.ExpectToSellAllServicingIndicator = null;
            respaServicingData.FirstTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear1;
            respaServicingData.FirstTransferYearValue = percentOrEmpty(m_dataLoan.sFirstLienLoanTransferRecordYear1Percent);
            //respaServicingData.HaveNotDecidedToServiceIndicator = null;
            respaServicingData.HaveNotServicedInPastThreeYearsIndicator = ToYNIndicator(m_dataLoan.sDontServiceInPast3Yrs);
            respaServicingData.HavePreviouslyAssignedServicingIndicator = ToYNIndicator(m_dataLoan.sFirstLienLoanPreviouslyTransfered);
            respaServicingData.SecondTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear2;
            respaServicingData.SecondTransferYearValue = percentOrEmpty(m_dataLoan.sFirstLienLoanTransferRecordYear2Percent);
            respaServicingData.ThirdTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear3;
            respaServicingData.ThirdTransferYearValue = percentOrEmpty(m_dataLoan.sFirstLienLoanTransferRecordYear3Percent);
            //respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = null;
            //respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = null;
            respaServicingData.ThisIsOurRecordOfTransferringServicingIndicator = ToYNIndicator(m_dataLoan.sFirstLienLoanDeclareServiceRecord);
            //respaServicingData.TwelveMonthPeriodTransferPercent = null;
            //respaServicingData.WillNotServiceIndicator = null;
            respaServicingData.MayAssignServicingIndicator = ToMismo(m_dataLoan.sMayAssignSellTransfer);

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // The DoNotServiceIndicator depends on whether there is possible intent to service
                // or whether the loan will explicitly not be serviced by the lender. We take both
                // the Servicing Disclosure and LE datapoints into account, with the LE taking precedence
                // if it conflicts with the Servicing Disclosure. (OPM 246737) 
                respaServicingData.DoNotServiceIndicator = ToMismo(
                    !m_dataLoan.sMayAssignSellTransfer &&
                    !m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan &&
                    (!m_dataLoan.sDontService ||
                    !m_dataLoan.sServiceDontIntendToTransfer));
                respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(!m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
                respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            }
            else
            {
                if (m_dataLoan.sMayAssignSellTransfer || m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.DoNotServiceIndicator = ToMismo(m_dataLoan.sDontService);
                }

                if (m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(m_dataLoan.sDontService);
                    respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sServiceDontIntendToTransfer);
                }
            }

            return respaServicingData;
        }

        /// <summary>
        /// OPM 116547: We are currently exporting the string "%   " for these values when they are uninitialized. 
        /// Export blank instead when they are uninitalized, to follow MISMO standards.
        /// </summary>
        /// <param name="transferRecord"></param>
        /// <returns></returns>
        private string percentOrEmpty(string transferRecord)
        {
            if (string.IsNullOrEmpty(transferRecord.Trim('%', ' '))) return "";
            return transferRecord;
        }

        private RespaSummary CreateRespaSummary()
        {
            RespaSummary respaSummary = new RespaSummary();
            //respaSummary.Id = null;
            respaSummary.Apr = this.m_dataLoan.sApr_rep;

            if (this.m_dataLoan.sIsPropertyBeingSoldByCreditor)
            {
                respaSummary.DisclosedTotalSalesPriceAmount = this.m_dataLoan.sPurchPrice_rep;
            }
            //respaSummary.TotalAprFeesAmount = null;
            respaSummary.TotalAmountFinancedAmount = this.m_dataLoan.sFinancedAmt_rep;
            //respaSummary.TotalDepositedReservesAmount = null;
            //respaSummary.TotalFilingRecordingFeeAmount = null;
            respaSummary.TotalFinanceChargeAmount = this.m_dataLoan.sFinCharge_rep;
            //respaSummary.TotalNetBorrowerFeesAmount = null;
            //respaSummary.TotalNetProceedsForFundingAmount = null;
            //respaSummary.TotalNetSellerFeesAmount = null;
            //respaSummary.TotalNonAprFeesAmount = null;
            respaSummary.TotalOfAllPaymentsAmount = this.m_dataLoan.sSchedPmtTot_rep;
            //respaSummary.TotalPaidToOthersAmount = null;
            //respaSummary.TotalPrepaidFinanceChargeAmount = null;
            //respaSummary.TotalFeesPaidToList.Add(CreateTotalFeesPaidTo());
            //respaSummary.TotalFeesPaidByList.Add(CreateTotalFeesPaidBy());
            return respaSummary;
        }

        private TotalFeesPaidTo CreateTotalFeesPaidTo()
        {
            TotalFeesPaidTo totalFeesPaidTo = new TotalFeesPaidTo();
            //totalFeesPaidTo.Id = null;
            //totalFeesPaidTo.Type = null;
            //totalFeesPaidTo.TypeAmount = null;
            //totalFeesPaidTo.TypeOtherDescription = null;
            return totalFeesPaidTo;
        }

        private TotalFeesPaidBy CreateTotalFeesPaidBy()
        {
            TotalFeesPaidBy totalFeesPaidBy = new TotalFeesPaidBy();
            //totalFeesPaidBy.Id = null;
            //totalFeesPaidBy.Type = null;
            //totalFeesPaidBy.TypeAmount = null;
            //totalFeesPaidBy.TypeOtherDescription = null;
            return totalFeesPaidBy;
        }

        private Seller CreateSeller(DataAccess.Sellers.Seller sellerInput)
        {
            Seller seller = new Seller();
            //seller.Id = null;
            seller.NonPersonEntityIndicator = ToMismo(sellerInput.Type == E_SellerType.Entity);
            seller.City = sellerInput.Address.City;
            //seller.Country = null;
            //seller.FirstName = null;
            //seller.Identifier = null;
            //seller.LastName = null;
            //seller.MiddleName = null;
            //seller.NameSuffix = null;
            seller.PostalCode = sellerInput.Address.PostalCode;
            //seller.SequenceIdentifier = null;
            seller.State = sellerInput.Address.State;
            seller.StreetAddress = sellerInput.Address.StreetAddress;
            //seller.StreetAddress2 = null;
            seller.UnparsedName = sellerInput.Name;
            //seller.ContactDetail = CreateContactDetail();
            seller.NonPersonEntityDetail = CreateNonPersonEntityDetail(sellerInput.EntityType);
            //seller.PowerOfAttorney = CreatePowerOfAttorney();
            return seller;
        }

        private E_NonPersonEntityDetailOrganizationType ToMismo(E_SellerEntityType sellerEntityType)
        {
            switch (sellerEntityType)
            {
                case E_SellerEntityType.Corporation: return E_NonPersonEntityDetailOrganizationType.Corporation;
                case E_SellerEntityType.GeneralPartnership: return E_NonPersonEntityDetailOrganizationType.Partnership;
                case E_SellerEntityType.LimitedLiabilityCompany: return E_NonPersonEntityDetailOrganizationType.LimitedLiabilityCompany;
                case E_SellerEntityType.LimitedPartnership: return E_NonPersonEntityDetailOrganizationType.LimitedPartnership;
                case E_SellerEntityType.SoleProprietor: return E_NonPersonEntityDetailOrganizationType.SoleProprietorship;
                case E_SellerEntityType.Company:
                case E_SellerEntityType.LimitedLiabilityCorporation: return E_NonPersonEntityDetailOrganizationType.Other;
                default:
                    return E_NonPersonEntityDetailOrganizationType.Undefined;
            }
        }

        private Servicer CreateServicer(CAgentFields agentFields)
        {
            Servicer servicer = new Servicer();
            //servicer.Id = null;
            //servicer.NonPersonEntityIndicator = null;
            servicer.City = agentFields.City;
            //servicer.Country = null;
            //servicer.DaysOfTheWeekDescription = null;
            //servicer.DirectInquiryToDescription = null;
            //servicer.HoursOfTheDayDescription = null;
            //servicer.InquiryTelephoneNumber = null;
            servicer.Name = agentFields.CompanyName;
            //servicer.PaymentAcceptanceEndDate = null;
            //servicer.PaymentAcceptanceStartDate = null;
            servicer.PostalCode = agentFields.Zip;
            servicer.State = agentFields.State;
            servicer.StreetAddress = agentFields.StreetAddr;
            //servicer.StreetAddress2 = null;
            servicer.TransferEffectiveDate = this.m_dataLoan.sGLServTransEffD_rep;
            //servicer.Type = null;
            //servicer.TypeOtherDescription = null;
            //servicer.ContactDetail = CreateContactDetail();
            servicer.NonPersonEntityDetail = CreateNonPersonEntityDetail(agentFields);
            //servicer.QualifiedWrittenRequestMailTo = CreateQualifiedWrittenRequestMailTo();
            return servicer;
        }

        private QualifiedWrittenRequestMailTo CreateQualifiedWrittenRequestMailTo()
        {
            QualifiedWrittenRequestMailTo qualifiedWrittenRequestMailTo = new QualifiedWrittenRequestMailTo();
            //qualifiedWrittenRequestMailTo.Id = null;
            //qualifiedWrittenRequestMailTo.City = null;
            //qualifiedWrittenRequestMailTo.Country = null;
            //qualifiedWrittenRequestMailTo.PostalCode = null;
            //qualifiedWrittenRequestMailTo.State = null;
            //qualifiedWrittenRequestMailTo.StreetAddress = null;
            //qualifiedWrittenRequestMailTo.StreetAddress2 = null;
            return qualifiedWrittenRequestMailTo;
        }

        private Builder CreateBuilder()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Builder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }
            Builder builder = new Builder();
            //builder.Id = null;
            //builder.NonPersonEntityIndicator = null;
            builder.City = agent.City;
            //builder.Country = null;
            //builder.County = null;
            builder.LIcenseIdentifier = agent.LicenseNumOfAgent;
            //builder.LicenseState = null;
            builder.PostalCode = agent.Zip;
            builder.State = agent.State;
            builder.StreetAddress = agent.StreetAddr;
            //builder.StreetAddress2 = null;
            builder.UnparsedName = agent.AgentName;
            builder.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return builder;
        }

        private Mismo.Closing2_6.ClosingCost CreateClosingCost()
        {
            Mismo.Closing2_6.ClosingCost closingCost = new Mismo.Closing2_6.ClosingCost();
            //closingCost.Id = null;
            //closingCost.ContributionAmount = null;
            //closingCost.FinancedIndicator = null;
            //closingCost.FundsType = null;
            //closingCost.FundsTypeOtherDescription = null;
            //closingCost.SourceType = null;
            //closingCost.SourceTypeOtherDescription = null;
            return closingCost;
        }

        private Trust CreateTrust()
        {
            Trust trust = new Trust();
            trust.Id = "_" + m_dataLoan.sTrustId;
            //trust.NonPersonEntityIndicator = null;
            trust.EstablishedDate = m_dataLoan.sTrustAgreementD_rep;
            trust.Name = m_dataLoan.sTrustName;
            trust.State = m_dataLoan.sTrustState;
            //trust.NonObligatedIndicator = null;
            //trust.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //trust.BeneficiaryList.Add(CreateBeneficiary());
            PopulateTrusteeList(trust.TrusteeList);
            PopulateGrantorList(trust.GrantorList);
            return trust;
        }

        private void PopulateTrusteeList(List<Trustee> ls)
        {
            int trusteeIndex = 1;
            foreach (var trustee in m_dataLoan.sTrustCollection.Trustees)
            {
                if (!string.IsNullOrWhiteSpace(trustee.Name))
                {
                    ls.Add(CreateTrustee(trusteeIndex.ToString(), trustee.Name, trustee.Phone, trustee.Address));
                    ++trusteeIndex;
                }
            }
        }

        private Trustee CreateTrustee(string id, string unparsedName, string phone, DataAccess.Trust.TrustCollection.Address address)
        {
            Trustee trustee = new Trustee();
            trustee.Id = "_" + id;
            trustee.UnparsedName = unparsedName;
            trustee.StreetAddress = address.StreetAddress;
            trustee.City = address.City;
            trustee.State = address.State;
            trustee.PostalCode = address.PostalCode;
            trustee.ContactDetail = new ContactDetail();
            trustee.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, E_ContactPointType.Phone, phone));
            return trustee;
        }

        private void PopulateGrantorList(List<Grantor> ls)
        {
            int grantorIndex = 1;
            foreach (var grantor in m_dataLoan.sTrustCollection.Trustors)
            {
                if (!string.IsNullOrWhiteSpace(grantor.Name))
                {
                    ls.Add(CreateGrantor(grantor.Name, grantorIndex.ToString()));
                    ++grantorIndex;
                }
            }
        }

        private Grantor CreateGrantor(string unparsedName, string sequenceIdentifier)
        {
            Grantor grantor = new Grantor();
            //grantor.Id = null;
            //grantor.MaritalStatusType = null;
            //grantor.NonPersonEntityIndicator = null;
            //grantor.CapacityDescription = null;
            //grantor.City = null;
            //grantor.Country = null;
            //grantor.County = null;
            //grantor.FirstName = null;
            //grantor.LastName = null;
            //grantor.MiddleName = null;
            //grantor.NameSuffix = null;
            //grantor.PostalCode = null;
            grantor.SequenceIdentifier = sequenceIdentifier;
            //grantor.State = null;
            //grantor.StreetAddress = null;
            //grantor.StreetAddress2 = null;
            grantor.UnparsedName = unparsedName;
            //grantor.AliasList.Add(CreateAlias());
            return grantor;
        }

        private EscrowAccountSummary CreateEscrowAccountSummary()
        {
            EscrowAccountSummary escrowAccountSummary = new EscrowAccountSummary();
            escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = m_useGfeFee
                ? m_dataLoanWithGfeArchiveApplied.sAggregateAdjRsrv_rep
                : m_dataLoan.sSettlementAggregateAdjRsrv_rep;
            //escrowAccountSummary.EscrowCushionNumberOfMonthsCount = null;
            return escrowAccountSummary;
        }



        private E_BorrowerMaritalStatusType ToMismo(E_aBMaritalStatT aBMaritalStatT)
        {
            switch (aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married: return E_BorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return E_BorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return E_BorrowerMaritalStatusType.Separated;
                case E_aBMaritalStatT.LeaveBlank: return E_BorrowerMaritalStatusType.NotProvided;
                default:
                    return E_BorrowerMaritalStatusType.Undefined;
            }
        }

        private E_BorrowerPrintPositionType ToMismo(E_BorrowerModeT BorrowerModeT)
        {
            switch (BorrowerModeT)
            {
                case E_BorrowerModeT.Borrower: return E_BorrowerPrintPositionType.Borrower;
                case E_BorrowerModeT.Coborrower: return E_BorrowerPrintPositionType.CoBorrower;
                default:
                    return E_BorrowerPrintPositionType.Undefined;
            }
        }

        private E_MortgageTermsMortgageType ToMismo(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.Conventional: return E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return E_MortgageTermsMortgageType.FHA;
                case E_sLT.VA: return E_MortgageTermsMortgageType.VA;
                case E_sLT.UsdaRural: return E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.Other: return E_MortgageTermsMortgageType.Other;
                default:
                    return E_MortgageTermsMortgageType.Undefined;
            }
        }

        private E_MortgageTermsLoanAmortizationType ToMismo(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed: return E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.ARM: return E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Graduated: return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    return E_MortgageTermsLoanAmortizationType.Undefined;
            }
        }

        private E_LoanFeaturesLienPriorityType ToMismo(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First: return E_LoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second: return E_LoanFeaturesLienPriorityType.SecondLien;
                default:
                    return E_LoanFeaturesLienPriorityType.Undefined;
            }
        }
        private E_LoanFeaturesGsePropertyType ToMismo(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank: return E_LoanFeaturesGsePropertyType.Undefined;
                case E_sGseSpT.Attached: return E_LoanFeaturesGsePropertyType.Attached;
                case E_sGseSpT.Condominium: return E_LoanFeaturesGsePropertyType.Condominium;
                case E_sGseSpT.Cooperative: return E_LoanFeaturesGsePropertyType.Cooperative;
                case E_sGseSpT.Detached: return E_LoanFeaturesGsePropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_LoanFeaturesGsePropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return E_LoanFeaturesGsePropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
                case E_sGseSpT.ManufacturedHousing: return E_LoanFeaturesGsePropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return E_LoanFeaturesGsePropertyType.Modular;
                case E_sGseSpT.PUD: return E_LoanFeaturesGsePropertyType.PUD;
                default:
                    return E_LoanFeaturesGsePropertyType.Undefined;
            }
        }
        private E_YNIndicator ToYNIndicator(bool value)
        {
            return value ? E_YNIndicator.Y : E_YNIndicator.N;
        }
        private E_InterviewerInformationApplicationTakenMethodType ToMismo(E_aIntrvwrMethodT aIntrvwrMethodT)
        {
            switch (aIntrvwrMethodT)
            {
                case E_aIntrvwrMethodT.LeaveBlank: return E_InterviewerInformationApplicationTakenMethodType.Undefined;
                case E_aIntrvwrMethodT.FaceToFace: return E_InterviewerInformationApplicationTakenMethodType.FaceToFace;
                case E_aIntrvwrMethodT.ByMail: return E_InterviewerInformationApplicationTakenMethodType.Mail;
                case E_aIntrvwrMethodT.ByTelephone: return E_InterviewerInformationApplicationTakenMethodType.Telephone;
                case E_aIntrvwrMethodT.Internet: return E_InterviewerInformationApplicationTakenMethodType.Internet;
                default:
                    return E_InterviewerInformationApplicationTakenMethodType.Undefined;
            }
        }

        /// <summary>
        /// Converts the given mortgage insurance type to MISMO 2.6 MI Duration Type.
        /// </summary>
        /// <param name="mortgageInsuranceType">The mortgage insurance option (BPMI monthly, BPMI single, etc).</param>
        /// <returns>The MI Duration Type converted from the given MI option.</returns>
        private E_MiDataMiDurationType ToMismo(E_sProdConvMIOptionT mortgageInsuranceType)
        {
            switch (mortgageInsuranceType)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    return E_MiDataMiDurationType.PeriodicMonthly;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                    return E_MiDataMiDurationType.SingleLifeOfLoan;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return E_MiDataMiDurationType.PeriodicMonthly;
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return E_MiDataMiDurationType.SingleLifeOfLoan;
                case E_sProdConvMIOptionT.NoMI:
                case E_sProdConvMIOptionT.Blank:
                    return E_MiDataMiDurationType.NotApplicable;
                default:
                    throw new UnhandledEnumException(mortgageInsuranceType);
            }
        }

        /// <summary>
        /// Determines the MISMO 2.6 Respa Fee Paid To Type from the HUD line number, fee classification, and other properties associated with the HUD line in the GFE/QM areas.
        /// </summary>
        /// <param name="props">The fee properties.</param>
        /// <param name="hudLineNumber">The HUD line number associated with the fee.</param>
        /// <param name="classification">The fee classification per RESPA.</param>
        /// <param name="paidToDescription">The paid to description.</param>
        /// <returns>The RESPA fee paid to type.</returns>
        private E_RespaFeePaidToType ToMismo(int props, string hudLineNumber, E_RespaFeeRespaSectionClassificationType classification, string paidToDescription)
        {
            E_RespaFeePaidToType paidToType = E_RespaFeePaidToType.Lender;

            if (LosConvert.GfeItemProps_ThisPartyIsAffiliate(props)
                || LosConvert.GfeItemProps_PaidToThirdParty(props)
                || paidToDescription.Length > 0
                || classification == E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges
                || hudLineNumber == HUDLines.APPRAISAL_FEE
                || hudLineNumber == HUDLines.CREDIT_REPORT_FEE)
            {
                paidToType = E_RespaFeePaidToType.Other;
            }
            else if (LosConvert.GfeItemProps_ToBr(props))
            {
                paidToType = E_RespaFeePaidToType.Broker;
            }

            if (hudLineNumber == HUDLines.MORTGAGE_BROKER_FEE)
            {
                // 11/17/2010 dd - Always set PaidTo Type to Broker for this hud item
                paidToType = E_RespaFeePaidToType.Broker;
            }

            return paidToType;
        }

        /// <summary>
        /// Determines the MISMO 2.6 Respa Fee Paid To Type Other Description from the Paid To Description and the affiliate indicator.
        /// </summary>
        /// <param name="lenderAffiliate">True if the fee is paid to an affiliate (QM). False otherwise.</param>
        /// <param name="paidToDescription">The value of the Paid To Description field (GFE).</param>
        /// <returns>A description of the party to which the fee is paid.</returns>
        private string ToMismoPaidToTypeOtherDescription(bool lenderAffiliate, string paidToDescription)
        {
            if (!string.IsNullOrEmpty(paidToDescription))
            {
                return paidToDescription;
            }

            return lenderAffiliate ? "AffiliateOfLender" : string.Empty;
        }
    }

    public static class HUDLines
    {
        public const string LOAN_ORIGINATION_FEE = "801";
        public const string APPRAISAL_FEE = "804";
        public const string CREDIT_REPORT_FEE = "805";
        public const string TAX_SERVICE_FEE = "806";
        public const string MORTGAGE_BROKER_FEE = "808";
        public const string LENDERS_INSPECTION_FEE = "809";
        public const string HAZ_INS_RESERVE = "1002";
        public const string MTG_INS_RESERVE = "1003";
        public const string REAL_ESTATE_TAX_RESERVE = "1004";
        public const string SCHOOL_TAXES = "1005";
        public const string FLOOD_INS_RESERVE = "1006";
        public const string CUSTOM_1008 = "1008";
        public const string CUSTOM_1009 = "1009";
        public const string CUSTOM_1010 = "1010";
        public const string CUSTOM_1011 = "1011";
        public const string CLOSING_ESCROW_FEE = "1102";
        public const string DOC_PREP_FEE = "1109";
        public const string NOTARY_FEES = "1110";
        public const string ATTORNEY_FEES = "1111";
        public const string CITY_COUNTY_TAX_STAMPS = "1204";
        public const string STATE_TAX_STAMPS = "1205";
    }
}