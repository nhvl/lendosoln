namespace LendersOffice.Conversions
{
    using System.IO;
    using System.Text;
    using System.Xml.Xsl;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a set of Html views on the XML from Freddie Mac's UCD solution.
    /// </summary>
    public class FreddieMacXmlTransform
    {
        /// <summary>
        /// The XSLT to generate the Loan Product Advisor Credit Infile.
        /// </summary>
        public static readonly FreddieMacXmlTransform LoanProductAdvisorCreditInfileXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.LoanProspector.CreditInfile_v4.1.xsl");

        /// <summary>
        /// The XSLT to generate the Loan Product Advisor Documentation Checklist.
        /// </summary>
        public static readonly FreddieMacXmlTransform LoanProductAdvisorDocumentationChecklistXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.LoanProspector.DocCheckList_v4.2.xsl");

        /// <summary>
        /// The XSLT to generate the Loan Product Advisor Errors.
        /// </summary>
        public static readonly FreddieMacXmlTransform LoanProductAdvisorErrorsXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.LoanProspector.Errors.xsl");

        /// <summary>
        /// The XSLT to generate the Loan Product Advisor Full Feedback Certificate.
        /// </summary>
        public static readonly FreddieMacXmlTransform LoanProductAdvisorFullFeedbackCertificateXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.LoanProspector.FullFeedback.xsl");

        /// <summary>
        /// The XSLT to generate the Loan Product Advisor Merged Credit Report.
        /// </summary>
        public static readonly FreddieMacXmlTransform LoanProductAdvisorMergedCreditReportXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.LoanProspector.MergedCredit_v4.1.xsl");

        /// <summary>
        /// The source XML from Freddie Mac, which will be transformed.
        /// </summary>
        private readonly string resourceName;

        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacXmlTransform"/> class.
        /// </summary>
        /// <param name="resourceName">The embedded resource name of the XSLT used to create HTML.</param>
        private FreddieMacXmlTransform(string resourceName)
        {
            this.resourceName = resourceName;
        }

        /// <summary>
        /// Converts XML to HTML via the specified transform resource.
        /// </summary>
        /// <param name="xml">The Xml string.</param>
        /// <returns>The resulting HTML string.</returns>
        public string TransformXmlToHtml(string xml)
        {
            Encoding utf8Encoding = new UTF8Encoding(false);
            byte[] xmlBytes = utf8Encoding.GetBytes(xml);

            using (var inputStream = new MemoryStream(xmlBytes))
            using (var outputStream = new MemoryStream(10000))
            {
                XsltArgumentList xslArgs = new XsltArgumentList();
                xslArgs.AddParam("VirtualRoot", string.Empty, DataAccess.Tools.VRoot);

                XslTransformHelper.TransformFromEmbeddedResource(this.resourceName, inputStream, outputStream, xslArgs);
                outputStream.Flush();

                return utf8Encoding.GetString(outputStream.GetBuffer(), 0, (int)outputStream.Length);
            }
        }

        /// <summary>
        /// Represents the stylesheet resources for Freddie Mac's Loan Closing Advisor.
        /// </summary>
        public static class LoanClosingAdvisor
        {
            /// <summary>
            /// Represents the stylesheet resources for version 1.0 of Freddie Mac's Loan Closing Advisor.
            /// </summary>
            public static class Version1Dot0
            {
                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Loan Evaluation Summary.
                /// </summary>
                public static readonly FreddieMacXmlTransform LoanEvaluationSummaryXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.09a_Appendix_A9_LCLALoanEvaluationSummary_v1.0.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Feedback Certificate Summary.
                /// </summary>
                public static readonly FreddieMacXmlTransform FeedbackCertificateSummaryXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.09b_Appendix_A9_LCLAFeedbackCertificate_v1.0.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Feedback Certificate Details.
                /// </summary>
                public static readonly FreddieMacXmlTransform FeedbackCertificateDetailsXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.09c_Appendix_A9_LCLADetailCertificate_v1.0.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor System Error.
                /// </summary>
                public static readonly FreddieMacXmlTransform SystemErrorXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.09d_Appendix_A9_LCLASystemError_v1.0.xsl");
            }

            /// <summary>
            /// Represents the stylesheet resources for version 2.1 of Freddie Mac's Loan Closing Advisor.
            /// </summary>
            public static class Version2Dot1
            {
                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Loan Evaluation Summary.
                /// </summary>
                public static readonly FreddieMacXmlTransform LoanEvaluationSummaryXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.v2_1.09a_Appendix_A9_LCLALoanEvaluationSummary.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Feedback Certificate Summary.
                /// </summary>
                public static readonly FreddieMacXmlTransform FeedbackCertificateSummaryXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.v2_1.09b_Appendix_A9_LCLAFeedbackCertificate.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor Feedback Certificate Details.
                /// </summary>
                public static readonly FreddieMacXmlTransform FeedbackCertificateDetailsXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.v2_1.09c_Appendix_A9_LCLADetailCertificate.xsl");

                /// <summary>
                /// The XSLT to generate the Loan Closing Advisor System Error.
                /// </summary>
                public static readonly FreddieMacXmlTransform SystemErrorXslt = new FreddieMacXmlTransform("LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac.v2_1.09d_Appendix_A9_LCLASystemError.xsl");
            }
        }
    }
}
