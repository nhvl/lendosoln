﻿namespace LendersOffice.ObjLib.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Email;

    /// <summary>
    /// Defines the export previously contained in the <c>SendDataToMCT</c> executable.
    /// </summary>
    /// <remarks>
    /// This code is currently about as close to a copy-paste as I can make it, and so is not
    /// representative of the style which I would normally seek.
    /// </remarks>
    public class MctXmlExport
    {
        /// <summary>
        /// The pricing mode for originator.
        /// </summary>
        private const string PricingModeOriginator = "originator";

        /// <summary>
        /// The pricing mode for investor.
        /// </summary>
        private const string PricingModeInvestor = "investor";

        /// <summary>
        /// A mapping of fields selected from the database to field names for MCT.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, string> FieldNameMapping = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            { "LockDate", "lockdate" },
            { "LoanNumber", "loannumber" },
            { "aBNm", "borrowername" },
            { "LoanProgram", "loanprogram" },
            { "NoteRate", "rate" },
            { "TotalLoanAmt", "loanamt" },
            { "State", "state" },
            { "LockExpiration", "lockexpire" },
            { "LockStatus", "stage" },
            { "Impound", "escrows" },
            { "LTV", "ltv" },
            { "CLTV", "cltv" },
            { "QualifyingScore", "fico" },
            { "LoanPurpose", "loanpurpose" },
            { "NumberOfUnits", "units" },
            { "OccupancyType", "occupancy" },
            { "DTI", "dti" },
            { "PropertyType", "proptype" },
            { "BEPrice", "beprice" },
            { "InvestorName", "investor" },
            { "PurchaseDate", "purchaseddate" },
            { "sInvestorLockConfNum", "commitnumber" },
            { "sEmployeeLoanRepName", "lonname" },
            { "sBranchNm", "branch" },
            { "sFundD", "fundeddate" },
            { "aBFirstNm", "borrowerfirstname" },
            { "aBLastNm", "borrowerlastname" },
            { "sBrokerLockBaseBrokComp1PcPrice", "originatorbaseprice" },
            { "sBrokerLockTotHiddenAdjBrokComp1PcPrice", "originatortotalhiddenadjustments" },
            { "sBrokerLockBrokerBaseBrokComp1PcPrice", "originatorbaseprice" },
            { "sBrokerLockTotVisibleAdjBrokComp1PcPrice", "originatortotalvisibleadjustments" },
            { "sInvestorLockTotAdjBrokComp1PcPrice", "investortotaladjustments" },
            { "pmlBrokerName", "TPOOriginatingCompany" },
            { "FEPointsBeforeLenderAdj", "FEPointsBeforeLenderAdj" },
            { "LPOCAdjustment", "LPOCAdjustment" }
        };

        /// <summary>
        /// Generates the XML of the specified lender's loans.
        /// </summary>
        /// <param name="brokerId">The broker id of the lender.</param>
        /// <param name="customerCode">The customer code of the lender.</param>
        /// <returns>A document of the lender's loans to export.</returns>
        public static XDocument GenerateXml(Guid brokerId, string customerCode)
        {
            var parameters = new[]
            {
                new System.Data.SqlClient.SqlParameter("CustomerCode", customerCode)
            };

            try
            {
                using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveMCTDataXml", parameters))
                {
                    return Generate(reader, customerCode);
                }
            }
            catch (Exception exc)
            {
                EmailUtilities.SendEmail(new CBaseEmail(brokerId)
                {
                    From = "support@pricemyloan.com",
                    To = "davidd@lendingqb.com",
                    Subject = "SendDataToMCT Failed For " + customerCode,
                    Message = exc.ToString()
                });

                throw;
            }
        }

        /// <summary>
        /// Generates the XML of the loans contained in the data reader.
        /// </summary>
        /// <param name="reader">The data reader to pull data from.</param>
        /// <param name="lender">The lender of the loans.</param>
        /// <returns>A document of the loans.</returns>
        private static XDocument Generate(IDataReader reader, string lender)
        {
            XDocument document = new XDocument();
            XElement clientElement = new XElement("client");
            document.Add(clientElement);

            WriteString(clientElement, "client_id", lender);
            WriteString(clientElement, "data_source", "LendingQB");

            XElement loandataElement = new XElement("loandata");
            clientElement.Add(loandataElement);

            while (reader.Read())
            {
                XElement loanElement = new XElement("loan");
                loandataElement.Add(loanElement);

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string lqbFieldName = reader.GetName(i);
                    if (lqbFieldName.Equals("sBrokerLockAdjustXmlContent"))
                    {
                        WritePricingAdjustments(loanElement, PricingModeOriginator, (string)reader[i]);
                    }
                    else if (lqbFieldName.Equals("sInvestorLockAdjustXmlContent"))
                    {
                        WritePricingAdjustments(loanElement, PricingModeInvestor, (string)reader[i]);
                    }
                    else
                    {
                        WriteString(loanElement, GetFieldName(lqbFieldName), reader[i]);
                    }
                }
            }

            return document;
        }

        /// <summary>
        /// Determines the field name MCT is expecting from the selected name.
        /// </summary>
        /// <param name="lqbFieldName">The selected name of the field from the database.</param>
        /// <returns>The field name MCT is expecting.</returns>
        private static string GetFieldName(string lqbFieldName)
        {
            string mctFieldName;
            if (!FieldNameMapping.TryGetValue(lqbFieldName, out mctFieldName))
            {
                mctFieldName = "LQB_" + lqbFieldName;
            }

            return mctFieldName;
        }

        /// <summary>
        /// Writes the specified pricing adjustments to the parent.
        /// </summary>
        /// <param name="parent">The parent element, which will contain the pricing adjustments.</param>
        /// <param name="pricingMode">The pricing mode of <paramref name="xmlContent"/>.</param>
        /// <param name="xmlContent">The XML of the pricing adjustments.</param>
        private static void WritePricingAdjustments(XElement parent, string pricingMode, string xmlContent)
        {
            int i = 1;
            if (!string.IsNullOrEmpty(xmlContent))
            {
                var adjustments = XElement.Parse(xmlContent).Elements("PricingAdjustment");
                foreach (XElement adjustment in adjustments)
                {
                    if (i > 10)
                    {
                        break;
                    }

                    WriteString(parent, pricingMode + "priceadj" + "pct" + i.ToString("00"), adjustment.Element("Price").Value.Replace("%", string.Empty));
                    WriteString(parent, pricingMode + "priceadj" + "desc" + i.ToString("00"), adjustment.Element("Description").Value);
                    if (pricingMode == PricingModeOriginator)
                    {
                        WriteString(parent, pricingMode + "priceadj" + "isfromlender" + i.ToString("00"), adjustment.Element("IsLenderAdjustment").Value);
                    }

                    i++;
                }
            }

            for (; i <= 10; i++)
            {
                WriteString(parent, pricingMode + "priceadj" + "pct" + i.ToString("00"), null);
                WriteString(parent, pricingMode + "priceadj" + "desc" + i.ToString("00"), null);
                if (pricingMode == PricingModeOriginator)
                {
                    WriteString(parent, pricingMode + "priceadj" + "isfromlender" + i.ToString("00"), false);
                }
            }
        }

        /// <summary>
        /// Writes the specified value as a new element under the parent.
        /// </summary>
        /// <param name="parent">The parent node to contain the field element.</param>
        /// <param name="fieldName">The element's name.</param>
        /// <param name="value">The value of the field.</param>
        private static void WriteString(XElement parent, string fieldName, object value)
        {
            XElement element = new XElement(fieldName);
            parent.Add(element);

            if (value == null || value == DBNull.Value)
            {
                return;
            }
            else if (value.GetType() == typeof(DateTime))
            {
                element.Value = ((DateTime)value).ToString("yyyy-MM-dd");
            }
            else
            {
                string s = value.ToString();
                if (string.IsNullOrEmpty(s) == false)
                {
                    element.Value = s;
                }
            }
        }
    }
}
