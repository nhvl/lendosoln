﻿// <copyright file="ClosingCostFeePaymentWrapper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Isaac Ribakoff
//    Date:   8/13/2015
// </summary>
namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a means to push LOXml data into a LoanClosingCostFeePayment object.
    /// </summary>
    internal class ClosingCostFeePaymentWrapper
    {
        /// <summary>
        /// Action in LOXml to add Fee.
        /// </summary>
        internal static readonly string FEEPAYMENTACTIONADD = "0";

        /// <summary>
        /// Action in LOXml to remove Fee.
        /// </summary>
        internal static readonly string FEEPAYMENTACTIONDELETE = "1";

        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingCostFeePaymentWrapper" /> class.
        /// </summary>
        /// <param name="paymentProperties">A collection of LoanClosingCostFeePayment properties to be modified.</param>
        internal ClosingCostFeePaymentWrapper(NameValueCollection paymentProperties)
        {
            this.PaymentProperties = paymentProperties;
            this.ClosingCostFeePaymentMarkedForDeletion = this.PaymentProperties["action"] == FEEPAYMENTACTIONDELETE;
        }

        /// <summary>
        /// Gets the LoanClosingCostFeePayment object to be modified.
        /// </summary>
        internal LoanClosingCostFeePayment ClosingCostFeePayment { get; private set; }

        /// <summary>
        /// Gets a value indicating whether associated ClosingCostFeePayment should be removed from parent LoanClosingCostFee.
        /// </summary>
        internal bool ClosingCostFeePaymentMarkedForDeletion { get; private set; }

        /// <summary>
        /// Gets a value indicating whether associated ClosingCostFeePayment is new.
        /// </summary>
        internal bool ClosingCostFeePaymentIsNew { get; private set; }

        /// <summary>
        /// Gets a collection of LoanClosingCostFeePayment properties to be modified.
        /// </summary>
        internal NameValueCollection PaymentProperties { get; private set; }

        /// <summary>
        /// Attempts to wrap a closing cost fee payment wrapper object to a new or existing closing cost fee payment.
        /// </summary>
        /// <param name="closingCostFeeWrapper">Closing cost fee wrapper to which the closing cost fee payment belongs.</param>
        /// <param name="collectionId">The name of the closing cost set being modified.</param>
        /// <param name="warningMessage">StringBuilder to which failure message is appended if closing cost fee payment cannot be matched.</param>
        /// <returns>Returns a boolean indicating whether wrapper object could be matched with a new or existing closing cost fee payment.</returns>
        internal bool TryMatchClosingCostFeePayment(ClosingCostFeeWrapper closingCostFeeWrapper, string collectionId, StringBuilder warningMessage)
        {
            Guid feePaymentId;
            string feePaymentIdUnparsed = this.PaymentProperties["id"];

            if (string.IsNullOrEmpty(feePaymentIdUnparsed))
            {
                if (this.ClosingCostFeePaymentMarkedForDeletion)
                {
                    warningMessage.Append("WARNING: A closing cost fee payment is marked for deletion. However, no Id was supplied. " + collectionId + " will not be modified.").Append(Environment.NewLine);
                    return false;
                }
                else
                {
                    if (closingCostFeeWrapper.IsBorrowerClosingCostFee)
                    {
                        this.ClosingCostFeePayment = new BorrowerClosingCostFeePayment();
                    }
                    else
                    {
                        this.ClosingCostFeePayment = new SellerClosingCostFeePayment();
                    }

                    this.ClosingCostFeePaymentIsNew = true;
                    this.ClosingCostFeePayment.SetParent(closingCostFeeWrapper.ClosingCostFee);
                    return true;
                }
            }
            else if (!Guid.TryParse(feePaymentIdUnparsed, out feePaymentId))
            {
                warningMessage.Append("WARNING: A closing cost fee payment has an invalid id: " + feePaymentId + ". " + collectionId + " will not be modified.").Append(Environment.NewLine);
                return false;
            }
            else
            {
                this.ClosingCostFeePayment = closingCostFeeWrapper.ClosingCostFee.ChildPaymentList.FirstOrDefault(p => p.Id == feePaymentId);
                if (this.ClosingCostFeePayment == null)
                {
                    warningMessage.Append("WARNING: A closing cost fee payment has an non-matching id: " + feePaymentIdUnparsed + ". " + collectionId + " will not be modified.").Append(Environment.NewLine);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Pushes updates to associated LoanClosingCostFeePayment object.
        /// </summary>
        internal void UpdateClosingCostFeePayment()
        {
            SetClosingCostFeePaymentProperties(this.ClosingCostFeePayment, this.PaymentProperties);
        }

        /// <summary>
        /// Applies updates to LoanClosingCostFeePayment object.
        /// </summary>
        /// <param name="closingCostFeePayment">The LoanClosingCostFeePayment object to be updated.</param>
        /// <param name="paymentProperties">Fee payment properties to be applied.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "This is not an enumerated switch. Invalid keys should simply be skipped")]
        private static void SetClosingCostFeePaymentProperties(LoanClosingCostFeePayment closingCostFeePayment, NameValueCollection paymentProperties)
        {
            bool b;
            foreach (string key in paymentProperties)
            {
                string value = paymentProperties[key];
                switch (key)
                {
                    case "amount":
                        closingCostFeePayment.Amount_rep = value;
                        break;
                    case "entity":
                        try
                        {
                            E_AgentRoleT agentRoleT = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), value);
                            closingCostFeePayment.Entity = agentRoleT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "gfeclosingcostfeepaymenttimingt":
                        try
                        {
                            E_GfeClosingCostFeePaymentTimingT gfeClosingCostFeePaymentTimingT = (E_GfeClosingCostFeePaymentTimingT)Enum.Parse(typeof(E_GfeClosingCostFeePaymentTimingT), value);
                            closingCostFeePayment.GfeClosingCostFeePaymentTimingT = gfeClosingCostFeePaymentTimingT;
                        }
                        catch (ArgumentException)
                        {
                        }

                        break;
                    case "ismade":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFeePayment.IsMade = b;
                        }

                        break;
                    case "paidbyt":
                        try
                        {
                            E_ClosingCostFeePaymentPaidByT paidByT = (E_ClosingCostFeePaymentPaidByT)Enum.Parse(typeof(E_ClosingCostFeePaymentPaidByT), value);
                            closingCostFeePayment.PaidByT = paidByT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "paymentdate":
                        closingCostFeePayment.PaymentDate_rep = value;
                        break;
                    case "responsiblepartyt":
                        try
                        {
                            E_GfeResponsiblePartyT responsiblePartyT = (E_GfeResponsiblePartyT)Enum.Parse(typeof(E_GfeResponsiblePartyT), value);
                            closingCostFeePayment.ResponsiblePartyT = responsiblePartyT;
                        }
                        catch (ArgumentException) { }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
