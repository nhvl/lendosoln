﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Importer
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Security;

    public class BasicLoanFileCreator : ILoanFileCreator
    {
        private const string FIND_LOAN_TEMPLATE = "FindLoanTemplate";


        public Guid CreateLoan(BrokerUserPrincipal principal, LendersOffice.Audit.E_LoanCreationSource creationSource, IDictionary<string, object> defaultValues, string templateName)
        {

            Guid templateId = Guid.Empty;
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, creationSource);

            if (!string.IsNullOrEmpty(templateName))
            {
                // If the template lookup succeeds, templateId will be populated.
                // If the template lookup fails, templateId will be Guid.Empty.
                // Thus we don't care out the return value for this call.
                GetLoanTemplateId(principal.BrokerId, templateName, out templateId);
            }

            return creator.CreateNewLoanFileForLoansPQ(templateId: templateId, defaultValues: defaultValues);
        }

        private bool GetLoanTemplateId(Guid brokerId, string templateName, out Guid sLId)
        {
            sLId = Guid.Empty;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId), 
                                            new SqlParameter("@sLNm", templateName)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, FIND_LOAN_TEMPLATE, parameters))
            {
                if (reader.Read())
                {
                    sLId = (Guid)reader["sLId"];
                }
            }
            Tools.LogInfo("[LPQ] Found template? " + (sLId != Guid.Empty));
            return sLId != Guid.Empty; 
        }

    }
}
