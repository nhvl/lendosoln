﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Importer
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Security;

    public interface ILoanFileCreator
    {
        /// <summary>
        /// Creates a loan file.
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="creationSource">What do we log to audit? </param>
        /// <param name="defaultValues">default values to set on the new loan </param>
        /// <param name="templateName">Null or empty if you dont want to use a template</param>
        /// <returns></returns>
        Guid CreateLoan(BrokerUserPrincipal principal, LendersOffice.Audit.E_LoanCreationSource creationSource, IDictionary<string, object> defaultValues, string templateName);
    }
}
