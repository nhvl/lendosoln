﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Importer
{
    using System.Web;

    /// <summary>
    /// This is the request handler that LPQ will hit in order to get the version our importer accepts.
    /// </summary>
    public class LPQImporterVersionHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether this is reusable.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="context">The http context.</param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Output.Write(APP.GetMaxSupportedVersion());
        }
    }
}
