﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Importer
{
    using System;

    public interface ILoanFileImporter
    {
        void Import();
    }
}
