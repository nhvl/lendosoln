﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Importer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.CreditReport.Mcl;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    public class CLFImporter : ILoanFileImporter
    {
        private CPageData _pageData;
        private Guid _loanID;
        private MORTGAGE_LOAN12 _ml;
        private bool hasSubjectPropertyNetCashFlow;
        private decimal totalSubjectPropertyNetCashFlow;

        private IDictionary<Guid, ICreditReportResponse> _reportsToImport = new Dictionary<Guid, ICreditReportResponse>();
        private static Guid DUMMYLPQCRAID = new Guid("c3ad86c8-774f-449f-b178-26294c93cb7f");

        public CLFImporter(Guid loanId, string data)
        {
            this._loanID = loanId;
            this._ml = DeserializeMortgageLoan(data);

            this.InitializeApplications(this._loanID, this._ml.APPLICANTS.APPLICANT.Length);

            this._pageData = CPageData.CreateUsingSmartDependency(loanId, typeof(CLFImporter));
            this._pageData.ByPassFieldSecurityCheck = true;
            this._pageData.SetFormatTarget(FormatTarget.MismoClosing);
            this._pageData.BypassClosingCostSetValidation = true;
            this._pageData.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        public CLFImporter(CPageData loanData, string data)
        {
            loanData.BypassClosingCostSetValidation = true;
            this._ml = DeserializeMortgageLoan(data);
            this._loanID = loanData.sLId;
            this._pageData = loanData;
        }

        public void Import()
        {
            this.PopulateLoan();
            _pageData.Save();
            ImportCreditReports();
        }

        public void PopulateLoan()
        {
            this.hasSubjectPropertyNetCashFlow = false;
            this.totalSubjectPropertyNetCashFlow = 0;

            ImportApplicants(this._ml.APPLICANTS?.APPLICANT);
            ImportLoanInfo(this._ml.LOAN_INFO);
            ImportPropertyInfo(this._ml.PROPERTY_INFO);
            ImportTransactionDetails(this._ml.DETAIL_TRANSACTION);
            ImportContacts(this._ml.CONTACTS);
            ImportLoanStatus(this._ml.LOAN_STATUS);
            ImportSystems(this._ml.SYSTEM);
            ImportFunding(this._ml.FUNDING);
            ImportCustomQuestions(this._ml.CUSTOM_QUESTIONS);
            ImportHmdaXml(this._ml.HMDA_XML, this._ml.APPLICANTS?.APPLICANT);
            this.ImportPropertyType(this._ml.PROPERTY_INFO, this._ml.LOAN_INFO?.LOAN_PRODUCT_DATA);
            this.ImportNoteRate(this._ml.FUNDING, this._ml.LOAN_INFO);
        }

        private void ImportNoteRate(MORTGAGE_LOANFUNDING funding, MORTGAGE_LOAN_INFO loanInfo)
        {
            decimal initialRate;
            if (funding != null && funding.initial_rateSpecified && decimal.TryParse(funding.initial_rate, out initialRate))
            {
                if (initialRate > 0)
                {
                    this._pageData.sNoteIR = initialRate;
                }
                else
                {
                    this._pageData.sNoteIR_rep = loanInfo?.rate;
                }
            }
        }

        private void ImportPropertyType(MORTGAGE_PROPERTY_INFO propertyInfo, MORTGAGE_LOAN_INFOLOAN_PRODUCT_DATA loanProductData)
        {
            /* There are 2 property type fields in LPQ. MORTGAGE_PROPERTY_INFO.property_type and LOAN_PRODUCT_DATA.gse_property_type.
             * property_type appears in the Mortgage Info page in LPQ, and is available for HE and ML loans.
             * gse_property_type only seems to appear in the Loan Product page for ML loans. 
             * property_type's values align more closely with sProdSpT
             * gse_property_type aligns more closely with sGseSpT
             * Since property_type is what most LPQ users will see, we'll prioritize setting sProdSpT with property_type and let the
             * transform populate sGseSpT. 
             * If property_type isn't specified for some strange reason, then fallback to setting from sGseSpT.
             */

            if (propertyInfo != null && propertyInfo.property_typeSpecified && propertyInfo.property_type != MORTGAGE_PROPERTY_INFOProperty_type.Item)
            {
                this.SetUsingPropertyType(this._pageData, propertyInfo.property_type);
            }
            else if (loanProductData != null)
            {
                var oldCalcMode = _pageData.CalcModeT;
                _pageData.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                _pageData.sGseSpT = MapGsePropertyTypeToLO(loanProductData.gse_property_type);
                _pageData.CalcModeT = oldCalcMode;
            }

            _pageData.sUnitsNum_rep = propertyInfo?.units ?? "";
        }

        private void SetUsingPropertyType(CPageData dataLoan, MORTGAGE_PROPERTY_INFOProperty_type propertyType)
        {
            if (propertyType == MORTGAGE_PROPERTY_INFOProperty_type.FARM ||
                propertyType == MORTGAGE_PROPERTY_INFOProperty_type.HOMEBUSINESS ||
                propertyType == MORTGAGE_PROPERTY_INFOProperty_type.LAND ||
                propertyType == MORTGAGE_PROPERTY_INFOProperty_type.Item5UNIT ||
                propertyType == MORTGAGE_PROPERTY_INFOProperty_type.OTHER ||
                propertyType == MORTGAGE_PROPERTY_INFOProperty_type.Item)
            {
                return;
            }

            dataLoan.sProdIsCondotel = false;

            var oldCalcMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            switch (propertyType)
            {
                case MORTGAGE_PROPERTY_INFOProperty_type.COMMERCIAL:
                    dataLoan.sProdSpT = E_sProdSpT.Commercial;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.CONDOTEL:
                    dataLoan.sProdSpT = E_sProdSpT.Condo;
                    dataLoan.sProdIsCondotel = true;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.COOP:
                    dataLoan.sProdSpT = E_sProdSpT.CoOp;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.HIGHRISECONDO:
                case MORTGAGE_PROPERTY_INFOProperty_type.LOWRISECONDO:
                    dataLoan.sProdSpT = E_sProdSpT.Condo;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.Item2UNIT:
                    dataLoan.sProdSpT = E_sProdSpT.TwoUnits;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.Item3UNIT:
                    dataLoan.sProdSpT = E_sProdSpT.ThreeUnits;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.Item4UNIT:
                    dataLoan.sProdSpT = E_sProdSpT.FourUnits;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.MANUFACTUREDHOME:
                    dataLoan.sProdSpT = E_sProdSpT.Manufactured;
                    dataLoan.sHmdaManufacturedTypeT = sHmdaManufacturedTypeT.ManufacturedHomeAndLand;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.MIXEDUSE:
                    dataLoan.sProdSpT = E_sProdSpT.MixedUse;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.MNL:
                    dataLoan.sProdSpT = E_sProdSpT.Manufactured;
                    dataLoan.sHmdaManufacturedTypeT = sHmdaManufacturedTypeT.ManufacturedHomeAndNotLand;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.MOBILEHOME:
                    dataLoan.sProdSpT = E_sProdSpT.Manufactured;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.PUD:
                    dataLoan.sProdSpT = E_sProdSpT.PUD;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.SFR:
                    dataLoan.sProdSpT = E_sProdSpT.SFR;
                    break;
                case MORTGAGE_PROPERTY_INFOProperty_type.TOWNHOUSE:
                    dataLoan.sProdSpT = E_sProdSpT.Townhouse;
                    break;
                default:
                    throw new UnhandledEnumException(propertyType);
            }

            dataLoan.CalcModeT = oldCalcMode;
        }

        /// <summary>
        /// Imports the HMDA data. Best to do this after importing applicant data.
        /// </summary>
        /// <param name="hmdaXml"></param>
        private void ImportHmdaXml(BASE_HMDA_XML hmdaXml, MORTGAGE_APPLICANT[] applicants)
        {
            if (hmdaXml?.ITEM == null || applicants == null)
            {
                return;
            }

            Dictionary<int, HmdaDataHelper> appsToModify = new Dictionary<int, HmdaDataHelper>();

            /* LPQ did not add the HMDA data to the APPLICANT container. Instead, they prefix each field with applicant_<index>.
             * They also make no distinction for coborrowers. 
             * Consider the following:
             *  LPQ file has 2 applicants.
             *   - Applicant1 has a coapplicant, Coapplicant1
             *   - Applicant2 has no coapplicant.
             *  The following would be their prefixes:
             *   Applicant1 => applicant_0
             *   Coapplicant1 => applicant_1
             *   Applicant2 => applicant_2
             */
            var keyStringRegex = "applicant_(?<index>[0-9]+)_(?<key>.+)";
            Dictionary<int, Dictionary<string, string>> hmdaItemsByIndex = new Dictionary<int, Dictionary<string, string>>();
            foreach (var item in hmdaXml.ITEM)
            {
                var keyString = item.key;
                Match match = Regex.Match(keyString, keyStringRegex);
                if (!match.Success || !match.Groups["index"].Success || !match.Groups["key"].Success)
                {
                    continue;
                }

                int hmdaItemIndex;
                if (!int.TryParse(match.Groups["index"].Value, out hmdaItemIndex))
                {
                    continue;
                }

                Dictionary<string, string> hmdaItems;
                if (!hmdaItemsByIndex.TryGetValue(hmdaItemIndex, out hmdaItems))
                {
                    hmdaItems = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    hmdaItemsByIndex.Add(hmdaItemIndex, hmdaItems);
                }

                string key = match.Groups["key"].Value;
                hmdaItems[key] = item.value;
            }

            if (hmdaItemsByIndex.Count == 0)
            {
                return;
            }

            int hmdaIndexCounter = 0;
            for (var i = 0; i < applicants.Length; i++)
            {
                var applicant = applicants[i];

                bool hasSpouse = applicant.SPOUSE != null;
                var borrowerIndex = hmdaIndexCounter++;
                var borrowerHmdaItems = hmdaItemsByIndex.GetValueOrNull(borrowerIndex);

                Dictionary<string, string> coborrowerHmdaItems = null;
                if (hasSpouse)
                {
                    var coborrowerIndex = hmdaIndexCounter++;
                    coborrowerHmdaItems = hmdaItemsByIndex.GetValueOrNull(coborrowerIndex);
                }

                appsToModify.Add(i, new HmdaDataHelper() { BorrowerHmdaData = borrowerHmdaItems, CoborrowerHmdaData = coborrowerHmdaItems });
            }

            // We've sorted all the data into appropriate app buckets. We can now set them.
            foreach (var appToModify in appsToModify)
            {
                var app = _pageData.GetAppData(appToModify.Key);
                var values = appToModify.Value;

                var oldBorrowerMode = app.BorrowerModeT;

                app.BorrowerModeT = E_BorrowerModeT.Borrower;
                this.ImportHmdaInfo(app, values.BorrowerHmdaData);

                app.BorrowerModeT = E_BorrowerModeT.Coborrower;
                this.ImportHmdaInfo(app, values.CoborrowerHmdaData);

                app.BorrowerModeT = oldBorrowerMode;
            }
        }

        private void ImportHmdaInfo(CAppData appData, Dictionary<string, string> values)
        {
            if (values == null || values.Count == 0)
            {
                return;
            }

            string sexNotProvidedValue = values.GetValueOrNull("sex_not_provided") ?? string.Empty;
            string sexValue = values.GetValueOrNull("sex") ?? string.Empty;
            this.ImportHmdaGender(appData, sexNotProvidedValue, sexValue);

            string ethnicityNotProvidedValue = values.GetValueOrNull("ethnicity_not_provided") ?? string.Empty;
            string ethnicityIsHispanic = values.GetValueOrNull("ethnicity_is_hispanic") ?? string.Empty;
            string ethnicityIsNotHispanic = values.GetValueOrNull("ethnicity_is_not_hispanic") ?? string.Empty;
            string ethnicityHispanicValue = values.GetValueOrNull("ethnicity_hispanic") ?? string.Empty;
            string ethnicityHispanicOtherValue = values.GetValueOrNull("ethnicity_hispanic_other") ?? string.Empty;
            this.ImportHmdaEthnicity(appData, ethnicityNotProvidedValue, ethnicityIsNotHispanic, ethnicityIsHispanic, ethnicityHispanicValue, ethnicityHispanicOtherValue);

            string raceNotProvidedValue = values.GetValueOrNull("race_not_provided") ?? string.Empty;
            string raceBaseValue = values.GetValueOrNull("race_base") ?? string.Empty;
            string tribeNameValue = values.GetValueOrNull("tribe_name") ?? string.Empty;
            string raceAsianValue = values.GetValueOrNull("race_asian") ?? string.Empty;
            string raceAsianOtherValue = values.GetValueOrNull("race_asian_other") ?? string.Empty;
            string racePacificIslanderValue = values.GetValueOrNull("race_pacificislander") ?? string.Empty;
            string racePacificIslanderOtherValue = values.GetValueOrNull("race_pacificislander_other") ?? string.Empty;
            this.ImportHmdaRace(appData, raceNotProvidedValue, raceBaseValue, tribeNameValue, raceAsianValue, raceAsianOtherValue, racePacificIslanderValue, racePacificIslanderOtherValue);

            string isEthnicityObservedValue = values.GetValueOrNull("is_ethnicity_observed") ?? string.Empty;
            string isSexObservedValue = values.GetValueOrNull("is_sex_observed") ?? string.Empty;
            string isRaceObservedValue = values.GetValueOrNull("is_race_observed") ?? string.Empty;
            this.ImportHmdaCollectedByInfo(appData, isEthnicityObservedValue, isSexObservedValue, isRaceObservedValue);
        }

        private E_TriState MapYNToTristate(string yn)
        {
            if (string.IsNullOrEmpty(yn))
            {
                return E_TriState.Blank;
            }

            if (yn.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                return E_TriState.Yes;
            }
            else if (yn.Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                return E_TriState.No;
            }
            else
            {
                return E_TriState.Blank;
            }
        }

        private void ImportHmdaCollectedByInfo(CAppData appData, string isEthnicityObservedValue, string isSexObservedValue, string isRaceObservedValue)
        {
            appData.aEthnicityCollectedByObservationOrSurname = this.MapYNToTristate(isEthnicityObservedValue);
            appData.aSexCollectedByObservationOrSurname = this.MapYNToTristate(isSexObservedValue);
            appData.aRaceCollectedByObservationOrSurname = this.MapYNToTristate(isRaceObservedValue);
        }

        private void ImportHmdaRace(CAppData appData, string raceNotProvidedValue, string raceBaseValue, string tribeNameValue, string raceAsianValue, string raceAsianOtherValue, string racePacificIslanderValue, string racePacificIslanderOtherValue)
        {
            this.ImportHmdaAggregateRace(appData, raceNotProvidedValue, raceBaseValue, tribeNameValue);
            this.ImportHmdaAsianSubcategories(appData, raceAsianValue, raceAsianOtherValue);
            this.ImportHmdaPacificIslanderSubcategories(appData, racePacificIslanderValue, racePacificIslanderOtherValue);
        }

        private void ImportHmdaPacificIslanderSubcategories(CAppData appdata, string racePacificIslanderValue, string racePacficIslanderOtherValue)
        {
            appdata.aIsNativeHawaiian = false;
            appdata.aIsGuamanianOrChamorro = false;
            appdata.aIsSamoan = false;
            appdata.aIsOtherPacificIslander = false;

            string[] pacificIslanderSubcategories = racePacificIslanderValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var subcategory in pacificIslanderSubcategories)
            {
                if (subcategory.Equals("PH", StringComparison.OrdinalIgnoreCase))
                {
                    appdata.aIsNativeHawaiian = true;
                }
                else if (subcategory.Equals("PG", StringComparison.OrdinalIgnoreCase))
                {
                    appdata.aIsGuamanianOrChamorro = true;
                }
                else if (subcategory.Equals("PS", StringComparison.OrdinalIgnoreCase))
                {
                    appdata.aIsSamoan = true;
                }
                else if (subcategory.Equals("PO", StringComparison.OrdinalIgnoreCase))
                {
                    appdata.aIsOtherPacificIslander = true;
                }
            }

            appdata.aOtherPacificIslanderDescription = racePacficIslanderOtherValue;
        }

        private void ImportHmdaAsianSubcategories(CAppData appData, string raceAsianValue, string raceAsianOtherValue)
        {
            appData.aIsAsianIndian = false;
            appData.aIsChinese = false;
            appData.aIsFilipino = false;
            appData.aIsJapanese = false;
            appData.aIsKorean = false;
            appData.aIsVietnamese = false;
            appData.aIsOtherAsian = false;

            string[] asianSubcategories = raceAsianValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var subcategory in asianSubcategories)
            {
                if (subcategory.Equals("AI", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsAsianIndian = true;
                }
                else if (subcategory.Equals("AC", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsChinese = true;
                }
                else if (subcategory.Equals("AF", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsFilipino = true;
                }
                else if (subcategory.Equals("AJ", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsJapanese = true;
                }
                else if (subcategory.Equals("AK", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsKorean = true;
                }
                else if (subcategory.Equals("AV", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsVietnamese = true;
                }
                else if (subcategory.Equals("AO", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsOtherAsian = true;
                }
            }

            appData.aOtherAsianDescription = raceAsianOtherValue;
        }

        private void ImportHmdaAggregateRace(CAppData appData, string raceNotProvidedValue, string raceBaseValue, string tribeNameValue)
        {
            appData.aDoesNotWishToProvideRace = raceNotProvidedValue.Equals("Y", StringComparison.OrdinalIgnoreCase);

            appData.aIsAmericanIndian = false;
            appData.aIsAsian = false;
            appData.aIsBlack = false;
            appData.aIsPacificIslander = false;
            appData.aIsWhite = false;
            string[] aggregateRaces = raceBaseValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var aggregateRace in aggregateRaces)
            {
                if (aggregateRace.Equals("AMERICAN_INDIAN", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsAmericanIndian = true;
                }
                else if (aggregateRace.Equals("ASIAN", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsAsian = true;
                }
                else if (aggregateRace.Equals("PACIFIC_ISLANDER", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsPacificIslander = true;
                }
                else if (aggregateRace.Equals("BLACK", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsBlack = true;
                }
                else if (aggregateRace.Equals("WHITE", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsWhite = true;
                }
            }

            appData.aOtherAmericanIndianDescription = tribeNameValue;
        }

        private void ImportHmdaEthnicity(CAppData appData, string ethnicityNotProvidedValue, string ethnicityIsNotHispanicValue, string ethnicityIsHispanicValue, string ethnicityHispanicValue, string ethnicityHispanicOtherValue)
        {
            this.ImportHmdaIsHispanic(appData, ethnicityNotProvidedValue, ethnicityIsNotHispanicValue, ethnicityIsHispanicValue);
            this.ImportHmdaHispanicSubcategories(appData, ethnicityHispanicValue, ethnicityHispanicOtherValue);
        }

        private void ImportHmdaHispanicSubcategories(CAppData appData, string ethnicityHispanicValue, string ethnicityHispanicOtherValue)
        {
            // We need to clear the entries first since LPQ uses existance to dictate the selected subcategories.
            appData.aIsMexican = false;
            appData.aIsPuertoRican = false;
            appData.aIsCuban = false;
            appData.aIsOtherHispanicOrLatino = false;

            var subcategories = ethnicityHispanicValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var subcategory in subcategories)
            {
                if (subcategory.Equals("M", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsMexican = true;
                }
                else if (subcategory.Equals("P", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsPuertoRican = true;
                }
                else if (subcategory.Equals("C", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsCuban = true;
                }
                else if (subcategory.Equals("O", StringComparison.OrdinalIgnoreCase))
                {
                    appData.aIsOtherHispanicOrLatino = true;
                }
            }

            appData.aOtherHispanicOrLatinoDescription = ethnicityHispanicOtherValue;
        }

        private void ImportHmdaIsHispanic(CAppData appData, string ethnicityNotProvidedValue, string ethnicityIsNotHispanicValue, string ethnicityIsHispanicValue)
        {
            appData.aDoesNotWishToProvideEthnicity = ethnicityNotProvidedValue.Equals("Y", StringComparison.OrdinalIgnoreCase);

            if (ethnicityIsNotHispanicValue.Equals("Y", StringComparison.OrdinalIgnoreCase) && ethnicityIsHispanicValue.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                appData.aHispanicT = E_aHispanicT.BothHispanicAndNotHispanic;
            }
            else if (ethnicityIsNotHispanicValue.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                appData.aHispanicT = E_aHispanicT.NotHispanic;
            }
            else if (ethnicityIsHispanicValue.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                appData.aHispanicT = E_aHispanicT.Hispanic;
            }
            else
            {
                appData.aHispanicT = E_aHispanicT.LeaveBlank;
            }
        }

        private void ImportHmdaGender(CAppData appData, string sexNotProvidedValue, string sexValue)
        {
            E_GenderT gender;
            if (sexNotProvidedValue.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                if (sexValue.Equals("FEMALE", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.FemaleAndNotFurnished;
                }
                else if (sexValue.Equals("MALE", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.MaleAndNotFurnished;
                }
                else if (sexValue.Equals("OTHER", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.MaleFemaleNotFurnished;
                }
                else
                {
                    gender = E_GenderT.Unfurnished;
                }
            }
            else if (sexNotProvidedValue.Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                if (sexValue.Equals("FEMALE", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.Female;
                }
                else if (sexValue.Equals("MALE", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.Male;
                }
                else if (sexValue.Equals("OTHER", StringComparison.OrdinalIgnoreCase))
                {
                    gender = E_GenderT.MaleAndFemale;
                }
                else
                {
                    gender = E_GenderT.LeaveBlank;
                }
            }
            else
            {
                gender = E_GenderT.NA;
            }

            appData.aGender = gender;
        }

        private void ImportCustomQuestions(BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION[] customQuestions)
        {
            if (customQuestions == null)
            {
                return;
            }

            foreach (var question in customQuestions)
            {
                SetCustomField(question);
            }
        }

        private void SetCustomField(BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION question)
        {
            if (question == null ||
                (question.question_type != null && question.question_type.Equals("textbox", StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }

            string answer;

            if (question.CUSTOM_QUESTION_ANSWER != null && question.CUSTOM_QUESTION_ANSWER.Length > 0)
            {
                //just pull first one. 
                answer = question.CUSTOM_QUESTION_ANSWER[0].answer_text;
            }
            else
            {
                answer = question.CUSTOM_QUESTION_ANSWER[1].answer_value;
            }
            if (string.IsNullOrEmpty(answer))
            {
                answer = "";
            }
            switch (question.question_name)
            {
                case "sCustomField1":
                    _pageData.sCustomField1Notes = answer;
                    break;
                case "sCustomField2":
                    _pageData.sCustomField2Notes = answer;
                    break;
                case "sCustomField3":
                    _pageData.sCustomField3Notes = answer;
                    break;
                case "sCustomField4":
                    _pageData.sCustomField4Notes = answer;
                    break;
                case "sCustomField5":
                    _pageData.sCustomField5Notes = answer;
                    break;
                case "sCustomField6":
                    _pageData.sCustomField6Notes = answer;
                    break;
                case "sCustomField7":
                    _pageData.sCustomField7Notes = answer;
                    break;
                case "sCustomField8":
                    _pageData.sCustomField8Notes = answer;
                    break;
                case "sCustomField9":
                    _pageData.sCustomField9Notes = answer;
                    break;
                case "sCustomField10":
                    _pageData.sCustomField10Notes = answer;
                    break;
                case "sCustomField11":
                    _pageData.sCustomField11Notes = answer;
                    break;
                case "sCustomField12":
                    _pageData.sCustomField12Notes = answer;
                    break;
                case "sCustomField13":
                    _pageData.sCustomField13Notes = answer;
                    break;
                case "sCustomField14":
                    _pageData.sCustomField14Notes = answer;
                    break;
                case "sCustomField15":
                    _pageData.sCustomField15Notes = answer;
                    break;
                case "sCustomField16":
                    _pageData.sCustomField16Notes = answer;
                    break;
                case "sCustomField17":
                    _pageData.sCustomField17Notes = answer;
                    break;
                case "sCustomField18":
                    _pageData.sCustomField18Notes = answer;
                    break;
                case "sCustomField19":
                    _pageData.sCustomField19Notes = answer;
                    break;
                case "sCustomField20":
                    _pageData.sCustomField20Notes = answer;
                    break;
                default:
                    //we dont handle this custom question....
                    break;
            }
        }

        private void ImportFunding(MORTGAGE_LOANFUNDING funding)
        {
            if (funding == null)
            {
                return;
            }

            _pageData.sSchedDueD1_rep = funding.first_payment_date ?? "";
            // 9/26/2013 gf - opm 130112 1st pmt date is now a calculated
            // field. Lock to maintain imported date.
            _pageData.sSchedDueD1Lckd = true;
            _pageData.sEstCloseD_rep = funding.funding_date ?? "";
            _pageData.sEstCloseDLckd = true;

            _pageData.sCreditLineAmt_rep = funding.loc_amount ?? string.Empty;
            _pageData.sLAmtCalc_rep = funding.initial_amount_advanced ?? string.Empty;

            if (funding.mi_insurer_codeSpecified)
            {
                this.PopulateFromInsurerCode(funding.mi_insurer_code);
            }

        }

        private void PopulateFromInsurerCode(MORTGAGE_FUNDING_INFOMi_insurer_code insurerCode)
        {
            switch (insurerCode)
            {
                case MORTGAGE_FUNDING_INFOMi_insurer_code.GE:
                    // no mapping -- should i reset?
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.MGIC:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.MGIC;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.PMI:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.PMI;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.UG:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.UnitedGuaranty;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.RMIC:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.RMIC;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.RGI:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.Radian;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.TRIAD:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.Triad;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.CMG:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.CMG;
                    break;
                case MORTGAGE_FUNDING_INFOMi_insurer_code.Item:
                    _pageData.sMiCompanyNmT = E_sMiCompanyNmT.LeaveBlank;
                    break;
                default:
                    throw new UnhandledEnumException(insurerCode);
            }
        }

        private void ImportLoanStatus(MORTGAGE_STATUS mortgageStatus)
        {
            if (mortgageStatus == null)
            {
                return;
            }

            _pageData.sApprovD_rep = mortgageStatus.approval_date ?? "";
            _pageData.sRejectD_rep = mortgageStatus.declined_date ?? "";
            _pageData.sStatusT = MapToLO(mortgageStatus.loan_status);
            _pageData.sInvestorLockRLckdDays_rep = mortgageStatus.rate_lock_term ?? "";
            _pageData.sUnderwritingD_rep = mortgageStatus.underwrite_submit_date ?? string.Empty;
            _pageData.sGfeRateLockPeriod_rep = mortgageStatus.rate_lock_term ?? "";

            _pageData.sOpenedD_rep = mortgageStatus.app_receive_dateSpecified ? mortgageStatus.app_receive_date : _pageData.sOpenedD_rep;

            if (mortgageStatus.submit_dateSpecified)
            {
                _pageData.sAppSubmittedDLckd = true;
                _pageData.sAppSubmittedD_rep = mortgageStatus.submit_date ?? "";
            }

            if (!_pageData.sIsRateLocked)
            {
                _pageData.sRLckdD_rep = mortgageStatus.rate_lock_date ?? "";
            }
        }

        private void ImportCreditReports()
        {
            if (_reportsToImport != null && _reportsToImport.Count <= 0)
            {
                return;
            }

            foreach (KeyValuePair<Guid, ICreditReportResponse> entry in _reportsToImport)
            {
                CreditReportUtilities.SaveXmlCreditReport(entry.Value, BrokerUserPrincipal.CurrentPrincipal, _loanID, entry.Key, DUMMYLPQCRAID, Guid.Empty, LendersOffice.Audit.E_CreditReportAuditSourceT.ImportFileFromLoansPQ);
                CreditReportUtilities.ImportLiabilities(_loanID, entry.Key, true, false, false, true);
            }
        }

        private void ImportContacts(MORTGAGE_CONTACTSCONTACT_INFO[] contacts)
        {
            if (contacts == null)
            {
                return;
            }

            HashSet<E_AgentRoleT> updatedContacts = new HashSet<E_AgentRoleT>();

            CAgentFields agent;

            foreach (MORTGAGE_CONTACTSCONTACT_INFO contact in contacts)
            {
                if (contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.APPLICANT ||
                    contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.ASSETOWN ||
                    contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.VESTING ||
                    contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.NSS)
                {
                    continue;
                }

                E_AgentRoleT role = MapToLO(contact.contact_type);

                if (role == E_AgentRoleT.Other)
                {
                    continue;
                }

                if (updatedContacts.Contains(role))
                {
                    throw new CBaseException("Duplicate contacts are not allowed. Remove duplicates for contact type: " + contact.contact_type.ToString() + ".", "Duplicate Contacts Found");
                }
                else
                {
                    updatedContacts.Add(role);
                }

                agent = _pageData.GetAgentOfRole(role, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);
                agent.AgentName = contact.contact_first_name + " " + contact.contact_last_name ?? agent.AgentName;
                agent.EmailAddr = contact.contact_email ?? agent.EmailAddr;
                agent.CompanyName = contact.company_name ?? agent.CompanyName;
                agent.DepartmentName = contact.department_name ?? agent.DepartmentName;
                agent.StreetAddr = contact.company_address ?? agent.StreetAddr;
                agent.State = contact.company_state ?? agent.State;
                agent.Zip = contact.company_zip ?? agent.Zip;
                agent.City = contact.company_city ?? agent.City;
                agent.LicenseNumOfAgent = contact.agent_license_number ?? agent.LicenseNumOfAgent;
                agent.CaseNum = contact.case_number ?? agent.CaseNum;
                agent.Notes = contact.notes ?? agent.Notes;
                agent.LicenseNumOfCompany = contact.company_license_number ?? agent.LicenseNumOfCompany;
                agent.Phone = contact.contact_phone ?? agent.Phone;
                agent.FaxNum = contact.contact_fax ?? agent.FaxNum;
                agent.CellPhone = contact.contact_cell_phone ?? agent.CellPhone;
                agent.PagerNum = contact.contact_pager ?? agent.PagerNum;
                agent.LoanOriginatorIdentifier = contact.agent_nmls ?? agent.LoanOriginatorIdentifier;
                agent.CompanyLoanOriginatorIdentifier = contact.company_nmls ?? agent.CompanyLoanOriginatorIdentifier;
                if (contact.is_ssn_taxidSpecified && contact.is_ssn_taxid)
                {
                    agent.TaxId = contact.ssn ?? agent.TaxId;
                }

                agent.Update();
            }
        }

        private string NormalizePhoneNumber(string phoneNumber)
        {
            string normalizedNumber;
            if (!Tools.ToPhoneFormat(phoneNumber ?? string.Empty, out normalizedNumber))
            {
                return null;
            }

            return normalizedNumber;
        }

        private bool ContactSameAsParty(BASE_PARTY party, string nmlsrUserId, string nmlsrLenderId, CAgentFields contact, CAgentFields lenderContact)
        {
            List<Tuple<string, string>> equivalentFields = new List<Tuple<string, string>>()
            {
                new Tuple<string, string>(party.name, contact.AgentName),
                new Tuple<string, string>(this.NormalizePhoneNumber(party.phone), this.NormalizePhoneNumber(contact.Phone)),
                new Tuple<string, string>(party.email, contact.EmailAddr),
            };

            if (lenderContact != null && lenderContact.IsValid)
            {
                equivalentFields.Add(new Tuple<string, string>(lenderContact.CompanyName, contact.CompanyName));
                equivalentFields.Add(new Tuple<string, string>(this.NormalizePhoneNumber(lenderContact.PhoneOfCompany), this.NormalizePhoneNumber(contact.PhoneOfCompany)));
                equivalentFields.Add(new Tuple<string, string>(lenderContact.StreetAddr, contact.StreetAddr));
                equivalentFields.Add(new Tuple<string, string>(lenderContact.City, contact.City));
                equivalentFields.Add(new Tuple<string, string>(lenderContact.State, contact.State));
                equivalentFields.Add(new Tuple<string, string>(lenderContact.Zip, contact.Zip));
            }

            bool isEquivalent = true;
            foreach (var fieldPair in equivalentFields)
            {
                var incomingField = fieldPair.Item1 ?? string.Empty;
                var setField = fieldPair.Item2 ?? string.Empty;

                isEquivalent &= incomingField.Equals(setField, StringComparison.OrdinalIgnoreCase);
            }

            return isEquivalent;
        }

        private void ImportPartyAsContact(BASE_PARTY party, string nmlsrUserId, string nmlsrLenderId, E_AgentRoleT type, string description, CAgentFields lenderContact)
        {
            if (party == null)
            {
                return;
            }

            CAgentFields contact;
            if (type != E_AgentRoleT.Other)
            {
                contact = _pageData.GetAgentOfRole(type, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (contact != CAgentFields.Empty)
                {
                    if (!this.ContactSameAsParty(party, nmlsrUserId, nmlsrLenderId, contact, lenderContact))
                    {
                        contact = _pageData.GetAgentFields(-1);
                        contact.AgentRoleT = type;
                    }
                }
                else
                {
                    contact = _pageData.GetAgentOfRole(type, E_ReturnOptionIfNotExist.CreateNew);
                }
            }
            else
            {
                contact = _pageData.sAgentCollection.GetAllAgents().FirstOrDefault(item => item.OtherAgentRoleTDesc.Equals(description, StringComparison.OrdinalIgnoreCase) &&
                                                                                           this.ContactSameAsParty(party, nmlsrUserId, nmlsrLenderId, item, lenderContact));
                if (contact == null)
                {
                    contact = _pageData.GetAgentFields(-1);
                    contact.AgentRoleT = E_AgentRoleT.Other;
                    contact.OtherAgentRoleTDesc = description;
                }
            }

            contact.AgentName = party.name;
            contact.Phone = party.phone;
            contact.FaxNum = party.fax;
            contact.EmailAddr = party.email;
            contact.LoanOriginatorIdentifier = nmlsrUserId;
            contact.CompanyLoanOriginatorIdentifier = nmlsrLenderId;

            if (lenderContact != null && lenderContact.IsValid)
            {
                contact.CompanyName = lenderContact.CompanyName;
                contact.PhoneOfCompany = lenderContact.PhoneOfCompany;
                contact.StreetAddr = lenderContact.StreetAddr;
                contact.City = lenderContact.City;
                contact.State = lenderContact.State;
                contact.Zip = lenderContact.Zip;
            }

            contact.Update();
        }

        /// <summary>
        /// IMports the loan officer lender and processor if they exist from the system node. 
        /// It also takes the systems loan number for lender case number.
        /// </summary>
        /// <param name="systems"></param>
        private void ImportSystems(MORTGAGE_SYSTEM[] systems)
        {
            if (systems == null)
            {
                return;
            }

            foreach (var system in systems)
            {
                if (system.type != MORTGAGE_SYSTEMType.LPQ)
                {
                    continue;
                }

                // We need to do lender first since all other contacts will copy information from it.
                CAgentFields lender = null;
                if (system.LENDER != null)
                {
                    var companyName = system.LENDER.name;
                    string phoneOfCompany = system.LENDER.phone;
                    var faxNum = system.LENDER.fax;
                    var companyLoanOriginatorIdentifier = system.nmlsr_lender_id;
                    var emailAddr = system.LENDER.email;
                    var streetAddress = (system.LENDER.ADDRESS?.street_address_1 ?? string.Empty) + (system.LENDER.ADDRESS?.street_address_2 ?? string.Empty);
                    var city = system.LENDER.ADDRESS?.city ?? string.Empty;
                    var state = system.LENDER.ADDRESS?.state ?? string.Empty;
                    var zip = system.LENDER.ADDRESS?.zip ?? string.Empty;

                    var currentLender = _pageData.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (currentLender != CAgentFields.Empty)
                    {
                        List<Tuple<string, string>> equivalencyFields = new List<Tuple<string, string>>()
                        {
                            new Tuple<string, string>(companyName, currentLender.CompanyName),
                            new Tuple<string, string>(this.NormalizePhoneNumber(phoneOfCompany), this.NormalizePhoneNumber(currentLender.PhoneOfCompany)),
                            new Tuple<string, string>(this.NormalizePhoneNumber(faxNum), this.NormalizePhoneNumber(currentLender.FaxNum)),
                            new Tuple<string, string>(companyLoanOriginatorIdentifier, currentLender.CompanyLoanOriginatorIdentifier),
                            new Tuple<string, string>(emailAddr, currentLender.EmailAddr),
                            new Tuple<string, string>(streetAddress, currentLender.StreetAddr),
                            new Tuple<string, string>(city, currentLender.City),
                            new Tuple<string, string>(state, currentLender.State),
                            new Tuple<string, string>(zip, currentLender.Zip)
                        };

                        bool isEquivalent = true;
                        foreach (var fieldPair in equivalencyFields)
                        {
                            var incomingField = fieldPair.Item1 ?? string.Empty;
                            var setField = fieldPair.Item2 ?? string.Empty;

                            isEquivalent &= incomingField.Equals(setField, StringComparison.OrdinalIgnoreCase);
                        }

                        if (isEquivalent)
                        {
                            lender = currentLender;
                        }
                        else
                        {
                            lender = _pageData.GetAgentFields(-1);
                            lender.AgentRoleT = E_AgentRoleT.Lender;
                        }
                    }
                    else
                    {
                        lender = _pageData.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
                    }

                    lender.CompanyName = companyName;
                    lender.PhoneOfCompany = phoneOfCompany;
                    lender.FaxNum = faxNum;
                    lender.CompanyLoanOriginatorIdentifier = companyLoanOriginatorIdentifier;
                    lender.EmailAddr = emailAddr;

                    lender.StreetAddr = streetAddress;
                    lender.City = city;
                    lender.State = state;
                    lender.Zip = zip;

                    lender.Update();
                }

                var partiesToImport = new[]
                {
                    new { Party = system.LOAN_OFFICER, Type = E_AgentRoleT.LoanOfficer, Description = string.Empty },
                    new { Party = system.PROCESSOR, Type = E_AgentRoleT.Processor, Description = string.Empty },
                    new { Party = system.APPROVAL_OFFICER, Type = E_AgentRoleT.Other, Description = "Approval Officer" },
                    new { Party = system.COUNTER_OFFER_OFFICER, Type = E_AgentRoleT.Other, Description = "Counter Offer Officer" },
                    new { Party = system.FUNDER, Type = E_AgentRoleT.Funder, Description = string.Empty },
                    new { Party = system.DECLINING_OFFICER, Type = E_AgentRoleT.Other, Description = "Declining Officer" },
                    new { Party = system.DECIDING_OFFICER, Type = E_AgentRoleT.Other, Description = "Deciding Officer" },
                    new { Party = system.ORGANIZATION, Type = E_AgentRoleT.Other, Description = "Organization" },
                    new { Party = system.INSURANCE_SELLER, Type = E_AgentRoleT.Other, Description = "Insurance Seller" },
                    new { Party = system.CLINIC, Type = E_AgentRoleT.Other, Description = "Clinic" },
                    new { Party = system.CLINIC_WORKER, Type = E_AgentRoleT.Other, Description = "Clinic Worker" },
                    new { Party = system.BOOKING_OFFICER, Type = E_AgentRoleT.Other, Description = "Booking Officer" },
                    new { Party = (BASE_PARTY)system.ORIGINATOR, Type = E_AgentRoleT.LoanOfficer, Description = string.Empty },
                    new { Party = system.INSURANCE_LAST_PULLED_OFFICER, Type = E_AgentRoleT.Other, Description = "Insurance Last Pulled Officer" }
                };

                foreach (var party in partiesToImport)
                {
                    ImportPartyAsContact(party.Party, system.nmlsr_user_id, system.nmlsr_lender_id, party.Type, party.Description, lender);
                }

                // And Branch does its own thing.
                if (system.BRANCH != null)
                {
                    BranchDB bdb = new BranchDB();
                    if (false == string.IsNullOrEmpty(system.BRANCH.reference_id) && bdb.Retrieve(system.BRANCH.reference_id, _pageData.sBrokerId))
                    {
                        _pageData.AssignBranch(bdb.BranchID);
                    }
                    else
                    {
                        throw new CBaseException("Origination branch " + system.BRANCH.name + " does not exist in LendingQB ", "Missing Branch in LO " + _loanID);
                    }
                }

                _pageData.sLenderCaseNum = system.loan_number;
                break;
            }
        }

        /// <summary>
        /// Call after Loan Info  Imports the stuff in transaction details except for pmi info. 
        /// That stuff is imported in loan info due to some calculations.
        /// </summary>
        /// <param name="details"></param>
        private void ImportTransactionDetails(MORTGAGE_DETAILS_TRANSACTION details)
        {
            details = details ?? new MORTGAGE_DETAILS_TRANSACTION();

            _pageData.sPurchPrice_rep = details.purchase_price;
            _pageData.sAltCost_rep = details.alteration_cost;
            _pageData.sLandCost_rep = details.land_cost;
            //_pageData.sRefPdOffAmt1003_rep = details.refinance_debts_amount; 
            _pageData.sRefPdOffAmt1003Lckd = false; //opm 45123 av 1 25 2010  per Binh's request
            _pageData.sTotCcPbsLocked = details.closing_cost_paid_by_seller_amountSpecified;
            _pageData.sTotCcPbs_rep = details.closing_cost_paid_by_seller_amount;

            _pageData.sLoads1003LineLFromAdjustments = false; // opm 212045.
            _pageData.sOCredit1Lckd = IsNonZeroDecimal(details.other_credit_amount_1);
            //_pageData.sOCredit2Lckd = IsNonZeroDecimal(details.other_credit_amount_2);

            _pageData.sOCredit1Amt_rep = details.other_credit_amount_1 ?? "";
            _pageData.sOCredit2Amt_rep = details.other_credit_amount_2 ?? "";
            _pageData.sOCredit3Amt_rep = details.other_credit_amount_3 ?? "";
            _pageData.sOCredit4Amt_rep = details.other_credit_amount_4 ?? "";

            _pageData.sOCredit1Desc = details.other_credit_type_1Specified ? MapToLO(details.other_credit_type_1) : "";
            _pageData.sOCredit2Desc = details.other_credit_type_2Specified ? MapToLO(details.other_credit_type_2) : "";
            _pageData.sOCredit3Desc = details.other_credit_type_3Specified ? MapToLO(details.other_credit_type_3) : "";
            _pageData.sOCredit4Desc = details.other_credit_type_4Specified ? MapToLO(details.other_credit_type_4) : "";
        }
        private void ImportLoanAmount()
        {
            if (_ml.DETAIL_TRANSACTION != null)
            {
                string loanAmount;
                decimal temp;
                if (Decimal.TryParse(_ml.LOAN_INFO.amount_approved, out temp) && Decimal.Compare(temp, 0m) > 0)
                {
                    loanAmount = _ml.LOAN_INFO.amount_approved;
                }
                else
                {
                    loanAmount = _ml.LOAN_INFO.amount_requested;
                }

                _pageData.SetLoansPQLAmt(loanAmount, _ml.DETAIL_TRANSACTION.pmi_fee ?? "0", _ml.DETAIL_TRANSACTION.pmi_fee_financed ?? "0");

            }
        }
        private void ImportLoanInfo(MORTGAGE_LOANLOAN_INFO loanInfo)
        {
            loanInfo = loanInfo ?? new MORTGAGE_LOANLOAN_INFO();
            loanInfo.PROPOSED_HOUSING_EXPENSE = loanInfo.PROPOSED_HOUSING_EXPENSE ?? new MORTGAGE_LOAN_INFOPROPOSED_HOUSING_EXPENSE();

            // Import the old GFE stuff first. We don't want whatever is in here to overwrite other stuff.
            ImportGFE(loanInfo.GFE);

            ImportLoanAmount();

            _pageData.sLT = loanInfo.mortgage_loan_typeSpecified ? MapToLO(loanInfo.mortgage_loan_type) : E_sLT.Conventional;
            _pageData.sLTODesc = loanInfo.mortgage_loan_type_other_desc ?? "";
            _pageData.sProRealETxR_rep = "0";
            _pageData.sProHazInsR_rep = "0";
            _pageData.sSubFinIR_rep = "0";
            _pageData.sProHazInsMb_rep = loanInfo.PROPOSED_HOUSING_EXPENSE.hazard_insurance ?? "";
            _pageData.sProRealETxMb_rep = loanInfo.PROPOSED_HOUSING_EXPENSE.real_estate_tax ?? "";
            _pageData.sProMIns_rep = loanInfo.PROPOSED_HOUSING_EXPENSE.mortgage_insurance ?? "";
            _pageData.sProHoAssocDues_rep = loanInfo.PROPOSED_HOUSING_EXPENSE.hoa_due ?? "";
            _pageData.sSubFinMb_rep = loanInfo.PROPOSED_HOUSING_EXPENSE.other_pi ?? "";
            _pageData.sProFloodIns = (decimal)loanInfo.PROPOSED_HOUSING_EXPENSE.flood_insurance;
            _pageData.sFloodExpense.IsEscrowedAtClosing = MapToLOIsEscrowedAtClosing(loanInfo.PROPOSED_HOUSING_EXPENSE.flood_is_escrow);
            _pageData.sHazardExpense.IsEscrowedAtClosing = MapToLOIsEscrowedAtClosing(loanInfo.PROPOSED_HOUSING_EXPENSE.hazard_is_escrow);

            _pageData.sLPurposeT = loanInfo.is_home_equity == BASE_APPLICANTDeclined_answer_race_gender.Y ? E_sLPurposeT.HomeEquity : MapToLO(loanInfo.purpose);
            _pageData.sIsLineOfCredit = loanInfo.is_LOC == BASE_APPLICANTDeclined_answer_race_gender.Y;

            ImportLoanProductData(loanInfo.LOAN_PRODUCT_DATA, isHeloc: loanInfo.is_home_equity == BASE_APPLICANTDeclined_answer_race_gender.Y && loanInfo.is_LOC == BASE_APPLICANTDeclined_answer_race_gender.Y);
            ImportGovermentData(loanInfo.GOVERNMENT_DATA);

            _pageData.sGseRefPurposeT = MapGSERefTypeToLO(loanInfo.refinance_purpose);
            _pageData.sApprVal_rep = loanInfo.estimated_property_value ?? "";
            _pageData.sProdCashoutAmt_rep = loanInfo.amount_cash_out ?? "";

            _pageData.sTerm_rep = loanInfo.loan_term ?? "";
            _pageData.sEquityCalc_rep = loanInfo.down_payment ?? "";

            _pageData.sFinMethT = loanInfo.rate_typeSpecified ? MapToLO(loanInfo.rate_type) : E_sFinMethT.Fixed;

            _pageData.sFinMethPrintAsOtherDesc = loanInfo.rate_type_other_explanation ?? "";
            _pageData.sFinMethodPrintAsOther = loanInfo.rate_type_other_explanation != string.Empty;
            _pageData.sFinMethDesc = loanInfo.arm_description ?? "";

            _pageData.sEstateHeldT = loanInfo.estate_held_inSpecified ? MapToLO(loanInfo.estate_held_in) : E_sEstateHeldT.FeeSimple;

            _pageData.sDwnPmtSrc = MapDownPaymentSourceToLO(loanInfo.down_payment_source);

            _pageData.sDwnPmtSrcExplain = loanInfo.down_payment_description ?? "";

            _pageData.sLienPosT = MapToLienPosition(loanInfo.lien_position);

            _pageData.sRAdjLifeCapR_rep = loanInfo.lifetime_cap ?? string.Empty;

            // These two LPQ fields map to the same LQB field. We'll prioritize using officer_is_other_non_amortizing.
            if (loanInfo.officer_is_other_non_amortizingSpecified)
            {
                _pageData.sHmdaOtherNonAmortFeatureT = MapToLOHmdaOtherNonAmortFeatureT(loanInfo.officer_is_other_non_amortizing);
            }
            else
            {
                _pageData.sHmdaOtherNonAmortFeatureT = MapToLOHmdaOtherNonAmortFeatureT(loanInfo.consumer_is_other_non_amortizing);
            }

            _pageData.sProdDocT = ToLO(loanInfo.document_type);
            ImportFreddieMac(loanInfo.FREDDIE_MAC);

            _pageData.GetAppData(0).aTitleNm1 = GetVestingName(loanInfo, 0);
            _pageData.GetAppData(0).aTitleNm2 = GetVestingName(loanInfo, 1);

            _pageData.sSpLegalDesc = string.IsNullOrEmpty(loanInfo.LEGAL_DESCRIPTION) ? "" : loanInfo.LEGAL_DESCRIPTION;

            _pageData.sLpTemplateNm = loanInfo.officer_program_name;

            var lienPosition = MapToLienPosition(loanInfo.lien_position);
            if (loanInfo.is_home_equity == BASE_APPLICANTDeclined_answer_race_gender.Y)
            {
                if (lienPosition == E_sLienPosT.First)
                {
                    var trustDeed = loanInfo.CURRENT_TRUST_DEED?.ElementAtOrDefault(1);
                    if (trustDeed != null && trustDeed.will_be_paid_off != BASE_APPLICANTDeclined_answer_race_gender.Y)
                    {
                        PopulateSubfinancingFields(trustDeed.loan_amount, trustDeed.balance, trustDeed.term, trustDeed.rate, trustDeed.payment, BASE_APPLICANTDeclined_answer_race_gender.N, trustDeed.is_heloc);
                    }
                }
                else if(lienPosition == E_sLienPosT.Second)
                {
                    var trustDeed = loanInfo.CURRENT_TRUST_DEED?.ElementAtOrDefault(0);
                    if (trustDeed != null && trustDeed.will_be_paid_off != BASE_APPLICANTDeclined_answer_race_gender.Y)
                    {
                        Populate1stFields(trustDeed.loan_amount, trustDeed.balance, trustDeed.payment, BASE_APPLICANTDeclined_answer_race_gender.N, trustDeed.is_heloc);
                    }
                }
            }
            else
            {
                if (lienPosition == E_sLienPosT.First)
                {
                    var trustDeed = loanInfo.NEW_TRUST_DEED?.ElementAtOrDefault(1);
                    if (trustDeed != null)
                    {
                        PopulateSubfinancingFields(trustDeed.loan_amount, trustDeed.balance, trustDeed.term, trustDeed.rate, trustDeed.payment, trustDeed.is_concurrent_closing, trustDeed.is_HELOC);
                    }
                }
                else if(lienPosition == E_sLienPosT.Second)
                {
                    var trustDeed = loanInfo.NEW_TRUST_DEED?.ElementAtOrDefault(0);
                    if (trustDeed != null)
                    {
                        Populate1stFields(trustDeed.loan_amount, trustDeed.balance, trustDeed.payment, trustDeed.is_concurrent_closing, trustDeed.is_HELOC);
                    }
                }
            }

            _pageData.sRAdjIndexR_rep = loanInfo.index ?? string.Empty;
            _pageData.sRAdjFloorR_rep = loanInfo.floor ?? "0";
            _pageData.sRAdjMarginR_rep = loanInfo.margin ?? "0";

            _pageData.sApprRprtRd_rep = loanInfo.property_value_source_date ?? "";

            // OPM 132520 - Map sSpValuationMethodT to property_value_source
            switch (loanInfo.property_value_source)
            {
                case MORTGAGE_LOAN_INFOProperty_value_source.AVM:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.AutomatedValuationModel;
                    break;
                case MORTGAGE_LOAN_INFOProperty_value_source.DESKTOP:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.DesktopAppraisal;
                    break;
                case MORTGAGE_LOAN_INFOProperty_value_source.DRIVE_BY:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.DriveBy;
                    break;
                case MORTGAGE_LOAN_INFOProperty_value_source.APPRAISAL:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.FullAppraisal;
                    break;
                case MORTGAGE_LOAN_INFOProperty_value_source.EXISTAPRSL:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.PriorAppraisalUsed;
                    break;
                case MORTGAGE_LOAN_INFOProperty_value_source.Item:
                case MORTGAGE_LOAN_INFOProperty_value_source.STATED:
                case MORTGAGE_LOAN_INFOProperty_value_source.TAX_NOTE:
                case MORTGAGE_LOAN_INFOProperty_value_source.PURCHPRICE:
                case MORTGAGE_LOAN_INFOProperty_value_source.CAUTION:
                case MORTGAGE_LOAN_INFOProperty_value_source.PROP_ASSES:
                case MORTGAGE_LOAN_INFOProperty_value_source.PMI_INSURE:
                case MORTGAGE_LOAN_INFOProperty_value_source.BRIDGELOAN:
                case MORTGAGE_LOAN_INFOProperty_value_source.EXCEPTION:
                    _pageData.sSpValuationMethodT = E_sSpValuationMethodT.LeaveBlank;
                    break;
                default:
                    throw new UnhandledEnumException(loanInfo.property_value_source);
            }
        }

        private void PopulateSubfinancingFields(string loanAmount, string balance, string term, string rate, string payment, BASE_APPLICANTDeclined_answer_race_gender isConcurrentClosing, BASE_APPLICANTDeclined_answer_race_gender isHeloc)
        {
            _pageData.sSubFin_rep = loanAmount ?? string.Empty;
            _pageData.sConcurSubFin_rep = balance ?? string.Empty;
            _pageData.sIsOFinNew = isConcurrentClosing == BASE_APPLICANTDeclined_answer_race_gender.Y;
            _pageData.sIsOFinCreditLineInDrawPeriod = isHeloc == BASE_APPLICANTDeclined_answer_race_gender.Y;

            int termParsed;
            decimal rateParsed;
            if (decimal.TryParse(rate, out rateParsed) && rateParsed > 0 && int.TryParse(term, out termParsed) && termParsed > 0)
            {
                _pageData.sSubFinIR = rateParsed;
                _pageData.sSubFinTerm = termParsed;
            }
            else
            {
                _pageData.sSubFinPmt_rep = payment ?? string.Empty;
                _pageData.sSubFinPmtLckd = true;
            }
        }

        private void Populate1stFields(string loanAmount, string balance, string payment, BASE_APPLICANTDeclined_answer_race_gender isConcurrentClosing, BASE_APPLICANTDeclined_answer_race_gender isHeloc)
        {
            _pageData.s1stMtgOrigLAmt_rep = loanAmount ?? string.Empty;
            _pageData.sRemain1stMBal_rep = balance ?? string.Empty;
            _pageData.sRemain1stMPmt_rep = payment ?? string.Empty;
            _pageData.sIsOFinNew = isConcurrentClosing == BASE_APPLICANTDeclined_answer_race_gender.Y;
            _pageData.sIsOFinCreditLineInDrawPeriod = isHeloc == BASE_APPLICANTDeclined_answer_race_gender.Y;
        }

        private void ImportGFE(MORTGAGE_LOAN_INFOGFE gfe)
        {
            if (gfe == null || string.IsNullOrEmpty(gfe._gfeversion))
            {
                return;
            }

            switch (gfe._gfeversion)
            {
                case "2010":
                    Import2010GFE(gfe);
                    break;
                case "2009":
                    Import2009GFE(gfe);
                    break;
                default:  //assume old version
                    break;
            }
        }


        private void Import2009GFE(MORTGAGE_LOAN_INFOGFE gfe)
        {
            gfe = gfe ?? new MORTGAGE_LOAN_INFOGFE();

            IPreparerFields gfeTil = _pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            gfeTil.PrepareDate_rep = gfe._prepareddate ?? "";

            gfe._daysinyear = _pageData.sDaysInYr_rep;
            gfeTil.Update();

            #region 800
            _pageData.sLOrigFPc_rep = gfe._801percentagefee ?? "";
            _pageData.sLOrigFMb_rep = gfe._801flatfee ?? "";

            _pageData.sLOrigFProps = GetGFEDetails(gfe._801isaprSpecified, gfe._801isapr, gfe._801ispaidtobrokerSpecified, gfe._801ispaidtobroker, gfe._801paidby, gfe._801pocSpecified, gfe._801poc);

            _pageData.sLDiscntPc_rep = gfe._802percentagefee ?? "";
            _pageData.sLDiscntFMb_rep = gfe._802flatfee ?? "";
            _pageData.sLDiscntProps = GetGFEDetails(gfe._802isaprSpecified, gfe._802isapr, gfe._802ispaidtobrokerSpecified, gfe._802ispaidtobroker, gfe._802paidby, gfe._802pocSpecified, gfe._802poc);

            _pageData.sApprF_rep = gfe._803amount ?? "";
            _pageData.sApprFPaid = MapYNToBool(gfe._803ispaid);
            _pageData.sApprFProps = GetGFEDetails(gfe._803isaprSpecified, gfe._803isapr, gfe._803ispaidtobrokerSpecified, gfe._803ispaidtobroker, gfe._803paidby, gfe._803pocSpecified, gfe._803poc);

            _pageData.sCrF_rep = gfe._804amount ?? "";
            _pageData.sCrFPaid = MapYNToBool(gfe._804ispaid);
            _pageData.sCrFProps = GetGFEDetails(gfe._804isaprSpecified, gfe._804isapr, gfe._804ispaidtobrokerSpecified, gfe._804ispaidtobroker, gfe._804paidby, gfe._804pocSpecified, gfe._804poc);

            _pageData.sInspectF_rep = gfe._805amount ?? "";
            _pageData.sInspectFProps = GetGFEDetails(gfe._805isaprSpecified, gfe._805isapr, gfe._805ispaidtobrokerSpecified, gfe._805ispaidtobroker, gfe._805paidby, gfe._805pocSpecified, gfe._805poc);

            _pageData.sMBrokFPc_rep = gfe._808percentagefee ?? "";
            _pageData.sMBrokFMb_rep = gfe._808flatfee ?? "";
            _pageData.sMBrokFProps = GetGFEDetails(gfe._808isaprSpecified, gfe._808isapr, gfe._808ispaidtobrokerSpecified, gfe._808ispaidtobroker, gfe._808paidby, gfe._808pocSpecified, gfe._808poc);

            _pageData.sTxServF_rep = gfe._809amount ?? "";
            _pageData.sTxServFProps = GetGFEDetails(gfe._809isaprSpecified, gfe._809isapr, gfe._809ispaidtobrokerSpecified, gfe._809ispaidtobroker, gfe._809paidby, gfe._809pocSpecified, gfe._809poc);

            _pageData.sProcF_rep = gfe._810amount ?? "";
            _pageData.sProcFPaid = MapYNToBool(gfe._810ispaid);
            _pageData.sProcFProps = GetGFEDetails(gfe._810isaprSpecified, gfe._810isapr, gfe._810ispaidtobrokerSpecified, gfe._810ispaidtobroker, gfe._810paidby, gfe._810pocSpecified, gfe._810poc);

            _pageData.sUwF_rep = gfe._811amount ?? "";
            _pageData.sUwFProps = GetGFEDetails(gfe._811isaprSpecified, gfe._811isapr, gfe._811ispaidtobrokerSpecified, gfe._811ispaidtobroker, gfe._811paidby, gfe._811pocSpecified, gfe._811poc);

            _pageData.sWireF_rep = gfe._812amount ?? "";
            _pageData.sWireFProps = GetGFEDetails(gfe._812isaprSpecified, gfe._812isapr, gfe._812ispaidtobrokerSpecified, gfe._812ispaidtobroker, gfe._812paidby, gfe._812pocSpecified, gfe._812poc);


            _pageData.s800U1FCode = gfe._800code1 ?? "";
            _pageData.s800U2FCode = gfe._800code2 ?? "";
            _pageData.s800U3FCode = gfe._800code3 ?? "";
            _pageData.s800U4FCode = gfe._800code4 ?? "";
            _pageData.s800U5FCode = gfe._800code5 ?? "";

            _pageData.s800U1FDesc = gfe._800description1 ?? "";
            _pageData.s800U2FDesc = gfe._800description2 ?? "";
            _pageData.s800U3FDesc = gfe._800description3 ?? "";
            _pageData.s800U4FDesc = gfe._800description4 ?? "";
            _pageData.s800U5FDesc = gfe._800description5 ?? "";

            _pageData.s800U1F_rep = gfe._800amount1 ?? "";
            _pageData.s800U2F_rep = gfe._800amount2 ?? "";
            _pageData.s800U3F_rep = gfe._800amount3 ?? "";
            _pageData.s800U4F_rep = gfe._800amount4 ?? "";
            _pageData.s800U5F_rep = gfe._800amount5 ?? "";

            _pageData.s800U1FProps = GetGFEDetails(gfe._800isapr1Specified, gfe._800isapr1, gfe._800ispaidtobroker1Specified, gfe._800ispaidtobroker1, gfe._800paidby1, gfe._800poc1Specified, gfe._800poc1);
            _pageData.s800U2FProps = GetGFEDetails(gfe._800isapr2Specified, gfe._800isapr2, gfe._800ispaidtobroker2Specified, gfe._800ispaidtobroker2, gfe._800paidby2, gfe._800poc2Specified, gfe._800poc2);
            _pageData.s800U3FProps = GetGFEDetails(gfe._800isapr3Specified, gfe._800isapr3, gfe._800ispaidtobroker3Specified, gfe._800ispaidtobroker3, gfe._800paidby3, gfe._800poc3Specified, gfe._800poc3);
            _pageData.s800U4FProps = GetGFEDetails(gfe._800isapr4Specified, gfe._800isapr4, gfe._800ispaidtobroker4Specified, gfe._800ispaidtobroker4, gfe._800paidby4, gfe._800poc4Specified, gfe._800poc4);
            _pageData.s800U5FProps = GetGFEDetails(gfe._800isapr5Specified, gfe._800isapr5, gfe._800ispaidtobroker5Specified, gfe._800ispaidtobroker5, gfe._800paidby5, gfe._800poc5Specified, gfe._800poc5);

            #endregion

            #region 900
            _pageData.sIPiaDy_rep = gfe._901numdays ?? "";
            //_pageData.sIPerDayLckd = IsNonZeroDecimal(gfe._901amount);
            //_pageData.sIPerDay_rep = gfe._901amount;
            _pageData.sIPiaProps = GetGFEDetails(gfe._901isaprSpecified, gfe._901isapr, gfe._901ispaidtobrokerSpecified, gfe._901ispaidtobroker, gfe._901paidby, gfe._901pocSpecified, gfe._901poc);


            if (gfe != null)
            {
                if (!string.IsNullOrEmpty(gfe._902amount))
                {
                    EnsureFieldCanBeUpdated(gfe._902amount, _pageData.sFfUfmip1003_rep, "MIP (GFE 902) has already been set at item g from Transaction Details in the 1003. Ensure values are the same.");
                    _pageData.sFfUfmip1003_rep = gfe._902amount;
                }

            }
            _pageData.sMipPiaProps = GetGFEDetails(gfe._902isaprSpecified, gfe._902isapr, gfe._902ispaidtobrokerSpecified, gfe._902ispaidtobroker, gfe._902paidby, gfe._902pocSpecified, gfe._902poc);

            _pageData.sHazInsPiaMon_rep = gfe._903nummonths ?? "";

            if (gfe._903amountpermonthSpecified)
            {
                _pageData.sProHazInsMb = (decimal)gfe._903amountpermonth;
            }

            _pageData.sProHazInsT = MapTosProHazInsT(gfe._903percentageof);
            _pageData.sProHazInsR_rep = gfe._903percentagefee ?? "";

            _pageData.sHazInsPiaProps = GetGFEDetails(gfe._903isaprSpecified, gfe._903isapr, gfe._903ispaidtobrokerSpecified, gfe._903ispaidtobroker, gfe._903paidby, gfe._903pocSpecified, gfe._903poc);

            _pageData.s904PiaDesc = gfe._904description ?? "";
            _pageData.s904Pia_rep = gfe._904amount ?? "";
            _pageData.s904PiaProps = GetGFEDetails(gfe._904isaprSpecified, gfe._904isapr, gfe._904ispaidtobrokerSpecified, gfe._904ispaidtobroker, gfe._904paidby, gfe._904pocSpecified, gfe._904poc);


            _pageData.sVaFfProps = GetGFEDetails(gfe._905isaprSpecified, gfe._905isapr, gfe._905ispaidtobrokerSpecified, gfe._905ispaidtobroker, gfe._905paidby, gfe._905pocSpecified, gfe._905poc);

            _pageData.s900U1PiaCode = gfe._900code1 ?? "";
            _pageData.s900U1PiaDesc = gfe._900description1 ?? "";
            _pageData.s900U1Pia_rep = gfe._900amount1 ?? "";
            _pageData.s900U1PiaProps = GetGFEDetails(gfe._900isapr1Specified, gfe._900isapr1, gfe._900ispaidtobroker1Specified, gfe._900ispaidtobroker1, gfe._900paidby1, gfe._900poc1Specified, gfe._900poc1);
            #endregion

            #region 1000
            if (null != gfe._1001nummonths) // opm 180983
            {
                _pageData.sHazInsRsrvMonLckd = true;
            }
            _pageData.sHazInsRsrvMon_rep = gfe._1001nummonths ?? "";
            //_pageData.sProHazIns
            _pageData.sHazInsRsrvProps = GetGFEDetails(gfe._1001isaprSpecified, gfe._1001isapr, gfe._1001ispaidtobrokerSpecified, gfe._1001ispaidtobroker, gfe._1001paidby, gfe._1001pocSpecified, gfe._1001poc);

            _pageData.sProMInsR_rep = gfe._1002percentagefee ?? "";
            _pageData.sProMInsT = (E_PercentBaseT)MapTosProHazInsT(gfe._1002percentageof);
            if (!string.IsNullOrEmpty(gfe._1002amountpermonth))
            {
                _pageData.sProMInsMb_rep = gfe._1002amountpermonth ?? "";
            }
            if (null != gfe._1002nummonths) // opm 180983
            {
                _pageData.sMInsRsrvMonLckd = true;
            }
            _pageData.sMInsRsrvMon_rep = gfe._1002nummonths ?? "";
            _pageData.sMInsRsrvProps = GetGFEDetails(gfe._1002isaprSpecified, gfe._1002isapr, gfe._1002ispaidtobrokerSpecified, gfe._1002ispaidtobroker, gfe._1002paidby, gfe._1002pocSpecified, gfe._1002poc);

            if (null != gfe._1003nummonths) // opm 180983
            {
                _pageData.sSchoolTxRsrvMonLckd = true;
            }
            _pageData.sSchoolTxRsrvMon_rep = gfe._1003nummonths ?? "";
            _pageData.sProSchoolTx_rep = gfe._1003amountpermonth ?? "";
            _pageData.sSchoolTxRsrvProps = GetGFEDetails(gfe._1003isaprSpecified, gfe._1003isapr, gfe._1003ispaidtobrokerSpecified, gfe._1003ispaidtobroker, gfe._1003paidby, gfe._1003pocSpecified, gfe._1003poc);

            _pageData.sProRealETxR_rep = gfe._1004percentagefee ?? "";
            _pageData.sProRealETxT = (E_PercentBaseT)MapTosProHazInsT(gfe._1004percentageof);
            if (!string.IsNullOrEmpty(gfe._1004amountpermonth))
            {
                _pageData.sProRealETxMb_rep = gfe._1004amountpermonth ?? "";
            }
            if (null != gfe._1004nummonths) // opm 180983
            {
                _pageData.sRealETxRsrvMonLckd = true;
            }
            _pageData.sRealETxRsrvMon_rep = gfe._1004nummonths ?? "";
            _pageData.sRealETxRsrvProps = GetGFEDetails(gfe._1004isaprSpecified, gfe._1004isapr, gfe._1004ispaidtobrokerSpecified, gfe._1004ispaidtobroker, gfe._1004paidby, gfe._1004pocSpecified, gfe._1004poc);

            if (null != gfe._1005nummonths) // opm 180983
            {
                _pageData.sFloodInsRsrvMonLckd = true;
            }
            _pageData.sFloodInsRsrvMon_rep = gfe._1005nummonths ?? "";
            _pageData.sProFloodIns_rep = gfe._1005amountpermonth ?? "";
            _pageData.sFloodInsRsrvProps = GetGFEDetails(gfe._1005isaprSpecified, gfe._1005isapr, gfe._1005ispaidtobrokerSpecified, gfe._1005ispaidtobroker, gfe._1005paidby, gfe._1005pocSpecified, gfe._1005poc);


            if (null != gfe._1006nummonths) // opm 180983
            {
                _pageData.s1006RsrvMonLckd = true;
            }
            _pageData.s1006ProHExpDesc = gfe._1006description ?? "";
            _pageData.s1006RsrvMon_rep = gfe._1006nummonths ?? "";
            _pageData.s1006ProHExp_rep = gfe._1006amountpermonth ?? "";
            _pageData.s1006RsrvProps = GetGFEDetails(gfe._1006isaprSpecified, gfe._1006isapr, gfe._1006ispaidtobrokerSpecified, gfe._1006ispaidtobroker, gfe._1006paidby, gfe._1006pocSpecified, gfe._1006poc);

            if (null != gfe._1007nummonths) // opm 180983
            {
                _pageData.s1007RsrvMonLckd = true;
            }
            _pageData.s1007ProHExpDesc = gfe._1007description ?? "";
            _pageData.s1007RsrvMon_rep = gfe._1007nummonths ?? "";
            _pageData.s1007ProHExp_rep = gfe._1007amountpermonth ?? "";
            _pageData.s1007RsrvProps = GetGFEDetails(gfe._1007isaprSpecified, gfe._1007isapr, gfe._1007ispaidtobrokerSpecified, gfe._1007ispaidtobroker, gfe._1007paidby, gfe._1007pocSpecified, gfe._1007poc);


            _pageData.sAggregateAdjRsrv_rep = gfe._1008amount ?? "";
            //opm 45122
            //_pageData.sAggregateAdjRsrvLckd = IsNonZeroDecimal(gfe._1008amount);
            _pageData.sAggregateAdjRsrvLckd = true;
            _pageData.sAggregateAdjRsrvProps = GetGFEDetails(gfe._1008isaprSpecified, gfe._1008isapr, gfe._1008ispaidtobrokerSpecified, gfe._1008ispaidtobroker, gfe._1008paidby, gfe._1008pocSpecified, gfe._1008poc);

            #endregion

            #region 1100
            _pageData.sEscrowFTable = gfe._1101description ?? "";
            _pageData.sEscrowF_rep = gfe._1101amount ?? "";
            _pageData.sEscrowFProps = GetGFEDetails(gfe._1101isaprSpecified, gfe._1101isapr, gfe._1101ispaidtobrokerSpecified, gfe._1101ispaidtobroker, gfe._1101paidby, gfe._1101pocSpecified, gfe._1101poc);

            _pageData.sDocPrepF_rep = gfe._1105amount ?? "";
            _pageData.sDocPrepFProps = GetGFEDetails(gfe._1105isaprSpecified, gfe._1105isapr, gfe._1105ispaidtobrokerSpecified, gfe._1105ispaidtobroker, gfe._1105paidby, gfe._1105pocSpecified, gfe._1105poc);

            _pageData.sNotaryF_rep = gfe._1106amount ?? "";
            _pageData.sNotaryFProps = GetGFEDetails(gfe._1106isaprSpecified, gfe._1106isapr, gfe._1106ispaidtobrokerSpecified, gfe._1106ispaidtobroker, gfe._1106paidby, gfe._1106pocSpecified, gfe._1106poc);

            _pageData.sAttorneyF_rep = gfe._1107amount ?? "";
            _pageData.sAttorneyFProps = GetGFEDetails(gfe._1107isaprSpecified, gfe._1107isapr, gfe._1107ispaidtobrokerSpecified, gfe._1107ispaidtobroker, gfe._1107paidby, gfe._1107pocSpecified, gfe._1107poc);

            _pageData.sTitleInsF_rep = gfe._1108amount ?? "";
            _pageData.sTitleInsFTable = gfe._1108description ?? "";
            _pageData.sTitleInsFProps = GetGFEDetails(gfe._1108isaprSpecified, gfe._1108isapr, gfe._1108ispaidtobrokerSpecified, gfe._1108ispaidtobroker, gfe._1108paidby, gfe._1108pocSpecified, gfe._1108poc);


            _pageData.sU1TcCode = gfe._1100code1 ?? "";
            _pageData.sU1Tc_rep = gfe._1100amount1 ?? "";
            _pageData.sU1TcDesc = gfe._1100description1 ?? "";
            _pageData.sU1TcProps = GetGFEDetails(gfe._1100isapr1Specified, gfe._1100isapr1, gfe._1100ispaidtobroker1Specified, gfe._1100ispaidtobroker1, gfe._1100paidby1, gfe._1100poc1Specified, gfe._1100poc1);


            _pageData.sU2TcCode = gfe._1100code2 ?? "";
            _pageData.sU2Tc_rep = gfe._1100amount2 ?? "";
            _pageData.sU2TcDesc = gfe._1100description2 ?? "";
            _pageData.sU2TcProps = GetGFEDetails(gfe._1100isapr2Specified, gfe._1100isapr2, gfe._1100ispaidtobroker2Specified, gfe._1100ispaidtobroker2, gfe._1100paidby2, gfe._1100poc2Specified, gfe._1100poc2);


            _pageData.sU3TcCode = gfe._1100code3 ?? "";
            _pageData.sU3Tc_rep = gfe._1100amount3 ?? "";
            _pageData.sU3TcDesc = gfe._1100description3 ?? "";
            _pageData.sU3TcProps = GetGFEDetails(gfe._1100isapr3Specified, gfe._1100isapr3, gfe._1100ispaidtobroker3Specified, gfe._1100ispaidtobroker3, gfe._1100paidby3, gfe._1100poc3Specified, gfe._1100poc3);


            _pageData.sU4TcCode = gfe._1100code4 ?? "";
            _pageData.sU4Tc_rep = gfe._1100amount4 ?? "";
            _pageData.sU4TcDesc = gfe._1100description4 ?? "";
            _pageData.sU4TcProps = GetGFEDetails(gfe._1100isapr4Specified, gfe._1100isapr4, gfe._1100ispaidtobroker4Specified, gfe._1100ispaidtobroker4, gfe._1100paidby4, gfe._1100poc4Specified, gfe._1100poc4);



            #endregion

            #region 1200
            _pageData.sRecFDesc = gfe._1201description ?? "";
            _pageData.sRecFPc_rep = gfe._1201percentagefee ?? "";
            _pageData.sRecBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1201percentageof);
            _pageData.sRecFMb = Sum(gfe._1201flatfee, gfe._1201mortgagefee, gfe._1201releasefee);
            _pageData.sRecFProps = GetGFEDetails(gfe._1201isaprSpecified, gfe._1201isapr, gfe._1201ispaidtobrokerSpecified, gfe._1201ispaidtobroker, gfe._1201paidby, gfe._1201pocSpecified, gfe._1201poc);

            _pageData.sCountyRtcDesc = gfe._1202description ?? "";
            _pageData.sCountyRtcPc_rep = gfe._1202percentagefee ?? "";
            _pageData.sCountyRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1202percentageof);
            _pageData.sCountyRtcMb = Sum(gfe._1202flatfee, gfe._1202mortgagefee);
            _pageData.sCountyRtcProps = GetGFEDetails(gfe._1202isaprSpecified, gfe._1202isapr, gfe._1202ispaidtobrokerSpecified, gfe._1202ispaidtobroker, gfe._1202paidby, gfe._1202pocSpecified, gfe._1202poc);

            _pageData.sStateRtcDesc = gfe._1203description ?? "";
            _pageData.sStateRtcPc_rep = gfe._1203percentagefee ?? "";
            _pageData.sStateRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1203percentageof);
            _pageData.sStateRtcMb = Sum(gfe._1203flatfee, gfe._1203mortgagefee);
            _pageData.sStateRtcProps = GetGFEDetails(gfe._1203isaprSpecified, gfe._1203isapr, gfe._1203ispaidtobrokerSpecified, gfe._1203ispaidtobroker, gfe._1203paidby, gfe._1203pocSpecified, gfe._1203poc);


            _pageData.sU1GovRtcCode = gfe._1200code1 ?? "";
            _pageData.sU1GovRtcDesc = gfe._1200description1 ?? "";
            _pageData.sU1GovRtcPc_rep = gfe._1200percentagefee1 ?? "";
            _pageData.sU1GovRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1200percentageof1);
            _pageData.sU1GovRtcMb_rep = gfe._1200flatfee1 ?? "";
            _pageData.sU1GovRtcProps = GetGFEDetails(gfe._1200isapr1Specified, gfe._1200isapr1, gfe._1200ispaidtobroker1Specified, gfe._1200ispaidtobroker1, gfe._1200paidby1, gfe._1200poc1Specified, gfe._1200poc1);


            _pageData.sU2GovRtcCode = gfe._1200code2 ?? "";
            _pageData.sU2GovRtcDesc = gfe._1200description2 ?? "";
            _pageData.sU2GovRtcPc_rep = gfe._1200percentagefee2 ?? "";
            _pageData.sU2GovRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1200percentageof2);
            _pageData.sU2GovRtcMb_rep = gfe._1200flatfee2 ?? "";
            _pageData.sU2GovRtcProps = GetGFEDetails(gfe._1200isapr2Specified, gfe._1200isapr2, gfe._1200ispaidtobroker2Specified, gfe._1200ispaidtobroker2, gfe._1200paidby2, gfe._1200poc2Specified, gfe._1200poc2);



            _pageData.sU3GovRtcCode = gfe._1200code3 ?? "";
            _pageData.sU3GovRtcDesc = gfe._1200description3 ?? "";
            _pageData.sU3GovRtcPc_rep = gfe._1200percentagefee3 ?? "";
            _pageData.sU3GovRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1200percentageof3);
            _pageData.sU3GovRtcMb_rep = gfe._1200flatfee3 ?? "";
            _pageData.sU3GovRtcProps = GetGFEDetails(gfe._1200isapr3Specified, gfe._1200isapr3, gfe._1200ispaidtobroker3Specified, gfe._1200ispaidtobroker3, gfe._1200paidby3, gfe._1200poc3Specified, gfe._1200poc3);


            #endregion

            #region 1300

            _pageData.sPestInspectF_rep = gfe._1302amount ?? "";
            _pageData.sPestInspectFProps = GetGFEDetails(gfe._1302isaprSpecified, gfe._1302isapr, gfe._1302ispaidtobrokerSpecified, gfe._1302ispaidtobroker, gfe._1302paidby, gfe._1302pocSpecified, gfe._1302poc);

            _pageData.sU1ScCode = gfe._1300code1 ?? "";
            _pageData.sU1ScDesc = gfe._1300description1 ?? "";
            _pageData.sU1Sc_rep = gfe._1300amount1 ?? "";
            _pageData.sU1ScProps = GetGFEDetails(gfe._1300isapr1Specified, gfe._1300isapr1, gfe._1300ispaidtobroker1Specified, gfe._1300ispaidtobroker1, gfe._1300paidby1, gfe._1300poc1Specified, gfe._1300poc1);

            _pageData.sU2ScCode = gfe._1300code2 ?? "";
            _pageData.sU2ScDesc = gfe._1300description2 ?? "";
            _pageData.sU2Sc_rep = gfe._1300amount2 ?? "";
            _pageData.sU2ScProps = GetGFEDetails(gfe._1300isapr2Specified, gfe._1300isapr2, gfe._1300ispaidtobroker2Specified, gfe._1300ispaidtobroker2, gfe._1300paidby2, gfe._1300poc2Specified, gfe._1300poc2);

            _pageData.sU3ScCode = gfe._1300code3 ?? "";
            _pageData.sU3ScDesc = gfe._1300description3 ?? "";
            _pageData.sU3Sc_rep = gfe._1300amount3 ?? "";
            _pageData.sU3ScProps = GetGFEDetails(gfe._1300isapr3Specified, gfe._1300isapr3, gfe._1300ispaidtobroker3Specified, gfe._1300ispaidtobroker3, gfe._1300paidby3, gfe._1300poc3Specified, gfe._1300poc3);

            _pageData.sU4ScCode = gfe._1300code4 ?? "";
            _pageData.sU4ScDesc = gfe._1300description4 ?? "";
            _pageData.sU4Sc_rep = gfe._1300amount4 ?? "";
            _pageData.sU4ScProps = GetGFEDetails(gfe._1300isapr4Specified, gfe._1300isapr4, gfe._1300ispaidtobroker4Specified, gfe._1300ispaidtobroker4, gfe._1300paidby4, gfe._1300poc4Specified, gfe._1300poc4);

            _pageData.sU5ScCode = gfe._1300code5 ?? "";
            _pageData.sU5ScDesc = gfe._1300description5 ?? "";
            _pageData.sU5Sc_rep = gfe._1300amount5 ?? "";
            _pageData.sU5ScProps = GetGFEDetails(gfe._1300isapr5Specified, gfe._1300isapr5, gfe._1300ispaidtobroker5Specified, gfe._1300ispaidtobroker5, gfe._1300paidby5, gfe._1300poc5Specified, gfe._1300poc5);


            #endregion

            _pageData.sBrokComp1Desc = gfe._compensationtobrokerdescription1 ?? "";
            _pageData.sBrokComp1_rep = gfe._compensationtobrokeramount1 ?? "";
            _pageData.sBrokComp1Pc_rep = "";
            _pageData.sBrokComp1Lckd = !string.IsNullOrEmpty(_pageData.sBrokComp1Pc_rep);

            _pageData.sBrokComp2Desc = gfe._compensationtobrokerdescription2 ?? "";
            _pageData.sBrokComp2_rep = gfe._compensationtobrokeramount2 ?? "";

        }

        private void Import2010GFE(MORTGAGE_LOAN_INFOGFE gfe)
        {
            gfe = gfe ?? new MORTGAGE_LOAN_INFOGFE();

            _pageData.sDaysInYr_rep = gfe._daysinyear ?? "";
            IPreparerFields gfeTil = _pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            gfeTil.PrepareDate_rep = gfe._prepareddate ?? "";
            gfeTil.Update();
            _pageData.sGfeLockPeriodBeforeSettlement_rep = gfe._daysrequireratelock;

            #region 800 
            //801
            _pageData.sLOrigFPc_rep = gfe._801percentagefee ?? "";
            _pageData.sLOrigFMb_rep = gfe._801flatfee ?? "";
            _pageData.sLOrigFProps = GetGFEDetails(gfe._801isaprSpecified, gfe._801isapr, gfe._801ispaidtobrokerSpecified, gfe._801ispaidtobroker, gfe._801paidby, gfe._801pocSpecified, gfe._801poc);


            //802
            _pageData.sLDiscntPc_rep = gfe._802percentagefee ?? "";
            _pageData.sLDiscntFMb_rep = gfe._802flatfee ?? "";
            _pageData.sLDiscntProps = GetGFEDetails(gfe._802isaprSpecified, gfe._802isapr, gfe._802ispaidtobrokerSpecified, gfe._802ispaidtobroker, gfe._802paidby, gfe._802pocSpecified, gfe._802poc);

            //803 - Dont Import PEr Request 

            //804
            _pageData.sApprF_rep = gfe._804amount ?? "";
            _pageData.sApprFPaid = MapYNToBool(gfe._804ispaid);
            _pageData.sApprFProps = GetGFEDetails(gfe._804isaprSpecified, gfe._804isapr, gfe._804ispaidtobrokerSpecified, gfe._804ispaidtobroker, gfe._804paidby, gfe._804pocSpecified, gfe._804poc);

            //805
            _pageData.sCrF_rep = gfe._805amount ?? "";
            _pageData.sCrFPaid = MapYNToBool(gfe._805ispaid);
            _pageData.sCrFProps = GetGFEDetails(gfe._805isaprSpecified, gfe._805isapr, gfe._805ispaidtobrokerSpecified, gfe._805ispaidtobroker, gfe._805paidby, gfe._805pocSpecified, gfe._805poc);

            //806
            _pageData.sTxServF_rep = gfe._806amount ?? "";
            _pageData.sTxServFProps = GetGFEDetails(gfe._806isaprSpecified, gfe._806isapr, gfe._806ispaidtobrokerSpecified, gfe._806ispaidtobroker, gfe._806paidby, gfe._806pocSpecified, gfe._806poc);

            //807 
            _pageData.sFloodCertificationF_rep = gfe._807amount ?? "";
            _pageData.sFloodCertificationFProps = GetGFEDetails(gfe._807isaprSpecified, gfe._807isapr, gfe._807ispaidtobrokerSpecified, gfe._807ispaidtobroker, gfe._807paidby, gfe._807pocSpecified, gfe._807poc);

            //808
            _pageData.sMBrokFPc_rep = gfe._808percentagefee ?? "";
            _pageData.sMBrokFMb_rep = gfe._808flatfee ?? "";
            if (string.IsNullOrEmpty(gfe._808flatfee))   //lpq didnt have the flat fee field editable in the new GFE so if its empty or null use the amount as the flat fee on export export correctly. 
            {
                _pageData.sMBrokFPc_rep = "0";
                _pageData.sMBrokFMb_rep = gfe._808amount;
            }
            _pageData.sMBrokFProps = GetGFEDetails(gfe._808isaprSpecified, gfe._808isapr, gfe._808ispaidtobrokerSpecified, gfe._808ispaidtobroker, gfe._808paidby, gfe._808pocSpecified, gfe._808poc);

            //809 
            _pageData.sInspectF_rep = gfe._809amount ?? "";
            _pageData.sInspectFProps = GetGFEDetails(gfe._809isaprSpecified, gfe._809isapr, gfe._809ispaidtobrokerSpecified, gfe._809ispaidtobroker, gfe._809paidby, gfe._809pocSpecified, gfe._809poc);

            //810
            _pageData.sProcF_rep = gfe._810amount ?? "";
            _pageData.sProcFPaid = MapYNToBool(gfe._810ispaid);
            _pageData.sProcFProps = GetGFEDetails(gfe._810isaprSpecified, gfe._810isapr, gfe._810ispaidtobrokerSpecified, gfe._810ispaidtobroker, gfe._810paidby, gfe._810pocSpecified, gfe._810poc);

            //811
            _pageData.sUwF_rep = gfe._811amount ?? "";
            _pageData.sUwFProps = GetGFEDetails(gfe._811isaprSpecified, gfe._811isapr, gfe._811ispaidtobrokerSpecified, gfe._811ispaidtobroker, gfe._811paidby, gfe._811pocSpecified, gfe._811poc);

            //812
            _pageData.sWireF_rep = gfe._812amount ?? "";
            _pageData.sWireFProps = GetGFEDetails(gfe._812isaprSpecified, gfe._812isapr, gfe._812ispaidtobrokerSpecified, gfe._812ispaidtobroker, gfe._812paidby, gfe._812pocSpecified, gfe._812poc);

            _pageData.s800U1FCode = gfe._800code1 ?? "";
            _pageData.s800U2FCode = gfe._800code2 ?? "";
            _pageData.s800U3FCode = gfe._800code3 ?? "";
            _pageData.s800U4FCode = gfe._800code4 ?? "";
            _pageData.s800U5FCode = gfe._800code5 ?? "";

            _pageData.s800U1FDesc = gfe._800description1 ?? "";
            _pageData.s800U2FDesc = gfe._800description2 ?? "";
            _pageData.s800U3FDesc = gfe._800description3 ?? "";
            _pageData.s800U4FDesc = gfe._800description4 ?? "";
            _pageData.s800U5FDesc = gfe._800description5 ?? "";

            _pageData.s800U1F_rep = gfe._800amount1 ?? "";
            _pageData.s800U2F_rep = gfe._800amount2 ?? "";
            _pageData.s800U3F_rep = gfe._800amount3 ?? "";
            _pageData.s800U4F_rep = gfe._800amount4 ?? "";
            _pageData.s800U5F_rep = gfe._800amount5 ?? "";

            _pageData.s800U1FProps = GetGFEDetails(gfe._800isapr1Specified, gfe._800isapr1, gfe._800ispaidtobroker1Specified, gfe._800ispaidtobroker1, gfe._800paidby1, gfe._800poc1Specified, gfe._800poc1);
            _pageData.s800U2FProps = GetGFEDetails(gfe._800isapr2Specified, gfe._800isapr2, gfe._800ispaidtobroker2Specified, gfe._800ispaidtobroker2, gfe._800paidby2, gfe._800poc2Specified, gfe._800poc2);
            _pageData.s800U3FProps = GetGFEDetails(gfe._800isapr3Specified, gfe._800isapr3, gfe._800ispaidtobroker3Specified, gfe._800ispaidtobroker3, gfe._800paidby3, gfe._800poc3Specified, gfe._800poc3);
            _pageData.s800U4FProps = GetGFEDetails(gfe._800isapr4Specified, gfe._800isapr4, gfe._800ispaidtobroker4Specified, gfe._800ispaidtobroker4, gfe._800paidby4, gfe._800poc4Specified, gfe._800poc4);
            _pageData.s800U5FProps = GetGFEDetails(gfe._800isapr5Specified, gfe._800isapr5, gfe._800ispaidtobroker5Specified, gfe._800ispaidtobroker5, gfe._800paidby5, gfe._800poc5Specified, gfe._800poc5);

            #endregion

            #region 900 
            _pageData.sIPiaDy_rep = gfe._901numdays ?? "";
            //_pageData.sIPerDayLckd = IsNonZeroDecimal(gfe._901amount);
            //_pageData.sIPerDay_rep = gfe._901amount;
            _pageData.sIPiaProps = GetGFEDetails(gfe._901isaprSpecified, gfe._901isapr, gfe._901ispaidtobrokerSpecified, gfe._901ispaidtobroker, gfe._901paidby, gfe._901pocSpecified, gfe._901poc);


            if (gfe != null)
            {
                if (!string.IsNullOrEmpty(gfe._902amount))
                {
                    EnsureFieldCanBeUpdated(gfe._902amount, _pageData.sFfUfmip1003_rep, "MIP (GFE 902) has already been set at item g from Transaction Details in the 1003. Ensure values are the same.");
                    _pageData.sFfUfmip1003_rep = gfe._902amount;
                }

            }
            _pageData.sMipPiaProps = GetGFEDetails(gfe._902isaprSpecified, gfe._902isapr, gfe._902ispaidtobrokerSpecified, gfe._902ispaidtobroker, gfe._902paidby, gfe._902pocSpecified, gfe._902poc);

            _pageData.sHazInsPiaMon_rep = gfe._903nummonths ?? "";

            if (gfe._903amountpermonthSpecified)
            {
                _pageData.sProHazInsMb = (decimal)gfe._903amountpermonth;
            }

            _pageData.sProHazInsT = MapTosProHazInsT(gfe._903percentageof);
            _pageData.sProHazInsR_rep = gfe._903percentagefee ?? "";

            _pageData.sHazInsPiaProps = GetGFEDetails(gfe._903isaprSpecified, gfe._903isapr, gfe._903ispaidtobrokerSpecified, gfe._903ispaidtobroker, gfe._903paidby, gfe._903pocSpecified, gfe._903poc);

            _pageData.s904PiaDesc = gfe._904description ?? "";
            _pageData.s904Pia_rep = gfe._904amount ?? "";
            _pageData.s904PiaProps = GetGFEDetails(gfe._904isaprSpecified, gfe._904isapr, gfe._904ispaidtobrokerSpecified, gfe._904ispaidtobroker, gfe._904paidby, gfe._904pocSpecified, gfe._904poc);


            _pageData.sVaFfProps = GetGFEDetails(gfe._905isaprSpecified, gfe._905isapr, gfe._905ispaidtobrokerSpecified, gfe._905ispaidtobroker, gfe._905paidby, gfe._905pocSpecified, gfe._905poc);

            _pageData.s900U1PiaCode = gfe._900code1 ?? "";
            _pageData.s900U1PiaDesc = gfe._900description1 ?? "";
            _pageData.s900U1Pia_rep = gfe._900amount1 ?? "";
            _pageData.s900U1PiaProps = GetGFEDetails(gfe._900isapr1Specified, gfe._900isapr1, gfe._900ispaidtobroker1Specified, gfe._900ispaidtobroker1, gfe._900paidby1, gfe._900poc1Specified, gfe._900poc1);
            #endregion

            #region 1000
            //1002
            if (null != gfe._1002nummonths) // opm 180983
            {
                _pageData.sHazInsRsrvMonLckd = true;
            }
            _pageData.sHazInsRsrvMon_rep = gfe._1002nummonths ?? "";
            _pageData.sHazInsRsrvProps = GetGFEDetails(gfe._1002isaprSpecified, gfe._1002isapr, gfe._1002ispaidtobrokerSpecified, gfe._1002ispaidtobroker, gfe._1002paidby, gfe._1002pocSpecified, gfe._1002poc);

            //1003
            if (null != gfe._1003nummonths) // opm 180983
            {
                _pageData.sMInsRsrvMonLckd = true;
            }
            _pageData.sMInsRsrvMon_rep = gfe._1003nummonths ?? "";
            _pageData.sMInsRsrvProps = GetGFEDetails(gfe._1003isaprSpecified, gfe._1003isapr, gfe._1003ispaidtobrokerSpecified, gfe._1003ispaidtobroker, gfe._1003paidby, gfe._1003pocSpecified, gfe._1003poc);
            //ignore importing the total for now i guess

            //1004
            _pageData.sProRealETxR_rep = gfe._1004percentagefee ?? "";
            _pageData.sProRealETxT = (E_PercentBaseT)MapTosProHazInsT(gfe._1004percentageof);
            if (!string.IsNullOrEmpty(gfe._1004amountpermonth))
            {
                _pageData.sProRealETxMb_rep = gfe._1004amountpermonth ?? "";
            }
            if (null != gfe._1004nummonths) // opm 180983
            {
                _pageData.sRealETxRsrvMonLckd = true;
            }
            _pageData.sRealETxRsrvMon_rep = gfe._1004nummonths ?? "";
            _pageData.sRealETxRsrvProps = GetGFEDetails(gfe._1004isaprSpecified, gfe._1004isapr, gfe._1004ispaidtobrokerSpecified, gfe._1004ispaidtobroker, gfe._1004paidby, gfe._1004pocSpecified, gfe._1004poc);

            if (null != gfe._1005nummonths) // opm 180983
            {
                _pageData.sSchoolTxRsrvMonLckd = true;
            }
            _pageData.sSchoolTxRsrvMon_rep = gfe._1005nummonths ?? "";
            _pageData.sProSchoolTx_rep = gfe._1005amountpermonth ?? "";
            _pageData.sSchoolTxRsrvProps = GetGFEDetails(gfe._1005isaprSpecified, gfe._1005isapr, gfe._1005ispaidtobrokerSpecified, gfe._1005ispaidtobroker, gfe._1005paidby, gfe._1005pocSpecified, gfe._1005poc);

            if (null != gfe._1006nummonths) // opm 180983
            {
                _pageData.sFloodInsRsrvMonLckd = true;
            }
            _pageData.sFloodInsRsrvMon_rep = gfe._1006nummonths ?? "";
            _pageData.sProFloodIns_rep = gfe._1006amountpermonth ?? "";
            _pageData.sFloodInsRsrvProps = GetGFEDetails(gfe._1006isaprSpecified, gfe._1006isapr, gfe._1006ispaidtobrokerSpecified, gfe._1006ispaidtobroker, gfe._1006paidby, gfe._1006pocSpecified, gfe._1006poc);


            _pageData.sAggregateAdjRsrv_rep = gfe._1007amount ?? "";
            //opm 45122
            //_pageData.sAggregateAdjRsrvLckd = IsNonZeroDecimal(gfe._1008amount);
            _pageData.sAggregateAdjRsrvLckd = true;
            _pageData.sAggregateAdjRsrvProps = GetGFEDetails(gfe._1007isaprSpecified, gfe._1007isapr, gfe._1007ispaidtobrokerSpecified, gfe._1007ispaidtobroker, gfe._1007paidby, gfe._1007pocSpecified, gfe._1007poc);


            // _pageData.s1006ProHExpDesc = gfe._1008description ?? "";   not in the CLF yet 
            // _pageData.s1006RsrvMon_rep = gfe._1008nummonths ?? "";              
            // _pageData.s1006ProHExp_rep = gfe._1008amountpermonth ?? "";
            //_pageData.s1006RsrvProps = GetGFEDetails(gfe._1008isapr, gfe._1008ispaidtobroker, gfe._1008paidby, gfe._1008poc);


            //_pageData.s1007ProHExpDesc = gfe._1009description ?? "";
            //_pageData.s1007RsrvMon_rep = gfe._1009nummonths ?? ""; // if add this back, use lock field
            //_pageData.s1007ProHExp_rep = gfe._1009amountpermonth ?? "";
            //_pageData.s1007RsrvProps = GetGFEDetails(gfe._1009isapr, gfe._1009ispaidtobroker, gfe._1009paidby, gfe._1009poc);

            #endregion

            #region 1100

            //do not import 1101 

            _pageData.sEscrowF_rep = gfe._1102amount ?? "";
            _pageData.sEscrowFProps = GetGFEDetails(gfe._1102isaprSpecified, gfe._1102isapr, gfe._1102ispaidtobrokerSpecified, gfe._1102ispaidtobroker, gfe._1102paidby, gfe._1102pocSpecified, gfe._1102poc);

            _pageData.sOwnerTitleInsF_rep = gfe._1103amount ?? "";
            _pageData.sOwnerTitleInsProps = GetGFEDetails(gfe._1103isaprSpecified, gfe._1103isapr, gfe._1103ispaidtobrokerSpecified, gfe._1103ispaidtobroker, gfe._1103paidby, gfe._1103pocSpecified, gfe._1103poc);

            _pageData.sTitleInsF_rep = gfe._1104amount ?? "";
            _pageData.sTitleInsFTable = gfe._1104description ?? "";
            _pageData.sTitleInsFProps = GetGFEDetails(gfe._1104isaprSpecified, gfe._1104isapr, gfe._1104ispaidtobrokerSpecified, gfe._1104ispaidtobroker, gfe._1104paidby, gfe._1104pocSpecified, gfe._1104poc);

            _pageData.sDocPrepF_rep = gfe._1109amount ?? "";
            _pageData.sDocPrepFProps = GetGFEDetails(gfe._1109isaprSpecified, gfe._1109isapr, gfe._1109ispaidtobrokerSpecified, gfe._1109ispaidtobroker, gfe._1109paidby, gfe._1109pocSpecified, gfe._1109poc);

            _pageData.sNotaryF_rep = gfe._1110amount ?? "";
            _pageData.sNotaryFProps = GetGFEDetails(gfe._1110isaprSpecified, gfe._1110isapr, gfe._1110ispaidtobrokerSpecified, gfe._1110ispaidtobroker, gfe._1110paidby, gfe._1110pocSpecified, gfe._1110poc);

            _pageData.sAttorneyF_rep = gfe._1111amount ?? "";
            _pageData.sAttorneyFProps = GetGFEDetails(gfe._1111isaprSpecified, gfe._1111isapr, gfe._1111ispaidtobrokerSpecified, gfe._1111ispaidtobroker, gfe._1111paidby, gfe._1111pocSpecified, gfe._1111poc);


            _pageData.sU1TcCode = gfe._1100code1 ?? "";
            _pageData.sU1Tc_rep = gfe._1100amount1 ?? "";
            _pageData.sU1TcDesc = gfe._1100description1 ?? "";
            _pageData.sU1TcProps = GetGFEDetails(gfe._1100isapr1Specified, gfe._1100isapr1, gfe._1100ispaidtobroker1Specified, gfe._1100ispaidtobroker1, gfe._1100paidby1, gfe._1100poc1Specified, gfe._1100poc1);


            _pageData.sU2TcCode = gfe._1100code2 ?? "";
            _pageData.sU2Tc_rep = gfe._1100amount2 ?? "";
            _pageData.sU2TcDesc = gfe._1100description2 ?? "";
            _pageData.sU2TcProps = GetGFEDetails(gfe._1100isapr2Specified, gfe._1100isapr2, gfe._1100ispaidtobroker2Specified, gfe._1100ispaidtobroker2, gfe._1100paidby2, gfe._1100poc2Specified, gfe._1100poc2);


            _pageData.sU3TcCode = gfe._1100code3 ?? "";
            _pageData.sU3Tc_rep = gfe._1100amount3 ?? "";
            _pageData.sU3TcDesc = gfe._1100description3 ?? "";
            _pageData.sU3TcProps = GetGFEDetails(gfe._1100isapr3Specified, gfe._1100isapr3, gfe._1100ispaidtobroker3Specified, gfe._1100ispaidtobroker3, gfe._1100paidby3, gfe._1100poc3Specified, gfe._1100poc3);


            _pageData.sU4TcCode = gfe._1100code4 ?? "";
            _pageData.sU4Tc_rep = gfe._1100amount4 ?? "";
            _pageData.sU4TcDesc = gfe._1100description4 ?? "";
            _pageData.sU4TcProps = GetGFEDetails(gfe._1100isapr4Specified, gfe._1100isapr4, gfe._1100ispaidtobroker4Specified, gfe._1100ispaidtobroker4, gfe._1100paidby4, gfe._1100poc4Specified, gfe._1100poc4);


            #endregion

            #region 1200
            //_pageData.sRecFPc_rep   = gfe._1202percentagefee ?? "";
            //_pageData.sRecBaseT     = (E_PercentBaseT)MapTosProHazInsT(gfe._1202percentageof);
            _pageData.sRecFMb_rep = gfe._1202amount;                                        //thew new gfe only has these fields
            _pageData.sRecFPc_rep = "0";
            _pageData.sRecFProps = GetGFEDetails(gfe._1202isaprSpecified, gfe._1202isapr, gfe._1202ispaidtobrokerSpecified, gfe._1202ispaidtobroker, gfe._1202paidby, gfe._1202pocSpecified, gfe._1202poc);

            _pageData.sCountyRtcPc_rep = gfe._1204deedpercentagefee ?? "";
            _pageData.sCountyRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1204deedpercentageof);
            _pageData.sCountyRtcMb_rep = gfe._1204flatfee;
            _pageData.sCountyRtcProps = GetGFEDetails(gfe._1204isaprSpecified, gfe._1204isapr, gfe._1204ispaidtobrokerSpecified, gfe._1204ispaidtobroker, gfe._1204paidby, gfe._1204pocSpecified, gfe._1204poc);

            _pageData.sStateRtcPc_rep = gfe._1205deedpercentagefee ?? "";
            _pageData.sStateRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1205deedpercentageof);
            _pageData.sStateRtcMb_rep = gfe._1205flatfee;
            _pageData.sStateRtcProps = GetGFEDetails(gfe._1205isaprSpecified, gfe._1205isapr, gfe._1205ispaidtobrokerSpecified, gfe._1205ispaidtobroker, gfe._1205paidby, gfe._1205pocSpecified, gfe._1205poc);

            _pageData.sU1GovRtcDesc = gfe._1206description ?? "";
            _pageData.sU1GovRtcPc_rep = gfe._1206percentagefee ?? "";
            _pageData.sU1GovRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1206percentageof);
            _pageData.sU1GovRtcMb_rep = gfe._1206flatfee ?? "";
            _pageData.sU1GovRtcProps = GetGFEDetails(gfe._1206isaprSpecified, gfe._1206isapr, gfe._1206ispaidtobrokerSpecified, gfe._1206ispaidtobroker, gfe._1206paidby, gfe._1206pocSpecified, gfe._1206poc);


            _pageData.sU2GovRtcDesc = gfe._1207description ?? "";
            _pageData.sU2GovRtcPc_rep = gfe._1207percentagefee ?? "";
            _pageData.sU2GovRtcBaseT = (E_PercentBaseT)MapTosProHazInsT(gfe._1207percentageof);
            _pageData.sU2GovRtcMb_rep = gfe._1207flatfee ?? "";
            _pageData.sU2GovRtcProps = GetGFEDetails(gfe._1207isaprSpecified, gfe._1207isapr, gfe._1207ispaidtobrokerSpecified, gfe._1207ispaidtobroker, gfe._1207paidby, gfe._1207pocSpecified, gfe._1207poc);


            #endregion

            #region 1300

            _pageData.sPestInspectF_rep = gfe._1302amount ?? "";
            _pageData.sPestInspectFProps = GetGFEDetails(gfe._1302isaprSpecified, gfe._1302isapr, gfe._1302ispaidtobrokerSpecified, gfe._1302ispaidtobroker, gfe._1302paidby, gfe._1302pocSpecified, gfe._1302poc);

            _pageData.sU1ScDesc = gfe._1303description ?? "";
            _pageData.sU1Sc_rep = gfe._1303amount ?? "";
            _pageData.sU1ScProps = GetGFEDetails(gfe._1303isaprSpecified, gfe._1303isapr, gfe._1303ispaidtobrokerSpecified, gfe._1303ispaidtobroker, gfe._1303paidby, gfe._1303pocSpecified, gfe._1303poc);

            _pageData.sU2ScDesc = gfe._1304description ?? "";
            _pageData.sU2Sc_rep = gfe._1304amount ?? "";
            _pageData.sU2ScProps = GetGFEDetails(gfe._1304isaprSpecified, gfe._1304isapr, gfe._1304ispaidtobrokerSpecified, gfe._1304ispaidtobroker, gfe._1304paidby, gfe._1304pocSpecified, gfe._1304poc);

            _pageData.sU3ScDesc = gfe._1305description ?? "";
            _pageData.sU3Sc_rep = gfe._1305amount ?? "";
            _pageData.sU3ScProps = GetGFEDetails(gfe._1305isaprSpecified, gfe._1305isapr, gfe._1305ispaidtobrokerSpecified, gfe._1305ispaidtobroker, gfe._1305paidby, gfe._1305pocSpecified, gfe._1305poc);

            _pageData.sU4ScCode = gfe._1300code1 ?? "";
            _pageData.sU4ScDesc = gfe._1300description1 ?? "";
            _pageData.sU4Sc_rep = gfe._1300amount1 ?? "";
            _pageData.sU4ScProps = GetGFEDetails(gfe._1300isapr1Specified, gfe._1300isapr1, gfe._1300ispaidtobroker1Specified, gfe._1300ispaidtobroker1, gfe._1300paidby1, gfe._1300poc1Specified, gfe._1300poc1);

            _pageData.sU5ScCode = gfe._1300code2 ?? "";
            _pageData.sU5ScDesc = gfe._1300description2 ?? "";
            _pageData.sU5Sc_rep = gfe._1300amount2 ?? "";
            _pageData.sU5ScProps = GetGFEDetails(gfe._1300isapr2Specified, gfe._1300isapr2, gfe._1300ispaidtobroker2Specified, gfe._1300ispaidtobroker2, gfe._1300paidby2, gfe._1300poc2Specified, gfe._1300poc2);


            #endregion

            _pageData.sBrokComp1Desc = gfe._compensationtobrokerdescription1 ?? "";
            _pageData.sBrokComp1_rep = gfe._compensationtobrokeramount1 ?? "";
            _pageData.sBrokComp1Pc_rep = "";
            _pageData.sBrokComp1Lckd = !string.IsNullOrEmpty(_pageData.sBrokComp1Pc_rep);

            _pageData.sBrokComp2Desc = gfe._compensationtobrokerdescription2 ?? "";
            _pageData.sBrokComp2_rep = gfe._compensationtobrokeramount2 ?? "";

        }

        private void EnsureFieldCanBeUpdated(string newValue, string existingValue, string errorMessage)
        {
            decimal convertedValue;
            decimal existingConvertedValue;
            if (Decimal.TryParse(existingValue, out existingConvertedValue) && Decimal.TryParse(newValue, out convertedValue)
                && convertedValue != existingConvertedValue && convertedValue != 0)
            {
                throw new InvalidOperationException(errorMessage);
            }
        }

        private string GetVestingName(MORTGAGE_LOANLOAN_INFO loanInfo, int num)
        {
            if (loanInfo.VESTING != null && loanInfo.VESTING[num] != null && !string.IsNullOrEmpty(loanInfo.VESTING[num].vesting_name))
            {
                return loanInfo.VESTING[num].vesting_name;
            }
            return "";
        }

        private void ImportFreddieMac(MORTGAGE_FREDDIE_MAC freData)
        {
            freData = freData ?? new MORTGAGE_FREDDIE_MAC();
            _pageData.sFreddieTransactionId = freData.fre_transaction_id ?? "";
            _pageData.sFredieReservesAmt_rep = freData.fre_reserve_amount ?? "";
        }

        private void ImportGovermentData(MORTGAGE_GOVERNMENT_DATA governmentData)
        {
            governmentData = governmentData ?? new MORTGAGE_GOVERNMENT_DATA();

            _pageData.sAgencyCaseNum = governmentData.agency_case_id ?? "";
            _pageData.sFHALenderIdCode = governmentData.FHA_lender_id ?? "";
            _pageData.sFHASponsorAgentIdCode = governmentData.FHA_sponsor_id ?? "";
            _pageData.sFHARefinanceTypeDesc = governmentData.type_of_refinance ?? "";
            _pageData.sFHAHousingActSection = governmentData.FNMA_section_of_the_act ?? "";
            _pageData.sFHASalesConcessions_rep = governmentData.sales_concession_amount ?? "";
            _pageData.sVaProUtilityPmt_rep = governmentData.VA_monthly_utilities ?? "";
        }

        private void ImportLoanProductData(MORTGAGE_LOAN_INFOLOAN_PRODUCT_DATA loanProductData, bool isHeloc)
        {
            loanProductData = loanProductData ?? new MORTGAGE_LOAN_INFOLOAN_PRODUCT_DATA();

            _pageData.sRAdj1stCapR_rep = loanProductData.arm_rate_first_period_cap ?? "";
            _pageData.sRAdj1stCapMon_rep = loanProductData.arm_rate_first_adjust_months ?? "";
            _pageData.sRAdjCapMon_rep = loanProductData.arm_rate_subsequent_adjust_months ?? "";
            _pageData.sRAdjCapR_rep = loanProductData.arm_rate_subsequent_periodic_cap ?? "";

            _pageData.sDue_rep = loanProductData.loan_term_due_months ?? "";
            if (isHeloc)
            {
                _pageData.sHelocDraw_rep = loanProductData.loan_term_due_months ?? string.Empty;
            }

            _pageData.sArmIndexNameVstr = loanProductData.index_type;
            _pageData.sBuydown = loanProductData.buydown_is_tempSpecified ? loanProductData.buydown_is_temp == BASE_APPLICANTDeclined_answer_race_gender.Y : false;
            _pageData.sBuydownContributorT = MapBuydownSourceToLO(loanProductData.buydown_source);
            _pageData.sBuydwnR1_rep = loanProductData.buydown_first_rate_adjust_percentSpecified ? loanProductData.buydown_first_rate_adjust_percent : "";
            _pageData.sSpProjClassT = MapToLOProjClass(loanProductData.gse_project_class_type);
            _pageData.sFredAffordProgId = loanProductData.fre_affordable_program_id ?? "";
        }

        private void ImportPropertyInfo(MORTGAGE_PROPERTY_INFO propertyInfo)
        {
            propertyInfo = propertyInfo ?? new MORTGAGE_PROPERTY_INFO();

            _pageData.sSpAddr = propertyInfo.address ?? "";
            _pageData.sSpCounty = propertyInfo.county ?? "";
            _pageData.sSpCity = propertyInfo.city ?? "";
            _pageData.sSpZip = propertyInfo.zip ?? "";
            _pageData.sSpState = propertyInfo.state ?? "";
            _pageData.sYrBuilt = propertyInfo.built_year ?? "";


            if (_pageData.sIsConstructionLoan)
            {
                _pageData.sLotOrigC_rep = propertyInfo.property_lot_original_cost ?? "";
                _pageData.sLotImprovC_rep = propertyInfo.property_lot_improvement_costs ?? "";
                _pageData.sLotVal_rep = propertyInfo.lot_present_value ?? "";
                _pageData.sLotAcqYr = propertyInfo.property_lot_year_acquired ?? "";
            }
            else if (_pageData.sIsRefinancing)
            {
                _pageData.sSpOrigC_rep = propertyInfo.property_lot_original_cost ?? "";
                _pageData.sSpImprovC_rep = propertyInfo.property_lot_improvement_costs ?? "";
                _pageData.sSpAcqYr = propertyInfo.property_lot_year_acquired ?? "";
            }

            _pageData.sSpImprovDesc = propertyInfo.property_lot_improvement_description ?? "";
            _pageData.sSpImprovTimeFrameT = propertyInfo.is_improvement_to_be_madeSpecified ? MapToLOImprovToBeMade(propertyInfo.is_improvement_to_be_made) : E_sSpImprovTimeFrameT.LeaveBlank;
            _pageData.sBuildingStatusT = MapToLOBuildingStatus(propertyInfo.building_status);
            _pageData.sHmdaCountyCode = propertyInfo.hmda_county_code ?? "";
            _pageData.sHmdaMsaNum = propertyInfo.hmda_msa_number ?? "";
            _pageData.sHmdaStateCode = propertyInfo.hmda_state_code ?? "";
            _pageData.sHmdaReportAsHoepaLoan = propertyInfo.is_hoepaSpecified ? propertyInfo.is_hoepa == BASE_APPLICANTDeclined_answer_race_gender.Y : false;
            _pageData.sHmdaCensusTract = propertyInfo.census_tract_number ?? "";

            if (_pageData.nApps > 0)
            {
                if (propertyInfo.occupancy_statusSpecified)
                {
                    _pageData.GetAppData(0).aOccT = MapToLO(propertyInfo.occupancy_status);
                }
                _pageData.GetAppData(0).aManner = propertyInfo.manner_title_held;
            }

            _pageData.sAssessorsParcelId = propertyInfo.tax_id;
            _pageData.sProjNm = propertyInfo.community_name;
            _pageData.sHmdaMultifamilyUnits_rep = propertyInfo.multifamily_affordable_units;
            _pageData.sHmdaBusinessPurposeT = ToHmdaBusinessPurposeT(propertyInfo.business_commercial_purpose);
            _pageData.sHmdaManufacturedInterestT = ToHmdaManufacturedInterestT(propertyInfo.manf_home_land_property_interest);
        }

        private sHmdaBusinessPurposeT ToHmdaBusinessPurposeT(string businessCommercialPurpose)
        {
            var text = businessCommercialPurpose?.Trim()?.ToUpper();
            if (string.IsNullOrEmpty(text))
            {
                return sHmdaBusinessPurposeT.LeaveBlank;
            }

            switch (text)
            {
                case "N":
                    return sHmdaBusinessPurposeT.NotPrimarilyForBusinessOrCommercial;
                case "P":
                    return sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial;
                default:
                    return sHmdaBusinessPurposeT.LeaveBlank;
            }
        }

        private sHmdaManufacturedInterestT ToHmdaManufacturedInterestT(string manfHomeLandPropertyInterest)
        {
            var text = manfHomeLandPropertyInterest?.Trim()?.ToUpper();
            if (string.IsNullOrEmpty(text))
            {
                return sHmdaManufacturedInterestT.LeaveBlank;
            }

            switch (text)
            {
                case "D":
                    return sHmdaManufacturedInterestT.DirectOwnership;
                case "I":
                    return sHmdaManufacturedInterestT.IndirectOwnership;
                case "P":
                    return sHmdaManufacturedInterestT.PaidLeasehold;
                case "U":
                    return sHmdaManufacturedInterestT.UnpaidLeasehold;
                default:
                    return sHmdaManufacturedInterestT.LeaveBlank;
            }
        }

        private void ImportApplicants(MORTGAGE_APPLICANT[] applicants)
        {
            if (applicants == null)
            {
                return;
            }

            for (int i = 0; i < applicants.Length; i++)
            {
                CAppData appData = _pageData.GetAppData(i);
                ImportApplicant(applicants[i], appData);
            }

            if (this._pageData.sIsIncomeCollectionEnabled)
            {
                this._pageData.ImportGrossRent(this.hasSubjectPropertyNetCashFlow, this.totalSubjectPropertyNetCashFlow);
            }
        }

        /// <summary>
        /// Initializes the apps. Done separately from the rest of the import due to loan state manipulation.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="numApplicantsToImport">The number of apps to import.</param>
        private void InitializeApplications(Guid loanId, int numApplicantsToImport)
        {
            if (numApplicantsToImport < 1)
            {
                return;
            }

            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(CLFImporter));
            loanData.ByPassFieldSecurityCheck = true;
            loanData.SetFormatTarget(FormatTarget.MismoClosing);
            loanData.InitLoad();

            int nApps = loanData.nApps;

            // This block handles the scenrio where LPQ's data has less apps than the loan we're importing into.
            // We'll delete apps until we get to the same number.
            while (nApps > numApplicantsToImport)
            {
                if (loanData.DelApp(nApps - 1))
                {
                    // Deleting an app puts the loan state back to normal (since it acts like a save).
                    // We'll want to initsave again to delete the next app since deletion requires a non-normal state.
                    nApps--;
                    loanData.InitLoad();
                }
            }

            // At this point, nApps <= numApplicantsToImport. 
            // The following block will add apps in the scenerio that we have less apps than what LPQ sent us.
            var numAppsToCreate = numApplicantsToImport - loanData.nApps;
            if (numAppsToCreate > 0)
            {
                // Adding a new app requires the loan to be in a normal state.
                loanData.DoneLoad();

                for (int i = 0; i < numAppsToCreate; i++)
                {
                    loanData.AddNewApp();
                }
            }
        }

        private void ImportApplicant(MORTGAGE_APPLICANT applicant, CAppData appData)
        {
            appData.aAssetCollection.ClearAll();
            appData.aReCollection.ClearAll();  //delete all existing assets and reos cause LPQ overwrites fully.
            appData.aLiaCollection.ClearAll();

            if (!_pageData.sIsIncomeCollectionEnabled)
            {
                appData.aOtherIncomeList = new List<OtherIncome>();
            }
            else
            {
                _pageData.ClearIncomeSources();
            }

            if (applicant.CREDIT_REPORT != null)
            {
                ImportCreditReport(applicant.CREDIT_REPORT, appData.aAppId);
            }

            ImportMortgageApplicant(E_BorrowerModeT.Borrower, applicant, appData);

            ImportPresentHousingExpense(applicant.PRESENT_HOUSING_EXPENSE, appData);

            if (applicant.SPOUSE != null)
            {
                ImportMortgageApplicant(E_BorrowerModeT.Coborrower, applicant.SPOUSE, appData);
            }
            else
            {
                appData.DelMarriedCobor();
            }

        }

        private void ImportCreditReport(MORTGAGE_APPLICANTCREDIT_REPORT creditReport, Guid appID)
        {

            if (creditReport.report_format != MORTGAGE_APPLICANTCREDIT_REPORTReport_format.MCL_COMMON)
            {
                throw new InvalidDataException(creditReport.report_format.ToString() + " is not a supported credit report format.");
            }

            string partialCreditReportXml = creditReport.encoding == MORTGAGE_APPLICANTCREDIT_REPORTEncoding.BASE64 ? DecodeBase64String(creditReport.Value) : creditReport.Value;
            Tools.LogInfo(partialCreditReportXml);

            string creditReportXml = "<OUTPUT ><RESPONSE><ORDER_DETAIL status_code=\"READY\" report_id=\"" + creditReport.report_id + "\" /><OUTPUT_FORMAT format_type=\"XML\" ><![CDATA[" + partialCreditReportXml + "]]></OUTPUT_FORMAT><OUTPUT_FORMAT format_type=\"HTML\" ><![CDATA[There is no viewable credit report.]]></OUTPUT_FORMAT></RESPONSE></OUTPUT>";
            XmlDocument report = Tools.CreateXmlDoc(creditReportXml);
            ICreditReportResponse response = new MclCreditReportResponse(report);

            _reportsToImport.Add(appID, response);
        }

        /// <summary>
        /// Import the pressent housing expense information. If it is not specified it is set to 0.
        /// </summary>
        /// <param name="presentHousingExpense"></param>
        /// <param name="appData"></param>
        private void ImportPresentHousingExpense(MORTGAGE_APPLICANTPRESENT_HOUSING_EXPENSE presentHousingExpense, CAppData appData)
        {
            presentHousingExpense = presentHousingExpense ?? new MORTGAGE_APPLICANTPRESENT_HOUSING_EXPENSE();
            appData.aPresHazIns_rep = presentHousingExpense.hazard_insurance ?? "0";
            appData.aPresMIns_rep = presentHousingExpense.mortgage_insurance ?? "0";
            appData.aPresHoAssocDues_rep = presentHousingExpense.hoa_due ?? "0";
            appData.aPres1stM_rep = presentHousingExpense.monthly_payment ?? "0";
            appData.aPresOFin_rep = presentHousingExpense.other_pi ?? "0";
            appData.aPresOHExp_rep = presentHousingExpense.other_expense ?? "0";
            appData.aPresRealETx_rep = presentHousingExpense.real_estate_tax ?? "0";
        }

        private void ImportRace(CAppData appData, E_BorrowerModeT mode, base_race_type_single[] races)
        {
            bool isBlack = false, isAsian = false, isPacific = false, isWhite = false, isIndian = false;
            if (races != null)
            {
                foreach (base_race_type_single race in races)
                {
                    switch (race)
                    {
                        case base_race_type_single.AMERICAN_INDIAN:
                            isIndian = true;
                            break;
                        case base_race_type_single.ASIAN:
                            isAsian = true;
                            break;
                        case base_race_type_single.BLACK:
                            isBlack = true;
                            break;
                        case base_race_type_single.PACIFIC_ISLANDER:
                            isPacific = true;
                            break;
                        case base_race_type_single.WHITE:
                            isWhite = true;
                            break;
                        default:
                            throw new ArgumentException("Unhandled race: " + race);
                    }
                }
            }

            //setting these values above will not work because  we also need to clear them if they are not in the array
            appData.aIsBlack = isBlack;
            appData.aIsAsian = isAsian;
            appData.aIsPacificIslander = isPacific;
            appData.aIsWhite = isWhite;
            appData.aIsAmericanIndian = isIndian;
        }

        private void ImportCurrentAddress(MORTGAGE_BASE_APPLICANTCURRENT_ADDRESS currentAddress, CAppData appData)
        {
            if (currentAddress == null)
            {
                return;
            }

            if (currentAddress.Item is BASE_ADDRESS_LOOSE)
            {
                BASE_ADDRESS_LOOSE looseAddress = currentAddress.Item as BASE_ADDRESS_LOOSE;
                if (string.IsNullOrEmpty(looseAddress.country) || looseAddress.country.Equals("USA", StringComparison.OrdinalIgnoreCase))
                {
                    // For CURRENT_ADDRESS, a BASE_ADDRESS_LOOSE is used when a domestic address is defined. So it's safe to assume a US address if country isn't defined.
                    appData.aAddr = $"{looseAddress.street_address_1 ?? string.Empty} {looseAddress.street_address_2 ?? string.Empty}";
                    appData.aCity = looseAddress.city ?? "";
                    appData.aZip = looseAddress.zip ?? "";
                    appData.aState = looseAddress.state ?? "";
                }
            }
            else if (currentAddress.Item is BASE_ADDRESS_STRICT)
            {
                // I don't think we're going to get this since LPQ says not to use it in their schema. So I'm not going worry too hard about it.
                BASE_ADDRESS_STRICT strictAddress = currentAddress.Item as BASE_ADDRESS_STRICT;
                if (strictAddress.county != null && strictAddress.county.Equals("USA", StringComparison.OrdinalIgnoreCase))
                {
                    string addr = strictAddress.street_no ?? "" + " " +
                                        strictAddress.street_direction ?? "" + " " +
                                        strictAddress.street_name ?? "" + " " +
                                        strictAddress.street_type ?? "" + " ";
                    appData.aAddr = addr.TrimWhitespaceAndBOM();
                    appData.aCity = strictAddress.city ?? "";
                    appData.aZip = strictAddress.zip ?? "";
                    appData.aState = strictAddress.state ?? string.Empty;
                }
            }
            else if (currentAddress.Item is BASE_ADDRESS_THREE_LINE)
            {
                BASE_ADDRESS_THREE_LINE threeLineAddress = currentAddress.Item as BASE_ADDRESS_THREE_LINE;
                if (threeLineAddress.country != null && threeLineAddress.country.Equals("USA", StringComparison.OrdinalIgnoreCase))
                {
                    // THREE_LINE is used when defining a foreign address, so we want to check for country.
                    appData.aAddr = $"{threeLineAddress.street_address_1 ?? string.Empty} {threeLineAddress.street_address_2 ?? string.Empty} {threeLineAddress.street_address_3 ?? string.Empty}";
                    appData.aAddr = threeLineAddress.street_address_1 ?? string.Empty + threeLineAddress;
                    appData.aCity = threeLineAddress.city;
                    appData.aZip = threeLineAddress.zip;
                }
            }

            appData.aAddrT = currentAddress.occupancy_statusSpecified ? ToLO(currentAddress.occupancy_status) : E_aBAddrT.LeaveBlank;
            appData.aAddrTotalMonths = currentAddress.occupancy_duration ?? "";
        }

        private void ImportPreviousAddress(MORTGAGE_BASE_APPLICANTPREVIOUS_ADDRESS previousAddress, CAppData appData)
        {
            if (previousAddress == null)
            {
                return;
            }

            appData.aPrev1Addr = previousAddress.street_address_1 ?? "";
            if (previousAddress.street_address_2 != null)
            {
                appData.aPrev1Addr += previousAddress.street_address_2;
            }

            appData.aPrev1Zip = previousAddress.zip ?? "";
            appData.aPrev1City = previousAddress.city ?? "";
            appData.aPrev1State = previousAddress.state ?? "";
            appData.aPrev1AddrTotalMonths = previousAddress.occupancy_duration ?? "";
            appData.aPrev1AddrT = previousAddress.occupancy_statusSpecified ? (E_aBPrev1AddrT)ToLO(previousAddress.occupancy_status) : E_aBPrev1AddrT.LeaveBlank;
        }

        private void ImportContactInfo(BASE_CONTACT_INFO contactInfo, CAppData appData)
        {
            if (contactInfo == null)
            {
                return;
            }

            appData.aCellPhone = contactInfo.cell_phone ?? "";
            appData.aEmail = contactInfo.email ?? "";
            appData.aFax = contactInfo.fax_number ?? "";
            appData.aHPhone = contactInfo.home_phone ?? "";
            appData.aBusPhone = contactInfo.work_phone ?? "";
            if (!string.IsNullOrEmpty(contactInfo.work_phone_extension))
            {
                appData.aBusPhone += "e" + contactInfo.work_phone_extension;
            }
        }

        private void ImportMailingAddress(MORTGAGE_BASE_APPLICANTMAILING_ADDRESS mailingAddress, CAppData appData)
        {
            if (mailingAddress == null)
            {
                return;
            }

            appData.aAddrMailUsePresentAddr = mailingAddress.is_current == BASE_APPLICANTDeclined_answer_race_gender.Y;
            appData.aAddrMail = mailingAddress.street_address_1 ?? "";
            if (!string.IsNullOrEmpty(mailingAddress.street_address_2))
                appData.aAddrMail += " " + mailingAddress.street_address_2;
            appData.aCityMail = mailingAddress.city ?? "";
            appData.aZipMail = mailingAddress.zip ?? "";
            appData.aStateMail = mailingAddress.state ?? "";
            appData.aZipMail = mailingAddress.zip ?? "";
        }

        private void ImportMortgageApplicant(E_BorrowerModeT e_BorrowerModeT, MORTGAGE_BASE_APPLICANT baseApplicant, CAppData appData)
        {
            appData.BorrowerModeT = e_BorrowerModeT;

            #region BASE_APPLICANT
            appData.aFirstNm = baseApplicant.first_name ?? "";
            appData.aLastNm = baseApplicant.last_name ?? "";
            appData.aMidNm = baseApplicant.middle_name ?? "";
            appData.aSuffix = baseApplicant.suffixSpecified ? ToLO(baseApplicant.suffix) : "";
            appData.aSsn = baseApplicant.ssn ?? "";
            appData.aDob_rep = baseApplicant.dob ?? "";
            appData.aDependNum_rep = baseApplicant.dependents ?? "";
            appData.aDependAges = baseApplicant.ages_of_dependents ?? "";
            appData.aSchoolTotalMonths_rep = baseApplicant.school_months ?? "";
            appData.aInterviewMethodT = MapToLO(baseApplicant.interview_method);

            #region citizenship
            if (baseApplicant.citizenshipSpecified)
            {
                switch (baseApplicant.citizenship)
                {
                    case BASE_APPLICANTCitizenship.USCITIZEN:
                        appData.aDecCitizen = "Y";
                        appData.aDecResidency = "N";
                        break;
                    case BASE_APPLICANTCitizenship.PERMRESIDENT:
                        appData.aDecResidency = "Y";
                        appData.aDecCitizen = "N";
                        break;
                    case BASE_APPLICANTCitizenship.NONPERMRESIDENT:
                        appData.aDecResidency = "N";
                        appData.aDecCitizen = "N";
                        break;
                    case BASE_APPLICANTCitizenship.NONRESIDENTALIEN:
                        appData.aDecResidency = "N";
                        appData.aDecCitizen = "N";
                        break;
                    case BASE_APPLICANTCitizenship.Item:
                        appData.aDecResidency = "";
                        appData.aDecCitizen = "";
                        break;
                    default:
                        throw new ArgumentException("Unhandled citizenship " + baseApplicant.citizenship.ToString());
                }
            }
            else
            {
                appData.aDecResidency = "";
                appData.aDecCitizen = "";
            }
            #endregion

            appData.aMaritalStatT = baseApplicant.marital_statusSpecified ? ToLO(baseApplicant.marital_status) : E_aBMaritalStatT.LeaveBlank;
            appData.aGender = baseApplicant.genderSpecified ? ToLO(baseApplicant.gender) : E_GenderT.LeaveBlank;
            appData.aHispanicT = baseApplicant.ethnicitySpecified ? ToLO(baseApplicant.ethnicity) : E_aHispanicT.LeaveBlank;

            ImportRace(appData, e_BorrowerModeT, baseApplicant.race);

            appData.aNoFurnish = baseApplicant.declined_answer_race_genderSpecified ? baseApplicant.declined_answer_race_gender == BASE_APPLICANTDeclined_answer_race_gender.Y : false;

            if (e_BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                appData.aBCoreSystemId = baseApplicant.member_number;
            }
            else
            {
                appData.aCCoreSystemId = baseApplicant.member_number;
            }

            #endregion

            ImportCurrentAddress(baseApplicant.CURRENT_ADDRESS, appData);
            ImportPreviousAddress(baseApplicant.PREVIOUS_ADDRESS, appData);
            ImportFinancialInfo(baseApplicant.FINANCIAL_INFO, appData);
            ImportContactInfo(baseApplicant.CONTACT_INFO, appData);
            ImportAssets(baseApplicant.ASSETS?.Where(p => p.asset_type != BASE_ASSETAsset_type.REALESTATE), appData);
            ImportReos(baseApplicant.ASSETS?.Where(p => p.asset_type == BASE_ASSETAsset_type.REALESTATE), appData);
            ImportDeclarations(baseApplicant.DECLARATION, appData);
            ImportMailingAddress(baseApplicant.MAILING_ADDRESS, appData);
        }


        private void ImportDeclarations(MORTGAGE_DECLARATION declaration, CAppData appData)
        {
            declaration = declaration ?? new MORTGAGE_DECLARATION();

            appData.aDecJudgment = declaration.has_outstanding_judgementSpecified ? MapToLO(declaration.has_outstanding_judgement) : "";
            appData.aDecBankrupt = declaration.has_bankruptcySpecified ? MapToLO(declaration.has_bankruptcy) : "";
            appData.aDecLawsuit = declaration.is_in_lawsuitSpecified ? MapToLO(declaration.is_in_lawsuit) : "";
            appData.aDecForeclosure = declaration.has_bad_loanSpecified ? MapToLO(declaration.has_bad_loan) : "";
            appData.aDecObligated = declaration.is_related_to_bad_loanSpecified ? MapToLO(declaration.is_related_to_bad_loan) : "";
            appData.aDecPastOwnership = declaration.has_owner_ship_interestSpecified ? MapToLO(declaration.has_owner_ship_interest) : "";
            appData.aDecAlimony = declaration.has_separate_maintenanceSpecified ? MapToLO(declaration.has_separate_maintenance) : "";
            appData.aDecBorrowing = declaration.is_down_payment_borrowedSpecified ? MapToLO(declaration.is_down_payment_borrowed) : "";
            appData.aDecEndorser = declaration.is_endorser_on_noteSpecified ? MapToLO(declaration.is_endorser_on_note) : "";
            appData.aDecDelinquent = declaration.is_presently_delinquentSpecified ? MapToLO(declaration.is_presently_delinquent) : "";
            appData.aDecOcc = declaration.is_property_primary_residenceSpecified ? MapToLO(declaration.is_property_primary_residence) : "";
            appData.aDecPastOwnedPropTitleT = declaration.title_hold_typeSpecified ? MapToLO(declaration.title_hold_type) : E_aBDecPastOwnedPropTitleT.Empty;
            appData.aDecPastOwnedPropT = declaration.type_of_property_ownedSpecified ? MapToLO(declaration.type_of_property_owned) : E_aBDecPastOwnedPropT.Empty;

        }

        private void ImportReos(IEnumerable<BASE_ASSET> assets, CAppData appData)
        {
            if (assets == null)
            {
                return;
            }

            var reos = appData.aReCollection;

            //ASUMING ASSETS belogin to the borrower mode setting 
            foreach (BASE_ASSET asset in assets)
            {
                if (asset is BASE_ASSET_REAL_ESTATE)
                {
                    BASE_ASSET_REAL_ESTATE reoAsset = asset as BASE_ASSET_REAL_ESTATE;

                    var reo = reos.AddRegularRecord();
                    reo.Val_rep = reoAsset.asset_value ?? "";
                    reo.MAmt_rep = reoAsset.existing_loan_amount ?? "";
                    reo.MPmt_rep = reoAsset.property_mortgage_payments ?? "";
                    reo.HExp_rep = reoAsset.property_other_costs ?? "";
                    reo.Addr = reoAsset.property_address ?? "";
                    reo.City = reoAsset.property_city ?? "";
                    reo.State = reoAsset.property_state ?? "";
                    reo.Zip = reoAsset.property_zip ?? "";

                    if (reoAsset.property_typeSpecified)
                    {
                        reo.Type = MapToLO(reoAsset.property_type);
                    }
                    if (reoAsset.is_subject_propertySpecified)
                    {
                        reo.IsSubjectProp = reoAsset.is_subject_property == BASE_APPLICANTDeclined_answer_race_gender.Y;
                    }
                    if (reoAsset.property_dispositionSpecified)
                    {
                        if (reoAsset.property_disposition != BASE_ASSET_REAL_ESTATEProperty_disposition.Item)
                        {
                            reo.StatT = MapDispositionToLO(reoAsset.property_disposition);
                        }
                    }

                    reo.ReOwnerT = appData.BorrowerModeT == E_BorrowerModeT.Borrower ? E_ReOwnerT.Borrower : E_ReOwnerT.CoBorrower;
                    reo.GrossRentI_rep = reoAsset.property_gross_rental_income ?? "";
                }
                else
                {
                    Tools.LogWarning("CLF Importer ran into reo that is not stored as reo. Ignoring.");
                }
            }

        }

        private void ImportAssets(IEnumerable<BASE_ASSET> assets, CAppData appData)
        {
            if (assets == null)
            {
                return;
            }

            IAssetCollection currentAssets = appData.aAssetCollection;

            bool usedCashDeposit1 = false;
            bool usedCashDeposit2 = false;
            decimal sumRetirementAssets = 0;
            decimal sumLifeInsuranceAsset = 0;
            decimal sumCashDeposits = 0;
            decimal workingValue;
            bool exportSumOfCashDeposits = false;

            foreach (BASE_ASSET asset in assets)
            {
                E_AssetT type = MapToLO(asset.asset_type);

                if (type == E_AssetT.Retirement)    //we only have 1 spot for this so its okay to add them all
                {
                    if (decimal.TryParse(asset.asset_value, out workingValue))
                    {
                        sumRetirementAssets += workingValue;
                    }
                    continue;
                }
                else if (type == E_AssetT.LifeInsurance)
                {
                    if (decimal.TryParse(asset.asset_value, out workingValue))
                    {
                        sumLifeInsuranceAsset += workingValue;
                    }
                    continue;
                }
                else if (type == E_AssetT.Business)
                {
                    var buisness = currentAssets.GetBusinessWorth(true);
                    buisness.Val_rep = asset.asset_value;
                    continue;
                }
                else if (type == E_AssetT.CashDeposit && !usedCashDeposit1)
                {
                    var cash = currentAssets.GetCashDeposit1(true);
                    cash.Val_rep = asset.asset_value;
                    usedCashDeposit1 = true;
                    cash.Desc = asset.description;
                    continue;
                }
                else if (type == E_AssetT.CashDeposit && !usedCashDeposit2)
                {
                    var cash = currentAssets.GetCashDeposit2(true);
                    usedCashDeposit2 = true;
                    cash.Val_rep = asset.asset_value;
                    sumCashDeposits = cash.Val;        //ensure we storethis in case there are more cash deposits
                    cash.Desc = asset.description;
                    continue;
                }
                else if (type == E_AssetT.CashDeposit)
                {
                    exportSumOfCashDeposits = true;
                    var cash = currentAssets.GetCashDeposit2(true);
                    if (decimal.TryParse(asset.asset_value, out workingValue))
                    {
                        sumCashDeposits += workingValue;
                    }
                    if (!string.IsNullOrEmpty(asset.description) && asset.description.Length > 0)
                    {
                        if (cash.Desc.Length > 0)
                        {
                            cash.Desc += ",";
                        }
                        cash.Desc += asset.description;
                    }

                    continue;
                }
                var regAsset = currentAssets.AddRegularRecord();
                regAsset.AssetT = ToRegularAssetType(type);
                regAsset.OwnerT = ToAssetOwnerT(asset.ownership_type);
                regAsset.Val_rep = asset.asset_value;
                regAsset.StAddr = asset.property_address ?? "";
                regAsset.State = asset.property_state ?? "";
                regAsset.Zip = asset.property_zip ?? "";
                regAsset.City = asset.property_city ?? "";
                regAsset.Desc = asset.description ?? "";
                regAsset.AccNum = asset.account_number ?? "";
                regAsset.ComNm = asset.bank_name ?? "";

                #region auto asset    adds make model year to desc to lo
                if (asset is BASE_ASSET_VEHICLE)
                {
                    BASE_ASSET_VEHICLE vehicleAsset = asset as BASE_ASSET_VEHICLE;
                    if (!string.IsNullOrEmpty(vehicleAsset.vehicle_make))
                    {
                        asset.description += " " + vehicleAsset.vehicle_make;
                    }
                    if (!string.IsNullOrEmpty(vehicleAsset.vehicle_model))
                    {
                        asset.description += " " + vehicleAsset.vehicle_model;
                    }
                    if (!string.IsNullOrEmpty(vehicleAsset.vehicle_color))
                    {
                        asset.description += " " + vehicleAsset.vehicle_color;
                    }
                    if (!string.IsNullOrEmpty(vehicleAsset.vehicle_year))
                    {
                        asset.description += " " + vehicleAsset.vehicle_year;
                    }

                    regAsset.Desc = asset.description.TrimWhitespaceAndBOM();

                }
                #endregion


                #region certificate asset  special handling not much to do here
                if (asset is BASE_ASSET_CERTIFICATE)
                {
                    BASE_ASSET_CERTIFICATE certificateAsset = asset as BASE_ASSET_CERTIFICATE;
                    if (!string.IsNullOrEmpty(certificateAsset.certificate_number))
                    {
                        if (regAsset.Desc != "")
                            regAsset.Desc += ";";
                        regAsset.Desc += "Cert #:" + certificateAsset.certificate_number;
                    }

                }
                #endregion

            }
            if (exportSumOfCashDeposits)
            {
                var deposit2 = currentAssets.GetCashDeposit2(true);
                deposit2.Val_rep = sumCashDeposits.ToString();
            }
            if (sumRetirementAssets > 0)
            {
                var retirementAsset = currentAssets.GetRetirement(true);
                retirementAsset.Val_rep = sumRetirementAssets.ToString();
            }
            if (sumLifeInsuranceAsset > 0)
            {
                var lifeInsuranceAsset = currentAssets.GetLifeInsurance(true);
                lifeInsuranceAsset.Val_rep = sumRetirementAssets.ToString();
            }


        }

        private void ImportMonthlyIncome(MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOME monthlyIncome, CAppData appData)
        {
            if (monthlyIncome == null)
            {
                return;
            }

            var consumerId = appData.aConsumerId.ToIdentifier<DataObjectKind.Consumer>();

            if (!_pageData.sIsIncomeCollectionEnabled)
            {
                appData.aBaseI_rep = monthlyIncome.monthly_income_base_salary ?? "0";
                appData.aOvertimeI_rep = monthlyIncome.monthly_income_over_time ?? "0";
                appData.aBonusesI_rep = monthlyIncome.monthly_income_bonus ?? "0";
                appData.aCommisionI_rep = monthlyIncome.monthly_income_commission ?? "0";
                appData.aDividendI_rep = monthlyIncome.monthly_income_dividends ?? "0";
            }
            else
            {
                _pageData.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(_pageData.m_convertLos.ToMoney(monthlyIncome.monthly_income_base_salary ?? "0"))
                    });

                _pageData.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Overtime,
                        MonthlyAmountData = Money.Create(_pageData.m_convertLos.ToMoney(monthlyIncome.monthly_income_over_time ?? "0"))
                    });

                _pageData.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Bonuses,
                        MonthlyAmountData = Money.Create(_pageData.m_convertLos.ToMoney(monthlyIncome.monthly_income_bonus ?? "0"))
                    });

                _pageData.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Commission,
                        MonthlyAmountData = Money.Create(_pageData.m_convertLos.ToMoney(monthlyIncome.monthly_income_commission ?? "0"))
                    });

                _pageData.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.DividendsOrInterest,
                        MonthlyAmountData = Money.Create(_pageData.m_convertLos.ToMoney(monthlyIncome.monthly_income_dividends ?? "0"))
                    });
            }

            appData.aNetRentI1003_rep = monthlyIncome.monthly_income_net_rental ?? "0";
            appData.aNetRentI1003Lckd = monthlyIncome.monthly_income_net_rentalSpecified;

            foreach (var income in monthlyIncome.OTHER_INCOME.CoalesceWithEmpty())
            {
                decimal incomeAmount;
                if (income.monthly_incomeSpecified && decimal.TryParse(income.monthly_income, out incomeAmount) && incomeAmount > 0)
                {
                    UpdateOtherIncome(appData, incomeAmount, income.monthly_income_description);
                }
            }

            foreach (var specialIncome in monthlyIncome.SPECIAL_INCOME.CoalesceWithEmpty())
            {
                decimal incomeAmount;
                if (specialIncome.amountSpecified && decimal.TryParse(specialIncome.amount, out incomeAmount) && incomeAmount > 0)
                {
                    UpdateOtherIncome(appData, incomeAmount, ToSIString(specialIncome.income_type));
                }
            }
        }

        private void ImportMonthlyDebt(BASE_MONTHLY_DEBT monthlyDebt, CAppData appData)
        {
            if (monthlyDebt == null)
            {
                return;
            }

            E_LiaOwnerT currentOwner = appData.BorrowerModeT == E_BorrowerModeT.Borrower ? E_LiaOwnerT.Borrower : E_LiaOwnerT.CoBorrower;
            var alimony = appData.aLiaCollection.GetAlimony(true);
            var jobExpense = appData.aLiaCollection.GetJobRelated1(true);
            var childSupport = appData.aLiaCollection.GetChildSupport(true);

            AppendToLiability(alimony, monthlyDebt.alimony);
            AppendToLiability(childSupport, monthlyDebt.child_support);
            AppendToLiability(alimony, monthlyDebt.separate_maintenance_expense);
            AppendToLiability(jobExpense, monthlyDebt.job_expense);

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower && monthlyDebt.monthly_rentSpecified)
            {
                appData.aPresRent_rep = monthlyDebt.monthly_rent;
            }
        }

        private void ImportFinancialInfo(MORTGAGE_BASE_APPLICANTFINANCIAL_INFO financialInfo, CAppData appData)
        {
            if (financialInfo == null)
            {
                return;
            }

            appData.aEmpCollection.ClearAll();
            ImportCurrentEmployment(financialInfo, appData);
            ImportPreviousEmployments(financialInfo, appData);
            ImportMonthlyIncome(financialInfo.MONTHLY_INCOME, appData);
            ImportMonthlyDebt(financialInfo.MONTHLY_DEBT, appData);
        }

        public void AppendToLiability(ILiabilitySpecial liability, string amount)
        {
            decimal convertedValue;

            if (decimal.TryParse(amount, out convertedValue))
            {
                liability.Pmt += convertedValue;
            }
        }

        /// <summary>
        /// Creates a new employment record and populates it using LPQ's employment record.
        /// </summary>
        /// <param name="employment">The LPQ employment record.</param>
        /// <param name="appData">The app data to populate.</param>
        /// <returns>The LQB employment record if successful. Null otherwise.</returns>
        private static IRegularEmploymentRecord ImportBaseEmployment(BASE_EMPLOYMENT employment, CAppData appData)
        {
            IRegularEmploymentRecord employmentRecord = appData.aEmpCollection.AddRegularRecord();

            if (employment.employment_start_dateSpecified)
            {
                employmentRecord.EmplmtStartD_rep = employment.employment_start_date;
                int months;
                if (int.TryParse(employment.employed_months, out months))         //calculate the end date so we dont lose the number of months
                {
                    DateTime dt = DateTime.Parse(employmentRecord.EmplmtStartD_rep);
                    employmentRecord.EmplmtEndD_rep = dt.AddMonths(months).ToShortDateString();
                }
            }

            if (employment.employment_status == BASE_EMPLOYMENTEmployment_status.SE)
            {
                employmentRecord.IsSelfEmplmt = true;
            }

            employmentRecord.JobTitle = employment.occupation ?? "";
            employmentRecord.EmplrBusPhone = employment.employment_phone ?? "";
            employmentRecord.EmplrNm = employment.employer ?? "";
            employmentRecord.EmplrAddr = employment.employment_address ?? "";
            employmentRecord.EmplrCity = employment.employment_city ?? "";
            employmentRecord.EmplrState = employment.employment_state ?? "";
            employmentRecord.EmplrZip = employment.employment_zip ?? "";
            employmentRecord.EmplmtStat = E_EmplmtStat.Previous; // All regular records in the list should be Previous. Only the primary employment gets Current.

            return employmentRecord;
        }

        /// <summary>
        /// Imports previous employments.
        /// </summary>
        /// <param name="financialInfo">The FINANCIAL_INFO container.</param>
        /// <param name="appData">The app data to populate.</param>
        private static void ImportPreviousEmployments(MORTGAGE_BASE_APPLICANTFINANCIAL_INFO financialInfo, CAppData appData)
        {
            if (financialInfo?.PREVIOUS_EMPLOYMENT == null)
            {
                return;
            }

            foreach (var previousEmployment in financialInfo.PREVIOUS_EMPLOYMENT)
            {
                if (!previousEmployment.monthly_incomeSpecified)
                {
                    continue;
                }

                var employmentRecord = ImportBaseEmployment(previousEmployment, appData);
                if (employmentRecord == null)
                {
                    continue;
                }

                if (previousEmployment.monthly_incomeSpecified)
                {
                    employmentRecord.MonI_rep = previousEmployment.monthly_income.ToString();
                }

                if (previousEmployment.employment_end_dateSpecified)
                {
                    employmentRecord.EmplmtEndD_rep = previousEmployment.employment_end_date;
                }
            }
        }

        /// <summary>
        /// Imports current employment information.
        /// </summary>
        /// <param name="financialInfo">The FINANCIAL_INFO container.</param>
        /// <param name="appData">The app data to populate.</param>
        private void ImportCurrentEmployment(MORTGAGE_BASE_APPLICANTFINANCIAL_INFO financialInfo, CAppData appData)
        {
            if (!(financialInfo?.CURRENT_EMPLOYMENT).CoalesceWithEmpty().Any())
            {
                return;
            }

            MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT primaryEmployment = financialInfo.CURRENT_EMPLOYMENT.FirstOrDefault();
            if (primaryEmployment != null)
            {
                IPrimaryEmploymentRecord primaryEmploymentRecord = appData.aEmpCollection.GetPrimaryEmp(true);

                if (primaryEmployment.employment_status == BASE_EMPLOYMENTEmployment_status.SE)
                {
                    primaryEmploymentRecord.IsSelfEmplmt = true;
                }

                primaryEmploymentRecord.JobTitle = primaryEmployment.occupation ?? "";
                primaryEmploymentRecord.EmplrBusPhone = primaryEmployment.employment_phone ?? "";
                appData.aEmplrBusPhoneLckd = true; //if this is not turned off it will get the number from borrower info work phone.
                primaryEmploymentRecord.ProfLenTotalMonths = primaryEmployment.profession_months ?? "";
                primaryEmploymentRecord.EmplrNm = primaryEmployment.employer ?? "";
                primaryEmploymentRecord.EmplrAddr = primaryEmployment.employment_address ?? "";
                primaryEmploymentRecord.EmplrCity = primaryEmployment.employment_city ?? "";
                primaryEmploymentRecord.EmplrState = primaryEmployment.employment_state ?? "";
                primaryEmploymentRecord.EmplrZip = primaryEmployment.employment_zip ?? "";
                primaryEmploymentRecord.EmplmtLenTotalMonths = primaryEmployment.employed_months ?? "";
                primaryEmploymentRecord.EmpltStartD_rep = primaryEmployment.employment_start_dateSpecified ? primaryEmployment.employment_start_date : string.Empty;
                primaryEmploymentRecord.EmplmtStat = E_EmplmtStat.Current;
            }

            foreach (var currentEmployment in financialInfo.CURRENT_EMPLOYMENT)
            {
                // Skip the primary employment item since its already been taken care of.
                if (primaryEmployment != currentEmployment)
                {
                    var employment = ImportBaseEmployment(currentEmployment, appData);
                    if (employment == null)
                    {
                        continue;
                    }

                    employment.IsCurrent = true;
                }
            }
        }

        /// <summary>
        /// This method helps figure out where to stick the other income. LoansPQ has 4 spots. It also has a unbounded number
        /// of special incomes.
        /// </summary>
        /// <param name="appData"></param>
        /// <param name="income"></param>
        /// <param name="desc"></param>
        private void UpdateOtherIncome(CAppData appData, decimal income, string desc)
        {
            if (!_pageData.sIsIncomeCollectionEnabled)
            {
                List<OtherIncome> otherIncomeList = appData.aOtherIncomeList;
                var emptySlot = otherIncomeList.FirstOrDefault(slot => slot.Amount == 0 && string.IsNullOrEmpty(slot.Desc));
                if (emptySlot == null)
                {
                    otherIncomeList.Add(new OtherIncome()
                    {
                        Amount = income,
                        Desc = desc,
                        IsForCoBorrower = appData.BorrowerModeT == E_BorrowerModeT.Coborrower
                    });
                }
                else
                {
                    emptySlot.Amount = income;
                    emptySlot.Desc = desc;
                    emptySlot.IsForCoBorrower = appData.BorrowerModeT == E_BorrowerModeT.Coborrower;
                }

                appData.aOtherIncomeList = otherIncomeList;
            }
            else
            {
                if (desc?.Equals(OtherIncome.GetDescription(E_aOIDescT.SubjPropNetCashFlow), StringComparison.OrdinalIgnoreCase) ?? false)
                {
                    _pageData.sSpCountRentalIForPrimaryResidToo = true;
                    this.hasSubjectPropertyNetCashFlow = true;
                    this.totalSubjectPropertyNetCashFlow += income;
                }
                else
                {
                    var otherIncome = new OtherIncome
                    {
                        Amount = income,
                        Desc = desc,
                        IsForCoBorrower = appData.BorrowerModeT == E_BorrowerModeT.Coborrower
                    };

                    var migratedOtherIncome = IncomeCollectionMigration.GetMigratedOtherIncome(new[] { otherIncome }, appData.aBConsumerId, appData.aCConsumerId);

                    foreach (var consumerIdIncomeSourcePair in migratedOtherIncome)
                    {
                        _pageData.AddIncomeSource(consumerIdIncomeSourcePair.Item1, consumerIdIncomeSourcePair.Item2);
                    }
                }
            }
        }

        private static string ShortenString(string longString, int size)
        {
            if (size < 3)
            {
                return longString;
            }
            if (string.IsNullOrEmpty(longString) || longString.Length < size)
            {
                return longString;
            }


            return longString.Substring(0, size - 2) + "..";
        }
        //private static string MonthsToYears(string months)
        //{
        //    int numOfMonths;
        //    if (int.TryParse(months, out numOfMonths))
        //    {
        //        return (numOfMonths / 12F).ToString();
        //    }
        //    return "";
        //}
        //private static int MonthsToYearsI(string months)
        //{
        //    int numOfMonths;
        //    if (int.TryParse(months, out numOfMonths))
        //    {
        //        return numOfMonths / 12;
        //    }
        //    return 0;
        //}

        #region mappings
        private E_aBMaritalStatT ToLO(BASE_APPLICANTMarital_status maritalStatus)
        {
            switch (maritalStatus)
            {
                case BASE_APPLICANTMarital_status.SEPARATED:
                    return E_aBMaritalStatT.Separated;
                case BASE_APPLICANTMarital_status.UNMARRIED:
                    return E_aBMaritalStatT.NotMarried;
                case BASE_APPLICANTMarital_status.MARRIED:
                    return E_aBMaritalStatT.Married;
                case BASE_APPLICANTMarital_status.DOMESTIC_PARTNER:
                case BASE_APPLICANTMarital_status.Item:
                    return E_aBMaritalStatT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled marital status : " + maritalStatus.ToString());

            }
        }

        private string ToLO(BASE_APPLICANTSuffix suffix)
        {
            switch (suffix)
            {
                case BASE_APPLICANTSuffix.JR:
                    return "JR";
                case BASE_APPLICANTSuffix.SR:
                    return "SR";
                case BASE_APPLICANTSuffix.II:
                    return "II";
                case BASE_APPLICANTSuffix.III:
                    return "III";
                case BASE_APPLICANTSuffix.IV:
                    return "IV";
                case BASE_APPLICANTSuffix.V:
                    return "V";
                case BASE_APPLICANTSuffix.VI:
                    return "VI";
                case BASE_APPLICANTSuffix.VII:
                    return "VII";
                case BASE_APPLICANTSuffix.VIII:
                    return "VIII";
                case BASE_APPLICANTSuffix.Item:
                    return "";
                default:
                    throw new ArgumentException("Unhandled suffix " + suffix.ToString());
            }
        }

        private E_aBAddrT ToLO(MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status occupancyStatus)
        {
            switch (occupancyStatus)
            {
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.LIVEWITHPARENTS:
                    return E_aBAddrT.LivingRentFree;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.OWN:
                    return E_aBAddrT.Own;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.RENT:
                    return E_aBAddrT.Rent;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.GOVQUARTERS:
                    return E_aBAddrT.Rent;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.BUYING:
                    return E_aBAddrT.Own;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.OTHER:
                    return E_aBAddrT.LeaveBlank;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.Item:
                    return E_aBAddrT.LeaveBlank;
                case MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.BARRACKS:
                    return E_aBAddrT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled occupancy status : " + occupancyStatus.ToString());

            }
        }


        private E_sStatusT MapToLO(BASE_LOAN_STATUSLoan_status baseLoanStatus)
        {
            switch (baseLoanStatus)
            {
                case BASE_LOAN_STATUSLoan_status.PREAPP://pre approved
                    return E_sStatusT.Loan_Preapproval;
                case BASE_LOAN_STATUSLoan_status.PEN:   //pen
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.APP:   //approved
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.AA:    //instant approved
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.AD:    //InstantDeclined
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.AP:    //ApprovedPending
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.OFF:   //CounterOffer
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.DEC:   //Declined
                    return E_sStatusT.Lead_Declined;
                case BASE_LOAN_STATUSLoan_status.DUP:   //duplicate
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.INC:   //incomplete
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.DEN:    //membership denied
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.CAN:    //cancelled
                    return E_sStatusT.Loan_Canceled;
                case BASE_LOAN_STATUSLoan_status.INQ:    //inquiring
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.NMI:    //need more info
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.CODEC:  //coappdeclined
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.AP2S:   //approved pending 2 sigs
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.MEMWAIT://member waiting
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.WTHDRN: //withdrawn
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.WTHDRNO:
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.REF:    //refferred
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.AMD:    //approved membership declined
                    return E_sStatusT.Loan_Approved;
                case BASE_LOAN_STATUSLoan_status.MEMDC:  //membership declined credit
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.REVIEW: //review
                    return E_sStatusT.Loan_Other;
                case BASE_LOAN_STATUSLoan_status.FRAUD: //fraud
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.CFI: //closed for incompleteness
                case BASE_LOAN_STATUSLoan_status.CFIO: // Closed for Incompleteness Other
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.PRD: // Preapproval request denied
                    return E_sStatusT.Loan_Rejected;
                case BASE_LOAN_STATUSLoan_status.PRA: // Preapproval request approved
                    return E_sStatusT.Loan_Approved;
                default:
                    throw new ArgumentException("Unhandled loan status : " + baseLoanStatus);
            }
        }
        private E_ReoStatusT MapDispositionToLO(BASE_ASSET_REAL_ESTATEProperty_disposition disposition)
        {
            switch (disposition)
            {
                case BASE_ASSET_REAL_ESTATEProperty_disposition.S:
                    return E_ReoStatusT.Sale;
                case BASE_ASSET_REAL_ESTATEProperty_disposition.H:
                    return E_ReoStatusT.Residence;
                case BASE_ASSET_REAL_ESTATEProperty_disposition.P:
                    return E_ReoStatusT.PendingSale;
                case BASE_ASSET_REAL_ESTATEProperty_disposition.R:
                    return E_ReoStatusT.Rental;
                case BASE_ASSET_REAL_ESTATEProperty_disposition.Item:
                    return E_ReoStatusT.Residence;
                default:
                    throw new ArgumentException("Unhandled disposition: " + disposition.ToString());
            }
        }

        private string ToSIString(MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type income_type)
        {
            E_aOIDescT descriptionType;
            if (income_type == MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.Pension)
            {
                return "Pension";
            }
            else if (income_type == MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.Trust)
            {
                return "Trust";
            }
            else if (income_type == MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.Unemployment)
            {
                return "Unemployment";
            }
            else if (income_type == MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MortgageDifferential)
            {
                return "Mortgage Differential";
            }
            else
            {
                switch (income_type)
                {
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.AlimonyChildSupport:
                        descriptionType = E_aOIDescT.AlimonyChildSupport;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.AutomobileExpenseAccount:
                        descriptionType = E_aOIDescT.AutomobileExpenseAccount;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.FosterCare:
                        descriptionType = E_aOIDescT.FosterCare;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.NotesReceivableInstallment:
                        descriptionType = E_aOIDescT.NotesReceivableInstallment;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.SocialSecurity:
                        descriptionType = E_aOIDescT.SocialSecurity;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.VABenefitsNonEducational:
                        descriptionType = E_aOIDescT.VABenefitsNonEducation;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryBasePay:
                        descriptionType = E_aOIDescT.MilitaryBasePay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryRationsAllowance:
                        descriptionType = E_aOIDescT.MilitaryRationsAllowance;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryFlightPay:
                        descriptionType = E_aOIDescT.MilitaryFlightPay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryHazardPay:
                        descriptionType = E_aOIDescT.MilitaryHazardPay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryClothesAllowance:
                        descriptionType = E_aOIDescT.MilitaryClothesAllowance;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryQuartersAllowance:
                        descriptionType = E_aOIDescT.MilitaryQuartersAllowance;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryPropPay:
                        descriptionType = E_aOIDescT.MilitaryPropPay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryOverseasPay:
                        descriptionType = E_aOIDescT.MilitaryOverseasPay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryCombatPay:
                        descriptionType = E_aOIDescT.MilitaryCombatPay;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.MilitaryVariableHousingAllowance:
                        descriptionType = E_aOIDescT.MilitaryVariableHousingAllowance;
                        break;
                    case MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOMESPECIAL_INCOMEIncome_type.SubjectPropertyNetCashFlow:
                        descriptionType = E_aOIDescT.SubjPropNetCashFlow;
                        break;
                    default:
                        throw new UnhandledEnumException(income_type);
                }

                return OtherIncome.GetDescription(descriptionType);
            }
        }

        private string MapToLO(BASE_ASSET_REAL_ESTATEProperty_type propertyType)
        {
            switch (propertyType)
            {
                case BASE_ASSET_REAL_ESTATEProperty_type.SFR:
                    return "SFR";
                case BASE_ASSET_REAL_ESTATEProperty_type.Item2UNIT:
                case BASE_ASSET_REAL_ESTATEProperty_type.Item3UNIT:
                case BASE_ASSET_REAL_ESTATEProperty_type.Item4UNIT:
                    return "2-4Plx";
                case BASE_ASSET_REAL_ESTATEProperty_type.Item5UNIT:
                    return "Multi";
                case BASE_ASSET_REAL_ESTATEProperty_type.TOWNHOUSE:
                    return "Town";
                case BASE_ASSET_REAL_ESTATEProperty_type.HIGHRISECONDO:
                    return "Condo";
                case BASE_ASSET_REAL_ESTATEProperty_type.LOWRISECONDO:
                    return "Condo";
                case BASE_ASSET_REAL_ESTATEProperty_type.MOBILEHOME:
                    return "Mobil";
                case BASE_ASSET_REAL_ESTATEProperty_type.MIXEDUSE:
                    return "Mixed";
                case BASE_ASSET_REAL_ESTATEProperty_type.CONDOTEL:
                    return "condo";
                case BASE_ASSET_REAL_ESTATEProperty_type.COOP:
                    return "Coop";
                case BASE_ASSET_REAL_ESTATEProperty_type.COMMERCIAL:
                    return "Com-NR";
                case BASE_ASSET_REAL_ESTATEProperty_type.FARM:
                    return "Farm";
                case BASE_ASSET_REAL_ESTATEProperty_type.HOMEBUSINESS:
                    return "Com-R";
                case BASE_ASSET_REAL_ESTATEProperty_type.LAND:
                    return "Land";
                case BASE_ASSET_REAL_ESTATEProperty_type.Item:
                case BASE_ASSET_REAL_ESTATEProperty_type.MANUFACTUREDHOME:
                case BASE_ASSET_REAL_ESTATEProperty_type.PUD:
                case BASE_ASSET_REAL_ESTATEProperty_type.OTHER:
                case BASE_ASSET_REAL_ESTATEProperty_type.MNL: // Manufactured - No land
                    return "Other";
                default:
                    throw new ArgumentException("Unhandled Base Asset Real Estate Property Type : " + propertyType);
            }
        }

        private E_AssetOwnerT ToAssetOwnerT(BASE_ASSETOwnership_type type)
        {
            switch (type)
            {
                case BASE_ASSETOwnership_type.B:
                    return E_AssetOwnerT.Borrower;
                case BASE_ASSETOwnership_type.J:
                    return E_AssetOwnerT.Joint;
                case BASE_ASSETOwnership_type.T:
                    return E_AssetOwnerT.CoBorrower;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private E_AssetRegularT ToRegularAssetType(E_AssetT type)
        {
            switch (type)
            {
                case E_AssetT.Auto:
                    return E_AssetRegularT.Auto;
                case E_AssetT.Bonds:
                    return E_AssetRegularT.Bonds;
                case E_AssetT.Checking:
                    return E_AssetRegularT.Checking;
                case E_AssetT.GiftFunds:
                    return E_AssetRegularT.GiftFunds;
                case E_AssetT.Savings:
                    return E_AssetRegularT.Savings;
                case E_AssetT.Stocks:
                    return E_AssetRegularT.Stocks;
                case E_AssetT.OtherIlliquidAsset:
                    return E_AssetRegularT.OtherIlliquidAsset;
                case E_AssetT.OtherLiquidAsset:
                    return E_AssetRegularT.OtherLiquidAsset;
                case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
                    return E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetT.GiftEquity:
                    return E_AssetRegularT.GiftEquity;
                case E_AssetT.CertificateOfDeposit:
                    return E_AssetRegularT.CertificateOfDeposit;
                case E_AssetT.MoneyMarketFund:
                    return E_AssetRegularT.MoneyMarketFund;
                case E_AssetT.MutualFunds:
                    return E_AssetRegularT.MutualFunds;
                case E_AssetT.SecuredBorrowedFundsNotDeposit:
                    return E_AssetRegularT.SecuredBorrowedFundsNotDeposit;
                case E_AssetT.BridgeLoanNotDeposited:
                    return E_AssetRegularT.BridgeLoanNotDeposited;
                case E_AssetT.TrustFunds:
                    return E_AssetRegularT.TrustFunds;
                case E_AssetT.OtherPurchaseCredit:
                    return E_AssetRegularT.OtherPurchaseCredit;
                //the following should never be encountered because the loop looks for them before calling this method
                case E_AssetT.CashDeposit:
                case E_AssetT.LifeInsurance:
                case E_AssetT.Retirement:
                case E_AssetT.Business:
                default:
                    throw new ArgumentException("Unhandled Asset Type : " + type);
            }
        }

        private E_AssetT MapToLO(BASE_ASSETAsset_type assetType)
        {
            switch (assetType)
            {
                case BASE_ASSETAsset_type.CERTIFICATE:
                    return E_AssetT.CertificateOfDeposit; //? 
                case BASE_ASSETAsset_type.CASH_DEPOSIT:
                    return E_AssetT.CashDeposit;
                case BASE_ASSETAsset_type.SHARE_BACK:
                    return E_AssetT.Savings;
                case BASE_ASSETAsset_type.STOCK:
                    return E_AssetT.Stocks;
                case BASE_ASSETAsset_type.BOND:
                    return E_AssetT.Bonds;
                case BASE_ASSETAsset_type.CHECKING_ACCOUNT:
                    return E_AssetT.Checking;
                case BASE_ASSETAsset_type.SAVINGS_ACCOUNT:
                    return E_AssetT.Savings;
                case BASE_ASSETAsset_type.REALESTATE:
                    return E_AssetT.PendingNetSaleProceedsFromRealEstateAssets;
                case BASE_ASSETAsset_type.MACHINERY_AND_EQUIPMENT:
                    return E_AssetT.OtherIlliquidAsset;
                case BASE_ASSETAsset_type.FURNITURE_AND_FIXTURES:
                    return E_AssetT.OtherIlliquidAsset;
                case BASE_ASSETAsset_type.AUTOMOBILE:
                    return E_AssetT.Auto;
                case BASE_ASSETAsset_type.LIFE_INSURANCE:
                    return E_AssetT.LifeInsurance;
                case BASE_ASSETAsset_type.RETIREMENT_FUND:
                    return E_AssetT.Retirement;
                case BASE_ASSETAsset_type.OWNED_BUSINESS:
                    return E_AssetT.Business;
                case BASE_ASSETAsset_type.OTHER:
                    return E_AssetT.OtherLiquidAsset;
                case BASE_ASSETAsset_type.OTHER_NON_LIQUID:
                    return E_AssetT.OtherIlliquidAsset;
                case BASE_ASSETAsset_type.BRIDGE_LOAN_NOT_DEPOSITED:
                    return E_AssetT.BridgeLoanNotDeposited;
                case BASE_ASSETAsset_type.GIFT:
                case BASE_ASSETAsset_type.GIFT_OF_EQUITY:
                    return E_AssetT.GiftEquity;
                case BASE_ASSETAsset_type.MONEY_MARKET:
                    return E_AssetT.MoneyMarketFund;
                case BASE_ASSETAsset_type.MUTUAL_FUNDS:
                    return E_AssetT.MutualFunds;
                case BASE_ASSETAsset_type.SECURED_BORROWER_FUNDS:
                    return E_AssetT.SecuredBorrowedFundsNotDeposit;
                case BASE_ASSETAsset_type.TRUST_FUNDS:
                    return E_AssetT.TrustFunds;
                case BASE_ASSETAsset_type.IRA:
                    return E_AssetT.Retirement;
                case BASE_ASSETAsset_type.NET_SALE_PROCEEDS:
                case BASE_ASSETAsset_type.CASH_ON_HAND:
                    return E_AssetT.OtherLiquidAsset;
                default:
                    throw new ArgumentException("Unhandled Base Asset Type : " + assetType);
            }
        }
        /// <summary>
        /// Converts the contact type to a LendingQB Type. If its not support the name is passed into other description.
        /// If the user doesnt modify the other description on export it map correctly back to loans pq. 
        /// </summary>
        /// <param name="contactType">LPQ contact type</param>
        /// <param name="otherDesc">Agent Other description</param>
        /// <returns>E_AgentRoleT</returns>
        /// <exception cref="ArgumentException"> Exception is thrown when a unsupported contact type is encountered </exception>
        private E_AgentRoleT MapToLO(MORTGAGE_CONTACTSCONTACT_INFOContact_type contactType)
        {
            switch (contactType)
            {
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.APPRAISER:
                    return E_AgentRoleT.Appraiser;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BANK:
                    return E_AgentRoleT.Bank;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BROKER:
                    return E_AgentRoleT.Broker;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BROKERREP:
                    return E_AgentRoleT.BrokerRep;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUILDER:
                    return E_AgentRoleT.Builder;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUYERAGENT:
                    return E_AgentRoleT.BuyerAgent;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUYERATTOR:
                    return E_AgentRoleT.BuyerAttorney;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.CALLCAGT:
                    return E_AgentRoleT.CallCenterAgent;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.CLOSEAGNT:
                    return E_AgentRoleT.ClosingAgent;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.CREDITRPT:
                    return E_AgentRoleT.CreditReport;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.ECOA:
                    return E_AgentRoleT.ECOA;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.ESCROW:
                    return E_AgentRoleT.Escrow;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.FAIRHOUSE:
                    return E_AgentRoleT.FairHousingLending;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.HOA:
                    return E_AgentRoleT.HomeOwnerAssociation;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.INSURANCE:
                    return E_AgentRoleT.HazardInsurance;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.INVESTOR:
                    return E_AgentRoleT.Investor;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.LISTAGENT:
                    return E_AgentRoleT.ListingAgent;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.MANAGER:
                    return E_AgentRoleT.Manager;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.MARKETLEAD:
                    return E_AgentRoleT.MarketingLead;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.MTGINSURAN:
                    return E_AgentRoleT.MortgageInsurance;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.REALTOR:
                    return E_AgentRoleT.Realtor;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLER:
                    return E_AgentRoleT.Seller;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLERATT:
                    return E_AgentRoleT.SellerAttorney;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLERAGNT:
                    return E_AgentRoleT.SellingAgent;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.SERVICING:
                    return E_AgentRoleT.Servicing;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.SURVEYOR:
                    return E_AgentRoleT.Surveyor;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.TITLE:
                    return E_AgentRoleT.Title;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.UNDERWRITE:
                    return E_AgentRoleT.Underwriter;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER:
                    return E_AgentRoleT.Other;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.FLOODINS:
                    return E_AgentRoleT.FloodProvider; // db - OPM 33365
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.FLOODCERT:
                    return E_AgentRoleT.Other;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.COLLAT_OFF:
                    return E_AgentRoleT.Other;
                case MORTGAGE_CONTACTSCONTACT_INFOContact_type.APPLICANT:
                    return E_AgentRoleT.Other;
                default:
                    throw new ArgumentException("Unhandled Contact Type : " + contactType);
            }
        }


        /// <summary>
        /// Converts LPQ other credit type to string. 
        /// </summary>
        /// <param name="type">The LPQ TYPE</param>
        /// <returns>string representation</returns>
        /// <exception cref="ArgumentException">Thrown when it encounters a non supported credit type.</exception>
        private string MapToLO(other_credit_type type)
        {
            switch (type)
            {
                case other_credit_type.CASHDEPOSITONSALESCONTRACT:
                    return "cash deposit on sales contract";
                case other_credit_type.SELLERCREDIT:
                    return "seller credit";
                case other_credit_type.LENDERCREDIT:
                    return "lender credit";
                case other_credit_type.RELOCATIONFUNDS:
                    return "relocation funds";
                case other_credit_type.EMPLOYERASSISTEDHOUSING:
                    return "employer assisted housing";
                case other_credit_type.LEASEPURCHASEFUND:
                    return "lease purchase fund";
                case other_credit_type.OTHER:
                    return "other";
                case other_credit_type.BORROWERPAIDFEES:
                    return "borrower paid fees";
                case other_credit_type.Item:
                    return "";
                default:
                    throw new ArgumentException("Unhandled other credit type : " + type);
            }

        }

        /// <summary>
        /// Converts the LPQ inteview method type to LO.
        /// </summary>
        /// <param name="methodType">LPQ interview method type</param>
        /// <returns>LO interview method T</returns>
        /// <exception cref="UnhandledEnumException">Thrown when it encounters a non supported method type.</exception>
        private static E_aIntrvwrMethodT MapToLO(MORTGAGE_BASE_APPLICANTInterview_method methodType)
        {
            switch (methodType)
            {
                case MORTGAGE_BASE_APPLICANTInterview_method.TELEPHONE:
                    return E_aIntrvwrMethodT.ByTelephone;
                case MORTGAGE_BASE_APPLICANTInterview_method.FACETOFACE:
                    return E_aIntrvwrMethodT.FaceToFace;
                case MORTGAGE_BASE_APPLICANTInterview_method.INTERNET:
                    return E_aIntrvwrMethodT.Internet;
                case MORTGAGE_BASE_APPLICANTInterview_method.MAIL:
                case MORTGAGE_BASE_APPLICANTInterview_method.FAX:
                    return E_aIntrvwrMethodT.ByMail;
                case MORTGAGE_BASE_APPLICANTInterview_method.Item:
                    return E_aIntrvwrMethodT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(methodType);
            }
        }

        /// <summary>
        /// Converts LPQ down payment source to LO string. 
        /// </summary>
        /// <param name="downpaymentSource">Lpq down payment soruce</param>
        /// <returns>lo string representation </returns>
        /// <exception cref="ArgumentException">Thrown when it encounters a non supported  type.</exception>
        private static string MapDownPaymentSourceToLO(MORTGAGE_LOAN_INFODown_payment_source downpaymentSource)
        {
            switch (downpaymentSource)
            {
                case MORTGAGE_LOAN_INFODown_payment_source.CS:
                    return "checking/savings";
                case MORTGAGE_LOAN_INFODown_payment_source.DOSC:
                    return "deposit on sales contract";
                case MORTGAGE_LOAN_INFODown_payment_source.ESOP:
                    return "equity on sold property";
                case MORTGAGE_LOAN_INFODown_payment_source.EPFSA:
                    return "equity from pending sale";
                case MORTGAGE_LOAN_INFODown_payment_source.EPFSU:
                    return "equity from subject property";
                case MORTGAGE_LOAN_INFODown_payment_source.GF:
                    return "gift funds";
                case MORTGAGE_LOAN_INFODown_payment_source.SAB:
                    return "stock & bonds";
                case MORTGAGE_LOAN_INFODown_payment_source.LEQ:
                    return "lot equity";
                case MORTGAGE_LOAN_INFODown_payment_source.BL:
                    return "bridge loan";
                case MORTGAGE_LOAN_INFODown_payment_source.UBF:
                    return "unsecured borrowed funds";
                case MORTGAGE_LOAN_INFODown_payment_source.TF:
                    return "trust funds";
                case MORTGAGE_LOAN_INFODown_payment_source.RF:
                    return "retirement funds";
                case MORTGAGE_LOAN_INFODown_payment_source.ROP:
                    return "rent with option to purchase";
                case MORTGAGE_LOAN_INFODown_payment_source.LICV:
                    return "life insurance cash value";
                case MORTGAGE_LOAN_INFODown_payment_source.SC:
                    return "sale of chattel";
                case MORTGAGE_LOAN_INFODown_payment_source.TEQ:
                    return "trade equity";
                case MORTGAGE_LOAN_INFODown_payment_source.SEQ:
                    return "sweat equity";
                case MORTGAGE_LOAN_INFODown_payment_source.COH:
                    return "cash on hand";
                case MORTGAGE_LOAN_INFODown_payment_source.O:
                    return "other";
                case MORTGAGE_LOAN_INFODown_payment_source.SBF:
                    return "secured borrowed funds";
                case MORTGAGE_LOAN_INFODown_payment_source.Item:
                    return "";
                default:
                    throw new ArgumentException("Unhandled Down Payment Source  : " + downpaymentSource);
            }

        }


        /// <summary>
        /// Converts lpq string  to gse refinance type. If it cant it returns the leave blank option. It logs it to pb.  
        /// </summary>
        /// <param name="p">string with the value from the XML</param>
        /// <returns></returns>
        private static E_sGseRefPurposeT MapGSERefTypeToLO(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToUpper())
            {
                // ---- refinance_purpose_first ---- //
                case "CASHOUT/CONSOLIDATE":
                    return E_sGseRefPurposeT.CashOutDebtConsolidation;
                case "CASHOUT/HOME IMPROV":
                    return E_sGseRefPurposeT.CashOutHomeImprovement;
                case "CASHOUT/LIMITED":
                    return E_sGseRefPurposeT.CashOutLimited;
                case "CASHOUT/OTHER":
                    return E_sGseRefPurposeT.CashOutOther;
                case "NC FHA STREAMLINED":
                    return E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance;
                case "NC FRE REFINANCE":
                    return E_sGseRefPurposeT.NoCashOutFREOwnedRefinance;
                case "NC OTHER":
                    return E_sGseRefPurposeT.NoCashOutOther;
                case "NC STREAMLINED":
                    return E_sGseRefPurposeT.NoCashOutStreamlinedRefinance;
                case "CHANGE RATE TERM":
                    return E_sGseRefPurposeT.ChangeInRateTerm;
                //---- homeequity_refinance_purpose ---- //
                case "CASH":
                    return E_sGseRefPurposeT.CashOutOther;
                case "HOME IMPROVEMENT":
                    return E_sGseRefPurposeT.CashOutHomeImprovement;
                case "DEBT CONSOLIDATION":
                    return E_sGseRefPurposeT.CashOutDebtConsolidation;
                case "PERSONAL":
                    return E_sGseRefPurposeT.CashOutOther;
                case "OTHER":
                case "REFINCE CASHOUT":
                    return E_sGseRefPurposeT.CashOutOther;
                case "REFINANCE":
                    return E_sGseRefPurposeT.ChangeInRateTerm;
                case "MANUFACTURED HOME PURCHASE":
                    return E_sGseRefPurposeT.CashOutOther;
                case "":
                    return E_sGseRefPurposeT.LeaveBlank;
                default:
                    Tools.LogError("Unhandled LPQ GSE Refinance type Leaving Blank  : " + p);
                    return E_sGseRefPurposeT.LeaveBlank;
            }

        }

        /// <summary>
        /// Convert LPQ Loan Purpose to LO. Throws exception if its a new type thats not in the switch.
        /// </summary>
        /// <param name="loanPurpose">LPQ loan purpose</param>
        /// <returns>Lo Loan Purpose</returns>
        private static E_sLPurposeT MapToLO(MORTGAGE_LOAN_INFOPurpose loanPurpose)
        {
            switch (loanPurpose)
            {
                case MORTGAGE_LOAN_INFOPurpose.CONSTRUCTION:
                    return E_sLPurposeT.Construct;
                case MORTGAGE_LOAN_INFOPurpose.CONSTRUCTIONPERM:
                    return E_sLPurposeT.ConstructPerm;
                case MORTGAGE_LOAN_INFOPurpose.PURCHASE:
                    return E_sLPurposeT.Purchase;
                case MORTGAGE_LOAN_INFOPurpose.REFINANCE:
                    return E_sLPurposeT.Refin;
                case MORTGAGE_LOAN_INFOPurpose.REFINANCECASHOUT:
                case MORTGAGE_LOAN_INFOPurpose.HOMEIMPROVEMENT:
                    return E_sLPurposeT.RefinCashout;
                case MORTGAGE_LOAN_INFOPurpose.OTHER:
                case MORTGAGE_LOAN_INFOPurpose.Item:
                    return E_sLPurposeT.Other;
                default:
                    throw new ArgumentException("Unhandled Loan Purpose type : " + loanPurpose);
            }
        }

        /// <summary>
        /// Converts a LPQ string to ssp project class. Leaves blank if its a Unhandled type and logs an error.
        /// </summary>
        /// <param name="p">LPQ project class string</param>
        /// <returns> Lo Enum Type </returns>
        private static E_sSpProjClassT MapToLOProjClass(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToUpper())
            {
                case "A_IIICONDOMINIUM":
                    return E_sSpProjClassT.AIIICondo;
                case "B_IICONDOMINIUM":
                    return E_sSpProjClassT.BIICondo;
                case "C_ICONDOMINIUM":
                    return E_sSpProjClassT.CICondo;
                case "APP_FHAVA_CONDOSPOT":
                    return E_sSpProjClassT.LeaveBlank;
                case "1_COOP":
                    return E_sSpProjClassT.COOP1;
                case "2_COOP":
                    return E_sSpProjClassT.COOP2;
                case "E_PUD":
                    return E_sSpProjClassT.EPUD;
                case "F_PUD":
                    return E_sSpProjClassT.FPUD;
                case "III_PUD":
                    return E_sSpProjClassT.IIIPUD;
                case "":
                    return E_sSpProjClassT.LeaveBlank;
                default:
                    Tools.LogError("Unhandled LPQ Project Class string Returning Empty " + p);
                    return E_sSpProjClassT.LeaveBlank;
            }

        }


        private static bool MapYNToBool(BASE_APPLICANTDeclined_answer_race_gender answer)
        {
            return answer == BASE_APPLICANTDeclined_answer_race_gender.Y;
        }


        /// <summary>
        /// Converts a base answer to a Y  N Empty String. 
        /// </summary>
        /// <param name="answer">The Y N answer from LPQ</param>
        /// <returns>Y N or string.empty </returns>
        private static string MapToLO(BASE_APPLICANTDeclined_answer_race_gender answer)
        {
            switch (answer)
            {
                case BASE_APPLICANTDeclined_answer_race_gender.Y:
                    return "Y";
                case BASE_APPLICANTDeclined_answer_race_gender.N:
                    return "N";
                case BASE_APPLICANTDeclined_answer_race_gender.Item:
                    return "";
                default:
                    throw new ArgumentException("Unhandled answer race gender type " + answer);
            }
        }

        /// <summary>
        /// Converts a LPQ gse property type to LO type.
        /// ATTACHED, DETACHED, CONDOMINIUM, COOPERATIVE, MANU_SINGLEWIDE, MANU_MULTIWIDE, MANU_HOUSING, PUD, HI_RISE_CONDO,DETACHED_CONDO, MANU_HCPCOOP
        /// If it encounters anything else it will leave it blank and log it to PB.
        /// </summary>
        /// <param name="p">LPQ string with the property type</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when theres a unhandled type.</exception>
        private static E_sGseSpT MapGsePropertyTypeToLO(string p)
        {
            p = p ?? "";
            switch (p.ToUpper())
            {
                case "ATTACHED":
                    return E_sGseSpT.Attached;
                case "CONDOMINIUM":
                    return E_sGseSpT.Condominium;
                case "COOPERATIVE":
                    return E_sGseSpT.Cooperative;
                case "DETACHED":
                    return E_sGseSpT.Detached;
                case "DETACHED_CONDO":
                    return E_sGseSpT.DetachedCondominium;
                case "HI_RISE_CONDO":
                    return E_sGseSpT.HighRiseCondominium;
                case "MANU_MULTIWIDE":
                    return E_sGseSpT.ManufacturedHomeMultiwide;
                case "MANU_HCPCOOP":
                    return E_sGseSpT.ManufacturedHomeCondominium;
                case "MANU_HOUSING":
                    return E_sGseSpT.ManufacturedHousing;
                case "MANU_SINGLEWIDE":
                    return E_sGseSpT.ManufacturedHousingSingleWide;
                case "PUD":
                    return E_sGseSpT.PUD;
                case "":
                    return E_sGseSpT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled LPQ Gse Property Type : " + p);
            }
        }
        /// <summary>
        /// Converts LPQ ethnicity to Lo type 
        /// </summary>
        /// <param name="bASE_APPLICANTEthnicity"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Throws ARgument exception when a unhandled ethnicity type is encountered</exception>
        private static E_aHispanicT ToLO(BASE_APPLICANTEthnicity ethnicity)
        {
            switch (ethnicity)
            {
                case BASE_APPLICANTEthnicity.HISPANIC:
                    return E_aHispanicT.Hispanic;
                case BASE_APPLICANTEthnicity.NOT_HISPANIC:
                    return E_aHispanicT.NotHispanic;
                case BASE_APPLICANTEthnicity.Item:
                    return E_aHispanicT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled LPQ Ethnicity Type " + ethnicity);
            }
        }

        /// <summary>
        /// Converts LPQ gender to LO Gender
        /// </summary>
        /// <param name="gender">LPQ Gender </param>
        /// <returns>LOQ ender</returns>
        /// <exception cref="ArgumentException">Throws argument exception when a unhandled type is encountered</exception>
        private static E_GenderT ToLO(BASE_APPLICANTGender gender)
        {
            switch (gender)
            {
                case BASE_APPLICANTGender.MALE:
                    return E_GenderT.Male;
                case BASE_APPLICANTGender.FEMALE:
                    return E_GenderT.Female;
                case BASE_APPLICANTGender.Item:
                case BASE_APPLICANTGender.OTHER:
                    return E_GenderT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled LPQ Gender " + gender);
            }
        }

        /// <summary>
        /// Converts LPQ buydown source string to LO enum.
        /// </summary>
        /// <param name="p">Buydown source</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">thrown when a Unhandled type is passed.</exception>
        private static E_sBuydownContributorT MapBuydownSourceToLO(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToUpper())
            {
                case "BORROWER":
                    return E_sBuydownContributorT.Borrower;
                case "OTHER":
                    return E_sBuydownContributorT.Other;
                case "LENDER_FINANCED":
                    return E_sBuydownContributorT.LenderPremiumFinanced;
                case "SELLER":
                    return E_sBuydownContributorT.Seller;
                case "BUILDER":
                    return E_sBuydownContributorT.Builder;
                case "":
                    return E_sBuydownContributorT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled LPQ Buydown source " + p);
            }
        }

        /// <summary>
        ///  Converts the LPQ building statis to LPQ 
        ///  taken From http://stage.loanspq.com/lender/mortgage/LoanApp/mortgage.aspx
        /// </summary>
        /// <param name="p">LPQ building statis </param>
        /// <returns>LO Enum</returns>
        /// <exception cref="ArgumentException">Argument exception is thrown when an Unhandled type is encountered.</exception> 
        private static E_sBuildingStatusT MapToLOBuildingStatus(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToUpper())
            {
                case "EXISTING":
                    return E_sBuildingStatusT.Existing;
                case "PROPOSED":
                    return E_sBuildingStatusT.Proposed;
                case "SUBJ_IMP_REPAIDREHAB":
                    return E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab;
                case "SUBSTANTIAL_REHAB":
                    return E_sBuildingStatusT.SubstantiallyRehabilitated;
                case "UNDER_CONSTRUCTION":
                    return E_sBuildingStatusT.UnderConstruction;
                case "":
                    return E_sBuildingStatusT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled type for LPQ Building Status " + p);
            }
        }


        /// <summary>
        /// Converts Answer Race Gender TO Lo Improvement Time Frame
        /// </summary>
        /// <param name="yn">LPQ YN Empty Enum </param>
        /// <returns>Improvement Frame Time</returns>
        /// <exception cref="ArgumentException">This is thrown when a unexpected input is entered.</exception>
        private static E_sSpImprovTimeFrameT MapToLOImprovToBeMade(BASE_APPLICANTDeclined_answer_race_gender yn)
        {
            switch (yn)
            {
                case BASE_APPLICANTDeclined_answer_race_gender.Y:
                    return E_sSpImprovTimeFrameT.ToBeMade;
                case BASE_APPLICANTDeclined_answer_race_gender.N:
                    return E_sSpImprovTimeFrameT.Made;
                case BASE_APPLICANTDeclined_answer_race_gender.Item:
                    return E_sSpImprovTimeFrameT.LeaveBlank;
                default:
                    throw new ArgumentException("Unhandled LPQ YN Answer for Improvements to be made. Answer=" + yn);
            }
        }
        /// <summary>
        /// Converts LPQ MORTGAGE_DECLARATIONTitle_hold_type to LO  E_aBDecPastOwnedPropTitleT
        /// </summary>
        /// <param name="title">The LPQ Type</param>
        /// <returns>LO Enum</returns>
        /// <exception cref="ArgumentException">Thrown when unexpcted type is entered.</exception>
        private static E_aBDecPastOwnedPropTitleT MapToLO(MORTGAGE_DECLARATIONTitle_hold_type title)
        {
            switch (title)
            {
                case MORTGAGE_DECLARATIONTitle_hold_type.S:
                    return E_aBDecPastOwnedPropTitleT.S;
                case MORTGAGE_DECLARATIONTitle_hold_type.SP:
                    return E_aBDecPastOwnedPropTitleT.SP;
                case MORTGAGE_DECLARATIONTitle_hold_type.O:
                    return E_aBDecPastOwnedPropTitleT.O;
                case MORTGAGE_DECLARATIONTitle_hold_type.Item:
                    return E_aBDecPastOwnedPropTitleT.Empty;
                default:
                    throw new ArgumentException("Unhandled LPQ Declaration Title hold type " + title);
            }
        }

        /// <summary>
        /// Converts LPQ Property Owned  to LO DecPastOwned 
        /// </summary>
        /// <param name="type">LPQ Type</param>
        /// <returns>LO Type</returns>
        /// <exception cref="ArgumentException">Thrown when Unhandled type is passed.</exception>
        private static E_aBDecPastOwnedPropT MapToLO(MORTGAGE_DECLARATIONType_of_property_owned type)
        {
            switch (type)
            {
                case MORTGAGE_DECLARATIONType_of_property_owned.PR:
                    return E_aBDecPastOwnedPropT.PR;
                case MORTGAGE_DECLARATIONType_of_property_owned.SH:
                    return E_aBDecPastOwnedPropT.SH;
                case MORTGAGE_DECLARATIONType_of_property_owned.IP:
                    return E_aBDecPastOwnedPropT.IP;
                case MORTGAGE_DECLARATIONType_of_property_owned.Item:
                    return E_aBDecPastOwnedPropT.Empty;
                default:
                    throw new ArgumentException("Unhandled LPQ property owned type " + type);
            }
        }

        /// <summary>
        /// Converts the Occupancy status to LO enum Defaults to primary residence for NA and empty string
        /// </summary>
        /// <param name="occupancyStatus"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when an Unhandled type is entered</exception>
        private static E_aOccT MapToLO(MORTGAGE_PROPERTY_INFOOccupancy_status occupancyStatus)
        {
            switch (occupancyStatus)
            {
                case MORTGAGE_PROPERTY_INFOOccupancy_status.INVESTMENT:
                    return E_aOccT.Investment;
                case MORTGAGE_PROPERTY_INFOOccupancy_status.PRIMARYRESIDENCE:
                    return E_aOccT.PrimaryResidence;
                case MORTGAGE_PROPERTY_INFOOccupancy_status.SECONDHOME:
                    return E_aOccT.SecondaryResidence;
                case MORTGAGE_PROPERTY_INFOOccupancy_status.NA:
                    return E_aOccT.PrimaryResidence;
                case MORTGAGE_PROPERTY_INFOOccupancy_status.Item:
                    return E_aOccT.PrimaryResidence;
                default:
                    throw new ArgumentException("Unhandled lpq occupancy status " + occupancyStatus);
            }

        }

        /// <summary>
        /// Converts the given type to E_slt throws argumetn type if its a enum value that was unexpected.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static E_sLT MapToLO(MORTGAGE_LOAN_INFOMortgage_loan_type type)
        {
            switch (type)
            {
                case MORTGAGE_LOAN_INFOMortgage_loan_type.CONV:
                    return E_sLT.Conventional;
                case MORTGAGE_LOAN_INFOMortgage_loan_type.FHA:
                    return E_sLT.FHA;
                case MORTGAGE_LOAN_INFOMortgage_loan_type.VA:
                    return E_sLT.VA;
                case MORTGAGE_LOAN_INFOMortgage_loan_type.USDA:
                    return E_sLT.UsdaRural;
                case MORTGAGE_LOAN_INFOMortgage_loan_type.OTHER:
                    return E_sLT.Other;
                case MORTGAGE_LOAN_INFOMortgage_loan_type.Item:
                    return E_sLT.Other;
                default:
                    throw new ArgumentException("Unhandled LPQ Loan Type " + type);
            }
        }

        private static sHmdaOtherNonAmortFeatureT MapToLOHmdaOtherNonAmortFeatureT(BASE_APPLICANTDeclined_answer_race_gender isOtherNonAmortizing)
        {
            switch (isOtherNonAmortizing)
            {
                case BASE_APPLICANTDeclined_answer_race_gender.Y:
                    return sHmdaOtherNonAmortFeatureT.OtherNonFullyAmortizingFeatures;
                case BASE_APPLICANTDeclined_answer_race_gender.N:
                    return sHmdaOtherNonAmortFeatureT.NoOtherNonFullyAmortizingFeatures;
                case BASE_APPLICANTDeclined_answer_race_gender.Item:
                    return sHmdaOtherNonAmortFeatureT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(isOtherNonAmortizing);
            }
        }

        private static E_TriState MapToLOIsEscrowedAtClosing(BASE_APPLICANTDeclined_answer_race_gender isEscrowed)
        {
            switch (isEscrowed)
            {
                case BASE_APPLICANTDeclined_answer_race_gender.Y:
                    return E_TriState.Yes;
                case BASE_APPLICANTDeclined_answer_race_gender.N:
                    return E_TriState.No;
                case BASE_APPLICANTDeclined_answer_race_gender.Item:
                    return E_TriState.Blank;
                default:
                    throw new UnhandledEnumException(isEscrowed);
            }
        }

        private static E_sFinMethT MapToLO(MORTGAGE_LOAN_INFORate_type rateType)
        {
            switch (rateType)
            {
                case MORTGAGE_LOAN_INFORate_type.F:
                    return E_sFinMethT.Fixed;
                case MORTGAGE_LOAN_INFORate_type.A:
                    return E_sFinMethT.ARM;
                case MORTGAGE_LOAN_INFORate_type.G:
                    return E_sFinMethT.Graduated;
                case MORTGAGE_LOAN_INFORate_type.O:
                    return E_sFinMethT.Fixed;
                case MORTGAGE_LOAN_INFORate_type.Item:
                    return E_sFinMethT.Fixed;
                case MORTGAGE_LOAN_INFORate_type.Item1:
                    return E_sFinMethT.Fixed;
                default:
                    throw new ArgumentException("Unhandled LPQ rate type " + rateType);
            }
        }

        private static E_sEstateHeldT MapToLO(MORTGAGE_LOAN_INFOEstate_held_in val)
        {
            switch (val)
            {
                case MORTGAGE_LOAN_INFOEstate_held_in.F:
                    return E_sEstateHeldT.FeeSimple;
                case MORTGAGE_LOAN_INFOEstate_held_in.L:
                    return E_sEstateHeldT.LeaseHold;
                case MORTGAGE_LOAN_INFOEstate_held_in.Item:
                    return E_sEstateHeldT.FeeSimple;
                default:
                    throw new ArgumentException("Unhandled LPQ estate held in type " + val);
            }
        }

        private static E_sLienPosT MapToLienPosition(string position)
        {
            if (string.IsNullOrEmpty(position) || position == "0")
            {
                return E_sLienPosT.First;
            }

            switch (position)
            {
                case "1":
                    return E_sLienPosT.First;
                case "2":
                    return E_sLienPosT.Second;
                default:
                    throw new ArgumentException("Lien Position is not supported " + position);
            }
        }

        private static decimal Sum(params string[] p)
        {
            decimal sum = 0;
            foreach (var entry in p)
            {
                decimal value;
                if (decimal.TryParse(entry, out value))
                {
                    sum += value;
                }
            }
            return sum;
        }

        private static E_PercentBaseT MapTosProHazInsT(string p)
        {
            if (string.IsNullOrEmpty(p))
            {
                return E_PercentBaseT.LoanAmount;
            }

            switch (p.TrimWhitespaceAndBOM().ToLower())
            {
                case "loan amount":
                    return E_PercentBaseT.LoanAmount;
                case "appraisal price":
                    return E_PercentBaseT.AppraisalValue;
                case "purchase price":
                    return E_PercentBaseT.SalesPrice;
                default:
                    throw new ArgumentException("Unhandled gfe percentage of " + p);
            }
        }

        private static bool GFEYNStringToBool(string a)
        {
            return a == "Y";
        }

        private static int GetGFEDetails(bool isAprSpecified, BASE_APPLICANTDeclined_answer_race_gender isApr, bool isBrokerSpecified, BASE_APPLICANTDeclined_answer_race_gender isBroker, string paidBy, bool isPocSpecified, BASE_APPLICANTDeclined_answer_race_gender ispoc)
        {
            bool apr = false;
            bool broker = false;
            bool poc = false;
            int ipaidBy = 0;

            if (isAprSpecified)
            {
                apr = isApr == BASE_APPLICANTDeclined_answer_race_gender.Y;
            }
            if (isBrokerSpecified)
            {
                broker = isBroker == BASE_APPLICANTDeclined_answer_race_gender.Y;
            }
            if (isPocSpecified)
            {
                poc = ispoc == BASE_APPLICANTDeclined_answer_race_gender.Y;
            }

            if (false == string.IsNullOrEmpty(paidBy))
            {
                switch (paidBy)
                {
                    case "B":
                        ipaidBy = 0;
                        break;
                    case "L":
                        ipaidBy = 3;
                        break;
                    default:
                        throw new ArgumentException("Unhandled paid by in gfe " + paidBy);
                }
            }

            return LosConvert.GfeItemProps_Pack(apr, broker, ipaidBy, false, poc);
        }

        private static E_sProdDocT ToLO(MORTGAGE_LOAN_INFODocument_type mORTGAGE_LOAN_INFODocument_type)
        {
            switch (mORTGAGE_LOAN_INFODocument_type)
            {
                case MORTGAGE_LOAN_INFODocument_type.FULL_DOC:
                    return E_sProdDocT.Full;
                case MORTGAGE_LOAN_INFODocument_type.STATED:
                    return E_sProdDocT.SIVA;
                case MORTGAGE_LOAN_INFODocument_type.NINA:
                    return E_sProdDocT.NINA;
                case MORTGAGE_LOAN_INFODocument_type.Item:
                    return E_sProdDocT.Full;
                default:
                    throw new ArgumentException("Unhandled document type.");
            }
        }

        private static bool IsNonZeroDecimal(string num)
        {
            decimal number;
            return Decimal.TryParse(num, out number) && number > 0;
        }

        #endregion

        private class HmdaDataHelper
        {
            public Dictionary<string, string> BorrowerHmdaData
            {
                get;
                set;
            }

            public Dictionary<string, string> CoborrowerHmdaData
            {
                get;
                set;
            }
        }

        private static string DecodeBase64String(string base64EncodedString)
        {
            byte[] b = System.Convert.FromBase64String(base64EncodedString);
            string xml = System.Text.Encoding.UTF8.GetString(b);
            return xml;
        }

        public static MORTGAGE_LOAN12 DeserializeMortgageLoan(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MORTGAGE_LOAN12));
            MORTGAGE_LOAN12 ml;
            using (StringReader r = new StringReader(xmlData))
            {
                ml = (MORTGAGE_LOAN12)serializer.Deserialize(r);
            }
            return ml;
        }
    }
}
