﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Exporter
{
    using System.Web;

    /// <summary>
    /// This is the request handler for LPQ that will give them the version for the export from LQB.
    /// </summary>
    public class LPQExporterVersionHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether this handler is reusable.
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="context">The http context.</param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Output.Write(APP.GetMaxSupportedVersion());
        }
    }
}
