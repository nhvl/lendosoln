﻿using System;

namespace LendersOffice.ObjLib.Conversions.LoansPQ.Exporter
{
    public interface ILoanFileExporter
    {
        string Export(Guid loanID);
        string ExportTo(Guid loanID, string file);
    }

}
