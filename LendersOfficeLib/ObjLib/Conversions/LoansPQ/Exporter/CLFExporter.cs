﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ.Exporter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// This class is written in such a way that we can both export to an existing file 
    /// or export to a new file. Thats why theres a lot of use of the coalasce operator for setting statements. 
    /// </summary>
    /// 
    public class CLFExporter : ILoanFileExporter
    {
        private CPageData _pageData;
        private MORTGAGE_LOAN12 _loanFile;
        private Guid _loanFileID;
        private IList<BASE_ASSET> _borrowerAssets;
        private IList<BASE_ASSET> _coborrowerAssets;
        private Random _randomnessProvider = new Random();
        private Dictionary<Guid, List<KeyValuePair<Guid, int>>> _newLiabilitiesId; //App + Liability Id + Int 
        private Dictionary<Guid, string> _memberMap = null;
        #region ILoanFileExporter Members

        private bool isNewFile = false;
        public string Export(Guid loanFileID)
        {
            this.isNewFile = true;
            _loanFile = new MORTGAGE_LOAN12();
            _loanFileID = loanFileID;
            return UpdateAndSerializeLoanFile();
        }


        public string Export(Guid loanFileID, Dictionary<Guid, string> info)
        {
            this.isNewFile = true;
            _loanFile = new MORTGAGE_LOAN12();
            _loanFileID = loanFileID;
            _memberMap = info;
            return UpdateAndSerializeLoanFile();
        }

        /// <summary>
        /// Exports the Loan with the given id to the given file.
        /// </summary>
        /// <param name="loanFileID"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Thrown when data could not be parsed.</exception>
        public string ExportTo(Guid loanFileID, string file)
        {
            _loanFileID = loanFileID;
            XmlSerializer serializer = new XmlSerializer(typeof(MORTGAGE_LOAN12));
            using (StringReader reader = new StringReader(file))
            {
                _loanFile = (MORTGAGE_LOAN12)serializer.Deserialize(reader);
            }
            return UpdateAndSerializeLoanFile();
        }
        #endregion

        /// <summary>
        /// Method that initiates the update of the loan file objects and then serializes.
        /// </summary>
        /// <returns></returns>
        private string UpdateAndSerializeLoanFile()
        {
            Update();

            UpdateExistingLiabilityData();


            return Serialize();
        }

        private void UpdateExistingLiabilityData()
        {
            if (_newLiabilitiesId.Count == 0)
            {
                return;
            }

            CPageData data = CPageData.CreateUsingSmartDependency(_loanFileID, typeof(CLFExporter));
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            foreach (KeyValuePair<Guid, List<KeyValuePair<Guid, int>>> application in _newLiabilitiesId)
            {
                CAppData appData = data.GetAppData(application.Key);

                if (appData == null)
                {
                    Tools.LogError("Programming Error Did not find app in UpdateExistingLiabilityData");
                    continue;
                }

                ILiaCollection appLiabs = appData.aLiaCollection;
                foreach (KeyValuePair<Guid, int> liabilityMap in application.Value)
                {
                    ILiabilityRegular newLiability = appLiabs.GetRegRecordOf(liabilityMap.Key);
                    newLiability.SetRecordId(liabilityMap.Value.ToString());
                }
                appLiabs.Flush();
            }

            data.Save();
        }

        /// <summary>
        /// Serializes the mortgage_loan file and returns a string representation.
        /// </summary>
        /// <returns></returns>
        private string Serialize()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = false;
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;
            Encoding ut = Encoding.UTF8;

            XmlSerializer serializer = new XmlSerializer(typeof(MORTGAGE_LOAN12));
            StringBuilder writer = new StringBuilder();
            using (XmlWriter xmlWriter = XmlWriter.Create(writer, settings))
            {
                serializer.Serialize(xmlWriter, _loanFile);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Updates the mortgage_loan object with data from the loan file.
        /// </summary>
        private void Update()
        {
            _pageData = CPageData.CreateUsingSmartDependency(this._loanFileID, typeof(CLFExporter));
            _pageData.SetFormatTarget(FormatTarget.MismoClosing);
            _pageData.ByPassFieldSecurityCheck = true;
            _pageData.InitLoad();
            _newLiabilitiesId = new Dictionary<Guid, List<KeyValuePair<Guid, int>>>();

            ExportApplications();
            ExportPropertyInfo();
            ExportLoanFile();
            ExportTransactionDetails();
            ExportContacts();
            ExportLoanStatus();
            ExportBranch();
            ExportQuestions();

            if (_loanFile.COMMENTS == null)  //required
            {
                _loanFile.COMMENTS = new MORTGAGE_LOANCOMMENTS();
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportQuestions()
        {
            string[] values = GetCustomQuestionAnswers();
            List<BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION> questions = new List<BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION>();
            if (_loanFile.CUSTOM_QUESTIONS == null)
            {
                #region handle new file
                _loanFile.CUSTOM_QUESTIONS = new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION[20];
                for (int x = 0; x < 20; x++)
                {
                    _loanFile.CUSTOM_QUESTIONS[x] = new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION()
                    {
                        question_name = "sCustomField" + (x + 1)
                    };

                    _loanFile.CUSTOM_QUESTIONS[x].CUSTOM_QUESTION_ANSWER = new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER[]
                    {
                        new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER() { answer_text = values[x], answer_value = values[x] }
                    };
                }
                #endregion

            }

            else
            {
                List<BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION> questionList = new List<BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION>(_loanFile.CUSTOM_QUESTIONS);

                for (int x = 0; x < 20; x++)
                {
                    string id = "sCustomField" + (x + 1);

                    BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION currentQuestion = questionList.Where(p => p.question_name.ToLower() == id.ToLower()).FirstOrDefault();
                    if (currentQuestion == null)
                    {
                        questionList.Add(new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTION()
                        {
                            question_name = id,
                            CUSTOM_QUESTION_ANSWER = new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER[] {
                                new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER(){ answer_text = values[x] , answer_value = values[x] }
                            }
                        });
                    }
                    else
                    {

                        currentQuestion.CUSTOM_QUESTION_ANSWER = new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER[]
                             {
                                 new BASE_CUSTOM_QUESTIONSCUSTOM_QUESTIONCUSTOM_QUESTION_ANSWER() { answer_text = values[x], answer_value = values[x] }
                             };
                    }
                }

                _loanFile.CUSTOM_QUESTIONS = questionList.ToArray();
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        /// <returns></returns>
        private string[] GetCustomQuestionAnswers()
        {
            string[] data = new string[] {
                 _pageData.sCustomField1Notes,
                 _pageData.sCustomField2Notes,
                 _pageData.sCustomField3Notes,
                 _pageData.sCustomField4Notes,
                 _pageData.sCustomField5Notes,
                 _pageData.sCustomField6Notes,
                 _pageData.sCustomField7Notes,
                 _pageData.sCustomField8Notes,
                 _pageData.sCustomField9Notes,
                 _pageData.sCustomField10Notes,
                 _pageData.sCustomField11Notes,
                 _pageData.sCustomField12Notes,
                 _pageData.sCustomField13Notes,
                 _pageData.sCustomField14Notes,
                 _pageData.sCustomField15Notes,
                 _pageData.sCustomField16Notes,
                 _pageData.sCustomField17Notes,
                 _pageData.sCustomField18Notes,
                 _pageData.sCustomField19Notes,
                 _pageData.sCustomField20Notes
            };
            return data;
        }



        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportBranch()
        {
            BrokerDB brokerdb = BrokerDB.RetrieveById(_pageData.sBrokerId);

            var lqbSystem = new MORTGAGE_SYSTEM { type = MORTGAGE_SYSTEMType.OTHER, loan_number = _pageData.sLNm, ORGANIZATION = new BASE_PARTY() };

            MORTGAGE_SYSTEM lpqSystem;
            if (_loanFile.SYSTEM == null)
            {
                _loanFile.SYSTEM = new MORTGAGE_SYSTEM[] { new MORTGAGE_SYSTEM() { type = MORTGAGE_SYSTEMType.LPQ }, lqbSystem };
                lpqSystem = _loanFile.SYSTEM[0];
                lpqSystem.ORGANIZATION = new BASE_PARTY();
                lpqSystem.ORGANIZATION.code = brokerdb.LpqOrgCode;
            }
            else if (false == _loanFile.SYSTEM.Any(p => p.type == MORTGAGE_SYSTEMType.LPQ))
            {
                List<MORTGAGE_SYSTEM> sytems = new List<MORTGAGE_SYSTEM>(_loanFile.SYSTEM);
                lpqSystem = new MORTGAGE_SYSTEM() { type = MORTGAGE_SYSTEMType.LPQ };
                sytems.Add(lpqSystem);
                _loanFile.SYSTEM = sytems.ToArray();
            }
            else
            {
                lpqSystem = _loanFile.SYSTEM.First(p => p.type == MORTGAGE_SYSTEMType.LPQ);
            }

            if (lpqSystem.BRANCH == null)
            {
                lpqSystem.BRANCH = new BASE_BRANCH();
            }

            BranchDB bd = new BranchDB(_pageData.sBranchId, _pageData.sBrokerId);
            bd.Retrieve();

            lpqSystem.BRANCH.reference_id = bd.BranchCode;

            if (lpqSystem.LENDER == null)
            {
                lpqSystem.LENDER = new BASE_SYSTEMLENDER();
                lpqSystem.LENDER.code = brokerdb.LpqLenderCode;
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportLoanStatus()
        {
            if (_loanFile.LOAN_STATUS == null)
            {
                _loanFile.LOAN_STATUS = new MORTGAGE_STATUS();
                _loanFile.LOAN_STATUS.is_locked = BASE_APPLICANTDeclined_answer_race_gender.N;
            }
            _loanFile.LOAN_STATUS.approval_date = _pageData.sApprovD_rep;
            _loanFile.LOAN_STATUS.declined_date = _pageData.sRejectD_rep;
            _loanFile.LOAN_STATUS.rate_lock_date = _pageData.sInvestorLockRLckdD_rep;
            _loanFile.LOAN_STATUS.rate_lock_term = _pageData.sInvestorLockRLckdDays_rep;
            _loanFile.LOAN_STATUS.submit_date = _pageData.sSubmitD_rep;
            _loanFile.LOAN_STATUS.app_receive_date = _pageData.sOpenedD_rep;

            if (_memberMap != null) //only do this for export from LO 
            {
                E_sStatusT[] approvedStatuses = new E_sStatusT[] {
                E_sStatusT.Loan_Approved,
                E_sStatusT.Loan_FinalUnderwriting,
                E_sStatusT.Loan_ClearToClose,
                E_sStatusT.Loan_Docs,
                E_sStatusT.Loan_DocsBack,
                E_sStatusT.Loan_FundingConditions,
                E_sStatusT.Loan_Funded,
                E_sStatusT.Loan_Recorded,
                E_sStatusT.Loan_FinalDocs,
                E_sStatusT.Loan_Closed,
                E_sStatusT.Loan_LoanPurchased,
                E_sStatusT.Loan_Shipped,
                //E_sStatusT.Loan_PreProcessing,    // start sk 1/6/2014 opm 145251
                //E_sStatusT.Loan_DocumentCheck,
                //E_sStatusT.Loan_DocumentCheckFailed,
                //E_sStatusT.Loan_PreUnderwriting,
                E_sStatusT.Loan_ConditionReview,
                E_sStatusT.Loan_PreDocQC,
                E_sStatusT.Loan_DocsOrdered,
                E_sStatusT.Loan_DocsDrawn,
                E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                E_sStatusT.Loan_ReadyForSale,
                E_sStatusT.Loan_SubmittedForPurchaseReview,
                E_sStatusT.Loan_InPurchaseReview,
                E_sStatusT.Loan_PrePurchaseConditions,
                E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                E_sStatusT.Loan_InFinalPurchaseReview,
                E_sStatusT.Loan_ClearToPurchase,
                E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                //E_sStatusT.Loan_CounterOffer,
                //E_sStatusT.Loan_Withdrawn,
                //E_sStatusT.Loan_Archived    
            };

                if (approvedStatuses.Contains(_pageData.sStatusT))
                {
                    _loanFile.LOAN_STATUS.loan_status = BASE_LOAN_STATUSLoan_status.APP;
                }
            }
            else
            {
                if (_pageData.sStatusT == E_sStatusT.Loan_Approved)
                {
                    _loanFile.LOAN_STATUS.loan_status = BASE_LOAN_STATUSLoan_status.APP;
                }
            }
            #region missing fields
            //mortgageStatus.delinquency_amount
            //mortgageStatus.delinquency_amountSpecified
            //mortgageStatus.delinquency_period
            //mortgageStatus.existing_balance
            //mortgageStatus.existing_balance_date
            //mortgageStatus.existing_balance_dateSpecified
            //mortgageStatus.existing_balanceSpecified
            //mortgageStatus.initial_entry_timestamp
            //mortgageStatus.is_locked
            //mortgageStatus.last_modified
            //mortgageStatus.last_modifiedSpecified
            //mortgageStatus.loan_status
            //mortgageStatus.next_payment_date
            //mortgageStatus.next_payment_dateSpecified
            //mortgageStatus.payoff_date
            //mortgageStatus.payoff_dateSpecified
            //mortgagestatus.amount_charge_off 
            //mortgageStatus.amount_charge_offSpecified
            //mortgageStatus.approval_date
            //mortgageStatus.approval_dateSpecified
            //mortgageStatus.counter_offer_date
            //mortgageStatus.counter_offer_dateSpecified 
            #endregion
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportContacts()
        {
            IList<MORTGAGE_CONTACTSCONTACT_INFO> contactsToPersist = new List<MORTGAGE_CONTACTSCONTACT_INFO>();

            if (_loanFile.CONTACTS != null)
            {
                foreach (MORTGAGE_CONTACTSCONTACT_INFO contact in _loanFile.CONTACTS)
                {
                    if (contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER || contact.contact_type == MORTGAGE_CONTACTSCONTACT_INFOContact_type.APPLICANT)
                    {
                        contactsToPersist.Add(contact);
                    }
                }
            }


            IList<MORTGAGE_CONTACTSCONTACT_INFO> contacts = new List<MORTGAGE_CONTACTSCONTACT_INFO>();
            CAgentCollection agents = _pageData.sAgentCollection;
            MORTGAGE_CONTACTSCONTACT_INFO currentContact;
            CAgentFields currentAgent;
            for (int i = 0; i < agents.GetAgentRecordCount(); i++)
            {
                currentAgent = agents.GetAgentFields(i);
                if (currentAgent.AgentRoleT == E_AgentRoleT.Lender      //we imported these from system so they should not go as contacts plus if they did they would be type other.
                    || currentAgent.AgentRoleT == E_AgentRoleT.LoanOfficer
                    || currentAgent.AgentRoleT == E_AgentRoleT.Processor)
                {
                    continue;
                }
                MORTGAGE_CONTACTSCONTACT_INFOContact_type lpqtype = FindContactType(currentAgent.AgentRoleT, currentAgent.OtherAgentRoleTDesc);

                if (lpqtype == MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER)
                {
                    continue;
                }
                string[] names = currentAgent.AgentName.Split(new string[] { " " }, 2, StringSplitOptions.RemoveEmptyEntries);
                string licenseNumAgent = currentAgent.LicenseNumOfAgent;
                string licenseNumCompany = currentAgent.LicenseNumOfCompany;

                if (licenseNumAgent != null && licenseNumAgent.Length > 20)
                {
                    licenseNumAgent = licenseNumAgent.Substring(0, 20);
                }

                if (licenseNumCompany != null && licenseNumCompany.Length > 20)
                {
                    licenseNumCompany = licenseNumCompany.Substring(0, 20);
                }

                currentContact = new MORTGAGE_CONTACTSCONTACT_INFO()
                {
                    contact_email = currentAgent.EmailAddr,
                    company_name = currentAgent.CompanyName,
                    department_name = currentAgent.DepartmentName,
                    company_address = currentAgent.StreetAddr,
                    company_city = currentAgent.City,
                    company_fax = currentAgent.FaxNum,
                    company_license_number = licenseNumCompany,
                    agent_license_number = licenseNumAgent,
                    //company_phone = currentAgent.Phone,
                    company_state = currentAgent.State,
                    company_zip = currentAgent.Zip,
                    contact_cell_phone = currentAgent.CellPhone,
                    contact_fax = currentAgent.FaxNum,
                    contact_phone = currentAgent.Phone,
                    contact_pager = currentAgent.PagerNum,
                    case_number = currentAgent.CaseNum,
                    notes = currentAgent.Notes,
                };

                if (names.Length >= 1)
                {
                    currentContact.contact_first_name = names[0];
                }

                if (names.Length >= 2)
                {
                    currentContact.contact_last_name = names[1];
                }

                currentContact.contact_type = lpqtype;
                contacts.Add(currentContact);

            }
            _loanFile.CONTACTS = contacts.Concat(contactsToPersist).ToArray();
        }

        /// <summary>
        /// Tries to get the contact type from the description, if its no tthere then it goes to the agent role. 
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="agentRole"></param>
        /// <param name="description"></param>
        /// <returns>Contact Type</returns>
        /// <exception cref="ArgumentException">Thrown when a Unhandled agent role is seen.</exception>
        private static MORTGAGE_CONTACTSCONTACT_INFOContact_type FindContactType(E_AgentRoleT agentRole, string description)
        {
            MORTGAGE_CONTACTSCONTACT_INFOContact_type contactType;

            if (!TryToMapContactTypeFromAgentRoleDesc(description, out contactType))
            {
                contactType = MapToLoansPQ(agentRole);
            }

            return contactType;
        }
        /// <summary>
        /// Tries to map the other description to a LPQ other contact type. Since users can put in their own description we cannot gurantee
        /// that the importer is the only one that sets it so this method never returns error. It sets contact type to other if it could not 
        /// do the mapping. Currently it supports insurance, flood insurance and collat_off.
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="otherDescription"></param>
        /// <param name="contactType"></param>
        /// <returns>whether the other description matched one of the types.</returns>
        private static bool TryToMapContactTypeFromAgentRoleDesc(string otherDescription, out MORTGAGE_CONTACTSCONTACT_INFOContact_type contactType)
        {
            otherDescription = otherDescription ?? "";
            otherDescription = otherDescription.ToLower().TrimWhitespaceAndBOM();

            contactType = MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;

            if (otherDescription == "flood insurance")
            {
                contactType = MORTGAGE_CONTACTSCONTACT_INFOContact_type.FLOODINS;
                return true;
            }
            else if (otherDescription == "flood cert")
            {
                contactType = MORTGAGE_CONTACTSCONTACT_INFOContact_type.FLOODCERT;
                return true;
            }
            else if (otherDescription == "collat_off")
            {
                contactType = MORTGAGE_CONTACTSCONTACT_INFOContact_type.COLLAT_OFF;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Converts the given LO Role to a LPQ contact type. 
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="e_AgentRoleT"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when a unsupported role type is encountered.</exception>
        private static MORTGAGE_CONTACTSCONTACT_INFOContact_type MapToLoansPQ(E_AgentRoleT e_AgentRoleT)
        {
            switch (e_AgentRoleT)
            {
                case E_AgentRoleT.Other:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Appraiser:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.APPRAISER;
                case E_AgentRoleT.Escrow:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.ESCROW;
                case E_AgentRoleT.CreditReport:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.CREDITRPT;
                case E_AgentRoleT.Title:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.TITLE;
                case E_AgentRoleT.MortgageInsurance:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.MTGINSURAN;
                case E_AgentRoleT.ListingAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.LISTAGENT;
                case E_AgentRoleT.SellingAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLERAGNT;
                case E_AgentRoleT.Builder:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUILDER;
                case E_AgentRoleT.Underwriter:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.UNDERWRITE;
                case E_AgentRoleT.Investor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.INVESTOR;
                case E_AgentRoleT.Servicing:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.SERVICING;
                case E_AgentRoleT.BuyerAttorney:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUYERATTOR;
                case E_AgentRoleT.SellerAttorney:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLERATT;
                case E_AgentRoleT.HomeOwnerInsurance:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.INSURANCE;
                case E_AgentRoleT.HazardInsurance:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.INSURANCE;
                case E_AgentRoleT.MarketingLead:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.MARKETLEAD;
                case E_AgentRoleT.Seller:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.SELLER;
                case E_AgentRoleT.Surveyor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.SURVEYOR;
                case E_AgentRoleT.LoanOfficer:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Bank:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BANK;
                case E_AgentRoleT.Lender:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Processor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.CallCenterAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.CALLCAGT;
                case E_AgentRoleT.LoanOpener:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Manager:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.MANAGER;
                case E_AgentRoleT.Broker:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BROKER;
                case E_AgentRoleT.BrokerRep:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BROKERREP;
                case E_AgentRoleT.ECOA:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.ECOA;
                case E_AgentRoleT.Realtor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.REALTOR;
                case E_AgentRoleT.Mortgagee:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.HomeOwnerAssociation:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.HOA;
                case E_AgentRoleT.BuyerAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.BUYERAGENT;
                case E_AgentRoleT.ClosingAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.CLOSEAGNT;
                case E_AgentRoleT.FairHousingLending:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.FAIRHOUSE;
                case E_AgentRoleT.HomeInspection:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.FloodProvider:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.FLOODINS;          // db - OPM 33365
                case E_AgentRoleT.CreditReportAgency2:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.CreditReportAgency3:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Shipper:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Funder:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.PostCloser:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Insuring:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.CollateralAgent:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.DocDrawer:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.PropertyManagement:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.TitleUnderwriter:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.CreditAuditor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.DisclosureDesk:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.JuniorProcessor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.JuniorUnderwriter:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.LegalAuditor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.LoanOfficerAssistant:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Purchaser:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.QCCompliance:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Secondary:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.BrokerProcessor:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Trustee:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.PestInspection:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Subservicer:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.ExternalSecondary:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.ExternalPostCloser:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.LoanPurchasePayee:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.AppraisalManagementCompany:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                case E_AgentRoleT.Referral:
                    return MORTGAGE_CONTACTSCONTACT_INFOContact_type.OTHER;
                default:
                    throw new ArgumentException("Unhandled Agent Role Type " + e_AgentRoleT);
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportTransactionDetails()
        {
            _loanFile.DETAIL_TRANSACTION = _loanFile.DETAIL_TRANSACTION ?? new MORTGAGE_DETAILS_TRANSACTION();
            _loanFile.DETAIL_TRANSACTION.purchase_price = _pageData.sPurchPrice_rep;
            _loanFile.DETAIL_TRANSACTION.alteration_cost = _pageData.sAltCost_rep;
            _loanFile.DETAIL_TRANSACTION.land_cost = _pageData.sLandCost_rep;
            _loanFile.DETAIL_TRANSACTION.refinance_debts_amount = _pageData.sRefPdOffAmt1003_rep;
            _loanFile.DETAIL_TRANSACTION.estimated_pre_paid_items = _pageData.sTotEstPp1003_rep;
            _loanFile.DETAIL_TRANSACTION.estimated_closing_costs = _pageData.sTotEstCcNoDiscnt1003_rep;
            _loanFile.DETAIL_TRANSACTION.pmi_fee = _pageData.sFfUfmip1003_rep;
            _loanFile.DETAIL_TRANSACTION.point_amount = _pageData.sLDiscnt1003_rep;
            _loanFile.DETAIL_TRANSACTION.subordinate_finance_amount = _pageData.sONewFinBal_rep;
            _loanFile.DETAIL_TRANSACTION.closing_cost_paid_by_seller_amount = _pageData.sTotCcPbs_rep;
            _loanFile.DETAIL_TRANSACTION.other_credit_type_1 = MapCreditTypeToLoansPQ(_pageData.sOCredit1Desc);
            _loanFile.DETAIL_TRANSACTION.other_credit_amount_1 = _pageData.sOCredit1Amt_rep;
            _loanFile.DETAIL_TRANSACTION.other_credit_type_2 = MapCreditTypeToLoansPQ(_pageData.sOCredit2Desc);
            _loanFile.DETAIL_TRANSACTION.other_credit_amount_2 = _pageData.sOCredit2Amt_rep;
            _loanFile.DETAIL_TRANSACTION.other_credit_type_3 = MapCreditTypeToLoansPQ(_pageData.sOCredit3Desc);
            _loanFile.DETAIL_TRANSACTION.other_credit_amount_3 = _pageData.sOCredit3Amt_rep;
            _loanFile.DETAIL_TRANSACTION.other_credit_type_4 = MapCreditTypeToLoansPQ(_pageData.sOCredit4Desc);
            _loanFile.DETAIL_TRANSACTION.other_credit_amount_4 = _pageData.sOCredit4Amt_rep;
            _loanFile.DETAIL_TRANSACTION.pmi_fee_financed = _pageData.sFfUfmipFinanced_rep;

        }

        /// <summary>
        /// Maps the other credit type from lo to LPQ. This doesnt throw an exception because LO allows free text. 
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static other_credit_type MapCreditTypeToLoansPQ(string p)
        {
            p = p ?? "";
            switch (p.ToLower().TrimWhitespaceAndBOM())
            {
                case "cash deposit on sales contract":
                    return other_credit_type.CASHDEPOSITONSALESCONTRACT;
                case "seller credit":
                    return other_credit_type.SELLERCREDIT;
                case "lender credit":
                    return other_credit_type.LENDERCREDIT;
                case "relocation funds":
                    return other_credit_type.RELOCATIONFUNDS;
                case "employer assisted housing":
                    return other_credit_type.EMPLOYERASSISTEDHOUSING;
                case "lease purchase fund":
                    return other_credit_type.LEASEPURCHASEFUND;
                case "other":
                    return other_credit_type.OTHER;
                case "borrower paid fees":
                    return other_credit_type.BORROWERPAIDFEES;
                case "":
                    return other_credit_type.Item;
                default:
                    return other_credit_type.OTHER;
            }
        }

        private static string TrimIfNeeded(string input, int size)
        {
            if (input.Length > size)
            {
                return input.Substring(0, size);
            }
            return input;
        }
        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportLoanFile()
        {
            _loanFile.LOAN_INFO = _loanFile.LOAN_INFO ?? new MORTGAGE_LOANLOAN_INFO();
            MORTGAGE_LOANLOAN_INFO loanInfo = _loanFile.LOAN_INFO;
            loanInfo.PROPOSED_HOUSING_EXPENSE = loanInfo.PROPOSED_HOUSING_EXPENSE ?? new MORTGAGE_LOAN_INFOPROPOSED_HOUSING_EXPENSE();
            loanInfo.PROPOSED_HOUSING_EXPENSE.hazard_insurance = _pageData.sProHazIns_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.mortgage_insurance = _pageData.sProMIns_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.other_expense = _pageData.sProOHExp_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.real_estate_tax = _pageData.sProRealETx_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.hoa_due = _pageData.sProHoAssocDues_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.monthly_payment = _pageData.sProFirstMPmt_rep;
            loanInfo.PROPOSED_HOUSING_EXPENSE.other_pi = _pageData.sProSecondMPmt_rep;
            loanInfo.point = _pageData.sLDiscntPc_rep;
            loanInfo.floor = _pageData.sRAdjFloorR_rep;
            loanInfo.margin = _pageData.sRAdjMarginR_rep;
            loanInfo.officer_program_name = _pageData.sLpTemplateNm;
            loanInfo.ceiling = _pageData.sRLifeCapR_rep;
            loanInfo.index = _pageData.sRAdjIndexR_rep;
            if (this.isNewFile)
            {
                this.ExportCurrentTrustDeed();
            }

            ExportLoanProductData();

            ExportGovernmentData();

            ExportFreddieMac();

            #region loan type
            if (_pageData.sLT == E_sLT.Conventional)
            {
                loanInfo.mortgage_loan_type = MORTGAGE_LOAN_INFOMortgage_loan_type.CONV;
                loanInfo.mortgage_loan_typeSpecified = true;
            }
            else if (_pageData.sLT == E_sLT.FHA)
            {
                loanInfo.mortgage_loan_type = MORTGAGE_LOAN_INFOMortgage_loan_type.FHA;
                loanInfo.mortgage_loan_typeSpecified = true;
            }

            else if (_pageData.sLT == E_sLT.Other)
            {
                loanInfo.mortgage_loan_type = MORTGAGE_LOAN_INFOMortgage_loan_type.OTHER;
                loanInfo.mortgage_loan_type_other_desc = _pageData.sLTODesc;
                loanInfo.mortgage_loan_typeSpecified = true;
            }
            else if (_pageData.sLT == E_sLT.UsdaRural)
            {
                loanInfo.mortgage_loan_type = MORTGAGE_LOAN_INFOMortgage_loan_type.USDA;
                loanInfo.mortgage_loan_typeSpecified = true;
            }
            else if (_pageData.sLT == E_sLT.VA)
            {
                loanInfo.mortgage_loan_type = MORTGAGE_LOAN_INFOMortgage_loan_type.VA;
                loanInfo.mortgage_loan_typeSpecified = true;
            }

            #endregion


            #region loan purpose
            if (_pageData.sLPurposeT == E_sLPurposeT.Construct)
            {
                loanInfo.purpose = MORTGAGE_LOAN_INFOPurpose.CONSTRUCTION;
                loanInfo.purposeSpecified = true;
            }
            else if (_pageData.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                loanInfo.purpose = MORTGAGE_LOAN_INFOPurpose.CONSTRUCTIONPERM;
                loanInfo.purposeSpecified = true;
            }
            else if (_pageData.sLPurposeT == E_sLPurposeT.Purchase)
            {
                loanInfo.purpose = MORTGAGE_LOAN_INFOPurpose.PURCHASE;
                loanInfo.purposeSpecified = true;
            }
            else if (_pageData.sLPurposeT == E_sLPurposeT.Refin ||
                     _pageData.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance ||
                     _pageData.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                loanInfo.purpose = MORTGAGE_LOAN_INFOPurpose.REFINANCE;
                loanInfo.purposeSpecified = true;
            }
            else if (_pageData.sLPurposeT == E_sLPurposeT.RefinCashout)
            {
                loanInfo.purpose = MORTGAGE_LOAN_INFOPurpose.REFINANCECASHOUT;
                loanInfo.purposeSpecified = true;
            }
            else if (_pageData.sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                this.ExportHomeEquityData();
            }
            #endregion

            loanInfo.refinance_purpose = MapToLoansPQRefinancePurpose(_pageData.sGseRefPurposeT);
            loanInfo.estimated_property_value = _pageData.sApprVal_rep;
            loanInfo.amount_cash_out = _pageData.sProdCashoutAmt_rep;
            loanInfo.amount_approved = _pageData.sIsLineOfCredit && _pageData.sLPurposeT == E_sLPurposeT.HomeEquity ? _pageData.sCreditLineAmt_rep : _pageData.sFinalLAmt_rep;
            if (!this.isNewFile && _memberMap != null)
            {
                loanInfo.amount_requested = _pageData.sFinalLAmt_rep;
            }
            loanInfo.loan_term = _pageData.sTerm_rep;
            loanInfo.down_payment = _pageData.sEquityCalc_rep;
            loanInfo.rate = _pageData.sLPurposeT == E_sLPurposeT.HomeEquity && _pageData.sIsLineOfCredit ? _pageData.sFullyIndexedR_rep : _pageData.sNoteIR_rep;

            #region funding 
            if (_loanFile.FUNDING == null)
            {
                _loanFile.FUNDING = new MORTGAGE_LOANFUNDING();
                _loanFile.FUNDING.SKIP_PAYMENT_INFO = new SKIP_PAYMENT_INFO();
                _loanFile.FUNDING.INSURANCE_OPTIONS = new MORTGAGE_LOANFUNDINGINSURANCE_OPTIONS();
                _loanFile.FUNDING.INSURANCE_OPTIONS.loan_class = MORTGAGE_LOANFUNDINGINSURANCE_OPTIONSLoan_class.C;
                if (this.isNewFile && _pageData.sIsLineOfCredit)
                {
                    // For new HELOC files, we want to send over loan_class "O".
                    _loanFile.FUNDING.INSURANCE_OPTIONS.loan_class = MORTGAGE_LOANFUNDINGINSURANCE_OPTIONSLoan_class.O;
                }

                _loanFile.FUNDING.INSURANCE_OPTIONS.loan_classSpecified = true;

            }

            _loanFile.FUNDING.last_payment_date = _pageData.sLoanMaturityD_rep;
            _loanFile.FUNDING.initial_amount_advanced = _pageData.sFinalLAmt_rep;
            _loanFile.FUNDING.rate = _pageData.sNoteIR_rep;
            _loanFile.FUNDING.loan_date = _pageData.sDocsD_rep;
            _loanFile.FUNDING.loan_term = _pageData.sTerm_rep;
            _loanFile.FUNDING.first_payment_date = _pageData.sSchedDueD1_rep;

            _loanFile.FUNDING.loc_amount = _pageData.sIsLineOfCredit && _pageData.sLPurposeT == E_sLPurposeT.HomeEquity ? _pageData.sCreditLineAmt_rep : _pageData.sFinalLAmt_rep;
            _loanFile.FUNDING.loc_amountSpecified = true;

            if (_pageData.BrokerDB.BrokerID == new Guid("B27CCD70-82DF-4A90-90D4-9953289B7000"))
            {
                _loanFile.FUNDING.funding_date = _pageData.sFundD_rep;
                if (_pageData.sFundD.IsValid)
                {
                    _loanFile.FUNDING.funded_status = BASE_FUNDING_INFOFunded_status.FUN;
                }
            }
            else
            {
                _loanFile.FUNDING.funding_date = _pageData.sEstCloseD_rep;
            }
            _loanFile.FUNDING.funding_dateSpecified = true;

            switch (_pageData.sMiCompanyNmT)
            {
                case E_sMiCompanyNmT.MGIC:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.MGIC;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.PMI:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.PMI;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.UnitedGuaranty:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.UG;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.RMIC:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.RMIC;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.Radian:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.RGI;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.Triad:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.TRIAD;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.CMG:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.CMG;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;
                case E_sMiCompanyNmT.LeaveBlank:
                    _loanFile.FUNDING.mi_insurer_code = MORTGAGE_FUNDING_INFOMi_insurer_code.Item;
                    _loanFile.FUNDING.mi_insurer_codeSpecified = true;
                    break;

                case E_sMiCompanyNmT.Essent:
                case E_sMiCompanyNmT.Genworth:
                case E_sMiCompanyNmT.Amerin:
                case E_sMiCompanyNmT.CAHLIF:
                case E_sMiCompanyNmT.CMGPre94:
                case E_sMiCompanyNmT.Commonwealth:
                case E_sMiCompanyNmT.MDHousing:
                case E_sMiCompanyNmT.MIF:

                case E_sMiCompanyNmT.RMICNC:
                case E_sMiCompanyNmT.SONYMA:

                case E_sMiCompanyNmT.Verex:
                case E_sMiCompanyNmT.WiscMtgAssr:
                case E_sMiCompanyNmT.Arch:
                case E_sMiCompanyNmT.MassHousing:
                    break;
                default:
                    Tools.LogBug("Unhandled sMICompanyNmT " + _pageData.sMiCompanyNmT);
                    break;
            }

            if (_pageData.sLPurposeT == E_sLPurposeT.HomeEquity && _pageData.sIsLineOfCredit)
            {
                _loanFile.FUNDING.initial_rate = _pageData.sNoteIR_rep;
            }
            if (this.isNewFile && _pageData.sInitialRateTermExpirationDate.IsValid)
            {
                _loanFile.FUNDING.rate_expire_date = _pageData.sInitialRateTermExpirationDate_rep;
            }

            #endregion

            #region rate type
            if (_pageData.sFinMethT == E_sFinMethT.ARM)
            {
                loanInfo.rate_type = MORTGAGE_LOAN_INFORate_type.A;
                loanInfo.rate_typeSpecified = true;
            }
            else if (_pageData.sFinMethT == E_sFinMethT.Fixed)
            {
                loanInfo.rate_type = MORTGAGE_LOAN_INFORate_type.F;
                loanInfo.rate_typeSpecified = true;
            }
            else if (_pageData.sFinMethT == E_sFinMethT.Graduated)
            {
                loanInfo.rate_type = MORTGAGE_LOAN_INFORate_type.G;
                loanInfo.rate_typeSpecified = true;
            }
            #endregion

            loanInfo.document_type = ToLPQ(_pageData.sProdDocT);
            loanInfo.rate_type_other_explanation = _pageData.sFinMethPrintAsOtherDesc;
            loanInfo.arm_description = _pageData.sFinMethDesc;

            if (_pageData.sEstateHeldT == E_sEstateHeldT.FeeSimple)
            {
                loanInfo.estate_held_in = MORTGAGE_LOAN_INFOEstate_held_in.F;
                loanInfo.estate_held_inSpecified = true;
            }
            else if (_pageData.sEstateHeldT == E_sEstateHeldT.LeaseHold)
            {
                loanInfo.estate_held_inSpecified = true;
                loanInfo.estate_held_in = MORTGAGE_LOAN_INFOEstate_held_in.L;
            }

            loanInfo.down_payment_source = MapDownPmntSrcToLoansPQ(_pageData.sDwnPmtSrc);
            loanInfo.down_payment_sourceSpecified = true;

            loanInfo.down_payment_description = _pageData.sDwnPmtSrcExplain;

            if (_pageData.sLienPosT == E_sLienPosT.First)
            {
                loanInfo.lien_position = "1";
            }
            else if (_pageData.sLienPosT == E_sLienPosT.Second)
            {
                loanInfo.lien_position = "2";
            }

            if (_pageData.sSpValuationEffectiveD.IsValid)
            {
                // OPM 132520 - Map property_value_source to sSpValuationMethodT
                switch (_pageData.sSpValuationMethodT)
                {
                    case E_sSpValuationMethodT.AutomatedValuationModel:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.AVM;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.DesktopAppraisal:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.DESKTOP;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.DriveBy:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.DRIVE_BY;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.FullAppraisal:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.APPRAISAL;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.PriorAppraisalUsed:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.EXISTAPRSL;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.None:
                        loanInfo.property_value_source = MORTGAGE_LOAN_INFOProperty_value_source.Item;
                        loanInfo.property_value_sourceSpecified = true;
                        break;
                    case E_sSpValuationMethodT.LeaveBlank:
                    case E_sSpValuationMethodT.Other:
                    case E_sSpValuationMethodT.FieldReview:
                    case E_sSpValuationMethodT.DeskReview:
                        break;
                    default:
                        throw new UnhandledEnumException(loanInfo.property_value_source);
                }

                loanInfo.property_value_source_date = _pageData.sSpValuationEffectiveD_rep;
            }

            //loanInfo.is_lot_loan;
            //loanInfo.lot_purpose;
            //loanInfo.is_home_equity; 
            //loanInfo.amount_approved;
            //loanInfo.contract_received_date;
            loanInfo.apr = _pageData.sApr_rep;
            //loanInfo.point;
            //loanInfo.request_ltv;
            //loanInfo.misc_implant;
            //loanInfo.consumer_product_code;
            //loanInfo.consumer_program_name;
            //loanInfo.officer_product_code;
            //loanInfo.officer_program_name;
            loanInfo.monthly_payment = _pageData.sProThisMPmt_rep;
            //loanInfo.case_state;
            //loanInfo.number_of_payments;
            //loanInfo.officer_min_payment;
            //loanInfo.officer_is_payment_amortized;
            //loanInfo.officer_payment_percent;
            //loanInfo.consumer_min_payment;
            //loanInfo.consumer_payment_percent;
            //loanInfo.GROUP_INDEX_MARGIN_FLOOR_CEILING;
            //loanInfo.is_balloon;
            //loanInfo.purchaser;
            //loanInfo.amount_approved_variance;
            //loanInfo.is_LOC;     -- take a look at funding this field is used in that section.

            //checked for specified


            loanInfo.LEGAL_DESCRIPTION = _pageData.sSpLegalDesc;


            #region vesting [title]
            List<MORTGAGE_LOAN_INFOVESTING> vestings = new List<MORTGAGE_LOAN_INFOVESTING>();
            if (loanInfo.VESTING != null)
            {
                /* LPQ sends us all 10 vesting records even if they are completely empty.
                 * In our efforts to preserve the last 8 records, we don't touch them. 
                 * However, when LPQ gets back the empty records that they sent over, they add default values to the vesting_name
                 * and it looks like we're populating the last 8 records. 
                 * So to prevent this, I'm removing any records that are completely blank.
                 */
                vestings = new List<MORTGAGE_LOAN_INFOVESTING>(loanInfo.VESTING);
            }

            while (vestings.Count < 2)
            {
                vestings.Add(new MORTGAGE_LOAN_INFOVESTING());
            }

            if (!string.IsNullOrEmpty(_pageData.GetAppData(0).aTitleNm1))
            {
                vestings[0].vesting_name = _pageData.GetAppData(0).aTitleNm1;
                vestings[0].vesting_is_on_appSpecified = true;
                vestings[0].vesting_is_on_app = BASE_APPLICANTDeclined_answer_race_gender.Y;
            }

            if (!string.IsNullOrEmpty(_pageData.GetAppData(0).aTitleNm2))
            {
                vestings[1].vesting_name = _pageData.GetAppData(0).aTitleNm2;
                vestings[1].vesting_is_on_appSpecified = true;
                vestings[1].vesting_is_on_app = BASE_APPLICANTDeclined_answer_race_gender.Y;
            }

            loanInfo.VESTING = vestings.Where((vesting) =>
            {
                return (!string.IsNullOrEmpty(vesting.vesting_contact_index) && vesting.vesting_contact_index != "0") ||
                       !string.IsNullOrEmpty(vesting.vesting_description) ||
                       !string.IsNullOrEmpty(vesting.vesting_marital_status) ||
                       !string.IsNullOrEmpty(vesting.vesting_name);
            }).ToArray();

            #endregion

            if (loanInfo.GFE == null)
            {
                loanInfo.GFE = new MORTGAGE_LOAN_INFOGFE();
            }
            ExportGFE(loanInfo.GFE);
        }

        private void ExportCurrentTrustDeed()
        {
            MORTGAGE_CURRENT_TD currentTrustDeed = new MORTGAGE_CURRENT_TD();
            foreach (CReFields realEstate in _pageData.GetAppData(0).aReCollection.GetSubcollection(true, E_ReoGroupT.All))
            {
                if (realEstate.IsSubjectProp)
                {
                    var firstLinkedCreditor = realEstate.LinkedLia?.Select(lia => lia.ComNm)?.FirstOrDefault(comNm => !string.IsNullOrEmpty(comNm));
                    if (firstLinkedCreditor != null)
                    {
                        currentTrustDeed.lender = firstLinkedCreditor;
                        break;
                    }
                }
            }

            currentTrustDeed.balance = _pageData.sRemain1stMBal_rep;
            currentTrustDeed.payment = _pageData.sRemain1stMPmt_rep;
            // The position is determined by the current loan. If the current loan is a 2nd, then this data is data about the 1st and vice versa.
            currentTrustDeed.position = _pageData.sLienPosT == E_sLienPosT.Second ? "1" : "2";

            _loanFile.LOAN_INFO.CURRENT_TRUST_DEED = new MORTGAGE_CURRENT_TD[] { currentTrustDeed };
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="gfe"></param>
        private void ExportGFE(MORTGAGE_LOAN_INFOGFE gfe)
        {

            switch (gfe._gfeversion)
            {
                case "2010":
                    ExportGFE2010(gfe);
                    break;
                case "2009":
                    ExportGFE2009(gfe);
                    break;
                default:  //dont assume old version assume new
                    gfe._gfeversion = "2010";
                    ExportGFE2010(gfe);
                    break;
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="gfe"></param>
        private void ExportGFE2009(MORTGAGE_LOAN_INFOGFE gfe)
        {


            IPreparerFields gfeTil = _pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (false == gfeTil.IsEmpty)
            {
                gfe._prepareddate = gfeTil.PrepareDate_rep;
            }

            gfe._daysinyear = _pageData.sDaysInYr_rep;

            #region 800

            gfe._801percentagefee = _pageData.sLOrigFPc_rep;
            gfe._801flatfee = _pageData.sLOrigFMb_rep;
            gfe._801isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sLOrigFProps));
            gfe._801ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sLOrigFProps));
            gfe._801paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sLOrigFProps));
            gfe._801poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sLOrigFProps));

            gfe._802percentagefee = _pageData.sLDiscntPc_rep;
            gfe._802flatfee = _pageData.sLDiscntFMb_rep;
            gfe._802isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sLDiscntProps));
            gfe._802ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sLDiscntProps));
            gfe._802paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sLDiscntProps));
            gfe._802poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sLDiscntProps));

            gfe._803amount = _pageData.sApprF_rep;
            gfe._803ispaid = ToYN(_pageData.sApprFPaid);
            gfe._803isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sApprFProps));
            gfe._803ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sApprFProps));
            gfe._803paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sApprFProps));
            gfe._803poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sApprFProps));

            gfe._804amount = _pageData.sCrF_rep;
            gfe._804ispaid = ToYN(_pageData.sCrFPaid);
            gfe._804isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sCrFProps));
            gfe._804ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sCrFProps));
            gfe._804paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sCrFProps));
            gfe._804poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sCrFProps));

            gfe._805amount = _pageData.sInspectF_rep;
            gfe._805isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sInspectFProps));
            gfe._805ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sInspectFProps));
            gfe._805paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sInspectFProps));
            gfe._805poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sInspectFProps));

            gfe._808percentagefee = _pageData.sMBrokFPc_rep;
            gfe._808flatfee = _pageData.sMBrokFMb_rep;
            gfe._808isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMBrokFProps));
            gfe._808ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMBrokFProps));
            gfe._808paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMBrokFProps));
            gfe._808poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMBrokFProps));

            gfe._809amount = _pageData.sTxServF_rep;
            gfe._809isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sTxServFProps));
            gfe._809ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sTxServFProps));
            gfe._809paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sTxServFProps));
            gfe._809poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sTxServFProps));

            gfe._810amount = _pageData.sProcF_rep;
            gfe._810ispaid = ToYN(_pageData.sProcFPaid);
            gfe._810isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sProcFProps));
            gfe._810ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sProcFProps));
            gfe._810paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sProcFProps));
            gfe._810poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sProcFProps));

            gfe._811amount = _pageData.sUwF_rep;
            gfe._811isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sUwFProps));
            gfe._811ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sUwFProps));
            gfe._811paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sUwFProps));
            gfe._811poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sUwFProps));

            gfe._812amount = _pageData.sWireF_rep;
            gfe._812isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sWireFProps));
            gfe._812ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sWireFProps));
            gfe._812paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sWireFProps));
            gfe._812poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sWireFProps));


            gfe._800code1 = _pageData.s800U1FCode;
            gfe._800code2 = _pageData.s800U2FCode;
            gfe._800code3 = _pageData.s800U3FCode;
            gfe._800code4 = _pageData.s800U4FCode;
            gfe._800code5 = _pageData.s800U5FCode;

            gfe._800description1 = TrimIfNeeded(_pageData.s800U1FDesc, 50);
            gfe._800description2 = TrimIfNeeded(_pageData.s800U2FDesc, 50);
            gfe._800description3 = TrimIfNeeded(_pageData.s800U3FDesc, 50);
            gfe._800description4 = TrimIfNeeded(_pageData.s800U4FDesc, 50);
            gfe._800description5 = TrimIfNeeded(_pageData.s800U5FDesc, 50);

            gfe._800amount1 = _pageData.s800U1F_rep;
            gfe._800amount2 = _pageData.s800U2F_rep;
            gfe._800amount3 = _pageData.s800U3F_rep;
            gfe._800amount4 = _pageData.s800U4F_rep;
            gfe._800amount5 = _pageData.s800U5F_rep;

            gfe._800isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U1FProps));
            gfe._800ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U1FProps));
            gfe._800paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U1FProps));
            gfe._800poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U1FProps));
            gfe._800isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U2FProps));
            gfe._800ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U2FProps));
            gfe._800paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U2FProps));
            gfe._800poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U2FProps));
            gfe._800isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U3FProps));
            gfe._800ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U3FProps));
            gfe._800paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U3FProps));
            gfe._800poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U3FProps));
            gfe._800isapr4 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U4FProps));
            gfe._800ispaidtobroker4 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U4FProps));
            gfe._800paidby4 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U4FProps));
            gfe._800poc4 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U4FProps));
            gfe._800isapr5 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U5FProps));
            gfe._800ispaidtobroker5 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U5FProps));
            gfe._800paidby5 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U5FProps));
            gfe._800poc5 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U5FProps));

            #endregion

            #region 900
            gfe._901numdays = _pageData.sIPiaDy_rep;
            //gfe._901amount = _pageData.sIPia;     // field is auto calculated on LPQ
            gfe._901isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sIPiaProps));
            gfe._901ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sIPiaProps));
            gfe._901paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sIPiaProps));
            gfe._901poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sIPiaProps));

            //To do what to do for mortgage insurance premium?
            gfe._902isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMipPiaProps));
            gfe._902ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMipPiaProps));
            gfe._902paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMipPiaProps));
            gfe._902poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMipPiaProps));
            gfe._902amount = _pageData.sMipPia_rep;    //why was this not here before? av-s

            gfe._903nummonths = _pageData.sHazInsPiaMon_rep;
            gfe._903amountpermonth = decimal.ToDouble(_pageData.sProHazIns);
            gfe._903percentageof = FeeFromEnumToString(_pageData.sProHazInsT.ToString(), "903");
            gfe._903percentagefee = _pageData.sProHazInsR_rep;

            gfe._903isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sHazInsPiaProps));
            gfe._903ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sHazInsPiaProps));
            gfe._903paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sHazInsPiaProps));
            gfe._903poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sHazInsPiaProps));

            gfe._904description = TrimIfNeeded(_pageData.s904PiaDesc, 50);
            gfe._904amount = _pageData.s904Pia_rep;
            gfe._904isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s904PiaProps));
            gfe._904ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s904PiaProps));
            gfe._904paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s904PiaProps));
            gfe._904poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s904PiaProps));


            gfe._905isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sVaFfProps));
            gfe._905ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sVaFfProps));
            gfe._905paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sVaFfProps));
            gfe._905poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sVaFfProps));

            gfe._900code1 = _pageData.s900U1PiaCode;
            gfe._900description1 = TrimIfNeeded(_pageData.s900U1PiaDesc, 50);
            gfe._900amount1 = _pageData.s900U1Pia_rep;
            gfe._900isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s900U1PiaProps));
            gfe._900ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s900U1PiaProps));
            gfe._900paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s900U1PiaProps));
            gfe._900poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s900U1PiaProps));
            #endregion

            #region 1000
            gfe._1001nummonths = _pageData.sHazInsRsrvMon_rep;
            //_pageData.sProHazIns
            gfe._1001isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sHazInsRsrvProps));
            gfe._1001ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sHazInsRsrvProps));
            gfe._1001paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sHazInsRsrvProps));
            gfe._1001poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sHazInsRsrvProps));

            gfe._1002percentagefee = _pageData.sProMInsR_rep;
            gfe._1002percentageof = FeeFromEnumToString(_pageData.sProMInsT.ToString(), "1002");
            gfe._1002amountpermonth = _pageData.sProMInsMb_rep;
            gfe._1002nummonths = _pageData.sMInsRsrvMon_rep;
            gfe._1002isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMInsRsrvProps));
            gfe._1002ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMInsRsrvProps));
            gfe._1002paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMInsRsrvProps));
            gfe._1002poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMInsRsrvProps));

            gfe._1003nummonths = _pageData.sSchoolTxRsrvMon_rep;
            gfe._1003amountpermonth = _pageData.sProSchoolTx_rep;
            gfe._1003isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sSchoolTxRsrvProps));
            gfe._1003ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sSchoolTxRsrvProps));
            gfe._1003paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sSchoolTxRsrvProps));
            gfe._1003poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sSchoolTxRsrvProps));

            gfe._1004percentagefee = _pageData.sProRealETxR_rep;
            gfe._1004percentageof = FeeFromEnumToString(_pageData.sProRealETxT.ToString(), "1004");
            gfe._1004amountpermonth = _pageData.sProRealETxMb_rep;
            gfe._1004nummonths = _pageData.sRealETxRsrvMon_rep;
            gfe._1004isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sRealETxRsrvProps));
            gfe._1004ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sRealETxRsrvProps));
            gfe._1004paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sRealETxRsrvProps));
            gfe._1004poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sRealETxRsrvProps));

            gfe._1005nummonths = _pageData.sFloodInsRsrvMon_rep;
            gfe._1005description = "Flood Insurance";
            gfe._1005amountpermonth = _pageData.sProFloodIns_rep;
            gfe._1005isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sFloodInsRsrvProps));
            gfe._1005ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sFloodInsRsrvProps));
            gfe._1005paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sFloodInsRsrvProps));
            gfe._1005poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sFloodInsRsrvProps));

            gfe._1006description = TrimIfNeeded(_pageData.s1006ProHExpDesc, 50);
            gfe._1006nummonths = _pageData.s1006RsrvMon_rep;
            gfe._1006amountpermonth = _pageData.s1006ProHExp_rep;
            gfe._1006isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s1006RsrvProps));
            gfe._1006ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s1006RsrvProps));
            gfe._1006paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s1006RsrvProps));
            gfe._1006poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s1006RsrvProps));

            gfe._1007description = TrimIfNeeded(_pageData.s1007ProHExpDesc, 50);
            gfe._1007nummonths = _pageData.s1007RsrvMon_rep;
            gfe._1007amountpermonth = _pageData.s1007ProHExp_rep;
            gfe._1007isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s1007RsrvProps));
            gfe._1007ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s1007RsrvProps));
            gfe._1007paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s1007RsrvProps));
            gfe._1007poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s1007RsrvProps));


            gfe._1008amount = _pageData.sAggregateAdjRsrv_rep;
            gfe._1008isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sAggregateAdjRsrvProps));
            gfe._1008ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sAggregateAdjRsrvProps));
            gfe._1008paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sAggregateAdjRsrvProps));
            gfe._1008poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sAggregateAdjRsrvProps));

            #endregion

            #region 1100
            gfe._1101description = TrimIfNeeded(_pageData.sEscrowFTable, 50);
            gfe._1101amount = _pageData.sEscrowF_rep;
            gfe._1101isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sEscrowFProps));
            gfe._1101ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sEscrowFProps));
            gfe._1101paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sEscrowFProps));
            gfe._1101poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sEscrowFProps));

            gfe._1105amount = _pageData.sDocPrepF_rep;
            gfe._1105isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sDocPrepFProps));
            gfe._1105ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sDocPrepFProps));
            gfe._1105paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sDocPrepFProps));
            gfe._1105poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sDocPrepFProps));

            gfe._1106amount = _pageData.sNotaryF_rep;
            gfe._1106isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sNotaryFProps));
            gfe._1106ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sNotaryFProps));
            gfe._1106paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sNotaryFProps));
            gfe._1106poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sNotaryFProps));

            gfe._1107amount = _pageData.sAttorneyF_rep;
            gfe._1107isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sAttorneyFProps));
            gfe._1107ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sAttorneyFProps));
            gfe._1107paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sAttorneyFProps));
            gfe._1107poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sAttorneyFProps));

            gfe._1108amount = _pageData.sTitleInsF_rep;
            gfe._1108description = TrimIfNeeded(_pageData.sTitleInsFTable, 50);
            gfe._1108isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sTitleInsFProps));
            gfe._1108ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sTitleInsFProps));
            gfe._1108paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sTitleInsFProps));
            gfe._1108poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sTitleInsFProps));


            gfe._1100code1 = _pageData.sU1TcCode;
            gfe._1100amount1 = _pageData.sU1Tc_rep;
            gfe._1100description1 = TrimIfNeeded(_pageData.sU1TcDesc, 50);
            gfe._1100isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1TcProps));
            gfe._1100ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1TcProps));
            gfe._1100paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1TcProps));
            gfe._1100poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1TcProps));


            gfe._1100code2 = _pageData.sU2TcCode;
            gfe._1100amount2 = _pageData.sU2Tc_rep;
            gfe._1100description2 = TrimIfNeeded(_pageData.sU2TcDesc, 50);
            gfe._1100isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2TcProps));
            gfe._1100ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2TcProps));
            gfe._1100paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2TcProps));
            gfe._1100poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2TcProps));


            gfe._1100code3 = _pageData.sU3TcCode;
            gfe._1100amount3 = _pageData.sU3Tc_rep;
            gfe._1100description3 = TrimIfNeeded(_pageData.sU3TcDesc, 50);
            gfe._1100isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU3TcProps));
            gfe._1100ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU3TcProps));
            gfe._1100paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU3TcProps));
            gfe._1100poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU3TcProps));


            gfe._1100code4 = _pageData.sU4TcCode;
            gfe._1100amount4 = _pageData.sU4Tc_rep;
            gfe._1100description4 = TrimIfNeeded(_pageData.sU4TcDesc, 50);
            gfe._1100isapr4 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU4TcProps));
            gfe._1100ispaidtobroker4 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU4TcProps));
            gfe._1100paidby4 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU4TcProps));
            gfe._1100poc4 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU4TcProps));



            #endregion

            #region 1200
            gfe._1201description = TrimIfNeeded(_pageData.sRecFDesc, 50);
            gfe._1201percentagefee = _pageData.sRecFPc_rep;
            gfe._1201percentageof = FeeFromEnumToString(_pageData.sRecBaseT.ToString(), "1201");
            gfe._1201flatfee = _pageData.sRecFMb_rep;
            gfe._1201mortgagefee = "";
            gfe._1201releasefee = "";
            gfe._1201isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sRecFProps));
            gfe._1201ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sRecFProps));
            gfe._1201paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sRecFProps));
            gfe._1201poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sRecFProps));

            gfe._1202description = TrimIfNeeded(_pageData.sCountyRtcDesc, 50);
            gfe._1202percentagefee = _pageData.sCountyRtcPc_rep;
            gfe._1202percentageof = FeeFromEnumToString(_pageData.sCountyRtcBaseT.ToString(), "1202");
            gfe._1202flatfee = _pageData.sCountyRtcMb_rep;
            gfe._1202mortgagefee = "";
            gfe._1202isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sCountyRtcProps));
            gfe._1202ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sCountyRtcProps));
            gfe._1202paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sCountyRtcProps));
            gfe._1202poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sCountyRtcProps));

            gfe._1203description = TrimIfNeeded(_pageData.sStateRtcDesc, 50);
            gfe._1203percentagefee = _pageData.sStateRtcPc_rep;
            gfe._1203percentageof = FeeFromEnumToString(_pageData.sStateRtcBaseT.ToString(), "1203");
            gfe._1203flatfee = _pageData.sStateRtcMb_rep;
            gfe._1203mortgagefee = "";
            gfe._1203isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sStateRtcProps));
            gfe._1203ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sStateRtcProps));
            gfe._1203paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sStateRtcProps));
            gfe._1203poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sStateRtcProps));


            gfe._1200code1 = _pageData.sU1GovRtcCode;
            gfe._1200description1 = TrimIfNeeded(_pageData.sU1GovRtcDesc, 50);
            gfe._1200percentagefee1 = _pageData.sU1GovRtcPc_rep;
            gfe._1200percentageof1 = FeeFromEnumToString(_pageData.sU1GovRtcBaseT.ToString(), "1200");
            gfe._1200flatfee1 = _pageData.sU1GovRtcMb_rep;
            gfe._1200isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1GovRtcProps));
            gfe._1200ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1GovRtcProps));
            gfe._1200paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1GovRtcProps));
            gfe._1200poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1GovRtcProps));


            gfe._1200code2 = _pageData.sU2GovRtcCode;
            gfe._1200description2 = TrimIfNeeded(_pageData.sU2GovRtcDesc, 50);
            gfe._1200percentagefee2 = _pageData.sU2GovRtcPc_rep;
            gfe._1200percentageof2 = FeeFromEnumToString(_pageData.sU2GovRtcBaseT.ToString(), "1200");
            gfe._1200flatfee2 = _pageData.sU2GovRtcMb_rep;
            gfe._1200isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2GovRtcProps));
            gfe._1200ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2GovRtcProps));
            gfe._1200paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2GovRtcProps));
            gfe._1200poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2GovRtcProps));



            gfe._1200code3 = _pageData.sU3GovRtcCode;
            gfe._1200description3 = TrimIfNeeded(_pageData.sU3GovRtcDesc, 50);
            gfe._1200percentagefee3 = _pageData.sU3GovRtcPc_rep;
            gfe._1200percentageof3 = FeeFromEnumToString(_pageData.sU3GovRtcBaseT.ToString(), "1200");
            gfe._1200flatfee3 = _pageData.sU3GovRtcMb_rep;
            gfe._1200isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU3GovRtcProps));
            gfe._1200ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU3GovRtcProps));
            gfe._1200paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU3GovRtcProps));
            gfe._1200poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU3GovRtcProps));


            #endregion

            #region 1300

            gfe._1302amount = _pageData.sPestInspectF_rep;
            gfe._1302isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sPestInspectFProps));
            gfe._1302ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sPestInspectFProps));
            gfe._1302paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sPestInspectFProps));
            gfe._1302poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sPestInspectFProps));

            gfe._1300code1 = _pageData.sU1ScCode;
            gfe._1300description1 = TrimIfNeeded(_pageData.sU1ScDesc, 50);
            gfe._1300amount1 = _pageData.sU1Sc_rep;
            gfe._1300isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1ScProps));
            gfe._1300ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1ScProps));
            gfe._1300paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1ScProps));
            gfe._1300poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1ScProps));

            gfe._1300code2 = _pageData.sU2ScCode;
            gfe._1300description2 = TrimIfNeeded(_pageData.sU2ScDesc, 50);
            gfe._1300amount2 = _pageData.sU2Sc_rep;
            gfe._1300isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2ScProps));
            gfe._1300ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2ScProps));
            gfe._1300paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2ScProps));
            gfe._1300poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2ScProps));

            gfe._1300code3 = _pageData.sU3ScCode;
            gfe._1300description3 = TrimIfNeeded(_pageData.sU3ScDesc, 50);
            gfe._1300amount3 = _pageData.sU3Sc_rep;
            gfe._1300isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU3ScProps));
            gfe._1300ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU3ScProps));
            gfe._1300paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU3ScProps));
            gfe._1300poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU3ScProps));

            gfe._1300code4 = _pageData.sU4ScCode;
            gfe._1300description4 = TrimIfNeeded(_pageData.sU4ScDesc, 50);
            gfe._1300amount4 = _pageData.sU4Sc_rep;
            gfe._1300isapr4 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU4ScProps));
            gfe._1300ispaidtobroker4 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU4ScProps));
            gfe._1300paidby4 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU4ScProps));
            gfe._1300poc4 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU4ScProps));

            gfe._1300code5 = _pageData.sU5ScCode;
            gfe._1300description5 = TrimIfNeeded(_pageData.sU5ScDesc, 50);
            gfe._1300amount5 = _pageData.sU5Sc_rep;
            gfe._1300isapr5 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU5ScProps));
            gfe._1300ispaidtobroker5 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU5ScProps));
            gfe._1300paidby5 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU5ScProps));
            gfe._1300poc5 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU5ScProps));
            #endregion

            gfe._compensationtobrokerdescription1 = _pageData.sBrokComp1Desc;
            gfe._compensationtobrokerdescription2 = _pageData.sBrokComp2Desc;

            gfe._compensationtobrokeramount1 = _pageData.sBrokComp1_rep;
            gfe._compensationtobrokeramount2 = _pageData.sBrokComp2_rep;
        }


        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        /// <param name="gfe"></param>
        private void ExportGFE2010(MORTGAGE_LOAN_INFOGFE gfe)
        {
            if (string.IsNullOrEmpty(gfe._gfeversion))
            {
                gfe._gfeversion = "2010";
            }

            if (false == string.IsNullOrEmpty(_pageData.sGfeLockPeriodBeforeSettlement_rep) && _pageData.sGfeLockPeriodBeforeSettlement_rep.ToLower() != "n/a")
            {
                gfe._daysrequireratelock = _pageData.sGfeLockPeriodBeforeSettlement_rep;
            }

            IPreparerFields gfeTil = _pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (false == gfeTil.IsEmpty)
            {
                gfe._prepareddate = gfeTil.PrepareDate_rep;
            }

            gfe._daysrequireratelock = _pageData.sGfeLockPeriodBeforeSettlement_rep;
            gfe._daysinyear = _pageData.sDaysInYr_rep;

            #region 800
            gfe._801percentagefee = _pageData.sLOrigFPc_rep;
            gfe._801flatfee = _pageData.sLOrigFMb_rep;
            gfe._801isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sLOrigFProps));
            gfe._801ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sLOrigFProps));
            gfe._801paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sLOrigFProps));
            gfe._801poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sLOrigFProps));

            gfe._802percentagefee = _pageData.sLDiscntPc_rep;
            gfe._802flatfee = _pageData.sLDiscntFMb_rep;
            gfe._802isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sLDiscntProps));
            gfe._802ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sLDiscntProps));
            gfe._802paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sLDiscntProps));
            gfe._802poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sLDiscntProps));


            gfe._804amount = _pageData.sApprF_rep;
            gfe._804ispaid = ToYN(_pageData.sApprFPaid);
            gfe._804isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sApprFProps));
            gfe._804ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sApprFProps));
            gfe._804paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sApprFProps));
            gfe._804poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sApprFProps));

            gfe._805amount = _pageData.sCrF_rep;
            gfe._805ispaid = ToYN(_pageData.sCrFPaid);
            gfe._805isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sCrFProps));
            gfe._805ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sCrFProps));
            gfe._805paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sCrFProps));
            gfe._805poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sCrFProps));

            gfe._806amount = _pageData.sTxServF_rep;
            //gfe._806ispaid          = ToYN(_pageData.sTxServFPaid);     l o doesnt have 
            gfe._806isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sTxServFProps));
            gfe._806ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sTxServFProps));
            gfe._806paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sTxServFProps));
            gfe._806poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sTxServFProps));

            gfe._807amount = _pageData.sFloodCertificationF_rep;
            //gfe._807ispaid          = ToYN(_pageData.sFloodCertificationFPaid);
            gfe._807isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sFloodCertificationFProps));
            gfe._807ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sFloodCertificationFProps));
            gfe._807paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sFloodCertificationFProps));
            gfe._807poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sFloodCertificationFProps));


            //808
            gfe._808percentagefee = _pageData.sMBrokFPc_rep;
            gfe._808flatfee = _pageData.sMBrokFMb_rep;
            gfe._808amount = _pageData.sMBrokF_rep;
            gfe._808description = "Mortgage broker fee";

            gfe._808isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMBrokFProps));
            gfe._808ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMBrokFProps));
            gfe._808paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMBrokFProps));
            gfe._808poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMBrokFProps));

            //809 
            gfe._809amount = _pageData.sInspectF_rep;
            gfe._809isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sInspectFProps));
            gfe._809ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sInspectFProps));
            gfe._809paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sInspectFProps));
            gfe._809poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sInspectFProps));
            gfe._809description = "Lender's inspection fee";

            //810                    _pageData.sProcFProps
            gfe._810amount = _pageData.sProcF_rep;
            gfe._810ispaid = ToYN(_pageData.sProcFPaid);
            gfe._810isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sProcFProps));
            gfe._810ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sProcFProps));
            gfe._810paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sProcFProps));
            gfe._810poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sProcFProps));
            gfe._810description = "Processing fee";

            //811                     _pageData.sUwFProps
            gfe._811amount = _pageData.sUwF_rep;
            gfe._811isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sUwFProps));
            gfe._811ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sUwFProps));
            gfe._811paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sUwFProps));
            gfe._811poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sUwFProps));
            gfe._811description = "Underwriting fee";

            //812                     _pageData.sWireFProps
            gfe._812amount = _pageData.sWireF_rep;
            gfe._812isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sWireFProps));
            gfe._812ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sWireFProps));
            gfe._812paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sWireFProps));
            gfe._812poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sWireFProps));
            gfe._812description = "Wire Transfer";


            gfe._800code1 = _pageData.s800U1FCode;
            gfe._800code2 = _pageData.s800U2FCode;
            gfe._800code3 = _pageData.s800U3FCode;
            gfe._800code4 = _pageData.s800U4FCode;
            gfe._800code5 = _pageData.s800U5FCode;

            gfe._800description1 = TrimIfNeeded(_pageData.s800U1FDesc, 50);
            gfe._800description2 = TrimIfNeeded(_pageData.s800U2FDesc, 50);
            gfe._800description3 = TrimIfNeeded(_pageData.s800U3FDesc, 50);
            gfe._800description4 = TrimIfNeeded(_pageData.s800U4FDesc, 50);
            gfe._800description5 = TrimIfNeeded(_pageData.s800U5FDesc, 50);

            gfe._800amount1 = _pageData.s800U1F_rep;
            gfe._800amount2 = _pageData.s800U2F_rep;
            gfe._800amount3 = _pageData.s800U3F_rep;
            gfe._800amount4 = _pageData.s800U4F_rep;
            gfe._800amount5 = _pageData.s800U5F_rep;

            gfe._800isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U1FProps));
            gfe._800ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U1FProps));
            gfe._800paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U1FProps));
            gfe._800poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U1FProps));
            gfe._800isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U2FProps));
            gfe._800ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U2FProps));
            gfe._800paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U2FProps));
            gfe._800poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U2FProps));
            gfe._800isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U3FProps));
            gfe._800ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U3FProps));
            gfe._800paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U3FProps));
            gfe._800poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U3FProps));
            gfe._800isapr4 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U4FProps));
            gfe._800ispaidtobroker4 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U4FProps));
            gfe._800paidby4 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U4FProps));
            gfe._800poc4 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U4FProps));
            gfe._800isapr5 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s800U5FProps));
            gfe._800ispaidtobroker5 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s800U5FProps));
            gfe._800paidby5 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s800U5FProps));
            gfe._800poc5 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s800U5FProps));

            #endregion

            #region 900
            gfe._901numdays = _pageData.sIPiaDy_rep;
            //gfe._901amount = _pageData.sIPia_rep;
            gfe._901isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sIPiaProps));
            gfe._901ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sIPiaProps));
            gfe._901paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sIPiaProps));
            gfe._901poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sIPiaProps));

            //To do what to do for mortgage insurance premium?    -- its pmi
            gfe._902isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMipPiaProps));
            gfe._902ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMipPiaProps));
            gfe._902paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMipPiaProps));
            gfe._902poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMipPiaProps));
            gfe._902amount = _pageData.sMipPia_rep;      //why was this not here?

            gfe._903nummonths = _pageData.sHazInsPiaMon_rep;
            gfe._903amountpermonth = decimal.ToDouble(_pageData.sProHazIns);
            gfe._903percentageof = FeeFromEnumToString(_pageData.sProHazInsT.ToString(), "903");
            gfe._903percentagefee = _pageData.sProHazInsR_rep;

            gfe._903isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sHazInsPiaProps));
            gfe._903ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sHazInsPiaProps));
            gfe._903paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sHazInsPiaProps));
            gfe._903poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sHazInsPiaProps));

            gfe._904description = TrimIfNeeded(_pageData.s904PiaDesc, 50);
            gfe._904amount = _pageData.s904Pia_rep;
            gfe._904isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s904PiaProps));
            gfe._904ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s904PiaProps));
            gfe._904paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s904PiaProps));
            gfe._904poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s904PiaProps));


            gfe._905isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sVaFfProps));
            gfe._905ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sVaFfProps));
            gfe._905paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sVaFfProps));
            gfe._905poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sVaFfProps));

            gfe._900code1 = _pageData.s900U1PiaCode;
            gfe._900description1 = TrimIfNeeded(_pageData.s900U1PiaDesc, 50);
            gfe._900amount1 = _pageData.s900U1Pia_rep;
            gfe._900isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s900U1PiaProps));
            gfe._900ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s900U1PiaProps));
            gfe._900paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s900U1PiaProps));
            gfe._900poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s900U1PiaProps));
            #endregion

            #region 1000
            //1002
            gfe._1002nummonths = _pageData.sHazInsRsrvMon_rep;
            gfe._1002isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sHazInsRsrvProps));
            gfe._1002ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sHazInsRsrvProps));
            gfe._1002paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sHazInsRsrvProps));
            gfe._1002poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sHazInsRsrvProps));

            //1003 Mortgage Insurance
            gfe._1003amountpermonth = _pageData.sProMInsMb_rep;
            gfe._1003nummonths = _pageData.sMInsRsrvMon_rep;
            gfe._1003isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sMInsRsrvProps));
            gfe._1003ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sMInsRsrvProps));
            gfe._1003paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sMInsRsrvProps));
            gfe._1003poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sMInsRsrvProps));

            //1004 Tax Reserves
            gfe._1004percentagefee = _pageData.sProRealETxR_rep;
            gfe._1004percentageof = FeeFromEnumToString(_pageData.sProRealETxT.ToString(), "1004");
            gfe._1004amountpermonth = _pageData.sProRealETxMb_rep;
            gfe._1004nummonths = _pageData.sRealETxRsrvMon_rep;
            gfe._1004isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sRealETxRsrvProps));
            gfe._1004ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sRealETxRsrvProps));
            gfe._1004paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sRealETxRsrvProps));
            gfe._1004poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sRealETxRsrvProps));

            //1005 School Tax
            gfe._1005nummonths = _pageData.sSchoolTxRsrvMon_rep;
            gfe._1005amountpermonth = _pageData.sProSchoolTx_rep;
            gfe._1005isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sSchoolTxRsrvProps));
            gfe._1005ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sSchoolTxRsrvProps));
            gfe._1005paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sSchoolTxRsrvProps));
            gfe._1005poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sSchoolTxRsrvProps));

            //1006 Flood Insurance
            gfe._1006nummonths = _pageData.sFloodInsRsrvMon_rep;
            gfe._1006amountpermonth = _pageData.sProFloodIns_rep;
            gfe._1006isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sFloodInsRsrvProps));
            gfe._1006ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sFloodInsRsrvProps));
            gfe._1006paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sFloodInsRsrvProps));
            gfe._1006poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sFloodInsRsrvProps));

            //1007 aggregate adjustments 
            gfe._1007amount = _pageData.sAggregateAdjRsrv_rep;
            gfe._1007isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sAggregateAdjRsrvProps));
            gfe._1007ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sAggregateAdjRsrvProps));
            gfe._1007paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sAggregateAdjRsrvProps));
            gfe._1007poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sAggregateAdjRsrvProps));

            //1008 
            //gfe._1008
            //gfe._1006description = _pageData.s1006ProHExpDesc;
            //gfe._1006nummonths = _pageData.s1006RsrvMon_rep;
            //gfe._1006amountpermonth = _pageData.s1006ProHExp_rep;
            //gfe._1006isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s1006RsrvProps));
            //gfe._1006ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s1006RsrvProps));
            //gfe._1006paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s1006RsrvProps));
            //gfe._1006poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s1006RsrvProps));

            //gfe._1007description = _pageData.s1007ProHExpDesc;
            //gfe._1007nummonths = _pageData.s1007RsrvMon_rep;
            //gfe._1007amountpermonth = _pageData.s1007ProHExp_rep;
            //gfe._1007isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.s1007RsrvProps));
            //gfe._1007ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.s1007RsrvProps));
            //gfe._1007paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.s1007RsrvProps));
            //gfe._1007poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.s1007RsrvProps));

            #endregion

            #region 1100

            //donot import 1101. 

            //1102  sEscrowFProps 
            gfe._1102amount = _pageData.sEscrowF_rep;
            gfe._1102isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sEscrowFProps));
            gfe._1102ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sEscrowFProps));
            gfe._1102paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sEscrowFProps));
            gfe._1102poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sEscrowFProps));

            //1103  sOwnerTitleInsProps
            gfe._1103amount = _pageData.sOwnerTitleInsF_rep;
            gfe._1103isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sOwnerTitleInsProps));
            gfe._1103ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sOwnerTitleInsProps));
            gfe._1103paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sOwnerTitleInsProps));
            gfe._1103poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sOwnerTitleInsProps));

            //1104 sTitleInsFProps
            gfe._1104amount = _pageData.sTitleInsF_rep;
            gfe._1104isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sTitleInsFProps));
            gfe._1104ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sTitleInsFProps));
            gfe._1104paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sTitleInsFProps));
            gfe._1104poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sTitleInsFProps));

            //1109 sDocPrepFProps 
            gfe._1109amount = _pageData.sDocPrepF_rep;
            gfe._1109isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sDocPrepFProps));
            gfe._1109ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sDocPrepFProps));
            gfe._1109paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sDocPrepFProps));
            gfe._1109poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sDocPrepFProps));

            //1110 notary fee 
            gfe._1110amount = _pageData.sNotaryF_rep;
            gfe._1110isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sNotaryFProps));
            gfe._1110ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sNotaryFProps));
            gfe._1110paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sNotaryFProps));
            gfe._1110poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sNotaryFProps));

            //1111 attorney fees 
            gfe._1111amount = _pageData.sAttorneyF_rep;
            gfe._1111isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sAttorneyFProps));
            gfe._1111ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sAttorneyFProps));
            gfe._1111paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sAttorneyFProps));
            gfe._1111poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sAttorneyFProps));

            //1112
            gfe._1100code1 = _pageData.sU1TcCode;
            gfe._1100amount1 = _pageData.sU1Tc_rep;
            gfe._1100description1 = TrimIfNeeded(_pageData.sU1TcDesc, 50);
            gfe._1100isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1TcProps));
            gfe._1100ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1TcProps));
            gfe._1100paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1TcProps));
            gfe._1100poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1TcProps));

            //113
            gfe._1100code2 = _pageData.sU2TcCode;
            gfe._1100amount2 = _pageData.sU2Tc_rep;
            gfe._1100description2 = TrimIfNeeded(_pageData.sU2TcDesc, 50);
            gfe._1100isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2TcProps));
            gfe._1100ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2TcProps));
            gfe._1100paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2TcProps));
            gfe._1100poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2TcProps));

            //1114
            gfe._1100code3 = _pageData.sU3TcCode;
            gfe._1100amount3 = _pageData.sU3Tc_rep;
            gfe._1100description3 = TrimIfNeeded(_pageData.sU3TcDesc, 50);
            gfe._1100isapr3 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU3TcProps));
            gfe._1100ispaidtobroker3 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU3TcProps));
            gfe._1100paidby3 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU3TcProps));
            gfe._1100poc3 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU3TcProps));

            //1115
            gfe._1100code4 = _pageData.sU4TcCode;
            gfe._1100amount4 = _pageData.sU4Tc_rep;
            gfe._1100description4 = TrimIfNeeded(_pageData.sU4TcDesc, 50);
            gfe._1100isapr4 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU4TcProps));
            gfe._1100ispaidtobroker4 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU4TcProps));
            gfe._1100paidby4 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU4TcProps));
            gfe._1100poc4 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU4TcProps));



            #endregion

            #region 1200
            //1201 in LO 1202 in LPQ recording fees.
            //gfe._1202percentagefee = _pageData.sRecFPc_rep;
            //gfe._1202percentageof = FeeFromEnumToString(_pageData.sRecBaseT.ToString());
            gfe._1202flatfee = _pageData.sRecF_rep; //change it to export the amount since they dont look at the % that we have.
            gfe._1202isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sRecFProps));
            gfe._1202ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sRecFProps));
            gfe._1202paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sRecFProps));
            gfe._1202poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sRecFProps));
            gfe._1202mortgagefee = "0";
            gfe._1202releasefee = "0";

            //1204 COunty 
            gfe._1204deedpercentagefee = _pageData.sCountyRtcPc_rep;
            gfe._1204deedpercentageof = FeeFromEnumToString(_pageData.sCountyRtcBaseT.ToString(), "1204");
            gfe._1204flatfee = _pageData.sCountyRtcMb_rep;
            gfe._1204isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sCountyRtcProps));
            gfe._1204ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sCountyRtcProps));
            gfe._1204paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sCountyRtcProps));
            gfe._1204poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sCountyRtcProps));

            //1205 State Taxes 
            gfe._1205deedpercentagefee = _pageData.sStateRtcPc_rep;
            gfe._1205deedpercentageof = FeeFromEnumToString(_pageData.sStateRtcBaseT.ToString(), "1205");
            gfe._1205flatfee = _pageData.sStateRtcMb_rep;
            gfe._1205isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sStateRtcProps));
            gfe._1205ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sStateRtcProps));
            gfe._1205paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sStateRtcProps));
            gfe._1205poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sStateRtcProps));

            //1206 
            gfe._1206description = TrimIfNeeded(_pageData.sU1GovRtcDesc, 50);
            gfe._1206percentagefee = _pageData.sU1GovRtcPc_rep;
            gfe._1206percentageof = FeeFromEnumToString(_pageData.sU1GovRtcBaseT.ToString(), "1206");
            gfe._1206flatfee = _pageData.sU1GovRtcMb_rep;
            gfe._1206isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1GovRtcProps));
            gfe._1206ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1GovRtcProps));
            gfe._1206paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1GovRtcProps));
            gfe._1206poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1GovRtcProps));


            //1207
            gfe._1207description = TrimIfNeeded(_pageData.sU2GovRtcDesc, 50);
            gfe._1207percentagefee = _pageData.sU2GovRtcPc_rep;
            gfe._1207percentageof = FeeFromEnumToString(_pageData.sU2GovRtcBaseT.ToString(), "1207");
            gfe._1207flatfee = _pageData.sU2GovRtcMb_rep;
            gfe._1207isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2GovRtcProps));
            gfe._1207ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2GovRtcProps));
            gfe._1207paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2GovRtcProps));
            gfe._1207poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2GovRtcProps));


            #endregion

            #region 1300

            gfe._1302amount = _pageData.sPestInspectF_rep;
            gfe._1302isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sPestInspectFProps));
            gfe._1302ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sPestInspectFProps));
            gfe._1302paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sPestInspectFProps));
            gfe._1302poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sPestInspectFProps));

            gfe._1303description = TrimIfNeeded(_pageData.sU1ScDesc, 50);
            gfe._1303amount = _pageData.sU1Sc_rep;
            gfe._1303isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU1ScProps));
            gfe._1303ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU1ScProps));
            gfe._1303paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU1ScProps));
            gfe._1303poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU1ScProps));

            gfe._1304description = TrimIfNeeded(_pageData.sU2ScDesc, 50);
            gfe._1304amount = _pageData.sU2Sc_rep;
            gfe._1304isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU2ScProps));
            gfe._1304ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU2ScProps));
            gfe._1304paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU2ScProps));
            gfe._1304poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU2ScProps));

            gfe._1305description = TrimIfNeeded(_pageData.sU3ScDesc, 50);
            gfe._1305amount = _pageData.sU3Sc_rep;
            gfe._1305isapr = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU3ScProps));
            gfe._1305ispaidtobroker = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU3ScProps));
            gfe._1305paidby = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU3ScProps));
            gfe._1305poc = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU3ScProps));

            gfe._1300code1 = _pageData.sU4ScCode;
            gfe._1300description1 = TrimIfNeeded(_pageData.sU4ScDesc, 50);
            gfe._1300amount1 = _pageData.sU4Sc_rep;
            gfe._1300isapr1 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU4ScProps));
            gfe._1300ispaidtobroker1 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU4ScProps));
            gfe._1300paidby1 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU4ScProps));
            gfe._1300poc1 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU4ScProps));


            gfe._1300description2 = TrimIfNeeded(_pageData.sU5ScDesc, 50);
            gfe._1300amount2 = _pageData.sU5Sc_rep;
            gfe._1300isapr2 = ToYN(LosConvert.GfeItemProps_Apr(_pageData.sU5ScProps));
            gfe._1300ispaidtobroker2 = ToYN(LosConvert.GfeItemProps_ToBr(_pageData.sU5ScProps));
            gfe._1300paidby2 = ToPaidBy(LosConvert.GfeItemProps_Payer(_pageData.sU5ScProps));
            gfe._1300poc2 = ToYN(LosConvert.GfeItemProps_Poc(_pageData.sU5ScProps));
            #endregion

            gfe._compensationtobrokerdescription1 = _pageData.sBrokComp1Desc;
            gfe._compensationtobrokerdescription2 = _pageData.sBrokComp2Desc;

            gfe._compensationtobrokeramount1 = _pageData.sBrokComp1_rep;
            gfe._compensationtobrokeramount2 = _pageData.sBrokComp2_rep;
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string FeeFromEnumToString(string p, string hudline)
        {
            switch (p.ToLower())
            {
                case "loanamount":
                    return "LOAN AMOUNT";
                case "appraisalvalue":
                    return "APPRAISAL PRICE";
                case "salesprice":
                    return "PURCHASE PRICE";
                default:
                    throw new ArgumentException("Line " + hudline + " of the GFE is currently using \"" + p + "\", which is unsupported by LPQ. Please change to 'Loan Amount', 'Appraisal Price', or 'Purchase Price'.");
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static string ToPaidBy(int p)
        {
            switch (p)
            {
                case 0:
                    return "B";
                case 1:
                    return "B";
                case 2:
                    throw new ArgumentException("Unhandled Paid By In GFE SELLER is not supported by LPQ.");
                case 3:
                    return "L";
                case 4:
                    throw new ArgumentException("Unhandled Paid By In GFE Broker is not supported by LPQ.");
                default:
                    throw new ArgumentException("Unhandled Paid By In GFE " + p);
            }
        }


        /// <summary>
        /// For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private static string ToYNString(bool answer)
        {
            return answer ? "Y" : "N";
        }


        /// <summary>
        /// For Scratch Null Check -- 
        /// Maps LendingQB product type to LPQ.
        /// </summary>
        /// <param name="e_sProdDocT"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Throws Argument exception when it encounters an Unhandled product type.</exception>
        private static MORTGAGE_LOAN_INFODocument_type ToLPQ(E_sProdDocT e_sProdDocT)
        {
            switch (e_sProdDocT)
            {
                case E_sProdDocT.Full:
                    return MORTGAGE_LOAN_INFODocument_type.FULL_DOC;
                case E_sProdDocT.Alt:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.Light:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.SIVA:
                    return MORTGAGE_LOAN_INFODocument_type.STATED;
                case E_sProdDocT.VISA:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.SISA:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.NIVA:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.NINA:
                    return MORTGAGE_LOAN_INFODocument_type.NINA;
                case E_sProdDocT.NISA:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.NINANE:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.NIVANE:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.VINA:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                case E_sProdDocT.Streamline:
                    return MORTGAGE_LOAN_INFODocument_type.Item;
                default:
                    throw new ArgumentException("Unhandled Product Type " + e_sProdDocT);
            }
        }
        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportFreddieMac()
        {
            _loanFile.LOAN_INFO.FREDDIE_MAC = _loanFile.LOAN_INFO.FREDDIE_MAC ?? new MORTGAGE_FREDDIE_MAC();
            _loanFile.LOAN_INFO.FREDDIE_MAC.fre_transaction_id = _pageData.sFreddieTransactionId;
            _loanFile.LOAN_INFO.FREDDIE_MAC.fre_reserve_amount = _pageData.sFredieReservesAmt_rep;

            //fre_key_id
            //fre_loan_id
            //fre_report_id
            //fre_credit_report_provider_id
            //fre_mi_coverage_type
            //fre_mi_payment_type
            //fre_mi_refund_type
            //fre_mi_renewel_calculation_type
            //fre_mi_renewel_type
            //fre_mi_coverage_months
            //fre_mi_payment_option
            //fre_construction_purpose_type
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// Converts LO interview type to LPQ.
        /// </summary>
        /// <param name="e_aIntrvwrMethodT"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Throws Argument exception when it encounters an Unhandled interview type.</exception>
        private static MORTGAGE_BASE_APPLICANTInterview_method MapToLoansPQ(E_aIntrvwrMethodT e_aIntrvwrMethodT)
        {
            switch (e_aIntrvwrMethodT)
            {
                case E_aIntrvwrMethodT.LeaveBlank:
                    return MORTGAGE_BASE_APPLICANTInterview_method.Item;
                case E_aIntrvwrMethodT.FaceToFace:
                    return MORTGAGE_BASE_APPLICANTInterview_method.FACETOFACE;
                case E_aIntrvwrMethodT.ByMail:
                    return MORTGAGE_BASE_APPLICANTInterview_method.MAIL;
                case E_aIntrvwrMethodT.ByTelephone:
                    return MORTGAGE_BASE_APPLICANTInterview_method.TELEPHONE;
                case E_aIntrvwrMethodT.Internet:
                    return MORTGAGE_BASE_APPLICANTInterview_method.INTERNET;
                default:
                    throw new ArgumentException("Unhandled interview Type " + e_aIntrvwrMethodT);
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// Converts a LO Down Pmnt source string to LPQ. This one doesnt throw an exception because once again its free text in LendingQB.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static MORTGAGE_LOAN_INFODown_payment_source MapDownPmntSrcToLoansPQ(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToLower())
            {
                case "checking/savings":
                    return MORTGAGE_LOAN_INFODown_payment_source.CS;
                case "deposit on sales contract":
                    return MORTGAGE_LOAN_INFODown_payment_source.DOSC;
                case "equity on sold property":
                    return MORTGAGE_LOAN_INFODown_payment_source.ESOP;
                case "equity from subject property":
                    return MORTGAGE_LOAN_INFODown_payment_source.EPFSU;
                case "equity from pending sale":
                    return MORTGAGE_LOAN_INFODown_payment_source.EPFSA;
                case "gift funds":
                    return MORTGAGE_LOAN_INFODown_payment_source.GF;
                case "stock & bonds":
                    return MORTGAGE_LOAN_INFODown_payment_source.SAB;
                case "lot equity":
                    return MORTGAGE_LOAN_INFODown_payment_source.LEQ;
                case "bridge loan":
                    return MORTGAGE_LOAN_INFODown_payment_source.BL;
                case "unsecured borrowed funds":
                    return MORTGAGE_LOAN_INFODown_payment_source.UBF;
                case "trust funds":
                    return MORTGAGE_LOAN_INFODown_payment_source.TF;
                case "retirement funds":
                    return MORTGAGE_LOAN_INFODown_payment_source.RF;
                case "rent with option to purchase":
                    return MORTGAGE_LOAN_INFODown_payment_source.ROP;
                case "life insurance cash value":
                    return MORTGAGE_LOAN_INFODown_payment_source.LICV;
                case "sale of chattel":
                    return MORTGAGE_LOAN_INFODown_payment_source.SC;
                case "trade equity":
                    return MORTGAGE_LOAN_INFODown_payment_source.TEQ;
                case "sweat equity":
                    return MORTGAGE_LOAN_INFODown_payment_source.SEQ;
                case "cash on hand":
                    return MORTGAGE_LOAN_INFODown_payment_source.COH;
                case "other":
                    return MORTGAGE_LOAN_INFODown_payment_source.O;
                case "secured borrowed funds":
                    return MORTGAGE_LOAN_INFODown_payment_source.SBF;
                case "":
                    return MORTGAGE_LOAN_INFODown_payment_source.Item;
                default:
                    return MORTGAGE_LOAN_INFODown_payment_source.Item;
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        ///  Converts a gse ref purpose t to lpq. 
        /// </summary>
        /// <param name="e_sGseRefPurposeT"></param>
        /// <returns></returns>
        private static string MapToLoansPQRefinancePurpose(E_sGseRefPurposeT e_sGseRefPurposeT)
        {
            switch (e_sGseRefPurposeT)
            {

                case E_sGseRefPurposeT.CashOutDebtConsolidation:
                    return "CASHOUT/CONSOLIDATE";
                case E_sGseRefPurposeT.CashOutHomeImprovement:
                    return "CASHOUT/HOME IMPROV";
                case E_sGseRefPurposeT.CashOutLimited:
                    return "CASHOUT/LIMITED";
                case E_sGseRefPurposeT.CashOutOther:
                    return "CASHOUT/OTHER";
                case E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance:
                    return "NC FHA STREAMLINED";
                case E_sGseRefPurposeT.NoCashOutFREOwnedRefinance:
                    return "NC FRE REFINANCE";
                case E_sGseRefPurposeT.NoCashOutOther:
                    return "NC OTHER";
                case E_sGseRefPurposeT.NoCashOutStreamlinedRefinance:
                    return "NC STREAMLINED";
                case E_sGseRefPurposeT.ChangeInRateTerm:
                    return "CHANGE RATE TERM";
                case E_sGseRefPurposeT.LeaveBlank:
                    return "";
                default:
                    throw new ArgumentException("Unhandled Gse Refinance Purpose Type " + e_sGseRefPurposeT);
            }
        }

        /// <summary>
        /// For Scratch Null Check -- 
        /// </summary>
        private void ExportGovernmentData()
        {
            _loanFile.LOAN_INFO.GOVERNMENT_DATA = _loanFile.LOAN_INFO.GOVERNMENT_DATA ?? new MORTGAGE_GOVERNMENT_DATA();
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.agency_case_id = _pageData.sAgencyCaseNum;
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.FHA_lender_id = _pageData.sFHALenderIdCode;
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.FHA_sponsor_id = _pageData.sFHASponsorAgentIdCode;
            // _loanFile.LOAN_INFO.GOVERNMENT_DATA.type_of_refinance = _pageData.sFHARefinanceTypeDesc;
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.FNMA_section_of_the_act = _pageData.sFHAHousingActSection;
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.sales_concession_amount = _pageData.sFHASalesConcessions_rep;
            _loanFile.LOAN_INFO.GOVERNMENT_DATA.VA_monthly_utilities = _pageData.sVaProUtilityPmt_rep;

            //governmentData.mortgage_credit_certificate;
            //governmentData.VA_is_borrower_co_borrower_married;
            //governmentData.VA_borrower_funding_fee_percent;
            //governmentData.VA_residual_income_amount;
            //governmentData.is_property_energy_efficient;
            //governmentData.FHA_borrower_financed_discount_point_amounts;
            //governmentData.FHA_alimony_liability_treatment_type;
            //governmentData.VA_entitlement_amount;
            //governmentData.FHA_MIP_refund_amount;
            //governmentData.VA_monthly_maintenance;
        }

        private void ExportHomeEquityData()
        {
            if (this.isNewFile)
            {
                _loanFile.LOAN_INFO.HOME_EQUITY_DATA = new MORTGAGE_LOAN_INFOHOME_EQUITY_DATA();
                _loanFile.LOAN_INFO.HOME_EQUITY_DATA.is_installment = _pageData.sIsLineOfCredit ? BASE_APPLICANTDeclined_answer_race_gender.N : BASE_APPLICANTDeclined_answer_race_gender.Y;
                _loanFile.LOAN_INFO.HOME_EQUITY_DATA.is_installmentSpecified = true;
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// </summary>
        private void ExportLoanProductData()
        {
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA = _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA ?? new MORTGAGE_LOAN_INFOLOAN_PRODUCT_DATA();

            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.fre_affordable_program_id = _pageData.sFredAffordProgId;

            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_first_adjust_months = _pageData.sRAdj1stCapMon_rep;
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_first_period_cap = _pageData.sRAdj1stCapR_rep;
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_subsequent_adjust_months = _pageData.sRAdjCapMon_rep;
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_subsequent_periodic_cap = _pageData.sRAdjCapR_rep;

            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_first_period_cap = _pageData.sRAdj1stCapR_rep;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_first_adjust_months = _pageData.sRAdj1stCapMon_rep;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_subsequent_adjust_months = _pageData.sRAdjCapMon_rep;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_rate_subsequent_periodic_cap = _pageData.sRAdjCapR_rep;

            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.index_type = MapToLoansPQProductDataIndex(_pageData.sFreddieArmIndexT);
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.loan_term_due_months = _pageData.sLPurposeT == E_sLPurposeT.HomeEquity && _pageData.sIsLineOfCredit ? _pageData.sHelocDraw_rep : _pageData.sDue_rep;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.buydown_source = MapToLoansPQBuyDownSource(_pageData.sBuydownContributorT);
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.buydown_is_temp = _pageData.sBuydown ? BASE_APPLICANTDeclined_answer_race_gender.Y : BASE_APPLICANTDeclined_answer_race_gender.N;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.buydown_is_tempSpecified = true;

            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.buydown_first_rate_adjust_percent = _pageData.sBuydwnR1_rep;
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.gse_property_type = MapToLoansPQ(_pageData.sGseSpT);
            _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.gse_project_class_type = MapToLPQGseProjectClass(_pageData.sSpProjClassT);

            if (this.isNewFile)
            {
                _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.is_prepay_penalty = _pageData.sGfeHavePpmtPenalty ? BASE_APPLICANTDeclined_answer_race_gender.Y : BASE_APPLICANTDeclined_answer_race_gender.N;
                _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.is_prepay_penaltySpecified = true;
                _loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.max_prepay_penalty = _pageData.sGfeMaxPpmtPenaltyAmt_rep;
            }
            //HELOC_max_balance_amount    //?possible sHelocCreditLimit_rep but their mismo exporter puts something else here 
            //_pageData.sFredAffordProgId = loanProductData.fre_offering_id;
            //loan_repayment_type
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.arm_payment_first_adjust_months;
            //buydown_total_months          
            //buydown_change_frequency_months

            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.is_prepay_penalty = ToYN(_pageData.sPrepmtPenaltyT == E_sPrepmtPenaltyT.May);
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.can_payment_rise = ToYN(_pageData.sGfeCanRateIncrease);
            //_loanFile.LOAN_INFO.LOAN_PRODUCT_DATA.can_rate_rise = ToYN(_pageData.sGfeCanRateIncrease); 
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// </summary>
        /// <param name="e_sGseSpT"></param>
        /// <returns></returns>
        private static string MapToLoansPQ(E_sGseSpT e_sGseSpT)
        {
            switch (e_sGseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                    return "";
                case E_sGseSpT.Attached:
                    return "ATTACHED";
                case E_sGseSpT.Condominium:
                    return "CONDOMINIUM";
                case E_sGseSpT.Cooperative:
                    return "COOPERATIVE";
                case E_sGseSpT.Detached:
                    return "DETACHED";
                case E_sGseSpT.DetachedCondominium:
                    return "DETACHED_CONDO";
                case E_sGseSpT.HighRiseCondominium:
                    return "HI_RISE_CONDO";
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return "MANU_HCPCOOP";
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return "MANU_MULTIWIDE";
                case E_sGseSpT.ManufacturedHousing:
                    return "MANU_HOUSING";
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return "MANU_SINGLEWIDE";
                case E_sGseSpT.Modular:
                    return "";
                case E_sGseSpT.PUD:
                    return "PUD";
                default:
                    throw new ArgumentException("Unhandled Gse Property Type " + e_sGseSpT);
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="e_sBuydownContributorT"></param>
        /// <returns></returns>
        private static string MapToLoansPQBuyDownSource(E_sBuydownContributorT e_sBuydownContributorT)
        {
            switch (e_sBuydownContributorT)
            {
                case E_sBuydownContributorT.Borrower:
                    return "BORROWER";
                case E_sBuydownContributorT.Other:
                    return "OTHER";
                case E_sBuydownContributorT.LenderPremiumFinanced:
                    return "LENDER_FINANCED";
                case E_sBuydownContributorT.Seller:
                    return "SELLER";
                case E_sBuydownContributorT.Builder:
                    return "BUILDER";
                case E_sBuydownContributorT.LeaveBlank:
                    return "";
                default:
                    throw new ArgumentException("Unhandled Buydown Contributor Type " + e_sBuydownContributorT);
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// </summary>
        /// <param name="e_sFreddieArmIndexT"></param>
        /// <returns></returns>
        private static string MapToLoansPQProductDataIndex(E_sFreddieArmIndexT e_sFreddieArmIndexT)
        {
            switch (e_sFreddieArmIndexT)
            {
                case E_sFreddieArmIndexT.LeaveBlank:
                    return "";
                case E_sFreddieArmIndexT.OneYearTreasury:
                    return "1_YEAR_TREASURY";
                case E_sFreddieArmIndexT.ThreeYearTreasury:
                    return "3_YEAR_TREASURY";
                case E_sFreddieArmIndexT.SixMonthTreasury:
                    return "6_MONTH_TREASURY";
                case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
                    return "11TH_DCOF";
                case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
                    return "NMMCOF";
                case E_sFreddieArmIndexT.LIBOR:
                    return "LIBOR";
                case E_sFreddieArmIndexT.Other:
                    return "OTHER";
                default:
                    throw new ArgumentException("Unhandled Freddie Arm Index Type : " + e_sFreddieArmIndexT);
            }
        }

        private void SetMortgagePropertyInfoPropertyType()
        {
            if (_pageData.sProdSpT == E_sProdSpT.SFR)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.SFR;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.PUD)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.PUD;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.CoOp)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.COOP;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.Manufactured)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.MANUFACTUREDHOME;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.Townhouse)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.TOWNHOUSE;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.Commercial)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.COMMERCIAL;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.MixedUse)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.MIXEDUSE;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.TwoUnits)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.Item2UNIT;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.ThreeUnits)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.Item3UNIT;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.FourUnits)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.Item4UNIT;
            }
            else if (_pageData.sProdSpT == E_sProdSpT.Condo)
            {
                if (_pageData.sProdIsCondotel)
                {
                    _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.CONDOTEL;
                }
                else
                {
                    _loanFile.PROPERTY_INFO.property_type = _pageData.sProdIsCondoStoriesHighRise ? MORTGAGE_PROPERTY_INFOProperty_type.HIGHRISECONDO : MORTGAGE_PROPERTY_INFOProperty_type.LOWRISECONDO;
                }
            }
            else if (_pageData.sProdSpT == E_sProdSpT.Modular || _pageData.sProdSpT == E_sProdSpT.Rowhouse)
            {
                _loanFile.PROPERTY_INFO.property_type = MORTGAGE_PROPERTY_INFOProperty_type.OTHER;
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// </summary>
        private void ExportPropertyInfo()
        {
            _loanFile.PROPERTY_INFO = _loanFile.PROPERTY_INFO ?? new MORTGAGE_PROPERTY_INFO();
            _loanFile.PROPERTY_INFO.address = _pageData.sSpAddr;
            _loanFile.PROPERTY_INFO.county = _pageData.sSpCounty;
            _loanFile.PROPERTY_INFO.city = _pageData.sSpCity;
            _loanFile.PROPERTY_INFO.zip = _pageData.sSpZip;
            _loanFile.PROPERTY_INFO.state = _pageData.sSpState;
            _loanFile.PROPERTY_INFO.units = _pageData.sUnitsNum_rep;
            _loanFile.PROPERTY_INFO.built_year = _pageData.sYrBuilt;
            _loanFile.PROPERTY_INFO.community_name = _pageData.sProjNm;

            if (this.isNewFile)
            {
                this.SetMortgagePropertyInfoPropertyType();
            }

            if (_pageData.sIsConstructionLoan)
            {
                _loanFile.PROPERTY_INFO.property_lot_year_acquired = _pageData.sLotAcqYr;
                _loanFile.PROPERTY_INFO.lot_present_value = _pageData.sLotVal_rep;
                _loanFile.PROPERTY_INFO.property_lot_improvement_costs = _pageData.sLotImprovC_rep;
                _loanFile.PROPERTY_INFO.property_lot_original_cost = _pageData.sLotOrigC_rep;
            }
            else if (_pageData.sIsRefinancing)
            {
                _loanFile.PROPERTY_INFO.property_lot_original_cost = _pageData.sSpOrigC_rep;
                _loanFile.PROPERTY_INFO.property_lot_improvement_costs = _pageData.sSpImprovC_rep;
                _loanFile.PROPERTY_INFO.property_lot_year_acquired = _pageData.sSpAcqYr;
            }
            _loanFile.PROPERTY_INFO.hmda_county_code = _pageData.sHmdaCountyCode;
            _loanFile.PROPERTY_INFO.is_hoepa_manualSpecified = true;
            _loanFile.PROPERTY_INFO.is_hoepa = ToYN(_pageData.sHmdaReportAsHoepaLoan);
            _loanFile.PROPERTY_INFO.is_hoepa_manual = BASE_APPLICANTDeclined_answer_race_gender.N;
            _loanFile.PROPERTY_INFO.hmda_msa_number = _pageData.sHmdaMsaNum;
            _loanFile.PROPERTY_INFO.hmda_state_code = _pageData.sHmdaStateCode;
            _loanFile.PROPERTY_INFO.property_lot_improvement_description = _pageData.sSpImprovDesc;
            _loanFile.PROPERTY_INFO.is_improvement_to_be_made = ToLPQYNE(_pageData.sSpImprovTimeFrameT);
            _loanFile.PROPERTY_INFO.is_improvement_to_be_madeSpecified = true;
            _loanFile.PROPERTY_INFO.building_status = MapToLoansPQBuildingStatus(_pageData.sBuildingStatusT);
            _loanFile.PROPERTY_INFO.tax_id = TrimIfNeeded(_pageData.sAssessorsParcelId, 25);
            _loanFile.PROPERTY_INFO.census_tract_number = _pageData.sHmdaCensusTract;

            if (_pageData.nApps > 0)
            {
                _loanFile.PROPERTY_INFO.manner_title_held = _pageData.GetAppData(0).aManner;
                _loanFile.PROPERTY_INFO.occupancy_status = MapToLoansPQ(_pageData.GetAppData(0).aOccT);
                _loanFile.PROPERTY_INFO.occupancy_statusSpecified = true;
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="e_sSpImprovTimeFrameT"></param>
        /// <returns></returns>
        private static BASE_APPLICANTDeclined_answer_race_gender ToLPQYNE(E_sSpImprovTimeFrameT e_sSpImprovTimeFrameT)
        {
            switch (e_sSpImprovTimeFrameT)
            {
                case E_sSpImprovTimeFrameT.LeaveBlank:
                    return BASE_APPLICANTDeclined_answer_race_gender.Item;
                case E_sSpImprovTimeFrameT.Made:
                    return BASE_APPLICANTDeclined_answer_race_gender.N;
                case E_sSpImprovTimeFrameT.ToBeMade:
                    return BASE_APPLICANTDeclined_answer_race_gender.Y;
                default:
                    throw new ArgumentException("Unhandled Improvement Timeframe Type " + e_sSpImprovTimeFrameT);
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="e_aOccT"></param>
        /// <returns></returns>
        private static MORTGAGE_PROPERTY_INFOOccupancy_status MapToLoansPQ(E_aOccT e_aOccT)
        {
            switch (e_aOccT)
            {
                case E_aOccT.PrimaryResidence:
                    return MORTGAGE_PROPERTY_INFOOccupancy_status.PRIMARYRESIDENCE;
                case E_aOccT.SecondaryResidence:
                    return MORTGAGE_PROPERTY_INFOOccupancy_status.SECONDHOME;
                case E_aOccT.Investment:
                    return MORTGAGE_PROPERTY_INFOOccupancy_status.INVESTMENT;
                default:
                    throw new ArgumentException("Unhandled Occupancy Status " + e_aOccT);
            }
        }

        /// <summary>
        ///  For Scratch Null Check -- 
        /// 
        /// </summary>
        /// <param name="e_sBuildingStatusT"></param>
        /// <returns></returns>
        private static string MapToLoansPQBuildingStatus(E_sBuildingStatusT e_sBuildingStatusT)
        {
            switch (e_sBuildingStatusT)
            {
                case E_sBuildingStatusT.Existing:
                    return "EXISTING";
                case E_sBuildingStatusT.Proposed:
                    return "PROPOSED";
                case E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab:
                    return "SUBJ_IMP_REPAIDREHAB";
                case E_sBuildingStatusT.SubstantiallyRehabilitated:
                    return "SUBSTANTIAL_REHAB";
                case E_sBuildingStatusT.UnderConstruction:
                    return "UNDER_CONSTRUCTION";
                case E_sBuildingStatusT.LeaveBlank:
                    return "";
                default:
                    throw new ArgumentException("Unhandled Building Status");
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        private void ExportApplications()
        {
            int appCount = _pageData.nApps;

            IList<MORTGAGE_APPLICANT> applicants;
            if (_loanFile.APPLICANTS != null)
            {
                applicants = new List<MORTGAGE_APPLICANT>(_loanFile.APPLICANTS.APPLICANT);
                for (int i = applicants.Count; i < appCount; i++)
                {
                    if (applicants.Count <= i)
                    {
                        applicants.Add(new MORTGAGE_APPLICANT());
                    }
                }
                _loanFile.APPLICANTS.APPLICANT = applicants.ToArray();
            }
            else
            {
                _loanFile.APPLICANTS = new MORTGAGE_LOANAPPLICANTS();
                _loanFile.APPLICANTS.APPLICANT = new MORTGAGE_APPLICANT[appCount];
            }

            var startingIndex = 0;
            var hmdaIndex = startingIndex;
            List<BASE_HMDA_XMLITEM> hmdaItems = _loanFile.HMDA_XML?.ITEM?.ToList() ?? new List<BASE_HMDA_XMLITEM>();
            for (int i = startingIndex; i < _loanFile.APPLICANTS.APPLICANT.Length; i++)
            {
                ExportApplication(i);
                ExportHmdaForApplication(i, ref hmdaIndex, _loanFile.APPLICANTS.APPLICANT[i], hmdaItems);
            }

            _loanFile.HMDA_XML = new BASE_HMDA_XML();
            _loanFile.HMDA_XML.ITEM = hmdaItems.ToArray();
        }

        private void ExportHmdaForApplication(int lqbAppIndex, ref int hmdaIndex, MORTGAGE_APPLICANT applicant, List<BASE_HMDA_XMLITEM> hmdaItems)
        {
            CAppData appData = _pageData.GetAppData(lqbAppIndex);

            int applicantIndex = hmdaIndex++;
            this.PopulateGenderItems(applicantIndex, appData.aBGender, hmdaItems);

            if (applicant.SPOUSE != null || appData.aCIsValidNameSsn)
            {
                int spouseIndex = hmdaIndex++;
                this.PopulateGenderItems(spouseIndex, appData.aCGender, hmdaItems);
            }
        }

        private void PopulateGenderItems(int applicantIndex, E_GenderT gender, List<BASE_HMDA_XMLITEM> hmdaItems)
        {
            var genderKey = $"applicant_{applicantIndex}_sex";
            var genderItem = hmdaItems.FirstOrDefault((item) => item.key.Equals(genderKey, StringComparison.OrdinalIgnoreCase));
            if (genderItem == null)
            {
                genderItem = new BASE_HMDA_XMLITEM();
                genderItem.key = genderKey;
                hmdaItems.Add(genderItem);
            }

            var genderNotProvidedKey = $"applicant_{applicantIndex}_sex_not_provided";
            var genderNotProvidedItem = hmdaItems.FirstOrDefault((item) => item.key.Equals(genderNotProvidedKey, StringComparison.OrdinalIgnoreCase));
            if (genderNotProvidedItem == null)
            {
                genderNotProvidedItem = new BASE_HMDA_XMLITEM();
                genderNotProvidedItem.key = genderNotProvidedKey;
                hmdaItems.Add(genderNotProvidedItem);
            }

            this.DetermineHmdaGenderValue(gender, genderItem, genderNotProvidedItem);
            if (genderItem == null)
            {
                hmdaItems.Remove(genderItem);
            }

            if (genderNotProvidedItem == null)
            {
                hmdaItems.Remove(genderNotProvidedItem);
            }
        }

        private void DetermineHmdaGenderValue(E_GenderT gender, BASE_HMDA_XMLITEM genderItem, BASE_HMDA_XMLITEM genderNotProvidedItem)
        {
            switch (gender)
            {
                case E_GenderT.Female:
                    genderItem.value = "FEMALE";
                    genderNotProvidedItem.value = "N";
                    break;
                case E_GenderT.FemaleAndNotFurnished:
                    genderItem.value = "FEMALE";
                    genderNotProvidedItem.value = "Y";
                    break;
                case E_GenderT.Male:
                    genderItem.value = "MALE";
                    genderNotProvidedItem.value = "N";
                    break;
                case E_GenderT.MaleAndFemale:
                    genderItem.value = "OTHER";
                    genderNotProvidedItem.value = "N";
                    break;
                case E_GenderT.MaleAndNotFurnished:
                    genderItem.value = "MALE";
                    genderNotProvidedItem.value = "Y";
                    break;
                case E_GenderT.MaleFemaleNotFurnished:
                    genderItem.value = "OTHER";
                    genderNotProvidedItem.value = "Y";
                    break;
                case E_GenderT.Unfurnished:
                    genderNotProvidedItem.value = "Y";
                    genderItem = null;
                    break;
                case E_GenderT.NA:
                case E_GenderT.LeaveBlank:
                    genderItem = null;
                    genderNotProvidedItem = null;
                    break;
                default:
                    throw new UnhandledEnumException(gender);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="applicantNumber"></param>
        private void ExportApplication(int applicantNumber)
        {
            CAppData appData = _pageData.GetAppData(applicantNumber);
            PopulateAssetList(appData);

            ExportBaseApplicant(E_BorrowerModeT.Borrower, applicantNumber, appData);
            EncodeCreditReport(applicantNumber, appData);
            ExportPresentHousingExpenses(applicantNumber, appData);
            if (_loanFile.APPLICANTS.APPLICANT[applicantNumber].SPOUSE != null || appData.aCIsValidNameSsn)
            {
                ExportBaseApplicant(E_BorrowerModeT.Coborrower, applicantNumber, appData);
            }

            if (_memberMap != null && _memberMap.ContainsKey(appData.aAppId))
            {
                _loanFile.APPLICANTS.APPLICANT[applicantNumber].member_number = _memberMap[appData.aAppId];
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="applicantNumber"></param>
        /// <param name="appData"></param>
        private void EncodeCreditReport(int applicantNumber, CAppData appData)
        {
            var creditObjectNode = _loanFile.APPLICANTS.APPLICANT[applicantNumber].CREDIT_REPORT;
            if (creditObjectNode == null)
            {
                return;
            }
            if (creditObjectNode.report_format != MORTGAGE_APPLICANTCREDIT_REPORTReport_format.MCL_COMMON)
            {
                throw new InvalidDataException("Credit Report Format : " + creditObjectNode.encoding.ToString() + " is not supported.");
            }
            if (creditObjectNode.encoding == MORTGAGE_APPLICANTCREDIT_REPORTEncoding.INNER_TEXT)
            {
                throw new InvalidDataException("Credit report encoding :" + creditObjectNode.encoding.ToString() + " is not supported.");
            }
            if (string.IsNullOrEmpty(creditObjectNode.Value))
            {
                throw new InvalidDataException("Credit data is null or empty");
            }

            XmlDocument doc = Tools.CreateXmlDoc(creditObjectNode.encoding == MORTGAGE_APPLICANTCREDIT_REPORTEncoding.BASE64 ? DecodeBase64String(creditObjectNode.Value) : creditObjectNode.Value);


            doc = ExportLiabilities(doc, appData);

            Tools.LogInfo(doc.OuterXml);
            creditObjectNode.Value = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(doc.OuterXml));
            creditObjectNode.encoding = MORTGAGE_APPLICANTCREDIT_REPORTEncoding.BASE64;
            creditObjectNode.encodingSpecified = true;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="creditReport"></param>
        /// <param name="appData"></param>
        /// <returns></returns>
        private XmlDocument ExportLiabilities(XmlDocument creditReport, CAppData appData)
        {
            XmlNodeList creditReportTradelines = creditReport.SelectNodes("CREDITDATA/TRADELINE");

            foreach (XmlElement tradeline in creditReportTradelines) //delete them all so if they dont match with LO they will be deleted
            {
                tradeline.SetAttribute("deleted", "T");
            }

            IDictionary<int, XmlNode> liabilitiesById = CreateLiabilityMap(creditReportTradelines);
            IDictionary<Guid, int> guidToIdMap = CreateGuidIdMap(liabilitiesById.Keys);

            ILiaCollection liabilitiesInLo = appData.aLiaCollection;
            for (int currentLiabilityIndex = 0; currentLiabilityIndex < liabilitiesInLo.CountRegular; currentLiabilityIndex++)
            {
                ILiabilityRegular currentLiability = liabilitiesInLo.GetRegularRecordAt(currentLiabilityIndex);
                int currentLiabilityId;
                #region add new liability 
                if (!guidToIdMap.TryGetValue(currentLiability.RecordId, out currentLiabilityId))
                {
                    currentLiabilityId = GenerateNewLiabilityId(liabilitiesById.Keys);
                    XmlElement newTradeline = creditReport.CreateElement("", "TRADELINE", "");

                    IDictionary<string, string> attributes = new Dictionary<string, string>() {
                        { "file_id", "" },
                        { "id", currentLiabilityId.ToString()},
                        { "creditor_name", "" },
                        { "account_type_code", "IA" },//lpq default
                        { "balance", "0" },
                        { "payment", "0" },
                        { "comment", "" },
                        { "edited", "T" },
                        { "include_in_debt", "T" },
                        { "will_be_paid", "F" },
                        { "deleted", "F" },
                        { "owner", "1" },
                        { "address", "" },
                        { "city", "" },
                        { "state", "" },
                        { "zip", "" },
                        { "phone", "" },
                        { "date_reported", DateTime.Now.ToString("MM/dd/yy") },
                        { "date_open", DateTime.Now.ToString("MM/dd/yy")},
                        { "status_date", DateTime.Now.ToString("MM/dd/yy") },
                        { "last_activity", DateTime.Now.ToString("yyyyMM") },
                        { "secured", "F" },
                        { "is_current_lender", "F" }
                    };
                    foreach (KeyValuePair<string, string> entry in attributes)
                    {
                        newTradeline.SetAttribute(entry.Key, entry.Value);
                    }
                    creditReport.DocumentElement.AppendChild(newTradeline);
                    liabilitiesById.Add(currentLiabilityId, newTradeline);


                    if (false == _newLiabilitiesId.ContainsKey(appData.aAppId))
                    {
                        _newLiabilitiesId[appData.aAppId] = new List<KeyValuePair<Guid, int>>();
                    }

                    _newLiabilitiesId[appData.aAppId].Add(new KeyValuePair<Guid, int>(currentLiability.RecordId, currentLiabilityId));
                }

                #endregion


                XmlElement tradeLineAsElement = liabilitiesById[currentLiabilityId] as XmlElement;

                tradeLineAsElement.SetAttribute("account_type_code", MapDebtTypeToAccountTypeCode(currentLiability.DebtT));
                tradeLineAsElement.SetAttribute("balance", currentLiability.Bal.ToString());
                tradeLineAsElement.SetAttribute("payment", currentLiability.Pmt.ToString());
                tradeLineAsElement.SetAttribute("comment", currentLiability.Desc);
                tradeLineAsElement.SetAttribute("will_be_paid", currentLiability.WillBePdOff ? "T" : "F");
                tradeLineAsElement.SetAttribute("creditor_name", currentLiability.ComNm);
                tradeLineAsElement.SetAttribute("address", currentLiability.ComAddr);
                tradeLineAsElement.SetAttribute("city", currentLiability.ComCity);
                tradeLineAsElement.SetAttribute("state", currentLiability.ComState);
                tradeLineAsElement.SetAttribute("zip", currentLiability.ComZip);
                tradeLineAsElement.SetAttribute("phone", CleanUpPhoneNumber(currentLiability.ComPhone));
                tradeLineAsElement.SetAttribute("fax", CleanUpPhoneNumber(currentLiability.ComFax));
                tradeLineAsElement.SetAttribute("owner", MapToOwnerId(currentLiability.OwnerT));
                tradeLineAsElement.SetAttribute("term", currentLiability.OrigTerm_rep);
                tradeLineAsElement.SetAttribute("day_30", currentLiability.Late30_rep);
                tradeLineAsElement.SetAttribute("day_60", currentLiability.Late60_rep);
                tradeLineAsElement.SetAttribute("day_90", currentLiability.Late90Plus_rep);
                tradeLineAsElement.SetAttribute("include_in_debt", currentLiability.NotUsedInRatio ? "F" : "T");
                tradeLineAsElement.SetAttribute("account_number", currentLiability.AccNum.Value);
                tradeLineAsElement.SetAttribute("deleted", "F");  //undelete since they exist in LO   
            }
            return creditReport;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string CleanUpPhoneNumber(string p)
        {
            if (string.IsNullOrEmpty(p))
            {
                return string.Empty;
            }
            return Regex.Replace(p, "[(]|[ ]|[-]|[)]", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        private IDictionary<Guid, int> CreateGuidIdMap(ICollection<int> ids)
        {
            IDictionary<Guid, int> guidsAndTheirID = new Dictionary<Guid, int>();
            foreach (var id in ids)
            {
                guidsAndTheirID.Add(CLiaRegular.ConvertStringToGuid(id.ToString()), id);
            }
            return guidsAndTheirID;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="e_DebtRegularT"></param>
        /// <returns></returns>
        private static string MapDebtTypeToAccountTypeCode(E_DebtRegularT e_DebtRegularT)
        {
            switch (e_DebtRegularT)
            {
                case E_DebtRegularT.Installment:
                    return LendersOffice.CreditReport.Mcl.Code.CMclCodeTradelineT.Installment;
                case E_DebtRegularT.Mortgage:
                    return LendersOffice.CreditReport.Mcl.Code.CMclCodeTradelineT.Mortgage;
                case E_DebtRegularT.Open:
                    return LendersOffice.CreditReport.Mcl.Code.CMclCodeTradelineT.OpenAccount_OO;
                case E_DebtRegularT.Revolving:
                    return LendersOffice.CreditReport.Mcl.Code.CMclCodeTradelineT.Revolving;
                case E_DebtRegularT.Other:
                    return LendersOffice.CreditReport.Mcl.Code.CMclCodeTradelineT.Other;
                default:
                    throw new ArgumentException("Unsupported debt type in LO liability " + e_DebtRegularT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="e_LiaOwnerT"></param>
        /// <returns></returns>
        private static string MapToOwnerId(E_LiaOwnerT e_LiaOwnerT)
        {
            switch (e_LiaOwnerT)
            {
                case E_LiaOwnerT.Borrower:
                    return "1";
                case E_LiaOwnerT.CoBorrower:
                    return "2";
                case E_LiaOwnerT.Joint:
                    return "3";
                default:
                    throw new ArgumentException("Unsupported Owner id in LO Liability " + e_LiaOwnerT.ToString());
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="creditReportTradelines"></param>
        /// <returns></returns>
        private IDictionary<int, XmlNode> CreateLiabilityMap(XmlNodeList creditReportTradelines)
        {
            IDictionary<int, XmlNode> liabilities = new Dictionary<int, XmlNode>();
            foreach (XmlNode tradeline in creditReportTradelines)
            {
                if (tradeline.Attributes["id"] == null || tradeline.Attributes["id"].Value == null)
                {
                    throw new ArgumentException("Found a tradeline without an id. All tradelines are required to have an id.");
                }

                int currentId;
                if (!int.TryParse(tradeline.Attributes["id"].Value, out currentId))
                {
                    throw new ArgumentException("A tradeline does not have an int for an ID.");
                }
                liabilities.Add(currentId, tradeline);
            }
            return liabilities;
        }

        /// <summary>
        /// Generates a new random id thats not in the passed in collection.
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        private int GenerateNewLiabilityId(ICollection<int> ids)
        {
            int potentialId;
            do
            {
                potentialId = _randomnessProvider.Next(1000);

            } while (ids.Contains(potentialId));

            return potentialId;
        }


        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static string DecodeBase64String(string p)
        {
            byte[] b = System.Convert.FromBase64String(p);
            string xml = System.Text.Encoding.UTF8.GetString(b);
            return xml;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="appData"></param>
        private void ExportPresentHousingExpenses(int applicationNumber, CAppData appData)
        {
            _loanFile.APPLICANTS.APPLICANT[applicationNumber].PRESENT_HOUSING_EXPENSE = _loanFile.APPLICANTS.APPLICANT[applicationNumber].PRESENT_HOUSING_EXPENSE ?? new MORTGAGE_APPLICANTPRESENT_HOUSING_EXPENSE();
            MORTGAGE_APPLICANTPRESENT_HOUSING_EXPENSE presentHousingExpense = _loanFile.APPLICANTS.APPLICANT[applicationNumber].PRESENT_HOUSING_EXPENSE;

            presentHousingExpense.hazard_insurance = appData.aPresHazIns_rep;
            presentHousingExpense.mortgage_insurance = appData.aPresMIns_rep;
            presentHousingExpense.hoa_due = appData.aPresHoAssocDues_rep;
            presentHousingExpense.monthly_payment = appData.aPres1stM_rep;
            presentHousingExpense.other_pi = appData.aPresOFin_rep;
            presentHousingExpense.other_expense = appData.aPresOHExp_rep;
            presentHousingExpense.real_estate_tax = appData.aPresRealETx_rep;
        }

        private void ExportBaseApplicant(E_BorrowerModeT e_BorrowerModeT, int applicantNumber, CAppData appData)
        {
            appData.BorrowerModeT = e_BorrowerModeT;
            MORTGAGE_APPLICANT application = _loanFile.APPLICANTS.APPLICANT[applicantNumber];
            MORTGAGE_BASE_APPLICANT applicant;
            if (e_BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                application.SPOUSE = application.SPOUSE ?? new MORTGAGE_BASE_APPLICANT();
                applicant = application.SPOUSE;
                applicant.applicant_type = base_applicant_type.C;
            }
            else
            {
                if (_loanFile.APPLICANTS.APPLICANT[applicantNumber] == null)
                {
                    _loanFile.APPLICANTS.APPLICANT[applicantNumber] = new MORTGAGE_APPLICANT();
                    _loanFile.APPLICANTS.APPLICANT[applicantNumber].applicant_type = appData.aIsPrimary ? base_applicant_type.P : base_applicant_type.C;
                }
                applicant = _loanFile.APPLICANTS.APPLICANT[applicantNumber];
            }

            #region base applicant stuff
            applicant.first_name = appData.aFirstNm;
            applicant.last_name = appData.aLastNm;
            applicant.middle_name = appData.aMidNm;
            applicant.ssn = appData.aSsn.Replace("-", "");
            applicant.dependents = appData.aDependNum_rep;
            applicant.suffix = MapLOSuffixToLoansPQ(appData.aSuffix);
            applicant.suffixSpecified = true;
            applicant.ages_of_dependents = appData.aDependAges;
            applicant.school_months = appData.aSchoolTotalMonths_rep;
            applicant.dob = appData.aDob_rep;
            applicant.interview_method = MapToLoansPQ(appData.aInterviewMethodT);
            applicant.interview_methodSpecified = true;

            if (appData.aDecCitizen.ToUpper() == "Y")
            {
                applicant.citizenship = BASE_APPLICANTCitizenship.USCITIZEN;
                applicant.citizenshipSpecified = true;
            }
            else if (appData.aDecResidency.ToUpper() == "Y")
            {
                applicant.citizenship = BASE_APPLICANTCitizenship.PERMRESIDENT;
                applicant.citizenshipSpecified = true;
            }
            else if (appData.aDecResidency.ToUpper() == "N")
            {
                applicant.citizenship = BASE_APPLICANTCitizenship.NONPERMRESIDENT;
                applicant.citizenshipSpecified = true;
            }
            applicant.marital_statusSpecified = true;
            applicant.marital_status = MapToLoansPQ(appData.aMaritalStatT);
            applicant.gender = ToLoansPQ(appData.aGenderFallback);
            applicant.genderSpecified = true;

            applicant.ethnicitySpecified = true;
            applicant.ethnicity = ToLPQ(appData.aHispanicTFallback);



            //av no opm second send mid of 3 lower of 2 or single score.

            int score = appData.aSecondHighestScore;
            if (score == 0) //if second highest is 0  means that there is 1 score or no score 
            {
                score = appData.aHighestScore; //just use the highest score its either 0 or an actual number
            }
            applicant.credit_score = score.ToString();


            #region races
            List<base_race_type_single> races = new List<base_race_type_single>(5);
            if (appData.aIsWhite)
            {
                races.Add(base_race_type_single.WHITE);
            }
            if (appData.aIsAmericanIndian)
            {
                races.Add(base_race_type_single.AMERICAN_INDIAN);
            }
            if (appData.aIsAsian)
            {
                races.Add(base_race_type_single.ASIAN);
            }
            if (appData.aIsBlack)
            {
                races.Add(base_race_type_single.BLACK);
            }
            if (appData.aIsPacificIslander)
            {
                races.Add(base_race_type_single.PACIFIC_ISLANDER);
            }
            if (races.Count != 0) //empty array causes import problems
            {
                applicant.race = races.ToArray();
            }
            #endregion


            applicant.declined_answer_race_gender = appData.aNoFurnish ? BASE_APPLICANTDeclined_answer_race_gender.Y : BASE_APPLICANTDeclined_answer_race_gender.N;
            applicant.declined_answer_race_genderSpecified = true;

            #endregion

            #region current address
            if (applicant.CURRENT_ADDRESS == null)
            {
                applicant.CURRENT_ADDRESS = new MORTGAGE_BASE_APPLICANTCURRENT_ADDRESS();
            }
            if (!(applicant.CURRENT_ADDRESS.Item is BASE_ADDRESS_THREE_LINE))
            {
                ExportCurrentAddress(appData, applicant);
            }

            #endregion

            #region previous address
            {
                if (appData.aPrev1Addr != "" || appData.aPrev1City != "" || appData.aPrev1State != "" || appData.aPrev1Zip != "")
                {

                    if (applicant.PREVIOUS_ADDRESS == null)
                    {
                        applicant.PREVIOUS_ADDRESS = new MORTGAGE_BASE_APPLICANTPREVIOUS_ADDRESS();
                    }
                    //    MORTGAGE_BASE_APPLICANTPREVIOUS_ADDRESS prevAddress = new MORTGAGE_BASE_APPLICANTPREVIOUS_ADDRESS();
                    // BASE_ADDRESS_LOOSE prevAddress = applicant.PREVIOUS_ADDRESS ?? new BASE_ADDRESS_LOOSE();
                    applicant.PREVIOUS_ADDRESS.street_address_1 = appData.aPrev1Addr;
                    if (appData.aPrev1Addr != string.Empty)
                    {
                        applicant.PREVIOUS_ADDRESS.street_address_2 = "";
                    }
                    applicant.PREVIOUS_ADDRESS.city = appData.aPrev1City;
                    applicant.PREVIOUS_ADDRESS.zip = appData.aPrev1Zip;
                    applicant.PREVIOUS_ADDRESS.state = appData.aPrev1State;
                    applicant.PREVIOUS_ADDRESS.occupancy_status = MapToLoansPQ((E_aBAddrT)appData.aPrev1AddrT);
                    applicant.PREVIOUS_ADDRESS.occupancy_statusSpecified = true;
                    applicant.PREVIOUS_ADDRESS.occupancy_duration = appData.aPrev1AddrTotalMonths;
                }
                else if (applicant.PREVIOUS_ADDRESS != null)
                {
                    applicant.PREVIOUS_ADDRESS = null;
                }
            }
            #endregion

            #region financial info
            {
                if (applicant.FINANCIAL_INFO == null)
                {
                    applicant.FINANCIAL_INFO = new MORTGAGE_BASE_APPLICANTFINANCIAL_INFO();
                }

                MORTGAGE_BASE_APPLICANTFINANCIAL_INFO financialInfo = applicant.FINANCIAL_INFO;
                IEmpCollection employment = appData.aEmpCollection;

                List<MORTGAGE_BASE_APPLICANTFINANCIAL_INFOPREVIOUS_EMPLOYMENT> previousEmployments = new List<MORTGAGE_BASE_APPLICANTFINANCIAL_INFOPREVIOUS_EMPLOYMENT>();
                List<MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT> currentEmployments = new List<MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT>();

                // Add the primary employment
                IPrimaryEmploymentRecord borPrimaryEmp = appData.aEmpCollection.GetPrimaryEmp(false);
                if (borPrimaryEmp != null)
                {
                    MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT primaryEmployment = new MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT();
                    var populated = this.PopulateBaseEmployment(borPrimaryEmp, primaryEmployment);

                    if (populated || !string.IsNullOrEmpty(borPrimaryEmp.ProfLenTotalMonths) || !string.IsNullOrEmpty(borPrimaryEmp.EmplmtLenTotalMonths) || !string.IsNullOrEmpty(borPrimaryEmp.EmpltStartD_rep))
                    {
                        primaryEmployment.profession_months = borPrimaryEmp.ProfLenTotalMonths;
                        primaryEmployment.employed_months = borPrimaryEmp.EmplmtLenTotalMonths;
                        primaryEmployment.employment_start_date = borPrimaryEmp.EmpltStartD_rep;

                        currentEmployments.Add(primaryEmployment);
                    }
                }

                // All employments have empGroupT == Previous except for the Primary employment.
                ISubcollection employmentRecords = employment.GetSubcollection(true, E_EmpGroupT.Previous);
                if (employmentRecords.Count > 0)
                {
                    for (int i = 0; i < employmentRecords.Count; i++)
                    {
                        var empRecord = employmentRecords.GetRecordAt(i) as IRegularEmploymentRecord;
                        if (!empRecord.IsCurrent)
                        {
                            previousEmployments.Add(this.CreatePreviousEmployment(empRecord));
                        }
                        else
                        {
                            var currentEmployment = this.CreateCurrentEmployment(empRecord);
                            currentEmployment.employment_start_date = empRecord.EmplmtStartD_rep;
                            
                            DateTime startDate;
                            if (DateTime.TryParse(empRecord.EmplmtStartD_rep, out startDate))
                            {
                                DateTime endDate = DateTime.TryParse(empRecord.EmplmtEndD_rep, out endDate) ? endDate : DateTime.Now;
                                currentEmployment.employed_months = Math.Abs(12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month).ToString();
                            }

                            currentEmployments.Add(currentEmployment);
                        }
                    }
                }

                financialInfo.CURRENT_EMPLOYMENT = currentEmployments.Count > 0 ? currentEmployments.ToArray() : null;
                financialInfo.PREVIOUS_EMPLOYMENT = previousEmployments.Count > 0 ? previousEmployments.ToArray() : null;

                #region monthly income  [POSSIBLE-COMPLETE] 
                {
                    financialInfo.MONTHLY_INCOME = financialInfo.MONTHLY_INCOME ?? new MORTGAGE_BASE_APPLICANTFINANCIAL_INFOMONTHLY_INCOME();
                    financialInfo.MONTHLY_INCOME.monthly_income_base_salary = appData.aBaseI_rep;
                    financialInfo.MONTHLY_INCOME.monthly_income_over_time = appData.aOvertimeI_rep;
                    financialInfo.MONTHLY_INCOME.monthly_income_bonus = appData.aBonusesI_rep;
                    financialInfo.MONTHLY_INCOME.monthly_income_commission = appData.aCommisionI_rep;
                    financialInfo.MONTHLY_INCOME.monthly_income_dividends = appData.aDividendI_rep;
                    financialInfo.MONTHLY_INCOME.monthly_income_net_rental = appData.aNetRentI1003_rep;

                    List<BASE_MONTHLY_INCOMEOTHER_INCOME> otherIncomes = new List<BASE_MONTHLY_INCOMEOTHER_INCOME>();
                    foreach (var otherIncome in appData.aOtherIncomeList.Where((income) => this.ToBorrower(income.IsForCoBorrower) == e_BorrowerModeT && income.Amount != 0))
                    {
                        var income = new BASE_MONTHLY_INCOMEOTHER_INCOME();
                        income.monthly_income = appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep);
                        income.monthly_income_description = otherIncome.Desc;

                        otherIncomes.Add(income);
                    }

                    // It looks like we clear this out and we add our own other incomes.
                    financialInfo.MONTHLY_INCOME.SPECIAL_INCOME = null;
                    financialInfo.MONTHLY_INCOME.OTHER_INCOME = otherIncomes.ToArray();
                }
                #endregion

                #region monthly debt
                {
                    financialInfo.MONTHLY_DEBT = financialInfo.MONTHLY_DEBT ?? new BASE_MONTHLY_DEBT();
                    //financialInfo.MONTHLY_DEBT.aggregate_amount_with_lender;
                    //financialInfo.MONTHLY_DEBT.aggregate_amount_with_lenderSpecified;
                    financialInfo.MONTHLY_DEBT.alimony = "0";
                    financialInfo.MONTHLY_DEBT.child_support = "0";
                    financialInfo.MONTHLY_DEBT.job_expense = "0";
                    //financialInfo.MONTHLY_DEBT.monthly_housing_cost;
                    //financialInfo.MONTHLY_DEBT.monthly_housing_costSpecified;
                    financialInfo.MONTHLY_DEBT.monthly_liability = "0";
                    //financialInfo.MONTHLY_DEBT.monthly_mortgage_payment;
                    //financialInfo.MONTHLY_DEBT.monthly_mortgage_paymentSpecified;

                    if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
                    {
                        financialInfo.MONTHLY_DEBT.monthly_rent = appData.aPresRent_rep;
                        financialInfo.MONTHLY_DEBT.monthly_rentSpecified = true;
                    }
                    //financialInfo.MONTHLY_DEBT.other_expense_1 = "0"; ;
                    //financialInfo.MONTHLY_DEBT.other_expense_2 = "0";
                    //financialInfo.MONTHLY_DEBT.other_expense_description_1 = "";
                    //financialInfo.MONTHLY_DEBT.other_expense_description_2 = "";
                    financialInfo.MONTHLY_DEBT.separate_maintenance_expense = "0";
                    //financialInfo.MONTHLY_DEBT.tax_expense = "0";

                    ILiaCollection liabilities = appData.aLiaCollection;

                    if (e_BorrowerModeT == E_BorrowerModeT.Borrower)
                    {
                        var alimony = liabilities.GetAlimony(false);
                        var job = liabilities.GetJobRelated1(false);
                        var childSupport = liabilities.GetChildSupport(false);
                        if (alimony != null)
                        {
                            financialInfo.MONTHLY_DEBT.alimony = alimony.Pmt_rep;
                        }
                        if (job != null)
                        {
                            financialInfo.MONTHLY_DEBT.job_expense = job.Pmt_rep;
                        }
                        if (childSupport != null)
                        {
                            financialInfo.MONTHLY_DEBT.child_support = childSupport.Pmt_rep;
                        }

                        decimal totalLiability = appData.aLiaMonTot;
                        decimal alimonyField = 0;
                        decimal jobExpense = 0;
                        decimal childSupportField = 0;
                        if (decimal.TryParse(financialInfo.MONTHLY_DEBT.job_expense, out jobExpense))
                        {
                            totalLiability -= jobExpense;
                        }
                        if (decimal.TryParse(financialInfo.MONTHLY_DEBT.alimony, out alimonyField))
                        {
                            totalLiability -= alimonyField;
                        }
                        if (decimal.TryParse(financialInfo.MONTHLY_DEBT.child_support, out childSupportField))
                        {
                            totalLiability += childSupportField;
                        }

                        financialInfo.MONTHLY_DEBT.monthly_liability = Math.Round(totalLiability, 2).ToString();
                        financialInfo.MONTHLY_DEBT.monthly_liabilitySpecified = true;
                    }

                }
                #endregion
            }

            #endregion

            #region contact info
            {
                applicant.CONTACT_INFO = applicant.CONTACT_INFO ?? new BASE_CONTACT_INFO();
                applicant.CONTACT_INFO.cell_phone = appData.aCellPhone;
                applicant.CONTACT_INFO.email = appData.aEmail;
                applicant.CONTACT_INFO.fax_number = appData.aFax;
                applicant.CONTACT_INFO.home_phone = appData.aHPhone;

                string phone;
                string extension;
                GetPhoneAndExtension(appData.aBusPhone, out phone, out extension);
                applicant.CONTACT_INFO.work_phone = phone;
                applicant.CONTACT_INFO.work_phone_extension = extension;


            }
            #endregion

            #region assets
            {
                applicant.ASSETS = e_BorrowerModeT == E_BorrowerModeT.Borrower ? _borrowerAssets.ToArray() : _coborrowerAssets.ToArray();
            }
            #endregion

            #region declarations
            {
                applicant.DECLARATION = applicant.DECLARATION ?? new MORTGAGE_DECLARATION();
                applicant.DECLARATION.has_outstanding_judgement = LoDeclerationAnswerToLPQ(appData.aDecJudgment);
                applicant.DECLARATION.has_bankruptcy = LoDeclerationAnswerToLPQ(appData.aDecBankrupt);
                applicant.DECLARATION.is_in_lawsuit = LoDeclerationAnswerToLPQ(appData.aDecLawsuit);
                applicant.DECLARATION.has_bad_loan = LoDeclerationAnswerToLPQ(appData.aDecForeclosure);
                applicant.DECLARATION.is_related_to_bad_loan = LoDeclerationAnswerToLPQ(appData.aDecObligated);
                applicant.DECLARATION.has_owner_ship_interest = LoDeclerationAnswerToLPQ(appData.aDecPastOwnership);
                applicant.DECLARATION.has_separate_maintenance = LoDeclerationAnswerToLPQ(appData.aDecAlimony);
                applicant.DECLARATION.is_down_payment_borrowed = LoDeclerationAnswerToLPQ(appData.aDecBorrowing);
                applicant.DECLARATION.is_endorser_on_note = LoDeclerationAnswerToLPQ(appData.aDecEndorser);
                applicant.DECLARATION.is_presently_delinquent = LoDeclerationAnswerToLPQ(appData.aDecDelinquent);
                applicant.DECLARATION.is_property_primary_residence = LoDeclerationAnswerToLPQ(appData.aDecOcc);
                applicant.DECLARATION.title_hold_type = MapToLoansPQ(appData.aDecPastOwnedPropTitleT);
                applicant.DECLARATION.type_of_property_owned = MapToLoansPQ(appData.aDecPastOwnedPropT);

                applicant.DECLARATION.has_outstanding_judgementSpecified = true;
                applicant.DECLARATION.has_bankruptcySpecified = true;
                applicant.DECLARATION.is_in_lawsuitSpecified = true;
                applicant.DECLARATION.has_bad_loanSpecified = true;
                applicant.DECLARATION.is_related_to_bad_loanSpecified = true;
                applicant.DECLARATION.has_owner_ship_interestSpecified = true;
                applicant.DECLARATION.has_separate_maintenanceSpecified = true;
                applicant.DECLARATION.is_down_payment_borrowedSpecified = true;
                applicant.DECLARATION.is_endorser_on_noteSpecified = true;
                applicant.DECLARATION.is_presently_delinquentSpecified = true;
                applicant.DECLARATION.is_property_primary_residenceSpecified = true;
                applicant.DECLARATION.title_hold_typeSpecified = true;
                applicant.DECLARATION.type_of_property_ownedSpecified = true;

            }
            #endregion

            #region mail address
            applicant.MAILING_ADDRESS = applicant.MAILING_ADDRESS ?? new MORTGAGE_BASE_APPLICANTMAILING_ADDRESS();
            applicant.MAILING_ADDRESS.is_current = appData.aAddrMailUsePresentAddr ? BASE_APPLICANTDeclined_answer_race_gender.Y : BASE_APPLICANTDeclined_answer_race_gender.N;
            if (!appData.aAddrMailUsePresentAddr)
            {
                applicant.MAILING_ADDRESS.city = appData.aCityMail;
                applicant.MAILING_ADDRESS.state = appData.aStateMail;
                applicant.MAILING_ADDRESS.street_address_1 = appData.aAddrMail;
                applicant.MAILING_ADDRESS.zip = appData.aZipMail;
                applicant.MAILING_ADDRESS.street_address_2 = "";
            }
            #endregion
        }

        private MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT CreateCurrentEmployment(IEmploymentRecord loCurrentEmployment)
        {
            var currentEmployment = new MORTGAGE_BASE_APPLICANTFINANCIAL_INFOCURRENT_EMPLOYMENT();
            this.PopulateBaseEmployment(loCurrentEmployment, currentEmployment);

            return currentEmployment;
        }

        private MORTGAGE_BASE_APPLICANTFINANCIAL_INFOPREVIOUS_EMPLOYMENT CreatePreviousEmployment(IRegularEmploymentRecord loPreviousEmployment)
        {
            var previousEmployment = new MORTGAGE_BASE_APPLICANTFINANCIAL_INFOPREVIOUS_EMPLOYMENT();
            this.PopulateBaseEmployment(loPreviousEmployment, previousEmployment);

            DateTime startDate;
            if (DateTime.TryParse(loPreviousEmployment.EmplmtStartD_rep, out startDate))
            {
                previousEmployment.employment_start_date = loPreviousEmployment.EmplmtStartD_rep;
                previousEmployment.employment_start_dateSpecified = true;
                DateTime endDate;
                if (DateTime.TryParse(loPreviousEmployment.EmplmtEndD_rep, out endDate))
                {
                    previousEmployment.employed_months = Math.Abs(12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month).ToString();
                    previousEmployment.employment_end_date = loPreviousEmployment.EmplmtEndD_rep;
                }
            }

            previousEmployment.monthly_income = loPreviousEmployment.MonI_rep;

            return previousEmployment;
        }

        private bool PopulateBaseEmployment(IEmploymentRecord empRec, BASE_EMPLOYMENT employment)
        {
            employment.employment_status = empRec.IsSelfEmplmt ? BASE_EMPLOYMENTEmployment_status.SE : BASE_EMPLOYMENTEmployment_status.EM;
            employment.employment_statusSpecified = true;

            if (string.IsNullOrEmpty(empRec.EmplrNm) && string.IsNullOrEmpty(empRec.EmplrBusPhone) && string.IsNullOrEmpty(empRec.EmplrAddr) &&
                string.IsNullOrEmpty(empRec.EmplrCity) && string.IsNullOrEmpty(empRec.EmplrState) && string.IsNullOrEmpty(empRec.EmplrZip) && string.IsNullOrEmpty(empRec.JobTitle))
            {
                return false;
            }

            employment.employer = empRec.EmplrNm;
            employment.employment_phone = empRec.EmplrBusPhone;
            employment.employment_address = empRec.EmplrAddr;
            employment.employment_city = empRec.EmplrCity;
            employment.employment_state = empRec.EmplrState;
            employment.employment_zip = empRec.EmplrZip;
            employment.occupation = empRec.JobTitle;

            return true;
        }

        private static BASE_APPLICANTEthnicity ToLPQ(E_aHispanicT e_aHispanicT)
        {
            switch (e_aHispanicT)
            {
                case E_aHispanicT.LeaveBlank:
                    return BASE_APPLICANTEthnicity.Item;
                case E_aHispanicT.Hispanic:
                    return BASE_APPLICANTEthnicity.HISPANIC;
                case E_aHispanicT.NotHispanic:
                    return BASE_APPLICANTEthnicity.NOT_HISPANIC;
                default:
                    throw new ArgumentException("Unhandled Ethnicity Type " + e_aHispanicT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="appData"></param>
        /// <param name="applicant"></param>
        private void ExportCurrentAddress(CAppData appData, MORTGAGE_BASE_APPLICANT applicant)
        {
            applicant.CURRENT_ADDRESS = applicant.CURRENT_ADDRESS ?? new MORTGAGE_BASE_APPLICANTCURRENT_ADDRESS();
            BASE_ADDRESS_LOOSE looseAddress;
            if (applicant.CURRENT_ADDRESS.Item == null || !(applicant.CURRENT_ADDRESS.Item is BASE_ADDRESS_LOOSE))
            {
                looseAddress = new BASE_ADDRESS_LOOSE();
                applicant.CURRENT_ADDRESS.Item = looseAddress;
            }
            else
            {
                looseAddress = applicant.CURRENT_ADDRESS.Item as BASE_ADDRESS_LOOSE;
            }

            looseAddress.city = appData.aCity;
            looseAddress.state = appData.aState;
            looseAddress.street_address_1 = appData.aAddr;
            if (appData.aAddr == string.Empty)
            {
                looseAddress.street_address_2 = "";
            }
            looseAddress.zip = appData.aZip;
            applicant.CURRENT_ADDRESS.occupancy_status = MapToLoansPQ(appData.aAddrT);
            applicant.CURRENT_ADDRESS.occupancy_statusSpecified = true;
            applicant.CURRENT_ADDRESS.occupancy_duration = appData.aAddrTotalMonths;
        }
        private void GetPhoneAndExtension(string fullphone, out string phone, out string extension)
        {
            phone = fullphone;
            extension = "";
            if (string.IsNullOrEmpty(fullphone))
            {
                return;
            }
            int e = fullphone.LastIndexOf('e');
            if (fullphone.Contains('e') && e > 0 && e + 1 <= fullphone.Length)
            {

                phone = fullphone.Substring(0, e);
                extension = fullphone.Substring(e + 1);
            }
            else if (e == 0)
            {
                phone = ""; //no phone number but there was an extension
                extension = fullphone.Substring(e + 1);
            }


        }
        private E_BorrowerModeT ToBorrower(bool p)
        {
            return p ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
        }

        private static BASE_APPLICANTGender ToLoansPQ(E_GenderT e_GenderT)
        {
            switch (e_GenderT)
            {
                case E_GenderT.Male:
                    return BASE_APPLICANTGender.MALE;
                case E_GenderT.Female:
                    return BASE_APPLICANTGender.FEMALE;
                case E_GenderT.NA:
                case E_GenderT.LeaveBlank:
                    return BASE_APPLICANTGender.Item;
                case E_GenderT.MaleFemaleNotFurnished:
                case E_GenderT.MaleAndNotFurnished:
                case E_GenderT.FemaleAndNotFurnished:
                case E_GenderT.Unfurnished:
                case E_GenderT.MaleAndFemale:
                    return BASE_APPLICANTGender.OTHER;
                default:
                    throw new ArgumentException("Unhandled gender type: " + e_GenderT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="appData"></param>
        private void PopulateAssetList(CAppData appData)
        {
            IAssetCollection assetCollection = appData.aAssetCollection;
            _borrowerAssets = new List<BASE_ASSET>();
            _coborrowerAssets = new List<BASE_ASSET>();

            #region handle assets that are always present
            {
                var buisnessAsset = assetCollection.GetBusinessWorth(false);
                if (buisnessAsset != null && buisnessAsset.Val != 0M)
                {
                    _borrowerAssets.Add(CreateAsset(buisnessAsset));
                }
            }
            {
                var lifeInsurance = assetCollection.GetLifeInsurance(false);
                if (lifeInsurance != null && lifeInsurance.Val != 0M)
                {
                    _borrowerAssets.Add(CreateAsset(lifeInsurance));
                }
            }
            var reitrementAsset = assetCollection.GetRetirement(false);
            if (reitrementAsset != null && reitrementAsset.Val != 0M)
            {
                _borrowerAssets.Add(CreateAsset(reitrementAsset));
            }
            {
                var cashDeposit1Asset = assetCollection.GetCashDeposit1(false);
                if (cashDeposit1Asset != null && cashDeposit1Asset.Val != 0M)
                {
                    _borrowerAssets.Add(CreateAsset(cashDeposit1Asset));
                }
            }
            {
                var cashDeposit2Asset = assetCollection.GetCashDeposit2(false);
                if (cashDeposit2Asset != null && cashDeposit2Asset.Val != 0M)
                {
                    _borrowerAssets.Add(CreateAsset(cashDeposit2Asset));

                }
            }
            #endregion 

            for (int i = 0; i < assetCollection.CountRegular; i++)
            {
                var tempAsset = assetCollection.GetRegularRecordAt(i);
                if (tempAsset.OwnerT == E_AssetOwnerT.Borrower || tempAsset.OwnerT == E_AssetOwnerT.Joint)
                {
                    _borrowerAssets.Add(CreateAsset(tempAsset, appData));
                }
                else
                {
                    _coborrowerAssets.Add(CreateAsset(tempAsset, appData));
                }
            }



            var reCollection = appData.aReCollection;
            for (int i = 0; i < reCollection.CountRegular; i++)
            {
                var currentReo = reCollection.GetRegularRecordAt(i);
                if (currentReo.ReOwnerT == E_ReOwnerT.CoBorrower)
                {
                    _coborrowerAssets.Add(CreateReoAsset(currentReo));
                }
                else
                {
                    _borrowerAssets.Add(CreateReoAsset(currentReo));
                }
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="e_aBAddrT"></param>
        /// <returns></returns>
        private static MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status MapToLoansPQ(E_aBAddrT e_aBAddrT)
        {
            switch (e_aBAddrT)
            {
                case E_aBAddrT.Own:
                    return MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.OWN;
                case E_aBAddrT.Rent:
                    return MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.RENT;
                case E_aBAddrT.LivingRentFree:
                    return MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.LIVEWITHPARENTS;
                case E_aBAddrT.LeaveBlank:
                    return MORTGAGE_BASE_APPLICANTCURRENT_ADDRESSOccupancy_status.Item;
                default:
                    throw new ArgumentException("Unhandled Occupancy Status " + e_aBAddrT);
            }
        }

        private string GetNullIfEmpty(string p)
        {
            return p == null || p == "" ? null : p;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="e_aBMaritalStatT"></param>
        /// <returns></returns>
        private static BASE_APPLICANTMarital_status MapToLoansPQ(E_aBMaritalStatT e_aBMaritalStatT)
        {
            switch (e_aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married:
                    return BASE_APPLICANTMarital_status.MARRIED;
                case E_aBMaritalStatT.NotMarried:
                    return BASE_APPLICANTMarital_status.UNMARRIED;
                case E_aBMaritalStatT.Separated:
                    return BASE_APPLICANTMarital_status.SEPARATED;
                case E_aBMaritalStatT.LeaveBlank:
                    return BASE_APPLICANTMarital_status.Item;
                default:
                    throw new ArgumentException("Unhandled marital status " + e_aBMaritalStatT);
            }
        }
        /// <summary>
        /// Tries to map a string p to suffix type. If it doesnt match one of the LPQ types then it returns lpq empty.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static BASE_APPLICANTSuffix MapLOSuffixToLoansPQ(string p)
        {
            p = p ?? "";
            p = p.TrimWhitespaceAndBOM().ToUpper();
            switch (p)
            {
                case "JR.":
                case "JR":
                    return BASE_APPLICANTSuffix.JR;
                case "SR.":
                case "SR":
                    return BASE_APPLICANTSuffix.SR;
                case "II":
                    return BASE_APPLICANTSuffix.II;
                case "III":
                    return BASE_APPLICANTSuffix.III;
                case "IV":
                    return BASE_APPLICANTSuffix.IV;
                case "V":
                    return BASE_APPLICANTSuffix.V;
                case "VI":
                    return BASE_APPLICANTSuffix.VI;
                case "VII":
                    return BASE_APPLICANTSuffix.VII;
                case "VIII":
                    return BASE_APPLICANTSuffix.VIII;
                default:
                    return BASE_APPLICANTSuffix.Item;
            }
        }

        #region  MortgageLoan Updater Helper Methods  
        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="gseClassType"></param>
        /// <returns></returns>
        private static string MapToLPQGseProjectClass(E_sSpProjClassT gseClassType)
        {
            switch (gseClassType)
            {
                case E_sSpProjClassT.LeaveBlank:
                    return "";
                case E_sSpProjClassT.AIIICondo:
                    return "A_IIICONDOMINIUM";
                case E_sSpProjClassT.BIICondo:
                    return "B_IICONDOMINIUM";
                case E_sSpProjClassT.CICondo:
                    return "C_ICONDOMINIUM";
                case E_sSpProjClassT.EPUD:
                    return "E_PUD";
                case E_sSpProjClassT.FPUD:
                    return "F_PUD";
                case E_sSpProjClassT.IIIPUD:
                    return "III_PUD";
                case E_sSpProjClassT.COOP1:
                    return "1_COOP";
                case E_sSpProjClassT.COOP2:
                    return "2_COOP";
                default:
                    throw new ArgumentException("Unhandled Gse Project Class Type " + gseClassType);
            }

        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="currentReo"></param>
        /// <returns></returns>
        private BASE_ASSET CreateReoAsset(IRealEstateOwned currentReo)
        {
            BASE_ASSET_REAL_ESTATE reoAsset = new BASE_ASSET_REAL_ESTATE();
            reoAsset.asset_type = BASE_ASSETAsset_type.REALESTATE;
            reoAsset.asset_value = currentReo.Val_rep;
            reoAsset.property_address = currentReo.Addr;
            reoAsset.property_city = currentReo.City;
            reoAsset.property_zip = currentReo.Zip;
            reoAsset.property_state = currentReo.State;
            reoAsset.property_type = ReoTypeToLPQ(currentReo.Type);
            reoAsset.property_typeSpecified = true;
            reoAsset.existing_loan_amount = currentReo.MAmt_rep;
            reoAsset.property_net_rental_income = currentReo.NetRentI_rep;
            reoAsset.property_net_rental_incomeSpecified = true;
            reoAsset.property_gross_rental_income = currentReo.GrossRentI_rep;
            reoAsset.property_mortgage_payments = currentReo.MPmt_rep;
            reoAsset.is_subject_property = ToYN(currentReo.IsSubjectProp);
            reoAsset.is_subject_propertySpecified = true;
            reoAsset.property_disposition = MapToLoansPQ(currentReo.Stat, currentReo.StatT);
            reoAsset.property_dispositionSpecified = true;
            reoAsset.property_other_costs = currentReo.HExp_rep;
            return reoAsset;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="reoStatus"></param>
        /// <param name="e_ReoStatusT"></param>
        /// <returns></returns>
        private static BASE_ASSET_REAL_ESTATEProperty_disposition MapToLoansPQ(string reoStatus, E_ReoStatusT e_ReoStatusT)
        {
            //if empty just return empty... Not sure why reo status does not have a empty option when we do in the UI.
            if (string.IsNullOrEmpty(reoStatus))
            {
                return BASE_ASSET_REAL_ESTATEProperty_disposition.Item;
            }

            switch (e_ReoStatusT)
            {
                case E_ReoStatusT.Residence:
                    return BASE_ASSET_REAL_ESTATEProperty_disposition.H;
                case E_ReoStatusT.Sale:
                    return BASE_ASSET_REAL_ESTATEProperty_disposition.S;
                case E_ReoStatusT.PendingSale:
                    return BASE_ASSET_REAL_ESTATEProperty_disposition.P;
                case E_ReoStatusT.Rental:
                    return BASE_ASSET_REAL_ESTATEProperty_disposition.R;
                default:
                    throw new ArgumentException("Unhandled reo status type.");
            }
        }


        /// <summary>
        /// Doesnt throw exception because its free text so default is to empty.;
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static BASE_ASSET_REAL_ESTATEProperty_type ReoTypeToLPQ(string type)
        {
            switch (type.ToLower())
            {
                case "2-4plx":
                    return BASE_ASSET_REAL_ESTATEProperty_type.Item2UNIT;
                case "com-nr":
                    return BASE_ASSET_REAL_ESTATEProperty_type.COMMERCIAL;
                case "com-r":
                    return BASE_ASSET_REAL_ESTATEProperty_type.HOMEBUSINESS;
                case "condo":
                    return BASE_ASSET_REAL_ESTATEProperty_type.CONDOTEL;
                case "coop":
                    return BASE_ASSET_REAL_ESTATEProperty_type.COOP;
                case "farm":
                    return BASE_ASSET_REAL_ESTATEProperty_type.FARM;
                case "land":
                    return BASE_ASSET_REAL_ESTATEProperty_type.LAND;
                case "mixed":
                    return BASE_ASSET_REAL_ESTATEProperty_type.MIXEDUSE;
                case "mobil":
                    return BASE_ASSET_REAL_ESTATEProperty_type.MOBILEHOME;
                case "multi":
                    return BASE_ASSET_REAL_ESTATEProperty_type.Item5UNIT;
                case "sfr":
                    return BASE_ASSET_REAL_ESTATEProperty_type.SFR;
                case "town":
                    return BASE_ASSET_REAL_ESTATEProperty_type.TOWNHOUSE;
                case "other":
                    return BASE_ASSET_REAL_ESTATEProperty_type.OTHER;
                default:
                    return BASE_ASSET_REAL_ESTATEProperty_type.Item;
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="e_aBDecPastOwnedPropT"></param>
        /// <returns></returns>
        private static MORTGAGE_DECLARATIONType_of_property_owned MapToLoansPQ(E_aBDecPastOwnedPropT e_aBDecPastOwnedPropT)
        {
            switch (e_aBDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.Empty:
                    return MORTGAGE_DECLARATIONType_of_property_owned.Item;
                case E_aBDecPastOwnedPropT.PR:
                    return MORTGAGE_DECLARATIONType_of_property_owned.PR;
                case E_aBDecPastOwnedPropT.SH:
                    return MORTGAGE_DECLARATIONType_of_property_owned.SH;
                case E_aBDecPastOwnedPropT.IP:
                    return MORTGAGE_DECLARATIONType_of_property_owned.IP;
                default:
                    throw new ArgumentException("Unhandled Past Owned Property Type " + e_aBDecPastOwnedPropT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="e_aBDecPastOwnedPropTitleT"></param>
        /// <returns></returns>
        private MORTGAGE_DECLARATIONTitle_hold_type MapToLoansPQ(E_aBDecPastOwnedPropTitleT e_aBDecPastOwnedPropTitleT)
        {
            switch (e_aBDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.S:
                    return MORTGAGE_DECLARATIONTitle_hold_type.S;
                case E_aBDecPastOwnedPropTitleT.SP:
                    return MORTGAGE_DECLARATIONTitle_hold_type.SP;
                case E_aBDecPastOwnedPropTitleT.O:
                    return MORTGAGE_DECLARATIONTitle_hold_type.O;
                case E_aBDecPastOwnedPropTitleT.Empty:
                    return MORTGAGE_DECLARATIONTitle_hold_type.Item;
                default:
                    throw new ArgumentException("Unhandled Dec Prop Title T " + e_aBDecPastOwnedPropTitleT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private BASE_APPLICANTDeclined_answer_race_gender ToYN(bool x)
        {
            return x ? BASE_APPLICANTDeclined_answer_race_gender.Y : BASE_APPLICANTDeclined_answer_race_gender.N;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private BASE_APPLICANTDeclined_answer_race_gender LoDeclerationAnswerToLPQ(string p)
        {
            p = p ?? "";
            switch (p.TrimWhitespaceAndBOM().ToUpper())
            {
                case "Y":
                    return BASE_APPLICANTDeclined_answer_race_gender.Y;
                case "N":
                    return BASE_APPLICANTDeclined_answer_race_gender.N;
                case "":
                    return BASE_APPLICANTDeclined_answer_race_gender.Item;
                default:
                    throw new ArgumentException("Unhandled Y N in Declarations " + p);
            }
        }

        #region assets 
        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="cAssetRegular"></param>
        /// <param name="appData"></param>
        /// <returns></returns>
        private BASE_ASSET CreateAsset(IAssetRegular cAssetRegular, CAppData appData)
        {
            if (cAssetRegular == null)
                return null;

            BASE_ASSET asset;

            if (cAssetRegular.AssetT == E_AssetRegularT.Auto)
            {
                asset = new BASE_ASSET_VEHICLE();
            }

            else
            {
                asset = new BASE_ASSET_LISTASSET();
            }

            asset.account_number = cAssetRegular.AccNum.Value;
            asset.asset_type = ToLPQ(cAssetRegular.AssetT);
            asset.asset_value = cAssetRegular.Val_rep;
            asset.bank_name = cAssetRegular.ComNm;
            asset.property_address = cAssetRegular.StAddr;
            asset.property_city = cAssetRegular.City;
            asset.property_state = cAssetRegular.State;
            asset.property_zip = cAssetRegular.Zip;
            asset.asset_valueSpecified = true;
            asset.description = cAssetRegular.Desc;
            if (asset.description == asset.bank_name)
            {
                asset.description = "";
            }


            return asset;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="e_AssetRegularT"></param>
        /// <returns></returns>
        private static BASE_ASSETAsset_type ToLPQ(E_AssetRegularT e_AssetRegularT)
        {
            switch (e_AssetRegularT)
            {
                case E_AssetRegularT.Auto:
                    return BASE_ASSETAsset_type.AUTOMOBILE;
                case E_AssetRegularT.Bonds:
                    return BASE_ASSETAsset_type.BOND;
                case E_AssetRegularT.BridgeLoanNotDeposited:
                    return BASE_ASSETAsset_type.BRIDGE_LOAN_NOT_DEPOSITED;
                case E_AssetRegularT.CertificateOfDeposit:
                    return BASE_ASSETAsset_type.CERTIFICATE;
                case E_AssetRegularT.Checking:
                    return BASE_ASSETAsset_type.CHECKING_ACCOUNT;
                case E_AssetRegularT.EmployerAssistance:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.GiftEquity:
                    return BASE_ASSETAsset_type.GIFT_OF_EQUITY;
                case E_AssetRegularT.GiftFunds:
                    return BASE_ASSETAsset_type.GIFT;
                case E_AssetRegularT.Grant:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.IndividualDevelopmentAccount:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.LeasePurchaseCredit:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.MoneyMarketFund:
                    return BASE_ASSETAsset_type.MONEY_MARKET;
                case E_AssetRegularT.MutualFunds:
                    return BASE_ASSETAsset_type.MUTUAL_FUNDS;
                case E_AssetRegularT.OtherIlliquidAsset:
                    return BASE_ASSETAsset_type.OTHER_NON_LIQUID;
                case E_AssetRegularT.OtherLiquidAsset:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.OtherPurchaseCredit:
                    return BASE_ASSETAsset_type.OTHER_NON_LIQUID;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets:
                    return BASE_ASSETAsset_type.NET_SALE_PROCEEDS;
                case E_AssetRegularT.ProceedsFromSaleOfNonRealEstateAsset:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.ProceedsFromUnsecuredLoan:
                    return BASE_ASSETAsset_type.OTHER;
                case E_AssetRegularT.Savings:
                    return BASE_ASSETAsset_type.SAVINGS_ACCOUNT;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit:
                    return BASE_ASSETAsset_type.SECURED_BORROWER_FUNDS;
                case E_AssetRegularT.StockOptions:
                    return BASE_ASSETAsset_type.OTHER_NON_LIQUID;
                case E_AssetRegularT.Stocks:
                    return BASE_ASSETAsset_type.STOCK;
                case E_AssetRegularT.SweatEquity:
                    return BASE_ASSETAsset_type.OTHER_NON_LIQUID;
                case E_AssetRegularT.TradeEquityFromPropertySwap:
                    return BASE_ASSETAsset_type.OTHER_NON_LIQUID;
                case E_AssetRegularT.TrustFunds:
                    return BASE_ASSETAsset_type.TRUST_FUNDS;
                default:
                    throw new ArgumentException("Unhandled Regular Asset Type " + e_AssetRegularT);
            }
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="cAssetCashDeposit"></param>
        /// <returns></returns>
        private BASE_ASSET_LISTASSET CreateAsset(IAssetCashDeposit cAssetCashDeposit)
        {
            if (null == cAssetCashDeposit)
                return null;

            BASE_ASSET_LISTASSET asset = new BASE_ASSET_LISTASSET()
            {
                asset_type = BASE_ASSETAsset_type.CASH_DEPOSIT,
                asset_value = cAssetCashDeposit.Val_rep,
                asset_valueSpecified = true,
                bank_name = cAssetCashDeposit.Desc
                // CashOrMarketValueAmount = lifeIns.Val_rep,
                //LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep  It looks like they s
            };

            return asset;
        }

        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="cAssetRetirement"></param>
        /// <returns></returns>
        private BASE_ASSET_LISTASSET CreateAsset(IAssetRetirement cAssetRetirement)
        {
            if (cAssetRetirement == null)
                return null;

            return new BASE_ASSET_LISTASSET()
            {
                asset_type = BASE_ASSETAsset_type.RETIREMENT_FUND,
                asset_value = cAssetRetirement.Val_rep,
                asset_valueSpecified = true
            };

        }

        //XX what about face val? 
        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="cAssetLifeIns"></param>
        /// <returns></returns>
        private BASE_ASSET_LISTASSET CreateAsset(IAssetLifeInsurance cAssetLifeIns)
        {
            if (cAssetLifeIns == null)
                return null;

            BASE_ASSET_LISTASSET asset = new BASE_ASSET_LISTASSET()
            {
                asset_type = BASE_ASSETAsset_type.LIFE_INSURANCE,
                asset_value = cAssetLifeIns.Val_rep,
                asset_valueSpecified = true
                // CashOrMarketValueAmount = lifeIns.Val_rep,
                //LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep
            };

            return asset;
        }


        /// <summary>
        /// Export From Scratch -- Checked
        /// </summary>
        /// <param name="cAssetBusiness"></param>
        /// <returns></returns>
        private BASE_ASSET_LISTASSET CreateAsset(IAssetBusiness cAssetBusiness)
        {
            if (cAssetBusiness == null)
                return null;
            BASE_ASSET_LISTASSET asset = new BASE_ASSET_LISTASSET()
            {
                asset_type = BASE_ASSETAsset_type.OWNED_BUSINESS,
                asset_value = cAssetBusiness.Val_rep,
                asset_valueSpecified = true

            };
            return asset;
        }

        #endregion


        #endregion
    }
}