﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOfficeApp.ObjLib.Conversions.LoansPQ
{
    public sealed partial class LPQTools
    {
        public static bool ConvertToIntString(string value, out string result, out int intResult)
        {
            double val;
            if (double.TryParse(value, out val))
            {
                try
                {
                    intResult = System.Convert.ToInt32(val);
                    result = intResult.ToString();
                    return true;
                }
                catch { }
            }
            result = "0";
            intResult = -1;
            return false;
        }
    }
}
