﻿namespace LendersOffice.ObjLib.Conversions.LoansPQ
{
    using System.Xml.Serialization;

    /// <summary>
    /// The container for a current/existing mortgage..
    /// </summary>
    public partial class MORTGAGE_CURRENT_TD
    {
        /// <summary>
        /// Specifies whether or not to include the "lender" value in the XML.
        /// </summary>
        [XmlIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Special property labeling an auto-generated property for serialization.")]
        public bool lenderSpecified => !string.IsNullOrEmpty(this.lender);
    }
}
