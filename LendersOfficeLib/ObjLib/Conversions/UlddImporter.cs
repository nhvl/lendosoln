﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml;
using System.IO;
using ULDD;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using LendingQB.Core.Data;

namespace LendersOffice.Conversions
{
    public class UlddImporter
    {
        public void ImportIntoExisting(Guid sLId, string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new CBaseException("Invalid ULDD Format", "Invalid ULDD Format");
            }
            Message message = new Message();

            using (TextReader stream = new StringReader(xml))
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    message.ReadXml(reader);
                }
            }

            string sLNm = string.Empty;
            Parse(message.DealSets, sLId, out sLNm);

        }
        public string ImportAsNewLoan(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new CBaseException("Invalid ULDD Format", "Invalid ULDD Format");
            }
            Message message = new Message();

            using (TextReader stream = new StringReader(xml)) 
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    message.ReadXml(reader);
                }
            }

            string sLNm = string.Empty;
            Parse(message.DealSets, Guid.Empty, out sLNm);
            return sLNm;
        }

        private void Parse(DealSets dealSets, Guid sLId, out string sLNm)
        {
            sLNm = string.Empty;
            if (dealSets == null)
            {
                return;
            }
            
            // Only support one DealSet
            if (dealSets.DealSetList.Count > 0)
            {
                DealSet dealSet = dealSets.DealSetList[0];
                Parse(dealSet, sLId, out sLNm);
            }
        }

        private void Parse(DealSet dealSet, Guid sLId, out string sLNm)
        {
            sLNm = string.Empty;
            if (dealSet == null)
            {
                return;
            }

            foreach (Deal deal in dealSet.Deals.DealList)
            {
                Parse(deal, sLId, out sLNm);
            }
        }

        private void Parse(Deal deal, Guid sLId, out string sLNm)
        {
            sLNm = string.Empty;
            if (deal == null)
            {
                return;
            }
            CPageData dataLoan = null;

            if (sLId == Guid.Empty)
            {
                dataLoan = CreateNewLoan();
            }
            else
            {
                dataLoan = LoadLoan(sLId);
            }

            if (dataLoan.sIsIncomeCollectionEnabled)
            {
                // We export the (co)borrower's total income as a single item,
                // so on import we will clear all existing income sources and
                // use the values provided.
                dataLoan.ClearIncomeSources();
            }

            #region Collateral
            if (deal.Collaterals.CollateralList.Count > 0)
            {
                Parse(deal.Collaterals.CollateralList[0], dataLoan);
            }
            #endregion

            #region Loans
            Parse(deal.Loans, dataLoan);
            #endregion
            #region Parties
            foreach (Party party in deal.Parties.PartyList)
            {
                if (party.Roles.RoleList.Count == 0)
                {
                    continue;
                }
                PartyRoleType partyRoleType = party.Roles.RoleList[0].RoleDetail.PartyRoleType;

                if (partyRoleType == PartyRoleType.Servicer)
                {
                    dataLoan.sGseDeliveryServicerId = GetPartyRoleIdentifier(party);
                    dataLoan.sGseDeliveryServicerIdLckd = true;
                }
                else if (partyRoleType == PartyRoleType.Payee)
                {
                    dataLoan.sGseDeliveryPayeeId = GetPartyRoleIdentifier(party);
                    dataLoan.sGseDeliveryPayeeIdLckd = true;
                }
                else if (partyRoleType == PartyRoleType.DocumentCustodian)
                {
                    dataLoan.sGseDeliveryDocumentCustodianId = GetPartyRoleIdentifier(party);
                }
                else if (partyRoleType == PartyRoleType.LoanSeller)
                {
                    dataLoan.sLenNum = GetPartyRoleIdentifier(party);
                }
                else if (partyRoleType == PartyRoleType.LoanOriginationCompany)
                {
                    IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                    if (preparer.IsLocked) 
                    {
                        preparer.CompanyLoanOriginatorIdentifier = GetPartyRoleIdentifier(party);
                        preparer.Update();
                    } 
                    else 
                    {
                        CAgentFields agent = dataLoan.GetAgentOfRole(preparer.AgentRoleT, E_ReturnOptionIfNotExist.CreateNew);
                        agent.CompanyLoanOriginatorIdentifier = GetPartyRoleIdentifier(party);
                        agent.Update();
                    }

                }
                else if (partyRoleType == PartyRoleType.Borrower)
                {
                    ParseBorrower(party, dataLoan);
                }
                else if (partyRoleType == PartyRoleType.Appraiser)
                {
                    #region Appraiser
                    CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.CreateNew);
                    agent.AgentRoleT = E_AgentRoleT.Appraiser;
                    agent.LicenseNumOfAgent = party.Roles.RoleList[0].Appraiser.AppraiserLicense.AppraiserLicenseIdentifier;
                    #endregion
                }
                else if (partyRoleType == PartyRoleType.AppraiserSupervisor)
                {
                    #region Appraiser Supervisor
                    List<CAgentFields> agentList = dataLoan.GetAgentsOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.CreateNew);

                    bool isFound = false;

                    foreach (CAgentFields agent in agentList)
                    {
                        if (agent.AgentRoleT == E_AgentRoleT.Other && agent.OtherAgentRoleTDesc == "AppraiserSupervisor")
                        {
                            isFound = true;
                            agent.LicenseNumOfAgent = party.Roles.RoleList[0].AppraiserSupervisor.AppraiserLicense.AppraiserLicenseIdentifier;
                            agent.Update();
                        }
                    }
                    if (isFound == false)
                    {
                        CAgentFields agent = dataLoan.GetAgentFields(-1);
                        agent.AgentRoleT = E_AgentRoleT.Other;
                        agent.OtherAgentRoleTDesc = "AppraiserSupervisor";
                        agent.LicenseNumOfAgent = party.Roles.RoleList[0].AppraiserSupervisor.AppraiserLicense.AppraiserLicenseIdentifier;
                        agent.Update();

                    }
                    #endregion
                }
                else if (partyRoleType == PartyRoleType.LoanOriginator)
                {
                    #region Loan Originator
                    IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                    if (preparer.IsLocked)
                    {
                        preparer.CompanyLoanOriginatorIdentifier = GetPartyRoleIdentifier(party);
                        preparer.Update();
                    }
                    else
                    {
                        CAgentFields agent = dataLoan.GetAgentOfRole(preparer.AgentRoleT, E_ReturnOptionIfNotExist.CreateNew);
                        agent.LoanOriginatorIdentifier = GetPartyRoleIdentifier(party);
                        agent.Update();
                    }
                    if (party.Roles.RoleList[0].LoanOriginator.LoanOriginatorType != LoanOriginatorType.Undefined)
                    {
                        // 2/24/2014 - dd Per OPM 150240 we are no longer set sBranchChannelT directly
                        switch (party.Roles.RoleList[0].LoanOriginator.LoanOriginatorType)
                        {
                            case LoanOriginatorType.Broker:
                                dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
                                dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;
                                break;
                            case LoanOriginatorType.Correspondent:
                                dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                                dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;
                                break;
                            case LoanOriginatorType.Lender:
                                dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Blank;
                                dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
                                break;
                            case LoanOriginatorType.Other:
                            case LoanOriginatorType.Undefined:
                                dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Blank;
                                dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Blank;
                                break;
                            default:
                                throw new UnhandledEnumException(party.Roles.RoleList[0].LoanOriginator.LoanOriginatorType);
                        }

                    }
                    #endregion
                }
                else if (partyRoleType == PartyRoleType.NotePayTo)
                {
                    #region Note Pay To
                    string name = party.LegalEntity.LegalEntityDetail.FullName;

                    // TODO: How to save this?

                    #endregion
                }
            }
            #endregion
            dataLoan.Save();
            sLNm = dataLoan.sLNm;
        }
        private void Parse(Loans loans, CPageData dataLoan)
        {
            // 9/21/2012 dd - Skip CombinedLtv because this is only contains calculate value.

            // 9/21/2012 dd - DOES NOT SUPPORT LINKED LOAN FOR NOW.

            Loan currentLoanClosing = null;
            Loan currentLoan = null;

            foreach (Loan loan in loans.LoanList)
            {
                if (loan.LoanRoleType == LoanRoleType.SubjectLoan)
                {
                    if (loan.LoanState.LoanStateType == LoanStateType.AtClosing)
                    {
                        currentLoanClosing = loan;
                    }
                    else if (loan.LoanState.LoanStateType == LoanStateType.Current)
                    {
                        currentLoan = loan;
                    }
                }
            }
            ParseLoanAtClosing(currentLoanClosing, dataLoan); // 9/21/2012 dd - This method must call first before ParseLoanCurrent
            ParseLoanCurrent(currentLoan, dataLoan);



        }

        private void ParseLoanAtClosing(Loan loan, CPageData dataLoan)
        {
            if (loan == null)
            {
                return;
            }
            Parse(loan.Amortization.AmortizationRule, dataLoan);

            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                 // Parse(loan.Adjustment, dataLoan); // TODO
            }

            Parse(loan.HmdaLoan, dataLoan);

            Parse(loan.LoanDetail, dataLoan);

            Parse(loan.Maturity, dataLoan);

            Parse(loan.Payment, dataLoan);

            Parse(loan.SelectedLoanProduct, dataLoan);

            Parse(loan.TermsOfMortgage, dataLoan);

            // Parse(loan.Underwriting, dataLoan); // TODO
        }
        private void Parse(Underwriting underwriting, CPageData dataLoan)
        {
            
        }
        private void Parse(TermsOfMortgage termsOfMortgage, CPageData dataLoan)
        {

            if (termsOfMortgage.LienPriorityType != LienPriorityType.Undefined)
            {
                dataLoan.sLienPosT = Parse(termsOfMortgage.LienPriorityType);
            }

            if (termsOfMortgage.LoanPurposeType != LoanPurposeType.Undefined)
            {
                dataLoan.sLPurposeT = Parse(termsOfMortgage.LoanPurposeType);
            }
            if (termsOfMortgage.MortgageType != MortgageType.Undefined)
            {
                dataLoan.sLT = Parse(termsOfMortgage.MortgageType);
            }

            dataLoan.sDocumentNoteD_rep = termsOfMortgage.NoteDate;
            dataLoan.sNoteIR_rep = termsOfMortgage.NoteRatePercent;
        }
        private E_sLT Parse(MortgageType type)
        {
            switch (type)
            {
                case MortgageType.Conventional:
                    return E_sLT.Conventional;
                case MortgageType.FHA:
                    return E_sLT.FHA;
                case MortgageType.Other:
                    return E_sLT.Other;
                case MortgageType.USDARuralHousing:
                    return E_sLT.UsdaRural;
                case MortgageType.VA:
                    return E_sLT.VA;
                case MortgageType.HELOC:
                case MortgageType.LocalAgency:
                case MortgageType.StateAgency:
                case MortgageType.Undefined:
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sLPurposeT Parse(LoanPurposeType type)
        {
            switch (type)
            {
                case LoanPurposeType.Other:
                    return E_sLPurposeT.Other;
                case LoanPurposeType.Purchase:
                    return E_sLPurposeT.Purchase;
                case LoanPurposeType.Refinance:
                    return E_sLPurposeT.Refin;
                case LoanPurposeType.MortgageModification:
                case LoanPurposeType.Undefined:
                case LoanPurposeType.Unknown:
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sLienPosT Parse(LienPriorityType type)
        {
            switch (type)
            {
                case LienPriorityType.FirstLien:
                    return E_sLienPosT.First;
                case LienPriorityType.SecondLien:
                    return E_sLienPosT.Second;
                case LienPriorityType.FourthLien:
                case LienPriorityType.Other:
                case LienPriorityType.ThirdLien:
                case LienPriorityType.Undefined:
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private void Parse(LoanProduct loanProduct, CPageData dataLoan)
        {
            if (loanProduct.PriceLocks.PriceLockList.Count > 0)
            {
                dataLoan.sRLckdD_rep = loanProduct.PriceLocks.PriceLockList[0].PriceLockDatetime;
            }
        }
        private void Parse(Payment payment, CPageData dataLoan)
        {
        }
        private void Parse(Maturity maturity, CPageData dataLoan)
        {
            dataLoan.sDue_rep = maturity.MaturityRule.LoanMaturityPeriodCount;
        }
        private void Parse(LoanDetail loanDetail, CPageData dataLoan)
        {
            dataLoan.sAppSubmittedD_rep = loanDetail.ApplicationReceivedDate;

            if (loanDetail.EscrowIndicator.HasValue)
            {
                dataLoan.sWillEscrowBeWaived = loanDetail.EscrowIndicator.Value == false;
            }
            if (loanDetail.PrepaymentPenaltyIndicator.HasValue)
            {
                if (loanDetail.PrepaymentPenaltyIndicator.Value == true)
                {
                    dataLoan.sPrepmtPenaltyT = E_sPrepmtPenaltyT.May;
                }
                else
                {
                    dataLoan.sPrepmtPenaltyT = E_sPrepmtPenaltyT.WillNot;
                }
            }
        }
        private void Parse(HmdaLoan hmdaLoan, CPageData dataLoan)
        {
            if (hmdaLoan.HMDA_HOEPALoanStatusIndicator.HasValue)
            {
                dataLoan.sHmdaReportAsHoepaLoan = hmdaLoan.HMDA_HOEPALoanStatusIndicator.Value;
            }
            dataLoan.sHmdaAprRateSpread = hmdaLoan.HMDARateSpreadPercent;
        }
        private void Parse(Adjustment adjustment, CPageData dataLoan)
        {
            Parse(adjustment.InterestRateAdjustment, dataLoan);
        }
        private void Parse(InterestRateAdjustment interestRateAdjustment, CPageData dataLoan)
        {

        }
        private void Parse(AmortizationRule amortizationRule, CPageData dataLoan)
        {
            dataLoan.sTerm_rep = amortizationRule.LoanAmortizationPeriodCount;
            dataLoan.sFinMethT = Parse(amortizationRule.LoanAmortizationType);
        }
        private E_sFinMethT Parse(LoanAmortizationType type)
        {
            switch (type)
            {
                case LoanAmortizationType.AdjustableRate:
                    return E_sFinMethT.ARM;
                case LoanAmortizationType.Fixed:
                    return E_sFinMethT.Fixed;
                case LoanAmortizationType.GraduatedPaymentARM:
                case LoanAmortizationType.GraduatedPaymentMortgage:
                case LoanAmortizationType.GrowingEquityMortgage:
                case LoanAmortizationType.OtherAmortizationType:
                case LoanAmortizationType.RateImprovementMortgage:
                case LoanAmortizationType.ReverseMortgage:
                case LoanAmortizationType.Step:
                    return E_sFinMethT.Graduated;
                case LoanAmortizationType.Undefined:
                    return E_sFinMethT.Fixed;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private void ParseLoanCurrent(Loan loan, CPageData dataLoan)
        {
            if (loan == null)
            {
                return;
            }
            // loan.InvestorFeatures;
            // loan.InvestorLoanInformation;

            if (loan.LoanComments.LoanCommentList.Count > 0)
            {
                dataLoan.sGseDeliveryComments = loan.LoanComments.LoanCommentList[0].LoanCommentText;
            }

            //loan.LoanDetail;
            ParseLoanIdentifiersCurrent(loan.LoanIdentifiers, dataLoan);
            Parse(loan.MiData.MiDataDetail, dataLoan);
        }

        private void Parse(MiDataDetail miDataDetail, CPageData dataLoan)
        {
            dataLoan.sMiLenderPaidAdj_rep = miDataDetail.LenderPaidMIInterestRateAdjustmentPercent;
            dataLoan.sMiCertId = miDataDetail.MICertificateIdentifier;
            dataLoan.sMiLenderPaidCoverage_rep = miDataDetail.MICoveragePercent;
        }
        private void ParseLoanIdentifiersCurrent(LoanIdentifiers loanIdentifiers, CPageData dataLoan)
        {
            foreach (LoanIdentifier loanIdentifier in loanIdentifiers.LoanIdentifierList)
            {
                if (string.IsNullOrEmpty(loanIdentifier.InvestorCommitmentIdentifier) == false)
                {
                    dataLoan.sGseDeliveryCommitmentId = loanIdentifier.InvestorCommitmentIdentifier;
                }
                else if (string.IsNullOrEmpty(loanIdentifier.InvestorContractIdentifier) == false)
                {
                    dataLoan.sGseDeliveryContractId = loanIdentifier.InvestorContractIdentifier;
                }
                else if (string.IsNullOrEmpty(loanIdentifier.MERS_MINIdentifier) == false)
                {
                    dataLoan.sMersMin = loanIdentifier.MERS_MINIdentifier;
                }
                else if (string.IsNullOrEmpty(loanIdentifier.SellerLoanIdentifier) == false)
                {
                    dataLoan.sLenLNum = loanIdentifier.SellerLoanIdentifier;
                }
            }
        }
        private void Parse(Collateral collateral, CPageData dataLoan)
        {
            if (collateral.Properties.PropertyList.Count > 0)
            {
                Parse(collateral.Properties.PropertyList[0], dataLoan);
            }
        }
        private void Parse(Property property, CPageData dataLoan)
        {
            #region Parse Address
            Address address = property.Address;
            dataLoan.sSpAddr = address.AddressLineText;
            dataLoan.sSpCity = address.CityName;
            dataLoan.sSpState = address.StateCode;
            dataLoan.sSpZip = address.PostalCode;
            #endregion 

            #region Parse Flood Determination
            if (property.FloodDetermination.FloodDeterminationDetail.SpecialFloodHazardAreaIndicator.HasValue)
            {
                if (property.FloodDetermination.FloodDeterminationDetail.SpecialFloodHazardAreaIndicator.Value)
                {
                    dataLoan.sFloodHazardBuilding = true;
                    dataLoan.sFloodHazardMobileHome = true;
                }
            }
            #endregion

            #region Parse Project
            Parse(property.Project.ProjectDetail, dataLoan);
            #endregion

            #region Parse Property Details
            Parse(property.PropertyDetail, dataLoan);
            #endregion

            #region Parse Property Valuation
            if (property.PropertyValuations.PropertyValuationList.Count > 0)
            {
                Parse(property.PropertyValuations.PropertyValuationList[0], dataLoan);
            }
            #endregion
        }
        private void Parse(PropertyValuation propertyValuation, CPageData dataLoan)
        {
            if (propertyValuation.Avms.AvmList.Count > 0)
            {
                dataLoan.sSpAvmModelT = Parse(propertyValuation.Avms.AvmList[0].AVMModelNameType);
            }
            Parse(propertyValuation.PropertyValuationDetail, dataLoan);
        }
        private void Parse(PropertyValuationDetail propertyValuationDetail, CPageData dataLoan)
        {
            dataLoan.sSpAppraisalId = propertyValuationDetail.AppraisalIdentifier;
            dataLoan.sSpValuationEffectiveD_rep = propertyValuationDetail.PropertyValuationEffectiveDate;
            dataLoan.sSpValuationMethodT = Parse(propertyValuationDetail.PropertyValuationMethodType);
        }

        private E_sSpValuationMethodT Parse(PropertyValuationMethodType type)
        {
            switch (type)
            {
                case PropertyValuationMethodType.AutomatedValuationModel:
                    return E_sSpValuationMethodT.AutomatedValuationModel;
                case PropertyValuationMethodType.DesktopAppraisal:
                    return E_sSpValuationMethodT.DesktopAppraisal;
                case PropertyValuationMethodType.DriveBy:
                    return E_sSpValuationMethodT.DriveBy;
                case PropertyValuationMethodType.FullAppraisal:
                    return E_sSpValuationMethodT.FullAppraisal;
                case PropertyValuationMethodType.None:
                    return E_sSpValuationMethodT.None;
                case PropertyValuationMethodType.PriorAppraisalUsed:
                    return E_sSpValuationMethodT.PriorAppraisalUsed;
                case PropertyValuationMethodType.TaxValuation:
                case PropertyValuationMethodType.Undefined:
                case PropertyValuationMethodType.Other:
                case PropertyValuationMethodType.BrokerPriceOpinion:
                    return E_sSpValuationMethodT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sSpAvmModelT Parse(AVMModelNameType type)
        {
            switch (type)
            {
                case AVMModelNameType.AutomatedPropertyService:
                    return E_sSpAvmModelT.AutomatedPropertyService;
                case AVMModelNameType.Casa:
                    return E_sSpAvmModelT.Casa;
                case AVMModelNameType.FidelityHansen:
                    return E_sSpAvmModelT.FidelityHansen;
                case AVMModelNameType.HomePriceAnalyzer:
                    return E_sSpAvmModelT.HomePriceAnalyzer;
                case AVMModelNameType.HomePriceIndex:
                    return E_sSpAvmModelT.HomePriceIndex;
                case AVMModelNameType.HomeValueExplorer:
                    return E_sSpAvmModelT.HomeValueExplorer;
                case AVMModelNameType.Indicator:
                    return E_sSpAvmModelT.Indicator;
                case AVMModelNameType.NetValue:
                    return E_sSpAvmModelT.NetValue;
                case AVMModelNameType.Other:
                    return E_sSpAvmModelT.LeaveBlank;
                case AVMModelNameType.Pass:
                    return E_sSpAvmModelT.Pass;
                case AVMModelNameType.PropertySurveyAnalysisReport:
                    return E_sSpAvmModelT.PropertySurveyAnalysisReport;
                case AVMModelNameType.Undefined:
                    return E_sSpAvmModelT.LeaveBlank;
                case AVMModelNameType.ValueFinder:
                    return E_sSpAvmModelT.ValueFinder;
                case AVMModelNameType.ValuePoint:
                    return E_sSpAvmModelT.ValuePoint;
                case AVMModelNameType.ValuePoint4:
                    return E_sSpAvmModelT.ValuePoint4;
                case AVMModelNameType.ValuePointPlus:
                    return E_sSpAvmModelT.ValuePointPlus;
                case AVMModelNameType.ValueSure:
                    return E_sSpAvmModelT.ValueSure;
                case AVMModelNameType.ValueWizard:
                    return E_sSpAvmModelT.ValueWizard;
                case AVMModelNameType.ValueWizardPlus:
                    return E_sSpAvmModelT.ValueWizardPlus;
                case AVMModelNameType.VeroIndexPlus:
                    return E_sSpAvmModelT.VeroIndexPlus;
                case AVMModelNameType.VeroValue:
                    return E_sSpAvmModelT.VeroValue;
                default:
                    throw new UnhandledEnumException(type);
            }

        }
        private void Parse(PropertyDetail propertyDetail, CPageData dataLoan)
        {
            if (propertyDetail.AttachmentType != AttachmentType.Undefined)
            {
                dataLoan.sProdSpStructureT = Parse(propertyDetail.AttachmentType);
            }

            if (propertyDetail.ConstructionMethodType == ConstructionMethodType.Modular)
            {
                dataLoan.sGseSpT = E_sGseSpT.Modular;
            }
            else if (propertyDetail.ConstructionMethodType == ConstructionMethodType.Manufactured)
            {
                dataLoan.sGseSpT = E_sGseSpT.ManufacturedHousing;
            }

            dataLoan.sUnitsNum_rep = propertyDetail.FinancedUnitCount;

            if (propertyDetail.PropertyEstateType != PropertyEstateType.Undefined)
            {
                dataLoan.sEstateHeldT = Parse(propertyDetail.PropertyEstateType);
            }

            dataLoan.sYrBuilt = propertyDetail.PropertyStructureBuiltYear;
            if (propertyDetail.PropertyUsageType != PropertyUsageType.Undefined)
            {
                dataLoan.GetAppData(0).aOccT = Parse(propertyDetail.PropertyUsageType);
            }
        }
        private E_aOccT Parse(PropertyUsageType type)
        {
            switch (type)
            {
                case PropertyUsageType.Investment:
                    return E_aOccT.Investment;
                case PropertyUsageType.PrimaryResidence:
                    return E_aOccT.PrimaryResidence;
                case PropertyUsageType.SecondHome:
                    return E_aOccT.SecondaryResidence;
                case PropertyUsageType.Other:
                case PropertyUsageType.Undefined:
                    return E_aOccT.PrimaryResidence;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sEstateHeldT Parse(PropertyEstateType type)
        {
            switch (type)
            {
                case PropertyEstateType.FeeSimple:
                    return E_sEstateHeldT.FeeSimple;
                case PropertyEstateType.Leasehold:
                    return E_sEstateHeldT.LeaseHold;
                case PropertyEstateType.Fractional:
                case PropertyEstateType.Other:
                case PropertyEstateType.Undefined:
                    return E_sEstateHeldT.FeeSimple; // We do not have these values.
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sProdSpStructureT Parse(AttachmentType type)
        {
            switch (type)
            {
                case AttachmentType.Attached:
                    return E_sProdSpStructureT.Attached;
                case AttachmentType.Detached:
                    return E_sProdSpStructureT.Detached;
                case AttachmentType.SemiDetached:
                    return E_sProdSpStructureT.Detached;
                case AttachmentType.Undefined:
                    return E_sProdSpStructureT.Detached;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private void Parse(ProjectDetail projectDetail, CPageData dataLoan)
        {
            dataLoan.sSpProjectStatusT = Parse(projectDetail.CondominiumProjectStatusType);
            dataLoan.sCpmProjectId = projectDetail.FNMCondominiumProjectManagerProjectIdentifier;

            dataLoan.sSpProjectAttachmentT = Parse(projectDetail.ProjectAttachmentType);

            //projectDetail.ProjectClassificationIdentifier // TODO

            dataLoan.sSpProjectDesignT = Parse(projectDetail.ProjectDesignType);
            dataLoan.sSpProjectDwellingUnitCount_rep = projectDetail.ProjectDwellingUnitCount;
            dataLoan.sSpProjectDwellingUnitSoldCount_rep = projectDetail.ProjectDwellingUnitsSoldCount;
            if (projectDetail.ProjectLegalStructureType != ProjectLegalStructureType.Undefined)
            {
                dataLoan.sGseSpT = Parse(projectDetail.ProjectLegalStructureType);
            }

            dataLoan.sProjNm = projectDetail.ProjectName;
            if (projectDetail.PUDIndicator.HasValue)
            {
                if (projectDetail.PUDIndicator.Value)
                {
                    dataLoan.sGseSpT = E_sGseSpT.PUD;
                }
            }
        }
        private E_sGseSpT Parse(ProjectLegalStructureType type)
        {
            switch (type)
            {
                case ProjectLegalStructureType.Undefined:
                case ProjectLegalStructureType.CommonInterestApartment:
                    return E_sGseSpT.LeaveBlank;
                case ProjectLegalStructureType.Condominium:
                    return E_sGseSpT.Condominium;
                case ProjectLegalStructureType.Cooperative:
                    return E_sGseSpT.Cooperative;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sSpProjectDesignT Parse(ProjectDesignType type)
        {
            switch (type)
            {
                case ProjectDesignType.GardenProject:
                    return E_sSpProjectDesignT.LowRise;
                case ProjectDesignType.HighriseProject:
                    return E_sSpProjectDesignT.HighRise;
                case ProjectDesignType.MidriseProject:
                    return E_sSpProjectDesignT.MidRise;
                case ProjectDesignType.TownhouseRowhouse:
                    return E_sSpProjectDesignT.Townhouse;
                case ProjectDesignType.Undefined:
                case ProjectDesignType.Other:
                    return E_sSpProjectDesignT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sSpProjectAttachmentT Parse(ProjectAttachmentType type)
        {
            switch (type)
            {
                case ProjectAttachmentType.Attached:
                    return E_sSpProjectAttachmentT.Attached;
                case ProjectAttachmentType.Detached:
                    return E_sSpProjectAttachmentT.Detached;
                case ProjectAttachmentType.Undefined:
                    return E_sSpProjectAttachmentT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_sSpProjectStatusT Parse(CondominiumProjectStatusType type)
        {
            switch (type)
            {
                case CondominiumProjectStatusType.Established:
                    return E_sSpProjectStatusT.Existing;
                case CondominiumProjectStatusType.New:
                    return E_sSpProjectStatusT.New;
                case CondominiumProjectStatusType.Undefined:
                    return E_sSpProjectStatusT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_BranchChannelT Parse(LoanOriginatorType originatorType)
        {
            switch (originatorType)
            {
                case LoanOriginatorType.Broker:
                    return E_BranchChannelT.Broker;
                case LoanOriginatorType.Correspondent:
                    return E_BranchChannelT.Correspondent;
                case LoanOriginatorType.Lender:
                    return E_BranchChannelT.Wholesale;
                case LoanOriginatorType.Other:
                case LoanOriginatorType.Undefined:
                    return E_BranchChannelT.Blank;
                default:
                    throw new UnhandledEnumException(originatorType);
            }
        }
        private void ParseBorrower(Party party, CPageData dataLoan)
        {
            CAppData dataApp = dataLoan.GetAppData(0); // Always use primary app.

            Borrower borrower = party.Roles.RoleList[0].Borrower;
            Individual individual = party.Individual;
            List<Address> addressList = party.Addresses.AddressList;
            TaxpayerIdentifier taxpayerIdentifier = null;
            if (party.TaxpayerIdentifiers.TaxpayerIdentifierList.Count > 0)
            {
                taxpayerIdentifier = party.TaxpayerIdentifiers.TaxpayerIdentifierList[0];
            }
            BorrowerDetail borrowerDetail = borrower.BorrowerDetail;
            if (borrowerDetail.BorrowerClassificationType == BorrowerClassificationType.Primary)
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            }
            else
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }
            Parse(borrowerDetail, dataLoan, dataApp);
            Parse(individual, dataApp);
            Parse(taxpayerIdentifier, dataApp);
            if (borrower.CreditScores.CreditScoreList.Count > 0)
            {
                Parse(borrower.CreditScores.CreditScoreList[0], dataApp); 
            }
            Parse(borrower.Declaration.DeclarationDetail, dataApp); 
            Parse(borrower.GovernmentMonitoring, dataApp); // TODO

            foreach (Address address in addressList)
            {
                Parse(address, dataApp);
            }


        }
        private void Parse(GovernmentMonitoring governmentMonitoring, CAppData dataApp)
        {
            Parse(governmentMonitoring.HmdaRaces, dataApp);
            Parse(governmentMonitoring.GovernmentMonitoringDetail, dataApp);
        }
        private void Parse(HmdaRaces hmdaRaces, CAppData dataApp)
        {
            foreach (HmdaRace hmdaRace in hmdaRaces.HmdaRaceList)
            {
                switch (hmdaRace.HMDARaceType)
                {
                    case HMDARaceType.AmericanIndianOrAlaskaNative:
                        dataApp.aIsAmericanIndian = true;
                        break;
                    case HMDARaceType.Asian:
                        dataApp.aIsAsian = true;
                        break;
                    case HMDARaceType.BlackOrAfricanAmerican:
                        dataApp.aIsBlack = true;
                        break;
                    case HMDARaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication:
                        dataApp.aNoFurnish = true;
                        break;
                    case HMDARaceType.NativeHawaiianOrOtherPacificIslander:
                        dataApp.aIsPacificIslander = true;
                        break;
                    case HMDARaceType.NotApplicable:
                    case HMDARaceType.Undefined:
                        break;
                    case HMDARaceType.White:
                        dataApp.aIsWhite = true;
                        break;
                    default:
                        throw new UnhandledEnumException(hmdaRace.HMDARaceType);
                }
            }
        }
        private void Parse(GovernmentMonitoringDetail governmentMonitoringDetail, CAppData dataApp)
        {
            if (governmentMonitoringDetail.GenderType == GenderType.Female)
            {
                dataApp.aGender = E_GenderT.Female;
            }
            else if (governmentMonitoringDetail.GenderType == GenderType.Male)
            {
                dataApp.aGender = E_GenderT.Male;
            }
            else if (governmentMonitoringDetail.GenderType == GenderType.InformationNotProvidedUnknown)
            {
                dataApp.aGender = E_GenderT.Unfurnished;
            }
            else if (governmentMonitoringDetail.GenderType == GenderType.NotApplicable)
            {
                dataApp.aGender = E_GenderT.NA;
            }

            switch (governmentMonitoringDetail.HMDAEthnicityType)
            {
                case HMDAEthnicityType.HispanicOrLatino:
                    dataApp.aHispanicT = E_aHispanicT.Hispanic;
                    break;
                case HMDAEthnicityType.NotHispanicOrLatino:
                    dataApp.aHispanicT = E_aHispanicT.NotHispanic;
                    break;
                case HMDAEthnicityType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication:
                case HMDAEthnicityType.NotApplicable:
                case HMDAEthnicityType.Undefined:
                    dataApp.aHispanicT = E_aHispanicT.LeaveBlank;
                    break;
                default:
                    throw new UnhandledEnumException(governmentMonitoringDetail.HMDAEthnicityType);
            }
        }
        private void Parse(DeclarationDetail declarationDetail, CAppData dataApp)
        {
            if (declarationDetail.BorrowerFirstTimeHomebuyerIndicator.HasValue)
            {
                
                dataApp.aDecPastOwnership = declarationDetail.BorrowerFirstTimeHomebuyerIndicator.Value ? "N" :"Y";
            }
            if (declarationDetail.CitizenshipResidencyType == CitizenshipResidencyType.USCitizen)
            {
                dataApp.aDecCitizen = "Y";
            }
            else if (declarationDetail.CitizenshipResidencyType == CitizenshipResidencyType.PermanentResidentAlien)
            {
                dataApp.aDecCitizen = "N";
                dataApp.aDecResidency = "Y";
            }
            else if (declarationDetail.CitizenshipResidencyType == CitizenshipResidencyType.NonPermanentResidentAlien)
            {
                dataApp.aDecCitizen = "N";
                dataApp.aDecResidency = "N";
            }
        }
        private void Parse(CreditScore creditScore, CAppData dataApp)
        {
            string score = creditScore.CreditScoreDetail.CreditScoreValue;
            switch (creditScore.CreditScoreDetail.CreditRepositorySourceType)
            {
                case CreditRepositorySourceType.Equifax:
                    dataApp.aEquifaxScore_rep = score;
                    break;
                case CreditRepositorySourceType.Experian:
                    dataApp.aExperianScore_rep = score;
                    break;
                case CreditRepositorySourceType.TransUnion:
                    dataApp.aTransUnionScore_rep = score;
                    break;
                case CreditRepositorySourceType.MergedData:
                case CreditRepositorySourceType.Other:
                case CreditRepositorySourceType.Undefined:
                    break;
                default:
                    throw new UnhandledEnumException(creditScore.CreditScoreDetail.CreditRepositorySourceType);
            }
        }
        private void Parse(TaxpayerIdentifier taxpayerIdentifier, CAppData dataApp)
        {
            if (null == taxpayerIdentifier)
            {
                return;
            }
            if (taxpayerIdentifier.TaxpayerIdentifierType == TaxpayerIdentifierType.SocialSecurityNumber)
            {
                dataApp.aSsn = taxpayerIdentifier.TaxpayerIdentifierValue;
            }
        }
        private void Parse(Address address, CAppData dataApp)
        {
            if (address == null)
            {
                return;
            }
            if (address.AddressType != AddressType.Mailing)
            {
                return; // Only handling mailing address now.
            }
            dataApp.aAddrMail = address.AddressLineText;
            dataApp.aCityMail = address.CityName;
            dataApp.aZipMail = address.PostalCode;
            dataApp.aStateMail = address.StateCode;

        }
        private void Parse(Individual individual, CAppData dataApp)
        {
            ULDD.Name name = individual.Name;
            dataApp.aFirstNm = name.FirstName;
            dataApp.aLastNm = name.LastName;
            dataApp.aMidNm = name.MiddleName;
            dataApp.aSuffix = name.SuffixName;
        }
        private void Parse(BorrowerDetail borrowerDetail, CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aAge_rep = borrowerDetail.BorrowerAgeAtApplicationYearsCount;
            dataApp.aDob_rep = borrowerDetail.BorrowerBirthDate;
            if (borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator.HasValue)
            {
                dataApp.aAddrMailUsePresentAddr = borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator.Value;
            }

            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aBaseI_rep = borrowerDetail.BorrowerQualifyingIncomeAmount;
            }
            else
            {
                var consumerId = dataApp.aConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                dataLoan.AddIncomeSource(
                    consumerId,
                    new IncomeSource
                    {
                        IncomeType = LqbGrammar.DataTypes.IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(dataLoan.m_convertLos.ToMoney(borrowerDetail.BorrowerQualifyingIncomeAmount))
                    });
            }
        }
        private string GetPartyRoleIdentifier(Party party)
        {
            if (party.Roles.PartyRoleIdentifiers.PartyRoleIdentifierList.Count == 0)
            {
                return string.Empty;
            }
            return party.Roles.PartyRoleIdentifiers.PartyRoleIdentifierList[0]._PartyRoleIdentifier;
        }

        private CPageData CreateNewLoan()
        {
            AbstractUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(brokerUser, LendersOffice.Audit.E_LoanCreationSource.ImportFromULDD);
            Guid sLId = creator.CreateBlankLoanFile();

            return LoadLoan(sLId);
        }
        private CPageData LoadLoan(Guid sLId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(UlddImporter));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            return dataLoan;

        }

    }
}
