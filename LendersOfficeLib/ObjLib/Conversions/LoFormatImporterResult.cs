﻿namespace LendersOffice.Conversions
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    /// Results class for LoFormatImporter.
    /// </summary>
    public class LoFormatImporterResult
    {
        /// <summary>
        /// Gets or sets the error messages.
        /// </summary>
        /// <value>The error messages from the importer.</value>
        public string ErrorMessages { get; set; }

        /// <summary>
        /// Gets or sets the result status of the import call.
        /// </summary>
        /// <value>The result status of the import call.</value>
        public LoFormatImporterResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets a mapping from field id to an XElement. 
        /// </summary>
        /// <value>A mapping from field id to an XElement.</value>
        /// <remarks>
        /// The XElement is in LO Xml format.
        /// sProHazInsR => &lt;field id="sProHazInsR"&gt;$1.00&lt;/field&gt;.
        /// </remarks>
        public Dictionary<string, XElement> FieldsToOutput { get; set; } = new Dictionary<string, XElement>();
    }
}
